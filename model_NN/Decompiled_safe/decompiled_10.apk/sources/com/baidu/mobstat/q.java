package com.baidu.mobstat;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.baidu.mobstat.a.a;
import com.baidu.mobstat.a.b;
import org.apache.commons.lang.StringUtils;

final class q {
    public static int a(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        try {
            displayMetrics = j(context);
        } catch (Exception e) {
            b.a("createAdReqURL", e);
        }
        return displayMetrics.widthPixels;
    }

    public static String a(Context context, String str) {
        String str2 = StringUtils.EMPTY;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                Object obj = null;
                if (applicationInfo.metaData != null) {
                    obj = applicationInfo.metaData.get(str);
                }
                if (obj == null) {
                    b.a("StatSDK", "null,can't find information for key:" + str);
                    if (str == "BaiduMobAd_STAT_ID") {
                        b.b("不能在manifest.xml中找到APP Key||can't find app key in manifest.xml.");
                        throw new SecurityException("不能在manifest.xml中找到APP Key||can't find app key in manifest.xml.");
                    }
                } else {
                    str2 = obj.toString();
                    if (str2.trim().equals(StringUtils.EMPTY) && str == "BaiduMobAd_STAT_ID") {
                        b.b("APP Key值为空||The value of APP Key is empty.");
                        throw new SecurityException("APP Key值为空||The value of APP Key is empty.");
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            if (str == "BaiduMobAd_STAT_ID") {
                b.b("不能在manifest.xml中找到APP Key||can't find app key in manifest.xml.");
                throw new SecurityException("不能在manifest.xml中找到APP Key||can't find app key in manifest.xml.");
            }
        }
        return str2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(android.content.Context r8, java.lang.String r9, java.lang.String r10, int r11, int r12) {
        /*
            r1 = 1
            r0 = 0
            r2 = 0
            java.net.HttpURLConnection r3 = com.baidu.mobstat.a.a.a(r8, r9, r11, r12)
            r3.setDoOutput(r1)
            r3.setUseCaches(r0)
            java.lang.String r0 = "Content-Type"
            java.lang.String r1 = "gzip"
            r3.setRequestProperty(r0, r1)
            r3.connect()
            java.lang.String r0 = "AdUtil.httpPost connected"
            com.baidu.mobstat.a.b.a(r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.io.BufferedWriter r1 = new java.io.BufferedWriter     // Catch:{ IOException -> 0x00a2 }
            java.io.OutputStreamWriter r4 = new java.io.OutputStreamWriter     // Catch:{ IOException -> 0x00a2 }
            java.util.zip.GZIPOutputStream r5 = new java.util.zip.GZIPOutputStream     // Catch:{ IOException -> 0x00a2 }
            java.io.OutputStream r6 = r3.getOutputStream()     // Catch:{ IOException -> 0x00a2 }
            r5.<init>(r6)     // Catch:{ IOException -> 0x00a2 }
            r4.<init>(r5)     // Catch:{ IOException -> 0x00a2 }
            r1.<init>(r4)     // Catch:{ IOException -> 0x00a2 }
            r1.write(r10)     // Catch:{ IOException -> 0x00aa }
            r1.close()     // Catch:{ IOException -> 0x00aa }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x00a2 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x00a2 }
            java.io.InputStream r5 = r3.getInputStream()     // Catch:{ IOException -> 0x00a2 }
            r4.<init>(r5)     // Catch:{ IOException -> 0x00a2 }
            r1.<init>(r4)     // Catch:{ IOException -> 0x00a2 }
        L_0x0048:
            java.lang.String r4 = r1.readLine()     // Catch:{ IOException -> 0x0052 }
            if (r4 == 0) goto L_0x0061
            r0.append(r4)     // Catch:{ IOException -> 0x0052 }
            goto L_0x0048
        L_0x0052:
            r0 = move-exception
        L_0x0053:
            if (r1 == 0) goto L_0x0058
            r1.close()
        L_0x0058:
            if (r2 == 0) goto L_0x005d
            r2.close()
        L_0x005d:
            r3.disconnect()
            throw r0
        L_0x0061:
            r1.close()     // Catch:{ IOException -> 0x0052 }
            r3.disconnect()     // Catch:{ IOException -> 0x00a2 }
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ IOException -> 0x00a2 }
            r4 = 0
            java.lang.String r5 = "AdUtil.httpPost statuscode"
            r1[r4] = r5     // Catch:{ IOException -> 0x00a2 }
            r4 = 1
            int r5 = r3.getResponseCode()     // Catch:{ IOException -> 0x00a2 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ IOException -> 0x00a2 }
            r1[r4] = r5     // Catch:{ IOException -> 0x00a2 }
            com.baidu.mobstat.a.b.a(r1)     // Catch:{ IOException -> 0x00a2 }
            int r1 = r3.getResponseCode()     // Catch:{ IOException -> 0x00a2 }
            r4 = 200(0xc8, float:2.8E-43)
            if (r1 == r4) goto L_0x00a5
            java.io.IOException r0 = new java.io.IOException     // Catch:{ IOException -> 0x00a2 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00a2 }
            r1.<init>()     // Catch:{ IOException -> 0x00a2 }
            java.lang.String r4 = "http code ="
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ IOException -> 0x00a2 }
            int r4 = r3.getResponseCode()     // Catch:{ IOException -> 0x00a2 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ IOException -> 0x00a2 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x00a2 }
            r0.<init>(r1)     // Catch:{ IOException -> 0x00a2 }
            throw r0     // Catch:{ IOException -> 0x00a2 }
        L_0x00a2:
            r0 = move-exception
            r1 = r2
            goto L_0x0053
        L_0x00a5:
            java.lang.String r0 = r0.toString()
            return r0
        L_0x00aa:
            r0 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.q.a(android.content.Context, java.lang.String, java.lang.String, int, int):java.lang.String");
    }

    public static int b(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        try {
            displayMetrics = j(context);
        } catch (Exception e) {
            b.a("createAdReqURL", e);
        }
        return displayMetrics.heightPixels;
    }

    public static int c(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            b.b("stat", "get app version code exception");
            return 1;
        }
    }

    public static String d(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            b.b("stat", "get app version name exception");
            return StringUtils.EMPTY;
        }
    }

    public static String e(Context context) {
        String format = String.format("%s_%s_%s", 0, 0, 0);
        try {
            if (a.e(context, "android.permission.ACCESS_FINE_LOCATION") || a.e(context, "android.permission.ACCESS_COARSE_LOCATION")) {
                CellLocation cellLocation = ((TelephonyManager) context.getSystemService("phone")).getCellLocation();
                b.a("getLocation cell:", cellLocation + StringUtils.EMPTY);
                if (cellLocation == null) {
                    return format;
                }
                if (cellLocation instanceof GsmCellLocation) {
                    GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                    return String.format("%s_%s_%s", String.format("%d", Integer.valueOf(gsmCellLocation.getCid())), String.format("%d", Integer.valueOf(gsmCellLocation.getLac())), 0);
                }
                String[] split = cellLocation.toString().replace("[", StringUtils.EMPTY).replace("]", StringUtils.EMPTY).split(",");
                return String.format("%s_%s_%s", split[0], split[3], split[4]);
            }
        } catch (Exception e) {
            b.a("getLocation", e);
        }
        return format;
    }

    public static String f(Context context) {
        try {
            if (a.e(context, "android.permission.ACCESS_FINE_LOCATION")) {
                Location lastKnownLocation = ((LocationManager) context.getSystemService("location")).getLastKnownLocation("gps");
                b.a("stat", "location: " + lastKnownLocation);
                if (lastKnownLocation != null) {
                    return String.format("%s_%s_%s", Long.valueOf(lastKnownLocation.getTime()), Double.valueOf(lastKnownLocation.getLongitude()), Double.valueOf(lastKnownLocation.getLatitude()));
                }
            }
        } catch (Exception e) {
            b.a("stat", e);
        }
        return StringUtils.EMPTY;
    }

    public static String g(Context context) {
        Exception e;
        String str;
        try {
            if (!a.e(context, "android.permission.ACCESS_WIFI_STATE")) {
                return StringUtils.EMPTY;
            }
            WifiInfo connectionInfo = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo();
            str = connectionInfo.getMacAddress();
            try {
                b.a(String.format("ssid=%s mac=%s", connectionInfo.getSSID(), connectionInfo.getMacAddress()));
                return str;
            } catch (Exception e2) {
                e = e2;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            str = StringUtils.EMPTY;
            e = exc;
            b.a("stat", e);
            return str;
        }
    }

    public static String h(Context context) {
        Exception exc;
        String str;
        String str2;
        int i;
        int i2;
        int i3 = 0;
        try {
            if (a.e(context, "android.permission.ACCESS_WIFI_STATE")) {
                WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
                if (wifiManager.isWifiEnabled()) {
                    b.a("[d]", wifiManager.getScanResults() + StringUtils.EMPTY);
                    int i4 = Integer.MAX_VALUE;
                    int i5 = -1;
                    while (i3 < wifiManager.getScanResults().size()) {
                        ScanResult scanResult = wifiManager.getScanResults().get(i3);
                        int abs = Math.abs(scanResult.level);
                        b.a(String.format("%s %s_%s", scanResult.SSID, scanResult.BSSID, Integer.valueOf(abs)));
                        if (i4 > abs) {
                            i = i3;
                            i2 = abs;
                        } else {
                            i = i5;
                            i2 = i4;
                        }
                        i3++;
                        i4 = i2;
                        i5 = i;
                    }
                    if (i5 >= 0) {
                        ScanResult scanResult2 = wifiManager.getScanResults().get(i5);
                        str2 = String.format("%s_%s", scanResult2.BSSID.replace(":", (CharSequence) StringUtils.EMPTY).toLowerCase(), Integer.valueOf(Math.abs(scanResult2.level)));
                    } else {
                        str2 = StringUtils.EMPTY;
                    }
                    try {
                        WifiInfo connectionInfo = wifiManager.getConnectionInfo();
                        b.a(String.format("[active]%s %s_%s", connectionInfo.getSSID(), connectionInfo.getMacAddress(), Integer.valueOf(Math.abs(connectionInfo.getRssi()))));
                        return str2;
                    } catch (Exception e) {
                        Exception exc2 = e;
                        str = str2;
                        exc = exc2;
                        b.a("getWifiLocation", exc);
                        return str;
                    }
                }
            }
            return StringUtils.EMPTY;
        } catch (Exception e2) {
            exc = e2;
            str = StringUtils.EMPTY;
            b.a("getWifiLocation", exc);
            return str;
        }
    }

    public static String i(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        String typeName = activeNetworkInfo.getTypeName();
        return (typeName.equals("WIFI") || activeNetworkInfo.getSubtypeName() == null) ? typeName : activeNetworkInfo.getSubtypeName();
    }

    public static DisplayMetrics j(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getApplicationContext().getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }
}
