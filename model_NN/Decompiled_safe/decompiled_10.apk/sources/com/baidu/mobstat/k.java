package com.baidu.mobstat;

import com.baidu.mobstat.a.b;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class k {
    List<l> a = new ArrayList();
    private long b = 0;
    private long c = 0;
    private int d = 0;

    public k() {
        a(System.currentTimeMillis());
    }

    public long a() {
        return this.b;
    }

    public void a(int i) {
        this.d = i;
    }

    public void a(long j) {
        this.b = j;
    }

    public void a(String str, long j) {
        this.a.add(new l(this, str, j));
    }

    public void b() {
        this.b = 0;
        this.c = 0;
        this.d = 0;
        this.a.clear();
        a(System.currentTimeMillis());
    }

    public void b(long j) {
        this.c = j;
    }

    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("s", this.b);
            jSONObject.put("e", this.c);
            jSONObject.put("i", System.currentTimeMillis());
            jSONObject.put("c", this.d);
            JSONArray jSONArray = new JSONArray();
            for (int i = 0; i < this.a.size(); i++) {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("n", this.a.get(i).a());
                jSONObject2.put("d", this.a.get(i).b());
                jSONArray.put(jSONObject2);
            }
            jSONObject.put("p", jSONArray);
        } catch (JSONException e) {
            b.a("stat", "StatSession.constructJSONObject() failed");
        }
        return jSONObject;
    }

    public int d() {
        return this.d;
    }
}
