package com.baidu.mobstat;

import android.app.Activity;
import android.content.Context;
import com.baidu.mobstat.a.b;
import org.apache.commons.lang.StringUtils;

public class StatService {
    public static final int EXCEPTION_LOG = 1;
    private static boolean a = false;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.g.a(boolean, android.content.Context):void
     arg types: [int, android.content.Context]
     candidates:
      com.baidu.mobstat.g.a(com.baidu.mobstat.g, int):int
      com.baidu.mobstat.g.a(com.baidu.mobstat.g, com.baidu.mobstat.SendStrategyEnum):com.baidu.mobstat.SendStrategyEnum
      com.baidu.mobstat.g.a(com.baidu.mobstat.g, java.util.Timer):java.util.Timer
      com.baidu.mobstat.g.a(com.baidu.mobstat.g, boolean):boolean
      com.baidu.mobstat.g.a(boolean, android.content.Context):void */
    private static void a(Context context) {
        if (a(context, "onError(...)")) {
            e.a().a(context.getApplicationContext());
            g.a().a(true, context.getApplicationContext());
        }
    }

    private static boolean a() {
        return a;
    }

    private static boolean a(Context context, String str) {
        if (context != null) {
            return true;
        }
        b.b("stat", str + ":context=null");
        return false;
    }

    private static boolean a(Class<?> cls, String str) {
        StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        b.a("isCalledBy", Integer.valueOf(stackTrace.length), cls, str);
        if (stackTrace.length >= 2) {
            for (int i = 2; i < stackTrace.length; i++) {
                StackTraceElement stackTraceElement = stackTrace[i];
                if (stackTraceElement.getMethodName().equals(str)) {
                    try {
                        Class<?> cls2 = Class.forName(stackTraceElement.getClassName());
                        b.a("isCalledBy", cls2);
                        while (cls2.getSuperclass() != null && cls2.getSuperclass() != cls) {
                            cls2 = cls2.getSuperclass();
                            b.a("isCalledBy", cls2);
                        }
                        if (cls2.getSuperclass() == cls) {
                            b.a("isCalledBy hit!", cls2.getSuperclass(), cls);
                            return true;
                        }
                    } catch (Exception e) {
                        b.a(e);
                    }
                }
            }
        }
        return false;
    }

    private static void b() {
        a = true;
    }

    public static void onEvent(Context context, String str, String str2) {
        onEvent(context, str, str2, 1);
    }

    public static void onEvent(Context context, String str, String str2, int i) {
        if (a(context, "onEvent(...)") && str != null && !str.equals(StringUtils.EMPTY)) {
            if (!f.a().b()) {
                f.a().a(context.getApplicationContext());
            }
            c.a().a(context.getApplicationContext(), str, str2, i);
        }
    }

    public static synchronized void onPause(Context context) {
        synchronized (StatService.class) {
            if (a(context, "onPause(...)")) {
                if (!a(Activity.class, "onPause")) {
                    throw new SecurityException("onPause(Context context)不在Activity.onPause()中被调用||onPause(Context context)is not called in Activity.onPause().");
                }
                m.a().b(context, System.currentTimeMillis());
            }
        }
    }

    public static synchronized void onResume(Context context) {
        synchronized (StatService.class) {
            if (a(context, "onResume(...)")) {
                if (!a(Activity.class, "onResume")) {
                    throw new SecurityException("onResume(Context context)不在Activity.onResume()中被调用||onResume(Context context)is not called in Activity.onResume().");
                }
                if (!f.a().b()) {
                    f.a().a(context.getApplicationContext());
                }
                m.a().a(context, System.currentTimeMillis());
            }
        }
    }

    public static void setAppChannel(String str) {
        b.a().b(str);
    }

    public static void setAppKey(String str) {
        b.a().a(str);
    }

    public static void setLogSenderDelayed(int i) {
        g.a().a(i);
    }

    public static void setOn(Context context, int i) {
        if (a(context, "setOn(...)")) {
            if (!a(Activity.class, "onCreate")) {
                throw new SecurityException("setOn()没有在Activity.onCreate()内被调用||setOn()is not called in Activity.onCreate().");
            } else if (!a()) {
                b();
                if ((i & 1) != 0) {
                    a(context);
                }
            }
        }
    }

    public static void setSendLogStrategy(Context context, SendStrategyEnum sendStrategyEnum, int i) {
        setSendLogStrategy(context, sendStrategyEnum, i, false);
    }

    public static void setSendLogStrategy(Context context, SendStrategyEnum sendStrategyEnum, int i, boolean z) {
        if (a(context, "setSendLogStrategy(...)")) {
            if (!f.a().b()) {
                f.a().a(context.getApplicationContext());
            }
            g.a().a(context.getApplicationContext(), sendStrategyEnum, i, z);
        }
    }
}
