package com.baidu.mobstat.a;

import android.util.Log;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import org.apache.commons.lang.StringUtils;

public final class b {
    private static final SimpleDateFormat a = new SimpleDateFormat("MM-dd HH:mm:ss.SSS");

    static {
        a();
    }

    public static int a(String str) {
        return a("Mobads SDK", str);
    }

    public static int a(String str, String str2) {
        if (!a(3)) {
            return -1;
        }
        b(str, str2);
        return Log.d(str, str2);
    }

    public static int a(String str, Throwable th) {
        if (!a(3)) {
            return -1;
        }
        a("Mobads SDK", str, th);
        return Log.d("Mobads SDK", str, th);
    }

    public static int a(Throwable th) {
        return a(StringUtils.EMPTY, th);
    }

    public static int a(Object... objArr) {
        if (!a(3)) {
            return -1;
        }
        return a(c(objArr));
    }

    public static void a() {
        a.a("_b_sdk.log");
    }

    private static void a(String str, String str2, Throwable th) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        b(str, str2 + "\n" + stringWriter.toString());
        printWriter.close();
        try {
            stringWriter.close();
        } catch (IOException e) {
            Log.w("Log.debug", StringUtils.EMPTY, e);
        }
    }

    public static boolean a(int i) {
        return a("Mobads SDK", i);
    }

    public static boolean a(String str, int i) {
        return i >= 4;
    }

    public static int b(String str) {
        if (!a(6)) {
            return -1;
        }
        b("Mobads SDK", str);
        return Log.e("Mobads SDK", str);
    }

    public static int b(Object... objArr) {
        if (!a(6)) {
            return -1;
        }
        return b(c(objArr));
    }

    private static synchronized void b(String str, String str2) {
        synchronized (b.class) {
        }
    }

    private static String c(Object[] objArr) {
        StringBuilder sb = new StringBuilder();
        for (Object append : objArr) {
            sb.append(append).append(' ');
        }
        return sb.toString();
    }
}
