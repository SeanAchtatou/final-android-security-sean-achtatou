package com.baidu.mobstat.a;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.support.v4.content.IntentCompat;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import org.apache.commons.lang.StringUtils;

public final class a {
    private static final Proxy a = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80));
    private static final Proxy b = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.200", 80));

    public static String a(Context context, String str) {
        b.a("AdUtil.read", str);
        try {
            byte[] b2 = b(context, str);
            if (b2 != null) {
                return new String(b2, "utf-8");
            }
        } catch (Exception e) {
            Log.w("Mobads SDK", "AdUtil.read", e);
        }
        return StringUtils.EMPTY;
    }

    public static String a(boolean z, Context context, String str) {
        return z ? b(str) : a(context, str);
    }

    public static HttpURLConnection a(Context context, String str, int i, int i2) {
        HttpURLConnection httpURLConnection;
        URL url = new URL(str);
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
        NetworkInfo networkInfo2 = connectivityManager.getNetworkInfo(1);
        if (networkInfo2 != null && networkInfo2.isAvailable()) {
            b.a(StringUtils.EMPTY, "WIFI is available");
            httpURLConnection = (HttpURLConnection) url.openConnection();
        } else if (networkInfo == null || !networkInfo.isAvailable()) {
            b.a(StringUtils.EMPTY, "getConnection:not wifi and mobile");
            httpURLConnection = (HttpURLConnection) url.openConnection();
        } else {
            String extraInfo = networkInfo.getExtraInfo();
            String lowerCase = extraInfo != null ? extraInfo.toLowerCase() : StringUtils.EMPTY;
            b.a("current APN", lowerCase);
            httpURLConnection = (lowerCase.startsWith("cmwap") || lowerCase.startsWith("uniwap") || lowerCase.startsWith("3gwap")) ? (HttpURLConnection) url.openConnection(a) : lowerCase.startsWith("ctwap") ? (HttpURLConnection) url.openConnection(b) : (HttpURLConnection) url.openConnection();
        }
        httpURLConnection.setConnectTimeout(i);
        httpURLConnection.setReadTimeout(i2);
        return httpURLConnection;
    }

    public static void a(Context context, String str, String str2, boolean z) {
        boolean z2 = false;
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = context.openFileOutput(str, z ? IntentCompat.FLAG_ACTIVITY_CLEAR_TASK : 0);
            if (fileOutputStream != null) {
                fileOutputStream.write(str2.getBytes("utf-8"));
            } else {
                StringBuilder append = new StringBuilder().append("AdUtil.write fout is null:");
                if (fileOutputStream == null) {
                    z2 = true;
                }
                Log.w("Mobads SDK", append.append(z2).toString());
            }
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (Exception e) {
                    Log.w("Mobads SDK", "AdUtil.write", e);
                }
            }
        } catch (Exception e2) {
            Log.w("Mobads SDK", "AdUtil.write", e2);
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (Exception e3) {
                    Log.w("Mobads SDK", "AdUtil.write", e3);
                }
            }
        } catch (Throwable th) {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (Exception e4) {
                    Log.w("Mobads SDK", "AdUtil.write", e4);
                }
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0065 A[SYNTHETIC, Splitter:B:21:0x0065] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x007c A[SYNTHETIC, Splitter:B:30:0x007c] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x008c A[SYNTHETIC, Splitter:B:36:0x008c] */
    /* JADX WARNING: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.String r4, java.lang.String r5, boolean r6) {
        /*
            java.lang.String r0 = android.os.Environment.getExternalStorageState()
            java.lang.String r1 = "mounted"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x000d
        L_0x000c:
            return
        L_0x000d:
            r1 = 0
            java.io.File r0 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0072 }
            java.io.File r3 = new java.io.File     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0072 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0072 }
            r2.<init>()     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0072 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0072 }
            java.lang.String r2 = java.io.File.separator     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0072 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0072 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0072 }
            java.lang.String r0 = r0.toString()     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0072 }
            r3.<init>(r0)     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0072 }
            boolean r0 = r3.exists()     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0072 }
            if (r0 != 0) goto L_0x003e
            java.io.File r0 = r3.getParentFile()     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0072 }
            r0.mkdirs()     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0072 }
            r3.createNewFile()     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0072 }
        L_0x003e:
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0072 }
            r2.<init>(r3, r6)     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0072 }
            java.lang.String r0 = "utf-8"
            byte[] r0 = r5.getBytes(r0)     // Catch:{ FileNotFoundException -> 0x009f, IOException -> 0x009c, all -> 0x0099 }
            r2.write(r0)     // Catch:{ FileNotFoundException -> 0x009f, IOException -> 0x009c, all -> 0x0099 }
            if (r2 == 0) goto L_0x000c
            r2.close()     // Catch:{ IOException -> 0x0052 }
            goto L_0x000c
        L_0x0052:
            r0 = move-exception
            java.lang.String r1 = "Mobads SDK"
            java.lang.String r2 = "AdUtil.writeExt"
            android.util.Log.w(r1, r2, r0)
            goto L_0x000c
        L_0x005b:
            r0 = move-exception
        L_0x005c:
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.writeExt"
            android.util.Log.e(r2, r3, r0)     // Catch:{ all -> 0x0089 }
            if (r1 == 0) goto L_0x000c
            r1.close()     // Catch:{ IOException -> 0x0069 }
            goto L_0x000c
        L_0x0069:
            r0 = move-exception
            java.lang.String r1 = "Mobads SDK"
            java.lang.String r2 = "AdUtil.writeExt"
            android.util.Log.w(r1, r2, r0)
            goto L_0x000c
        L_0x0072:
            r0 = move-exception
        L_0x0073:
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.writeExt"
            android.util.Log.e(r2, r3, r0)     // Catch:{ all -> 0x0089 }
            if (r1 == 0) goto L_0x000c
            r1.close()     // Catch:{ IOException -> 0x0080 }
            goto L_0x000c
        L_0x0080:
            r0 = move-exception
            java.lang.String r1 = "Mobads SDK"
            java.lang.String r2 = "AdUtil.writeExt"
            android.util.Log.w(r1, r2, r0)
            goto L_0x000c
        L_0x0089:
            r0 = move-exception
        L_0x008a:
            if (r1 == 0) goto L_0x008f
            r1.close()     // Catch:{ IOException -> 0x0090 }
        L_0x008f:
            throw r0
        L_0x0090:
            r1 = move-exception
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.writeExt"
            android.util.Log.w(r2, r3, r1)
            goto L_0x008f
        L_0x0099:
            r0 = move-exception
            r1 = r2
            goto L_0x008a
        L_0x009c:
            r0 = move-exception
            r1 = r2
            goto L_0x0073
        L_0x009f:
            r0 = move-exception
            r1 = r2
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.a.a.a(java.lang.String, java.lang.String, boolean):void");
    }

    public static void a(boolean z, Context context, String str, String str2, boolean z2) {
        if (z) {
            a(str, str2, z2);
        } else {
            a(context, str, str2, z2);
        }
    }

    public static boolean a(String str) {
        b.a("AdUtil.deleteExt", str);
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return false;
        }
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + str);
        if (file.exists()) {
            return file.delete();
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0075 A[SYNTHETIC, Splitter:B:24:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x008f A[SYNTHETIC, Splitter:B:35:0x008f] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00a2 A[SYNTHETIC, Splitter:B:43:0x00a2] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String b(java.lang.String r5) {
        /*
            java.lang.String r0 = "AdUtil.readExt"
            com.baidu.mobstat.a.b.a(r0, r5)
            java.lang.String r0 = android.os.Environment.getExternalStorageState()
            java.lang.String r1 = "mounted"
            boolean r1 = r1.equals(r0)
            if (r1 != 0) goto L_0x001c
            java.lang.String r1 = "mounted_ro"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x001c
            java.lang.String r1 = ""
        L_0x001b:
            return r1
        L_0x001c:
            java.io.File r0 = android.os.Environment.getExternalStorageDirectory()
            java.io.File r4 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = java.io.File.separator
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r0 = r0.toString()
            r4.<init>(r0)
            java.lang.String r1 = ""
            boolean r0 = r4.exists()
            if (r0 == 0) goto L_0x001b
            r3 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x006a, IOException -> 0x0084, all -> 0x009e }
            r2.<init>(r4)     // Catch:{ FileNotFoundException -> 0x006a, IOException -> 0x0084, all -> 0x009e }
            int r0 = r2.available()     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00b1 }
            byte[] r3 = new byte[r0]     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00b1 }
            r2.read(r3)     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00b1 }
            java.lang.String r0 = new java.lang.String     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00b1 }
            java.lang.String r4 = "utf-8"
            r0.<init>(r3, r4)     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00b1 }
            if (r2 == 0) goto L_0x005f
            r2.close()     // Catch:{ IOException -> 0x0061 }
        L_0x005f:
            r1 = r0
            goto L_0x001b
        L_0x0061:
            r1 = move-exception
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.readExt"
            android.util.Log.w(r2, r3, r1)
            goto L_0x005f
        L_0x006a:
            r0 = move-exception
            r2 = r3
        L_0x006c:
            java.lang.String r3 = "Mobads SDK"
            java.lang.String r4 = "AdUtil.readExt"
            android.util.Log.e(r3, r4, r0)     // Catch:{ all -> 0x00af }
            if (r2 == 0) goto L_0x0078
            r2.close()     // Catch:{ IOException -> 0x007a }
        L_0x0078:
            r0 = r1
            goto L_0x005f
        L_0x007a:
            r0 = move-exception
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.readExt"
            android.util.Log.w(r2, r3, r0)
            r0 = r1
            goto L_0x005f
        L_0x0084:
            r0 = move-exception
            r2 = r3
        L_0x0086:
            java.lang.String r3 = "Mobads SDK"
            java.lang.String r4 = "AdUtil.readExt"
            android.util.Log.e(r3, r4, r0)     // Catch:{ all -> 0x00af }
            if (r2 == 0) goto L_0x0092
            r2.close()     // Catch:{ IOException -> 0x0094 }
        L_0x0092:
            r0 = r1
            goto L_0x005f
        L_0x0094:
            r0 = move-exception
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.readExt"
            android.util.Log.w(r2, r3, r0)
            r0 = r1
            goto L_0x005f
        L_0x009e:
            r0 = move-exception
            r2 = r3
        L_0x00a0:
            if (r2 == 0) goto L_0x00a5
            r2.close()     // Catch:{ IOException -> 0x00a6 }
        L_0x00a5:
            throw r0
        L_0x00a6:
            r1 = move-exception
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.readExt"
            android.util.Log.w(r2, r3, r1)
            goto L_0x00a5
        L_0x00af:
            r0 = move-exception
            goto L_0x00a0
        L_0x00b1:
            r0 = move-exception
            goto L_0x0086
        L_0x00b3:
            r0 = move-exception
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.a.a.b(java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0030, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0031, code lost:
        android.util.Log.e("Mobads SDK", "AdUtil.readBinary", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0062, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0063, code lost:
        r2 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0065, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0066, code lost:
        r5 = r0;
        r0 = null;
        r2 = r1;
        r1 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0071, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0072, code lost:
        r5 = r0;
        r0 = null;
        r2 = r1;
        r1 = r5;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x002c A[SYNTHETIC, Splitter:B:21:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0045 A[SYNTHETIC, Splitter:B:31:0x0045] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0055 A[SYNTHETIC, Splitter:B:37:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0062 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:4:0x0007] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static byte[] b(android.content.Context r6, java.lang.String r7) {
        /*
            r2 = 0
            java.io.FileInputStream r1 = r6.openFileInput(r7)     // Catch:{ FileNotFoundException -> 0x0020, IOException -> 0x0039 }
            if (r1 == 0) goto L_0x007d
            int r0 = r1.available()     // Catch:{ FileNotFoundException -> 0x0071, IOException -> 0x0065, all -> 0x0062 }
            byte[] r2 = new byte[r0]     // Catch:{ FileNotFoundException -> 0x0071, IOException -> 0x0065, all -> 0x0062 }
            r1.read(r2)     // Catch:{ FileNotFoundException -> 0x0077, IOException -> 0x006b, all -> 0x0062 }
            r0 = r2
        L_0x0011:
            if (r1 == 0) goto L_0x0016
            r1.close()     // Catch:{ IOException -> 0x0017 }
        L_0x0016:
            return r0
        L_0x0017:
            r1 = move-exception
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.readBinary"
            android.util.Log.e(r2, r3, r1)
            goto L_0x0016
        L_0x0020:
            r0 = move-exception
            r1 = r0
            r0 = r2
        L_0x0023:
            java.lang.String r3 = "Mobads SDK"
            java.lang.String r4 = "AdUtil.readBinary"
            android.util.Log.e(r3, r4, r1)     // Catch:{ all -> 0x0052 }
            if (r2 == 0) goto L_0x0016
            r2.close()     // Catch:{ IOException -> 0x0030 }
            goto L_0x0016
        L_0x0030:
            r1 = move-exception
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.readBinary"
            android.util.Log.e(r2, r3, r1)
            goto L_0x0016
        L_0x0039:
            r0 = move-exception
            r1 = r0
            r0 = r2
        L_0x003c:
            java.lang.String r3 = "Mobads SDK"
            java.lang.String r4 = "AdUtil.readBinary"
            android.util.Log.e(r3, r4, r1)     // Catch:{ all -> 0x0052 }
            if (r2 == 0) goto L_0x0016
            r2.close()     // Catch:{ IOException -> 0x0049 }
            goto L_0x0016
        L_0x0049:
            r1 = move-exception
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.readBinary"
            android.util.Log.e(r2, r3, r1)
            goto L_0x0016
        L_0x0052:
            r0 = move-exception
        L_0x0053:
            if (r2 == 0) goto L_0x0058
            r2.close()     // Catch:{ IOException -> 0x0059 }
        L_0x0058:
            throw r0
        L_0x0059:
            r1 = move-exception
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.readBinary"
            android.util.Log.e(r2, r3, r1)
            goto L_0x0058
        L_0x0062:
            r0 = move-exception
            r2 = r1
            goto L_0x0053
        L_0x0065:
            r0 = move-exception
            r5 = r0
            r0 = r2
            r2 = r1
            r1 = r5
            goto L_0x003c
        L_0x006b:
            r0 = move-exception
            r5 = r0
            r0 = r2
            r2 = r1
            r1 = r5
            goto L_0x003c
        L_0x0071:
            r0 = move-exception
            r5 = r0
            r0 = r2
            r2 = r1
            r1 = r5
            goto L_0x0023
        L_0x0077:
            r0 = move-exception
            r5 = r0
            r0 = r2
            r2 = r1
            r1 = r5
            goto L_0x0023
        L_0x007d:
            r0 = r2
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.a.a.b(android.content.Context, java.lang.String):byte[]");
    }

    public static void c(String str) {
        b.b("BaiduMobAds SDK", str);
        throw new SecurityException(str);
    }

    public static boolean c(Context context, String str) {
        boolean exists = context.getFileStreamPath(str).exists();
        b.a("AdUtil.exists", exists + " " + str);
        return exists;
    }

    public static void d(Context context, String str) {
        if (!e(context, str)) {
            c("You need the " + str + " permission. Open AndroidManifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"" + str + "\" />");
        }
    }

    public static boolean e(Context context, String str) {
        boolean z = context.checkCallingOrSelfPermission(str) != -1;
        b.a("hasPermission ", z + " | " + str);
        return z;
    }
}
