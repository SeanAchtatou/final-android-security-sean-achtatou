package com.baidu.mobstat;

import android.content.Context;
import com.baidu.mobstat.a.b;

class f extends Thread {
    private static f a = new f();
    private Context b;
    private boolean c = false;
    private boolean d = false;

    private f() {
    }

    public static f a() {
        return a;
    }

    private void d() {
        this.c = true;
    }

    private synchronized void e() {
        this.d = true;
    }

    public void a(Context context) {
        if (context != null && !b()) {
            this.b = context;
            d();
            start();
        }
    }

    public boolean b() {
        return this.c;
    }

    public synchronized boolean c() {
        return this.d;
    }

    public void run() {
        g.a().b(this.b);
        b.a().d(this.b);
        b.a().c(this.b);
        e();
        synchronized (a) {
            try {
                notifyAll();
            } catch (IllegalMonitorStateException e) {
                b.a("stat", e);
            }
        }
        b.a().a(this.b);
        b.a().b();
        g.a().c(this.b);
    }
}
