package com.baidu.mobstat;

import android.content.Context;
import com.baidu.mobstat.a.b;
import org.json.JSONArray;
import org.json.JSONObject;

class e {
    private static e a = new e();
    private boolean b = false;

    private e() {
    }

    public static e a() {
        return a;
    }

    public void a(Context context) {
        b.a("stat", "openExceptonAnalysis");
        if (!this.b) {
            this.b = true;
            a.a().a(context);
        }
    }

    public void b(Context context) {
        if (context == null) {
            b.a("stat", "exceptonAnalysis, context=null");
            return;
        }
        JSONArray b2 = a.a().b(context);
        if (b2 == null) {
            b.a("stat", "no exception str");
            return;
        }
        b.a("stat", "move exception cache to stat cache");
        int i = 0;
        while (true) {
            try {
                int i2 = i;
                if (i2 < b2.length()) {
                    JSONObject jSONObject = (JSONObject) b2.get(i2);
                    b.a().a(jSONObject.getLong("t"), jSONObject.getString("c"));
                    b.a().b(context);
                    i = i2 + 1;
                } else {
                    return;
                }
            } catch (Exception e) {
                b.a("stat", e);
                return;
            }
        }
    }
}
