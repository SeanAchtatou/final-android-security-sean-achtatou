package com.baidu.mobstat;

import android.content.Context;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.baidu.mobstat.a.b;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

class a implements Thread.UncaughtExceptionHandler {
    private static a a = new a();
    private Thread.UncaughtExceptionHandler b = null;
    private Context c = null;

    private a() {
    }

    public static a a() {
        return a;
    }

    private void a(long j, String str) {
        if (this.c != null && str != null && !str.trim().equals(StringUtils.EMPTY)) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("t", j);
                jSONObject.put("c", str);
                JSONArray b2 = b(this.c);
                if (b2 == null) {
                    b2 = new JSONArray();
                }
                b2.put(jSONObject);
                FileOutputStream openFileOutput = this.c.openFileOutput("__local_except_cache.json", 0);
                openFileOutput.write(b2.toString().getBytes());
                openFileOutput.flush();
                openFileOutput.close();
                b.a("SDKCrashHandler", "Save Exception String Successlly");
            } catch (Exception e) {
                b.a("SDKCrashHandler", e);
            }
        }
    }

    public void a(Context context) {
        if (this.b == null) {
            this.b = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(this);
        }
        if (this.c == null) {
            this.c = context.getApplicationContext();
        }
    }

    /* access modifiers changed from: protected */
    public JSONArray b(Context context) {
        JSONArray jSONArray = null;
        if (context != null) {
            File file = new File(context.getFilesDir(), "__local_except_cache.json");
            try {
                if (file.exists()) {
                    FileInputStream openFileInput = context.openFileInput("__local_except_cache.json");
                    StringBuffer stringBuffer = new StringBuffer();
                    byte[] bArr = new byte[AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END];
                    while (true) {
                        int read = openFileInput.read(bArr);
                        if (read == -1) {
                            break;
                        }
                        stringBuffer.append(new String(bArr, 0, read));
                    }
                    openFileInput.close();
                    jSONArray = stringBuffer.length() != 0 ? new JSONArray(stringBuffer.toString()) : null;
                    try {
                        file.delete();
                    } catch (Exception e) {
                        b.a("SDKCrashHandler", e);
                    }
                }
            } catch (Exception e2) {
                b.a("SDKCrashHandler", e2);
            }
        }
        return jSONArray;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        printWriter.close();
        String obj = stringWriter.toString();
        b.a("SDKCrashHandler", obj);
        a(System.currentTimeMillis(), obj);
        if (!this.b.equals(this)) {
            this.b.uncaughtException(thread, th);
        }
    }
}
