package com.baidu.mobstat;

import android.app.Activity;
import android.content.Context;
import com.baidu.mobstat.a.b;
import java.lang.ref.WeakReference;

class o implements Runnable {
    final /* synthetic */ m a;
    private long b;
    private WeakReference<Context> c;
    private long d;
    private WeakReference<Context> e;

    public o(m mVar, long j, Context context, long j2, Context context2) {
        this.a = mVar;
        this.b = j;
        this.c = new WeakReference<>(context);
        this.d = j2;
        this.e = new WeakReference<>(context2);
    }

    public void run() {
        if (this.c.get() != this.e.get()) {
            b.b("stat", "onPause() 或 onResume()安放错误||onPause() or onResume() install error.");
            return;
        }
        long j = this.b - this.d;
        Activity activity = (Activity) this.c.get();
        if (activity == null) {
            b.b("stat", "onPause,WeakReference is already been released");
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(activity.getComponentName().getShortClassName());
        if (sb.charAt(0) == '.') {
            sb.deleteCharAt(0);
        }
        b.a("stat", "new page view, page name = " + sb.toString() + ",stay time = " + j + "(ms)");
        this.a.f.a(sb.toString(), j);
        this.a.c(this.c.get(), this.b);
    }
}
