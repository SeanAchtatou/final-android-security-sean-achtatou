package com.baidu.mobstat;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import com.baidu.mobstat.a.b;
import java.util.Date;
import java.util.Timer;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

class g {
    private static HandlerThread a = new HandlerThread("LogSenderThread");
    /* access modifiers changed from: private */
    public static Handler h;
    private static g i = new g();
    /* access modifiers changed from: private */
    public boolean b = false;
    /* access modifiers changed from: private */
    public SendStrategyEnum c = SendStrategyEnum.APP_START;
    /* access modifiers changed from: private */
    public int d = 1;
    /* access modifiers changed from: private */
    public Timer e;
    /* access modifiers changed from: private */
    public int f = 0;
    /* access modifiers changed from: private */
    public boolean g = false;

    private g() {
        a.start();
        h = new Handler(a.getLooper());
    }

    public static g a() {
        return i;
    }

    public SharedPreferences a(Context context) {
        return context.getSharedPreferences("__Baidu_Stat_SDK_SendRem", 0);
    }

    public void a(int i2) {
        if (i2 >= 0 && i2 <= 30) {
            this.f = i2;
        }
    }

    public void a(Context context, SendStrategyEnum sendStrategyEnum, int i2, boolean z) {
        SharedPreferences.Editor edit = a(context).edit();
        if (!sendStrategyEnum.equals(SendStrategyEnum.SET_TIME_INTERVAL)) {
            this.c = sendStrategyEnum;
            edit.putInt("sendLogtype", this.c.ordinal());
            if (sendStrategyEnum.equals(SendStrategyEnum.ONCE_A_DAY)) {
                edit.putInt("timeinterval", 24);
            }
        } else if (i2 <= 0 || i2 > 24) {
            b.b("setSendLogStrategy", "time_interval is invalid, new strategy does not work");
        } else {
            this.d = i2;
            this.c = SendStrategyEnum.SET_TIME_INTERVAL;
            edit.putInt("sendLogtype", this.c.ordinal());
            edit.putInt("timeinterval", this.d);
        }
        this.b = z;
        edit.putBoolean("onlywifi", this.b);
        b.a("Mobads SDK", "sstype is:" + this.c.name() + " And time_interval is:" + this.d + " And m_only_wifi:" + this.b);
        edit.commit();
    }

    public void a(boolean z, Context context) {
        this.g = z;
        SharedPreferences.Editor edit = a(context).edit();
        edit.putBoolean("exceptionanalysisflag", this.g);
        b.a("Mobads SDK", "APP_ANALYSIS_EXCEPTION is:" + this.g);
        edit.commit();
    }

    public void b(Context context) {
        SendStrategyEnum sendStrategyEnum = SendStrategyEnum.APP_START;
        SharedPreferences.Editor edit = a(context).edit();
        try {
            String a2 = q.a(context, "BaiduMobAd_EXCEPTION_LOG");
            if (!a2.equals(StringUtils.EMPTY)) {
                if (a2.equals("true")) {
                    e.a().a(context);
                    edit.putBoolean("exceptionanalysisflag", true);
                } else if (a2.equals("false")) {
                    edit.putBoolean("exceptionanalysisflag", false);
                }
            }
        } catch (Exception e2) {
            b.a(e2);
        }
        try {
            String a3 = q.a(context, "BaiduMobAd_SEND_STRATEGY");
            if (!a3.equals(StringUtils.EMPTY)) {
                if (a3.equals(SendStrategyEnum.APP_START.name())) {
                    sendStrategyEnum = SendStrategyEnum.APP_START;
                    edit.putInt("sendLogtype", SendStrategyEnum.APP_START.ordinal());
                } else if (a3.equals(SendStrategyEnum.ONCE_A_DAY.name())) {
                    sendStrategyEnum = SendStrategyEnum.ONCE_A_DAY;
                    edit.putInt("sendLogtype", SendStrategyEnum.ONCE_A_DAY.ordinal());
                    edit.putInt("timeinterval", 24);
                } else if (a3.equals(SendStrategyEnum.SET_TIME_INTERVAL.name())) {
                    sendStrategyEnum = SendStrategyEnum.SET_TIME_INTERVAL;
                    edit.putInt("sendLogtype", SendStrategyEnum.SET_TIME_INTERVAL.ordinal());
                }
            }
        } catch (Exception e3) {
            Exception exc = e3;
            b.a(exc);
            sendStrategyEnum = sendStrategyEnum;
        }
        try {
            String a4 = q.a(context, "BaiduMobAd_TIME_INTERVAL");
            if (!a4.equals(StringUtils.EMPTY)) {
                int parseInt = Integer.parseInt(a4);
                if (sendStrategyEnum.ordinal() == SendStrategyEnum.SET_TIME_INTERVAL.ordinal() && parseInt > 0 && parseInt <= 24) {
                    edit.putInt("timeinterval", parseInt);
                }
            }
        } catch (Exception e4) {
            b.a(e4);
        }
        try {
            String a5 = q.a(context, "BaiduMobAd_ONLY_WIFI");
            if (!a5.equals(StringUtils.EMPTY)) {
                if (a5.equals("true")) {
                    edit.putBoolean("onlywifi", true);
                } else if (a5.equals("false")) {
                    edit.putBoolean("onlywifi", false);
                }
            }
        } catch (Exception e5) {
            b.a(e5);
        }
        edit.commit();
    }

    public void c(Context context) {
        h.post(new h(this, context));
    }

    public void d(Context context) {
        a(context).edit().putLong("lastsendtime", new Date().getTime()).commit();
    }

    public void e(Context context) {
        this.e = new Timer();
        this.e.schedule(new j(this, context), (long) (this.d * DateUtils.MILLIS_IN_HOUR), (long) (this.d * DateUtils.MILLIS_IN_HOUR));
    }
}
