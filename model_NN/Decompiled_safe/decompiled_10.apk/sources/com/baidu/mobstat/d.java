package com.baidu.mobstat;

import android.content.Context;
import com.baidu.mobstat.a.b;

class d implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ String b;
    final /* synthetic */ int c;
    final /* synthetic */ Context d;
    final /* synthetic */ c e;

    d(c cVar, String str, String str2, int i, Context context) {
        this.e = cVar;
        this.a = str;
        this.b = str2;
        this.c = i;
        this.d = context;
    }

    public void run() {
        if (!f.a().c()) {
            synchronized (f.a()) {
                try {
                    f.a().wait();
                } catch (InterruptedException e2) {
                    b.a("stat", e2);
                }
            }
        }
        b.a().a(this.a, this.b, this.c, System.currentTimeMillis());
        b.a().b(this.d);
    }
}
