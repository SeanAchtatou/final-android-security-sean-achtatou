package com.baidu.mobstat;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.Timer;

class h implements Runnable {
    final /* synthetic */ Context a;
    final /* synthetic */ g b;

    h(g gVar, Context context) {
        this.b = gVar;
        this.a = context;
    }

    public void run() {
        SharedPreferences a2 = this.b.a(this.a);
        boolean unused = this.b.g = a2.getBoolean("exceptionanalysisflag", false);
        if (this.b.g) {
            e.a().b(this.a);
        }
        if (this.b.e != null) {
            this.b.e.cancel();
            Timer unused2 = this.b.e = (Timer) null;
        }
        SendStrategyEnum unused3 = this.b.c = SendStrategyEnum.values()[a2.getInt("sendLogtype", 0)];
        int unused4 = this.b.d = a2.getInt("timeinterval", 1);
        boolean unused5 = this.b.b = a2.getBoolean("onlywifi", false);
        if (this.b.c.equals(SendStrategyEnum.SET_TIME_INTERVAL)) {
            this.b.e(this.a);
        } else if (this.b.c.equals(SendStrategyEnum.ONCE_A_DAY)) {
            this.b.e(this.a);
        }
        g.h.postDelayed(new i(this), (long) (this.b.f * 1000));
    }
}
