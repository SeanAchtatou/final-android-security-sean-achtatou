package com.baidu.mobstat;

import android.content.Context;
import com.baidu.mobstat.a.b;
import java.lang.ref.WeakReference;

class p implements Runnable {
    final /* synthetic */ m a;
    private long b;
    private long c;
    private WeakReference<Context> d;

    public p(m mVar, long j, long j2, Context context) {
        this.a = mVar;
        this.b = j;
        this.c = j2;
        this.d = new WeakReference<>(context);
    }

    public void run() {
        if (this.c - this.b >= 30000 && this.b > 0 && this.d.get() != null) {
            this.a.f.b(this.b);
            String jSONObject = this.a.f.c().toString();
            b.a("stat", "new session:" + jSONObject);
            b.a().c(jSONObject);
            b.a().b(this.d.get());
            this.a.f.b();
            this.a.a(this.d.get());
        }
    }
}
