package com.baidu.mobstat;

import android.app.Activity;
import com.baidu.mobstat.a.b;

public class StatActivity extends Activity {
    public void onPause() {
        super.onPause();
        b.a("stat", "StatActivity.OnResume()");
        StatService.onPause(this);
    }

    public void onResume() {
        super.onResume();
        b.a("stat", "StatActivity.OnResume()");
        StatService.onResume(this);
    }
}
