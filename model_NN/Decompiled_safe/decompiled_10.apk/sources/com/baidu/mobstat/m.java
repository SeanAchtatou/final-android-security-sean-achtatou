package com.baidu.mobstat;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import com.baidu.mobstat.a.a;
import com.baidu.mobstat.a.b;
import java.lang.ref.WeakReference;
import org.json.JSONException;
import org.json.JSONObject;

class m {
    private static HandlerThread a = new HandlerThread("SessionAnalysisThread");
    private static Handler b;
    private static m g = new m();
    private long c = 0;
    private long d = 0;
    private WeakReference<Context> e;
    /* access modifiers changed from: private */
    public k f = new k();
    private boolean h = true;
    private boolean i = false;

    private m() {
        a.start();
        b = new Handler(a.getLooper());
    }

    public static m a() {
        return g;
    }

    /* access modifiers changed from: private */
    public void a(Context context) {
        if (context == null) {
            b.a("stat", "clearLastSession(Context context):context=null");
        } else {
            a.a(false, context, "__local_last_session.json", "{}", false);
        }
    }

    private void a(boolean z) {
        this.h = z;
    }

    /* access modifiers changed from: private */
    public void c(Context context, long j) {
        b.a("stat", "flush current session to last_session.json");
        new JSONObject();
        JSONObject c2 = this.f.c();
        try {
            c2.put("e", j);
        } catch (JSONException e2) {
            b.a("stat", "StatSession.flushSession() failed");
        }
        a.a(false, context, "__local_last_session.json", c2.toString(), false);
    }

    private boolean d() {
        return this.h;
    }

    public void a(Context context, long j) {
        b.a("stat", "post resume job");
        if (this.i) {
            b.b("stat", "遗漏StatService.onPause() || missing StatService.onPause()");
        }
        this.i = true;
        if (d()) {
            a(false);
            b.post(new n(this));
        }
        b.post(new p(this, this.c, j, context));
        this.e = new WeakReference<>(context);
        this.d = j;
    }

    public void b() {
        this.f.a(this.f.d() + 1);
    }

    public void b(Context context, long j) {
        b.a("stat", "post pause job");
        if (!this.i) {
            b.b("stat", "遗漏StatService.onResume() || missing StatService.onResume()");
            return;
        }
        this.i = false;
        b.post(new o(this, j, context, this.d, this.e.get()));
        this.c = j;
    }

    public long c() {
        return this.f.a();
    }
}
