package com.baidu.mapapi;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.HashMap;
import org.apache.commons.lang.StringUtils;

public class d {
    static Context a;
    static HashMap<String, SoftReference<u>> b = new HashMap<>();
    public static boolean c = false;
    public static int d = 4;
    public static boolean e = false;
    public static byte f = 0;
    public static String g = "10.0.0.200";
    public static int h = 80;

    public interface a {
        void onError(int i, int i2, String str, Object obj);

        void onOk(int i, int i2, String str, Object obj);
    }

    public static HttpURLConnection a(String str) throws IOException {
        String substring;
        String substring2;
        if (!c) {
            a();
            if (!c) {
                return null;
            }
        }
        if (!e) {
            return (HttpURLConnection) new URL(str).openConnection();
        }
        int indexOf = str.indexOf(47, 7);
        if (indexOf < 0) {
            substring = str.substring(7);
            substring2 = StringUtils.EMPTY;
        } else {
            substring = str.substring(7, indexOf);
            substring2 = str.substring(indexOf);
        }
        if (f == 1) {
            return (HttpURLConnection) new URL(str).openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(g, 80)));
        }
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("http://" + g + substring2).openConnection();
        httpURLConnection.setRequestProperty("X-Online-Host", substring);
        return httpURLConnection;
    }

    public static void a() {
        ConnectivityManager connectivityManager = null;
        if (a != null) {
            connectivityManager = (ConnectivityManager) a.getSystemService("connectivity");
        }
        if (connectivityManager != null) {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                a(activeNetworkInfo, activeNetworkInfo.isConnected());
            } else {
                c = false;
            }
        } else {
            c = false;
        }
    }

    public static void a(final int i, final int i2, final String str, final a aVar) {
        if (str != null && str.startsWith("http://")) {
            new Thread() {
                /* JADX INFO: additional move instructions added (1) to help type inference */
                /* JADX WARN: Type inference failed for: r1v1 */
                /* JADX WARN: Type inference failed for: r1v2, types: [java.net.HttpURLConnection] */
                /* JADX WARN: Type inference failed for: r1v3 */
                /* JADX WARN: Type inference failed for: r1v4, types: [java.net.HttpURLConnection] */
                /* JADX WARN: Type inference failed for: r1v7 */
                /* JADX WARNING: Multi-variable type inference failed */
                /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
                /* JADX WARNING: Removed duplicated region for block: B:58:0x00cd A[SYNTHETIC, Splitter:B:58:0x00cd] */
                /* JADX WARNING: Removed duplicated region for block: B:61:0x00d2 A[SYNTHETIC, Splitter:B:61:0x00d2] */
                /* JADX WARNING: Unknown variable types count: 1 */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r9 = this;
                        r2 = 0
                        java.util.HashMap<java.lang.String, java.lang.ref.SoftReference<com.baidu.mapapi.u>> r3 = com.baidu.mapapi.d.b
                        monitor-enter(r3)
                        java.util.HashMap<java.lang.String, java.lang.ref.SoftReference<com.baidu.mapapi.u>> r0 = com.baidu.mapapi.d.b     // Catch:{ all -> 0x008a }
                        java.lang.String r1 = r3     // Catch:{ all -> 0x008a }
                        java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x008a }
                        java.lang.ref.SoftReference r0 = (java.lang.ref.SoftReference) r0     // Catch:{ all -> 0x008a }
                        if (r0 == 0) goto L_0x0025
                        java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x008a }
                        com.baidu.mapapi.u r0 = (com.baidu.mapapi.u) r0     // Catch:{ all -> 0x008a }
                        if (r0 == 0) goto L_0x0025
                        com.baidu.mapapi.d$a r1 = r4     // Catch:{ all -> 0x008a }
                        int r2 = r1     // Catch:{ all -> 0x008a }
                        int r4 = r2     // Catch:{ all -> 0x008a }
                        java.lang.String r5 = r3     // Catch:{ all -> 0x008a }
                        r1.onOk(r2, r4, r5, r0)     // Catch:{ all -> 0x008a }
                        monitor-exit(r3)     // Catch:{ all -> 0x008a }
                    L_0x0024:
                        return
                    L_0x0025:
                        java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x008a }
                        r4.<init>()     // Catch:{ all -> 0x008a }
                        java.lang.String r0 = r3     // Catch:{ Exception -> 0x00dd, all -> 0x00c9 }
                        java.net.HttpURLConnection r1 = com.baidu.mapapi.d.a(r0)     // Catch:{ Exception -> 0x00dd, all -> 0x00c9 }
                        if (r1 == 0) goto L_0x008d
                        r0 = 20000(0x4e20, float:2.8026E-41)
                        r1.setConnectTimeout(r0)     // Catch:{ Exception -> 0x009f }
                        r1.connect()     // Catch:{ Exception -> 0x009f }
                        int r0 = r1.getResponseCode()     // Catch:{ Exception -> 0x009f }
                        r5 = 200(0xc8, float:2.8E-43)
                        if (r0 != r5) goto L_0x007e
                        java.io.InputStream r2 = r1.getInputStream()     // Catch:{ Exception -> 0x009f }
                        r0 = 2048(0x800, float:2.87E-42)
                        byte[] r5 = new byte[r0]     // Catch:{ Exception -> 0x009f }
                        int r0 = r2.read(r5)     // Catch:{ Exception -> 0x009f }
                    L_0x004e:
                        r6 = -1
                        if (r0 == r6) goto L_0x005a
                        r6 = 0
                        r4.write(r5, r6, r0)     // Catch:{ Exception -> 0x009f }
                        int r0 = r2.read(r5)     // Catch:{ Exception -> 0x009f }
                        goto L_0x004e
                    L_0x005a:
                        com.baidu.mapapi.d$a r0 = r4     // Catch:{ Exception -> 0x009f }
                        if (r0 == 0) goto L_0x007e
                        com.baidu.mapapi.u r0 = new com.baidu.mapapi.u     // Catch:{ Exception -> 0x009f }
                        byte[] r4 = r4.toByteArray()     // Catch:{ Exception -> 0x009f }
                        r0.<init>(r4)     // Catch:{ Exception -> 0x009f }
                        java.lang.ref.SoftReference r4 = new java.lang.ref.SoftReference     // Catch:{ Exception -> 0x009f }
                        r4.<init>(r0)     // Catch:{ Exception -> 0x009f }
                        java.util.HashMap<java.lang.String, java.lang.ref.SoftReference<com.baidu.mapapi.u>> r5 = com.baidu.mapapi.d.b     // Catch:{ Exception -> 0x009f }
                        java.lang.String r6 = r3     // Catch:{ Exception -> 0x009f }
                        r5.put(r6, r4)     // Catch:{ Exception -> 0x009f }
                        com.baidu.mapapi.d$a r4 = r4     // Catch:{ Exception -> 0x009f }
                        int r5 = r1     // Catch:{ Exception -> 0x009f }
                        int r6 = r2     // Catch:{ Exception -> 0x009f }
                        java.lang.String r7 = r3     // Catch:{ Exception -> 0x009f }
                        r4.onOk(r5, r6, r7, r0)     // Catch:{ Exception -> 0x009f }
                    L_0x007e:
                        if (r2 == 0) goto L_0x0083
                        r2.close()     // Catch:{ IOException -> 0x00bf }
                    L_0x0083:
                        if (r1 == 0) goto L_0x0088
                        r1.disconnect()     // Catch:{ all -> 0x008a }
                    L_0x0088:
                        monitor-exit(r3)     // Catch:{ all -> 0x008a }
                        goto L_0x0024
                    L_0x008a:
                        r0 = move-exception
                        monitor-exit(r3)     // Catch:{ all -> 0x008a }
                        throw r0
                    L_0x008d:
                        com.baidu.mapapi.d$a r0 = r4     // Catch:{ Exception -> 0x009f }
                        if (r0 == 0) goto L_0x007e
                        com.baidu.mapapi.d$a r0 = r4     // Catch:{ Exception -> 0x009f }
                        int r4 = r1     // Catch:{ Exception -> 0x009f }
                        int r5 = r2     // Catch:{ Exception -> 0x009f }
                        java.lang.String r6 = r3     // Catch:{ Exception -> 0x009f }
                        java.lang.String r7 = "网络连接失败"
                        r0.onError(r4, r5, r6, r7)     // Catch:{ Exception -> 0x009f }
                        goto L_0x007e
                    L_0x009f:
                        r0 = move-exception
                    L_0x00a0:
                        com.baidu.mapapi.d$a r4 = r4     // Catch:{ all -> 0x00db }
                        if (r4 == 0) goto L_0x00b1
                        com.baidu.mapapi.d$a r4 = r4     // Catch:{ all -> 0x00db }
                        int r5 = r1     // Catch:{ all -> 0x00db }
                        int r6 = r2     // Catch:{ all -> 0x00db }
                        java.lang.String r7 = r3     // Catch:{ all -> 0x00db }
                        java.lang.String r8 = "网络连接失败"
                        r4.onError(r5, r6, r7, r8)     // Catch:{ all -> 0x00db }
                    L_0x00b1:
                        r0.printStackTrace()     // Catch:{ all -> 0x00db }
                        if (r2 == 0) goto L_0x00b9
                        r2.close()     // Catch:{ IOException -> 0x00c4 }
                    L_0x00b9:
                        if (r1 == 0) goto L_0x0088
                        r1.disconnect()     // Catch:{ all -> 0x008a }
                        goto L_0x0088
                    L_0x00bf:
                        r0 = move-exception
                        r0.printStackTrace()     // Catch:{ all -> 0x008a }
                        goto L_0x0083
                    L_0x00c4:
                        r0 = move-exception
                        r0.printStackTrace()     // Catch:{ all -> 0x008a }
                        goto L_0x00b9
                    L_0x00c9:
                        r0 = move-exception
                        r1 = r2
                    L_0x00cb:
                        if (r2 == 0) goto L_0x00d0
                        r2.close()     // Catch:{ IOException -> 0x00d6 }
                    L_0x00d0:
                        if (r1 == 0) goto L_0x00d5
                        r1.disconnect()     // Catch:{ all -> 0x008a }
                    L_0x00d5:
                        throw r0     // Catch:{ all -> 0x008a }
                    L_0x00d6:
                        r2 = move-exception
                        r2.printStackTrace()     // Catch:{ all -> 0x008a }
                        goto L_0x00d0
                    L_0x00db:
                        r0 = move-exception
                        goto L_0x00cb
                    L_0x00dd:
                        r0 = move-exception
                        r1 = r2
                        goto L_0x00a0
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapapi.d.AnonymousClass1.run():void");
                }
            }.start();
        }
    }

    static void a(Context context) {
        a = context;
    }

    public static void a(NetworkInfo networkInfo, boolean z) {
        c = z;
        try {
            if (networkInfo.getType() == 1) {
                d = 4;
                e = false;
                return;
            }
            String extraInfo = networkInfo.getExtraInfo();
            if (extraInfo == null) {
                d = 0;
                g = android.net.Proxy.getDefaultHost();
                h = android.net.Proxy.getDefaultPort();
                if (g == null || StringUtils.EMPTY.equals(g)) {
                    d = 1;
                    e = false;
                    return;
                }
                d = 2;
                e = true;
                if ("10.0.0.200".equals(g)) {
                    f = 1;
                } else {
                    f = 0;
                }
            } else {
                String trim = extraInfo.toLowerCase().trim();
                if (trim.startsWith("cmwap") || trim.startsWith("uniwap") || trim.startsWith("3gwap")) {
                    d = 2;
                    e = true;
                    f = 0;
                    g = "10.0.0.172";
                } else if (trim.startsWith("ctwap")) {
                    d = 2;
                    e = true;
                    f = 1;
                    g = "10.0.0.200";
                } else if (trim.startsWith("cmnet") || trim.startsWith("uninet") || trim.startsWith("ctnet") || trim.startsWith("3gnet")) {
                    d = 1;
                    e = false;
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
