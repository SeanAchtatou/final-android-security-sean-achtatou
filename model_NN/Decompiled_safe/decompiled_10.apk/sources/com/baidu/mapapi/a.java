package com.baidu.mapapi;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import com.baidu.mapapi.MapView;
import java.lang.reflect.Method;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.time.DateUtils;

public class a extends View {
    private static Method J = null;
    private static Method K = null;
    private static Method L = null;
    private static Bitmap h = null;
    private static ShortBuffer i = null;
    private static short[] j = null;
    private static int k = 0;
    private static int l = 0;
    private static int m = 0;
    private float A;
    private float B;
    private float C;
    private float D;
    private int E = 0;
    private int F = 0;
    private boolean G = false;
    private int H = 0;
    private Paint I = new Paint();
    private MapView.a M = MapView.a.DRAW_RETICLE_OVER;
    boolean a = false;
    int b = 0;
    int c = 0;
    int d = 0;
    double e = 1.0d;
    int f = 0;
    int g = 0;
    private int n = 0;
    private int o = 0;
    private int p = 0;
    private int q = 0;
    private MapView r = null;
    private List<Overlay> s = new ArrayList();
    private boolean t = false;
    private boolean u = true;
    private long v = 0;
    private long w = 0;
    private long x = 0;
    private float y;
    private float z;

    public a(Context context, MapView mapView) {
        super(context);
        this.r = mapView;
        this.I.setColor(-7829368);
        this.I.setStyle(Paint.Style.STROKE);
    }

    private void c(Canvas canvas) {
    }

    private void d(Canvas canvas) {
        long nanoTime = (System.nanoTime() * 1000) / 1000000000;
        for (Overlay draw : this.s) {
            draw.draw(canvas, this.r, false, nanoTime);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (h != null && h.isRecycled()) {
            h.recycle();
        }
        l = 0;
        m = 0;
        h = null;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        int minZoomLevel = this.r.getMinZoomLevel();
        int maxZoomLevel = this.r.getMaxZoomLevel();
        if (minZoomLevel <= i2 && i2 <= maxZoomLevel) {
            Mj.MapProc(4098, i2, 1);
        }
    }

    public void a(int i2, int i3, int i4) {
        this.F += i2;
        this.E = 3;
        if (i3 == 0 && i4 == 0) {
            this.f = h.getWidth() / 2;
            this.g = h.getHeight() / 2;
        } else {
            this.f = i3;
            this.g = i4;
        }
        Mj.p = 1;
        invalidate();
    }

    public void a(int i2, int i3, int i4, int i5) {
        if (i5 > 0 && i4 > 0) {
            int i6 = ((i4 + 3) / 4) * 4;
            int i7 = ((i5 + 3) / 4) * 4;
            if (l != i6 || m != i7) {
                for (Overlay a2 : this.s) {
                    a2.a(i2, i3, i6, i7);
                }
                int i8 = i6 * i7 * 2;
                if (i8 > k) {
                    j = new short[i8];
                    i = ShortBuffer.allocate(i8);
                    k = i8;
                }
                if (h != null && !h.isRecycled()) {
                    h.recycle();
                }
                try {
                    h = Bitmap.createBitmap(i6, i7, Bitmap.Config.RGB_565);
                } catch (OutOfMemoryError e2) {
                    System.gc();
                    if (h != null && !h.isRecycled()) {
                        h.recycle();
                    }
                }
                Mj.p = 1;
                Mj.renderUpdateScreen(j, i6, i7);
                l = i6;
                m = i7;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas) {
        canvas.drawRGB(192, 192, 192);
        Matrix matrix = new Matrix();
        if (this.H == 1 || this.e == 1.0d) {
            canvas.drawBitmap(h, (float) this.c, (float) this.d, (Paint) null);
            return;
        }
        double abs = Math.abs(this.e);
        matrix.postScale((float) abs, (float) abs, (float) this.f, (float) this.g);
        matrix.postTranslate((float) this.c, (float) this.d);
        canvas.drawBitmap(h, matrix, null);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        this.u = z2;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2, int i2, Rect rect) {
        super.onFocusChanged(z2, i2, rect);
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, KeyEvent keyEvent) {
        boolean z2 = false;
        for (int size = this.s.size() - 1; size >= 0; size--) {
            z2 = this.s.get(size).onKeyDown(i2, keyEvent, this.r);
            if (z2) {
                break;
            }
        }
        return z2;
    }

    /* access modifiers changed from: package-private */
    public boolean a(MotionEvent motionEvent) {
        int i2;
        if (Integer.parseInt(Build.VERSION.SDK) <= 4) {
            i2 = 1;
        } else {
            try {
                if (J == null) {
                    Class<?> cls = Class.forName("android.view.MotionEvent");
                    J = cls.getMethod("getPointerCount", null);
                    L = cls.getMethod("getX", Integer.TYPE);
                    K = cls.getMethod("getY", Integer.TYPE);
                }
                i2 = ((Integer) J.invoke(motionEvent, new Object[0])).intValue();
            } catch (Exception e2) {
                e2.printStackTrace();
                i2 = 0;
            }
        }
        if (i2 == 2) {
            try {
                this.y = ((Float) L.invoke(motionEvent, 0)).floatValue();
                this.z = ((Float) K.invoke(motionEvent, 0)).floatValue();
                this.A = ((Float) L.invoke(motionEvent, 1)).floatValue();
                this.B = ((Float) K.invoke(motionEvent, 1)).floatValue();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
            this.D = ((float) Math.sqrt((double) (((this.y - this.A) * (this.y - this.A)) + ((this.z - this.B) * (this.z - this.B))))) / 2.0f;
            if (this.E == 0) {
                if (this.D > Mj.j * 30.0f) {
                    this.t = false;
                    this.f = (int) (((this.y + this.A) / 2.0f) - ((float) this.c));
                    this.g = (int) (((this.z + this.B) / 2.0f) - ((float) this.d));
                    this.C = this.D;
                    this.E = 1;
                }
            } else if (this.H == 1) {
                double log = Math.log((double) (this.D / this.C)) / Math.log(3.0d);
                int i3 = log >= 0.0d ? (int) (log + 0.5d) : (int) (log - 0.5d);
                if (i3 != 0) {
                    this.C = this.D;
                    b(i3, (this.f + this.c) - (l / 2), (this.g + this.d) - (m / 2));
                }
            } else {
                this.e = (double) (this.D / this.C);
                invalidate();
            }
        } else if (this.E == 1) {
            this.E = 2;
            invalidate();
        }
        if (this.E != 0) {
            return false;
        }
        for (int size = this.s.size() - 1; size >= 0; size--) {
            if (this.s.get(size).onTouchEvent(motionEvent, this.r)) {
                return true;
            }
        }
        switch (motionEvent.getAction()) {
            case 0:
                this.n = (int) motionEvent.getX();
                this.o = (int) motionEvent.getY();
                this.c = 0;
                this.d = 0;
                this.t = true;
                Mj.l = (int) motionEvent.getX();
                Mj.m = (int) motionEvent.getY();
                Mj.MapProc(4, (int) motionEvent.getX(), (int) motionEvent.getY());
                break;
            case 1:
                if (this.t) {
                    this.t = false;
                    if (!(this.c == 0 && this.d == 0)) {
                        this.c = 0;
                        this.d = 0;
                    }
                    int x2 = (int) motionEvent.getX();
                    int y2 = (int) motionEvent.getY();
                    if (this.u) {
                        int i4 = (int) (50.0f * Mj.j);
                        if (this.x - this.w < 1000 && Math.abs(x2 - this.n) < i4 && Math.abs(y2 - this.o) < i4) {
                            this.v++;
                        }
                        if (this.v != 1) {
                            if (this.v >= 2) {
                                this.x = System.currentTimeMillis();
                                if (this.x - this.w < 1000 && Math.abs(this.p - x2) < i4 && Math.abs(this.q - y2) < i4) {
                                    if (this.r.getZoomLevel() < 18) {
                                        a(1, x2, y2);
                                    }
                                    this.v = 0;
                                    this.w = 0;
                                    this.x = 0;
                                    break;
                                } else {
                                    this.v = 0;
                                    this.w = 0;
                                    this.x = 0;
                                }
                            }
                        } else {
                            this.w = System.currentTimeMillis();
                        }
                    }
                    int i5 = (int) (10.0f * Mj.j);
                    if (Math.abs(this.n - x2) < i5 && Math.abs(this.o - y2) < i5) {
                        x2 = this.n;
                        y2 = this.o;
                    }
                    this.p = x2;
                    this.q = y2;
                    Mj.n = x2;
                    Mj.o = y2;
                    Mj.MapProc(5, x2, y2);
                    if (Math.abs(x2 - this.n) < 20 && Math.abs(y2 - this.o) < 20) {
                        for (int size2 = this.s.size() - 1; size2 >= 0; size2--) {
                            Overlay overlay = this.s.get(size2);
                            GeoPoint fromPixels = this.r.getProjection().fromPixels((int) motionEvent.getX(), (int) motionEvent.getY());
                            if (overlay instanceof ItemizedOverlay ? ((ItemizedOverlay) overlay).onTap(fromPixels, this.r) : overlay.onTap(fromPixels, this.r)) {
                                return true;
                            }
                        }
                        break;
                    }
                } else {
                    return true;
                }
                break;
            case 2:
                if (this.t) {
                    this.c = ((int) motionEvent.getX()) - this.n;
                    this.d = ((int) motionEvent.getY()) - this.o;
                    invalidate();
                    Mj.MapProc(3, (int) motionEvent.getX(), (int) motionEvent.getY());
                    break;
                }
                break;
            default:
                return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void b(int i2) {
        this.H = i2;
    }

    /* access modifiers changed from: package-private */
    public void b(int i2, int i3, int i4) {
        int minZoomLevel = this.r.getMinZoomLevel();
        int maxZoomLevel = this.r.getMaxZoomLevel();
        int zoomLevel = this.r.getZoomLevel() + i2;
        if (minZoomLevel <= zoomLevel && zoomLevel <= maxZoomLevel) {
            Bundle bundle = new Bundle();
            bundle.putInt(NDEFRecord.ACTION_WELL_KNOWN_TYPE, DateUtils.SEMI_MONTH);
            bundle.putInt("opt", 10020600);
            bundle.putInt("level", i2);
            bundle.putInt("x", this.f);
            bundle.putInt("y", this.g);
            bundle.putInt("dx", i3);
            bundle.putInt("dy", i4);
            Mj.sendBundle(bundle);
        }
    }

    /* access modifiers changed from: protected */
    public void b(Canvas canvas) {
        a(canvas);
        if (this.G || this.e == 1.0d) {
            switch (this.M) {
                case DRAW_RETICLE_NEVER:
                    d(canvas);
                    return;
                case DRAW_RETICLE_OVER:
                    d(canvas);
                    c(canvas);
                    return;
                case DRAW_RETICLE_UNDER:
                    c(canvas);
                    d(canvas);
                    return;
                default:
                    c(canvas);
                    d(canvas);
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z2) {
        this.G = z2;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.u;
    }

    /* access modifiers changed from: package-private */
    public boolean b(int i2, KeyEvent keyEvent) {
        boolean z2 = false;
        for (int size = this.s.size() - 1; size >= 0; size--) {
            z2 = this.s.get(size).onKeyUp(i2, keyEvent, this.r);
            if (z2) {
                break;
            }
        }
        return z2;
    }

    /* access modifiers changed from: package-private */
    public boolean b(MotionEvent motionEvent) {
        boolean z2 = false;
        for (int size = this.s.size() - 1; size >= 0; size--) {
            z2 = this.s.get(size).onTrackballEvent(motionEvent, this.r);
            if (z2) {
                break;
            }
        }
        return z2;
    }

    public final List<Overlay> c() {
        return this.s;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        for (int size = this.s.size() - 1; size >= 0; size--) {
            if (this.s.get(size) instanceof MyLocationOverlay) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        double d2;
        this.r.e();
        switch (this.E) {
            case 0:
                if (j != null && this.a) {
                    this.c = 0;
                    this.d = 0;
                    i.rewind();
                    i.put(j);
                    i.rewind();
                    h.copyPixelsFromBuffer(i);
                    this.a = false;
                    this.e = 1.0d;
                }
                b(canvas);
                return;
            case 1:
                if (this.H == 1 && j != null && this.a) {
                    this.c = 0;
                    this.d = 0;
                    i.rewind();
                    i.put(j);
                    i.rewind();
                    h.copyPixelsFromBuffer(i);
                    this.a = false;
                    this.e = 1.0d;
                }
                b(canvas);
                return;
            case 2:
                if (this.H == 1) {
                    this.E = 0;
                    b(canvas);
                    return;
                }
                double log = Math.log(this.e) / Math.log(2.0d);
                int i2 = log >= 0.0d ? (int) (0.5d + log) : (int) (log - 0.5d);
                int i3 = (int) ((((double) i2) - log) * 10.0d);
                if (i3 != 0) {
                    if (i3 > 0) {
                        d2 = log + 0.1d;
                        int i4 = i3 - 1;
                    } else {
                        d2 = log - 0.1d;
                        int i5 = i3 + 1;
                    }
                    this.e = Math.pow(2.0d, d2);
                    b(canvas);
                    postInvalidate();
                    return;
                }
                this.e = Math.pow(2.0d, (double) i2);
                b(canvas);
                if (log != 0.0d) {
                    b(i2, (this.f + this.c) - (l / 2), (this.g + this.d) - (m / 2));
                }
                this.E = 0;
                return;
            case 3:
                if (this.H == 1) {
                    b(canvas);
                    b(this.F, 0, 0);
                    this.E = 0;
                    this.F = 0;
                    return;
                }
                double log2 = Math.log(this.e) / Math.log(2.0d);
                if (this.F > 0) {
                    this.e = (Math.pow(2.0d, (double) ((int) log2)) / 10.0d) + this.e;
                } else if (this.F < 0) {
                    this.e -= Math.pow(2.0d, (double) ((int) log2)) / 20.0d;
                }
                if ((this.e >= ((double) (this.F * 2)) || this.e <= 0.0d) && (this.F >= 0 || this.e <= ((double) (-1.0f / ((float) (this.F * 2)))))) {
                    b(canvas);
                    b(this.F, 0, 0);
                    this.E = 0;
                    this.F = 0;
                    return;
                }
                b(canvas);
                postInvalidate();
                return;
            default:
                b(canvas);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        super.onLayout(z2, i2, i3, i4, i5);
        a(i2, i3, i4 - i2, i5 - i3);
    }
}
