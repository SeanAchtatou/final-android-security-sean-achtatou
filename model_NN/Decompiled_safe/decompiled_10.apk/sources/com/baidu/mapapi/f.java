package com.baidu.mapapi;

import android.content.Context;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.os.Bundle;
import android.telephony.NeighboringCellInfo;
import android.util.Log;
import com.house365.newhouse.constant.App;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;

class f {
    Location a = null;
    /* access modifiers changed from: private */
    public LocationManager b = null;
    private LocationListener c = null;
    /* access modifiers changed from: private */
    public Context d;
    private a e = null;
    /* access modifiers changed from: private */
    public int f = 0;
    /* access modifiers changed from: private */
    public GpsStatus g;

    private class a implements GpsStatus.Listener {
        private a() {
        }

        public void onGpsStatusChanged(int i) {
            switch (i) {
                case 2:
                    Mj.UpdataGPS(0.0d, 0.0d, SystemUtils.JAVA_VERSION_FLOAT, SystemUtils.JAVA_VERSION_FLOAT, SystemUtils.JAVA_VERSION_FLOAT, 0);
                    return;
                case 3:
                default:
                    return;
                case 4:
                    if (f.this.b == null) {
                        LocationManager unused = f.this.b = (LocationManager) f.this.d.getSystemService("location");
                    }
                    if (f.this.b != null) {
                        if (f.this.g == null) {
                            GpsStatus unused2 = f.this.g = f.this.b.getGpsStatus(null);
                        } else {
                            f.this.b.getGpsStatus(f.this.g);
                        }
                    }
                    int i2 = 0;
                    for (GpsSatellite usedInFix : f.this.g.getSatellites()) {
                        i2 = usedInFix.usedInFix() ? i2 + 1 : i2;
                    }
                    if (i2 < 3 && f.this.f >= 3) {
                        Mj.UpdataGPS(0.0d, 0.0d, SystemUtils.JAVA_VERSION_FLOAT, SystemUtils.JAVA_VERSION_FLOAT, SystemUtils.JAVA_VERSION_FLOAT, 0);
                    }
                    int unused3 = f.this.f = i2;
                    return;
            }
        }
    }

    public f(Context context) {
        this.d = context;
    }

    /* access modifiers changed from: package-private */
    public String a(int i, int i2, int i3, int i4, List<NeighboringCellInfo> list, String str) {
        if (i3 == 0 || i4 == 0) {
            return StringUtils.EMPTY;
        }
        return str.concat(String.format("%d|%d|%d|%d", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4)));
    }

    public String a(List<ScanResult> list, String str) {
        String concat;
        String str2 = StringUtils.EMPTY;
        int size = list.size();
        if (list.size() > 10) {
            size = 10;
        }
        int i = 0;
        while (i < size) {
            if (i == 0) {
                if (list.get(i).level == 0) {
                    concat = str2;
                } else {
                    concat = str2.concat(list.get(i).BSSID.replace(":", StringUtils.EMPTY)).concat(String.format(";%d;", Integer.valueOf(list.get(i).level))).concat(list.get(i).SSID);
                }
            } else if (list.get(i).level == 0) {
                break;
            } else {
                concat = str2.concat("|").concat(list.get(i).BSSID.replace(":", StringUtils.EMPTY)).concat(String.format(";%d;", Integer.valueOf(list.get(i).level))).concat(list.get(i).SSID);
            }
            i++;
            str2 = concat;
        }
        return str2;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.d != null) {
            Mj.UpdataGPS(0.0d, 0.0d, SystemUtils.JAVA_VERSION_FLOAT, SystemUtils.JAVA_VERSION_FLOAT, SystemUtils.JAVA_VERSION_FLOAT, 0);
            if (this.c == null) {
                this.c = new LocationListener() {
                    public void onLocationChanged(Location location) {
                        int i = 0;
                        if (location != null) {
                            Bundle extras = location.getExtras();
                            if (extras != null) {
                                i = extras.getInt("NumSatellite", 0);
                            }
                            float f = SystemUtils.JAVA_VERSION_FLOAT;
                            if (location.hasAccuracy()) {
                                f = location.getAccuracy();
                            }
                            Mj.UpdataGPS(location.getLongitude(), location.getLatitude(), (float) (((double) location.getSpeed()) * 3.6d), location.getBearing(), f, i);
                            f.this.a = location;
                        }
                    }

                    public void onProviderDisabled(String str) {
                        Mj.UpdataGPS(0.0d, 0.0d, SystemUtils.JAVA_VERSION_FLOAT, SystemUtils.JAVA_VERSION_FLOAT, SystemUtils.JAVA_VERSION_FLOAT, 0);
                        f.this.a = null;
                    }

                    public void onProviderEnabled(String str) {
                    }

                    public void onStatusChanged(String str, int i, Bundle bundle) {
                        switch (i) {
                            case 0:
                            case 1:
                                Mj.UpdataGPS(0.0d, 0.0d, SystemUtils.JAVA_VERSION_FLOAT, SystemUtils.JAVA_VERSION_FLOAT, SystemUtils.JAVA_VERSION_FLOAT, 0);
                                f.this.a = null;
                                return;
                            default:
                                return;
                        }
                    }
                };
            }
            if (this.b == null) {
                this.b = (LocationManager) this.d.getSystemService("location");
            }
            if (this.b != null) {
                try {
                    this.b.requestLocationUpdates("gps", 1000, (float) SystemUtils.JAVA_VERSION_FLOAT, this.c);
                    this.e = new a();
                    this.b.addGpsStatusListener(this.e);
                } catch (Exception e2) {
                    Log.d("InitGPS", e2.getMessage());
                }
            }
        }
    }

    public void a(int i, int i2, long j) {
        switch (i) {
            case App.Ad.FULL_SCREEN_CLOSE_TIME /*5000*/:
                if (i2 == 1) {
                    a();
                    return;
                } else {
                    b();
                    return;
                }
            default:
                return;
        }
    }

    public void a(List<ScanResult> list) {
        boolean z;
        boolean z2 = true;
        for (int size = list.size() - 1; size >= 1 && z2; size--) {
            int i = 0;
            z2 = false;
            while (i < size) {
                if (list.get(i).level < list.get(i + 1).level) {
                    list.set(i + 1, list.get(i));
                    list.set(i, list.get(i + 1));
                    z = true;
                } else {
                    z = z2;
                }
                i++;
                z2 = z;
            }
        }
    }

    public boolean a(List<ScanResult> list, List<ScanResult> list2) {
        int i;
        if (list == list2) {
            return true;
        }
        if (list == null || list2 == null) {
            return false;
        }
        int size = list.size();
        int size2 = list2.size();
        if (size == 0 || size2 == 0) {
            return false;
        }
        int i2 = 0;
        int i3 = 0;
        while (i2 < size) {
            String str = list.get(i2).BSSID;
            if (str != null) {
                int i4 = 0;
                while (true) {
                    if (i4 >= size2) {
                        i = i3;
                        break;
                    } else if (str.equals(list2.get(i4).BSSID)) {
                        i = i3 + 1;
                        break;
                    } else {
                        i4++;
                    }
                }
            } else {
                i = i3;
            }
            i2++;
            i3 = i;
        }
        return i3 >= size / 2;
    }

    public String b(List<ScanResult> list, String str) {
        String concat;
        String str2 = StringUtils.EMPTY;
        int size = list.size();
        if (list.size() > 10) {
            size = 10;
        }
        int i = 0;
        while (i < size) {
            if (i == 0) {
                concat = list.get(i).level == 0 ? str2 : str2.concat(list.get(i).BSSID.replace(":", StringUtils.EMPTY));
            } else if (list.get(i).level == 0) {
                break;
            } else {
                concat = str2.concat("|").concat(list.get(i).BSSID.replace(":", StringUtils.EMPTY));
            }
            i++;
            str2 = concat;
        }
        return str2;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        try {
            if (this.b != null) {
                this.b.removeUpdates(this.c);
            }
        } catch (Exception e2) {
            Log.d("UnInitGPS", e2.getMessage());
        }
        Mj.UpdataGPS(0.0d, 0.0d, SystemUtils.JAVA_VERSION_FLOAT, SystemUtils.JAVA_VERSION_FLOAT, SystemUtils.JAVA_VERSION_FLOAT, 0);
    }
}
