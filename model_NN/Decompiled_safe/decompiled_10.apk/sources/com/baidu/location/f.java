package com.baidu.location;

import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.net.wifi.ScanResult;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Process;
import android.util.Log;
import com.baidu.location.c;
import com.baidu.location.e;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.ui.privilege.CouponDetailsActivity;
import java.io.File;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.StringWriter;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Calendar;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public final class f extends Service {
    static final int B = 92;
    static final int D = 57;
    static final int G = 52;
    static final int I = 26;
    static final int K = 64;
    static final int L = 27;
    static final int M = 62;
    static final int S = 1000;
    static final int U = 54;
    static final int V = 81;
    static final int W = 25;
    private static String a = (ac + "/glb.dat");
    static final int aa = 21;
    static String ac = (Environment.getExternalStorageDirectory().getPath() + "/baidu/tempdata");
    private static final int af = 200;
    static final int ag = 43;
    static final int ah = 14;
    static final int ai = 3000;
    static final int ak = 56;
    static final int ao = 101;
    static final float ap = 3.3f;
    static final int aq = 61;
    static final int ar = 53;
    private static final int at = 800;
    static final int b = 63;

    /* renamed from: byte  reason: not valid java name */
    private static final int f131byte = 24;
    static final int c = 12;

    /* renamed from: case  reason: not valid java name */
    static final int f132case = 42;

    /* renamed from: do  reason: not valid java name */
    static final int f133do = 28;
    static final int e = 65;

    /* renamed from: else  reason: not valid java name */
    static final int f134else = 2000;

    /* renamed from: for  reason: not valid java name */
    static final int f135for = 22;
    static final int g = 15;
    static final int i = 55;

    /* renamed from: int  reason: not valid java name */
    static final int f136int = 31;
    /* access modifiers changed from: private */
    public static File j = null;
    /* access modifiers changed from: private */
    public static File k = null;
    static final int l = 11;

    /* renamed from: long  reason: not valid java name */
    static final int f137long = 13;
    static final int p = 41;
    static final int s = 23;
    static final int t = 91;
    public static final String v = "baidu_location_service";

    /* renamed from: void  reason: not valid java name */
    static final int f138void = 71;
    static final int w = 24;
    static final int x = 3000;
    static final int z = 51;
    private String A = null;
    private e.c C = null;
    private long E = 0;
    /* access modifiers changed from: private */
    public e F = null;
    private String H = null;
    private boolean J = true;
    private boolean N = true;
    private boolean O = false;
    private long P = 0;
    private boolean Q = false;
    final Handler R = new d();
    private SQLiteDatabase T = null;
    private String X = null;
    private boolean Y = true;
    private int Z = 0;
    private b ab = null;
    /* access modifiers changed from: private */
    public boolean ad = false;
    private e.c ae = null;
    /* access modifiers changed from: private */
    public boolean aj = false;
    private c.a al = null;
    private boolean am = false;
    final Messenger an = new Messenger(this.R);
    private String as = null;
    /* access modifiers changed from: private */
    public a au = null;

    /* renamed from: char  reason: not valid java name */
    private e.c f139char = null;
    private long d = 0;
    private Location f = null;

    /* renamed from: goto  reason: not valid java name */
    private boolean f140goto = false;
    private String h = null;

    /* renamed from: if  reason: not valid java name */
    private String f141if = "bdcltb09";
    /* access modifiers changed from: private */
    public String m = (ac + "/vm.dat");
    private double n = 0.0d;

    /* renamed from: new  reason: not valid java name */
    private String f142new = null;
    private double o = 0.0d;
    private double q = 0.0d;
    /* access modifiers changed from: private */
    public c r = null;

    /* renamed from: try  reason: not valid java name */
    private c.a f143try = null;
    private c.a u = null;
    /* access modifiers changed from: private */
    public c y = null;

    public class a implements Thread.UncaughtExceptionHandler {

        /* renamed from: if  reason: not valid java name */
        private Context f144if;

        a(Context context) {
            this.f144if = context;
            a();
        }

        private String a(Throwable th) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            th.printStackTrace(printWriter);
            printWriter.close();
            return stringWriter.toString();
        }

        public void a() {
            String str;
            String str2 = null;
            try {
                File file = new File((Environment.getExternalStorageDirectory().getPath() + "/traces") + "/error_fs.dat");
                if (file.exists()) {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                    randomAccessFile.seek(280);
                    if (1326 == randomAccessFile.readInt()) {
                        randomAccessFile.seek(308);
                        int readInt = randomAccessFile.readInt();
                        if (readInt <= 0 || readInt >= 2048) {
                            str = null;
                        } else {
                            j.m250if(f.v, "A" + readInt);
                            byte[] bArr = new byte[readInt];
                            randomAccessFile.read(bArr, 0, readInt);
                            str = new String(bArr, 0, readInt);
                        }
                        randomAccessFile.seek(600);
                        int readInt2 = randomAccessFile.readInt();
                        if (readInt2 > 0 && readInt2 < 2048) {
                            j.m250if(f.v, "A" + readInt2);
                            byte[] bArr2 = new byte[readInt2];
                            randomAccessFile.read(bArr2, 0, readInt2);
                            str2 = new String(bArr2, 0, readInt2);
                        }
                        j.m250if(f.v, str + str2);
                        if (a(str, str2)) {
                            randomAccessFile.seek(280);
                            randomAccessFile.writeInt(12346);
                        }
                    }
                    randomAccessFile.close();
                }
            } catch (Exception e) {
            }
        }

        public void a(File file, String str, String str2) {
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                randomAccessFile.seek(280);
                randomAccessFile.writeInt(12346);
                randomAccessFile.seek(300);
                randomAccessFile.writeLong(System.currentTimeMillis());
                byte[] bytes = str.getBytes();
                randomAccessFile.writeInt(bytes.length);
                randomAccessFile.write(bytes, 0, bytes.length);
                randomAccessFile.seek(600);
                byte[] bytes2 = str2.getBytes();
                randomAccessFile.writeInt(bytes2.length);
                randomAccessFile.write(bytes2, 0, bytes2.length);
                if (!a(str, str2)) {
                    randomAccessFile.seek(280);
                    randomAccessFile.writeInt(1326);
                }
                randomAccessFile.close();
            } catch (Exception e) {
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(String str, String str2) {
            if (!g.a(this.f144if)) {
                return false;
            }
            try {
                HttpPost httpPost = new HttpPost(j.N);
                ArrayList arrayList = new ArrayList();
                arrayList.add(new BasicNameValuePair("e0", str));
                arrayList.add(new BasicNameValuePair("e1", str2));
                httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "utf-8"));
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                defaultHttpClient.getParams().setParameter("http.connection.timeout", 12000);
                defaultHttpClient.getParams().setParameter("http.socket.timeout", 12000);
                j.m250if(f.v, "send begin ...");
                if (defaultHttpClient.execute(httpPost).getStatusLine().getStatusCode() != 200) {
                    return false;
                }
                j.m250if(f.v, "send ok....");
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0083 A[Catch:{ Exception -> 0x00c9 }] */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00a8 A[SYNTHETIC, Splitter:B:26:0x00a8] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void uncaughtException(java.lang.Thread r9, java.lang.Throwable r10) {
            /*
                r8 = this;
                r1 = 0
                boolean r0 = com.baidu.location.j.x
                if (r0 != 0) goto L_0x000d
                int r0 = android.os.Process.myPid()
                android.os.Process.killProcess(r0)
            L_0x000c:
                return
            L_0x000d:
                java.lang.String r2 = r8.a(r10)     // Catch:{ Exception -> 0x00a3 }
                java.lang.String r0 = "baidu_location_service"
                com.baidu.location.j.m250if(r0, r2)     // Catch:{ Exception -> 0x00cb }
                com.baidu.location.f r0 = com.baidu.location.f.this     // Catch:{ Exception -> 0x00cb }
                com.baidu.location.c unused = r0.r     // Catch:{ Exception -> 0x00cb }
                r0 = 0
                java.lang.String r0 = com.baidu.location.c.a(r0)     // Catch:{ Exception -> 0x00cb }
                com.baidu.location.f r3 = com.baidu.location.f.this     // Catch:{ Exception -> 0x00cb }
                com.baidu.location.a r3 = r3.au     // Catch:{ Exception -> 0x00cb }
                if (r3 == 0) goto L_0x0043
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00cb }
                r3.<init>()     // Catch:{ Exception -> 0x00cb }
                java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x00cb }
                com.baidu.location.f r3 = com.baidu.location.f.this     // Catch:{ Exception -> 0x00cb }
                com.baidu.location.a r3 = r3.au     // Catch:{ Exception -> 0x00cb }
                java.lang.String r3 = r3.m39byte()     // Catch:{ Exception -> 0x00cb }
                java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x00cb }
                java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00cb }
            L_0x0043:
                if (r0 == 0) goto L_0x00d0
                java.lang.String r0 = com.baidu.location.Jni.m0if(r0)     // Catch:{ Exception -> 0x00cb }
            L_0x0049:
                r3 = r0
            L_0x004a:
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c9 }
                r0.<init>()     // Catch:{ Exception -> 0x00c9 }
                java.io.File r4 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x00c9 }
                java.lang.String r4 = r4.getPath()     // Catch:{ Exception -> 0x00c9 }
                java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x00c9 }
                java.lang.String r4 = "/traces"
                java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x00c9 }
                java.lang.String r4 = r0.toString()     // Catch:{ Exception -> 0x00c9 }
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c9 }
                r0.<init>()     // Catch:{ Exception -> 0x00c9 }
                java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x00c9 }
                java.lang.String r5 = "/error_fs.dat"
                java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Exception -> 0x00c9 }
                java.lang.String r5 = r0.toString()     // Catch:{ Exception -> 0x00c9 }
                java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x00c9 }
                r0.<init>(r5)     // Catch:{ Exception -> 0x00c9 }
                boolean r5 = r0.exists()     // Catch:{ Exception -> 0x00c9 }
                if (r5 != 0) goto L_0x00a8
                java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x00c9 }
                r5.<init>(r4)     // Catch:{ Exception -> 0x00c9 }
                boolean r4 = r5.exists()     // Catch:{ Exception -> 0x00c9 }
                if (r4 != 0) goto L_0x0091
                r5.mkdirs()     // Catch:{ Exception -> 0x00c9 }
            L_0x0091:
                boolean r4 = r0.createNewFile()     // Catch:{ Exception -> 0x00c9 }
                if (r4 != 0) goto L_0x00ce
            L_0x0097:
                r8.a(r1, r3, r2)     // Catch:{ Exception -> 0x00c9 }
            L_0x009a:
                int r0 = android.os.Process.myPid()
                android.os.Process.killProcess(r0)
                goto L_0x000c
            L_0x00a3:
                r0 = move-exception
                r0 = r1
            L_0x00a5:
                r2 = r0
                r3 = r1
                goto L_0x004a
            L_0x00a8:
                java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x00c9 }
                java.lang.String r4 = "rw"
                r1.<init>(r0, r4)     // Catch:{ Exception -> 0x00c9 }
                r4 = 300(0x12c, double:1.48E-321)
                r1.seek(r4)     // Catch:{ Exception -> 0x00c9 }
                long r4 = r1.readLong()     // Catch:{ Exception -> 0x00c9 }
                long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00c9 }
                long r4 = r6 - r4
                r6 = 604800000(0x240c8400, double:2.988109026E-315)
                int r1 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r1 <= 0) goto L_0x009a
                r8.a(r0, r3, r2)     // Catch:{ Exception -> 0x00c9 }
                goto L_0x009a
            L_0x00c9:
                r0 = move-exception
                goto L_0x009a
            L_0x00cb:
                r0 = move-exception
                r0 = r2
                goto L_0x00a5
            L_0x00ce:
                r1 = r0
                goto L_0x0097
            L_0x00d0:
                r0 = r1
                goto L_0x0049
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.f.a.uncaughtException(java.lang.Thread, java.lang.Throwable):void");
        }
    }

    private class b implements Runnable {
        private b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.f.a(com.baidu.location.f, boolean):boolean
         arg types: [com.baidu.location.f, int]
         candidates:
          com.baidu.location.f.a(android.os.Message, int):void
          com.baidu.location.f.a(com.baidu.location.f, int):void
          com.baidu.location.f.a(com.baidu.location.f, android.os.Message):void
          com.baidu.location.f.a(com.baidu.location.f, boolean):boolean */
        public void run() {
            if (f.this.aj) {
                boolean unused = f.this.aj = false;
                f.this.m153byte();
            }
        }
    }

    public class c {

        /* renamed from: for  reason: not valid java name */
        public static final String f145for = "com.baidu.locTest.LocationServer";
        private long[] a = new long[20];
        private c.a b = null;

        /* renamed from: byte  reason: not valid java name */
        private int f146byte = 1;
        /* access modifiers changed from: private */
        public String c = null;

        /* renamed from: case  reason: not valid java name */
        private a f147case = null;

        /* renamed from: char  reason: not valid java name */
        private final int f148char = 200;
        private PendingIntent d = null;

        /* renamed from: do  reason: not valid java name */
        private boolean f149do = false;

        /* renamed from: else  reason: not valid java name */
        private boolean f150else = false;

        /* renamed from: goto  reason: not valid java name */
        private Context f151goto = null;

        /* renamed from: if  reason: not valid java name */
        private boolean f152if = false;

        /* renamed from: int  reason: not valid java name */
        private int f153int = 0;

        /* renamed from: long  reason: not valid java name */
        private String f154long = null;

        /* renamed from: new  reason: not valid java name */
        private final long f155new = 86100000;

        /* renamed from: try  reason: not valid java name */
        private AlarmManager f156try = null;

        /* renamed from: void  reason: not valid java name */
        private long f157void = 0;

        public class a extends BroadcastReceiver {
            public a() {
            }

            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action.equals(c.f145for)) {
                    f.this.R.obtainMessage(f.ao).sendToTarget();
                    return;
                }
                try {
                    if (action.equals("android.intent.action.BATTERY_CHANGED")) {
                        int intExtra = intent.getIntExtra("status", 0);
                        int intExtra2 = intent.getIntExtra("plugged", 0);
                        switch (intExtra) {
                            case 2:
                                String unused = c.this.c = "4";
                                break;
                            case 3:
                            case 4:
                                String unused2 = c.this.c = "3";
                                break;
                            default:
                                String unused3 = c.this.c = null;
                                break;
                        }
                        switch (intExtra2) {
                            case 1:
                                String unused4 = c.this.c = "6";
                                return;
                            case 2:
                                String unused5 = c.this.c = "5";
                                return;
                            default:
                                return;
                        }
                    }
                } catch (Exception e) {
                    String unused6 = c.this.c = null;
                }
            }
        }

        public c(Context context) {
            this.f151goto = context;
            this.f157void = System.currentTimeMillis();
            this.f156try = (AlarmManager) context.getSystemService("alarm");
            this.f147case = new a();
            context.registerReceiver(this.f147case, new IntentFilter(f145for));
            this.d = PendingIntent.getBroadcast(context, 0, new Intent(f145for), 134217728);
            this.f156try.setRepeating(2, j.M, j.M, this.d);
            f.this.registerReceiver(this.f147case, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        }

        public void a() {
            m197if();
            if (f.j != null) {
                try {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(f.j, "rw");
                    if (randomAccessFile.length() < 1) {
                        randomAccessFile.close();
                        return;
                    }
                    randomAccessFile.seek(0);
                    int readInt = randomAccessFile.readInt();
                    randomAccessFile.seek(0);
                    randomAccessFile.writeInt(readInt + 1);
                    randomAccessFile.seek((long) ((readInt * 200) + 4));
                    randomAccessFile.writeLong(System.currentTimeMillis());
                    randomAccessFile.writeInt(this.f146byte);
                    randomAccessFile.writeInt(0);
                    randomAccessFile.writeInt(this.f153int);
                    randomAccessFile.writeInt(this.b.f107do);
                    randomAccessFile.writeInt(this.b.f109if);
                    randomAccessFile.writeInt(this.b.f108for);
                    randomAccessFile.writeInt(this.b.f112try);
                    byte[] bArr = new byte[160];
                    for (int i = 0; i < this.f153int; i++) {
                        bArr[(i * 8) + 7] = (byte) ((int) this.a[i]);
                        bArr[(i * 8) + 6] = (byte) ((int) (this.a[i] >> 8));
                        bArr[(i * 8) + 5] = (byte) ((int) (this.a[i] >> 16));
                        bArr[(i * 8) + 4] = (byte) ((int) (this.a[i] >> 24));
                        bArr[(i * 8) + 3] = (byte) ((int) (this.a[i] >> 32));
                        bArr[(i * 8) + 2] = (byte) ((int) (this.a[i] >> 40));
                        bArr[(i * 8) + 1] = (byte) ((int) (this.a[i] >> 48));
                        bArr[(i * 8) + 0] = (byte) ((int) (this.a[i] >> 56));
                    }
                    if (this.f153int > 0) {
                        randomAccessFile.write(bArr, 0, this.f153int * 8);
                    }
                    randomAccessFile.writeInt(this.f153int);
                    randomAccessFile.close();
                } catch (Exception e2) {
                }
            }
        }

        /* renamed from: byte  reason: not valid java name */
        public void m193byte() {
            int i = 0;
            if (this.f149do) {
                this.f146byte = 1;
                j.M = j.ac * 1000 * 60;
                j.al = j.M >> 2;
                Calendar instance = Calendar.getInstance();
                int i2 = instance.get(5);
                int i3 = instance.get(1);
                if (i3 > f.f134else) {
                    i = i3 - 2000;
                }
                String str = i + "," + (instance.get(2) + 1) + "," + i2 + "," + instance.get(11) + "," + instance.get(12) + "," + j.ac;
                if (this.f152if) {
                    this.f154long = "&tr=" + j.f199do + "," + str;
                } else {
                    this.f154long += "|T" + str;
                }
                j.m250if(f.v, "trace begin:" + this.f154long);
                try {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(f.k, "rw");
                    randomAccessFile.seek(12);
                    randomAccessFile.writeLong(System.currentTimeMillis());
                    randomAccessFile.writeInt(this.f146byte);
                    randomAccessFile.close();
                    RandomAccessFile randomAccessFile2 = new RandomAccessFile(f.j, "rw");
                    randomAccessFile2.seek(0);
                    randomAccessFile2.writeInt(0);
                    randomAccessFile2.close();
                } catch (Exception e2) {
                }
            }
        }

        /* renamed from: case  reason: not valid java name */
        public void m194case() {
            int i;
            f.m181long();
            if (f.k != null) {
                try {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(f.k, "rw");
                    if (randomAccessFile.length() < 1) {
                        randomAccessFile.close();
                        return;
                    }
                    randomAccessFile.seek(0);
                    int readInt = randomAccessFile.readInt();
                    int readInt2 = randomAccessFile.readInt();
                    int readInt3 = randomAccessFile.readInt();
                    if (!this.f149do || !this.f152if) {
                        i = (readInt2 * f.at) + 24;
                    } else {
                        j.m250if(f.v, "trace new info:" + readInt + ":" + readInt2 + ":" + readInt3);
                        int i2 = (readInt2 + 1) % 200;
                        randomAccessFile.seek(4);
                        randomAccessFile.writeInt(i2);
                        readInt++;
                        if (readInt >= 200) {
                            readInt = 199;
                        }
                        if (i2 == readInt3 && readInt > 0) {
                            readInt3 = (readInt3 + 1) % 200;
                            randomAccessFile.writeInt(readInt3);
                        }
                        j.m250if(f.v, "trace new info:" + readInt + ":" + readInt2 + ":" + readInt3);
                        i = (i2 * f.at) + 24;
                    }
                    randomAccessFile.seek((long) (i + 4));
                    byte[] bytes = this.f154long.getBytes();
                    for (int i3 = 0; i3 < bytes.length; i3++) {
                        bytes[i3] = (byte) (bytes[i3] ^ 90);
                    }
                    randomAccessFile.write(bytes, 0, bytes.length);
                    randomAccessFile.writeInt(bytes.length);
                    randomAccessFile.seek((long) i);
                    randomAccessFile.writeInt(bytes.length);
                    if (this.f149do && this.f152if) {
                        randomAccessFile.seek(0);
                        randomAccessFile.writeInt(readInt);
                    }
                    randomAccessFile.close();
                } catch (Exception e2) {
                }
            }
        }

        /* renamed from: do  reason: not valid java name */
        public void m195do() {
            e.c cVar;
            int i;
            int i2 = 0;
            try {
                j.m250if(f.v, "regular expire...");
                m199new();
                if (this.f150else) {
                    this.f150else = false;
                    return;
                }
                m193byte();
                this.f153int = 0;
                this.b = null;
                if (f.this.F != null) {
                    f.this.F.m137new();
                }
                if (!(f.this.F == null || (cVar = f.this.F.m131byte()) == null || cVar.f128for == null)) {
                    int size = cVar.f128for.size();
                    if (size > 20) {
                        size = 20;
                    }
                    int i3 = 0;
                    while (i3 < size) {
                        try {
                            i = i2 + 1;
                            try {
                                this.a[i2] = Long.parseLong(((ScanResult) cVar.f128for.get(i3)).BSSID.replace(":", StringUtils.EMPTY), 16);
                            } catch (Exception e2) {
                            }
                        } catch (Exception e3) {
                            i = i2;
                        }
                        i3++;
                        i2 = i;
                    }
                    this.f153int = i2;
                }
                if (f.this.r != null) {
                    this.b = f.this.r.a();
                }
                if (this.b != null) {
                    m196for();
                }
            } catch (Exception e4) {
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:66:0x0291  */
        /* renamed from: for  reason: not valid java name */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m196for() {
            /*
                r20 = this;
                r3 = 0
                r20.m197if()
                java.lang.String r1 = "baidu_location_service"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r4 = "trace1:"
                java.lang.StringBuilder r2 = r2.append(r4)
                r0 = r20
                java.lang.String r4 = r0.f154long
                java.lang.StringBuilder r2 = r2.append(r4)
                java.lang.String r2 = r2.toString()
                com.baidu.location.j.m250if(r1, r2)
                r0 = r20
                com.baidu.location.f r1 = com.baidu.location.f.this     // Catch:{ Exception -> 0x004e }
                boolean r1 = r1.m192else()     // Catch:{ Exception -> 0x004e }
                if (r1 == 0) goto L_0x004b
                java.lang.String r1 = "y2"
            L_0x002c:
                r0 = r20
                boolean r2 = r0.f149do
                if (r2 != 0) goto L_0x0452
                java.io.RandomAccessFile r7 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x044c }
                java.io.File r2 = com.baidu.location.f.j     // Catch:{ Exception -> 0x044c }
                java.lang.String r4 = "rw"
                r7.<init>(r2, r4)     // Catch:{ Exception -> 0x044c }
                long r4 = r7.length()     // Catch:{ Exception -> 0x044c }
                r8 = 1
                int r2 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
                if (r2 >= 0) goto L_0x0052
                r7.close()     // Catch:{ Exception -> 0x044c }
            L_0x004a:
                return
            L_0x004b:
                java.lang.String r1 = "y1"
                goto L_0x002c
            L_0x004e:
                r1 = move-exception
                java.lang.String r1 = "y"
                goto L_0x002c
            L_0x0052:
                int r8 = r7.readInt()     // Catch:{ Exception -> 0x044c }
                r2 = 0
                r6 = r2
            L_0x0058:
                if (r6 >= r8) goto L_0x0452
                int r2 = r6 * 200
                int r2 = r2 + 4
                long r4 = (long) r2     // Catch:{ Exception -> 0x044c }
                r7.seek(r4)     // Catch:{ Exception -> 0x044c }
                r7.readLong()     // Catch:{ Exception -> 0x044c }
                int r9 = r7.readInt()     // Catch:{ Exception -> 0x044c }
                int r10 = r7.readInt()     // Catch:{ Exception -> 0x044c }
                int r11 = r7.readInt()     // Catch:{ Exception -> 0x044c }
                r2 = 200(0xc8, float:2.8E-43)
                byte[] r4 = new byte[r2]     // Catch:{ Exception -> 0x044c }
                r2 = 0
                int r5 = r11 * 8
                int r5 = r5 + 16
                r7.read(r4, r2, r5)     // Catch:{ Exception -> 0x044c }
                r2 = 3
                byte r2 = r4[r2]     // Catch:{ Exception -> 0x044c }
                r2 = r2 & 255(0xff, float:3.57E-43)
                r5 = 2
                byte r5 = r4[r5]     // Catch:{ Exception -> 0x044c }
                int r5 = r5 << 8
                r12 = 65280(0xff00, float:9.1477E-41)
                r5 = r5 & r12
                r2 = r2 | r5
                r5 = 1
                byte r5 = r4[r5]     // Catch:{ Exception -> 0x044c }
                int r5 = r5 << 16
                r12 = 16711680(0xff0000, float:2.3418052E-38)
                r5 = r5 & r12
                r2 = r2 | r5
                r5 = 0
                byte r5 = r4[r5]     // Catch:{ Exception -> 0x044c }
                int r5 = r5 << 24
                r12 = -16777216(0xffffffffff000000, float:-1.7014118E38)
                r5 = r5 & r12
                r2 = r2 | r5
                r5 = 7
                byte r5 = r4[r5]     // Catch:{ Exception -> 0x044c }
                r5 = r5 & 255(0xff, float:3.57E-43)
                r12 = 6
                byte r12 = r4[r12]     // Catch:{ Exception -> 0x044c }
                int r12 = r12 << 8
                r13 = 65280(0xff00, float:9.1477E-41)
                r12 = r12 & r13
                r5 = r5 | r12
                r12 = 5
                byte r12 = r4[r12]     // Catch:{ Exception -> 0x044c }
                int r12 = r12 << 16
                r13 = 16711680(0xff0000, float:2.3418052E-38)
                r12 = r12 & r13
                r5 = r5 | r12
                r12 = 4
                byte r12 = r4[r12]     // Catch:{ Exception -> 0x044c }
                int r12 = r12 << 24
                r13 = -16777216(0xffffffffff000000, float:-1.7014118E38)
                r12 = r12 & r13
                r5 = r5 | r12
                r12 = 11
                byte r12 = r4[r12]     // Catch:{ Exception -> 0x044c }
                r12 = r12 & 255(0xff, float:3.57E-43)
                r13 = 10
                byte r13 = r4[r13]     // Catch:{ Exception -> 0x044c }
                int r13 = r13 << 8
                r14 = 65280(0xff00, float:9.1477E-41)
                r13 = r13 & r14
                r12 = r12 | r13
                r13 = 9
                byte r13 = r4[r13]     // Catch:{ Exception -> 0x044c }
                int r13 = r13 << 16
                r14 = 16711680(0xff0000, float:2.3418052E-38)
                r13 = r13 & r14
                r12 = r12 | r13
                r13 = 8
                byte r13 = r4[r13]     // Catch:{ Exception -> 0x044c }
                int r13 = r13 << 24
                r14 = -16777216(0xffffffffff000000, float:-1.7014118E38)
                r13 = r13 & r14
                r12 = r12 | r13
                r13 = 15
                byte r13 = r4[r13]     // Catch:{ Exception -> 0x044c }
                r13 = r13 & 255(0xff, float:3.57E-43)
                r14 = 14
                byte r14 = r4[r14]     // Catch:{ Exception -> 0x044c }
                int r14 = r14 << 8
                r15 = 65280(0xff00, float:9.1477E-41)
                r14 = r14 & r15
                r13 = r13 | r14
                r14 = 13
                byte r14 = r4[r14]     // Catch:{ Exception -> 0x044c }
                int r14 = r14 << 16
                r15 = 16711680(0xff0000, float:2.3418052E-38)
                r14 = r14 & r15
                r13 = r13 | r14
                r14 = 12
                byte r14 = r4[r14]     // Catch:{ Exception -> 0x044c }
                int r14 = r14 << 24
                r15 = -16777216(0xffffffffff000000, float:-1.7014118E38)
                r14 = r14 & r15
                r13 = r13 | r14
                r0 = r20
                com.baidu.location.c$a r14 = r0.b     // Catch:{ Exception -> 0x044c }
                int r14 = r14.f107do     // Catch:{ Exception -> 0x044c }
                if (r14 != r2) goto L_0x03aa
                r0 = r20
                com.baidu.location.c$a r2 = r0.b     // Catch:{ Exception -> 0x044c }
                int r2 = r2.f109if     // Catch:{ Exception -> 0x044c }
                if (r2 != r5) goto L_0x03aa
                r0 = r20
                com.baidu.location.c$a r2 = r0.b     // Catch:{ Exception -> 0x044c }
                int r2 = r2.f108for     // Catch:{ Exception -> 0x044c }
                if (r2 != r12) goto L_0x03aa
                r0 = r20
                com.baidu.location.c$a r2 = r0.b     // Catch:{ Exception -> 0x044c }
                int r2 = r2.f112try     // Catch:{ Exception -> 0x044c }
                if (r2 != r13) goto L_0x03aa
                long[] r12 = new long[r11]     // Catch:{ Exception -> 0x044c }
                r2 = 0
            L_0x012c:
                if (r2 >= r11) goto L_0x01b5
                int r5 = r2 * 8
                int r5 = r5 + 16
                byte r5 = r4[r5]     // Catch:{ Exception -> 0x044c }
                long r13 = (long) r5     // Catch:{ Exception -> 0x044c }
                r15 = 255(0xff, double:1.26E-321)
                long r13 = r13 & r15
                r5 = 56
                long r13 = r13 << r5
                int r5 = r2 * 8
                int r5 = r5 + 16
                int r5 = r5 + 1
                byte r5 = r4[r5]     // Catch:{ Exception -> 0x044c }
                long r15 = (long) r5     // Catch:{ Exception -> 0x044c }
                r17 = 255(0xff, double:1.26E-321)
                long r15 = r15 & r17
                r5 = 48
                long r15 = r15 << r5
                long r13 = r13 | r15
                int r5 = r2 * 8
                int r5 = r5 + 16
                int r5 = r5 + 2
                byte r5 = r4[r5]     // Catch:{ Exception -> 0x044c }
                long r15 = (long) r5     // Catch:{ Exception -> 0x044c }
                r17 = 255(0xff, double:1.26E-321)
                long r15 = r15 & r17
                r5 = 40
                long r15 = r15 << r5
                long r13 = r13 | r15
                int r5 = r2 * 8
                int r5 = r5 + 16
                int r5 = r5 + 3
                byte r5 = r4[r5]     // Catch:{ Exception -> 0x044c }
                long r15 = (long) r5     // Catch:{ Exception -> 0x044c }
                r17 = 255(0xff, double:1.26E-321)
                long r15 = r15 & r17
                r5 = 32
                long r15 = r15 << r5
                long r13 = r13 | r15
                int r5 = r2 * 8
                int r5 = r5 + 16
                int r5 = r5 + 4
                byte r5 = r4[r5]     // Catch:{ Exception -> 0x044c }
                long r15 = (long) r5     // Catch:{ Exception -> 0x044c }
                r17 = 255(0xff, double:1.26E-321)
                long r15 = r15 & r17
                r5 = 24
                long r15 = r15 << r5
                long r13 = r13 | r15
                int r5 = r2 * 8
                int r5 = r5 + 16
                int r5 = r5 + 5
                byte r5 = r4[r5]     // Catch:{ Exception -> 0x044c }
                long r15 = (long) r5     // Catch:{ Exception -> 0x044c }
                r17 = 255(0xff, double:1.26E-321)
                long r15 = r15 & r17
                r5 = 16
                long r15 = r15 << r5
                long r13 = r13 | r15
                int r5 = r2 * 8
                int r5 = r5 + 16
                int r5 = r5 + 6
                byte r5 = r4[r5]     // Catch:{ Exception -> 0x044c }
                long r15 = (long) r5     // Catch:{ Exception -> 0x044c }
                r17 = 255(0xff, double:1.26E-321)
                long r15 = r15 & r17
                r5 = 8
                long r15 = r15 << r5
                long r13 = r13 | r15
                int r5 = r2 * 8
                int r5 = r5 + 16
                int r5 = r5 + 7
                byte r5 = r4[r5]     // Catch:{ Exception -> 0x044c }
                long r15 = (long) r5     // Catch:{ Exception -> 0x044c }
                r17 = 255(0xff, double:1.26E-321)
                long r15 = r15 & r17
                long r13 = r13 | r15
                r12[r2] = r13     // Catch:{ Exception -> 0x044c }
                int r2 = r2 + 1
                goto L_0x012c
            L_0x01b5:
                r4 = 0
                r2 = 0
                r5 = r2
            L_0x01b8:
                r0 = r20
                int r2 = r0.f153int     // Catch:{ Exception -> 0x044c }
                if (r5 >= r2) goto L_0x01dc
                r2 = 0
                r19 = r2
                r2 = r4
                r4 = r19
            L_0x01c4:
                if (r4 >= r11) goto L_0x01d7
                r0 = r20
                long[] r13 = r0.a     // Catch:{ Exception -> 0x044c }
                r13 = r13[r5]     // Catch:{ Exception -> 0x044c }
                r15 = r12[r4]     // Catch:{ Exception -> 0x044c }
                int r13 = (r13 > r15 ? 1 : (r13 == r15 ? 0 : -1))
                if (r13 != 0) goto L_0x01d4
                int r2 = r2 + 1
            L_0x01d4:
                int r4 = r4 + 1
                goto L_0x01c4
            L_0x01d7:
                int r4 = r5 + 1
                r5 = r4
                r4 = r2
                goto L_0x01b8
            L_0x01dc:
                r2 = 5
                if (r4 > r2) goto L_0x022e
                int r2 = r4 * 8
                r0 = r20
                int r4 = r0.f153int     // Catch:{ Exception -> 0x044c }
                int r4 = r4 + r11
                if (r2 > r4) goto L_0x022e
                if (r11 != 0) goto L_0x01f0
                r0 = r20
                int r2 = r0.f153int     // Catch:{ Exception -> 0x044c }
                if (r2 == 0) goto L_0x022e
            L_0x01f0:
                r2 = 1
                if (r11 != r2) goto L_0x0208
                r0 = r20
                int r2 = r0.f153int     // Catch:{ Exception -> 0x044c }
                r4 = 1
                if (r2 != r4) goto L_0x0208
                r0 = r20
                long[] r2 = r0.a     // Catch:{ Exception -> 0x044c }
                r4 = 0
                r4 = r2[r4]     // Catch:{ Exception -> 0x044c }
                r2 = 0
                r13 = r12[r2]     // Catch:{ Exception -> 0x044c }
                int r2 = (r4 > r13 ? 1 : (r4 == r13 ? 0 : -1))
                if (r2 == 0) goto L_0x022e
            L_0x0208:
                r2 = 1
                if (r11 <= r2) goto L_0x03aa
                r0 = r20
                int r2 = r0.f153int     // Catch:{ Exception -> 0x044c }
                r4 = 1
                if (r2 <= r4) goto L_0x03aa
                r0 = r20
                long[] r2 = r0.a     // Catch:{ Exception -> 0x044c }
                r4 = 0
                r4 = r2[r4]     // Catch:{ Exception -> 0x044c }
                r2 = 0
                r13 = r12[r2]     // Catch:{ Exception -> 0x044c }
                int r2 = (r4 > r13 ? 1 : (r4 == r13 ? 0 : -1))
                if (r2 != 0) goto L_0x03aa
                r0 = r20
                long[] r2 = r0.a     // Catch:{ Exception -> 0x044c }
                r4 = 1
                r4 = r2[r4]     // Catch:{ Exception -> 0x044c }
                r2 = 1
                r11 = r12[r2]     // Catch:{ Exception -> 0x044c }
                int r2 = (r4 > r11 ? 1 : (r4 == r11 ? 0 : -1))
                if (r2 != 0) goto L_0x03aa
            L_0x022e:
                r2 = 1
                int r3 = r10 + 1
                int r4 = r6 * 200
                int r4 = r4 + 16
                long r4 = (long) r4     // Catch:{ Exception -> 0x044c }
                r7.seek(r4)     // Catch:{ Exception -> 0x044c }
                r7.writeInt(r3)     // Catch:{ Exception -> 0x044c }
                r0 = r20
                java.lang.String r3 = r0.f154long     // Catch:{ Exception -> 0x044c }
                if (r3 == 0) goto L_0x0288
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x044c }
                r3.<init>()     // Catch:{ Exception -> 0x044c }
                r0 = r20
                java.lang.String r4 = r0.f154long     // Catch:{ Exception -> 0x044c }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x044c }
                java.lang.String r4 = "|"
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x044c }
                java.lang.StringBuilder r3 = r3.append(r9)     // Catch:{ Exception -> 0x044c }
                java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ Exception -> 0x044c }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x044c }
                r0 = r20
                r0.f154long = r3     // Catch:{ Exception -> 0x044c }
                r0 = r20
                java.lang.String r3 = r0.c     // Catch:{ Exception -> 0x044c }
                if (r3 == 0) goto L_0x0288
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x044c }
                r3.<init>()     // Catch:{ Exception -> 0x044c }
                r0 = r20
                java.lang.String r4 = r0.f154long     // Catch:{ Exception -> 0x044c }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x044c }
                r0 = r20
                java.lang.String r4 = r0.c     // Catch:{ Exception -> 0x044c }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x044c }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x044c }
                r0 = r20
                r0.f154long = r3     // Catch:{ Exception -> 0x044c }
            L_0x0288:
                java.lang.String r3 = "baidu_location_service"
                java.lang.String r4 = "daily info:is same"
                com.baidu.location.j.m250if(r3, r4)     // Catch:{ Exception -> 0x044c }
            L_0x028f:
                if (r2 != 0) goto L_0x0384
                r0 = r20
                com.baidu.location.c$a r2 = r0.b
                int r2 = r2.f107do
                r3 = 460(0x1cc, float:6.45E-43)
                if (r2 != r3) goto L_0x03af
                java.lang.String r2 = "|x,"
            L_0x029d:
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.StringBuilder r2 = r3.append(r2)
                r0 = r20
                com.baidu.location.c$a r3 = r0.b
                int r3 = r3.f109if
                java.lang.StringBuilder r2 = r2.append(r3)
                java.lang.String r3 = ","
                java.lang.StringBuilder r2 = r2.append(r3)
                r0 = r20
                com.baidu.location.c$a r3 = r0.b
                int r3 = r3.f108for
                java.lang.StringBuilder r2 = r2.append(r3)
                java.lang.String r3 = ","
                java.lang.StringBuilder r2 = r2.append(r3)
                r0 = r20
                com.baidu.location.c$a r3 = r0.b
                int r3 = r3.f112try
                java.lang.StringBuilder r2 = r2.append(r3)
                java.lang.String r4 = r2.toString()
                r2 = 0
                r0 = r20
                com.baidu.location.f r5 = com.baidu.location.f.this
                com.baidu.location.e r5 = r5.F
                if (r5 == 0) goto L_0x02f4
                r0 = r20
                com.baidu.location.f r5 = com.baidu.location.f.this
                com.baidu.location.e r5 = r5.F
                java.lang.String r5 = r5.m133char()
                if (r5 == 0) goto L_0x02f4
                r6 = 16
                long r2 = java.lang.Long.parseLong(r5, r6)     // Catch:{ Exception -> 0x0449 }
            L_0x02f4:
                r0 = r20
                int r5 = r0.f153int
                r6 = 1
                if (r5 != r6) goto L_0x03b3
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.StringBuilder r4 = r5.append(r4)
                java.lang.String r5 = "w"
                java.lang.StringBuilder r4 = r4.append(r5)
                r0 = r20
                long[] r5 = r0.a
                r6 = 0
                r5 = r5[r6]
                java.lang.String r5 = java.lang.Long.toHexString(r5)
                java.lang.StringBuilder r4 = r4.append(r5)
                java.lang.String r5 = "k"
                java.lang.StringBuilder r4 = r4.append(r5)
                java.lang.String r4 = r4.toString()
                r0 = r20
                long[] r5 = r0.a
                r6 = 0
                r5 = r5[r6]
                int r2 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
                if (r2 != 0) goto L_0x044f
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.StringBuilder r2 = r2.append(r4)
                java.lang.String r3 = "k"
                java.lang.StringBuilder r2 = r2.append(r3)
                java.lang.String r2 = r2.toString()
            L_0x0341:
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                r0 = r20
                java.lang.String r4 = r0.f154long
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.StringBuilder r2 = r3.append(r2)
                java.lang.StringBuilder r1 = r2.append(r1)
                java.lang.String r1 = r1.toString()
                r0 = r20
                r0.f154long = r1
                r0 = r20
                java.lang.String r1 = r0.c
                if (r1 == 0) goto L_0x0381
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                r0 = r20
                java.lang.String r2 = r0.f154long
                java.lang.StringBuilder r1 = r1.append(r2)
                r0 = r20
                java.lang.String r2 = r0.c
                java.lang.StringBuilder r1 = r1.append(r2)
                java.lang.String r1 = r1.toString()
                r0 = r20
                r0.f154long = r1
            L_0x0381:
                r20.a()
            L_0x0384:
                java.lang.String r1 = "baidu_location_service"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "trace2:"
                java.lang.StringBuilder r2 = r2.append(r3)
                r0 = r20
                java.lang.String r3 = r0.f154long
                java.lang.StringBuilder r2 = r2.append(r3)
                java.lang.String r2 = r2.toString()
                com.baidu.location.j.m250if(r1, r2)
                r20.m194case()
                r1 = 0
                r0 = r20
                r0.f154long = r1
                goto L_0x004a
            L_0x03aa:
                int r2 = r6 + 1
                r6 = r2
                goto L_0x0058
            L_0x03af:
                java.lang.String r2 = "|x460,"
                goto L_0x029d
            L_0x03b3:
                r0 = r20
                int r5 = r0.f153int
                r6 = 1
                if (r5 <= r6) goto L_0x044f
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.StringBuilder r4 = r5.append(r4)
                java.lang.String r5 = "w"
                java.lang.StringBuilder r4 = r4.append(r5)
                r0 = r20
                long[] r5 = r0.a
                r6 = 0
                r5 = r5[r6]
                java.lang.String r5 = java.lang.Long.toHexString(r5)
                java.lang.StringBuilder r4 = r4.append(r5)
                java.lang.String r4 = r4.toString()
                r0 = r20
                long[] r5 = r0.a
                r6 = 0
                r5 = r5[r6]
                int r5 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
                if (r5 != 0) goto L_0x03fc
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.StringBuilder r2 = r2.append(r4)
                java.lang.String r3 = "k"
                java.lang.StringBuilder r2 = r2.append(r3)
                java.lang.String r4 = r2.toString()
                r2 = 0
            L_0x03fc:
                r5 = 0
                int r5 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
                if (r5 <= 0) goto L_0x0425
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.StringBuilder r4 = r5.append(r4)
                java.lang.String r5 = ","
                java.lang.StringBuilder r4 = r4.append(r5)
                java.lang.String r2 = java.lang.Long.toHexString(r2)
                java.lang.StringBuilder r2 = r4.append(r2)
                java.lang.String r3 = "k"
                java.lang.StringBuilder r2 = r2.append(r3)
                java.lang.String r2 = r2.toString()
                goto L_0x0341
            L_0x0425:
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.StringBuilder r2 = r2.append(r4)
                java.lang.String r3 = ","
                java.lang.StringBuilder r2 = r2.append(r3)
                r0 = r20
                long[] r3 = r0.a
                r4 = 1
                r3 = r3[r4]
                java.lang.String r3 = java.lang.Long.toHexString(r3)
                java.lang.StringBuilder r2 = r2.append(r3)
                java.lang.String r2 = r2.toString()
                goto L_0x0341
            L_0x0449:
                r5 = move-exception
                goto L_0x02f4
            L_0x044c:
                r1 = move-exception
                goto L_0x004a
            L_0x044f:
                r2 = r4
                goto L_0x0341
            L_0x0452:
                r2 = r3
                goto L_0x028f
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.f.c.m196for():void");
        }

        /* renamed from: if  reason: not valid java name */
        public void m197if() {
            try {
                if (f.this.m != null) {
                    File unused = f.j = new File(f.this.m);
                    if (!f.j.exists()) {
                        File file = new File(f.ac);
                        if (!file.exists()) {
                            file.mkdirs();
                        }
                        f.j.createNewFile();
                        RandomAccessFile randomAccessFile = new RandomAccessFile(f.j, "rw");
                        randomAccessFile.seek(0);
                        randomAccessFile.writeInt(0);
                        randomAccessFile.close();
                        return;
                    }
                    return;
                }
                File unused2 = f.j = (File) null;
            } catch (Exception e2) {
                File unused3 = f.j = (File) null;
            }
        }

        /* renamed from: int  reason: not valid java name */
        public void m198int() {
        }

        /* renamed from: new  reason: not valid java name */
        public void m199new() {
            this.f149do = false;
            this.f152if = false;
            m197if();
            f.m181long();
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(f.k, "rw");
                randomAccessFile.seek(0);
                int readInt = randomAccessFile.readInt();
                int readInt2 = randomAccessFile.readInt();
                randomAccessFile.readInt();
                long readLong = randomAccessFile.readLong();
                int readInt3 = randomAccessFile.readInt();
                if (readInt < 0) {
                    this.f149do = true;
                    this.f152if = true;
                    randomAccessFile.close();
                    return;
                }
                randomAccessFile.seek((long) ((readInt2 * f.at) + 24));
                int readInt4 = randomAccessFile.readInt();
                if (readInt4 > 680) {
                    this.f149do = true;
                    this.f152if = true;
                    randomAccessFile.close();
                    return;
                }
                byte[] bArr = new byte[f.at];
                randomAccessFile.read(bArr, 0, readInt4);
                if (readInt4 != randomAccessFile.readInt()) {
                    j.m250if(f.v, "trace true check fail");
                    this.f149do = true;
                    this.f152if = true;
                    randomAccessFile.close();
                    return;
                }
                for (int i = 0; i < bArr.length; i++) {
                    bArr[i] = (byte) (bArr[i] ^ 90);
                }
                this.f154long = new String(bArr, 0, readInt4);
                if (!this.f154long.contains("&tr=")) {
                    this.f149do = true;
                    this.f152if = true;
                    randomAccessFile.close();
                    return;
                }
                long currentTimeMillis = System.currentTimeMillis();
                long j = currentTimeMillis - readLong;
                if (j > (j.M * 3) - j.al) {
                    this.f149do = true;
                } else if (j > (j.M * 2) - j.al) {
                    this.f154long += "|" + readInt3;
                    this.f146byte = readInt3 + 2;
                } else if (j > j.M - j.al) {
                    this.f146byte = readInt3 + 1;
                } else {
                    this.f150else = true;
                    randomAccessFile.close();
                    return;
                }
                randomAccessFile.seek(12);
                randomAccessFile.writeLong(currentTimeMillis);
                randomAccessFile.writeInt(this.f146byte);
                randomAccessFile.close();
                RandomAccessFile randomAccessFile2 = new RandomAccessFile(f.j, "rw");
                randomAccessFile2.seek(0);
                if (randomAccessFile2.readInt() == 0) {
                    this.f149do = true;
                    randomAccessFile2.close();
                    j.m250if(f.v, "Day file number 0");
                    return;
                }
                randomAccessFile2.close();
            } catch (Exception e2) {
                j.m250if(f.v, "exception!!!");
                this.f149do = true;
                this.f152if = true;
            }
        }

        /* renamed from: try  reason: not valid java name */
        public void m200try() {
            this.f151goto.unregisterReceiver(this.f147case);
            this.f156try.cancel(this.d);
            File unused = f.j = (File) null;
        }
    }

    public class d extends Handler {
        public d() {
        }

        public void handleMessage(Message message) {
            if (f.this.ad) {
                switch (message.what) {
                    case 11:
                        f.this.m172if(message);
                        break;
                    case 12:
                        f.this.m184new(message);
                        break;
                    case 15:
                        f.this.m187try(message);
                        break;
                    case 21:
                        f.this.a(message, 21);
                        break;
                    case 22:
                        f.this.m177int(message);
                        break;
                    case 25:
                        f.this.m160do(message);
                        break;
                    case f.I /*26*/:
                        f.this.a(message, f.I);
                        break;
                    case f.f133do /*28*/:
                        f.this.m165for(message);
                        break;
                    case 31:
                        f.this.m169goto();
                        break;
                    case f.p /*41*/:
                        f.this.m159do();
                        break;
                    case 51:
                        f.this.m171if();
                        break;
                    case 52:
                        f.this.b();
                        break;
                    case f.ar /*53*/:
                        f.this.c();
                        break;
                    case f.D /*57*/:
                        f.this.a(message);
                        break;
                    case 62:
                    case 63:
                        f.this.a(21);
                        break;
                    case f.K /*64*/:
                    case 65:
                        f.this.a((int) f.I);
                        break;
                    case 81:
                        f.this.m186try();
                        break;
                    case f.t /*91*/:
                        f.this.m176int();
                        break;
                    case f.B /*92*/:
                        f.this.m156char();
                        break;
                    case f.ao /*101*/:
                        if (f.this.y != null) {
                            f.this.y.m195do();
                            break;
                        }
                        break;
                }
            }
            super.handleMessage(message);
        }
    }

    private String a(String str) {
        String str2;
        String str3 = null;
        j.m250if(v, "generate locdata ...");
        if ((this.f143try == null || !this.f143try.m120do()) && this.r != null) {
            this.f143try = this.r.a();
        }
        this.A = this.f143try.a();
        if (this.f143try != null) {
            j.a(v, this.f143try.m122if());
        } else {
            j.a(v, "cellInfo null...");
        }
        if ((this.C == null || !this.C.m145for()) && this.F != null) {
            this.C = this.F.m131byte();
        }
        if (this.C != null) {
            j.a(v, this.C.m144else());
        } else {
            j.a(v, "wifi list null");
        }
        if (this.ab == null || !this.ab.m88new()) {
            this.f = null;
        } else {
            this.f = this.ab.m89try();
        }
        if (this.au != null) {
            str3 = this.au.m39byte();
        }
        String format = 3 == g.m204do(this) ? "&cn=32" : String.format("&cn=%d", Integer.valueOf(this.r.m119new()));
        if (this.Y && (str2 = k.m261if()) != null) {
            format = format + str2;
        }
        String str4 = format + str3;
        if (str != null) {
            str4 = str + str4;
        }
        return j.a(this.f143try, this.C, this.f, str4, 0);
    }

    private String a(boolean z2) {
        if ((this.f143try == null || !this.f143try.m120do()) && this.r != null) {
            this.f143try = this.r.a();
        }
        m162do(this.f143try.a());
        return m170if(z2);
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        j.m250if(v, "on network exception");
        j.a(v, "on network exception");
        this.f142new = null;
        this.f139char = null;
        if (this.au != null) {
            this.au.a(a(false), i2);
        }
        if (i2 == 21) {
            m154case();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.k.a(android.location.Location, boolean):boolean
     arg types: [android.location.Location, int]
     candidates:
      com.baidu.location.k.a(java.util.List, int):int
      com.baidu.location.k.a(int, int):void
      com.baidu.location.k.a(int, boolean):void
      com.baidu.location.k.a(java.lang.String, int):void
      com.baidu.location.k.a(android.location.Location, com.baidu.location.e$c):boolean
      com.baidu.location.k.a(java.lang.String, java.util.List):boolean
      com.baidu.location.k.a(android.location.Location, boolean):boolean */
    /* access modifiers changed from: private */
    public void a(Message message) {
        if (message == null || message.obj == null) {
            j.m250if(v, "Gps updateloation is null");
            return;
        }
        Location location = (Location) message.obj;
        if (location != null) {
            j.m250if(v, "on update gps...");
            if (!k.a(location, true) || this.r == null || this.F == null || this.au != null) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(Message message, int i2) {
        j.m250if(v, "on network success");
        j.a(v, "on network success");
        String str = (String) message.obj;
        j.m250if(v, "network:" + str);
        if (this.au != null) {
            this.au.a(str, i2);
        }
        if (j.m244do(str)) {
            if (i2 == 21) {
                this.f142new = str;
            } else {
                this.H = str;
            }
        } else if (i2 == 21) {
            this.f142new = null;
        } else {
            this.H = null;
        }
        int i3 = j.m246if(str, "ssid\":\"", "\"");
        if (i3 == Integer.MIN_VALUE || this.f139char == null) {
            this.h = null;
        } else {
            this.h = this.f139char.m146if(i3);
        }
        m174if(str);
        double d2 = j.m242do(str, "a\":\"", "\"");
        if (d2 != Double.MIN_VALUE) {
            k.a(d2, j.m242do(str, "b\":\"", "\""), j.m242do(str, "c\":\"", "\""), j.m242do(str, "b\":\"", "\""));
        }
        int i4 = j.m246if(str, "rWifiN\":\"", "\"");
        if (i4 > 15) {
            j.Y = i4;
        }
        int i5 = j.m246if(str, "rWifiT\":\"", "\"");
        if (i5 > 500) {
            j.S = i5;
        }
        float a2 = j.a(str, "hSpeedDis\":\"", "\"");
        if (a2 > 5.0f) {
            j.Q = a2;
        }
        float a3 = j.a(str, "mSpeedDis\":\"", "\"");
        if (a3 > 5.0f) {
            j.ai = a3;
        }
        float a4 = j.a(str, "mWifiR\":\"", "\"");
        if (a4 < 1.0f && ((double) a4) > 0.2d) {
            j.f196byte = a4;
        }
        if (i2 == 21) {
            m154case();
        }
    }

    private boolean a(c.a aVar) {
        boolean z2 = true;
        if (this.r == null) {
            return false;
        }
        this.f143try = this.r.a();
        if (this.f143try == aVar) {
            return false;
        }
        if (this.f143try == null || aVar == null) {
            return true;
        }
        if (aVar.a(this.f143try)) {
            z2 = false;
        }
        return z2;
    }

    private boolean a(e.c cVar) {
        boolean z2 = true;
        if (this.F == null) {
            return false;
        }
        this.C = this.F.m131byte();
        if (cVar == this.C) {
            return false;
        }
        if (this.C == null || cVar == null) {
            return true;
        }
        if (cVar.a(this.C)) {
            z2 = false;
        }
        return z2;
    }

    /* access modifiers changed from: private */
    public void b() {
        j.m250if(v, "on switch gps ...");
        if (this.au != null) {
            if (this.au.m41for()) {
                if (this.ab == null) {
                    this.ab = new b(this, this.R);
                }
                this.ab.k();
            } else if (this.ab != null) {
                this.ab.l();
                this.ab = null;
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: byte  reason: not valid java name */
    public void m153byte() {
        if (!this.O) {
            if (System.currentTimeMillis() - this.P < 1000) {
                j.m250if(v, "request too frequency ...");
                if (this.f142new != null) {
                    this.au.a(this.f142new);
                    m154case();
                    return;
                }
            }
            j.m250if(v, "start network locating ...");
            j.a(v, "start network locating ...");
            this.O = true;
            this.J = a(this.al);
            if (a(this.f139char) || this.J || this.f142new == null) {
                String a2 = a((String) null);
                if (a2 == null) {
                    this.au.a("{\"result\":{\"time\":\"" + j.m245for() + "\",\"error\":\"62\"}}");
                    m154case();
                    return;
                }
                if (this.h != null) {
                    a2 = a2 + this.h;
                    this.h = null;
                }
                if (g.a(a2, this.R)) {
                    this.al = this.f143try;
                    this.f139char = this.C;
                } else {
                    j.m250if(v, "request error ..");
                }
                if (this.Y) {
                    this.Y = false;
                }
                this.P = System.currentTimeMillis();
                return;
            }
            this.au.a(this.f142new);
            m154case();
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.au != null) {
            this.au.m47new();
        }
    }

    /* renamed from: case  reason: not valid java name */
    private void m154case() {
        this.O = false;
        m190void();
    }

    /* access modifiers changed from: private */
    /* renamed from: char  reason: not valid java name */
    public void m156char() {
        try {
            if (j.n && j.G) {
                this.y = new c(this);
            }
        } catch (Exception e2) {
        }
    }

    private void d() {
        File file = new File(ac);
        File file2 = new File(ac + "/ls.db");
        if (!file.exists()) {
            file.mkdirs();
        }
        if (!file2.exists()) {
            try {
                file2.createNewFile();
            } catch (Exception e2) {
            }
        }
        if (file2.exists()) {
            this.T = SQLiteDatabase.openOrCreateDatabase(file2, (SQLiteDatabase.CursorFactory) null);
            this.T.execSQL("CREATE TABLE IF NOT EXISTS " + this.f141if + "(id CHAR(40) PRIMARY KEY,time DOUBLE,tag DOUBLE, type DOUBLE , ac INT);");
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: do  reason: not valid java name */
    public void m159do() {
        j.m250if(v, "on new wifi ...");
        if (this.aj) {
            m153byte();
            this.aj = false;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: do  reason: not valid java name */
    public void m160do(Message message) {
        if (System.currentTimeMillis() - this.d < 3000) {
            j.m250if(v, "request too frequency ...");
            if (this.H != null) {
                this.au.a(this.H, (int) I);
                return;
            }
        }
        if (this.au != null) {
            String a2 = a(this.au.a(message));
            if (this.h != null) {
                a2 = a2 + this.h;
                this.h = null;
            }
            g.m204do(this);
            if (g.m219if(a2, this.R)) {
                this.u = this.f143try;
                this.ae = this.C;
            } else {
                j.m250if(v, "request poi error ..");
            }
            this.d = System.currentTimeMillis();
        }
    }

    /* renamed from: do  reason: not valid java name */
    private void m162do(String str) {
        if (this.T == null || str == null) {
            j.m250if(v, "db is null...");
            this.Q = false;
            return;
        }
        j.m250if(v, "LOCATING...");
        if (System.currentTimeMillis() - this.E >= 1500 && !str.equals(this.as)) {
            this.Q = false;
            try {
                Cursor rawQuery = this.T.rawQuery("select * from " + this.f141if + " where id = \"" + str + "\";", null);
                this.as = str;
                this.E = System.currentTimeMillis();
                if (rawQuery != null) {
                    if (rawQuery.moveToFirst()) {
                        j.m250if(v, "lookup DB success:" + this.as);
                        this.o = rawQuery.getDouble(1) - 1235.4323d;
                        this.q = rawQuery.getDouble(2) - 4326.0d;
                        this.n = rawQuery.getDouble(3) - 2367.3217d;
                        this.Q = true;
                        j.m250if(v, "lookup DB success:x" + this.o + "y" + this.n + "r" + this.q);
                    }
                    rawQuery.close();
                }
            } catch (Exception e2) {
                this.E = System.currentTimeMillis();
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: for  reason: not valid java name */
    public void m165for(Message message) {
        if (this.au != null) {
            this.au.a(a(true), message);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: goto  reason: not valid java name */
    public void m169goto() {
        j.m250if(v, "on new cell ...");
    }

    /* renamed from: if  reason: not valid java name */
    private String m170if(boolean z2) {
        if (!this.Q) {
            return z2 ? "{\"result\":{\"time\":\"" + j.m245for() + "\",\"error\":\"67\"}}" : "{\"result\":{\"time\":\"" + j.m245for() + "\",\"error\":\"63\"}}";
        }
        if (z2) {
            return String.format("{\"result\":{\"time\":\"" + j.m245for() + "\",\"error\":\"66\"},\"content\":{\"point\":{\"x\":" + "\"%f\",\"y\":\"%f\"},\"radius\":\"%f\",\"isCellChanged\":\"%b\"}}", Double.valueOf(this.o), Double.valueOf(this.n), Double.valueOf(this.q), true);
        }
        return String.format("{\"result\":{\"time\":\"" + j.m245for() + "\",\"error\":\"68\"},\"content\":{\"point\":{\"x\":" + "\"%f\",\"y\":\"%f\"},\"radius\":\"%f\",\"isCellChanged\":\"%b\"}}", Double.valueOf(this.o), Double.valueOf(this.n), Double.valueOf(this.q), Boolean.valueOf(this.J));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.k.a(android.location.Location, boolean):boolean
     arg types: [android.location.Location, int]
     candidates:
      com.baidu.location.k.a(java.util.List, int):int
      com.baidu.location.k.a(int, int):void
      com.baidu.location.k.a(int, boolean):void
      com.baidu.location.k.a(java.lang.String, int):void
      com.baidu.location.k.a(android.location.Location, com.baidu.location.e$c):boolean
      com.baidu.location.k.a(java.lang.String, java.util.List):boolean
      com.baidu.location.k.a(android.location.Location, boolean):boolean */
    /* access modifiers changed from: private */
    /* renamed from: if  reason: not valid java name */
    public void m171if() {
        if (this.ab != null) {
            j.m250if(v, "on new gps...");
            Location location = this.ab.m89try();
            if (!(!this.ab.m88new() || !k.a(location, true) || this.r == null || this.F == null || this.au == null)) {
                if (this.F != null) {
                    this.F.a();
                }
                k.a(this.r.a(), this.F.m136int(), location, this.au.m39byte());
            }
            if (this.au != null && this.ab.m88new()) {
                this.au.m45if(this.ab.m87int());
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: if  reason: not valid java name */
    public void m172if(Message message) {
        if (this.au != null) {
            this.au.m46int(message);
        }
        if (this.F != null) {
            this.F.m132case();
        }
        if (this.N) {
            g.m211for(this.R);
            this.N = false;
        }
    }

    /* renamed from: if  reason: not valid java name */
    private void m174if(String str) {
        float f2;
        double d2;
        double d3;
        boolean z2 = false;
        if (this.T != null && this.J) {
            try {
                j.m250if(v, "DB:" + str);
                JSONObject jSONObject = new JSONObject(str);
                int parseInt = Integer.parseInt(jSONObject.getJSONObject("result").getString("error"));
                if (parseInt == 161) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject("content");
                    if (jSONObject2.has("clf")) {
                        String string = jSONObject2.getString("clf");
                        if (string.equals("0")) {
                            JSONObject jSONObject3 = jSONObject2.getJSONObject("point");
                            d2 = Double.parseDouble(jSONObject3.getString("x"));
                            d3 = Double.parseDouble(jSONObject3.getString("y"));
                            f2 = Float.parseFloat(jSONObject2.getString("radius"));
                        } else {
                            String[] split = string.split("\\|");
                            d2 = Double.parseDouble(split[0]);
                            d3 = Double.parseDouble(split[1]);
                            f2 = Float.parseFloat(split[2]);
                        }
                        j.m250if(v, "DB PARSE:x" + d2 + "y" + d3 + "R" + f2);
                    }
                    z2 = true;
                    f2 = 0.0f;
                    d2 = 0.0d;
                    d3 = 0.0d;
                } else {
                    if (parseInt == 167) {
                        this.T.delete(this.f141if, "id = \"" + this.A + "\"", null);
                        return;
                    }
                    z2 = true;
                    f2 = 0.0f;
                    d2 = 0.0d;
                    d3 = 0.0d;
                }
                if (!z2) {
                    float f3 = 4326.0f + f2;
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("time", Double.valueOf(d2 + 1235.4323d));
                    contentValues.put(HouseBaseInfo.TAG, Float.valueOf(f3));
                    contentValues.put(CouponDetailsActivity.INTENT_TYPE, Double.valueOf(d3 + 2367.3217d));
                    try {
                        if (this.T.update(this.f141if, contentValues, "id = \"" + this.A + "\"", null) <= 0) {
                            contentValues.put("id", this.A);
                            this.T.insert(this.f141if, null, contentValues);
                            j.m250if(v, "insert DB success!");
                        }
                    } catch (Exception e2) {
                    }
                }
            } catch (Exception e3) {
                j.m250if(v, "DB PARSE:exp!");
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: int  reason: not valid java name */
    public void m176int() {
        if (g.a(this)) {
            g.f();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: int  reason: not valid java name */
    public void m177int(Message message) {
        j.m250if(v, "on request location ...");
        j.a(v, "on request location ...");
        if (this.au != null) {
            if (this.au.m40do(message) == 1 && this.ab != null && this.ab.m88new()) {
                j.m250if(v, "send gps location to client ...");
                this.au.a(this.ab.m87int(), message);
            } else if (this.Y) {
                m153byte();
            } else if (this.O) {
            } else {
                if (this.F == null || !this.F.m137new()) {
                    m153byte();
                    return;
                }
                this.aj = true;
                this.R.postDelayed(new b(), 2000);
            }
        }
    }

    /* renamed from: long  reason: not valid java name */
    public static void m181long() {
        try {
            if (a != null) {
                k = new File(a);
                if (!k.exists()) {
                    File file = new File(ac);
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                    k.createNewFile();
                    RandomAccessFile randomAccessFile = new RandomAccessFile(k, "rw");
                    randomAccessFile.seek(0);
                    randomAccessFile.writeInt(-1);
                    randomAccessFile.writeInt(-1);
                    randomAccessFile.writeInt(0);
                    randomAccessFile.writeLong(0);
                    randomAccessFile.writeInt(0);
                    randomAccessFile.writeInt(0);
                    randomAccessFile.close();
                    return;
                }
                return;
            }
            k = null;
        } catch (Exception e2) {
            k = null;
        }
    }

    /* renamed from: new  reason: not valid java name */
    public static String m183new() {
        j.m250if(v, "read trace log1..");
        return null;
    }

    /* access modifiers changed from: private */
    /* renamed from: new  reason: not valid java name */
    public void m184new(Message message) {
        if (this.au != null) {
            this.au.m44if(message);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: try  reason: not valid java name */
    public void m186try() {
    }

    /* access modifiers changed from: private */
    /* renamed from: try  reason: not valid java name */
    public void m187try(Message message) {
        if (!(this.au == null || !this.au.m42for(message) || this.F == null)) {
            this.F.m135for();
        }
        this.f142new = null;
    }

    /* renamed from: void  reason: not valid java name */
    private void m190void() {
        if (this.f142new != null && g.a(this)) {
            g.f();
        }
    }

    /* renamed from: else  reason: not valid java name */
    public boolean m192else() {
        return ((KeyguardManager) getSystemService("keyguard")).inKeyguardRestrictedInputMode();
    }

    public IBinder onBind(Intent intent) {
        return this.an.getBinder();
    }

    public void onCreate() {
        Thread.setDefaultUncaughtExceptionHandler(new a(this));
        this.r = new c(this, this.R);
        this.F = new e(this, this.R);
        this.au = new a(this.R);
        this.r.m116do();
        this.F.m138try();
        this.ad = true;
        this.O = false;
        this.aj = false;
        try {
            d();
        } catch (Exception e2) {
        }
        j.m250if(v, "OnCreate");
        Log.d(v, "baidu location service start1 ..." + Process.myPid());
    }

    public void onDestroy() {
        if (this.r != null) {
            this.r.m115byte();
        }
        if (this.F != null) {
            this.F.m134else();
        }
        if (this.ab != null) {
            this.ab.l();
        }
        k.a();
        this.O = false;
        this.aj = false;
        this.ad = false;
        if (this.y != null) {
            this.y.m200try();
        }
        if (this.T != null) {
            this.T.close();
        }
        j.m250if(v, "onDestroy");
        Log.d(v, "baidu location service stop ...");
        Process.killProcess(Process.myPid());
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        j.m250if(v, "onStratCommandNotSticky");
        return 2;
    }
}
