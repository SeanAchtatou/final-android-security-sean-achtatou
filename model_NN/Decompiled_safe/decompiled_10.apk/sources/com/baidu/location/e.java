package com.baidu.location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import org.apache.commons.lang.StringUtils;

class e {

    /* renamed from: byte  reason: not valid java name */
    private static final int f114byte = 15;
    /* access modifiers changed from: private */

    /* renamed from: try  reason: not valid java name */
    public static String f115try = f.v;
    private final long a = 5000;
    private long b = 0;
    private a c = null;

    /* renamed from: case  reason: not valid java name */
    private c f116case = null;

    /* renamed from: char  reason: not valid java name */
    private b f117char = null;
    /* access modifiers changed from: private */
    public boolean d = false;
    /* access modifiers changed from: private */

    /* renamed from: do  reason: not valid java name */
    public boolean f118do = false;
    private Method e = null;

    /* renamed from: else  reason: not valid java name */
    private boolean f119else = false;
    private final long f = 3000;

    /* renamed from: for  reason: not valid java name */
    private boolean f120for = true;
    private Object g = null;

    /* renamed from: goto  reason: not valid java name */
    private Context f121goto;
    /* access modifiers changed from: private */

    /* renamed from: if  reason: not valid java name */
    public Handler f122if = null;

    /* renamed from: int  reason: not valid java name */
    private boolean f123int = false;

    /* renamed from: long  reason: not valid java name */
    private long f124long = 0;

    /* renamed from: new  reason: not valid java name */
    private final long f125new = 3000;

    /* renamed from: void  reason: not valid java name */
    private WifiManager f126void = null;

    private class a extends BroadcastReceiver {
        private a() {
        }

        public void onReceive(Context context, Intent intent) {
            if (context != null && e.this.f122if != null) {
                e.this.m128goto();
            }
        }
    }

    private class b extends BroadcastReceiver {
        private b() {
        }

        public void onReceive(Context context, Intent intent) {
            if (context != null && e.this.f122if != null) {
                e.this.m130if();
                e.this.f122if.obtainMessage(41).sendToTarget();
                j.m250if(e.f115try, "wifi manager receive new wifi...");
            }
        }
    }

    protected class c {

        /* renamed from: do  reason: not valid java name */
        private boolean f127do = false;

        /* renamed from: for  reason: not valid java name */
        public List f128for = null;

        /* renamed from: if  reason: not valid java name */
        private long f129if = 0;

        /* renamed from: int  reason: not valid java name */
        private long f130int = 0;

        public c(List list, long j) {
            this.f129if = j;
            this.f128for = list;
            this.f130int = System.currentTimeMillis();
            a();
            j.a(e.f115try, m149int());
        }

        private void a() {
            boolean z;
            if (m151try() >= 1) {
                boolean z2 = true;
                for (int size = this.f128for.size() - 1; size >= 1 && z2; size--) {
                    int i = 0;
                    z2 = false;
                    while (i < size) {
                        if (((ScanResult) this.f128for.get(i)).level < ((ScanResult) this.f128for.get(i + 1)).level) {
                            this.f128for.set(i + 1, this.f128for.get(i));
                            this.f128for.set(i, (ScanResult) this.f128for.get(i + 1));
                            z = true;
                        } else {
                            z = z2;
                        }
                        i++;
                        z2 = z;
                    }
                }
            }
        }

        public String a(int i) {
            int i2;
            boolean z;
            if (m151try() < 1) {
                return null;
            }
            StringBuffer stringBuffer = new StringBuffer(512);
            String str = e.this.m133char();
            int size = this.f128for.size();
            if (size <= i) {
                i = size;
            }
            int i3 = 0;
            boolean z2 = true;
            int i4 = 0;
            int i5 = 0;
            while (i3 < i) {
                if (((ScanResult) this.f128for.get(i3)).level == 0) {
                    z = z2;
                    i2 = i5;
                } else if (z2) {
                    stringBuffer.append("&wf=");
                    String replace = ((ScanResult) this.f128for.get(i3)).BSSID.replace(":", StringUtils.EMPTY);
                    stringBuffer.append(replace);
                    int i6 = ((ScanResult) this.f128for.get(i3)).level;
                    if (i6 < 0) {
                        i6 = -i6;
                    }
                    stringBuffer.append(String.format(";%d;", Integer.valueOf(i6)));
                    i2 = i5 + 1;
                    i4 = (str == null || !str.equals(replace)) ? i4 : i2;
                    z = false;
                } else {
                    stringBuffer.append("|");
                    String replace2 = ((ScanResult) this.f128for.get(i3)).BSSID.replace(":", StringUtils.EMPTY);
                    stringBuffer.append(replace2);
                    int i7 = ((ScanResult) this.f128for.get(i3)).level;
                    if (i7 < 0) {
                        i7 = -i7;
                    }
                    stringBuffer.append(String.format(";%d;", Integer.valueOf(i7)));
                    int i8 = i5 + 1;
                    if (str == null || !str.equals(replace2)) {
                        boolean z3 = z2;
                        i2 = i8;
                        z = z3;
                    } else {
                        i4 = i8;
                        boolean z4 = z2;
                        i2 = i8;
                        z = z4;
                    }
                }
                i3++;
                i5 = i2;
                z2 = z;
            }
            if (z2) {
                return null;
            }
            j.m250if(e.f115try, str + i4);
            stringBuffer.append("&wf_n=" + i4);
            stringBuffer.append("&wf_st=");
            stringBuffer.append(this.f129if);
            stringBuffer.append("&wf_et=");
            stringBuffer.append(this.f130int);
            if (i4 > 0) {
                this.f127do = true;
            }
            return stringBuffer.toString();
        }

        public boolean a(c cVar) {
            return a(cVar, this, j.a);
        }

        public boolean a(c cVar, c cVar2, float f) {
            int i;
            if (cVar == null || cVar2 == null) {
                return false;
            }
            List list = cVar.f128for;
            List list2 = cVar2.f128for;
            if (list == list2) {
                return true;
            }
            if (list == null || list2 == null) {
                return false;
            }
            int size = list.size();
            int size2 = list2.size();
            float f2 = (float) (size + size2);
            if (size == 0 && size2 == 0) {
                return true;
            }
            if (size == 0 || size2 == 0) {
                return false;
            }
            int i2 = 0;
            int i3 = 0;
            while (i2 < size) {
                String str = ((ScanResult) list.get(i2)).BSSID;
                if (str != null) {
                    int i4 = 0;
                    while (true) {
                        if (i4 >= size2) {
                            i = i3;
                            break;
                        } else if (str.equals(((ScanResult) list2.get(i4)).BSSID)) {
                            i = i3 + 1;
                            break;
                        } else {
                            i4++;
                        }
                    }
                } else {
                    i = i3;
                }
                i2++;
                i3 = i;
            }
            j.m250if(e.f115try, String.format("same %d,total %f,rate %f...", Integer.valueOf(i3), Float.valueOf(f2), Float.valueOf(f)));
            return ((float) (i3 * 2)) >= f2 * f;
        }

        /* renamed from: byte  reason: not valid java name */
        public String m139byte() {
            try {
                return a(15);
            } catch (Exception e) {
                return null;
            }
        }

        /* renamed from: case  reason: not valid java name */
        public boolean m140case() {
            return this.f127do;
        }

        /* renamed from: char  reason: not valid java name */
        public String m141char() {
            try {
                return a(j.Y);
            } catch (Exception e) {
                return null;
            }
        }

        /* renamed from: do  reason: not valid java name */
        public int m142do() {
            for (int i = 0; i < m151try(); i++) {
                int i2 = -((ScanResult) this.f128for.get(i)).level;
                if (i2 > 0) {
                    return i2;
                }
            }
            return 0;
        }

        /* renamed from: do  reason: not valid java name */
        public boolean m143do(c cVar) {
            if (this.f128for == null || cVar == null || cVar.f128for == null) {
                return false;
            }
            int size = this.f128for.size() < cVar.f128for.size() ? this.f128for.size() : cVar.f128for.size();
            for (int i = 0; i < size; i++) {
                String str = ((ScanResult) this.f128for.get(i)).BSSID;
                int i2 = ((ScanResult) this.f128for.get(i)).level;
                String str2 = ((ScanResult) cVar.f128for.get(i)).BSSID;
                int i3 = ((ScanResult) cVar.f128for.get(i)).level;
                if (!str.equals(str2) || i2 != i3) {
                    return false;
                }
            }
            return true;
        }

        /* renamed from: else  reason: not valid java name */
        public String m144else() {
            boolean z;
            StringBuffer stringBuffer = new StringBuffer(512);
            stringBuffer.append("wifi info:");
            if (m151try() < 1) {
                return stringBuffer.toString();
            }
            int size = this.f128for.size();
            if (size > 10) {
                size = 10;
            }
            int i = 0;
            boolean z2 = true;
            while (i < size) {
                if (((ScanResult) this.f128for.get(i)).level == 0) {
                    z = z2;
                } else if (z2) {
                    stringBuffer.append("wifi=");
                    stringBuffer.append(((ScanResult) this.f128for.get(i)).BSSID.replace(":", StringUtils.EMPTY));
                    stringBuffer.append(String.format(";%d;", Integer.valueOf(((ScanResult) this.f128for.get(i)).level)));
                    z = false;
                } else {
                    stringBuffer.append(";");
                    stringBuffer.append(((ScanResult) this.f128for.get(i)).BSSID.replace(":", StringUtils.EMPTY));
                    stringBuffer.append(String.format(",%d;", Integer.valueOf(((ScanResult) this.f128for.get(i)).level)));
                    z = z2;
                }
                i++;
                z2 = z;
            }
            return stringBuffer.toString();
        }

        /* renamed from: for  reason: not valid java name */
        public boolean m145for() {
            return System.currentTimeMillis() - this.f130int < 3000;
        }

        /* renamed from: if  reason: not valid java name */
        public String m146if(int i) {
            int i2 = 0;
            if (i == 0 || m151try() < 1) {
                return null;
            }
            StringBuffer stringBuffer = new StringBuffer((int) AccessibilityEventCompat.TYPE_VIEW_HOVER_EXIT);
            int i3 = 0;
            int i4 = 1;
            while (true) {
                int i5 = i2;
                if (i5 >= j.Y) {
                    return stringBuffer.toString();
                }
                if ((i4 & i) != 0) {
                    if (i3 == 0) {
                        stringBuffer.append("&ssid=");
                    } else {
                        stringBuffer.append("|");
                    }
                    stringBuffer.append(((ScanResult) this.f128for.get(i5)).BSSID);
                    stringBuffer.append(";");
                    stringBuffer.append(((ScanResult) this.f128for.get(i5)).SSID);
                    i3++;
                }
                i4 <<= 1;
                i2 = i5 + 1;
            }
        }

        /* renamed from: if  reason: not valid java name */
        public boolean m147if() {
            return System.currentTimeMillis() - this.f129if < 3000;
        }

        /* renamed from: if  reason: not valid java name */
        public boolean m148if(c cVar) {
            if (this.f128for == null || cVar == null || cVar.f128for == null) {
                return false;
            }
            int size = this.f128for.size() < cVar.f128for.size() ? this.f128for.size() : cVar.f128for.size();
            for (int i = 0; i < size; i++) {
                if (!((ScanResult) this.f128for.get(i)).BSSID.equals(((ScanResult) cVar.f128for.get(i)).BSSID)) {
                    return false;
                }
            }
            return true;
        }

        /* renamed from: int  reason: not valid java name */
        public String m149int() {
            StringBuilder sb = new StringBuilder();
            sb.append("wifi=");
            if (this.f128for == null) {
                return sb.toString();
            }
            for (int i = 0; i < this.f128for.size(); i++) {
                int i2 = ((ScanResult) this.f128for.get(i)).level;
                sb.append(((ScanResult) this.f128for.get(i)).BSSID.replace(":", StringUtils.EMPTY));
                sb.append(String.format(",%d;", Integer.valueOf(i2)));
            }
            return sb.toString();
        }

        /* renamed from: new  reason: not valid java name */
        public boolean m150new() {
            return System.currentTimeMillis() - this.f130int < 5000;
        }

        /* renamed from: try  reason: not valid java name */
        public int m151try() {
            if (this.f128for == null) {
                return 0;
            }
            return this.f128for.size();
        }
    }

    private class d implements Runnable {
        private d() {
        }

        public void run() {
            if (!e.this.d || !j.R) {
                boolean unused = e.this.f118do = false;
                return;
            }
            e.this.f122if.obtainMessage(91).sendToTarget();
            e.this.f122if.postDelayed(this, (long) j.S);
            boolean unused2 = e.this.f118do = true;
        }
    }

    public e(Context context, Handler handler) {
        this.f121goto = context;
        this.f122if = handler;
    }

    /* access modifiers changed from: private */
    /* renamed from: goto  reason: not valid java name */
    public void m128goto() {
        NetworkInfo.State state;
        NetworkInfo.State state2 = NetworkInfo.State.UNKNOWN;
        try {
            state = ((ConnectivityManager) this.f121goto.getSystemService("connectivity")).getNetworkInfo(1).getState();
        } catch (Exception e2) {
            state = state2;
        }
        if (NetworkInfo.State.CONNECTED != state) {
            this.d = false;
        } else if (!this.d) {
            this.d = true;
            this.f122if.postDelayed(new d(), (long) j.S);
            this.f118do = true;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: if  reason: not valid java name */
    public void m130if() {
        if (this.f126void != null) {
            try {
                c cVar = new c(this.f126void.getScanResults(), this.b);
                this.b = 0;
                if (this.f116case == null || !cVar.m148if(this.f116case)) {
                    this.f116case = cVar;
                }
            } catch (Exception e2) {
            }
        }
    }

    public boolean a() {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.f124long <= 10000) {
            return false;
        }
        this.f124long = currentTimeMillis;
        return m137new();
    }

    /* renamed from: byte  reason: not valid java name */
    public c m131byte() {
        if ((this.f116case != null && this.f116case.m150new()) || this.f126void == null) {
            return this.f116case;
        }
        try {
            return new c(this.f126void.getScanResults(), 0);
        } catch (Exception e2) {
            return new c(null, 0);
        }
    }

    /* renamed from: case  reason: not valid java name */
    public void m132case() {
        if (this.c == null) {
            this.c = new a();
        }
    }

    /* renamed from: char  reason: not valid java name */
    public String m133char() {
        WifiInfo connectionInfo = this.f126void.getConnectionInfo();
        if (connectionInfo == null) {
            return null;
        }
        try {
            String bssid = connectionInfo.getBSSID();
            if (bssid != null) {
                return bssid.replace(":", StringUtils.EMPTY);
            }
            return null;
        } catch (Exception e2) {
            return null;
        }
    }

    /* renamed from: else  reason: not valid java name */
    public void m134else() {
        if (this.f123int) {
            try {
                this.f121goto.unregisterReceiver(this.f117char);
            } catch (Exception e2) {
            }
            this.f117char = null;
            this.f126void = null;
            this.c = null;
            this.f123int = false;
            j.m250if(f115try, "wifimanager stop ...");
        }
    }

    /* renamed from: for  reason: not valid java name */
    public void m135for() {
        if (this.f118do) {
        }
    }

    /* renamed from: int  reason: not valid java name */
    public c m136int() {
        if ((this.f116case != null && this.f116case.m145for()) || this.f126void == null) {
            return this.f116case;
        }
        try {
            return new c(this.f126void.getScanResults(), 0);
        } catch (Exception e2) {
            return new c(null, 0);
        }
    }

    /* renamed from: new  reason: not valid java name */
    public boolean m137new() {
        if (this.f126void == null) {
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.b <= 3000) {
            return false;
        }
        try {
            if (this.f126void.isWifiEnabled()) {
                if (this.e == null || this.g == null) {
                    this.f126void.startScan();
                } else {
                    try {
                        this.e.invoke(this.g, Boolean.valueOf(this.f120for));
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        this.f126void.startScan();
                    }
                }
                this.b = currentTimeMillis;
                j.m250if(f115try, "wifimanager start scan ...");
                return true;
            }
            this.b = 0;
            return false;
        } catch (Exception e3) {
            return false;
        }
    }

    /* renamed from: try  reason: not valid java name */
    public void m138try() {
        if (!this.f123int) {
            this.f126void = (WifiManager) this.f121goto.getSystemService("wifi");
            this.f117char = new b();
            try {
                this.f121goto.registerReceiver(this.f117char, new IntentFilter("android.net.wifi.SCAN_RESULTS"));
                this.c = new a();
            } catch (Exception e2) {
            }
            this.f123int = true;
            j.m250if(f115try, "wifimanager start ...");
            try {
                Field declaredField = Class.forName("android.net.wifi.WifiManager").getDeclaredField("mService");
                if (declaredField == null) {
                    j.m250if(f115try, "android.net.wifi.WifiManager.mService  NOT  found ...");
                    return;
                }
                declaredField.setAccessible(true);
                this.g = declaredField.get(this.f126void);
                Class<?> cls = this.g.getClass();
                j.m250if(f115try, "mserviceClass : " + cls.getName());
                this.e = cls.getDeclaredMethod("startScan", Boolean.TYPE);
                if (this.e == null) {
                    j.m250if(f115try, "mService.startScan NOT  found ...");
                } else {
                    this.e.setAccessible(true);
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }
}
