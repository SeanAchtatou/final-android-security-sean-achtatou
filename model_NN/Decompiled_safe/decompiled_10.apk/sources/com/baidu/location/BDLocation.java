package com.baidu.location;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.json.JSONObject;

public final class BDLocation {
    public static final int TypeCacheLocation = 65;
    public static final int TypeCriteriaException = 62;
    public static final int TypeGpsLocation = 61;
    public static final int TypeNetWorkException = 63;
    public static final int TypeNetWorkLocation = 161;
    public static final int TypeNone = 0;
    public static final int TypeOffLineLocation = 66;
    public static final int TypeOffLineLocationFail = 67;
    public static final int TypeOffLineLocationNetworkFail = 68;
    public static final int TypeServerError = 167;
    private String a = null;
    private boolean b = false;

    /* renamed from: byte  reason: not valid java name */
    private String f0byte = null;
    private boolean c = false;

    /* renamed from: case  reason: not valid java name */
    private boolean f1case = false;

    /* renamed from: char  reason: not valid java name */
    private float f2char = -1.0f;
    private String d = null;

    /* renamed from: do  reason: not valid java name */
    private int f3do = -1;
    private boolean e = false;

    /* renamed from: else  reason: not valid java name */
    private double f4else = Double.MIN_VALUE;
    private double f = Double.MIN_VALUE;

    /* renamed from: for  reason: not valid java name */
    private double f5for = Double.MIN_VALUE;

    /* renamed from: goto  reason: not valid java name */
    private String f6goto = null;

    /* renamed from: if  reason: not valid java name */
    private int f7if = 0;

    /* renamed from: int  reason: not valid java name */
    private boolean f8int = false;

    /* renamed from: long  reason: not valid java name */
    private float f9long = SystemUtils.JAVA_VERSION_FLOAT;
    public a mAddr = new a();
    public String mServerString = null;

    /* renamed from: new  reason: not valid java name */
    private float f10new = SystemUtils.JAVA_VERSION_FLOAT;

    /* renamed from: try  reason: not valid java name */
    private boolean f11try = false;

    /* renamed from: void  reason: not valid java name */
    private boolean f12void = false;

    public class a {

        /* renamed from: byte  reason: not valid java name */
        public String f13byte = null;

        /* renamed from: do  reason: not valid java name */
        public String f14do = null;

        /* renamed from: for  reason: not valid java name */
        public String f15for = null;

        /* renamed from: if  reason: not valid java name */
        public String f16if = null;

        /* renamed from: int  reason: not valid java name */
        public String f17int = null;

        /* renamed from: new  reason: not valid java name */
        public String f18new = null;

        /* renamed from: try  reason: not valid java name */
        public String f19try = null;

        public a() {
        }
    }

    public BDLocation() {
    }

    public BDLocation(double d2, double d3, float f2) {
        this.f4else = d3;
        this.f5for = d2;
        this.f9long = f2;
        this.d = j.m245for();
    }

    public BDLocation(String str) {
        String str2;
        if (str != null && !str.equals(StringUtils.EMPTY)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                JSONObject jSONObject2 = jSONObject.getJSONObject("result");
                int parseInt = Integer.parseInt(jSONObject2.getString("error"));
                setLocType(parseInt);
                setTime(jSONObject2.getString("time"));
                if (parseInt == 61) {
                    JSONObject jSONObject3 = jSONObject.getJSONObject("content");
                    JSONObject jSONObject4 = jSONObject3.getJSONObject("point");
                    setLatitude(Double.parseDouble(jSONObject4.getString("y")));
                    setLongitude(Double.parseDouble(jSONObject4.getString("x")));
                    setRadius(Float.parseFloat(jSONObject3.getString("radius")));
                    setSpeed(Float.parseFloat(jSONObject3.getString("s")));
                    setDerect(Float.parseFloat(jSONObject3.getString("d")));
                    setSatelliteNumber(Integer.parseInt(jSONObject3.getString("n")));
                } else if (parseInt == 161) {
                    JSONObject jSONObject5 = jSONObject.getJSONObject("content");
                    JSONObject jSONObject6 = jSONObject5.getJSONObject("point");
                    setLatitude(Double.parseDouble(jSONObject6.getString("y")));
                    setLongitude(Double.parseDouble(jSONObject6.getString("x")));
                    setRadius(Float.parseFloat(jSONObject5.getString("radius")));
                    if (jSONObject5.has("addr")) {
                        String string = jSONObject5.getString("addr");
                        this.mAddr.f19try = string;
                        j.m250if(f.v, string);
                        String[] split = string.split(",");
                        this.mAddr.f16if = split[0];
                        this.mAddr.f18new = split[1];
                        this.mAddr.f17int = split[2];
                        this.mAddr.f13byte = split[3];
                        this.mAddr.f14do = split[4];
                        this.mAddr.f15for = split[5];
                        if ((!this.mAddr.f16if.contains("北京") || !this.mAddr.f18new.contains("北京")) && ((!this.mAddr.f16if.contains("上海") || !this.mAddr.f18new.contains("上海")) && ((!this.mAddr.f16if.contains("天津") || !this.mAddr.f18new.contains("天津")) && (!this.mAddr.f16if.contains("重庆") || !this.mAddr.f18new.contains("重庆"))))) {
                            str2 = this.mAddr.f16if + this.mAddr.f18new;
                        } else {
                            j.m250if(f.v, "true,beijing");
                            str2 = this.mAddr.f16if;
                        }
                        this.mAddr.f19try = str2 + this.mAddr.f17int + this.mAddr.f13byte + this.mAddr.f14do;
                        this.f12void = true;
                    } else {
                        this.f12void = false;
                        setAddrStr(null);
                    }
                    if (jSONObject5.has("poi")) {
                        this.f1case = true;
                        this.f0byte = jSONObject5.getJSONObject("poi").toString();
                    }
                } else if (parseInt == 66 || parseInt == 68) {
                    JSONObject jSONObject7 = jSONObject.getJSONObject("content");
                    JSONObject jSONObject8 = jSONObject7.getJSONObject("point");
                    setLatitude(Double.parseDouble(jSONObject8.getString("y")));
                    setLongitude(Double.parseDouble(jSONObject8.getString("x")));
                    setRadius(Float.parseFloat(jSONObject7.getString("radius")));
                    a(Boolean.valueOf(Boolean.parseBoolean(jSONObject7.getString("isCellChanged"))));
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                this.f7if = 0;
                this.f12void = false;
            }
        }
    }

    public BDLocation(String str, double d2, double d3, float f2, String str2, String str3) {
        this.d = str;
        this.f4else = d2;
        this.f5for = d3;
        this.f9long = f2;
        this.f6goto = str2;
        this.a = str3;
        this.d = j.m245for();
    }

    private void a(Boolean bool) {
        this.e = bool.booleanValue();
    }

    public String getAddrStr() {
        return this.mAddr.f19try;
    }

    public double getAltitude() {
        return this.f;
    }

    public String getCity() {
        return this.mAddr.f18new;
    }

    public String getCityCode() {
        return this.mAddr.f15for;
    }

    public String getCoorType() {
        return this.f6goto;
    }

    public float getDerect() {
        return this.f2char;
    }

    public String getDistrict() {
        return this.mAddr.f17int;
    }

    public double getLatitude() {
        return this.f4else;
    }

    public int getLocType() {
        return this.f7if;
    }

    public double getLongitude() {
        return this.f5for;
    }

    public String getPoi() {
        return this.f0byte;
    }

    public String getProvince() {
        return this.mAddr.f16if;
    }

    public float getRadius() {
        return this.f9long;
    }

    public int getSatelliteNumber() {
        this.b = true;
        return this.f3do;
    }

    public float getSpeed() {
        return this.f10new;
    }

    public String getStreet() {
        return this.mAddr.f13byte;
    }

    public String getStreetNumber() {
        return this.mAddr.f14do;
    }

    public String getTime() {
        return this.d;
    }

    public boolean hasAddr() {
        return this.f12void;
    }

    public boolean hasAltitude() {
        return this.c;
    }

    public boolean hasPoi() {
        return this.f1case;
    }

    public boolean hasRadius() {
        return this.f11try;
    }

    public boolean hasSateNumber() {
        return this.b;
    }

    public boolean hasSpeed() {
        return this.f8int;
    }

    public boolean isCellChangeFlag() {
        return this.e;
    }

    public void setAddrStr(String str) {
        this.a = str;
        this.f12void = true;
    }

    public void setAltitude(double d2) {
        this.f = d2;
        this.c = true;
    }

    public void setCoorType(String str) {
        this.f6goto = str;
    }

    public void setDerect(float f2) {
        this.f2char = f2;
    }

    public void setLatitude(double d2) {
        this.f4else = d2;
    }

    public void setLocType(int i) {
        this.f7if = i;
    }

    public void setLongitude(double d2) {
        this.f5for = d2;
    }

    public void setRadius(float f2) {
        this.f9long = f2;
        this.f11try = true;
    }

    public void setSatelliteNumber(int i) {
        this.f3do = i;
    }

    public void setSpeed(float f2) {
        this.f10new = f2;
        this.f8int = true;
    }

    public void setTime(String str) {
        this.d = str;
    }

    public String toJsonString() {
        return null;
    }

    public BDLocation toNewLocation(String str) {
        return null;
    }
}
