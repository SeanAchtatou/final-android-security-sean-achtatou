package com.baidu.location;

import android.content.Context;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

class g {
    /* access modifiers changed from: private */
    public static int a = 12000;
    /* access modifiers changed from: private */
    public static boolean b = false;

    /* renamed from: byte  reason: not valid java name */
    private static final int f158byte = 1;
    /* access modifiers changed from: private */
    public static Handler c = null;

    /* renamed from: case  reason: not valid java name */
    private static final int f159case = 2;
    /* access modifiers changed from: private */

    /* renamed from: char  reason: not valid java name */
    public static boolean f160char = false;
    private static int d = AccessibilityEventCompat.TYPE_WINDOW_CONTENT_CHANGED;
    /* access modifiers changed from: private */

    /* renamed from: do  reason: not valid java name */
    public static String f161do = null;
    /* access modifiers changed from: private */
    public static String e = null;
    /* access modifiers changed from: private */

    /* renamed from: else  reason: not valid java name */
    public static boolean f162else = false;
    /* access modifiers changed from: private */
    public static ArrayList f = null;
    /* access modifiers changed from: private */

    /* renamed from: for  reason: not valid java name */
    public static String f163for = null;
    /* access modifiers changed from: private */
    public static int g = 80;
    /* access modifiers changed from: private */

    /* renamed from: goto  reason: not valid java name */
    public static int f164goto = 0;
    /* access modifiers changed from: private */
    public static boolean h = false;
    private static final int i = 4;

    /* renamed from: if  reason: not valid java name */
    private static Uri f165if = null;
    /* access modifiers changed from: private */

    /* renamed from: int  reason: not valid java name */
    public static Handler f166int = null;
    /* access modifiers changed from: private */
    public static boolean j = false;
    /* access modifiers changed from: private */
    public static String k = "10.0.0.172";
    /* access modifiers changed from: private */
    public static String l = null;
    /* access modifiers changed from: private */

    /* renamed from: long  reason: not valid java name */
    public static String f167long = null;
    /* access modifiers changed from: private */
    public static Handler m = null;
    public static final int n = 3;
    /* access modifiers changed from: private */

    /* renamed from: new  reason: not valid java name */
    public static String f168new = f.v;
    /* access modifiers changed from: private */

    /* renamed from: try  reason: not valid java name */
    public static int f169try = 4;
    /* access modifiers changed from: private */

    /* renamed from: void  reason: not valid java name */
    public static int f170void = 3;

    g() {
    }

    private static int a(Context context, NetworkInfo networkInfo) {
        String lowerCase;
        if (!(networkInfo == null || networkInfo.getExtraInfo() == null || (lowerCase = networkInfo.getExtraInfo().toLowerCase()) == null)) {
            if (lowerCase.startsWith("cmwap") || lowerCase.startsWith("uniwap") || lowerCase.startsWith("3gwap")) {
                String defaultHost = Proxy.getDefaultHost();
                if (defaultHost == null || defaultHost.equals(StringUtils.EMPTY) || defaultHost.equals("null")) {
                    defaultHost = "10.0.0.172";
                }
                k = defaultHost;
                return 1;
            } else if (lowerCase.startsWith("ctwap")) {
                String defaultHost2 = Proxy.getDefaultHost();
                if (defaultHost2 == null || defaultHost2.equals(StringUtils.EMPTY) || defaultHost2.equals("null")) {
                    defaultHost2 = "10.0.0.200";
                }
                k = defaultHost2;
                return 1;
            } else if (lowerCase.startsWith("cmnet") || lowerCase.startsWith("uninet") || lowerCase.startsWith("ctnet") || lowerCase.startsWith("3gnet")) {
                return 2;
            }
        }
        String defaultHost3 = Proxy.getDefaultHost();
        if (defaultHost3 == null || defaultHost3.length() <= 0) {
            return 2;
        }
        if ("10.0.0.172".equals(defaultHost3.trim())) {
            k = "10.0.0.172";
            return 1;
        } else if (!"10.0.0.200".equals(defaultHost3.trim())) {
            return 2;
        } else {
            k = "10.0.0.200";
            return 1;
        }
    }

    public static void a(String str, boolean z) {
        if (!f160char && str != null) {
            f167long = Jni.m0if(str);
            h = z;
            f160char = true;
            new Thread() {
                public void run() {
                    int i;
                    boolean z = true;
                    int i2 = 3;
                    while (true) {
                        if (i2 > 0) {
                            try {
                                HttpPost httpPost = new HttpPost(j.m243do());
                                ArrayList arrayList = new ArrayList();
                                if (g.h) {
                                    arrayList.add(new BasicNameValuePair("qt", "grid"));
                                } else {
                                    arrayList.add(new BasicNameValuePair("qt", "conf"));
                                }
                                arrayList.add(new BasicNameValuePair("req", g.f167long));
                                httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "utf-8"));
                                DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                                defaultHttpClient.getParams().setParameter("http.connection.timeout", Integer.valueOf(g.a));
                                defaultHttpClient.getParams().setParameter("http.socket.timeout", Integer.valueOf(g.a));
                                j.m250if(g.f168new, "req config...");
                                HttpResponse execute = defaultHttpClient.execute(httpPost);
                                if (execute.getStatusLine().getStatusCode() != 200) {
                                    httpPost.abort();
                                    i2--;
                                } else if (g.h) {
                                    j.m250if(g.f168new, "req config response...");
                                    try {
                                        byte[] byteArray = EntityUtils.toByteArray(execute.getEntity());
                                        if (byteArray == null) {
                                            z = false;
                                        } else if (byteArray.length < 640) {
                                            j.m250if(g.f168new, "req config response.<640.");
                                            j.ag = false;
                                            j.o = j.Z + 0.025d;
                                            j.f203if = j.J - 0.025d;
                                        } else {
                                            j.ag = true;
                                            Long valueOf = Long.valueOf(((((long) byteArray[7]) & 255) << 56) | ((((long) byteArray[6]) & 255) << 48) | ((((long) byteArray[5]) & 255) << 40) | ((((long) byteArray[4]) & 255) << 32) | ((((long) byteArray[3]) & 255) << 24) | ((((long) byteArray[2]) & 255) << 16) | ((((long) byteArray[1]) & 255) << 8) | (((long) byteArray[0]) & 255));
                                            j.m250if(g.f168new, "req config 1...");
                                            j.f203if = Double.longBitsToDouble(valueOf.longValue());
                                            j.m250if(g.f168new, "req config response:" + Double.longBitsToDouble(valueOf.longValue()));
                                            Long valueOf2 = Long.valueOf(((((long) byteArray[15]) & 255) << 56) | ((((long) byteArray[14]) & 255) << 48) | ((((long) byteArray[13]) & 255) << 40) | ((((long) byteArray[12]) & 255) << 32) | ((((long) byteArray[11]) & 255) << 24) | ((((long) byteArray[10]) & 255) << 16) | ((((long) byteArray[9]) & 255) << 8) | (((long) byteArray[8]) & 255));
                                            j.o = Double.longBitsToDouble(valueOf2.longValue());
                                            j.j = new byte[625];
                                            j.m250if(g.f168new, "req config response:" + Double.longBitsToDouble(valueOf2.longValue()));
                                            for (int i3 = 0; i3 < 625; i3++) {
                                                j.j[i3] = byteArray[i3 + 16];
                                                j.m250if(g.f168new, "req config value:" + ((int) j.j[i3]));
                                            }
                                        }
                                        if (z) {
                                            g.m210for();
                                        }
                                    } catch (Exception e) {
                                    }
                                } else {
                                    String entityUtils = EntityUtils.toString(execute.getEntity(), "utf-8");
                                    j.m250if(g.f168new, "req config value:" + entityUtils);
                                    try {
                                        if (g.m218if(entityUtils)) {
                                            j.m250if(g.f168new, "Save to config");
                                            g.c();
                                        }
                                    } catch (Exception e2) {
                                    }
                                    try {
                                        JSONObject jSONObject = new JSONObject(entityUtils);
                                        if (jSONObject.has("ctr")) {
                                            j.t = Integer.parseInt(jSONObject.getString("ctr"));
                                        }
                                    } catch (Exception e3) {
                                    }
                                }
                            } catch (Exception e4) {
                                j.m250if(g.f168new, "Exception!!!");
                            }
                        }
                    }
                    try {
                        j.m247if();
                        if (j.t != -1) {
                            i = j.t;
                            j.m248if(j.t);
                        } else {
                            i = j.f206new != -1 ? j.f206new : -1;
                        }
                        if (i != -1) {
                            j.a(i);
                        }
                        g.c.obtainMessage(92).sendToTarget();
                        Handler unused = g.c = (Handler) null;
                    } catch (Exception e5) {
                        Handler unused2 = g.c = (Handler) null;
                    }
                    String unused3 = g.f167long = (String) null;
                    boolean unused4 = g.f160char = false;
                }
            }.start();
        }
    }

    public static boolean a(Context context) {
        if (context == null) {
            return false;
        }
        m204do(context);
        return f169try == 3;
    }

    public static boolean a(String str, Handler handler) {
        if (j || str == null) {
            return false;
        }
        j = true;
        j.m250if(f168new, "bloc : " + l);
        l = Jni.m0if(str);
        j.m250if(f168new, "NUMBER_e : " + l.length());
        j.m250if(f168new, "content: " + l);
        f166int = handler;
        if (f161do == null) {
            f161do = k.m257do();
        }
        new Thread() {
            public void run() {
                int i = g.f170void;
                while (true) {
                    if (i <= 0) {
                        break;
                    }
                    try {
                        HttpPost httpPost = new HttpPost(j.m243do());
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(new BasicNameValuePair("bloc", g.l));
                        if (g.f161do != null) {
                            arrayList.add(new BasicNameValuePair("up", g.f161do));
                        }
                        httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "utf-8"));
                        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                        defaultHttpClient.getParams().setParameter("http.connection.timeout", Integer.valueOf(g.a));
                        defaultHttpClient.getParams().setParameter("http.socket.timeout", Integer.valueOf(g.a));
                        HttpProtocolParams.setUseExpectContinue(defaultHttpClient.getParams(), false);
                        j.m250if(g.f168new, "apn type : " + g.f169try);
                        if ((g.f169try == 1 || g.f169try == 4) && (g.f170void - i) % 2 == 0) {
                            j.m250if(g.f168new, "apn type : ADD PROXY" + g.k + g.g);
                            defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(g.k, g.g, "http"));
                        }
                        HttpResponse execute = defaultHttpClient.execute(httpPost);
                        int statusCode = execute.getStatusLine().getStatusCode();
                        j.m250if(g.f168new, "===status error : " + statusCode);
                        if (statusCode == 200) {
                            String entityUtils = EntityUtils.toString(execute.getEntity(), "utf-8");
                            j.m250if(g.f168new, "status error : " + execute.getEntity().getContentType());
                            Message obtainMessage = g.f166int.obtainMessage(21);
                            obtainMessage.obj = entityUtils;
                            obtainMessage.sendToTarget();
                            String unused = g.f161do = (String) null;
                            break;
                        }
                        httpPost.abort();
                        Message obtainMessage2 = g.f166int.obtainMessage(63);
                        obtainMessage2.obj = "HttpStatus error";
                        obtainMessage2.sendToTarget();
                        i--;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (i <= 0 && g.f166int != null) {
                    j.m250if(g.f168new, "have tried 3 times...");
                    g.f166int.obtainMessage(62).sendToTarget();
                }
                Handler unused2 = g.f166int = (Handler) null;
                boolean unused3 = g.j = false;
            }
        }.start();
        return true;
    }

    public static void c() {
        String str = f.ac + "/config.dat";
        String format = String.format("{\"ver\":\"%d\",\"gps\":\"%.1f|%.1f|%.1f|%.1f|%d|%d|%d|%d|%d|%d|%d\",\"up\":\"%.1f|%.1f|%.1f|%.1f\",\"wf\":\"%d|%.1f|%d|%.1f\",\"ab\":\"%.2f|%.2f|%d|%d\",\"gpc\":\"%d|%d|%d|%d|%d|%d\",\"zxd\":\"%.1f|%.1f|%d|%d|%d\"}", Integer.valueOf(j.g), Float.valueOf(j.am), Float.valueOf(j.c), Float.valueOf(j.F), Float.valueOf(j.U), Integer.valueOf(j.p), Integer.valueOf(j.K), Integer.valueOf(j.X), Integer.valueOf(j.f204int), Integer.valueOf(j.f201for), Integer.valueOf(j.ad), Integer.valueOf(j.f205long), Float.valueOf(j.D), Float.valueOf(j.C), Float.valueOf(j.ai), Float.valueOf(j.Q), Integer.valueOf(j.Y), Float.valueOf(j.f196byte), Integer.valueOf(j.S), Float.valueOf(j.a), Float.valueOf(j.u), Float.valueOf(j.s), Integer.valueOf(j.r), Integer.valueOf(j.q), Integer.valueOf(j.f208void ? 1 : 0), Integer.valueOf(j.f207try ? 1 : 0), Integer.valueOf(j.V), Integer.valueOf(j.L), Long.valueOf(j.ac), Integer.valueOf(j.af), Float.valueOf(j.w), Float.valueOf(j.W), Integer.valueOf(j.v), Integer.valueOf(j.ae), Integer.valueOf(j.f202goto));
        j.m250if(f168new, "save2Config : " + format);
        byte[] bytes = format.getBytes();
        try {
            File file = new File(str);
            if (!file.exists()) {
                File file2 = new File(f.ac);
                if (!file2.exists()) {
                    file2.mkdirs();
                }
                if (file.createNewFile()) {
                    j.m250if(f168new, "upload manager create file success");
                    RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                    randomAccessFile.seek(0);
                    randomAccessFile.writeBoolean(false);
                    randomAccessFile.writeBoolean(false);
                    randomAccessFile.close();
                } else {
                    return;
                }
            }
            RandomAccessFile randomAccessFile2 = new RandomAccessFile(file, "rw");
            randomAccessFile2.seek(0);
            randomAccessFile2.writeBoolean(true);
            randomAccessFile2.seek(2);
            randomAccessFile2.writeInt(bytes.length);
            randomAccessFile2.write(bytes);
            randomAccessFile2.close();
        } catch (Exception e2) {
        }
    }

    /* renamed from: do  reason: not valid java name */
    public static int m204do(Context context) {
        f169try = m215if(context);
        return f169try;
    }

    public static void f() {
        if (!b) {
            b = true;
            if (f == null) {
                f164goto = 0;
                f = new ArrayList();
                int i2 = 0;
                do {
                    String str = f164goto < 2 ? k.m257do() : null;
                    if (str != null || f164goto == 1) {
                        f164goto = 1;
                    } else {
                        f164goto = 2;
                        try {
                            if (j.af == 0) {
                                str = f.m183new();
                                if (str == null) {
                                    str = b.j();
                                }
                            } else if (j.af == 1 && (str = b.j()) == null) {
                                str = f.m183new();
                            }
                        } catch (Exception e2) {
                            str = null;
                        }
                    }
                    if (str == null) {
                        break;
                    }
                    f.add(str);
                    i2 += str.length();
                    j.m250if(f168new, "upload data size:" + i2);
                } while (i2 < d);
            }
            if (f == null || f.size() < 1) {
                f = null;
                b = false;
                j.m250if(f168new, "No upload data...");
                return;
            }
            j.m250if(f168new, "Beging upload data...");
            new Thread() {
                public void run() {
                    try {
                        HttpPost httpPost = new HttpPost(j.m243do());
                        ArrayList arrayList = new ArrayList();
                        for (int i = 0; i < g.f.size(); i++) {
                            if (g.f164goto == 1) {
                                arrayList.add(new BasicNameValuePair("cldc[" + i + "]", (String) g.f.get(i)));
                            } else {
                                arrayList.add(new BasicNameValuePair("cltr[" + i + "]", (String) g.f.get(i)));
                            }
                        }
                        httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "utf-8"));
                        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                        defaultHttpClient.getParams().setParameter("http.connection.timeout", Integer.valueOf(g.a));
                        defaultHttpClient.getParams().setParameter("http.socket.timeout", Integer.valueOf(g.a));
                        if (defaultHttpClient.execute(httpPost).getStatusLine().getStatusCode() == 200) {
                            j.m250if(g.f168new, "Status ok1...");
                            g.f.clear();
                            ArrayList unused = g.f = (ArrayList) null;
                        } else {
                            j.m250if(g.f168new, "Status err1...");
                        }
                    } catch (Exception e) {
                    } finally {
                        boolean unused2 = g.b = false;
                    }
                }
            }.start();
        }
    }

    /* renamed from: for  reason: not valid java name */
    public static void m210for() {
        try {
            File file = new File(f.ac + "/config.dat");
            if (!file.exists()) {
                File file2 = new File(f.ac);
                if (!file2.exists()) {
                    file2.mkdirs();
                }
                if (file.createNewFile()) {
                    j.m250if(f168new, "upload manager create file success");
                    RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                    randomAccessFile.seek(0);
                    randomAccessFile.writeBoolean(false);
                    randomAccessFile.writeBoolean(false);
                    randomAccessFile.close();
                } else {
                    return;
                }
            }
            RandomAccessFile randomAccessFile2 = new RandomAccessFile(file, "rw");
            randomAccessFile2.seek(1);
            randomAccessFile2.writeBoolean(true);
            randomAccessFile2.seek(1024);
            randomAccessFile2.writeDouble(j.f203if);
            randomAccessFile2.writeDouble(j.o);
            randomAccessFile2.writeBoolean(j.ag);
            if (j.ag && j.j != null) {
                randomAccessFile2.write(j.j);
            }
            randomAccessFile2.close();
        } catch (Exception e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.g.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.baidu.location.g.a(android.content.Context, android.net.NetworkInfo):int
      com.baidu.location.g.a(java.lang.String, android.os.Handler):boolean
      com.baidu.location.g.a(java.lang.String, boolean):void */
    /* renamed from: for  reason: not valid java name */
    public static void m211for(Handler handler) {
        try {
            File file = new File(f.ac + "/config.dat");
            if (file.exists()) {
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                if (randomAccessFile.readBoolean()) {
                    randomAccessFile.seek(2);
                    int readInt = randomAccessFile.readInt();
                    byte[] bArr = new byte[readInt];
                    randomAccessFile.read(bArr, 0, readInt);
                    m218if(new String(bArr));
                }
                randomAccessFile.seek(1);
                if (randomAccessFile.readBoolean()) {
                    randomAccessFile.seek(1024);
                    j.f203if = randomAccessFile.readDouble();
                    j.o = randomAccessFile.readDouble();
                    j.ag = randomAccessFile.readBoolean();
                    if (j.ag) {
                        j.j = new byte[625];
                        randomAccessFile.read(j.j, 0, 625);
                    }
                }
                randomAccessFile.close();
            }
            String str = "&ver=" + j.g + "&usr=" + j.f199do + "&app=" + j.ak + "&prod=" + j.b;
            j.m250if(f168new, str);
            c = handler;
            a(str, false);
        } catch (Exception e2) {
        }
    }

    /* renamed from: for  reason: not valid java name */
    public static boolean m212for(Context context) {
        boolean z = true;
        if (context == null) {
            return false;
        }
        m204do(context);
        if (f169try != 1) {
            z = false;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00d1, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00d2, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00d9, code lost:
        r0 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
        return 4;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00d1 A[ExcHandler: Exception (r0v1 'e' java.lang.Exception A[CUSTOM_DECLARE]), Splitter:B:1:0x0005] */
    /* renamed from: if  reason: not valid java name */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int m215if(android.content.Context r9) {
        /*
            r7 = 1
            r6 = 4
            r1 = 0
            java.lang.String r0 = "connectivity"
            java.lang.Object r0 = r9.getSystemService(r0)     // Catch:{ SecurityException -> 0x00be, Exception -> 0x00d1 }
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0     // Catch:{ SecurityException -> 0x00be, Exception -> 0x00d1 }
            if (r0 != 0) goto L_0x000f
            r0 = r6
        L_0x000e:
            return r0
        L_0x000f:
            android.net.NetworkInfo r8 = r0.getActiveNetworkInfo()     // Catch:{ SecurityException -> 0x00be, Exception -> 0x00d1 }
            if (r8 == 0) goto L_0x001b
            boolean r0 = r8.isAvailable()     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            if (r0 != 0) goto L_0x001d
        L_0x001b:
            r0 = r6
            goto L_0x000e
        L_0x001d:
            int r0 = r8.getType()     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            if (r0 != r7) goto L_0x0025
            r0 = 3
            goto L_0x000e
        L_0x0025:
            java.lang.String r0 = "content://telephony/carriers/preferapn"
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            com.baidu.location.g.f165if = r0     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            android.content.ContentResolver r0 = r9.getContentResolver()     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            android.net.Uri r1 = com.baidu.location.g.f165if     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            if (r1 == 0) goto L_0x00b6
            boolean r0 = r1.moveToFirst()     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            if (r0 == 0) goto L_0x00b6
            java.lang.String r0 = "apn"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            if (r0 == 0) goto L_0x0081
            java.lang.String r2 = r0.toLowerCase()     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            java.lang.String r3 = "ctwap"
            boolean r2 = r2.contains(r3)     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            if (r2 == 0) goto L_0x0081
            java.lang.String r0 = android.net.Proxy.getDefaultHost()     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            if (r0 == 0) goto L_0x007e
            java.lang.String r2 = ""
            boolean r2 = r0.equals(r2)     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            if (r2 != 0) goto L_0x007e
            java.lang.String r2 = "null"
            boolean r2 = r0.equals(r2)     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            if (r2 != 0) goto L_0x007e
        L_0x0071:
            com.baidu.location.g.k = r0     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            r0 = 80
            com.baidu.location.g.g = r0     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            if (r1 == 0) goto L_0x007c
            r1.close()     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
        L_0x007c:
            r0 = r7
            goto L_0x000e
        L_0x007e:
            java.lang.String r0 = "10.0.0.200"
            goto L_0x0071
        L_0x0081:
            if (r0 == 0) goto L_0x00b6
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            java.lang.String r2 = "wap"
            boolean r0 = r0.contains(r2)     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            if (r0 == 0) goto L_0x00b6
            java.lang.String r0 = android.net.Proxy.getDefaultHost()     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            if (r0 == 0) goto L_0x00b3
            java.lang.String r2 = ""
            boolean r2 = r0.equals(r2)     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            if (r2 != 0) goto L_0x00b3
            java.lang.String r2 = "null"
            boolean r2 = r0.equals(r2)     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            if (r2 != 0) goto L_0x00b3
        L_0x00a5:
            com.baidu.location.g.k = r0     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            r0 = 80
            com.baidu.location.g.g = r0     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
            if (r1 == 0) goto L_0x00b0
            r1.close()     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
        L_0x00b0:
            r0 = r7
            goto L_0x000e
        L_0x00b3:
            java.lang.String r0 = "10.0.0.172"
            goto L_0x00a5
        L_0x00b6:
            if (r1 == 0) goto L_0x00bb
            r1.close()     // Catch:{ SecurityException -> 0x00d8, Exception -> 0x00d1 }
        L_0x00bb:
            r0 = 2
            goto L_0x000e
        L_0x00be:
            r0 = move-exception
            r0 = r1
        L_0x00c0:
            java.lang.String r1 = com.baidu.location.g.f168new     // Catch:{ Exception -> 0x00cd }
            java.lang.String r2 = "APN security..."
            com.baidu.location.j.m250if(r1, r2)     // Catch:{ Exception -> 0x00cd }
            int r0 = a(r9, r0)     // Catch:{ Exception -> 0x00cd }
            goto L_0x000e
        L_0x00cd:
            r0 = move-exception
            r0 = r6
            goto L_0x000e
        L_0x00d1:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r6
            goto L_0x000e
        L_0x00d8:
            r0 = move-exception
            r0 = r8
            goto L_0x00c0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.g.m215if(android.content.Context):int");
    }

    /* renamed from: if  reason: not valid java name */
    public static boolean m218if(String str) {
        if (str != null) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                int parseInt = Integer.parseInt(jSONObject.getString("ver"));
                if (parseInt > j.g) {
                    j.g = parseInt;
                    if (jSONObject.has("gps")) {
                        j.m250if(f168new, "has gps...");
                        String[] split = jSONObject.getString("gps").split("\\|");
                        if (split.length > 10) {
                            if (split[0] != null && !split[0].equals(StringUtils.EMPTY)) {
                                j.am = Float.parseFloat(split[0]);
                            }
                            if (split[1] != null && !split[1].equals(StringUtils.EMPTY)) {
                                j.c = Float.parseFloat(split[1]);
                            }
                            if (split[2] != null && !split[2].equals(StringUtils.EMPTY)) {
                                j.F = Float.parseFloat(split[2]);
                            }
                            if (split[3] != null && !split[3].equals(StringUtils.EMPTY)) {
                                j.U = Float.parseFloat(split[3]);
                            }
                            if (split[4] != null && !split[4].equals(StringUtils.EMPTY)) {
                                j.p = Integer.parseInt(split[4]);
                            }
                            if (split[5] != null && !split[5].equals(StringUtils.EMPTY)) {
                                j.K = Integer.parseInt(split[5]);
                            }
                            if (split[6] != null && !split[6].equals(StringUtils.EMPTY)) {
                                j.X = Integer.parseInt(split[6]);
                            }
                            if (split[7] != null && !split[7].equals(StringUtils.EMPTY)) {
                                j.f204int = Integer.parseInt(split[7]);
                            }
                            if (split[8] != null && !split[8].equals(StringUtils.EMPTY)) {
                                j.f201for = Integer.parseInt(split[8]);
                            }
                            if (split[9] != null && !split[9].equals(StringUtils.EMPTY)) {
                                j.ad = Integer.parseInt(split[9]);
                            }
                            if (split[10] != null && !split[10].equals(StringUtils.EMPTY)) {
                                j.f205long = Integer.parseInt(split[10]);
                            }
                        }
                    }
                    if (jSONObject.has("up")) {
                        j.m250if(f168new, "has up...");
                        String[] split2 = jSONObject.getString("up").split("\\|");
                        if (split2.length > 3) {
                            if (split2[0] != null && !split2[0].equals(StringUtils.EMPTY)) {
                                j.D = Float.parseFloat(split2[0]);
                            }
                            if (split2[1] != null && !split2[1].equals(StringUtils.EMPTY)) {
                                j.C = Float.parseFloat(split2[1]);
                            }
                            if (split2[2] != null && !split2[2].equals(StringUtils.EMPTY)) {
                                j.ai = Float.parseFloat(split2[2]);
                            }
                            if (split2[3] != null && !split2[3].equals(StringUtils.EMPTY)) {
                                j.Q = Float.parseFloat(split2[3]);
                            }
                        }
                    }
                    if (jSONObject.has("wf")) {
                        j.m250if(f168new, "has wf...");
                        String[] split3 = jSONObject.getString("wf").split("\\|");
                        if (split3.length > 3) {
                            if (split3[0] != null && !split3[0].equals(StringUtils.EMPTY)) {
                                j.Y = Integer.parseInt(split3[0]);
                            }
                            if (split3[1] != null && !split3[1].equals(StringUtils.EMPTY)) {
                                j.f196byte = Float.parseFloat(split3[1]);
                            }
                            if (split3[2] != null && !split3[2].equals(StringUtils.EMPTY)) {
                                j.S = Integer.parseInt(split3[2]);
                            }
                            if (split3[3] != null && !split3[3].equals(StringUtils.EMPTY)) {
                                j.a = Float.parseFloat(split3[3]);
                            }
                        }
                    }
                    if (jSONObject.has("ab")) {
                        j.m250if(f168new, "has ab...");
                        String[] split4 = jSONObject.getString("ab").split("\\|");
                        if (split4.length > 3) {
                            if (split4[0] != null && !split4[0].equals(StringUtils.EMPTY)) {
                                j.u = Float.parseFloat(split4[0]);
                            }
                            if (split4[1] != null && !split4[1].equals(StringUtils.EMPTY)) {
                                j.s = Float.parseFloat(split4[1]);
                            }
                            if (split4[2] != null && !split4[2].equals(StringUtils.EMPTY)) {
                                j.r = Integer.parseInt(split4[2]);
                            }
                            if (split4[3] != null && !split4[3].equals(StringUtils.EMPTY)) {
                                j.q = Integer.parseInt(split4[3]);
                            }
                        }
                    }
                    if (jSONObject.has("zxd")) {
                        String[] split5 = jSONObject.getString("zxd").split("\\|");
                        if (split5.length > 4) {
                            if (split5[0] != null && !split5[0].equals(StringUtils.EMPTY)) {
                                j.w = Float.parseFloat(split5[0]);
                            }
                            if (split5[1] != null && !split5[1].equals(StringUtils.EMPTY)) {
                                j.W = Float.parseFloat(split5[1]);
                            }
                            if (split5[2] != null && !split5[2].equals(StringUtils.EMPTY)) {
                                j.v = Integer.parseInt(split5[2]);
                            }
                            if (split5[3] != null && !split5[3].equals(StringUtils.EMPTY)) {
                                j.ae = Integer.parseInt(split5[3]);
                            }
                            if (split5[4] != null && !split5[4].equals(StringUtils.EMPTY)) {
                                j.f202goto = Integer.parseInt(split5[4]);
                            }
                        }
                    }
                    if (jSONObject.has("gpc")) {
                        j.m250if(f168new, "has gpc...");
                        String[] split6 = jSONObject.getString("gpc").split("\\|");
                        if (split6.length > 5) {
                            if (split6[0] != null && !split6[0].equals(StringUtils.EMPTY)) {
                                if (Integer.parseInt(split6[0]) > 0) {
                                    j.f208void = true;
                                } else {
                                    j.f208void = false;
                                }
                            }
                            if (split6[1] != null && !split6[1].equals(StringUtils.EMPTY)) {
                                if (Integer.parseInt(split6[1]) > 0) {
                                    j.f207try = true;
                                } else {
                                    j.f207try = false;
                                }
                            }
                            if (split6[2] != null && !split6[2].equals(StringUtils.EMPTY)) {
                                j.V = Integer.parseInt(split6[2]);
                            }
                            if (split6[3] != null && !split6[3].equals(StringUtils.EMPTY)) {
                                j.L = Integer.parseInt(split6[3]);
                            }
                            if (split6[4] != null && !split6[4].equals(StringUtils.EMPTY)) {
                                int parseInt2 = Integer.parseInt(split6[4]);
                                if (parseInt2 > 0) {
                                    j.ac = (long) parseInt2;
                                    j.M = j.ac * 1000 * 60;
                                    j.al = j.M >> 2;
                                } else {
                                    j.G = false;
                                }
                            }
                            if (split6[5] != null && !split6[5].equals(StringUtils.EMPTY)) {
                                j.af = Integer.parseInt(split6[5]);
                            }
                        }
                    }
                    try {
                        j.m250if(f168new, "config change true...");
                        return true;
                    } catch (Exception e2) {
                        return true;
                    }
                }
            } catch (Exception e3) {
                return false;
            }
        }
        return false;
    }

    /* renamed from: if  reason: not valid java name */
    public static boolean m219if(String str, Handler handler) {
        if (f162else || str == null) {
            return false;
        }
        f162else = true;
        e = Jni.m0if(str);
        j.m250if(f168new, "bloc : " + e);
        m = handler;
        if (f163for == null) {
            f163for = k.m257do();
        }
        new Thread() {
            public void run() {
                int i = g.f170void;
                while (true) {
                    if (i <= 0) {
                        break;
                    }
                    try {
                        HttpPost httpPost = new HttpPost(j.m243do());
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(new BasicNameValuePair("bloc", g.e));
                        if (g.f163for != null) {
                            arrayList.add(new BasicNameValuePair("up", g.f163for));
                        }
                        httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "utf-8"));
                        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                        defaultHttpClient.getParams().setParameter("http.connection.timeout", Integer.valueOf(g.a));
                        defaultHttpClient.getParams().setParameter("http.socket.timeout", Integer.valueOf(g.a));
                        HttpProtocolParams.setUseExpectContinue(defaultHttpClient.getParams(), false);
                        if (g.f169try == 1) {
                            defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(g.k, g.g, "http"));
                        }
                        HttpResponse execute = defaultHttpClient.execute(httpPost);
                        int statusCode = execute.getStatusLine().getStatusCode();
                        j.m250if(g.f168new, "===status error : " + statusCode);
                        if (statusCode == 200) {
                            String entityUtils = EntityUtils.toString(execute.getEntity(), "utf-8");
                            Message obtainMessage = g.m.obtainMessage(26);
                            obtainMessage.obj = entityUtils;
                            obtainMessage.sendToTarget();
                            String unused = g.f161do = (String) null;
                            break;
                        }
                        httpPost.abort();
                        Message obtainMessage2 = g.m.obtainMessage(65);
                        obtainMessage2.obj = "HttpStatus error";
                        obtainMessage2.sendToTarget();
                        i--;
                    } catch (Exception e) {
                    }
                }
                if (i <= 0 && g.m != null) {
                    j.m250if(g.f168new, "have tried 3 times...");
                    g.m.obtainMessage(64).sendToTarget();
                }
                Handler unused2 = g.m = (Handler) null;
                boolean unused3 = g.f162else = false;
            }
        }.start();
        return true;
    }
}
