package com.baidu.location;

import com.house365.core.util.lbs.MyLocation;
import org.apache.commons.lang.SystemUtils;

public abstract class BDNotifyListener {
    public int Notified = 0;
    public float differDistance = SystemUtils.JAVA_VERSION_FLOAT;
    public boolean isAdded = false;
    public String mCoorType = null;
    public double mLatitude = Double.MIN_VALUE;
    public double mLatitudeC = Double.MIN_VALUE;
    public double mLongitude = Double.MIN_VALUE;
    public double mLongitudeC = Double.MIN_VALUE;
    public i mNotifyCache = null;
    public float mRadius = SystemUtils.JAVA_VERSION_FLOAT;

    public void SetNotifyLocation(double d, double d2, float f, String str) {
        this.mLatitude = d;
        this.mLongitude = d2;
        if (f < SystemUtils.JAVA_VERSION_FLOAT) {
            this.mRadius = 200.0f;
        } else {
            this.mRadius = f;
        }
        if (str.equals(MyLocation.LOACTION_TYPE_COMMON) || str.equals("bd09") || str.equals(MyLocation.LOACTION_TYPE_BAIDU) || str.equals("gps")) {
            this.mCoorType = str;
        } else {
            this.mCoorType = MyLocation.LOACTION_TYPE_COMMON;
        }
        if (this.mCoorType.equals(MyLocation.LOACTION_TYPE_COMMON)) {
            this.mLatitudeC = this.mLatitude;
            this.mLongitudeC = this.mLongitude;
        }
        if (this.isAdded) {
            this.Notified = 0;
            this.mNotifyCache.a(this);
        }
    }

    public void onNotify(BDLocation bDLocation, float f) {
        j.m250if(f.v, "new location, not far from the destination..." + f);
    }
}
