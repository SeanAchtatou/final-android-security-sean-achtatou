package org.jivesoftware.smack.filter;

import org.jivesoftware.smack.packet.Packet;

public class PacketTypeFilter implements PacketFilter {
    Class packetType;

    public PacketTypeFilter(Class packetType2) {
        if (!Packet.class.isAssignableFrom(packetType2)) {
            throw new IllegalArgumentException("Packet type must be a sub-class of Packet.");
        }
        this.packetType = packetType2;
    }

    public boolean accept(Packet packet) {
        return this.packetType.isInstance(packet);
    }

    public String toString() {
        return "PacketTypeFilter: " + this.packetType.getName();
    }
}
