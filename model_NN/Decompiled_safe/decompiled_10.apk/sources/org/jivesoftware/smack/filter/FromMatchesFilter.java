package org.jivesoftware.smack.filter;

import org.apache.commons.lang.StringUtils;
import org.jivesoftware.smack.packet.Packet;

public class FromMatchesFilter implements PacketFilter {
    private String address;
    private boolean matchBareJID = false;

    public FromMatchesFilter(String address2) {
        if (address2 == null) {
            throw new IllegalArgumentException("Parameter cannot be null.");
        }
        this.address = address2.toLowerCase();
        this.matchBareJID = StringUtils.EMPTY.equals(org.jivesoftware.smack.util.StringUtils.parseResource(address2));
    }

    public boolean accept(Packet packet) {
        if (packet.getFrom() == null) {
            return false;
        }
        if (this.matchBareJID) {
            return packet.getFrom().toLowerCase().startsWith(this.address);
        }
        return this.address.equals(packet.getFrom().toLowerCase());
    }

    public String toString() {
        return "FromMatchesFilter: " + this.address;
    }
}
