package org.jivesoftware.smack.util;

import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import org.apache.commons.lang.CharEncoding;

public class Base64 {
    public static final int DECODE = 0;
    public static final int DONT_BREAK_LINES = 8;
    public static final int ENCODE = 1;
    private static final byte EQUALS_SIGN = 61;
    private static final byte EQUALS_SIGN_ENC = -1;
    public static final int GZIP = 2;
    private static final int MAX_LINE_LENGTH = 76;
    private static final byte NEW_LINE = 10;
    public static final int NO_OPTIONS = 0;
    public static final int ORDERED = 32;
    private static final String PREFERRED_ENCODING = "UTF-8";
    public static final int URL_SAFE = 16;
    private static final byte WHITE_SPACE_ENC = -5;
    private static final byte[] _ORDERED_ALPHABET = {45, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 95, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122};
    private static final byte[] _ORDERED_DECODABET;
    private static final byte[] _STANDARD_ALPHABET = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] _STANDARD_DECODABET;
    private static final byte[] _URL_SAFE_ALPHABET = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private static final byte[] _URL_SAFE_DECODABET;

    static {
        byte[] bArr = new byte[127];
        bArr[0] = -9;
        bArr[1] = -9;
        bArr[2] = -9;
        bArr[3] = -9;
        bArr[4] = -9;
        bArr[5] = -9;
        bArr[6] = -9;
        bArr[7] = -9;
        bArr[8] = -9;
        bArr[9] = WHITE_SPACE_ENC;
        bArr[10] = WHITE_SPACE_ENC;
        bArr[11] = -9;
        bArr[12] = -9;
        bArr[13] = WHITE_SPACE_ENC;
        bArr[14] = -9;
        bArr[15] = -9;
        bArr[16] = -9;
        bArr[17] = -9;
        bArr[18] = -9;
        bArr[19] = -9;
        bArr[20] = -9;
        bArr[21] = -9;
        bArr[22] = -9;
        bArr[23] = -9;
        bArr[24] = -9;
        bArr[25] = -9;
        bArr[26] = -9;
        bArr[27] = -9;
        bArr[28] = -9;
        bArr[29] = -9;
        bArr[30] = -9;
        bArr[31] = -9;
        bArr[32] = WHITE_SPACE_ENC;
        bArr[33] = -9;
        bArr[34] = -9;
        bArr[35] = -9;
        bArr[36] = -9;
        bArr[37] = -9;
        bArr[38] = -9;
        bArr[39] = -9;
        bArr[40] = -9;
        bArr[41] = -9;
        bArr[42] = -9;
        bArr[43] = 62;
        bArr[44] = -9;
        bArr[45] = -9;
        bArr[46] = -9;
        bArr[47] = 63;
        bArr[48] = 52;
        bArr[49] = 53;
        bArr[50] = 54;
        bArr[51] = 55;
        bArr[52] = 56;
        bArr[53] = 57;
        bArr[54] = 58;
        bArr[55] = 59;
        bArr[56] = 60;
        bArr[57] = EQUALS_SIGN;
        bArr[58] = -9;
        bArr[59] = -9;
        bArr[60] = -9;
        bArr[61] = EQUALS_SIGN_ENC;
        bArr[62] = -9;
        bArr[63] = -9;
        bArr[64] = -9;
        bArr[66] = 1;
        bArr[67] = 2;
        bArr[68] = 3;
        bArr[69] = 4;
        bArr[70] = 5;
        bArr[71] = 6;
        bArr[72] = 7;
        bArr[73] = 8;
        bArr[74] = 9;
        bArr[75] = NEW_LINE;
        bArr[MAX_LINE_LENGTH] = 11;
        bArr[77] = 12;
        bArr[78] = 13;
        bArr[79] = 14;
        bArr[80] = 15;
        bArr[81] = 16;
        bArr[82] = 17;
        bArr[83] = 18;
        bArr[84] = 19;
        bArr[85] = 20;
        bArr[86] = 21;
        bArr[87] = 22;
        bArr[88] = 23;
        bArr[89] = 24;
        bArr[90] = 25;
        bArr[91] = -9;
        bArr[92] = -9;
        bArr[93] = -9;
        bArr[94] = -9;
        bArr[95] = -9;
        bArr[96] = -9;
        bArr[97] = 26;
        bArr[98] = 27;
        bArr[99] = 28;
        bArr[100] = 29;
        bArr[101] = 30;
        bArr[102] = 31;
        bArr[103] = 32;
        bArr[104] = 33;
        bArr[105] = 34;
        bArr[106] = 35;
        bArr[107] = 36;
        bArr[108] = 37;
        bArr[109] = 38;
        bArr[110] = 39;
        bArr[111] = 40;
        bArr[112] = 41;
        bArr[113] = 42;
        bArr[114] = 43;
        bArr[115] = 44;
        bArr[116] = 45;
        bArr[117] = 46;
        bArr[118] = 47;
        bArr[119] = 48;
        bArr[120] = 49;
        bArr[121] = 50;
        bArr[122] = 51;
        bArr[123] = -9;
        bArr[124] = -9;
        bArr[125] = -9;
        bArr[126] = -9;
        _STANDARD_DECODABET = bArr;
        byte[] bArr2 = new byte[127];
        bArr2[0] = -9;
        bArr2[1] = -9;
        bArr2[2] = -9;
        bArr2[3] = -9;
        bArr2[4] = -9;
        bArr2[5] = -9;
        bArr2[6] = -9;
        bArr2[7] = -9;
        bArr2[8] = -9;
        bArr2[9] = WHITE_SPACE_ENC;
        bArr2[10] = WHITE_SPACE_ENC;
        bArr2[11] = -9;
        bArr2[12] = -9;
        bArr2[13] = WHITE_SPACE_ENC;
        bArr2[14] = -9;
        bArr2[15] = -9;
        bArr2[16] = -9;
        bArr2[17] = -9;
        bArr2[18] = -9;
        bArr2[19] = -9;
        bArr2[20] = -9;
        bArr2[21] = -9;
        bArr2[22] = -9;
        bArr2[23] = -9;
        bArr2[24] = -9;
        bArr2[25] = -9;
        bArr2[26] = -9;
        bArr2[27] = -9;
        bArr2[28] = -9;
        bArr2[29] = -9;
        bArr2[30] = -9;
        bArr2[31] = -9;
        bArr2[32] = WHITE_SPACE_ENC;
        bArr2[33] = -9;
        bArr2[34] = -9;
        bArr2[35] = -9;
        bArr2[36] = -9;
        bArr2[37] = -9;
        bArr2[38] = -9;
        bArr2[39] = -9;
        bArr2[40] = -9;
        bArr2[41] = -9;
        bArr2[42] = -9;
        bArr2[43] = -9;
        bArr2[44] = -9;
        bArr2[45] = 62;
        bArr2[46] = -9;
        bArr2[47] = -9;
        bArr2[48] = 52;
        bArr2[49] = 53;
        bArr2[50] = 54;
        bArr2[51] = 55;
        bArr2[52] = 56;
        bArr2[53] = 57;
        bArr2[54] = 58;
        bArr2[55] = 59;
        bArr2[56] = 60;
        bArr2[57] = EQUALS_SIGN;
        bArr2[58] = -9;
        bArr2[59] = -9;
        bArr2[60] = -9;
        bArr2[61] = EQUALS_SIGN_ENC;
        bArr2[62] = -9;
        bArr2[63] = -9;
        bArr2[64] = -9;
        bArr2[66] = 1;
        bArr2[67] = 2;
        bArr2[68] = 3;
        bArr2[69] = 4;
        bArr2[70] = 5;
        bArr2[71] = 6;
        bArr2[72] = 7;
        bArr2[73] = 8;
        bArr2[74] = 9;
        bArr2[75] = NEW_LINE;
        bArr2[MAX_LINE_LENGTH] = 11;
        bArr2[77] = 12;
        bArr2[78] = 13;
        bArr2[79] = 14;
        bArr2[80] = 15;
        bArr2[81] = 16;
        bArr2[82] = 17;
        bArr2[83] = 18;
        bArr2[84] = 19;
        bArr2[85] = 20;
        bArr2[86] = 21;
        bArr2[87] = 22;
        bArr2[88] = 23;
        bArr2[89] = 24;
        bArr2[90] = 25;
        bArr2[91] = -9;
        bArr2[92] = -9;
        bArr2[93] = -9;
        bArr2[94] = -9;
        bArr2[95] = 63;
        bArr2[96] = -9;
        bArr2[97] = 26;
        bArr2[98] = 27;
        bArr2[99] = 28;
        bArr2[100] = 29;
        bArr2[101] = 30;
        bArr2[102] = 31;
        bArr2[103] = 32;
        bArr2[104] = 33;
        bArr2[105] = 34;
        bArr2[106] = 35;
        bArr2[107] = 36;
        bArr2[108] = 37;
        bArr2[109] = 38;
        bArr2[110] = 39;
        bArr2[111] = 40;
        bArr2[112] = 41;
        bArr2[113] = 42;
        bArr2[114] = 43;
        bArr2[115] = 44;
        bArr2[116] = 45;
        bArr2[117] = 46;
        bArr2[118] = 47;
        bArr2[119] = 48;
        bArr2[120] = 49;
        bArr2[121] = 50;
        bArr2[122] = 51;
        bArr2[123] = -9;
        bArr2[124] = -9;
        bArr2[125] = -9;
        bArr2[126] = -9;
        _URL_SAFE_DECODABET = bArr2;
        byte[] bArr3 = new byte[127];
        bArr3[0] = -9;
        bArr3[1] = -9;
        bArr3[2] = -9;
        bArr3[3] = -9;
        bArr3[4] = -9;
        bArr3[5] = -9;
        bArr3[6] = -9;
        bArr3[7] = -9;
        bArr3[8] = -9;
        bArr3[9] = WHITE_SPACE_ENC;
        bArr3[10] = WHITE_SPACE_ENC;
        bArr3[11] = -9;
        bArr3[12] = -9;
        bArr3[13] = WHITE_SPACE_ENC;
        bArr3[14] = -9;
        bArr3[15] = -9;
        bArr3[16] = -9;
        bArr3[17] = -9;
        bArr3[18] = -9;
        bArr3[19] = -9;
        bArr3[20] = -9;
        bArr3[21] = -9;
        bArr3[22] = -9;
        bArr3[23] = -9;
        bArr3[24] = -9;
        bArr3[25] = -9;
        bArr3[26] = -9;
        bArr3[27] = -9;
        bArr3[28] = -9;
        bArr3[29] = -9;
        bArr3[30] = -9;
        bArr3[31] = -9;
        bArr3[32] = WHITE_SPACE_ENC;
        bArr3[33] = -9;
        bArr3[34] = -9;
        bArr3[35] = -9;
        bArr3[36] = -9;
        bArr3[37] = -9;
        bArr3[38] = -9;
        bArr3[39] = -9;
        bArr3[40] = -9;
        bArr3[41] = -9;
        bArr3[42] = -9;
        bArr3[43] = -9;
        bArr3[44] = -9;
        bArr3[46] = -9;
        bArr3[47] = -9;
        bArr3[48] = 1;
        bArr3[49] = 2;
        bArr3[50] = 3;
        bArr3[51] = 4;
        bArr3[52] = 5;
        bArr3[53] = 6;
        bArr3[54] = 7;
        bArr3[55] = 8;
        bArr3[56] = 9;
        bArr3[57] = NEW_LINE;
        bArr3[58] = -9;
        bArr3[59] = -9;
        bArr3[60] = -9;
        bArr3[61] = EQUALS_SIGN_ENC;
        bArr3[62] = -9;
        bArr3[63] = -9;
        bArr3[64] = -9;
        bArr3[65] = 11;
        bArr3[66] = 12;
        bArr3[67] = 13;
        bArr3[68] = 14;
        bArr3[69] = 15;
        bArr3[70] = 16;
        bArr3[71] = 17;
        bArr3[72] = 18;
        bArr3[73] = 19;
        bArr3[74] = 20;
        bArr3[75] = 21;
        bArr3[MAX_LINE_LENGTH] = 22;
        bArr3[77] = 23;
        bArr3[78] = 24;
        bArr3[79] = 25;
        bArr3[80] = 26;
        bArr3[81] = 27;
        bArr3[82] = 28;
        bArr3[83] = 29;
        bArr3[84] = 30;
        bArr3[85] = 31;
        bArr3[86] = 32;
        bArr3[87] = 33;
        bArr3[88] = 34;
        bArr3[89] = 35;
        bArr3[90] = 36;
        bArr3[91] = -9;
        bArr3[92] = -9;
        bArr3[93] = -9;
        bArr3[94] = -9;
        bArr3[95] = 37;
        bArr3[96] = -9;
        bArr3[97] = 38;
        bArr3[98] = 39;
        bArr3[99] = 40;
        bArr3[100] = 41;
        bArr3[101] = 42;
        bArr3[102] = 43;
        bArr3[103] = 44;
        bArr3[104] = 45;
        bArr3[105] = 46;
        bArr3[106] = 47;
        bArr3[107] = 48;
        bArr3[108] = 49;
        bArr3[109] = 50;
        bArr3[110] = 51;
        bArr3[111] = 52;
        bArr3[112] = 53;
        bArr3[113] = 54;
        bArr3[114] = 55;
        bArr3[115] = 56;
        bArr3[116] = 57;
        bArr3[117] = 58;
        bArr3[118] = 59;
        bArr3[119] = 60;
        bArr3[120] = EQUALS_SIGN;
        bArr3[121] = 62;
        bArr3[122] = 63;
        bArr3[123] = -9;
        bArr3[124] = -9;
        bArr3[125] = -9;
        bArr3[126] = -9;
        _ORDERED_DECODABET = bArr3;
    }

    /* access modifiers changed from: private */
    public static final byte[] getAlphabet(int options) {
        if ((options & 16) == 16) {
            return _URL_SAFE_ALPHABET;
        }
        if ((options & 32) == 32) {
            return _ORDERED_ALPHABET;
        }
        return _STANDARD_ALPHABET;
    }

    /* access modifiers changed from: private */
    public static final byte[] getDecodabet(int options) {
        if ((options & 16) == 16) {
            return _URL_SAFE_DECODABET;
        }
        if ((options & 32) == 32) {
            return _ORDERED_DECODABET;
        }
        return _STANDARD_DECODABET;
    }

    private Base64() {
    }

    public static final void main(String[] args) {
        if (args.length < 3) {
            usage("Not enough arguments.");
            return;
        }
        String flag = args[0];
        String infile = args[1];
        String outfile = args[2];
        if (flag.equals("-e")) {
            encodeFileToFile(infile, outfile);
        } else if (flag.equals("-d")) {
            decodeFileToFile(infile, outfile);
        } else {
            usage("Unknown flag: " + flag);
        }
    }

    private static final void usage(String msg) {
        System.err.println(msg);
        System.err.println("Usage: java Base64 -e|-d inputfile outputfile");
    }

    /* access modifiers changed from: private */
    public static byte[] encode3to4(byte[] b4, byte[] threeBytes, int numSigBytes, int options) {
        encode3to4(threeBytes, 0, numSigBytes, b4, 0, options);
        return b4;
    }

    /* access modifiers changed from: private */
    public static byte[] encode3to4(byte[] source, int srcOffset, int numSigBytes, byte[] destination, int destOffset, int options) {
        int i;
        int i2;
        int i3 = 0;
        byte[] ALPHABET = getAlphabet(options);
        if (numSigBytes > 0) {
            i = (source[srcOffset] << 24) >>> 8;
        } else {
            i = 0;
        }
        if (numSigBytes > 1) {
            i2 = (source[srcOffset + 1] << 24) >>> 16;
        } else {
            i2 = 0;
        }
        int i4 = i2 | i;
        if (numSigBytes > 2) {
            i3 = (source[srcOffset + 2] << 24) >>> 24;
        }
        int inBuff = i4 | i3;
        switch (numSigBytes) {
            case 1:
                destination[destOffset] = ALPHABET[inBuff >>> 18];
                destination[destOffset + 1] = ALPHABET[(inBuff >>> 12) & 63];
                destination[destOffset + 2] = EQUALS_SIGN;
                destination[destOffset + 3] = EQUALS_SIGN;
                break;
            case 2:
                destination[destOffset] = ALPHABET[inBuff >>> 18];
                destination[destOffset + 1] = ALPHABET[(inBuff >>> 12) & 63];
                destination[destOffset + 2] = ALPHABET[(inBuff >>> 6) & 63];
                destination[destOffset + 3] = EQUALS_SIGN;
                break;
            case 3:
                destination[destOffset] = ALPHABET[inBuff >>> 18];
                destination[destOffset + 1] = ALPHABET[(inBuff >>> 12) & 63];
                destination[destOffset + 2] = ALPHABET[(inBuff >>> 6) & 63];
                destination[destOffset + 3] = ALPHABET[inBuff & 63];
                break;
        }
        return destination;
    }

    public static String encodeObject(Serializable serializableObject) {
        return encodeObject(serializableObject, 0);
    }

    public static String encodeObject(Serializable serializableObject, int options) {
        ByteArrayOutputStream baos = null;
        java.io.OutputStream b64os = null;
        ObjectOutputStream oos = null;
        GZIPOutputStream gzos = null;
        int gzip = options & 2;
        int i = options & 8;
        try {
            ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
            try {
                java.io.OutputStream b64os2 = new OutputStream(baos2, options | 1);
                if (gzip == 2) {
                    try {
                        GZIPOutputStream gzos2 = new GZIPOutputStream(b64os2);
                        try {
                            gzos = gzos2;
                            oos = new ObjectOutputStream(gzos2);
                        } catch (IOException e) {
                            e = e;
                            gzos = gzos2;
                            b64os = b64os2;
                            baos = baos2;
                            try {
                                e.printStackTrace();
                                try {
                                    oos.close();
                                } catch (Exception e2) {
                                }
                                try {
                                    gzos.close();
                                } catch (Exception e3) {
                                }
                                try {
                                    b64os.close();
                                } catch (Exception e4) {
                                }
                                try {
                                    baos.close();
                                } catch (Exception e5) {
                                }
                                return null;
                            } catch (Throwable th) {
                                th = th;
                                try {
                                    oos.close();
                                } catch (Exception e6) {
                                }
                                try {
                                    gzos.close();
                                } catch (Exception e7) {
                                }
                                try {
                                    b64os.close();
                                } catch (Exception e8) {
                                }
                                try {
                                    baos.close();
                                } catch (Exception e9) {
                                }
                                throw th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            gzos = gzos2;
                            b64os = b64os2;
                            baos = baos2;
                            oos.close();
                            gzos.close();
                            b64os.close();
                            baos.close();
                            throw th;
                        }
                    } catch (IOException e10) {
                        e = e10;
                        b64os = b64os2;
                        baos = baos2;
                        e.printStackTrace();
                        oos.close();
                        gzos.close();
                        b64os.close();
                        baos.close();
                        return null;
                    } catch (Throwable th3) {
                        th = th3;
                        b64os = b64os2;
                        baos = baos2;
                        oos.close();
                        gzos.close();
                        b64os.close();
                        baos.close();
                        throw th;
                    }
                } else {
                    oos = new ObjectOutputStream(b64os2);
                }
                oos.writeObject(serializableObject);
                try {
                    oos.close();
                } catch (Exception e11) {
                }
                try {
                    gzos.close();
                } catch (Exception e12) {
                }
                try {
                    b64os2.close();
                } catch (Exception e13) {
                }
                try {
                    baos2.close();
                } catch (Exception e14) {
                }
                try {
                    return new String(baos2.toByteArray(), "UTF-8");
                } catch (UnsupportedEncodingException e15) {
                    return new String(baos2.toByteArray());
                }
            } catch (IOException e16) {
                e = e16;
                baos = baos2;
                e.printStackTrace();
                oos.close();
                gzos.close();
                b64os.close();
                baos.close();
                return null;
            } catch (Throwable th4) {
                th = th4;
                baos = baos2;
                oos.close();
                gzos.close();
                b64os.close();
                baos.close();
                throw th;
            }
        } catch (IOException e17) {
            e = e17;
            e.printStackTrace();
            oos.close();
            gzos.close();
            b64os.close();
            baos.close();
            return null;
        }
    }

    public static String encodeBytes(byte[] source) {
        return encodeBytes(source, 0, source.length, 0);
    }

    public static String encodeBytes(byte[] source, int options) {
        return encodeBytes(source, 0, source.length, options);
    }

    public static String encodeBytes(byte[] source, int off, int len) {
        return encodeBytes(source, off, len, 0);
    }

    public static String encodeBytes(byte[] source, int off, int len, int options) {
        OutputStream b64os;
        GZIPOutputStream gZIPOutputStream;
        int dontBreakLines = options & 8;
        if ((options & 2) == 2) {
            ByteArrayOutputStream baos = null;
            GZIPOutputStream gzos = null;
            OutputStream b64os2 = null;
            try {
                ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
                try {
                    b64os = new OutputStream(baos2, options | 1);
                    try {
                        gZIPOutputStream = new GZIPOutputStream(b64os);
                    } catch (IOException e) {
                        e = e;
                        b64os2 = b64os;
                        baos = baos2;
                        try {
                            e.printStackTrace();
                            try {
                                gzos.close();
                            } catch (Exception e2) {
                            }
                            try {
                                b64os2.close();
                            } catch (Exception e3) {
                            }
                            try {
                                baos.close();
                            } catch (Exception e4) {
                            }
                            return null;
                        } catch (Throwable th) {
                            th = th;
                            try {
                                gzos.close();
                            } catch (Exception e5) {
                            }
                            try {
                                b64os2.close();
                            } catch (Exception e6) {
                            }
                            try {
                                baos.close();
                            } catch (Exception e7) {
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        b64os2 = b64os;
                        baos = baos2;
                        gzos.close();
                        b64os2.close();
                        baos.close();
                        throw th;
                    }
                } catch (IOException e8) {
                    e = e8;
                    baos = baos2;
                    e.printStackTrace();
                    gzos.close();
                    b64os2.close();
                    baos.close();
                    return null;
                } catch (Throwable th3) {
                    th = th3;
                    baos = baos2;
                    gzos.close();
                    b64os2.close();
                    baos.close();
                    throw th;
                }
                try {
                    gZIPOutputStream.write(source, off, len);
                    gZIPOutputStream.close();
                    try {
                        gZIPOutputStream.close();
                    } catch (Exception e9) {
                    }
                    try {
                        b64os.close();
                    } catch (Exception e10) {
                    }
                    try {
                        baos2.close();
                    } catch (Exception e11) {
                    }
                    try {
                        return new String(baos2.toByteArray(), "UTF-8");
                    } catch (UnsupportedEncodingException e12) {
                        return new String(baos2.toByteArray());
                    }
                } catch (IOException e13) {
                    e = e13;
                    b64os2 = b64os;
                    gzos = gZIPOutputStream;
                    baos = baos2;
                    e.printStackTrace();
                    gzos.close();
                    b64os2.close();
                    baos.close();
                    return null;
                } catch (Throwable th4) {
                    th = th4;
                    b64os2 = b64os;
                    gzos = gZIPOutputStream;
                    baos = baos2;
                    gzos.close();
                    b64os2.close();
                    baos.close();
                    throw th;
                }
            } catch (IOException e14) {
                e = e14;
                e.printStackTrace();
                gzos.close();
                b64os2.close();
                baos.close();
                return null;
            }
        } else {
            boolean breakLines = dontBreakLines == 0;
            int len43 = (len * 4) / 3;
            byte[] outBuff = new byte[((breakLines ? len43 / MAX_LINE_LENGTH : 0) + len43 + (len % 3 > 0 ? 4 : 0))];
            int d = 0;
            int e15 = 0;
            int len2 = len - 2;
            int lineLength = 0;
            while (d < len2) {
                encode3to4(source, d + off, 3, outBuff, e15, options);
                lineLength += 4;
                if (breakLines && lineLength == MAX_LINE_LENGTH) {
                    outBuff[e15 + 4] = NEW_LINE;
                    e15++;
                    lineLength = 0;
                }
                d += 3;
                e15 += 4;
            }
            if (d < len) {
                encode3to4(source, d + off, len - d, outBuff, e15, options);
                e15 += 4;
            }
            try {
                return new String(outBuff, 0, e15, "UTF-8");
            } catch (UnsupportedEncodingException e16) {
                return new String(outBuff, 0, e15);
            }
        }
    }

    /* access modifiers changed from: private */
    public static int decode4to3(byte[] source, int srcOffset, byte[] destination, int destOffset, int options) {
        byte[] DECODABET = getDecodabet(options);
        if (source[srcOffset + 2] == 61) {
            destination[destOffset] = (byte) ((((DECODABET[source[srcOffset]] & EQUALS_SIGN_ENC) << 18) | ((DECODABET[source[srcOffset + 1]] & EQUALS_SIGN_ENC) << 12)) >>> 16);
            return 1;
        } else if (source[srcOffset + 3] == 61) {
            int outBuff = ((DECODABET[source[srcOffset]] & EQUALS_SIGN_ENC) << 18) | ((DECODABET[source[srcOffset + 1]] & EQUALS_SIGN_ENC) << 12) | ((DECODABET[source[srcOffset + 2]] & EQUALS_SIGN_ENC) << 6);
            destination[destOffset] = (byte) (outBuff >>> 16);
            destination[destOffset + 1] = (byte) (outBuff >>> 8);
            return 2;
        } else {
            try {
                int outBuff2 = ((DECODABET[source[srcOffset]] & EQUALS_SIGN_ENC) << 18) | ((DECODABET[source[srcOffset + 1]] & EQUALS_SIGN_ENC) << 12) | ((DECODABET[source[srcOffset + 2]] & EQUALS_SIGN_ENC) << 6) | (DECODABET[source[srcOffset + 3]] & MotionEventCompat.ACTION_MASK);
                destination[destOffset] = (byte) (outBuff2 >> 16);
                destination[destOffset + 1] = (byte) (outBuff2 >> 8);
                destination[destOffset + 2] = (byte) outBuff2;
                return 3;
            } catch (Exception e) {
                System.out.println(((int) source[srcOffset]) + ": " + ((int) DECODABET[source[srcOffset]]));
                System.out.println(((int) source[srcOffset + 1]) + ": " + ((int) DECODABET[source[srcOffset + 1]]));
                System.out.println(((int) source[srcOffset + 2]) + ": " + ((int) DECODABET[source[srcOffset + 2]]));
                System.out.println(((int) source[srcOffset + 3]) + ": " + ((int) DECODABET[source[srcOffset + 3]]));
                return -1;
            }
        }
    }

    public static byte[] decode(byte[] source, int off, int len, int options) {
        byte[] DECODABET = getDecodabet(options);
        byte[] outBuff = new byte[((len * 3) / 4)];
        int outBuffPosn = 0;
        byte[] b4 = new byte[4];
        int b4Posn = 0;
        int i = off;
        while (true) {
            int b4Posn2 = b4Posn;
            if (i >= off + len) {
                break;
            }
            byte sbiCrop = (byte) (source[i] & Byte.MAX_VALUE);
            byte sbiDecode = DECODABET[sbiCrop];
            if (sbiDecode >= -5) {
                if (sbiDecode >= -1) {
                    b4Posn = b4Posn2 + 1;
                    b4[b4Posn2] = sbiCrop;
                    if (b4Posn > 3) {
                        outBuffPosn += decode4to3(b4, 0, outBuff, outBuffPosn, options);
                        b4Posn = 0;
                        if (sbiCrop == 61) {
                            break;
                        }
                    } else {
                        continue;
                    }
                } else {
                    b4Posn = b4Posn2;
                }
                i++;
            } else {
                System.err.println("Bad Base64 input character at " + i + ": " + ((int) source[i]) + "(decimal)");
                return null;
            }
        }
        byte[] out = new byte[outBuffPosn];
        System.arraycopy(outBuff, 0, out, 0, outBuffPosn);
        return out;
    }

    public static byte[] decode(String s) {
        return decode(s, 0);
    }

    public static byte[] decode(String s, int options) {
        byte[] bytes;
        try {
            bytes = s.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            bytes = s.getBytes();
        }
        byte[] bytes2 = decode(bytes, 0, bytes.length, options);
        if (bytes2 != null && bytes2.length >= 4 && 35615 == ((bytes2[0] & MotionEventCompat.ACTION_MASK) | ((bytes2[1] << 8) & MotionEventCompat.ACTION_POINTER_INDEX_MASK))) {
            ByteArrayInputStream bais = null;
            GZIPInputStream gzis = null;
            ByteArrayOutputStream baos = null;
            byte[] buffer = new byte[AccessibilityEventCompat.TYPE_WINDOW_CONTENT_CHANGED];
            try {
                ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
                try {
                    ByteArrayInputStream bais2 = new ByteArrayInputStream(bytes2);
                    try {
                        GZIPInputStream gzis2 = new GZIPInputStream(bais2);
                        while (true) {
                            try {
                                int length = gzis2.read(buffer);
                                if (length < 0) {
                                    break;
                                }
                                baos2.write(buffer, 0, length);
                            } catch (IOException e2) {
                                baos = baos2;
                                gzis = gzis2;
                                bais = bais2;
                            } catch (Throwable th) {
                                th = th;
                                baos = baos2;
                                gzis = gzis2;
                                bais = bais2;
                                try {
                                    baos.close();
                                } catch (Exception e3) {
                                }
                                try {
                                    gzis.close();
                                } catch (Exception e4) {
                                }
                                try {
                                    bais.close();
                                } catch (Exception e5) {
                                }
                                throw th;
                            }
                        }
                        bytes2 = baos2.toByteArray();
                        try {
                            baos2.close();
                        } catch (Exception e6) {
                        }
                        try {
                            gzis2.close();
                        } catch (Exception e7) {
                        }
                        try {
                            bais2.close();
                        } catch (Exception e8) {
                        }
                    } catch (IOException e9) {
                        baos = baos2;
                        bais = bais2;
                        try {
                            baos.close();
                        } catch (Exception e10) {
                        }
                        try {
                            gzis.close();
                        } catch (Exception e11) {
                        }
                        try {
                            bais.close();
                        } catch (Exception e12) {
                        }
                        return bytes2;
                    } catch (Throwable th2) {
                        th = th2;
                        baos = baos2;
                        bais = bais2;
                        baos.close();
                        gzis.close();
                        bais.close();
                        throw th;
                    }
                } catch (IOException e13) {
                    baos = baos2;
                    baos.close();
                    gzis.close();
                    bais.close();
                    return bytes2;
                } catch (Throwable th3) {
                    th = th3;
                    baos = baos2;
                    baos.close();
                    gzis.close();
                    bais.close();
                    throw th;
                }
            } catch (IOException e14) {
                baos.close();
                gzis.close();
                bais.close();
                return bytes2;
            } catch (Throwable th4) {
                th = th4;
                baos.close();
                gzis.close();
                bais.close();
                throw th;
            }
        }
        return bytes2;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x002d=Splitter:B:23:0x002d, B:14:0x001f=Splitter:B:14:0x001f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object decodeToObject(java.lang.String r9) {
        /*
            byte[] r4 = decode(r9)
            r0 = 0
            r5 = 0
            r3 = 0
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream     // Catch:{ IOException -> 0x001e, ClassNotFoundException -> 0x002c }
            r1.<init>(r4)     // Catch:{ IOException -> 0x001e, ClassNotFoundException -> 0x002c }
            java.io.ObjectInputStream r6 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x005e, ClassNotFoundException -> 0x0057, all -> 0x0050 }
            r6.<init>(r1)     // Catch:{ IOException -> 0x005e, ClassNotFoundException -> 0x0057, all -> 0x0050 }
            java.lang.Object r3 = r6.readObject()     // Catch:{ IOException -> 0x0061, ClassNotFoundException -> 0x005a, all -> 0x0053 }
            r1.close()     // Catch:{ Exception -> 0x004e }
        L_0x0018:
            r6.close()     // Catch:{ Exception -> 0x0042 }
            r5 = r6
            r0 = r1
        L_0x001d:
            return r3
        L_0x001e:
            r2 = move-exception
        L_0x001f:
            r2.printStackTrace()     // Catch:{ all -> 0x003a }
            r3 = 0
            r0.close()     // Catch:{ Exception -> 0x0046 }
        L_0x0026:
            r5.close()     // Catch:{ Exception -> 0x002a }
            goto L_0x001d
        L_0x002a:
            r7 = move-exception
            goto L_0x001d
        L_0x002c:
            r2 = move-exception
        L_0x002d:
            r2.printStackTrace()     // Catch:{ all -> 0x003a }
            r3 = 0
            r0.close()     // Catch:{ Exception -> 0x0048 }
        L_0x0034:
            r5.close()     // Catch:{ Exception -> 0x0038 }
            goto L_0x001d
        L_0x0038:
            r7 = move-exception
            goto L_0x001d
        L_0x003a:
            r7 = move-exception
        L_0x003b:
            r0.close()     // Catch:{ Exception -> 0x004a }
        L_0x003e:
            r5.close()     // Catch:{ Exception -> 0x004c }
        L_0x0041:
            throw r7
        L_0x0042:
            r7 = move-exception
            r5 = r6
            r0 = r1
            goto L_0x001d
        L_0x0046:
            r7 = move-exception
            goto L_0x0026
        L_0x0048:
            r7 = move-exception
            goto L_0x0034
        L_0x004a:
            r8 = move-exception
            goto L_0x003e
        L_0x004c:
            r8 = move-exception
            goto L_0x0041
        L_0x004e:
            r7 = move-exception
            goto L_0x0018
        L_0x0050:
            r7 = move-exception
            r0 = r1
            goto L_0x003b
        L_0x0053:
            r7 = move-exception
            r5 = r6
            r0 = r1
            goto L_0x003b
        L_0x0057:
            r2 = move-exception
            r0 = r1
            goto L_0x002d
        L_0x005a:
            r2 = move-exception
            r5 = r6
            r0 = r1
            goto L_0x002d
        L_0x005e:
            r2 = move-exception
            r0 = r1
            goto L_0x001f
        L_0x0061:
            r2 = move-exception
            r5 = r6
            r0 = r1
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.util.Base64.decodeToObject(java.lang.String):java.lang.Object");
    }

    public static boolean encodeToFile(byte[] dataToEncode, String filename) {
        boolean success;
        OutputStream bos = null;
        try {
            OutputStream bos2 = new OutputStream(new FileOutputStream(filename), 1);
            try {
                bos2.write(dataToEncode);
                success = true;
                try {
                    bos2.close();
                } catch (Exception e) {
                }
            } catch (IOException e2) {
                bos = bos2;
                success = false;
                try {
                    bos.close();
                } catch (Exception e3) {
                }
                return success;
            } catch (Throwable th) {
                th = th;
                bos = bos2;
                try {
                    bos.close();
                } catch (Exception e4) {
                }
                throw th;
            }
        } catch (IOException e5) {
            success = false;
            bos.close();
            return success;
        } catch (Throwable th2) {
            th = th2;
            bos.close();
            throw th;
        }
        return success;
    }

    public static boolean decodeToFile(String dataToDecode, String filename) {
        boolean success;
        OutputStream bos = null;
        try {
            OutputStream bos2 = new OutputStream(new FileOutputStream(filename), 0);
            try {
                bos2.write(dataToDecode.getBytes("UTF-8"));
                success = true;
                try {
                    bos2.close();
                } catch (Exception e) {
                }
            } catch (IOException e2) {
                bos = bos2;
                success = false;
                try {
                    bos.close();
                } catch (Exception e3) {
                }
                return success;
            } catch (Throwable th) {
                th = th;
                bos = bos2;
                try {
                    bos.close();
                } catch (Exception e4) {
                }
                throw th;
            }
        } catch (IOException e5) {
            success = false;
            bos.close();
            return success;
        } catch (Throwable th2) {
            th = th2;
            bos.close();
            throw th;
        }
        return success;
    }

    public static byte[] decodeFromFile(String filename) {
        byte[] decodedData = null;
        InputStream bis = null;
        try {
            File file = new File(filename);
            byte[] bArr = null;
            int length = 0;
            if (file.length() > 2147483647L) {
                System.err.println("File is too big for this convenience method (" + file.length() + " bytes).");
                try {
                    bis.close();
                    return null;
                } catch (Exception e) {
                    return null;
                }
            } else {
                byte[] buffer = new byte[((int) file.length())];
                InputStream bis2 = new InputStream(new BufferedInputStream(new FileInputStream(file)), 0);
                while (true) {
                    try {
                        int numBytes = bis2.read(buffer, length, 4096);
                        if (numBytes < 0) {
                            break;
                        }
                        length += numBytes;
                    } catch (IOException e2) {
                        bis = bis2;
                        try {
                            System.err.println("Error decoding from file " + filename);
                            try {
                                bis.close();
                            } catch (Exception e3) {
                            }
                            return decodedData;
                        } catch (Throwable th) {
                            th = th;
                            try {
                                bis.close();
                            } catch (Exception e4) {
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        bis = bis2;
                        bis.close();
                        throw th;
                    }
                }
                decodedData = new byte[length];
                System.arraycopy(buffer, 0, decodedData, 0, length);
                try {
                    bis2.close();
                } catch (Exception e5) {
                }
                return decodedData;
            }
        } catch (IOException e6) {
            System.err.println("Error decoding from file " + filename);
            bis.close();
            return decodedData;
        }
    }

    public static String encodeFromFile(String filename) {
        InputStream bis = null;
        try {
            File file = new File(filename);
            byte[] buffer = new byte[Math.max((int) (((double) file.length()) * 1.4d), 40)];
            int length = 0;
            InputStream bis2 = new InputStream(new BufferedInputStream(new FileInputStream(file)), 1);
            while (true) {
                try {
                    int numBytes = bis2.read(buffer, length, 4096);
                    if (numBytes < 0) {
                        String encodedData = new String(buffer, 0, length, "UTF-8");
                        try {
                            bis2.close();
                            return encodedData;
                        } catch (Exception e) {
                            return encodedData;
                        }
                    } else {
                        length += numBytes;
                    }
                } catch (IOException e2) {
                    bis = bis2;
                    try {
                        System.err.println("Error encoding from file " + filename);
                        try {
                            bis.close();
                            return null;
                        } catch (Exception e3) {
                            return null;
                        }
                    } catch (Throwable th) {
                        th = th;
                        try {
                            bis.close();
                        } catch (Exception e4) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    bis = bis2;
                    bis.close();
                    throw th;
                }
            }
        } catch (IOException e5) {
            System.err.println("Error encoding from file " + filename);
            bis.close();
            return null;
        }
    }

    public static void encodeFileToFile(String infile, String outfile) {
        String encoded = encodeFromFile(infile);
        java.io.OutputStream out = null;
        try {
            java.io.OutputStream out2 = new BufferedOutputStream(new FileOutputStream(outfile));
            try {
                out2.write(encoded.getBytes(CharEncoding.US_ASCII));
                try {
                    out2.close();
                } catch (Exception e) {
                }
            } catch (IOException e2) {
                ex = e2;
                out = out2;
                try {
                    ex.printStackTrace();
                    try {
                        out.close();
                    } catch (Exception e3) {
                    }
                } catch (Throwable th) {
                    th = th;
                    try {
                        out.close();
                    } catch (Exception e4) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                out = out2;
                out.close();
                throw th;
            }
        } catch (IOException e5) {
            ex = e5;
            ex.printStackTrace();
            out.close();
        }
    }

    public static void decodeFileToFile(String infile, String outfile) {
        byte[] decoded = decodeFromFile(infile);
        java.io.OutputStream out = null;
        try {
            java.io.OutputStream out2 = new BufferedOutputStream(new FileOutputStream(outfile));
            try {
                out2.write(decoded);
                try {
                    out2.close();
                } catch (Exception e) {
                }
            } catch (IOException e2) {
                ex = e2;
                out = out2;
                try {
                    ex.printStackTrace();
                    try {
                        out.close();
                    } catch (Exception e3) {
                    }
                } catch (Throwable th) {
                    th = th;
                    try {
                        out.close();
                    } catch (Exception e4) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                out = out2;
                out.close();
                throw th;
            }
        } catch (IOException e5) {
            ex = e5;
            ex.printStackTrace();
            out.close();
        }
    }

    public static class InputStream extends FilterInputStream {
        private byte[] alphabet;
        private boolean breakLines;
        private byte[] buffer;
        private int bufferLength;
        private byte[] decodabet;
        private boolean encode;
        private int lineLength;
        private int numSigBytes;
        private int options;
        private int position;

        public InputStream(java.io.InputStream in) {
            this(in, 0);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public InputStream(java.io.InputStream in, int options2) {
            super(in);
            boolean z = true;
            this.breakLines = (options2 & 8) != 8;
            this.encode = (options2 & 1) != 1 ? false : z;
            this.bufferLength = this.encode ? 4 : 3;
            this.buffer = new byte[this.bufferLength];
            this.position = -1;
            this.lineLength = 0;
            this.options = options2;
            this.alphabet = Base64.getAlphabet(options2);
            this.decodabet = Base64.getDecodabet(options2);
        }

        public int read() throws IOException {
            int b;
            if (this.position < 0) {
                if (this.encode) {
                    byte[] b3 = new byte[3];
                    int numBinaryBytes = 0;
                    for (int i = 0; i < 3; i++) {
                        try {
                            int b2 = this.in.read();
                            if (b2 >= 0) {
                                b3[i] = (byte) b2;
                                numBinaryBytes++;
                            }
                        } catch (IOException e) {
                            if (i == 0) {
                                throw e;
                            }
                        }
                    }
                    if (numBinaryBytes <= 0) {
                        return -1;
                    }
                    byte[] unused = Base64.encode3to4(b3, 0, numBinaryBytes, this.buffer, 0, this.options);
                    this.position = 0;
                    this.numSigBytes = 4;
                } else {
                    byte[] b4 = new byte[4];
                    int i2 = 0;
                    while (i2 < 4) {
                        do {
                            b = this.in.read();
                            if (b < 0) {
                                break;
                            }
                        } while (this.decodabet[b & 127] <= -5);
                        if (b < 0) {
                            break;
                        }
                        b4[i2] = (byte) b;
                        i2++;
                    }
                    if (i2 == 4) {
                        this.numSigBytes = Base64.decode4to3(b4, 0, this.buffer, 0, this.options);
                        this.position = 0;
                    } else if (i2 == 0) {
                        return -1;
                    } else {
                        throw new IOException("Improperly padded Base64 input.");
                    }
                }
            }
            if (this.position < 0) {
                throw new IOException("Error in Base64 code reading stream.");
            } else if (this.position >= this.numSigBytes) {
                return -1;
            } else {
                if (!this.encode || !this.breakLines || this.lineLength < Base64.MAX_LINE_LENGTH) {
                    this.lineLength++;
                    byte[] bArr = this.buffer;
                    int i3 = this.position;
                    this.position = i3 + 1;
                    byte b5 = bArr[i3];
                    if (this.position >= this.bufferLength) {
                        this.position = -1;
                    }
                    return b5 & Base64.EQUALS_SIGN_ENC;
                }
                this.lineLength = 0;
                return 10;
            }
        }

        public int read(byte[] dest, int off, int len) throws IOException {
            int i = 0;
            while (i < len) {
                int b = read();
                if (b >= 0) {
                    dest[off + i] = (byte) b;
                    i++;
                } else if (i == 0) {
                    return -1;
                } else {
                    return i;
                }
            }
            return i;
        }
    }

    public static class OutputStream extends FilterOutputStream {
        private byte[] alphabet;
        private byte[] b4;
        private boolean breakLines;
        private byte[] buffer;
        private int bufferLength;
        private byte[] decodabet;
        private boolean encode;
        private int lineLength;
        private int options;
        private int position;
        private boolean suspendEncoding;

        public OutputStream(java.io.OutputStream out) {
            this(out, 1);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public OutputStream(java.io.OutputStream out, int options2) {
            super(out);
            int i;
            boolean z = true;
            this.breakLines = (options2 & 8) != 8;
            this.encode = (options2 & 1) != 1 ? false : z;
            if (this.encode) {
                i = 3;
            } else {
                i = 4;
            }
            this.bufferLength = i;
            this.buffer = new byte[this.bufferLength];
            this.position = 0;
            this.lineLength = 0;
            this.suspendEncoding = false;
            this.b4 = new byte[4];
            this.options = options2;
            this.alphabet = Base64.getAlphabet(options2);
            this.decodabet = Base64.getDecodabet(options2);
        }

        public void write(int theByte) throws IOException {
            if (this.suspendEncoding) {
                this.out.write(theByte);
            } else if (this.encode) {
                byte[] bArr = this.buffer;
                int i = this.position;
                this.position = i + 1;
                bArr[i] = (byte) theByte;
                if (this.position >= this.bufferLength) {
                    this.out.write(Base64.encode3to4(this.b4, this.buffer, this.bufferLength, this.options));
                    this.lineLength += 4;
                    if (this.breakLines && this.lineLength >= Base64.MAX_LINE_LENGTH) {
                        this.out.write(10);
                        this.lineLength = 0;
                    }
                    this.position = 0;
                }
            } else if (this.decodabet[theByte & 127] > -5) {
                byte[] bArr2 = this.buffer;
                int i2 = this.position;
                this.position = i2 + 1;
                bArr2[i2] = (byte) theByte;
                if (this.position >= this.bufferLength) {
                    this.out.write(this.b4, 0, Base64.decode4to3(this.buffer, 0, this.b4, 0, this.options));
                    this.position = 0;
                }
            } else if (this.decodabet[theByte & 127] != -5) {
                throw new IOException("Invalid character in Base64 data.");
            }
        }

        public void write(byte[] theBytes, int off, int len) throws IOException {
            if (this.suspendEncoding) {
                this.out.write(theBytes, off, len);
                return;
            }
            for (int i = 0; i < len; i++) {
                write(theBytes[off + i]);
            }
        }

        public void flushBase64() throws IOException {
            if (this.position <= 0) {
                return;
            }
            if (this.encode) {
                this.out.write(Base64.encode3to4(this.b4, this.buffer, this.position, this.options));
                this.position = 0;
                return;
            }
            throw new IOException("Base64 input not properly padded.");
        }

        public void close() throws IOException {
            flushBase64();
            super.close();
            this.buffer = null;
            this.out = null;
        }

        public void suspendEncoding() throws IOException {
            flushBase64();
            this.suspendEncoding = true;
        }

        public void resumeEncoding() {
            this.suspendEncoding = false;
        }
    }
}
