package org.jivesoftware.smack.proxy;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import javax.net.SocketFactory;

public class Socks4ProxySocketFactory extends SocketFactory {
    private ProxyInfo proxy;

    public Socks4ProxySocketFactory(ProxyInfo proxy2) {
        this.proxy = proxy2;
    }

    public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
        return socks4ProxifiedSocket(host, port);
    }

    public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException, UnknownHostException {
        return socks4ProxifiedSocket(host, port);
    }

    public Socket createSocket(InetAddress host, int port) throws IOException {
        return socks4ProxifiedSocket(host.getHostAddress(), port);
    }

    public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
        return socks4ProxifiedSocket(address.getHostAddress(), port);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00d8, code lost:
        r7 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00d8 A[ExcHandler: RuntimeException (e java.lang.RuntimeException), Splitter:B:3:0x0037] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0100 A[SYNTHETIC, Splitter:B:36:0x0100] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:30:0x00e7=Splitter:B:30:0x00e7, B:52:0x0137=Splitter:B:52:0x0137} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.net.Socket socks4ProxifiedSocket(java.lang.String r29, int r30) throws java.io.IOException {
        /*
            r28 = this;
            r19 = 0
            r9 = 0
            r14 = 0
            r0 = r28
            org.jivesoftware.smack.proxy.ProxyInfo r0 = r0.proxy
            r24 = r0
            java.lang.String r16 = r24.getProxyAddress()
            r0 = r28
            org.jivesoftware.smack.proxy.ProxyInfo r0 = r0.proxy
            r24 = r0
            int r17 = r24.getProxyPort()
            r0 = r28
            org.jivesoftware.smack.proxy.ProxyInfo r0 = r0.proxy
            r24 = r0
            java.lang.String r23 = r24.getProxyUsername()
            r0 = r28
            org.jivesoftware.smack.proxy.ProxyInfo r0 = r0.proxy
            r24 = r0
            java.lang.String r15 = r24.getProxyPassword()
            java.net.Socket r20 = new java.net.Socket     // Catch:{ RuntimeException -> 0x0172, Exception -> 0x0170 }
            r0 = r20
            r1 = r16
            r2 = r17
            r0.<init>(r1, r2)     // Catch:{ RuntimeException -> 0x0172, Exception -> 0x0170 }
            java.io.InputStream r9 = r20.getInputStream()     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            java.io.OutputStream r14 = r20.getOutputStream()     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            r24 = 1
            r0 = r20
            r1 = r24
            r0.setTcpNoDelay(r1)     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            r24 = 1024(0x400, float:1.435E-42)
            r0 = r24
            byte[] r5 = new byte[r0]     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            r10 = 0
            r10 = 0
            int r11 = r10 + 1
            r24 = 4
            r5[r10] = r24     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            int r10 = r11 + 1
            r24 = 1
            r5[r11] = r24     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            int r11 = r10 + 1
            int r24 = r30 >>> 8
            r0 = r24
            byte r0 = (byte) r0     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            r24 = r0
            r5[r10] = r24     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            int r10 = r11 + 1
            r0 = r30
            r0 = r0 & 255(0xff, float:3.57E-43)
            r24 = r0
            r0 = r24
            byte r0 = (byte) r0     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            r24 = r0
            r5[r11] = r24     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            java.net.InetAddress r4 = java.net.InetAddress.getByName(r29)     // Catch:{ UnknownHostException -> 0x00e6 }
            byte[] r6 = r4.getAddress()     // Catch:{ UnknownHostException -> 0x00e6 }
            r8 = 0
            r11 = r10
        L_0x0080:
            int r0 = r6.length     // Catch:{ UnknownHostException -> 0x0175 }
            r24 = r0
            r0 = r24
            if (r8 < r0) goto L_0x00dc
            if (r23 == 0) goto L_0x00a3
            byte[] r24 = r23.getBytes()     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            r25 = 0
            int r26 = r23.length()     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            r0 = r24
            r1 = r25
            r2 = r26
            java.lang.System.arraycopy(r0, r1, r5, r11, r2)     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            int r24 = r23.length()     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            int r10 = r11 + r24
            r11 = r10
        L_0x00a3:
            int r10 = r11 + 1
            r24 = 0
            r5[r11] = r24     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            r24 = 0
            r0 = r24
            r14.write(r5, r0, r10)     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            r12 = 6
            r18 = 0
        L_0x00b3:
            r0 = r18
            if (r0 < r12) goto L_0x010f
            r24 = 0
            byte r24 = r5[r24]     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            if (r24 == 0) goto L_0x0128
            org.jivesoftware.smack.proxy.ProxyException r24 = new org.jivesoftware.smack.proxy.ProxyException     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            org.jivesoftware.smack.proxy.ProxyInfo$ProxyType r25 = org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS4     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            java.lang.StringBuilder r26 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            java.lang.String r27 = "server returns VN "
            r26.<init>(r27)     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            r27 = 0
            byte r27 = r5[r27]     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            java.lang.StringBuilder r26 = r26.append(r27)     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            java.lang.String r26 = r26.toString()     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            r24.<init>(r25, r26)     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            throw r24     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
        L_0x00d8:
            r7 = move-exception
            r19 = r20
        L_0x00db:
            throw r7
        L_0x00dc:
            int r10 = r11 + 1
            byte r24 = r6[r8]     // Catch:{ UnknownHostException -> 0x00e6 }
            r5[r11] = r24     // Catch:{ UnknownHostException -> 0x00e6 }
            int r8 = r8 + 1
            r11 = r10
            goto L_0x0080
        L_0x00e6:
            r22 = move-exception
        L_0x00e7:
            org.jivesoftware.smack.proxy.ProxyException r24 = new org.jivesoftware.smack.proxy.ProxyException     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            org.jivesoftware.smack.proxy.ProxyInfo$ProxyType r25 = org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS4     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            java.lang.String r26 = r22.toString()     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            r0 = r24
            r1 = r25
            r2 = r26
            r3 = r22
            r0.<init>(r1, r2, r3)     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            throw r24     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
        L_0x00fb:
            r7 = move-exception
            r19 = r20
        L_0x00fe:
            if (r19 == 0) goto L_0x0103
            r19.close()     // Catch:{ Exception -> 0x016e }
        L_0x0103:
            org.jivesoftware.smack.proxy.ProxyException r24 = new org.jivesoftware.smack.proxy.ProxyException
            org.jivesoftware.smack.proxy.ProxyInfo$ProxyType r25 = org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS4
            java.lang.String r26 = r7.toString()
            r24.<init>(r25, r26)
            throw r24
        L_0x010f:
            int r24 = r12 - r18
            r0 = r18
            r1 = r24
            int r8 = r9.read(r5, r0, r1)     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            if (r8 > 0) goto L_0x0125
            org.jivesoftware.smack.proxy.ProxyException r24 = new org.jivesoftware.smack.proxy.ProxyException     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            org.jivesoftware.smack.proxy.ProxyInfo$ProxyType r25 = org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS4     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            java.lang.String r26 = "stream is closed"
            r24.<init>(r25, r26)     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            throw r24     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
        L_0x0125:
            int r18 = r18 + r8
            goto L_0x00b3
        L_0x0128:
            r24 = 1
            byte r24 = r5[r24]     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            r25 = 90
            r0 = r24
            r1 = r25
            if (r0 == r1) goto L_0x0156
            r20.close()     // Catch:{ Exception -> 0x016c, RuntimeException -> 0x00d8 }
        L_0x0137:
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            java.lang.String r25 = "ProxySOCKS4: server returns CD "
            r24.<init>(r25)     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            r25 = 1
            byte r25 = r5[r25]     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            java.lang.StringBuilder r24 = r24.append(r25)     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            java.lang.String r13 = r24.toString()     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            org.jivesoftware.smack.proxy.ProxyException r24 = new org.jivesoftware.smack.proxy.ProxyException     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            org.jivesoftware.smack.proxy.ProxyInfo$ProxyType r25 = org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS4     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            r0 = r24
            r1 = r25
            r0.<init>(r1, r13)     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            throw r24     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
        L_0x0156:
            r24 = 2
            r0 = r24
            byte[] r0 = new byte[r0]     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            r21 = r0
            r24 = 0
            r25 = 2
            r0 = r21
            r1 = r24
            r2 = r25
            r9.read(r0, r1, r2)     // Catch:{ RuntimeException -> 0x00d8, Exception -> 0x00fb }
            return r20
        L_0x016c:
            r24 = move-exception
            goto L_0x0137
        L_0x016e:
            r24 = move-exception
            goto L_0x0103
        L_0x0170:
            r7 = move-exception
            goto L_0x00fe
        L_0x0172:
            r7 = move-exception
            goto L_0x00db
        L_0x0175:
            r22 = move-exception
            r10 = r11
            goto L_0x00e7
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.proxy.Socks4ProxySocketFactory.socks4ProxifiedSocket(java.lang.String, int):java.net.Socket");
    }
}
