package org.jivesoftware.smack.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.jivesoftware.smack.packet.IQ;

public class ProviderManager {
    private static ProviderManager instance;
    private Map<String, Object> extensionProviders = new ConcurrentHashMap();
    private Map<String, Object> iqProviders = new ConcurrentHashMap();

    public static synchronized ProviderManager getInstance() {
        ProviderManager providerManager;
        synchronized (ProviderManager.class) {
            if (instance == null) {
                instance = new ProviderManager();
            }
            providerManager = instance;
        }
        return providerManager;
    }

    public static synchronized void setInstance(ProviderManager providerManager) {
        synchronized (ProviderManager.class) {
            if (instance != null) {
                throw new IllegalStateException("ProviderManager singleton already set");
            }
            instance = providerManager;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void initialize() {
        /*
            r20 = this;
            java.lang.ClassLoader[] r3 = r20.getClassLoaders()     // Catch:{ Exception -> 0x00e1 }
            int r0 = r3.length     // Catch:{ Exception -> 0x00e1 }
            r17 = r0
            r16 = 0
        L_0x0009:
            r0 = r16
            r1 = r17
            if (r0 < r1) goto L_0x0010
        L_0x000f:
            return
        L_0x0010:
            r2 = r3[r16]     // Catch:{ Exception -> 0x00e1 }
            java.lang.String r18 = "META-INF/smack.providers"
            r0 = r18
            java.util.Enumeration r13 = r2.getResources(r0)     // Catch:{ Exception -> 0x00e1 }
        L_0x001a:
            boolean r18 = r13.hasMoreElements()     // Catch:{ Exception -> 0x00e1 }
            if (r18 != 0) goto L_0x0023
            int r16 = r16 + 1
            goto L_0x0009
        L_0x0023:
            java.lang.Object r15 = r13.nextElement()     // Catch:{ Exception -> 0x00e1 }
            java.net.URL r15 = (java.net.URL) r15     // Catch:{ Exception -> 0x00e1 }
            r14 = 0
            java.io.InputStream r14 = r15.openStream()     // Catch:{ all -> 0x00dc }
            org.xmlpull.v1.XmlPullParserFactory r18 = org.xmlpull.v1.XmlPullParserFactory.newInstance()     // Catch:{ all -> 0x00dc }
            org.xmlpull.v1.XmlPullParser r11 = r18.newPullParser()     // Catch:{ all -> 0x00dc }
            java.lang.String r18 = "http://xmlpull.org/v1/doc/features.html#process-namespaces"
            r19 = 1
            r0 = r18
            r1 = r19
            r11.setFeature(r0, r1)     // Catch:{ all -> 0x00dc }
            java.lang.String r18 = "UTF-8"
            r0 = r18
            r11.setInput(r14, r0)     // Catch:{ all -> 0x00dc }
            int r8 = r11.getEventType()     // Catch:{ all -> 0x00dc }
        L_0x004c:
            r18 = 2
            r0 = r18
            if (r8 != r0) goto L_0x00af
            java.lang.String r18 = r11.getName()     // Catch:{ all -> 0x00dc }
            java.lang.String r19 = "iqProvider"
            boolean r18 = r18.equals(r19)     // Catch:{ all -> 0x00dc }
            if (r18 == 0) goto L_0x00e7
            r11.next()     // Catch:{ all -> 0x00dc }
            r11.next()     // Catch:{ all -> 0x00dc }
            java.lang.String r7 = r11.nextText()     // Catch:{ all -> 0x00dc }
            r11.next()     // Catch:{ all -> 0x00dc }
            r11.next()     // Catch:{ all -> 0x00dc }
            java.lang.String r10 = r11.nextText()     // Catch:{ all -> 0x00dc }
            r11.next()     // Catch:{ all -> 0x00dc }
            r11.next()     // Catch:{ all -> 0x00dc }
            java.lang.String r4 = r11.nextText()     // Catch:{ all -> 0x00dc }
            r0 = r20
            java.lang.String r9 = r0.getProviderKey(r7, r10)     // Catch:{ all -> 0x00dc }
            r0 = r20
            java.util.Map<java.lang.String, java.lang.Object> r0 = r0.iqProviders     // Catch:{ all -> 0x00dc }
            r18 = r0
            r0 = r18
            boolean r18 = r0.containsKey(r9)     // Catch:{ all -> 0x00dc }
            if (r18 != 0) goto L_0x00af
            java.lang.Class r12 = java.lang.Class.forName(r4)     // Catch:{ ClassNotFoundException -> 0x00d7 }
            java.lang.Class<org.jivesoftware.smack.provider.IQProvider> r18 = org.jivesoftware.smack.provider.IQProvider.class
            r0 = r18
            boolean r18 = r0.isAssignableFrom(r12)     // Catch:{ ClassNotFoundException -> 0x00d7 }
            if (r18 == 0) goto L_0x00c1
            r0 = r20
            java.util.Map<java.lang.String, java.lang.Object> r0 = r0.iqProviders     // Catch:{ ClassNotFoundException -> 0x00d7 }
            r18 = r0
            java.lang.Object r19 = r12.newInstance()     // Catch:{ ClassNotFoundException -> 0x00d7 }
            r0 = r18
            r1 = r19
            r0.put(r9, r1)     // Catch:{ ClassNotFoundException -> 0x00d7 }
        L_0x00af:
            int r8 = r11.next()     // Catch:{ all -> 0x00dc }
            r18 = 1
            r0 = r18
            if (r8 != r0) goto L_0x004c
            r14.close()     // Catch:{ Exception -> 0x00be }
            goto L_0x001a
        L_0x00be:
            r18 = move-exception
            goto L_0x001a
        L_0x00c1:
            java.lang.Class<org.jivesoftware.smack.packet.IQ> r18 = org.jivesoftware.smack.packet.IQ.class
            r0 = r18
            boolean r18 = r0.isAssignableFrom(r12)     // Catch:{ ClassNotFoundException -> 0x00d7 }
            if (r18 == 0) goto L_0x00af
            r0 = r20
            java.util.Map<java.lang.String, java.lang.Object> r0 = r0.iqProviders     // Catch:{ ClassNotFoundException -> 0x00d7 }
            r18 = r0
            r0 = r18
            r0.put(r9, r12)     // Catch:{ ClassNotFoundException -> 0x00d7 }
            goto L_0x00af
        L_0x00d7:
            r5 = move-exception
            r5.printStackTrace()     // Catch:{ all -> 0x00dc }
            goto L_0x00af
        L_0x00dc:
            r16 = move-exception
            r14.close()     // Catch:{ Exception -> 0x0163 }
        L_0x00e0:
            throw r16     // Catch:{ Exception -> 0x00e1 }
        L_0x00e1:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x000f
        L_0x00e7:
            java.lang.String r18 = r11.getName()     // Catch:{ all -> 0x00dc }
            java.lang.String r19 = "extensionProvider"
            boolean r18 = r18.equals(r19)     // Catch:{ all -> 0x00dc }
            if (r18 == 0) goto L_0x00af
            r11.next()     // Catch:{ all -> 0x00dc }
            r11.next()     // Catch:{ all -> 0x00dc }
            java.lang.String r7 = r11.nextText()     // Catch:{ all -> 0x00dc }
            r11.next()     // Catch:{ all -> 0x00dc }
            r11.next()     // Catch:{ all -> 0x00dc }
            java.lang.String r10 = r11.nextText()     // Catch:{ all -> 0x00dc }
            r11.next()     // Catch:{ all -> 0x00dc }
            r11.next()     // Catch:{ all -> 0x00dc }
            java.lang.String r4 = r11.nextText()     // Catch:{ all -> 0x00dc }
            r0 = r20
            java.lang.String r9 = r0.getProviderKey(r7, r10)     // Catch:{ all -> 0x00dc }
            r0 = r20
            java.util.Map<java.lang.String, java.lang.Object> r0 = r0.extensionProviders     // Catch:{ all -> 0x00dc }
            r18 = r0
            r0 = r18
            boolean r18 = r0.containsKey(r9)     // Catch:{ all -> 0x00dc }
            if (r18 != 0) goto L_0x00af
            java.lang.Class r12 = java.lang.Class.forName(r4)     // Catch:{ ClassNotFoundException -> 0x0146 }
            java.lang.Class<org.jivesoftware.smack.provider.PacketExtensionProvider> r18 = org.jivesoftware.smack.provider.PacketExtensionProvider.class
            r0 = r18
            boolean r18 = r0.isAssignableFrom(r12)     // Catch:{ ClassNotFoundException -> 0x0146 }
            if (r18 == 0) goto L_0x014c
            r0 = r20
            java.util.Map<java.lang.String, java.lang.Object> r0 = r0.extensionProviders     // Catch:{ ClassNotFoundException -> 0x0146 }
            r18 = r0
            java.lang.Object r19 = r12.newInstance()     // Catch:{ ClassNotFoundException -> 0x0146 }
            r0 = r18
            r1 = r19
            r0.put(r9, r1)     // Catch:{ ClassNotFoundException -> 0x0146 }
            goto L_0x00af
        L_0x0146:
            r5 = move-exception
            r5.printStackTrace()     // Catch:{ all -> 0x00dc }
            goto L_0x00af
        L_0x014c:
            java.lang.Class<org.jivesoftware.smack.packet.PacketExtension> r18 = org.jivesoftware.smack.packet.PacketExtension.class
            r0 = r18
            boolean r18 = r0.isAssignableFrom(r12)     // Catch:{ ClassNotFoundException -> 0x0146 }
            if (r18 == 0) goto L_0x00af
            r0 = r20
            java.util.Map<java.lang.String, java.lang.Object> r0 = r0.extensionProviders     // Catch:{ ClassNotFoundException -> 0x0146 }
            r18 = r0
            r0 = r18
            r0.put(r9, r12)     // Catch:{ ClassNotFoundException -> 0x0146 }
            goto L_0x00af
        L_0x0163:
            r17 = move-exception
            goto L_0x00e0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.provider.ProviderManager.initialize():void");
    }

    public Object getIQProvider(String elementName, String namespace) {
        return this.iqProviders.get(getProviderKey(elementName, namespace));
    }

    public Collection<Object> getIQProviders() {
        return Collections.unmodifiableCollection(this.iqProviders.values());
    }

    public void addIQProvider(String elementName, String namespace, Object provider) {
        if ((provider instanceof IQProvider) || ((provider instanceof Class) && IQ.class.isAssignableFrom((Class) provider))) {
            this.iqProviders.put(getProviderKey(elementName, namespace), provider);
            return;
        }
        throw new IllegalArgumentException("Provider must be an IQProvider or a Class instance.");
    }

    public void removeIQProvider(String elementName, String namespace) {
        this.iqProviders.remove(getProviderKey(elementName, namespace));
    }

    public Object getExtensionProvider(String elementName, String namespace) {
        return this.extensionProviders.get(getProviderKey(elementName, namespace));
    }

    public void addExtensionProvider(String elementName, String namespace, Object provider) {
        if ((provider instanceof PacketExtensionProvider) || (provider instanceof Class)) {
            this.extensionProviders.put(getProviderKey(elementName, namespace), provider);
            return;
        }
        throw new IllegalArgumentException("Provider must be a PacketExtensionProvider or a Class instance.");
    }

    public void removeExtensionProvider(String elementName, String namespace) {
        this.extensionProviders.remove(getProviderKey(elementName, namespace));
    }

    public Collection<Object> getExtensionProviders() {
        return Collections.unmodifiableCollection(this.extensionProviders.values());
    }

    private String getProviderKey(String elementName, String namespace) {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append(elementName).append("/><").append(namespace).append("/>");
        return buf.toString();
    }

    private ClassLoader[] getClassLoaders() {
        ClassLoader[] classLoaders = {ProviderManager.class.getClassLoader(), Thread.currentThread().getContextClassLoader()};
        List<ClassLoader> loaders = new ArrayList<>();
        for (ClassLoader classLoader : classLoaders) {
            if (classLoader != null) {
                loaders.add(classLoader);
            }
        }
        return (ClassLoader[]) loaders.toArray(new ClassLoader[loaders.size()]);
    }

    private ProviderManager() {
        initialize();
    }
}
