package org.jivesoftware.smack;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Registration;

public class AccountManager {
    private boolean accountCreationSupported = false;
    private Connection connection;
    private Registration info = null;

    public AccountManager(Connection connection2) {
        this.connection = connection2;
    }

    /* access modifiers changed from: package-private */
    public void setSupportsAccountCreation(boolean accountCreationSupported2) {
        this.accountCreationSupported = accountCreationSupported2;
    }

    public boolean supportsAccountCreation() {
        boolean z = true;
        if (this.accountCreationSupported) {
            return true;
        }
        try {
            if (this.info == null) {
                getRegistrationInfo();
                if (this.info.getType() == IQ.Type.ERROR) {
                    z = false;
                }
                this.accountCreationSupported = z;
            }
            return this.accountCreationSupported;
        } catch (XMPPException e) {
            return false;
        }
    }

    public Collection<String> getAccountAttributes() {
        try {
            if (this.info == null) {
                getRegistrationInfo();
            }
            List<String> attributes = this.info.getRequiredFields();
            if (attributes.size() > 0) {
                return Collections.unmodifiableSet(new HashSet<>(attributes));
            }
        } catch (XMPPException xe) {
            xe.printStackTrace();
        }
        return Collections.emptySet();
    }

    public String getAccountAttribute(String name) {
        try {
            if (this.info == null) {
                getRegistrationInfo();
            }
            return this.info.getAttributes().get(name);
        } catch (XMPPException xe) {
            xe.printStackTrace();
            return null;
        }
    }

    public String getAccountInstructions() {
        try {
            if (this.info == null) {
                getRegistrationInfo();
            }
            return this.info.getInstructions();
        } catch (XMPPException e) {
            return null;
        }
    }

    public void createAccount(String username, String password) throws XMPPException {
        if (!supportsAccountCreation()) {
            throw new XMPPException("Server does not support account creation.");
        }
        Map<String, String> attributes = new HashMap<>();
        for (String attributeName : getAccountAttributes()) {
            attributes.put(attributeName, StringUtils.EMPTY);
        }
        createAccount(username, password, attributes);
    }

    public void createAccount(String username, String password, Map<String, String> attributes) throws XMPPException {
        if (!supportsAccountCreation()) {
            throw new XMPPException("Server does not support account creation.");
        }
        Registration reg = new Registration();
        reg.setType(IQ.Type.SET);
        reg.setTo(this.connection.getServiceName());
        reg.setUsername(username);
        reg.setPassword(password);
        for (String s : attributes.keySet()) {
            reg.addAttribute(s, attributes.get(s));
        }
        PacketCollector collector = this.connection.createPacketCollector(new AndFilter(new PacketIDFilter(reg.getPacketID()), new PacketTypeFilter(IQ.class)));
        this.connection.sendPacket(reg);
        IQ result = (IQ) collector.nextResult((long) SmackConfiguration.getPacketReplyTimeout());
        collector.cancel();
        if (result == null) {
            throw new XMPPException("No response from server.");
        } else if (result.getType() == IQ.Type.ERROR) {
            throw new XMPPException(result.getError());
        }
    }

    public void changePassword(String newPassword) throws XMPPException {
        Registration reg = new Registration();
        reg.setType(IQ.Type.SET);
        reg.setTo(this.connection.getServiceName());
        reg.setUsername(org.jivesoftware.smack.util.StringUtils.parseName(this.connection.getUser()));
        reg.setPassword(newPassword);
        PacketCollector collector = this.connection.createPacketCollector(new AndFilter(new PacketIDFilter(reg.getPacketID()), new PacketTypeFilter(IQ.class)));
        this.connection.sendPacket(reg);
        IQ result = (IQ) collector.nextResult((long) SmackConfiguration.getPacketReplyTimeout());
        collector.cancel();
        if (result == null) {
            throw new XMPPException("No response from server.");
        } else if (result.getType() == IQ.Type.ERROR) {
            throw new XMPPException(result.getError());
        }
    }

    public void deleteAccount() throws XMPPException {
        if (!this.connection.isAuthenticated()) {
            throw new IllegalStateException("Must be logged in to delete a account.");
        }
        Registration reg = new Registration();
        reg.setType(IQ.Type.SET);
        reg.setTo(this.connection.getServiceName());
        reg.setRemove(true);
        PacketCollector collector = this.connection.createPacketCollector(new AndFilter(new PacketIDFilter(reg.getPacketID()), new PacketTypeFilter(IQ.class)));
        this.connection.sendPacket(reg);
        IQ result = (IQ) collector.nextResult((long) SmackConfiguration.getPacketReplyTimeout());
        collector.cancel();
        if (result == null) {
            throw new XMPPException("No response from server.");
        } else if (result.getType() == IQ.Type.ERROR) {
            throw new XMPPException(result.getError());
        }
    }

    private synchronized void getRegistrationInfo() throws XMPPException {
        Registration reg = new Registration();
        reg.setTo(this.connection.getServiceName());
        PacketCollector collector = this.connection.createPacketCollector(new AndFilter(new PacketIDFilter(reg.getPacketID()), new PacketTypeFilter(IQ.class)));
        this.connection.sendPacket(reg);
        IQ result = (IQ) collector.nextResult((long) SmackConfiguration.getPacketReplyTimeout());
        collector.cancel();
        if (result == null) {
            throw new XMPPException("No response from server.");
        } else if (result.getType() == IQ.Type.ERROR) {
            throw new XMPPException(result.getError());
        } else {
            this.info = (Registration) result;
        }
    }
}
