package org.jivesoftware.smack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.RosterPacket;
import org.jivesoftware.smack.util.StringUtils;

public class Roster {
    private static SubscriptionMode defaultSubscriptionMode = SubscriptionMode.accept_all;
    /* access modifiers changed from: private */
    public Connection connection;
    /* access modifiers changed from: private */
    public final Map<String, RosterEntry> entries;
    private final Map<String, RosterGroup> groups;
    /* access modifiers changed from: private */
    public RosterStorage persistentStorage;
    /* access modifiers changed from: private */
    public Map<String, Map<String, Presence>> presenceMap;
    private PresencePacketListener presencePacketListener;
    private String requestPacketId;
    boolean rosterInitialized;
    private final List<RosterListener> rosterListeners;
    /* access modifiers changed from: private */
    public SubscriptionMode subscriptionMode;
    private final List<RosterEntry> unfiledEntries;

    public enum SubscriptionMode {
        accept_all,
        reject_all,
        manual
    }

    public static SubscriptionMode getDefaultSubscriptionMode() {
        return defaultSubscriptionMode;
    }

    public static void setDefaultSubscriptionMode(SubscriptionMode subscriptionMode2) {
        defaultSubscriptionMode = subscriptionMode2;
    }

    Roster(Connection connection2, RosterStorage persistentStorage2) {
        this(connection2);
        this.persistentStorage = persistentStorage2;
    }

    Roster(Connection connection2) {
        this.rosterInitialized = false;
        this.subscriptionMode = getDefaultSubscriptionMode();
        this.connection = connection2;
        if (!connection2.getConfiguration().isRosterVersioningAvailable()) {
            this.persistentStorage = null;
        }
        this.groups = new ConcurrentHashMap();
        this.unfiledEntries = new CopyOnWriteArrayList();
        this.entries = new ConcurrentHashMap();
        this.rosterListeners = new CopyOnWriteArrayList();
        this.presenceMap = new ConcurrentHashMap();
        connection2.addPacketListener(new RosterPacketListener(this, null), new PacketTypeFilter(RosterPacket.class));
        PacketFilter presenceFilter = new PacketTypeFilter(Presence.class);
        this.presencePacketListener = new PresencePacketListener(this, null);
        connection2.addPacketListener(this.presencePacketListener, presenceFilter);
        connection2.addConnectionListener(new ConnectionListener() {
            public void connectionClosed() {
                Roster.this.setOfflinePresences();
            }

            public void connectionClosedOnError(Exception e) {
                Roster.this.setOfflinePresences();
            }

            public void reconnectingIn(int seconds) {
            }

            public void reconnectionFailed(Exception e) {
            }

            public void reconnectionSuccessful() {
            }
        });
    }

    public SubscriptionMode getSubscriptionMode() {
        return this.subscriptionMode;
    }

    public void setSubscriptionMode(SubscriptionMode subscriptionMode2) {
        this.subscriptionMode = subscriptionMode2;
    }

    public void reload() {
        RosterPacket packet = new RosterPacket();
        if (this.persistentStorage != null) {
            packet.setVersion(this.persistentStorage.getRosterVersion());
        }
        this.requestPacketId = packet.getPacketID();
        this.connection.addPacketListener(new RosterResultListener(this, null), new PacketIDFilter(this.requestPacketId));
        this.connection.sendPacket(packet);
    }

    public void addRosterListener(RosterListener rosterListener) {
        if (!this.rosterListeners.contains(rosterListener)) {
            this.rosterListeners.add(rosterListener);
        }
    }

    public void removeRosterListener(RosterListener rosterListener) {
        this.rosterListeners.remove(rosterListener);
    }

    public RosterGroup createGroup(String name) {
        if (this.groups.containsKey(name)) {
            throw new IllegalArgumentException("Group with name " + name + " alread exists.");
        }
        RosterGroup group = new RosterGroup(name, this.connection);
        this.groups.put(name, group);
        return group;
    }

    public void createEntry(String user, String name, String[] groups2) throws XMPPException {
        RosterPacket rosterPacket = new RosterPacket();
        rosterPacket.setType(IQ.Type.SET);
        RosterPacket.Item item = new RosterPacket.Item(user, name);
        if (groups2 != null) {
            for (String group : groups2) {
                if (group != null && group.trim().length() > 0) {
                    item.addGroupName(group);
                }
            }
        }
        rosterPacket.addRosterItem(item);
        PacketCollector collector = this.connection.createPacketCollector(new PacketIDFilter(rosterPacket.getPacketID()));
        this.connection.sendPacket(rosterPacket);
        IQ response = (IQ) collector.nextResult((long) SmackConfiguration.getPacketReplyTimeout());
        collector.cancel();
        if (response == null) {
            throw new XMPPException("No response from the server.");
        } else if (response.getType() == IQ.Type.ERROR) {
            throw new XMPPException(response.getError());
        } else {
            Presence presencePacket = new Presence(Presence.Type.subscribe);
            presencePacket.setTo(user);
            this.connection.sendPacket(presencePacket);
        }
    }

    private void insertRosterItems(List<RosterPacket.Item> items) {
        Collection<String> addedEntries = new ArrayList<>();
        Collection<String> updatedEntries = new ArrayList<>();
        Collection<String> deletedEntries = new ArrayList<>();
        for (RosterPacket.Item insertRosterItem : items) {
            insertRosterItem(insertRosterItem, addedEntries, updatedEntries, deletedEntries);
        }
        fireRosterChangedEvent(addedEntries, updatedEntries, deletedEntries);
    }

    /* access modifiers changed from: private */
    public void insertRosterItem(RosterPacket.Item item, Collection<String> addedEntries, Collection<String> updatedEntries, Collection<String> deletedEntries) {
        RosterEntry entry = new RosterEntry(item.getUser(), item.getName(), item.getItemType(), item.getItemStatus(), this, this.connection);
        if (RosterPacket.ItemType.remove.equals(item.getItemType())) {
            if (this.entries.containsKey(item.getUser())) {
                this.entries.remove(item.getUser());
            }
            if (this.unfiledEntries.contains(entry)) {
                this.unfiledEntries.remove(entry);
            }
            this.presenceMap.remove(String.valueOf(StringUtils.parseName(item.getUser())) + "@" + StringUtils.parseServer(item.getUser()));
            if (deletedEntries != null) {
                deletedEntries.add(item.getUser());
            }
        } else {
            if (!this.entries.containsKey(item.getUser())) {
                this.entries.put(item.getUser(), entry);
                if (addedEntries != null) {
                    addedEntries.add(item.getUser());
                }
            } else {
                this.entries.put(item.getUser(), entry);
                if (updatedEntries != null) {
                    updatedEntries.add(item.getUser());
                }
            }
            if (!item.getGroupNames().isEmpty()) {
                this.unfiledEntries.remove(entry);
            } else if (!this.unfiledEntries.contains(entry)) {
                this.unfiledEntries.add(entry);
            }
        }
        List<String> currentGroupNames = new ArrayList<>();
        for (RosterGroup group : getGroups()) {
            if (group.contains(entry)) {
                currentGroupNames.add(group.getName());
            }
        }
        if (!RosterPacket.ItemType.remove.equals(item.getItemType())) {
            List<String> newGroupNames = new ArrayList<>();
            for (String groupName : item.getGroupNames()) {
                newGroupNames.add(groupName);
                RosterGroup group2 = getGroup(groupName);
                if (group2 == null) {
                    group2 = createGroup(groupName);
                    this.groups.put(groupName, group2);
                }
                group2.addEntryLocal(entry);
            }
            for (String newGroupName : newGroupNames) {
                currentGroupNames.remove(newGroupName);
            }
        }
        for (String groupName2 : currentGroupNames) {
            RosterGroup group3 = getGroup(groupName2);
            group3.removeEntryLocal(entry);
            if (group3.getEntryCount() == 0) {
                this.groups.remove(groupName2);
            }
        }
        for (RosterGroup group4 : getGroups()) {
            if (group4.getEntryCount() == 0) {
                this.groups.remove(group4.getName());
            }
        }
    }

    public void removeEntry(RosterEntry entry) throws XMPPException {
        if (this.entries.containsKey(entry.getUser())) {
            RosterPacket packet = new RosterPacket();
            packet.setType(IQ.Type.SET);
            RosterPacket.Item item = RosterEntry.toRosterItem(entry);
            item.setItemType(RosterPacket.ItemType.remove);
            packet.addRosterItem(item);
            PacketCollector collector = this.connection.createPacketCollector(new PacketIDFilter(packet.getPacketID()));
            this.connection.sendPacket(packet);
            IQ response = (IQ) collector.nextResult((long) SmackConfiguration.getPacketReplyTimeout());
            collector.cancel();
            if (response == null) {
                throw new XMPPException("No response from the server.");
            } else if (response.getType() == IQ.Type.ERROR) {
                throw new XMPPException(response.getError());
            }
        }
    }

    public int getEntryCount() {
        return getEntries().size();
    }

    public Collection<RosterEntry> getEntries() {
        Set<RosterEntry> allEntries = new HashSet<>();
        for (RosterGroup rosterGroup : getGroups()) {
            allEntries.addAll(rosterGroup.getEntries());
        }
        allEntries.addAll(this.unfiledEntries);
        return Collections.unmodifiableCollection(allEntries);
    }

    public int getUnfiledEntryCount() {
        return this.unfiledEntries.size();
    }

    public Collection<RosterEntry> getUnfiledEntries() {
        return Collections.unmodifiableList(this.unfiledEntries);
    }

    public RosterEntry getEntry(String user) {
        if (user == null) {
            return null;
        }
        return this.entries.get(user.toLowerCase());
    }

    public boolean contains(String user) {
        return getEntry(user) != null;
    }

    public RosterGroup getGroup(String name) {
        return this.groups.get(name);
    }

    public int getGroupCount() {
        return this.groups.size();
    }

    public Collection<RosterGroup> getGroups() {
        return Collections.unmodifiableCollection(this.groups.values());
    }

    public Presence getPresence(String user) {
        Map<String, Presence> userPresences = this.presenceMap.get(getPresenceMapKey(StringUtils.parseBareAddress(user)));
        if (userPresences == null) {
            Presence presence = new Presence(Presence.Type.unavailable);
            presence.setFrom(user);
            return presence;
        }
        Presence presence2 = null;
        for (String resource : userPresences.keySet()) {
            Presence p = (Presence) userPresences.get(resource);
            if (p.isAvailable()) {
                if (presence2 == null || p.getPriority() > presence2.getPriority()) {
                    presence2 = p;
                } else if (p.getPriority() == presence2.getPriority()) {
                    Presence.Mode pMode = p.getMode();
                    if (pMode == null) {
                        pMode = Presence.Mode.available;
                    }
                    Presence.Mode presenceMode = presence2.getMode();
                    if (presenceMode == null) {
                        presenceMode = Presence.Mode.available;
                    }
                    if (pMode.compareTo((Enum) presenceMode) < 0) {
                        presence2 = p;
                    }
                }
            }
        }
        if (presence2 != null) {
            return presence2;
        }
        Presence presence3 = new Presence(Presence.Type.unavailable);
        presence3.setFrom(user);
        return presence3;
    }

    public Presence getPresenceResource(String userWithResource) {
        String key = getPresenceMapKey(userWithResource);
        String resource = StringUtils.parseResource(userWithResource);
        Map<String, Presence> userPresences = this.presenceMap.get(key);
        if (userPresences == null) {
            Presence presence = new Presence(Presence.Type.unavailable);
            presence.setFrom(userWithResource);
            return presence;
        }
        Presence presence2 = (Presence) userPresences.get(resource);
        if (presence2 != null) {
            return presence2;
        }
        Presence presence3 = new Presence(Presence.Type.unavailable);
        presence3.setFrom(userWithResource);
        return presence3;
    }

    public Iterator<Presence> getPresences(String user) {
        Map<String, Presence> userPresences = this.presenceMap.get(getPresenceMapKey(user));
        if (userPresences == null) {
            Presence presence = new Presence(Presence.Type.unavailable);
            presence.setFrom(user);
            return Arrays.asList(presence).iterator();
        }
        Collection<Presence> answer = new ArrayList<>();
        for (Presence presence2 : userPresences.values()) {
            if (presence2.isAvailable()) {
                answer.add(presence2);
            }
        }
        if (!answer.isEmpty()) {
            return answer.iterator();
        }
        Presence presence3 = new Presence(Presence.Type.unavailable);
        presence3.setFrom(user);
        return Arrays.asList(presence3).iterator();
    }

    /* access modifiers changed from: package-private */
    public void cleanup() {
        this.rosterListeners.clear();
    }

    /* access modifiers changed from: private */
    public String getPresenceMapKey(String user) {
        if (user == null) {
            return null;
        }
        String key = user;
        if (!contains(user)) {
            key = StringUtils.parseBareAddress(user);
        }
        return key.toLowerCase();
    }

    /* access modifiers changed from: private */
    public void setOfflinePresences() {
        for (String user : this.presenceMap.keySet()) {
            Map<String, Presence> resources = this.presenceMap.get(user);
            if (resources != null) {
                for (String resource : resources.keySet()) {
                    Presence packetUnavailable = new Presence(Presence.Type.unavailable);
                    packetUnavailable.setFrom(String.valueOf(user) + "/" + resource);
                    this.presencePacketListener.processPacket(packetUnavailable);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void fireRosterChangedEvent(Collection<String> addedEntries, Collection<String> updatedEntries, Collection<String> deletedEntries) {
        for (RosterListener listener : this.rosterListeners) {
            if (!addedEntries.isEmpty()) {
                listener.entriesAdded(addedEntries);
            }
            if (!updatedEntries.isEmpty()) {
                listener.entriesUpdated(updatedEntries);
            }
            if (!deletedEntries.isEmpty()) {
                listener.entriesDeleted(deletedEntries);
            }
        }
    }

    /* access modifiers changed from: private */
    public void fireRosterPresenceEvent(Presence presence) {
        for (RosterListener listener : this.rosterListeners) {
            listener.presenceChanged(presence);
        }
    }

    private class PresencePacketListener implements PacketListener {
        private PresencePacketListener() {
        }

        /* synthetic */ PresencePacketListener(Roster roster, PresencePacketListener presencePacketListener) {
            this();
        }

        public void processPacket(Packet packet) {
            Map<String, Presence> userPresences;
            Map<String, Presence> userPresences2;
            Map<String, Presence> userPresences3;
            Presence presence = (Presence) packet;
            String from = presence.getFrom();
            String key = Roster.this.getPresenceMapKey(from);
            if (presence.getType() == Presence.Type.available) {
                if (Roster.this.presenceMap.get(key) == null) {
                    userPresences3 = new ConcurrentHashMap<>();
                    Roster.this.presenceMap.put(key, userPresences3);
                } else {
                    userPresences3 = (Map) Roster.this.presenceMap.get(key);
                }
                userPresences3.remove(org.apache.commons.lang.StringUtils.EMPTY);
                userPresences3.put(StringUtils.parseResource(from), presence);
                if (((RosterEntry) Roster.this.entries.get(key)) != null) {
                    Roster.this.fireRosterPresenceEvent(presence);
                }
            } else if (presence.getType() == Presence.Type.unavailable) {
                if (org.apache.commons.lang.StringUtils.EMPTY.equals(StringUtils.parseResource(from))) {
                    if (Roster.this.presenceMap.get(key) == null) {
                        userPresences2 = new ConcurrentHashMap<>();
                        Roster.this.presenceMap.put(key, userPresences2);
                    } else {
                        userPresences2 = (Map) Roster.this.presenceMap.get(key);
                    }
                    userPresences2.put(org.apache.commons.lang.StringUtils.EMPTY, presence);
                } else if (Roster.this.presenceMap.get(key) != null) {
                    ((Map) Roster.this.presenceMap.get(key)).put(StringUtils.parseResource(from), presence);
                }
                if (((RosterEntry) Roster.this.entries.get(key)) != null) {
                    Roster.this.fireRosterPresenceEvent(presence);
                }
            } else if (presence.getType() == Presence.Type.subscribe) {
                if (Roster.this.subscriptionMode == SubscriptionMode.accept_all) {
                    Presence response = new Presence(Presence.Type.subscribed);
                    response.setTo(presence.getFrom());
                    Roster.this.connection.sendPacket(response);
                } else if (Roster.this.subscriptionMode == SubscriptionMode.reject_all) {
                    Presence response2 = new Presence(Presence.Type.unsubscribed);
                    response2.setTo(presence.getFrom());
                    Roster.this.connection.sendPacket(response2);
                }
            } else if (presence.getType() == Presence.Type.unsubscribe) {
                if (Roster.this.subscriptionMode != SubscriptionMode.manual) {
                    Presence response3 = new Presence(Presence.Type.unsubscribed);
                    response3.setTo(presence.getFrom());
                    Roster.this.connection.sendPacket(response3);
                }
            } else if (presence.getType() == Presence.Type.error && org.apache.commons.lang.StringUtils.EMPTY.equals(StringUtils.parseResource(from))) {
                if (!Roster.this.presenceMap.containsKey(key)) {
                    userPresences = new ConcurrentHashMap<>();
                    Roster.this.presenceMap.put(key, userPresences);
                } else {
                    userPresences = (Map) Roster.this.presenceMap.get(key);
                    userPresences.clear();
                }
                userPresences.put(org.apache.commons.lang.StringUtils.EMPTY, presence);
                if (((RosterEntry) Roster.this.entries.get(key)) != null) {
                    Roster.this.fireRosterPresenceEvent(presence);
                }
            }
        }
    }

    private class RosterResultListener implements PacketListener {
        private RosterResultListener() {
        }

        /* synthetic */ RosterResultListener(Roster roster, RosterResultListener rosterResultListener) {
            this();
        }

        public void processPacket(Packet packet) {
            if (packet instanceof IQ) {
                IQ result = (IQ) packet;
                if (result.getType().equals(IQ.Type.RESULT) && result.getExtensions().isEmpty()) {
                    Collection<String> addedEntries = new ArrayList<>();
                    Collection<String> updatedEntries = new ArrayList<>();
                    Collection<String> deletedEntries = new ArrayList<>();
                    if (Roster.this.persistentStorage != null) {
                        for (RosterPacket.Item item : Roster.this.persistentStorage.getEntries()) {
                            Roster.this.insertRosterItem(item, addedEntries, updatedEntries, deletedEntries);
                        }
                        synchronized (Roster.this) {
                            Roster.this.rosterInitialized = true;
                            Roster.this.notifyAll();
                        }
                        Roster.this.fireRosterChangedEvent(addedEntries, updatedEntries, deletedEntries);
                    }
                }
            }
            Roster.this.connection.removePacketListener(this);
        }
    }

    private class RosterPacketListener implements PacketListener {
        private RosterPacketListener() {
        }

        /* synthetic */ RosterPacketListener(Roster roster, RosterPacketListener rosterPacketListener) {
            this();
        }

        public void processPacket(Packet packet) {
            Collection<String> addedEntries = new ArrayList<>();
            Collection<String> updatedEntries = new ArrayList<>();
            Collection<String> deletedEntries = new ArrayList<>();
            String version = null;
            RosterPacket rosterPacket = (RosterPacket) packet;
            List<RosterPacket.Item> rosterItems = new ArrayList<>();
            for (RosterPacket.Item item : rosterPacket.getRosterItems()) {
                rosterItems.add(item);
            }
            if (rosterPacket.getVersion() == null) {
                Roster.this.persistentStorage = null;
            } else {
                version = rosterPacket.getVersion();
            }
            if (Roster.this.persistentStorage != null && !Roster.this.rosterInitialized) {
                for (RosterPacket.Item item2 : Roster.this.persistentStorage.getEntries()) {
                    rosterItems.add(item2);
                }
            }
            for (RosterPacket.Item item3 : rosterItems) {
                Roster.this.insertRosterItem(item3, addedEntries, updatedEntries, deletedEntries);
            }
            if (Roster.this.persistentStorage != null) {
                for (RosterPacket.Item i : rosterPacket.getRosterItems()) {
                    if (i.getItemType().equals(RosterPacket.ItemType.remove)) {
                        Roster.this.persistentStorage.removeEntry(i.getUser());
                    } else {
                        Roster.this.persistentStorage.addEntry(i, version);
                    }
                }
            }
            synchronized (Roster.this) {
                Roster.this.rosterInitialized = true;
                Roster.this.notifyAll();
            }
            Roster.this.fireRosterChangedEvent(addedEntries, updatedEntries, deletedEntries);
        }
    }
}
