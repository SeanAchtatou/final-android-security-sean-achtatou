package org.jivesoftware.smack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.RosterPacket;

public class RosterEntry {
    private final Connection connection;
    private String name;
    private final Roster roster;
    private RosterPacket.ItemStatus status;
    private RosterPacket.ItemType type;
    private String user;

    RosterEntry(String user2, String name2, RosterPacket.ItemType type2, RosterPacket.ItemStatus status2, Roster roster2, Connection connection2) {
        this.user = user2;
        this.name = name2;
        this.type = type2;
        this.status = status2;
        this.roster = roster2;
        this.connection = connection2;
    }

    public String getUser() {
        return this.user;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        if (name2 == null || !name2.equals(this.name)) {
            this.name = name2;
            RosterPacket packet = new RosterPacket();
            packet.setType(IQ.Type.SET);
            packet.addRosterItem(toRosterItem(this));
            this.connection.sendPacket(packet);
        }
    }

    /* access modifiers changed from: package-private */
    public void updateState(String name2, RosterPacket.ItemType type2, RosterPacket.ItemStatus status2) {
        this.name = name2;
        this.type = type2;
        this.status = status2;
    }

    public Collection<RosterGroup> getGroups() {
        List<RosterGroup> results = new ArrayList<>();
        for (RosterGroup group : this.roster.getGroups()) {
            if (group.contains(this)) {
                results.add(group);
            }
        }
        return Collections.unmodifiableCollection(results);
    }

    public RosterPacket.ItemType getType() {
        return this.type;
    }

    public RosterPacket.ItemStatus getStatus() {
        return this.status;
    }

    public String toString() {
        StringBuilder buf = new StringBuilder();
        if (this.name != null) {
            buf.append(this.name).append(": ");
        }
        buf.append(this.user);
        Collection<RosterGroup> groups = getGroups();
        if (!groups.isEmpty()) {
            buf.append(" [");
            Iterator<RosterGroup> iter = groups.iterator();
            buf.append(iter.next().getName());
            while (iter.hasNext()) {
                buf.append(", ");
                buf.append(iter.next().getName());
            }
            buf.append("]");
        }
        return buf.toString();
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || !(object instanceof RosterEntry)) {
            return false;
        }
        return this.user.equals(((RosterEntry) object).getUser());
    }

    static RosterPacket.Item toRosterItem(RosterEntry entry) {
        RosterPacket.Item item = new RosterPacket.Item(entry.getUser(), entry.getName());
        item.setItemType(entry.getType());
        item.setItemStatus(entry.getStatus());
        for (RosterGroup group : entry.getGroups()) {
            item.addGroupName(group.getName());
        }
        return item;
    }
}
