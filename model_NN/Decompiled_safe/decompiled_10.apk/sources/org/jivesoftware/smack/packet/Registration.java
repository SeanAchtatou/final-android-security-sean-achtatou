package org.jivesoftware.smack.packet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Registration extends IQ {
    private Map<String, String> attributes = new HashMap();
    private String instructions = null;
    private boolean registered = false;
    private boolean remove = false;
    private List<String> requiredFields = new ArrayList();

    public String getInstructions() {
        return this.instructions;
    }

    public void setInstructions(String instructions2) {
        this.instructions = instructions2;
    }

    public Map<String, String> getAttributes() {
        return this.attributes;
    }

    public void addRequiredField(String field) {
        this.requiredFields.add(field);
    }

    public List<String> getRequiredFields() {
        return this.requiredFields;
    }

    public void addAttribute(String key, String value) {
        this.attributes.put(key, value);
    }

    public void setRegistered(boolean registered2) {
        this.registered = registered2;
    }

    public boolean isRegistered() {
        return this.registered;
    }

    public String getField(String key) {
        return this.attributes.get(key);
    }

    public List<String> getFieldNames() {
        return new ArrayList(this.attributes.keySet());
    }

    public void setUsername(String username) {
        this.attributes.put("username", username);
    }

    public void setPassword(String password) {
        this.attributes.put("password", password);
    }

    public void setRemove(boolean remove2) {
        this.remove = remove2;
    }

    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<query xmlns=\"jabber:iq:register\">");
        if (this.instructions != null && !this.remove) {
            buf.append("<instructions>").append(this.instructions).append("</instructions>");
        }
        if (this.attributes != null && this.attributes.size() > 0 && !this.remove) {
            for (String name : this.attributes.keySet()) {
                buf.append("<").append(name).append(">");
                buf.append(this.attributes.get(name));
                buf.append("</").append(name).append(">");
            }
        } else if (this.remove) {
            buf.append("</remove>");
        }
        buf.append(getExtensionsXML());
        buf.append("</query>");
        return buf.toString();
    }
}
