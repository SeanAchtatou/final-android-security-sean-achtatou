package org.jivesoftware.smack.packet;

import org.jivesoftware.smack.packet.IQ;

public class Bind extends IQ {
    private String jid = null;
    private String resource = null;

    public Bind() {
        setType(IQ.Type.SET);
    }

    public String getResource() {
        return this.resource;
    }

    public void setResource(String resource2) {
        this.resource = resource2;
    }

    public String getJid() {
        return this.jid;
    }

    public void setJid(String jid2) {
        this.jid = jid2;
    }

    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<bind xmlns=\"urn:ietf:params:xml:ns:xmpp-bind\">");
        if (this.resource != null) {
            buf.append("<resource>").append(this.resource).append("</resource>");
        }
        if (this.jid != null) {
            buf.append("<jid>").append(this.jid).append("</jid>");
        }
        buf.append("</bind>");
        return buf.toString();
    }
}
