package org.jivesoftware.smack;

import java.io.PrintStream;
import java.io.PrintWriter;
import org.jivesoftware.smack.packet.StreamError;
import org.jivesoftware.smack.packet.XMPPError;

public class XMPPException extends Exception {
    private XMPPError error = null;
    private StreamError streamError = null;
    private Throwable wrappedThrowable = null;

    public XMPPException() {
    }

    public XMPPException(String message) {
        super(message);
    }

    public XMPPException(Throwable wrappedThrowable2) {
        this.wrappedThrowable = wrappedThrowable2;
    }

    public XMPPException(StreamError streamError2) {
        this.streamError = streamError2;
    }

    public XMPPException(XMPPError error2) {
        this.error = error2;
    }

    public XMPPException(String message, Throwable wrappedThrowable2) {
        super(message);
        this.wrappedThrowable = wrappedThrowable2;
    }

    public XMPPException(String message, XMPPError error2, Throwable wrappedThrowable2) {
        super(message);
        this.error = error2;
        this.wrappedThrowable = wrappedThrowable2;
    }

    public XMPPException(String message, XMPPError error2) {
        super(message);
        this.error = error2;
    }

    public XMPPError getXMPPError() {
        return this.error;
    }

    public StreamError getStreamError() {
        return this.streamError;
    }

    public Throwable getWrappedThrowable() {
        return this.wrappedThrowable;
    }

    public void printStackTrace() {
        printStackTrace(System.err);
    }

    public void printStackTrace(PrintStream out) {
        super.printStackTrace(out);
        if (this.wrappedThrowable != null) {
            out.println("Nested Exception: ");
            this.wrappedThrowable.printStackTrace(out);
        }
    }

    public void printStackTrace(PrintWriter out) {
        super.printStackTrace(out);
        if (this.wrappedThrowable != null) {
            out.println("Nested Exception: ");
            this.wrappedThrowable.printStackTrace(out);
        }
    }

    public String getMessage() {
        String msg = super.getMessage();
        if (msg == null && this.error != null) {
            return this.error.toString();
        }
        if (msg != null || this.streamError == null) {
            return msg;
        }
        return this.streamError.toString();
    }

    public String toString() {
        StringBuilder buf = new StringBuilder();
        String message = super.getMessage();
        if (message != null) {
            buf.append(message).append(": ");
        }
        if (this.error != null) {
            buf.append(this.error);
        }
        if (this.streamError != null) {
            buf.append(this.streamError);
        }
        if (this.wrappedThrowable != null) {
            buf.append("\n  -- caused by: ").append(this.wrappedThrowable);
        }
        return buf.toString();
    }
}
