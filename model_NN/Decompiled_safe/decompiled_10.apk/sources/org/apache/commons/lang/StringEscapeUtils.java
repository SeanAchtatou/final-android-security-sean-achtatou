package org.apache.commons.lang;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Locale;
import org.apache.commons.lang.exception.NestableRuntimeException;
import org.apache.james.mime4j.field.datetime.parser.DateTimeParserConstants;

public class StringEscapeUtils {
    private static final char CSV_DELIMITER = ',';
    private static final char CSV_QUOTE = '\"';
    private static final String CSV_QUOTE_STR = String.valueOf((char) CSV_QUOTE);
    private static final char[] CSV_SEARCH_CHARS = {CSV_DELIMITER, CSV_QUOTE, CharUtils.CR, 10};

    public static String escapeJava(String str) {
        return escapeJavaStyleString(str, false, false);
    }

    public static void escapeJava(Writer out, String str) throws IOException {
        escapeJavaStyleString(out, str, false, false);
    }

    public static String escapeJavaScript(String str) {
        return escapeJavaStyleString(str, true, true);
    }

    public static void escapeJavaScript(Writer out, String str) throws IOException {
        escapeJavaStyleString(out, str, true, true);
    }

    private static String escapeJavaStyleString(String str, boolean escapeSingleQuotes, boolean escapeForwardSlash) {
        if (str == null) {
            return null;
        }
        try {
            StringWriter writer = new StringWriter(str.length() * 2);
            escapeJavaStyleString(writer, str, escapeSingleQuotes, escapeForwardSlash);
            return writer.toString();
        } catch (IOException ioe) {
            throw new UnhandledException(ioe);
        }
    }

    private static void escapeJavaStyleString(Writer out, String str, boolean escapeSingleQuote, boolean escapeForwardSlash) throws IOException {
        if (out == null) {
            throw new IllegalArgumentException("The Writer must not be null");
        } else if (str != null) {
            int sz = str.length();
            for (int i = 0; i < sz; i++) {
                char ch = str.charAt(i);
                if (ch > 4095) {
                    out.write(new StringBuffer().append("\\u").append(hex(ch)).toString());
                } else if (ch > 255) {
                    out.write(new StringBuffer().append("\\u0").append(hex(ch)).toString());
                } else if (ch > 127) {
                    out.write(new StringBuffer().append("\\u00").append(hex(ch)).toString());
                } else if (ch < ' ') {
                    switch (ch) {
                        case 8:
                            out.write(92);
                            out.write(98);
                            continue;
                        case 9:
                            out.write(92);
                            out.write(116);
                            continue;
                        case 10:
                            out.write(92);
                            out.write(110);
                            continue;
                        case 11:
                        default:
                            if (ch <= 15) {
                                out.write(new StringBuffer().append("\\u000").append(hex(ch)).toString());
                                break;
                            } else {
                                out.write(new StringBuffer().append("\\u00").append(hex(ch)).toString());
                                continue;
                            }
                        case 12:
                            out.write(92);
                            out.write(102);
                            continue;
                        case 13:
                            out.write(92);
                            out.write(114);
                            continue;
                    }
                } else {
                    switch (ch) {
                        case '\"':
                            out.write(92);
                            out.write(34);
                            continue;
                        case '\'':
                            if (escapeSingleQuote) {
                                out.write(92);
                            }
                            out.write(39);
                            continue;
                        case DateTimeParserConstants.QUOTEDPAIR /*47*/:
                            if (escapeForwardSlash) {
                                out.write(92);
                            }
                            out.write(47);
                            continue;
                        case '\\':
                            out.write(92);
                            out.write(92);
                            continue;
                        default:
                            out.write(ch);
                            continue;
                    }
                }
            }
        }
    }

    private static String hex(char ch) {
        return Integer.toHexString(ch).toUpperCase(Locale.ENGLISH);
    }

    public static String unescapeJava(String str) {
        if (str == null) {
            return null;
        }
        try {
            StringWriter writer = new StringWriter(str.length());
            unescapeJava(writer, str);
            return writer.toString();
        } catch (IOException ioe) {
            throw new UnhandledException(ioe);
        }
    }

    public static void unescapeJava(Writer out, String str) throws IOException {
        if (out == null) {
            throw new IllegalArgumentException("The Writer must not be null");
        } else if (str != null) {
            int sz = str.length();
            StringBuffer unicode = new StringBuffer(4);
            boolean hadSlash = false;
            boolean inUnicode = false;
            for (int i = 0; i < sz; i++) {
                char ch = str.charAt(i);
                if (inUnicode) {
                    unicode.append(ch);
                    if (unicode.length() == 4) {
                        try {
                            out.write((char) Integer.parseInt(unicode.toString(), 16));
                            unicode.setLength(0);
                            inUnicode = false;
                            hadSlash = false;
                        } catch (NumberFormatException nfe) {
                            throw new NestableRuntimeException(new StringBuffer().append("Unable to parse unicode value: ").append((Object) unicode).toString(), nfe);
                        }
                    }
                } else if (hadSlash) {
                    hadSlash = false;
                    switch (ch) {
                        case '\"':
                            out.write(34);
                            continue;
                        case '\'':
                            out.write(39);
                            continue;
                        case '\\':
                            out.write(92);
                            continue;
                        case 'b':
                            out.write(8);
                            continue;
                        case 'f':
                            out.write(12);
                            continue;
                        case 'n':
                            out.write(10);
                            continue;
                        case 'r':
                            out.write(13);
                            continue;
                        case 't':
                            out.write(9);
                            continue;
                        case 'u':
                            inUnicode = true;
                            continue;
                        default:
                            out.write(ch);
                            continue;
                    }
                } else if (ch == '\\') {
                    hadSlash = true;
                } else {
                    out.write(ch);
                }
            }
            if (hadSlash) {
                out.write(92);
            }
        }
    }

    public static String unescapeJavaScript(String str) {
        return unescapeJava(str);
    }

    public static void unescapeJavaScript(Writer out, String str) throws IOException {
        unescapeJava(out, str);
    }

    public static String escapeHtml(String str) {
        if (str == null) {
            return null;
        }
        try {
            StringWriter writer = new StringWriter((int) (((double) str.length()) * 1.5d));
            escapeHtml(writer, str);
            return writer.toString();
        } catch (IOException ioe) {
            throw new UnhandledException(ioe);
        }
    }

    public static void escapeHtml(Writer writer, String string) throws IOException {
        if (writer == null) {
            throw new IllegalArgumentException("The Writer must not be null.");
        } else if (string != null) {
            Entities.HTML40.escape(writer, string);
        }
    }

    public static String unescapeHtml(String str) {
        if (str == null) {
            return null;
        }
        try {
            StringWriter writer = new StringWriter((int) (((double) str.length()) * 1.5d));
            unescapeHtml(writer, str);
            return writer.toString();
        } catch (IOException ioe) {
            throw new UnhandledException(ioe);
        }
    }

    public static void unescapeHtml(Writer writer, String string) throws IOException {
        if (writer == null) {
            throw new IllegalArgumentException("The Writer must not be null.");
        } else if (string != null) {
            Entities.HTML40.unescape(writer, string);
        }
    }

    public static void escapeXml(Writer writer, String str) throws IOException {
        if (writer == null) {
            throw new IllegalArgumentException("The Writer must not be null.");
        } else if (str != null) {
            Entities.XML.escape(writer, str);
        }
    }

    public static String escapeXml(String str) {
        if (str == null) {
            return null;
        }
        return Entities.XML.escape(str);
    }

    public static void unescapeXml(Writer writer, String str) throws IOException {
        if (writer == null) {
            throw new IllegalArgumentException("The Writer must not be null.");
        } else if (str != null) {
            Entities.XML.unescape(writer, str);
        }
    }

    public static String unescapeXml(String str) {
        if (str == null) {
            return null;
        }
        return Entities.XML.unescape(str);
    }

    public static String escapeSql(String str) {
        if (str == null) {
            return null;
        }
        return StringUtils.replace(str, "'", "''");
    }

    public static String escapeCsv(String str) {
        if (StringUtils.containsNone(str, CSV_SEARCH_CHARS)) {
            return str;
        }
        try {
            StringWriter writer = new StringWriter();
            escapeCsv(writer, str);
            return writer.toString();
        } catch (IOException ioe) {
            throw new UnhandledException(ioe);
        }
    }

    public static void escapeCsv(Writer out, String str) throws IOException {
        if (!StringUtils.containsNone(str, CSV_SEARCH_CHARS)) {
            out.write(34);
            for (int i = 0; i < str.length(); i++) {
                char c = str.charAt(i);
                if (c == '\"') {
                    out.write(34);
                }
                out.write(c);
            }
            out.write(34);
        } else if (str != null) {
            out.write(str);
        }
    }

    public static String unescapeCsv(String str) {
        if (str == null) {
            return null;
        }
        try {
            StringWriter writer = new StringWriter();
            unescapeCsv(writer, str);
            return writer.toString();
        } catch (IOException ioe) {
            throw new UnhandledException(ioe);
        }
    }

    public static void unescapeCsv(Writer out, String str) throws IOException {
        if (str != null) {
            if (str.length() < 2) {
                out.write(str);
            } else if (str.charAt(0) == '\"' && str.charAt(str.length() - 1) == '\"') {
                String quoteless = str.substring(1, str.length() - 1);
                if (StringUtils.containsAny(quoteless, CSV_SEARCH_CHARS)) {
                    str = StringUtils.replace(quoteless, new StringBuffer().append(CSV_QUOTE_STR).append(CSV_QUOTE_STR).toString(), CSV_QUOTE_STR);
                }
                out.write(str);
            } else {
                out.write(str);
            }
        }
    }
}
