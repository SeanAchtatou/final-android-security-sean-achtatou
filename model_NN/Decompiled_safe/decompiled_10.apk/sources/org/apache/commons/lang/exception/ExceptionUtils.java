package org.apache.commons.lang.exception;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;

public class ExceptionUtils {
    private static String[] CAUSE_METHOD_NAMES = {"getCause", "getNextException", "getTargetException", "getException", "getSourceException", "getRootCause", "getCausedByException", "getNested", "getLinkedException", "getNestedException", "getLinkedCause", "getThrowable"};
    private static final Object CAUSE_METHOD_NAMES_LOCK = new Object();
    private static final Method THROWABLE_CAUSE_METHOD;
    private static final Method THROWABLE_INITCAUSE_METHOD;
    static final String WRAPPED_MARKER = " [wrapped] ";
    static Class class$java$lang$Throwable;

    static {
        Method causeMethod;
        Method causeMethod2;
        Class cls;
        Class cls2;
        Class cls3;
        try {
            if (class$java$lang$Throwable == null) {
                cls3 = class$("java.lang.Throwable");
                class$java$lang$Throwable = cls3;
            } else {
                cls3 = class$java$lang$Throwable;
            }
            causeMethod = cls3.getMethod("getCause", null);
        } catch (Exception e) {
            causeMethod = null;
        }
        THROWABLE_CAUSE_METHOD = causeMethod;
        try {
            if (class$java$lang$Throwable == null) {
                Class class$ = class$("java.lang.Throwable");
                class$java$lang$Throwable = class$;
                cls = class$;
            } else {
                cls = class$java$lang$Throwable;
            }
            Class[] clsArr = new Class[1];
            if (class$java$lang$Throwable == null) {
                cls2 = class$("java.lang.Throwable");
                class$java$lang$Throwable = cls2;
            } else {
                cls2 = class$java$lang$Throwable;
            }
            clsArr[0] = cls2;
            causeMethod2 = cls.getMethod("initCause", clsArr);
        } catch (Exception e2) {
            causeMethod2 = null;
        }
        THROWABLE_INITCAUSE_METHOD = causeMethod2;
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError(x1.getMessage());
        }
    }

    public static void addCauseMethodName(String methodName) {
        if (StringUtils.isNotEmpty(methodName) && !isCauseMethodName(methodName)) {
            List list = getCauseMethodNameList();
            if (list.add(methodName)) {
                synchronized (CAUSE_METHOD_NAMES_LOCK) {
                    CAUSE_METHOD_NAMES = toArray(list);
                }
            }
        }
    }

    public static void removeCauseMethodName(String methodName) {
        if (StringUtils.isNotEmpty(methodName)) {
            List list = getCauseMethodNameList();
            if (list.remove(methodName)) {
                synchronized (CAUSE_METHOD_NAMES_LOCK) {
                    CAUSE_METHOD_NAMES = toArray(list);
                }
            }
        }
    }

    public static boolean setCause(Throwable target, Throwable cause) {
        Class cls;
        if (target == null) {
            throw new NullArgumentException("target");
        }
        Object[] causeArgs = {cause};
        boolean modifiedTarget = false;
        if (THROWABLE_INITCAUSE_METHOD != null) {
            try {
                THROWABLE_INITCAUSE_METHOD.invoke(target, causeArgs);
                modifiedTarget = true;
            } catch (IllegalAccessException | InvocationTargetException e) {
            }
        }
        try {
            Class<?> cls2 = target.getClass();
            Class[] clsArr = new Class[1];
            if (class$java$lang$Throwable == null) {
                cls = class$("java.lang.Throwable");
                class$java$lang$Throwable = cls;
            } else {
                cls = class$java$lang$Throwable;
            }
            clsArr[0] = cls;
            cls2.getMethod("setCause", clsArr).invoke(target, causeArgs);
            return true;
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e2) {
            return modifiedTarget;
        }
    }

    private static String[] toArray(List list) {
        return (String[]) list.toArray(new String[list.size()]);
    }

    private static ArrayList getCauseMethodNameList() {
        ArrayList arrayList;
        synchronized (CAUSE_METHOD_NAMES_LOCK) {
            arrayList = new ArrayList(Arrays.asList(CAUSE_METHOD_NAMES));
        }
        return arrayList;
    }

    public static boolean isCauseMethodName(String methodName) {
        boolean z;
        synchronized (CAUSE_METHOD_NAMES_LOCK) {
            z = ArrayUtils.indexOf(CAUSE_METHOD_NAMES, methodName) >= 0;
        }
        return z;
    }

    public static Throwable getCause(Throwable throwable) {
        Throwable cause;
        synchronized (CAUSE_METHOD_NAMES_LOCK) {
            cause = getCause(throwable, CAUSE_METHOD_NAMES);
        }
        return cause;
    }

    public static Throwable getCause(Throwable throwable, String[] methodNames) {
        if (throwable == null) {
            return null;
        }
        Throwable cause = getCauseUsingWellKnownTypes(throwable);
        if (cause != null) {
            return cause;
        }
        if (methodNames == null) {
            synchronized (CAUSE_METHOD_NAMES_LOCK) {
                methodNames = CAUSE_METHOD_NAMES;
            }
        }
        int i = 0;
        while (i < methodNames.length && ((methodName = methodNames[i]) == null || (cause = getCauseUsingMethodName(throwable, methodName)) == null)) {
            i++;
        }
        if (cause == null) {
            return getCauseUsingFieldName(throwable, "detail");
        }
        return cause;
    }

    public static Throwable getRootCause(Throwable throwable) {
        List list = getThrowableList(throwable);
        if (list.size() < 2) {
            return null;
        }
        return (Throwable) list.get(list.size() - 1);
    }

    private static Throwable getCauseUsingWellKnownTypes(Throwable throwable) {
        if (throwable instanceof Nestable) {
            return ((Nestable) throwable).getCause();
        }
        if (throwable instanceof SQLException) {
            return ((SQLException) throwable).getNextException();
        }
        if (throwable instanceof InvocationTargetException) {
            return ((InvocationTargetException) throwable).getTargetException();
        }
        return null;
    }

    private static Throwable getCauseUsingMethodName(Throwable throwable, String methodName) {
        Class cls;
        Method method = null;
        try {
            method = throwable.getClass().getMethod(methodName, null);
        } catch (NoSuchMethodException | SecurityException e) {
        }
        if (method != null) {
            if (class$java$lang$Throwable == null) {
                cls = class$("java.lang.Throwable");
                class$java$lang$Throwable = cls;
            } else {
                cls = class$java$lang$Throwable;
            }
            if (cls.isAssignableFrom(method.getReturnType())) {
                try {
                    return (Throwable) method.invoke(throwable, ArrayUtils.EMPTY_OBJECT_ARRAY);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
                }
            }
        }
        return null;
    }

    private static Throwable getCauseUsingFieldName(Throwable throwable, String fieldName) {
        Class cls;
        Field field = null;
        try {
            field = throwable.getClass().getField(fieldName);
        } catch (NoSuchFieldException | SecurityException e) {
        }
        if (field != null) {
            if (class$java$lang$Throwable == null) {
                cls = class$("java.lang.Throwable");
                class$java$lang$Throwable = cls;
            } else {
                cls = class$java$lang$Throwable;
            }
            if (cls.isAssignableFrom(field.getType())) {
                try {
                    return (Throwable) field.get(throwable);
                } catch (IllegalAccessException | IllegalArgumentException e2) {
                }
            }
        }
        return null;
    }

    public static boolean isThrowableNested() {
        return THROWABLE_CAUSE_METHOD != null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x005e, code lost:
        if (r0.getField("detail") == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean isNestedThrowable(java.lang.Throwable r10) {
        /*
            r5 = 0
            r6 = 1
            if (r10 != 0) goto L_0x0005
        L_0x0004:
            return r5
        L_0x0005:
            boolean r7 = r10 instanceof org.apache.commons.lang.exception.Nestable
            if (r7 == 0) goto L_0x000b
            r5 = r6
            goto L_0x0004
        L_0x000b:
            boolean r7 = r10 instanceof java.sql.SQLException
            if (r7 == 0) goto L_0x0011
            r5 = r6
            goto L_0x0004
        L_0x0011:
            boolean r7 = r10 instanceof java.lang.reflect.InvocationTargetException
            if (r7 == 0) goto L_0x0017
            r5 = r6
            goto L_0x0004
        L_0x0017:
            boolean r7 = isThrowableNested()
            if (r7 == 0) goto L_0x001f
            r5 = r6
            goto L_0x0004
        L_0x001f:
            java.lang.Class r0 = r10.getClass()
            java.lang.Object r8 = org.apache.commons.lang.exception.ExceptionUtils.CAUSE_METHOD_NAMES_LOCK
            monitor-enter(r8)
            r2 = 0
            java.lang.String[] r7 = org.apache.commons.lang.exception.ExceptionUtils.CAUSE_METHOD_NAMES     // Catch:{ all -> 0x0062 }
            int r3 = r7.length     // Catch:{ all -> 0x0062 }
        L_0x002a:
            if (r2 >= r3) goto L_0x0057
            java.lang.String[] r7 = org.apache.commons.lang.exception.ExceptionUtils.CAUSE_METHOD_NAMES     // Catch:{ NoSuchMethodException -> 0x0069, SecurityException -> 0x0053 }
            r7 = r7[r2]     // Catch:{ NoSuchMethodException -> 0x0069, SecurityException -> 0x0053 }
            r9 = 0
            java.lang.reflect.Method r4 = r0.getMethod(r7, r9)     // Catch:{ NoSuchMethodException -> 0x0069, SecurityException -> 0x0053 }
            if (r4 == 0) goto L_0x0054
            java.lang.Class r7 = org.apache.commons.lang.exception.ExceptionUtils.class$java$lang$Throwable     // Catch:{ NoSuchMethodException -> 0x0069, SecurityException -> 0x0053 }
            if (r7 != 0) goto L_0x0050
            java.lang.String r7 = "java.lang.Throwable"
            java.lang.Class r7 = class$(r7)     // Catch:{ NoSuchMethodException -> 0x0069, SecurityException -> 0x0053 }
            org.apache.commons.lang.exception.ExceptionUtils.class$java$lang$Throwable = r7     // Catch:{ NoSuchMethodException -> 0x0069, SecurityException -> 0x0053 }
        L_0x0043:
            java.lang.Class r9 = r4.getReturnType()     // Catch:{ NoSuchMethodException -> 0x0069, SecurityException -> 0x0053 }
            boolean r7 = r7.isAssignableFrom(r9)     // Catch:{ NoSuchMethodException -> 0x0069, SecurityException -> 0x0053 }
            if (r7 == 0) goto L_0x0054
            monitor-exit(r8)     // Catch:{ all -> 0x0062 }
            r5 = r6
            goto L_0x0004
        L_0x0050:
            java.lang.Class r7 = org.apache.commons.lang.exception.ExceptionUtils.class$java$lang$Throwable     // Catch:{ NoSuchMethodException -> 0x0069, SecurityException -> 0x0053 }
            goto L_0x0043
        L_0x0053:
            r7 = move-exception
        L_0x0054:
            int r2 = r2 + 1
            goto L_0x002a
        L_0x0057:
            monitor-exit(r8)     // Catch:{ all -> 0x0062 }
            java.lang.String r7 = "detail"
            java.lang.reflect.Field r1 = r0.getField(r7)     // Catch:{ NoSuchFieldException -> 0x0067, SecurityException -> 0x0065 }
            if (r1 == 0) goto L_0x0004
            r5 = r6
            goto L_0x0004
        L_0x0062:
            r5 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0062 }
            throw r5
        L_0x0065:
            r6 = move-exception
            goto L_0x0004
        L_0x0067:
            r6 = move-exception
            goto L_0x0004
        L_0x0069:
            r7 = move-exception
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang.exception.ExceptionUtils.isNestedThrowable(java.lang.Throwable):boolean");
    }

    public static int getThrowableCount(Throwable throwable) {
        return getThrowableList(throwable).size();
    }

    public static Throwable[] getThrowables(Throwable throwable) {
        List list = getThrowableList(throwable);
        return (Throwable[]) list.toArray(new Throwable[list.size()]);
    }

    public static List getThrowableList(Throwable throwable) {
        List list = new ArrayList();
        while (throwable != null && !list.contains(throwable)) {
            list.add(throwable);
            throwable = getCause(throwable);
        }
        return list;
    }

    public static int indexOfThrowable(Throwable throwable, Class clazz) {
        return indexOf(throwable, clazz, 0, false);
    }

    public static int indexOfThrowable(Throwable throwable, Class clazz, int fromIndex) {
        return indexOf(throwable, clazz, fromIndex, false);
    }

    public static int indexOfType(Throwable throwable, Class type) {
        return indexOf(throwable, type, 0, true);
    }

    public static int indexOfType(Throwable throwable, Class type, int fromIndex) {
        return indexOf(throwable, type, fromIndex, true);
    }

    private static int indexOf(Throwable throwable, Class type, int fromIndex, boolean subclass) {
        if (throwable == null || type == null) {
            return -1;
        }
        if (fromIndex < 0) {
            fromIndex = 0;
        }
        Throwable[] throwables = getThrowables(throwable);
        if (fromIndex >= throwables.length) {
            return -1;
        }
        if (subclass) {
            for (int i = fromIndex; i < throwables.length; i++) {
                if (type.isAssignableFrom(throwables[i].getClass())) {
                    return i;
                }
            }
        } else {
            for (int i2 = fromIndex; i2 < throwables.length; i2++) {
                if (type.equals(throwables[i2].getClass())) {
                    return i2;
                }
            }
        }
        return -1;
    }

    public static void printRootCauseStackTrace(Throwable throwable) {
        printRootCauseStackTrace(throwable, System.err);
    }

    public static void printRootCauseStackTrace(Throwable throwable, PrintStream stream) {
        if (throwable != null) {
            if (stream == null) {
                throw new IllegalArgumentException("The PrintStream must not be null");
            }
            String[] trace = getRootCauseStackTrace(throwable);
            for (String println : trace) {
                stream.println(println);
            }
            stream.flush();
        }
    }

    public static void printRootCauseStackTrace(Throwable throwable, PrintWriter writer) {
        if (throwable != null) {
            if (writer == null) {
                throw new IllegalArgumentException("The PrintWriter must not be null");
            }
            String[] trace = getRootCauseStackTrace(throwable);
            for (String println : trace) {
                writer.println(println);
            }
            writer.flush();
        }
    }

    public static String[] getRootCauseStackTrace(Throwable throwable) {
        if (throwable == null) {
            return ArrayUtils.EMPTY_STRING_ARRAY;
        }
        Throwable[] throwables = getThrowables(throwable);
        int count = throwables.length;
        ArrayList frames = new ArrayList();
        List nextTrace = getStackFrameList(throwables[count - 1]);
        int i = count;
        while (true) {
            i--;
            if (i < 0) {
                return (String[]) frames.toArray(new String[0]);
            }
            List trace = nextTrace;
            if (i != 0) {
                nextTrace = getStackFrameList(throwables[i - 1]);
                removeCommonFrames(trace, nextTrace);
            }
            if (i == count - 1) {
                frames.add(throwables[i].toString());
            } else {
                frames.add(new StringBuffer().append(WRAPPED_MARKER).append(throwables[i].toString()).toString());
            }
            for (int j = 0; j < trace.size(); j++) {
                frames.add(trace.get(j));
            }
        }
    }

    public static void removeCommonFrames(List causeFrames, List wrapperFrames) {
        if (causeFrames == null || wrapperFrames == null) {
            throw new IllegalArgumentException("The List must not be null");
        }
        int causeFrameIndex = causeFrames.size() - 1;
        int wrapperFrameIndex = wrapperFrames.size() - 1;
        while (causeFrameIndex >= 0 && wrapperFrameIndex >= 0) {
            if (((String) causeFrames.get(causeFrameIndex)).equals((String) wrapperFrames.get(wrapperFrameIndex))) {
                causeFrames.remove(causeFrameIndex);
            }
            causeFrameIndex--;
            wrapperFrameIndex--;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.PrintWriter.<init>(java.io.Writer, boolean):void}
     arg types: [java.io.StringWriter, int]
     candidates:
      ClspMth{java.io.PrintWriter.<init>(java.io.File, java.lang.String):void throws java.io.FileNotFoundException, java.io.UnsupportedEncodingException}
      ClspMth{java.io.PrintWriter.<init>(java.io.OutputStream, boolean):void}
      ClspMth{java.io.PrintWriter.<init>(java.lang.String, java.lang.String):void throws java.io.FileNotFoundException, java.io.UnsupportedEncodingException}
      ClspMth{java.io.PrintWriter.<init>(java.io.Writer, boolean):void} */
    public static String getFullStackTrace(Throwable throwable) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter((Writer) sw, true);
        Throwable[] ts = getThrowables(throwable);
        for (int i = 0; i < ts.length; i++) {
            ts[i].printStackTrace(pw);
            if (isNestedThrowable(ts[i])) {
                break;
            }
        }
        return sw.getBuffer().toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.PrintWriter.<init>(java.io.Writer, boolean):void}
     arg types: [java.io.StringWriter, int]
     candidates:
      ClspMth{java.io.PrintWriter.<init>(java.io.File, java.lang.String):void throws java.io.FileNotFoundException, java.io.UnsupportedEncodingException}
      ClspMth{java.io.PrintWriter.<init>(java.io.OutputStream, boolean):void}
      ClspMth{java.io.PrintWriter.<init>(java.lang.String, java.lang.String):void throws java.io.FileNotFoundException, java.io.UnsupportedEncodingException}
      ClspMth{java.io.PrintWriter.<init>(java.io.Writer, boolean):void} */
    public static String getStackTrace(Throwable throwable) {
        StringWriter sw = new StringWriter();
        throwable.printStackTrace(new PrintWriter((Writer) sw, true));
        return sw.getBuffer().toString();
    }

    public static String[] getStackFrames(Throwable throwable) {
        if (throwable == null) {
            return ArrayUtils.EMPTY_STRING_ARRAY;
        }
        return getStackFrames(getStackTrace(throwable));
    }

    static String[] getStackFrames(String stackTrace) {
        StringTokenizer frames = new StringTokenizer(stackTrace, SystemUtils.LINE_SEPARATOR);
        List list = new ArrayList();
        while (frames.hasMoreTokens()) {
            list.add(frames.nextToken());
        }
        return toArray(list);
    }

    static List getStackFrameList(Throwable t) {
        StringTokenizer frames = new StringTokenizer(getStackTrace(t), SystemUtils.LINE_SEPARATOR);
        List list = new ArrayList();
        boolean traceStarted = false;
        while (frames.hasMoreTokens()) {
            String token = frames.nextToken();
            int at = token.indexOf("at");
            if (at != -1 && token.substring(0, at).trim().length() == 0) {
                traceStarted = true;
                list.add(token);
            } else if (traceStarted) {
                break;
            }
        }
        return list;
    }

    public static String getMessage(Throwable th) {
        if (th == null) {
            return StringUtils.EMPTY;
        }
        return new StringBuffer().append(ClassUtils.getShortClassName(th, null)).append(": ").append(StringUtils.defaultString(th.getMessage())).toString();
    }

    public static String getRootCauseMessage(Throwable th) {
        Throwable root = getRootCause(th);
        if (root == null) {
            root = th;
        }
        return getMessage(root);
    }
}
