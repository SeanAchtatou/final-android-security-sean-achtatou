package org.apache.commons.lang.text;

import android.support.v4.view.accessibility.AccessibilityEventCompat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StrSubstitutor {
    public static final char DEFAULT_ESCAPE = '$';
    public static final StrMatcher DEFAULT_PREFIX = StrMatcher.stringMatcher("${");
    public static final StrMatcher DEFAULT_SUFFIX = StrMatcher.stringMatcher("}");
    private char escapeChar;
    private StrMatcher prefixMatcher;
    private StrMatcher suffixMatcher;
    private StrLookup variableResolver;

    public static String replace(Object source, Map valueMap) {
        return new StrSubstitutor(valueMap).replace(source);
    }

    public static String replace(Object source, Map valueMap, String prefix, String suffix) {
        return new StrSubstitutor(valueMap, prefix, suffix).replace(source);
    }

    public static String replaceSystemProperties(Object source) {
        return new StrSubstitutor(StrLookup.systemPropertiesLookup()).replace(source);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.lang.text.StrSubstitutor.<init>(org.apache.commons.lang.text.StrLookup, org.apache.commons.lang.text.StrMatcher, org.apache.commons.lang.text.StrMatcher, char):void
     arg types: [?[OBJECT, ARRAY], org.apache.commons.lang.text.StrMatcher, org.apache.commons.lang.text.StrMatcher, int]
     candidates:
      org.apache.commons.lang.text.StrSubstitutor.<init>(java.util.Map, java.lang.String, java.lang.String, char):void
      org.apache.commons.lang.text.StrSubstitutor.<init>(org.apache.commons.lang.text.StrLookup, java.lang.String, java.lang.String, char):void
      org.apache.commons.lang.text.StrSubstitutor.<init>(org.apache.commons.lang.text.StrLookup, org.apache.commons.lang.text.StrMatcher, org.apache.commons.lang.text.StrMatcher, char):void */
    public StrSubstitutor() {
        this((StrLookup) null, DEFAULT_PREFIX, DEFAULT_SUFFIX, '$');
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.lang.text.StrSubstitutor.<init>(org.apache.commons.lang.text.StrLookup, org.apache.commons.lang.text.StrMatcher, org.apache.commons.lang.text.StrMatcher, char):void
     arg types: [org.apache.commons.lang.text.StrLookup, org.apache.commons.lang.text.StrMatcher, org.apache.commons.lang.text.StrMatcher, int]
     candidates:
      org.apache.commons.lang.text.StrSubstitutor.<init>(java.util.Map, java.lang.String, java.lang.String, char):void
      org.apache.commons.lang.text.StrSubstitutor.<init>(org.apache.commons.lang.text.StrLookup, java.lang.String, java.lang.String, char):void
      org.apache.commons.lang.text.StrSubstitutor.<init>(org.apache.commons.lang.text.StrLookup, org.apache.commons.lang.text.StrMatcher, org.apache.commons.lang.text.StrMatcher, char):void */
    public StrSubstitutor(Map valueMap) {
        this(StrLookup.mapLookup(valueMap), DEFAULT_PREFIX, DEFAULT_SUFFIX, '$');
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.lang.text.StrSubstitutor.<init>(org.apache.commons.lang.text.StrLookup, java.lang.String, java.lang.String, char):void
     arg types: [org.apache.commons.lang.text.StrLookup, java.lang.String, java.lang.String, int]
     candidates:
      org.apache.commons.lang.text.StrSubstitutor.<init>(java.util.Map, java.lang.String, java.lang.String, char):void
      org.apache.commons.lang.text.StrSubstitutor.<init>(org.apache.commons.lang.text.StrLookup, org.apache.commons.lang.text.StrMatcher, org.apache.commons.lang.text.StrMatcher, char):void
      org.apache.commons.lang.text.StrSubstitutor.<init>(org.apache.commons.lang.text.StrLookup, java.lang.String, java.lang.String, char):void */
    public StrSubstitutor(Map valueMap, String prefix, String suffix) {
        this(StrLookup.mapLookup(valueMap), prefix, suffix, '$');
    }

    public StrSubstitutor(Map valueMap, String prefix, String suffix, char escape) {
        this(StrLookup.mapLookup(valueMap), prefix, suffix, escape);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.lang.text.StrSubstitutor.<init>(org.apache.commons.lang.text.StrLookup, org.apache.commons.lang.text.StrMatcher, org.apache.commons.lang.text.StrMatcher, char):void
     arg types: [org.apache.commons.lang.text.StrLookup, org.apache.commons.lang.text.StrMatcher, org.apache.commons.lang.text.StrMatcher, int]
     candidates:
      org.apache.commons.lang.text.StrSubstitutor.<init>(java.util.Map, java.lang.String, java.lang.String, char):void
      org.apache.commons.lang.text.StrSubstitutor.<init>(org.apache.commons.lang.text.StrLookup, java.lang.String, java.lang.String, char):void
      org.apache.commons.lang.text.StrSubstitutor.<init>(org.apache.commons.lang.text.StrLookup, org.apache.commons.lang.text.StrMatcher, org.apache.commons.lang.text.StrMatcher, char):void */
    public StrSubstitutor(StrLookup variableResolver2) {
        this(variableResolver2, DEFAULT_PREFIX, DEFAULT_SUFFIX, '$');
    }

    public StrSubstitutor(StrLookup variableResolver2, String prefix, String suffix, char escape) {
        setVariableResolver(variableResolver2);
        setVariablePrefix(prefix);
        setVariableSuffix(suffix);
        setEscapeChar(escape);
    }

    public StrSubstitutor(StrLookup variableResolver2, StrMatcher prefixMatcher2, StrMatcher suffixMatcher2, char escape) {
        setVariableResolver(variableResolver2);
        setVariablePrefixMatcher(prefixMatcher2);
        setVariableSuffixMatcher(suffixMatcher2);
        setEscapeChar(escape);
    }

    public String replace(String source) {
        if (source == null) {
            return null;
        }
        StrBuilder buf = new StrBuilder(source);
        return substitute(buf, 0, source.length()) ? buf.toString() : source;
    }

    public String replace(String source, int offset, int length) {
        if (source == null) {
            return null;
        }
        StrBuilder buf = new StrBuilder(length).append(source, offset, length);
        if (!substitute(buf, 0, length)) {
            return source.substring(offset, offset + length);
        }
        return buf.toString();
    }

    public String replace(char[] source) {
        if (source == null) {
            return null;
        }
        StrBuilder buf = new StrBuilder(source.length).append(source);
        substitute(buf, 0, source.length);
        return buf.toString();
    }

    public String replace(char[] source, int offset, int length) {
        if (source == null) {
            return null;
        }
        StrBuilder buf = new StrBuilder(length).append(source, offset, length);
        substitute(buf, 0, length);
        return buf.toString();
    }

    public String replace(StringBuffer source) {
        if (source == null) {
            return null;
        }
        StrBuilder buf = new StrBuilder(source.length()).append(source);
        substitute(buf, 0, buf.length());
        return buf.toString();
    }

    public String replace(StringBuffer source, int offset, int length) {
        if (source == null) {
            return null;
        }
        StrBuilder buf = new StrBuilder(length).append(source, offset, length);
        substitute(buf, 0, length);
        return buf.toString();
    }

    public String replace(StrBuilder source) {
        if (source == null) {
            return null;
        }
        StrBuilder buf = new StrBuilder(source.length()).append(source);
        substitute(buf, 0, buf.length());
        return buf.toString();
    }

    public String replace(StrBuilder source, int offset, int length) {
        if (source == null) {
            return null;
        }
        StrBuilder buf = new StrBuilder(length).append(source, offset, length);
        substitute(buf, 0, length);
        return buf.toString();
    }

    public String replace(Object source) {
        if (source == null) {
            return null;
        }
        StrBuilder buf = new StrBuilder().append(source);
        substitute(buf, 0, buf.length());
        return buf.toString();
    }

    public boolean replaceIn(StringBuffer source) {
        if (source == null) {
            return false;
        }
        return replaceIn(source, 0, source.length());
    }

    public boolean replaceIn(StringBuffer source, int offset, int length) {
        if (source == null) {
            return false;
        }
        StrBuilder buf = new StrBuilder(length).append(source, offset, length);
        if (!substitute(buf, 0, length)) {
            return false;
        }
        source.replace(offset, offset + length, buf.toString());
        return true;
    }

    public boolean replaceIn(StrBuilder source) {
        if (source == null) {
            return false;
        }
        return substitute(source, 0, source.length());
    }

    public boolean replaceIn(StrBuilder source, int offset, int length) {
        if (source == null) {
            return false;
        }
        return substitute(source, offset, length);
    }

    /* access modifiers changed from: protected */
    public boolean substitute(StrBuilder buf, int offset, int length) {
        return substitute(buf, offset, length, null) > 0;
    }

    private int substitute(StrBuilder buf, int offset, int length, List priorVariables) {
        StrMatcher prefixMatcher2 = getVariablePrefixMatcher();
        StrMatcher suffixMatcher2 = getVariableSuffixMatcher();
        char escape = getEscapeChar();
        boolean top = priorVariables == null;
        boolean altered = false;
        int lengthChange = 0;
        char[] chars = buf.buffer;
        int bufEnd = offset + length;
        int pos = offset;
        while (pos < bufEnd) {
            int startMatchLen = prefixMatcher2.isMatch(chars, pos, offset, bufEnd);
            if (startMatchLen == 0) {
                pos++;
            } else if (pos <= offset || chars[pos - 1] != escape) {
                int startPos = pos;
                int pos2 = pos + startMatchLen;
                while (true) {
                    if (pos >= bufEnd) {
                        break;
                    }
                    int endMatchLen = suffixMatcher2.isMatch(chars, pos, offset, bufEnd);
                    if (endMatchLen == 0) {
                        pos2 = pos + 1;
                    } else {
                        String str = new String(chars, startPos + startMatchLen, (pos - startPos) - startMatchLen);
                        pos += endMatchLen;
                        int endPos = pos;
                        if (priorVariables == null) {
                            priorVariables = new ArrayList();
                            priorVariables.add(new String(chars, offset, length));
                        }
                        checkCyclicSubstitution(str, priorVariables);
                        priorVariables.add(str);
                        String varValue = resolveVariable(str, buf, startPos, endPos);
                        if (varValue != null) {
                            int varLen = varValue.length();
                            buf.replace(startPos, endPos, varValue);
                            altered = true;
                            int change = substitute(buf, startPos, varLen, priorVariables) + (varLen - (endPos - startPos));
                            pos += change;
                            bufEnd += change;
                            lengthChange += change;
                            chars = buf.buffer;
                        }
                        priorVariables.remove(priorVariables.size() - 1);
                    }
                }
            } else {
                buf.deleteCharAt(pos - 1);
                chars = buf.buffer;
                lengthChange--;
                altered = true;
                bufEnd--;
            }
        }
        if (top) {
            return altered ? 1 : 0;
        }
        return lengthChange;
    }

    private void checkCyclicSubstitution(String varName, List priorVariables) {
        if (priorVariables.contains(varName)) {
            StrBuilder buf = new StrBuilder((int) AccessibilityEventCompat.TYPE_VIEW_HOVER_EXIT);
            buf.append("Infinite loop in property interpolation of ");
            buf.append(priorVariables.remove(0));
            buf.append(": ");
            buf.appendWithSeparators(priorVariables, "->");
            throw new IllegalStateException(buf.toString());
        }
    }

    /* access modifiers changed from: protected */
    public String resolveVariable(String variableName, StrBuilder buf, int startPos, int endPos) {
        StrLookup resolver = getVariableResolver();
        if (resolver == null) {
            return null;
        }
        return resolver.lookup(variableName);
    }

    public char getEscapeChar() {
        return this.escapeChar;
    }

    public void setEscapeChar(char escapeCharacter) {
        this.escapeChar = escapeCharacter;
    }

    public StrMatcher getVariablePrefixMatcher() {
        return this.prefixMatcher;
    }

    public StrSubstitutor setVariablePrefixMatcher(StrMatcher prefixMatcher2) {
        if (prefixMatcher2 == null) {
            throw new IllegalArgumentException("Variable prefix matcher must not be null!");
        }
        this.prefixMatcher = prefixMatcher2;
        return this;
    }

    public StrSubstitutor setVariablePrefix(char prefix) {
        return setVariablePrefixMatcher(StrMatcher.charMatcher(prefix));
    }

    public StrSubstitutor setVariablePrefix(String prefix) {
        if (prefix != null) {
            return setVariablePrefixMatcher(StrMatcher.stringMatcher(prefix));
        }
        throw new IllegalArgumentException("Variable prefix must not be null!");
    }

    public StrMatcher getVariableSuffixMatcher() {
        return this.suffixMatcher;
    }

    public StrSubstitutor setVariableSuffixMatcher(StrMatcher suffixMatcher2) {
        if (suffixMatcher2 == null) {
            throw new IllegalArgumentException("Variable suffix matcher must not be null!");
        }
        this.suffixMatcher = suffixMatcher2;
        return this;
    }

    public StrSubstitutor setVariableSuffix(char suffix) {
        return setVariableSuffixMatcher(StrMatcher.charMatcher(suffix));
    }

    public StrSubstitutor setVariableSuffix(String suffix) {
        if (suffix != null) {
            return setVariableSuffixMatcher(StrMatcher.stringMatcher(suffix));
        }
        throw new IllegalArgumentException("Variable suffix must not be null!");
    }

    public StrLookup getVariableResolver() {
        return this.variableResolver;
    }

    public void setVariableResolver(StrLookup variableResolver2) {
        this.variableResolver = variableResolver2;
    }
}
