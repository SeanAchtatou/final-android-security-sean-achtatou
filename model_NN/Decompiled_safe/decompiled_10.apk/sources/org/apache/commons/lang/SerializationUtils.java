package org.apache.commons.lang;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;

public class SerializationUtils {
    public static Object clone(Serializable object) {
        return deserialize(serialize(object));
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0023 A[SYNTHETIC, Splitter:B:17:0x0023] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void serialize(java.io.Serializable r5, java.io.OutputStream r6) {
        /*
            if (r6 != 0) goto L_0x000a
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            java.lang.String r4 = "The OutputStream must not be null"
            r3.<init>(r4)
            throw r3
        L_0x000a:
            r1 = 0
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x0019 }
            r2.<init>(r6)     // Catch:{ IOException -> 0x0019 }
            r2.writeObject(r5)     // Catch:{ IOException -> 0x002e, all -> 0x002b }
            if (r2 == 0) goto L_0x0018
            r2.close()     // Catch:{ IOException -> 0x0027 }
        L_0x0018:
            return
        L_0x0019:
            r0 = move-exception
        L_0x001a:
            org.apache.commons.lang.SerializationException r3 = new org.apache.commons.lang.SerializationException     // Catch:{ all -> 0x0020 }
            r3.<init>(r0)     // Catch:{ all -> 0x0020 }
            throw r3     // Catch:{ all -> 0x0020 }
        L_0x0020:
            r3 = move-exception
        L_0x0021:
            if (r1 == 0) goto L_0x0026
            r1.close()     // Catch:{ IOException -> 0x0029 }
        L_0x0026:
            throw r3
        L_0x0027:
            r3 = move-exception
            goto L_0x0018
        L_0x0029:
            r4 = move-exception
            goto L_0x0026
        L_0x002b:
            r3 = move-exception
            r1 = r2
            goto L_0x0021
        L_0x002e:
            r0 = move-exception
            r1 = r2
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang.SerializationUtils.serialize(java.io.Serializable, java.io.OutputStream):void");
    }

    public static byte[] serialize(Serializable obj) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(512);
        serialize(obj, baos);
        return baos.toByteArray();
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0024 A[SYNTHETIC, Splitter:B:18:0x0024] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:22:0x0029=Splitter:B:22:0x0029, B:13:0x001b=Splitter:B:13:0x001b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object deserialize(java.io.InputStream r5) {
        /*
            if (r5 != 0) goto L_0x000a
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            java.lang.String r4 = "The InputStream must not be null"
            r3.<init>(r4)
            throw r3
        L_0x000a:
            r1 = 0
            java.io.ObjectInputStream r2 = new java.io.ObjectInputStream     // Catch:{ ClassNotFoundException -> 0x001a, IOException -> 0x0028 }
            r2.<init>(r5)     // Catch:{ ClassNotFoundException -> 0x001a, IOException -> 0x0028 }
            java.lang.Object r3 = r2.readObject()     // Catch:{ ClassNotFoundException -> 0x0039, IOException -> 0x0036, all -> 0x0033 }
            if (r2 == 0) goto L_0x0019
            r2.close()     // Catch:{ IOException -> 0x002f }
        L_0x0019:
            return r3
        L_0x001a:
            r0 = move-exception
        L_0x001b:
            org.apache.commons.lang.SerializationException r3 = new org.apache.commons.lang.SerializationException     // Catch:{ all -> 0x0021 }
            r3.<init>(r0)     // Catch:{ all -> 0x0021 }
            throw r3     // Catch:{ all -> 0x0021 }
        L_0x0021:
            r3 = move-exception
        L_0x0022:
            if (r1 == 0) goto L_0x0027
            r1.close()     // Catch:{ IOException -> 0x0031 }
        L_0x0027:
            throw r3
        L_0x0028:
            r0 = move-exception
        L_0x0029:
            org.apache.commons.lang.SerializationException r3 = new org.apache.commons.lang.SerializationException     // Catch:{ all -> 0x0021 }
            r3.<init>(r0)     // Catch:{ all -> 0x0021 }
            throw r3     // Catch:{ all -> 0x0021 }
        L_0x002f:
            r4 = move-exception
            goto L_0x0019
        L_0x0031:
            r4 = move-exception
            goto L_0x0027
        L_0x0033:
            r3 = move-exception
            r1 = r2
            goto L_0x0022
        L_0x0036:
            r0 = move-exception
            r1 = r2
            goto L_0x0029
        L_0x0039:
            r0 = move-exception
            r1 = r2
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang.SerializationUtils.deserialize(java.io.InputStream):java.lang.Object");
    }

    public static Object deserialize(byte[] objectData) {
        if (objectData != null) {
            return deserialize(new ByteArrayInputStream(objectData));
        }
        throw new IllegalArgumentException("The byte[] must not be null");
    }
}
