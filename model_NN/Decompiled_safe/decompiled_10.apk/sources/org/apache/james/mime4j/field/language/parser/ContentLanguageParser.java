package org.apache.james.mime4j.field.language.parser;

import java.io.InputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class ContentLanguageParser implements ContentLanguageParserConstants {
    private static int[] jj_la1_0;
    private Vector<int[]> jj_expentries;
    private int[] jj_expentry;
    private int jj_gen;
    SimpleCharStream jj_input_stream;
    private int jj_kind;
    private final int[] jj_la1;
    public Token jj_nt;
    private int jj_ntk;
    private List<String> languages;
    public Token token;
    public ContentLanguageParserTokenManager token_source;

    public List<String> parse() throws ParseException {
        try {
            return doParse();
        } catch (TokenMgrError e) {
            throw new ParseException(e);
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0008  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0019  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001c A[FALL_THROUGH, LOOP:0: B:1:0x0003->B:8:0x001c, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x000f A[SYNTHETIC] */
    private final java.util.List<java.lang.String> doParse() throws org.apache.james.mime4j.field.language.parser.ParseException {
        /*
            r3 = this;
            r3.language()
        L_0x0003:
            int r0 = r3.jj_ntk
            r1 = -1
            if (r0 != r1) goto L_0x0019
            int r0 = r3.jj_ntk()
        L_0x000c:
            switch(r0) {
                case 1: goto L_0x001c;
                default: goto L_0x000f;
            }
        L_0x000f:
            int[] r0 = r3.jj_la1
            r1 = 0
            int r2 = r3.jj_gen
            r0[r1] = r2
            java.util.List<java.lang.String> r0 = r3.languages
            return r0
        L_0x0019:
            int r0 = r3.jj_ntk
            goto L_0x000c
        L_0x001c:
            r0 = 1
            r3.jj_consume_token(r0)
            r3.language()
            goto L_0x0003
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.field.language.parser.ContentLanguageParser.doParse():java.util.List");
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x001f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0018  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0033 A[FALL_THROUGH] */
    public final java.lang.String language() throws org.apache.james.mime4j.field.language.parser.ParseException {
        /*
            r8 = this;
            r7 = 45
            r4 = 18
            r6 = 2
            r5 = -1
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            org.apache.james.mime4j.field.language.parser.Token r2 = r8.jj_consume_token(r4)
            java.lang.String r3 = r2.image
            r0.append(r3)
        L_0x0014:
            int r3 = r8.jj_ntk
            if (r3 != r5) goto L_0x0030
            int r3 = r8.jj_ntk()
        L_0x001c:
            switch(r3) {
                case 2: goto L_0x0033;
                case 19: goto L_0x0033;
                default: goto L_0x001f;
            }
        L_0x001f:
            int[] r3 = r8.jj_la1
            r4 = 1
            int r5 = r8.jj_gen
            r3[r4] = r5
            java.lang.String r1 = r0.toString()
            java.util.List<java.lang.String> r3 = r8.languages
            r3.add(r1)
            return r1
        L_0x0030:
            int r3 = r8.jj_ntk
            goto L_0x001c
        L_0x0033:
            int r3 = r8.jj_ntk
            if (r3 != r5) goto L_0x004d
            int r3 = r8.jj_ntk()
        L_0x003b:
            switch(r3) {
                case 2: goto L_0x0050;
                case 19: goto L_0x0060;
                default: goto L_0x003e;
            }
        L_0x003e:
            int[] r3 = r8.jj_la1
            int r4 = r8.jj_gen
            r3[r6] = r4
            r8.jj_consume_token(r5)
            org.apache.james.mime4j.field.language.parser.ParseException r3 = new org.apache.james.mime4j.field.language.parser.ParseException
            r3.<init>()
            throw r3
        L_0x004d:
            int r3 = r8.jj_ntk
            goto L_0x003b
        L_0x0050:
            r8.jj_consume_token(r6)
            org.apache.james.mime4j.field.language.parser.Token r2 = r8.jj_consume_token(r4)
            r0.append(r7)
            java.lang.String r3 = r2.image
            r0.append(r3)
            goto L_0x0014
        L_0x0060:
            r3 = 19
            org.apache.james.mime4j.field.language.parser.Token r2 = r8.jj_consume_token(r3)
            r0.append(r7)
            java.lang.String r3 = r2.image
            r0.append(r3)
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.field.language.parser.ContentLanguageParser.language():java.lang.String");
    }

    static {
        jj_la1_0();
    }

    private static void jj_la1_0() {
        jj_la1_0 = new int[]{2, 524292, 524292};
    }

    public ContentLanguageParser(InputStream stream) {
        this(stream, null);
    }

    public ContentLanguageParser(InputStream stream, String encoding) {
        this.languages = new ArrayList();
        this.jj_la1 = new int[3];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        try {
            this.jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1);
            this.token_source = new ContentLanguageParserTokenManager(this.jj_input_stream);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 3; i++) {
                this.jj_la1[i] = -1;
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public void ReInit(InputStream stream) {
        ReInit(stream, null);
    }

    public void ReInit(InputStream stream, String encoding) {
        try {
            this.jj_input_stream.ReInit(stream, encoding, 1, 1);
            this.token_source.ReInit(this.jj_input_stream);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 3; i++) {
                this.jj_la1[i] = -1;
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public ContentLanguageParser(Reader stream) {
        this.languages = new ArrayList();
        this.jj_la1 = new int[3];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.jj_input_stream = new SimpleCharStream(stream, 1, 1);
        this.token_source = new ContentLanguageParserTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public void ReInit(Reader stream) {
        this.jj_input_stream.ReInit(stream, 1, 1);
        this.token_source.ReInit(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public ContentLanguageParser(ContentLanguageParserTokenManager tm) {
        this.languages = new ArrayList();
        this.jj_la1 = new int[3];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public void ReInit(ContentLanguageParserTokenManager tm) {
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; i++) {
            this.jj_la1[i] = -1;
        }
    }

    private final Token jj_consume_token(int kind) throws ParseException {
        Token oldToken = this.token;
        if (oldToken.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token nextToken = this.token_source.getNextToken();
            token2.next = nextToken;
            this.token = nextToken;
        }
        this.jj_ntk = -1;
        if (this.token.kind == kind) {
            this.jj_gen++;
            return this.token;
        }
        this.token = oldToken;
        this.jj_kind = kind;
        throw generateParseException();
    }

    public final Token getNextToken() {
        if (this.token.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token nextToken = this.token_source.getNextToken();
            token2.next = nextToken;
            this.token = nextToken;
        }
        this.jj_ntk = -1;
        this.jj_gen++;
        return this.token;
    }

    public final Token getToken(int index) {
        Token t;
        int i = 0;
        Token t2 = this.token;
        while (i < index) {
            if (t2.next != null) {
                t = t2.next;
            } else {
                t = this.token_source.getNextToken();
                t2.next = t;
            }
            i++;
            t2 = t;
        }
        return t2;
    }

    private final int jj_ntk() {
        Token token2 = this.token.next;
        this.jj_nt = token2;
        if (token2 == null) {
            Token token3 = this.token;
            Token nextToken = this.token_source.getNextToken();
            token3.next = nextToken;
            int i = nextToken.kind;
            this.jj_ntk = i;
            return i;
        }
        int i2 = this.jj_nt.kind;
        this.jj_ntk = i2;
        return i2;
    }

    public ParseException generateParseException() {
        this.jj_expentries.removeAllElements();
        boolean[] la1tokens = new boolean[23];
        for (int i = 0; i < 23; i++) {
            la1tokens[i] = false;
        }
        if (this.jj_kind >= 0) {
            la1tokens[this.jj_kind] = true;
            this.jj_kind = -1;
        }
        for (int i2 = 0; i2 < 3; i2++) {
            if (this.jj_la1[i2] == this.jj_gen) {
                for (int j = 0; j < 32; j++) {
                    if ((jj_la1_0[i2] & (1 << j)) != 0) {
                        la1tokens[j] = true;
                    }
                }
            }
        }
        for (int i3 = 0; i3 < 23; i3++) {
            if (la1tokens[i3]) {
                this.jj_expentry = new int[1];
                this.jj_expentry[0] = i3;
                this.jj_expentries.addElement(this.jj_expentry);
            }
        }
        int[][] exptokseq = new int[this.jj_expentries.size()][];
        for (int i4 = 0; i4 < this.jj_expentries.size(); i4++) {
            exptokseq[i4] = this.jj_expentries.elementAt(i4);
        }
        return new ParseException(this.token, exptokseq, tokenImage);
    }

    public final void enable_tracing() {
    }

    public final void disable_tracing() {
    }
}
