package org.apache.harmony.javax.security.auth.callback;

import java.io.Serializable;

public class TextOutputCallback implements Callback, Serializable {
    public static final int ERROR = 2;
    public static final int INFORMATION = 0;
    public static final int WARNING = 1;
    private static final long serialVersionUID = 1689502495511663102L;
    private String message;
    private int messageType;

    public TextOutputCallback(int messageType2, String message2) {
        if (messageType2 > 2 || messageType2 < 0) {
            throw new IllegalArgumentException("auth.16");
        } else if (message2 == null || message2.length() == 0) {
            throw new IllegalArgumentException("auth.1F");
        } else {
            this.messageType = messageType2;
            this.message = message2;
        }
    }

    public String getMessage() {
        return this.message;
    }

    public int getMessageType() {
        return this.messageType;
    }
}
