package org.apache.harmony.javax.security.auth.login;

import org.apache.harmony.javax.security.auth.AuthPermission;

public abstract class Configuration {
    private static final AuthPermission GET_LOGIN_CONFIGURATION = new AuthPermission("getLoginConfiguration");
    private static final String LOGIN_CONFIGURATION_PROVIDER = "login.configuration.provider";
    private static final AuthPermission SET_LOGIN_CONFIGURATION = new AuthPermission("setLoginConfiguration");
    private static Configuration configuration;

    public abstract AppConfigurationEntry[] getAppConfigurationEntry(String str);

    public abstract void refresh();

    protected Configuration() {
    }

    public static Configuration getConfiguration() {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            sm.checkPermission(GET_LOGIN_CONFIGURATION);
        }
        return getAccessibleConfiguration();
    }

    private static final Configuration getDefaultProvider() {
        return new Configuration() {
            public void refresh() {
            }

            public AppConfigurationEntry[] getAppConfigurationEntry(String applicationName) {
                return new AppConfigurationEntry[0];
            }
        };
    }

    static Configuration getAccessibleConfiguration() {
        Configuration current = configuration;
        if (current == null) {
            synchronized (Configuration.class) {
                if (configuration == null) {
                    configuration = getDefaultProvider();
                }
                current = configuration;
            }
        }
        return current;
    }

    public static void setConfiguration(Configuration configuration2) {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            sm.checkPermission(SET_LOGIN_CONFIGURATION);
        }
        configuration = configuration2;
    }
}
