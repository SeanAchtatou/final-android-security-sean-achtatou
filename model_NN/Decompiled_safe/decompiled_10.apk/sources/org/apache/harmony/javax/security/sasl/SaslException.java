package org.apache.harmony.javax.security.sasl;

import java.io.IOException;

public class SaslException extends IOException {
    private static final long serialVersionUID = 4579784287983423626L;
    private Throwable _exception;

    public SaslException() {
    }

    public SaslException(String detail) {
        super(detail);
    }

    public SaslException(String detail, Throwable ex) {
        super(detail);
        if (ex != null) {
            super.initCause(ex);
            this._exception = ex;
        }
    }

    public Throwable getCause() {
        return this._exception;
    }

    public Throwable initCause(Throwable cause) {
        super.initCause(cause);
        this._exception = cause;
        return this;
    }

    public String toString() {
        if (this._exception == null) {
            return super.toString();
        }
        return super.toString() + ", caused by: " + this._exception.toString();
    }
}
