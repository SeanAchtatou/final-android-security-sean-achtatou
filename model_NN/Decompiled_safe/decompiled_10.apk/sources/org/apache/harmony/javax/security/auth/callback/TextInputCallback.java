package org.apache.harmony.javax.security.auth.callback;

import java.io.Serializable;

public class TextInputCallback implements Callback, Serializable {
    private static final long serialVersionUID = -8064222478852811804L;
    private String defaultText;
    private String inputText;
    private String prompt;

    private void setPrompt(String prompt2) {
        if (prompt2 == null || prompt2.length() == 0) {
            throw new IllegalArgumentException("auth.14");
        }
        this.prompt = prompt2;
    }

    private void setDefaultText(String defaultText2) {
        if (defaultText2 == null || defaultText2.length() == 0) {
            throw new IllegalArgumentException("auth.15");
        }
        this.defaultText = defaultText2;
    }

    public TextInputCallback(String prompt2) {
        setPrompt(prompt2);
    }

    public TextInputCallback(String prompt2, String defaultText2) {
        setPrompt(prompt2);
        setDefaultText(defaultText2);
    }

    public String getDefaultText() {
        return this.defaultText;
    }

    public String getPrompt() {
        return this.prompt;
    }

    public String getText() {
        return this.inputText;
    }

    public void setText(String text) {
        this.inputText = text;
    }
}
