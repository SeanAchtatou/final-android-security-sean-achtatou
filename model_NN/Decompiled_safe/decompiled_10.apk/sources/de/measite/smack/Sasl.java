package de.measite.smack;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.apache.harmony.javax.security.sasl.SaslClient;
import org.apache.harmony.javax.security.sasl.SaslException;
import org.apache.harmony.javax.security.sasl.SaslServer;
import org.apache.harmony.javax.security.sasl.SaslServerFactory;

public class Sasl {
    private static final String CLIENTFACTORYSRV = "SaslClientFactory";
    public static final String MAX_BUFFER = "javax.security.sasl.maxbuffer";
    public static final String POLICY_FORWARD_SECRECY = "javax.security.sasl.policy.forward";
    public static final String POLICY_NOACTIVE = "javax.security.sasl.policy.noactive";
    public static final String POLICY_NOANONYMOUS = "javax.security.sasl.policy.noanonymous";
    public static final String POLICY_NODICTIONARY = "javax.security.sasl.policy.nodictionary";
    public static final String POLICY_NOPLAINTEXT = "javax.security.sasl.policy.noplaintext";
    public static final String POLICY_PASS_CREDENTIALS = "javax.security.sasl.policy.credentials";
    public static final String QOP = "javax.security.sasl.qop";
    public static final String RAW_SEND_SIZE = "javax.security.sasl.rawsendsize";
    public static final String REUSE = "javax.security.sasl.reuse";
    private static final String SERVERFACTORYSRV = "SaslServerFactory";
    public static final String SERVER_AUTH = "javax.security.sasl.server.authentication";
    public static final String STRENGTH = "javax.security.sasl.strength";

    public static Enumeration<SaslClientFactory> getSaslClientFactories() {
        Hashtable<SaslClientFactory, Object> factories = new Hashtable<>();
        factories.put(new SaslClientFactory(), new Object());
        return factories.keys();
    }

    public static Enumeration<SaslServerFactory> getSaslServerFactories() {
        return org.apache.harmony.javax.security.sasl.Sasl.getSaslServerFactories();
    }

    public static SaslServer createSaslServer(String mechanism, String protocol, String serverName, Map<String, ?> prop, CallbackHandler cbh) throws SaslException {
        return org.apache.harmony.javax.security.sasl.Sasl.createSaslServer(mechanism, protocol, serverName, prop, cbh);
    }

    public static SaslClient createSaslClient(String[] mechanisms, String authanticationID, String protocol, String serverName, Map<String, ?> prop, CallbackHandler cbh) throws SaslException {
        if (mechanisms == null) {
            throw new NullPointerException("auth.33");
        }
        SaslClientFactory fact = getSaslClientFactories().nextElement();
        String[] mech = fact.getMechanismNames(null);
        boolean is = false;
        if (mech != null) {
            for (String equals : mech) {
                int n = 0;
                while (true) {
                    if (n >= mechanisms.length) {
                        break;
                    } else if (equals.equals(mechanisms[n])) {
                        is = true;
                        break;
                    } else {
                        n++;
                    }
                }
            }
        }
        if (is) {
            return fact.createSaslClient(mechanisms, authanticationID, protocol, serverName, prop, cbh);
        }
        return null;
    }
}
