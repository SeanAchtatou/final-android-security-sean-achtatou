package com.munic.conyFunnyEffects;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class conyFunnyEffects extends Activity {
    private static final int INPUT = 0;
    Button Notification;
    private AdView adView;
    int courros = 0;
    String[] files = {"1Steve", "2BruceGuitarSolo", "3F1Equation", "4Moving", "5Nocturne", "6110PoliceCarAudio", "7TitleIns", "8ApacheHelicopter", "9LoveRomance", "10MusicBox", "11120Ambulance", "12BruceGuitarSolo", "13ShawStudios", "14ThePond", "15FlushTheToilet", "16SymphonyJungle", "17Snoring", "18EngineOfMotorcycle", "19WolfHowling"};
    Handler handler = new Handler();
    Button left;
    private View.OnClickListener leftb = new View.OnClickListener() {
        public void onClick(View arg0) {
            if (Integer.valueOf(conyFunnyEffects.this.sCurrentMusic).intValue() == 1) {
                conyFunnyEffects.this.showDialog("1");
                return;
            }
            if (conyFunnyEffects.this.player.isPlaying()) {
                conyFunnyEffects.this.player.stop();
            }
            Intent intent = new Intent(conyFunnyEffects.this, conyFunnyEffects.class);
            Bundle bundle = new Bundle();
            bundle.putString("COUNTMUSIC", conyFunnyEffects.this.sCountMusic);
            bundle.putString("CURRENTMUSIC", new StringBuilder(String.valueOf(Integer.valueOf(conyFunnyEffects.this.sCurrentMusic).intValue() - 1)).toString());
            intent.putExtras(bundle);
            conyFunnyEffects.this.startActivity(intent);
            conyFunnyEffects.this.finish();
        }
    };
    private View.OnClickListener nfb = new View.OnClickListener() {
        public void onClick(View arg0) {
            String temp = conyFunnyEffects.this.createFileName();
            conyFunnyEffects.this.copydb(conyFunnyEffects.this.playint[conyFunnyEffects.this.courros], temp, 944128);
            conyFunnyEffects.this.setMyNotification(temp);
        }
    };
    String[] paths = {"s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10", "s11", "s12", "s13", "s14", "s15", "s16", "s17", "s18", "s19"};
    Button play;
    /* access modifiers changed from: private */
    public MediaPlayer player = new MediaPlayer();
    int[] playint = {R.raw.s1, R.raw.s2, R.raw.s3, R.raw.s4, R.raw.s5, R.raw.s6, R.raw.s7, R.raw.s8, R.raw.s9, R.raw.s10, R.raw.s11, R.raw.s12, R.raw.s13, R.raw.s14, R.raw.s15, R.raw.s16, R.raw.s17, R.raw.s18, R.raw.s19};
    boolean playstatus = false;
    private View.OnClickListener pp = new View.OnClickListener() {
        public void onClick(View v) {
            conyFunnyEffects.this.onStart("file:///android_asset/" + conyFunnyEffects.this.paths[conyFunnyEffects.this.courros] + ".mp3");
        }
    };
    String prePath = "/sdcard/";
    Button right;
    private View.OnClickListener rightb = new View.OnClickListener() {
        public void onClick(View arg0) {
            if (Integer.valueOf(conyFunnyEffects.this.sCurrentMusic) == Integer.valueOf(conyFunnyEffects.this.sCountMusic)) {
                conyFunnyEffects.this.showDialog(conyFunnyEffects.this.sCountMusic);
                return;
            }
            if (conyFunnyEffects.this.player.isPlaying()) {
                conyFunnyEffects.this.player.stop();
            }
            Intent intent = new Intent(conyFunnyEffects.this, conyFunnyEffects.class);
            Bundle bundle = new Bundle();
            bundle.putString("COUNTMUSIC", conyFunnyEffects.this.sCountMusic);
            bundle.putString("CURRENTMUSIC", new StringBuilder(String.valueOf(Integer.valueOf(conyFunnyEffects.this.sCurrentMusic).intValue() + 1)).toString());
            intent.putExtras(bundle);
            conyFunnyEffects.this.startActivity(intent);
            conyFunnyEffects.this.finish();
        }
    };
    private View.OnClickListener ringoneb = new View.OnClickListener() {
        public void onClick(View arg0) {
            String temp = conyFunnyEffects.this.createFileName();
            conyFunnyEffects.this.copydb(conyFunnyEffects.this.playint[conyFunnyEffects.this.courros], temp, 944128);
            conyFunnyEffects.this.setMyRingtone(temp);
        }
    };
    Button ringset;
    Runnable runnable = new Runnable() {
        public void run() {
            conyFunnyEffects.this.changestatus();
            conyFunnyEffects.this.handler.postDelayed(this, 50);
        }
    };
    /* access modifiers changed from: private */
    public String sCountMusic = "19";
    /* access modifiers changed from: private */
    public String sCurrentMusic = "1";
    Button save;
    private View.OnClickListener saveb = new View.OnClickListener() {
        public void onClick(View arg0) {
            try {
                conyFunnyEffects.this.copydb(conyFunnyEffects.this.playint[conyFunnyEffects.this.courros], conyFunnyEffects.this.createFileName(), 944128);
                Toast.makeText(conyFunnyEffects.this.getApplicationContext(), "Save successful.", 0).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    Button stop;
    TextView tv;

    public void changestatus() {
        if (!this.player.isPlaying()) {
            this.play.setBackgroundResource(R.drawable.play);
        } else {
            this.play.setBackgroundResource(R.drawable.pausenormalred);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        new Bundle();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.getString("COUNTMUSIC") != null) {
                this.sCountMusic = bundle.getString("COUNTMUSIC");
            }
            if (bundle.getString("CURRENTMUSIC") != null) {
                this.sCurrentMusic = bundle.getString("CURRENTMUSIC");
                this.courros = Integer.valueOf(this.sCurrentMusic).intValue() - 1;
            }
        }
        this.tv = (TextView) findViewById(R.id.widget45);
        this.play = (Button) findViewById(R.id.widget44);
        this.play.setOnClickListener(this.pp);
        this.left = (Button) findViewById(R.id.widget38);
        this.left.setOnClickListener(this.leftb);
        this.right = (Button) findViewById(R.id.widget39);
        this.right.setOnClickListener(this.rightb);
        this.save = (Button) findViewById(R.id.widget40);
        this.save.setOnClickListener(this.saveb);
        this.ringset = (Button) findViewById(R.id.widget41);
        this.ringset.setOnClickListener(this.ringoneb);
        this.Notification = (Button) findViewById(R.id.widget43);
        this.Notification.setOnClickListener(this.nfb);
        onStart("file:///android_asset/" + this.paths[this.courros] + ".mp3");
        this.handler.postDelayed(this.runnable, 50);
        this.adView = new AdView(this, AdSize.BANNER, "a14e4c96511e0d8");
        ((LinearLayout) findViewById(R.id.AdmainLayout)).addView(this.adView);
        this.adView.loadAd(new AdRequest());
    }

    /* access modifiers changed from: private */
    public void showDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        builder.create().show();
    }

    public void onDestroy() {
        this.adView.destroy();
        if (this.player.isPlaying()) {
            this.player.stop();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: private */
    public void copydb(int dbint, String databaseFilename, int FileSize) {
        try {
            InputStream is = getResources().openRawResource(dbint);
            FileOutputStream fos = new FileOutputStream(databaseFilename);
            byte[] buffer = new byte[FileSize];
            while (true) {
                int count = is.read(buffer);
                if (count <= 0) {
                    fos.close();
                    is.close();
                    return;
                }
                fos.write(buffer, 0, count);
            }
        } catch (Exception e) {
        }
    }

    public void save(String filename) {
        try {
            DataOutputStream out = new DataOutputStream(new FileOutputStream("/sdcard/data2.bat"));
            try {
                out.writeInt(11);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                out.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        } catch (FileNotFoundException e3) {
            Toast.makeText(getApplicationContext(), "aa", 0).show();
            e3.printStackTrace();
        }
        Toast.makeText(getApplicationContext(), "Save error", 0).show();
        FileInputStream inStream = (FileInputStream) getResources().openRawResource(this.playint[this.courros]);
        byte[] buffer = new byte[20480];
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        while (true) {
            try {
                int len = inStream.read(buffer);
                if (len == -1) {
                    break;
                }
                outStream.write(buffer, 0, len);
            } catch (IOException e4) {
                e4.printStackTrace();
            }
        }
        byte[] data = outStream.toByteArray();
        try {
            outStream.close();
        } catch (IOException e5) {
            e5.printStackTrace();
        }
        try {
            inStream.close();
        } catch (IOException e6) {
            e6.printStackTrace();
        }
        saveToSDCard(createFileName(), data);
    }

    public String createFileName() {
        return "/sdcard/m_" + this.files[this.courros] + String.valueOf(System.currentTimeMillis()) + ".mp3";
    }

    public void saveToSDCard(String filename, byte[] content) {
        if (new File("/sdcard").isDirectory()) {
            DataOutputStream out = null;
            try {
                out = new DataOutputStream(new FileOutputStream(filename));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                out.write(content);
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            try {
                out.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public void setMyRingtone(String p) {
        this.player.stop();
        File sdfile = new File(p);
        Log.i("File", p);
        ContentValues values = new ContentValues();
        values.put("_data", sdfile.getAbsolutePath());
        values.put("title", sdfile.getName());
        values.put("mime_type", "audio/*");
        values.put("is_ringtone", (Boolean) true);
        values.put("is_notification", (Boolean) false);
        values.put("is_alarm", (Boolean) false);
        values.put("is_music", (Boolean) false);
        RingtoneManager.setActualDefaultRingtoneUri(this, 1, getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(sdfile.getAbsolutePath()), values));
        Toast.makeText(getApplicationContext(), (int) R.string.setRingtoneSucceed, 0).show();
        System.out.println("setMyRingtone()-------------");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public void setMyNotification(String p) {
        this.player.stop();
        File sdfile = new File(p);
        ContentValues values = new ContentValues();
        values.put("_data", sdfile.getAbsolutePath());
        values.put("title", sdfile.getName());
        values.put("mime_type", "audio/*");
        values.put("is_ringtone", (Boolean) false);
        values.put("is_notification", (Boolean) true);
        values.put("is_alarm", (Boolean) false);
        values.put("is_music", (Boolean) false);
        RingtoneManager.setActualDefaultRingtoneUri(this, 2, getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(sdfile.getAbsolutePath()), values));
        Toast.makeText(getApplicationContext(), (int) R.string.setNotificationSucceed, 0).show();
        System.out.println("setMyNOTIFICATION-------------");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public void setMyAlarm(String p) {
        this.player.stop();
        File sdfile = new File(p);
        ContentValues values = new ContentValues();
        values.put("_data", sdfile.getAbsolutePath());
        values.put("title", sdfile.getName());
        values.put("mime_type", "audio/*");
        values.put("is_ringtone", (Boolean) false);
        values.put("is_notification", (Boolean) false);
        values.put("is_alarm", (Boolean) true);
        values.put("is_music", (Boolean) false);
        RingtoneManager.setActualDefaultRingtoneUri(this, 4, getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(sdfile.getAbsolutePath()), values));
        Toast.makeText(getApplicationContext(), (int) R.string.setAlarmSucceed, 0).show();
        System.out.println("setMyNOTIFICATION-------------");
    }

    public void onStart(String p) {
        if (!this.prePath.equals(p) || !this.player.isPlaying()) {
            Uri parse = Uri.parse(p);
            this.prePath = p;
            this.player.stop();
            this.player = MediaPlayer.create(this, this.playint[this.courros]);
            this.player.start();
            this.tv.setText(this.files[this.courros]);
            return;
        }
        this.player.stop();
    }
}
