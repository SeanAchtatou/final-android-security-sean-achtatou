package com.munic.conyFunnyEffects;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int audio = 2130837504;
        public static final int folder = 2130837505;
        public static final int icon = 2130837506;
        public static final int l = 2130837507;
        public static final int pausenormalred = 2130837508;
        public static final int play = 2130837509;
        public static final int r = 2130837510;
        public static final int save = 2130837511;
        public static final int selectall = 2130837512;
        public static final int settingmessage = 2130837513;
        public static final int settingphone = 2130837514;
        public static final int toggleall = 2130837515;
        public static final int unselectall = 2130837516;
        public static final int uponelevel = 2130837517;
    }

    public static final class id {
        public static final int AdmainLayout = 2131099648;
        public static final int widget29 = 2131099649;
        public static final int widget38 = 2131099655;
        public static final int widget39 = 2131099654;
        public static final int widget40 = 2131099656;
        public static final int widget41 = 2131099653;
        public static final int widget43 = 2131099652;
        public static final int widget44 = 2131099651;
        public static final int widget45 = 2131099650;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int s1 = 2130968576;
        public static final int s10 = 2130968577;
        public static final int s11 = 2130968578;
        public static final int s12 = 2130968579;
        public static final int s13 = 2130968580;
        public static final int s14 = 2130968581;
        public static final int s15 = 2130968582;
        public static final int s16 = 2130968583;
        public static final int s17 = 2130968584;
        public static final int s18 = 2130968585;
        public static final int s19 = 2130968586;
        public static final int s2 = 2130968587;
        public static final int s3 = 2130968588;
        public static final int s4 = 2130968589;
        public static final int s5 = 2130968590;
        public static final int s6 = 2130968591;
        public static final int s7 = 2130968592;
        public static final int s8 = 2130968593;
        public static final int s9 = 2130968594;
    }

    public static final class string {
        public static final int about = 2131034118;
        public static final int about_android = 2131034124;
        public static final int about_app_homepage = 2131034122;
        public static final int about_app_mail = 2131034123;
        public static final int about_app_moto = 2131034120;
        public static final int about_app_name = 2131034119;
        public static final int about_app_ver = 2131034121;
        public static final int app_name = 2131034113;
        public static final int confirm = 2131034116;
        public static final int declaration = 2131034131;
        public static final int delete = 2131034129;
        public static final int exit = 2131034117;
        public static final int hello = 2131034112;
        public static final int neverMind = 2131034130;
        public static final int none = 2131034115;
        public static final int setAlarm = 2131034128;
        public static final int setAlarmSucceed = 2131034134;
        public static final int setNotification = 2131034127;
        public static final int setNotificationSucceed = 2131034133;
        public static final int setRingtone = 2131034126;
        public static final int setRingtoneSucceed = 2131034132;
        public static final int tips = 2131034125;
        public static final int up_one_level = 2131034114;
    }
}
