package com.psc.fukumoto.adlib;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.FrameLayout;
import com.psc.fukumoto.lib.TimerHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import jp.co.nobot.libAdMaker.libAdMaker;

public class AdMakerView extends AdView implements TimerHandler.OnTimerListener {
    public static final int ADMAKER_HEIGHT = 50;
    public static final int ADMAKER_WIDTH = 320;
    private static final int FILL_PARENT = -1;
    private static final int TIME_SHOW_AD = 5000;
    private static final int WRAP_CONTENT = -2;
    private libAdMaker mAdMaker = null;
    private boolean mAdStart = false;
    private TimerHandler mTimer = null;

    public AdMakerView(Activity activity, String siteId, String zoneId, String url) {
        super(activity);
        this.mAdMaker = new libAdMaker(activity);
        this.mAdMaker.setHorizontalScrollBarEnabled(false);
        setActivity(this.mAdMaker, activity);
        this.mAdMaker.siteId = siteId;
        this.mAdMaker.zoneId = zoneId;
        this.mAdMaker.setUrl(url);
        this.mAdMaker.setBackgroundColor(0);
        this.mAdMaker.start();
        this.mAdStart = true;
        addView(this.mAdMaker, new FrameLayout.LayoutParams((int) FILL_PARENT, (int) WRAP_CONTENT));
    }

    public void onRestart() {
        setVisibility(0);
    }

    public void onResume() {
        setVisibility(4);
        this.mTimer = new TimerHandler(this, TIME_SHOW_AD, false);
        this.mTimer.start();
    }

    public void onPause() {
        setVisibility(4);
        if (this.mTimer != null) {
            this.mTimer.stop();
            this.mTimer = null;
        }
    }

    public void onDestroy() {
        if (this.mAdMaker != null) {
            this.mAdMaker.destroy();
            this.mAdMaker = null;
        }
    }

    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility == 0) {
            if (this.mAdMaker != null && !this.mAdStart) {
                this.mAdMaker.start();
                this.mAdStart = true;
            }
        } else if (this.mAdStart) {
            stop(this.mAdMaker);
            this.mAdStart = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Context context = getContext();
        super.onMeasure(getDrawWidth(context), getDrawHeight(context));
    }

    public void onTimer() {
        this.mTimer = null;
        setVisibility(0);
    }

    public static final int getDrawWidth(Context context) {
        return (int) (getScaledDensity(context) * 320.0f);
    }

    public static final int getDrawHeight(Context context) {
        return (int) (getScaledDensity(context) * 50.0f);
    }

    private static final float getScaledDensity(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(metrics);
        return metrics.scaledDensity;
    }

    public static final void setActivity(libAdMaker adMaker, Activity activity) {
        if (adMaker != null) {
            Method method = null;
            try {
                method = adMaker.getClass().getMethod("setActivity", Activity.class);
            } catch (NoSuchMethodException e) {
            }
            if (method != null) {
                try {
                    method.invoke(adMaker, activity);
                } catch (InvocationTargetException e2) {
                    InvocationTargetException ex = e2;
                    Throwable cause = ex.getCause();
                    if (cause instanceof RuntimeException) {
                        throw ((RuntimeException) cause);
                    } else if (cause instanceof Error) {
                        throw ((Error) cause);
                    } else {
                        throw new RuntimeException(ex);
                    }
                } catch (IllegalAccessException e3) {
                }
            }
        }
    }

    public static final void stop(libAdMaker adMaker) {
        if (adMaker != null) {
            Method method = null;
            try {
                method = adMaker.getClass().getMethod("stop", new Class[0]);
            } catch (NoSuchMethodException e) {
            }
            if (method != null) {
                try {
                    method.invoke(adMaker, new Object[0]);
                } catch (InvocationTargetException e2) {
                    InvocationTargetException ex = e2;
                    Throwable cause = ex.getCause();
                    if (cause instanceof RuntimeException) {
                        throw ((RuntimeException) cause);
                    } else if (cause instanceof Error) {
                        throw ((Error) cause);
                    } else {
                        throw new RuntimeException(ex);
                    }
                } catch (IllegalAccessException e3) {
                }
            }
        }
    }
}
