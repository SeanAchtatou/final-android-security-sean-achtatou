package com.psc.fukumoto.adlib;

import android.app.Activity;
import android.content.Context;
import android.widget.FrameLayout;

public class AdDummyView extends AdView {
    private static final int FILL_PARENT = -1;
    private AdImageView mAdImageView = null;

    public AdDummyView(Context context) {
        super(context);
    }

    public AdDummyView(Activity activity, String siteId, String zoneId, String url) {
        super(activity);
        this.mAdImageView = new AdImageView(activity);
        addView(this.mAdImageView, new FrameLayout.LayoutParams((int) FILL_PARENT, (int) FILL_PARENT));
    }

    public void onRestart() {
    }

    public void onResume() {
    }

    public void onPause() {
    }

    public void onDestroy() {
    }
}
