package com.psc.fukumoto.adlib;

import android.content.Context;
import android.widget.FrameLayout;

public class AdView extends FrameLayout {
    public AdView(Context context) {
        super(context);
    }

    public void onRestart() {
    }

    public void onResume() {
    }

    public void onPause() {
    }

    public void onDestroy() {
    }
}
