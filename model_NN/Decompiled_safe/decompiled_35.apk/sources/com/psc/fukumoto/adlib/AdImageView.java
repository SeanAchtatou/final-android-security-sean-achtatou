package com.psc.fukumoto.adlib;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.ImageView;

public class AdImageView extends ImageView {
    private static final int ADVIEW_HEIGHT = 50;
    private static final int ADVIEW_WIDTH = 320;

    public AdImageView(Context context) {
        super(context);
        int width = (int) (getScaledDensity(context) * 320.0f);
        int height = (int) (getScaledDensity(context) * 50.0f);
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(-65536);
        Paint paint = new Paint();
        paint.setColor(-16776961);
        paint.setTextSize((float) (height / 2));
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        canvas.drawText("広告欄", (((float) width) - paint.measureText("広告欄")) / 2.0f, ((((float) height) - ((float) Math.ceil((double) (fontMetrics.bottom - fontMetrics.top)))) / 2.0f) - fontMetrics.top, paint);
        setImageBitmap(bitmap);
    }

    public void onRestart() {
    }

    public void onPause() {
    }

    public void onDestroy() {
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure((int) (getScaledDensity(getContext()) * 320.0f), (int) (getScaledDensity(getContext()) * 50.0f));
    }

    private static final float getScaledDensity(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(metrics);
        return metrics.scaledDensity;
    }
}
