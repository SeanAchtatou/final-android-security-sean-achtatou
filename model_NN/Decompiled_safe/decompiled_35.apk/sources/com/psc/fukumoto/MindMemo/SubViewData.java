package com.psc.fukumoto.MindMemo;

import java.io.Serializable;

public class SubViewData implements Serializable {
    private static final long serialVersionUID = 1;
    private float mCenterX;
    private float mCenterY;
    private int mId = 0;

    public SubViewData(SubView subView) {
        if (subView != null) {
            this.mId = subView.getId();
            this.mCenterX = subView.getCenterX();
            this.mCenterY = subView.getCenterY();
        }
    }

    public SubView createSubView() {
        return new SubView(this);
    }

    public int getId() {
        return this.mId;
    }

    public float getCenterX() {
        return this.mCenterX;
    }

    public float getCenterY() {
        return this.mCenterY;
    }
}
