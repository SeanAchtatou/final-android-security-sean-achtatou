package com.psc.fukumoto.MindMemo;

public final class R {

    public static final class array {
        public static final int list_fontsize = 2130968576;
        public static final int select_fontsize = 2130968577;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int MainView_background = 2131034112;
        public static final int dialog_filename = 2131034113;
    }

    public static final class drawable {
        public static final int button_save = 2130837504;
        public static final int button_save_normal = 2130837505;
        public static final int button_save_push = 2130837506;
        public static final int button_touchadd = 2130837507;
        public static final int button_touchadd_normal = 2130837508;
        public static final int button_touchadd_push = 2130837509;
        public static final int button_touchmove = 2130837510;
        public static final int button_touchmove_normal = 2130837511;
        public static final int button_touchmove_push = 2130837512;
        public static final int color_000000 = 2130837513;
        public static final int color_7171ee = 2130837514;
        public static final int color_71ee71 = 2130837515;
        public static final int color_71eeee = 2130837516;
        public static final int color_e3991b = 2130837517;
        public static final int color_ee7171 = 2130837518;
        public static final int color_ee71ee = 2130837519;
        public static final int color_eeee71 = 2130837520;
        public static final int color_ffffff = 2130837521;
        public static final int icon = 2130837522;
        public static final int list_selector_background_focus = 2130837523;
    }

    public static final class id {
        public static final int button_colorbackground = 2131230721;
        public static final int button_colorframe = 2131230722;
        public static final int button_colorline = 2131230723;
        public static final int button_colortext = 2131230733;
        public static final int checkbox_saveinview = 2131230726;
        public static final int checkbox_showgallery = 2131230727;
        public static final int checkbox_transparent = 2131230720;
        public static final int edittext_filename = 2131230725;
        public static final int edittext_textmemo = 2131230729;
        public static final int edittext_textshare = 2131230730;
        public static final int filename = 2131230728;
        public static final int spinner_fontsize = 2131230732;
        public static final int table_color = 2131230731;
        public static final int text_foldername = 2131230724;
    }

    public static final class integer {
        public static final int mIdAdView = 2131099652;
        public static final int mIdButtonSave = 2131099650;
        public static final int mIdButtonTouchMode = 2131099649;
        public static final int mIdMemoView = 2131099648;
        public static final int mIdZoomControls = 2131099651;
    }

    public static final class layout {
        public static final int dialog_group_view = 2130903040;
        public static final int dialog_memo_view = 2130903041;
        public static final int dialog_save_file = 2130903042;
        public static final int dialog_save_image = 2130903043;
        public static final int dialog_select_file = 2130903044;
        public static final int dialog_text_memo_view_easy = 2130903045;
        public static final int dialog_text_memo_view_full = 2130903046;
        public static final int preferences = 2130903047;
    }

    public static final class string {
        public static final int app_name = 2131165186;
        public static final int button_cancel = 2131165202;
        public static final int button_create = 2131165200;
        public static final int button_delete = 2131165199;
        public static final int button_detail = 2131165203;
        public static final int button_load = 2131165198;
        public static final int button_no = 2131165205;
        public static final int button_save = 2131165197;
        public static final int button_set = 2131165201;
        public static final int button_yes = 2131165204;
        public static final int contact_url = 2131165184;
        public static final int key_contact = 2131165251;
        public static final int key_createtap = 2131165242;
        public static final int key_deletesinglegroup = 2131165239;
        public static final int key_menuaftertouchup = 2131165248;
        public static final int key_sharetap = 2131165245;
        public static final int menu_contact = 2131165185;
        public static final int menu_copygroup = 2131165210;
        public static final int menu_copyitem = 2131165214;
        public static final int menu_createitem = 2131165212;
        public static final int menu_deletegroup = 2131165211;
        public static final int menu_deleteitem = 2131165215;
        public static final int menu_editgroup = 2131165209;
        public static final int menu_edititem = 2131165213;
        public static final int menu_editmemo = 2131165208;
        public static final int menu_leavegroup = 2131165216;
        public static final int menu_moveback = 2131165207;
        public static final int menu_movefront = 2131165206;
        public static final int menu_new = 2131165219;
        public static final int menu_preference = 2131165222;
        public static final int menu_removeline = 2131165217;
        public static final int menu_saveimage = 2131165221;
        public static final int menu_selectfile = 2131165220;
        public static final int menu_sharetext = 2131165218;
        public static final int message_confirmclear = 2131165228;
        public static final int message_confirmdelete = 2131165227;
        public static final int message_confirmoverwrite = 2131165226;
        public static final int message_failedload = 2131165225;
        public static final int message_failedsave = 2131165224;
        public static final int message_failedshare = 2131165229;
        public static final int message_finishsave = 2131165223;
        public static final int summary_contact = 2131165253;
        public static final int summary_createtap = 2131165244;
        public static final int summary_deletesinglegroup = 2131165241;
        public static final int summary_menuaftertouchup = 2131165250;
        public static final int summary_sharetap = 2131165247;
        public static final int title_colorbackground = 2131165232;
        public static final int title_colorframe = 2131165233;
        public static final int title_colorline = 2131165234;
        public static final int title_colortext = 2131165231;
        public static final int title_confirmclear = 2131165195;
        public static final int title_contact = 2131165252;
        public static final int title_creategroup = 2131165189;
        public static final int title_createtap = 2131165243;
        public static final int title_createview = 2131165187;
        public static final int title_deletesinglegroup = 2131165240;
        public static final int title_editgroup = 2131165190;
        public static final int title_editmemo = 2131165191;
        public static final int title_editview = 2131165188;
        public static final int title_fontsize = 2131165230;
        public static final int title_menuaftertouchup = 2131165249;
        public static final int title_savefile = 2131165192;
        public static final int title_saveimage = 2131165193;
        public static final int title_saveinview = 2131165236;
        public static final int title_selectapplication = 2131165196;
        public static final int title_selectfile = 2131165194;
        public static final int title_share = 2131165238;
        public static final int title_sharetap = 2131165246;
        public static final int title_showgallery = 2131165237;
        public static final int title_transparent = 2131165235;
    }
}
