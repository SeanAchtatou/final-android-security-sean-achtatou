package com.psc.fukumoto.MindMemo;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.psc.fukumoto.MindMemo.MainView;
import com.psc.fukumoto.MindMemo.MemoView;
import com.psc.fukumoto.adlib.AdMakerView;
import com.psc.fukumoto.lib.FileData;

public class MindMemoActivity extends Activity implements MemoView.ShareListener, MainView.FileDataListener, MainView.MoveToActivity {
    private static final String ADMAKER_SITEID = "1377";
    private static final String ADMAKER_URL = "http://images.ad-maker.info/apps/qnwpnsgxw3n1.html";
    private static final String ADMAKER_ZONEID = "5051";
    private static final int REQUEST_PREFERENCE = 2;
    private AdMakerView mAdMakerView = null;
    private MainView mMainView = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        FileData fileData = null;
        if (intent != null && !intent.getAction().equals("android.intent.action.MAIN")) {
            fileData = FileData.getIntentFileName(this, intent);
        }
        this.mAdMakerView = new AdMakerView(this, ADMAKER_SITEID, ADMAKER_ZONEID, ADMAKER_URL);
        this.mMainView = new MainView(this, this.mAdMakerView, fileData);
        setContentView(this.mMainView);
        this.mMainView.setShareListener(this);
        this.mMainView.setFileDataListener(this);
        this.mMainView.setMoveToActivity(this);
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.mMainView != null) {
            this.mMainView.onStart();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mMainView != null) {
            this.mMainView.onResume();
        }
        if (this.mAdMakerView != null) {
            this.mAdMakerView.onResume();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.mMainView != null) {
            this.mMainView.onPause();
        }
        if (this.mAdMakerView != null) {
            this.mAdMakerView.onPause();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.mMainView != null) {
            this.mMainView.onStop();
        }
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.mMainView != null) {
            this.mMainView.onDestroy();
        }
        if (this.mAdMakerView != null) {
            this.mAdMakerView.onDestroy();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (this.mMainView != null) {
            this.mMainView.onSaveState(outState);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (this.mMainView != null) {
            this.mMainView.onRestoreState(savedInstanceState);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (this.mMainView != null) {
            this.mMainView.createMenu(menu);
        }
        menu.add(0, 1, 0, (int) R.string.menu_preference).setIcon(17301577);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        if (this.mMainView != null) {
            this.mMainView.showMenu(menu);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (this.mMainView != null && this.mMainView.onMenu(id)) {
            return true;
        }
        switch (id) {
            case 1:
                onMenu_Preference();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onMenu_Preference() {
        startActivityForResult(new Intent(this, MindMemoPreferenceActivity.class), REQUEST_PREFERENCE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode) {
            case REQUEST_PREFERENCE /*2*/:
                onRequestPreference(resultCode, intent);
                return;
            default:
                return;
        }
    }

    private void onRequestPreference(int resultCode, Intent intent) {
        this.mMainView.changePreference();
    }

    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        this.mMainView.onCreateContextMenu(this, menu, view, menuInfo);
        super.onCreateContextMenu(menu, view, menuInfo);
    }

    public boolean onContextItemSelected(MenuItem item) {
        if (this.mMainView.onContextItemSelected(this, item)) {
            return true;
        }
        return super.onContextItemSelected(item);
    }

    public void onContextMenuClosed(Menu menu) {
        this.mMainView.onContextMenuClosed(menu);
        super.onContextMenuClosed(menu);
    }

    public boolean shareText(String textShare) {
        String action;
        if (textShare.contains("@")) {
            textShare = "mailto:" + textShare;
            action = "android.intent.action.SENDTO";
        } else {
            if (!textShare.startsWith("http")) {
                textShare = "http://" + textShare;
            }
            action = "android.intent.action.VIEW";
        }
        try {
            startActivity(Intent.createChooser(new Intent(action, Uri.parse(textShare)), getString(R.string.title_selectapplication)));
            return true;
        } catch (ActivityNotFoundException ex) {
            ex.fillInStackTrace();
            return false;
        }
    }

    public void setFileData(FileData fileData) {
        String appName = getString(R.string.app_name);
        StringBuilder builder = new StringBuilder();
        builder.append(appName);
        if (fileData != null) {
            String fileName = fileData.getFileName();
            builder.append('(');
            builder.append(fileName);
            builder.append(')');
        }
        setTitle(builder.toString());
    }

    public boolean moveToGallery(Uri uri) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(uri, "image/*");
        try {
            startActivity(intent);
            return true;
        } catch (ActivityNotFoundException ex) {
            ex.fillInStackTrace();
            return false;
        }
    }
}
