package com.psc.fukumoto.MindMemo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;

public class SaveImageDialog extends AlertDialog implements DialogInterface.OnClickListener {
    private static final int LAYOUT_RESOURCE = 2130903043;
    private CheckBox mCheckBoxSaveInView;
    private CheckBox mCheckBoxShowGallery;
    private EditText mEditText;
    private OnButtonListener mOnButtonListener = null;

    public interface OnButtonListener {
        void onButtonCancel_SaveImageDialog();

        void onButtonYes_SaveImageDialog(String str, boolean z, boolean z2);
    }

    public SaveImageDialog(Context context, OnButtonListener listener) {
        super(context);
        this.mOnButtonListener = listener;
        setTitle(context.getString(R.string.title_saveimage));
        setIcon(17301582);
        setButton(-1, context.getString(R.string.button_save), this);
        setButton(-3, context.getString(R.string.button_cancel), this);
        View inputTextView = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.dialog_save_image, (ViewGroup) null);
        setView(inputTextView);
        this.mEditText = (EditText) inputTextView.findViewById(R.id.edittext_filename);
        this.mCheckBoxSaveInView = (CheckBox) inputTextView.findViewById(R.id.checkbox_saveinview);
        this.mCheckBoxShowGallery = (CheckBox) inputTextView.findViewById(R.id.checkbox_showgallery);
        setCancelable(true);
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case -3:
                this.mOnButtonListener.onButtonCancel_SaveImageDialog();
                return;
            case -2:
            default:
                return;
            case -1:
                this.mOnButtonListener.onButtonYes_SaveImageDialog(this.mEditText.getText().toString(), this.mCheckBoxSaveInView.isChecked(), this.mCheckBoxShowGallery.isChecked());
                return;
        }
    }
}
