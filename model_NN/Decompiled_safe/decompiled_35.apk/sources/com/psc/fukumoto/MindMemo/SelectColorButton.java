package com.psc.fukumoto.MindMemo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import com.psc.fukumoto.MindMemo.SelectColorDialog;

public class SelectColorButton extends Button implements View.OnClickListener, SelectColorDialog.OnSelectColorListener {
    private static final int COLOR_FRAME = -8355712;
    private static final float FRAME_WIDTH = 3.0f;
    private static final float PADDING = 5.0f;
    private static final float RAIOUS = 10.0f;
    private static final String SELECT_COLOR_TEXT = "        ";
    private int mColorSelected;
    private SelectColorDialog mDialog;
    private String mTitle;

    public SelectColorButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        super.setText(SELECT_COLOR_TEXT);
        setBackgroundDrawable(null);
        setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void onDraw(Canvas canvas) {
        RectF rectDraw = new RectF(PADDING, PADDING, ((float) getWidth()) - PADDING, ((float) getHeight()) - PADDING);
        Paint paintFill = new Paint();
        paintFill.setColor(this.mColorSelected);
        paintFill.setAntiAlias(true);
        paintFill.setStyle(Paint.Style.FILL);
        canvas.drawRoundRect(rectDraw, RAIOUS, RAIOUS, paintFill);
        Paint paintFrame = new Paint();
        paintFrame.setColor((int) COLOR_FRAME);
        paintFrame.setAntiAlias(true);
        paintFrame.setStyle(Paint.Style.STROKE);
        paintFrame.setStrokeWidth(FRAME_WIDTH);
        canvas.drawRoundRect(rectDraw, RAIOUS, RAIOUS, paintFrame);
    }

    /* access modifiers changed from: protected */
    public void setColorSelected(int colorSelected) {
        this.mColorSelected = colorSelected;
        invalidate();
    }

    /* access modifiers changed from: protected */
    public int getColorSelected() {
        return this.mColorSelected;
    }

    /* access modifiers changed from: protected */
    public void setTitle(int id) {
        this.mTitle = getContext().getString(id);
    }

    public void onClick(View v) {
        this.mDialog = new SelectColorDialog(getContext(), this.mTitle, this.mColorSelected);
        this.mDialog.setOnSelectColorListener(this);
        this.mDialog.show();
    }

    public void onSelectColor(int colorSelected) {
        setColorSelected(colorSelected);
    }
}
