package com.psc.fukumoto.MindMemo;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import com.psc.fukumoto.lib.ImageSystem;

public class TextMemoView extends SubView {
    private static final float FONT_SIZE = 16.0f;
    private static final float PADDING = 5.0f;
    private static final float RADIUS = 10.0f;
    private float mBaseline;
    private Paint mPaintBackground;
    private Paint mPaintFrame;
    private Paint mPaintText;
    private float mTextHeight;
    private String[] mTextMemoList;
    private String mTextShare;

    public TextMemoView(PreferenceData preferenceData) {
        createPaint();
        setColorText(preferenceData.getColorText());
        setColorBackground(preferenceData.getColorBackground_TextMemo());
        setColorFrame(preferenceData.getColorFrame_TextMemo());
        setFontSize(FONT_SIZE);
    }

    public TextMemoView(TextMemoViewData textMemoViewData) {
        super(textMemoViewData);
        createPaint();
        setTextMemoViewData(textMemoViewData);
    }

    public TextMemoView(TextMemoView textMemoViewBase, float dx, float dy) {
        super(textMemoViewBase, dx, dy);
        createPaint();
        setColorText(textMemoViewBase.getColorText());
        setColorBackground(textMemoViewBase.getColorBackground());
        setColorFrame(textMemoViewBase.getColorFrame());
        setFontSize(textMemoViewBase.getFontSize());
        setTextMemo(textMemoViewBase.getTextMemo());
        setTextShare(textMemoViewBase.getTextShare());
    }

    private void createPaint() {
        this.mPaintText = new Paint();
        this.mPaintText.setStyle(Paint.Style.FILL);
        this.mPaintText.setAntiAlias(true);
        this.mPaintBackground = new Paint();
        this.mPaintBackground.setStyle(Paint.Style.FILL);
        this.mPaintBackground.setAntiAlias(true);
        this.mPaintFrame = new Paint();
        this.mPaintFrame.setStyle(Paint.Style.STROKE);
        this.mPaintFrame.setAntiAlias(true);
    }

    public SubViewData getSubViewData() {
        return new TextMemoViewData(this);
    }

    public void setTextMemoViewData(TextMemoViewData textMemoViewData) {
        setColorText(textMemoViewData.getColorText());
        setColorBackground(textMemoViewData.getColorBackground());
        setColorFrame(textMemoViewData.getColorFrame());
        setFontSize(textMemoViewData.getFontSize());
        setTextMemo(textMemoViewData.getTextMemo());
        setTextShare(textMemoViewData.getTextShare());
    }

    public int getColorText() {
        return this.mPaintText.getColor();
    }

    public void setColorText(int colorText) {
        this.mPaintText.setColor(colorText);
    }

    public int getColorBackground() {
        return this.mPaintBackground.getColor();
    }

    public void setColorBackground(int colorBackground) {
        this.mPaintBackground.setColor(colorBackground);
    }

    public int getColorFrame() {
        return this.mPaintFrame.getColor();
    }

    public void setColorFrame(int colorFrame) {
        this.mPaintFrame.setColor(colorFrame);
    }

    public float getFontSize() {
        return this.mPaintText.getTextSize();
    }

    public void setFontSize(float fontSize) {
        this.mPaintText.setTextSize(fontSize);
        this.mPaintBackground.setTextSize(fontSize);
        calcRectDraw();
    }

    public String getTextMemo() {
        StringBuilder builder = new StringBuilder();
        int lineNum = this.mTextMemoList.length;
        for (int index = 0; index < lineNum; index++) {
            if (index > 0) {
                builder.append(10);
            }
            builder.append(this.mTextMemoList[index]);
        }
        return builder.toString();
    }

    public void setTextMemo(String textMemo) {
        if (textMemo == null) {
            this.mTextMemoList = null;
            setSize(RADIUS, RADIUS);
            return;
        }
        int length = textMemo.length();
        int lineNum = 1;
        for (int index = 0; index < length; index++) {
            if (textMemo.charAt(index) == 10) {
                lineNum++;
            }
        }
        this.mTextMemoList = new String[lineNum];
        int start = 0;
        int line = 0;
        for (int index2 = 0; index2 < length; index2++) {
            if (textMemo.charAt(index2) == 10) {
                this.mTextMemoList[line] = textMemo.substring(start, index2);
                line++;
                start = index2 + 1;
            }
        }
        this.mTextMemoList[line] = textMemo.substring(start, length);
        calcRectDraw();
    }

    public String getTextShare() {
        return this.mTextShare;
    }

    public void setTextShare(String textShare) {
        this.mTextShare = textShare;
    }

    public void calcRectDraw() {
        int lineNum;
        float maxWidth;
        if (this.mTextMemoList == null) {
            lineNum = 0;
            maxWidth = ImageSystem.ROTATE_NONE;
        } else {
            Paint.FontMetrics fontMetrics = this.mPaintText.getFontMetrics();
            this.mBaseline = -fontMetrics.top;
            this.mTextHeight = (float) Math.ceil((double) (fontMetrics.bottom - fontMetrics.top));
            maxWidth = ImageSystem.ROTATE_NONE;
            for (String data : this.mTextMemoList) {
                float textWidth = this.mPaintText.measureText(data);
                if (maxWidth < textWidth) {
                    maxWidth = textWidth;
                }
            }
        }
        setSize(maxWidth + RADIUS, (this.mTextHeight * ((float) lineNum)) + RADIUS);
    }

    public void draw(Canvas canvas) {
        Paint paintBackground;
        Paint paintText;
        RectF rectDraw = getRectDraw();
        if (getSelected()) {
            paintBackground = this.mPaintText;
            paintText = this.mPaintBackground;
        } else {
            paintBackground = this.mPaintBackground;
            paintText = this.mPaintText;
        }
        canvas.drawRoundRect(rectDraw, RADIUS, RADIUS, paintBackground);
        if (this.mTextMemoList != null) {
            float left = rectDraw.left + PADDING;
            float top = rectDraw.top + PADDING + this.mBaseline;
            for (String data : this.mTextMemoList) {
                canvas.drawText(data, left, top, paintText);
                top += this.mTextHeight;
            }
        }
        canvas.drawRoundRect(rectDraw, RADIUS, RADIUS, this.mPaintFrame);
    }

    public void clearAll() {
        super.clearAll();
        this.mTextMemoList = null;
    }
}
