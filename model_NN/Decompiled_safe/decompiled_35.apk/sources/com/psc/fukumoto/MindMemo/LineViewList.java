package com.psc.fukumoto.MindMemo;

import java.util.ArrayList;

public class LineViewList extends ArrayList<LineView> {
    private static final long serialVersionUID = 1;

    public void setLineViewDataList(LineViewData[] lineViewDataList, SubViewList subViewList) {
        clearAll();
        if (lineViewDataList != null) {
            for (LineViewData lineViewData : lineViewDataList) {
                add(lineViewData.createLineView(subViewList.searchSubView(lineViewData.getSubView1Id()), subViewList.searchSubView(lineViewData.getSubView2Id())));
            }
        }
    }

    public LineViewData[] getLineViewDataList() {
        int lineViewNum = size();
        LineViewData[] lineViewDataList = new LineViewData[lineViewNum];
        for (int index = 0; index < lineViewNum; index++) {
            lineViewDataList[index] = ((LineView) get(index)).getLineViewData();
        }
        return lineViewDataList;
    }

    public void removeSubView(SubView subView) {
        for (int index = size() - 1; index >= 0; index--) {
            if (((LineView) get(index)).isMember(subView)) {
                remove(index);
            }
        }
    }

    public void clearAll() {
        int lineViewNum = size();
        for (int index = 0; index < lineViewNum; index++) {
            ((LineView) get(index)).clearAll();
        }
        clear();
    }
}
