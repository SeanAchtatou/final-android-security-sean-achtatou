package com.psc.fukumoto.MindMemo;

import java.io.Serializable;

public class LineViewData implements Serializable {
    private static final long serialVersionUID = 1;
    private int mColorLine;
    private int mSubView1Id;
    private int mSubView2Id;

    public LineViewData(LineView lineView) {
        this.mSubView1Id = lineView.getSubView1Id();
        this.mSubView2Id = lineView.getSubView2Id();
        this.mColorLine = lineView.getColorLine();
    }

    public LineView createLineView(SubView subView1, SubView subView2) {
        return new LineView(subView1, subView2, this.mColorLine);
    }

    public int getSubView1Id() {
        return this.mSubView1Id;
    }

    public int getSubView2Id() {
        return this.mSubView2Id;
    }

    public int getColorLine() {
        return this.mColorLine;
    }
}
