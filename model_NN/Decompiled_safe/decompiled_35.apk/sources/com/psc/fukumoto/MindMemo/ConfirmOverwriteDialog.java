package com.psc.fukumoto.MindMemo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.psc.fukumoto.lib.FileData;

public class ConfirmOverwriteDialog extends AlertDialog implements DialogInterface.OnClickListener {
    private FileData mFileData;
    private OnButtonListener mOnButtonListener = null;

    public interface OnButtonListener {
        void onButtonCancel_ConfirmOverrideDialog();

        void onButtonYes_ConfirmOverrideDialog(FileData fileData);
    }

    public ConfirmOverwriteDialog(Context context, OnButtonListener listener, FileData fileData) {
        super(context);
        this.mOnButtonListener = listener;
        this.mFileData = fileData;
        setTitle(fileData.getFileName());
        setIcon(17301582);
        setMessage(context.getString(R.string.message_confirmoverwrite));
        setButton(-1, context.getString(R.string.button_yes), this);
        setButton(-2, context.getString(R.string.button_no), this);
        setCancelable(true);
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case -3:
                this.mOnButtonListener.onButtonCancel_ConfirmOverrideDialog();
                return;
            case -2:
            default:
                return;
            case -1:
                this.mOnButtonListener.onButtonYes_ConfirmOverrideDialog(this.mFileData);
                return;
        }
    }
}
