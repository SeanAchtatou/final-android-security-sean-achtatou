package com.psc.fukumoto.MindMemo;

public class TextMemoViewData extends SubViewData {
    private static final long serialVersionUID = 1;
    private int mColorBackground;
    private int mColorFrame;
    private int mColorText;
    private float mFontSize;
    private String mTextMemo;
    private String mTextShare;

    public TextMemoViewData(TextMemoView textMemoView) {
        super(textMemoView);
        this.mFontSize = textMemoView.getFontSize();
        this.mColorText = textMemoView.getColorText();
        this.mColorBackground = textMemoView.getColorBackground();
        this.mColorFrame = textMemoView.getColorFrame();
        this.mTextMemo = textMemoView.getTextMemo();
        this.mTextShare = textMemoView.getTextShare();
    }

    public TextMemoViewData(int fontSize, int colorText, int colorBackground, int colorFrame, String textMemo, String textShare) {
        super(null);
        this.mFontSize = (float) fontSize;
        this.mColorText = colorText;
        this.mColorBackground = colorBackground;
        this.mColorFrame = colorFrame;
        this.mTextMemo = textMemo;
        this.mTextShare = textShare;
    }

    public SubView createSubView() {
        return new TextMemoView(this);
    }

    public float getFontSize() {
        return this.mFontSize;
    }

    public int getColorText() {
        return this.mColorText;
    }

    public int getColorBackground() {
        return this.mColorBackground;
    }

    public int getColorFrame() {
        return this.mColorFrame;
    }

    public String getTextMemo() {
        return this.mTextMemo;
    }

    public String getTextShare() {
        return this.mTextShare;
    }
}
