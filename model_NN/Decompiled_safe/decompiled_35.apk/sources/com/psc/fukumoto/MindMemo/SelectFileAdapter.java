package com.psc.fukumoto.MindMemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import com.psc.fukumoto.lib.FileData;
import java.util.List;

public class SelectFileAdapter extends ArrayAdapter<FileData> implements AdapterView.OnItemClickListener {
    private static final int LAYOUT_RESOURCE = 2130903044;
    private int mCount;
    private LayoutInflater mInflater;

    public SelectFileAdapter(Context context, List<FileData> fileDataList) {
        super(context, (int) R.layout.dialog_select_file, fileDataList);
        if (fileDataList != null) {
            this.mCount = fileDataList.size();
        } else {
            this.mCount = 0;
        }
        this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public int getCount() {
        return this.mCount;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public FileData getItem(int position) {
        if (position < 0 || this.mCount <= position) {
            return null;
        }
        return (FileData) super.getItem(position);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = this.mInflater.inflate((int) R.layout.dialog_select_file, parent, false);
        } else {
            view = convertView;
        }
        ((CheckedTextView) view.findViewById(R.id.filename)).setText(getItem(position).getFileName());
        return view;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
    }
}
