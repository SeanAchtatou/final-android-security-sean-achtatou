package com.psc.fukumoto.MindMemo;

import java.io.Serializable;

public class MemoViewData implements Serializable {
    private static final long serialVersionUID = 1;
    private int mColorBackground;
    private int mColorLine;
    private float mDrawX;
    private float mDrawY;
    private float mScale;
    private SubViewData[] mSubViewDataList;

    public MemoViewData(MemoView memoView) {
        this.mSubViewDataList = memoView.getSubViewDataList();
        this.mColorBackground = memoView.getColorBackground();
        this.mColorLine = memoView.getColorLine();
        this.mDrawX = memoView.getDrawX();
        this.mDrawY = memoView.getDrawY();
        this.mScale = memoView.getScale();
    }

    public SubViewData[] getSubViewDataList() {
        return this.mSubViewDataList;
    }

    public int getColorBackground() {
        return this.mColorBackground;
    }

    public int getColorLine() {
        return this.mColorLine;
    }

    public float getDrawX() {
        return this.mDrawX;
    }

    public float getDrawY() {
        return this.mDrawY;
    }

    public float getScale() {
        return this.mScale;
    }
}
