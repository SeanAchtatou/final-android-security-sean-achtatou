package com.psc.fukumoto.MindMemo;

import android.graphics.Canvas;
import android.graphics.Paint;

public class LineView {
    private Paint mPaintLine;
    private SubView mSubView1;
    private SubView mSubView2;

    public LineView(SubView subView1, SubView subView2, int colorLine) {
        this.mSubView1 = subView1;
        this.mSubView2 = subView2;
        setColor(colorLine);
    }

    public LineViewData getLineViewData() {
        return new LineViewData(this);
    }

    public int getSubView1Id() {
        return this.mSubView1.getId();
    }

    public int getSubView2Id() {
        return this.mSubView2.getId();
    }

    public int getColorLine() {
        return this.mPaintLine.getColor();
    }

    public void setColor(int colorLine) {
        this.mPaintLine = new Paint();
        this.mPaintLine.setColor(colorLine);
        this.mPaintLine.setStyle(Paint.Style.STROKE);
        this.mPaintLine.setAntiAlias(true);
    }

    public void draw(Canvas canvas) {
        if (this.mSubView1 != null && this.mSubView2 != null) {
            canvas.drawLine(this.mSubView1.getCenterX(), this.mSubView1.getCenterY(), this.mSubView2.getCenterX(), this.mSubView2.getCenterY(), this.mPaintLine);
        }
    }

    public boolean equal(LineView lineView) {
        if (this.mSubView1 == lineView.mSubView1 && this.mSubView2 == lineView.mSubView2) {
            return true;
        }
        if (this.mSubView1 == lineView.mSubView2 && this.mSubView2 == lineView.mSubView1) {
            return true;
        }
        return false;
    }

    public boolean isMember(SubView subView) {
        if (this.mSubView1 == subView) {
            return true;
        }
        if (this.mSubView2 == subView) {
            return true;
        }
        return false;
    }

    public void clearAll() {
        this.mSubView1 = null;
        this.mSubView2 = null;
    }
}
