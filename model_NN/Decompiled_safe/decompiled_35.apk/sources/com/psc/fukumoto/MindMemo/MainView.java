package com.psc.fukumoto.MindMemo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.ZoomControls;
import com.psc.fukumoto.MindMemo.ConfirmClearDialog;
import com.psc.fukumoto.MindMemo.ConfirmDeleteDialog;
import com.psc.fukumoto.MindMemo.ConfirmOverwriteDialog;
import com.psc.fukumoto.MindMemo.MemoView;
import com.psc.fukumoto.MindMemo.SaveFileDialog;
import com.psc.fukumoto.MindMemo.SaveImageDialog;
import com.psc.fukumoto.MindMemo.SelectFileDialog;
import com.psc.fukumoto.adlib.AdView;
import com.psc.fukumoto.lib.FileData;
import com.psc.fukumoto.lib.ImageSystem;
import java.util.List;

public class MainView extends RelativeLayout implements View.OnClickListener, SaveFileDialog.OnButtonListener, SaveImageDialog.OnButtonListener, SelectFileDialog.OnButtonListener, ConfirmOverwriteDialog.OnButtonListener, ConfirmDeleteDialog.OnButtonListener, ConfirmClearDialog.OnButtonListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$psc$fukumoto$MindMemo$MemoView$TOUCH_MODE = null;
    private static final int FILL_PARENT = -1;
    private static final int WRAP_CONTENT = -2;
    private AdView mAdView = null;
    private ImageButton mButtonSave = null;
    private ImageButton mButtonTouchMode = null;
    private AlertDialog mDialog = null;
    private FileData mFileData;
    private FileDataListener mFileDataListener = null;
    private int mHeightMeasureSpec = 0;
    private int mMemoHeight;
    /* access modifiers changed from: private */
    public MemoView mMemoView = null;
    private int mMemoWidth;
    private MoveToActivity mMoveToActivity = null;
    private PreferenceData mPreferenceData;
    private int mWidthMeasureSpec = 0;
    private ZoomControls mZoomControls;

    public interface FileDataListener {
        void setFileData(FileData fileData);
    }

    public interface MoveToActivity {
        boolean moveToGallery(Uri uri);
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$psc$fukumoto$MindMemo$MemoView$TOUCH_MODE() {
        int[] iArr = $SWITCH_TABLE$com$psc$fukumoto$MindMemo$MemoView$TOUCH_MODE;
        if (iArr == null) {
            iArr = new int[MemoView.TOUCH_MODE.values().length];
            try {
                iArr[MemoView.TOUCH_MODE.ADD.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[MemoView.TOUCH_MODE.MOVE.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$com$psc$fukumoto$MindMemo$MemoView$TOUCH_MODE = iArr;
        }
        return iArr;
    }

    public void setFileDataListener(FileDataListener listener) {
        this.mFileDataListener = listener;
        listener.setFileData(this.mFileData);
    }

    public void setMoveToActivity(MoveToActivity moveToActivity) {
        this.mMoveToActivity = moveToActivity;
    }

    public void setShareListener(MemoView.ShareListener listener) {
        this.mMemoView.setShareListener(listener);
    }

    public MainView(Context context, AdView adView, FileData fileData) {
        super(context);
        boolean result;
        this.mAdView = adView;
        this.mPreferenceData = new PreferenceData(context);
        Resources resources = context.getResources();
        setLayoutParams(new ViewGroup.LayoutParams((int) FILL_PARENT, (int) FILL_PARENT));
        setBackgroundColor(resources.getColor(R.color.MainView_background));
        createChildView(context);
        if (fileData != null) {
            result = loadFile(fileData);
        } else {
            result = false;
        }
        if (!result) {
            this.mMemoView.setMemoViewData(MindMemoFile.loadTempFile(getContext()));
        }
        registerForContextMenu(context);
        loadPreference(this.mPreferenceData);
    }

    public void onStart() {
    }

    public void onResume() {
        if (this.mMemoView != null) {
            this.mMemoView.onResume();
        }
    }

    public void onPause() {
        if (this.mDialog != null) {
            this.mDialog.dismiss();
            this.mDialog = null;
        }
        if (this.mMemoView != null) {
            this.mMemoView.onPause();
        }
    }

    public void onStop() {
    }

    public void onDestroy() {
        savePreference(this.mPreferenceData);
        if (this.mMemoView != null) {
            MindMemoFile.saveTempFile(getContext(), new MemoViewData(this.mMemoView));
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveState(Bundle outState) {
        if (this.mMemoView != null) {
            this.mMemoView.onSaveState(outState);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreState(Bundle savedState) {
        if (this.mMemoView != null) {
            this.mMemoView.onRestoreState(savedState);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int width, int height, int oldw, int oldh) {
        super.onSizeChanged(width, height, oldw, oldh);
        layoutChildView(width, height, getResources().getConfiguration().orientation);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        this.mWidthMeasureSpec = widthMeasureSpec;
        this.mHeightMeasureSpec = heightMeasureSpec;
    }

    private void createChildView(Context context) {
        if (this.mAdView != null) {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) FILL_PARENT, (int) FILL_PARENT);
            this.mAdView.setId(R.integer.mIdAdView);
            addViewInLayout(this.mAdView, FILL_PARENT, params);
        }
        RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams((int) WRAP_CONTENT, (int) WRAP_CONTENT);
        this.mMemoView = new MemoView(context);
        this.mMemoView.setId(R.integer.mIdMemoView);
        addView(this.mMemoView, (int) FILL_PARENT, params2);
        RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams((int) WRAP_CONTENT, (int) WRAP_CONTENT);
        this.mZoomControls = new ZoomControls(context);
        this.mZoomControls.setId(R.integer.mIdZoomControls);
        addViewInLayout(this.mZoomControls, FILL_PARENT, params3);
        this.mZoomControls.setOnZoomInClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainView.this.mMemoView.zoomIn();
            }
        });
        this.mZoomControls.setOnZoomOutClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainView.this.mMemoView.zoomOut();
            }
        });
        RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams((int) WRAP_CONTENT, (int) WRAP_CONTENT);
        this.mButtonTouchMode = new ImageButton(context);
        this.mButtonTouchMode.setId(R.integer.mIdButtonTouchMode);
        addViewInLayout(this.mButtonTouchMode, FILL_PARENT, params4);
        this.mButtonTouchMode.setOnClickListener(this);
        setButtonBackground(this.mMemoView.getTouchMode());
        RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams((int) WRAP_CONTENT, (int) WRAP_CONTENT);
        this.mButtonSave = new ImageButton(context);
        this.mButtonSave.setId(R.integer.mIdButtonSave);
        addViewInLayout(this.mButtonSave, FILL_PARENT, params5);
        this.mButtonSave.setOnClickListener(this);
        this.mButtonSave.setBackgroundResource(R.drawable.button_save);
    }

    private boolean layoutChildView(int width, int height, int orientation) {
        if (this.mAdView != null) {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) FILL_PARENT, (int) WRAP_CONTENT);
            params.addRule(10);
            params.addRule(9);
            updateViewLayout(this.mAdView, params);
        }
        RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams((int) FILL_PARENT, (int) FILL_PARENT);
        if (this.mAdView != null) {
            params2.addRule(3, R.integer.mIdAdView);
        } else {
            params2.addRule(10);
        }
        params2.addRule(12);
        params2.addRule(14);
        updateViewLayout(this.mMemoView, params2);
        RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams((int) WRAP_CONTENT, (int) WRAP_CONTENT);
        params3.addRule(12);
        params3.addRule(13);
        updateViewLayout(this.mZoomControls, params3);
        RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams((int) WRAP_CONTENT, (int) WRAP_CONTENT);
        params4.addRule(12);
        params4.addRule(9);
        updateViewLayout(this.mButtonTouchMode, params4);
        RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams((int) WRAP_CONTENT, (int) WRAP_CONTENT);
        params5.addRule(12);
        params5.addRule(11);
        updateViewLayout(this.mButtonSave, params5);
        requestLayout();
        measure(this.mWidthMeasureSpec, this.mHeightMeasureSpec);
        layout(0, 0, width, height);
        return true;
    }

    private void setFileData(FileData fileData) {
        this.mFileData = fileData;
        if (this.mFileDataListener != null) {
            this.mFileDataListener.setFileData(fileData);
        }
    }

    public void createMenu(Menu menu) {
        menu.add(0, 2, 0, (int) R.string.menu_new);
        menu.add(0, 3, 0, (int) R.string.menu_selectfile);
        menu.add(0, 4, 0, (int) R.string.menu_saveimage).setIcon(17301567);
    }

    public void showMenu(Menu menu) {
    }

    public boolean onMenu(int id) {
        switch (id) {
            case 2:
                onMenu_New();
                return true;
            case 3:
                onMenu_SelectFile();
                return true;
            case 4:
                onMenu_SaveImage();
                return true;
            default:
                return false;
        }
    }

    private void onMenu_New() {
        showConfirmClearDialog();
    }

    private void onMenu_SelectFile() {
        showSelectFileDialog(MindMemoFile.getFileDataList(getContext(), null));
    }

    private void onMenu_SaveImage() {
        showSaveImageDialog();
    }

    private void loadPreference(PreferenceData preferenceData) {
        MemoView.TOUCH_MODE touchMode;
        if (preferenceData.getTouchModeIsMove()) {
            touchMode = MemoView.TOUCH_MODE.MOVE;
        } else {
            touchMode = MemoView.TOUCH_MODE.ADD;
        }
        this.mMemoView.setTouchMode(touchMode);
        setButtonBackground(touchMode);
        if (this.mFileData == null) {
            this.mFileData = preferenceData.getFileData();
        }
        this.mMemoView.loadPreference(preferenceData);
    }

    private void savePreference(PreferenceData preferenceData) {
        if (this.mMemoView.getTouchMode() == MemoView.TOUCH_MODE.MOVE) {
            preferenceData.setTouchModeIsMove(true);
        } else {
            preferenceData.setTouchModeIsMove(false);
        }
        preferenceData.setFileData(this.mFileData);
        this.mMemoView.savePreference(preferenceData);
    }

    /* access modifiers changed from: protected */
    public void changePreference() {
        this.mMemoView.changePreference(this.mPreferenceData);
    }

    public void registerForContextMenu(Context context) {
        ((Activity) context).registerForContextMenu(this.mMemoView);
    }

    public void onCreateContextMenu(Context context, ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        switch (view.getId()) {
            case R.integer.mIdMemoView /*2131099648*/:
                this.mMemoView.onCreateContextMenu(context, menu, menuInfo);
                return;
            default:
                return;
        }
    }

    public boolean onContextItemSelected(Context context, MenuItem item) {
        return this.mMemoView.onMenu(item.getItemId());
    }

    public void onContextMenuClosed(Menu menu) {
        this.mMemoView.onMenuClosed();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.integer.mIdButtonTouchMode /*2131099649*/:
                onClick_ButtonTouchMode();
                return;
            case R.integer.mIdButtonSave /*2131099650*/:
                onClick_ButtonSave();
                return;
            default:
                return;
        }
    }

    private void onClick_ButtonTouchMode() {
        MemoView.TOUCH_MODE touchMode;
        switch ($SWITCH_TABLE$com$psc$fukumoto$MindMemo$MemoView$TOUCH_MODE()[this.mMemoView.getTouchMode().ordinal()]) {
            case 2:
                touchMode = MemoView.TOUCH_MODE.MOVE;
                break;
            default:
                touchMode = MemoView.TOUCH_MODE.ADD;
                break;
        }
        this.mMemoView.setTouchMode(touchMode);
        setButtonBackground(touchMode);
    }

    /* access modifiers changed from: protected */
    public void setButtonBackground(MemoView.TOUCH_MODE touchMode) {
        int id;
        switch ($SWITCH_TABLE$com$psc$fukumoto$MindMemo$MemoView$TOUCH_MODE()[touchMode.ordinal()]) {
            case 2:
                id = R.drawable.button_touchadd;
                break;
            default:
                id = R.drawable.button_touchmove;
                break;
        }
        this.mButtonTouchMode.setBackgroundResource(id);
    }

    private void onClick_ButtonSave() {
        showSaveFileDialog(this.mFileData);
    }

    private void showSaveFileDialog(FileData fileData) {
        this.mDialog = new SaveFileDialog(getContext(), this, fileData);
        this.mDialog.show();
    }

    private void showSaveImageDialog() {
        this.mMemoWidth = this.mMemoView.getWidth();
        this.mMemoHeight = this.mMemoView.getHeight();
        this.mDialog = new SaveImageDialog(getContext(), this);
        this.mDialog.show();
    }

    private void showSelectFileDialog(List<FileData> fileDataList) {
        this.mDialog = new SelectFileDialog(getContext(), this, fileDataList);
        this.mDialog.show();
    }

    private void showConfirmOverwriteDialog(FileData fileData) {
        this.mDialog = new ConfirmOverwriteDialog(getContext(), this, fileData);
        this.mDialog.show();
    }

    private void showConfirmDeleteDialog(FileData fileData) {
        this.mDialog = new ConfirmDeleteDialog(getContext(), this, fileData);
        this.mDialog.show();
    }

    private void showConfirmClearDialog() {
        this.mDialog = new ConfirmClearDialog(getContext(), this);
        this.mDialog.show();
    }

    public void onButtonYes_SaveFileDialog(FileData fileData) {
        boolean result;
        int id;
        if (fileData != null) {
            if (fileData.isSame(this.mFileData)) {
                result = saveFile(fileData);
            } else if (!fileData.exist()) {
                result = saveFile(fileData);
            } else if (!fileData.isFolder()) {
                showConfirmOverwriteDialog(fileData);
                return;
            } else {
                result = false;
            }
            Context context = getContext();
            if (result) {
                id = R.string.message_finishsave;
            } else {
                id = R.string.message_failedsave;
            }
            Toast.makeText(context, id, 0).show();
        }
    }

    public void onButtonCancel_SaveFileDialog() {
    }

    public void onButtonYes_SaveImageDialog(String fileName, boolean saveInView, boolean showGallery) {
        int id;
        if (fileName != null && fileName.length() != 0) {
            Uri uri = saveFileAsImage(MindMemoFile.getDefaultFolderName(), fileName, saveInView);
            Context context = getContext();
            if (uri != null) {
                id = R.string.message_finishsave;
            } else {
                id = R.string.message_failedsave;
            }
            Toast.makeText(context, id, 0).show();
            if (uri != null && showGallery) {
                this.mMoveToActivity.moveToGallery(uri);
            }
        }
    }

    public void onButtonCancel_SaveImageDialog() {
    }

    public void onButtonYes_SelectFileDialog(FileData fileData) {
        if (fileData != null && !loadFile(fileData)) {
            Toast.makeText(getContext(), (int) R.string.message_failedload, 0).show();
        }
    }

    public void onButtonNo_SelectFileDialog(FileData fileData) {
        if (fileData != null) {
            showConfirmDeleteDialog(fileData);
        }
    }

    public void onButtonCancel_SelectFileDialog() {
    }

    public void onButtonYes_ConfirmOverrideDialog(FileData fileData) {
        saveFile(fileData);
    }

    public void onButtonCancel_ConfirmOverrideDialog() {
    }

    public void onButtonYes_ConfirmDeleteDialog(FileData fileData) {
        if (fileData != null) {
            deleteFile(fileData);
        }
    }

    public void onButtonCancel_ConfirmDeleteDialog() {
    }

    public void onButtonYes_ConfirmClearDialog() {
        this.mMemoView.clearAll();
        setFileData(null);
    }

    public void onButtonCancel_ConfirmClearDialog() {
    }

    private Uri saveFileAsImage(String folderName, String fileName, boolean saveInView) {
        Bitmap bitmap = this.mMemoView.createBitmap(saveInView, this.mMemoWidth, this.mMemoHeight);
        if (bitmap == null) {
            return null;
        }
        return ImageSystem.saveImageFile_Png(folderName, String.valueOf(fileName) + ".png", bitmap);
    }

    private boolean saveFile(FileData fileData) {
        setFileData(fileData);
        return MindMemoFile.saveMemoFile(fileData, new MemoViewData(this.mMemoView));
    }

    private boolean loadFile(FileData fileData) {
        if (fileData == null) {
            return false;
        }
        MemoViewData memoViewData = MindMemoFile.loadMemoFile(fileData);
        if (memoViewData == null) {
            Toast.makeText(getContext(), (int) R.string.message_failedload, 0).show();
            return false;
        }
        this.mMemoView.setMemoViewData(memoViewData);
        setFileData(fileData);
        return true;
    }

    private void deleteFile(FileData fileData) {
        MindMemoFile.deleteMemoFile(fileData);
    }
}
