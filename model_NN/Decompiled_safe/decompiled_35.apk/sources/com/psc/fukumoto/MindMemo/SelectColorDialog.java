package com.psc.fukumoto.MindMemo;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;

public class SelectColorDialog extends AlertDialog implements AdapterView.OnItemClickListener {
    private static final int[] COLOR_ID_LIST = {R.drawable.color_000000, R.drawable.color_ffffff, R.drawable.color_e3991b, R.drawable.color_ee7171, R.drawable.color_71ee71, R.drawable.color_7171ee, R.drawable.color_eeee71, R.drawable.color_ee71ee, R.drawable.color_71eeee};
    private static final int[] COLOR_LIST = {-16777216, -1, -1861349, -1150607, -9310607, -9342482, -1118607, -1150482, -9310482};
    protected static final int COLUMN_NUM = 3;
    protected static final int SPACING_H = 10;
    protected static final int SPACING_V = 10;
    private OnSelectColorListener mOnSelectColorListener = null;

    public interface OnSelectColorListener {
        void onSelectColor(int i);
    }

    public void setOnSelectColorListener(OnSelectColorListener listener) {
        this.mOnSelectColorListener = listener;
    }

    public SelectColorDialog(Context context, String title, int colorSelected) {
        super(context);
        setTitle(title);
        GridView gridView = new GridView(context);
        gridView.setNumColumns(COLUMN_NUM);
        gridView.setHorizontalSpacing(10);
        gridView.setVerticalSpacing(10);
        SelectResourceImageAdapter adapter = new SelectResourceImageAdapter(context, COLOR_ID_LIST);
        gridView.setAdapter((ListAdapter) adapter);
        gridView.setOnItemClickListener(this);
        int colorNum = COLOR_LIST.length;
        int position = 0;
        while (true) {
            if (position >= colorNum) {
                break;
            } else if (colorSelected == COLOR_LIST[position]) {
                adapter.setSelection(position);
                break;
            } else {
                position++;
            }
        }
        setView(gridView);
        setCancelable(true);
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        int colorSelected = COLOR_LIST[position];
        if (this.mOnSelectColorListener != null) {
            this.mOnSelectColorListener.onSelectColor(colorSelected);
        }
        dismiss();
    }
}
