package com.psc.fukumoto.MindMemo;

import java.util.ArrayList;

public class SubViewList extends ArrayList<SubView> {
    private static final long serialVersionUID = 1;

    public SubViewData[] getSubViewDataList() {
        int subViewNum = size();
        SubViewData[] subViewDataList = new SubViewData[subViewNum];
        for (int index = 0; index < subViewNum; index++) {
            subViewDataList[index] = ((SubView) get(index)).getSubViewData();
        }
        return subViewDataList;
    }

    public void setSubViewDataList(SubViewData[] subViewDataList, GroupView parentView) {
        clearAll();
        if (subViewDataList != null) {
            for (SubViewData subViewData : subViewDataList) {
                SubView subView = subViewData.createSubView();
                if (subView != null) {
                    if (parentView != null) {
                        parentView.addSubView(subView);
                    } else {
                        add(subView);
                        subView.setParentView(null);
                    }
                }
            }
        }
    }

    public SubView searchSubView(int id) {
        int subViewNum = size();
        for (int index = 0; index < subViewNum; index++) {
            SubView subView = (SubView) get(index);
            if (subView.getId() == id) {
                return subView;
            }
        }
        return null;
    }

    public int searchSubViewIndex(int id) {
        int subViewNum = size();
        for (int index = 0; index < subViewNum; index++) {
            if (((SubView) get(index)).getId() == id) {
                return index;
            }
        }
        return -1;
    }

    public void removeSubView(SubView subView) {
        remove(subView);
        subView.setParentView(null);
    }

    public void clearAll() {
        int subViewNum = size();
        for (int index = 0; index < subViewNum; index++) {
            ((SubView) get(index)).clearAll();
        }
        clear();
    }
}
