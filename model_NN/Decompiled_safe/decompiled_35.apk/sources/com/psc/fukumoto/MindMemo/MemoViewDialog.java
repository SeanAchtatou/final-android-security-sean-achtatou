package com.psc.fukumoto.MindMemo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MemoViewDialog extends AlertDialog implements DialogInterface.OnClickListener {
    private static final int LAYOUT_RESOURCE = 2130903041;
    private SelectColorButton mButtonColorBackground;
    private SelectColorButton mButtonColorLine;
    private OnButtonListener mOnButtonListener = null;

    public interface OnButtonListener {
        void onButtonCancel_MemoViewDialog();

        void onButtonYes_MemoViewDialog(int i, int i2);
    }

    public MemoViewDialog(Context context, OnButtonListener listener) {
        super(context);
        this.mOnButtonListener = listener;
        setTitle(context.getString(R.string.title_editmemo));
        setButton(-1, context.getString(R.string.button_set), this);
        setButton(-3, context.getString(R.string.button_cancel), this);
        View inputTextView = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.dialog_memo_view, (ViewGroup) null);
        setView(inputTextView);
        this.mButtonColorBackground = (SelectColorButton) inputTextView.findViewById(R.id.button_colorbackground);
        this.mButtonColorBackground.setTitle(R.string.title_colorbackground);
        this.mButtonColorLine = (SelectColorButton) inputTextView.findViewById(R.id.button_colorline);
        this.mButtonColorLine.setTitle(R.string.title_colorline);
        setCancelable(true);
    }

    /* access modifiers changed from: protected */
    public void setColor(int colorBackground, int colorLine) {
        this.mButtonColorBackground.setColorSelected(colorBackground);
        this.mButtonColorLine.setColorSelected(colorLine);
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case -3:
                this.mOnButtonListener.onButtonCancel_MemoViewDialog();
                return;
            case -2:
            default:
                return;
            case -1:
                onClick_ButtonYes();
                return;
        }
    }

    private void onClick_ButtonYes() {
        this.mOnButtonListener.onButtonYes_MemoViewDialog(this.mButtonColorBackground.getColorSelected(), this.mButtonColorLine.getColorSelected());
    }
}
