package com.psc.fukumoto.MindMemo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.psc.fukumoto.lib.FileData;

public class SaveFileDialog extends AlertDialog implements DialogInterface.OnClickListener {
    private static final int LAYOUT_RESOURCE = 2130903042;
    private EditText mEditText;
    private FileData mFileData;
    private OnButtonListener mOnButtonListener = null;

    public interface OnButtonListener {
        void onButtonCancel_SaveFileDialog();

        void onButtonYes_SaveFileDialog(FileData fileData);
    }

    public SaveFileDialog(Context context, OnButtonListener listener, FileData fileData) {
        super(context);
        String folderName;
        this.mOnButtonListener = listener;
        setTitle(context.getString(R.string.title_savefile));
        setIcon(17301582);
        setButton(-1, context.getString(R.string.button_save), this);
        setButton(-3, context.getString(R.string.button_cancel), this);
        View inputTextView = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.dialog_save_file, (ViewGroup) null);
        setView(inputTextView);
        if (fileData != null) {
            folderName = fileData.getFolderName();
        } else {
            folderName = MindMemoFile.getDefaultFolderName();
        }
        ((TextView) inputTextView.findViewById(R.id.text_foldername)).setText(folderName);
        this.mEditText = (EditText) inputTextView.findViewById(R.id.edittext_filename);
        this.mFileData = fileData;
        if (fileData != null) {
            String fileName = fileData.getFileName();
            this.mEditText.setText(fileData.getFileName());
            this.mEditText.setSelection(fileName.length());
        }
        setCancelable(true);
    }

    private FileData getFileData() {
        FileData fileData;
        String fileName = this.mEditText.getText().toString();
        if (fileName == null || fileName.length() == 0) {
            return null;
        }
        if (this.mFileData == null) {
            fileData = MindMemoFile.getMemoFileName(getContext(), fileName);
        } else {
            fileData = new FileData(this.mFileData.getFolderName(), fileName, MindMemoFile.DATA_FILE_EXTENSION);
        }
        return fileData;
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case -3:
                this.mOnButtonListener.onButtonCancel_SaveFileDialog();
                return;
            case -2:
            default:
                return;
            case -1:
                this.mOnButtonListener.onButtonYes_SaveFileDialog(getFileData());
                return;
        }
    }
}
