package com.psc.fukumoto.MindMemo;

public class GroupViewData extends SubViewData {
    private static final long serialVersionUID = 1;
    private int mColorBackground;
    private int mColorFrame;
    private int mColorLine;
    private LineViewData[] mLineViewDataList;
    private SubViewData[] mSubViewDataList;
    private boolean mTransparent;

    public GroupViewData(GroupView groupView) {
        super(groupView);
        this.mSubViewDataList = groupView.getSubViewDataList();
        this.mLineViewDataList = groupView.getLineViewDataList();
        this.mTransparent = groupView.getTransparent();
        this.mColorBackground = groupView.getColorBackground();
        this.mColorFrame = groupView.getColorFrame();
        this.mColorLine = groupView.getColorLine();
    }

    public GroupViewData(boolean transparent, int colorBackground, int colorFrame, int colorLine) {
        super(null);
        this.mTransparent = transparent;
        this.mColorBackground = colorBackground;
        this.mColorFrame = colorFrame;
        this.mColorLine = colorLine;
    }

    public SubView createSubView() {
        if (this.mSubViewDataList.length == 0) {
            return null;
        }
        return new GroupView(this);
    }

    public SubViewData[] getSubViewDataList() {
        return this.mSubViewDataList;
    }

    public LineViewData[] getLineViewDataList() {
        return this.mLineViewDataList;
    }

    public boolean getTransparent() {
        return this.mTransparent;
    }

    public int getColorBackground() {
        return this.mColorBackground;
    }

    public int getColorFrame() {
        return this.mColorFrame;
    }

    public int getColorLine() {
        return this.mColorLine;
    }
}
