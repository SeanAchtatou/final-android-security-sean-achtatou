package com.psc.fukumoto.MindMemo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.PointF;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.psc.fukumoto.MindMemo.TextMemoViewDialog;

public class TextMemoViewFullDialog extends TextMemoViewDialog implements DialogInterface.OnClickListener {
    private static final int LAYOUT_RESOURCE = 2130903046;
    private SelectColorButton mButtonColorBackground;
    private SelectColorButton mButtonColorFrame;
    private SelectColorButton mButtonColorText;
    private EditText mEditTextMemo;
    private EditText mEditTextShare;
    private Spinner mSpinnerFontSize;
    private TextMemoView mTextMemoView;

    public TextMemoViewFullDialog(Context context, TextMemoViewDialog.OnButtonListener listener, PointF pointCenter, TextMemoView textMemoView) {
        super(context, listener, pointCenter);
        int id;
        String buttonYes;
        this.mTextMemoView = textMemoView;
        if (textMemoView != null) {
            id = R.string.title_editview;
        } else {
            id = R.string.title_createview;
        }
        setTitle(context.getString(id));
        if (textMemoView != null) {
            buttonYes = context.getString(R.string.button_set);
        } else {
            buttonYes = context.getString(R.string.button_create);
        }
        setButton(-1, buttonYes, this);
        setButton(-3, context.getString(R.string.button_cancel), this);
        View inputTextView = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.dialog_text_memo_view_full, (ViewGroup) null);
        setView(inputTextView);
        this.mSpinnerFontSize = (Spinner) inputTextView.findViewById(R.id.spinner_fontsize);
        this.mButtonColorText = (SelectColorButton) inputTextView.findViewById(R.id.button_colortext);
        this.mButtonColorText.setTitle(R.string.title_colortext);
        this.mButtonColorBackground = (SelectColorButton) inputTextView.findViewById(R.id.button_colorbackground);
        this.mButtonColorBackground.setTitle(R.string.title_colorbackground);
        this.mButtonColorFrame = (SelectColorButton) inputTextView.findViewById(R.id.button_colorframe);
        this.mButtonColorFrame.setTitle(R.string.title_colorframe);
        this.mEditTextMemo = (EditText) inputTextView.findViewById(R.id.edittext_textmemo);
        this.mEditTextShare = (EditText) inputTextView.findViewById(R.id.edittext_textshare);
        setCancelable(true);
    }

    private String getTextMemo() {
        String textMemo = this.mEditTextMemo.getText().toString();
        if (textMemo == null || textMemo.length() == 0) {
            return " ";
        }
        return textMemo;
    }

    /* access modifiers changed from: protected */
    public void setTextMemo(String textMemo) {
        this.mEditTextMemo.setText(textMemo);
        if (textMemo != null) {
            this.mEditTextMemo.setSelection(textMemo.length());
        }
    }

    private String getTextShare() {
        return this.mEditTextShare.getText().toString();
    }

    /* access modifiers changed from: protected */
    public void setTextShare(String textShare) {
        this.mEditTextShare.setText(textShare);
    }

    private int getFontSize(Spinner spinner) {
        return getContext().getResources().getIntArray(R.array.list_fontsize)[spinner.getSelectedItemPosition()];
    }

    /* access modifiers changed from: protected */
    public void setFontSize(float fontSize) {
        Context context = getContext();
        Resources resources = context.getResources();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, 17367048, resources.getStringArray(R.array.select_fontsize));
        adapter.setDropDownViewResource(17367049);
        this.mSpinnerFontSize.setAdapter((SpinnerAdapter) adapter);
        int[] fontSizeList = resources.getIntArray(R.array.list_fontsize);
        int fontSizeNum = fontSizeList.length;
        for (int position = 0; position < fontSizeNum; position++) {
            if (((float) fontSizeList[position]) == fontSize) {
                this.mSpinnerFontSize.setSelection(position);
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void setColor(int colorText, int colorBackground, int colorFrame) {
        this.mButtonColorText.setColorSelected(colorText);
        this.mButtonColorBackground.setColorSelected(colorBackground);
        this.mButtonColorFrame.setColorSelected(colorFrame);
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case -3:
                onClick_ButtonCancel();
                return;
            case -2:
            default:
                return;
            case -1:
                onClick_ButtonYes();
                return;
        }
    }

    private void onClick_ButtonYes() {
        TextMemoViewData textMemoViewData;
        String textMemo = getTextMemo();
        if (textMemo == null || textMemo.length() == 0) {
            textMemoViewData = null;
        } else {
            textMemoViewData = new TextMemoViewData(getFontSize(this.mSpinnerFontSize), this.mButtonColorText.getColorSelected(), this.mButtonColorBackground.getColorSelected(), this.mButtonColorFrame.getColorSelected(), textMemo, getTextShare());
        }
        super.onClick_ButtonYes(this.mTextMemoView, textMemoViewData);
    }
}
