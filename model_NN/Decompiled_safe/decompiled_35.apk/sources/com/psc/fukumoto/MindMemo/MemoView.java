package com.psc.fukumoto.MindMemo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.FloatMath;
import android.view.ContextMenu;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;
import com.psc.fukumoto.MindMemo.GroupViewDialog;
import com.psc.fukumoto.MindMemo.MemoViewDialog;
import com.psc.fukumoto.MindMemo.TextMemoViewDialog;
import com.psc.fukumoto.lib.ImageSystem;
import com.psc.fukumoto.lib.SoundPlayer;
import com.psc.fukumoto.lib.TimerHandler;

public class MemoView extends SurfaceView implements SurfaceHolder.Callback, TimerHandler.OnTimerListener, DialogInterface.OnCancelListener, TextMemoViewDialog.OnButtonListener, GroupViewDialog.OnButtonListener, MemoViewDialog.OnButtonListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$psc$fukumoto$MindMemo$MemoView$TOUCH_MODE = null;
    protected static final float PADDING = 5.0f;
    private static final float SCALE_DEFAULT = 1.0f;
    private static final float SCALE_ZOOM = 1.4142135f;
    private static final float TOUCH_MOVE_MIN = 10.0f;
    private int TIME_REDRAW = 50;
    private int mColorBackground_Group;
    private int mColorBackground_MemoView;
    private int mColorBackground_TextMemo;
    private int mColorFrame_Group;
    private int mColorFrame_TextMemo;
    private int mColorLine_Group;
    private int mColorLine_MemoView;
    private int mColorText;
    private boolean mCreateTap;
    private boolean mDeleteSingleGroup;
    private AlertDialog mDialog = null;
    private float mDistance;
    private float mDrawX;
    private float mDrawY;
    private float mFontSize;
    private int mHeight;
    private boolean mMenuAfterTouchUp;
    private boolean mMenuShown = false;
    private Paint mPaintDrawing;
    private Path mPathDrawing;
    private PointF mPointTouch;
    private float mPosX;
    private float mPosY;
    private RectF mRectDraw;
    private boolean mRedraw = false;
    private float mScale = SCALE_DEFAULT;
    private SubView mSelectedSubView;
    private ShareListener mShareListener = null;
    private boolean mShareTap;
    private SubViewList mSubViewList = new SubViewList();
    private TimerHandler mTimerHandler = new TimerHandler(this, this.TIME_REDRAW, true);
    private boolean mTouchDown = false;
    private boolean mTouchLong = false;
    private TOUCH_MODE mTouchMode = TOUCH_MODE.ADD;
    private boolean mTouchMove = false;
    private boolean mTouchMulti = false;
    private SubView mTouchedSubView;
    private boolean mTransparent;
    private int mWidth;

    public interface ShareListener {
        boolean shareText(String str);
    }

    public enum TOUCH_MODE {
        MOVE,
        ADD
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$psc$fukumoto$MindMemo$MemoView$TOUCH_MODE() {
        int[] iArr = $SWITCH_TABLE$com$psc$fukumoto$MindMemo$MemoView$TOUCH_MODE;
        if (iArr == null) {
            iArr = new int[TOUCH_MODE.values().length];
            try {
                iArr[TOUCH_MODE.ADD.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[TOUCH_MODE.MOVE.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$com$psc$fukumoto$MindMemo$MemoView$TOUCH_MODE = iArr;
        }
        return iArr;
    }

    public void setShareListener(ShareListener listener) {
        this.mShareListener = listener;
    }

    public MemoView(Context context) {
        super(context);
        initSurface();
        this.mPaintDrawing = new Paint();
        this.mPaintDrawing.setStyle(Paint.Style.STROKE);
        this.mPaintDrawing.setAntiAlias(true);
    }

    public void onResume() {
        redraw();
        this.mTimerHandler.start();
    }

    public void onPause() {
        if (this.mDialog != null) {
            this.mDialog.dismiss();
            this.mDialog = null;
        }
        onMenuClosed();
        this.mTimerHandler.stop();
    }

    /* access modifiers changed from: protected */
    public void onSaveState(Bundle outState) {
    }

    /* access modifiers changed from: protected */
    public void onRestoreState(Bundle savedState) {
    }

    public int getColorBackground() {
        return this.mColorBackground_MemoView;
    }

    public int getColorLine() {
        return this.mColorLine_MemoView;
    }

    public float getDrawX() {
        return this.mDrawX;
    }

    public float getDrawY() {
        return this.mDrawY;
    }

    public float getScale() {
        return this.mScale;
    }

    public SubViewData[] getSubViewDataList() {
        return this.mSubViewList.getSubViewDataList();
    }

    public void setMemoViewData(MemoViewData memoViewData) {
        if (memoViewData != null) {
            this.mSubViewList.setSubViewDataList(memoViewData.getSubViewDataList(), null);
            this.mColorBackground_MemoView = memoViewData.getColorBackground();
            this.mColorLine_MemoView = memoViewData.getColorLine();
            this.mDrawX = memoViewData.getDrawX();
            this.mDrawY = memoViewData.getDrawY();
            this.mScale = memoViewData.getScale();
            if (this.mScale <= ImageSystem.ROTATE_NONE) {
                this.mScale = SCALE_DEFAULT;
            }
            this.mRectDraw = null;
            redraw();
        }
    }

    public TOUCH_MODE getTouchMode() {
        return this.mTouchMode;
    }

    public void setTouchMode(TOUCH_MODE touchMode) {
        this.mTouchMode = touchMode;
    }

    public void surfaceCreated(SurfaceHolder holder) {
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        this.mWidth = width;
        this.mHeight = height;
        redraw();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    private void initSurface() {
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        holder.setType(0);
        holder.setFormat(-2);
    }

    /* access modifiers changed from: protected */
    public void loadPreference(PreferenceData preferenceData) {
        this.mColorBackground_MemoView = preferenceData.getColorBackground_MemoView();
        this.mColorLine_MemoView = preferenceData.getColorLine_MemoView();
        this.mTransparent = preferenceData.getTransparent();
        this.mColorBackground_Group = preferenceData.getColorBackground_Group();
        this.mColorFrame_Group = preferenceData.getColorFrame_Group();
        this.mColorLine_Group = preferenceData.getColorLine_Group();
        this.mFontSize = preferenceData.getFontSize();
        this.mColorText = preferenceData.getColorText();
        this.mColorBackground_TextMemo = preferenceData.getColorBackground_TextMemo();
        this.mColorFrame_TextMemo = preferenceData.getColorFrame_TextMemo();
    }

    /* access modifiers changed from: protected */
    public void savePreference(PreferenceData preferenceData) {
        preferenceData.setColorBackground_MemoView(this.mColorBackground_MemoView);
        preferenceData.setColorLine_MemoView(this.mColorLine_MemoView);
        preferenceData.setTransparent(this.mTransparent);
        preferenceData.setColorBackground_Group(this.mColorBackground_Group);
        preferenceData.setColorFrame_Group(this.mColorFrame_Group);
        preferenceData.setColorLine_Group(this.mColorLine_Group);
        preferenceData.setFontSize(this.mFontSize);
        preferenceData.setColorText(this.mColorText);
        preferenceData.setColorBackground_TextMemo(this.mColorBackground_TextMemo);
        preferenceData.setColorFrame_TextMemo(this.mColorFrame_TextMemo);
    }

    /* access modifiers changed from: protected */
    public void changePreference(PreferenceData preferenceData) {
        this.mDeleteSingleGroup = preferenceData.getDeleteSingleGroup();
        this.mCreateTap = preferenceData.getCreateTap();
        this.mShareTap = preferenceData.getShareTap();
        this.mMenuAfterTouchUp = preferenceData.getMenuAfterTouchUp();
    }

    /* access modifiers changed from: protected */
    public void onCreateContextMenu(Context context, ContextMenu menu, ContextMenu.ContextMenuInfo menuInfo) {
        String textShare;
        if (this.mMenuAfterTouchUp) {
            if (!this.mTouchLong) {
                this.mTouchLong = true;
                return;
            }
        } else if (!this.mTouchDown || this.mTouchMove || this.mTouchMulti) {
            return;
        }
        if (this.mSelectedSubView == null) {
            menu.add(0, 10, 0, (int) R.string.menu_editmemo);
            menu.add(0, 14, 0, (int) R.string.menu_createitem);
        } else if (this.mSelectedSubView instanceof GroupView) {
            menu.add(0, 11, 0, (int) R.string.menu_editgroup);
            menu.add(0, 12, 0, (int) R.string.menu_copygroup);
            menu.add(0, 13, 0, (int) R.string.menu_deletegroup);
            menu.add(0, 14, 0, (int) R.string.menu_createitem);
            menu.add(0, 20, 0, (int) R.string.menu_movefront);
            menu.add(0, 21, 0, (int) R.string.menu_moveback);
        } else {
            menu.add(0, 15, 0, (int) R.string.menu_edititem);
            menu.add(0, 16, 0, (int) R.string.menu_copyitem);
            menu.add(0, 17, 0, (int) R.string.menu_deleteitem);
            if (this.mSelectedSubView.getParentView() != null) {
                menu.add(0, 18, 0, (int) R.string.menu_leavegroup);
            }
            menu.add(0, 20, 0, (int) R.string.menu_movefront);
            menu.add(0, 21, 0, (int) R.string.menu_moveback);
            if ((this.mSelectedSubView instanceof TextMemoView) && (textShare = ((TextMemoView) this.mSelectedSubView).getTextShare()) != null && textShare.length() > 0) {
                menu.add(0, 22, 0, (int) R.string.menu_sharetext);
            }
        }
        this.mMenuShown = true;
    }

    public boolean onMenu(int id) {
        switch (id) {
            case 10:
                onMenu_EditMemo();
                break;
            case 11:
                onMenu_EditGroup(this.mSelectedSubView);
                break;
            case SoundPlayer.SCALE_NUM /*12*/:
                onMenu_CopyGroup(this.mSelectedSubView);
                break;
            case 13:
                onMenu_DeleteGroup(this.mSelectedSubView);
                break;
            case 14:
                onMenu_CreateItem(this.mSelectedSubView);
                break;
            case 15:
                onMenu_EditItem(this.mSelectedSubView);
                break;
            case 16:
                onMenu_CopyItem(this.mSelectedSubView);
                break;
            case 17:
                onMenu_DeleteItem(this.mSelectedSubView);
                break;
            case 18:
                onMenu_LeaveGroup(this.mSelectedSubView);
                break;
            case 19:
                onMenu_RemoveLine(this.mSelectedSubView);
                break;
            case 20:
                onMenu_MoveFront(this.mSelectedSubView);
                break;
            case 21:
                onMenu_MoveBack(this.mSelectedSubView);
                break;
            case 22:
                onMenu_ShareText(this.mSelectedSubView);
                break;
            default:
                return false;
        }
        this.mMenuShown = false;
        if (this.mTouchedSubView != null) {
            this.mTouchedSubView.setSelected(false);
            this.mTouchedSubView = null;
            redraw();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMenuClosed() {
        this.mMenuShown = false;
        if (this.mTouchedSubView != null) {
            this.mTouchedSubView.setSelected(false);
            this.mTouchedSubView = null;
            redraw();
        }
    }

    /* access modifiers changed from: protected */
    public void onMenu_EditMemo() {
        showMemoViewDialog();
    }

    /* access modifiers changed from: protected */
    public void onMenu_EditGroup(SubView subView) {
        if (subView != null && (subView instanceof GroupView)) {
            showGroupViewDialog((GroupView) subView, null);
        }
    }

    /* access modifiers changed from: protected */
    public void onMenu_CopyGroup(SubView subView) {
        if (subView != null) {
            if (subView instanceof GroupView) {
                GroupView groupView = (GroupView) subView;
                addSubView(new GroupView(groupView, groupView.getWidth() / 2.0f, groupView.getHeight() / 2.0f));
            }
            redraw();
        }
    }

    /* access modifiers changed from: protected */
    public void onMenu_DeleteGroup(SubView subView) {
        if (subView != null) {
            if (subView instanceof GroupView) {
                removeSubView((GroupView) subView);
            }
            this.mRectDraw = null;
            redraw();
        }
    }

    /* access modifiers changed from: protected */
    public void onMenu_CreateItem(SubView subView) {
        showTextMemoViewEasyDialog(this.mPointTouch);
    }

    /* access modifiers changed from: protected */
    public void onMenu_EditItem(SubView subView) {
        if (subView != null && (subView instanceof TextMemoView)) {
            showTextMemoViewFullDialog((TextMemoView) subView);
        }
    }

    /* access modifiers changed from: protected */
    public void onMenu_CopyItem(SubView subView) {
        if (subView != null) {
            if (subView instanceof TextMemoView) {
                TextMemoView textMemoView = (TextMemoView) subView;
                addSubView(new TextMemoView(textMemoView, textMemoView.getWidth() / 2.0f, textMemoView.getHeight() / 2.0f));
            }
            redraw();
        }
    }

    /* access modifiers changed from: protected */
    public void onMenu_DeleteItem(SubView subView) {
        if (subView != null) {
            GroupView parentView = subView.getParentView();
            if (parentView != null) {
                removeFromGroup(parentView, subView);
            } else {
                removeSubView(subView);
            }
            this.mRectDraw = null;
            redraw();
        }
    }

    /* access modifiers changed from: protected */
    public void onMenu_LeaveGroup(SubView subView) {
        GroupView parentView;
        if (subView != null && !(subView instanceof GroupView) && (parentView = subView.getParentView()) != null) {
            removeFromGroup(parentView, subView);
            addSubView(subView, searchSubViewIndex(parentView.getId()) + 1);
            this.mRectDraw = null;
            redraw();
        }
    }

    /* access modifiers changed from: protected */
    public void onMenu_RemoveLine(SubView subView) {
        GroupView parentView;
        if (subView != null && (parentView = subView.getParentView()) != null) {
            parentView.removeLine(subView);
            redraw();
        }
    }

    /* access modifiers changed from: protected */
    public void onMenu_MoveFront(SubView subView) {
        if (subView != null) {
            GroupView parentView = subView.getParentView();
            if (parentView != null) {
                parentView.moveToFront(subView);
            } else {
                moveToFront(subView);
            }
            redraw();
        }
    }

    /* access modifiers changed from: protected */
    public void onMenu_MoveBack(SubView subView) {
        if (subView != null) {
            GroupView parentView = subView.getParentView();
            if (parentView != null) {
                parentView.moveToBack(subView);
            } else {
                moveToBack(subView);
            }
            redraw();
        }
    }

    /* access modifiers changed from: protected */
    public boolean onMenu_ShareText(SubView subView) {
        String textShare;
        if (subView == null) {
            return false;
        }
        if (this.mShareListener == null) {
            return false;
        }
        if ((subView instanceof TextMemoView) && (textShare = ((TextMemoView) subView).getTextShare()) != null && textShare.length() > 0) {
            if (this.mShareListener.shareText(textShare)) {
                return true;
            }
            Toast.makeText(getContext(), (int) R.string.message_failedshare, 0).show();
        }
        return false;
    }

    public void onTimer() {
        SurfaceHolder holder;
        Canvas canvas;
        if (this.mRedraw && (canvas = (holder = getHolder()).lockCanvas()) != null) {
            drawAll(canvas, this.mDrawX, this.mDrawY, this.mScale);
            holder.unlockCanvasAndPost(canvas);
            this.mRedraw = false;
        }
    }

    /* access modifiers changed from: protected */
    public Bitmap createBitmap(boolean saveInView, int viewWidth, int viewHeight) {
        float drawX;
        float drawY;
        int width;
        int height;
        float scale;
        calcRectDraw();
        if (saveInView || this.mRectDraw == null) {
            drawX = this.mDrawX;
            drawY = this.mDrawY;
            width = viewWidth;
            height = viewHeight;
            scale = this.mScale;
        } else {
            drawX = -this.mRectDraw.left;
            drawY = -this.mRectDraw.top;
            width = (int) Math.ceil((double) this.mRectDraw.width());
            height = (int) Math.ceil((double) this.mRectDraw.height());
            scale = SCALE_DEFAULT;
        }
        try {
            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            drawAll(new Canvas(bitmap), drawX, drawY, scale);
            return bitmap;
        } catch (OutOfMemoryError e) {
            e.fillInStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void zoomIn() {
        this.mScale *= SCALE_ZOOM;
        redraw();
    }

    /* access modifiers changed from: protected */
    public void zoomOut() {
        this.mScale /= SCALE_ZOOM;
        redraw();
    }

    private void redraw() {
        this.mRedraw = true;
    }

    private void drawAll(Canvas canvas, float drawX, float drawY, float scale) {
        canvas.save();
        canvas.scale(scale, scale, ((float) this.mWidth) / 2.0f, ((float) this.mHeight) / 2.0f);
        canvas.translate(drawX, drawY);
        canvas.drawColor(this.mColorBackground_MemoView);
        drawSubView(canvas);
        drawDrawing(canvas);
        canvas.restore();
    }

    private void drawSubView(Canvas canvas) {
        int subViewNum = this.mSubViewList.size();
        for (int index = 0; index < subViewNum; index++) {
            ((SubView) this.mSubViewList.get(index)).draw(canvas);
        }
    }

    private void drawDrawing(Canvas canvas) {
        if (this.mPathDrawing != null) {
            this.mPaintDrawing.setColor(this.mColorLine_MemoView);
            canvas.drawPath(this.mPathDrawing, this.mPaintDrawing);
        }
    }

    private void calcRectDraw() {
        int subViewNum = this.mSubViewList.size();
        if (subViewNum != 0) {
            RectF rectSubView = ((SubView) this.mSubViewList.get(0)).getRectDraw();
            float left = rectSubView.left;
            float right = rectSubView.right;
            float top = rectSubView.top;
            float bottom = rectSubView.bottom;
            for (int index = 1; index < subViewNum; index++) {
                RectF rectSubView2 = ((SubView) this.mSubViewList.get(index)).getRectDraw();
                if (left > rectSubView2.left) {
                    left = rectSubView2.left;
                }
                if (right < rectSubView2.right) {
                    right = rectSubView2.right;
                }
                if (top > rectSubView2.top) {
                    top = rectSubView2.top;
                }
                if (bottom < rectSubView2.bottom) {
                    bottom = rectSubView2.bottom;
                }
            }
            this.mRectDraw = new RectF(left, top, right, bottom);
        }
    }

    private void clearPathDrawing() {
        if (this.mPathDrawing != null) {
            this.mPathDrawing = null;
            redraw();
        }
    }

    private PointF getViewPosition(MultiTouchEvent multiTouchEvent, int index) {
        float centerX = ((float) this.mWidth) / 2.0f;
        float centerY = ((float) this.mHeight) / 2.0f;
        PointF point = multiTouchEvent.getPoint(index);
        if (point == null) {
            return null;
        }
        return new PointF((((point.x - centerX) / this.mScale) + centerX) - this.mDrawX, (((point.y - centerY) / this.mScale) + centerY) - this.mDrawY);
    }

    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        if (this.mMenuShown) {
            return true;
        }
        MultiTouchEvent multiTouchEvent = new MultiTouchEvent(event);
        switch (multiTouchEvent.getAction()) {
            case 0:
                onTouchDown(multiTouchEvent);
                break;
            case 1:
                onTouchUp(multiTouchEvent);
                break;
            case 2:
                onTouchMove(multiTouchEvent);
                break;
            case 3:
                onTouchCancel(multiTouchEvent);
                break;
        }
        return true;
    }

    private void onTouchDown(MultiTouchEvent multiTouchEvent) {
        if (multiTouchEvent.getPointerCount() == 1) {
            onTouchDown_Single(multiTouchEvent);
        } else {
            onTouchDown_Multi(multiTouchEvent);
        }
    }

    private void onTouchDown_Single(MultiTouchEvent multiTouchEvent) {
        PointF pointTouch = getViewPosition(multiTouchEvent, 0);
        this.mSelectedSubView = searchSubView(pointTouch);
        this.mTouchedSubView = this.mSelectedSubView;
        if (this.mTouchedSubView != null) {
            this.mTouchedSubView.setSelected(true);
            redraw();
        }
        this.mTouchDown = true;
        this.mTouchMove = false;
        this.mTouchLong = false;
        this.mTouchMulti = false;
        this.mPointTouch = pointTouch;
        this.mPosX = multiTouchEvent.getPosX(0);
        this.mPosY = multiTouchEvent.getPosY(0);
    }

    private void onTouchDown_Multi(MultiTouchEvent multiTouchEvent) {
        if (!this.mTouchMove) {
            PointF pointTouch0 = multiTouchEvent.getPoint(0);
            PointF pointTouch1 = multiTouchEvent.getPoint(1);
            float distanceX = pointTouch0.x - pointTouch1.x;
            float distanceY = pointTouch0.y - pointTouch1.y;
            this.mDistance = FloatMath.sqrt((distanceX * distanceX) + (distanceY * distanceY));
            this.mTouchMulti = true;
            clearPathDrawing();
            if (this.mTouchedSubView != null) {
                this.mTouchedSubView.setSelected(false);
                this.mTouchedSubView = null;
                redraw();
            }
        }
    }

    private void onTouchMove(MultiTouchEvent multiTouchEvent) {
        if (multiTouchEvent.getPointerCount() == 1) {
            onTouchMove_Single(multiTouchEvent);
        } else {
            onTouchMove_Multi(multiTouchEvent);
        }
    }

    private void onTouchMove_Single(MultiTouchEvent multiTouchEvent) {
        if (!this.mTouchMulti) {
            PointF pointTouch = getViewPosition(multiTouchEvent, 0);
            float posX = multiTouchEvent.getPosX(0);
            float posY = multiTouchEvent.getPosY(0);
            float distanceX = posX - this.mPosX;
            float distanceY = posY - this.mPosY;
            if (!this.mTouchMove) {
                if (Math.abs(distanceX) >= TOUCH_MOVE_MIN || Math.abs(distanceY) >= TOUCH_MOVE_MIN) {
                    this.mTouchMove = true;
                } else {
                    return;
                }
            }
            this.mPosX = posX;
            this.mPosY = posY;
            switch ($SWITCH_TABLE$com$psc$fukumoto$MindMemo$MemoView$TOUCH_MODE()[this.mTouchMode.ordinal()]) {
                case 1:
                    if (this.mSelectedSubView == null) {
                        onTouchMove_MoveMemoView(distanceX, distanceY);
                        break;
                    } else {
                        onTouchMove_MoveSubView(distanceX, distanceY);
                        break;
                    }
                case 2:
                    if (this.mSelectedSubView == null) {
                        onTouchMove_AddGroup(pointTouch);
                        break;
                    } else {
                        onTouchMove_AddLine(pointTouch);
                        break;
                    }
            }
            redraw();
        }
    }

    private void onTouchMove_Multi(MultiTouchEvent multiTouchEvent) {
        if (!this.mTouchMove) {
            PointF pointTouch0 = multiTouchEvent.getPoint(0);
            PointF pointTouch1 = multiTouchEvent.getPoint(1);
            float distanceX = pointTouch0.x - pointTouch1.x;
            float distanceY = pointTouch0.y - pointTouch1.y;
            float distance = FloatMath.sqrt((distanceX * distanceX) + (distanceY * distanceY));
            if (this.mDistance > ImageSystem.ROTATE_NONE) {
                this.mScale = (this.mScale * distance) / this.mDistance;
            }
            this.mDistance = distance;
            redraw();
        }
    }

    private void onTouchUp(MultiTouchEvent multiTouchEvent) {
        if (this.mTouchDown && multiTouchEvent.getPointerCount() == 1) {
            this.mTouchDown = false;
            if (!this.mTouchMulti) {
                PointF pointTouch = getViewPosition(multiTouchEvent, 0);
                if (pointTouch != null) {
                    if (this.mTouchMove) {
                        onTouchUp_Move(pointTouch);
                    } else if (!this.mMenuAfterTouchUp || !this.mTouchLong) {
                        onTouchUp_NotMove(pointTouch);
                    } else {
                        showContextMenu();
                        return;
                    }
                }
                if (this.mTouchedSubView != null) {
                    this.mTouchedSubView.setSelected(false);
                    this.mTouchedSubView = null;
                    redraw();
                }
            }
        }
    }

    private void onTouchCancel(MultiTouchEvent multiTouchEvent) {
        if (this.mTouchDown && multiTouchEvent.getPointerCount() <= 0) {
            this.mTouchDown = false;
            clearPathDrawing();
            if (this.mTouchedSubView != null) {
                this.mTouchedSubView.setSelected(false);
                this.mTouchedSubView = null;
                redraw();
            }
        }
    }

    private boolean onTouchMove_MoveMemoView(float distanceX, float distanceY) {
        if (this.mRectDraw == null) {
            calcRectDraw();
            if (this.mRectDraw == null) {
                return false;
            }
        }
        float centerX = ((float) this.mWidth) / 2.0f;
        float centerY = ((float) this.mHeight) / 2.0f;
        float left = (((this.mRectDraw.left - centerX) + this.mDrawX) * this.mScale) + centerX;
        float right = (((this.mRectDraw.right - centerX) + this.mDrawX) * this.mScale) + centerX;
        float top = (((this.mRectDraw.top - centerY) + this.mDrawY) * this.mScale) + centerY;
        float bottom = (((this.mRectDraw.bottom - centerY) + this.mDrawY) * this.mScale) + centerY;
        float width = (float) this.mWidth;
        float height = (float) this.mHeight;
        if (left + distanceX > width - PADDING) {
            distanceX = (width - PADDING) - left;
        } else if (right + distanceX < PADDING) {
            distanceX = PADDING - right;
        }
        if (top + distanceY > height - PADDING) {
            distanceY = (height - PADDING) - top;
        } else if (bottom + distanceY < PADDING) {
            distanceY = PADDING - bottom;
        }
        if (distanceX == ImageSystem.ROTATE_NONE && distanceY == ImageSystem.ROTATE_NONE) {
            return false;
        }
        this.mDrawX += distanceX / this.mScale;
        this.mDrawY += distanceY / this.mScale;
        return true;
    }

    private void onTouchMove_MoveSubView(float distanceX, float distanceY) {
        this.mSelectedSubView.move(distanceX / this.mScale, distanceY / this.mScale);
        this.mRectDraw = null;
        redraw();
    }

    private void onTouchMove_AddGroup(PointF pointTouch) {
        if (pointTouch != null) {
            RectF rectGroup = getRectSelected(pointTouch);
            this.mPathDrawing = new Path();
            this.mPathDrawing.addRect(rectGroup, Path.Direction.CW);
        }
    }

    private void onTouchMove_AddLine(PointF pointTouch) {
        if (pointTouch != null) {
            float startX = this.mSelectedSubView.getCenterX();
            float startY = this.mSelectedSubView.getCenterY();
            this.mPathDrawing = new Path();
            this.mPathDrawing.moveTo(startX, startY);
            this.mPathDrawing.lineTo(pointTouch.x, pointTouch.y);
        }
    }

    private void onTouchUp_Move(PointF pointTouch) {
        switch ($SWITCH_TABLE$com$psc$fukumoto$MindMemo$MemoView$TOUCH_MODE()[this.mTouchMode.ordinal()]) {
            case 1:
                if (this.mSelectedSubView != null) {
                    onTouchUp_MoveSubView(pointTouch);
                    return;
                } else {
                    onTouchUp_MoveMemoView(pointTouch);
                    return;
                }
            case 2:
                if (this.mSelectedSubView != null) {
                    onTouchUp_AddLine(pointTouch);
                    return;
                } else {
                    onTouchUp_AddGroup(pointTouch);
                    return;
                }
            default:
                return;
        }
    }

    private void onTouchUp_NotMove(PointF pointTouch) {
        if (this.mShareTap && onMenu_ShareText(this.mSelectedSubView)) {
            return;
        }
        if (this.mTouchMode == TOUCH_MODE.MOVE && !this.mCreateTap) {
            return;
        }
        if (this.mSelectedSubView instanceof TextMemoView) {
            showTextMemoViewFullDialog((TextMemoView) this.mSelectedSubView);
        } else if (this.mSelectedSubView instanceof GroupView) {
            showTextMemoViewEasyDialog(pointTouch);
        } else if (this.mSelectedSubView == null) {
            showTextMemoViewEasyDialog(pointTouch);
        }
    }

    private void onTouchUp_MoveMemoView(PointF pointTouch) {
    }

    private void onTouchUp_MoveSubView(PointF pointTouch) {
        GroupView parentView = this.mSelectedSubView.getParentView();
        if (parentView != null) {
            parentView.calcRectDraw();
        }
        redraw();
    }

    private void onTouchUp_AddGroup(PointF pointTouch) {
        RectF rectGroup = getRectSelected(pointTouch);
        SubViewList containList = new SubViewList();
        int subViewNum = this.mSubViewList.size();
        for (int index = 0; index < subViewNum; index++) {
            SubView subView = (SubView) this.mSubViewList.get(index);
            if (!(subView instanceof GroupView) && rectGroup.contains(subView.getRectDraw())) {
                containList.add(subView);
            }
        }
        int containNum = containList.size();
        if (this.mDeleteSingleGroup) {
            if (containNum < 2) {
                clearPathDrawing();
                return;
            }
        } else if (containNum < 1) {
            clearPathDrawing();
            return;
        }
        showGroupViewDialog(null, containList);
    }

    private void onTouchUp_AddLine(PointF pointTouch) {
        SubView selectedSubView1 = this.mSelectedSubView;
        if (selectedSubView1 != null) {
            SubView selectedSubView2 = searchSubView(pointTouch);
            if (selectedSubView2 == null) {
                showTextMemoViewEasyDialog(pointTouch);
                return;
            }
            if (selectedSubView2 instanceof GroupView) {
                if (selectedSubView1 instanceof GroupView) {
                    showTextMemoViewEasyDialog(pointTouch);
                    return;
                }
                moveToGroup((GroupView) selectedSubView2, selectedSubView1);
            } else if (selectedSubView1 instanceof GroupView) {
                moveToGroup((GroupView) selectedSubView1, selectedSubView2);
            } else {
                addLine(selectedSubView1, selectedSubView2);
            }
            clearPathDrawing();
            redraw();
        }
    }

    private RectF getRectSelected(PointF pointTouch) {
        float posX = this.mPointTouch.x;
        float posY = this.mPointTouch.y;
        RectF rectSelected = new RectF();
        if (pointTouch.x < posX) {
            rectSelected.left = pointTouch.x;
            rectSelected.right = posX;
        } else {
            rectSelected.left = posX;
            rectSelected.right = pointTouch.x;
        }
        if (pointTouch.y < posY) {
            rectSelected.top = pointTouch.y;
            rectSelected.bottom = posY;
        } else {
            rectSelected.top = posY;
            rectSelected.bottom = pointTouch.y;
        }
        return rectSelected;
    }

    public void clearAll() {
        this.mSubViewList.clearAll();
        this.mDrawX = ImageSystem.ROTATE_NONE;
        this.mDrawY = ImageSystem.ROTATE_NONE;
        this.mScale = SCALE_DEFAULT;
        this.mRectDraw = null;
        redraw();
    }

    private SubView searchSubView(PointF pointTouch) {
        for (int index = this.mSubViewList.size() - 1; index >= 0; index--) {
            SubView subViewPosition = ((SubView) this.mSubViewList.get(index)).searchSubView(pointTouch);
            if (subViewPosition != null) {
                return subViewPosition;
            }
        }
        return null;
    }

    private int searchSubViewIndex(int id) {
        for (int index = this.mSubViewList.size() - 1; index >= 0; index--) {
            if (((SubView) this.mSubViewList.get(index)).getId() == id) {
                return index;
            }
        }
        return -1;
    }

    public void moveToFront(SubView subView) {
        this.mSubViewList.remove(subView);
        this.mSubViewList.add(subView);
    }

    public void moveToBack(SubView subView) {
        this.mSubViewList.remove(subView);
        this.mSubViewList.add(0, subView);
    }

    private void addSubView(SubView subView) {
        this.mSubViewList.add(subView);
        subView.setParentView(null);
    }

    private void addSubView(SubView subView, int index) {
        if (index < 0) {
            index = 0;
        } else if (index > this.mSubViewList.size()) {
            index = this.mSubViewList.size();
        }
        this.mSubViewList.add(index, subView);
        subView.setParentView(null);
    }

    private void removeSubView(SubView subView) {
        this.mSubViewList.remove(subView);
    }

    private void moveToGroup(GroupView groupView, SubView subView) {
        GroupView parentView = subView.getParentView();
        if (parentView == null) {
            removeSubView(subView);
        } else if (parentView.getId() != groupView.getId()) {
            removeFromGroup(parentView, subView);
            parentView.removeSubView(subView);
        } else {
            return;
        }
        groupView.addSubView(subView);
    }

    private void removeFromGroup(GroupView groupView, SubView subView) {
        SubView lastView = groupView.removeSubView(subView);
        int subViewNum = groupView.getSubViewNum();
        if (subViewNum == 0) {
            removeSubView(groupView);
        } else if (this.mDeleteSingleGroup && subViewNum == 1) {
            int index = searchSubViewIndex(groupView.getId());
            removeSubView(groupView);
            addSubView(lastView, index);
        }
    }

    private void addLine(SubView subView1, SubView subView2) {
        if (subView1 != null && subView1 != null && subView1 != subView2) {
            LineView lineViewAdd = new LineView(subView1, subView2, -16776961);
            GroupView parentView1 = subView1.getParentView();
            GroupView parentView2 = subView2.getParentView();
            if (parentView1 == null) {
                if (parentView2 == null) {
                    parentView2 = new GroupView(this.mTransparent, this.mColorBackground_Group, this.mColorFrame_Group, this.mColorLine_Group);
                    moveToGroup(parentView2, subView2);
                    addSubView(parentView2);
                }
                moveToGroup(parentView2, subView1);
                parentView2.addLineView(lineViewAdd);
                return;
            }
            if (parentView2 == null) {
                moveToGroup(parentView1, subView2);
            } else if (parentView1 != parentView2) {
                moveToGroup(parentView1, parentView2);
            }
            parentView1.addLineView(lineViewAdd);
        }
    }

    private void showTextMemoViewEasyDialog(PointF pointTouch) {
        if (this.mDialog == null) {
            Context context = getContext();
            PointF pointCenter = new PointF();
            pointCenter.set(pointTouch);
            TextMemoViewEasyDialog dialog = new TextMemoViewEasyDialog(context, this, pointCenter);
            this.mDialog = dialog;
            dialog.setOnCancelListener(this);
            dialog.show();
        }
    }

    private void showTextMemoViewFullDialog(TextMemoView textMemoView) {
        float fontSize;
        int colorText;
        int colorBackground;
        int colorFrame;
        String textMemo;
        String textShare;
        if (this.mDialog == null) {
            TextMemoViewFullDialog dialog = new TextMemoViewFullDialog(getContext(), this, null, textMemoView);
            this.mDialog = dialog;
            dialog.setOnCancelListener(this);
            if (textMemoView != null) {
                fontSize = textMemoView.getFontSize();
            } else {
                fontSize = this.mFontSize;
            }
            dialog.setFontSize(fontSize);
            if (textMemoView != null) {
                colorText = textMemoView.getColorText();
            } else {
                colorText = this.mColorText;
            }
            if (textMemoView != null) {
                colorBackground = textMemoView.getColorBackground();
            } else {
                colorBackground = this.mColorBackground_TextMemo;
            }
            if (textMemoView != null) {
                colorFrame = textMemoView.getColorFrame();
            } else {
                colorFrame = this.mColorFrame_TextMemo;
            }
            dialog.setColor(colorText, colorBackground, colorFrame);
            if (textMemoView != null) {
                textMemo = textMemoView.getTextMemo();
            } else {
                textMemo = null;
            }
            dialog.setTextMemo(textMemo);
            if (textMemoView != null) {
                textShare = textMemoView.getTextShare();
            } else {
                textShare = null;
            }
            dialog.setTextShare(textShare);
            dialog.show();
        }
    }

    private void showGroupViewDialog(GroupView groupView, SubViewList containList) {
        boolean transparent;
        int colorBackground;
        int colorFrame;
        int colorLine;
        if (this.mDialog == null) {
            GroupViewDialog dialog = new GroupViewDialog(getContext(), this, groupView, containList);
            this.mDialog = dialog;
            dialog.setOnCancelListener(this);
            if (groupView != null) {
                transparent = groupView.getTransparent();
            } else {
                transparent = this.mTransparent;
            }
            dialog.setTransparent(transparent);
            if (groupView != null) {
                colorBackground = groupView.getColorBackground();
            } else {
                colorBackground = this.mColorBackground_Group;
            }
            if (groupView != null) {
                colorFrame = groupView.getColorFrame();
            } else {
                colorFrame = this.mColorFrame_Group;
            }
            if (groupView != null) {
                colorLine = groupView.getColorLine();
            } else {
                colorLine = this.mColorLine_Group;
            }
            dialog.setColor(colorBackground, colorFrame, colorLine);
            dialog.show();
        }
    }

    private void showMemoViewDialog() {
        if (this.mDialog == null) {
            MemoViewDialog dialog = new MemoViewDialog(getContext(), this);
            this.mDialog = dialog;
            dialog.setOnCancelListener(this);
            dialog.setColor(this.mColorBackground_MemoView, this.mColorLine_MemoView);
            dialog.show();
        }
    }

    public void onCancel(DialogInterface dialog) {
        this.mDialog = null;
        clearPathDrawing();
    }

    public void onButtonYes_TextMemoViewDialog(TextMemoView textMemoView, TextMemoViewData textMemoViewData, PointF pointCenter) {
        this.mDialog = null;
        clearPathDrawing();
        if (textMemoViewData != null) {
            if (textMemoView != null) {
                textMemoView.setTextMemoViewData(textMemoViewData);
            } else {
                textMemoView = new TextMemoView(textMemoViewData);
                textMemoView.setCenter(pointCenter);
                if (this.mSelectedSubView instanceof GroupView) {
                    ((GroupView) this.mSelectedSubView).addSubView(textMemoView);
                    this.mSelectedSubView = null;
                } else {
                    addSubView(textMemoView);
                }
            }
            this.mFontSize = textMemoViewData.getFontSize();
            this.mColorText = textMemoViewData.getColorText();
            this.mColorBackground_TextMemo = textMemoViewData.getColorBackground();
            this.mColorFrame_TextMemo = textMemoViewData.getColorFrame();
            addLine(this.mSelectedSubView, textMemoView);
            GroupView parentView = textMemoView.getParentView();
            if (parentView != null) {
                parentView.calcRectDraw();
            }
            this.mRectDraw = null;
            redraw();
        }
    }

    public void onButtonNo_TextMemoViewDialog(String textMemo, String textShare, PointF pointCenter) {
        TextMemoViewFullDialog dialog = new TextMemoViewFullDialog(getContext(), this, pointCenter, null);
        this.mDialog = dialog;
        dialog.setOnCancelListener(this);
        dialog.setFontSize(this.mFontSize);
        dialog.setColor(this.mColorText, this.mColorBackground_TextMemo, this.mColorFrame_TextMemo);
        dialog.setTextMemo(textMemo);
        dialog.setTextShare(textShare);
        dialog.show();
    }

    public void onButtonCancel_TextMemoViewDialog() {
        onCancel(this.mDialog);
    }

    public void onButtonYes_GroupViewDialog(GroupView groupView, GroupViewData groupViewData, SubViewList containList) {
        this.mDialog = null;
        clearPathDrawing();
        if (groupView != null) {
            groupView.setGroupViewData(groupViewData);
        } else {
            GroupView groupView2 = new GroupView(groupViewData);
            addSubView(groupView2);
            int containNum = containList.size();
            for (int index = 0; index < containNum; index++) {
                moveToGroup(groupView2, (SubView) containList.get(index));
            }
        }
        this.mTransparent = groupViewData.getTransparent();
        this.mColorBackground_Group = groupViewData.getColorBackground();
        this.mColorFrame_Group = groupViewData.getColorFrame();
        this.mColorLine_Group = groupViewData.getColorLine();
        redraw();
    }

    public void onButtonCancel_GroupViewDialog() {
        onCancel(this.mDialog);
    }

    public void onButtonYes_MemoViewDialog(int colorBackground, int colorLine) {
        this.mDialog = null;
        this.mColorBackground_MemoView = colorBackground;
        this.mColorLine_MemoView = colorLine;
        redraw();
    }

    public void onButtonCancel_MemoViewDialog() {
        onCancel(this.mDialog);
    }
}
