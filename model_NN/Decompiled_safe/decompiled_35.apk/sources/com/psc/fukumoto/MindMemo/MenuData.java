package com.psc.fukumoto.MindMemo;

public class MenuData {

    protected static class Id {
        protected static final int COPYGROUP = 12;
        protected static final int COPYITEM = 16;
        protected static final int CREATEITEM = 14;
        protected static final int DELETEGROUP = 13;
        protected static final int DELETEITEM = 17;
        protected static final int EDITGROUP = 11;
        protected static final int EDITITEM = 15;
        protected static final int EDITMEMO = 10;
        protected static final int LEAVEGROUP = 18;
        protected static final int MOVEBACK = 21;
        protected static final int MOVEFRONT = 20;
        protected static final int NEW = 2;
        protected static final int PREFERENCE = 1;
        protected static final int REMOVELINE = 19;
        protected static final int SAVEIMAGE = 4;
        protected static final int SELECTFILE = 3;
        protected static final int SHARETEXT = 22;

        protected Id() {
        }
    }

    protected static class Icon {
        protected static final int PREFERENCE = 17301577;
        protected static final int SAVEIMAGE = 17301567;

        protected Icon() {
        }
    }

    protected static class Title {
        protected static final int COPYGROUP = 2131165210;
        protected static final int COPYITEM = 2131165214;
        protected static final int CREATEITEM = 2131165212;
        protected static final int DELETEGROUP = 2131165211;
        protected static final int DELETEITEM = 2131165215;
        protected static final int EDITGROUP = 2131165209;
        protected static final int EDITITEM = 2131165213;
        protected static final int EDITMEMO = 2131165208;
        protected static final int LEAVEGROUP = 2131165216;
        protected static final int MOVEBACK = 2131165207;
        protected static final int MOVEFRONT = 2131165206;
        protected static final int NEW = 2131165219;
        protected static final int PREFERENCE = 2131165222;
        protected static final int REMOVELINE = 2131165217;
        protected static final int SAVEIMAGE = 2131165221;
        protected static final int SELECTFILE = 2131165220;
        protected static final int SHARETEXT = 2131165218;

        protected Title() {
        }
    }
}
