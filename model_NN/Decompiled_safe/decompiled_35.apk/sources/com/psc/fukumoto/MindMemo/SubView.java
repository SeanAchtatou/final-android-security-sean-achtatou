package com.psc.fukumoto.MindMemo;

import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.RectF;

public class SubView {
    public static final int ID_NULL = 0;
    private static int sIdBase = 0;
    private int mId;
    private GroupView mParentView;
    private RectF mRectDraw = new RectF();
    private boolean mSelected = false;

    public SubView() {
        sIdBase++;
        this.mId = sIdBase;
    }

    public SubView(SubViewData subViewData) {
        int id = subViewData.getId();
        if (id == 0) {
            sIdBase++;
            id = sIdBase;
        } else if (sIdBase < id) {
            sIdBase = id;
        }
        this.mId = id;
        setCenter(subViewData.getCenterX(), subViewData.getCenterY());
    }

    public SubView(SubView subViewBase, float dx, float dy) {
        sIdBase++;
        this.mId = sIdBase;
        setRectDraw(subViewBase.getCenterX() + dx, subViewBase.getCenterY() + dy, subViewBase.getWidth(), subViewBase.getHeight());
    }

    public SubViewData getSubViewData() {
        return new SubViewData(this);
    }

    public void setParentView(GroupView parentView) {
        this.mParentView = parentView;
    }

    public GroupView getParentView() {
        return this.mParentView;
    }

    public boolean getSelected() {
        return this.mSelected;
    }

    public void setSelected(boolean selected) {
        this.mSelected = selected;
    }

    public int getId() {
        return this.mId;
    }

    public void setCenter(PointF pointCenter) {
        if (pointCenter != null) {
            setRectDraw(pointCenter.x, pointCenter.y, this.mRectDraw.width(), this.mRectDraw.height());
        }
    }

    private void setCenter(float centerX, float centerY) {
        setRectDraw(centerX, centerY, this.mRectDraw.width(), this.mRectDraw.height());
    }

    public PointF getCenter() {
        PointF pointCenter = new PointF();
        pointCenter.x = (this.mRectDraw.left + this.mRectDraw.right) / 2.0f;
        pointCenter.y = (this.mRectDraw.top + this.mRectDraw.bottom) / 2.0f;
        return pointCenter;
    }

    public float getCenterX() {
        return (this.mRectDraw.left + this.mRectDraw.right) / 2.0f;
    }

    public float getCenterY() {
        return (this.mRectDraw.top + this.mRectDraw.bottom) / 2.0f;
    }

    public void setSize(float width, float height) {
        setRectDraw(getCenterX(), getCenterY(), width, height);
    }

    private void setRectDraw(float centerX, float centerY, float width, float height) {
        this.mRectDraw.left = centerX - (width / 2.0f);
        this.mRectDraw.right = (width / 2.0f) + centerX;
        this.mRectDraw.top = centerY - (height / 2.0f);
        this.mRectDraw.bottom = (height / 2.0f) + centerY;
    }

    public void setRectDraw(RectF rectDraw) {
        this.mRectDraw = new RectF(rectDraw);
    }

    public RectF getRectDraw() {
        return this.mRectDraw;
    }

    public float getLeft() {
        return this.mRectDraw.left;
    }

    public float getRight() {
        return this.mRectDraw.right;
    }

    public float getTop() {
        return this.mRectDraw.top;
    }

    public float getBottom() {
        return this.mRectDraw.bottom;
    }

    public float getWidth() {
        return this.mRectDraw.width();
    }

    public float getHeight() {
        return this.mRectDraw.height();
    }

    public SubView searchSubView(PointF pointTouch) {
        if (pointTouch == null) {
            return null;
        }
        if (this.mRectDraw.contains(pointTouch.x, pointTouch.y)) {
            return this;
        }
        return null;
    }

    public void move(float distanceX, float distanceY) {
        this.mRectDraw.left += distanceX;
        this.mRectDraw.right += distanceX;
        this.mRectDraw.top += distanceY;
        this.mRectDraw.bottom += distanceY;
    }

    public void clearAll() {
        this.mParentView = null;
    }

    public void calcRectDraw() {
    }

    public void draw(Canvas canvas) {
    }
}
