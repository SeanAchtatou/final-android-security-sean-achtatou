package com.psc.fukumoto.MindMemo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;

public class MindMemoPreferenceActivity extends PreferenceActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.layout.preferences);
        ((PreferenceScreen) findPreference(getText(R.string.key_contact))).setIntent(new Intent("android.intent.action.VIEW", Uri.parse(getString(R.string.contact_url))));
    }
}
