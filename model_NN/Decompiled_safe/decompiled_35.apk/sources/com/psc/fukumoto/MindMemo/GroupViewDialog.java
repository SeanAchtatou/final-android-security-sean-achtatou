package com.psc.fukumoto.MindMemo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

public class GroupViewDialog extends AlertDialog implements DialogInterface.OnClickListener {
    private static final int LAYOUT_RESOURCE = 2130903040;
    private SelectColorButton mButtonColorBackground;
    private SelectColorButton mButtonColorFrame;
    private SelectColorButton mButtonColorLine;
    private CheckBox mCheckBoxTransparent;
    private SubViewList mContainList;
    private GroupView mGroupView;
    private OnButtonListener mOnButtonListener = null;

    public interface OnButtonListener {
        void onButtonCancel_GroupViewDialog();

        void onButtonYes_GroupViewDialog(GroupView groupView, GroupViewData groupViewData, SubViewList subViewList);
    }

    public GroupViewDialog(Context context, OnButtonListener listener, GroupView groupView, SubViewList containList) {
        super(context);
        int id;
        String buttonYes;
        this.mOnButtonListener = listener;
        this.mGroupView = groupView;
        this.mContainList = containList;
        if (groupView != null) {
            id = R.string.title_editgroup;
        } else {
            id = R.string.title_creategroup;
        }
        setTitle(context.getString(id));
        if (groupView != null) {
            buttonYes = context.getString(R.string.button_set);
        } else {
            buttonYes = context.getString(R.string.button_create);
        }
        setButton(-1, buttonYes, this);
        setButton(-3, context.getString(R.string.button_cancel), this);
        View inputTextView = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.dialog_group_view, (ViewGroup) null);
        setView(inputTextView);
        this.mCheckBoxTransparent = (CheckBox) inputTextView.findViewById(R.id.checkbox_transparent);
        this.mButtonColorBackground = (SelectColorButton) inputTextView.findViewById(R.id.button_colorbackground);
        this.mButtonColorBackground.setTitle(R.string.title_colorbackground);
        this.mButtonColorFrame = (SelectColorButton) inputTextView.findViewById(R.id.button_colorframe);
        this.mButtonColorFrame.setTitle(R.string.title_colorframe);
        this.mButtonColorLine = (SelectColorButton) inputTextView.findViewById(R.id.button_colorline);
        this.mButtonColorLine.setTitle(R.string.title_colorline);
        setCancelable(true);
    }

    /* access modifiers changed from: protected */
    public void setTransparent(boolean transparent) {
        this.mCheckBoxTransparent.setChecked(transparent);
    }

    /* access modifiers changed from: protected */
    public void setColor(int colorBackground, int colorFrame, int colorLine) {
        this.mButtonColorBackground.setColorSelected(colorBackground);
        this.mButtonColorFrame.setColorSelected(colorFrame);
        this.mButtonColorLine.setColorSelected(colorLine);
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case -3:
                this.mOnButtonListener.onButtonCancel_GroupViewDialog();
                return;
            case -2:
            default:
                return;
            case -1:
                onClick_ButtonYes();
                return;
        }
    }

    private void onClick_ButtonYes() {
        this.mOnButtonListener.onButtonYes_GroupViewDialog(this.mGroupView, new GroupViewData(this.mCheckBoxTransparent.isChecked(), this.mButtonColorBackground.getColorSelected(), this.mButtonColorFrame.getColorSelected(), this.mButtonColorLine.getColorSelected()), this.mContainList);
    }
}
