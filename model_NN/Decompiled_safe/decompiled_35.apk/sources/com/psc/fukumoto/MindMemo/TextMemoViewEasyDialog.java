package com.psc.fukumoto.MindMemo;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PointF;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.psc.fukumoto.MindMemo.TextMemoViewDialog;

public class TextMemoViewEasyDialog extends TextMemoViewDialog implements DialogInterface.OnClickListener {
    private static final int LAYOUT_RESOURCE = 2130903045;
    private EditText mEditTextMemo;
    private EditText mEditTextShare;

    public TextMemoViewEasyDialog(Context context, TextMemoViewDialog.OnButtonListener listener, PointF pointCenter) {
        super(context, listener, pointCenter);
        setTitle(context.getString(R.string.title_createview));
        setButton(-1, context.getString(R.string.button_create), this);
        setButton(-2, context.getString(R.string.button_detail), this);
        setButton(-3, context.getString(R.string.button_cancel), this);
        View inputTextView = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.dialog_text_memo_view_easy, (ViewGroup) null);
        setView(inputTextView);
        this.mEditTextMemo = (EditText) inputTextView.findViewById(R.id.edittext_textmemo);
        this.mEditTextShare = (EditText) inputTextView.findViewById(R.id.edittext_textshare);
        setCancelable(true);
    }

    private String getTextMemo() {
        String textMemo = this.mEditTextMemo.getText().toString();
        if (textMemo == null || textMemo.length() == 0) {
            return " ";
        }
        return textMemo;
    }

    private String getTextShare() {
        return this.mEditTextShare.getText().toString();
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case -3:
                onClick_ButtonCancel();
                return;
            case -2:
                onClick_ButtonNo(getTextMemo(), getTextShare());
                return;
            case -1:
                onClick_ButtonYes();
                return;
            default:
                return;
        }
    }

    private void onClick_ButtonYes() {
        TextMemoViewData textMemoViewData;
        String textMemo = getTextMemo();
        if (textMemo == null || textMemo.length() == 0) {
            textMemoViewData = null;
        } else {
            PreferenceData preferenceData = new PreferenceData(getContext());
            textMemoViewData = new TextMemoViewData((int) preferenceData.getFontSize(), preferenceData.getColorText(), preferenceData.getColorBackground_TextMemo(), preferenceData.getColorFrame_TextMemo(), textMemo, getTextShare());
        }
        super.onClick_ButtonYes(null, textMemoViewData);
    }
}
