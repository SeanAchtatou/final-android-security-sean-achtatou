package com.psc.fukumoto.MindMemo;

import android.content.Context;
import com.psc.fukumoto.lib.FileData;
import com.psc.fukumoto.lib.FileSystem;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class MindMemoFile {
    private static final String APP_NAME = "MindMemo";
    public static final String DATA_FILE_EXTENSION = ".mmd";
    private static final String FILENAME_TMP = "tmp.mmd";

    protected static final boolean deleteTempFile(Context context) {
        return context.deleteFile(FILENAME_TMP);
    }

    protected static final boolean saveTempFile(Context context, MemoViewData memoViewData) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = context.openFileOutput(FILENAME_TMP, 0);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(memoViewData);
            objectOutputStream.close();
            FileOutputStream fileOutputStream2 = null;
            if (fileOutputStream2 != null) {
                try {
                    fileOutputStream2.close();
                } catch (Exception ex) {
                    ex.fillInStackTrace();
                }
            }
            return true;
        } catch (IOException e) {
            e.fillInStackTrace();
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (Exception ex2) {
                    ex2.fillInStackTrace();
                }
            }
            return false;
        } catch (Throwable th) {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (Exception ex3) {
                    ex3.fillInStackTrace();
                }
            }
            throw th;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v6, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.psc.fukumoto.MindMemo.MemoViewData} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static final com.psc.fukumoto.MindMemo.MemoViewData loadTempFile(android.content.Context r8) {
        /*
            java.lang.String r4 = "tmp.mmd"
            r3 = 0
            r5 = 0
            java.lang.String r7 = "tmp.mmd"
            java.io.FileInputStream r3 = r8.openFileInput(r7)     // Catch:{ FileNotFoundException -> 0x0021, StreamCorruptedException -> 0x0031, IOException -> 0x0041, ClassNotFoundException -> 0x0051 }
            java.io.ObjectInputStream r6 = new java.io.ObjectInputStream     // Catch:{ FileNotFoundException -> 0x0021, StreamCorruptedException -> 0x0031, IOException -> 0x0041, ClassNotFoundException -> 0x0051 }
            r6.<init>(r3)     // Catch:{ FileNotFoundException -> 0x0021, StreamCorruptedException -> 0x0031, IOException -> 0x0041, ClassNotFoundException -> 0x0051 }
            java.lang.Object r7 = r6.readObject()     // Catch:{ FileNotFoundException -> 0x0021, StreamCorruptedException -> 0x0031, IOException -> 0x0041, ClassNotFoundException -> 0x0051 }
            r0 = r7
            com.psc.fukumoto.MindMemo.MemoViewData r0 = (com.psc.fukumoto.MindMemo.MemoViewData) r0     // Catch:{ FileNotFoundException -> 0x0021, StreamCorruptedException -> 0x0031, IOException -> 0x0041, ClassNotFoundException -> 0x0051 }
            r5 = r0
            r6.close()     // Catch:{ FileNotFoundException -> 0x0021, StreamCorruptedException -> 0x0031, IOException -> 0x0041, ClassNotFoundException -> 0x0051 }
            r3 = 0
            if (r3 == 0) goto L_0x0020
            r3.close()     // Catch:{ Exception -> 0x006d }
        L_0x0020:
            return r5
        L_0x0021:
            r7 = move-exception
            r1 = r7
            r1.fillInStackTrace()     // Catch:{ all -> 0x0061 }
            if (r3 == 0) goto L_0x0020
            r3.close()     // Catch:{ Exception -> 0x002c }
            goto L_0x0020
        L_0x002c:
            r2 = move-exception
            r2.fillInStackTrace()
            goto L_0x0020
        L_0x0031:
            r7 = move-exception
            r1 = r7
            r1.fillInStackTrace()     // Catch:{ all -> 0x0061 }
            if (r3 == 0) goto L_0x0020
            r3.close()     // Catch:{ Exception -> 0x003c }
            goto L_0x0020
        L_0x003c:
            r2 = move-exception
            r2.fillInStackTrace()
            goto L_0x0020
        L_0x0041:
            r7 = move-exception
            r1 = r7
            r1.fillInStackTrace()     // Catch:{ all -> 0x0061 }
            if (r3 == 0) goto L_0x0020
            r3.close()     // Catch:{ Exception -> 0x004c }
            goto L_0x0020
        L_0x004c:
            r2 = move-exception
            r2.fillInStackTrace()
            goto L_0x0020
        L_0x0051:
            r7 = move-exception
            r1 = r7
            r1.fillInStackTrace()     // Catch:{ all -> 0x0061 }
            if (r3 == 0) goto L_0x0020
            r3.close()     // Catch:{ Exception -> 0x005c }
            goto L_0x0020
        L_0x005c:
            r2 = move-exception
            r2.fillInStackTrace()
            goto L_0x0020
        L_0x0061:
            r7 = move-exception
            if (r3 == 0) goto L_0x0067
            r3.close()     // Catch:{ Exception -> 0x0068 }
        L_0x0067:
            throw r7
        L_0x0068:
            r2 = move-exception
            r2.fillInStackTrace()
            goto L_0x0067
        L_0x006d:
            r2 = move-exception
            r2.fillInStackTrace()
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.psc.fukumoto.MindMemo.MindMemoFile.loadTempFile(android.content.Context):com.psc.fukumoto.MindMemo.MemoViewData");
    }

    protected static final boolean deleteMemoFile(FileData fileData) {
        if (fileData == null) {
            return false;
        }
        return FileSystem.deleteFile(fileData.getFullName());
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0035 A[SYNTHETIC, Splitter:B:18:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0042 A[SYNTHETIC, Splitter:B:25:0x0042] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static final boolean saveMemoFile(com.psc.fukumoto.lib.FileData r9, com.psc.fukumoto.MindMemo.MemoViewData r10) {
        /*
            r8 = 0
            if (r9 != 0) goto L_0x0005
            r7 = r8
        L_0x0004:
            return r7
        L_0x0005:
            java.lang.String r4 = r9.getFolderName()
            com.psc.fukumoto.lib.FileSystem.createFolder(r4, r8)
            java.lang.String r5 = r9.getFullName()
            r2 = 0
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x002e }
            r1.<init>(r5)     // Catch:{ IOException -> 0x002e }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x002e }
            r3.<init>(r1)     // Catch:{ IOException -> 0x002e }
            java.io.ObjectOutputStream r6 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x0053, all -> 0x0050 }
            r6.<init>(r3)     // Catch:{ IOException -> 0x0053, all -> 0x0050 }
            r6.writeObject(r10)     // Catch:{ IOException -> 0x0053, all -> 0x0050 }
            r6.close()     // Catch:{ IOException -> 0x0053, all -> 0x0050 }
            r2 = 0
            if (r2 == 0) goto L_0x002c
            r2.close()     // Catch:{ IOException -> 0x004b }
        L_0x002c:
            r7 = 1
            goto L_0x0004
        L_0x002e:
            r7 = move-exception
            r0 = r7
        L_0x0030:
            r0.fillInStackTrace()     // Catch:{ all -> 0x003f }
            if (r2 == 0) goto L_0x0038
            r2.close()     // Catch:{ IOException -> 0x003a }
        L_0x0038:
            r7 = r8
            goto L_0x0004
        L_0x003a:
            r0 = move-exception
            r0.fillInStackTrace()
            goto L_0x0038
        L_0x003f:
            r7 = move-exception
        L_0x0040:
            if (r2 == 0) goto L_0x0045
            r2.close()     // Catch:{ IOException -> 0x0046 }
        L_0x0045:
            throw r7
        L_0x0046:
            r0 = move-exception
            r0.fillInStackTrace()
            goto L_0x0045
        L_0x004b:
            r0 = move-exception
            r0.fillInStackTrace()
            goto L_0x002c
        L_0x0050:
            r7 = move-exception
            r2 = r3
            goto L_0x0040
        L_0x0053:
            r7 = move-exception
            r0 = r7
            r2 = r3
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.psc.fukumoto.MindMemo.MindMemoFile.saveMemoFile(com.psc.fukumoto.lib.FileData, com.psc.fukumoto.MindMemo.MemoViewData):boolean");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v12, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.psc.fukumoto.MindMemo.MemoViewData} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0033 A[SYNTHETIC, Splitter:B:17:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0043 A[SYNTHETIC, Splitter:B:26:0x0043] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0053 A[SYNTHETIC, Splitter:B:35:0x0053] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0063 A[SYNTHETIC, Splitter:B:44:0x0063] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x006f A[SYNTHETIC, Splitter:B:50:0x006f] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:14:0x002e=Splitter:B:14:0x002e, B:23:0x003e=Splitter:B:23:0x003e, B:32:0x004e=Splitter:B:32:0x004e, B:41:0x005e=Splitter:B:41:0x005e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static final com.psc.fukumoto.MindMemo.MemoViewData loadMemoFile(com.psc.fukumoto.lib.FileData r10) {
        /*
            if (r10 != 0) goto L_0x0004
            r9 = 0
        L_0x0003:
            return r9
        L_0x0004:
            java.lang.String r6 = r10.getFullName()
            r4 = 0
            r7 = 0
            java.io.File r3 = new java.io.File     // Catch:{ FileNotFoundException -> 0x002c, StreamCorruptedException -> 0x003c, IOException -> 0x004c, ClassNotFoundException -> 0x005c }
            r3.<init>(r6)     // Catch:{ FileNotFoundException -> 0x002c, StreamCorruptedException -> 0x003c, IOException -> 0x004c, ClassNotFoundException -> 0x005c }
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x002c, StreamCorruptedException -> 0x003c, IOException -> 0x004c, ClassNotFoundException -> 0x005c }
            r5.<init>(r3)     // Catch:{ FileNotFoundException -> 0x002c, StreamCorruptedException -> 0x003c, IOException -> 0x004c, ClassNotFoundException -> 0x005c }
            java.io.ObjectInputStream r8 = new java.io.ObjectInputStream     // Catch:{ FileNotFoundException -> 0x008c, StreamCorruptedException -> 0x0088, IOException -> 0x0084, ClassNotFoundException -> 0x0080, all -> 0x007d }
            r8.<init>(r5)     // Catch:{ FileNotFoundException -> 0x008c, StreamCorruptedException -> 0x0088, IOException -> 0x0084, ClassNotFoundException -> 0x0080, all -> 0x007d }
            java.lang.Object r9 = r8.readObject()     // Catch:{ FileNotFoundException -> 0x008c, StreamCorruptedException -> 0x0088, IOException -> 0x0084, ClassNotFoundException -> 0x0080, all -> 0x007d }
            r0 = r9
            com.psc.fukumoto.MindMemo.MemoViewData r0 = (com.psc.fukumoto.MindMemo.MemoViewData) r0     // Catch:{ FileNotFoundException -> 0x008c, StreamCorruptedException -> 0x0088, IOException -> 0x0084, ClassNotFoundException -> 0x0080, all -> 0x007d }
            r7 = r0
            r8.close()     // Catch:{ FileNotFoundException -> 0x008c, StreamCorruptedException -> 0x0088, IOException -> 0x0084, ClassNotFoundException -> 0x0080, all -> 0x007d }
            r4 = 0
            if (r4 == 0) goto L_0x002a
            r4.close()     // Catch:{ Exception -> 0x0078 }
        L_0x002a:
            r9 = r7
            goto L_0x0003
        L_0x002c:
            r9 = move-exception
            r1 = r9
        L_0x002e:
            r1.fillInStackTrace()     // Catch:{ all -> 0x006c }
            if (r4 == 0) goto L_0x002a
            r4.close()     // Catch:{ Exception -> 0x0037 }
            goto L_0x002a
        L_0x0037:
            r2 = move-exception
            r2.fillInStackTrace()
            goto L_0x002a
        L_0x003c:
            r9 = move-exception
            r1 = r9
        L_0x003e:
            r1.fillInStackTrace()     // Catch:{ all -> 0x006c }
            if (r4 == 0) goto L_0x002a
            r4.close()     // Catch:{ Exception -> 0x0047 }
            goto L_0x002a
        L_0x0047:
            r2 = move-exception
            r2.fillInStackTrace()
            goto L_0x002a
        L_0x004c:
            r9 = move-exception
            r1 = r9
        L_0x004e:
            r1.fillInStackTrace()     // Catch:{ all -> 0x006c }
            if (r4 == 0) goto L_0x002a
            r4.close()     // Catch:{ Exception -> 0x0057 }
            goto L_0x002a
        L_0x0057:
            r2 = move-exception
            r2.fillInStackTrace()
            goto L_0x002a
        L_0x005c:
            r9 = move-exception
            r1 = r9
        L_0x005e:
            r1.fillInStackTrace()     // Catch:{ all -> 0x006c }
            if (r4 == 0) goto L_0x002a
            r4.close()     // Catch:{ Exception -> 0x0067 }
            goto L_0x002a
        L_0x0067:
            r2 = move-exception
            r2.fillInStackTrace()
            goto L_0x002a
        L_0x006c:
            r9 = move-exception
        L_0x006d:
            if (r4 == 0) goto L_0x0072
            r4.close()     // Catch:{ Exception -> 0x0073 }
        L_0x0072:
            throw r9
        L_0x0073:
            r2 = move-exception
            r2.fillInStackTrace()
            goto L_0x0072
        L_0x0078:
            r2 = move-exception
            r2.fillInStackTrace()
            goto L_0x002a
        L_0x007d:
            r9 = move-exception
            r4 = r5
            goto L_0x006d
        L_0x0080:
            r9 = move-exception
            r1 = r9
            r4 = r5
            goto L_0x005e
        L_0x0084:
            r9 = move-exception
            r1 = r9
            r4 = r5
            goto L_0x004e
        L_0x0088:
            r9 = move-exception
            r1 = r9
            r4 = r5
            goto L_0x003e
        L_0x008c:
            r9 = move-exception
            r1 = r9
            r4 = r5
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.psc.fukumoto.MindMemo.MindMemoFile.loadMemoFile(com.psc.fukumoto.lib.FileData):com.psc.fukumoto.MindMemo.MemoViewData");
    }

    protected static final FileData getMemoFileName(Context context, String fileName) {
        return new FileData(getDefaultFolderName(), fileName, DATA_FILE_EXTENSION);
    }

    protected static final List<FileData> getFileDataList(Context context, String folderName) {
        if (folderName == null) {
            folderName = getDefaultFolderName();
        }
        File folder = new File(folderName);
        if (!folder.exists()) {
            return null;
        }
        if (!folder.isDirectory()) {
            return null;
        }
        List<FileData> fileDataList = new ArrayList<>();
        for (String fileData : folder.list()) {
            FileData fileData2 = new FileData(folderName, fileData);
            if (!fileData2.isFolder() && DATA_FILE_EXTENSION.equals(fileData2.getExtension())) {
                fileDataList.add(fileData2);
            }
        }
        return fileDataList;
    }

    protected static String getDefaultFolderName() {
        return String.valueOf(FileSystem.FOLDER_NAME_SDCARD) + File.separator + APP_NAME;
    }
}
