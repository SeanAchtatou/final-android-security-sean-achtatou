package com.psc.fukumoto.MindMemo;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.PointF;

public class TextMemoViewDialog extends AlertDialog {
    private OnButtonListener mOnButtonListener = null;
    private PointF mPointCenter;

    public interface OnButtonListener {
        void onButtonCancel_TextMemoViewDialog();

        void onButtonNo_TextMemoViewDialog(String str, String str2, PointF pointF);

        void onButtonYes_TextMemoViewDialog(TextMemoView textMemoView, TextMemoViewData textMemoViewData, PointF pointF);
    }

    protected TextMemoViewDialog(Context context, OnButtonListener listener, PointF pointCenter) {
        super(context);
        this.mOnButtonListener = listener;
        this.mPointCenter = pointCenter;
    }

    /* access modifiers changed from: protected */
    public void onClick_ButtonYes(TextMemoView textMemoView, TextMemoViewData textMemoViewData) {
        this.mOnButtonListener.onButtonYes_TextMemoViewDialog(textMemoView, textMemoViewData, this.mPointCenter);
    }

    /* access modifiers changed from: protected */
    public void onClick_ButtonNo(String textMemo, String textShare) {
        this.mOnButtonListener.onButtonNo_TextMemoViewDialog(textMemo, textShare, this.mPointCenter);
    }

    /* access modifiers changed from: protected */
    public void onClick_ButtonCancel() {
        this.mOnButtonListener.onButtonCancel_TextMemoViewDialog();
    }
}
