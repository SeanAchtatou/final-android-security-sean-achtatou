package com.psc.fukumoto.MindMemo;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import java.util.ArrayList;

public class GroupView extends SubView {
    private static final float PADDING = 5.0f;
    private static final int PAINT_ALPHA_OFF = 255;
    private static final int PAINT_ALPHA_ON = 128;
    private static final float RADIUS = 10.0f;
    private int mColorLine;
    private LineViewList mLineViewList = new LineViewList();
    private Paint mPaintBackground;
    private Paint mPaintFrame;
    private SubViewList mSubViewList = new SubViewList();
    private boolean mTransparent;

    public GroupView(boolean transparent, int colorBackgroud, int colorFrame, int colorLine) {
        createPaint();
        setTransparent(transparent);
        setColorBackground(colorBackgroud);
        setColorFrame(colorFrame);
        setColorLine(colorLine);
    }

    public GroupView(GroupViewData groupViewData) {
        super(groupViewData);
        createPaint();
        this.mSubViewList.setSubViewDataList(groupViewData.getSubViewDataList(), this);
        this.mLineViewList.setLineViewDataList(groupViewData.getLineViewDataList(), this.mSubViewList);
        setGroupViewData(groupViewData);
        calcRectDraw();
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:23:0x0085 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:22:0x0085 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:21:0x0085 */
    /* JADX WARN: Type inference failed for: r0v28, types: [com.psc.fukumoto.MindMemo.SubView] */
    /* JADX WARN: Type inference failed for: r0v31, types: [com.psc.fukumoto.MindMemo.TextMemoView] */
    /* JADX WARN: Type inference failed for: r17v2 */
    /* JADX WARN: Type inference failed for: r0v35, types: [com.psc.fukumoto.MindMemo.GroupView] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public GroupView(com.psc.fukumoto.MindMemo.GroupView r23, float r24, float r25) {
        /*
            r22 = this;
            r22.<init>(r23, r24, r25)
            com.psc.fukumoto.MindMemo.SubViewList r21 = new com.psc.fukumoto.MindMemo.SubViewList
            r21.<init>()
            r0 = r21
            r1 = r22
            r1.mSubViewList = r0
            com.psc.fukumoto.MindMemo.LineViewList r21 = new com.psc.fukumoto.MindMemo.LineViewList
            r21.<init>()
            r0 = r21
            r1 = r22
            r1.mLineViewList = r0
            r22.createPaint()
            boolean r21 = r23.getTransparent()
            r0 = r22
            r1 = r21
            r0.setTransparent(r1)
            int r21 = r23.getColorBackground()
            r0 = r22
            r1 = r21
            r0.setColorBackground(r1)
            int r21 = r23.getColorFrame()
            r0 = r22
            r1 = r21
            r0.setColorFrame(r1)
            int r21 = r23.getColorLine()
            r0 = r22
            r1 = r21
            r0.setColorLine(r1)
            r0 = r23
            com.psc.fukumoto.MindMemo.SubViewList r0 = r0.mSubViewList
            r18 = r0
            int r19 = r18.size()
            r7 = 0
        L_0x0053:
            r0 = r7
            r1 = r19
            if (r0 < r1) goto L_0x0065
            r0 = r23
            com.psc.fukumoto.MindMemo.LineViewList r0 = r0.mLineViewList
            r12 = r0
            int r13 = r12.size()
            r7 = 0
        L_0x0062:
            if (r7 < r13) goto L_0x00b6
            return
        L_0x0065:
            r0 = r18
            r1 = r7
            java.lang.Object r14 = r0.get(r1)
            com.psc.fukumoto.MindMemo.SubView r14 = (com.psc.fukumoto.MindMemo.SubView) r14
            r0 = r14
            boolean r0 = r0 instanceof com.psc.fukumoto.MindMemo.GroupView
            r21 = r0
            if (r21 == 0) goto L_0x008f
            r0 = r14
            com.psc.fukumoto.MindMemo.GroupView r0 = (com.psc.fukumoto.MindMemo.GroupView) r0
            r4 = r0
            com.psc.fukumoto.MindMemo.GroupView r17 = new com.psc.fukumoto.MindMemo.GroupView
            r0 = r17
            r1 = r4
            r2 = r24
            r3 = r25
            r0.<init>(r1, r2, r3)
        L_0x0085:
            r0 = r22
            r1 = r17
            r0.addSubView(r1)
            int r7 = r7 + 1
            goto L_0x0053
        L_0x008f:
            r0 = r14
            boolean r0 = r0 instanceof com.psc.fukumoto.MindMemo.TextMemoView
            r21 = r0
            if (r21 == 0) goto L_0x00a9
            r0 = r14
            com.psc.fukumoto.MindMemo.TextMemoView r0 = (com.psc.fukumoto.MindMemo.TextMemoView) r0
            r20 = r0
            com.psc.fukumoto.MindMemo.TextMemoView r17 = new com.psc.fukumoto.MindMemo.TextMemoView
            r0 = r17
            r1 = r20
            r2 = r24
            r3 = r25
            r0.<init>(r1, r2, r3)
            goto L_0x0085
        L_0x00a9:
            com.psc.fukumoto.MindMemo.SubView r17 = new com.psc.fukumoto.MindMemo.SubView
            r0 = r17
            r1 = r14
            r2 = r24
            r3 = r25
            r0.<init>(r1, r2, r3)
            goto L_0x0085
        L_0x00b6:
            java.lang.Object r10 = r12.get(r7)
            com.psc.fukumoto.MindMemo.LineView r10 = (com.psc.fukumoto.MindMemo.LineView) r10
            int r5 = r10.getSubView1Id()
            r0 = r18
            r1 = r5
            int r8 = r0.searchSubViewIndex(r1)
            if (r8 >= 0) goto L_0x00cc
        L_0x00c9:
            int r7 = r7 + 1
            goto L_0x0062
        L_0x00cc:
            r0 = r22
            com.psc.fukumoto.MindMemo.SubViewList r0 = r0.mSubViewList
            r21 = r0
            r0 = r21
            r1 = r8
            java.lang.Object r15 = r0.get(r1)
            com.psc.fukumoto.MindMemo.SubView r15 = (com.psc.fukumoto.MindMemo.SubView) r15
            int r6 = r10.getSubView2Id()
            r0 = r18
            r1 = r6
            int r9 = r0.searchSubViewIndex(r1)
            if (r9 < 0) goto L_0x00c9
            r0 = r22
            com.psc.fukumoto.MindMemo.SubViewList r0 = r0.mSubViewList
            r21 = r0
            r0 = r21
            r1 = r9
            java.lang.Object r16 = r0.get(r1)
            com.psc.fukumoto.MindMemo.SubView r16 = (com.psc.fukumoto.MindMemo.SubView) r16
            com.psc.fukumoto.MindMemo.LineView r11 = new com.psc.fukumoto.MindMemo.LineView
            r0 = r22
            int r0 = r0.mColorLine
            r21 = r0
            r0 = r11
            r1 = r15
            r2 = r16
            r3 = r21
            r0.<init>(r1, r2, r3)
            r0 = r22
            r1 = r11
            r0.addLineView(r1)
            goto L_0x00c9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.psc.fukumoto.MindMemo.GroupView.<init>(com.psc.fukumoto.MindMemo.GroupView, float, float):void");
    }

    private void createPaint() {
        this.mPaintBackground = new Paint();
        this.mPaintBackground.setStyle(Paint.Style.FILL);
        this.mPaintBackground.setAntiAlias(true);
        this.mPaintFrame = new Paint();
        this.mPaintFrame.setStyle(Paint.Style.STROKE);
        this.mPaintFrame.setAntiAlias(true);
    }

    public SubViewData getSubViewData() {
        return new GroupViewData(this);
    }

    public void setGroupViewData(GroupViewData groupViewData) {
        setTransparent(groupViewData.getTransparent());
        setColorBackground(groupViewData.getColorBackground());
        setColorFrame(groupViewData.getColorFrame());
        setColorLine(groupViewData.getColorLine());
    }

    public int getSubViewNum() {
        return this.mSubViewList.size();
    }

    public boolean getTransparent() {
        return this.mTransparent;
    }

    public void setTransparent(boolean transparent) {
        this.mTransparent = transparent;
        if (transparent) {
            this.mPaintBackground.setAlpha(PAINT_ALPHA_ON);
            this.mPaintFrame.setAlpha(PAINT_ALPHA_ON);
            return;
        }
        this.mPaintBackground.setAlpha(255);
        this.mPaintFrame.setAlpha(255);
    }

    public int getColorBackground() {
        return -16777216 | this.mPaintBackground.getColor();
    }

    private void setColorBackground(int colorBackground) {
        this.mPaintBackground.setColor(colorBackground);
        if (this.mTransparent) {
            this.mPaintBackground.setAlpha(PAINT_ALPHA_ON);
        } else {
            this.mPaintBackground.setAlpha(255);
        }
    }

    public int getColorFrame() {
        return -16777216 | this.mPaintFrame.getColor();
    }

    private void setColorFrame(int colorFrame) {
        this.mPaintFrame.setColor(colorFrame);
        if (this.mTransparent) {
            this.mPaintFrame.setAlpha(PAINT_ALPHA_ON);
        } else {
            this.mPaintFrame.setAlpha(255);
        }
    }

    public int getColorLine() {
        return this.mColorLine;
    }

    private void setColorLine(int colorLine) {
        this.mColorLine = colorLine;
        int lineViewNum = this.mLineViewList.size();
        for (int index = 0; index < lineViewNum; index++) {
            ((LineView) this.mLineViewList.get(index)).setColor(colorLine);
        }
    }

    public SubViewData[] getSubViewDataList() {
        return this.mSubViewList.getSubViewDataList();
    }

    public LineViewData[] getLineViewDataList() {
        return this.mLineViewList.getLineViewDataList();
    }

    public void addSubView(SubView subViewAdd) {
        RectF rectDraw;
        if (subViewAdd != null) {
            if (subViewAdd instanceof GroupView) {
                addGroupView((GroupView) subViewAdd);
                return;
            }
            subViewAdd.setParentView(this);
            this.mSubViewList.add(subViewAdd);
            RectF rectSubView = subViewAdd.getRectDraw();
            if (this.mSubViewList.size() == 1) {
                rectDraw = new RectF(rectSubView);
                rectDraw.left -= PADDING;
                rectDraw.right += PADDING;
                rectDraw.top -= PADDING;
                rectDraw.bottom += PADDING;
            } else {
                rectDraw = getRectDraw();
                float left = rectSubView.left - PADDING;
                if (rectDraw.left > left) {
                    rectDraw.left = left;
                }
                float right = rectSubView.right + PADDING;
                if (rectDraw.right < right) {
                    rectDraw.right = right;
                }
                float top = rectSubView.top - PADDING;
                if (rectDraw.top > top) {
                    rectDraw.top = top;
                }
                float bottom = rectSubView.bottom + PADDING;
                if (rectDraw.bottom < bottom) {
                    rectDraw.bottom = bottom;
                }
            }
            setRectDraw(rectDraw);
        }
    }

    private void addGroupView(GroupView groupViewAdd) {
        ArrayList<SubView> subViewList = groupViewAdd.mSubViewList;
        int subViewNum = subViewList.size();
        for (int index = 0; index < subViewNum; index++) {
            addSubView((SubView) subViewList.get(index));
        }
        this.mLineViewList.addAll(groupViewAdd.mLineViewList);
    }

    public void addLineView(LineView lineViewAdd) {
        if (lineViewAdd != null) {
            int lineViewNum = this.mLineViewList.size();
            for (int index = 0; index < lineViewNum; index++) {
                if (((LineView) this.mLineViewList.get(index)).equal(lineViewAdd)) {
                    this.mLineViewList.remove(index);
                    return;
                }
            }
            this.mLineViewList.add(lineViewAdd);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public SubView removeSubView(SubView subView) {
        this.mSubViewList.removeSubView(subView);
        this.mLineViewList.removeSubView(subView);
        calcRectDraw();
        if (this.mSubViewList.size() == 1) {
            return (SubView) this.mSubViewList.get(0);
        }
        return null;
    }

    public boolean isInclude(SubView subViewCheck) {
        int subViewNum = this.mSubViewList.size();
        for (int index = 0; index < subViewNum; index++) {
            SubView subView = (SubView) this.mSubViewList.get(index);
            if (subView == subViewCheck) {
                return true;
            }
            if ((subView instanceof GroupView) && ((GroupView) subView).isInclude(subViewCheck)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasLine(SubView subViewCheck) {
        int lineViewNum = this.mLineViewList.size();
        for (int index = 0; index < lineViewNum; index++) {
            if (((LineView) this.mLineViewList.get(index)).isMember(subViewCheck)) {
                return true;
            }
        }
        return false;
    }

    public void removeLine(SubView subView) {
        for (int index = this.mLineViewList.size() - 1; index >= 0; index--) {
            if (((LineView) this.mLineViewList.get(index)).isMember(subView)) {
                this.mLineViewList.remove(index);
            }
        }
    }

    public void draw(Canvas canvas) {
        drawFrame(canvas);
        drawLineView(canvas);
        drawSubView(canvas);
    }

    private void drawFrame(Canvas canvas) {
        RectF rectDraw = getRectDraw();
        canvas.drawRoundRect(rectDraw, RADIUS, RADIUS, this.mPaintBackground);
        canvas.drawRoundRect(rectDraw, RADIUS, RADIUS, this.mPaintFrame);
    }

    private void drawLineView(Canvas canvas) {
        int lineViewNum = this.mLineViewList.size();
        for (int index = 0; index < lineViewNum; index++) {
            ((LineView) this.mLineViewList.get(index)).draw(canvas);
        }
    }

    private void drawSubView(Canvas canvas) {
        int subViewNum = this.mSubViewList.size();
        for (int index = 0; index < subViewNum; index++) {
            ((SubView) this.mSubViewList.get(index)).draw(canvas);
        }
    }

    public void move(float distanceX, float distanceY) {
        super.move(distanceX, distanceY);
        int subViewNum = this.mSubViewList.size();
        for (int index = 0; index < subViewNum; index++) {
            ((SubView) this.mSubViewList.get(index)).move(distanceX, distanceY);
        }
    }

    public SubView searchSubView(PointF pointTouch) {
        for (int index = this.mSubViewList.size() - 1; index >= 0; index--) {
            SubView subView = (SubView) this.mSubViewList.get(index);
            if (subView.searchSubView(pointTouch) != null) {
                return subView;
            }
        }
        return super.searchSubView(pointTouch);
    }

    public void calcRectDraw() {
        int subViewNum = this.mSubViewList.size();
        if (subViewNum != 0) {
            RectF rectDraw = new RectF(((SubView) this.mSubViewList.get(0)).getRectDraw());
            rectDraw.left -= PADDING;
            rectDraw.right += PADDING;
            rectDraw.top -= PADDING;
            rectDraw.bottom += PADDING;
            for (int index = 1; index < subViewNum; index++) {
                RectF rectSubView = ((SubView) this.mSubViewList.get(index)).getRectDraw();
                float left = rectSubView.left - PADDING;
                if (rectDraw.left > left) {
                    rectDraw.left = left;
                }
                float right = rectSubView.right + PADDING;
                if (rectDraw.right < right) {
                    rectDraw.right = right;
                }
                float top = rectSubView.top - PADDING;
                if (rectDraw.top > top) {
                    rectDraw.top = top;
                }
                float bottom = rectSubView.bottom + PADDING;
                if (rectDraw.bottom < bottom) {
                    rectDraw.bottom = bottom;
                }
            }
            setRectDraw(rectDraw);
            GroupView parentView = getParentView();
            if (parentView != null) {
                parentView.calcRectDraw();
            }
        }
    }

    public void moveToFront(SubView subView) {
        this.mSubViewList.remove(subView);
        this.mSubViewList.add(subView);
    }

    public void moveToBack(SubView subView) {
        this.mSubViewList.remove(subView);
        this.mSubViewList.add(0, subView);
    }

    public void clearAll() {
        super.clearAll();
        this.mSubViewList.clearAll();
        this.mLineViewList.clearAll();
    }
}
