package com.psc.fukumoto.MindMemo;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class SelectResourceImageAdapter extends BaseAdapter {
    private static final int COLOR_NOTSELECTED = 0;
    private Context mContext;
    private Drawable mDrawableSelected;
    private int[] mIdList;
    private int mPosition = -1;

    public SelectResourceImageAdapter(Context context, int[] idList) {
        this.mContext = context;
        this.mIdList = idList;
        this.mDrawableSelected = context.getResources().getDrawable(R.drawable.list_selector_background_focus);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        this.mContext = null;
    }

    /* access modifiers changed from: protected */
    public void setSelection(int position) {
        this.mPosition = position;
    }

    public int getCount() {
        return this.mIdList.length;
    }

    public Object getItem(int position) {
        if (position < 0 || this.mIdList.length <= position) {
            return null;
        }
        return Integer.valueOf(this.mIdList[position]);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView instanceof ImageView) {
            imageView = (ImageView) convertView;
        } else {
            imageView = new ImageView(this.mContext);
        }
        imageView.setAdjustViewBounds(true);
        imageView.setScaleType(ImageView.ScaleType.CENTER);
        imageView.setImageResource(this.mIdList[position]);
        if (this.mPosition == position) {
            imageView.setBackgroundDrawable(this.mDrawableSelected);
        } else {
            imageView.setBackgroundColor(0);
        }
        return imageView;
    }
}
