package com.psc.fukumoto.MindMemo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class ConfirmClearDialog extends AlertDialog implements DialogInterface.OnClickListener {
    private OnButtonListener mOnButtonListener = null;

    public interface OnButtonListener {
        void onButtonCancel_ConfirmClearDialog();

        void onButtonYes_ConfirmClearDialog();
    }

    public ConfirmClearDialog(Context context, OnButtonListener listener) {
        super(context);
        this.mOnButtonListener = listener;
        setTitle(context.getString(R.string.title_confirmclear));
        setIcon(17301543);
        setMessage(context.getString(R.string.message_confirmclear));
        setButton(-1, context.getString(R.string.button_yes), this);
        setButton(-2, context.getString(R.string.button_no), this);
        setCancelable(true);
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case -3:
                this.mOnButtonListener.onButtonCancel_ConfirmClearDialog();
                return;
            case -2:
            default:
                return;
            case -1:
                this.mOnButtonListener.onButtonYes_ConfirmClearDialog();
                return;
        }
    }
}
