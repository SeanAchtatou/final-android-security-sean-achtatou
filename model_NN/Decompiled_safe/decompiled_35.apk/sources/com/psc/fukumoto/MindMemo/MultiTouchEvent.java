package com.psc.fukumoto.MindMemo;

import android.graphics.PointF;
import android.view.MotionEvent;
import com.psc.fukumoto.lib.ImageSystem;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class MultiTouchEvent {
    public static final int ACTION_MASK = 255;
    public static final int ACTION_POINTER_DOWN = 5;
    public static final int ACTION_POINTER_INDEX_MASK = 65280;
    public static final int ACTION_POINTER_INDEX_SHIFT = 8;
    public static final int ACTION_POINTER_UP = 6;
    private int mAction;
    private ArrayList<PointF> mPointList = new ArrayList<>();
    private int mPointerCount;

    public MultiTouchEvent(MotionEvent event) {
        int pointerCount = getPointerCount(event);
        if (pointerCount < 0) {
            setData_Single(event, 1);
        } else {
            setData_Multi(event, pointerCount);
        }
    }

    private void setData_Single(MotionEvent event, int pointerCount) {
        this.mAction = event.getAction();
        this.mPointerCount = pointerCount;
        if (pointerCount == 1) {
            this.mPointList.add(new PointF(event.getX(), event.getY()));
        }
    }

    private void setData_Multi(MotionEvent event, int pointerCount) {
        switch (event.getAction() & ACTION_MASK) {
            case 0:
            case ACTION_POINTER_DOWN /*5*/:
                this.mAction = 0;
                break;
            case 1:
            case ACTION_POINTER_UP /*6*/:
                this.mAction = 1;
                break;
            case 2:
                this.mAction = 2;
                break;
            case 3:
            case 4:
            default:
                this.mAction = 3;
                break;
        }
        this.mPointerCount = pointerCount;
        for (int pointerIndex = 0; pointerIndex < pointerCount; pointerIndex++) {
            this.mPointList.add(new PointF(getX(event, pointerIndex), getY(event, pointerIndex)));
        }
    }

    public int getAction() {
        return this.mAction;
    }

    public int getPointerCount() {
        return this.mPointerCount;
    }

    public float getPosX(int index) {
        if (index < 0 || this.mPointerCount <= index) {
            return ImageSystem.ROTATE_NONE;
        }
        return this.mPointList.get(index).x;
    }

    public float getPosY(int index) {
        if (index < 0 || this.mPointerCount <= index) {
            return ImageSystem.ROTATE_NONE;
        }
        return this.mPointList.get(index).y;
    }

    public PointF getPoint(int index) {
        if (index < 0 || this.mPointerCount <= index) {
            return null;
        }
        return this.mPointList.get(index);
    }

    private static final int getPointerCount(MotionEvent event) {
        if (event == null) {
            return -1;
        }
        Method method = null;
        try {
            method = event.getClass().getMethod("getPointerCount", new Class[0]);
        } catch (NoSuchMethodException e) {
        }
        if (method == null) {
            return -1;
        }
        try {
            return ((Integer) method.invoke(event, new Object[0])).intValue();
        } catch (InvocationTargetException e2) {
            InvocationTargetException ex = e2;
            Throwable cause = ex.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException(ex);
            }
        } catch (IllegalAccessException e3) {
            return -1;
        }
    }

    private static final float getX(MotionEvent event, int pointerIndex) {
        if (event == null) {
            return ImageSystem.ROTATE_NONE;
        }
        Method method = null;
        try {
            method = event.getClass().getMethod("getX", Integer.TYPE);
        } catch (NoSuchMethodException e) {
        }
        if (method == null) {
            return ImageSystem.ROTATE_NONE;
        }
        try {
            return ((Float) method.invoke(event, Integer.valueOf(pointerIndex))).floatValue();
        } catch (InvocationTargetException e2) {
            InvocationTargetException ex = e2;
            Throwable cause = ex.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException(ex);
            }
        } catch (IllegalAccessException e3) {
            return ImageSystem.ROTATE_NONE;
        }
    }

    private static final float getY(MotionEvent event, int pointerIndex) {
        if (event == null) {
            return ImageSystem.ROTATE_NONE;
        }
        Method method = null;
        try {
            method = event.getClass().getMethod("getY", Integer.TYPE);
        } catch (NoSuchMethodException e) {
        }
        if (method == null) {
            return ImageSystem.ROTATE_NONE;
        }
        try {
            return ((Float) method.invoke(event, Integer.valueOf(pointerIndex))).floatValue();
        } catch (InvocationTargetException e2) {
            InvocationTargetException ex = e2;
            Throwable cause = ex.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException(ex);
            }
        } catch (IllegalAccessException e3) {
            return ImageSystem.ROTATE_NONE;
        }
    }
}
