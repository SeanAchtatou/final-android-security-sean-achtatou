package com.psc.fukumoto.MindMemo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.psc.fukumoto.lib.FileData;
import java.util.List;

public class SelectFileDialog extends AlertDialog implements DialogInterface.OnClickListener {
    private ListView mListView;
    private OnButtonListener mOnButtonListener = null;

    public interface OnButtonListener {
        void onButtonCancel_SelectFileDialog();

        void onButtonNo_SelectFileDialog(FileData fileData);

        void onButtonYes_SelectFileDialog(FileData fileData);
    }

    public SelectFileDialog(Context context, OnButtonListener listener, List<FileData> fileDataList) {
        super(context);
        this.mOnButtonListener = listener;
        setTitle(context.getString(R.string.title_selectfile));
        setButton(-1, context.getString(R.string.button_load), this);
        setButton(-2, context.getString(R.string.button_delete), this);
        setButton(-3, context.getString(R.string.button_cancel), this);
        this.mListView = new ListView(context);
        this.mListView.setChoiceMode(1);
        SelectFileAdapter adapter = new SelectFileAdapter(context, fileDataList);
        this.mListView.setAdapter((ListAdapter) adapter);
        this.mListView.setOnItemClickListener(adapter);
        setView(this.mListView);
        setCancelable(true);
    }

    private FileData getSelectedFileData() {
        return ((SelectFileAdapter) this.mListView.getAdapter()).getItem(this.mListView.getCheckedItemPosition());
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case -3:
                this.mOnButtonListener.onButtonCancel_SelectFileDialog();
                return;
            case -2:
                this.mOnButtonListener.onButtonNo_SelectFileDialog(getSelectedFileData());
                return;
            case -1:
                this.mOnButtonListener.onButtonYes_SelectFileDialog(getSelectedFileData());
                return;
            default:
                return;
        }
    }
}
