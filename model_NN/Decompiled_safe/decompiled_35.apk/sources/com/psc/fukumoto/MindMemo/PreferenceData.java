package com.psc.fukumoto.MindMemo;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.psc.fukumoto.lib.FileData;

public class PreferenceData {
    private Context mContext;

    private static class Key {
        private static final String COLORBACKGROUND_GROUP = "ColorBackground_Group";
        private static final String COLORBACKGROUND_MEMOVIEW = "ColorBackground_MemoView";
        private static final String COLORBACKGROUND_TEXTMEMO = "ColorBackground_TextMemo";
        private static final String COLORFRAME_GROUP = "ColorFrame_Group";
        private static final String COLORFRAME_TEXTMEMO = "ColorFrame_TextMemo";
        private static final String COLORLINE_GROUP = "ColorLine_Group";
        private static final String COLORLINE_MEMOVIEW = "ColorLine_MemoView";
        private static final String COLORTEXT = "ColorText";
        private static final int CREATETAP = 2131165242;
        private static final int DELETESINGLEGROUP = 2131165239;
        private static final String FILENAME = "FileName";
        private static final String FOLDERNAME = "FolderName";
        private static final String FONTSIZE = "FontSize";
        private static final int MENUAFTERTOUCHUP = 2131165248;
        private static final int SHARETAP = 2131165245;
        private static final String TOUCHMODE = "TouchMode";
        private static final String TRANSPARENT = "Transparent";

        private Key() {
        }
    }

    private static class Default {
        private static final int COLORBACKGROUND_GROUP = -160;
        private static final int COLORBACKGROUND_MEMOVIEW = -16777216;
        private static final int COLORBACKGROUND_TEXTMEMO = -1;
        private static final int COLORFRAME_GROUP = -10420225;
        private static final int COLORFRAME_TEXTMEMO = -40864;
        private static final int COLORLINE_GROUP = -10460929;
        private static final int COLORLINE_MEMOVIEW = -10420384;
        private static final int COLORTEXT = -16777216;
        private static final boolean CREATETAP = true;
        private static final boolean DELETESINGLEGROUP = false;
        /* access modifiers changed from: private */
        public static final String FILENAME = null;
        /* access modifiers changed from: private */
        public static final String FOLDERNAME = null;
        private static final float FONTSIZE = 18.0f;
        private static final boolean MENUAFTERTOUCHUP = false;
        private static final boolean SHARETAP = true;
        private static final boolean TOUCHMODE = false;
        private static final boolean TRANSPARENT = true;

        private Default() {
        }
    }

    public PreferenceData(Context context) {
        this.mContext = context;
    }

    /* access modifiers changed from: protected */
    public boolean getTouchModeIsMove() {
        try {
            return getPreferences().getBoolean("TouchMode", false);
        } catch (Exception e) {
            e.fillInStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void setTouchModeIsMove(boolean touchModeIsMove) {
        try {
            SharedPreferences.Editor editor = getPreferences().edit();
            editor.putBoolean("TouchMode", touchModeIsMove);
            editor.commit();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public FileData getFileData() {
        String folderName = Default.FOLDERNAME;
        String fileName = Default.FILENAME;
        try {
            SharedPreferences pref = getPreferences();
            folderName = pref.getString("FolderName", folderName);
            fileName = pref.getString("FileName", fileName);
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        if (fileName == null) {
            return null;
        }
        if (folderName == null) {
            folderName = MindMemoFile.getDefaultFolderName();
        }
        return new FileData(folderName, fileName, MindMemoFile.DATA_FILE_EXTENSION);
    }

    /* access modifiers changed from: protected */
    public void setFileData(FileData fileData) {
        if (fileData != null) {
            try {
                SharedPreferences.Editor editor = getPreferences().edit();
                editor.putString("FolderName", fileData.getFolderName());
                editor.putString("FileName", fileData.getFileName());
                editor.commit();
            } catch (Exception e) {
                e.fillInStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public float getFontSize() {
        try {
            return getPreferences().getFloat("FontSize", 18.0f);
        } catch (Exception e) {
            e.fillInStackTrace();
            return 18.0f;
        }
    }

    /* access modifiers changed from: protected */
    public void setFontSize(float fontSize) {
        try {
            SharedPreferences.Editor editor = getPreferences().edit();
            editor.putFloat("FontSize", fontSize);
            editor.commit();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public int getColorText() {
        try {
            return getPreferences().getInt("ColorText", -16777216);
        } catch (Exception e) {
            e.fillInStackTrace();
            return -16777216;
        }
    }

    /* access modifiers changed from: protected */
    public void setColorText(int colorText) {
        try {
            SharedPreferences.Editor editor = getPreferences().edit();
            editor.putInt("ColorText", colorText);
            editor.commit();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public int getColorBackground_TextMemo() {
        try {
            return getPreferences().getInt("ColorBackground_TextMemo", -1);
        } catch (Exception e) {
            e.fillInStackTrace();
            return -1;
        }
    }

    /* access modifiers changed from: protected */
    public void setColorBackground_TextMemo(int colorBackground) {
        try {
            SharedPreferences.Editor editor = getPreferences().edit();
            editor.putInt("ColorBackground_TextMemo", colorBackground);
            editor.commit();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public int getColorFrame_TextMemo() {
        try {
            return getPreferences().getInt("ColorFrame_TextMemo", -40864);
        } catch (Exception e) {
            e.fillInStackTrace();
            return -40864;
        }
    }

    /* access modifiers changed from: protected */
    public void setColorFrame_TextMemo(int colorFrame) {
        try {
            SharedPreferences.Editor editor = getPreferences().edit();
            editor.putInt("ColorFrame_TextMemo", colorFrame);
            editor.commit();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public boolean getTransparent() {
        try {
            return getPreferences().getBoolean("Transparent", true);
        } catch (Exception e) {
            e.fillInStackTrace();
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void setTransparent(boolean transparent) {
        try {
            SharedPreferences.Editor editor = getPreferences().edit();
            editor.putBoolean("Transparent", transparent);
            editor.commit();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public int getColorBackground_Group() {
        try {
            return getPreferences().getInt("ColorBackground_Group", -160);
        } catch (Exception e) {
            e.fillInStackTrace();
            return -160;
        }
    }

    /* access modifiers changed from: protected */
    public void setColorBackground_Group(int colorBackground) {
        try {
            SharedPreferences.Editor editor = getPreferences().edit();
            editor.putInt("ColorBackground_Group", colorBackground);
            editor.commit();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public int getColorFrame_Group() {
        try {
            return getPreferences().getInt("ColorFrame_Group", -10420225);
        } catch (Exception e) {
            e.fillInStackTrace();
            return -10420225;
        }
    }

    /* access modifiers changed from: protected */
    public void setColorFrame_Group(int colorFrame) {
        try {
            SharedPreferences.Editor editor = getPreferences().edit();
            editor.putInt("ColorFrame_Group", colorFrame);
            editor.commit();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public int getColorLine_Group() {
        try {
            return getPreferences().getInt("ColorLine_Group", -10460929);
        } catch (Exception e) {
            e.fillInStackTrace();
            return -10460929;
        }
    }

    /* access modifiers changed from: protected */
    public void setColorLine_Group(int colorLine) {
        try {
            SharedPreferences.Editor editor = getPreferences().edit();
            editor.putInt("ColorLine_Group", colorLine);
            editor.commit();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public int getColorBackground_MemoView() {
        try {
            return getPreferences().getInt("ColorBackground_MemoView", -16777216);
        } catch (Exception e) {
            e.fillInStackTrace();
            return -16777216;
        }
    }

    /* access modifiers changed from: protected */
    public void setColorBackground_MemoView(int colorBackground) {
        try {
            SharedPreferences.Editor editor = getPreferences().edit();
            editor.putInt("ColorBackground_MemoView", colorBackground);
            editor.commit();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public int getColorLine_MemoView() {
        try {
            return getPreferences().getInt("ColorLine_MemoView", -10420384);
        } catch (Exception e) {
            e.fillInStackTrace();
            return -10420384;
        }
    }

    /* access modifiers changed from: protected */
    public void setColorLine_MemoView(int colorLine) {
        try {
            SharedPreferences.Editor editor = getPreferences().edit();
            editor.putInt("ColorLine_MemoView", colorLine);
            editor.commit();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public boolean getDeleteSingleGroup() {
        try {
            return getPreferences().getBoolean(this.mContext.getString(R.string.key_deletesinglegroup), false);
        } catch (Exception e) {
            e.fillInStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean getCreateTap() {
        try {
            return getPreferences().getBoolean(this.mContext.getString(R.string.key_createtap), true);
        } catch (Exception e) {
            e.fillInStackTrace();
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public boolean getShareTap() {
        try {
            return getPreferences().getBoolean(this.mContext.getString(R.string.key_sharetap), true);
        } catch (Exception e) {
            e.fillInStackTrace();
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public boolean getMenuAfterTouchUp() {
        try {
            return getPreferences().getBoolean(this.mContext.getString(R.string.key_menuaftertouchup), false);
        } catch (Exception e) {
            e.fillInStackTrace();
            return false;
        }
    }

    private SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(this.mContext);
    }
}
