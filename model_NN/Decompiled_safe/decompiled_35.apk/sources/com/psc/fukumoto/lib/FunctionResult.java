package com.psc.fukumoto.lib;

public class FunctionResult<T> {
    private Exception mException;
    private boolean mSuccess;
    private T mValue;

    public FunctionResult(boolean success, T value) {
        this.mSuccess = success;
        this.mValue = value;
    }

    public FunctionResult(Exception exception) {
        this.mSuccess = false;
        this.mException = exception;
    }

    public boolean success() {
        return this.mSuccess;
    }

    public Exception getException() {
        return this.mException;
    }

    public T getValue() {
        return this.mValue;
    }
}
