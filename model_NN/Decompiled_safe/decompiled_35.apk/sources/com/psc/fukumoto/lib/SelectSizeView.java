package com.psc.fukumoto.lib;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

public class SelectSizeView extends ListView implements AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener {
    private MoveToActivity mMoveToActivity = null;
    private SizeList mSizeList = null;

    public interface MoveToActivity {
        void moveToPrevious(int i, int i2);
    }

    public SelectSizeView(Context context, MoveToActivity activity) {
        super(context);
        this.mMoveToActivity = activity;
        setItemsCanFocus(false);
        setChoiceMode(1);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        this.mMoveToActivity = null;
    }

    public void setSizeList(SizeList sizeList, int previewWidth, int previewHeight) {
        this.mSizeList = sizeList;
        int sizeNum = this.mSizeList.getSizeNum();
        int selectedIndex = -1;
        String[] selectList = new String[sizeNum];
        for (int index = 0; index < sizeNum; index++) {
            int width = this.mSizeList.getWidth(index);
            int height = this.mSizeList.getHeight(index);
            selectList[index] = String.valueOf(Integer.toString(width)) + " x " + Integer.toString(height);
            if (width == previewWidth && height == previewHeight) {
                selectedIndex = index;
            }
        }
        setAdapter((ListAdapter) new ArrayAdapter<>(getContext(), 17367055, selectList));
        setItemChecked(selectedIndex, true);
        setOnItemSelectedListener(this);
        setOnItemClickListener(this);
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        finishSelect(position);
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        finishSelect(position);
    }

    private void finishSelect(int position) {
        this.mMoveToActivity.moveToPrevious(this.mSizeList.getWidth(position), this.mSizeList.getHeight(position));
    }
}
