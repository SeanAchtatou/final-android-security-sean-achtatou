package com.psc.fukumoto.lib;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TextImageView extends RelativeLayout {
    private static final int FILL_PARENT = -1;
    private ImageView mImageView = null;
    private TextView mTextView = null;

    public TextImageView(Context context) {
        super(context);
        createChildView(context);
    }

    private void createChildView(Context context) {
        this.mImageView = new ImageView(context);
        this.mImageView.setAdjustViewBounds(true);
        this.mImageView.setScaleType(ImageView.ScaleType.CENTER);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) FILL_PARENT, (int) FILL_PARENT);
        params.addRule(13);
        addViewInLayout(this.mImageView, FILL_PARENT, params);
        this.mTextView = new TextView(context);
        this.mTextView.setGravity(17);
        RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams((int) FILL_PARENT, (int) FILL_PARENT);
        params2.addRule(13);
        addViewInLayout(this.mTextView, FILL_PARENT, params2);
    }

    public void setImageBitmap(Bitmap bitmapImage) {
        if (this.mImageView != null) {
            this.mImageView.setImageBitmap(bitmapImage);
        }
    }

    public void setText(String text) {
        if (this.mTextView != null) {
            this.mTextView.setText(text);
        }
    }
}
