package com.psc.fukumoto.lib;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;
import android.widget.TextView;
import com.psc.fukumoto.MindMemo.MultiTouchEvent;

public class RotateTextView extends TextView {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$psc$fukumoto$lib$RotateTextView$DIRECTION;
    private DIRECTION mDirection = DIRECTION.RIGHT_RIGHT;
    private int mTextHeight = 0;
    private int mTextWidth = 0;

    public enum DIRECTION {
        NONE,
        LEFT_LEFT,
        LEFT_RIGHT,
        RIGHT_LEFT,
        RIGHT_RIGHT,
        REVERSE
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$psc$fukumoto$lib$RotateTextView$DIRECTION() {
        int[] iArr = $SWITCH_TABLE$com$psc$fukumoto$lib$RotateTextView$DIRECTION;
        if (iArr == null) {
            iArr = new int[DIRECTION.values().length];
            try {
                iArr[DIRECTION.LEFT_LEFT.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[DIRECTION.LEFT_RIGHT.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[DIRECTION.NONE.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[DIRECTION.REVERSE.ordinal()] = 6;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[DIRECTION.RIGHT_LEFT.ordinal()] = 4;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[DIRECTION.RIGHT_RIGHT.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            $SWITCH_TABLE$com$psc$fukumoto$lib$RotateTextView$DIRECTION = iArr;
        }
        return iArr;
    }

    public RotateTextView(Context context, DIRECTION direction) {
        super(context);
        this.mDirection = direction;
    }

    public void setText(CharSequence text, TextView.BufferType type) {
        this.mTextWidth = 0;
        this.mTextHeight = 0;
        super.setText(text, type);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthMeasureSpec2 = getMeasuredWidth();
        int heightMeasureSpec2 = getMeasuredHeight();
        int width = View.MeasureSpec.getSize(widthMeasureSpec2);
        int height = View.MeasureSpec.getSize(heightMeasureSpec2);
        if (this.mTextWidth == 0) {
            this.mTextWidth = width;
        }
        if (this.mTextHeight == 0) {
            this.mTextHeight = height;
        }
        if (this.mDirection != DIRECTION.REVERSE) {
            if (width > height) {
                height = width;
            } else {
                width = height;
            }
            setMeasuredDimension(width, height);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        canvas.save();
        switch ($SWITCH_TABLE$com$psc$fukumoto$lib$RotateTextView$DIRECTION()[this.mDirection.ordinal()]) {
            case 2:
                onLeft_Left(canvas);
                break;
            case 3:
                onLeft_Right(canvas);
                break;
            case 4:
                onRight_Left(canvas);
                break;
            case MultiTouchEvent.ACTION_POINTER_DOWN:
                onRight_Right(canvas);
                break;
            case MultiTouchEvent.ACTION_POINTER_UP:
                onReverse(canvas);
                break;
        }
        super.onDraw(canvas);
        canvas.restore();
    }

    private void onLeft_Left(Canvas canvas) {
        int height = getHeight();
        canvas.rotate(-90.0f);
        canvas.translate((float) (-height), ImageSystem.ROTATE_NONE);
    }

    private void onLeft_Right(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();
        canvas.rotate(-90.0f);
        canvas.translate((float) (-height), (float) (width - this.mTextHeight));
    }

    private void onRight_Left(Canvas canvas) {
        canvas.rotate(90.0f);
        canvas.translate(ImageSystem.ROTATE_NONE, (float) (-this.mTextHeight));
    }

    private void onRight_Right(Canvas canvas) {
        int width = getWidth();
        canvas.rotate(90.0f);
        canvas.translate(ImageSystem.ROTATE_NONE, (float) (-width));
    }

    private void onReverse(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();
        canvas.rotate(180.0f);
        canvas.translate((float) (-width), (float) (-height));
    }
}
