package com.psc.fukumoto.lib;

public interface PlayEffectListener {
    void playEffect(int i);
}
