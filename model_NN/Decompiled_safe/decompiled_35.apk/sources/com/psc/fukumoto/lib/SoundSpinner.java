package com.psc.fukumoto.lib;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

public class SoundSpinner extends Spinner {
    private int mClickSoundId = 0;
    private AlertDialog mDialog;
    private PlayEffectListener mPlayEffectListener = null;
    private PlaySoundListener mPlaySoundListener = null;
    private int mSelectedSoundId = 0;

    public void setPlayEffectListener(PlayEffectListener playEffectListener) {
        this.mPlayEffectListener = playEffectListener;
    }

    public void setPlaySoundListener(PlaySoundListener playSoundListener) {
        this.mPlaySoundListener = playSoundListener;
    }

    public SoundSpinner(Context context) {
        super(context, (AttributeSet) null);
    }

    public SoundSpinner(Context context, PlaySoundListener listener) {
        super(context);
        this.mPlaySoundListener = listener;
        setSoundEffectsEnabled(false);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        this.mPlayEffectListener = null;
        this.mPlaySoundListener = null;
    }

    public void setClickSoundId(int clickSoundId) {
        this.mClickSoundId = clickSoundId;
    }

    public void setSelectedSoundId(int selectedSoundId) {
        this.mSelectedSoundId = selectedSoundId;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mDialog != null && this.mDialog.isShowing()) {
            this.mDialog.dismiss();
            this.mDialog = null;
        }
    }

    public boolean performClick() {
        if (this.mClickSoundId > 0) {
            if (this.mPlayEffectListener != null) {
                this.mPlayEffectListener.playEffect(this.mClickSoundId);
            }
            if (this.mPlaySoundListener != null) {
                this.mPlaySoundListener.playSound(this.mClickSoundId);
            }
        }
        Context context = getContext();
        this.mDialog = new AlertDialog.Builder(context).setSingleChoiceItems(new DropDownAdapter(getAdapter()), getSelectedItemPosition(), this).show();
        this.mDialog.getListView().setSoundEffectsEnabled(false);
        return true;
    }

    public void onClick(DialogInterface dialog, int which) {
        if (this.mSelectedSoundId > 0) {
            if (this.mPlayEffectListener != null) {
                this.mPlayEffectListener.playEffect(this.mSelectedSoundId);
            }
            if (this.mPlaySoundListener != null) {
                this.mPlaySoundListener.playSound(this.mSelectedSoundId);
            }
        }
        setSelection(which);
        dialog.dismiss();
        this.mDialog = null;
    }

    private static class DropDownAdapter implements ListAdapter, SpinnerAdapter {
        private SpinnerAdapter mAdapter;
        private ListAdapter mListAdapter;

        public DropDownAdapter(SpinnerAdapter adapter) {
            this.mAdapter = adapter;
            if (adapter instanceof ListAdapter) {
                this.mListAdapter = (ListAdapter) adapter;
            }
        }

        public int getCount() {
            if (this.mAdapter == null) {
                return 0;
            }
            return this.mAdapter.getCount();
        }

        public Object getItem(int position) {
            if (this.mAdapter == null) {
                return null;
            }
            return this.mAdapter.getItem(position);
        }

        public long getItemId(int position) {
            if (this.mAdapter == null) {
                return -1;
            }
            return this.mAdapter.getItemId(position);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getDropDownView(position, convertView, parent);
            view.setSoundEffectsEnabled(false);
            return view;
        }

        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View view = this.mAdapter == null ? null : this.mAdapter.getDropDownView(position, convertView, parent);
            view.setSoundEffectsEnabled(false);
            return view;
        }

        public boolean hasStableIds() {
            return this.mAdapter != null && this.mAdapter.hasStableIds();
        }

        public void registerDataSetObserver(DataSetObserver observer) {
            if (this.mAdapter != null) {
                this.mAdapter.registerDataSetObserver(observer);
            }
        }

        public void unregisterDataSetObserver(DataSetObserver observer) {
            if (this.mAdapter != null) {
                this.mAdapter.unregisterDataSetObserver(observer);
            }
        }

        public boolean areAllItemsEnabled() {
            ListAdapter adapter = this.mListAdapter;
            if (adapter != null) {
                return adapter.areAllItemsEnabled();
            }
            return true;
        }

        public boolean isEnabled(int position) {
            ListAdapter adapter = this.mListAdapter;
            if (adapter != null) {
                return adapter.isEnabled(position);
            }
            return true;
        }

        public int getItemViewType(int position) {
            return 0;
        }

        public int getViewTypeCount() {
            return 1;
        }

        public boolean isEmpty() {
            return getCount() == 0;
        }
    }
}
