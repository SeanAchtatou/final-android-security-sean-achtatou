package com.psc.fukumoto.lib;

import java.io.Serializable;

public class SizeList implements Serializable {
    private static final long serialVersionUID = 1;
    private int[] mHeightList;
    private int mLength;
    private int mSizeNum = 0;
    private int[] mWidthList;

    public SizeList(int length) {
        this.mLength = length;
        this.mWidthList = new int[length];
        this.mHeightList = new int[length];
    }

    public boolean addSize(int width, int height) {
        if (this.mSizeNum > this.mLength) {
            return false;
        }
        this.mWidthList[this.mSizeNum] = width;
        this.mHeightList[this.mSizeNum] = height;
        this.mSizeNum++;
        return true;
    }

    public int getSizeNum() {
        return this.mSizeNum;
    }

    public int getWidth(int index) {
        if (index < 0 || this.mSizeNum <= index) {
            return 0;
        }
        return this.mWidthList[index];
    }

    public int getHeight(int index) {
        if (index < 0 || this.mSizeNum <= index) {
            return 0;
        }
        return this.mHeightList[index];
    }
}
