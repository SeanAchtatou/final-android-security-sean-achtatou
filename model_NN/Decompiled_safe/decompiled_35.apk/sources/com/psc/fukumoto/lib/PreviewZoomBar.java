package com.psc.fukumoto.lib;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.widget.SeekBar;
import java.util.List;

public class PreviewZoomBar extends SeekBar {
    private int mColorText = -1;
    private int mMaxZoom = 0;
    private List<Integer> mZoomRatioList = null;

    public PreviewZoomBar(Context context) {
        super(context);
    }

    public void setTextColor(int colorText) {
        this.mColorText = colorText;
    }

    public void setZoomRatioList(int maxZoom, List<Integer> zoomRatioList) {
        if (maxZoom >= zoomRatioList.size()) {
            maxZoom = zoomRatioList.size() - 1;
        }
        this.mMaxZoom = maxZoom;
        this.mZoomRatioList = zoomRatioList;
        setMax(this.mMaxZoom);
        setProgress(this.mMaxZoom);
        if (this.mMaxZoom > 1) {
            setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.mZoomRatioList != null) {
            int zoom = this.mMaxZoom - getProgress();
            if (zoom < 0) {
                zoom = 0;
            }
            if (zoom > this.mMaxZoom) {
                zoom = this.mMaxZoom;
            }
            String ratioText = String.format("x%2.2f", Float.valueOf(((float) this.mZoomRatioList.get(zoom).intValue()) / 100.0f));
            Paint paint = new Paint();
            paint.setColor(this.mColorText);
            Paint.FontMetrics fontMetrics = paint.getFontMetrics();
            canvas.drawText(ratioText, (((float) getWidth()) - paint.measureText(ratioText)) / 2.0f, ((((float) getHeight()) - (fontMetrics.bottom - fontMetrics.top)) / 2.0f) - fontMetrics.ascent, paint);
        }
    }
}
