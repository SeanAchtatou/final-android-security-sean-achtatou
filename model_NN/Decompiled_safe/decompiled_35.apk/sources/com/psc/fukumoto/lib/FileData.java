package com.psc.fukumoto.lib;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import java.io.File;

public class FileData {
    private boolean mExist;
    private String mExtension;
    private String mFileName;
    private String mFolderName;
    private boolean mIsFolder;

    public FileData(String folderName, String fileName) {
        this.mFolderName = folderName;
        File file = new File(String.valueOf(folderName) + File.separator + fileName);
        this.mExist = file.exists();
        this.mIsFolder = file.isDirectory();
        if (this.mIsFolder) {
            this.mFileName = fileName;
            this.mExtension = null;
            return;
        }
        int length = fileName.length();
        int index = fileName.lastIndexOf(46);
        if (index <= 0 || index >= length - 1) {
            this.mFileName = fileName;
            this.mExtension = null;
            return;
        }
        this.mFileName = fileName.substring(0, index);
        this.mExtension = fileName.substring(index);
    }

    public FileData(String folderName, String fileName, String extension) {
        this.mFolderName = folderName;
        this.mFileName = fileName;
        this.mExtension = extension;
        File file = new File(String.valueOf(folderName) + File.separator + fileName + extension);
        this.mExist = file.exists();
        this.mIsFolder = file.isDirectory();
    }

    public String getFullName() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.mFolderName);
        builder.append(File.separator);
        builder.append(this.mFileName);
        if (this.mExtension != null) {
            builder.append(this.mExtension);
        }
        return builder.toString();
    }

    public String getFolderName() {
        return this.mFolderName;
    }

    public String getFileName() {
        return this.mFileName;
    }

    public String getExtension() {
        return this.mExtension;
    }

    public String getFileWithExtention() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.mFileName);
        if (this.mExtension != null) {
            builder.append(this.mExtension);
        }
        return builder.toString();
    }

    public boolean isFolder() {
        return this.mIsFolder;
    }

    public boolean exist() {
        return this.mExist;
    }

    public boolean isSame(FileData fileData) {
        if (fileData == null) {
            return false;
        }
        if (!this.mFolderName.equals(fileData.mFolderName)) {
            return false;
        }
        if (!this.mFileName.equals(fileData.mFileName)) {
            return false;
        }
        if (this.mExtension != null) {
            if (!this.mExtension.equals(fileData.mExtension)) {
                return false;
            }
        } else if (fileData.mExtension != null) {
            return false;
        }
        return true;
    }

    public static final FileData getIntentFileName(Context context, Intent intent) {
        String dataString = intent.getDataString();
        if (dataString == null || dataString.length() == 0) {
            return null;
        }
        String fullName = Uri.parse(dataString).getPath();
        if (fullName == null || fullName.length() == 0) {
            return null;
        }
        int position = fullName.lastIndexOf(47);
        if (position < 0) {
            return null;
        }
        return new FileData(fullName.substring(0, position), fullName.substring(position + 1));
    }
}
