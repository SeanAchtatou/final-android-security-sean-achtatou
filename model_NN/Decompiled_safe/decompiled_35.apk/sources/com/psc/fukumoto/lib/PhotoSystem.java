package com.psc.fukumoto.lib;

import android.graphics.Bitmap;
import com.psc.fukumoto.MindMemo.MultiTouchEvent;

public class PhotoSystem {
    public static final Bitmap createBitmapFromCamera(byte[] data, int dataWidth, int dataHeight) {
        if (data == null || dataWidth <= 0 || dataHeight <= 0) {
            return null;
        }
        int imageWidth = (dataWidth / 2) * 2;
        int imageHeight = (dataHeight / 2) * 2;
        Bitmap bitmap = null;
        try {
            bitmap = Bitmap.createBitmap(decodeYUV420ToRGB(data, dataWidth, dataHeight, imageWidth, imageHeight), imageWidth, imageHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError e) {
            e.fillInStackTrace();
        }
        return bitmap;
    }

    /* JADX INFO: Multiple debug info for r3v13 int: [D('bx' int), D('v' int)] */
    /* JADX INFO: Multiple debug info for r20v16 int: [D('y' int), D('b' int)] */
    /* JADX INFO: Multiple debug info for r20v25 int: [D('y' int), D('b' int)] */
    /* JADX INFO: Multiple debug info for r20v34 int: [D('y' int), D('b' int)] */
    /* JADX INFO: Multiple debug info for r20v44 int: [D('y' int), D('b' int)] */
    /* JADX INFO: Multiple debug info for r5v28 int: [D('yPoint' int), D('g' int)] */
    public static final int[] decodeYUV420ToRGB(byte[] yuv420sp, int dataWidth, int dataHeight, int imageWidth, int imageHeight) {
        if (yuv420sp == null) {
            return null;
        }
        int frameSize = dataWidth * dataHeight;
        int[] rgb = new int[(imageWidth * imageHeight)];
        int posY = 0;
        while (true) {
            int posY2 = posY;
            if (posY2 >= imageHeight) {
                return rgb;
            }
            int posX = 0;
            int yPoint = posY2 * dataWidth;
            int rgbPoint = posY2 * imageWidth;
            int rgbPoint2 = frameSize + ((posY2 >> 1) * dataWidth);
            while (posX < imageWidth) {
                int uvPoint = rgbPoint2 + 1;
                int v = yuv420sp[rgbPoint2] & MultiTouchEvent.ACTION_MASK;
                int uvPoint2 = uvPoint + 1;
                int u = yuv420sp[uvPoint] & MultiTouchEvent.ACTION_MASK;
                int rx = (v * 1634) - 228224;
                int gx = ((u * -400) - (v * 833)) - -138752;
                int v2 = (u * 2066) - 283520;
                int y = (yuv420sp[yPoint] & 255) * 1192;
                int r = roundValue(y + rx, 0, 262143);
                int g = roundValue(y + gx, 0, 262143);
                rgb[rgbPoint] = ((roundValue(y + v2, 0, 262143) >> 10) & MultiTouchEvent.ACTION_MASK) | ((g >> 2) & MultiTouchEvent.ACTION_POINTER_INDEX_MASK) | ((r << 6) & 16711680) | -16777216;
                int y2 = (yuv420sp[yPoint + 1] & 255) * 1192;
                int r2 = roundValue(y2 + rx, 0, 262143);
                int g2 = roundValue(y2 + gx, 0, 262143);
                rgb[rgbPoint + 1] = ((roundValue(y2 + v2, 0, 262143) >> 10) & MultiTouchEvent.ACTION_MASK) | ((g2 >> 2) & MultiTouchEvent.ACTION_POINTER_INDEX_MASK) | ((r2 << 6) & 16711680) | -16777216;
                int y3 = (yuv420sp[yPoint + dataWidth] & 255) * 1192;
                int r3 = roundValue(y3 + rx, 0, 262143);
                int g3 = roundValue(y3 + gx, 0, 262143);
                rgb[rgbPoint + imageWidth] = ((roundValue(y3 + v2, 0, 262143) >> 10) & MultiTouchEvent.ACTION_MASK) | ((g3 >> 2) & MultiTouchEvent.ACTION_POINTER_INDEX_MASK) | ((r3 << 6) & 16711680) | -16777216;
                int y4 = (yuv420sp[yPoint + dataWidth + 1] & 255) * 1192;
                int r4 = roundValue(y4 + rx, 0, 262143);
                int g4 = roundValue(y4 + gx, 0, 262143);
                rgb[rgbPoint + imageWidth + 1] = ((roundValue(y4 + v2, 0, 262143) >> 10) & MultiTouchEvent.ACTION_MASK) | ((g4 >> 2) & MultiTouchEvent.ACTION_POINTER_INDEX_MASK) | -16777216 | ((r4 << 6) & 16711680);
                posX += 2;
                yPoint += 2;
                rgbPoint += 2;
                rgbPoint2 = uvPoint2;
            }
            posY = posY2 + 2;
        }
    }

    public static final int roundValue(int value, int min, int max) {
        if (value < min) {
            return min;
        }
        if (value > max) {
            return max;
        }
        return value;
    }
}
