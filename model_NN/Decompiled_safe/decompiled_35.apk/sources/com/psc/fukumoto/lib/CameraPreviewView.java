package com.psc.fukumoto.lib;

import android.content.Context;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.SeekBar;
import java.util.List;

public class CameraPreviewView extends SurfaceView implements SurfaceHolder.Callback, SeekBar.OnSeekBarChangeListener {
    public static final String IMAGE_EXTENSION = ".jpg";
    private Camera.AutoFocusCallback mAutoFocusListener;
    /* access modifiers changed from: private */
    public Camera mCamera;
    private boolean mFixPhotoSize;
    private boolean mHasSurface;
    private int mHeight;
    private int mImageHeight;
    private int mImageWidth;
    private boolean mIsZoomSupported;
    /* access modifiers changed from: private */
    public Camera.PictureCallback mJpegListener;
    private int mMaxZoom;
    private OnCaptureListener mOnCaptureListener;
    private OnPhotoListener mOnPhotoListener;
    private final PreviewCallback mPreviewCallback;
    private int mPreviewHeight;
    private int mPreviewWidth;
    private int mWidth;
    private PreviewZoomBar mZoomBar;
    private List<Integer> mZoomRatioList;

    public interface OnCaptureListener {
        void onFinishCapture(byte[] bArr, int i, int i2);
    }

    public interface OnPhotoListener {
        void onFinishPhoto(byte[] bArr, int i, int i2);
    }

    public CameraPreviewView(Context context) {
        super(context);
        this.mCamera = null;
        this.mHasSurface = false;
        this.mWidth = 0;
        this.mHeight = 0;
        this.mPreviewWidth = 0;
        this.mPreviewHeight = 0;
        this.mImageWidth = 0;
        this.mImageHeight = 0;
        this.mFixPhotoSize = false;
        this.mIsZoomSupported = false;
        this.mMaxZoom = 0;
        this.mZoomRatioList = null;
        this.mZoomBar = null;
        this.mOnPhotoListener = null;
        this.mOnCaptureListener = null;
        this.mPreviewCallback = new PreviewCallback(this, null);
        this.mAutoFocusListener = new Camera.AutoFocusCallback() {
            public void onAutoFocus(boolean success, Camera camera) {
                if (CameraPreviewView.this.mCamera != null) {
                    camera.takePicture(null, null, CameraPreviewView.this.mJpegListener);
                }
            }
        };
        this.mJpegListener = new Camera.PictureCallback() {
            public void onPictureTaken(byte[] data, Camera camera) {
                if (CameraPreviewView.this.mCamera != null) {
                    Camera.Size previewSize = camera.getParameters().getPreviewSize();
                    CameraPreviewView.this.finishPhoto(data, previewSize.width, previewSize.height);
                }
            }
        };
        initSurface();
    }

    public CameraPreviewView(Context context, boolean fixPhotoSize) {
        this(context);
        this.mFixPhotoSize = fixPhotoSize;
    }

    public CameraPreviewView(Context context, boolean fixPhotoSize, OnCaptureListener captureListener) {
        this(context, fixPhotoSize);
        this.mOnCaptureListener = captureListener;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        this.mOnPhotoListener = null;
        this.mOnCaptureListener = null;
        closeCamera();
        try {
            super.finalize();
        } catch (Throwable ex) {
            ex.fillInStackTrace();
        }
    }

    public void onResume() {
        if (this.mHasSurface) {
            openCamera();
            CameraSystem.startPreview(this.mCamera);
            return;
        }
        initSurface();
    }

    public void onPause() {
        closeCamera();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (this.mWidth <= 0 || this.mHeight <= 0) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        } else {
            setMeasuredDimension(this.mWidth, this.mHeight);
        }
    }

    public void setViewSize(int viewWidth, int viewHeight) {
        this.mWidth = viewWidth;
        this.mHeight = viewHeight;
    }

    public boolean calcPreviewSize(int width, int height) {
        if (this.mCamera != null) {
            return true;
        }
        this.mWidth = width;
        this.mHeight = height;
        if (!openCamera()) {
            return false;
        }
        closeCamera();
        return true;
    }

    public List<Camera.Size> getPreviewSizeList() {
        if (this.mCamera == null) {
            return null;
        }
        return CameraSystem.getSupportedPreviewSizes(this.mCamera.getParameters());
    }

    public void setPreviewSize(int previewWidth, int previewHeight) {
        if (previewWidth != 0 && previewHeight != 0) {
            this.mPreviewWidth = previewWidth;
            this.mPreviewHeight = previewHeight;
            setCameraParameters(this.mCamera);
        }
    }

    public int getPreviewWidth() {
        return this.mPreviewWidth;
    }

    public int getPreviewHeight() {
        return this.mPreviewHeight;
    }

    public int getImageWidth() {
        return this.mImageWidth;
    }

    public int getImageHeight() {
        return this.mImageHeight;
    }

    public void setZoomBar(PreviewZoomBar zoomBar) {
        this.mZoomBar = zoomBar;
        this.mZoomBar.setOnSeekBarChangeListener(this);
        this.mZoomBar.setProgress(this.mZoomBar.getMax());
        this.mZoomBar.setVisibility(4);
    }

    public boolean isZoomSupported() {
        if (this.mMaxZoom < 1) {
            return false;
        }
        return this.mIsZoomSupported;
    }

    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser && this.mZoomBar != null && seekBar.getId() == this.mZoomBar.getId()) {
            onProgressChanged_ZoomBar(progress);
        }
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    private void onProgressChanged_ZoomBar(int progress) {
        int zoom = this.mZoomBar.getMax() - progress;
        Camera.Parameters cameraParams = this.mCamera.getParameters();
        CameraSystem.setZoom(cameraParams, zoom);
        this.mCamera.setParameters(cameraParams);
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        this.mWidth = width;
        this.mHeight = height;
        openCamera();
        CameraSystem.startPreview(this.mCamera);
        if (this.mOnCaptureListener != null) {
            takeCapture(this.mOnCaptureListener);
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.mHasSurface = true;
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        closeCamera();
        this.mHasSurface = false;
    }

    private void initSurface() {
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        holder.setType(3);
    }

    private boolean openCamera() {
        if (this.mCamera != null) {
            return true;
        }
        this.mCamera = CameraSystem.openCamera(getHolder());
        if (this.mCamera == null) {
            return false;
        }
        if (setCameraParameters(this.mCamera)) {
            return true;
        }
        closeCamera();
        return false;
    }

    private void closeCamera() {
        if (this.mCamera != null) {
            Camera camera = this.mCamera;
            this.mCamera = null;
            CameraSystem.closeCamera(camera);
        }
    }

    private boolean setCameraParameters(Camera camera) {
        Camera.Size previewSize;
        Camera.Size pictureSize;
        if (camera == null) {
            return false;
        }
        try {
            Camera.Parameters cameraParams = camera.getParameters();
            if (this.mImageWidth == 0 || this.mImageHeight == 0) {
                int width = this.mWidth;
                int height = this.mHeight;
                if (!this.mFixPhotoSize) {
                    width = 0;
                    height = 0;
                }
                List<Camera.Size> pictureSizeList = CameraSystem.getSupportedPictureSizes(cameraParams);
                if (pictureSizeList != null) {
                    pictureSize = CameraSystem.searchCameraSize(pictureSizeList, width, height);
                } else {
                    pictureSize = cameraParams.getPictureSize();
                }
                if (pictureSize != null) {
                    this.mImageWidth = pictureSize.width;
                    this.mImageHeight = pictureSize.height;
                } else {
                    this.mImageWidth = this.mWidth;
                    this.mImageHeight = this.mHeight;
                }
            }
            cameraParams.setPictureSize(this.mImageWidth, this.mImageHeight);
            if (this.mPreviewWidth == 0 || this.mPreviewHeight == 0) {
                int width2 = this.mImageWidth;
                int height2 = this.mImageHeight;
                if (!this.mFixPhotoSize) {
                    width2 = this.mWidth;
                    height2 = this.mHeight;
                }
                List<Camera.Size> previewSizeList = CameraSystem.getSupportedPreviewSizes(cameraParams);
                if (previewSizeList != null) {
                    previewSize = CameraSystem.searchCameraSize(previewSizeList, width2, height2);
                } else {
                    previewSize = cameraParams.getPreviewSize();
                }
                if (previewSize != null) {
                    this.mPreviewWidth = previewSize.width;
                    this.mPreviewHeight = previewSize.height;
                } else {
                    this.mPreviewWidth = width2;
                    this.mPreviewHeight = height2;
                }
            }
            cameraParams.setPreviewSize(this.mPreviewWidth, this.mPreviewHeight);
            camera.setParameters(cameraParams);
            Camera.Parameters cameraParams2 = this.mCamera.getParameters();
            this.mIsZoomSupported = CameraSystem.isZoomSupported(cameraParams2);
            if (this.mIsZoomSupported) {
                this.mZoomRatioList = CameraSystem.getZoomRatios(cameraParams2);
                this.mMaxZoom = CameraSystem.getMaxZoom(cameraParams2);
                if (this.mZoomBar != null) {
                    this.mZoomBar.setZoomRatioList(this.mMaxZoom, this.mZoomRatioList);
                }
            }
            return true;
        } catch (Exception e) {
            e.fillInStackTrace();
            return false;
        }
    }

    public boolean takeCapture(OnCaptureListener captureListener) {
        this.mOnCaptureListener = captureListener;
        if (this.mOnCaptureListener == null) {
            return false;
        }
        if (this.mCamera == null) {
            return false;
        }
        System.gc();
        this.mCamera.setOneShotPreviewCallback(this.mPreviewCallback);
        return true;
    }

    public void finishCapture(byte[] data, int width, int height) {
        int size = 0;
        if (data != null) {
            size = data.length;
        }
        byte[] dataPhoto = null;
        if (size > 0) {
            dataPhoto = new byte[size];
            System.arraycopy(data, 0, dataPhoto, 0, size);
        }
        if (this.mOnCaptureListener != null) {
            OnCaptureListener listener = this.mOnCaptureListener;
            this.mOnCaptureListener = null;
            listener.onFinishCapture(dataPhoto, width, height);
        }
    }

    private class PreviewCallback implements Camera.PreviewCallback {
        private PreviewCallback() {
        }

        /* synthetic */ PreviewCallback(CameraPreviewView cameraPreviewView, PreviewCallback previewCallback) {
            this();
        }

        public void onPreviewFrame(byte[] data, Camera camera) {
            if (CameraPreviewView.this.mCamera != null) {
                Camera.Size previewSize = camera.getParameters().getPreviewSize();
                CameraPreviewView.this.finishCapture(data, previewSize.width, previewSize.height);
            }
        }
    }

    public void doAutoFocus(Camera.AutoFocusCallback cb) {
        if (this.mCamera != null) {
            this.mCamera.autoFocus(cb);
        }
    }

    public boolean takePhoto(OnPhotoListener photoListener, boolean useAutoFocus) {
        this.mOnPhotoListener = photoListener;
        if (this.mCamera == null) {
            return false;
        }
        System.gc();
        if (useAutoFocus) {
            this.mCamera.autoFocus(this.mAutoFocusListener);
        } else {
            this.mCamera.takePicture(null, null, this.mJpegListener);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void finishPhoto(byte[] data, int width, int height) {
        int size = 0;
        if (data != null) {
            size = data.length;
        }
        byte[] dataPhoto = null;
        if (size > 0) {
            dataPhoto = new byte[size];
            System.arraycopy(data, 0, dataPhoto, 0, size);
        }
        CameraSystem.startPreview(this.mCamera);
        if (this.mOnPhotoListener != null) {
            OnPhotoListener listener = this.mOnPhotoListener;
            this.mOnPhotoListener = null;
            listener.onFinishPhoto(dataPhoto, width, height);
        }
    }
}
