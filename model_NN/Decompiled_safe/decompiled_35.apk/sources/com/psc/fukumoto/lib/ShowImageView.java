package com.psc.fukumoto.lib;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.SeekBar;

public class ShowImageView extends SurfaceView implements SurfaceHolder.Callback, GestureDetector.OnGestureListener, SeekBar.OnSeekBarChangeListener {
    public static final float POWER_RESET = 0.0f;
    private Bitmap mBitmapImage = null;
    private int mColorBackground = -16777216;
    private int mDecreaseVelocity = 60;
    private GestureDetector mGestureDetector = null;
    private int mImageHeight = 0;
    private int mImageWidth = 0;
    private String mMessage = null;
    private int mOrientation = 0;
    private Paint mPaintImage = null;
    private Paint mPaintText = null;
    private PHOTO_INIT_POSITION mPhotoInitPosition = PHOTO_INIT_POSITION.CENTER;
    private float mPhotoX = ImageSystem.ROTATE_NONE;
    private float mPhotoY = ImageSystem.ROTATE_NONE;
    private float mPower = ImageSystem.ROTATE_NONE;
    private SeekBar mPowerBar = null;
    private float mPowerDefault = 2.0f;
    private float mPowerMax = 10.0f;
    private float mPowerMin = 2.0f;
    private float mRotateDegree = ImageSystem.ROTATE_NONE;
    private int mTimeTouchRedraw = 50;
    private TimerHandler mTimerHandler = new TimerHandler(this, null);
    private boolean mUseAdjustImage = false;
    private float mVelocityX = ImageSystem.ROTATE_NONE;
    private float mVelocityY = ImageSystem.ROTATE_NONE;

    public enum PHOTO_INIT_POSITION {
        CENTER,
        LEFT_TOP
    }

    public ShowImageView(Context context) {
        super(context);
        setFocusable(true);
        setFocusableInTouchMode(true);
        createPaint();
        initSurface();
        this.mGestureDetector = new GestureDetector(context, this);
    }

    private void createPaint() {
        this.mPaintImage = new Paint();
        this.mPaintImage.setAntiAlias(true);
        this.mPaintImage.setDither(true);
        this.mPaintImage.setFilterBitmap(true);
        this.mPaintText = new Paint();
        this.mPaintText.setAntiAlias(true);
    }

    public void onResume() {
        if (this.mBitmapImage != null && this.mBitmapImage.isRecycled()) {
            this.mBitmapImage = null;
        }
    }

    public void onPause() {
    }

    public void setUseAdjustImage(boolean useAdjustImage) {
        this.mUseAdjustImage = useAdjustImage;
    }

    public void setRotateDegree(float rotateDegree) {
        if (this.mRotateDegree != rotateDegree) {
            this.mRotateDegree = rotateDegree;
            if (this.mBitmapImage == null) {
                redraw();
            }
        }
    }

    public void setColorBackground(int colorBackground) {
        this.mColorBackground = colorBackground;
    }

    public void setColorMessage(int colorMessage) {
        this.mPaintText.setColor(colorMessage);
    }

    public void setMessageFontSize(int fontSize) {
        this.mPaintText.setTextSize((float) fontSize);
    }

    public void setTimeTouchRedraw(int timeTouchRedraw) {
        this.mTimeTouchRedraw = timeTouchRedraw;
    }

    public void setDecreaseVelocity(int decreaseVelocity) {
        this.mDecreaseVelocity = decreaseVelocity;
    }

    public void setMessage(String message) {
        this.mMessage = message;
        if (message != null) {
            this.mBitmapImage = null;
        }
        if (this.mBitmapImage == null) {
            redraw();
        }
    }

    public void setOrientation(int orientation) {
        if (this.mOrientation != orientation) {
            this.mOrientation = orientation;
        }
    }

    public void setBitmapImage(Bitmap bitmapImage) {
        setBitmapImage(bitmapImage, PHOTO_INIT_POSITION.CENTER);
    }

    public void setBitmapImage(Bitmap bitmapImage, PHOTO_INIT_POSITION photoInitPosition) {
        this.mBitmapImage = bitmapImage;
        this.mPhotoInitPosition = photoInitPosition;
        if (bitmapImage != null) {
            this.mImageWidth = bitmapImage.getWidth();
            this.mImageHeight = bitmapImage.getHeight();
        }
        redraw();
    }

    public Bitmap getBitmapImage() {
        return this.mBitmapImage;
    }

    public Bitmap getBitmapShown() {
        int imageWidth = getWidth();
        int imageHeight = getHeight();
        float posX = this.mPhotoX;
        float posY = this.mPhotoY;
        if (posX > ImageSystem.ROTATE_NONE) {
            imageWidth -= (int) posX;
            posX = ImageSystem.ROTATE_NONE;
        }
        if (posY > ImageSystem.ROTATE_NONE) {
            imageHeight -= (int) posY;
            posY = ImageSystem.ROTATE_NONE;
        }
        Bitmap bitmap = null;
        try {
            bitmap = Bitmap.createBitmap(imageWidth, imageHeight, Bitmap.Config.RGB_565);
            drawBitmapImage(new Canvas(bitmap), posX, posY);
            return bitmap;
        } catch (OutOfMemoryError e) {
            e.fillInStackTrace();
            return bitmap;
        }
    }

    public void setPowerBar(SeekBar powerBar) {
        this.mPowerBar = powerBar;
        powerBar.setOnSeekBarChangeListener(this);
        this.mPowerBar.setProgress(this.mPowerBar.getMax());
    }

    public void setPowerMinMax(float powerMin, float powerMax) {
        this.mPowerMin = powerMin;
        this.mPowerMax = powerMax;
        float max = (float) this.mPowerBar.getMax();
        this.mPowerBar.setProgress((int) (max - (((this.mPowerDefault - this.mPowerMin) / (this.mPowerMax - this.mPowerMin)) * max)));
    }

    public boolean setPower(float power) {
        float width = (float) getWidth();
        float height = (float) getHeight();
        if (width == ImageSystem.ROTATE_NONE || height == ImageSystem.ROTATE_NONE) {
            return false;
        }
        if (power == ImageSystem.ROTATE_NONE || this.mPower == ImageSystem.ROTATE_NONE) {
            power = this.mPowerDefault;
            if (this.mPhotoInitPosition == PHOTO_INIT_POSITION.LEFT_TOP) {
                this.mPhotoX = ImageSystem.ROTATE_NONE;
                this.mPhotoY = ImageSystem.ROTATE_NONE;
            } else {
                this.mPhotoX = (width - (((float) this.mImageWidth) * power)) / 2.0f;
                this.mPhotoY = (height - (((float) this.mImageHeight) * power)) / 2.0f;
            }
        } else {
            float drawCenterX = (width / 2.0f) - this.mPhotoX;
            float drawCenterY = (height / 2.0f) - this.mPhotoY;
            float drawWidth = (drawCenterX * power) / this.mPower;
            float drawHeight = (drawCenterY * power) / this.mPower;
            this.mPhotoX = (width / 2.0f) - drawWidth;
            this.mPhotoY = (height / 2.0f) - drawHeight;
        }
        this.mPower = power;
        adjustImagePosition();
        redraw();
        return true;
    }

    public float getPowerDefault(int imageWidth, int imageHeight) {
        float width = (float) getWidth();
        float height = (float) getHeight();
        this.mImageWidth = imageWidth;
        this.mImageHeight = imageHeight;
        this.mPowerDefault = Math.min(width / ((float) imageWidth), height / ((float) imageHeight));
        setPower(ImageSystem.ROTATE_NONE);
        return this.mPowerDefault;
    }

    private void redraw() {
        Canvas canvas;
        SurfaceHolder holder = getHolder();
        if (holder != null && (canvas = holder.lockCanvas()) != null) {
            drawView(canvas);
            holder.unlockCanvasAndPost(canvas);
        }
    }

    private void drawView(Canvas canvas) {
        canvas.drawColor(this.mColorBackground);
        if (this.mBitmapImage != null) {
            drawBitmapImage(canvas, this.mPhotoX, this.mPhotoY);
        } else {
            drawMessage(canvas, this.mMessage);
        }
    }

    private void drawBitmapImage(Canvas canvas, float posX, float posY) {
        if (this.mBitmapImage != null) {
            Rect rectPhoto = new Rect(0, 0, this.mImageWidth, this.mImageHeight);
            Rect rectDraw = new Rect();
            rectDraw.left = (int) posX;
            rectDraw.right = (int) ((((float) this.mImageWidth) * this.mPower) + posX);
            rectDraw.top = (int) posY;
            rectDraw.bottom = (int) ((((float) this.mImageHeight) * this.mPower) + posY);
            canvas.drawBitmap(this.mBitmapImage, rectPhoto, rectDraw, this.mPaintImage);
        }
    }

    private void drawMessage(Canvas canvas, String message) {
        float drawX;
        float drawY;
        if (message != null) {
            Paint.FontMetrics fontMetrics = this.mPaintText.getFontMetrics();
            float textWidth = this.mPaintText.measureText(message);
            float textHeight = fontMetrics.bottom - fontMetrics.top;
            int width = getWidth();
            int height = getHeight();
            canvas.save();
            canvas.rotate(this.mRotateDegree);
            if (this.mRotateDegree == -90.0f) {
                drawX = ((((float) height) - textWidth) / 2.0f) - ((float) height);
                drawY = ((((float) width) - textHeight) / 2.0f) - fontMetrics.ascent;
            } else if (this.mRotateDegree == 90.0f) {
                drawX = (((float) height) - textWidth) / 2.0f;
                drawY = (((((float) width) - textHeight) / 2.0f) - fontMetrics.ascent) - ((float) width);
            } else if (this.mRotateDegree == 180.0f) {
                drawX = ((((float) width) - textWidth) / 2.0f) - ((float) width);
                drawY = (((((float) height) - textHeight) / 2.0f) - fontMetrics.ascent) - ((float) height);
            } else {
                drawX = (((float) width) - textWidth) / 2.0f;
                drawY = ((((float) height) - textHeight) / 2.0f) - fontMetrics.ascent;
            }
            canvas.drawText(message, drawX, drawY, this.mPaintText);
            canvas.restore();
        }
    }

    private void adjustImagePosition() {
        if (this.mUseAdjustImage) {
            int width = getWidth();
            int height = getHeight();
            float drawWidth = ((float) this.mImageWidth) * this.mPower;
            float drawHeight = ((float) this.mImageHeight) * this.mPower;
            boolean adjusted = false;
            if (drawWidth < ((float) width)) {
                if (this.mPhotoX + drawWidth > ((float) width)) {
                    this.mPhotoX = ((float) width) - drawWidth;
                    adjusted = true;
                }
                if (this.mPhotoX < ImageSystem.ROTATE_NONE) {
                    this.mPhotoX = ImageSystem.ROTATE_NONE;
                    adjusted = true;
                }
            } else {
                if (this.mPhotoX + drawWidth < ((float) width)) {
                    this.mPhotoX = ((float) width) - drawWidth;
                    adjusted = true;
                }
                if (this.mPhotoX > ImageSystem.ROTATE_NONE) {
                    this.mPhotoX = ImageSystem.ROTATE_NONE;
                    adjusted = true;
                }
            }
            if (drawHeight < ((float) height)) {
                if (this.mPhotoY + drawHeight > ((float) height)) {
                    this.mPhotoY = ((float) height) - drawHeight;
                    adjusted = true;
                }
                if (drawHeight < ((float) height) && this.mPhotoY < ImageSystem.ROTATE_NONE) {
                    this.mPhotoY = ImageSystem.ROTATE_NONE;
                    adjusted = true;
                }
            } else {
                if (this.mPhotoY + drawHeight < ((float) height)) {
                    this.mPhotoY = ((float) height) - drawHeight;
                    adjusted = true;
                }
                if (this.mPhotoY > ImageSystem.ROTATE_NONE) {
                    this.mPhotoY = ImageSystem.ROTATE_NONE;
                    adjusted = true;
                }
            }
            if (adjusted) {
                this.mVelocityX = ImageSystem.ROTATE_NONE;
                this.mVelocityY = ImageSystem.ROTATE_NONE;
            }
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        this.mGestureDetector.onTouchEvent(event);
        return true;
    }

    public boolean onDown(MotionEvent e) {
        this.mVelocityX = ImageSystem.ROTATE_NONE;
        this.mVelocityY = ImageSystem.ROTATE_NONE;
        return false;
    }

    public void onShowPress(MotionEvent e) {
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        this.mPhotoX -= distanceX;
        this.mPhotoY -= distanceY;
        adjustImagePosition();
        redraw();
        return true;
    }

    public void onLongPress(MotionEvent e) {
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        this.mVelocityX = (((float) this.mTimeTouchRedraw) * velocityX) / 1000.0f;
        this.mVelocityY = (((float) this.mTimeTouchRedraw) * velocityY) / 1000.0f;
        this.mTimerHandler.start(0, this.mTimeTouchRedraw);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onTimer(int what) {
        if (this.mVelocityX != ImageSystem.ROTATE_NONE || this.mVelocityY != ImageSystem.ROTATE_NONE) {
            this.mPhotoX += this.mVelocityX;
            this.mPhotoY += this.mVelocityY;
            this.mVelocityX *= ((float) this.mDecreaseVelocity) / 100.0f;
            if (-1.0f < this.mVelocityX && this.mVelocityX < 1.0f) {
                this.mVelocityX = ImageSystem.ROTATE_NONE;
            }
            this.mVelocityY *= ((float) this.mDecreaseVelocity) / 100.0f;
            if (-1.0f < this.mVelocityY && this.mVelocityY < 1.0f) {
                this.mVelocityY = ImageSystem.ROTATE_NONE;
            }
            adjustImagePosition();
            redraw();
            if (this.mVelocityX != ImageSystem.ROTATE_NONE || this.mVelocityY != ImageSystem.ROTATE_NONE) {
                this.mTimerHandler.start(0, this.mTimeTouchRedraw);
            }
        }
    }

    private class TimerHandler extends Handler {
        private TimerHandler() {
        }

        /* synthetic */ TimerHandler(ShowImageView showImageView, TimerHandler timerHandler) {
            this();
        }

        public void start(int what, int time) {
            sendMessageDelayed(obtainMessage(what), (long) time);
        }

        public void handleMessage(Message message) {
            ShowImageView.this.onTimer(message.what);
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (this.mImageWidth == 0 || this.mImageHeight == 0) {
            this.mImageWidth = (int) (((float) width) / this.mPowerDefault);
            this.mImageHeight = (int) (((float) height) / this.mPowerDefault);
            setPower(ImageSystem.ROTATE_NONE);
        }
        redraw();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    private void initSurface() {
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        holder.setType(0);
    }

    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser && this.mPowerBar != null && seekBar.getId() == this.mPowerBar.getId()) {
            onProgressChanged_RangeBar(progress);
        }
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    private void onProgressChanged_RangeBar(int progress) {
        float max = (float) this.mPowerBar.getMax();
        setPower(((this.mPowerMax - this.mPowerMin) * ((max - ((float) progress)) / max)) + this.mPowerMin);
    }
}
