package com.psc.fukumoto.lib;

import android.os.Handler;
import android.os.Message;

public class TimerHandler extends Handler {
    private boolean mMoveTimer = false;
    private OnTimerListener mOnTimerListener = null;
    private boolean mRepeat;
    private int mTime;

    public interface OnTimerListener {
        void onTimer();
    }

    public void setOnTimerListener(OnTimerListener listener) {
        this.mOnTimerListener = listener;
    }

    public TimerHandler(int time, boolean repeat) {
        this.mTime = time;
        this.mRepeat = repeat;
    }

    public TimerHandler(OnTimerListener listener, int time, boolean repeat) {
        this.mOnTimerListener = listener;
        this.mTime = time;
        this.mRepeat = repeat;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        this.mOnTimerListener = null;
    }

    public void setTime(int time) {
        this.mTime = time;
    }

    public int getTime() {
        return this.mTime;
    }

    public void start() {
        sendMessageDelayed(obtainMessage(0), (long) this.mTime);
        this.mMoveTimer = true;
    }

    public void stop() {
        this.mMoveTimer = false;
    }

    public void handleMessage(Message message) {
        if (this.mMoveTimer) {
            if (this.mRepeat) {
                start();
            } else {
                this.mMoveTimer = false;
            }
            this.mOnTimerListener.onTimer();
        }
    }
}
