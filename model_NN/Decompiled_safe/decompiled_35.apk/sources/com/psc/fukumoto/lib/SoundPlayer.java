package com.psc.fukumoto.lib;

import android.media.AudioFormat;
import android.media.AudioTrack;
import com.psc.fukumoto.MindMemo.MultiTouchEvent;
import java.lang.reflect.Field;

public class SoundPlayer implements AudioTrack.OnPlaybackPositionUpdateListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$psc$fukumoto$lib$SoundPlayer$SOUND_SCALE = null;
    private static /* synthetic */ int[] $SWITCH_TABLE$com$psc$fukumoto$lib$SoundPlayer$WAVE_TYPE = null;
    private static final int AUDIO_FORMAT = 2;
    private static final int CHANNEL_CONFIG = CHANNEL_OUT_MONO();
    public static final int SCALE_NUM = 12;
    private static final int SIZE_OF_SHORT = 2;
    public static final double SOUND_FREQ_4A = 880.0d;
    public static final double SOUND_FREQ_4AS = 932.33d;
    public static final double SOUND_FREQ_4B = 987.77d;
    public static final double SOUND_FREQ_4C = 523.25d;
    public static final double SOUND_FREQ_4CS = 553.37d;
    public static final double SOUND_FREQ_4D = 587.33d;
    public static final double SOUND_FREQ_4DS = 622.25d;
    public static final double SOUND_FREQ_4E = 659.26d;
    public static final double SOUND_FREQ_4F = 698.46d;
    public static final double SOUND_FREQ_4FS = 739.99d;
    public static final double SOUND_FREQ_4G = 783.99d;
    public static final double SOUND_FREQ_4GS = 830.61d;
    private static final int STREAM_TYPE = 3;
    private AudioTrack mAudioTrack;
    private int mBufferSize;
    /* access modifiers changed from: private */
    public double mFreq;
    private boolean mIsStatic;
    /* access modifiers changed from: private */
    public int mPlayTime;
    /* access modifiers changed from: private */
    public int mRate;
    /* access modifiers changed from: private */
    public WAVE_TYPE mType;

    static /* synthetic */ int[] $SWITCH_TABLE$com$psc$fukumoto$lib$SoundPlayer$SOUND_SCALE() {
        int[] iArr = $SWITCH_TABLE$com$psc$fukumoto$lib$SoundPlayer$SOUND_SCALE;
        if (iArr == null) {
            iArr = new int[SOUND_SCALE.values().length];
            try {
                iArr[SOUND_SCALE.A.ordinal()] = 11;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[SOUND_SCALE.AS.ordinal()] = 12;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[SOUND_SCALE.B.ordinal()] = 13;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[SOUND_SCALE.C.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[SOUND_SCALE.CS.ordinal()] = STREAM_TYPE;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[SOUND_SCALE.D.ordinal()] = 4;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[SOUND_SCALE.DS.ordinal()] = 5;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[SOUND_SCALE.E.ordinal()] = 6;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[SOUND_SCALE.F.ordinal()] = 7;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[SOUND_SCALE.FS.ordinal()] = 8;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[SOUND_SCALE.G.ordinal()] = 9;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[SOUND_SCALE.GS.ordinal()] = 10;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[SOUND_SCALE.NONE.ordinal()] = 1;
            } catch (NoSuchFieldError e13) {
            }
            $SWITCH_TABLE$com$psc$fukumoto$lib$SoundPlayer$SOUND_SCALE = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$psc$fukumoto$lib$SoundPlayer$WAVE_TYPE() {
        int[] iArr = $SWITCH_TABLE$com$psc$fukumoto$lib$SoundPlayer$WAVE_TYPE;
        if (iArr == null) {
            iArr = new int[WAVE_TYPE.values().length];
            try {
                iArr[WAVE_TYPE.WAVE_TYPE_SINE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[WAVE_TYPE.WAVE_TYPE_SQUARE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$com$psc$fukumoto$lib$SoundPlayer$WAVE_TYPE = iArr;
        }
        return iArr;
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class SOUND_SCALE extends Enum<SOUND_SCALE> {
        public static final SOUND_SCALE A = new SOUND_SCALE("A", 10);
        public static final SOUND_SCALE AS = new SOUND_SCALE("AS", 11);
        public static final SOUND_SCALE B = new SOUND_SCALE("B", 12);
        public static final SOUND_SCALE C = new SOUND_SCALE("C", 1);
        public static final SOUND_SCALE CS = new SOUND_SCALE("CS", 2);
        public static final SOUND_SCALE D = new SOUND_SCALE("D", SoundPlayer.STREAM_TYPE);
        public static final SOUND_SCALE DS = new SOUND_SCALE("DS", 4);
        public static final SOUND_SCALE E = new SOUND_SCALE("E", 5);
        private static final /* synthetic */ SOUND_SCALE[] ENUM$VALUES;
        public static final SOUND_SCALE F = new SOUND_SCALE("F", 6);
        public static final SOUND_SCALE FS = new SOUND_SCALE("FS", 7);
        public static final SOUND_SCALE G = new SOUND_SCALE("G", 8);
        public static final SOUND_SCALE GS = new SOUND_SCALE("GS", 9);
        public static final SOUND_SCALE NONE = new SOUND_SCALE("NONE", SoundPlayer.CHANNEL_CONFIG);

        public static SOUND_SCALE valueOf(String str) {
            return (SOUND_SCALE) Enum.valueOf(SOUND_SCALE.class, str);
        }

        public static SOUND_SCALE[] values() {
            SOUND_SCALE[] sound_scaleArr = ENUM$VALUES;
            int length = sound_scaleArr.length;
            SOUND_SCALE[] sound_scaleArr2 = new SOUND_SCALE[length];
            System.arraycopy(sound_scaleArr, SoundPlayer.CHANNEL_CONFIG, sound_scaleArr2, SoundPlayer.CHANNEL_CONFIG, length);
            return sound_scaleArr2;
        }

        private SOUND_SCALE(String str, int i) {
        }

        static {
            SOUND_SCALE[] sound_scaleArr = new SOUND_SCALE[13];
            sound_scaleArr[SoundPlayer.CHANNEL_CONFIG] = NONE;
            sound_scaleArr[1] = C;
            sound_scaleArr[2] = CS;
            sound_scaleArr[SoundPlayer.STREAM_TYPE] = D;
            sound_scaleArr[4] = DS;
            sound_scaleArr[5] = E;
            sound_scaleArr[6] = F;
            sound_scaleArr[7] = FS;
            sound_scaleArr[8] = G;
            sound_scaleArr[9] = GS;
            sound_scaleArr[10] = A;
            sound_scaleArr[11] = AS;
            sound_scaleArr[12] = B;
            ENUM$VALUES = sound_scaleArr;
        }
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class WAVE_TYPE extends Enum<WAVE_TYPE> {
        private static final /* synthetic */ WAVE_TYPE[] ENUM$VALUES;
        public static final WAVE_TYPE WAVE_TYPE_SINE = new WAVE_TYPE("WAVE_TYPE_SINE", SoundPlayer.CHANNEL_CONFIG);
        public static final WAVE_TYPE WAVE_TYPE_SQUARE = new WAVE_TYPE("WAVE_TYPE_SQUARE", 1);

        public static WAVE_TYPE valueOf(String str) {
            return (WAVE_TYPE) Enum.valueOf(WAVE_TYPE.class, str);
        }

        public static WAVE_TYPE[] values() {
            WAVE_TYPE[] wave_typeArr = ENUM$VALUES;
            int length = wave_typeArr.length;
            WAVE_TYPE[] wave_typeArr2 = new WAVE_TYPE[length];
            System.arraycopy(wave_typeArr, SoundPlayer.CHANNEL_CONFIG, wave_typeArr2, SoundPlayer.CHANNEL_CONFIG, length);
            return wave_typeArr2;
        }

        private WAVE_TYPE(String str, int i) {
        }

        static {
            WAVE_TYPE[] wave_typeArr = new WAVE_TYPE[2];
            wave_typeArr[SoundPlayer.CHANNEL_CONFIG] = WAVE_TYPE_SINE;
            wave_typeArr[1] = WAVE_TYPE_SQUARE;
            ENUM$VALUES = wave_typeArr;
        }
    }

    public SoundPlayer(int playTime, WAVE_TYPE type, double freq) {
        this.mAudioTrack = null;
        this.mBufferSize = CHANNEL_CONFIG;
        this.mIsStatic = false;
        this.mRate = AudioTrack.getNativeOutputSampleRate(STREAM_TYPE);
        this.mPlayTime = playTime;
        this.mType = type;
        this.mFreq = freq;
        this.mAudioTrack = createStaticAudioTrack((this.mRate * this.mPlayTime) / 1000);
    }

    public SoundPlayer(int rate, short[] data) {
        this.mAudioTrack = null;
        this.mBufferSize = CHANNEL_CONFIG;
        this.mIsStatic = false;
        if (data != null) {
            this.mRate = rate;
            this.mAudioTrack = createStaticAudioTrack(data.length);
            setSoundData(data);
        }
    }

    public SoundPlayer(int rate, int bufferSize) {
        this.mAudioTrack = null;
        this.mBufferSize = CHANNEL_CONFIG;
        this.mIsStatic = false;
        this.mRate = rate;
        this.mAudioTrack = createStaticAudioTrack(bufferSize);
    }

    public SoundPlayer(int rate) {
        this.mAudioTrack = null;
        this.mBufferSize = CHANNEL_CONFIG;
        this.mIsStatic = false;
        this.mRate = rate;
        this.mAudioTrack = createStreamAudioTrack();
    }

    public void setSoundData(short[] data) {
        if (this.mAudioTrack != null && data != null) {
            stopSound();
            this.mAudioTrack.write(data, (int) CHANNEL_CONFIG, data.length);
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        finishSound();
        try {
            super.finalize();
        } catch (Throwable ex) {
            ex.fillInStackTrace();
        }
    }

    public void onResume() {
        if (this.mAudioTrack == null) {
            if (this.mBufferSize > 0) {
                this.mAudioTrack = createStaticAudioTrack(this.mBufferSize);
            } else {
                this.mAudioTrack = createStreamAudioTrack();
            }
        }
        if (this.mAudioTrack != null && this.mFreq > 0.0d) {
            new Thread(new RunSetSoundData(this, null)).start();
        }
    }

    public void onPause() {
        finishSound();
    }

    private AudioTrack createStaticAudioTrack(int bufferSize) {
        Exception ex;
        AudioTrack autdioTrack;
        this.mBufferSize = bufferSize;
        int minBufferSize = calcMinBufferSize(bufferSize);
        if (minBufferSize == 0) {
            return null;
        }
        try {
            autdioTrack = new AudioTrack(STREAM_TYPE, this.mRate, CHANNEL_CONFIG, 2, minBufferSize, CHANNEL_CONFIG);
            try {
                autdioTrack.setPlaybackPositionUpdateListener(this);
                this.mIsStatic = true;
            } catch (Exception e) {
                ex = e;
                ex.fillInStackTrace();
                return autdioTrack;
            }
        } catch (Exception e2) {
            ex = e2;
            autdioTrack = null;
            ex.fillInStackTrace();
            return autdioTrack;
        }
        return autdioTrack;
    }

    private AudioTrack createStreamAudioTrack() {
        Exception ex;
        AudioTrack autdioTrack;
        int minBufferSize = calcMinBufferSize(CHANNEL_CONFIG);
        if (minBufferSize == 0) {
            return null;
        }
        try {
            autdioTrack = new AudioTrack(STREAM_TYPE, this.mRate, CHANNEL_CONFIG, 2, minBufferSize, 1);
            try {
                this.mIsStatic = false;
            } catch (Exception e) {
                ex = e;
                ex.fillInStackTrace();
                return autdioTrack;
            }
        } catch (Exception e2) {
            ex = e2;
            autdioTrack = null;
            ex.fillInStackTrace();
            return autdioTrack;
        }
        return autdioTrack;
    }

    public int calcMinBufferSize(int bufferSize) {
        int minBufferSize = AudioTrack.getMinBufferSize(this.mRate, CHANNEL_CONFIG, 2);
        if (minBufferSize < 0) {
            return CHANNEL_CONFIG;
        }
        if (minBufferSize < bufferSize * 2) {
            minBufferSize = bufferSize * 2;
        }
        return minBufferSize;
    }

    public boolean playStream(short[] data, int startPos, int playSize) {
        if (this.mAudioTrack == null) {
            return CHANNEL_CONFIG;
        }
        if (this.mIsStatic) {
            return CHANNEL_CONFIG;
        }
        if (data == null) {
            return CHANNEL_CONFIG;
        }
        if (this.mAudioTrack.getPlayState() != STREAM_TYPE) {
            this.mAudioTrack.play();
        }
        this.mAudioTrack.write(data, startPos, playSize);
        return true;
    }

    public boolean playSound() {
        if (this.mAudioTrack == null) {
            return CHANNEL_CONFIG;
        }
        if (!this.mIsStatic) {
            return CHANNEL_CONFIG;
        }
        if (this.mAudioTrack.getState() != 1) {
            return true;
        }
        stopSound();
        this.mAudioTrack.reloadStaticData();
        this.mAudioTrack.setNotificationMarkerPosition(this.mBufferSize);
        this.mAudioTrack.play();
        return true;
    }

    public void onMarkerReached(AudioTrack track) {
        stopSound();
    }

    public void onPeriodicNotification(AudioTrack track) {
    }

    public boolean stopSound() {
        if (this.mAudioTrack == null) {
            return CHANNEL_CONFIG;
        }
        if (this.mAudioTrack.getState() != 1) {
            return CHANNEL_CONFIG;
        }
        if (this.mAudioTrack.getPlayState() == 1) {
            return true;
        }
        this.mAudioTrack.stop();
        return true;
    }

    private void finishSound() {
        if (this.mAudioTrack != null) {
            this.mAudioTrack.release();
            this.mAudioTrack = null;
        }
    }

    private class RunSetSoundData implements Runnable {
        private RunSetSoundData() {
        }

        /* synthetic */ RunSetSoundData(SoundPlayer soundPlayer, RunSetSoundData runSetSoundData) {
            this();
        }

        public void run() {
            SoundPlayer.this.createAndSetSoundData(SoundPlayer.this.mRate, SoundPlayer.this.mPlayTime, SoundPlayer.this.mType, SoundPlayer.this.mFreq);
        }
    }

    /* access modifiers changed from: private */
    public void createAndSetSoundData(int rate, int playTime, WAVE_TYPE type, double freq) {
        short[] data;
        if (this.mAudioTrack != null) {
            int bufferSize = (rate * playTime) / 1000;
            switch ($SWITCH_TABLE$com$psc$fukumoto$lib$SoundPlayer$WAVE_TYPE()[type.ordinal()]) {
                case 1:
                    data = createSineWave(rate, bufferSize, freq);
                    break;
                case 2:
                    data = createSquareWave(rate, bufferSize, freq);
                    break;
                default:
                    return;
            }
            setSoundData(data);
        }
    }

    public static double calcFrequency(SOUND_SCALE scale, int oct) {
        double freq;
        switch ($SWITCH_TABLE$com$psc$fukumoto$lib$SoundPlayer$SOUND_SCALE()[scale.ordinal()]) {
            case 2:
                freq = 523.25d;
                break;
            case STREAM_TYPE /*3*/:
                freq = 553.37d;
                break;
            case 4:
                freq = 587.33d;
                break;
            case MultiTouchEvent.ACTION_POINTER_DOWN:
                freq = 622.25d;
                break;
            case MultiTouchEvent.ACTION_POINTER_UP:
                freq = 659.26d;
                break;
            case 7:
                freq = 698.46d;
                break;
            case MultiTouchEvent.ACTION_POINTER_INDEX_SHIFT:
                freq = 739.99d;
                break;
            case 9:
                freq = 783.99d;
                break;
            case 10:
                freq = 830.61d;
                break;
            case 11:
            default:
                freq = 880.0d;
                break;
            case SCALE_NUM /*12*/:
                freq = 932.33d;
                break;
            case 13:
                freq = 987.77d;
                break;
        }
        while (oct > 4) {
            freq *= 2.0d;
            oct--;
        }
        while (oct < 4) {
            freq /= 2.0d;
            oct++;
        }
        return freq;
    }

    public static int scaleToIndex(SOUND_SCALE scale) {
        switch ($SWITCH_TABLE$com$psc$fukumoto$lib$SoundPlayer$SOUND_SCALE()[scale.ordinal()]) {
            case 2:
                return CHANNEL_CONFIG;
            case STREAM_TYPE /*3*/:
                return 1;
            case 4:
                return 2;
            case MultiTouchEvent.ACTION_POINTER_DOWN:
                return STREAM_TYPE;
            case MultiTouchEvent.ACTION_POINTER_UP:
                return 4;
            case 7:
                return 5;
            case MultiTouchEvent.ACTION_POINTER_INDEX_SHIFT:
                return 6;
            case 9:
                return 7;
            case 10:
                return 8;
            case 11:
                return 9;
            case SCALE_NUM /*12*/:
                return 10;
            case 13:
                return 11;
            default:
                return -1;
        }
    }

    /* JADX INFO: Multiple debug info for r9v2 double: [D('freq' double), D('dr' double)] */
    /* JADX INFO: Multiple debug info for r9v3 double: [D('dRadian' double), D('dr' double)] */
    public static short[] createSineWave(int rate, int bufferSize, double freq) {
        short[] audioData = new short[bufferSize];
        double dr = getDecimal(freq * (1.0d / ((double) rate))) * 6.283185307179586d;
        double radian = 0.0d;
        for (int index = CHANNEL_CONFIG; index < bufferSize; index++) {
            audioData[index] = (short) ((int) (Math.sin(radian) * 32767.0d));
            radian += dr;
        }
        return audioData;
    }

    public static short[] createSquareWave(int rate, int bufferSize, double freq) {
        short[] audioData = new short[bufferSize];
        double round = 0.0d;
        double dr = getDecimal((1.0d / ((double) rate)) * freq);
        for (int index = CHANNEL_CONFIG; index < bufferSize; index++) {
            if (round < 0.5d) {
                audioData[index] = Short.MAX_VALUE;
            } else {
                audioData[index] = -32767;
            }
            round = getDecimal(round + dr);
        }
        return audioData;
    }

    private static final double getDecimal(double value) {
        return value - Math.floor(value);
    }

    public static int CHANNEL_OUT_MONO() {
        int value = 1;
        try {
            Field field = AudioFormat.class.getField("CHANNEL_OUT_MONO");
            if (field == null) {
                return 1;
            }
            try {
                value = field.getInt(null);
            } catch (IllegalAccessException e) {
            }
            return value;
        } catch (NoSuchFieldException e2) {
            return 1;
        }
    }
}
