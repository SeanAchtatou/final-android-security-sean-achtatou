package com.psc.fukumoto.lib;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.widget.SeekBar;

public class PictureZoomBar extends SeekBar {
    private int mColorText = -57312;
    private String mMessageExpand;
    private String mMessageReduce;

    public PictureZoomBar(Context context, int idMessageExpand, int itMessageReduce) {
        super(context);
        this.mMessageExpand = context.getString(idMessageExpand);
        this.mMessageReduce = context.getString(itMessageReduce);
    }

    public void setTextColor(int colorText) {
        this.mColorText = colorText;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint();
        paint.setColor(this.mColorText);
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        canvas.drawText(this.mMessageExpand, ImageSystem.ROTATE_NONE, ((((float) getHeight()) - (fontMetrics.bottom - fontMetrics.top)) / 2.0f) - fontMetrics.ascent, paint);
        float textWidth = paint.measureText(this.mMessageReduce);
        float textHeight = fontMetrics.bottom - fontMetrics.top;
        canvas.drawText(this.mMessageReduce, ((float) getWidth()) - textWidth, ((((float) getHeight()) - textHeight) / 2.0f) - fontMetrics.ascent, paint);
    }
}
