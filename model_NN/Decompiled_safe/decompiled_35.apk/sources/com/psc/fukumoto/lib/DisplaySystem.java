package com.psc.fukumoto.lib;

import android.content.Context;
import android.hardware.SensorEvent;
import android.view.Display;
import android.view.WindowManager;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class DisplaySystem {
    private boolean mDisplayPortrait;

    public DisplaySystem(Context context) {
        Display display = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        int rotation = getRotation(display);
        switch (rotation < 0 ? display.getOrientation() : rotation) {
            case 1:
            case 3:
                if (width > height) {
                    this.mDisplayPortrait = true;
                    return;
                } else {
                    this.mDisplayPortrait = false;
                    return;
                }
            case 2:
            default:
                if (height > width) {
                    this.mDisplayPortrait = true;
                    return;
                } else {
                    this.mDisplayPortrait = false;
                    return;
                }
        }
    }

    public float getRotateX(SensorEvent event) {
        if (this.mDisplayPortrait) {
            return event.values[1];
        }
        return event.values[2];
    }

    public float getRotateY(SensorEvent event) {
        if (this.mDisplayPortrait) {
            return event.values[2];
        }
        return -event.values[1];
    }

    public static final int getRotation(Display display) {
        if (display == null) {
            return -1;
        }
        Method method = null;
        try {
            method = display.getClass().getMethod("getRotation", new Class[0]);
        } catch (NoSuchMethodException e) {
        }
        if (method == null) {
            return -1;
        }
        try {
            return ((Integer) method.invoke(display, new Object[0])).intValue();
        } catch (InvocationTargetException e2) {
            InvocationTargetException ex = e2;
            Throwable cause = ex.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException(ex);
            }
        } catch (IllegalAccessException e3) {
            return -1;
        }
    }
}
