package com.psc.fukumoto.lib;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.SeekBar;

public class MessageZoomBar extends SeekBar {
    private int mColorText = -57312;
    private String mMessage;
    private int mMinProgress = 0;

    public MessageZoomBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MessageZoomBar(Context context, int idMessage) {
        super(context);
        this.mMessage = context.getString(idMessage);
    }

    public void setTextColor(int colorText) {
        this.mColorText = colorText;
    }

    public void setMin(int min) {
        this.mMinProgress = min;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint();
        paint.setColor(this.mColorText);
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        int progress = getProgress() + this.mMinProgress;
        StringBuilder builder = new StringBuilder();
        if (this.mMessage != null) {
            builder.append(this.mMessage);
            builder.append("(");
            builder.append(Integer.toString(progress));
            builder.append(")");
        } else {
            builder.append(Integer.toString(progress));
        }
        String message = builder.toString();
        canvas.drawText(message, (((float) getWidth()) - paint.measureText(message)) / 2.0f, ((((float) getHeight()) - (fontMetrics.bottom - fontMetrics.top)) / 2.0f) - fontMetrics.ascent, paint);
    }
}
