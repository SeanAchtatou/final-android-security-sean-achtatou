package com.psc.fukumoto.lib;

import android.content.Context;
import java.io.File;
import java.lang.Thread;

public class MyUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
    private static final String FILE_NAME_REPORT = "Exception.txt";
    private String mFileName;

    public MyUncaughtExceptionHandler(Context context) {
        this.mFileName = String.valueOf(FileSystem.getSdcardFolderName(context)) + File.separator + FILE_NAME_REPORT;
    }

    public void uncaughtException(Thread th, Throwable e) {
        saveStackList(e.getStackTrace());
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void saveStackList(java.lang.StackTraceElement[] r9) {
        /*
            r8 = this;
            r4 = 0
            java.io.PrintWriter r5 = new java.io.PrintWriter     // Catch:{ Exception -> 0x0049 }
            java.lang.String r6 = r8.mFileName     // Catch:{ Exception -> 0x0049 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0049 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005e, all -> 0x005b }
            r0.<init>()     // Catch:{ Exception -> 0x005e, all -> 0x005b }
            r2 = 0
        L_0x000e:
            int r6 = r9.length     // Catch:{ Exception -> 0x005e, all -> 0x005b }
            if (r2 < r6) goto L_0x0018
            if (r5 == 0) goto L_0x0016
            r5.close()
        L_0x0016:
            r4 = r5
        L_0x0017:
            return
        L_0x0018:
            r3 = r9[r2]     // Catch:{ Exception -> 0x005e, all -> 0x005b }
            r6 = 0
            r0.setLength(r6)     // Catch:{ Exception -> 0x005e, all -> 0x005b }
            java.lang.String r6 = r3.getClassName()     // Catch:{ Exception -> 0x005e, all -> 0x005b }
            java.lang.StringBuilder r6 = r0.append(r6)     // Catch:{ Exception -> 0x005e, all -> 0x005b }
            java.lang.String r7 = "#"
            r6.append(r7)     // Catch:{ Exception -> 0x005e, all -> 0x005b }
            java.lang.String r6 = r3.getMethodName()     // Catch:{ Exception -> 0x005e, all -> 0x005b }
            java.lang.StringBuilder r6 = r0.append(r6)     // Catch:{ Exception -> 0x005e, all -> 0x005b }
            java.lang.String r7 = ":"
            r6.append(r7)     // Catch:{ Exception -> 0x005e, all -> 0x005b }
            int r6 = r3.getLineNumber()     // Catch:{ Exception -> 0x005e, all -> 0x005b }
            r0.append(r6)     // Catch:{ Exception -> 0x005e, all -> 0x005b }
            java.lang.String r6 = r0.toString()     // Catch:{ Exception -> 0x005e, all -> 0x005b }
            r5.println(r6)     // Catch:{ Exception -> 0x005e, all -> 0x005b }
            int r2 = r2 + 1
            goto L_0x000e
        L_0x0049:
            r6 = move-exception
            r1 = r6
        L_0x004b:
            r1.printStackTrace()     // Catch:{ all -> 0x0054 }
            if (r4 == 0) goto L_0x0017
            r4.close()
            goto L_0x0017
        L_0x0054:
            r6 = move-exception
        L_0x0055:
            if (r4 == 0) goto L_0x005a
            r4.close()
        L_0x005a:
            throw r6
        L_0x005b:
            r6 = move-exception
            r4 = r5
            goto L_0x0055
        L_0x005e:
            r6 = move-exception
            r1 = r6
            r4 = r5
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.psc.fukumoto.lib.MyUncaughtExceptionHandler.saveStackList(java.lang.StackTraceElement[]):void");
    }
}
