package com.psc.fukumoto.lib;

import android.graphics.Matrix;
import android.widget.ImageView;
import com.psc.fukumoto.lib.TimerHandler;

public class ImageZoomAction extends TimerHandler implements TimerHandler.OnTimerListener {
    private float mCenterX;
    private float mCenterY;
    private int mCount;
    private int mCountStart;
    private float mDScale;
    private ImageView mImageViewZoom = null;
    private OnFinishZoomListener mOnFinishZoomListener;
    private float mScale;
    private float mScaleEnd;
    private float mScaleStart;

    public interface OnFinishZoomListener {
        void onFinishZoom(ImageZoomAction imageZoomAction);
    }

    public ImageZoomAction(int time, OnFinishZoomListener onFinishZoomListener) {
        super(time, true);
        setOnTimerListener(this);
        this.mOnFinishZoomListener = onFinishZoomListener;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        this.mOnFinishZoomListener = null;
    }

    public void setZoom(ImageView imageView, float centerX, float centerY, float scaleStart, float scaleEnd, int period) {
        this.mImageViewZoom = imageView;
        this.mCountStart = period / getTime();
        this.mScaleEnd = scaleEnd;
        this.mScaleStart = scaleStart;
        this.mDScale = (this.mScaleEnd - this.mScaleStart) / ((float) this.mCountStart);
        this.mCenterX = centerX;
        this.mCenterY = centerY;
    }

    public void startZoom() {
        this.mScale = this.mScaleStart;
        this.mCount = this.mCountStart;
        if (this.mImageViewZoom != null) {
            zoomView(this.mScale);
            super.start();
        }
    }

    public void onTimer() {
        this.mCount--;
        if (this.mCount <= 0) {
            stop();
            this.mScale = this.mScaleEnd;
            if (this.mOnFinishZoomListener != null) {
                this.mOnFinishZoomListener.onFinishZoom(this);
            }
        } else {
            this.mScale += this.mDScale;
        }
        if (this.mImageViewZoom != null) {
            zoomView(this.mScale);
        }
    }

    private void zoomView(float scale) {
        if (this.mImageViewZoom != null) {
            if (scale == ImageSystem.ROTATE_NONE) {
                this.mImageViewZoom.setVisibility(4);
                return;
            }
            this.mImageViewZoom.setScaleType(ImageView.ScaleType.MATRIX);
            Matrix matrix = new Matrix();
            matrix.setScale(scale, scale);
            matrix.postTranslate(this.mCenterX * (1.0f - scale), this.mCenterY * (1.0f - scale));
            this.mImageViewZoom.setImageMatrix(matrix);
            this.mImageViewZoom.setVisibility(0);
        }
    }
}
