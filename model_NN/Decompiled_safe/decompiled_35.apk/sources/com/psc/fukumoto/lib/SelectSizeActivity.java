package com.psc.fukumoto.lib;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.psc.fukumoto.lib.SelectSizeView;

public class SelectSizeActivity extends Activity implements SelectSizeView.MoveToActivity {
    public static final String EXTRA_PREVIEW_HEIGHT = "previewheight";
    public static final String EXTRA_PREVIEW_WIDTH = "previewwidth";
    public static final String EXTRA_SIZE_LIST = "sizelist";
    private SelectSizeView mView = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(2048);
        getWindow().addFlags(1024);
        requestWindowFeature(1);
        this.mView = new SelectSizeView(this, this);
        setContentView(this.mView);
        Intent intent = getIntent();
        int previewWidth = intent.getIntExtra(EXTRA_PREVIEW_WIDTH, 0);
        int previewHeight = intent.getIntExtra(EXTRA_PREVIEW_HEIGHT, 0);
        this.mView.setSizeList((SizeList) intent.getSerializableExtra(EXTRA_SIZE_LIST), previewWidth, previewHeight);
    }

    public void moveToPrevious(int previewWidth, int previewHeight) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_PREVIEW_WIDTH, previewWidth);
        intent.putExtra(EXTRA_PREVIEW_HEIGHT, previewHeight);
        setResult(-1, intent);
        finish();
    }
}
