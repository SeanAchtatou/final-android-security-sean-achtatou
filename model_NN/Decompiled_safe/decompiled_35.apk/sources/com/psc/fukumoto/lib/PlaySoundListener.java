package com.psc.fukumoto.lib;

public interface PlaySoundListener {
    boolean playSound(int i);

    boolean playSound(int i, boolean z);
}
