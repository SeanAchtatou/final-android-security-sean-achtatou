package com.psc.fukumoto.lib;

import android.content.Context;
import android.os.Environment;
import java.io.File;
import java.util.Calendar;

public class FileSystem {
    public static final String FOLDER_NAME_SDCARD = Environment.getExternalStorageDirectory().getPath();

    public static String createDateFileName(String suffix) {
        Calendar calendar = Calendar.getInstance();
        return String.format("%04d%02d%02d%02d%02d%02d%s", Integer.valueOf(calendar.get(1)), Integer.valueOf(calendar.get(2) + 1), Integer.valueOf(calendar.get(5)), Integer.valueOf(calendar.get(10)), Integer.valueOf(calendar.get(12)), Integer.valueOf(calendar.get(13)), suffix);
    }

    public static String getSdcardFolderName(Context context) {
        return String.valueOf(FOLDER_NAME_SDCARD) + File.separator + context.getPackageName();
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0042 A[SYNTHETIC, Splitter:B:21:0x0042] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x004e A[SYNTHETIC, Splitter:B:27:0x004e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean writeTextFile(java.lang.String r8, java.lang.String r9, java.lang.String r10) {
        /*
            if (r9 != 0) goto L_0x0004
            r6 = 0
        L_0x0003:
            return r6
        L_0x0004:
            if (r8 == 0) goto L_0x0038
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = java.lang.String.valueOf(r8)
            r6.<init>(r7)
            java.lang.String r7 = java.io.File.separator
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r9)
            java.lang.String r4 = r6.toString()
        L_0x001d:
            r2 = 0
            r5 = 1
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x003a }
            r1.<init>(r4)     // Catch:{ Exception -> 0x003a }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x003a }
            r3.<init>(r1)     // Catch:{ Exception -> 0x003a }
            byte[] r6 = r10.getBytes()     // Catch:{ Exception -> 0x0060, all -> 0x005d }
            r3.write(r6)     // Catch:{ Exception -> 0x0060, all -> 0x005d }
            if (r3 == 0) goto L_0x005b
            r3.close()     // Catch:{ IOException -> 0x0057 }
            r2 = r3
        L_0x0036:
            r6 = r5
            goto L_0x0003
        L_0x0038:
            r4 = r9
            goto L_0x001d
        L_0x003a:
            r6 = move-exception
            r0 = r6
        L_0x003c:
            r0.fillInStackTrace()     // Catch:{ all -> 0x004b }
            r5 = 0
            if (r2 == 0) goto L_0x0036
            r2.close()     // Catch:{ IOException -> 0x0046 }
            goto L_0x0036
        L_0x0046:
            r0 = move-exception
            r0.fillInStackTrace()
            goto L_0x0036
        L_0x004b:
            r6 = move-exception
        L_0x004c:
            if (r2 == 0) goto L_0x0051
            r2.close()     // Catch:{ IOException -> 0x0052 }
        L_0x0051:
            throw r6
        L_0x0052:
            r0 = move-exception
            r0.fillInStackTrace()
            goto L_0x0051
        L_0x0057:
            r0 = move-exception
            r0.fillInStackTrace()
        L_0x005b:
            r2 = r3
            goto L_0x0036
        L_0x005d:
            r6 = move-exception
            r2 = r3
            goto L_0x004c
        L_0x0060:
            r6 = move-exception
            r0 = r6
            r2 = r3
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.psc.fukumoto.lib.FileSystem.writeTextFile(java.lang.String, java.lang.String, java.lang.String):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0055 A[SYNTHETIC, Splitter:B:24:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0061 A[SYNTHETIC, Splitter:B:30:0x0061] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String readTextFile(java.lang.String r13, java.lang.String r14) {
        /*
            if (r14 != 0) goto L_0x0004
            r11 = 0
        L_0x0003:
            return r11
        L_0x0004:
            if (r13 == 0) goto L_0x004a
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            java.lang.String r12 = java.lang.String.valueOf(r13)
            r11.<init>(r12)
            java.lang.String r12 = java.io.File.separator
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r14)
            java.lang.String r9 = r11.toString()
        L_0x001d:
            r0 = 0
            r7 = 0
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x004e }
            r4.<init>(r9)     // Catch:{ Exception -> 0x004e }
            long r5 = r4.length()     // Catch:{ Exception -> 0x004e }
            r11 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r11 = (r5 > r11 ? 1 : (r5 == r11 ? 0 : -1))
            if (r11 <= 0) goto L_0x004c
            r2 = 2147483647(0x7fffffff, float:NaN)
        L_0x0032:
            byte[] r10 = new byte[r2]     // Catch:{ Exception -> 0x004e }
            java.io.FileInputStream r8 = new java.io.FileInputStream     // Catch:{ Exception -> 0x004e }
            r8.<init>(r4)     // Catch:{ Exception -> 0x004e }
            r8.read(r10)     // Catch:{ Exception -> 0x0074, all -> 0x0071 }
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x0074, all -> 0x0071 }
            r1.<init>(r10)     // Catch:{ Exception -> 0x0074, all -> 0x0071 }
            if (r8 == 0) goto L_0x006e
            r8.close()     // Catch:{ IOException -> 0x006a }
            r7 = r8
            r0 = r1
        L_0x0048:
            r11 = r0
            goto L_0x0003
        L_0x004a:
            r9 = r14
            goto L_0x001d
        L_0x004c:
            int r2 = (int) r5
            goto L_0x0032
        L_0x004e:
            r11 = move-exception
            r3 = r11
        L_0x0050:
            r3.fillInStackTrace()     // Catch:{ all -> 0x005e }
            if (r7 == 0) goto L_0x0048
            r7.close()     // Catch:{ IOException -> 0x0059 }
            goto L_0x0048
        L_0x0059:
            r3 = move-exception
            r3.fillInStackTrace()
            goto L_0x0048
        L_0x005e:
            r11 = move-exception
        L_0x005f:
            if (r7 == 0) goto L_0x0064
            r7.close()     // Catch:{ IOException -> 0x0065 }
        L_0x0064:
            throw r11
        L_0x0065:
            r3 = move-exception
            r3.fillInStackTrace()
            goto L_0x0064
        L_0x006a:
            r3 = move-exception
            r3.fillInStackTrace()
        L_0x006e:
            r7 = r8
            r0 = r1
            goto L_0x0048
        L_0x0071:
            r11 = move-exception
            r7 = r8
            goto L_0x005f
        L_0x0074:
            r11 = move-exception
            r3 = r11
            r7 = r8
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: com.psc.fukumoto.lib.FileSystem.readTextFile(java.lang.String, java.lang.String):java.lang.String");
    }

    public static boolean deleteFile(String folderName, String fileName) {
        String fullName;
        if (fileName == null) {
            return false;
        }
        if (folderName != null) {
            fullName = String.valueOf(folderName) + File.separator + fileName;
        } else {
            fullName = fileName;
        }
        return deleteFile(fullName);
    }

    public static boolean deleteFile(String fullName) {
        try {
            File file = new File(fullName);
            if (!file.exists()) {
                return true;
            }
            if (!file.isFile()) {
                return false;
            }
            if (!file.delete()) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.fillInStackTrace();
            return false;
        }
    }

    public static boolean createFolder(String folderName, boolean empty) {
        try {
            File folder = new File(folderName);
            if (folder.exists()) {
                if (!folder.isDirectory()) {
                    return false;
                }
                if (!empty) {
                    return true;
                }
                if (!deleteFolder(folderName)) {
                    return false;
                }
            }
            if (!folder.mkdirs()) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.fillInStackTrace();
            return false;
        }
    }

    public static boolean moveFolder(String folderNameFrom, String folderNameTo) {
        try {
            File folderFrom = new File(folderNameFrom);
            if (!folderFrom.exists()) {
                return false;
            }
            if (!deleteFolder(folderNameTo)) {
                return false;
            }
            if (!folderFrom.renameTo(new File(folderNameTo))) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.fillInStackTrace();
            return false;
        }
    }

    public static boolean deleteFolder(String folderName) {
        try {
            File folder = new File(folderName);
            if (!folder.exists()) {
                return true;
            }
            if (folder.isDirectory()) {
                String[] fileList = folder.list();
                int fileNum = fileList.length;
                for (int index = 0; index < fileNum; index++) {
                    deleteFolder(String.valueOf(folderName) + File.separator + fileList[index]);
                }
            }
            if (!folder.delete()) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.fillInStackTrace();
            return false;
        }
    }
}
