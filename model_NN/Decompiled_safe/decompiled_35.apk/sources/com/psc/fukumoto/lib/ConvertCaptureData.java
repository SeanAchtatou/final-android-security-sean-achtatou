package com.psc.fukumoto.lib;

public class ConvertCaptureData implements Runnable {
    private int mDataHeight = 0;
    private byte[] mDataPhoto = null;
    private int mDataWidth = 0;
    private OnConvertListener mOnConvertListener = null;

    public interface OnConvertListener {
        void onFinishConvert(int[] iArr, int i, int i2);
    }

    public ConvertCaptureData(OnConvertListener listener) {
        this.mOnConvertListener = listener;
    }

    public synchronized boolean convertData(byte[] data, int dataWidth, int dataHeight) {
        boolean z;
        if (data == null) {
            z = false;
        } else {
            this.mDataPhoto = data;
            this.mDataWidth = dataWidth;
            this.mDataHeight = dataHeight;
            try {
                new Thread(this).start();
                z = true;
            } catch (Exception e) {
                e.fillInStackTrace();
                this.mDataPhoto = null;
                z = false;
            }
        }
        return z;
    }

    public synchronized void run() {
        int[] rgb;
        int imageWidth = (this.mDataWidth / 2) * 2;
        int imageHeight = (this.mDataHeight / 2) * 2;
        int[] iArr = null;
        try {
            rgb = PhotoSystem.decodeYUV420ToRGB(this.mDataPhoto, this.mDataWidth, this.mDataHeight, imageWidth, imageHeight);
        } catch (OutOfMemoryError e) {
            e.fillInStackTrace();
        }
        this.mDataPhoto = null;
        if (this.mOnConvertListener != null) {
            this.mOnConvertListener.onFinishConvert(rgb, imageWidth, imageHeight);
        }
        return;
    }
}
