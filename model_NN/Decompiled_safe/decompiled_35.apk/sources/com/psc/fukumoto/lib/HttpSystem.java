package com.psc.fukumoto.lib;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class HttpSystem {
    public static String doGet(String url) {
        try {
            HttpGet method = new HttpGet(url);
            DefaultHttpClient client = new DefaultHttpClient();
            method.setHeader("Connection", "Keep-Alive");
            HttpResponse response = client.execute(method);
            if (response.getStatusLine().getStatusCode() != 200) {
                return null;
            }
            return EntityUtils.toString(response.getEntity(), "UTF-8");
        } catch (Exception e) {
            return null;
        }
    }

    public static String doPost(String url, String params) {
        try {
            HttpPost method = new HttpPost(url);
            DefaultHttpClient client = new DefaultHttpClient();
            StringEntity paramEntity = new StringEntity(params);
            paramEntity.setChunked(false);
            paramEntity.setContentType("application/x-www-form-urlencoded");
            method.setEntity(paramEntity);
            HttpResponse response = client.execute(method);
            if (response.getStatusLine().getStatusCode() != 200) {
                return null;
            }
            return EntityUtils.toString(response.getEntity(), "UTF-8");
        } catch (Exception e) {
            return null;
        }
    }
}
