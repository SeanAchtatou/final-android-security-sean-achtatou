package com.psc.fukumoto.lib;

import android.hardware.Camera;
import android.view.SurfaceHolder;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class CameraSystem {
    public static final String IMAGE_EXTENSION = ".jpg";

    public static final Camera openCamera(SurfaceHolder holder) {
        Camera camera = null;
        try {
            camera = Camera.open();
            camera.setPreviewDisplay(holder);
            return camera;
        } catch (Exception e) {
            e.fillInStackTrace();
            if (camera == null) {
                return camera;
            }
            camera.release();
            return null;
        }
    }

    public static final void closeCamera(Camera camera) {
        if (camera != null) {
            camera.setOneShotPreviewCallback(null);
            camera.setPreviewCallback(null);
            camera.stopPreview();
            camera.release();
        }
    }

    public static final Camera.Size searchCameraSize(List<Camera.Size> sizeList, int width, int height) {
        int itemNum;
        if (sizeList == null || (itemNum = sizeList.size()) == 0) {
            return null;
        }
        Camera.Size sizeMax = null;
        Camera.Size sizeMin = sizeList.get(0);
        for (int index = 0; index < itemNum; index++) {
            Camera.Size size = sizeList.get(index);
            if (size.width == width && size.height == height) {
                return size;
            }
            if (size.width <= sizeMin.width && size.height <= sizeMin.height) {
                sizeMin = size;
            }
            if ((width <= 0 || size.width <= width) && (height <= 0 || size.height <= height)) {
                if (sizeMax == null) {
                    sizeMax = size;
                } else if (size.width >= sizeMax.width && size.height >= sizeMax.height) {
                    sizeMax = size;
                }
            }
        }
        if (sizeMax == null) {
            sizeMax = sizeMin;
        }
        return sizeMax;
    }

    public static final boolean startPreview(Camera camera) {
        if (camera == null) {
            return false;
        }
        camera.startPreview();
        return true;
    }

    public static final void stopPreview(Camera camera) {
        if (camera != null) {
            camera.stopPreview();
        }
    }

    public static final List<Camera.Size> getSupportedPreviewSizes(Camera.Parameters cameraParams) {
        if (cameraParams == null) {
            return null;
        }
        try {
            Method method = cameraParams.getClass().getMethod("getSupportedPreviewSizes", new Class[0]);
            if (method == null) {
                return null;
            }
            try {
                return (List) method.invoke(cameraParams, new Object[0]);
            } catch (InvocationTargetException e) {
                InvocationTargetException ex = e;
                Throwable cause = ex.getCause();
                if (cause instanceof RuntimeException) {
                    throw ((RuntimeException) cause);
                } else if (cause instanceof Error) {
                    throw ((Error) cause);
                } else {
                    throw new RuntimeException(ex);
                }
            } catch (IllegalAccessException e2) {
                return null;
            }
        } catch (NoSuchMethodException e3) {
            return null;
        }
    }

    public static final List<Camera.Size> getSupportedPictureSizes(Camera.Parameters cameraParams) {
        if (cameraParams == null) {
            return null;
        }
        Method method = null;
        try {
            method = cameraParams.getClass().getMethod("getSupportedPictureSizes", new Class[0]);
        } catch (NoSuchMethodException e) {
        }
        if (method == null) {
            return null;
        }
        try {
            return (List) method.invoke(cameraParams, new Object[0]);
        } catch (InvocationTargetException e2) {
            InvocationTargetException ex = e2;
            Throwable cause = ex.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException(ex);
            }
        } catch (IllegalAccessException e3) {
            return null;
        }
    }

    public static final boolean isZoomSupported(Camera.Parameters cameraParams) {
        if (cameraParams == null) {
            return false;
        }
        Method method = null;
        try {
            method = cameraParams.getClass().getMethod("isZoomSupported", new Class[0]);
        } catch (NoSuchMethodException e) {
        }
        if (method == null) {
            return false;
        }
        try {
            return ((Boolean) method.invoke(cameraParams, new Object[0])).booleanValue();
        } catch (InvocationTargetException e2) {
            InvocationTargetException ex = e2;
            Throwable cause = ex.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException(ex);
            }
        } catch (IllegalAccessException e3) {
            return false;
        }
    }

    public static final int getMaxZoom(Camera.Parameters cameraParams) {
        if (cameraParams == null) {
            return 0;
        }
        Method method = null;
        try {
            method = cameraParams.getClass().getMethod("getMaxZoom", new Class[0]);
        } catch (NoSuchMethodException e) {
        }
        if (method == null) {
            return 0;
        }
        try {
            return ((Integer) method.invoke(cameraParams, new Object[0])).intValue();
        } catch (InvocationTargetException e2) {
            InvocationTargetException ex = e2;
            Throwable cause = ex.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException(ex);
            }
        } catch (IllegalAccessException e3) {
            return 0;
        }
    }

    public static final List<Integer> getZoomRatios(Camera.Parameters cameraParams) {
        if (cameraParams == null) {
            return null;
        }
        Method method = null;
        try {
            method = cameraParams.getClass().getMethod("getZoomRatios", new Class[0]);
        } catch (NoSuchMethodException e) {
        }
        if (method == null) {
            return null;
        }
        try {
            return (List) method.invoke(cameraParams, new Object[0]);
        } catch (InvocationTargetException e2) {
            InvocationTargetException ex = e2;
            Throwable cause = ex.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException(ex);
            }
        } catch (IllegalAccessException e3) {
            return null;
        }
    }

    public static final int getZoom(Camera.Parameters cameraParams) {
        if (cameraParams == null) {
            return 0;
        }
        Method method = null;
        try {
            method = cameraParams.getClass().getMethod("getZoom", new Class[0]);
        } catch (NoSuchMethodException e) {
        }
        if (method == null) {
            return 0;
        }
        try {
            return ((Integer) method.invoke(cameraParams, new Object[0])).intValue();
        } catch (InvocationTargetException e2) {
            InvocationTargetException ex = e2;
            Throwable cause = ex.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException(ex);
            }
        } catch (IllegalAccessException e3) {
            return 0;
        }
    }

    public static final void setZoom(Camera.Parameters cameraParams, int value) {
        if (cameraParams != null) {
            Method method = null;
            try {
                method = cameraParams.getClass().getMethod("setZoom", Integer.TYPE);
            } catch (NoSuchMethodException e) {
            }
            if (method != null) {
                try {
                    method.invoke(cameraParams, Integer.valueOf(value));
                } catch (InvocationTargetException e2) {
                    InvocationTargetException ex = e2;
                    Throwable cause = ex.getCause();
                    if (cause instanceof RuntimeException) {
                        throw ((RuntimeException) cause);
                    } else if (cause instanceof Error) {
                        throw ((Error) cause);
                    } else {
                        throw new RuntimeException(ex);
                    }
                } catch (IllegalAccessException e3) {
                }
            }
        }
    }
}
