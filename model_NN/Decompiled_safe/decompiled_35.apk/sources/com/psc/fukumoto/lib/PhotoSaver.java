package com.psc.fukumoto.lib;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.text.format.DateFormat;

public class PhotoSaver implements Runnable {
    private Bitmap mBitmapPhoto = null;
    private OnFinishSaveListener mOnFinishSaveListener = null;
    private ContentResolver mResolver;

    public interface OnFinishSaveListener {
        void onFinishSave(String str);
    }

    public PhotoSaver(Context context, OnFinishSaveListener listener) {
        this.mResolver = context.getContentResolver();
        this.mOnFinishSaveListener = listener;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        this.mOnFinishSaveListener = null;
    }

    public void savePhoto(Bitmap bitmapPhoto) {
        this.mBitmapPhoto = bitmapPhoto;
        new Thread(this).start();
    }

    public void run() {
        String imageUrl = MediaStore.Images.Media.insertImage(this.mResolver, this.mBitmapPhoto, DateFormat.format("yyyy/MM/dd kk:mm:ss", System.currentTimeMillis()).toString(), "");
        if (this.mOnFinishSaveListener != null) {
            this.mOnFinishSaveListener.onFinishSave(imageUrl);
        }
    }
}
