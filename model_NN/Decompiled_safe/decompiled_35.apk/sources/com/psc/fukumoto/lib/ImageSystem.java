package com.psc.fukumoto.lib;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.widget.ImageButton;
import java.io.ByteArrayOutputStream;

public class ImageSystem {
    public static final float ROTATE_HALF = 180.0f;
    public static final float ROTATE_LEFT = -90.0f;
    public static final float ROTATE_NONE = 0.0f;
    public static final float ROTATE_RIGHT = 90.0f;

    public static final Bitmap loadFixImageFile(String folderName, String fileName, int width, int height) {
        String fullName;
        if (width <= 0 || height <= 0) {
            return null;
        }
        if (fileName == null || fileName.length() == 0) {
            return null;
        }
        if (folderName == null || folderName.length() == 0) {
            fullName = fileName;
        } else {
            fullName = String.valueOf(folderName) + "/" + fileName;
        }
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(fullName, bitmapOptions);
        if (!(width == 0 || height == 0)) {
            int scale = Math.max(((bitmapOptions.outWidth + width) - 1) / width, ((bitmapOptions.outHeight + height) - 1) / height);
            int base = 1;
            while (true) {
                if (base >= scale) {
                    break;
                } else if (base * 2 > scale) {
                    scale = base * 2;
                    break;
                } else {
                    base *= 2;
                }
            }
            bitmapOptions.inSampleSize = scale;
        }
        Bitmap bitmapImage = null;
        try {
            bitmapOptions.inJustDecodeBounds = false;
            bitmapOptions.inPurgeable = true;
            bitmapImage = BitmapFactory.decodeFile(fullName, bitmapOptions);
        } catch (OutOfMemoryError e) {
            e.fillInStackTrace();
        }
        return bitmapImage;
    }

    public static final Bitmap loadImageFile(String folderName, String fileName) {
        String fullName;
        if (fileName == null || fileName.length() == 0) {
            return null;
        }
        if (folderName == null || folderName.length() == 0) {
            fullName = fileName;
        } else {
            fullName = String.valueOf(folderName) + "/" + fileName;
        }
        Bitmap bitmapImage = null;
        try {
            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmapOptions.inJustDecodeBounds = false;
            bitmapOptions.inPurgeable = true;
            bitmapImage = BitmapFactory.decodeFile(fullName, bitmapOptions);
        } catch (OutOfMemoryError e) {
            e.fillInStackTrace();
        }
        return bitmapImage;
    }

    public static final boolean saveImageFile_Jpeg(String folderName, String fileName, Bitmap bitmap, int compress) {
        if (fileName == null) {
            return false;
        }
        if (bitmap == null) {
            return false;
        }
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, compress, outputStream);
            return saveBinaryData(folderName, fileName, outputStream.toByteArray());
        } catch (OutOfMemoryError e) {
            e.fillInStackTrace();
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x0069 A[SYNTHETIC, Splitter:B:47:0x0069] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final android.net.Uri saveImageFile_Png(java.lang.String r8, java.lang.String r9, android.graphics.Bitmap r10) {
        /*
            r7 = 0
            if (r9 != 0) goto L_0x0005
            r5 = r7
        L_0x0004:
            return r5
        L_0x0005:
            if (r10 != 0) goto L_0x0009
            r5 = r7
            goto L_0x0004
        L_0x0009:
            r2 = 0
            r4 = 0
            r5 = 0
            boolean r5 = com.psc.fukumoto.lib.FileSystem.createFolder(r8, r5)     // Catch:{ IOException -> 0x0055 }
            if (r5 != 0) goto L_0x001e
            if (r2 == 0) goto L_0x0017
            r2.close()     // Catch:{ Exception -> 0x0019 }
        L_0x0017:
            r5 = r7
            goto L_0x0004
        L_0x0019:
            r0 = move-exception
            r0.fillInStackTrace()
            goto L_0x0017
        L_0x001e:
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x0055 }
            r1.<init>(r8, r9)     // Catch:{ IOException -> 0x0055 }
            boolean r5 = r1.exists()     // Catch:{ IOException -> 0x0055 }
            if (r5 == 0) goto L_0x002c
            r1.delete()     // Catch:{ IOException -> 0x0055 }
        L_0x002c:
            boolean r5 = r1.createNewFile()     // Catch:{ IOException -> 0x0055 }
            if (r5 != 0) goto L_0x003e
            if (r2 == 0) goto L_0x0037
            r2.close()     // Catch:{ Exception -> 0x0039 }
        L_0x0037:
            r5 = r7
            goto L_0x0004
        L_0x0039:
            r0 = move-exception
            r0.fillInStackTrace()
            goto L_0x0037
        L_0x003e:
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0055 }
            r3.<init>(r1)     // Catch:{ IOException -> 0x0055 }
            android.graphics.Bitmap$CompressFormat r5 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ IOException -> 0x007a, all -> 0x0077 }
            r6 = 100
            r10.compress(r5, r6, r3)     // Catch:{ IOException -> 0x007a, all -> 0x0077 }
            android.net.Uri r4 = android.net.Uri.fromFile(r1)     // Catch:{ IOException -> 0x007a, all -> 0x0077 }
            if (r3 == 0) goto L_0x0053
            r3.close()     // Catch:{ Exception -> 0x0072 }
        L_0x0053:
            r5 = r4
            goto L_0x0004
        L_0x0055:
            r5 = move-exception
            r0 = r5
        L_0x0057:
            r0.fillInStackTrace()     // Catch:{ all -> 0x0066 }
            if (r2 == 0) goto L_0x005f
            r2.close()     // Catch:{ Exception -> 0x0061 }
        L_0x005f:
            r5 = r7
            goto L_0x0004
        L_0x0061:
            r0 = move-exception
            r0.fillInStackTrace()
            goto L_0x005f
        L_0x0066:
            r5 = move-exception
        L_0x0067:
            if (r2 == 0) goto L_0x006c
            r2.close()     // Catch:{ Exception -> 0x006d }
        L_0x006c:
            throw r5
        L_0x006d:
            r0 = move-exception
            r0.fillInStackTrace()
            goto L_0x006c
        L_0x0072:
            r0 = move-exception
            r0.fillInStackTrace()
            goto L_0x0053
        L_0x0077:
            r5 = move-exception
            r2 = r3
            goto L_0x0067
        L_0x007a:
            r5 = move-exception
            r0 = r5
            r2 = r3
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.psc.fukumoto.lib.ImageSystem.saveImageFile_Png(java.lang.String, java.lang.String, android.graphics.Bitmap):android.net.Uri");
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x0060 A[SYNTHETIC, Splitter:B:47:0x0060] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final boolean saveBinaryData(java.lang.String r6, java.lang.String r7, byte[] r8) {
        /*
            r5 = 0
            if (r7 != 0) goto L_0x0005
            r4 = r5
        L_0x0004:
            return r4
        L_0x0005:
            if (r8 != 0) goto L_0x0009
            r4 = r5
            goto L_0x0004
        L_0x0009:
            r2 = 0
            r4 = 0
            boolean r4 = com.psc.fukumoto.lib.FileSystem.createFolder(r6, r4)     // Catch:{ IOException -> 0x004c }
            if (r4 != 0) goto L_0x001d
            if (r2 == 0) goto L_0x0016
            r2.close()     // Catch:{ Exception -> 0x0018 }
        L_0x0016:
            r4 = r5
            goto L_0x0004
        L_0x0018:
            r0 = move-exception
            r0.fillInStackTrace()
            goto L_0x0016
        L_0x001d:
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x004c }
            r1.<init>(r6, r7)     // Catch:{ IOException -> 0x004c }
            boolean r4 = r1.exists()     // Catch:{ IOException -> 0x004c }
            if (r4 == 0) goto L_0x002b
            r1.delete()     // Catch:{ IOException -> 0x004c }
        L_0x002b:
            boolean r4 = r1.createNewFile()     // Catch:{ IOException -> 0x004c }
            if (r4 != 0) goto L_0x003d
            if (r2 == 0) goto L_0x0036
            r2.close()     // Catch:{ Exception -> 0x0038 }
        L_0x0036:
            r4 = r5
            goto L_0x0004
        L_0x0038:
            r0 = move-exception
            r0.fillInStackTrace()
            goto L_0x0036
        L_0x003d:
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x004c }
            r3.<init>(r1)     // Catch:{ IOException -> 0x004c }
            r3.write(r8)     // Catch:{ IOException -> 0x0071, all -> 0x006e }
            if (r3 == 0) goto L_0x004a
            r3.close()     // Catch:{ Exception -> 0x0069 }
        L_0x004a:
            r4 = 1
            goto L_0x0004
        L_0x004c:
            r4 = move-exception
            r0 = r4
        L_0x004e:
            r0.fillInStackTrace()     // Catch:{ all -> 0x005d }
            if (r2 == 0) goto L_0x0056
            r2.close()     // Catch:{ Exception -> 0x0058 }
        L_0x0056:
            r4 = r5
            goto L_0x0004
        L_0x0058:
            r0 = move-exception
            r0.fillInStackTrace()
            goto L_0x0056
        L_0x005d:
            r4 = move-exception
        L_0x005e:
            if (r2 == 0) goto L_0x0063
            r2.close()     // Catch:{ Exception -> 0x0064 }
        L_0x0063:
            throw r4
        L_0x0064:
            r0 = move-exception
            r0.fillInStackTrace()
            goto L_0x0063
        L_0x0069:
            r0 = move-exception
            r0.fillInStackTrace()
            goto L_0x004a
        L_0x006e:
            r4 = move-exception
            r2 = r3
            goto L_0x005e
        L_0x0071:
            r4 = move-exception
            r0 = r4
            r2 = r3
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.psc.fukumoto.lib.ImageSystem.saveBinaryData(java.lang.String, java.lang.String, byte[]):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static final Bitmap rotateBitmap(Bitmap bitmapImage, float degrees) {
        if (bitmapImage == null) {
            return null;
        }
        if (degrees == ROTATE_NONE) {
            return bitmapImage;
        }
        int bitmapWidth = bitmapImage.getWidth();
        int bitmapHeight = bitmapImage.getHeight();
        try {
            Matrix matrix = new Matrix();
            matrix.postRotate(degrees);
            bitmapImage = Bitmap.createBitmap(bitmapImage, 0, 0, bitmapWidth, bitmapHeight, matrix, true);
        } catch (OutOfMemoryError e) {
            e.fillInStackTrace();
        }
        return bitmapImage;
    }

    public static final Bitmap orientationBitmap(Bitmap bitmapImage, int orientation) {
        if (bitmapImage == null) {
            return null;
        }
        int bitmapWidth = bitmapImage.getWidth();
        int bitmapHeight = bitmapImage.getHeight();
        if (orientation == 1) {
            if (bitmapWidth > bitmapHeight) {
                bitmapImage = rotateBitmap(bitmapImage, 90.0f);
            }
        } else if (orientation == 2 && bitmapWidth < bitmapHeight) {
            bitmapImage = rotateBitmap(bitmapImage, -90.0f);
        }
        return bitmapImage;
    }

    public static final void setButtonImage(ImageButton imageButton, Resources resources, int resourceId, float degrees) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        imageButton.setImageBitmap(rotateBitmap(BitmapFactory.decodeResource(resources, resourceId, options), degrees));
    }
}
