package wall.bt.wp.P052;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.xl.util.BookMarkUtil;
import com.xl.util.Constant;
import com.xl.util.FileFolder;
import com.xl.util.FileUtil;
import com.xl.util.Folder;
import com.xl.util.GenUtil;
import com.xl.util.MultiDownloadNew;
import com.xl.util.WifiUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import wall.bt.wp.P052.Protocals;

public class MoreImg extends BaseImgActivity {
    private static final int DIALOG_ADDFAVORITE = 0;
    private static final int NumPerPage = 8;
    private static final String TAG = "SortByViews";
    private static boolean bPageInterrupt;
    private static String mPath;
    private Protocals.Album albumBookMark = null;
    /* access modifiers changed from: private */
    public boolean bIndownload = false;
    private ImageButton btnNext;
    private ImageButton btnPrev;
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    private MyAdapter mAdapter;
    private List<Protocals.Album> mDataAlbum = new ArrayList();
    /* access modifiers changed from: private */
    public List<String> mDataAlbumName = new ArrayList();
    /* access modifiers changed from: private */
    public List<Protocals.Album> mDataAlbumProc = new ArrayList();
    private List mDataImg = new ArrayList();
    /* access modifiers changed from: private */
    public List<String> mDataURLList = new ArrayList();
    private TextView mDownloadPercent;
    /* access modifiers changed from: private */
    public GridView mGridView;
    private int mPageIndex = 0;
    private TextView mPageInfo;
    private int mPageTotal;
    private ProgressBar mProgress;
    private RelativeLayout mProgressLayout;
    /* access modifiers changed from: private */
    public MyHandler myHandler = new MyHandler();
    /* access modifiers changed from: private */
    public String strRelativePath = "";
    /* access modifiers changed from: private */
    public String strWebImgRootURL = "";

    static /* synthetic */ int access$208(MoreImg x0) {
        int i = x0.mPageIndex;
        x0.mPageIndex = i + 1;
        return i;
    }

    static /* synthetic */ int access$210(MoreImg x0) {
        int i = x0.mPageIndex;
        x0.mPageIndex = i - 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.moreimg);
        initData();
        initView();
        if (!Boolean.valueOf(new WifiUtil(this).wifiConnect()).booleanValue()) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.txt_no_wifi_connection), 1).show();
        }
        this.strWebImgRootURL = getResources().getString(R.string.webimgrooturl);
        showAlbums(this.strWebImgRootURL);
    }

    /* access modifiers changed from: private */
    public void showAlbums(String strURL) {
        this.myHandler.showMyDialog();
        this.mDataURLList.add(strURL);
        this.mDataAlbum.clear();
        this.mDataAlbum = Protocals.getInstance().getCategories(strURL);
        this.mPageTotal = this.mDataAlbum.size() / NumPerPage;
        if (this.mPageTotal * NumPerPage != this.mDataAlbum.size()) {
            this.mPageTotal++;
        }
        this.mPageIndex = 0;
        downAlbum();
        updateView();
    }

    /* access modifiers changed from: private */
    public void downAlbum() {
        clearImg();
        this.bIndownload = false;
        this.mDataAlbumProc.clear();
        Boolean bConn = Boolean.valueOf(new WifiUtil(this).wifiConnect());
        this.mAdapter.notifyDataSetChanged();
        this.strRelativePath = getRelativePath(this.mDataURLList);
        GenUtil.systemPrintln("====================>getRelativePath = " + this.strRelativePath);
        int iTask = 0;
        int iStarts = this.mPageIndex * NumPerPage;
        int iEnds = (iStarts + NumPerPage) - 1;
        if (iStarts >= this.mDataAlbum.size()) {
            this.myHandler.dismissMyDialog();
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.txt_no_wifi_data), 1).show();
            return;
        }
        if (iEnds >= this.mDataAlbum.size()) {
            iEnds = this.mDataAlbum.size() - 1;
        }
        GenUtil.systemPrintln("iStarts = " + iStarts);
        GenUtil.systemPrintln("iEnds = " + iEnds);
        for (int iIndex = iStarts; iIndex <= iEnds; iIndex++) {
            Protocals.Album album = this.mDataAlbum.get(iIndex);
            if (album != null) {
                Object[] arrObjects = getObjectList(album);
                GenUtil.systemPrintln("---------- >bConn = " + bConn);
                if (FileUtil.fileExist((String) arrObjects[1])) {
                    addAdapterData(album.name);
                } else if (iTask > 2 || !bConn.booleanValue()) {
                    this.mDataAlbumProc.add(album);
                } else {
                    try {
                        Thread.currentThread();
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    this.bIndownload = true;
                    new QueryImgTask().execute(arrObjects);
                    iTask++;
                }
            }
        }
        this.myHandler.dismissMyDialog();
    }

    /* access modifiers changed from: private */
    public Object[] getObjectList(Protocals.Album album) {
        Object[] arrObjects = {this.strWebImgRootURL + this.strRelativePath + album.image, mPath + this.strRelativePath + album.image, String.valueOf(3), album.name, this.mDataURLList.get(this.mDataURLList.size() - 1)};
        GenUtil.systemPrintln("arrObjects[0] = " + arrObjects[0]);
        GenUtil.systemPrintln("arrObjects[1] = " + arrObjects[1]);
        GenUtil.systemPrintln("arrObjects[2] = " + arrObjects[2]);
        return arrObjects;
    }

    /* access modifiers changed from: private */
    public void clearImg() {
        this.mDataAlbumName.clear();
        for (int i = 0; i < this.mDataImg.size(); i++) {
            Bitmap bitmap = (Bitmap) this.mDataImg.get(i);
        }
        this.mDataImg.clear();
    }

    private String getRelativePath(List<String> mURLList) {
        if (mURLList.size() <= 1) {
            return "";
        }
        return mURLList.get(mURLList.size() - 1).substring(mURLList.get(0).length());
    }

    /* access modifiers changed from: private */
    public void updateView() {
        if (this.mPageIndex <= 0) {
            this.btnPrev.setVisibility(NumPerPage);
        } else {
            this.btnPrev.setVisibility(0);
        }
        if (this.mPageIndex < this.mPageTotal - 1) {
            this.btnNext.setVisibility(0);
        } else {
            this.btnNext.setVisibility(NumPerPage);
        }
        StringBuilder sb = new StringBuilder(getResources().getString(R.string.page_info_head));
        sb.append(this.mPageIndex + 1).append("/");
        sb.append(this.mPageTotal);
        this.mPageInfo.setText(sb.toString());
    }

    private void initData() {
        this.dialog = new ProgressDialog(this);
        if (Environment.getExternalStorageState().equals("mounted")) {
            mPath = Folder.MYCACHE;
        } else {
            mPath = Folder.MYCACHE1;
        }
    }

    private void initView() {
        this.mProgressLayout = (RelativeLayout) findViewById(R.id.progress_layout);
        this.mProgress = (ProgressBar) findViewById(R.id.progress);
        this.mDownloadPercent = (TextView) findViewById(R.id.download_percent);
        this.mGridView = (GridView) findViewById(R.id.album_list);
        this.mPageInfo = (TextView) findViewById(R.id.page_info);
        this.btnPrev = (ImageButton) findViewById(R.id.prev);
        this.btnNext = (ImageButton) findViewById(R.id.next);
        this.mAdapter = new MyAdapter();
        this.mGridView.setAdapter((ListAdapter) this.mAdapter);
        this.btnPrev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MoreImg.access$210(MoreImg.this);
                MoreImg.this.downAlbum();
                MoreImg.this.updateView();
            }
        });
        this.btnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MoreImg.access$208(MoreImg.this);
                MoreImg.this.downAlbum();
                MoreImg.this.updateView();
            }
        });
        this.mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                GenUtil.systemPrintln("arg2 = " + arg2);
                Protocals.Album album = MoreImg.this.getAlbum((String) MoreImg.this.mDataAlbumName.get(arg2));
                String strURl = (String) MoreImg.this.mDataURLList.get(MoreImg.this.mDataURLList.size() - 1);
                if (album != null) {
                    GenUtil.systemPrintln("album.category = " + album.category);
                    if (album.category.equals("M")) {
                        boolean unused = MoreImg.this.bIndownload = false;
                        GenUtil.systemPrintln("album.name = " + album.name);
                        String strURl2 = strURl + album.name + "/";
                        GenUtil.systemPrintln("strURl = " + strURl2);
                        MoreImg.this.clearImg();
                        ((MyAdapter) MoreImg.this.mGridView.getAdapter()).notifyDataSetChanged();
                        MoreImg.this.showAlbums(strURl2);
                    } else if (album.category.equals("S")) {
                        GenUtil.systemPrintln("album.name = " + album.name);
                        Toast.makeText(MoreImg.this.getApplicationContext(), MoreImg.this.getResources().getString(R.string.txt_opening), 1).show();
                        Intent intent = new Intent();
                        Bundle bundle = new Bundle();
                        bundle.putString("url", MoreImg.this.strWebImgRootURL + MoreImg.this.strRelativePath + album.name + "/");
                        MoreImg.this.setBookMarkAlbum(MoreImg.this.getAlbum((String) MoreImg.this.mDataAlbumName.get(arg2)));
                        bundle.putString("bookmark", MoreImg.this.getBookMarkString());
                        intent.putExtras(bundle);
                        intent.setClass(MoreImg.this, GridActivity.class);
                        MoreImg.this.startActivity(intent);
                    }
                }
            }
        });
        this.mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                GenUtil.systemPrintln("arg2 = " + arg2);
                Protocals.Album album = MoreImg.this.getAlbum((String) MoreImg.this.mDataAlbumName.get(arg2));
                if (!album.category.equals("S")) {
                    return true;
                }
                MoreImg.this.setBookMarkAlbum(album);
                MoreImg.this.showInfoBookMark(MoreImg.this.getResources().getString(R.string.txt_bookmark_title), MoreImg.this.getResources().getString(R.string.txt_addbookmark_body));
                return true;
            }
        });
        ((AdView) findViewById(R.id.ad)).loadAd(new AdRequest());
    }

    public void setBookMarkAlbum(Protocals.Album album) {
        this.albumBookMark = album;
    }

    public void setBookMark() {
        new BookMarkUtil(Environment.getExternalStorageState().equals("mounted") ? Folder.MYBOOKMARK : Folder.MYBOOKMARK1, "bookmark").add(getBookMarkString());
    }

    public String getBookMarkString() {
        return this.albumBookMark.name + "|||" + (this.strWebImgRootURL + this.strRelativePath + this.albumBookMark.name + "/") + "|||" + (mPath + this.strRelativePath + this.albumBookMark.image);
    }

    public void showInfoBookMark(String msgTitle, String msgString) {
        new AlertDialog.Builder(this).setTitle(msgTitle).setMessage(msgString).setPositiveButton(getResources().getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                MoreImg.this.setBookMark();
            }
        }).setNegativeButton(getResources().getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).show();
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public Protocals.Album getAlbum(String strAlbumName) {
        for (int i = 0; i < this.mDataAlbum.size(); i++) {
            if (this.mDataAlbum.get(i).name.equals(strAlbumName)) {
                return this.mDataAlbum.get(i);
            }
        }
        return null;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.mDataURLList.size() > 1) {
            GenUtil.systemPrintln("mDataURLList.size() = " + this.mDataURLList.size());
            this.strWebImgRootURL = this.mDataURLList.get(0);
            GenUtil.systemPrintln("strWebImgRootURL = " + this.strWebImgRootURL);
            this.mDataURLList.clear();
            showAlbums(this.strWebImgRootURL);
            this.bIndownload = false;
        } else {
            quitSystem();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    }

    /* access modifiers changed from: private */
    public int getSize() {
        if (this.mDataImg != null) {
            return this.mDataImg.size();
        }
        return 0;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    /* access modifiers changed from: private */
    public Bitmap getElement(int iIndex) {
        if (this.mDataImg == null || iIndex < 0 || iIndex >= this.mDataImg.size()) {
            return null;
        }
        return (Bitmap) this.mDataImg.get(iIndex);
    }

    class AlbumInfo {
        Protocals.Album mAlbum;
        Bitmap mThumbnail;
        int tag = -1;

        public AlbumInfo() {
        }
    }

    public final class ViewHolder {
        public ImageView iv_img_cat;
        public TextView tv_txt_cat;

        public ViewHolder() {
        }
    }

    class MyAdapter extends BaseAdapter {
        public MyAdapter() {
        }

        public int getCount() {
            return MoreImg.this.getSize();
        }

        public Object getItem(int iIndex) {
            return MoreImg.this.getElement(iIndex);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        /* Debug info: failed to restart local var, previous not found, register: 5 */
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(MoreImg.this).inflate((int) R.layout.grid_category, (ViewGroup) null);
                holder.iv_img_cat = (ImageView) convertView.findViewById(R.id.iv_img_cat);
                holder.tv_txt_cat = (TextView) convertView.findViewById(R.id.tv_txt_cat);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.iv_img_cat.setImageBitmap(MoreImg.this.getElement(position));
            holder.tv_txt_cat.setText((CharSequence) MoreImg.this.mDataAlbumName.get(position));
            return convertView;
        }
    }

    class MyHandler extends Handler {
        private static final int DISMISS_DIALOG = 7;
        private static final int DISMISS_PROGRESSBAR = 3;
        private static final String INFO = "info";
        private static final int NOTIFY_DATA = 1;
        private static final String REMOTE_VERSION = "remote_version";
        private static final int SET_PAGEINFO = 5;
        private static final int SHOW_DIALOG = 6;
        private static final int SHOW_DOWNLOAD_PERCENT = 4;
        private static final int SHOW_MSG = 8;
        private static final int SHOW_PROGRESSBAR = 2;

        MyHandler() {
        }

        /* access modifiers changed from: private */
        public void dismissMyDialog() {
            sendEmptyMessage(DISMISS_DIALOG);
        }

        private void notifyDataSetChanged() {
            Message localMessage = new Message();
            localMessage.what = 1;
            sendMessage(localMessage);
        }

        /* access modifiers changed from: private */
        public void showMyDialog() {
            sendEmptyMessage(SHOW_DIALOG);
        }

        public void handleMessage(Message paramMessage) {
            switch (paramMessage.what) {
                case 1:
                    if (MoreImg.this.dialog != null) {
                        MoreImg.this.dialog.dismiss();
                    }
                    String[] strTmpArray = ((String) paramMessage.obj).split("\\|\\|\\|");
                    if (strTmpArray.length >= SHOW_PROGRESSBAR && strTmpArray[1].equals(MoreImg.this.mDataURLList.get(MoreImg.this.mDataURLList.size() - 1))) {
                        MoreImg.this.addAdapterData(strTmpArray[0]);
                        if (MoreImg.this.mDataAlbumProc.size() > 0) {
                            new QueryImgTask().execute(MoreImg.this.getObjectList((Protocals.Album) MoreImg.this.mDataAlbumProc.get(0)));
                            MoreImg.this.mDataAlbumProc.remove(0);
                            break;
                        }
                    } else {
                        return;
                    }
                    break;
                case SHOW_DIALOG /*6*/:
                    if (MoreImg.this.dialog != null) {
                        MoreImg.this.dialog.setMessage("Loading...");
                        MoreImg.this.dialog.show();
                        break;
                    }
                    break;
                case DISMISS_DIALOG /*7*/:
                    if (MoreImg.this.dialog != null) {
                        MoreImg.this.dialog.dismiss();
                        break;
                    }
                    break;
                case SHOW_MSG /*8*/:
                    Bundle bundle = paramMessage.getData();
                    if (bundle != null) {
                        new AlertDialog.Builder(MoreImg.this).setTitle("Message:").setIcon((int) R.drawable.toast_warnning).setMessage(bundle.getString(INFO)).setPositiveButton("Never show", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();
                        break;
                    }
                    break;
            }
            super.handleMessage(paramMessage);
        }
    }

    private String GetImagePathByName(String strImgName) {
        for (int i = 0; i < this.mDataAlbum.size(); i++) {
            Protocals.Album album = this.mDataAlbum.get(i);
            if (album.name.equals(strImgName)) {
                return album.image;
            }
        }
        return "";
    }

    /* access modifiers changed from: private */
    public void addAdapterData(String strImgName) {
        this.mDataAlbumName.add(strImgName);
        String strImgPath = mPath + this.strRelativePath + GetImagePathByName(strImgName);
        GenUtil.systemPrintln("strImgPath = " + strImgPath);
        this.mDataImg.add(BitmapFactory.decodeFile(strImgPath));
        this.mAdapter.notifyDataSetChanged();
    }

    class QueryImgTask extends AsyncTask {
        QueryImgTask() {
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String strtxt) {
            MoreImg.this.myHandler.sendEmptyMessage(1);
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(int iPercent) {
            Message m = new Message();
            m.what = 6;
            m.obj = iPercent + "%";
            MoreImg.this.myHandler.sendMessageDelayed(m, 0);
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object[] arrParams) {
            String bookFolderTMP = (Environment.getExternalStorageState().equals("mounted") ? Folder.POPIMG : Folder.POPIMG1) + Constant.TMP;
            int startThread = Integer.parseInt((String) arrParams[2]);
            System.out.println((String) arrParams[0]);
            System.out.println((String) arrParams[1]);
            System.out.println(bookFolderTMP);
            final String strAlbumImage = ((String) arrParams[3]) + "|||" + ((String) arrParams[4]);
            final MultiDownloadNew multiDownload = new MultiDownloadNew(startThread, (String) arrParams[0], (String) arrParams[1], bookFolderTMP);
            multiDownload.start();
            new Thread(new Runnable() {
                public void run() {
                    long time = new Date().getTime();
                    long lastdata = 0;
                    while (multiDownload.getPercntInt() < 100 && MoreImg.this.bIndownload) {
                        try {
                            Thread.sleep(200);
                            long currTime = new Date().getTime();
                            long currData = multiDownload.getFileDownloadTotal();
                            if (currData != lastdata) {
                                long lasttime = currTime;
                                lastdata = currData;
                            }
                            Message m = new Message();
                            m.obj = multiDownload.getPercntInt() + "";
                            m.what = 2;
                            MoreImg.this.myHandler.sendMessage(m);
                        } catch (InterruptedException e) {
                            Message m1 = new Message();
                            m1.what = -1;
                            MoreImg.this.myHandler.sendMessage(m1);
                        }
                    }
                    Message m2 = new Message();
                    if (multiDownload.getPercntInt() != 100 || !MoreImg.this.bIndownload) {
                        m2.what = 6;
                    } else {
                        m2.obj = strAlbumImage;
                        m2.what = 1;
                    }
                    MoreImg.this.myHandler.sendMessage(m2);
                }
            }).start();
            return true;
        }
    }

    class ClearImgTask extends AsyncTask {
        ClearImgTask() {
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String strtxt) {
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(int iPercent) {
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object[] arrParams) {
            clearcache();
            Message m = new Message();
            m.what = 7;
            MoreImg.this.myHandler.sendMessage(m);
            return true;
        }

        private void clearcache() {
            List<String> strValidFoldArr = getValidFold();
            List<String> listFolder = new FileFolder(Environment.getExternalStorageState().equals("mounted") ? Folder.MYCACHE : Folder.MYCACHE1).getFoldList();
            for (int i = 0; i < listFolder.size(); i++) {
                String strKey = listFolder.get(i);
                if (!isValid(strKey, strValidFoldArr).booleanValue()) {
                    GenUtil.systemPrintln("Removed folder = " + strKey);
                    GenUtil.systemPrintln("Removed file = " + strKey.substring(0, strKey.lastIndexOf("/")) + ".jpg");
                    FileUtil.delFile(strKey.substring(0, strKey.lastIndexOf("/")) + ".jpg");
                    try {
                        FileUtil.delAllFile(strKey);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        private Boolean isValid(String strKey, List<String> listArr) {
            for (int i = 0; i < listArr.size(); i++) {
                if (listArr.get(i).equals(strKey)) {
                    return true;
                }
            }
            return false;
        }

        private List<String> getValidFold() {
            List<String> listBookMark = new BookMarkUtil(Environment.getExternalStorageState().equals("mounted") ? Folder.MYBOOKMARK : Folder.MYBOOKMARK1, "bookmark").getElementList();
            List<String> listValidFold = new ArrayList<>();
            GenUtil.systemPrintln("Folder.MYBOOKMARK = /sdcard/PopImg/mycollection/bookmark.xml");
            GenUtil.systemPrintln("listBookMark = " + listBookMark.size());
            for (int i = 0; i < listBookMark.size(); i++) {
                String[] strTmpArray = listBookMark.get(i).split("\\|\\|\\|");
                if (strTmpArray.length >= 3) {
                    String strValidTmp = (Environment.getExternalStorageState().equals("mounted") ? Folder.MYCACHE : Folder.MYCACHE1) + strTmpArray[1].substring(MoreImg.this.getResources().getString(R.string.webimgrooturl).length());
                    GenUtil.systemPrintln("strValidTmp = " + strValidTmp);
                    listValidFold.add(strValidTmp);
                }
            }
            return listValidFold;
        }
    }

    public boolean onCreateOptionsMenu(Menu paramMenu) {
        getMenuInflater().inflate(R.menu.more_menu, paramMenu);
        return super.onCreateOptionsMenu(paramMenu);
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
        super.onOptionsItemSelected(paramMenuItem);
        switch (paramMenuItem.getItemId()) {
            case R.id.clearcache:
                this.myHandler.showMyDialog();
                new ClearImgTask().execute((Object[]) null);
                return true;
            default:
                return true;
        }
    }

    public void quitSystem() {
        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.txt_quit_title)).setMessage(getResources().getString(R.string.txt_quit_body)).setIcon((int) R.drawable.icon).setPositiveButton(getResources().getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MoreImg.this.finish();
            }
        }).setNegativeButton(getResources().getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }
}
