package com.xl.util;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XmlUtil {
    public static final String root = "xmedia";
    private StringBuffer xml;

    public static final class User {
        public static final String password = "password";
        public static final String username = "name";
    }

    public XmlUtil() {
    }

    public XmlUtil(String username, String password) {
        setXml(initXml(username, password));
    }

    public boolean buildXml(String xmlPath, StringBuffer xml2) {
        try {
            File myFilePath = new File(xmlPath.toString());
            if (!myFilePath.exists()) {
                myFilePath.createNewFile();
            }
            PrintWriter myFile = new PrintWriter(myFilePath, "UTF-8");
            myFile.println(xml2.toString());
            myFile.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public StringBuffer initXml(String username, String password) {
        Class<User> cls = User.class;
        StringBuffer xml2 = new StringBuffer();
        xml2.append(beginformatElement(root));
        Class<User> cls2 = User.class;
        xml2.append(beginformatElement(cls.getSimpleName()));
        xml2.append(addElementText("name", username));
        xml2.append(addElementText(User.password, password));
        Class<User> cls3 = User.class;
        xml2.append(endformatElement(cls.getSimpleName()));
        xml2.append(endformatElement(root));
        return xml2;
    }

    public String addElementText(String rootName, String content) {
        StringBuffer xml2 = new StringBuffer("");
        xml2.append("<" + rootName + ">").append(content).append("</" + rootName + ">");
        return xml2.toString();
    }

    public StringBuffer removeAllElement(String content, String rootName) {
        StringBuffer xml2 = new StringBuffer(content);
        int beginIndex = xml2.toString().indexOf(beginformatElement(rootName));
        int lastIndex = xml2.toString().lastIndexOf(endformatElement(rootName)) + endformatElement(rootName).length();
        if (beginIndex > 0) {
            return xml2.delete(beginIndex, lastIndex);
        }
        return new StringBuffer("");
    }

    public StringBuffer removeElement(String content, String rootName, int index) {
        StringBuffer xml2 = new StringBuffer("");
        List<String> mylist = getElementList(content, rootName);
        if (mylist.size() > 0) {
            xml2 = removeAllElement(content, rootName);
            for (int i = 0; i < mylist.size(); i++) {
                if (i != index) {
                    xml2.append(addElementText(rootName, mylist.get(i)));
                }
            }
        }
        return xml2;
    }

    public StringBuffer updateElement(String content, String rootName, String newContent) {
        StringBuffer xml2 = new StringBuffer(content);
        int beginIndex = xml2.toString().indexOf(beginformatElement(rootName));
        int lastIndex = xml2.toString().lastIndexOf(endformatElement(rootName)) + endformatElement(rootName).length();
        if (beginIndex <= 0) {
            return xml2;
        }
        StringBuffer xml3 = xml2.delete(beginIndex, lastIndex);
        xml3.append(addElementText(rootName, newContent));
        return xml3;
    }

    private String beginformatElement(String rootName) {
        return "<" + rootName + ">";
    }

    private String endformatElement(String rootName) {
        return "</" + rootName + ">";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0055, code lost:
        r11 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0056, code lost:
        r0 = r1;
        r7 = r8;
        r5 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0063, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0064, code lost:
        r4 = r11;
        r0 = r1;
        r7 = r8;
        r5 = r6;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0055 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:8:0x001f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getFileContent(java.lang.String r13) throws java.io.IOException {
        /*
            r12 = this;
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            java.lang.String r11 = ""
            r10.<init>(r11)
            r5 = 0
            r7 = 0
            r0 = 0
            java.lang.String r9 = ""
            java.io.FileInputStream r6 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0040 }
            r6.<init>(r13)     // Catch:{ IOException -> 0x0040 }
            java.io.InputStreamReader r8 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x005a, all -> 0x004e }
            java.lang.String r11 = "UTF-8"
            r8.<init>(r6, r11)     // Catch:{ IOException -> 0x005a, all -> 0x004e }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x005e, all -> 0x0051 }
            r1.<init>(r8)     // Catch:{ IOException -> 0x005e, all -> 0x0051 }
            java.lang.String r2 = ""
        L_0x001f:
            java.lang.String r2 = r1.readLine()     // Catch:{ IOException -> 0x0029, all -> 0x0055 }
            if (r2 == 0) goto L_0x0032
            r10.append(r2)     // Catch:{ IOException -> 0x0029, all -> 0x0055 }
            goto L_0x001f
        L_0x0029:
            r11 = move-exception
            r3 = r11
            java.lang.String r11 = r3.toString()     // Catch:{ IOException -> 0x0063, all -> 0x0055 }
            r10.append(r11)     // Catch:{ IOException -> 0x0063, all -> 0x0055 }
        L_0x0032:
            java.lang.String r9 = r10.toString()     // Catch:{ IOException -> 0x0063, all -> 0x0055 }
            r1.close()
            r8.close()
            r6.close()
            return r9
        L_0x0040:
            r11 = move-exception
            r4 = r11
        L_0x0042:
            throw r4     // Catch:{ all -> 0x0043 }
        L_0x0043:
            r11 = move-exception
        L_0x0044:
            r0.close()
            r7.close()
            r5.close()
            throw r11
        L_0x004e:
            r11 = move-exception
            r5 = r6
            goto L_0x0044
        L_0x0051:
            r11 = move-exception
            r7 = r8
            r5 = r6
            goto L_0x0044
        L_0x0055:
            r11 = move-exception
            r0 = r1
            r7 = r8
            r5 = r6
            goto L_0x0044
        L_0x005a:
            r11 = move-exception
            r4 = r11
            r5 = r6
            goto L_0x0042
        L_0x005e:
            r11 = move-exception
            r4 = r11
            r7 = r8
            r5 = r6
            goto L_0x0042
        L_0x0063:
            r11 = move-exception
            r4 = r11
            r0 = r1
            r7 = r8
            r5 = r6
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xl.util.XmlUtil.getFileContent(java.lang.String):java.lang.String");
    }

    public List<String> getElementList(String content, String rootName) {
        List<String> list = new ArrayList<>();
        Matcher m = Pattern.compile("<" + rootName + ">(.*?)</" + rootName + ">").matcher(content);
        while (m.find()) {
            list.add(m.group(1).trim().replaceAll(" ", "%20"));
        }
        return list;
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public String getElementContent(String content, String rootName, int index) {
        List<String> list = new ArrayList<>();
        Matcher m = Pattern.compile("<" + rootName + ">(.*?)</" + rootName + ">").matcher(content);
        while (m.find()) {
            list.add(m.group(1).trim().replaceAll(" ", "%20"));
        }
        if (index < list.size()) {
            return (String) list.get(index);
        }
        return null;
    }

    public StringBuffer getXml() {
        return this.xml;
    }

    public void setXml(StringBuffer xml2) {
        this.xml = xml2;
    }
}
