package com.xl.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileMatch {
    public static String body = "<body>(.*?)</body>";
    public static String signiture = "<signiture>(.*?)</signiture>";
    public static String size = "<size>(.*?)</size>";
    public static String subject = "<subject>(.*?)</subject>";
    public static String time = "<time>(.*?)</time>";
    public static String title = "<title>(.*?)</title>";
    public static String type = "<type>(.*?)</type>";
    public static String url = "<url>(.*?)</url>";
    public static String ziped = "<ziped>(.*?)</ziped>";

    public static List matchResultList(String src, String patt) {
        List list = new ArrayList();
        Matcher m = Pattern.compile(patt).matcher(src);
        while (m.find()) {
            if (m.group(1) != null) {
                list.add(m.group(1).trim());
            }
        }
        return list;
    }

    public static FileBean matchResultforStr(String matchResult) {
        FileBean fileBean = null;
        if (matchResult != null && matchResult.length() > 1) {
            fileBean = new FileBean();
            List listTmp = matchResultList(matchResult, ziped);
            String strTmp = "0";
            if (listTmp.size() > 0) {
                strTmp = listTmp.get(0).toString();
            }
            fileBean.setZiped(strTmp);
            List listTmp2 = matchResultList(matchResult, title);
            String strTmp2 = "";
            if (listTmp2.size() > 0) {
                strTmp2 = listTmp2.get(0).toString();
            }
            fileBean.setName(strTmp2.replaceAll(" ", "%20"));
            List listTmp3 = matchResultList(matchResult, url);
            String strTmp3 = "";
            if (listTmp3.size() > 0) {
                strTmp3 = listTmp3.get(0).toString();
            }
            fileBean.setUrl(strTmp3);
            List listTmp4 = matchResultList(matchResult, subject);
            String strTmp4 = "";
            if (listTmp4.size() > 0) {
                strTmp4 = listTmp4.get(0).toString();
            }
            fileBean.setSubject(strTmp4);
            List listTmp5 = matchResultList(matchResult, signiture);
            String strTmp5 = "";
            if (listTmp5.size() > 0) {
                strTmp5 = listTmp5.get(0).toString();
            }
            fileBean.setSigniture(strTmp5);
            List listTmp6 = matchResultList(matchResult, time);
            String strTmp6 = "";
            if (listTmp6.size() > 0) {
                strTmp6 = listTmp6.get(0).toString();
            }
            fileBean.setTime(strTmp6);
            List listTmp7 = matchResultList(matchResult, size);
            String strTmp7 = "";
            if (listTmp7.size() > 0) {
                strTmp7 = listTmp7.get(0).toString();
            }
            fileBean.setSize(strTmp7);
            List listTmp8 = matchResultList(matchResult, type);
            String strTmp8 = "f";
            if (listTmp8.size() > 0) {
                strTmp8 = listTmp8.get(0).toString();
            }
            fileBean.setType(strTmp8);
        }
        return fileBean;
    }

    public static List<String> matchRootList(String content, String rootName) {
        List<String> list = new ArrayList<>();
        Matcher m = Pattern.compile("<" + rootName + ">(.*?)</" + rootName + ">").matcher(content);
        while (m.find()) {
            list.add(m.group(1).trim().replaceAll(" ", "%20"));
        }
        return list;
    }

    public static List<String> matchRootListNoencode(String content, String rootName) {
        List<String> list = new ArrayList<>();
        Matcher m = Pattern.compile("<" + rootName + ">(.*?)</" + rootName + ">").matcher(content);
        while (m.find()) {
            list.add(m.group(1).trim());
        }
        return list;
    }

    public static String matchRootContent(String content, String rootName, int index) {
        List<String> list = new ArrayList<>();
        Matcher m = Pattern.compile("<" + rootName + ">(.*?)</" + rootName + ">").matcher(content);
        while (m.find()) {
            list.add(m.group(1).trim().replaceAll(" ", "%20"));
        }
        if (index < list.size()) {
            return list.get(index);
        }
        return null;
    }

    public static String matchRootContentNoencode(String content, String rootName, int index) {
        List<String> list = new ArrayList<>();
        Matcher m = Pattern.compile("<" + rootName + ">(.*?)</" + rootName + ">").matcher(content);
        while (m.find()) {
            list.add(m.group(1).trim());
        }
        if (index < list.size()) {
            return list.get(index);
        }
        return null;
    }
}
