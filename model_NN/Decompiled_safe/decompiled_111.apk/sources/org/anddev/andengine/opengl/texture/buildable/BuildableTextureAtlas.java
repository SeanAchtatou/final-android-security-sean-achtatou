package org.anddev.andengine.opengl.texture.buildable;

import java.util.ArrayList;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.ITextureAtlas;
import org.anddev.andengine.opengl.texture.source.ITextureAtlasSource;
import org.anddev.andengine.util.Callback;

public class BuildableTextureAtlas<T extends ITextureAtlasSource, A extends ITextureAtlas<T>> implements ITextureAtlas<T> {
    private final A mTextureAtlas;
    private final ArrayList<TextureAtlasSourceWithWithLocationCallback<T>> mTextureAtlasSourcesToPlace = new ArrayList<>();

    public BuildableTextureAtlas(A pTextureAtlas) {
        this.mTextureAtlas = pTextureAtlas;
    }

    public int getWidth() {
        return this.mTextureAtlas.getWidth();
    }

    public int getHeight() {
        return this.mTextureAtlas.getHeight();
    }

    public int getHardwareTextureID() {
        return this.mTextureAtlas.getHardwareTextureID();
    }

    public boolean isLoadedToHardware() {
        return this.mTextureAtlas.isLoadedToHardware();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void setLoadedToHardware(boolean r2) {
        /*
            r1 = this;
            A r0 = r1.mTextureAtlas
            r0.setLoadedToHardware(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas.setLoadedToHardware(boolean):void");
    }

    public boolean isUpdateOnHardwareNeeded() {
        return this.mTextureAtlas.isUpdateOnHardwareNeeded();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void setUpdateOnHardwareNeeded(boolean r2) {
        /*
            r1 = this;
            A r0 = r1.mTextureAtlas
            r0.setUpdateOnHardwareNeeded(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas.setUpdateOnHardwareNeeded(boolean):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void loadToHardware(javax.microedition.khronos.opengles.GL10 r2) throws java.io.IOException {
        /*
            r1 = this;
            A r0 = r1.mTextureAtlas
            r0.loadToHardware(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas.loadToHardware(javax.microedition.khronos.opengles.GL10):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void unloadFromHardware(javax.microedition.khronos.opengles.GL10 r2) {
        /*
            r1 = this;
            A r0 = r1.mTextureAtlas
            r0.unloadFromHardware(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas.unloadFromHardware(javax.microedition.khronos.opengles.GL10):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void reloadToHardware(javax.microedition.khronos.opengles.GL10 r2) throws java.io.IOException {
        /*
            r1 = this;
            A r0 = r1.mTextureAtlas
            r0.reloadToHardware(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas.reloadToHardware(javax.microedition.khronos.opengles.GL10):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void bind(javax.microedition.khronos.opengles.GL10 r2) {
        /*
            r1 = this;
            A r0 = r1.mTextureAtlas
            r0.bind(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas.bind(javax.microedition.khronos.opengles.GL10):void");
    }

    public TextureOptions getTextureOptions() {
        return this.mTextureAtlas.getTextureOptions();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    @java.lang.Deprecated
    public void addTextureAtlasSource(T r2, int r3, int r4) {
        /*
            r1 = this;
            A r0 = r1.mTextureAtlas
            r0.addTextureAtlasSource(r2, r3, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas.addTextureAtlasSource(org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void removeTextureAtlasSource(T r2, int r3, int r4) {
        /*
            r1 = this;
            A r0 = r1.mTextureAtlas
            r0.removeTextureAtlasSource(r2, r3, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas.removeTextureAtlasSource(org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int):void");
    }

    public void clearTextureAtlasSources() {
        this.mTextureAtlas.clearTextureAtlasSources();
        this.mTextureAtlasSourcesToPlace.clear();
    }

    public boolean hasTextureStateListener() {
        return this.mTextureAtlas.hasTextureStateListener();
    }

    public ITextureAtlas.ITextureAtlasStateListener<T> getTextureStateListener() {
        return this.mTextureAtlas.getTextureStateListener();
    }

    public void addTextureAtlasSource(T pTextureAtlasSource, Callback<T> pCallback) {
        this.mTextureAtlasSourcesToPlace.add(new TextureAtlasSourceWithWithLocationCallback(pTextureAtlasSource, pCallback));
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void removeTextureAtlasSource(org.anddev.andengine.opengl.texture.source.ITextureAtlasSource r6) {
        /*
            r5 = this;
            r4 = 1
            java.util.ArrayList<org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas$TextureAtlasSourceWithWithLocationCallback<T>> r2 = r5.mTextureAtlasSourcesToPlace
            int r3 = r2.size()
            int r0 = r3 - r4
        L_0x0009:
            if (r0 >= 0) goto L_0x000c
        L_0x000b:
            return
        L_0x000c:
            java.lang.Object r1 = r2.get(r0)
            org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas$TextureAtlasSourceWithWithLocationCallback r1 = (org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas.TextureAtlasSourceWithWithLocationCallback) r1
            org.anddev.andengine.opengl.texture.source.ITextureAtlasSource r3 = r1.mTextureAtlasSource
            if (r3 != r6) goto L_0x0021
            r2.remove(r0)
            A r3 = r5.mTextureAtlas
            r3.setUpdateOnHardwareNeeded(r4)
            goto L_0x000b
        L_0x0021:
            int r0 = r0 + -1
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas.removeTextureAtlasSource(org.anddev.andengine.opengl.texture.source.ITextureAtlasSource):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void build(org.anddev.andengine.opengl.texture.buildable.builder.ITextureBuilder<T, A> r3) throws org.anddev.andengine.opengl.texture.buildable.builder.ITextureBuilder.TextureAtlasSourcePackingException {
        /*
            r2 = this;
            A r0 = r2.mTextureAtlas
            java.util.ArrayList<org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas$TextureAtlasSourceWithWithLocationCallback<T>> r1 = r2.mTextureAtlasSourcesToPlace
            r3.pack(r0, r1)
            java.util.ArrayList<org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas$TextureAtlasSourceWithWithLocationCallback<T>> r0 = r2.mTextureAtlasSourcesToPlace
            r0.clear()
            A r0 = r2.mTextureAtlas
            r1 = 1
            r0.setUpdateOnHardwareNeeded(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas.build(org.anddev.andengine.opengl.texture.buildable.builder.ITextureBuilder):void");
    }

    public static class TextureAtlasSourceWithWithLocationCallback<T extends ITextureAtlasSource> {
        private final Callback<T> mCallback;
        /* access modifiers changed from: private */
        public final T mTextureAtlasSource;

        public TextureAtlasSourceWithWithLocationCallback(T pTextureAtlasSource, Callback<T> pCallback) {
            this.mTextureAtlasSource = pTextureAtlasSource;
            this.mCallback = pCallback;
        }

        public Callback<T> getCallback() {
            return this.mCallback;
        }

        public T getTextureAtlasSource() {
            return this.mTextureAtlasSource;
        }
    }
}
