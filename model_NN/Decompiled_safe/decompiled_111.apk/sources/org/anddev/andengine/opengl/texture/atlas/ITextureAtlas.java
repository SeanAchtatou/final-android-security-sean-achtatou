package org.anddev.andengine.opengl.texture.atlas;

import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.texture.source.ITextureAtlasSource;
import org.anddev.andengine.util.Debug;

public interface ITextureAtlas<T extends ITextureAtlasSource> extends ITexture {
    void addTextureAtlasSource(T t, int i, int i2) throws IllegalArgumentException;

    void clearTextureAtlasSources();

    ITextureAtlasStateListener<T> getTextureStateListener();

    void removeTextureAtlasSource(T t, int i, int i2);

    public interface ITextureAtlasStateListener<T extends ITextureAtlasSource> extends ITexture.ITextureStateListener {
        void onTextureAtlasSourceLoadExeption(ITextureAtlas<T> iTextureAtlas, T t, Throwable th);

        public static class TextureAtlasStateAdapter<T extends ITextureAtlasSource> implements ITextureAtlasStateListener<T> {
            public void onLoadedToHardware(ITexture pTexture) {
            }

            public void onTextureAtlasSourceLoadExeption(ITextureAtlas<T> iTextureAtlas, T t, Throwable pThrowable) {
            }

            public void onUnloadedFromHardware(ITexture pTexture) {
            }
        }

        public static class DebugTextureAtlasStateListener<T extends ITextureAtlasSource> implements ITextureAtlasStateListener<T> {
            public void onLoadedToHardware(ITexture pTexture) {
                Debug.d("Texture loaded: " + pTexture.toString());
            }

            public void onTextureAtlasSourceLoadExeption(ITextureAtlas<T> pTextureAtlas, T pTextureAtlasSource, Throwable pThrowable) {
                Debug.e("Exception loading TextureAtlasSource. TextureAtlas: " + pTextureAtlas.toString() + " TextureAtlasSource: " + pTextureAtlasSource.toString(), pThrowable);
            }

            public void onUnloadedFromHardware(ITexture pTexture) {
                Debug.d("Texture unloaded: " + pTexture.toString());
            }
        }
    }
}
