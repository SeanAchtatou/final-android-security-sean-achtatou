package org.anddev.andengine.opengl.texture.region.crop;

import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.opengl.texture.region.BaseTextureRegion;
import org.anddev.andengine.opengl.util.GLHelper;

public class TextureRegionCrop {
    private static final int BOTTOM_INDEX = 1;
    private static final int HEIGHT_INDEX = 3;
    private static final int LEFT_INDEX = 0;
    private static final int WIDTH_INDEX = 2;
    private final int[] mData = new int[4];
    private boolean mDirty = true;
    private boolean mFlippedHorizontal;
    private boolean mFlippedVertical;
    protected final BaseTextureRegion mTextureRegion;

    public TextureRegionCrop(BaseTextureRegion pBaseTextureRegion) {
        this.mTextureRegion = pBaseTextureRegion;
    }

    public boolean isDirty() {
        return this.mDirty;
    }

    public int[] getData() {
        return this.mData;
    }

    public boolean isFlippedHorizontal() {
        return this.mFlippedHorizontal;
    }

    public void setFlippedHorizontal(boolean pFlippedHorizontal) {
        if (this.mFlippedHorizontal != pFlippedHorizontal) {
            this.mFlippedHorizontal = pFlippedHorizontal;
            update();
        }
    }

    public boolean isFlippedVertical() {
        return this.mFlippedVertical;
    }

    public void setFlippedVertical(boolean pFlippedVertical) {
        if (this.mFlippedVertical != pFlippedVertical) {
            this.mFlippedVertical = pFlippedVertical;
            update();
        }
    }

    public void update() {
        BaseTextureRegion textureRegion = this.mTextureRegion;
        if (textureRegion.getTexture() != null) {
            int[] values = this.mData;
            int textureCropLeft = textureRegion.getTextureCropLeft();
            int textureCropTop = textureRegion.getTextureCropTop();
            int textureCropWidth = textureRegion.getTextureCropWidth();
            int textureCropHeight = textureRegion.getTextureCropHeight();
            if (!this.mFlippedVertical && !this.mFlippedHorizontal) {
                values[0] = textureCropLeft;
                values[1] = textureCropTop + textureCropHeight;
                values[2] = textureCropWidth;
                values[3] = -textureCropHeight;
            }
            this.mDirty = true;
        }
    }

    public void selectOnHardware(GL11 pGL11) {
        if (this.mDirty) {
            this.mDirty = false;
            synchronized (this) {
                GLHelper.textureCrop(pGL11, this);
            }
        }
    }

    public void apply(GL11 pGL11) {
        GLHelper.textureCrop(pGL11, this);
    }
}
