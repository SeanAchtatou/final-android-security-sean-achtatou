package org.anddev.andengine.opengl.font;

import android.graphics.Paint;
import android.graphics.Typeface;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;

public class StrokeFont extends Font {
    private final boolean mStrokeOnly;
    private final Paint mStrokePaint;

    public StrokeFont(BitmapTextureAtlas pBitmapTextureAtlas, Typeface pTypeface, float pSize, boolean pAntiAlias, int pColor, float pStrokeWidth, int pStrokeColor) {
        this(pBitmapTextureAtlas, pTypeface, pSize, pAntiAlias, pColor, pStrokeWidth, pStrokeColor, false);
    }

    public StrokeFont(BitmapTextureAtlas pBitmapTextureAtlas, Typeface pTypeface, float pSize, boolean pAntiAlias, int pColor, float pStrokeWidth, int pStrokeColor, boolean pStrokeOnly) {
        super(pBitmapTextureAtlas, pTypeface, pSize, pAntiAlias, pColor);
        this.mStrokePaint = new Paint();
        this.mStrokePaint.setTypeface(pTypeface);
        this.mStrokePaint.setStyle(Paint.Style.STROKE);
        this.mStrokePaint.setStrokeWidth(pStrokeWidth);
        this.mStrokePaint.setColor(pStrokeColor);
        this.mStrokePaint.setTextSize(pSize);
        this.mStrokePaint.setAntiAlias(pAntiAlias);
        this.mStrokeOnly = pStrokeOnly;
    }

    /* access modifiers changed from: protected */
    public void drawCharacterString(String pCharacterAsString) {
        if (!this.mStrokeOnly) {
            super.drawCharacterString(pCharacterAsString);
        }
        this.mCanvas.drawText(pCharacterAsString, 0.0f, -this.mFontMetrics.ascent, this.mStrokePaint);
    }
}
