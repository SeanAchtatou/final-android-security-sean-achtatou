package org.anddev.andengine.opengl.texture.atlas.bitmap.source;

import android.graphics.Bitmap;
import org.anddev.andengine.opengl.texture.source.ITextureAtlasSource;

public interface IBitmapTextureAtlasSource extends ITextureAtlasSource {
    IBitmapTextureAtlasSource clone();

    Bitmap onLoadBitmap(Bitmap.Config config);
}
