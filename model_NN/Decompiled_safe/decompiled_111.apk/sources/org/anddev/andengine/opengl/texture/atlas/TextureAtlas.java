package org.anddev.andengine.opengl.texture.atlas;

import java.util.ArrayList;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.ITextureAtlas;
import org.anddev.andengine.opengl.texture.source.ITextureAtlasSource;
import org.anddev.andengine.util.MathUtils;

public abstract class TextureAtlas<T extends ITextureAtlasSource> extends Texture implements ITextureAtlas<T> {
    protected final int mHeight;
    protected final ArrayList<T> mTextureAtlasSources = new ArrayList<>();
    protected final int mWidth;

    public TextureAtlas(int pWidth, int pHeight, Texture.PixelFormat pPixelFormat, TextureOptions pTextureOptions, ITextureAtlas.ITextureAtlasStateListener<T> pTextureAtlasStateListener) {
        super(pPixelFormat, pTextureOptions, pTextureAtlasStateListener);
        if (!MathUtils.isPowerOfTwo(pWidth) || !MathUtils.isPowerOfTwo(pHeight)) {
            throw new IllegalArgumentException("pWidth and pHeight must be a power of 2!");
        }
        this.mWidth = pWidth;
        this.mHeight = pHeight;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public ITextureAtlas.ITextureAtlasStateListener<T> getTextureStateListener() {
        return (ITextureAtlas.ITextureAtlasStateListener) super.getTextureStateListener();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void addTextureAtlasSource(T r2, int r3, int r4) throws java.lang.IllegalArgumentException {
        /*
            r1 = this;
            r1.checkTextureAtlasSourcePosition(r2, r3, r4)
            r2.setTexturePositionX(r3)
            r2.setTexturePositionY(r4)
            java.util.ArrayList<T> r0 = r1.mTextureAtlasSources
            r0.add(r2)
            r0 = 1
            r1.mUpdateOnHardwareNeeded = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.opengl.texture.atlas.TextureAtlas.addTextureAtlasSource(org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int):void");
    }

    public void removeTextureAtlasSource(T pTextureAtlasSource, int pTexturePositionX, int pTexturePositionY) {
        ArrayList<T> textureSources = this.mTextureAtlasSources;
        for (int i = textureSources.size() - 1; i >= 0; i--) {
            T textureSource = (ITextureAtlasSource) textureSources.get(i);
            if (textureSource == pTextureAtlasSource && textureSource.getTexturePositionX() == pTexturePositionX && textureSource.getTexturePositionY() == pTexturePositionY) {
                textureSources.remove(i);
                this.mUpdateOnHardwareNeeded = true;
                return;
            }
        }
    }

    public void clearTextureAtlasSources() {
        this.mTextureAtlasSources.clear();
        this.mUpdateOnHardwareNeeded = true;
    }

    private void checkTextureAtlasSourcePosition(T pTextureAtlasSource, int pTexturePositionX, int pTexturePositionY) throws IllegalArgumentException {
        if (pTexturePositionX < 0) {
            throw new IllegalArgumentException("Illegal negative pTexturePositionX supplied: '" + pTexturePositionX + "'");
        } else if (pTexturePositionY < 0) {
            throw new IllegalArgumentException("Illegal negative pTexturePositionY supplied: '" + pTexturePositionY + "'");
        } else if (pTextureAtlasSource.getWidth() + pTexturePositionX > getWidth() || pTextureAtlasSource.getHeight() + pTexturePositionY > getHeight()) {
            throw new IllegalArgumentException("Supplied pTextureAtlasSource must not exceed bounds of Texture.");
        }
    }
}
