package org.anddev.andengine.opengl.texture.atlas.bitmap.source;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.IOException;
import java.io.InputStream;
import org.anddev.andengine.opengl.texture.source.BaseTextureAtlasSource;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.StreamUtils;

public class AssetBitmapTextureAtlasSource extends BaseTextureAtlasSource implements IBitmapTextureAtlasSource {
    private final String mAssetPath;
    private final Context mContext;
    private final int mHeight;
    private final int mWidth;

    public AssetBitmapTextureAtlasSource(Context pContext, String pAssetPath) {
        this(pContext, pAssetPath, 0, 0);
    }

    public AssetBitmapTextureAtlasSource(Context pContext, String pAssetPath, int pTexturePositionX, int pTexturePositionY) {
        super(pTexturePositionX, pTexturePositionY);
        this.mContext = pContext;
        this.mAssetPath = pAssetPath;
        BitmapFactory.Options decodeOptions = new BitmapFactory.Options();
        decodeOptions.inJustDecodeBounds = true;
        InputStream in = null;
        try {
            in = pContext.getAssets().open(pAssetPath);
            BitmapFactory.decodeStream(in, null, decodeOptions);
        } catch (IOException e) {
            Debug.e("Failed loading Bitmap in AssetBitmapTextureAtlasSource. AssetPath: " + pAssetPath, e);
        } finally {
            StreamUtils.close(in);
        }
        this.mWidth = decodeOptions.outWidth;
        this.mHeight = decodeOptions.outHeight;
    }

    AssetBitmapTextureAtlasSource(Context pContext, String pAssetPath, int pTexturePositionX, int pTexturePositionY, int pWidth, int pHeight) {
        super(pTexturePositionX, pTexturePositionY);
        this.mContext = pContext;
        this.mAssetPath = pAssetPath;
        this.mWidth = pWidth;
        this.mHeight = pHeight;
    }

    public AssetBitmapTextureAtlasSource clone() {
        return new AssetBitmapTextureAtlasSource(this.mContext, this.mAssetPath, this.mTexturePositionX, this.mTexturePositionY, this.mWidth, this.mHeight);
    }

    public int getWidth() {
        return this.mWidth;
    }

    public int getHeight() {
        return this.mHeight;
    }

    /* JADX INFO: finally extract failed */
    public Bitmap onLoadBitmap(Bitmap.Config pBitmapConfig) {
        InputStream in = null;
        try {
            BitmapFactory.Options decodeOptions = new BitmapFactory.Options();
            decodeOptions.inPreferredConfig = pBitmapConfig;
            in = this.mContext.getAssets().open(this.mAssetPath);
            Bitmap decodeStream = BitmapFactory.decodeStream(in, null, decodeOptions);
            StreamUtils.close(in);
            return decodeStream;
        } catch (IOException e) {
            Debug.e("Failed loading Bitmap in " + getClass().getSimpleName() + ". AssetPath: " + this.mAssetPath, e);
            StreamUtils.close(in);
            return null;
        } catch (Throwable th) {
            StreamUtils.close(in);
            throw th;
        }
    }

    public String toString() {
        return String.valueOf(getClass().getSimpleName()) + "(" + this.mAssetPath + ")";
    }
}
