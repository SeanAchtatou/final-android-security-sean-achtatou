package org.anddev.andengine.opengl.texture.buildable.builder;

import java.util.Comparator;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.anddev.andengine.opengl.texture.atlas.ITextureAtlas;
import org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas;
import org.anddev.andengine.opengl.texture.source.ITextureAtlasSource;

public class BlackPawnTextureBuilder<T extends ITextureAtlasSource, A extends ITextureAtlas<T>> implements ITextureBuilder<T, A> {
    private static final Comparator<BuildableTextureAtlas.TextureAtlasSourceWithWithLocationCallback<?>> TEXTURESOURCE_COMPARATOR = new Comparator<BuildableTextureAtlas.TextureAtlasSourceWithWithLocationCallback<?>>() {
        public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            return compare((BuildableTextureAtlas.TextureAtlasSourceWithWithLocationCallback<?>) ((BuildableTextureAtlas.TextureAtlasSourceWithWithLocationCallback) obj), (BuildableTextureAtlas.TextureAtlasSourceWithWithLocationCallback<?>) ((BuildableTextureAtlas.TextureAtlasSourceWithWithLocationCallback) obj2));
        }

        public int compare(BuildableTextureAtlas.TextureAtlasSourceWithWithLocationCallback<?> pTextureAtlasSourceWithWithLocationCallbackA, BuildableTextureAtlas.TextureAtlasSourceWithWithLocationCallback<?> pTextureAtlasSourceWithWithLocationCallbackB) {
            int deltaWidth = pTextureAtlasSourceWithWithLocationCallbackB.getTextureAtlasSource().getWidth() - pTextureAtlasSourceWithWithLocationCallbackA.getTextureAtlasSource().getWidth();
            if (deltaWidth != 0) {
                return deltaWidth;
            }
            return pTextureAtlasSourceWithWithLocationCallbackB.getTextureAtlasSource().getHeight() - pTextureAtlasSourceWithWithLocationCallbackA.getTextureAtlasSource().getHeight();
        }
    };
    private final int mTextureAtlasSourceSpacing;

    public BlackPawnTextureBuilder(int pTextureAtlasSourceSpacing) {
        this.mTextureAtlasSourceSpacing = pTextureAtlasSourceSpacing;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void pack(A r11, java.util.ArrayList<org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas.TextureAtlasSourceWithWithLocationCallback<T>> r12) throws org.anddev.andengine.opengl.texture.buildable.builder.ITextureBuilder.TextureAtlasSourcePackingException {
        /*
            r10 = this;
            r9 = 0
            java.util.Comparator<org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas$TextureAtlasSourceWithWithLocationCallback<?>> r6 = org.anddev.andengine.opengl.texture.buildable.builder.BlackPawnTextureBuilder.TEXTURESOURCE_COMPARATOR
            java.util.Collections.sort(r12, r6)
            org.anddev.andengine.opengl.texture.buildable.builder.BlackPawnTextureBuilder$Node r2 = new org.anddev.andengine.opengl.texture.buildable.builder.BlackPawnTextureBuilder$Node
            org.anddev.andengine.opengl.texture.buildable.builder.BlackPawnTextureBuilder$Rect r6 = new org.anddev.andengine.opengl.texture.buildable.builder.BlackPawnTextureBuilder$Rect
            int r7 = r11.getWidth()
            int r8 = r11.getHeight()
            r6.<init>(r9, r9, r7, r8)
            r2.<init>(r6)
            int r4 = r12.size()
            r0 = 0
        L_0x001d:
            if (r0 < r4) goto L_0x0020
            return
        L_0x0020:
            java.lang.Object r5 = r12.get(r0)
            org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas$TextureAtlasSourceWithWithLocationCallback r5 = (org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlas.TextureAtlasSourceWithWithLocationCallback) r5
            org.anddev.andengine.opengl.texture.source.ITextureAtlasSource r3 = r5.getTextureAtlasSource()
            int r6 = r11.getWidth()
            int r7 = r11.getHeight()
            int r8 = r10.mTextureAtlasSourceSpacing
            org.anddev.andengine.opengl.texture.buildable.builder.BlackPawnTextureBuilder$Node r1 = r2.insert(r3, r6, r7, r8)
            if (r1 != 0) goto L_0x0053
            org.anddev.andengine.opengl.texture.buildable.builder.ITextureBuilder$TextureAtlasSourcePackingException r6 = new org.anddev.andengine.opengl.texture.buildable.builder.ITextureBuilder$TextureAtlasSourcePackingException
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "Could not pack: "
            r7.<init>(r8)
            java.lang.String r8 = r3.toString()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            r6.<init>(r7)
            throw r6
        L_0x0053:
            org.anddev.andengine.opengl.texture.buildable.builder.BlackPawnTextureBuilder$Rect r6 = r1.mRect
            int r6 = r6.mLeft
            org.anddev.andengine.opengl.texture.buildable.builder.BlackPawnTextureBuilder$Rect r7 = r1.mRect
            int r7 = r7.mTop
            r11.addTextureAtlasSource(r3, r6, r7)
            org.anddev.andengine.util.Callback r6 = r5.getCallback()
            r6.onCallback(r3)
            int r0 = r0 + 1
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.opengl.texture.buildable.builder.BlackPawnTextureBuilder.pack(org.anddev.andengine.opengl.texture.atlas.ITextureAtlas, java.util.ArrayList):void");
    }

    protected static class Rect {
        private final int mHeight;
        /* access modifiers changed from: private */
        public final int mLeft;
        /* access modifiers changed from: private */
        public final int mTop;
        private final int mWidth;

        public Rect(int pLeft, int pTop, int pWidth, int pHeight) {
            this.mLeft = pLeft;
            this.mTop = pTop;
            this.mWidth = pWidth;
            this.mHeight = pHeight;
        }

        public int getWidth() {
            return this.mWidth;
        }

        public int getHeight() {
            return this.mHeight;
        }

        public int getLeft() {
            return this.mLeft;
        }

        public int getTop() {
            return this.mTop;
        }

        public int getRight() {
            return this.mLeft + this.mWidth;
        }

        public int getBottom() {
            return this.mTop + this.mHeight;
        }

        public String toString() {
            return "@: " + this.mLeft + "/" + this.mTop + " * " + this.mWidth + TMXConstants.TAG_OBJECT_ATTRIBUTE_X + this.mHeight;
        }
    }

    protected static class Node {
        private Node mChildA;
        private Node mChildB;
        /* access modifiers changed from: private */
        public final Rect mRect;
        private ITextureAtlasSource mTextureAtlasSource;

        public Node(int pLeft, int pTop, int pWidth, int pHeight) {
            this(new Rect(pLeft, pTop, pWidth, pHeight));
        }

        public Node(Rect pRect) {
            this.mRect = pRect;
        }

        public Rect getRect() {
            return this.mRect;
        }

        public Node getChildA() {
            return this.mChildA;
        }

        public Node getChildB() {
            return this.mChildB;
        }

        public Node insert(ITextureAtlasSource pTextureAtlasSource, int pTextureWidth, int pTextureHeight, int pTextureSpacing) throws IllegalArgumentException {
            if (this.mChildA != null && this.mChildB != null) {
                Node newNode = this.mChildA.insert(pTextureAtlasSource, pTextureWidth, pTextureHeight, pTextureSpacing);
                if (newNode != null) {
                    return newNode;
                }
                return this.mChildB.insert(pTextureAtlasSource, pTextureWidth, pTextureHeight, pTextureSpacing);
            } else if (this.mTextureAtlasSource != null) {
                return null;
            } else {
                int textureSourceWidth = pTextureAtlasSource.getWidth();
                int textureSourceHeight = pTextureAtlasSource.getHeight();
                int rectWidth = this.mRect.getWidth();
                int rectHeight = this.mRect.getHeight();
                if (textureSourceWidth > rectWidth || textureSourceHeight > rectHeight) {
                    return null;
                }
                int textureSourceWidthWithSpacing = textureSourceWidth + pTextureSpacing;
                int textureSourceHeightWithSpacing = textureSourceHeight + pTextureSpacing;
                int rectLeft = this.mRect.getLeft();
                boolean fitToBottomWithoutSpacing = textureSourceHeight == rectHeight && this.mRect.getTop() + textureSourceHeight == pTextureHeight;
                boolean fitToRightWithoutSpacing = textureSourceWidth == rectWidth && rectLeft + textureSourceWidth == pTextureWidth;
                if (textureSourceWidthWithSpacing == rectWidth) {
                    if (textureSourceHeightWithSpacing == rectHeight) {
                        this.mTextureAtlasSource = pTextureAtlasSource;
                        return this;
                    } else if (fitToBottomWithoutSpacing) {
                        this.mTextureAtlasSource = pTextureAtlasSource;
                        return this;
                    }
                }
                if (fitToRightWithoutSpacing) {
                    if (textureSourceHeightWithSpacing == rectHeight) {
                        this.mTextureAtlasSource = pTextureAtlasSource;
                        return this;
                    } else if (fitToBottomWithoutSpacing) {
                        this.mTextureAtlasSource = pTextureAtlasSource;
                        return this;
                    } else if (textureSourceHeightWithSpacing > rectHeight) {
                        return null;
                    } else {
                        return createChildren(pTextureAtlasSource, pTextureWidth, pTextureHeight, pTextureSpacing, rectWidth - textureSourceWidth, rectHeight - textureSourceHeightWithSpacing);
                    }
                } else if (fitToBottomWithoutSpacing) {
                    if (textureSourceWidthWithSpacing == rectWidth) {
                        this.mTextureAtlasSource = pTextureAtlasSource;
                        return this;
                    } else if (textureSourceWidthWithSpacing > rectWidth) {
                        return null;
                    } else {
                        return createChildren(pTextureAtlasSource, pTextureWidth, pTextureHeight, pTextureSpacing, rectWidth - textureSourceWidthWithSpacing, rectHeight - textureSourceHeight);
                    }
                } else if (textureSourceWidthWithSpacing > rectWidth || textureSourceHeightWithSpacing > rectHeight) {
                    return null;
                } else {
                    return createChildren(pTextureAtlasSource, pTextureWidth, pTextureHeight, pTextureSpacing, rectWidth - textureSourceWidthWithSpacing, rectHeight - textureSourceHeightWithSpacing);
                }
            }
        }

        private Node createChildren(ITextureAtlasSource pTextureAtlasSource, int pTextureWidth, int pTextureHeight, int pTextureSpacing, int pDeltaWidth, int pDeltaHeight) {
            Rect rect = this.mRect;
            if (pDeltaWidth >= pDeltaHeight) {
                this.mChildA = new Node(rect.getLeft(), rect.getTop(), pTextureAtlasSource.getWidth() + pTextureSpacing, rect.getHeight());
                this.mChildB = new Node(rect.getLeft() + pTextureAtlasSource.getWidth() + pTextureSpacing, rect.getTop(), rect.getWidth() - (pTextureAtlasSource.getWidth() + pTextureSpacing), rect.getHeight());
            } else {
                this.mChildA = new Node(rect.getLeft(), rect.getTop(), rect.getWidth(), pTextureAtlasSource.getHeight() + pTextureSpacing);
                this.mChildB = new Node(rect.getLeft(), rect.getTop() + pTextureAtlasSource.getHeight() + pTextureSpacing, rect.getWidth(), rect.getHeight() - (pTextureAtlasSource.getHeight() + pTextureSpacing));
            }
            return this.mChildA.insert(pTextureAtlasSource, pTextureWidth, pTextureHeight, pTextureSpacing);
        }
    }
}
