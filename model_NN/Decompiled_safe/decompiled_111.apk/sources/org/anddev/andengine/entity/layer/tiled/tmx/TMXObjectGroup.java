package org.anddev.andengine.entity.layer.tiled.tmx;

import java.util.ArrayList;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.anddev.andengine.util.SAXUtils;
import org.xml.sax.Attributes;

public class TMXObjectGroup implements TMXConstants {
    private final int mHeight;
    private final String mName;
    private final TMXProperties<TMXObjectGroupProperty> mTMXObjectGroupProperties = new TMXProperties<>();
    private final ArrayList<TMXObject> mTMXObjects = new ArrayList<>();
    private final int mWidth;

    public TMXObjectGroup(Attributes pAttributes) {
        this.mName = pAttributes.getValue("", "name");
        this.mWidth = SAXUtils.getIntAttributeOrThrow(pAttributes, "width");
        this.mHeight = SAXUtils.getIntAttributeOrThrow(pAttributes, "height");
    }

    public String getName() {
        return this.mName;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public int getHeight() {
        return this.mHeight;
    }

    /* access modifiers changed from: package-private */
    public void addTMXObject(TMXObject pTMXObject) {
        this.mTMXObjects.add(pTMXObject);
    }

    public ArrayList<TMXObject> getTMXObjects() {
        return this.mTMXObjects;
    }

    public void addTMXObjectGroupProperty(TMXObjectGroupProperty pTMXObjectGroupProperty) {
        this.mTMXObjectGroupProperties.add(pTMXObjectGroupProperty);
    }

    public TMXProperties<TMXObjectGroupProperty> getTMXObjectGroupProperties() {
        return this.mTMXObjectGroupProperties;
    }
}
