package org.anddev.andengine.entity.particle.initializer;

import org.anddev.andengine.entity.particle.Particle;

public interface IParticleInitializer {
    void onInitializeParticle(Particle particle);
}
