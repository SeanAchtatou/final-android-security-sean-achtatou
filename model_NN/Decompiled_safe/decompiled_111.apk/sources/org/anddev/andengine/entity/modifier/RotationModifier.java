package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class RotationModifier extends SingleValueSpanEntityModifier {
    public RotationModifier(float pDuration, float pFromRotation, float pToRotation) {
        this(pDuration, pFromRotation, pToRotation, null, IEaseFunction.DEFAULT);
    }

    public RotationModifier(float pDuration, float pFromRotation, float pToRotation, IEaseFunction pEaseFunction) {
        this(pDuration, pFromRotation, pToRotation, null, pEaseFunction);
    }

    public RotationModifier(float pDuration, float pFromRotation, float pToRotation, IEntityModifier.IEntityModifierListener pEntityModifierListener) {
        super(pDuration, pFromRotation, pToRotation, pEntityModifierListener, IEaseFunction.DEFAULT);
    }

    public RotationModifier(float pDuration, float pFromRotation, float pToRotation, IEntityModifier.IEntityModifierListener pEntityModifierListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromRotation, pToRotation, pEntityModifierListener, pEaseFunction);
    }

    protected RotationModifier(RotationModifier pRotationModifier) {
        super(pRotationModifier);
    }

    public RotationModifier clone() {
        return new RotationModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(IEntity pEntity, float pRotation) {
        pEntity.setRotation(pRotation);
    }

    /* access modifiers changed from: protected */
    public void onSetValue(IEntity pEntity, float pPercentageDone, float pRotation) {
        pEntity.setRotation(pRotation);
    }
}
