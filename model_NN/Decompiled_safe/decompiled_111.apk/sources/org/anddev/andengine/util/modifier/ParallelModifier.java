package org.anddev.andengine.util.modifier;

import java.util.Arrays;
import org.anddev.andengine.util.modifier.IModifier;

public class ParallelModifier<T> extends BaseModifier<T> implements IModifier.IModifierListener<T> {
    private final float mDuration;
    private boolean mFinishedCached;
    private final IModifier<T>[] mModifiers;
    private float mSecondsElapsed;

    public ParallelModifier(IModifier... pModifiers) throws IllegalArgumentException {
        this(null, pModifiers);
    }

    public ParallelModifier(IModifier.IModifierListener<T> pModifierListener, IModifier<T>... pModifiers) throws IllegalArgumentException {
        super(pModifierListener);
        if (pModifiers.length == 0) {
            throw new IllegalArgumentException("pModifiers must not be empty!");
        }
        Arrays.sort(pModifiers, MODIFIER_COMPARATOR_DURATION_DESCENDING);
        this.mModifiers = pModifiers;
        IModifier<T> modifierWithLongestDuration = pModifiers[0];
        this.mDuration = modifierWithLongestDuration.getDuration();
        modifierWithLongestDuration.addModifierListener(this);
    }

    protected ParallelModifier(ParallelModifier parallelModifier) throws IModifier.CloneNotSupportedException {
        IModifier[] otherModifiers = parallelModifier.mModifiers;
        this.mModifiers = new IModifier[otherModifiers.length];
        IModifier<T>[] modifiers = this.mModifiers;
        for (int i = modifiers.length - 1; i >= 0; i--) {
            modifiers[i] = otherModifiers[i].clone();
        }
        IModifier<T> modifierWithLongestDuration = modifiers[0];
        this.mDuration = modifierWithLongestDuration.getDuration();
        modifierWithLongestDuration.addModifierListener(this);
    }

    public ParallelModifier<T> clone() throws IModifier.CloneNotSupportedException {
        return new ParallelModifier<>(this);
    }

    public float getSecondsElapsed() {
        return this.mSecondsElapsed;
    }

    public float getDuration() {
        return this.mDuration;
    }

    public float onUpdate(float pSecondsElapsed, T pItem) {
        if (this.mFinished) {
            return 0.0f;
        }
        float secondsElapsedRemaining = pSecondsElapsed;
        IModifier[] shapeModifiers = this.mModifiers;
        this.mFinishedCached = false;
        while (secondsElapsedRemaining > 0.0f && !this.mFinishedCached) {
            float secondsElapsedUsed = 0.0f;
            for (int i = shapeModifiers.length - 1; i >= 0; i--) {
                secondsElapsedUsed = Math.max(secondsElapsedUsed, shapeModifiers[i].onUpdate(pSecondsElapsed, pItem));
            }
            secondsElapsedRemaining -= secondsElapsedUsed;
        }
        this.mFinishedCached = false;
        float secondsElapsedUsed2 = pSecondsElapsed - secondsElapsedRemaining;
        this.mSecondsElapsed += secondsElapsedUsed2;
        return secondsElapsedUsed2;
    }

    public void reset() {
        this.mFinished = false;
        this.mSecondsElapsed = 0.0f;
        IModifier[] shapeModifiers = this.mModifiers;
        for (int i = shapeModifiers.length - 1; i >= 0; i--) {
            shapeModifiers[i].reset();
        }
    }

    public void onModifierStarted(IModifier<T> iModifier, T pItem) {
        onModifierStarted(pItem);
    }

    public void onModifierFinished(IModifier<T> iModifier, T pItem) {
        this.mFinished = true;
        this.mFinishedCached = true;
        onModifierFinished(pItem);
    }
}
