package org.anddev.andengine.util;

import java.util.HashMap;
import java.util.Map;

public class MultiKeyHashMap<K, V> extends HashMap<MultiKey<K>, V> {
    private static final long serialVersionUID = -6262447639526561122L;

    public V get(K... pKeys) {
        int hashCode = MultiKey.hash(pKeys);
        for (Map.Entry<MultiKey<K>, V> entry : entrySet()) {
            MultiKey<K> entryKey = entry.getKey();
            if (entryKey.hashCode() == hashCode && isEqualKey(entryKey.getKeys(), pKeys)) {
                return entry.getValue();
            }
        }
        return null;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private boolean isEqualKey(K[] r7, K[] r8) {
        /*
            r6 = this;
            r5 = 0
            int r3 = r7.length
            int r4 = r8.length
            if (r3 == r4) goto L_0x0007
            r3 = r5
        L_0x0006:
            return r3
        L_0x0007:
            r0 = 0
        L_0x0008:
            int r3 = r7.length
            if (r0 < r3) goto L_0x000d
            r3 = 1
            goto L_0x0006
        L_0x000d:
            r1 = r7[r0]
            r2 = r8[r0]
            if (r1 != 0) goto L_0x0017
            if (r2 == 0) goto L_0x001f
            r3 = r5
            goto L_0x0006
        L_0x0017:
            boolean r3 = r1.equals(r2)
            if (r3 != 0) goto L_0x001f
            r3 = r5
            goto L_0x0006
        L_0x001f:
            int r0 = r0 + 1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.util.MultiKeyHashMap.isEqualKey(java.lang.Object[], java.lang.Object[]):boolean");
    }
}
