package com.scoreloop.client.android.core.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.scoreloop.client.android.core.util.Logger;
import com.scoreloop.client.android.core.util.PlistParser;
import com.scoreloop.client.android.core.util.StringsParser;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.apache.http.util.EncodingUtils;

final class a {
    private static final Object a = "amount";
    private static final Object b = "currency";
    private static final String c = ("en.lproj" + File.separator + "Localizable.strings");
    private final Context d;
    private Pattern e;

    a(Context context) {
        this.d = context;
    }

    private int a(InputStream inputStream, byte[] bArr) throws IOException {
        int read = inputStream.read(bArr);
        int i = 0;
        for (int i2 = 0; i2 < read; i2++) {
            if (bArr[i2] != 13) {
                bArr[i] = bArr[i2];
                i++;
            }
        }
        return i;
    }

    private Award a(AwardList awardList, Map<String, Object> map) {
        String d2 = PlistParser.d(map.get("identifier"));
        Range c2 = c(PlistParser.d(map.get("counterRange")));
        Integer a2 = PlistParser.a(map.get("importance"));
        Money a3 = a(PlistParser.c(map.get("rewardMoney")));
        return new Award(awardList, d2, c2, a2.intValue(), a3);
    }

    private Money a(Map<String, Object> map) {
        return new Money(PlistParser.d(map.get(b)), new BigDecimal(PlistParser.a(map.get(a)).intValue()));
    }

    private static byte[] a(String str) {
        int i = 0;
        int length = str.length();
        if (length != 40) {
            throw new IllegalStateException("checksum must be 40 characters long");
        }
        byte[] bArr = new byte[20];
        int i2 = 0;
        while (i < length) {
            bArr[i2] = (byte) Integer.parseInt(str.substring(i, i + 2), 16);
            i += 2;
            i2++;
        }
        return bArr;
    }

    private String b(String str, String str2) {
        if (str2 != null) {
            String str3 = str + File.separator + str2 + File.separator + "SLAwards.bundle";
            if (b(str3)) {
                return str3;
            }
        }
        String str4 = str + File.separator + "SLAwards.bundle";
        if (b(str4)) {
            return str4;
        }
        if (!b("SLAwards.bundle")) {
            return null;
        }
        return "SLAwards.bundle";
    }

    private boolean b(String str) {
        try {
            String[] list = this.d.getAssets().list(str);
            for (String equalsIgnoreCase : list) {
                if (equalsIgnoreCase.equalsIgnoreCase("Info.plist")) {
                    return true;
                }
            }
        } catch (IOException e2) {
        }
        return false;
    }

    private Bitmap c(String str, String str2) {
        try {
            return BitmapFactory.decodeStream(d(str, str2));
        } catch (IOException e2) {
            return null;
        }
    }

    private Range c(String str) {
        if (this.e == null) {
            this.e = Pattern.compile("\\s*\\{\\s*(\\d+)\\s*,\\s*(\\d+)\\s*\\}\\s*");
        }
        Matcher matcher = this.e.matcher(str);
        if (matcher.matches()) {
            return new Range(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)));
        }
        throw new IllegalStateException("wrong counterRange value in plist");
    }

    private InputStream d(String str, String str2) throws IOException {
        return this.d.getAssets().open(str + File.separator + str2);
    }

    private void d(String str) throws IOException, NoSuchAlgorithmException {
        byte[] a2 = a(new BufferedReader(new InputStreamReader(d(str, "Info.plist.checksum"))).readLine());
        MessageDigest instance = MessageDigest.getInstance("SHA1");
        InputStream d2 = d(str, "Info.plist");
        byte[] bArr = new byte[PVRTexture.FLAG_BUMPMAP];
        int a3 = a(d2, bArr);
        while (a3 > 0) {
            instance.update(bArr, 0, a3);
            a3 = a(d2, bArr);
        }
        if (!MessageDigest.isEqual(a2, instance.digest(EncodingUtils.getBytes("f02e3c85572dc9ad7cb77c2a638e3", "UTF-8")))) {
            throw new IllegalStateException("invalid plist - checksums do not match");
        }
    }

    /* access modifiers changed from: package-private */
    public AwardList a(String str, String str2) {
        String b2 = b(str, str2);
        if (b2 == null) {
            return null;
        }
        try {
            AwardList awardList = new AwardList();
            d(b2);
            Map<String, Object> c2 = PlistParser.c(new PlistParser().a(d(b2, "Info.plist")));
            awardList.a(PlistParser.d(c2.get("SLAchievableListIdentifier")));
            for (Object c3 : PlistParser.b(c2.get("SLAwards"))) {
                awardList.a(a(awardList, PlistParser.c(c3)));
            }
            Bitmap c4 = c(b2, "Award.default.achieved.png");
            Bitmap c5 = c(b2, "Award.default.unachieved.png");
            for (Award next : awardList.getAwards()) {
                Bitmap c6 = c(b2, next.a());
                if (c6 == null) {
                    c6 = c4;
                }
                next.a(c6);
                Bitmap c7 = c(b2, next.f());
                if (c7 == null) {
                    c7 = c5;
                }
                next.b(c7);
            }
            Map<String, String> a2 = new StringsParser().a(d(b2, c));
            for (Award next2 : awardList.getAwards()) {
                next2.b(a2.get(next2.d()));
                next2.a(a2.get(next2.c()));
            }
            return awardList;
        } catch (Exception e2) {
            e2.printStackTrace();
            Logger.b("error reading awards from bundle" + e2);
            return null;
        }
    }
}
