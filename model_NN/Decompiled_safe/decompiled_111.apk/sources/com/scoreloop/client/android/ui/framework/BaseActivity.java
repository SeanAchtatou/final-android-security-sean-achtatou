package com.scoreloop.client.android.ui.framework;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.chorragames.math.R;
import com.scoreloop.client.android.ui.framework.ValueStore;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public abstract class BaseActivity extends Activity implements ValueStore.Observer, Runnable, DialogInterface.OnDismissListener, OptionsMenuForActivityGroup {
    protected static final String BUNDLE_KEY_VISIBLE_DIALOG_ID = "bundle_key_visible_dialog_id";
    private static boolean LOG_ENABLED = false;
    private View _contentView;
    private ValueStore _currentScreenValues;
    private Handler _handler;
    private boolean _isPaused;
    private boolean _needsRefresh;
    private Set<String> _observedKeys = null;
    private int _refreshFlags;
    private ValueStore _screenValuesSnapshot = null;
    private final Set<Object> _spinnerControllers = new HashSet();
    private int _spinnerSemaphore;
    private View _spinnerView;
    protected int _visibleDialogId = -1;

    public enum RefreshMode {
        MERGE,
        SET
    }

    public static void showToast(Context context, String message) {
        showToast(context, message, null, 0);
    }

    public static void showToast(Context context, String message, Drawable drawable, int duration) {
        View view = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.sl_dialog_toast, (ViewGroup) null);
        ((TextView) view.findViewById(R.id.message)).setText(message);
        if (drawable != null) {
            ((ImageView) view.findViewById(R.id.icon)).setImageDrawable(drawable);
        }
        Toast toast = new Toast(context.getApplicationContext());
        toast.setDuration(duration);
        toast.setView(view);
        toast.show();
    }

    public void onDismiss(DialogInterface dialogInterface) {
        this._visibleDialogId = -1;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(BUNDLE_KEY_VISIBLE_DIALOG_ID, this._visibleDialogId);
    }

    public void addObservedKeys(String... keys) {
        unobserveKeys();
        if (this._observedKeys == null) {
            this._observedKeys = new HashSet();
        }
        Collections.addAll(this._observedKeys, keys);
        if (!this._isPaused) {
            observeKeys();
        }
    }

    public void display(ScreenDescription screenDescription) {
        ScreenManagerSingleton.get().display(screenDescription);
    }

    /* access modifiers changed from: protected */
    public void displayPrevious() {
        if (!isPaused()) {
            ScreenManagerSingleton.get().displayPreviousDescription();
        }
    }

    /* access modifiers changed from: protected */
    public void finishDisplay() {
        ScreenManagerSingleton.get().finishDisplay();
    }

    public ValueStore getActivityArguments() {
        return ScreenManagerSingleton.get().getActivityDescription(getIntent().getStringExtra(ActivityDescription.EXTRA_IDENTIFIER)).getArguments();
    }

    /* access modifiers changed from: protected */
    public ScreenDescription getCurrentScreenDescription() {
        return ScreenManagerSingleton.get().getCurrentDescription();
    }

    /* access modifiers changed from: protected */
    public Handler getHandler() {
        return this._handler;
    }

    public ValueStore getScreenValues() {
        if (this._currentScreenValues == null) {
            this._currentScreenValues = getCurrentScreenDescription().getScreenValues();
        }
        return this._currentScreenValues;
    }

    private FrameLayout getSpinnerParentFrameLayout() {
        for (ViewParent parent = this._contentView.getParent(); parent != null; parent = parent.getParent()) {
            if (parent instanceof FrameLayout) {
                return (FrameLayout) parent;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void hideSpinner() {
        if (this._spinnerSemaphore == 0) {
            throw new IllegalStateException("spinner not shown you want to hide");
        }
        int i = this._spinnerSemaphore - 1;
        this._spinnerSemaphore = i;
        if (i == 0) {
            onSpinnerShow(false);
        }
    }

    /* access modifiers changed from: protected */
    public void hideSpinnerFor(Object controller) {
        if (this._spinnerControllers.contains(controller)) {
            this._spinnerControllers.remove(controller);
            hideSpinner();
        }
    }

    /* access modifiers changed from: protected */
    public boolean isNavigationAllowed(NavigationIntent navigationIntent) {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean isPaused() {
        return this._isPaused;
    }

    private void log(String method) {
        String ownString;
        int index;
        if (LOG_ENABLED && (index = (ownString = toString()).lastIndexOf("@")) != -1) {
            String address = ownString.substring(index);
        }
    }

    private void makeScreenValuesSnapshot() {
        if (this._observedKeys != null) {
            if (this._screenValuesSnapshot == null) {
                this._screenValuesSnapshot = new ValueStore();
            }
            this._screenValuesSnapshot.copyFromOtherForKeys(this._currentScreenValues, this._observedKeys);
        }
    }

    private void observeKeys() {
        if (this._observedKeys != null) {
            ValueStore screenValues = getScreenValues();
            for (String key : this._observedKeys) {
                screenValues.addObserver(key, this);
            }
        }
    }

    public void onBackPressed() {
        displayPrevious();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this._handler = new Handler();
        log("onCreate");
        this._isPaused = true;
        if (savedInstanceState != null) {
            this._visibleDialogId = savedInstanceState.getInt(BUNDLE_KEY_VISIBLE_DIALOG_ID);
            if (this._visibleDialogId != -1) {
                showDialog(this._visibleDialogId);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        log("onDestroy");
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        displayPrevious();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        log("onPause");
        this._isPaused = true;
        unobserveKeys();
        makeScreenValuesSnapshot();
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
    }

    public void onRefresh(int flags) {
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        log("onRestart");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        log("onResume");
        this._isPaused = false;
        observeKeys();
        updateScreenValues();
        refreshIfNeeded();
    }

    /* access modifiers changed from: protected */
    public void onSpinnerShow(boolean show) {
        if (this._contentView != null) {
            if (show) {
                this._spinnerView = getLayoutInflater().inflate((int) R.layout.sl_spinner_view, (ViewGroup) null);
                getSpinnerParentFrameLayout().addView(this._spinnerView);
                return;
            }
            getSpinnerParentFrameLayout().removeView(this._spinnerView);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        log("onStart");
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        log("onStop");
    }

    public void onValueChanged(ValueStore valueStore, String key, Object oldValue, Object newValue) {
    }

    public void onValueSetDirty(ValueStore valueStore, String key) {
    }

    /* access modifiers changed from: protected */
    public void refreshIfNeeded() {
        if (!this._isPaused && this._needsRefresh) {
            this._needsRefresh = false;
            int flags = this._refreshFlags;
            this._refreshFlags = 0;
            onRefresh(flags);
        }
    }

    public void run() {
        refreshIfNeeded();
    }

    /* access modifiers changed from: protected */
    public void setContentView(int resId, boolean supportSpinner) {
        View view = getLayoutInflater().inflate(resId, (ViewGroup) null);
        setContentView(view);
        if (supportSpinner) {
            this._contentView = view;
        }
    }

    /* access modifiers changed from: protected */
    public void setNeedsRefresh() {
        setNeedsRefresh(0, RefreshMode.MERGE);
    }

    /* access modifiers changed from: protected */
    public void setNeedsRefresh(int flags, RefreshMode mode) {
        this._needsRefresh = true;
        if (mode == RefreshMode.MERGE) {
            this._refreshFlags |= flags;
        } else {
            this._refreshFlags = flags;
        }
        getHandler().post(this);
    }

    /* access modifiers changed from: protected */
    public void showDialogSafe(int res) {
        showDialogSafe(res, false);
    }

    /* access modifiers changed from: protected */
    public void showDialogSafe(int res, boolean saveDialogState) {
        if (!this._isPaused) {
            if (saveDialogState) {
                this._visibleDialogId = res;
            } else {
                this._visibleDialogId = -1;
            }
            showDialog(res);
        }
    }

    /* access modifiers changed from: protected */
    public void showSpinner() {
        int i = this._spinnerSemaphore;
        this._spinnerSemaphore = i + 1;
        if (i == 0) {
            onSpinnerShow(true);
        }
    }

    /* access modifiers changed from: protected */
    public void showSpinnerFor(Object controller) {
        if (!this._spinnerControllers.contains(controller)) {
            this._spinnerControllers.add(controller);
            showSpinner();
        }
    }

    public void showToast(String message) {
        showToast(this, message);
    }

    private void unobserveKeys() {
        if (this._observedKeys != null) {
            ValueStore screenValues = getScreenValues();
            for (String key : this._observedKeys) {
                screenValues.removeObserver(key, this);
            }
        }
    }

    private void updateScreenValues() {
        this._currentScreenValues = null;
        ValueStore newScreenValues = getScreenValues();
        if (this._observedKeys != null) {
            newScreenValues.runObserverForKeys(this._screenValuesSnapshot, this._observedKeys, this);
        }
    }

    public final boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public final boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    public final boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenuForActivityGroup(Menu menu) {
        return true;
    }

    public boolean onPrepareOptionsMenuForActivityGroup(Menu menu) {
        return true;
    }

    public boolean onOptionsItemSelectedForActivityGroup(MenuItem item) {
        return false;
    }
}
