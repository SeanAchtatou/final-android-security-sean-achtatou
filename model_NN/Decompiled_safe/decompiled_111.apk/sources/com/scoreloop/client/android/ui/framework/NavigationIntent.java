package com.scoreloop.client.android.ui.framework;

public class NavigationIntent {
    private final Runnable _runnable;
    private final Type _type;

    public enum Type {
        BACK,
        SHORTCUT,
        FORWARD,
        EXIT
    }

    public NavigationIntent(Type type, Runnable runnable) {
        this._type = type;
        this._runnable = runnable;
    }

    public void execute() {
        this._runnable.run();
    }

    public Type getType() {
        return this._type;
    }
}
