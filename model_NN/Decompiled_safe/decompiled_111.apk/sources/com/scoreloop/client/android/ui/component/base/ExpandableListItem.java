package com.scoreloop.client.android.ui.component.base;

import com.chorragames.math.R;

public class ExpandableListItem extends StandardListItem<Void> {
    public static final int COLLAPSED_LIMIT = 2;

    public ExpandableListItem(ComponentActivity context) {
        super(context, context.getResources().getDrawable(R.drawable.sl_icon_see_more), context.getResources().getString(R.string.sl_see_more), null, null);
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.sl_list_item_icon_title_small;
    }

    public int getType() {
        return 11;
    }
}
