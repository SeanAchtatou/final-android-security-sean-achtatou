package com.scoreloop.client.android.core.addon;

import android.os.Handler;
import com.scoreloop.client.android.core.PublishedFor__2_0_0;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.xml.parsers.DocumentBuilderFactory;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.anddev.andengine.util.constants.MIMETypes;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class RSSFeed {
    @PublishedFor__2_0_0
    public static final Policy ON_START_POLICY = new ChainedPolicy(UNREAD_POLICY, STICKY_POLICY);
    @PublishedFor__2_0_0
    public static final Policy STANDARD_POLICY = new a();
    @PublishedFor__2_0_0
    public static final Policy STICKY_POLICY = new b();
    @PublishedFor__2_0_0
    public static final Policy UNREAD_POLICY = new c();
    private static final Set<String> a = new HashSet();
    private RSSItem b;
    private ExecutorService c;
    /* access modifiers changed from: private */
    public List<RSSItem> d;
    /* access modifiers changed from: private */
    public final Handler e;
    /* access modifiers changed from: private */
    public HttpUriRequest f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public RSSItem h;
    /* access modifiers changed from: private */
    public final RSSFeedObserver i;
    /* access modifiers changed from: private */
    public final Session j;
    private final Set<RSSItem> k;
    /* access modifiers changed from: private */
    public State l;

    public static final class ChainedPolicy implements Policy {
        Policy[] a;

        @PublishedFor__2_0_0
        public ChainedPolicy(Policy... policyArr) {
            this.a = policyArr;
        }

        public void collectItems(RSSFeed rSSFeed, List<RSSItem> list, List<RSSItem> list2) {
            ArrayList arrayList = new ArrayList(list);
            ArrayList arrayList2 = new ArrayList();
            for (Policy collectItems : this.a) {
                arrayList2.clear();
                collectItems.collectItems(rSSFeed, arrayList, arrayList2);
                arrayList.clear();
                arrayList.addAll(arrayList2);
            }
            list2.addAll(arrayList2);
        }
    }

    public interface Continuation {
        @PublishedFor__2_0_0
        void withLoadedFeed(List<RSSItem> list, Exception exc);
    }

    public interface Policy {
        @PublishedFor__2_0_0
        void collectItems(RSSFeed rSSFeed, List<RSSItem> list, List<RSSItem> list2);
    }

    public static final class RequestNextItemCanceledException extends RuntimeException {
        private static final long serialVersionUID = 1;
    }

    public enum State {
        IDLE,
        PENDING
    }

    private static class a implements Policy {
        private a() {
        }

        public void collectItems(RSSFeed rSSFeed, List<RSSItem> list, List<RSSItem> list2) {
            for (RSSItem next : list) {
                if (next.isSticky() && !rSSFeed.hasSessionReadFlag(next)) {
                    list2.add(next);
                }
            }
            for (RSSItem next2 : list) {
                if (!next2.isSticky() && rSSFeed.isUnread(next2)) {
                    list2.add(next2);
                }
            }
            RSSItem defaultItem = rSSFeed.getDefaultItem();
            if (defaultItem != null) {
                list2.add(defaultItem);
            }
        }
    }

    private static class b implements Policy {
        private b() {
        }

        public void collectItems(RSSFeed rSSFeed, List<RSSItem> list, List<RSSItem> list2) {
            for (RSSItem next : list) {
                if (next.isSticky()) {
                    list2.add(next);
                }
            }
        }
    }

    private static class c implements Policy {
        private c() {
        }

        public void collectItems(RSSFeed rSSFeed, List<RSSItem> list, List<RSSItem> list2) {
            for (RSSItem next : list) {
                if (rSSFeed.isUnread(next)) {
                    list2.add(next);
                }
            }
        }
    }

    static {
        Collections.addAll(a, MIMETypes.PNG, "image/x-png", MIMETypes.JPEG);
    }

    @PublishedFor__2_0_0
    public RSSFeed(RSSFeedObserver rSSFeedObserver) {
        this(null, rSSFeedObserver);
    }

    @PublishedFor__2_0_0
    public RSSFeed(Session session, RSSFeedObserver rSSFeedObserver) {
        this.k = new HashSet();
        this.l = State.IDLE;
        if (session == null) {
            this.j = Session.getCurrentSession();
        } else {
            this.j = session;
        }
        this.i = rSSFeedObserver;
        this.e = new Handler();
    }

    /* access modifiers changed from: private */
    public String a() {
        String identifier;
        String str = "http://community.scoreloop.com/games/" + this.j.getGame().getIdentifier();
        User user = this.j.getUser();
        if (!(user == null || (identifier = user.getIdentifier()) == null)) {
            str = str + "/users/" + identifier;
        }
        return str + "/feed";
    }

    private String a(Node node) {
        return node.getFirstChild().getNodeValue();
    }

    /* access modifiers changed from: private */
    public List<RSSItem> a(InputStream inputStream) throws IOException {
        NamedNodeMap attributes;
        String nodeValue;
        Node namedItem;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            newInstance.setIgnoringComments(true);
            Document parse = newInstance.newDocumentBuilder().parse(inputStream);
            ArrayList arrayList = new ArrayList();
            NodeList elementsByTagName = parse.getElementsByTagName("item");
            int length = elementsByTagName.getLength();
            for (int i2 = 0; i2 < length; i2++) {
                Node item = elementsByTagName.item(i2);
                RSSItem rSSItem = new RSSItem(this.j.d());
                for (Node firstChild = item.getFirstChild(); firstChild != null; firstChild = firstChild.getNextSibling()) {
                    if (firstChild.getNodeType() == 1) {
                        String nodeName = firstChild.getNodeName();
                        if (nodeName.equalsIgnoreCase("guid")) {
                            rSSItem.b(a(firstChild));
                        } else if (nodeName.equalsIgnoreCase("title")) {
                            rSSItem.e(a(firstChild));
                        } else if (nodeName.equalsIgnoreCase("description")) {
                            rSSItem.a(a(firstChild));
                        } else if (nodeName.equalsIgnoreCase("link")) {
                            rSSItem.d(a(firstChild));
                        } else if (nodeName.equalsIgnoreCase("source")) {
                            String a2 = a(firstChild);
                            NamedNodeMap attributes2 = firstChild.getAttributes();
                            if (!(attributes2 == null || (namedItem = attributes2.getNamedItem("url")) == null || !a().equalsIgnoreCase(namedItem.getNodeValue()))) {
                                rSSItem.a("Sticky".equalsIgnoreCase(a2));
                            }
                        } else if (nodeName.equalsIgnoreCase("enclosure") && (attributes = firstChild.getAttributes()) != null) {
                            Node namedItem2 = attributes.getNamedItem("url");
                            Node namedItem3 = attributes.getNamedItem(TMXConstants.TAG_OBJECT_ATTRIBUTE_TYPE);
                            if (!(namedItem2 == null || namedItem3 == null || (nodeValue = namedItem3.getNodeValue()) == null || !a.contains(nodeValue))) {
                                rSSItem.c(namedItem2.getNodeValue());
                            }
                        }
                    }
                }
                if (rSSItem.a()) {
                    arrayList.add(rSSItem);
                }
            }
            return arrayList;
        } catch (Exception e2) {
            throw new IOException(e2.getMessage());
        }
    }

    private void a(final Continuation continuation) {
        synchronized (this) {
            this.g = false;
            this.f = null;
        }
        if (this.d != null) {
            continuation.withLoadedFeed(this.d, null);
            return;
        }
        if (this.c == null) {
            this.c = Executors.newSingleThreadExecutor();
        }
        this.c.execute(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.scoreloop.client.android.core.addon.RSSFeed.a(com.scoreloop.client.android.core.addon.RSSFeed, org.apache.http.client.methods.HttpUriRequest):org.apache.http.client.methods.HttpUriRequest
             arg types: [com.scoreloop.client.android.core.addon.RSSFeed, org.apache.http.client.methods.HttpGet]
             candidates:
              com.scoreloop.client.android.core.addon.RSSFeed.a(com.scoreloop.client.android.core.addon.RSSFeed, com.scoreloop.client.android.core.addon.RSSFeed$State):com.scoreloop.client.android.core.addon.RSSFeed$State
              com.scoreloop.client.android.core.addon.RSSFeed.a(com.scoreloop.client.android.core.addon.RSSFeed, java.io.InputStream):java.util.List
              com.scoreloop.client.android.core.addon.RSSFeed.a(com.scoreloop.client.android.core.addon.RSSFeed, java.util.List):java.util.List
              com.scoreloop.client.android.core.addon.RSSFeed.a(com.scoreloop.client.android.core.addon.RSSFeed, com.scoreloop.client.android.core.addon.RSSItem):void
              com.scoreloop.client.android.core.addon.RSSFeed.a(java.util.List<com.scoreloop.client.android.core.addon.RSSItem>, com.scoreloop.client.android.core.addon.RSSFeed$Continuation):void
              com.scoreloop.client.android.core.addon.RSSFeed.a(com.scoreloop.client.android.core.addon.RSSFeed, org.apache.http.client.methods.HttpUriRequest):org.apache.http.client.methods.HttpUriRequest */
            public void run() {
                try {
                    DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                    synchronized (RSSFeed.this) {
                        if (RSSFeed.this.g) {
                            throw new RequestNextItemCanceledException();
                        }
                        HttpUriRequest unused = RSSFeed.this.f = (HttpUriRequest) new HttpGet(RSSFeed.this.a());
                    }
                    RSSFeed.this.f.setHeader("Accept", "application/rss+xml");
                    RSSFeed.this.a((List) defaultHttpClient.execute(RSSFeed.this.f, new ResponseHandler<List<RSSItem>>() {
                        /* renamed from: a */
                        public List<RSSItem> handleResponse(HttpResponse httpResponse) throws ClientProtocolException, IOException {
                            HttpEntity entity = httpResponse.getEntity();
                            if (entity != null) {
                                return RSSFeed.this.a(entity.getContent());
                            }
                            return null;
                        }
                    }), continuation);
                } catch (Exception e) {
                    RSSFeed.this.e.post(new Runnable() {
                        public void run() {
                            Exception exc = e;
                            synchronized (RSSFeed.this) {
                                if (RSSFeed.this.g) {
                                    exc = new RequestNextItemCanceledException();
                                }
                            }
                            continuation.withLoadedFeed(null, exc);
                        }
                    });
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(RSSItem rSSItem) {
        if (rSSItem != null) {
            this.k.add(rSSItem);
            rSSItem.setHasPersistentReadFlag(true);
        }
    }

    /* access modifiers changed from: private */
    public void a(final List<RSSItem> list, final Continuation continuation) {
        this.e.post(new Runnable() {
            public void run() {
                RequestNextItemCanceledException requestNextItemCanceledException;
                List unused = RSSFeed.this.d = list;
                HashSet<RSSItem> hashSet = new HashSet<>();
                for (RSSItem rSSItem : list) {
                    if (rSSItem.hasPersistentReadFlag()) {
                        hashSet.add(rSSItem);
                    }
                }
                RSSItem.resetPersistentReadFlags(RSSFeed.this.j.d(), list);
                for (RSSItem hasPersistentReadFlag : hashSet) {
                    hasPersistentReadFlag.setHasPersistentReadFlag(true);
                }
                synchronized (RSSFeed.this) {
                    requestNextItemCanceledException = RSSFeed.this.g ? new RequestNextItemCanceledException() : null;
                }
                if (requestNextItemCanceledException != null) {
                    continuation.withLoadedFeed(null, requestNextItemCanceledException);
                } else {
                    continuation.withLoadedFeed(list, null);
                }
            }
        });
    }

    @PublishedFor__2_0_0
    public void cancelRequestNextItem() {
        if (getState() == State.PENDING) {
            synchronized (this) {
                this.g = true;
                if (this.f != null && !this.f.isAborted()) {
                    this.f.abort();
                }
            }
        }
    }

    @PublishedFor__2_0_0
    public RSSItem getDefaultItem() {
        return this.b;
    }

    @PublishedFor__2_0_0
    public RSSItem getLastItem() {
        return this.h;
    }

    @PublishedFor__2_0_0
    public State getState() {
        return this.l;
    }

    @PublishedFor__2_0_0
    public boolean hasSessionReadFlag(RSSItem rSSItem) {
        return this.k.contains(rSSItem);
    }

    @PublishedFor__2_0_0
    public boolean isUnread(RSSItem rSSItem) {
        return !hasSessionReadFlag(rSSItem) && !rSSItem.hasPersistentReadFlag();
    }

    @PublishedFor__2_0_0
    public void reloadOnNextRequest() {
        this.d = null;
        this.k.clear();
    }

    @PublishedFor__2_0_0
    public boolean requestAllItems(final Continuation continuation, final boolean z, final Policy policy) {
        if (getState() != State.IDLE) {
            return false;
        }
        this.l = State.PENDING;
        a(new Continuation() {
            public void withLoadedFeed(List<RSSItem> list, Exception exc) {
                ArrayList arrayList;
                if (policy != null) {
                    ArrayList arrayList2 = new ArrayList();
                    policy.collectItems(RSSFeed.this, list, arrayList2);
                    arrayList = arrayList2;
                } else {
                    arrayList = list;
                }
                if (z) {
                    for (RSSItem a2 : arrayList) {
                        RSSFeed.this.a(a2);
                    }
                }
                State unused = RSSFeed.this.l = State.IDLE;
                continuation.withLoadedFeed(arrayList, exc);
            }
        });
        return true;
    }

    @PublishedFor__2_0_0
    public void requestNextItem(final Policy policy) {
        if (policy == null) {
            throw new IllegalArgumentException("policy argument must not be null");
        } else if (getState() == State.IDLE) {
            this.l = State.PENDING;
            if (this.i != null) {
                this.i.feedDidRequestNextItem(this);
            }
            a(new Continuation() {
                public void withLoadedFeed(List<RSSItem> list, Exception exc) {
                    RSSItem rSSItem;
                    if (list != null) {
                        ArrayList arrayList = new ArrayList();
                        policy.collectItems(RSSFeed.this, list, arrayList);
                        if (!arrayList.isEmpty()) {
                            rSSItem = (RSSItem) arrayList.get(0);
                            RSSFeed.this.a(rSSItem);
                        } else {
                            rSSItem = null;
                        }
                        RSSItem unused = RSSFeed.this.h = rSSItem;
                        State unused2 = RSSFeed.this.l = State.IDLE;
                        if (RSSFeed.this.i != null) {
                            RSSFeed.this.i.feedDidReceiveNextItem(RSSFeed.this, rSSItem);
                            return;
                        }
                        return;
                    }
                    State unused3 = RSSFeed.this.l = State.IDLE;
                    if (RSSFeed.this.i != null) {
                        RSSFeed.this.i.feedDidFailToReceiveNextItem(RSSFeed.this, exc);
                    }
                }
            });
        }
    }

    @PublishedFor__2_0_0
    public void setDefaultItem(RSSItem rSSItem) {
        if (rSSItem != null) {
            rSSItem.b("29007410-1D00-4291-AD44-EBAB78720949");
            if (!rSSItem.a()) {
                throw new IllegalArgumentException("the item needs to have at least a title set");
            }
        }
        this.b = rSSItem;
    }
}
