package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Device;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.util.Logger;
import java.nio.channels.IllegalSelectorException;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.json.JSONException;
import org.json.JSONObject;

class b extends RequestController {
    private Request c;
    private Device d;
    private Request e;

    private static class a extends Request {
        private final Device a;
        private final C0002b b;

        public a(RequestCompletionCallback requestCompletionCallback, Device device, C0002b bVar) {
            super(requestCompletionCallback);
            this.a = device;
            this.b = bVar;
        }

        public String a() {
            return "/service/device";
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                switch (this.b) {
                    case VERIFY:
                        jSONObject.put("uuid", this.a.h());
                        jSONObject.put("system", this.a.c());
                        break;
                    case CREATE:
                        jSONObject.put(Device.a, this.a.d());
                        break;
                    case RESET:
                        JSONObject jSONObject2 = new JSONObject();
                        jSONObject2.put("uuid", this.a.h());
                        jSONObject2.put(TMXConstants.TAG_TILE_ATTRIBUTE_ID, this.a.getIdentifier());
                        jSONObject2.put("system", this.a.c());
                        jSONObject2.put("state", "freed");
                        jSONObject.put(Device.a, jSONObject2);
                        break;
                    case UPDATE:
                        jSONObject.put(Device.a, this.a.d());
                        break;
                    default:
                        throw new IllegalSelectorException();
                }
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid device data", e);
            }
        }

        public RequestMethod c() {
            switch (this.b) {
                case VERIFY:
                    return RequestMethod.GET;
                case CREATE:
                    return RequestMethod.POST;
                case RESET:
                case UPDATE:
                    return RequestMethod.PUT;
                default:
                    throw new IllegalSelectorException();
            }
        }

        public C0002b d() {
            return this.b;
        }
    }

    /* renamed from: com.scoreloop.client.android.core.controller.b$b  reason: collision with other inner class name */
    enum C0002b {
        CREATE(21),
        RESET(22),
        UPDATE(23),
        VERIFY(20);
        
        private final int a;

        private C0002b(int i) {
            this.a = i;
        }
    }

    b(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.d = session.a();
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        int f = response.f();
        JSONObject optJSONObject = response.e().optJSONObject(Device.a);
        if (((a) request).d() == C0002b.VERIFY) {
            if (f == 404) {
                return false;
            }
            if (this.c != null) {
                this.c.n();
            }
            this.c = null;
        }
        if ((f == 200 || f == 201) && optJSONObject != null) {
            this.d.b(optJSONObject.getString(TMXConstants.TAG_TILE_ATTRIBUTE_ID));
            if ("freed".equalsIgnoreCase(optJSONObject.optString("state"))) {
                this.d.a(Device.State.FREED);
            } else {
                this.d.a(f == 200 ? Device.State.VERIFIED : Device.State.CREATED);
            }
            return true;
        }
        throw new Exception("Request failed with status: " + f);
    }

    /* access modifiers changed from: protected */
    public void a_() {
        Logger.a("DeviceController", "reset()");
        super.a_();
        if (this.e != null) {
            if (!this.e.m()) {
                Logger.a("DeviceController", "reset() - canceling verify request");
                h().b().b(this.e);
            }
            this.e = null;
        }
    }

    /* access modifiers changed from: package-private */
    public Device b() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        a_();
        this.e = new a(g(), b(), C0002b.VERIFY);
        a(this.e);
        this.c = new a(g(), b(), C0002b.CREATE);
        a(this.c);
    }
}
