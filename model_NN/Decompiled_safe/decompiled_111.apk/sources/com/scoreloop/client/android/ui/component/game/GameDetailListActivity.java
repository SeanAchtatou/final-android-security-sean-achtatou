package com.scoreloop.client.android.ui.component.game;

import android.content.Context;
import android.os.Bundle;
import com.chorragames.math.R;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.UsersController;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.CaptionListItem;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.EmptyListItem;
import com.scoreloop.client.android.ui.framework.BaseListAdapter;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import java.util.List;

public class GameDetailListActivity extends ComponentListActivity<BaseListItem> implements RequestControllerObserver {
    protected UsersController _usersController;

    class GameDetailListAdapter extends BaseListAdapter<BaseListItem> {
        public GameDetailListAdapter(Context context) {
            super(context);
            add(new CaptionListItem(context, null, context.getString(R.string.sl_details)));
            add(new GameDetailListItem(context, null, GameDetailListActivity.this.getGame()));
        }
    }

    /* access modifiers changed from: protected */
    public void addUsers(BaseListAdapter<BaseListItem> adapter, List<User> users) {
        if (users.size() == 0) {
            adapter.add(new EmptyListItem(this, getString(R.string.sl_no_friends_playing)));
            return;
        }
        for (User user : users) {
            adapter.add(new GameDetailUserListItem(this, user));
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new GameDetailListAdapter(this));
        this._usersController = new UsersController(this);
        this._usersController.loadBuddies(getUser(), getGame());
    }

    public void onRefresh(int flags) {
        super.onRefresh(flags);
        getBaseListAdapter().notifyDataSetChanged();
    }

    public void onListItemClick(BaseListItem item) {
        if (item.getType() == 14) {
            display(getFactory().createUserDetailScreenDescription((User) ((GameDetailUserListItem) item).getTarget(), null));
        }
    }

    public void requestControllerDidReceiveResponseSafe(RequestController controller) {
        if (controller == this._usersController) {
            List<User> users = this._usersController.getUsers();
            BaseListAdapter<BaseListItem> adapter = getBaseListAdapter();
            adapter.add(new CaptionListItem(this, null, String.format(getString(R.string.sl_format_friends_playing), getGame().getName())));
            addUsers(adapter, users);
        }
    }
}
