package com.scoreloop.client.android.ui.framework;

import android.app.Activity;

public interface ScreenActivityProtocol {
    void cleanOutSubactivities();

    Activity getActivity();

    boolean isNavigationAllowed(NavigationIntent navigationIntent);

    void setShortcuts(ScreenDescription screenDescription);

    void startBody(ActivityDescription activityDescription, int i);

    void startEmptyBody();

    void startHeader(ActivityDescription activityDescription, int i);

    void startNewScreen();

    void startTabBody(ScreenDescription screenDescription, int i);
}
