package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.util.SetterIntent;
import java.util.ArrayList;
import java.util.List;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class BaseEntity implements Entity {
    protected String b;

    private static class a {
        final String a;
        final String b;

        a(String str, String str2) {
            this.a = str;
            this.b = str2;
        }

        /* access modifiers changed from: package-private */
        public void a(StringBuilder sb) {
            sb.append(this.a);
            sb.append("=");
            sb.append(this.b);
        }
    }

    protected BaseEntity() {
        this(null);
    }

    protected BaseEntity(String str) {
        this.b = str;
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) {
        a(obj, TMXConstants.TAG_TILE_ATTRIBUTE_ID, this.b);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, String str, Object obj2) {
        ((List) obj).add(new a(str, obj2 != null ? obj2.toString() : "null"));
    }

    public void a(JSONObject jSONObject) throws JSONException {
        SetterIntent setterIntent = new SetterIntent();
        if (setterIntent.h(jSONObject, TMXConstants.TAG_TILE_ATTRIBUTE_ID, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            b((String) setterIntent.a());
        }
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        if (str == null) {
            throw new IllegalArgumentException("identifier must not be null");
        }
        this.b = str;
    }

    public boolean b() {
        return getIdentifier() != null;
    }

    public JSONObject d() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (this.b != null) {
            jSONObject.put(TMXConstants.TAG_TILE_ATTRIBUTE_ID, this.b);
        }
        return jSONObject;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        BaseEntity baseEntity = (BaseEntity) obj;
        if (this.b == null) {
            if (baseEntity.b != null) {
                return false;
            }
        } else if (!this.b.equals(baseEntity.b)) {
            return false;
        }
        return true;
    }

    @PublishedFor__1_0_0
    public String getIdentifier() {
        return this.b;
    }

    public int hashCode() {
        int i = 1 * 31;
        return (this.b == null ? 0 : this.b.hashCode()) + 31;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        ArrayList<a> arrayList = new ArrayList<>();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        a(arrayList);
        boolean z = true;
        for (a aVar : arrayList) {
            if (!z) {
                sb.append(",");
            }
            aVar.a(sb);
            z = false;
        }
        sb.append("]");
        return sb.toString();
    }
}
