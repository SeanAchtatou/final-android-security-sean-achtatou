package com.scoreloop.client.android.ui.framework;

import android.content.Context;
import android.util.AttributeSet;
import com.chorragames.math.R;

public class TabView extends SegmentedView {
    private final int[][] res2 = {new int[]{R.drawable.sl_tab_active, R.drawable.sl_tab_default_left_border}, new int[]{R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_active}};
    private final int[][] res3 = {new int[]{R.drawable.sl_tab_active, R.drawable.sl_tab_default_left_border, R.drawable.sl_tab_default_left_border}, new int[]{R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_active, R.drawable.sl_tab_default_left_border}, new int[]{R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_active}};
    private final int[][] res4 = {new int[]{R.drawable.sl_tab_active, R.drawable.sl_tab_default_left_border, R.drawable.sl_tab_default_left_border, R.drawable.sl_tab_default_left_border}, new int[]{R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_active, R.drawable.sl_tab_default_left_border, R.drawable.sl_tab_default_left_border}, new int[]{R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_active, R.drawable.sl_tab_default_left_border}, new int[]{R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_active}};

    public TabView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void prepareSelection() {
        if (getChildCount() != 0) {
            switchToSegment(0);
        }
    }

    /* access modifiers changed from: protected */
    public void setSegmentEnabled(int segment, boolean enabled) {
        getChildAt(segment).setEnabled(true);
    }

    public void switchToSegment(int segment) {
        if (segment != this.selectedSegment) {
            updateHighlight(segment);
            setSegment(segment);
        }
    }

    private void updateHighlight(int segment) {
        int[][] res;
        int count = getChildCount();
        switch (count) {
            case 2:
                res = this.res2;
                break;
            case 3:
                res = this.res3;
                break;
            case 4:
                res = this.res4;
                break;
            default:
                throw new IllegalStateException("unsupported number of tabs");
        }
        for (int i = 0; i < count; i++) {
            getChildAt(i).setBackgroundResource(res[segment][i]);
        }
    }
}
