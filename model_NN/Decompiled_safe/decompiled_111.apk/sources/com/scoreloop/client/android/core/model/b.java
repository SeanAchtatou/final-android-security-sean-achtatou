package com.scoreloop.client.android.core.model;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.scoreloop.client.android.core.util.Logger;
import java.util.Arrays;
import java.util.List;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;

class b {
    private static final List<String> b = Arrays.asList("com.google", "com.android", "com.sec.android");
    final Context a;
    private final long c = 10000;

    b(Context context) {
        this.a = context;
    }

    private void a(SharedDeviceUuidStore sharedDeviceUuidStore, SharedDeviceUuidStore sharedDeviceUuidStore2, SharedDeviceUuid sharedDeviceUuid) {
        if (c()) {
            Logger.a("saving device if to shared store.");
            sharedDeviceUuidStore2.b(sharedDeviceUuid);
        }
        Logger.a("saving device if to app store.");
        sharedDeviceUuidStore.b(sharedDeviceUuid);
    }

    private boolean a(PackageInfo packageInfo) {
        if (packageInfo.versionName == null) {
            return true;
        }
        for (String startsWith : b) {
            if (packageInfo.packageName.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    private String b() {
        SharedDeviceUuid sharedDeviceUuid = new SharedDeviceUuid();
        try {
            a(new SharedDeviceUuidStore(this.a), new SharedDeviceUuidStore(), sharedDeviceUuid);
        } catch (Exception e) {
        }
        return sharedDeviceUuid.a();
    }

    private boolean c() {
        try {
            return Arrays.asList(this.a.getPackageManager().getPackageInfo(this.a.getPackageName(), (int) PVRTexture.FLAG_CUBEMAP).requestedPermissions).contains("android.permission.WRITE_EXTERNAL_STORAGE");
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private SharedDeviceUuid d() {
        long currentTimeMillis = System.currentTimeMillis();
        List<PackageInfo> installedPackages = this.a.getPackageManager().getInstalledPackages(0);
        int i = 0;
        for (PackageInfo next : installedPackages) {
            if (System.currentTimeMillis() - currentTimeMillis > 10000) {
                Logger.a("timeout while scanning apps for shared device id.");
                return null;
            }
            try {
                String str = next.packageName;
                if (!a(next)) {
                    i++;
                    SharedDeviceUuid sharedDeviceUuid = (SharedDeviceUuid) new SharedDeviceUuidStore(this.a.createPackageContext(str, 2)).b();
                    if (sharedDeviceUuid != null) {
                        Logger.a("using shared device id from: " + str);
                        return sharedDeviceUuid;
                    }
                } else {
                    continue;
                }
            } catch (PackageManager.NameNotFoundException e) {
            }
            i = i;
        }
        Logger.a(String.format("scanned %s/%s packages for devices id; time=%s", Integer.valueOf(i), Integer.valueOf(installedPackages.size()), Long.valueOf(System.currentTimeMillis() - currentTimeMillis)));
        return null;
    }

    private String e() {
        return Settings.Secure.getString(this.a.getContentResolver(), "android_id");
    }

    private String f() {
        try {
            return ((TelephonyManager) this.a.getSystemService("phone")).getDeviceId();
        } catch (Throwable th) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public String a() {
        try {
            String f = f();
            if (f != null && !f.equalsIgnoreCase("000000000000000")) {
                return f;
            }
            String e = e();
            if (e != null && !e.equals("9774d56d682e549c")) {
                return e;
            }
            SharedDeviceUuidStore sharedDeviceUuidStore = new SharedDeviceUuidStore(this.a);
            SharedDeviceUuid sharedDeviceUuid = (SharedDeviceUuid) sharedDeviceUuidStore.b();
            if (sharedDeviceUuid != null) {
                Logger.a("using app device id.");
                return sharedDeviceUuid.a();
            }
            SharedDeviceUuidStore sharedDeviceUuidStore2 = new SharedDeviceUuidStore();
            SharedDeviceUuid sharedDeviceUuid2 = (SharedDeviceUuid) sharedDeviceUuidStore2.b();
            if (sharedDeviceUuid2 != null) {
                Logger.a("read device id from shared store. saving to app store.");
                sharedDeviceUuidStore.b(sharedDeviceUuid2);
                return sharedDeviceUuid2.a();
            }
            SharedDeviceUuid d = d();
            if (d != null) {
                a(sharedDeviceUuidStore, sharedDeviceUuidStore2, d);
                return d.a();
            }
            SharedDeviceUuid sharedDeviceUuid3 = new SharedDeviceUuid();
            Logger.a("created new shared device uuid=" + sharedDeviceUuid3);
            a(sharedDeviceUuidStore, sharedDeviceUuidStore2, sharedDeviceUuid3);
            return sharedDeviceUuid3.a();
        } catch (Exception e2) {
            return b();
        }
    }
}
