package com.scoreloop.client.android.ui.component.base;

import android.view.View;
import android.view.WindowManager;
import com.scoreloop.client.android.ui.framework.BaseListAdapter;
import com.scoreloop.client.android.ui.framework.BaseListItem;

public abstract class Constant {
    public static final String ACHIEVEMENTS_ENGINE = "achievementsEngine";
    public static final String CHALLENGE = "challenge";
    public static final String CHALLENGE_HEADER_MODE = "challengeHeaderMode";
    public static final int CHALLENGE_HEADER_MODE_BUY = 1;
    public static final int CHALLENGE_HEADER_MODE_INFO = 0;
    public static final String CONFIGURATION = "configuration";
    public static final String CONTESTANT = "contestant";
    public static final int DIALOG_ADD_FRIEND_LOGIN = 19;
    public static final int DIALOG_CHALLENGE_ERROR_ACCEPT = 2;
    public static final int DIALOG_CHALLENGE_ERROR_BALANCE = 1;
    public static final int DIALOG_CHALLENGE_ERROR_REJECT = 3;
    public static final int DIALOG_CHALLENGE_GAME_NOT_READY = 6;
    public static final int DIALOG_CHALLENGE_LEAVE_ACCEPT = 4;
    public static final int DIALOG_CHALLENGE_LEAVE_PAYMENT = 7;
    public static final int DIALOG_CHALLENGE_ONGOING = 5;
    public static final int DIALOG_CONFIRMATION_MATCH_BUDDIES = 10;
    public static final int DIALOG_CONFIRMATION_RECOMMEND_GAME = 11;
    public static final int DIALOG_ERROR_NETWORK = 0;
    public static final int DIALOG_GAME_MODE = 18;
    public static final int DIALOG_PROFILE_CHANGE_EMAIL = 13;
    public static final int DIALOG_PROFILE_CHANGE_USERNAME = 12;
    public static final int DIALOG_PROFILE_FIRST_TIME = 14;
    public static final int DIALOG_PROFILE_MERGE_ACCOUNTS = 17;
    public static final int DIALOG_PROFILE_MSG = 15;
    public static final String FACTORY = "factory";
    public static final String FEATURED_GAME = "featuredGame";
    public static final String FEATURED_GAME_IMAGE_URL = "featuredGameImageUrl";
    public static final String FEATURED_GAME_NAME = "featuredGameName";
    public static final String FEATURED_GAME_PUBLISHER = "featuredGamePublisher";
    public static final String GAME = "game";
    public static final String GAME_HEADER_CONTROL = "gameHeaderControl";
    public static final String GAME_IMAGE_URL = "gameImageUrl";
    public static final String GAME_LIST_MODE = "gameListMode";
    public static final int GAME_MODE_BUDDIES = 3;
    public static final int GAME_MODE_LAST = 4;
    public static final int GAME_MODE_NEW = 2;
    public static final int GAME_MODE_POPULAR = 1;
    public static final int GAME_MODE_USER = 0;
    public static final String GAME_NAME = "gameName";
    public static final String GAME_PUBLISHER = "gamePublisher";
    public static final String GAME_VALUES = "gameValues";
    public static final String IMAGE_URL_BUDDIES_GAMES = "imageUrlBuddiesGames";
    public static final String IMAGE_URL_NEW_GAMES = "imageUrlNewGames";
    public static final String IMAGE_URL_POPULAR_GAMES = "imageUrlPopularGames";
    public static final String IMAGE_URL_USER_GAMES = "imageUrlUserGames";
    public static final String IS_LOCAL_LEADEARBOARD = "isLocalLeadearboard";
    public static final int LEADERBOARD_LOCAL = 3;
    public static final int LIST_ITEM_TYPE_ACHIEVEMENT = 1;
    public static final int LIST_ITEM_TYPE_CAPTION = 2;
    public static final int LIST_ITEM_TYPE_CHALLENGE_CONTROLS = 3;
    public static final int LIST_ITEM_TYPE_CHALLENGE_HISTORY = 4;
    public static final int LIST_ITEM_TYPE_CHALLENGE_NEW = 5;
    public static final int LIST_ITEM_TYPE_CHALLENGE_OPEN = 6;
    public static final int LIST_ITEM_TYPE_CHALLENGE_PARTICIPANTS = 7;
    public static final int LIST_ITEM_TYPE_CHALLENGE_STAKE_AND_MODE = 9;
    public static final int LIST_ITEM_TYPE_EMPTY = 16;
    public static final int LIST_ITEM_TYPE_ENTRY = 10;
    public static final int LIST_ITEM_TYPE_EXPANDABLE = 11;
    public static final int LIST_ITEM_TYPE_GAME = 12;
    public static final int LIST_ITEM_TYPE_GAME_DETAIL = 13;
    public static final int LIST_ITEM_TYPE_GAME_DETAIL_USER = 14;
    public static final int LIST_ITEM_TYPE_NEWS = 15;
    public static final int LIST_ITEM_TYPE_PAGING = 0;
    public static final int LIST_ITEM_TYPE_PROFILE = 17;
    public static final int LIST_ITEM_TYPE_PROFILE_PICTURE = 18;
    public static final int LIST_ITEM_TYPE_SCORE = 19;
    public static final int LIST_ITEM_TYPE_SCORE_EXCLUDED = 20;
    public static final int LIST_ITEM_TYPE_SCORE_HIGHLIGHTED = 21;
    public static final int LIST_ITEM_TYPE_SCORE_SUBMIT_LOCAL = 22;
    public static final int LIST_ITEM_TYPE_STANDARD = 23;
    public static final int LIST_ITEM_TYPE_USER = 24;
    public static final int LIST_ITEM_TYPE_USER_ADD_BUDDIES = 25;
    public static final int LIST_ITEM_TYPE_USER_ADD_BUDDY = 26;
    public static final int LIST_ITEM_TYPE_USER_DETAIL = 27;
    public static final int LIST_ITEM_TYPE_USER_FIND_MATCH = 28;
    public static final int LIST_ITEM_TYPE_X_COUNT = 29;
    public static final String MANAGER = "manager";
    public static final long MARKET_REFRESH_TIME = 300000;
    private static final int MIN_RANGE_LENGTH = 10;
    public static final String MODE = "mode";
    public static final String NAVIGATION_ALLOWED = "navigationAllowed";
    public static final String NAVIGATION_DIALOG_CONTINUATION = "navigationDialogContinuation";
    public static final String NAVIGATION_INTENT = "navigationIntent";
    public static final String NEWS_FEED = "newsFeed";
    public static final long NEWS_FEED_REFRESH_TIME = 30000;
    public static final String NEWS_NUMBER_UNREAD_ITEMS = "newsNumberUnreadItems";
    public static final String NUMBER_ACHIEVEMENTS = "numberAchievements";
    public static final String NUMBER_AWARDS = "numberAwards";
    public static final String NUMBER_BUDDIES = "numberBuddies";
    public static final String NUMBER_CHALLENGES_PLAYED = "numberChallengesPlayed";
    public static final String NUMBER_CHALLENGES_WON = "numberChallengesWon";
    public static final String NUMBER_GAMES = "numberGames";
    public static final String NUMBER_GLOBAL_ACHIEVEMENTS = "numberGlobalAchievements";
    public static final String SEARCH_LIST = "searchList";
    public static final String SESSION_GAME_VALUES = "sessionGameValues";
    public static final String SESSION_USER_VALUES = "sessionUserValues";
    public static final String TRACKER = "tracker";
    public static final String USER = "user";
    public static final String USER_BALANCE = "userBalance";
    public static final String USER_BUDDIES = "userBuddies";
    public static final String USER_IMAGE_URL = "userImageUrl";
    public static final String USER_NAME = "userName";
    public static final String USER_PLAYS_SESSION_GAME = "userPlaysSessionGame";
    public static final String USER_VALUES = "userValues";

    public static void setup() {
        BaseListAdapter.setViewTypeCount(29);
    }

    public static int getOptimalRangeLength(View listView, BaseListItem item) {
        View itemView = item.getView(null, null);
        itemView.measure(0, 0);
        int listHeight = listView.getHeight();
        if (listHeight == 0) {
            listHeight = ((WindowManager) listView.getContext().getSystemService("window")).getDefaultDisplay().getHeight();
        }
        return Math.max(10, (listHeight / itemView.getMeasuredHeight()) + 1);
    }
}
