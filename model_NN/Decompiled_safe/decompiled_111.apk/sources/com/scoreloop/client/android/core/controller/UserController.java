package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.PublishedFor__2_1_0;
import com.scoreloop.client.android.core.model.BaseEntity;
import com.scoreloop.client.android.core.model.Entity;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.ImageSource;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.util.JSONUtils;
import com.scoreloop.client.android.core.util.SetterIntent;
import com.scoreloop.client.android.ui.component.base.TrackerEvents;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UserController extends RequestController {
    private User c;

    private static class a extends g {
        public static Object a = a.class;

        public a(RequestCompletionCallback requestCompletionCallback, Game game, User user) {
            super(requestCompletionCallback, game, user);
            a(a);
        }

        public String a() {
            if (this.e == null || this.e.getIdentifier() == null) {
                return String.format("/service/users/%s/buddies", this.f.getIdentifier());
            }
            return String.format("/service/users/%s/buddies", this.f.getIdentifier());
        }

        public JSONObject b() {
            return null;
        }
    }

    private static class b extends f {
        public static Object a = b.class;

        public b(RequestCompletionCallback requestCompletionCallback, User user, User user2) {
            super(requestCompletionCallback, null, user, user2);
            a(a);
        }

        public String a() {
            return String.format("/service/users/%s/buddies", this.c.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("buddy_id", this.f.getIdentifier());
                jSONObject.put("buddyhood", jSONObject2);
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException();
            }
        }

        public RequestMethod c() {
            return RequestMethod.POST;
        }
    }

    private static class c extends f {
        public static Object a = c.class;

        public c(RequestCompletionCallback requestCompletionCallback, User user, User user2) {
            super(requestCompletionCallback, null, user, user2);
            a(a);
        }

        public String a() {
            return String.format("/service/users/%s/buddies/%s", this.c.getIdentifier(), this.f.getIdentifier());
        }

        public JSONObject b() {
            return null;
        }

        public RequestMethod c() {
            return RequestMethod.DELETE;
        }
    }

    private static class d extends g {
        public static Object a = d.class;
        private final Game b;

        public d(RequestCompletionCallback requestCompletionCallback, User user, Game game) {
            super(requestCompletionCallback, game, user);
            this.b = game;
            a(a);
        }

        public String a() {
            return String.format("/service/games/%s/users/%s/context", this.b.getIdentifier(), this.f.getIdentifier());
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    private static class e extends g {
        public static Object a = e.class;
        private final Game b;

        public e(RequestCompletionCallback requestCompletionCallback, User user, Game game) {
            super(requestCompletionCallback, game, user);
            this.b = game;
            a(a);
        }

        public String a() {
            return String.format("/service/games/%s/users/%s/context", this.b.getIdentifier(), this.f.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                if (this.f != null) {
                    jSONObject.put("context", JSONUtils.a(this.f.getContext()));
                    jSONObject.put("version", this.f.h());
                }
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid user data");
            }
        }

        public RequestMethod c() {
            return RequestMethod.POST;
        }
    }

    private static class f extends g {
        public static Object b = f.class;
        protected User c;

        public f(RequestCompletionCallback requestCompletionCallback, Game game, User user, User user2) {
            super(requestCompletionCallback, game, user);
            this.c = user2;
            a(b);
        }

        public String a() {
            if (this.e == null || this.e.getIdentifier() == null) {
                return String.format("/service/users/%s/detail", this.f.getIdentifier());
            }
            return String.format("/service/games/%s/users/%s/detail", this.e.getIdentifier(), this.f.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                if (this.c != null) {
                    jSONObject.put("reference_user_id", this.c.getIdentifier());
                }
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid user data");
            }
        }
    }

    private static class g extends Request {
        public static Object d = g.class;
        protected Game e;
        protected User f;

        public g(RequestCompletionCallback requestCompletionCallback, Game game, User user) {
            super(requestCompletionCallback);
            this.f = user;
            this.e = game;
            a(d);
        }

        public String a() {
            if (this.e == null || this.e.getIdentifier() == null) {
                return String.format("/service/users/%s", this.f.getIdentifier());
            }
            return String.format("/service/games/%s/users/%s", this.e.getIdentifier(), this.f.getIdentifier());
        }

        public JSONObject b() {
            return null;
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    private static class h extends g {
        public static Object a = h.class;
        private final String b;
        private final ImageSource c;
        private final String h;

        public h(RequestCompletionCallback requestCompletionCallback, Game game, User user) {
            super(requestCompletionCallback, game, user);
            this.h = user.getLogin();
            this.b = user.getEmailAddress();
            this.c = user.getImageSource();
            a(a);
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                this.f.setLogin(this.h);
                this.f.setEmailAddress(this.b);
                this.f.setImageSource(this.c);
                jSONObject.put(User.a, this.f.d());
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid user data", e);
            }
        }

        public RequestMethod c() {
            return RequestMethod.PUT;
        }
    }

    @PublishedFor__2_1_0
    public UserController(RequestControllerObserver requestControllerObserver) {
        this((Session) null, requestControllerObserver);
    }

    @PublishedFor__1_0_0
    @Deprecated
    public UserController(UserControllerObserver userControllerObserver) {
        this((Session) null, userControllerObserver);
    }

    @PublishedFor__2_1_0
    public UserController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
    }

    @PublishedFor__1_0_0
    @Deprecated
    public UserController(Session session, UserControllerObserver userControllerObserver) {
        super(session, userControllerObserver);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        Object l = request.l();
        if (l == g.d || l == a.a || l == d.a || l == f.b) {
            return d(request, response);
        }
        if (l == b.a) {
            return b(request, response);
        }
        if (l == c.a) {
            return c(request, response);
        }
        if (l == h.a) {
            return e(request, response);
        }
        if (l == e.a) {
            return e(request, response);
        }
        throw new IllegalStateException("response of unknown request");
    }

    @PublishedFor__1_0_0
    public void addAsBuddy() {
        User user = getUser();
        User i = i();
        if (h().isOwnedByUser(user)) {
            b(new Exception("User you're trying to add as buddy is a current session user"));
            return;
        }
        b bVar = new b(g(), user, i);
        a_();
        b(bVar);
    }

    /* access modifiers changed from: package-private */
    public boolean b(Request request, Response response) throws Exception {
        switch (response.f()) {
            case 200:
            case 201:
                break;
            case 422:
                throw RequestControllerException.a(response);
            default:
                throw new Exception("Request failed");
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean c(Request request, Response response) throws Exception {
        int f2 = response.f();
        JSONObject e2 = response.e();
        SetterIntent setterIntent = new SetterIntent();
        switch (f2) {
            case 200:
            case 201:
                break;
            case 422:
                throw RequestControllerException.a(response);
            default:
                throw new Exception("Request failed");
        }
        if (setterIntent.h(e2, "buddy_id", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            return true;
        }
        throw new Exception("Request failed");
    }

    /* access modifiers changed from: package-private */
    public boolean d(Request request, Response response) throws Exception {
        int f2 = response.f();
        JSONObject e2 = response.e();
        SetterIntent setterIntent = new SetterIntent();
        switch (f2) {
            case 200:
            case 201:
                if ((request.l() == d.a || setterIntent.h(e2, User.a, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) && (request.l() != d.a || setterIntent.h(e2, "context", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE))) {
                    User user = getUser();
                    if (setterIntent.f(e2, User.a, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
                        user.a((JSONObject) setterIntent.a());
                    }
                    if (!setterIntent.f(e2, "context", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
                        return true;
                    }
                    user.setContext(JSONUtils.a((JSONObject) setterIntent.a()));
                    user.a(setterIntent.c(e2, "version", SetterIntent.KeyMode.USE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_NULL_VALUE));
                    return true;
                }
                throw new Exception("Request failed");
            case 422:
                throw RequestControllerException.a(response);
            default:
                throw new Exception("Request failed");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.core.model.User.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.scoreloop.client.android.core.model.User.a(com.scoreloop.client.android.core.model.Session, java.lang.String):com.scoreloop.client.android.core.model.Entity
      com.scoreloop.client.android.core.model.User.a(org.json.JSONObject, java.lang.String):void
      com.scoreloop.client.android.core.model.User.a(org.json.JSONObject, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean e(Request request, Response response) throws Exception {
        int f2 = response.f();
        JSONObject e2 = response.e();
        RequestControllerObserver f3 = f();
        HashMap hashMap = new HashMap();
        SetterIntent setterIntent = new SetterIntent();
        User user = getUser();
        if (setterIntent.f(e2, User.a, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            user.a((JSONObject) setterIntent.a(), true);
        }
        if (setterIntent.f(e2, "context", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            hashMap.put("oldUserContext", new HashMap(user.getContext()));
            user.setContext(JSONUtils.a((JSONObject) setterIntent.a()));
            user.a(setterIntent.c(e2, "version", SetterIntent.KeyMode.USE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_NULL_VALUE));
            hashMap.put("newUserContext", new HashMap(user.getContext()));
        }
        switch (f2) {
            case 200:
            case 201:
                if ((request.l() == e.a || setterIntent.h(e2, User.a, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) && (request.l() != e.a || setterIntent.h(e2, "context", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE))) {
                    return true;
                }
                throw new Exception("Request failed");
            case 409:
                if (setterIntent.f(e2, "context", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
                    RequestControllerException requestControllerException = new RequestControllerException();
                    requestControllerException.a(hashMap);
                    throw requestControllerException;
                }
                throw new Exception("Request failed");
            case 422:
                JSONObject jSONObject = e2.getJSONObject(TrackerEvents.LABEL_ERROR);
                Integer a2 = setterIntent.a(jSONObject, "details", SetterIntent.KeyMode.COERCE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_AND_COERCES_NULL_VALUE);
                JSONArray jSONArray = jSONObject.get("args") instanceof JSONArray ? jSONObject.getJSONArray("args") : null;
                int length = jSONArray == null ? 0 : jSONArray.length();
                RequestControllerException requestControllerException2 = new RequestControllerException();
                if (a2.intValue() != 0) {
                    if ((a2.intValue() & 1) != 0) {
                        requestControllerException2.a(16);
                    }
                    if ((a2.intValue() & 2) != 0) {
                        requestControllerException2.a(8);
                    }
                    if ((a2.intValue() & 4) != 0) {
                        requestControllerException2.a(4);
                    }
                    if ((a2.intValue() & 8) != 0) {
                        requestControllerException2.a(1);
                    }
                    if ((a2.intValue() & 16) != 0) {
                        requestControllerException2.a(2);
                    }
                    if ((a2.intValue() & 32) != 0) {
                        requestControllerException2.a(32);
                    }
                    if ((a2.intValue() & 64) != 0) {
                        requestControllerException2.a(64);
                    }
                }
                String str = "";
                for (int i = 0; i < length; i++) {
                    JSONArray jSONArray2 = jSONArray.getJSONArray(i);
                    if (jSONArray2.getString(1) != null) {
                        str = str + ":" + jSONArray2.getString(1);
                    }
                }
                requestControllerException2.a(str);
                if (!(f3 instanceof UserControllerObserver)) {
                    f3.requestControllerDidFail(this, requestControllerException2);
                } else if (requestControllerException2.hasDetail(16)) {
                    ((UserControllerObserver) f3).userControllerDidFailOnEmailAlreadyTaken(this);
                } else if (requestControllerException2.hasDetail(8)) {
                    ((UserControllerObserver) f3).userControllerDidFailOnInvalidEmailFormat(this);
                } else if (requestControllerException2.a()) {
                    ((UserControllerObserver) f3).userControllerDidFailOnUsernameAlreadyTaken(this);
                } else {
                    f3.requestControllerDidFail(this, requestControllerException2);
                }
                return false;
            default:
                throw new Exception("Request failed");
        }
    }

    @PublishedFor__1_0_0
    public User getUser() {
        if (this.c == null) {
            this.c = super.i();
            if (this.c == null) {
                throw new IllegalStateException("No session user assigned");
            }
        }
        return this.c;
    }

    @PublishedFor__1_0_0
    public void loadBuddies() {
        a aVar = new a(g(), getGame(), getUser());
        a_();
        aVar.a(120000);
        b(aVar);
    }

    @PublishedFor__1_0_0
    public void loadUser() {
        g gVar = new g(g(), getGame(), getUser());
        a_();
        gVar.a(120000);
        b(gVar);
    }

    @PublishedFor__1_0_0
    public void loadUserContext() {
        if (getGame() == null) {
            throw new IllegalStateException("user context is being held within a null game, therefore it's kinda hard to retrieve");
        }
        d dVar = new d(g(), getUser(), getGame());
        a_();
        dVar.a(120000);
        b(dVar);
    }

    @PublishedFor__1_0_0
    public void loadUserDetail() {
        User user = getUser();
        User i = i();
        if (h().isOwnedByUser(user)) {
            i = null;
        }
        f fVar = new f(g(), getGame(), user, i);
        a_();
        fVar.a(120000);
        b(fVar);
    }

    @PublishedFor__1_0_0
    public void removeAsBuddy() {
        c cVar = new c(g(), getUser(), i());
        a_();
        b(cVar);
    }

    @PublishedFor__1_0_0
    public void setUser(Entity entity) {
        if (entity == null) {
            this.c = null;
        } else if (!entity.a().equalsIgnoreCase(User.a)) {
            throw new IllegalArgumentException("entity must be of type user");
        } else if (!(entity instanceof BaseEntity)) {
            throw new IllegalArgumentException("currently Entity objects must be subclasses of BaseEntity");
        } else {
            this.c = (User) entity;
        }
    }

    @PublishedFor__1_0_0
    public void submitUser() {
        h hVar = new h(g(), getGame(), getUser());
        a_();
        b(hVar);
    }

    @PublishedFor__1_0_0
    public void submitUserContext() {
        if (getGame() == null) {
            throw new IllegalStateException("trying to put user context into a null game...");
        }
        e eVar = new e(g(), getUser(), getGame());
        a_();
        b(eVar);
    }
}
