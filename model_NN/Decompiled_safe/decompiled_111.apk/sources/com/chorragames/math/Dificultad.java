package com.chorragames.math;

public class Dificultad {
    private int mMaximo;
    private int mMultiplicaciones;
    private boolean mNegativos;
    private int mNumero;
    private int mRestas;
    private int mSumas;
    private int mTiempo;

    public void setNegativos(boolean mNegativos2) {
        this.mNegativos = mNegativos2;
    }

    public boolean isNegativos() {
        return this.mNegativos;
    }

    public void setSumas(int mSumas2) {
        this.mSumas = mSumas2;
    }

    public int getSumas() {
        return this.mSumas;
    }

    public void setRestas(int mRestas2) {
        this.mRestas = mRestas2;
    }

    public int getRestas() {
        return this.mRestas;
    }

    public void setNumero(int mNumero2) {
        this.mNumero = mNumero2;
    }

    public int getNumero() {
        return this.mNumero;
    }

    public void setMaximo(int mMaximo2) {
        this.mMaximo = mMaximo2;
    }

    public int getMaximo() {
        return this.mMaximo;
    }

    public void setMultiplicaciones(int mMultiplicaciones2) {
        this.mMultiplicaciones = mMultiplicaciones2;
    }

    public int getMultiplicaciones() {
        return this.mMultiplicaciones;
    }

    public void setTiempo(int mTiempo2) {
        this.mTiempo = mTiempo2;
    }

    public int getTiempo() {
        return this.mTiempo;
    }
}
