package com.chorragames.math;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.chorragames.framework.BaseButton;
import com.chorragames.framework.GameActivityBase;
import com.chorragames.framework.GameScene;
import com.chorragames.framework.geom.Box;
import com.chorragames.math.SceneFactory;
import com.scoreloop.client.android.ui.EntryScreenActivity;
import com.scoreloop.client.android.ui.component.base.TrackerEvents;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.buildable.builder.BlackPawnTextureBuilder;
import org.anddev.andengine.opengl.texture.buildable.builder.ITextureBuilder;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.util.Debug;

public class MenuScene extends GameScene {
    private static final int[] LAYOUT_BOXES = {2, 1, 1, 1, 1, 1};
    private static final int[] LAYOUT_BOXES_H = {1, 1, 1};
    private static final String TAG = "MenuScene";
    private TextureRegion mAndEngine;
    private TextureRegion mBotonActivo;
    private TextureRegion mBotonInactivo;
    private Box[] mBoxes;
    private Box[] mBoxesH;
    private TextureRegion mLogoJuego;
    private TextureRegion mScoreLoop;
    private TextureRegion mTextoJuego;
    private BuildableBitmapTextureAtlas mTexture;

    public MenuScene(Main gab, int screenwidth, int screenheight, String name) {
        super(gab, screenwidth, screenheight, name);
    }

    public void onActivate(GameActivityBase gab) {
    }

    public void onCreateOptionsMenu(Menu pMenu) {
    }

    public void onDeactivate() {
    }

    public void onDestroy() {
        freeBoxes(this.mBoxes);
        freeBoxes(this.mBoxesH);
        this.aBase.getEngine().getTextureManager().unloadTexture(this.mTexture);
    }

    public void onLoadAudio(GameActivityBase gab) {
    }

    public boolean onMenuItemSelected(int pId, MenuItem pItem) {
        return false;
    }

    public void onPrepareOptionsMenu(Menu pMenu) {
    }

    public void onReset() {
    }

    public void onGameUpdate(float fTime) {
    }

    /* access modifiers changed from: protected */
    public void LoadLayout(GameActivityBase gab) {
        this.mBoxes = initBoxes(LAYOUT_BOXES);
        this.mBoxesH = initBoxesIn(this.mBoxes[4], LAYOUT_BOXES_H);
    }

    public void onLoadSprites(GameActivityBase gab) {
        Sprite sp = new Sprite(0.0f, 0.0f, this.mBoxes[0].getHeight(), this.mBoxes[0].getHeight(), this.mLogoJuego);
        getChild(1).attachChild(sp);
        Sprite sp2 = new Sprite(0.0f, 0.0f, ((float) Main.CAMERA_WIDTH) - sp.getWidth(), this.mBoxes[0].getHeight(), this.mTextoJuego);
        sp2.setPosition(((float) Main.CAMERA_WIDTH) - sp2.getWidth(), 0.0f);
        getChild(1).attachChild(sp2);
        BaseButton bb = new BaseButton(this.mBoxes[1], this.mBotonActivo, this.mBotonInactivo, ((Main) gab).getFont(), gab.getString(R.string.menu_easy)) {
            public void onClick() {
                SceneFactory.getInstance((Main) MenuScene.this.aBase).cambiaEscena(SceneFactory.Stages.INSTRUCCIONES, 1);
            }
        };
        getChild(1).attachChild(bb);
        registerTouchArea(bb);
        BaseButton bb2 = new BaseButton(this.mBoxes[2], this.mBotonActivo, this.mBotonInactivo, ((Main) gab).getFont(), gab.getString(R.string.menu_medium)) {
            public void onClick() {
                SceneFactory.getInstance((Main) MenuScene.this.aBase).cambiaEscena(SceneFactory.Stages.INSTRUCCIONES, 2);
            }
        };
        getChild(1).attachChild(bb2);
        registerTouchArea(bb2);
        BaseButton bb3 = new BaseButton(this.mBoxes[3], this.mBotonActivo, this.mBotonInactivo, ((Main) gab).getFont(), gab.getString(R.string.menu_crazy)) {
            public void onClick() {
                SceneFactory.getInstance((Main) MenuScene.this.aBase).cambiaEscena(SceneFactory.Stages.INSTRUCCIONES, 3);
            }
        };
        getChild(1).attachChild(bb3);
        registerTouchArea(bb3);
        BaseButton bb4 = new BaseButton(this.mBoxes[4], this.mBotonActivo, this.mBotonInactivo, ((Main) gab).getFont(), gab.getString(R.string.game_online)) {
            public void onClick() {
                MenuScene.this.aBase.startActivity(new Intent(MenuScene.this.aBase, EntryScreenActivity.class));
            }
        };
        getChild(1).attachChild(bb4);
        registerTouchArea(bb4);
        Sprite sc = new Sprite(bb4.getX(), this.mBoxes[5].getY(), this.mBoxes[5].getHeight(), this.mBoxes[5].getHeight(), this.mAndEngine);
        getChild(1).attachChild(sc);
        Text tx = new Text(sc.getWidth() + sc.getX(), sc.getY(), ((Main) this.aBase).getFontB(), getSoftwareVersion());
        tx.setPosition(tx.getX(), (sc.getY() + sc.getHeight()) - (tx.getHeight() * 2.0f));
        getChild(1).attachChild(tx);
        getChild(1).attachChild(new Sprite((bb4.getX() + bb4.getWidth()) - this.mBoxes[5].getHeight(), this.mBoxes[5].getY(), this.mBoxes[5].getHeight(), this.mBoxes[5].getHeight(), this.mScoreLoop));
        setTouchAreaBindingEnabled(true);
    }

    private String getSoftwareVersion() {
        try {
            return "V " + this.aBase.getPackageManager().getPackageInfo(this.aBase.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Package name not found", e);
            return TrackerEvents.LABEL_ERROR;
        }
    }

    public void onLoadTextures(GameActivityBase gab) {
        this.mTexture = new BuildableBitmapTextureAtlas(PVRTexture.FLAG_BUMPMAP, PVRTexture.FLAG_BUMPMAP);
        this.mLogoJuego = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/logo.png");
        this.mBotonActivo = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/button.png");
        this.mBotonInactivo = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/button2.png");
        this.mTextoJuego = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/text.png");
        this.mAndEngine = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/andengine.png");
        this.mScoreLoop = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/scoreloop.png");
        try {
            this.mTexture.build(new BlackPawnTextureBuilder(1));
        } catch (ITextureBuilder.TextureAtlasSourcePackingException e) {
            Debug.e(TAG, e);
        }
        gab.getEngine().getTextureManager().loadTexture(this.mTexture);
    }

    public void goBack() {
        this.aBase.finish();
    }

    public void onLoadGameScene(GameActivityBase gab) {
    }

    public void onPause() {
    }

    public void onUnPause() {
    }
}
