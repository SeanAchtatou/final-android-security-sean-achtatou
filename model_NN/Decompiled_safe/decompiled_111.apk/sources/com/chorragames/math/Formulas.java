package com.chorragames.math;

import java.util.Random;

public class Formulas {
    private static Random generator = new Random();

    static final String creaSuma(int pValor) {
        if (pValor == 1) {
            return Integer.toString(pValor);
        }
        if (pValor == -1) {
            return Integer.toString(pValor);
        }
        if (pValor > 0) {
            int operando1 = generator.nextInt(pValor - 1) + 1;
            return String.valueOf(operando1) + " + " + (pValor - operando1);
        }
        int operando12 = generator.nextInt(Math.abs(pValor) - 1) + 1;
        return String.valueOf(Integer.toString(operando12)) + (pValor - operando12);
    }

    static final String creaResta(int pValor) {
        if (pValor > 0) {
            int operando1 = generator.nextInt(pValor) + 1;
            if (operando1 == pValor) {
                operando1 = pValor + 2;
            }
            if (operando1 <= pValor) {
                return String.valueOf(pValor + operando1) + " - " + operando1;
            }
            return String.valueOf(operando1) + " - " + (operando1 - pValor);
        }
        int valor = Math.abs(pValor);
        int operando12 = generator.nextInt(valor) + 1;
        return String.valueOf(operando12) + " - " + (operando12 + valor);
    }
}
