package com.chorragames.math;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import org.anddev.andengine.entity.primitive.Line;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class Bola extends Sprite {
    private static final FixtureDef FIXTURE_DEF = PhysicsFactory.createFixtureDef(1.0f, 0.5f, 0.5f);
    private static final String NUMEROS = "1234567";
    private static final String TAG = "Bola";
    private boolean mActivated = false;
    private boolean mActive = false;
    private Body mBody = null;
    private PhysicsConnector mConector = null;
    private Line mLinea;
    private IBola mListener = null;
    private PhysicsWorld mPhysicsWorld = null;
    private ChangeableText mTexto;
    private int mValor;

    public interface IBola {
        void pulsaBola(Bola bola);
    }

    public Bola(int pSize, TextureRegion pTextureRegion, Font pFuente) {
        super(100.0f, 100.0f, (float) pSize, (float) pSize, pTextureRegion);
        this.mTexto = new ChangeableText(0.0f, 0.0f, pFuente, NUMEROS);
        this.mTexto.setPosition(0.0f, (getHeight() - this.mTexto.getHeight()) / 2.0f);
        attachChild(this.mTexto);
        float line_length = getWidth() * 0.5f;
        float pX1 = (getWidth() - line_length) / 2.0f;
        float pY1 = (this.mTexto.getHeight() * 1.2f) + this.mTexto.getY();
        this.mLinea = new Line(pX1, pY1, pX1 + line_length, pY1, 10.0f);
        this.mLinea.setColor(0.3f, 0.3f, 0.3f);
        attachChild(this.mLinea);
        this.mActivated = false;
    }

    public void setPosicion(float x, float y) {
        this.mBody.setTransform(x, y, 0.0f);
    }

    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        if (!this.mActive) {
            return false;
        }
        switch (pSceneTouchEvent.getAction()) {
            case 0:
                if (this.mListener == null) {
                    return false;
                }
                this.mListener.pulsaBola(this);
                return true;
            default:
                return false;
        }
    }

    public void setTexto(String pTexto) {
        this.mTexto.setText(pTexto);
        this.mTexto.setPosition((getWidth() - this.mTexto.getWidth()) / 2.0f, this.mTexto.getY());
    }

    public void setValor(int mValor2) {
        this.mValor = mValor2;
    }

    public int getValor() {
        return this.mValor;
    }

    public void activate(int pValor, String pText, PhysicsWorld pPhysicsWorld, IBola pListener) {
        if (!this.mActive) {
            setValor(pValor);
            setTexto(pText);
            this.mListener = pListener;
            if (!this.mActivated) {
                this.mPhysicsWorld = pPhysicsWorld;
                this.mBody = PhysicsFactory.createCircleBody(this.mPhysicsWorld, this, BodyDef.BodyType.DynamicBody, FIXTURE_DEF);
                this.mConector = new PhysicsConnector(this, this.mBody, true, true);
                this.mPhysicsWorld.registerPhysicsConnector(this.mConector);
            }
            this.mBody.setActive(true);
            this.mBody.setAwake(true);
            this.mActive = true;
            cshow();
        }
    }

    public void cshow() {
        setIgnoreUpdate(false);
        setVisible(true);
    }

    public void deactivate() {
        if (this.mActive) {
            this.mActive = false;
            this.mBody.setActive(false);
            this.mBody.setAwake(false);
            this.mActivated = true;
            chide();
        }
    }

    public void chide() {
        setIgnoreUpdate(true);
        setVisible(false);
    }

    public boolean isActive() {
        return this.mActive;
    }

    public void move(int velocityX, int velocityY) {
        this.mBody.setLinearVelocity((float) velocityX, (float) velocityY);
    }

    public boolean isActivated() {
        return this.mActivated;
    }
}
