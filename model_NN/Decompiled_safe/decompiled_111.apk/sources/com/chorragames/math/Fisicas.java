package com.chorragames.math;

import com.badlogic.gdx.math.Vector2;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;

public class Fisicas extends PhysicsWorld {
    private boolean mPaused = false;

    public Fisicas(Vector2 pGravity, boolean pAllowSleep) {
        super(pGravity, pAllowSleep);
    }

    public void onUpdate(float pSecondsElapsed) {
        if (!this.mPaused) {
            super.onUpdate(pSecondsElapsed);
        }
    }

    public void setPaused(boolean mPaused2) {
        this.mPaused = mPaused2;
    }

    public boolean isPaused() {
        return this.mPaused;
    }
}
