package com.chorragames.math;

import android.view.Menu;
import android.view.MenuItem;
import com.chorragames.framework.GameActivityBase;
import com.chorragames.framework.GameScene;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class SplashScene extends GameScene {
    private TextureRegion mFondoS;
    private BitmapTextureAtlas mTexture;

    public SplashScene(Main gab, int screenwidth, int screenheight, String name) {
        super(gab, 3, screenwidth, screenheight, name, 0, false);
    }

    public void goBack() {
    }

    public void onActivate(GameActivityBase gab) {
    }

    public void onCreateOptionsMenu(Menu pMenu) {
    }

    public void onDeactivate() {
    }

    public void onDestroy() {
        this.aBase.getEngine().getTextureManager().unloadTexture(this.mTexture);
    }

    public void onGameUpdate(float fTime) {
    }

    public void onLoadAudio(GameActivityBase gab) {
    }

    public void onLoadGameScene(GameActivityBase gab) {
    }

    public void onLoadSprites(GameActivityBase gab) {
        getChild(1).attachChild(new Sprite(0.0f, 0.0f, (float) Main.CAMERA_WIDTH, (float) Main.CAMERA_HEIGHT, this.mFondoS));
    }

    public void onLoadTextures(GameActivityBase gab) {
        this.mTexture = new BitmapTextureAtlas(PVRTexture.FLAG_TWIDDLE, PVRTexture.FLAG_BUMPMAP);
        this.mFondoS = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/splash.png", 0, 0);
        gab.getEngine().getTextureManager().loadTexture(this.mTexture);
    }

    public boolean onMenuItemSelected(int pId, MenuItem pItem) {
        return false;
    }

    public void onPause() {
    }

    public void onPrepareOptionsMenu(Menu pMenu) {
    }

    public void onReset() {
    }

    public void onUnPause() {
    }
}
