package com.chorragames.math;

import java.util.Random;

public class BallPool {
    private static Random generator = new Random();
    private final Bola[] mBolas;
    private final int mCuantos;
    private Main mMain;

    public BallPool(int nNumber, int pMaxSize, int pMinSize, Main pMain) {
        this.mBolas = new Bola[nNumber];
        this.mMain = pMain;
        allocatePool(pMaxSize, pMinSize);
        this.mCuantos = nNumber;
    }

    public final Bola dameIndex(int index) {
        return this.mBolas[index];
    }

    public final int dameNumeroBolas() {
        return this.mBolas.length;
    }

    private void allocatePool(int pMaxSize, int pMinSize) {
        int mRandom = pMaxSize - pMinSize;
        for (int i = 0; i < this.mBolas.length; i++) {
            this.mBolas[i] = new Bola(pMinSize + generator.nextInt(mRandom), this.mMain.getBola1(), this.mMain.getFontB());
        }
    }

    public Bola dameBola() {
        int i;
        do {
            i = generator.nextInt(this.mBolas.length);
        } while (this.mBolas[i].isActive());
        return this.mBolas[i];
    }

    public void destroy() {
        for (int i = 0; i < this.mBolas.length; i++) {
            this.mBolas[i] = null;
        }
    }
}
