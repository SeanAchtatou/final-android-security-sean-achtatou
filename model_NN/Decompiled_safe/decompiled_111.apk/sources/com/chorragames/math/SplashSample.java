package com.chorragames.math;

import android.app.Activity;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.AssetBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.anddev.andengine.ui.activity.BaseGameActivity;
import org.anddev.andengine.ui.activity.BaseSplashActivity;

public class SplashSample extends BaseSplashActivity {
    private final int SPLASH_DURATION_SEC = 3;
    private final String backgroundImage = "gfx/splash.png";
    private final Class<? extends BaseGameActivity> nextActivity = Main.class;
    private final EngineOptions.ScreenOrientation screenOrientation = EngineOptions.ScreenOrientation.PORTRAIT;

    /* access modifiers changed from: protected */
    public Class<? extends Activity> getFollowUpActivity() {
        return this.nextActivity;
    }

    /* access modifiers changed from: protected */
    public EngineOptions.ScreenOrientation getScreenOrientation() {
        return this.screenOrientation;
    }

    /* access modifiers changed from: protected */
    public float getSplashDuration() {
        return 3.0f;
    }

    /* access modifiers changed from: protected */
    public IBitmapTextureAtlasSource onGetSplashTextureAtlasSource() {
        return new AssetBitmapTextureAtlasSource(this, "gfx/splash.png");
    }
}
