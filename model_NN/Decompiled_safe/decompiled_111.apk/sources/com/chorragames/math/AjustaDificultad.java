package com.chorragames.math;

import com.scoreloop.client.android.ui.component.base.Constant;

public class AjustaDificultad {
    public static final String TAG = "Ajusta";
    private static final int TIEMPO = 90;

    public static void Ajusta(int pDificultad, int pFase, Dificultad pDiff) {
        switch (pDificultad) {
            case 1:
                ajustaFacil(pFase, pDiff);
                return;
            case 2:
                ajustaMedio(pFase, pDiff);
                return;
            case 3:
                ajustaDificil(pFase, pDiff);
                return;
            default:
                return;
        }
    }

    public static int dameEstrellas(int pDificultad, int pAciertos, int pFallos) {
        int resultado = pAciertos - pFallos;
        if (resultado < 10) {
            return 0;
        }
        if (resultado < 40) {
            return 2;
        }
        if (resultado < 60) {
            return 3;
        }
        if (resultado < 80) {
            return 4;
        }
        return 5;
    }

    private static void ajustaFacil(int pFase, Dificultad pDiff) {
        pDiff.setTiempo(TIEMPO);
        switch (pFase) {
            case 0:
            case 1:
                pDiff.setMaximo(10);
                pDiff.setMultiplicaciones(0);
                pDiff.setNegativos(false);
                pDiff.setNumero(4);
                pDiff.setRestas(0);
                pDiff.setSumas(0);
                return;
            case 2:
            case 3:
            case 4:
                pDiff.setMaximo(10);
                pDiff.setMultiplicaciones(0);
                pDiff.setNegativos(false);
                pDiff.setNumero(4);
                pDiff.setRestas(0);
                pDiff.setSumas(1);
                return;
            case 5:
            case 6:
            case 7:
                pDiff.setMaximo(12);
                pDiff.setMultiplicaciones(0);
                pDiff.setNegativos(false);
                pDiff.setNumero(4);
                pDiff.setRestas(0);
                pDiff.setSumas(2);
                return;
            case 8:
            case Constant.LIST_ITEM_TYPE_CHALLENGE_STAKE_AND_MODE /*9*/:
                pDiff.setMaximo(15);
                pDiff.setMultiplicaciones(0);
                pDiff.setNegativos(false);
                pDiff.setNumero(5);
                pDiff.setRestas(1);
                pDiff.setSumas(2);
                return;
            case 10:
            case 11:
                pDiff.setMaximo(15);
                pDiff.setMultiplicaciones(0);
                pDiff.setNegativos(false);
                pDiff.setNumero(5);
                pDiff.setRestas(2);
                pDiff.setSumas(1);
                return;
            case 12:
                pDiff.setMaximo(15);
                pDiff.setMultiplicaciones(0);
                pDiff.setNegativos(false);
                pDiff.setNumero(5);
                pDiff.setRestas(2);
                pDiff.setSumas(2);
                return;
            default:
                return;
        }
    }

    private static void ajustaMedio(int pFase, Dificultad pDiff) {
        pDiff.setTiempo(TIEMPO);
        switch (pFase) {
            case 0:
            case 1:
                pDiff.setMaximo(12);
                pDiff.setMultiplicaciones(0);
                pDiff.setNegativos(false);
                pDiff.setNumero(4);
                pDiff.setRestas(0);
                pDiff.setSumas(1);
                return;
            case 2:
            case 3:
            case 4:
                pDiff.setMaximo(12);
                pDiff.setMultiplicaciones(0);
                pDiff.setNegativos(false);
                pDiff.setNumero(4);
                pDiff.setRestas(1);
                pDiff.setSumas(1);
                return;
            case 5:
            case 6:
            case 7:
                pDiff.setMaximo(15);
                pDiff.setMultiplicaciones(0);
                pDiff.setNegativos(true);
                pDiff.setNumero(5);
                pDiff.setRestas(1);
                pDiff.setSumas(2);
                return;
            case 8:
            case Constant.LIST_ITEM_TYPE_CHALLENGE_STAKE_AND_MODE /*9*/:
                pDiff.setMaximo(20);
                pDiff.setMultiplicaciones(0);
                pDiff.setNegativos(true);
                pDiff.setNumero(5);
                pDiff.setRestas(2);
                pDiff.setSumas(2);
                return;
            case 10:
            case 11:
                pDiff.setMaximo(20);
                pDiff.setMultiplicaciones(0);
                pDiff.setNegativos(true);
                pDiff.setNumero(5);
                pDiff.setRestas(2);
                pDiff.setSumas(1);
                return;
            case 12:
                pDiff.setMaximo(15);
                pDiff.setMultiplicaciones(0);
                pDiff.setNegativos(true);
                pDiff.setNumero(5);
                pDiff.setRestas(2);
                pDiff.setSumas(2);
                return;
            default:
                return;
        }
    }

    private static void ajustaDificil(int pFase, Dificultad pDiff) {
        pDiff.setTiempo(TIEMPO);
        switch (pFase) {
            case 0:
            case 1:
                pDiff.setMaximo(15);
                pDiff.setMultiplicaciones(0);
                pDiff.setNegativos(true);
                pDiff.setNumero(4);
                pDiff.setRestas(1);
                pDiff.setSumas(1);
                return;
            case 2:
            case 3:
            case 4:
                pDiff.setMaximo(15);
                pDiff.setMultiplicaciones(0);
                pDiff.setNegativos(true);
                pDiff.setNumero(4);
                pDiff.setRestas(2);
                pDiff.setSumas(1);
                return;
            case 5:
            case 6:
            case 7:
                pDiff.setMaximo(18);
                pDiff.setMultiplicaciones(0);
                pDiff.setNegativos(false);
                pDiff.setNumero(5);
                pDiff.setRestas(2);
                pDiff.setSumas(2);
                return;
            case 8:
            case Constant.LIST_ITEM_TYPE_CHALLENGE_STAKE_AND_MODE /*9*/:
                pDiff.setMaximo(20);
                pDiff.setMultiplicaciones(0);
                pDiff.setNegativos(false);
                pDiff.setNumero(5);
                pDiff.setRestas(2);
                pDiff.setSumas(2);
                return;
            case 10:
            case 11:
                pDiff.setMaximo(20);
                pDiff.setMultiplicaciones(0);
                pDiff.setNegativos(false);
                pDiff.setNumero(5);
                pDiff.setRestas(2);
                pDiff.setSumas(3);
                return;
            case 12:
                pDiff.setMaximo(25);
                pDiff.setMultiplicaciones(0);
                pDiff.setNegativos(false);
                pDiff.setNumero(5);
                pDiff.setRestas(2);
                pDiff.setSumas(3);
                return;
            default:
                return;
        }
    }
}
