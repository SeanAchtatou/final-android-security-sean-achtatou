package com.chorragames.math;

import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.WindowManager;
import com.chorragames.framework.GameActivityBase;
import com.chorragames.framework.GameScene;
import com.chorragames.framework.ScreenAdvertisement;
import com.chorragames.math.SceneFactory;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.font.FontFactory;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.buildable.builder.BlackPawnTextureBuilder;
import org.anddev.andengine.opengl.texture.buildable.builder.ITextureBuilder;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.util.Debug;

public class Main extends GameActivityBase {
    public static int CAMERA_HEIGHT = 800;
    public static int CAMERA_WIDTH = 480;
    public static final int DIFFICULTY_EASY = 1;
    private static final String TAG = "Main";
    private int TITLE_FONT_SIZE;
    private ScreenAdvertisement mAdvertisement;
    private TextureRegion mBackground;
    private TextureRegion mBola1;
    private Camera mCamera;
    private Engine mEngine;
    private Font mFont;
    private Font mFontB;
    private Font mFontBB;
    private BitmapTextureAtlas mFontTexture;
    private BitmapTextureAtlas mFontTextureB;
    private BitmapTextureAtlas mFontTextureBB;
    private BuildableBitmapTextureAtlas mPublicTexture;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.TITLE_FONT_SIZE = CAMERA_WIDTH / 12;
        ScoreloopManagerSingleton.init(this, "FQ1d3dZuh90LR239l5Kkc7GgAm3R2zJFvYySLERXyhDkrW8tSbhnFw==");
    }

    public void onCreateActivity(Bundle savedInstanceState) {
    }

    /* access modifiers changed from: protected */
    public int getLayoutID() {
        return R.layout.main;
    }

    /* access modifiers changed from: protected */
    public int getRenderSurfaceViewID() {
        return R.id.andengine_surface;
    }

    public void onLoadComplete() {
        this.mAdvertisement = new ScreenAdvertisement(this, R.id.adView);
        showAd();
    }

    /* access modifiers changed from: protected */
    public void showAd() {
        this.mAdvertisement.showAdvertisement();
    }

    /* access modifiers changed from: protected */
    public void hideAd() {
        this.mAdvertisement.hideAdvertisement();
    }

    public Engine onLoadEngine() {
        Display display = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        CAMERA_WIDTH = display.getWidth();
        CAMERA_HEIGHT = display.getHeight();
        this.mCamera = new Camera(0.0f, 0.0f, (float) CAMERA_WIDTH, (float) CAMERA_HEIGHT);
        EngineOptions eo = new EngineOptions(true, EngineOptions.ScreenOrientation.PORTRAIT, new RatioResolutionPolicy((float) CAMERA_WIDTH, (float) CAMERA_HEIGHT), this.mCamera);
        eo.setNeedsMusic(true);
        eo.setNeedsSound(true);
        this.mEngine = new Engine(eo);
        return this.mEngine;
    }

    public void onLoadResources() {
        this.mPublicTexture = new BuildableBitmapTextureAtlas((int) PVRTexture.FLAG_BUMPMAP, (int) PVRTexture.FLAG_BUMPMAP, TextureOptions.BILINEAR);
        this.mBackground = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mPublicTexture, this, "gfx/background.png");
        this.mBola1 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mPublicTexture, this, "gfx/bola.png");
        this.mFontTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_TWIDDLE, (int) PVRTexture.FLAG_TWIDDLE, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFontTextureB = new BitmapTextureAtlas((int) PVRTexture.FLAG_TWIDDLE, (int) PVRTexture.FLAG_TWIDDLE, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFontTextureBB = new BitmapTextureAtlas((int) PVRTexture.FLAG_BUMPMAP, (int) PVRTexture.FLAG_BUMPMAP, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFont = FontFactory.createFromAsset(this.mFontTexture, this, "font/fuente.ttf", (float) this.TITLE_FONT_SIZE, true, -1);
        this.mFontB = FontFactory.createFromAsset(this.mFontTextureB, this, "font/fuente.ttf", (float) this.TITLE_FONT_SIZE, true, -16777216);
        this.mFontBB = FontFactory.createFromAsset(this.mFontTextureBB, this, "font/fuente.ttf", (float) (this.TITLE_FONT_SIZE * 2), true, -16777216);
        try {
            this.mPublicTexture.build(new BlackPawnTextureBuilder(1));
        } catch (ITextureBuilder.TextureAtlasSourcePackingException e) {
            Debug.e(TAG, e);
        }
        this.mEngine.getTextureManager().loadTextures(this.mFontTexture, this.mFontTextureB, this.mFontTextureBB);
        this.mEngine.getFontManager().loadFonts(this.mFont, this.mFontB, this.mFontBB);
        this.mEngine.getTextureManager().loadTexture(this.mPublicTexture);
    }

    public Scene onLoadScene() {
        this.mEngine.registerUpdateHandler(new FPSLogger());
        SceneFactory.getInstance(this).inicializa();
        GameScene escena = SceneFactory.getInstance(this).dameEscena(SceneFactory.Stages.MENU, 0);
        escena.activate(this);
        return escena;
    }

    /* access modifiers changed from: protected */
    public void inicializa() {
        SceneFactory.getInstance(this).inicializa();
    }

    public TextureRegion getBackground() {
        return this.mBackground;
    }

    public Font getFont() {
        return this.mFont;
    }

    public Font getFontB() {
        return this.mFontB;
    }

    public Font getFontBB() {
        return this.mFontBB;
    }

    public TextureRegion getBola1() {
        return this.mBola1;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        boolean z = getEngine().getScene() instanceof GameScene;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        ScoreloopManagerSingleton.destroy();
        SceneFactory.getInstance(this).cambiaEscena(SceneFactory.Stages.SPLASH, 0);
        SceneFactory.clearFactory();
        super.onDestroy();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getAction() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        ((GameScene) this.mEngine.getScene()).goBack();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Scene sc = getEngine().getScene();
        if (sc instanceof BasePuzzle) {
            ((BasePuzzle) sc).onPause();
        }
    }

    public void resumeGame() {
        Scene sc = getEngine().getScene();
        if (sc instanceof BasePuzzle) {
            ((BasePuzzle) sc).onUnPause();
        }
    }
}
