package com.chorragames.math;

import android.view.Menu;
import android.view.MenuItem;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.chorragames.framework.GameActivityBase;
import com.chorragames.framework.GameScene;
import com.chorragames.framework.geom.Box;
import com.chorragames.framework.geom.BoxFactory;
import com.chorragames.framework.geom.Marcador;
import com.chorragames.math.Bola;
import com.chorragames.math.SceneFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.entity.modifier.AlphaModifier;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.buildable.builder.BlackPawnTextureBuilder;
import org.anddev.andengine.opengl.texture.buildable.builder.ITextureBuilder;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.util.Debug;

public class BasePuzzle extends GameScene implements Bola.IBola {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$chorragames$math$BasePuzzle$Estado = null;
    private static final String TAG = "Puzzle";
    private static Random generator = new Random();
    /* access modifiers changed from: private */
    public Marcador mBBien;
    /* access modifiers changed from: private */
    public Marcador mBMal;
    /* access modifiers changed from: private */
    public Marcador mBTiempo;
    private BallPool mBolas = null;
    private Box[] mBoxes;
    private Box[] mBoxesBolas;
    private Dialogo mDialogo;
    private Dificultad mDificultad = new Dificultad();
    private TextureRegion mDos;
    private Estado mEstado;
    private TextureRegion mFail;
    private int mFase = -1;
    private final int mLevel;
    private final List<Integer> mLista = new ArrayList();
    private TextureRegion mNota;
    private int mNumero = 0;
    private Estado mOldEstado;
    private Fisicas mPhysicsWorld;
    private float mSegundosAtras;
    private Sound mSonidoBeep;
    private Sound mSonidoDing;
    private Sound mSonidoFallo;
    private BuildableBitmapTextureAtlas mTexture;
    private float mTiempoAtras;
    private TextureRegion mTime;
    private TextureRegion mTres;
    private TextureRegion mUno;
    /* access modifiers changed from: private */
    public Sprite msDos;
    /* access modifiers changed from: private */
    public Sprite msTres;
    /* access modifiers changed from: private */
    public Sprite msUno;
    /* access modifiers changed from: private */
    public int nAciertos = 0;
    /* access modifiers changed from: private */
    public int nFallos = 0;
    private float nTime;

    private enum Estado {
        PAUSA,
        JUGANDO,
        CREANDO_BOLAS,
        FIN,
        CUENTA_INICIO,
        UNDEFINED
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$chorragames$math$BasePuzzle$Estado() {
        int[] iArr = $SWITCH_TABLE$com$chorragames$math$BasePuzzle$Estado;
        if (iArr == null) {
            iArr = new int[Estado.values().length];
            try {
                iArr[Estado.CREANDO_BOLAS.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Estado.CUENTA_INICIO.ordinal()] = 5;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Estado.FIN.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[Estado.JUGANDO.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[Estado.PAUSA.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[Estado.UNDEFINED.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            $SWITCH_TABLE$com$chorragames$math$BasePuzzle$Estado = iArr;
        }
        return iArr;
    }

    public BasePuzzle(Main gab, int screenwidth, int screenheight, String name, int pDif) {
        super(gab, 3, screenwidth, screenheight, name, pDif);
        this.mLevel = pDif;
        AjustaDificultad.Ajusta(this.mLevel, this.mFase, this.mDificultad);
        this.nTime = (float) this.mDificultad.getTiempo();
        this.mDificultad.getNumero();
        this.mEstado = Estado.CUENTA_INICIO;
        this.mTiempoAtras = 4.0f;
        int minSize = screenwidth / 4;
        int maxSize = (int) (((double) minSize) * 1.3d);
        this.mDialogo = new Dialogo(gab, screenwidth, screenheight, "dlg", pDif);
        this.mDialogo.activate(gab);
        if (this.mBolas == null) {
            this.mBolas = new BallPool(10, maxSize, minSize, (Main) this.aBase);
        }
    }

    public void goBack() {
        onPause();
    }

    public void onActivate(GameActivityBase gab) {
    }

    public void onCreateOptionsMenu(Menu pMenu) {
    }

    public void onDeactivate() {
    }

    public void onDestroy() {
        freeBoxes(this.mBoxes);
        freeBoxes(this.mBoxesBolas);
        unregisterUpdateHandler(this.mPhysicsWorld);
        this.mBolas.destroy();
        this.mBolas = null;
        this.aBase.getEngine().getTextureManager().unloadTexture(this.mTexture);
        this.mPhysicsWorld = null;
    }

    public void onGameUpdate(float fTime) {
        switch ($SWITCH_TABLE$com$chorragames$math$BasePuzzle$Estado()[getmEstado().ordinal()]) {
            case 2:
                this.nTime -= fTime;
                final int segundos = (int) this.nTime;
                this.aBase.runOnUiThread(new Runnable() {
                    public void run() {
                        BasePuzzle.this.mBTiempo.setText(Integer.toString(segundos));
                    }
                });
                if (this.nTime <= 0.0f) {
                    setmEstado(Estado.FIN);
                    return;
                } else if (this.mLista.size() == 0) {
                    setmEstado(Estado.CREANDO_BOLAS);
                    return;
                } else {
                    return;
                }
            case 3:
            default:
                return;
            case 4:
                SceneFactory.getInstance((Main) this.aBase).cambiaEscena(SceneFactory.Stages.FIN, getPuzzleId(), this.nAciertos, this.nFallos);
                this.mEstado = Estado.UNDEFINED;
                return;
            case 5:
                this.mTiempoAtras -= fTime;
                setTiempoAtras((int) this.mTiempoAtras);
                return;
        }
    }

    private void setTiempoAtras(int seg) {
        if (((float) seg) != this.mSegundosAtras) {
            desactivaNumeros();
            this.mSegundosAtras = (float) seg;
            switch (seg) {
                case 0:
                    this.mEstado = Estado.JUGANDO;
                    return;
                case 1:
                    activaNumero(this.msUno);
                    return;
                case 2:
                    activaNumero(this.msDos);
                    return;
                case 3:
                    activaNumero(this.msTres);
                    return;
                default:
                    return;
            }
        }
    }

    private void desactivaNumeros() {
        this.aBase.runOnUiThread(new Runnable() {
            public void run() {
                BasePuzzle.this.msUno.setVisible(false);
                BasePuzzle.this.msDos.setVisible(false);
                BasePuzzle.this.msTres.setVisible(false);
            }
        });
    }

    private void activaNumero(final Sprite msTres2) {
        this.mSonidoBeep.play();
        this.aBase.runOnUiThread(new Runnable() {
            public void run() {
                msTres2.setAlpha(0.0f);
                msTres2.registerEntityModifier(new AlphaModifier(1.0f, 0.0f, 1.0f));
                msTres2.setVisible(true);
            }
        });
    }

    public void onLoadAudio(GameActivityBase gab) {
        SoundFactory.setAssetBasePath("mfx/");
        try {
            this.mSonidoFallo = SoundFactory.createSoundFromAsset(this.aBase.getSoundManager(), this.aBase, "buzzer.ogg");
            this.mSonidoDing = SoundFactory.createSoundFromAsset(this.aBase.getSoundManager(), this.aBase, "ding.ogg");
            this.mSonidoBeep = SoundFactory.createSoundFromAsset(this.aBase.getSoundManager(), this.aBase, "beep.ogg");
        } catch (IOException e) {
            Debug.e(e);
        }
    }

    public void creaPuzzle(Dificultad pDiff) {
    }

    private void avanzaNivel() {
        this.mFase++;
        AjustaDificultad.Ajusta(this.mLevel, this.mFase, this.mDificultad);
        if (this.mNumero != this.mDificultad.getNumero()) {
            this.mBoxesBolas = BoxFactory.creaCajasElectionPuzzle(this.mBoxes[1], this.mDificultad.getNumero());
            this.mNumero = this.mDificultad.getNumero();
        }
    }

    private void creaLista() {
        this.mSonidoDing.play();
        this.mLista.clear();
        while (this.mLista.size() < this.mDificultad.getNumero()) {
            int valor = generator.nextInt(this.mDificultad.getMaximo()) + 1;
            if (this.mDificultad.isNegativos() && !generator.nextBoolean()) {
                valor = -valor;
            }
            if (this.mLista.indexOf(Integer.valueOf(valor)) == -1) {
                this.mLista.add(Integer.valueOf(valor));
            }
        }
        Collections.sort(this.mLista);
    }

    /* access modifiers changed from: private */
    public void creaBolas() {
        this.mPhysicsWorld.clearForces();
        this.mPhysicsWorld.setPaused(true);
        int contador = 0;
        int sumas = this.mDificultad.getSumas();
        int restas = this.mDificultad.getRestas();
        for (Integer intValue : this.mLista) {
            int i = intValue.intValue();
            Bola bl = this.mBolas.dameBola();
            String cadena = Integer.toString(i);
            if (sumas > 0) {
                cadena = Formulas.creaSuma(i);
                sumas--;
            } else if (restas > 0) {
                cadena = Formulas.creaResta(i);
                restas--;
            }
            int positionX = (int) ((this.mBoxesBolas[contador].getWidth() - bl.getWidth()) / 2.0f);
            int positionY = (int) ((this.mBoxesBolas[contador].getHeight() - bl.getHeight()) / 2.0f);
            registerTouchArea(bl);
            if (!bl.isActivated()) {
                getChild(1).attachChild(bl);
            }
            int velocityX = generator.nextInt(10);
            int velocityY = generator.nextInt(10);
            if (generator.nextBoolean()) {
                velocityX = -velocityX;
            }
            if (generator.nextBoolean()) {
                velocityY = -velocityY;
            }
            bl.activate(i, cadena, this.mPhysicsWorld, this);
            bl.move(velocityX, velocityY);
            bl.setPosition((float) positionX, (float) positionY);
            contador++;
        }
        this.mPhysicsWorld.setPaused(false);
        setmEstado(Estado.JUGANDO);
    }

    public void onLoadGameScene(GameActivityBase gab) {
    }

    private void crearFisicas(GameActivityBase gab) {
        this.mPhysicsWorld = new Fisicas(new Vector2(0.0f, 0.0f), true);
        Shape ground = new Rectangle(this.mBoxes[1].getX(), this.mBoxes[1].getY() + this.mBoxes[1].getHeight(), this.mBoxes[1].getWidth(), 2.0f);
        Shape roof = new Rectangle(this.mBoxes[1].getX(), this.mBoxes[1].getY(), this.mBoxes[1].getWidth(), 2.0f);
        Shape left = new Rectangle(this.mBoxes[1].getX(), this.mBoxes[1].getY(), 2.0f, this.mBoxes[1].getHeight());
        Shape right = new Rectangle(this.mBoxes[1].getWidth() - 2.0f, this.mBoxes[1].getY(), 2.0f, this.mBoxes[1].getHeight());
        FixtureDef wallFixtureDef = PhysicsFactory.createFixtureDef(1.0f, 0.5f, 0.0f);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, ground, BodyDef.BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, roof, BodyDef.BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, left, BodyDef.BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, right, BodyDef.BodyType.StaticBody, wallFixtureDef);
        roof.setColor(0.0f, 0.0f, 0.0f);
        ground.setColor(0.0f, 0.0f, 0.0f);
        left.setColor(0.0f, 0.0f, 0.0f);
        right.setColor(0.0f, 0.0f, 0.0f);
        attachChild(ground);
        attachChild(roof);
        registerUpdateHandler(this.mPhysicsWorld);
    }

    public void onLoadSprites(GameActivityBase gab) {
        crearFisicas(gab);
        this.mBTiempo = new Marcador(this.mBoxes[0], this.mTime, "00", Marcador.Alineacion.IZQUIERDA, 0, null, ((Main) gab).getFontB());
        getChild(2).attachChild(this.mBTiempo.dameSprite());
        this.mBBien = new Marcador(this.mBoxes[0], this.mTime, "000", Marcador.Alineacion.DERECHA, 0, this.mNota, ((Main) gab).getFontB());
        getChild(2).attachChild(this.mBBien.dameSprite());
        this.mBMal = new Marcador(this.mBoxes[0], this.mTime, "000", Marcador.Alineacion.DERECHA, 1, this.mFail, ((Main) gab).getFontB());
        getChild(2).attachChild(this.mBMal.dameSprite());
        int witdth = gab.getEngine().getSurfaceWidth() / 2;
        int height = gab.getEngine().getSurfaceHeight() / 2;
        int px = gab.getEngine().getSurfaceWidth() / 4;
        int py = gab.getEngine().getSurfaceHeight() / 4;
        this.msUno = new Sprite((float) px, (float) py, (float) witdth, (float) height, this.mUno);
        this.msUno.setVisible(false);
        this.msUno.setAlpha(0.0f);
        this.msDos = new Sprite((float) px, (float) py, (float) witdth, (float) height, this.mDos);
        this.msDos.setVisible(false);
        this.msDos.setAlpha(0.0f);
        this.msTres = new Sprite((float) px, (float) py, (float) witdth, (float) height, this.mTres);
        this.msTres.setVisible(false);
        this.msTres.setAlpha(0.0f);
        getChild(2).attachChild(this.msUno);
        getChild(2).attachChild(this.msDos);
        getChild(2).attachChild(this.msTres);
        setTouchAreaBindingEnabled(true);
    }

    public void onLoadTextures(GameActivityBase gab) {
        this.mTexture = new BuildableBitmapTextureAtlas(PVRTexture.FLAG_BUMPMAP, PVRTexture.FLAG_BUMPMAP);
        BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/bola.png");
        this.mNota = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/ok.png");
        this.mFail = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/bad.png");
        this.mTime = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/time.png");
        this.mUno = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/uno.png");
        this.mDos = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/dos.png");
        this.mTres = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/tres.png");
        try {
            this.mTexture.build(new BlackPawnTextureBuilder(1));
        } catch (ITextureBuilder.TextureAtlasSourcePackingException e) {
            Debug.e(TAG, e);
        }
        gab.getEngine().getTextureManager().loadTexture(this.mTexture);
    }

    public boolean onMenuItemSelected(int pId, MenuItem pItem) {
        return false;
    }

    public void onPause() {
        this.mOldEstado = this.mEstado;
        setmEstado(Estado.PAUSA);
        setChildScene(this.mDialogo, false, true, true);
    }

    public void onPrepareOptionsMenu(Menu pMenu) {
    }

    public void onReset() {
    }

    public void onUnPause() {
        clearChildScene();
        setmEstado(this.mOldEstado);
    }

    /* access modifiers changed from: protected */
    public void LoadLayout(GameActivityBase gab) {
        this.mBoxes = new Box[3];
        this.mBoxes[0] = new Box(0.0f, 0.0f, (float) Main.CAMERA_WIDTH, (float) (((Main) gab).getFont().getLineHeight() * 2));
        this.mBoxes[1] = new Box(0.0f, (float) (((Main) gab).getFont().getLineHeight() * 2), (float) Main.CAMERA_WIDTH, ((float) (Main.CAMERA_HEIGHT - 60)) - this.mBoxes[0].getHeight());
        this.mBoxes[2] = new Box(0.0f, (float) (Main.CAMERA_HEIGHT - 60), (float) Main.CAMERA_WIDTH, 60.0f);
    }

    public void pulsaBola(Bola pBola) {
        if (this.mEstado == Estado.JUGANDO) {
            if (this.mLista.indexOf(Integer.valueOf(pBola.getValor())) == 0) {
                addAcierto();
                removeBola(pBola);
                return;
            }
            addFallo();
        }
    }

    private void removeBola(final Bola pBola) {
        this.aBase.runOnUpdateThread(new Runnable() {
            public void run() {
                pBola.deactivate();
            }
        });
        this.mLista.remove(0);
    }

    public void addAcierto() {
        this.nAciertos++;
        this.aBase.runOnUiThread(new Runnable() {
            public void run() {
                BasePuzzle.this.mBBien.setText(Integer.toString(BasePuzzle.this.nAciertos));
            }
        });
    }

    public void addFallo() {
        this.mSonidoFallo.play();
        this.nFallos++;
        this.nTime -= 3.0f;
        this.aBase.runOnUiThread(new Runnable() {
            public void run() {
                BasePuzzle.this.mBMal.setText(Integer.toString(BasePuzzle.this.nFallos));
            }
        });
    }

    public int getAciertos() {
        return this.nAciertos;
    }

    public void setFallos(int nFallos2) {
        this.nFallos = nFallos2;
    }

    public int getFallos() {
        return this.nFallos;
    }

    private Estado getmEstado() {
        return this.mEstado;
    }

    private void setmEstado(Estado mEstado2) {
        if (this.mEstado != mEstado2) {
            this.mEstado = mEstado2;
            if (mEstado2 == Estado.CREANDO_BOLAS) {
                avanzaNivel();
                creaLista();
                this.aBase.runOnUpdateThread(new Runnable() {
                    public void run() {
                        BasePuzzle.this.creaBolas();
                    }
                });
            }
        }
    }
}
