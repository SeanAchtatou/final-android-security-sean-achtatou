package com.chorragames.math;

import android.view.Menu;
import android.view.MenuItem;
import com.chorragames.framework.BaseButton;
import com.chorragames.framework.GameActivityBase;
import com.chorragames.framework.GameScene;
import com.chorragames.framework.geom.Box;
import com.chorragames.math.SceneFactory;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.buildable.builder.BlackPawnTextureBuilder;
import org.anddev.andengine.opengl.texture.buildable.builder.ITextureBuilder;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.util.Debug;

public class Dialogo extends GameScene {
    private static final int[] LAYOUT_BOXES = {1, 1, 1, 1, 1, 1, 1};
    private static final String TAG = "Dialogo";
    private Box[] mBoxes;
    private TextureRegion mButton;
    private TextureRegion mButtonA;
    private BuildableBitmapTextureAtlas mTexture;

    public Dialogo(Main gab, int screenwidth, int screenheight, String name, int pId) {
        super(gab, 3, screenwidth, screenheight, name, pId);
    }

    public void goBack() {
        this.aBase.getEngine().getScene().clearChildScene();
    }

    public void onActivate(GameActivityBase gab) {
    }

    public void onCreateOptionsMenu(Menu pMenu) {
    }

    public void onDeactivate() {
    }

    public void onDestroy() {
        freeBoxes(this.mBoxes);
        this.aBase.getEngine().getTextureManager().unloadTexture(this.mTexture);
    }

    public void onGameUpdate(float fTime) {
    }

    public void onLoadAudio(GameActivityBase gab) {
    }

    public void onLoadGameScene(GameActivityBase gab) {
    }

    public void onLoadSprites(GameActivityBase gab) {
        BaseButton bb = new BaseButton(this.mBoxes[1], this.mButtonA, this.mButton, ((Main) gab).getFont(), gab.getString(R.string.game_continue)) {
            public void onClick() {
                ((Main) Dialogo.this.aBase).resumeGame();
            }
        };
        attachChild(bb);
        registerTouchArea(bb);
        BaseButton bb2 = new BaseButton(this.mBoxes[3], this.mButtonA, this.mButton, ((Main) gab).getFont(), gab.getString(R.string.game_retry)) {
            public void onClick() {
                SceneFactory.getInstance((Main) Dialogo.this.aBase).cambiaEscena(SceneFactory.Stages.INSTRUCCIONES, Dialogo.this.getPuzzleId());
            }
        };
        attachChild(bb2);
        registerTouchArea(bb2);
        BaseButton bb3 = new BaseButton(this.mBoxes[5], this.mButtonA, this.mButton, ((Main) gab).getFont(), gab.getString(R.string.game_menu)) {
            public void onClick() {
                SceneFactory.getInstance((Main) Dialogo.this.aBase).cambiaEscena(SceneFactory.Stages.MENU, Dialogo.this.getPuzzleId());
            }
        };
        attachChild(bb3);
        registerTouchArea(bb3);
    }

    public void onLoadTextures(GameActivityBase gab) {
        this.mTexture = new BuildableBitmapTextureAtlas(PVRTexture.FLAG_TWIDDLE, PVRTexture.FLAG_TWIDDLE);
        this.mButtonA = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/button.png");
        this.mButton = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/button2.png");
        try {
            this.mTexture.build(new BlackPawnTextureBuilder(1));
        } catch (ITextureBuilder.TextureAtlasSourcePackingException e) {
            Debug.e(TAG, e);
        }
        gab.getEngine().getTextureManager().loadTexture(this.mTexture);
    }

    public boolean onMenuItemSelected(int pId, MenuItem pItem) {
        return false;
    }

    public void onPause() {
    }

    public void onPrepareOptionsMenu(Menu pMenu) {
    }

    public void onReset() {
    }

    public void onUnPause() {
    }

    /* access modifiers changed from: protected */
    public void LoadLayout(GameActivityBase gab) {
        this.mBoxes = initBoxes(LAYOUT_BOXES);
    }
}
