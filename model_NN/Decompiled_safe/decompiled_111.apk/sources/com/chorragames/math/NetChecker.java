package com.chorragames.math;

import android.net.ConnectivityManager;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.anddev.andengine.ui.activity.BaseGameActivity;

public class NetChecker {

    public enum NetStatus {
        NONET,
        FAULTYURL,
        NETOK,
        CONN_MISSING
    }

    public static boolean isOnline(BaseGameActivity gab) {
        if (((ConnectivityManager) gab.getSystemService("connectivity")).getActiveNetworkInfo() == null) {
            return false;
        }
        return true;
    }

    public static NetStatus checkNet(BaseGameActivity gab) {
        if (!isOnline(gab)) {
            return NetStatus.CONN_MISSING;
        }
        try {
            HttpURLConnection urlc = (HttpURLConnection) new URL("http://www.google.es").openConnection();
            urlc.setConnectTimeout(5000);
            urlc.connect();
            if (urlc.getResponseCode() == 200) {
                return NetStatus.NETOK;
            }
            return NetStatus.NETOK;
        } catch (MalformedURLException e) {
            return NetStatus.FAULTYURL;
        } catch (IOException e2) {
            return NetStatus.NONET;
        }
    }
}
