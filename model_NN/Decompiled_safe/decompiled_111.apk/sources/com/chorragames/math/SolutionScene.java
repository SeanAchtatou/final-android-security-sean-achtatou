package com.chorragames.math;

import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import com.chorragames.framework.BaseButton;
import com.chorragames.framework.GameActivityBase;
import com.chorragames.framework.GameScene;
import com.chorragames.framework.geom.Box;
import com.chorragames.framework.geom.Marcador;
import com.chorragames.math.SceneFactory;
import com.scoreloop.client.android.core.controller.RankingController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.ui.OnScoreSubmitObserver;
import com.scoreloop.client.android.ui.PostScoreOverlayActivity;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.scoreloop.client.android.ui.ShowResultOverlayActivity;
import java.io.IOException;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.entity.modifier.LoopEntityModifier;
import org.anddev.andengine.entity.modifier.MoveXModifier;
import org.anddev.andengine.entity.modifier.RotationModifier;
import org.anddev.andengine.entity.primitive.Line;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.buildable.builder.BlackPawnTextureBuilder;
import org.anddev.andengine.opengl.texture.buildable.builder.ITextureBuilder;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.modifier.IModifier;

public class SolutionScene extends GameScene implements RequestControllerObserver, OnScoreSubmitObserver {
    private static final int[] LAYOUT_BOXES = {10, 15, 10, 15, 15, 15, 40};
    private static final int[] LAYOUT_BOXES_H = {1, 1, 1};
    private static final String TAG = "Solution";
    private final int mAciertos;
    private Sound mAplauso;
    /* access modifiers changed from: private */
    public BaseButton mBotonMenu;
    /* access modifiers changed from: private */
    public BaseButton mBotonOnline;
    /* access modifiers changed from: private */
    public BaseButton mBotonReintentar;
    private Box[] mBoxes;
    private TextureRegion mButton;
    private TextureRegion mButtonA;
    private int mCuantos;
    private final int mEstrellas;
    private final int mFallos;
    private TextureRegion mLoading;
    /* access modifiers changed from: private */
    public TextureRegion mOk;
    /* access modifiers changed from: private */
    public int mOldRankingDiario;
    private int mOldRankingTotal;
    private final Double mPuntuacion;
    /* access modifiers changed from: private */
    public Marcador mRankingAmigos;
    /* access modifiers changed from: private */
    public Marcador mRankingDiario;
    private int mRankingToday = -1;
    private int mRankingTotal = -1;
    private Sound mSonidoBeep;
    private TextureRegion mStar;
    private ChangeableText mTextoOnline;
    private BuildableBitmapTextureAtlas mTexture;
    private float mTime;
    private TextureRegion mTimeT;
    private Box[] mboxesH;
    private RankingController rankingController;
    /* access modifiers changed from: private */
    public Sprite sLoading;
    private RankingController todaysRankingController;

    public SolutionScene(GameActivityBase gab, int screenwidth, int screenheight, int pPuzzleId, int pAciertos, int pFallos) {
        super(gab, 3, screenwidth, screenheight, "sol", pPuzzleId);
        this.mAciertos = pAciertos;
        this.mFallos = pFallos;
        this.mPuntuacion = Double.valueOf(Double.parseDouble(Integer.toString(pAciertos - pFallos)));
        this.mTime = 0.0f;
        this.mOldRankingDiario = 0;
        this.mOldRankingTotal = 0;
        this.mEstrellas = AjustaDificultad.dameEstrellas(pPuzzleId, this.mAciertos, this.mFallos);
        ScoreloopManagerSingleton.get().setOnScoreSubmitObserver(this);
        this.rankingController = new RankingController(this);
        this.todaysRankingController = new RankingController(this);
        this.rankingController.setSearchList(SearchList.getBuddiesScoreSearchList());
        this.todaysRankingController.setSearchList(SearchList.getTwentyFourHourScoreSearchList());
    }

    public void goBack() {
        SceneFactory.getInstance((Main) this.aBase).cambiaEscena(SceneFactory.Stages.MENU, 0);
    }

    public void onActivate(GameActivityBase gab) {
    }

    public void onCreateOptionsMenu(Menu pMenu) {
    }

    public void onDeactivate() {
    }

    public void onDestroy() {
        freeBoxes(this.mBoxes);
        ScoreloopManagerSingleton.get().setOnScoreSubmitObserver(null);
        this.aBase.getEngine().getTextureManager().unloadTexture(this.mTexture);
        this.mTextoOnline = null;
        this.rankingController = null;
        this.mButton = null;
        this.mStar = null;
        this.mButtonA = null;
    }

    public void onGameUpdate(float fTime) {
        this.mTime += fTime;
        final int donde = (int) this.mTime;
        if (donde > this.mCuantos && donde <= this.mEstrellas) {
            this.mCuantos++;
            this.aBase.runOnUiThread(new Runnable() {
                public void run() {
                    SolutionScene.this.creaEstrella(donde);
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void creaEstrella(int donde) {
        int width = Main.CAMERA_WIDTH / 5;
        int size = (int) Math.min((float) width, this.mBoxes[2].getHeight());
        getChild(1).attachChild(new Sprite((float) (((donde - 1) * width) + ((width - size) / 2)), this.mBoxes[2].getY() + ((float) ((int) ((this.mBoxes[2].getHeight() - ((float) size)) / 2.0f))), (float) size, (float) size, this.mStar));
        this.mSonidoBeep.play();
    }

    public void onLoadAudio(GameActivityBase gab) {
        SoundFactory.setAssetBasePath("mfx/");
        try {
            this.mAplauso = SoundFactory.createSoundFromAsset(this.aBase.getSoundManager(), this.aBase, "applause.ogg");
            this.mSonidoBeep = SoundFactory.createSoundFromAsset(this.aBase.getSoundManager(), this.aBase, "beep.ogg");
        } catch (IOException e) {
            Debug.e(e);
        }
    }

    public void onLoadGameScene(GameActivityBase gab) {
    }

    private void compruebaResultadoOnline(GameActivityBase gab) {
        int i = 0;
        while (i < this.mboxesH.length) {
            getChild(0).attachChild(creaRectangulo(this.mboxesH[i], i != 0 ? 0.6f : 0.3f));
            i++;
        }
        Text text = new Text(this.mboxesH[0].getX(), this.mboxesH[0].getY(), ((Main) gab).getFont(), gab.getString(R.string.game_social));
        text.setPosition((this.mboxesH[0].getWidth() - text.getWidth()) / 2.0f, this.mboxesH[0].getY() + ((this.mboxesH[0].getHeight() - text.getHeight()) / 2.0f));
        getChild(1).attachChild(text);
        int size = (int) this.mboxesH[1].getHeight();
        this.sLoading = new Sprite((float) ((int) ((this.mboxesH[1].getWidth() - ((float) size)) / 2.0f)), this.mboxesH[1].getY() + ((float) ((int) (((double) this.mboxesH[1].getHeight()) * 0.5d))), (float) size, (float) size, this.mLoading);
        this.sLoading.registerEntityModifier(new LoopEntityModifier(new RotationModifier(2.0f, 0.0f, 360.0f)));
        getChild(2).attachChild(this.sLoading);
        Text text2 = new Text(0.0f, 0.0f, ((Main) gab).getFontB(), gab.getString(R.string.game_ranking_d));
        int pY = (int) ((this.mboxesH[1].getHeight() - text2.getHeight()) / 2.0f);
        text2.setPosition(0.0f, this.mboxesH[1].getY() + ((float) pY));
        getChild(1).attachChild(text2);
        Text text3 = new Text(0.0f, 0.0f, ((Main) gab).getFontB(), gab.getString(R.string.game_ranking_f));
        text3.setPosition(0.0f, this.mboxesH[2].getY() + ((float) pY));
        getChild(1).attachChild(text3);
        this.mRankingDiario = new Marcador(this.mboxesH[1], this.mTimeT, "000000", Marcador.Alineacion.DERECHA, 0, null, ((Main) gab).getFontB());
        this.mRankingAmigos = new Marcador(this.mboxesH[2], this.mTimeT, "000000", Marcador.Alineacion.DERECHA, 0, null, ((Main) gab).getFontB());
        getChild(1).attachChild(this.mRankingDiario.dameSprite());
        getChild(1).attachChild(this.mRankingAmigos.dameSprite());
        ScoreloopManagerSingleton.get().onGamePlayEnded(this.mPuntuacion, Integer.valueOf(getPuzzleId() - 1));
    }

    private void createTexto(GameActivityBase gab) {
        Text textoBien = new Text(0.0f, 0.0f, ((Main) gab).getFontB(), gab.getString(R.string.game_correct));
        Text textoMal = new Text(0.0f, 0.0f, ((Main) gab).getFontB(), gab.getString(R.string.game_wrong));
        int height = (int) (this.mBoxes[1].getHeight() / 2.0f);
        int posY = (int) ((((float) height) - textoBien.getHeight()) / 2.0f);
        int posX = (int) (((double) Main.CAMERA_WIDTH) * 0.6d);
        textoBien.setPosition(((float) posX) - textoBien.getWidth(), this.mBoxes[0].getHeight() + ((float) posY));
        textoMal.setPosition(((float) posX) - textoMal.getWidth(), this.mBoxes[0].getHeight() + ((float) height) + ((float) posY));
        getChild(1).attachChild(textoBien);
        getChild(1).attachChild(textoMal);
        Text textoBien2 = new Text(0.0f, 0.0f, ((Main) gab).getFontB(), Integer.toString(this.mAciertos));
        Text textoMal2 = new Text(0.0f, 0.0f, ((Main) gab).getFontB(), Integer.toString(this.mFallos));
        textoBien2.setPosition((float) (posX + posY), this.mBoxes[0].getHeight() + ((float) posY));
        textoMal2.setPosition((float) (posX + posY), this.mBoxes[0].getHeight() + ((float) height) + ((float) posY));
        getChild(1).attachChild(textoBien2);
        getChild(1).attachChild(textoMal2);
    }

    public void onLoadSprites(GameActivityBase gab) {
        Text texto = new Text(0.0f, 0.0f, ((Main) gab).getFontB(), gab.getString(R.string.game_result));
        texto.setPosition((this.mBoxes[0].getWidth() - texto.getWidth()) / 2.0f, (this.mBoxes[0].getHeight() - texto.getHeight()) / 2.0f);
        getChild(1).attachChild(texto);
        Line ln = new Line(0.0f, this.mBoxes[1].getY(), (float) Main.CAMERA_WIDTH, this.mBoxes[1].getY(), 2.0f);
        ln.setColor(0.0f, 0.0f, 0.0f);
        getChild(1).attachChild(ln);
        createTexto(gab);
        creaBotonesStd(gab);
        compruebaResultadoOnline(gab);
        this.mAplauso.play();
    }

    private void creaBotonesStd(GameActivityBase gab) {
        final GameActivityBase gameActivityBase = gab;
        this.mBotonReintentar = new BaseButton(this.mBoxes[3], this.mButtonA, this.mButton, ((Main) gab).getFont(), gab.getString(R.string.game_retry)) {
            public void onClick() {
                SceneFactory.getInstance((Main) gameActivityBase).cambiaEscena(SceneFactory.Stages.INSTRUCCIONES, SolutionScene.this.getPuzzleId());
            }
        };
        attachChild(this.mBotonReintentar);
        registerTouchArea(this.mBotonReintentar);
        this.mBotonMenu = new BaseButton(this.mBoxes[4], this.mButtonA, this.mButton, ((Main) gab).getFont(), gab.getString(R.string.game_menu)) {
            public void onClick() {
                SceneFactory.getInstance((Main) SolutionScene.this.aBase).cambiaEscena(SceneFactory.Stages.MENU, SolutionScene.this.getPuzzleId());
            }
        };
        attachChild(this.mBotonMenu);
        registerTouchArea(this.mBotonMenu);
        this.mBotonOnline = new BaseButton(this.mBoxes[5], this.mButtonA, this.mButton, ((Main) gab).getFont(), gab.getString(R.string.game_share)) {
            public void onClick() {
                SolutionScene.this.aBase.startActivity(new Intent(SolutionScene.this.aBase, PostScoreOverlayActivity.class));
            }
        };
        attachChild(this.mBotonOnline);
    }

    private void creaBotones(GameActivityBase gab) {
        this.mBotonReintentar = new BaseButton(this.mBoxes[3], this.mButtonA, this.mButton, ((Main) gab).getFont(), gab.getString(R.string.game_retry)) {
            public void onClick() {
                SceneFactory.getInstance((Main) SolutionScene.this.aBase).cambiaEscena(SceneFactory.Stages.INSTRUCCIONES, SolutionScene.this.getPuzzleId());
            }
        };
        this.mBotonReintentar.registerEntityModifier(new MoveXModifier(2.0f, this.mBotonReintentar.getX() + ((float) Main.CAMERA_WIDTH), this.mBotonReintentar.getX(), new IEntityModifier.IEntityModifierListener() {
            public /* bridge */ /* synthetic */ void onModifierFinished(IModifier iModifier, Object obj) {
                onModifierFinished((IModifier<IEntity>) iModifier, (IEntity) obj);
            }

            public /* bridge */ /* synthetic */ void onModifierStarted(IModifier iModifier, Object obj) {
                onModifierStarted((IModifier<IEntity>) iModifier, (IEntity) obj);
            }

            public void onModifierStarted(IModifier<IEntity> iModifier, IEntity arg1) {
            }

            public void onModifierFinished(IModifier<IEntity> iModifier, IEntity arg1) {
                SolutionScene.this.registerTouchArea(SolutionScene.this.mBotonReintentar);
            }
        }));
        this.mBotonReintentar.setPosition(this.mBotonReintentar.getX() + ((float) Main.CAMERA_WIDTH), this.mBotonReintentar.getY());
        attachChild(this.mBotonReintentar);
        this.mBotonMenu = new BaseButton(this.mBoxes[4], this.mButtonA, this.mButton, ((Main) gab).getFont(), gab.getString(R.string.game_menu)) {
            public void onClick() {
                SceneFactory.getInstance((Main) SolutionScene.this.aBase).cambiaEscena(SceneFactory.Stages.MENU, SolutionScene.this.getPuzzleId());
            }
        };
        this.mBotonMenu.registerEntityModifier(new MoveXModifier(2.0f, this.mBotonMenu.getX() - ((float) Main.CAMERA_WIDTH), this.mBotonMenu.getX(), new IEntityModifier.IEntityModifierListener() {
            public /* bridge */ /* synthetic */ void onModifierFinished(IModifier iModifier, Object obj) {
                onModifierFinished((IModifier<IEntity>) iModifier, (IEntity) obj);
            }

            public /* bridge */ /* synthetic */ void onModifierStarted(IModifier iModifier, Object obj) {
                onModifierStarted((IModifier<IEntity>) iModifier, (IEntity) obj);
            }

            public void onModifierStarted(IModifier<IEntity> iModifier, IEntity arg1) {
            }

            public void onModifierFinished(IModifier<IEntity> iModifier, IEntity arg1) {
                SolutionScene.this.registerTouchArea(SolutionScene.this.mBotonMenu);
            }
        }));
        this.mBotonMenu.setPosition(this.mBotonMenu.getX() - ((float) Main.CAMERA_WIDTH), this.mBotonMenu.getY());
        attachChild(this.mBotonMenu);
        this.mBotonOnline = new BaseButton(this.mBoxes[5], this.mButtonA, this.mButton, ((Main) gab).getFont(), gab.getString(R.string.game_share)) {
            public void onClick() {
                SolutionScene.this.aBase.startActivity(new Intent(SolutionScene.this.aBase, PostScoreOverlayActivity.class));
            }
        };
        this.mBotonOnline.registerEntityModifier(new MoveXModifier(2.0f, this.mBotonOnline.getX() + ((float) Main.CAMERA_WIDTH), this.mBotonOnline.getX(), new IEntityModifier.IEntityModifierListener() {
            public /* bridge */ /* synthetic */ void onModifierFinished(IModifier iModifier, Object obj) {
                onModifierFinished((IModifier<IEntity>) iModifier, (IEntity) obj);
            }

            public /* bridge */ /* synthetic */ void onModifierStarted(IModifier iModifier, Object obj) {
                onModifierStarted((IModifier<IEntity>) iModifier, (IEntity) obj);
            }

            public void onModifierStarted(IModifier<IEntity> iModifier, IEntity arg1) {
            }

            public void onModifierFinished(IModifier<IEntity> iModifier, IEntity arg1) {
                SolutionScene.this.registerTouchArea(SolutionScene.this.mBotonOnline);
            }
        }));
        this.mBotonOnline.setPosition(this.mBotonOnline.getX() + ((float) Main.CAMERA_WIDTH), this.mBotonOnline.getY());
        attachChild(this.mBotonOnline);
    }

    public void onLoadTextures(GameActivityBase gab) {
        this.mTexture = new BuildableBitmapTextureAtlas(PVRTexture.FLAG_BUMPMAP, PVRTexture.FLAG_BUMPMAP);
        this.mStar = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/star.png");
        this.mButtonA = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/button.png");
        this.mLoading = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/loading.png");
        this.mButton = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/button2.png");
        this.mTimeT = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/time.png");
        this.mOk = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, gab, "gfx/ok.png");
        try {
            this.mTexture.build(new BlackPawnTextureBuilder(1));
        } catch (ITextureBuilder.TextureAtlasSourcePackingException e) {
            Debug.e(TAG, e);
        }
        gab.getEngine().getTextureManager().loadTexture(this.mTexture);
    }

    public boolean onMenuItemSelected(int pId, MenuItem pItem) {
        return false;
    }

    public void onPause() {
    }

    public void onPrepareOptionsMenu(Menu pMenu) {
    }

    public void onReset() {
    }

    public void onUnPause() {
    }

    /* access modifiers changed from: protected */
    public void LoadLayout(GameActivityBase gab) {
        this.mBoxes = initBoxes(LAYOUT_BOXES);
        this.mboxesH = initBoxesInV(this.mBoxes[6], LAYOUT_BOXES_H);
    }

    public void onScoreSubmit(int status, Exception error) {
        if (error != null) {
            Log.d(TAG, error.getMessage());
        }
        if (status == 1) {
            Log.d(TAG, "STATUS_SUCCESS_SCORE");
            this.todaysRankingController.loadRankingForUserInGameMode(Session.getCurrentSession().getUser(), Integer.valueOf(getPuzzleId() - 1));
        } else if (status == 4) {
            Log.d(TAG, "STATUS_ERROR_NETWORK");
            this.aBase.runOnUiThread(new Runnable() {
                public void run() {
                    SolutionScene.this.sLoading.setVisible(false);
                    Toast.makeText(SolutionScene.this.aBase, "Network Error", 1).show();
                }
            });
        } else if (status == 2) {
            Log.d(TAG, "STATUS_SUCCESS_LOCAL_SCORE");
            this.aBase.runOnUiThread(new Runnable() {
                public void run() {
                    SolutionScene.this.sLoading.setVisible(false);
                    Toast.makeText(SolutionScene.this.aBase, "SUcess Local", 1).show();
                }
            });
        } else if (status == 5) {
            Log.d(TAG, "STATUS_ERROR_BALANCE");
            this.aBase.runOnUiThread(new Runnable() {
                public void run() {
                    SolutionScene.this.sLoading.setVisible(false);
                    Toast.makeText(SolutionScene.this.aBase, "Balance Error", 1).show();
                }
            });
        } else {
            this.aBase.runOnUiThread(new Runnable() {
                public void run() {
                    SolutionScene.this.sLoading.setVisible(false);
                    Toast.makeText(SolutionScene.this.aBase, (int) R.string.game_error_score, 1).show();
                }
            });
        }
    }

    public void setRankingTotal(int mRankingTotal2) {
        this.mRankingTotal = mRankingTotal2;
        updateRankings();
    }

    private void updateRankings() {
        if (this.mRankingToday >= 0 && this.mRankingTotal >= 0) {
            creaTextoRanking();
            creaBotones(this.aBase);
        }
    }

    private void creaTextoRanking() {
        this.mTextoOnline.setVisible(false);
        Text txt = new Text(0.0f, 0.0f, ((Main) this.aBase).getFontB(), this.aBase.getString(R.string.game_rank, new Object[]{Integer.valueOf(this.mRankingToday), Integer.valueOf(this.mRankingTotal)}));
        txt.setPosition((float) Main.CAMERA_WIDTH, this.mBoxes[6].getY());
        txt.registerEntityModifier(new LoopEntityModifier(new MoveXModifier(5.0f, (float) Main.CAMERA_WIDTH, -txt.getWidth())));
        getChild(2).attachChild(txt);
    }

    public void requestControllerDidFail(RequestController arg0, Exception arg1) {
        this.aBase.runOnUiThread(new Runnable() {
            public void run() {
                SolutionScene.this.sLoading.setVisible(false);
                Toast.makeText(SolutionScene.this.aBase, (int) R.string.game_offline, 1).show();
            }
        });
    }

    public void requestControllerDidReceiveResponse(RequestController arg0) {
        if (arg0 == this.rankingController) {
            final int mRanking = this.rankingController.getRanking().getRank().intValue();
            this.aBase.runOnUiThread(new Runnable() {
                public void run() {
                    SolutionScene.this.sLoading.setVisible(false);
                }
            });
            this.aBase.runOnUpdateThread(new Runnable() {
                public void run() {
                    SolutionScene.this.registerTouchArea(SolutionScene.this.mBotonOnline);
                }
            });
            this.aBase.runOnUiThread(new Runnable() {
                public void run() {
                    SolutionScene.this.mRankingAmigos.setText(Integer.toString(mRanking));
                }
            });
        }
        if (arg0 == this.todaysRankingController) {
            this.mOldRankingDiario = this.todaysRankingController.getRanking().getRank().intValue();
            this.aBase.runOnUiThread(new Runnable() {
                public void run() {
                    SolutionScene.this.mRankingDiario.setText(Integer.toString(SolutionScene.this.mOldRankingDiario));
                }
            });
            this.rankingController.loadRankingForUserInGameMode(Session.getCurrentSession().getUser(), Integer.valueOf(getPuzzleId() - 1));
        }
    }

    private void creaFelicitacion(Marcador pRanking) {
        Sprite sp = pRanking.dameSprite();
        final int size = (int) (sp.getHeight() * 0.6f);
        int margin = (int) ((sp.getHeight() - ((float) size)) / 2.0f);
        final int pX = (int) (((sp.getX() + sp.getWidth()) - ((float) margin)) - ((float) size));
        final int pY = (int) (((sp.getY() + sp.getHeight()) - ((float) margin)) - ((float) size));
        this.aBase.runOnUiThread(new Runnable() {
            public void run() {
                SolutionScene.this.getChild(2).attachChild(new Sprite((float) pX, (float) pY, (float) size, (float) size, SolutionScene.this.mOk));
            }
        });
        this.mAplauso.play();
    }

    public void setRankingToday(int mRankingToday2) {
        this.mRankingToday = mRankingToday2;
        updateRankings();
    }

    public void comparteScore() {
        this.aBase.startActivity(new Intent(this.aBase, ShowResultOverlayActivity.class));
    }
}
