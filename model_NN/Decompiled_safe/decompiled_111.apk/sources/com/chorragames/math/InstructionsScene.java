package com.chorragames.math;

import android.view.Menu;
import android.view.MenuItem;
import com.chorragames.framework.GameActivityBase;
import com.chorragames.framework.GameScene;
import com.chorragames.math.SceneFactory;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;

public class InstructionsScene extends GameScene {
    public InstructionsScene(Main gab, int screenwidth, int screenheight, int id) {
        super(gab, 3, screenwidth, screenheight, "inst", id);
    }

    public void goBack() {
        SceneFactory.getInstance((Main) this.aBase).cambiaEscena(SceneFactory.Stages.MENU, getPuzzleId());
    }

    public void onActivate(GameActivityBase gab) {
    }

    public void onCreateOptionsMenu(Menu pMenu) {
    }

    public void onDeactivate() {
    }

    public void onDestroy() {
    }

    public void onGameUpdate(float fTime) {
    }

    public void onLoadAudio(GameActivityBase gab) {
    }

    public void onLoadGameScene(GameActivityBase gab) {
    }

    public void onLoadSprites(GameActivityBase gab) {
        Font fuente = ((Main) gab).getFontB();
        String cadena = gab.getString(R.string.menu_help);
        int posX = Main.CAMERA_WIDTH / 12;
        Text txt = new Text((float) posX, (float) posX, fuente, cadena);
        txt.setScale(1.5f);
        txt.setPosition((float) (posX * 2), (float) (posX * 2));
        getChild(1).attachChild(txt);
        Text txt2 = new Text(txt.getX(), txt.getY() + txt.getHeight() + ((float) posX), fuente, gab.getString(R.string.menu_touch));
        txt2.setPosition((((float) Main.CAMERA_WIDTH) - txt2.getWidth()) / 2.0f, (((float) Main.CAMERA_HEIGHT) - txt2.getHeight()) / 2.0f);
        getChild(1).attachChild(txt2);
        setOnSceneTouchListener(new Scene.IOnSceneTouchListener() {
            public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
                SceneFactory.getInstance((Main) InstructionsScene.this.aBase).cambiaEscena(SceneFactory.Stages.PUZZLE, InstructionsScene.this.getPuzzleId());
                return true;
            }
        });
    }

    public void onLoadTextures(GameActivityBase gab) {
    }

    public boolean onMenuItemSelected(int pId, MenuItem pItem) {
        return false;
    }

    public void onPause() {
    }

    public void onPrepareOptionsMenu(Menu pMenu) {
    }

    public void onReset() {
    }

    public void onUnPause() {
    }
}
