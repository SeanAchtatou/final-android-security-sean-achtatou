package com.chorragames.math;

import com.chorragames.framework.AsyncTaskLoader;
import com.chorragames.framework.GameScene;
import com.chorragames.framework.IAsyncCallback;

public class SceneFactory {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$chorragames$math$SceneFactory$Stages = null;
    private static SceneFactory INSTANCE = null;
    private static final String TAG = "SceneFactory";
    private static Main aBase;
    private static GameScene mLoading;

    public enum Stages {
        SPLASH,
        MENU,
        LISTA,
        PUZZLE,
        INSTRUCCIONES,
        FIN
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$chorragames$math$SceneFactory$Stages() {
        int[] iArr = $SWITCH_TABLE$com$chorragames$math$SceneFactory$Stages;
        if (iArr == null) {
            iArr = new int[Stages.values().length];
            try {
                iArr[Stages.FIN.ordinal()] = 6;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Stages.INSTRUCCIONES.ordinal()] = 5;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Stages.LISTA.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[Stages.MENU.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[Stages.PUZZLE.ordinal()] = 4;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[Stages.SPLASH.ordinal()] = 1;
            } catch (NoSuchFieldError e6) {
            }
            $SWITCH_TABLE$com$chorragames$math$SceneFactory$Stages = iArr;
        }
        return iArr;
    }

    private SceneFactory(Main pMain) {
        aBase = pMain;
    }

    public static void clearFactory() {
        INSTANCE = null;
    }

    public static SceneFactory getInstance(Main pMain) {
        if (INSTANCE == null) {
            INSTANCE = new SceneFactory(pMain);
        }
        return INSTANCE;
    }

    public void inicializa() {
        mLoading = new LoadingScene(aBase, Main.CAMERA_WIDTH, Main.CAMERA_HEIGHT, "loading");
    }

    public void changeScene(GameScene newScene) {
        newScene.activate(aBase);
    }

    public void cambiaEscena(Stages escena, int id) {
        cambiaEscena(escena, id, 0, 0);
    }

    public void cambiaEscena(Stages escena, int id, int nAciertos, int nFallos) {
        final GameScene localScene = (GameScene) aBase.getEngine().getScene();
        mLoading.activate(aBase);
        aBase.getEngine().setScene(mLoading);
        final Stages stages = escena;
        final int i = id;
        final int i2 = nAciertos;
        final int i3 = nFallos;
        IAsyncCallback callback = new IAsyncCallback() {
            private GameScene mNewScene;

            public void workToDo() {
                this.mNewScene = SceneFactory.this.dameEscena(stages, i, i2, i3);
            }

            public void onComplete() {
                SceneFactory.this.onSwitchScene(localScene, this.mNewScene);
            }
        };
        new AsyncTaskLoader().execute(callback);
    }

    /* access modifiers changed from: protected */
    public void onSwitchScene(GameScene mLocalScene, GameScene mNewScene) {
        if (mLocalScene != null) {
            mLocalScene.deactivate();
            mLocalScene.destroy();
        }
        mNewScene.activate(aBase);
        aBase.getEngine().setScene(mNewScene);
    }

    /* access modifiers changed from: protected */
    public GameScene dameEscena(Stages escena, int id) {
        return dameEscena(escena, id, 0, 0);
    }

    /* access modifiers changed from: protected */
    public GameScene dameEscena(Stages escena, int id, int pAciertos, int pFallos) {
        switch ($SWITCH_TABLE$com$chorragames$math$SceneFactory$Stages()[escena.ordinal()]) {
            case 1:
                return getSplashScene();
            case 2:
                return getMenuScene();
            case 3:
            default:
                return null;
            case 4:
                return getPuzzleScene(id);
            case 5:
                return getInstrucciones(id);
            case 6:
                return getSolucion(id, pAciertos, pFallos);
        }
    }

    private GameScene getSolucion(int id, int pAciertos, int pFallos) {
        return new SolutionScene(aBase, Main.CAMERA_WIDTH, Main.CAMERA_HEIGHT, id, pAciertos, pFallos);
    }

    private GameScene getInstrucciones(int id) {
        return new InstructionsScene(aBase, Main.CAMERA_WIDTH, Main.CAMERA_HEIGHT, id);
    }

    public GameScene getMenuScene() {
        return new MenuScene(aBase, Main.CAMERA_WIDTH, Main.CAMERA_HEIGHT, "menu");
    }

    public GameScene getPuzzleScene(int id) {
        return new BasePuzzle(aBase, Main.CAMERA_WIDTH, Main.CAMERA_HEIGHT, "menu", id);
    }

    public GameScene getSplashScene() {
        return new SplashScene(aBase, Main.CAMERA_WIDTH, Main.CAMERA_HEIGHT, "splash");
    }
}
