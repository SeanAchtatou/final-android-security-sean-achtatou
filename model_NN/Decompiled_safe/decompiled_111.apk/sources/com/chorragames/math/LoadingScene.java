package com.chorragames.math;

import android.view.Menu;
import android.view.MenuItem;
import com.chorragames.framework.GameActivityBase;
import com.chorragames.framework.GameScene;

public class LoadingScene extends GameScene {
    private static final int[] LAYOUT_BOXES = {1};
    private static final String TAG = "LoadingScene";

    public LoadingScene(Main gab, int screenwidth, int screenheight, String name) {
        super(gab, screenwidth, screenheight, name);
    }

    public void onActivate(GameActivityBase gab) {
    }

    public void onCreateOptionsMenu(Menu pMenu) {
    }

    public void onDeactivate() {
    }

    public void onDestroy() {
    }

    public void onLoadAudio(GameActivityBase gab) {
    }

    public boolean onMenuItemSelected(int pId, MenuItem pItem) {
        return false;
    }

    public void onPrepareOptionsMenu(Menu pMenu) {
    }

    public void onReset() {
    }

    public void onLoadSprites(GameActivityBase gab) {
    }

    public void onGameUpdate(float fTime) {
    }

    public void onLoadTextures(GameActivityBase gab) {
    }

    /* access modifiers changed from: protected */
    public void LoadLayout(GameActivityBase gab) {
        initBoxes(LAYOUT_BOXES);
    }

    public void goBack() {
    }

    public void onLoadGameScene(GameActivityBase gab) {
    }

    public void onPause() {
    }

    public void onUnPause() {
    }
}
