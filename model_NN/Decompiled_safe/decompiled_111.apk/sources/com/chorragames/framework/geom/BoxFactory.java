package com.chorragames.framework.geom;

import com.scoreloop.client.android.ui.component.base.Constant;

public class BoxFactory {
    public static final Box[] creaCajasElectionPuzzle(Box pCaja, int pOpciones) {
        Box[] ret = new Box[pOpciones];
        switch (pOpciones) {
            case 1:
                ret[0] = pCaja;
                break;
            case 2:
            case 3:
            default:
                return null;
            case 4:
                ret[0] = creaCajaDentro(pCaja, 2, 2, 0.0f, 0.0f);
                ret[1] = creaCajaDentro(pCaja, 2, 2, 0.0f, 1.0f);
                ret[2] = creaCajaDentro(pCaja, 2, 2, 1.0f, 0.0f);
                ret[3] = creaCajaDentro(pCaja, 2, 2, 1.0f, 1.0f);
                break;
            case 5:
                ret[0] = creaCajaDentro(pCaja, 2, 3, 0.0f, 0.0f);
                ret[1] = creaCajaDentro(pCaja, 2, 3, 1.0f, 0.0f);
                ret[2] = creaCajaDentro(pCaja, 2, 3, 0.5f, 1.0f);
                ret[3] = creaCajaDentro(pCaja, 2, 3, 0.0f, 2.0f);
                ret[4] = creaCajaDentro(pCaja, 2, 3, 1.0f, 2.0f);
                break;
            case 6:
                ret[0] = creaCajaDentro(pCaja, 2, 3, 0.0f, 0.0f);
                ret[1] = creaCajaDentro(pCaja, 2, 3, 1.0f, 0.0f);
                ret[2] = creaCajaDentro(pCaja, 2, 3, 0.0f, 1.0f);
                ret[3] = creaCajaDentro(pCaja, 2, 3, 1.0f, 1.0f);
                ret[4] = creaCajaDentro(pCaja, 2, 3, 0.0f, 2.0f);
                ret[5] = creaCajaDentro(pCaja, 2, 3, 1.0f, 2.0f);
                break;
            case 7:
                ret[0] = creaCajaDentro(pCaja, 3, 3, 0.0f, 0.0f);
                ret[1] = creaCajaDentro(pCaja, 3, 3, 1.0f, 0.0f);
                ret[2] = creaCajaDentro(pCaja, 3, 3, 2.0f, 0.0f);
                ret[3] = creaCajaDentro(pCaja, 3, 3, 1.0f, 1.0f);
                ret[4] = creaCajaDentro(pCaja, 3, 3, 0.0f, 2.0f);
                ret[5] = creaCajaDentro(pCaja, 3, 3, 1.0f, 2.0f);
                ret[6] = creaCajaDentro(pCaja, 3, 3, 2.0f, 2.0f);
                break;
            case 8:
                ret[0] = creaCajaDentro(pCaja, 3, 3, 0.0f, 0.0f);
                ret[1] = creaCajaDentro(pCaja, 3, 3, 1.0f, 0.0f);
                ret[2] = creaCajaDentro(pCaja, 3, 3, 2.0f, 0.0f);
                ret[3] = creaCajaDentro(pCaja, 3, 3, 0.0f, 1.0f);
                ret[4] = creaCajaDentro(pCaja, 3, 3, 1.0f, 1.0f);
                ret[5] = creaCajaDentro(pCaja, 3, 3, 0.0f, 2.0f);
                ret[6] = creaCajaDentro(pCaja, 3, 3, 1.0f, 2.0f);
                ret[7] = creaCajaDentro(pCaja, 3, 3, 2.0f, 2.0f);
                break;
            case Constant.LIST_ITEM_TYPE_CHALLENGE_STAKE_AND_MODE /*9*/:
                ret[0] = creaCajaDentro(pCaja, 3, 3, 0.0f, 0.0f);
                ret[1] = creaCajaDentro(pCaja, 3, 3, 1.0f, 0.0f);
                ret[2] = creaCajaDentro(pCaja, 3, 3, 2.0f, 0.0f);
                ret[3] = creaCajaDentro(pCaja, 3, 3, 0.0f, 1.0f);
                ret[4] = creaCajaDentro(pCaja, 3, 3, 1.0f, 1.0f);
                ret[5] = creaCajaDentro(pCaja, 3, 3, 2.0f, 1.0f);
                ret[6] = creaCajaDentro(pCaja, 3, 3, 0.0f, 2.0f);
                ret[7] = creaCajaDentro(pCaja, 3, 3, 1.0f, 2.0f);
                ret[8] = creaCajaDentro(pCaja, 3, 3, 2.0f, 2.0f);
                break;
        }
        return ret;
    }

    public static final Box creaCajaDentro(Box pCaja, int pTotalColumnas, int pTotalFilas, float pColumna, float d) {
        float altoCelda = pCaja.getHeight() / ((float) pTotalFilas);
        return new Box(pCaja.getX() + ((pCaja.getWidth() * pColumna) / ((float) pTotalColumnas)), pCaja.getY() + ((pCaja.getHeight() * d) / ((float) pTotalFilas)), pCaja.getWidth() / ((float) pTotalColumnas), altoCelda);
    }
}
