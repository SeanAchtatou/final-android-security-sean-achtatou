package com.chorragames.framework.geom;

public class Box extends Point {
    protected float fHeight;
    protected float fWidth;

    public float getWidth() {
        return this.fWidth;
    }

    public float getHeight() {
        return this.fHeight;
    }

    public void setWidth(float width) {
        this.fWidth = width;
    }

    public void setHeight(float height) {
        this.fHeight = height;
    }

    public Box() {
        super(0.0f, 0.0f);
        this.fWidth = 0.0f;
        this.fHeight = 0.0f;
    }

    public Box(float x, float y, float width, float height) {
        super(x, y);
        this.fWidth = width;
        this.fHeight = height;
    }

    public Box(Box box) {
        super(box.getX(), box.getY());
        this.fWidth = box.getWidth();
        this.fHeight = box.getHeight();
    }

    public float getXCenter() {
        return this.fWidth / 2.0f;
    }

    public float getYCenter() {
        return this.fHeight / 2.0f;
    }

    public Point getTopLeft() {
        return new Point(this.X, this.Y);
    }

    public Point getTopRight() {
        return new Point(this.X + this.fWidth, this.Y);
    }

    public Point getBottomLeft() {
        return new Point(this.X, this.Y + this.fHeight);
    }

    public Point getBottomRight() {
        return new Point(this.X + this.fWidth, this.Y + this.fHeight);
    }

    public String toString() {
        return "Box [Height=" + this.fHeight + ", Width=" + this.fWidth + ", X=" + this.X + ", Y=" + this.Y + "]";
    }
}
