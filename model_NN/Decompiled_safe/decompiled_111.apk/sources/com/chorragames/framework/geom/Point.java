package com.chorragames.framework.geom;

public class Point {
    protected float X;
    protected float Y;

    public Point(float x, float y) {
        this.X = x;
        this.Y = y;
    }

    public float getX() {
        return this.X;
    }

    public float getY() {
        return this.Y;
    }

    public void setX(float x) {
        this.X = x;
    }

    public void setY(float y) {
        this.Y = y;
    }
}
