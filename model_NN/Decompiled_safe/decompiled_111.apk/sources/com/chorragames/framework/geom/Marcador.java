package com.chorragames.framework.geom;

import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class Marcador {
    private Sprite mCaja;
    private ChangeableText mTexto;

    public enum Alineacion {
        IZQUIERDA,
        DERECHA
    }

    public Marcador(Box pCaja, TextureRegion pRegion, String pTexto, Alineacion pAlign, int pDonde, TextureRegion pIcono, Font pFuente) {
        int margen = (int) (pCaja.getHeight() / 10.0f);
        int height = (int) (pCaja.getHeight() - ((float) (margen * 2)));
        int iconowidth = (int) (((float) height) * 0.8f);
        this.mTexto = new ChangeableText(0.0f, 0.0f, pFuente, pTexto);
        int width = (int) (((float) (margen * 2)) + this.mTexto.getWidth());
        width = pIcono != null ? width + iconowidth + margen : width;
        this.mCaja = new Sprite((float) (pAlign == Alineacion.DERECHA ? ((int) ((pCaja.getWidth() - ((float) margen)) - ((float) width))) - (pDonde * width) : margen + (pDonde * width)), pCaja.getY() + ((float) margen), (float) width, (float) height, pRegion);
        this.mTexto.setPosition((float) margen, (float) ((int) ((((float) height) - this.mTexto.getHeight()) / 2.0f)));
        this.mCaja.attachChild(this.mTexto);
        setText("0");
        if (pIcono != null) {
            this.mCaja.attachChild(new Sprite((float) ((width - iconowidth) - margen), (float) ((height - iconowidth) / 2), (float) iconowidth, (float) iconowidth, pIcono));
        }
    }

    public Sprite dameSprite() {
        return this.mCaja;
    }

    public void setText(String pTexto) {
        this.mTexto.setText(pTexto);
    }
}
