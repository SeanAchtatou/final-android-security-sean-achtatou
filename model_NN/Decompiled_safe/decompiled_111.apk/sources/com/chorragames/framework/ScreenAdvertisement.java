package com.chorragames.framework;

import android.app.Activity;
import android.os.Handler;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class ScreenAdvertisement {
    private static final String ID_DEBUG = "8E22E867AC8225A0EFC10675CA93A4F4";
    private static final String TAG = "Ads";
    final Activity activity;
    final Handler adsHandler = new Handler();
    final int advertisementId;
    final Runnable showAdsRunnable = new Runnable() {
        public void run() {
            ScreenAdvertisement.this.showAds();
        }
    };
    final Runnable unshowAdsRunnable = new Runnable() {
        public void run() {
            ScreenAdvertisement.this.unshowAds();
        }
    };

    public ScreenAdvertisement(Activity activity2, int advertisementId2) {
        this.activity = activity2;
        this.advertisementId = advertisementId2;
    }

    /* access modifiers changed from: private */
    public void showAds() {
        AdView adView = (AdView) this.activity.findViewById(this.advertisementId);
        adView.setVisibility(0);
        adView.setEnabled(true);
        AdRequest request = new AdRequest();
        request.addTestDevice(AdRequest.TEST_EMULATOR);
        adView.loadAd(request);
        adView.setAdListener(new AdListener() {
            public void onReceiveAd(Ad arg0) {
            }

            public void onPresentScreen(Ad arg0) {
            }

            public void onLeaveApplication(Ad arg0) {
            }

            public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
            }

            public void onDismissScreen(Ad arg0) {
            }
        });
    }

    /* access modifiers changed from: private */
    public void unshowAds() {
        AdView adView = (AdView) this.activity.findViewById(this.advertisementId);
        adView.setVisibility(4);
        adView.setEnabled(false);
    }

    public void showAdvertisement() {
        this.adsHandler.post(this.showAdsRunnable);
    }

    public void hideAdvertisement() {
        this.adsHandler.post(this.unshowAdsRunnable);
    }
}
