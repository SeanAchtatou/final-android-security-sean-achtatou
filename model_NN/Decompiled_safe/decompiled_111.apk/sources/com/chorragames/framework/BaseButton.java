package com.chorragames.framework;

import com.chorragames.framework.geom.Box;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.util.constants.TimeConstants;

public abstract class BaseButton extends Sprite implements TimeConstants {
    private static final String TAG = "BaseButton";
    private static final long TRIGGER_CLICK_MAXIMUM_MILLISECONDS_DEFAULT = 200;
    private Sprite mActivo;
    private long mDownTimeMilliseconds;
    private boolean mPulsado = false;
    private long mTriggerClickMaximumMilliseconds = TRIGGER_CLICK_MAXIMUM_MILLISECONDS_DEFAULT;

    public enum ButtonStatus {
        ACTIVO,
        INACTIVO
    }

    public abstract void onClick();

    public BaseButton(Box pBox, TextureRegion pActivo, TextureRegion pInactivo, Font pFont, String pString) {
        super(pBox.getX(), pBox.getY(), pInactivo);
        float height = pBox.getHeight() * 0.8f;
        float width = pBox.getWidth() * 0.8f;
        setPosition(pBox.getX() + ((pBox.getWidth() - width) / 2.0f), pBox.getY() + ((pBox.getHeight() - height) / 2.0f));
        setSize(width, height);
        if (pActivo != null) {
            this.mActivo = new Sprite(0.0f, 0.0f, width, height, pActivo);
            attachChild(this.mActivo);
            this.mActivo.setVisible(false);
        }
        if (pString.length() > 0) {
            Text text = new Text(0.0f, 0.0f, pFont, pString);
            text.setPosition((width - text.getWidth()) / 2.0f, (height - text.getHeight()) / 2.0f);
            attachChild(text);
        }
    }

    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        boolean z;
        switch (pSceneTouchEvent.getAction()) {
            case 0:
                this.mDownTimeMilliseconds = pSceneTouchEvent.getMotionEvent().getDownTime();
                setPulsado(true);
                break;
            case 1:
            case 2:
                if (pSceneTouchEvent.getAction() == 2) {
                    setPulsado(contains(pSceneTouchEvent.getX(), pSceneTouchEvent.getY()));
                }
                if (pSceneTouchEvent.getAction() == 1) {
                    z = true;
                } else {
                    z = false;
                }
                if (z && isPulsado()) {
                    setPulsado(false);
                    onClick();
                    break;
                }
                break;
        }
        return true;
    }

    public void setPulsado(boolean mPulsado2) {
        if (mPulsado2 != this.mPulsado) {
            this.mPulsado = mPulsado2;
            if (this.mActivo != null) {
                this.mActivo.setVisible(mPulsado2);
            }
        }
    }

    public boolean isPulsado() {
        return this.mPulsado;
    }
}
