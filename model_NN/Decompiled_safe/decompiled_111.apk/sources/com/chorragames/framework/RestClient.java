package com.chorragames.framework;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class RestClient {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$chorragames$framework$RestClient$RequestMethod;
    private ArrayList<NameValuePair> headers = new ArrayList<>();
    private String message;
    private ArrayList<NameValuePair> params = new ArrayList<>();
    private String response;
    private int responseCode;
    private String url;

    public enum RequestMethod {
        GET,
        POST
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$chorragames$framework$RestClient$RequestMethod() {
        int[] iArr = $SWITCH_TABLE$com$chorragames$framework$RestClient$RequestMethod;
        if (iArr == null) {
            iArr = new int[RequestMethod.values().length];
            try {
                iArr[RequestMethod.GET.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[RequestMethod.POST.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$com$chorragames$framework$RestClient$RequestMethod = iArr;
        }
        return iArr;
    }

    public String getResponse() {
        return this.response;
    }

    public String getErrorMessage() {
        return this.message;
    }

    public int getResponseCode() {
        return this.responseCode;
    }

    public RestClient(String url2) {
        this.url = url2;
    }

    public void AddParam(String name, String value) {
        this.params.add(new BasicNameValuePair(name, value));
    }

    public void AddHeader(String name, String value) {
        this.headers.add(new BasicNameValuePair(name, value));
    }

    public void Execute(RequestMethod method) throws Exception {
        switch ($SWITCH_TABLE$com$chorragames$framework$RestClient$RequestMethod()[method.ordinal()]) {
            case 1:
                String combinedParams = "";
                if (!this.params.isEmpty()) {
                    combinedParams = String.valueOf(combinedParams) + "?";
                    Iterator<NameValuePair> it = this.params.iterator();
                    while (it.hasNext()) {
                        NameValuePair p = it.next();
                        String paramString = String.valueOf(p.getName()) + "=" + URLEncoder.encode(p.getValue(), "UTF-8");
                        if (combinedParams.length() > 1) {
                            combinedParams = String.valueOf(combinedParams) + "&" + paramString;
                        } else {
                            combinedParams = String.valueOf(combinedParams) + paramString;
                        }
                    }
                }
                HttpGet request = new HttpGet(String.valueOf(this.url) + combinedParams);
                Iterator<NameValuePair> it2 = this.headers.iterator();
                while (it2.hasNext()) {
                    NameValuePair h = it2.next();
                    request.addHeader(h.getName(), h.getValue());
                }
                executeRequest(request, this.url);
                return;
            case 2:
                HttpPost request2 = new HttpPost(this.url);
                Iterator<NameValuePair> it3 = this.headers.iterator();
                while (it3.hasNext()) {
                    NameValuePair h2 = it3.next();
                    request2.addHeader(h2.getName(), h2.getValue());
                }
                if (!this.params.isEmpty()) {
                    request2.setEntity(new UrlEncodedFormEntity(this.params, "UTF-8"));
                }
                executeRequest(request2, this.url);
                return;
            default:
                return;
        }
    }

    private void executeRequest(HttpUriRequest request, String url2) {
        HttpClient client = new DefaultHttpClient();
        try {
            HttpResponse httpResponse = client.execute(request);
            this.responseCode = httpResponse.getStatusLine().getStatusCode();
            this.message = httpResponse.getStatusLine().getReasonPhrase();
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                InputStream instream = entity.getContent();
                this.response = convertStreamToString(instream);
                instream.close();
            }
        } catch (ClientProtocolException e) {
            client.getConnectionManager().shutdown();
            e.printStackTrace();
        } catch (IOException e2) {
            client.getConnectionManager().shutdown();
            e2.printStackTrace();
        }
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                try {
                    is.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                throw th;
            }
        }
        is.close();
        return sb.toString();
    }
}
