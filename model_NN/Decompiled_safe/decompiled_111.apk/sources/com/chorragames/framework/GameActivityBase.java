package com.chorragames.framework;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import org.anddev.andengine.ui.activity.LayoutGameActivity;

public abstract class GameActivityBase extends LayoutGameActivity {
    public boolean boolHidden = false;

    public abstract void onCreateActivity(Bundle bundle);

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.i("GameActivityBase", "onDestroy Called");
        super.onDestroy();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onCreateActivity(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.boolHidden = true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.boolHidden = false;
    }

    public AlertDialog DialogMessage(String title, String message, String buttontext) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message).setTitle(title).setCancelable(true).setNegativeButton(buttontext, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
        return alert;
    }

    public void LongToast(String message) {
        if (!this.boolHidden) {
            Toast.makeText(this, message, 1).show();
        }
    }

    public void ShortToast(String message) {
        if (!this.boolHidden) {
            Toast.makeText(this, message, 0).show();
        }
    }

    public void GeneralNotify(int ID, String ticker, String title, String message, int icon, int flags) {
        long when = System.currentTimeMillis();
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(), 0);
        Notification notification = new Notification(icon, ticker, when);
        notification.flags = flags;
        notification.setLatestEventInfo(this, title, message, contentIntent);
        ((NotificationManager) getSystemService("notification")).notify(ID, notification);
    }
}
