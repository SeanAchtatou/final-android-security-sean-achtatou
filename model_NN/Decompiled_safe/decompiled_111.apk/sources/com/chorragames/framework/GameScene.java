package com.chorragames.framework;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.chorragames.framework.geom.Box;
import com.chorragames.math.Main;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.primitive.Line;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.shape.RectangularShape;
import org.anddev.andengine.entity.sprite.Sprite;

public abstract class GameScene extends Scene {
    private static final String PTAG = "GameScene";
    protected GameActivityBase aBase;
    /* access modifiers changed from: private */
    public boolean bActivated;
    /* access modifiers changed from: private */
    public boolean bDead;
    private boolean bDestroying;
    /* access modifiers changed from: private */
    public boolean bFirstRun;
    private boolean bPaused;
    private Sprite blackboard;
    private final boolean mFondo;
    private final int mPuzzleId;
    protected int screenHeight;
    protected int screenWidth;
    /* access modifiers changed from: private */
    public String strName;
    private IUpdateHandler uhUpdater;

    public abstract void goBack();

    public abstract void onActivate(GameActivityBase gameActivityBase);

    public abstract void onCreateOptionsMenu(Menu menu);

    public abstract void onDeactivate();

    public abstract void onDestroy();

    public abstract void onGameUpdate(float f);

    public abstract void onLoadAudio(GameActivityBase gameActivityBase);

    public abstract void onLoadGameScene(GameActivityBase gameActivityBase);

    public abstract void onLoadSprites(GameActivityBase gameActivityBase);

    public abstract void onLoadTextures(GameActivityBase gameActivityBase);

    public abstract boolean onMenuItemSelected(int i, MenuItem menuItem);

    public abstract void onPause();

    public abstract void onPrepareOptionsMenu(Menu menu);

    public abstract void onReset();

    public abstract void onUnPause();

    public boolean isPaused() {
        return this.bPaused;
    }

    public void setPaused(boolean bPaused2) {
        this.bPaused = bPaused2;
        Log.i(PTAG, "Scene " + this.strName + ".isPaused() == " + Boolean.toString(bPaused2));
        if (!bPaused2) {
            onUnPause();
        } else {
            onPause();
        }
    }

    public GameActivityBase getBase() {
        return this.aBase;
    }

    public int getScreenWidth() {
        return this.screenWidth;
    }

    public int getScreenHeight() {
        return this.screenHeight;
    }

    public String getName() {
        return this.strName;
    }

    public boolean isDead() {
        return this.bDead;
    }

    public GameScene(GameActivityBase gab, int pLayerCount, int screenwidth, int screenheight, String name, int pPuzzleId) {
        this(gab, pLayerCount, screenwidth, screenheight, name, pPuzzleId, true);
    }

    public GameScene(GameActivityBase gab, int pLayerCount, int screenwidth, int screenheight, String name, int pPuzzleId, boolean fondo) {
        super(pLayerCount);
        this.screenWidth = 0;
        this.screenHeight = 0;
        this.strName = "";
        this.bPaused = false;
        this.bActivated = false;
        this.bFirstRun = true;
        this.bDestroying = false;
        this.bDead = false;
        Log.i(PTAG, "Scene " + name + " constructing.");
        this.screenWidth = screenwidth;
        this.screenHeight = screenheight;
        this.aBase = gab;
        this.strName = name;
        this.mFondo = fondo;
        this.mPuzzleId = pPuzzleId;
        Log.i(PTAG, "Scene " + name + " Loading Layout.");
        LoadLayout(gab);
        Log.i(PTAG, "Scene " + name + " Loading textures.");
        onLoadTextures(gab);
        Log.i(PTAG, "Scene " + name + " Loaded textures.");
        Log.i(PTAG, "Scene " + name + " Loading audio.");
        onLoadAudio(gab);
        Log.i(PTAG, "Scene " + name + " Loaded audio.");
        this.uhUpdater = new IUpdateHandler() {
            public void onUpdate(float pSecondsElapsed) {
                if (GameScene.this.bActivated && !GameScene.this.bDead) {
                    GameScene.this.Update(pSecondsElapsed);
                }
            }

            public void reset() {
            }
        };
        registerUpdateHandler(this.uhUpdater);
        Log.i(PTAG, "Scene " + this.strName + " being constructed.");
    }

    public GameScene(Main gab, int screenwidth, int screenheight, String name) {
        this(gab, 3, screenwidth, screenheight, name, 0);
    }

    /* access modifiers changed from: protected */
    public void makeDebugRectangle(RectangularShape pBox, int pIndex, float pRed, float pGreen, float pBlue) {
        Line linea = new Line(pBox.getX(), pBox.getY(), pBox.getX(), pBox.getY() + pBox.getHeight());
        linea.setColor(1.0f, 1.0f, 1.0f);
        getChild(pIndex).attachChild(linea);
        Line linea2 = new Line(pBox.getX(), pBox.getY(), pBox.getX() + pBox.getWidth(), pBox.getY());
        linea2.setColor(1.0f, 1.0f, 1.0f);
        getChild(pIndex).attachChild(linea2);
        Line linea3 = new Line(pBox.getX() + pBox.getWidth(), pBox.getY(), pBox.getX() + pBox.getWidth(), pBox.getY() + pBox.getHeight());
        linea3.setColor(1.0f, 1.0f, 1.0f);
        getChild(pIndex).attachChild(linea3);
        Line linea4 = new Line(pBox.getX(), pBox.getY() + pBox.getHeight(), pBox.getX() + pBox.getWidth(), pBox.getY() + pBox.getHeight());
        linea4.setColor(pRed, pGreen, pBlue);
        getChild(pIndex).attachChild(linea4);
    }

    public Box[] initBoxes(int[] layout) {
        Box[] ret = new Box[layout.length];
        float endHeight = 0.0f;
        int suma = 0;
        float totalHeight = (float) (this.screenHeight - 50);
        for (int i : layout) {
            suma += i;
        }
        for (int i2 = 0; i2 < layout.length; i2++) {
            float actualHeight = (((float) layout[i2]) * totalHeight) / ((float) suma);
            ret[i2] = new Box(0.0f, 0.0f + endHeight, (float) this.screenWidth, actualHeight);
            endHeight += actualHeight;
        }
        return ret;
    }

    public Box[] initBoxesIn(Box caja, int[] layout) {
        Box[] ret = new Box[layout.length];
        float endWidth = 0.0f;
        int suma = 0;
        float totalWidth = caja.getWidth();
        for (int i : layout) {
            suma += i;
        }
        for (int i2 = 0; i2 < layout.length; i2++) {
            float actualWidth = (((float) layout[i2]) * totalWidth) / ((float) suma);
            ret[i2] = new Box(endWidth, caja.getY(), actualWidth, caja.getHeight());
            endWidth += actualWidth;
        }
        return ret;
    }

    public final Rectangle creaRectangulo(Box caja, float color) {
        Rectangle rc = new Rectangle(caja.getX(), caja.getY(), caja.getWidth(), caja.getHeight());
        rc.setColor(color, color, color);
        Line ln = new Line(0.0f, 0.0f, caja.getWidth(), 0.0f);
        ln.setColor(1.0f, 1.0f, 1.0f);
        rc.attachChild(ln);
        Line ln2 = new Line(0.0f, 0.0f, 0.0f, caja.getHeight());
        ln2.setColor(1.0f, 1.0f, 1.0f);
        rc.attachChild(ln2);
        Line ln3 = new Line(0.0f, caja.getHeight() - 1.0f, caja.getWidth(), caja.getHeight() - 1.0f);
        ln3.setColor(1.0f, 1.0f, 1.0f);
        rc.attachChild(ln3);
        Line ln4 = new Line(caja.getWidth(), 0.0f, caja.getWidth(), caja.getHeight() - 1.0f);
        ln4.setColor(1.0f, 1.0f, 1.0f);
        rc.attachChild(ln4);
        return rc;
    }

    public Box[] initBoxesInV(Box caja, int[] layout) {
        Box[] ret = new Box[layout.length];
        float endHeight = 0.0f;
        int suma = 0;
        float totalHeight = caja.getHeight();
        for (int i : layout) {
            suma += i;
        }
        for (int i2 = 0; i2 < layout.length; i2++) {
            float actualHeight = (((float) layout[i2]) * totalHeight) / ((float) suma);
            ret[i2] = new Box(caja.getX(), caja.getY() + endHeight, caja.getX() + caja.getWidth(), actualHeight);
            endHeight += actualHeight;
        }
        return ret;
    }

    public void activate(GameActivityBase gab) {
        if (this.bActivated || this.bDead) {
            Log.i(PTAG, "Scene " + this.strName + " already activated.");
            return;
        }
        Log.i(PTAG, "Scene " + this.strName + " activating.");
        onActivate(gab);
        if (this.bFirstRun) {
            Log.i(PTAG, "Escena " + this.strName + " activada por primera vez.");
            this.aBase = gab;
            Log.i(PTAG, "Scene " + this.strName + " Loading sprites.");
            loadFondo();
            onLoadSprites(gab);
            Log.i(PTAG, "Scene " + this.strName + " Loaded sprites.");
            onLoadGameScene(gab);
            this.bFirstRun = false;
        }
        this.bActivated = true;
        Log.i(PTAG, "Scene " + this.strName + " activated");
    }

    private void loadFondo() {
        if (this.mFondo) {
            this.blackboard = new Sprite(0.0f, 0.0f, (float) this.screenWidth, (float) this.screenHeight, ((Main) this.aBase).getBackground());
            getChild(0).attachChild(this.blackboard);
        }
    }

    public void debugBoxes(Box[] layout, int pIndex) {
        for (Box makeDebugRectangle : layout) {
            makeDebugRectangle(makeDebugRectangle, pIndex);
        }
    }

    private void makeDebugRectangle(Box pBox, int pIndex) {
        makeDebugRectangle(pBox, pIndex, 1.0f, 1.0f, 1.0f);
    }

    private void makeDebugRectangle(Box pBox, int pIndex, float pRed, float pGreen, float pBlue) {
        Line linea = new Line(pBox.getX(), pBox.getY(), pBox.getX(), pBox.getY() + pBox.getHeight());
        linea.setColor(pRed, pGreen, pBlue);
        getChild(pIndex).attachChild(linea);
        Line linea2 = new Line(pBox.getX(), pBox.getY(), pBox.getX() + pBox.getWidth(), pBox.getY());
        linea2.setColor(pRed, pGreen, pBlue);
        getChild(pIndex).attachChild(linea2);
        Line linea3 = new Line(pBox.getX() + pBox.getWidth(), pBox.getY(), pBox.getX() + pBox.getWidth(), pBox.getY() + pBox.getHeight());
        linea3.setColor(pRed, pGreen, pBlue);
        getChild(pIndex).attachChild(linea3);
        Line linea4 = new Line(pBox.getX(), pBox.getY() + pBox.getHeight(), pBox.getX() + pBox.getWidth(), pBox.getY() + pBox.getHeight());
        linea4.setColor(pRed, pGreen, pBlue);
        getChild(pIndex).attachChild(linea4);
    }

    public void freeBoxes(Box[] layout) {
        for (int i = 0; i < layout.length; i++) {
            layout[i] = null;
        }
    }

    public void deactivate() {
        if (this.bActivated && !this.bDead) {
            Log.i(PTAG, "Scene " + this.strName + " deactivating.");
            this.bActivated = false;
            onDeactivate();
            Log.i(PTAG, "Scene " + this.strName + " deactivated.");
        }
    }

    public void resetScene() {
        if (!this.bDead) {
            Log.i(PTAG, "Scene " + this.strName + " resetting.");
            postRunnable(new Runnable() {
                public void run() {
                    Log.i(GameScene.PTAG, "Scene " + GameScene.this.strName + " Clearing sprites.");
                    for (int i = 0; i < GameScene.this.getChildCount(); i++) {
                        GameScene.this.getChild(i).detachChildren();
                    }
                    Log.i(GameScene.PTAG, "Scene " + GameScene.this.strName + " Cleared sprites.");
                    GameScene.this.onReset();
                    GameScene.this.bFirstRun = true;
                }
            });
            Log.i(PTAG, "Scene " + this.strName + " reset.");
        }
    }

    public void destroy() {
        if (!this.bDestroying && !this.bDead) {
            Log.i(PTAG, "Scene " + this.strName + " being destroyed.");
            this.bDestroying = true;
            unregisterUpdateHandler(this.uhUpdater);
            resetScene();
        }
    }

    private void onPostResetDestroy() {
        onDestroy();
        this.uhUpdater = null;
        this.aBase = null;
        this.strName = null;
        Log.i(PTAG, "Scene " + this.strName + " destroyed.");
    }

    /* access modifiers changed from: private */
    public void Update(float curtime) {
        onGameUpdate(curtime);
    }

    /* access modifiers changed from: protected */
    public void LoadLayout(GameActivityBase gab) {
    }

    public int getPuzzleId() {
        return this.mPuzzleId;
    }
}
