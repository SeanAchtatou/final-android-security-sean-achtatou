package com.game.DodgeBall_AdSence;

import oms.GameEngine.C_Lib;
import oms.GameEngine.GameEvent;

public class C_TestIco extends GameEvent {
    public static final int BUTTONBLINK = 3;
    public static final int[][] BitTestIcoEVT;
    public static final int ICOADJUSTPOSTION = 1;
    public static final int ICOBEGINFLYTOSENCE = 0;
    public static final int ICOCONTINUEBTN = 11;
    public static final int ICOCRAZYMATCH = 2;
    public static final int ICOCRAZYMATCHBTN = 7;
    public static final int ICOCRAZYMOUSE = 0;
    public static final int ICOECRAZYMATCH = 5;
    public static final int ICOECRAZYMOUSE = 3;
    public static final int ICOEHITMOUSE = 4;
    public static final int ICOFLYOUTSENCE = 4;
    public static final int ICOGAMEQUITBTN = 12;
    public static final int ICOGAMESETTINGBTN = 9;
    public static final int ICOGAMESTARTBTN = 13;
    public static final int ICOHITMOUSE = 1;
    public static final int ICOHITMOUSEBTN = 6;
    public static final int ICONEWGAMEBTN = 10;
    public static final int ICOSCORELISTBTN = 8;
    public static final int MSG_CONTINUEGAME = 7;
    public static final int MSG_CRAZYMATCH = 4;
    public static final int MSG_GAMEQUIT = 5;
    public static final int MSG_GAMESETTING = 2;
    public static final int MSG_GAMESTART = 0;
    public static final int MSG_HITMOUSE = 3;
    public static final int MSG_NEWGAME = 6;
    public static final int MSG_SCORELIST = 1;
    public static final int MSG_SELECTEDTestICO = 10;
    public static final int MSG_TestICOCLR = 9;
    public static final int MSG_TestICOREACHTARGET = 8;
    private static final int[][] TestIco00 = {TestIco0000, TestIco0000, TestIco0000, TestIco0000, TestIco0000};
    private static final int[] TestIco0000 = new int[0];
    private static final int[][] TestIco01 = {TestIco0100, TestIco0100, TestIco0100, TestIco0100, TestIco0100};
    private static final int[] TestIco0100 = new int[1];
    private static final int[][] TestIco02 = {TestIco0200, TestIco0200, TestIco0200, TestIco0200, TestIco0200};
    private static final int[] TestIco0200 = new int[1];
    private static final int[][] TestIco03 = {TestIco0300, TestIco0300, TestIco0300, TestIco0300, TestIco0300};
    private static final int[] TestIco0300 = new int[1];
    private static final int[][] TestIco04 = {TestIco0400, TestIco0400, TestIco0400, TestIco0400, TestIco0400};
    private static final int[] TestIco0400 = new int[1];
    private static final int[][] TestIco05 = {TestIco0500, TestIco0500, TestIco0500, TestIco0500, TestIco0500};
    private static final int[] TestIco0500 = new int[1];
    private static final int[][] TestIco06 = {TestIco0600, TestIco0600, TestIco0600, TestIco0601, TestIco0600};
    private static final int[] TestIco0600 = new int[1];
    private static final int[] TestIco0601 = new int[0];
    private static final int[][] TestIco07 = {TestIco0700, TestIco0700, TestIco0700, TestIco0701, TestIco0700};
    private static final int[] TestIco0700 = new int[0];
    private static final int[] TestIco0701 = new int[1];
    private static final int[][] TestIco08 = {TestIco0800, TestIco0800, TestIco0800, TestIco0801, TestIco0800};
    private static final int[] TestIco0800 = new int[1];
    private static final int[] TestIco0801 = new int[0];
    private static final int[][] TestIco09 = {TestIco0900, TestIco0900, TestIco0900, TestIco0901, TestIco0900};
    private static final int[] TestIco0900 = new int[1];
    private static final int[] TestIco0901 = new int[0];
    private static final int[][] TestIco0A = {TestIco0A00, TestIco0A00, TestIco0A00, TestIco0A01, TestIco0A00};
    private static final int[] TestIco0A00 = new int[0];
    private static final int[] TestIco0A01 = new int[0];
    private static final int[][] TestIco0B = {TestIco0B00, TestIco0B00, TestIco0B00, TestIco0B01, TestIco0B00};
    private static final int[] TestIco0B00 = new int[1];
    private static final int[] TestIco0B01 = new int[1];
    private static final int[][] TestIco0C = {TestIco0C00, TestIco0C00, TestIco0C00, TestIco0C01, TestIco0C00};
    private static final int[] TestIco0C00 = new int[1];
    private static final int[] TestIco0C01 = new int[0];
    private static final int[][] TestIco0D = {TestIco0D00, TestIco0D00, TestIco0D00, TestIco0D01, TestIco0D00};
    private static final int[] TestIco0D00 = new int[1];
    private static final int[] TestIco0D01 = new int[1];
    public static final int WAITUSEROPER = 2;
    private int DXVal;
    private int EVTType = 0;
    private C_Lib cLib;

    static {
        int[] iArr = new int[8];
        iArr[6] = 2;
        iArr[7] = 3;
        int[] iArr2 = new int[8];
        iArr2[6] = 16;
        iArr2[7] = 3;
        int[] iArr3 = new int[8];
        iArr3[6] = 16;
        iArr3[7] = 3;
        int[] iArr4 = new int[8];
        iArr4[6] = 6;
        iArr4[7] = 6;
        int[] iArr5 = new int[8];
        iArr5[0] = 1572864;
        iArr5[6] = 16;
        iArr5[7] = 1;
        BitTestIcoEVT = new int[][]{iArr, iArr2, iArr3, iArr4, iArr5};
    }

    public C_TestIco(C_Lib clib) {
        this.cLib = clib;
        this.EVT.ACTPtr = TestIco00;
        this.EVT.EVTPtr = BitTestIcoEVT;
    }

    public void SetTestIcoType(int IcoType) {
        this.EVTType = IcoType;
        switch (this.EVTType) {
            case 0:
                this.EVT.ACTPtr = TestIco00;
                return;
            case 1:
                this.EVT.ACTPtr = TestIco01;
                return;
            case 2:
                this.EVT.ACTPtr = TestIco02;
                return;
            case 3:
                this.EVT.ACTPtr = TestIco03;
                return;
            case 4:
                this.EVT.ACTPtr = TestIco04;
                return;
            case 5:
                this.EVT.ACTPtr = TestIco05;
                return;
            case 6:
                this.EVT.ACTPtr = TestIco06;
                return;
            case 7:
                this.EVT.ACTPtr = TestIco07;
                return;
            case 8:
                this.EVT.ACTPtr = TestIco08;
                return;
            case 9:
                this.EVT.ACTPtr = TestIco09;
                return;
            case 10:
                this.EVT.ACTPtr = TestIco0A;
                return;
            case 11:
                this.EVT.ACTPtr = TestIco0B;
                return;
            case 12:
                this.EVT.ACTPtr = TestIco0C;
                return;
            case 13:
                this.EVT.ACTPtr = TestIco0D;
                return;
            default:
                return;
        }
    }

    public void SetTestIcoDXVal(int xval) {
        this.DXVal = xval;
    }

    public void EVTRUN() {
        switch (this.EVT.Ctrl) {
            case 0:
                TestIco00EXE();
                return;
            case 1:
                TestIco01EXE();
                return;
            case 2:
                TestIco02EXE();
                return;
            case 3:
                TestIco03EXE();
                return;
            case 4:
                TestIco04EXE();
                return;
            default:
                return;
        }
    }

    public void TestIco00EXE() {
        this.EVT.XVal += 65536;
        int CurXVal = this.EVT.XVal >> 16;
        this.EVT.ACTPtr = TestIco00;
        if (CurXVal >= 320) {
            SetEVTCtrl(2, 0);
        }
    }

    public void TestIco01EXE() {
        this.EVT.ACTPtr = TestIco01;
    }

    public void TestIco02EXE() {
        this.EVT.ACTPtr = TestIco01;
        this.EVT.XVal -= 65536;
        if ((this.EVT.XVal >> 16) <= 0) {
            SetEVTCtrl(0, 0);
        }
    }

    public void TestIco03EXE() {
        if (CHKEVTACTEnd()) {
            switch (this.EVTType) {
                case 6:
                    this.cLib.getMessageMgr().SendMessage(this.EVTType, 3, 0, (int) System.currentTimeMillis(), this.cLib.getInput().GetTouchDownX(), this.cLib.getInput().GetTouchDownY());
                    break;
                case 7:
                    this.cLib.getMessageMgr().SendMessage(this.EVTType, 4, 0, (int) System.currentTimeMillis(), this.cLib.getInput().GetTouchDownX(), this.cLib.getInput().GetTouchDownY());
                    break;
                case 12:
                    this.cLib.getMessageMgr().SendMessage(this.EVTType, 5, 0, (int) System.currentTimeMillis(), this.cLib.getInput().GetTouchDownX(), this.cLib.getInput().GetTouchDownY());
                    break;
            }
            this.cLib.getMessageMgr().SendMessage(this.EVTType, 10, 0, (int) System.currentTimeMillis(), this.cLib.getInput().GetTouchDownX(), this.cLib.getInput().GetTouchDownY());
        }
    }

    public void TestIco04EXE() {
        if ((this.EVT.XVal >> 16) >= this.DXVal + 330) {
            EVTCLR();
            this.cLib.getMessageMgr().SendMessage(this.EVTType, 9, 0, (int) System.currentTimeMillis(), this.cLib.getInput().GetTouchDownX(), this.cLib.getInput().GetTouchDownY());
        }
    }

    public void SetButtonBlink() {
        SetEVTCtrl(3, 0);
    }

    public void SetBitTestIcoRemove() {
        SetEVTCtrl(4, 0);
    }
}
