package com.game.DodgeBall_AdSence;

import oms.GameEngine.C_Lib;
import oms.GameEngine.C_MotionEventWrapper8;
import oms.GameEngine.DataAccess;

public class C_UserRecordData {
    private static final int[] DefaultUserScore;
    private static final String FILENAME = "PlaneData.ini";
    public static final int USERRECORDMAX = 105;
    private static C_Lib cLib;
    public static int mCurIdx;
    public static byte[] mMusic = new byte[2];
    public static int[] mUserScore;

    static {
        int[] iArr = new int[114];
        iArr[3] = 100;
        DefaultUserScore = iArr;
    }

    public C_UserRecordData(C_Lib clib) {
        mUserScore = new int[USERRECORDMAX];
        cLib = clib;
        LoadRecord();
    }

    public void ResetRecord() {
        for (int i = 0; i < 105; i++) {
            mUserScore[i] = DefaultUserScore[i];
        }
        mCurIdx = 65535;
    }

    public void LoadRecord() {
        DataAccess userData = new DataAccess();
        if (userData.OpenInputFile(cLib.getMContext(), FILENAME)) {
            byte[] buffer = new byte[userData.inputFileLen];
            userData.read(buffer);
            int bufferIdx = 0;
            for (int i = 0; i < 105; i++) {
                mUserScore[i] = 0;
                for (int j = 0; j < 4; j++) {
                    int temp = (buffer[bufferIdx + j] & C_MotionEventWrapper8.ACTION_MASK) << (j * 8);
                    int[] iArr = mUserScore;
                    iArr[i] = iArr[i] + temp;
                }
                bufferIdx += 4;
            }
            mCurIdx = 65535;
            userData.CloseInputFile();
            return;
        }
        ResetRecord();
        SaveRecord();
    }

    public static void SaveRecord() {
        DataAccess userData = new DataAccess();
        byte[] buffer = new byte[420];
        int bufferIdx = 0;
        for (int i = 0; i < 105; i++) {
            int shift = C_MotionEventWrapper8.ACTION_MASK;
            for (int j = 0; j < 4; j++) {
                buffer[bufferIdx + j] = (byte) ((mUserScore[i] & shift) >> (j * 8));
                shift <<= 8;
            }
            bufferIdx += 4;
        }
        if (userData.OpenOutFile(cLib.getMContext(), FILENAME)) {
            userData.write(buffer);
            userData.CloseOutputFile();
        }
    }

    public void UpdataRecord(int cur, int score) {
        mCurIdx = USERRECORDMAX;
        mUserScore[cur] = score;
        SaveRecord();
    }
}
