package com.game.DodgeBall_AdSence;

import android.content.Context;
import android.view.KeyEvent;
import android.view.MotionEvent;
import oms.GameEngine.C_Lib;
import oms.GameEngine.GameEvent;

public class C_OPhoneApp {
    public static int DownCount = 0;
    public static int UpCount = 0;
    public static C_Lib cLib;
    private C_GameThread cGameThread = null;
    private C_MainMenu cMainMenu;

    private class C_GameThread extends Thread {
        private C_MainMenu cMainMenu;
        boolean mRun = false;

        public C_GameThread(C_Lib clib, C_MainMenu mainMenu) {
            this.cMainMenu = mainMenu;
        }

        public void setRun(boolean run) {
            this.mRun = run;
        }

        public void run() {
            this.cMainMenu.MainLoop();
            System.exit(0);
        }
    }

    public C_OPhoneApp(Context context) {
        cLib = new C_Lib(context, 2, GameEvent.V3DEVT, GameEvent.KeepHIT);
        cLib.getMediaManager().Init(20, 2);
        cLib.setFrameReflashTime(33);
        cLib.getGameCanvas().SetMaxLogicLayer(12);
        this.cMainMenu = new C_MainMenu(cLib);
    }

    public C_Lib getCLib() {
        return cLib;
    }

    public void onPause() {
        cLib.onPause();
    }

    public void onResume() {
        if (this.cGameThread == null) {
            this.cGameThread = new C_GameThread(cLib, this.cMainMenu);
            this.cGameThread.start();
        } else {
            this.cGameThread.resume();
        }
        cLib.onResume();
    }

    public void onDestory() {
        cLib.getMediaManager().release();
        boolean retry = true;
        while (retry) {
            try {
                this.cGameThread.join();
                retry = false;
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        this.cGameThread = null;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case 3:
            case 5:
            case 6:
            case C_MainMenuMemory.PadMoveFlag:
            case 82:
                return false;
            case C_MainMenuMemory.EnemyReturnFlag:
                cLib.getMediaManager().SetSystemVolume(0);
                return true;
            case C_MainMenuMemory.StarActFlag:
                cLib.getMediaManager().SetSystemVolume(1);
                return true;
            default:
                cLib.getInput().onKeyDown(keyCode);
                return true;
        }
    }

    public boolean onKeyMultiple(int keyCode, int repeatCount, KeyEvent event) {
        return true;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case 3:
            case 5:
            case 6:
            case C_MainMenuMemory.EnemyReturnFlag:
            case C_MainMenuMemory.StarActFlag:
            case C_MainMenuMemory.PadMoveFlag:
            case 82:
                return false;
            default:
                cLib.getInput().onKeyUp(keyCode);
                return true;
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int X = (int) event.getX();
        int Y = (int) event.getY();
        switch (event.getAction()) {
            case 0:
                cLib.getInput().SetTouchDown(X, Y);
                DownCount++;
                return true;
            case 1:
                cLib.getInput().SetTouchUp(X, Y);
                UpCount++;
                return true;
            case 2:
                cLib.getInput().SetTouchMove(X, Y);
                return true;
            default:
                return true;
        }
    }

    private int CM_GetNumCount(long num) {
        if (num < 10) {
            return 1;
        }
        if (num < 100) {
            return 2;
        }
        if (num < 1000) {
            return 3;
        }
        if (num < 10000) {
            return 4;
        }
        if (num < 100000) {
            return 5;
        }
        if (num < 1000000) {
            return 6;
        }
        if (num < 10000000) {
            return 7;
        }
        return 8;
    }

    public void CM_PaintNum(long num, int x, int y, int NumWidth, int Attrib, int[] ACTPtr) {
        int Count = CM_GetNumCount(num);
        int multip1 = 10;
        int multip2 = 1;
        for (int i = 0; i < Count; i++) {
            int data = (int) ((num % ((long) multip1)) / ((long) multip2));
            multip1 *= 10;
            multip2 = multip1 / 10;
            cLib.getGameCanvas().WriteSprite(ACTPtr[data], x, y, Attrib);
            x -= NumWidth;
        }
    }
}
