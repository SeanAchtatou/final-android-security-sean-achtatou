package com.game.DodgeBall_AdSence;

import java.util.Random;
import oms.GameEngine.C_Lib;
import oms.GameEngine.GameEvent;

public class C_MainMenu {
    public static final int ATTRIB_ENEMYSHOW = 2;
    public static final int ATTRIB_PLAYSHOW = 3;
    public static final int ATTRIB_STARTBOMBSHOW = 4;
    public static final int ATTRIB_UPDOWNSHOW = 1;
    public static final int[] Ball_RandomEnemyFlag = {2, 3, 5, 6};
    public static final int[] Ball_ShowBj = {R.drawable.game_bj0, R.drawable.game_bj1, R.drawable.game_bj2, R.drawable.game_bj3};
    public static final int GAME_GAME_CURLESS = 0;
    public static final int GAME_GAME_LESS = 58;
    public static final int GAME_GAME_TOTAL = 48;
    public static final int GAME_MENU_LESS = 1;
    public static final int GAME_MENU_MAIN = 0;
    public static final int MAKE_ADDSPEED_FALG = 7;
    public static final int MAKE_ALL_END = 118;
    public static final int MAKE_ALL_STR = 108;
    public static final int MAKE_BALLGOD_FALG = 9;
    public static final int MAKE_BALLUP_FALG = 10;
    public static final int MAKE_BLOCK_END = 62;
    public static final int MAKE_BLOCK_FALG = 0;
    public static final int MAKE_BLOCK_STR = 2;
    public static final int MAKE_DECSPEED_FALG = 8;
    public static final int MAKE_ENEMY0_FALG = 2;
    public static final int MAKE_ENEMY1_FALG = 3;
    public static final int MAKE_ENEMY2_FALG = 5;
    public static final int MAKE_ENEMY3_FALG = 6;
    public static final int MAKE_FIRE_FALG = 1;
    public static final int MAKE_HAND_FALG = 13;
    public static final int MAKE_HITFLY_FALG = 15;
    public static final int MAKE_OTHER_END = 120;
    public static final int MAKE_OTHER_STR = 118;
    public static final int MAKE_PLANE_END = 1;
    public static final int MAKE_PLANE_STR = 0;
    public static final int MAKE_RETURN_FALG = 11;
    public static final int MAKE_SCR_END = 2;
    public static final int MAKE_SCR_STR = 1;
    public static final int MAKE_SPEED_END = 108;
    public static final int MAKE_SPEED_STR = 106;
    public static final int MAKE_STAR_END = 92;
    public static final int MAKE_STAR_FALG = 4;
    public static final int MAKE_STAR_STR = 62;
    public static final int MAKE_TOOL_END = 106;
    public static final int MAKE_TOOL_STR = 92;
    public static final int MAKE_UPDOWN_FALG = 12;
    public static final int MENUPADTIME = 10;
    public static final int[][] MenuRun_TouchX = {new int[]{165, 216}, new int[]{37, 87}, new int[]{72, 118}, new int[]{22, 67}, new int[]{13, 65}, new int[]{100, C_MainMenuMemory.BALLTOTAL}};
    public static final int[][] MenuRun_TouchY;
    private static final int[][] MenuSecondRun_TouchX = {new int[]{72, 228}, new int[]{72, 229}, new int[]{5, 423}};
    private static final int[][] MenuSecondRun_TouchY;
    public static final int NULL_FALG = 88;
    public static final int PLANE_NULL = 88;
    public static final int PREEKEYTIME = 3;
    public static final int[] Plane_NumberAct = {R.drawable.act_number03, R.drawable.act_number04, R.drawable.act_number05, R.drawable.act_number06, R.drawable.act_number07, R.drawable.act_number08, R.drawable.act_number09, R.drawable.act_number0a, R.drawable.act_number0b, R.drawable.act_number0c};
    public static final float[] Plane_ScoreCmp = {0.0f, 10.0f, 100.0f, 1000.0f, 10000.0f, 100000.0f, 1000000.0f};
    public static final int ROAD_BOMBSHOW = 8;
    public static final int ROAD_GRASSSHOW = 7;
    public static final int ROAD_LANDSHOW = 3;
    public static final int ROAD_MOUNDSHOW = 2;
    public static final int ROAD_PLANESHOW = 7;
    public static final int ROAD_SCORESHOW = 9;
    public static final int ROAD_SHOTSHOW = 1;
    public static final int ROAD_SMOKESHOW = 6;
    public static final int ROAD_TOOLSHOW = 1;
    public static final int ROAD_TREEHITGASSHOW = 1;
    public static final int ROAD_TREESHOW = 1;
    public static final int ROAD_WATERSHOW = 7;
    public static final int SAVE_CHALLENGE_SCORE = 3;
    public static final int SAVE_CHALLENGE_SCORE_1 = 104;
    public static final int SAVE_DRM = 0;
    public static final int SAVE_GAMESTR = 2;
    public static final int SAVE_GAME_HELP = 53;
    public static final int SAVE_GAME_SCORECUREND = 103;
    public static final int SAVE_GAME_SCORECURSTR = 55;
    public static final int SAVE_GAME_SCOREEND = 52;
    public static final int SAVE_GAME_SCORESTR = 4;
    public static final int SAVE_MEDIA = 1;
    public static final int SAVE_MENU_LEVEL = 54;
    public static final int[] StarShowX = {209, 127, 51};
    public static final int[] StarShowY = {65, 136, 209, 280};
    public static final int[] StoneShowX = {248, 165, 88};
    public static final int[] StoneShowY = {86, 155, 228, 297};
    public static C_Lib cLib;
    public static C_MainMenuMemory cMemory;
    public static final Random m_random = new Random();
    private C_Media cMedia;
    private C_UserRecordData cUserRecordData;

    static {
        int[] iArr = new int[2];
        iArr[1] = 52;
        MenuRun_TouchY = new int[][]{new int[]{174, 301}, new int[]{174, 301}, new int[]{432, 480}, new int[]{432, 479}, iArr, new int[]{174, 301}};
        int[] iArr2 = new int[2];
        iArr2[1] = 448;
        MenuSecondRun_TouchY = new int[][]{new int[]{73, 218}, new int[]{278, 423}, iArr2};
    }

    public C_MainMenu(C_Lib clib) {
        cLib = clib;
        cMemory = new C_MainMenuMemory();
        this.cMedia = new C_Media(clib);
        this.cUserRecordData = new C_UserRecordData(clib);
        InitEVENT();
        C_Media.Initialize();
    }

    private void Logo() {
        cLib.getGameCanvas().LoadText(R.drawable.scr_logo, 0, 0);
        cLib.getGameCanvas().SetTextYVal(0, -48);
        cLib.ViewOpen(30);
        cLib.getGameCanvas().InitACT(0, R.raw.opeapp);
        cLib.ViewDark(30);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x011b  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0124  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void MainLoop() {
        /*
            r12 = this;
            r3 = 0
            r12.Logo()
            r6 = 0
            r5 = 0
            r4 = 60
            r0 = 0
            r2 = 0
            oms.GameEngine.C_Lib r7 = com.game.DodgeBall_AdSence.C_MainMenu.cLib
            oms.GameEngine.GameCanvas r7 = r7.getGameCanvas()
            r8 = -48
            r9 = -96
            r10 = 48
            r11 = 96
            r7.SetViewRect(r8, r9, r10, r11)
            r7 = 0
            r12.Ball_Activity(r7)
            r12.SaveDrmMusic()
            int[] r7 = com.game.DodgeBall_AdSence.C_UserRecordData.mUserScore
            r8 = 54
            r7 = r7[r8]
            com.game.DodgeBall_AdSence.C_MainMenuMemory.SaveMenuLevelStop = r7
        L_0x002a:
            oms.GameEngine.C_Lib r7 = com.game.DodgeBall_AdSence.C_MainMenu.cLib
            r7.ClearACT()
            oms.GameEngine.C_Lib r7 = com.game.DodgeBall_AdSence.C_MainMenu.cLib
            oms.GameEngine.InputInterface r7 = r7.getInput()
            r7.ReadTouch()
            oms.GameEngine.C_Lib r7 = com.game.DodgeBall_AdSence.C_MainMenu.cLib
            oms.GameEngine.InputInterface r7 = r7.getInput()
            r7.ReadKeyBoard()
            int r7 = com.game.DodgeBall_AdSence.C_MainMenuMemory.mMenuCtrl
            switch(r7) {
                case 0: goto L_0x0064;
                case 1: goto L_0x0046;
                case 2: goto L_0x0070;
                case 3: goto L_0x0074;
                case 4: goto L_0x0086;
                case 5: goto L_0x008a;
                case 6: goto L_0x00b4;
                case 7: goto L_0x00bb;
                case 8: goto L_0x00f2;
                case 9: goto L_0x0046;
                case 10: goto L_0x0046;
                case 11: goto L_0x0046;
                case 12: goto L_0x0130;
                case 13: goto L_0x0139;
                default: goto L_0x0046;
            }
        L_0x0046:
            r12.GameKeyBack()
            oms.GameEngine.C_Lib r7 = com.game.DodgeBall_AdSence.C_MainMenu.cLib
            r7.WaitBLK()
            int[] r7 = com.game.DodgeBall_AdSence.C_MainMenuMemory.BallValPtr
            r8 = 20
            r7 = r7[r8]
            r8 = 1
            if (r7 != r8) goto L_0x002a
            com.game.DodgeBall_AdSence.C_UserRecordData r7 = r12.cUserRecordData
            r8 = 54
            int r9 = com.game.DodgeBall_AdSence.C_MainMenuMemory.SaveMenuLevelStop
            r7.UpdataRecord(r8, r9)
            r12.GameQuit()
            return
        L_0x0064:
            r12.MenuInit()
            r7 = 2
            com.game.DodgeBall_AdSence.C_MainMenuMemory.mMenuCtrl = r7
            r12.MenuOpenScr()
            r7 = 0
            com.game.DodgeBall_AdSence.C_MainMenuMemory.MenuMusicFlag = r7
        L_0x0070:
            r12.MenuRun_Show()
            goto L_0x0046
        L_0x0074:
            r7 = 0
            com.game.DodgeBall_AdSence.C_MainMenuMemory.SaveHelpFlag = r7
            r7 = 0
            com.game.DodgeBall_AdSence.C_MainMenuMemory.GameOpenScrFlag = r7
            r7 = 0
            com.game.DodgeBall_AdSence.C_MainMenuMemory.PlayPreeFlag = r7
            r7 = 4
            com.game.DodgeBall_AdSence.C_MainMenuMemory.mMenuCtrl = r7
            r7 = 0
            com.game.DodgeBall_AdSence.C_MainMenuMemory.GameTotalScore = r7
            r12.MakeScrBJB()
        L_0x0086:
            r12.MenuSecondRun_Show()
            goto L_0x0046
        L_0x008a:
            r7 = 6
            com.game.DodgeBall_AdSence.C_MainMenuMemory.mMenuCtrl = r7
            r7 = 0
            com.game.DodgeBall_AdSence.C_MainMenuMemory.PreeOtherCurKey = r7
            r7 = 88
            com.game.DodgeBall_AdSence.C_MainMenuMemory.PreeCurKey = r7
            int[] r7 = com.game.DodgeBall_AdSence.C_UserRecordData.mUserScore
            r8 = 2
            r7 = r7[r8]
            com.game.DodgeBall_AdSence.C_MainMenuMemory.GameCurChoice = r7
            r7 = 0
            com.game.DodgeBall_AdSence.C_MainMenuMemory.BuyFlag = r7
            r7 = 0
            com.game.DodgeBall_AdSence.C_MainMenuMemory.PauseSmail = r7
            r7 = 0
            com.game.DodgeBall_AdSence.C_MainMenuMemory.PauseTime = r7
            r12.Ball_MainChoiceGameInit()
            r12.Ball_MenuAssess_Init()
            int[] r7 = com.game.DodgeBall_AdSence.C_MainMenuMemory.BallValPtr
            r8 = 28
            r9 = 0
            r7[r8] = r9
            r7 = 0
            com.game.DodgeBall_AdSence.C_MainMenuMemory.MenuMusicFlag = r7
        L_0x00b4:
            r12.Main_ChoiceGameRun_Show()
            r12.MenuRun()
            goto L_0x0046
        L_0x00bb:
            r7 = 7
            com.game.DodgeBall_AdSence.C_MainMenuMemory.SaveHelpJsr = r7
            int r7 = com.game.DodgeBall_AdSence.C_MainMenuMemory.SaveHelpFlag
            if (r7 != 0) goto L_0x00d2
            int r1 = r12.GreeHelpWait()
            r7 = 1
            if (r1 != r7) goto L_0x00d2
            r12.HelpOpenScr()
            r7 = 13
            com.game.DodgeBall_AdSence.C_MainMenuMemory.mMenuCtrl = r7
            goto L_0x0046
        L_0x00d2:
            int r7 = com.game.DodgeBall_AdSence.C_MainMenuMemory.MenuMusicFlag
            if (r7 != 0) goto L_0x00da
            r7 = 0
            com.game.DodgeBall_AdSence.C_Media.StopMusic(r7)
        L_0x00da:
            r12.GameInitFun()
            r12.MakeTitleEVT()
            int r7 = com.game.DodgeBall_AdSence.C_MainMenuMemory.MenuMusicFlag
            if (r7 != 0) goto L_0x00e8
            r7 = 2
            com.game.DodgeBall_AdSence.C_Media.PlayMusic(r7)
        L_0x00e8:
            r7 = 1
            com.game.DodgeBall_AdSence.C_MainMenuMemory.MenuMusicFlag = r7
            r7 = 8
            com.game.DodgeBall_AdSence.C_MainMenuMemory.mMenuCtrl = r7
            r12.Ball_ActivityShow()
        L_0x00f2:
            r12.MakePause()
            r12.PauseShow()
            r12.GameRunOpenScr()
            r12.Save_HighScore_JSR()
            r12.GameTouch()
            r12.GameEndJC()
            r12.Plane_ShowScore()
            r12.MenuRun()
            oms.GameEngine.C_Lib r7 = com.game.DodgeBall_AdSence.C_MainMenu.cLib
            oms.GameEngine.InputInterface r7 = r7.getInput()
            r7.ClearTouchDown()
            int[] r7 = com.game.DodgeBall_AdSence.C_MainMenuMemory.BallValPtr
            r8 = 19
            r7 = r7[r8]
            if (r7 != 0) goto L_0x011e
            r12.BallMakEvt()
        L_0x011e:
            int r7 = com.game.DodgeBall_AdSence.C_MainMenuMemory.mMenuCtrl
            r8 = 9
            if (r7 != r8) goto L_0x0127
            r12.GameExit_JC()
        L_0x0127:
            int[] r7 = com.game.DodgeBall_AdSence.C_MainMenuMemory.BallValPtr
            r8 = 25
            r9 = 0
            r7[r8] = r9
            goto L_0x0046
        L_0x0130:
            r12.HelpOpenScr()
            r7 = 13
            com.game.DodgeBall_AdSence.C_MainMenuMemory.mMenuCtrl = r7
            goto L_0x0046
        L_0x0139:
            oms.GameEngine.C_Lib r7 = com.game.DodgeBall_AdSence.C_MainMenu.cLib
            oms.GameEngine.InputInterface r7 = r7.getInput()
            boolean r7 = r7.CHKTouchDown()
            if (r7 == 0) goto L_0x0046
            int r7 = com.game.DodgeBall_AdSence.C_MainMenuMemory.SaveHelpJsr
            if (r7 != 0) goto L_0x0152
            r7 = 0
            com.game.DodgeBall_AdSence.C_MainMenuMemory.mMenuCtrl = r7
        L_0x014c:
            r7 = 0
            com.game.DodgeBall_AdSence.C_Media.PlaySound(r7)
            goto L_0x0046
        L_0x0152:
            r7 = 1
            com.game.DodgeBall_AdSence.C_MainMenuMemory.SaveHelpFlag = r7
            r7 = 7
            com.game.DodgeBall_AdSence.C_MainMenuMemory.mMenuCtrl = r7
            goto L_0x014c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.game.DodgeBall_AdSence.C_MainMenu.MainLoop():void");
    }

    public void GameQuit() {
        ExitEVENT();
        cLib.getMediaManager().release();
    }

    private void InitEVENT() {
        if (cMemory.MenuIcoEVT == null) {
            cMemory.MenuIcoEVT = new C_MenuIco[C_MainMenuData.MENUICOEVTMAX];
            for (int i = 0; i < 124; i++) {
                cMemory.MenuIcoEVT[i] = new C_MenuIco(cLib);
            }
        }
    }

    private void ExitEVENT() {
        for (int i = 0; i < 124; i++) {
            if (cMemory.MenuIcoEVT != null) {
                cMemory.MenuIcoEVT[i].EVTCLR();
            }
        }
    }

    private void ExecEVENT() {
        for (int i = 0; i < 124; i++) {
            if (cMemory.MenuIcoEVT[i].EVT.Valid) {
                cMemory.MenuIcoEVT[i].ExecEVT(cLib.getGameCanvas());
                cMemory.MenuIcoEVT[i].ExecRUN(cLib.getGameCanvas());
            }
        }
    }

    private void ShowEVENT() {
        for (int i = 0; i < 124; i++) {
            if (cMemory.MenuIcoEVT[i].EVT.Valid) {
                cMemory.MenuIcoEVT[i].ShowEVENT(cLib.getGameCanvas());
            }
        }
    }

    private void MakeTitleEVT() {
        int YVal;
        int YVal2;
        if (0 < 1) {
            cMemory.MenuIcoEVT[0].Angle = 0;
            cMemory.MenuIcoEVT[0].Flag = 0;
            cMemory.MenuIcoEVT[0].CURACT = 0;
            cMemory.MenuIcoEVT[0].FlySpeedFlag = 0;
            if (C_MainMenuMemory.BallValPtr[12] == 0) {
                cMemory.MenuIcoEVT[0].MakeEVENT(GameEvent.KeepACT, 112, 0);
            } else {
                cMemory.MenuIcoEVT[0].MakeEVENT(GameEvent.KeepACT, 368, 0);
            }
            cMemory.MenuIcoEVT[0].SetMenuIcoType(0);
            cMemory.MenuIcoEVT[0].EVT.Status |= 9728;
            cMemory.MenuIcoEVT[0].EVT.Attrib = 3;
            cMemory.MenuIcoEVT[0].EVT.Ctrl = 0;
            cMemory.MenuIcoEVT[0].EVT.Flag = 0;
            cMemory.MenuIcoEVT[0].EVTCur = 0;
            if (0 == 2) {
                cMemory.MenuIcoEVT[0].Angle = 0;
            } else {
                cMemory.MenuIcoEVT[0].Angle = 1;
            }
        }
        if (1 < 2) {
            cMemory.MenuIcoEVT[1].Angle = 0;
            cMemory.MenuIcoEVT[1].CURACT = 0;
            cMemory.MenuIcoEVT[1].FlySpeedFlag = 0;
            cMemory.MenuIcoEVT[1].MakeEVENT(96, 0, 0);
            cMemory.MenuIcoEVT[1].SetMenuIcoType(4);
            cMemory.MenuIcoEVT[1].EVT.Status |= 9728;
            cMemory.MenuIcoEVT[1].EVT.Attrib = 3;
            cMemory.MenuIcoEVT[1].EVT.Ctrl = 4;
            cMemory.MenuIcoEVT[1].EVT.Flag = 1;
            cMemory.MenuIcoEVT[1].EVTCur = 1;
        }
        if (118 < 120) {
            cMemory.MenuIcoEVT[118].Angle = 0;
            cMemory.MenuIcoEVT[118].Flag = 0;
            cMemory.MenuIcoEVT[118].CURACT = 0;
            cMemory.MenuIcoEVT[118].FlySpeedFlag = 0;
            if (C_MainMenuMemory.BallValPtr[12] == 0) {
                cMemory.MenuIcoEVT[118].MakeEVENT(180, -32, 0);
            } else {
                cMemory.MenuIcoEVT[118].MakeEVENT(180, GameEvent.KeepADC, 0);
            }
            cMemory.MenuIcoEVT[118].SetMenuIcoType(9);
            cMemory.MenuIcoEVT[118].EVT.Ctrl = 9;
            cMemory.MenuIcoEVT[118].EVT.Status |= 9728;
            cMemory.MenuIcoEVT[118].EVT.Attrib = 3;
            cMemory.MenuIcoEVT[118].EVT.Flag = 118;
            cMemory.MenuIcoEVT[118].EVTCur = 118;
        }
        if (C_MainMenuMemory.BallValPtr[12] == 0) {
            YVal = -16;
        } else {
            YVal = 496;
        }
        BallMakEvt_JSR(2, 95, YVal, 0, 2);
        cMemory.MenuIcoEVT[2].EVT.Ctrl = 1;
        cMemory.MenuIcoEVT[2].SetMenuIcoType(1);
        int i = 2 + 1;
        if (C_MainMenuMemory.BallValPtr[12] == 0) {
            YVal2 = -48;
        } else {
            YVal2 = 528;
        }
        BallMakEvt_JSR(i, 95, YVal2, 0, 2);
        cMemory.MenuIcoEVT[i].EVT.Ctrl = 1;
        cMemory.MenuIcoEVT[i].SetMenuIcoType(1);
    }

    private void MenuOpenScr() {
        cLib.getGameCanvas().LoadText(R.drawable.menubj, 0, 0);
        cLib.getGameCanvas().SetTextXVal(0, 0);
        cLib.getGameCanvas().SetTextYVal(0, -48);
        cLib.ViewOpen(36);
    }

    private void GameTouch() {
        for (int Sum = 0; Sum < 1; Sum++) {
            if (cMemory.MenuIcoEVT[Sum].EVT.Valid) {
                int i = cMemory.MenuIcoEVT[Sum].EVT.XVal >> 16;
                int i2 = cMemory.MenuIcoEVT[Sum].EVT.YVal >> 16;
                if (cMemory.MenuIcoEVT[Sum].EVT.Flag == Sum) {
                    for (int Total = 2; Total < 62; Total++) {
                        cLib.getGameCanvas().CHKACTTouch(R.drawable.act_ballkt00, GameEvent.KeepACT, 112, cMemory.MenuIcoEVT[Total].EVT.ACTIdx, cMemory.MenuIcoEVT[Total].EVT.XVal >> 16, cMemory.MenuIcoEVT[Total].EVT.YVal >> 16);
                    }
                }
            }
        }
    }

    private void MenuInit() {
        ExitEVENT();
        for (int i = 0; i < 150; i++) {
            C_MainMenuMemory.BallValPtr[i] = 0;
        }
    }

    private void BallMakEvt() {
        int Flag;
        int YVal;
        int XVal;
        int y;
        int ChoiceLimit = C_MainMenuMemory.BallValPtr[14];
        int ChoiceLevel = C_MainMenuMemory.GameLevel;
        if (C_MainMenuData.Ball_MakeXYTotal[ChoiceLimit][ChoiceLevel][(C_MainMenuMemory.BallValPtr[2] * 3) + 2] == 65535 && ChoiceLimit == 0) {
            C_MainMenuMemory.GameLevel++;
            if (C_MainMenuMemory.GameLevel >= 45) {
                C_MainMenuMemory.GameLevel = 27;
            }
            int i = C_MainMenuMemory.OpenBjFlag;
            if (C_MainMenuMemory.GameLevel == 32) {
                C_MainMenuMemory.OpenBjFlag = 3;
            } else if (C_MainMenuMemory.GameLevel == 22) {
                C_MainMenuMemory.OpenBjFlag = 2;
            } else if (C_MainMenuMemory.GameLevel == 10) {
                C_MainMenuMemory.OpenBjFlag = 1;
            }
            if (i != C_MainMenuMemory.OpenBjFlag) {
                cLib.getGameCanvas().LoadText(Ball_ShowBj[i], 0, 0);
                cLib.getGameCanvas().SetTextXVal(0, 0);
                cLib.getGameCanvas().SetTextYVal(0, -48);
                cLib.ViewOpen(36);
            }
            ChoiceLevel = C_MainMenuMemory.GameLevel;
            int i2 = C_MainMenuMemory.BallValPtr[2];
            C_MainMenuMemory.BallValPtr[6] = 1;
            int i3 = C_MainMenuMemory.BallValPtr[4];
            C_MainMenuMemory.BallValPtr[2] = 4;
            if (C_MainMenuMemory.BallValPtr[12] == 0) {
                y = 112;
            } else {
                y = 368;
            }
            cMemory.MenuIcoEVT[0].EVT.YVal = y << 16;
            cMemory.MenuIcoEVT[1].EVT.YVal = 0 << 16;
            C_MainMenuMemory.BallValPtr[4] = 0;
        }
        int AddScrY = MyPocketPlane.BallScrAddY;
        while (true) {
            if (C_MainMenuMemory.BallValPtr[12] == 0) {
                int Cur = C_MainMenuMemory.BallValPtr[2] * 3;
                int YVal2 = C_MainMenuData.Ball_MakeXYTotal[ChoiceLimit][ChoiceLevel][Cur + 2];
                if (YVal2 != 65535) {
                    if (C_MainMenuMemory.BallValPtr[6] == 1) {
                        YVal2 += AddScrY + 480;
                    }
                    if ((-(cMemory.MenuIcoEVT[1].EVT.YVal >> 16)) + AddScrY + GameEvent.KeepADC >= YVal2) {
                        Flag = C_MainMenuData.Ball_MakeXYTotal[ChoiceLimit][ChoiceLevel][Cur];
                        int XVal2 = C_MainMenuData.Ball_MakeXYTotal[ChoiceLimit][ChoiceLevel][Cur + 1];
                        YVal = C_MainMenuData.Ball_MakeXYTotal[ChoiceLimit][ChoiceLevel][Cur + 2];
                        if (C_MainMenuMemory.BallValPtr[6] == 1) {
                            YVal += AddScrY + 480;
                        }
                        if (YVal >= AddScrY + 495) {
                            YVal -= -C_MainMenuMemory.BallValPtr[4];
                        }
                        XVal = XVal2 - C_MainMenuMemory.BallValPtr[3];
                    } else {
                        return;
                    }
                } else if (C_MainMenuMemory.BallValPtr[14] == 1) {
                    C_MainMenuMemory.BallValPtr[17] = 1;
                    return;
                } else {
                    return;
                }
            } else {
                int Cur2 = C_MainMenuMemory.BallValPtr[2] * 3;
                int YVal3 = C_MainMenuData.Ball_MakeXYTotal[ChoiceLimit][ChoiceLevel][Cur2 + 2];
                if (YVal3 != 65535) {
                    if (C_MainMenuMemory.BallValPtr[6] == 1) {
                        YVal3 += AddScrY + 480;
                    }
                    if ((cMemory.MenuIcoEVT[1].EVT.YVal >> 16) + AddScrY + GameEvent.KeepADC >= YVal3) {
                        Flag = C_MainMenuData.Ball_MakeXYTotal[ChoiceLimit][ChoiceLevel][Cur2];
                        int XVal3 = C_MainMenuData.Ball_MakeXYTotal[ChoiceLimit][ChoiceLevel][Cur2 + 1];
                        int YVal4 = C_MainMenuData.Ball_MakeXYTotal[ChoiceLimit][ChoiceLevel][Cur2 + 2];
                        if (C_MainMenuMemory.BallValPtr[6] == 1) {
                            YVal4 += AddScrY + 480;
                        }
                        if (YVal4 >= AddScrY + 495) {
                            YVal4 -= C_MainMenuMemory.BallValPtr[4];
                        }
                        YVal = 480 - YVal4;
                        XVal = XVal3 - C_MainMenuMemory.BallValPtr[3];
                    } else {
                        return;
                    }
                } else if (C_MainMenuMemory.BallValPtr[14] == 1) {
                    C_MainMenuMemory.BallValPtr[17] = 1;
                    return;
                } else {
                    return;
                }
            }
            int[] iArr = C_MainMenuMemory.BallValPtr;
            iArr[2] = iArr[2] + 1;
            switch (Flag) {
                case 0:
                    int i4 = 2;
                    while (true) {
                        if (i4 >= 62) {
                            break;
                        } else if (!cMemory.MenuIcoEVT[i4].EVT.Valid) {
                            BallMakEvt_JSR(i4, XVal, YVal, 0, 2);
                            cMemory.MenuIcoEVT[i4].EVT.Ctrl = 1;
                            cMemory.MenuIcoEVT[i4].SetMenuIcoType(1);
                            break;
                        } else {
                            i4++;
                        }
                    }
                case 1:
                    int i5 = 92;
                    while (true) {
                        if (i5 >= 106) {
                            break;
                        } else if (!cMemory.MenuIcoEVT[i5].EVT.Valid) {
                            BallMakEvt_JSR(i5, XVal, YVal, 1, 2);
                            cMemory.MenuIcoEVT[i5].EVT.Ctrl = 3;
                            cMemory.MenuIcoEVT[i5].SetMenuIcoType(3);
                            cMemory.MenuIcoEVT[i5].CURACT = 4;
                            break;
                        } else {
                            i5++;
                        }
                    }
                case 2:
                case 3:
                case 5:
                case 6:
                    int i6 = 92;
                    while (true) {
                        if (i6 >= 106) {
                            break;
                        } else if (!cMemory.MenuIcoEVT[i6].EVT.Valid) {
                            BallMakEvt_JSR(i6, XVal, YVal, 2, 2);
                            cMemory.MenuIcoEVT[i6].EVT.Ctrl = 3;
                            cMemory.MenuIcoEVT[i6].SetMenuIcoType(3);
                            cMemory.MenuIcoEVT[i6].CURACT = Ball_RandomEnemyFlag[Random(4)];
                            break;
                        } else {
                            i6++;
                        }
                    }
                case 4:
                    int i7 = 62;
                    while (true) {
                        if (i7 >= 92) {
                            break;
                        } else if (!cMemory.MenuIcoEVT[i7].EVT.Valid) {
                            BallMakEvt_JSR(i7, XVal, YVal, 4, 2);
                            cMemory.MenuIcoEVT[i7].EVT.Ctrl = 2;
                            cMemory.MenuIcoEVT[i7].CURACT = 0;
                            cMemory.MenuIcoEVT[i7].CurTime = 0;
                            cMemory.MenuIcoEVT[i7].SetMenuIcoType(2);
                            break;
                        } else {
                            i7++;
                        }
                    }
                case 7:
                    int i8 = 106;
                    while (true) {
                        if (i8 >= 108) {
                            break;
                        } else if (!cMemory.MenuIcoEVT[i8].EVT.Valid) {
                            BallMakEvt_JSR(i8, XVal, YVal, 7, 2);
                            cMemory.MenuIcoEVT[i8].EVT.Ctrl = 5;
                            cMemory.MenuIcoEVT[i8].SetMenuIcoType(5);
                            cMemory.MenuIcoEVT[i8].CURACT = 7;
                            break;
                        } else {
                            i8++;
                        }
                    }
                case 8:
                    int i9 = 106;
                    while (true) {
                        if (i9 >= 108) {
                            break;
                        } else if (!cMemory.MenuIcoEVT[i9].EVT.Valid) {
                            BallMakEvt_JSR(i9, XVal, YVal, 8, 2);
                            cMemory.MenuIcoEVT[i9].EVT.Ctrl = 5;
                            cMemory.MenuIcoEVT[i9].SetMenuIcoType(5);
                            cMemory.MenuIcoEVT[i9].CURACT = 8;
                            break;
                        } else {
                            i9++;
                        }
                    }
                case 9:
                    int i10 = 108;
                    while (true) {
                        if (i10 >= 118) {
                            break;
                        } else if (!cMemory.MenuIcoEVT[i10].EVT.Valid) {
                            BallMakEvt_JSR(i10, XVal, YVal, 9, 2);
                            cMemory.MenuIcoEVT[i10].EVT.Ctrl = 6;
                            cMemory.MenuIcoEVT[i10].SetMenuIcoType(6);
                            cMemory.MenuIcoEVT[i10].CURACT = 9;
                            break;
                        } else {
                            i10++;
                        }
                    }
                case 10:
                    int i11 = 108;
                    while (true) {
                        if (i11 >= 118) {
                            break;
                        } else if (!cMemory.MenuIcoEVT[i11].EVT.Valid) {
                            BallMakEvt_JSR(i11, XVal, YVal, 10, 2);
                            cMemory.MenuIcoEVT[i11].EVT.Ctrl = 7;
                            cMemory.MenuIcoEVT[i11].SetMenuIcoType(7);
                            cMemory.MenuIcoEVT[i11].CURACT = 10;
                            break;
                        } else {
                            i11++;
                        }
                    }
                case 11:
                    if (C_MainMenuMemory.BallValPtr[24] != 0) {
                        break;
                    } else {
                        int i12 = 108;
                        while (true) {
                            if (i12 >= 118) {
                                break;
                            } else if (!cMemory.MenuIcoEVT[i12].EVT.Valid) {
                                BallMakEvt_JSR(i12, XVal, YVal, 11, 2);
                                cMemory.MenuIcoEVT[i12].EVT.Ctrl = 11;
                                cMemory.MenuIcoEVT[i12].SetMenuIcoType(11);
                                cMemory.MenuIcoEVT[i12].CURACT = 11;
                                if (C_MainMenuMemory.BallValPtr[12] != 0) {
                                    cMemory.MenuIcoEVT[i12].CURACT = 1;
                                    break;
                                } else {
                                    cMemory.MenuIcoEVT[i12].CURACT = 0;
                                    break;
                                }
                            } else {
                                i12++;
                            }
                        }
                    }
                case 12:
                    if (C_MainMenuMemory.BallValPtr[24] != 0) {
                        break;
                    } else {
                        int i13 = 108;
                        while (true) {
                            if (i13 >= 118) {
                                break;
                            } else if (!cMemory.MenuIcoEVT[i13].EVT.Valid) {
                                BallMakEvt_JSR(i13, XVal, YVal, 12, 1);
                                cMemory.MenuIcoEVT[i13].EVT.Ctrl = 12;
                                cMemory.MenuIcoEVT[i13].SetMenuIcoType(12);
                                cMemory.MenuIcoEVT[i13].CURACT = 12;
                                cMemory.MenuIcoEVT[i13].FlySpeedFlag = 0;
                                cMemory.MenuIcoEVT[i13].AddXSpeed = 0;
                                break;
                            } else {
                                i13++;
                            }
                        }
                    }
                case 13:
                    if (C_MainMenuMemory.BallValPtr[24] != 0) {
                        break;
                    } else {
                        int i14 = 108;
                        while (true) {
                            if (i14 >= 118) {
                                break;
                            } else if (!cMemory.MenuIcoEVT[i14].EVT.Valid) {
                                BallMakEvt_JSR(i14, XVal, YVal, 13, 2);
                                cMemory.MenuIcoEVT[i14].EVT.Ctrl = 13;
                                cMemory.MenuIcoEVT[i14].SetMenuIcoType(13);
                                cMemory.MenuIcoEVT[i14].CURACT = 13;
                                cMemory.MenuIcoEVT[i14].FlySpeedFlag = 0;
                                cMemory.MenuIcoEVT[i14].AddXSpeed = 0;
                                break;
                            } else {
                                i14++;
                            }
                        }
                    }
            }
        }
    }

    private void BallMakEvt_JSR(int i, int XVal, int YVal, int Flag, int Attrib) {
        cMemory.MenuIcoEVT[i].MakeEVENT(XVal + 1, YVal, 0);
        cMemory.MenuIcoEVT[i].EVT.Status |= 9728;
        cMemory.MenuIcoEVT[i].EVT.Attrib = Attrib;
        cMemory.MenuIcoEVT[i].EVT.Flag = Flag;
        cMemory.MenuIcoEVT[i].EVTCur = i;
        cMemory.MenuIcoEVT[i].EVT.YInc = cMemory.MenuIcoEVT[1].EVT.YInc;
        cMemory.MenuIcoEVT[i].EVT.YAdc = cMemory.MenuIcoEVT[1].EVT.YAdc;
        cMemory.MenuIcoEVT[i].EVT.XInc = cMemory.MenuIcoEVT[1].EVT.XInc;
        cMemory.MenuIcoEVT[i].EVT.XAdc = cMemory.MenuIcoEVT[1].EVT.XAdc;
    }

    public int Random(int Ran) {
        return m_random.nextInt(Ran) % Ran;
    }

    private void MenuRun_Show() {
        cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt0a, 267, 240, 7);
        int AddScrY = MyPocketPlane.BallScrAddY;
        int[] iArr = C_MainMenuMemory.BallValPtr;
        iArr[16] = iArr[16] + 7;
        if (C_MainMenuMemory.BallValPtr[16] >= AddScrY + 496) {
            C_MainMenuMemory.BallValPtr[16] = -10 - AddScrY;
        }
        int[] iArr2 = C_MainMenuMemory.BallValPtr;
        iArr2[29] = iArr2[29] + 10;
        if (C_MainMenuMemory.BallValPtr[29] > 360) {
            int[] iArr3 = C_MainMenuMemory.BallValPtr;
            iArr3[29] = iArr3[29] - 360;
        }
        cLib.getGameCanvas().WriteSprite(R.drawable.act_ballenda06, 27, C_MainMenuMemory.BallValPtr[16], 7, (float) C_MainMenuMemory.BallValPtr[29], 1.0f);
        if (C_MainMenuMemory.MoreGamePreeFlag == 1) {
            C_MainMenuMemory.PreeShowTime++;
            if (C_MainMenuMemory.PreeShowTime >= 3) {
                C_MainMenuMemory.MoreGamePreeFlag = 0;
                C_MainMenuMemory.PreeShowTime = 0;
                C_UserRecordData.mUserScore[0] = C_MainMenuMemory.SongPreeFlag;
                C_UserRecordData.mUserScore[1] = C_MainMenuMemory.MusicPreeFlag;
                MyPocketPlane activity = (MyPocketPlane) cLib.GetActivity();
                if (activity != null) {
                    activity.clickMoreGames();
                }
            }
            cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt03, 64, 234, 7);
            cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt00, 64, 234, 7);
        } else {
            cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt03, 64, 234, 7);
        }
        if (C_MainMenuMemory.PlayPreeFlag == 1) {
            C_MainMenuMemory.PreeShowTime++;
            if (C_MainMenuMemory.PreeShowTime >= 3) {
                C_MainMenuMemory.PlayPreeFlag = 0;
                C_MainMenuMemory.PreeShowTime = 0;
                C_UserRecordData.mUserScore[0] = C_MainMenuMemory.SongPreeFlag;
                C_UserRecordData.mUserScore[1] = C_MainMenuMemory.MusicPreeFlag;
                C_MainMenuMemory.GameMenuChoice = 0;
                C_MainMenuMemory.BallValPtr[14] = 1;
                C_MainMenuMemory.mMenuCtrl = 5;
            }
            cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt01, 191, 234, 7);
            cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt00, 191, 234, 7);
        } else {
            cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt01, 191, 234, 7);
        }
        if (C_MainMenuMemory.HelpFlag == 1) {
            C_MainMenuMemory.PreeShowTime++;
            if (C_MainMenuMemory.PreeShowTime >= 3) {
                C_MainMenuMemory.PlayPreeFlag = 0;
                C_MainMenuMemory.PreeShowTime = 0;
                C_MainMenuMemory.HelpFlag = 0;
                C_MainMenuMemory.mMenuCtrl = 12;
            }
            cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt0b, 39, 25 - AddScrY, 7);
            cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt09, 39, 25 - AddScrY, 7);
        } else {
            cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt0b, 39, 25 - AddScrY, 7);
        }
        if (C_MainMenuMemory.SongPreeFlag == 1) {
            cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt08, 96, AddScrY + 454, 7);
        } else {
            cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt06, 96, AddScrY + 454, 7);
        }
        if (C_MainMenuMemory.MusicPreeFlag == 1) {
            cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt07, 44, AddScrY + 454, 7);
        } else {
            cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt05, 44, AddScrY + 454, 7);
        }
        if (C_MainMenuMemory.EndLessFlag == 1) {
            C_MainMenuMemory.PreeShowTime++;
            if (C_MainMenuMemory.PreeShowTime >= 3) {
                C_MainMenuMemory.PlayPreeFlag = 0;
                C_MainMenuMemory.PreeShowTime = 0;
                C_MainMenuMemory.EndLessFlag = 0;
                C_UserRecordData.mUserScore[0] = C_MainMenuMemory.SongPreeFlag;
                C_UserRecordData.mUserScore[1] = C_MainMenuMemory.MusicPreeFlag;
                C_MainMenuMemory.GameMenuChoice = 1;
                C_MainMenuMemory.GameLevel = 0;
                C_MainMenuMemory.BallValPtr[14] = 0;
                C_MainMenuMemory.mMenuCtrl = 7;
            }
            cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt02, 126, 234, 7);
            cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt00, 126, 234, 7);
        } else {
            cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt02, 126, 234, 7);
        }
        MenuRunTouch();
    }

    private void MenuRunTouch() {
        int Sum = 0;
        int z = 0;
        int AddScrY = MyPocketPlane.BallScrAddY;
        if (C_MainMenuMemory.PreeShowTime == 0) {
            if (cLib.getInput().CHKTouchDown()) {
                int mTouchX = cLib.getInput().GetTouchDownX();
                int mTouchY = cLib.getInput().GetTouchDownY();
                Sum = 0;
                while (true) {
                    if (Sum <= 5) {
                        int x = MenuRun_TouchX[Sum][0];
                        int x1 = MenuRun_TouchX[Sum][1];
                        int y = MenuRun_TouchY[Sum][0];
                        int y1 = MenuRun_TouchY[Sum][1];
                        if (Sum == 2 || Sum == 3) {
                            y += AddScrY;
                            y1 += AddScrY;
                        }
                        if (Sum == 4) {
                            y -= AddScrY;
                            y1 -= AddScrY;
                        }
                        if (mTouchX >= x && mTouchX <= x1 && mTouchY >= y && mTouchY <= y1) {
                            z = 1;
                            break;
                        }
                        Sum++;
                    } else {
                        break;
                    }
                }
            }
            if (z == 1) {
                switch (Sum) {
                    case 0:
                        C_MainMenuMemory.PlayPreeFlag = 1;
                        break;
                    case 1:
                        C_MainMenuMemory.MoreGamePreeFlag = 1;
                        break;
                    case 2:
                        if (C_MainMenuMemory.SongPreeFlag == 0) {
                            C_MainMenuMemory.SongPreeFlag = 1;
                        } else {
                            C_MainMenuMemory.SongPreeFlag = 0;
                        }
                        if (C_MainMenuMemory.SongPreeFlag != 0) {
                            C_OPhoneApp.cLib.getMediaManager().SetSoundEnable(false);
                            break;
                        } else {
                            C_OPhoneApp.cLib.getMediaManager().SetSoundEnable(true);
                            break;
                        }
                    case 3:
                        if (C_MainMenuMemory.MusicPreeFlag == 0) {
                            C_MainMenuMemory.MusicPreeFlag = 1;
                        } else {
                            C_MainMenuMemory.MusicPreeFlag = 0;
                        }
                        if (C_MainMenuMemory.MusicPreeFlag != 0) {
                            C_Media.StopMusic(0);
                            C_OPhoneApp.cLib.getMediaManager().SetMediaEnable(false);
                            break;
                        } else {
                            C_OPhoneApp.cLib.getMediaManager().SetMediaEnable(true);
                            if (!(C_MainMenuMemory.mMenuCtrl == 8 || C_MainMenuMemory.mMenuCtrl == 9 || C_MainMenuMemory.mMenuCtrl == 7)) {
                                C_Media.PlayMusic(0);
                                break;
                            }
                        }
                    case 4:
                        C_MainMenuMemory.SaveHelpJsr = 0;
                        C_MainMenuMemory.HelpFlag = 1;
                        break;
                    case 5:
                        C_MainMenuMemory.EndLessFlag = 1;
                        break;
                }
                C_Media.PlaySound(0);
            }
        }
    }

    private void MakeScrBJB() {
        if (C_MainMenuMemory.GameOpenScrFlag == 0) {
            C_MainMenuMemory.GameOpenScrFlag = 1;
            cLib.getGameCanvas().LoadText(R.drawable.menuscrb, 0, 0);
            cLib.getGameCanvas().SetTextXVal(0, 0);
            cLib.getGameCanvas().SetTextYVal(0, -48);
            cLib.ViewOpen(36);
        }
    }

    private void MenuSecondRun_Show() {
    }

    private void MenuSecondRunTouch() {
        if (C_MainMenuMemory.PreeShowTime == 0 && cLib.getInput().CHKTouchDown()) {
            int mTouchX = cLib.getInput().GetTouchDownX();
            int mTouchY = cLib.getInput().GetTouchDownY();
            int Sum = 0;
            while (Sum <= 2) {
                int x = MenuSecondRun_TouchX[Sum][0];
                int x1 = MenuSecondRun_TouchX[Sum][1];
                int y = MenuSecondRun_TouchY[Sum][0];
                int y1 = MenuSecondRun_TouchY[Sum][1];
                if (mTouchX < x || mTouchX > x1 || mTouchY < y || mTouchY > y1) {
                    Sum++;
                } else {
                    C_MainMenuMemory.PlayPreeFlag = Sum + 1;
                    if (Sum == 2) {
                        C_Media.PlaySound(0);
                        return;
                    } else {
                        C_Media.PlaySound(0);
                        return;
                    }
                }
            }
        }
    }

    private void GameChoiceTouch() {
        if (cLib.getInput().CHKTouchDown()) {
            int mTouchX = cLib.getInput().GetTouchDownX();
            int mTouchY = cLib.getInput().GetTouchDownY();
            int Sum = 0;
            while (true) {
                if (Sum > 4) {
                    break;
                }
                int x = C_MainMenuData.GameChoicePadX_Other[Sum][0];
                int x1 = C_MainMenuData.GameChoicePadX_Other[Sum][1];
                int y = C_MainMenuData.GameChoicePadY_Other[Sum][0];
                int y1 = C_MainMenuData.GameChoicePadY_Other[Sum][1];
                if (mTouchX < x || mTouchX > x1 || mTouchY < y || mTouchY > y1) {
                    Sum++;
                } else {
                    if (Sum < 4) {
                        C_MainMenuMemory.GameChoiceBigKey = Sum;
                    } else {
                        C_MainMenuMemory.PreeOtherCurKey = Sum;
                    }
                    C_Media.PlaySound(0);
                }
            }
            if (Sum == 5) {
                for (int Sum2 = 0; Sum2 <= 2; Sum2++) {
                    int x2 = C_MainMenuData.GameChoicePadX[Sum2][0];
                    int x12 = C_MainMenuData.GameChoicePadX[Sum2][1];
                    int Total = 0;
                    while (true) {
                        if (Total <= 3) {
                            int y2 = C_MainMenuData.GameChoicePadY[Total][0];
                            int y12 = C_MainMenuData.GameChoicePadY[Total][1];
                            if (mTouchX >= x2 && mTouchX <= x12 && mTouchY >= y2 && mTouchY <= y12) {
                                C_MainMenuMemory.PreeCurKey = (Sum2 * 4) + Total;
                                C_Media.PlaySound(0);
                                break;
                            }
                            Total++;
                        } else {
                            break;
                        }
                    }
                }
            }
        }
    }

    private void Main_ChoiceGameRun_Show() {
        cLib.getGameCanvas().WriteSprite(R.drawable.act_menuktb01, 320, -48, 4);
        cLib.getGameCanvas().WriteSprite(R.drawable.act_menuktb00, 0, -48, 4);
        cLib.getGameCanvas().WriteSprite(R.drawable.act_menukt25, 289, 228, 4);
        C_MainMenuMemory.MenuUpDownTime++;
        if (C_MainMenuMemory.MenuUpDownTime >= 18) {
            C_MainMenuMemory.MenuUpDownTime = 0;
        }
        if (C_MainMenuMemory.MenuUpDownTime <= 9) {
            cLib.getGameCanvas().WriteSprite(R.drawable.act_menuktb02, 13, 79, 4);
        } else {
            cLib.getGameCanvas().WriteSprite(R.drawable.act_menuktb03, 13, 79, 4);
        }
        if (C_MainMenuMemory.PreeOtherCurKey == 1) {
            if (C_MainMenuMemory.BallValPtr[26] <= C_MainMenuMemory.GameCurChoice) {
                C_MainMenuMemory.PreeShowTime++;
                if (C_MainMenuMemory.PreeShowTime >= 14) {
                    C_MainMenuMemory.PreeShowTime = 0;
                    C_MainMenuMemory.mMenuCtrl = 7;
                    C_MainMenuMemory.GameLevel = C_MainMenuMemory.BallValPtr[26];
                    C_MainMenuMemory.SaveMenuLevelStop = C_MainMenuMemory.BallValPtr[26];
                }
                int CurXVal = cMemory.MenuIcoEVT[0].EVT.XVal >> 16;
                int CurYVal = cMemory.MenuIcoEVT[0].EVT.YVal >> 16;
                int CmpX = CurXVal - (C_MainMenuMemory.BallValPtr[26] * 59);
                if (CmpX >= 0 && CmpX <= 320 && (C_MainMenuMemory.PreeShowTime <= 3 || C_MainMenuMemory.PreeShowTime >= 8)) {
                    cLib.getGameCanvas().WriteSprite(R.drawable.act_menudw00, CmpX, CurYVal, 3);
                }
            } else {
                C_MainMenuMemory.PreeShowTime++;
                if (C_MainMenuMemory.PreeShowTime >= 14) {
                    C_MainMenuMemory.PreeShowTime = 0;
                    C_MainMenuMemory.PreeOtherCurKey = 0;
                }
                int CurXVal2 = cMemory.MenuIcoEVT[0].EVT.XVal >> 16;
                int CurYVal2 = cMemory.MenuIcoEVT[0].EVT.YVal >> 16;
                int CmpX2 = CurXVal2 - (C_MainMenuMemory.BallValPtr[26] * 59);
                if (CmpX2 >= 0 && CmpX2 <= 320 && (C_MainMenuMemory.PreeShowTime <= 3 || C_MainMenuMemory.PreeShowTime >= 8)) {
                    cLib.getGameCanvas().WriteSprite(R.drawable.act_menudw00, CmpX2, CurYVal2, 3);
                }
            }
        }
        if (C_MainMenuMemory.PreeOtherCurKey == 2) {
            C_MainMenuMemory.PreeShowTime++;
            if (C_MainMenuMemory.PreeShowTime >= 3) {
                C_MainMenuMemory.PreeShowTime = 0;
                C_MainMenuMemory.mMenuCtrl = 0;
            }
            cLib.getGameCanvas().WriteSprite(R.drawable.act_menukt23, 285, 416, 7);
            cLib.getGameCanvas().WriteSprite(R.drawable.act_menukt24, 285, 416, 7);
            return;
        }
        cLib.getGameCanvas().WriteSprite(R.drawable.act_menukt23, 285, 416, 7);
    }

    private void MakePause() {
        int AddScrY = MyPocketPlane.BallScrAddY;
        cLib.getGameCanvas().WriteSprite(R.drawable.act_number23, 15, AddScrY + 464, 7);
        if (C_MainMenuMemory.MakePauseFlag == 0 && cLib.getInput().CHKTouchDown()) {
            int mTouchX = cLib.getInput().GetTouchDownX();
            int mTouchY = cLib.getInput().GetTouchDownY();
            int y = AddScrY + 436;
            int y1 = AddScrY + 479;
            if (mTouchX >= 0 && mTouchX <= 38 && mTouchY >= y && mTouchY <= y1) {
                Ball_Activity(1);
                C_MainMenuMemory.PauseFlag = 1;
                C_MainMenuMemory.PauseTime = 0;
                C_MainMenuMemory.PauseSmail = 0.1f;
                C_MainMenuMemory.MakePauseFlag = 1;
                C_Media.PlayPauseMusic(2);
                C_Media.PlaySound(0);
            }
        }
    }

    private void PauseShow() {
        if (C_MainMenuMemory.PauseFlag >= 1) {
            PauseTouch();
            if (C_MainMenuMemory.PauseSmail < 1.0f && C_MainMenuMemory.PauseTime == 0) {
                C_MainMenuMemory.PauseSmail = (float) (((double) C_MainMenuMemory.PauseSmail) + 0.1d);
            }
            if (C_MainMenuMemory.PauseTime >= 1) {
                C_MainMenuMemory.PauseSmail = (float) (((double) C_MainMenuMemory.PauseSmail) - 0.1d);
                if (((double) C_MainMenuMemory.PauseSmail) <= 0.4d) {
                    if (C_MainMenuMemory.PauseFlag == 2) {
                        C_MainMenuMemory.PauseFlag = 0;
                        if (C_MainMenuMemory.MusicPreeFlag == 1) {
                            C_MainMenuMemory.MenuMusicFlag = 0;
                        } else {
                            C_MainMenuMemory.MenuMusicFlag = 1;
                            if (C_UserRecordData.mUserScore[1] != C_MainMenuMemory.MusicPreeFlag) {
                                C_Media.PlayMusic(2);
                            } else {
                                C_Media.PlayResumeMusic(2);
                            }
                        }
                        C_UserRecordData.mUserScore[0] = C_MainMenuMemory.SongPreeFlag;
                        C_UserRecordData.mUserScore[1] = C_MainMenuMemory.MusicPreeFlag;
                    }
                    if (C_MainMenuMemory.PauseFlag == 3) {
                        C_MainMenuMemory.PauseFlag = 0;
                        C_Media.StopAllSound();
                        C_MainMenuMemory.GameEndFlag = 3;
                        C_MainMenuMemory.GameCurWinLost = 1;
                        if (C_MainMenuMemory.MusicPreeFlag == 1) {
                            C_MainMenuMemory.MenuMusicFlag = 0;
                        } else {
                            C_MainMenuMemory.MenuMusicFlag = 1;
                            if (C_UserRecordData.mUserScore[1] != C_MainMenuMemory.MusicPreeFlag) {
                                C_Media.PlayMusic(2);
                            } else {
                                C_Media.PlayResumeMusic(2);
                            }
                        }
                        C_UserRecordData.mUserScore[0] = C_MainMenuMemory.SongPreeFlag;
                        C_UserRecordData.mUserScore[1] = C_MainMenuMemory.MusicPreeFlag;
                        C_MainMenuMemory.mMenuCtrl = 9;
                    }
                    if (C_MainMenuMemory.PauseFlag == 4) {
                        C_MainMenuMemory.PauseFlag = 0;
                        C_Media.StopAllSound();
                        C_MainMenuMemory.GameEndFlag = 4;
                        C_MainMenuMemory.GameCurWinLost = 1;
                        C_UserRecordData.mUserScore[0] = C_MainMenuMemory.SongPreeFlag;
                        C_UserRecordData.mUserScore[1] = C_MainMenuMemory.MusicPreeFlag;
                        C_MainMenuMemory.mMenuCtrl = 9;
                        if (C_MainMenuMemory.MusicPreeFlag == 0) {
                            C_Media.PlayMusic(0);
                        }
                    }
                    C_MainMenuMemory.PauseSmail = 0.0f;
                    C_MainMenuMemory.PauseTime = 0;
                    C_MainMenuMemory.MakePauseFlag = 0;
                    Ball_Activity(0);
                }
            }
            Ball_BlackShow();
            int x = (int) (160.0f + (73.0f * C_MainMenuMemory.PauseSmail));
            if (C_MainMenuMemory.PauseFlag == 2) {
                C_MainMenuMemory.PauseTime++;
                cLib.getGameCanvas().WriteSprite(R.drawable.act_pausekt03, x, 236, 9, 0.0f, C_MainMenuMemory.PauseSmail);
                cLib.getGameCanvas().WriteSprite(R.drawable.act_pausekt02, x, 236, 9, 0.0f, C_MainMenuMemory.PauseSmail);
            } else {
                cLib.getGameCanvas().WriteSprite(R.drawable.act_pausekt03, x, 236, 9, 0.0f, C_MainMenuMemory.PauseSmail);
            }
            int x2 = (int) (160.0f + (18.0f * C_MainMenuMemory.PauseSmail));
            if (C_MainMenuMemory.PauseFlag == 3) {
                C_MainMenuMemory.PauseTime++;
                cLib.getGameCanvas().WriteSprite(R.drawable.act_pausekt01, x2, 236, 9, 0.0f, C_MainMenuMemory.PauseSmail);
                cLib.getGameCanvas().WriteSprite(R.drawable.act_pausekt02, x2, 236, 9, 0.0f, C_MainMenuMemory.PauseSmail);
            } else {
                cLib.getGameCanvas().WriteSprite(R.drawable.act_pausekt01, x2, 236, 9, 0.0f, C_MainMenuMemory.PauseSmail);
            }
            int x3 = (int) (160.0f - (39.0f * C_MainMenuMemory.PauseSmail));
            if (C_MainMenuMemory.PauseFlag == 4) {
                C_MainMenuMemory.PauseTime++;
                cLib.getGameCanvas().WriteSprite(R.drawable.act_pausekt00, x3, 236, 9, 0.0f, C_MainMenuMemory.PauseSmail);
                cLib.getGameCanvas().WriteSprite(R.drawable.act_pausekt02, x3, 236, 9, 0.0f, C_MainMenuMemory.PauseSmail);
            } else {
                cLib.getGameCanvas().WriteSprite(R.drawable.act_pausekt00, x3, 236, 9, 0.0f, C_MainMenuMemory.PauseSmail);
            }
            int x4 = (int) (160.0f - (89.0f * C_MainMenuMemory.PauseSmail));
            int y = (int) (236.0f - (63.0f * C_MainMenuMemory.PauseSmail));
            if (C_MainMenuMemory.SongPreeFlag == 0) {
                cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt06, x4, y, 9, 0.0f, C_MainMenuMemory.PauseSmail);
            } else {
                cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt08, x4, y, 9, 0.0f, C_MainMenuMemory.PauseSmail);
            }
            int x5 = (int) (160.0f - (89.0f * C_MainMenuMemory.PauseSmail));
            int y2 = (int) (236.0f + (56.0f * C_MainMenuMemory.PauseSmail));
            if (C_MainMenuMemory.MusicPreeFlag == 0) {
                cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt05, x5, y2, 9, 0.0f, C_MainMenuMemory.PauseSmail);
            } else {
                cLib.getGameCanvas().WriteSprite(R.drawable.act_ballmenukt07, x5, y2, 9, 0.0f, C_MainMenuMemory.PauseSmail);
            }
        }
    }

    private void PauseTouch() {
        if (cLib.getInput().CHKTouchDown()) {
            int mTouchX = cLib.getInput().GetTouchDownX();
            int mTouchY = cLib.getInput().GetTouchDownY();
            int Sum = 0;
            while (Sum <= 4) {
                int x = C_MainMenuData.PausePadX[Sum][0];
                int x1 = C_MainMenuData.PausePadX[Sum][1];
                int y = C_MainMenuData.PausePadY[Sum][0];
                int y1 = C_MainMenuData.PausePadY[Sum][1];
                if (mTouchX < x || mTouchX > x1 || mTouchY < y || mTouchY > y1) {
                    Sum++;
                } else {
                    if (Sum <= 2) {
                        C_MainMenuMemory.PauseFlag = Sum + 2;
                    }
                    if (Sum == 3) {
                        if (C_MainMenuMemory.SongPreeFlag == 0) {
                            C_MainMenuMemory.SongPreeFlag = 1;
                        } else {
                            C_MainMenuMemory.SongPreeFlag = 0;
                        }
                        if (C_MainMenuMemory.SongPreeFlag == 0) {
                            C_OPhoneApp.cLib.getMediaManager().SetSoundEnable(true);
                        } else {
                            C_OPhoneApp.cLib.getMediaManager().SetSoundEnable(false);
                        }
                    }
                    if (Sum == 4) {
                        if (C_MainMenuMemory.MusicPreeFlag == 0) {
                            C_MainMenuMemory.MusicPreeFlag = 1;
                        } else {
                            C_MainMenuMemory.MusicPreeFlag = 0;
                        }
                        if (C_MainMenuMemory.MusicPreeFlag == 0) {
                            C_OPhoneApp.cLib.getMediaManager().SetMediaEnable(true);
                            if (!(C_MainMenuMemory.mMenuCtrl == 8 || C_MainMenuMemory.mMenuCtrl == 9 || C_MainMenuMemory.mMenuCtrl == 7)) {
                                C_Media.PlayMusic(0);
                            }
                        } else {
                            C_Media.StopMusic(0);
                            C_OPhoneApp.cLib.getMediaManager().SetMediaEnable(false);
                        }
                        C_Media.StopMusic(2);
                    }
                    C_Media.PlaySound(0);
                    return;
                }
            }
        }
    }

    private void MenuRun() {
        if (C_MainMenuMemory.PauseFlag == 0) {
            ExecEVENT();
        }
        ShowEVENT();
    }

    private void GameRunOpenScr() {
        int i;
        if (C_MainMenuMemory.GameOpenScrFlag == 0) {
            C_MainMenuMemory.GameOpenScrFlag = 1;
            if (C_MainMenuMemory.GameLevel >= 36) {
                i = 3;
            } else if (C_MainMenuMemory.GameLevel >= 24) {
                i = 2;
            } else if (C_MainMenuMemory.GameLevel >= 12) {
                i = 1;
            } else {
                i = 0;
            }
            cLib.getGameCanvas().LoadText(Ball_ShowBj[i], 0, 0);
            cLib.getGameCanvas().SetTextXVal(0, 0);
            cLib.getGameCanvas().SetTextYVal(0, -48);
            cLib.ViewOpen(36);
        }
    }

    private void Save_HighScore_JSR() {
        if (C_MainMenuMemory.SaveScoreFlag == 1) {
            C_MainMenuMemory.SaveScoreFlag = 2;
            if (C_MainMenuMemory.GameLevel < 48 && C_MainMenuMemory.GameCurWinLost == 0 && C_MainMenuMemory.GameCurChoice < C_MainMenuMemory.GameLevel + 1) {
                C_MainMenuMemory.GameCurChoice = C_MainMenuMemory.GameLevel + 1;
                C_UserRecordData.mUserScore[2] = C_MainMenuMemory.GameCurChoice;
                C_MainMenuMemory.AssessStr[C_MainMenuMemory.GameCurChoice] = 0;
            }
            if (C_MainMenuMemory.GameMenuChoice == 0) {
                if (C_MainMenuMemory.GameCurWinLost == 0) {
                    if (C_MainMenuMemory.GameBestScore < C_MainMenuMemory.GameTotalScore) {
                        C_UserRecordData.mUserScore[C_MainMenuMemory.GameLevel + 4] = (int) C_MainMenuMemory.GameTotalScore;
                        if (C_MainMenuMemory.GameTotalScore >= ((float) C_MainMenuData.Plane_BestScore[C_MainMenuMemory.GameLevel])) {
                            C_MainMenuMemory.SaveNewAssess = 3;
                            C_MainMenuMemory.AssessStr[C_MainMenuMemory.GameLevel] = 3;
                        } else {
                            if (C_MainMenuMemory.GameTotalScore >= ((float) ((int) (((float) C_MainMenuData.Plane_BestScore[C_MainMenuMemory.GameLevel]) * 0.9f)))) {
                                C_MainMenuMemory.SaveNewAssess = 3;
                                C_MainMenuMemory.AssessStr[C_MainMenuMemory.GameLevel] = 3;
                            } else {
                                C_MainMenuMemory.SaveNewAssess = 2;
                                C_MainMenuMemory.AssessStr[C_MainMenuMemory.GameLevel] = 2;
                            }
                        }
                    } else {
                        if (C_MainMenuMemory.GameTotalScore >= ((float) C_MainMenuData.Plane_BestScore[C_MainMenuMemory.GameLevel])) {
                            C_MainMenuMemory.SaveNewAssess = 3;
                        } else {
                            if (C_MainMenuMemory.GameTotalScore >= ((float) ((int) (((float) C_MainMenuData.Plane_BestScore[C_MainMenuMemory.GameLevel]) * 0.9f)))) {
                                C_MainMenuMemory.SaveNewAssess = 3;
                            } else {
                                C_MainMenuMemory.SaveNewAssess = 2;
                            }
                        }
                    }
                }
                this.cUserRecordData.UpdataRecord(C_MainMenuMemory.GameLevel + 55, (int) C_MainMenuMemory.GameTotalScore);
            } else if (C_MainMenuMemory.GameBestScore < C_MainMenuMemory.GameTotalScore) {
                C_UserRecordData.mUserScore[3] = (int) (C_MainMenuMemory.GameTotalScore % 10000.0f);
                this.cUserRecordData.UpdataRecord(SAVE_CHALLENGE_SCORE_1, (int) (C_MainMenuMemory.GameTotalScore / 10000.0f));
            }
        }
    }

    private void GameExit_JC() {
        ExitEVENT();
        Ball_Activity(0);
        if (C_MainMenuMemory.GameMenuChoice == 0) {
            if (C_MainMenuMemory.GameEndFlag == 3) {
                C_MainMenuMemory.mMenuCtrl = 7;
                C_MainMenuMemory.GameEndFlag = 0;
            }
            if (C_MainMenuMemory.GameEndFlag == 4) {
                C_MainMenuMemory.mMenuCtrl = 5;
                C_MainMenuMemory.GameEndFlag = 0;
                if (C_MainMenuMemory.MusicPreeFlag == 0) {
                    C_Media.PlayMusic(0);
                }
            }
            if (C_MainMenuMemory.GameEndFlag == 5) {
                C_MainMenuMemory.mMenuCtrl = 7;
                if (C_MainMenuMemory.GameLevel >= 47) {
                    C_MainMenuMemory.mMenuCtrl = 5;
                    if (C_MainMenuMemory.MusicPreeFlag == 0) {
                        C_Media.PlayMusic(0);
                    }
                } else {
                    C_MainMenuMemory.GameLevel++;
                }
                C_MainMenuMemory.SaveMenuLevelStop = C_MainMenuMemory.GameLevel;
                C_MainMenuMemory.GameEndFlag = 0;
            }
            C_MainMenuMemory.PauseFlag = 0;
        } else if (C_MainMenuMemory.GameEndFlag == 3) {
            C_MainMenuMemory.GameLevel = 0;
            C_MainMenuMemory.GameEndFlag = 0;
            C_MainMenuMemory.mMenuCtrl = 7;
            C_MainMenuMemory.GameTotalScore = 0.0f;
            C_MainMenuMemory.GameEndFlag = 0;
        } else {
            C_MainMenuMemory.mMenuCtrl = 0;
            if (C_MainMenuMemory.MusicPreeFlag == 0) {
                C_Media.PlayMusic(0);
            }
            C_MainMenuMemory.GameTotalScore = 0.0f;
            C_MainMenuMemory.GameEndFlag = 0;
        }
    }

    private void HelpOpenScr() {
        cLib.getGameCanvas().LoadText(R.drawable.help, 0, 0);
        cLib.getGameCanvas().SetTextXVal(0, 0);
        cLib.getGameCanvas().SetTextYVal(0, -48);
        cLib.ViewOpen(36);
    }

    public int GreeHelpWait() {
        int i = C_UserRecordData.mUserScore[53];
        if (i >= 3) {
            return 0;
        }
        C_UserRecordData.mUserScore[53] = i + 1;
        C_MainMenuMemory.mMenuCtrl = 12;
        C_MainMenuMemory.SaveHelpFlag = 0;
        return 1;
    }

    public void SaveDrmMusic() {
        C_MainMenuMemory.MusicPreeFlag = C_UserRecordData.mUserScore[1];
        C_MainMenuMemory.SongPreeFlag = C_UserRecordData.mUserScore[0];
        if (C_MainMenuMemory.SongPreeFlag == 0) {
            C_OPhoneApp.cLib.getMediaManager().SetSoundEnable(true);
        } else {
            C_OPhoneApp.cLib.getMediaManager().SetSoundEnable(false);
        }
        if (C_MainMenuMemory.MusicPreeFlag == 0) {
            C_OPhoneApp.cLib.getMediaManager().SetMediaEnable(true);
            C_Media.PlayMusic(0);
            return;
        }
        C_OPhoneApp.cLib.getMediaManager().SetMediaEnable(false);
    }

    public void GameKeyBack() {
        if (cLib.getInput().CHKUpKey(4)) {
            if (C_MainMenuMemory.mMenuCtrl == 0 || C_MainMenuMemory.mMenuCtrl == 2) {
                C_UserRecordData.mUserScore[0] = C_MainMenuMemory.SongPreeFlag;
                C_UserRecordData.mUserScore[1] = C_MainMenuMemory.MusicPreeFlag;
                this.cUserRecordData.UpdataRecord(1, C_MainMenuMemory.MusicPreeFlag);
                C_MainMenuMemory.BallValPtr[20] = 1;
            }
            if (C_MainMenuMemory.mMenuCtrl == 3 || C_MainMenuMemory.mMenuCtrl == 4) {
                C_MainMenuMemory.PlayPreeFlag = 0;
                C_MainMenuMemory.PreeShowTime = 0;
                C_MainMenuMemory.GameMenuChoice = 0;
                C_MainMenuMemory.mMenuCtrl = 0;
            }
            if (C_MainMenuMemory.mMenuCtrl == 5 || C_MainMenuMemory.mMenuCtrl == 6) {
                C_MainMenuMemory.PreeShowTime = 0;
                C_MainMenuMemory.mMenuCtrl = 0;
            }
            if ((C_MainMenuMemory.mMenuCtrl == 7 || C_MainMenuMemory.mMenuCtrl == 8 || C_MainMenuMemory.mMenuCtrl == 9) && C_MainMenuMemory.MakePauseFlag == 0) {
                C_MainMenuMemory.PauseFlag = 1;
                C_MainMenuMemory.PauseTime = 0;
                C_MainMenuMemory.PauseSmail = 0.1f;
                C_MainMenuMemory.MakePauseFlag = 1;
                C_Media.StopMusic(2);
                C_Media.PlaySound(0);
            }
            if (C_MainMenuMemory.mMenuCtrl == 12 || C_MainMenuMemory.mMenuCtrl == 13) {
                if (C_MainMenuMemory.SaveHelpJsr == 0) {
                    C_MainMenuMemory.mMenuCtrl = 0;
                } else {
                    C_MainMenuMemory.SaveHelpFlag = 1;
                    C_MainMenuMemory.mMenuCtrl = 7;
                }
                C_UserRecordData.mUserScore[0] = C_MainMenuMemory.SongPreeFlag;
                C_UserRecordData.mUserScore[1] = C_MainMenuMemory.MusicPreeFlag;
            }
            C_Media.PlaySound(0);
        }
    }

    private void GameEndJC() {
        if (C_MainMenuMemory.BallValPtr[17] == 1 && C_MainMenuMemory.GameEndFlag == 0) {
            int AddScrY = MyPocketPlane.BallScrAddY;
            int y = C_MainMenuMemory.BallValPtr[16];
            if (y < -36 - AddScrY || y > AddScrY + 516) {
                C_MainMenuMemory.GameEndFlag = 1;
                cMemory.MenuIcoEVT[0].EVT.XInc = 0;
                cMemory.MenuIcoEVT[0].EVT.XAdc = 0;
                cMemory.MenuIcoEVT[0].EVT.YAdc = 0;
                cMemory.MenuIcoEVT[0].EVT.YAdc = 0;
                cMemory.MenuIcoEVT[0].EVTCLR();
                C_MainMenuMemory.GameCurWinLost = 0;
            }
        }
        if (C_MainMenuMemory.GameEndFlag == 1) {
            Ball_Activity(1);
            C_MainMenuMemory.GameEndFlag = 2;
            for (int i = 118; i < 120; i++) {
                if (!cMemory.MenuIcoEVT[i].EVT.Valid) {
                    cMemory.MenuIcoEVT[i].Angle = 0;
                    cMemory.MenuIcoEVT[i].Flag = 0;
                    cMemory.MenuIcoEVT[i].CURACT = 0;
                    cMemory.MenuIcoEVT[i].FlySpeedFlag = 0;
                    C_MainMenuMemory.BestScoreAddFlag = 0;
                    C_MainMenuMemory.BestScoreAdd = 0.0f;
                    cMemory.MenuIcoEVT[i].StarSmall = 2.0f;
                    C_MainMenuMemory.BestScorePadAddFlag = 0;
                    cMemory.MenuIcoEVT[i].MakeEVENT(160, 240, 0);
                    cMemory.MenuIcoEVT[i].SetMenuIcoType(10);
                    cMemory.MenuIcoEVT[i].EVT.Status |= 9728;
                    cMemory.MenuIcoEVT[i].EVT.Attrib = 8;
                    cMemory.MenuIcoEVT[i].EVT.Ctrl = 10;
                    cMemory.MenuIcoEVT[i].EVT.Flag = i;
                    cMemory.MenuIcoEVT[i].EVTCur = i;
                    cMemory.MenuIcoEVT[i].MenuSmall = 0.1f;
                    cMemory.MenuIcoEVT[i].CurTime = 0;
                    C_MainMenuMemory.MakePauseFlag = 1;
                    return;
                }
            }
        }
    }

    private void GameInitFun() {
        ExitEVENT();
        for (int i = 0; i < 150; i++) {
            if (i != 14) {
                C_MainMenuMemory.BallValPtr[i] = 0;
            }
        }
        C_MainMenuMemory.SaveHelpFlag = 0;
        C_MainMenuMemory.GameOpenScrFlag = 0;
        C_MainMenuMemory.MakePlaneFlag = 0;
        C_MainMenuMemory.PlaneBombFlag = 0;
        C_MainMenuMemory.PlaneTreeDrmFlag = 0;
        C_MainMenuMemory.SaveScoreFlag = 0;
        C_MainMenuMemory.SaveOldAssess = 0;
        C_MainMenuMemory.SaveNewAssess = 0;
        C_MainMenuMemory.BuyFlag = 0;
        C_MainMenuMemory.GameEndFlag = 0;
        C_MainMenuMemory.MakePauseFlag = 0;
        C_MainMenuMemory.GameTotalScore = 0.0f;
        C_MainMenuMemory.BestScoreAdd = 0.0f;
        C_MainMenuMemory.BestScoreAddFlag = 0;
        C_MainMenuMemory.OpenBjFlag = 0;
        if (C_MainMenuMemory.BallValPtr[14] == 1) {
            C_MainMenuMemory.BallValPtr[12] = C_MainMenuData.Ball_MakeAngle[C_MainMenuMemory.GameLevel];
        } else {
            C_MainMenuMemory.BallValPtr[12] = 0;
        }
        int AddScrY = MyPocketPlane.BallScrAddY;
        if (C_MainMenuMemory.BallValPtr[12] == 0) {
            C_MainMenuMemory.BallValPtr[16] = -32 - AddScrY;
        } else {
            C_MainMenuMemory.BallValPtr[16] = AddScrY + GameEvent.KeepADC;
        }
        if (C_MainMenuMemory.GameMenuChoice == 0) {
            C_MainMenuMemory.GameBestScore = (float) C_UserRecordData.mUserScore[C_MainMenuMemory.GameLevel + 4];
            C_MainMenuMemory.GameTotalScore = 0.0f;
            return;
        }
        C_MainMenuMemory.GameBestScore = (float) ((C_UserRecordData.mUserScore[104] * 10000) + C_UserRecordData.mUserScore[3]);
    }

    private void Plane_ShowScore() {
        int AddScrY = MyPocketPlane.BallScrAddY;
        int GG_Y = 0;
        if (C_MainMenuMemory.BallValPtr[14] == 1) {
            Ball_ActivityShow();
            if (C_MainMenuMemory.ActivityFlag == 1) {
                GG_Y = 20;
            }
            int i = (70 + 28) - 40;
            cLib.getGameCanvas().WriteSprite(R.drawable.act_number01, 304, (GG_Y + 58) - AddScrY, 7);
            int Total = C_MainMenuMemory.GameLevel + 1;
            int i2 = ((70 + 53) - 2) - 40;
            cLib.getGameCanvas().WriteSprite(Plane_NumberAct[(Total % 100) / 10], 304, (GG_Y + 81) - AddScrY, 7);
            int i3 = ((70 + 66) - 2) - 40;
            cLib.getGameCanvas().WriteSprite(Plane_NumberAct[Total % 10], 304, (GG_Y + 94) - AddScrY, 7);
            int i4 = ((70 + 76) - 2) - 40;
            cLib.getGameCanvas().WriteSprite(R.drawable.act_number22, 304, (GG_Y + SAVE_CHALLENGE_SCORE_1) - AddScrY, 7);
            int i5 = ((70 + 87) - 2) - 40;
            cLib.getGameCanvas().WriteSprite(Plane_NumberAct[48 / 10], 304, (GG_Y + 115) - AddScrY, 7);
            int i6 = ((70 + 100) - 2) - 40;
            cLib.getGameCanvas().WriteSprite(Plane_NumberAct[48 % 10], 304, (GG_Y + GameEvent.KeepACT) - AddScrY, 7);
            if (C_MainMenuMemory.ActivityFlag == 1) {
                GG_Y = 10;
            }
            int i7 = 70 + 136;
            cLib.getGameCanvas().WriteSprite(R.drawable.act_number00, 304, GG_Y + 206, 7);
            int i8 = (70 + 176) - 2;
            ScoreShow_JSR(C_MainMenuMemory.GameBestScore, GG_Y + 244);
            int i9 = 70 + 265 + 20;
            cLib.getGameCanvas().WriteSprite(R.drawable.act_number21, 304, AddScrY + 355, 7);
            int i10 = ((70 + 310) - 2) + 20;
            ScoreShow_JSR(C_MainMenuMemory.GameTotalScore, AddScrY + 398);
            return;
        }
        Ball_ActivityShow();
        int i11 = (50 + 28) - 20;
        cLib.getGameCanvas().WriteSprite(R.drawable.act_number00, 304, 0 + 58 + 20, 7);
        int i12 = ((50 + 68) - 2) - 20;
        ScoreShow_JSR(C_MainMenuMemory.GameBestScore, 0 + 96 + 20);
        cLib.getGameCanvas().WriteSprite(R.drawable.act_number21, 304, (50 + 265) - 20, 7);
        ScoreShow_JSR(C_MainMenuMemory.GameTotalScore, ((50 + 310) - 2) - 20);
    }

    private void ScoreShow_JSR(float Total, int y) {
        int z = 0;
        int Sum = 6;
        while (true) {
            if (Sum < 0) {
                break;
            } else if (Total >= Plane_ScoreCmp[Sum]) {
                z = Sum;
                break;
            } else {
                Sum--;
            }
        }
        if (Total >= 1000000.0f) {
            cLib.getGameCanvas().WriteSprite(Plane_NumberAct[(int) ((Total % 1.0E7f) / 1000000.0f)], 304, y, 7);
        }
        if (Total >= 100000.0f) {
            if (z != 5) {
                y += 11;
            }
            cLib.getGameCanvas().WriteSprite(Plane_NumberAct[(int) ((Total % 1000000.0f) / 100000.0f)], 304, y, 7);
        }
        if (Total >= 10000.0f) {
            if (z != 4) {
                y += 11;
            }
            cLib.getGameCanvas().WriteSprite(Plane_NumberAct[(int) ((Total % 100000.0f) / 10000.0f)], 304, y, 7);
        }
        if (Total >= 1000.0f) {
            if (z != 3) {
                y += 11;
            }
            cLib.getGameCanvas().WriteSprite(Plane_NumberAct[(int) ((Total % 10000.0f) / 1000.0f)], 304, y, 7);
        }
        if (Total >= 100.0f) {
            if (z != 2) {
                y += 11;
            }
            cLib.getGameCanvas().WriteSprite(Plane_NumberAct[(int) ((Total % 1000.0f) / 100.0f)], 304, y, 7);
        }
        if (Total >= 10.0f) {
            if (z != 1) {
                y += 11;
            }
            cLib.getGameCanvas().WriteSprite(Plane_NumberAct[(int) ((Total % 100.0f) / 10.0f)], 304, y, 7);
        }
        if (z != 0) {
            y += 11;
        }
        cLib.getGameCanvas().WriteSprite(Plane_NumberAct[(int) (Total % 10.0f)], 304, y, 7);
    }

    private void Ball_MainChoiceGameInit() {
        int x;
        for (int i = 0; i < 1; i++) {
            cMemory.MenuIcoEVT[i].Angle = 0;
            cMemory.MenuIcoEVT[i].Flag = 0;
            cMemory.MenuIcoEVT[i].CURACT = 0;
            cMemory.MenuIcoEVT[i].FlySpeedFlag = 0;
            int Cur = C_MainMenuMemory.SaveMenuLevelStop;
            if (Cur <= 1) {
                x = 253;
            } else if (Cur <= 45) {
                x = ((Cur - 1) * 59) + 253 + 1;
            } else {
                x = 2849;
            }
            cMemory.MenuIcoEVT[i].MakeEVENT(x, 9, 0);
            cMemory.MenuIcoEVT[i].SetMenuIcoType(14);
            cMemory.MenuIcoEVT[i].EVT.Status |= 9728;
            cMemory.MenuIcoEVT[i].EVT.Attrib = 2;
            cMemory.MenuIcoEVT[i].EVT.Ctrl = 14;
            cMemory.MenuIcoEVT[i].EVT.Flag = i;
            cMemory.MenuIcoEVT[i].EVTCur = i;
            cMemory.MenuIcoEVT[i].CurTime = 0;
            cMemory.MenuIcoEVT[i].AddXSpeed = 0;
        }
    }

    private void Ball_MenuAssess_Init() {
        int Total = C_MainMenuMemory.GameCurChoice;
        for (int i = 0; i < Total; i++) {
            if (C_UserRecordData.mUserScore[i + 4] >= ((int) (((float) C_MainMenuData.Plane_BestScore[i]) * 0.9f))) {
                C_MainMenuMemory.AssessStr[i] = 3;
            } else {
                C_MainMenuMemory.AssessStr[i] = 2;
            }
        }
    }

    private void Ball_Activity(int Flag) {
        if (Flag == 0) {
            ((MyPocketPlane) cLib.GetActivity()).setAdVisibility(false);
        } else {
            ((MyPocketPlane) cLib.GetActivity()).setAdVisibility(true);
        }
    }

    private void Ball_ActivityShow() {
        if (MyPocketPlane.ActivityTureFlag == 1) {
            C_MainMenuMemory.ActivityFlag = 1;
        } else {
            C_MainMenuMemory.ActivityFlag = 0;
        }
    }

    public static void Ball_BlackShow() {
        int x = 0;
        for (int i = 0; i <= 19; i++) {
            cLib.getGameCanvas().WriteSprite(R.drawable.act_black00, x, -48, 8);
            x += 16;
        }
    }
}
