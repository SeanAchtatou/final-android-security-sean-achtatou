package com.game.DodgeBall_AdSence;

import oms.GameEngine.C_Lib;

public class C_Media {
    public static final int FIX_BALLCOLLIDE = 5;
    public static final int FIX_BALLDOWN = 8;
    public static final int FIX_BALLJUMP = 3;
    public static final int FIX_BALLLONGJUMP = 4;
    public static final int FIX_BALLSPEEDDOWN = 6;
    public static final int FIX_BALLSPEEDUP = 7;
    public static final int FIX_BUTTONDOWN = 0;
    public static final int FIX_BUTTONUP = 0;
    public static final int FIX_SCORE = 9;
    public static final int FIX_STARTCOLLECT = 2;
    public static final int FIX_WIN = 10;
    public static final int[] MusicPlayChannel = new int[3];
    public static final boolean[] MusicPlayMode;
    private static final int[] MusicResIDTBL = {R.raw.pl_menu, R.raw.pl_game2, R.raw.pl_game2};
    public static final int PL_MUSIC_GAME = 2;
    public static final int PL_MUSIC_GAME2 = 2;
    public static final int PL_MUSIC_MENU = 0;
    public static final int[] SoundPlayChannel;
    public static final boolean[] SoundPlayMode = new boolean[15];
    public static final int[] SoundResIDTBL = {R.raw.pl_buttondown, R.raw.pl_buttonup, R.raw.pl_startcollect, R.raw.pl_balljump, R.raw.pl_balllongjump, R.raw.pl_ballcollide, R.raw.pl_ballspeeddown, R.raw.pl_ballspeedup, R.raw.pl_buttondown, R.raw.pl_score, R.raw.pl_win, 65535};
    private static C_Lib c_lib;
    public static int m_FIXControl = 0;

    static {
        boolean[] zArr = new boolean[4];
        zArr[0] = true;
        zArr[1] = true;
        zArr[2] = true;
        MusicPlayMode = zArr;
        int[] iArr = new int[15];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 3;
        iArr[4] = 3;
        iArr[6] = 5;
        iArr[7] = 5;
        iArr[10] = 1;
        iArr[11] = 1;
        iArr[12] = 2;
        iArr[13] = 5;
        iArr[14] = 5;
        SoundPlayChannel = iArr;
    }

    public C_Media(C_Lib clib) {
        c_lib = clib;
    }

    public static void Initialize() {
        for (int i = 0; SoundResIDTBL[i] != 65535; i++) {
            c_lib.getMediaManager().addSound(SoundResIDTBL[i]);
        }
    }

    public static void StopAllSound() {
        for (int i = 0; SoundResIDTBL[i] != 65535; i++) {
            c_lib.getMediaManager().soundStop(SoundResIDTBL[i]);
        }
    }

    public static void StopMusic(int MusicIdx) {
        c_lib.getMediaManager().CH_MediaStop(MusicPlayChannel[MusicIdx]);
    }

    public static void PlaySound(int SoundIdx) {
        c_lib.getMediaManager().CH_SoundPlay(SoundPlayChannel[SoundIdx], SoundResIDTBL[SoundIdx]);
    }

    public static void StopSound(int SoundIdx) {
        c_lib.getMediaManager().CH_SoundStop(SoundPlayChannel[SoundIdx]);
    }

    public static void PlayMusic(int MusicIdx) {
        C_OPhoneApp.cLib.getMediaManager().CH_MediaPlay(MusicPlayChannel[MusicIdx], MusicResIDTBL[MusicIdx], MusicPlayMode[MusicIdx]);
    }

    public static void PlayPauseMusic(int MusicIdx) {
        c_lib.getMediaManager().CH_MediaPause(MusicPlayChannel[MusicIdx]);
    }

    public static void PlayResumeMusic(int MusicIdx) {
        c_lib.getMediaManager().CH_MediaResume(MusicPlayChannel[MusicIdx]);
    }
}
