package com.game.DodgeBall_AdSence;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import com.MoreGames.API.MoreGames;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import oms.GameEngine.GameEvent;
import oms.GameEngine.GameView;

public class MyPocketPlane extends Activity implements AdListener {
    static int ActivityTureFlag = 0;
    static int BallScrAddY = 0;
    static int BallScrAddY_3_1 = 0;
    static int BigScrFlag = 0;
    public static AdView adView;
    private String AdMobID = "a14e4d0362cae02";
    private RelativeLayout activityLayout;
    /* access modifiers changed from: private */
    public boolean adViewStatus = true;
    private C_OPhoneApp cOPhoneApp;
    private GameView cView;
    private Handler mAdHandler = new Handler() {
        public void handleMessage(Message msg) {
            MyPocketPlane.this.AdViewHandler();
        }
    };
    private boolean mClickMoreGames = false;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (MyPocketPlane.this.adViewStatus) {
                MyPocketPlane.adView.setVisibility(0);
            } else {
                MyPocketPlane.adView.setVisibility(8);
            }
        }
    };
    private int mLayoutMode = 10;
    private Handler mMoreGames = new Handler() {
        public void handleMessage(Message msg) {
            MyPocketPlane.this.moreGames();
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        int scrWidth;
        int scrHeight;
        boolean hdFlag;
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(GameEvent.KeepINC, GameEvent.KeepINC);
        this.cOPhoneApp = new C_OPhoneApp(this);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        this.cOPhoneApp.getCLib().SetActivity(this);
        if (dm.widthPixels < dm.heightPixels) {
            scrWidth = dm.widthPixels;
            scrHeight = dm.heightPixels;
        } else {
            scrWidth = dm.heightPixels;
            scrHeight = dm.widthPixels;
        }
        if (scrWidth > 320 || scrHeight > 576) {
            hdFlag = true;
        } else {
            hdFlag = false;
        }
        if (hdFlag) {
            this.cOPhoneApp.getCLib().SetCanvaseSize(scrWidth, 576);
            this.cOPhoneApp.getCLib().getInput().SetScreenOffset(0, 48);
            this.cOPhoneApp.getCLib().getGameCanvas().SetBackgroundDrawOffset(0, 48);
            this.cOPhoneApp.getCLib().getGameCanvas().SetSpriteDrawOffset(0, 48);
            this.cOPhoneApp.getCLib().SetCanvasScale(((float) scrWidth) / 320.0f, ((float) scrHeight) / 576.0f);
            scrHeight = 576;
            BigScrFlag = 1;
        } else {
            this.cOPhoneApp.getCLib().SetReflashSize(scrWidth, scrHeight, dm.scaledDensity);
            BigScrFlag = 0;
        }
        BallScrAddY = 0;
        BallScrAddY_3_1 = 0;
        if (scrHeight > 480) {
            BallScrAddY = (scrHeight - 480) / 2;
            BallScrAddY_3_1 = (scrHeight - 480) / 3;
        }
        this.activityLayout = new RelativeLayout(this);
        setContentView(this.activityLayout);
        this.cView = new GameView(this);
        this.cView.SetDraw(this.cOPhoneApp.getCLib());
        this.activityLayout.addView(this.cView);
        adView = new AdView(this, AdSize.BANNER, this.AdMobID);
        AdRequest request = new AdRequest();
        adView.setAdListener(this);
        adView.loadAd(request);
        RelativeLayout.LayoutParams adWhirlLayoutParams = new RelativeLayout.LayoutParams(-1, -2);
        adWhirlLayoutParams.addRule(10, -1);
        this.mLayoutMode = 10;
        this.activityLayout.addView(adView, adWhirlLayoutParams);
    }

    public void FullScreen(boolean full) {
        WindowManager.LayoutParams attrs = getWindow().getAttributes();
        if (full) {
            attrs.flags |= GameEvent.KeepINC;
        } else {
            attrs.flags &= -1025;
        }
        getWindow().setAttributes(attrs);
        getWindow().addFlags(GameEvent.KeepADC);
    }

    public void resumeMoreGames() {
        if (this.mClickMoreGames) {
            this.mClickMoreGames = false;
            RelativeLayout.LayoutParams adWhirlLayoutParams = new RelativeLayout.LayoutParams(-1, -2);
            adWhirlLayoutParams.addRule(this.mLayoutMode, -1);
            this.activityLayout.addView(adView, adWhirlLayoutParams);
        }
    }

    public void clickMoreGames() {
        this.mClickMoreGames = true;
        this.mMoreGames.sendMessage(this.mMoreGames.obtainMessage());
    }

    /* access modifiers changed from: private */
    public void moreGames() {
        this.activityLayout.removeView(adView);
        startActivity(new Intent(this, MoreGames.class));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.cOPhoneApp.onPause();
        this.cView.onPause();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        FullScreen(true);
        resumeMoreGames();
        this.cOPhoneApp.onResume();
        this.cView.onResume();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.cOPhoneApp.onDestory();
        adView.destroy();
        this.cView.onDestory();
        super.onDestroy();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return this.cOPhoneApp.onKeyDown(keyCode, event);
    }

    public boolean onKeyMultiple(int keyCode, int repeatCount, KeyEvent event) {
        return super.onKeyMultiple(keyCode, repeatCount, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return this.cOPhoneApp.onKeyUp(keyCode, event);
    }

    public boolean onTouchEvent(MotionEvent event) {
        return this.cOPhoneApp.onTouchEvent(event);
    }

    public void onDismissScreen(Ad arg0) {
    }

    public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
    }

    public void onLeaveApplication(Ad arg0) {
    }

    public void onPresentScreen(Ad arg0) {
    }

    public void onReceiveAd(Ad arg0) {
        ActivityTureFlag = 1;
    }

    public void setAdVisibility(boolean status) {
        this.adViewStatus = status;
        this.mHandler.sendMessage(this.mHandler.obtainMessage());
    }

    public void AdViewHandler() {
        RelativeLayout.LayoutParams adWhirlLayoutParams = new RelativeLayout.LayoutParams(-1, -2);
        if (this.mLayoutMode == 10) {
            adWhirlLayoutParams.addRule(12, 0);
            adWhirlLayoutParams.addRule(10, -1);
        } else if (this.mLayoutMode == 12) {
            adWhirlLayoutParams.addRule(12, -1);
            adWhirlLayoutParams.addRule(10, 0);
        }
        adView.setLayoutParams(adWhirlLayoutParams);
        adView.invalidate();
    }

    public void SetAdViewLayout(int align) {
        this.mLayoutMode = align;
        this.mAdHandler.sendMessage(this.mAdHandler.obtainMessage());
    }
}
