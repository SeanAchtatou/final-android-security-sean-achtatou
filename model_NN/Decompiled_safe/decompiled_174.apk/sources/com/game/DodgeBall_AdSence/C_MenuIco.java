package com.game.DodgeBall_AdSence;

import oms.GameEngine.C_Lib;
import oms.GameEngine.GameEvent;

public class C_MenuIco extends GameEvent {
    private static final int[] Ball_PassStarAct = {R.drawable.act_passstar02, R.drawable.act_passstar02, R.drawable.act_passstar01, R.drawable.act_passstar00};
    public static final int[][] BitMenuIcoEVT;
    private static final int[][] BsetGameEnd_TouchX = {new int[]{26, 66}, new int[]{26, 66}, new int[]{26, 66}};
    private static final int[][] BsetGameEnd_TouchY = {new int[]{63, 165}, new int[]{189, 290}, new int[]{315, 415}};
    private static final int[][] GameEnd_TouchX = {new int[]{125, 165}, new int[]{65, 106}, new int[]{102, 139}, new int[]{65, C_MainMenu.SAVE_GAME_SCORECUREND}};
    private static final int[][] GameEnd_TouchY = {new int[]{166, 303}, new int[]{166, 303}, new int[]{170, 303}, new int[]{168, 303}};
    private static final int[][] MenuIco00 = {MenuIco0000, MenuIco0000, MenuIco0000, MenuIco0000, MenuIco0000, MenuIco0000, MenuIco0000, MenuIco0000, MenuIco0000, MenuIco0000, MenuIco0000, MenuIco0000, MenuIco0000, MenuIco0000, MenuIco0000, MenuIco0000, MenuIco0000, MenuIco0000, MenuIco0000, MenuIco0000};
    private static final int[] MenuIco0000 = new int[2];
    private static final int[] MenuIco0001 = {R.drawable.act_ballenda00};
    private static final int[] MenuIco0002 = {R.drawable.act_star00, R.drawable.act_star01, R.drawable.act_star02, R.drawable.act_star03};
    private static final int[] MenuIco0002_BOMB = {R.drawable.act_startbomb00, R.drawable.act_startbomb01, R.drawable.act_startbomb02, R.drawable.act_startbomb03, R.drawable.act_startbomb04, R.drawable.act_startbomb05, R.drawable.act_startbomb06};
    private static final int[] MenuIco0003 = {R.drawable.act_ballenda02, R.drawable.act_ballenda03, R.drawable.act_ballenda04, R.drawable.act_ballenda03, R.drawable.act_ballenda05};
    private static final int[] MenuIco0004 = new int[1];
    private static final int[] MenuIco0005 = {R.drawable.act_tool02, R.drawable.act_tool04, R.drawable.act_tool00};
    private static final int[] MenuIco0006 = {R.drawable.act_ballenda06};
    private static final int[] MenuIco0007 = {R.drawable.act_tool05};
    private static final int[] MenuIco0008 = {R.drawable.act_ballbomb00, R.drawable.act_ballbomb01, R.drawable.act_ballbomb02, R.drawable.act_ballbomb03, R.drawable.act_ballbomb04, R.drawable.act_ballbomb05, R.drawable.act_ballbomb06, R.drawable.act_ballbomb07, R.drawable.act_ballbomb08};
    private static final int[] MenuIco0009 = {R.drawable.act_getready00};
    private static final int[] MenuIco000a = new int[1];
    private static final int[] MenuIco000b = {R.drawable.act_tool03, R.drawable.act_tool01};
    private static final int[] MenuIco000c = {R.drawable.act_ballenda0d};
    private static final int[] MenuIco000d = {R.drawable.act_ballenda0d};
    private static final int[] MenuIco000e = {R.drawable.act_menudw01};
    private static final int[] MenuIco000f = {R.drawable.act_starshow00, R.drawable.act_starshow01, R.drawable.act_starshow02, R.drawable.act_starshow03, R.drawable.act_starshow04, R.drawable.act_starshow05, R.drawable.act_starshow06, R.drawable.act_starshow07, R.drawable.act_starshow08, R.drawable.act_starshow09, R.drawable.act_starshow0a, R.drawable.act_starshow0b};
    private static final int[][] MenuIco01 = {MenuIco0001, MenuIco0001, MenuIco0001, MenuIco0001, MenuIco0001, MenuIco0001, MenuIco0001, MenuIco0001, MenuIco0001, MenuIco0001, MenuIco0001, MenuIco0001, MenuIco0001, MenuIco0001, MenuIco0001, MenuIco0001, MenuIco0001, MenuIco0001, MenuIco0001, MenuIco0001};
    private static final int[][] MenuIco02 = {MenuIco0002, MenuIco0002, MenuIco0002, MenuIco0002, MenuIco0002, MenuIco0002, MenuIco0002, MenuIco0002, MenuIco0002, MenuIco0002, MenuIco0002, MenuIco0002, MenuIco0002, MenuIco0002, MenuIco0002, MenuIco0002, MenuIco0002, MenuIco0002, MenuIco0002, MenuIco0002};
    private static final int[][] MenuIco02_BOMB = {MenuIco0002_BOMB, MenuIco0002_BOMB, MenuIco0002_BOMB, MenuIco0002_BOMB, MenuIco0002_BOMB, MenuIco0002_BOMB, MenuIco0002_BOMB, MenuIco0002_BOMB, MenuIco0002_BOMB, MenuIco0002_BOMB, MenuIco0002_BOMB, MenuIco0002_BOMB, MenuIco0002_BOMB, MenuIco0002_BOMB, MenuIco0002_BOMB, MenuIco0002_BOMB, MenuIco0002_BOMB, MenuIco0002_BOMB, MenuIco0002_BOMB, MenuIco0002_BOMB};
    private static final int[][] MenuIco03 = {MenuIco0003, MenuIco0003, MenuIco0003, MenuIco0003, MenuIco0003, MenuIco0003, MenuIco0003, MenuIco0003, MenuIco0003, MenuIco0003, MenuIco0003, MenuIco0003, MenuIco0003, MenuIco0003, MenuIco0003, MenuIco0003, MenuIco0003, MenuIco0003, MenuIco0003, MenuIco0003};
    private static final int[][] MenuIco04 = {MenuIco0004, MenuIco0004, MenuIco0004, MenuIco0004, MenuIco0004, MenuIco0004, MenuIco0004, MenuIco0004, MenuIco0004, MenuIco0004, MenuIco0004, MenuIco0004, MenuIco0004, MenuIco0004, MenuIco0004, MenuIco0004, MenuIco0004, MenuIco0004, MenuIco0004, MenuIco0004};
    private static final int[][] MenuIco05 = {MenuIco0005, MenuIco0005, MenuIco0005, MenuIco0005, MenuIco0005, MenuIco0005, MenuIco0005, MenuIco0005, MenuIco0005, MenuIco0005, MenuIco0005, MenuIco0005, MenuIco0005, MenuIco0005, MenuIco0005, MenuIco0005, MenuIco0005, MenuIco0005, MenuIco0005, MenuIco0005};
    private static final int[][] MenuIco06 = {MenuIco0006, MenuIco0006, MenuIco0006, MenuIco0006, MenuIco0006, MenuIco0006, MenuIco0006, MenuIco0006, MenuIco0006, MenuIco0006, MenuIco0006, MenuIco0006, MenuIco0006, MenuIco0006, MenuIco0006, MenuIco0006, MenuIco0006, MenuIco0006, MenuIco0006, MenuIco0006};
    private static final int[][] MenuIco07 = {MenuIco0007, MenuIco0007, MenuIco0007, MenuIco0007, MenuIco0007, MenuIco0007, MenuIco0007, MenuIco0007, MenuIco0007, MenuIco0007, MenuIco0007, MenuIco0007, MenuIco0007, MenuIco0007, MenuIco0007, MenuIco0007, MenuIco0007, MenuIco0007, MenuIco0007, MenuIco0007};
    private static final int[][] MenuIco08 = {MenuIco0008, MenuIco0008, MenuIco0008, MenuIco0008, MenuIco0008, MenuIco0008, MenuIco0008, MenuIco0008, MenuIco0008, MenuIco0008, MenuIco0008, MenuIco0008, MenuIco0008, MenuIco0008, MenuIco0008, MenuIco0008, MenuIco0008, MenuIco0008, MenuIco0008, MenuIco0008};
    private static final int[][] MenuIco09 = {MenuIco0009, MenuIco0009, MenuIco0009, MenuIco0009, MenuIco0009, MenuIco0009, MenuIco0009, MenuIco0009, MenuIco0009, MenuIco0009, MenuIco0009, MenuIco0009, MenuIco0009, MenuIco0009, MenuIco0009, MenuIco0009, MenuIco0009, MenuIco0009, MenuIco0009, MenuIco0009};
    private static final int[][] MenuIco0a = {MenuIco000a, MenuIco000a, MenuIco000a, MenuIco000a, MenuIco000a, MenuIco000a, MenuIco000a, MenuIco000a, MenuIco000a, MenuIco000a, MenuIco000a, MenuIco000a, MenuIco000a, MenuIco000a, MenuIco000a, MenuIco000a, MenuIco000a, MenuIco000a, MenuIco000a, MenuIco000a};
    private static final int[][] MenuIco0b = {MenuIco000b, MenuIco000b, MenuIco000b, MenuIco000b, MenuIco000b, MenuIco000b, MenuIco000b, MenuIco000b, MenuIco000b, MenuIco000b, MenuIco000b, MenuIco000b, MenuIco000b, MenuIco000b, MenuIco000b, MenuIco000b, MenuIco000b, MenuIco000b, MenuIco000b, MenuIco000b};
    private static final int[][] MenuIco0c = {MenuIco000c, MenuIco000c, MenuIco000c, MenuIco000c, MenuIco000c, MenuIco000c, MenuIco000c, MenuIco000c, MenuIco000c, MenuIco000c, MenuIco000c, MenuIco000c, MenuIco000c, MenuIco000c, MenuIco000c, MenuIco000c, MenuIco000c, MenuIco000c, MenuIco000c, MenuIco000c};
    private static final int[][] MenuIco0d = {MenuIco000d, MenuIco000d, MenuIco000d, MenuIco000d, MenuIco000d, MenuIco000d, MenuIco000d, MenuIco000d, MenuIco000d, MenuIco000d, MenuIco000d, MenuIco000d, MenuIco000d, MenuIco000d, MenuIco000d, MenuIco000d, MenuIco000d, MenuIco000d, MenuIco000d, MenuIco000d};
    private static final int[][] MenuIco0e = {MenuIco000e, MenuIco000e, MenuIco000e, MenuIco000e, MenuIco000e, MenuIco000e, MenuIco000e, MenuIco000e, MenuIco000e, MenuIco000e, MenuIco000e, MenuIco000e, MenuIco000e, MenuIco000e, MenuIco000e, MenuIco000e, MenuIco000e, MenuIco000e, MenuIco000e, MenuIco000e};
    private static final int[][] MenuIco0f = {MenuIco000f, MenuIco000f, MenuIco000f, MenuIco000f, MenuIco000f, MenuIco000f, MenuIco000f, MenuIco000f, MenuIco000f, MenuIco000f, MenuIco000f, MenuIco000f, MenuIco000f, MenuIco000f, MenuIco000f, MenuIco000f, MenuIco000f, MenuIco000f, MenuIco000f, MenuIco000f};
    private static final int[] N_CmpXY = {16, 48, 80, 112, 144, 176, 208, 240, 272, 304, 336, 368, 400, 432, 464, 496};
    private static final int[] Plane_Number01Act = {R.drawable.act_writeend00, R.drawable.act_writeend01, R.drawable.act_writeend02, R.drawable.act_writeend03, R.drawable.act_writeend04, R.drawable.act_writeend05, R.drawable.act_writeend06, R.drawable.act_writeend07, R.drawable.act_writeend08, R.drawable.act_writeend09};
    private static final int[] Plane_NumberAct_Best = {R.drawable.act_menukt00, R.drawable.act_menukt01, R.drawable.act_menukt02, R.drawable.act_menukt03, R.drawable.act_menukt04, R.drawable.act_menukt05, R.drawable.act_menukt06, R.drawable.act_menukt07, R.drawable.act_menukt08, R.drawable.act_menukt09};
    public long AddXSpeed = 0;
    public int Angle = 0;
    public int CURACT = 0;
    public int CurTime = 0;
    private int DXVal;
    public int EVTCur = 0;
    private int EVTType;
    public int Flag = 0;
    public int FlySpeedFlag = 0;
    public float MenuSmall = 0.0f;
    public int NewTouchXVal = 0;
    public int OldTouchXVal = 0;
    public int SaveXVal = 0;
    public float StarSmall = 0.0f;
    private C_Lib cLib;

    static {
        int[] iArr = new int[8];
        iArr[6] = 9;
        iArr[7] = 2;
        int[] iArr2 = new int[8];
        iArr2[6] = 9;
        iArr2[7] = 1;
        int[] iArr3 = new int[8];
        iArr3[6] = 4;
        iArr3[7] = 4;
        int[] iArr4 = new int[8];
        iArr4[6] = 9;
        iArr4[7] = 1;
        int[] iArr5 = new int[8];
        iArr5[6] = 9;
        iArr5[7] = 1;
        int[] iArr6 = new int[8];
        iArr6[6] = 9;
        iArr6[7] = 1;
        int[] iArr7 = new int[8];
        iArr7[6] = 9;
        iArr7[7] = 1;
        int[] iArr8 = new int[8];
        iArr8[6] = 9;
        iArr8[7] = 1;
        int[] iArr9 = new int[8];
        iArr9[6] = 9;
        iArr9[7] = 1;
        int[] iArr10 = new int[8];
        iArr10[6] = 9;
        iArr10[7] = 1;
        int[] iArr11 = new int[8];
        iArr11[6] = 9;
        iArr11[7] = 1;
        int[] iArr12 = new int[8];
        iArr12[6] = 9;
        iArr12[7] = 1;
        int[] iArr13 = new int[8];
        iArr13[7] = 1;
        int[] iArr14 = new int[8];
        iArr14[6] = 4;
        iArr14[7] = 1;
        int[] iArr15 = new int[8];
        iArr15[6] = 4;
        iArr15[7] = 1;
        int[] iArr16 = new int[8];
        iArr16[6] = 2;
        iArr16[7] = 12;
        BitMenuIcoEVT = new int[][]{iArr, iArr2, iArr3, iArr4, iArr5, iArr6, iArr7, iArr8, iArr9, iArr10, iArr11, iArr12, iArr13, iArr14, iArr15, iArr16};
    }

    public C_MenuIco(C_Lib clib) {
        this.cLib = clib;
        this.EVTType = 0;
        this.EVT.ACTPtr = MenuIco00;
        this.EVT.EVTPtr = BitMenuIcoEVT;
    }

    public void SetMenuIcoType(int IcoType) {
        this.EVTType = IcoType;
        switch (this.EVTType) {
            case 0:
                this.EVT.ACTPtr = MenuIco00;
                return;
            case 1:
                this.EVT.ACTPtr = MenuIco01;
                return;
            case 2:
                this.EVT.ACTPtr = MenuIco02;
                return;
            case 3:
                this.EVT.ACTPtr = MenuIco03;
                return;
            case 4:
                this.EVT.ACTPtr = MenuIco04;
                return;
            case 5:
                this.EVT.ACTPtr = MenuIco05;
                return;
            case 6:
                this.EVT.ACTPtr = MenuIco06;
                return;
            case 7:
                this.EVT.ACTPtr = MenuIco07;
                return;
            case 8:
                this.EVT.ACTPtr = MenuIco08;
                return;
            case 9:
                this.EVT.ACTPtr = MenuIco09;
                return;
            case 10:
                this.EVT.ACTPtr = MenuIco0a;
                return;
            case 11:
                this.EVT.ACTPtr = MenuIco0b;
                return;
            case 12:
                this.EVT.ACTPtr = MenuIco0c;
                return;
            case 13:
                this.EVT.ACTPtr = MenuIco0d;
                return;
            case 14:
                this.EVT.ACTPtr = MenuIco0e;
                return;
            case 15:
                this.EVT.ACTPtr = MenuIco0f;
                return;
            default:
                return;
        }
    }

    public void SetMenuIcoDXVal(int xval) {
        this.DXVal = xval;
    }

    public void EVTRUN() {
        switch (this.EVT.Ctrl) {
            case 0:
                MenuIco00EXE();
                return;
            case 1:
                MenuIco01EXE();
                return;
            case 2:
                MenuIco02EXE();
                return;
            case 3:
                MenuIco03EXE();
                return;
            case 4:
                MenuIco04EXE();
                return;
            case 5:
                MenuIco05EXE();
                return;
            case 6:
                MenuIco06EXE();
                return;
            case 7:
                MenuIco07EXE();
                return;
            case 8:
                MenuIco08EXE();
                return;
            case 9:
                MenuIco09EXE();
                return;
            case 10:
                MenuIco0aEXE();
                return;
            case 11:
                MenuIco0bEXE();
                return;
            case 12:
                MenuIco0cEXE();
                return;
            case 13:
                MenuIco0dEXE();
                return;
            case 14:
                MenuIco0eEXE();
                return;
            case 15:
                MenuIco0fEXE();
                return;
            default:
                return;
        }
    }

    public void MenuIco00EXE() {
        int i = this.EVT.XVal >> 16;
        int i2 = this.EVT.YVal >> 16;
        int i3 = this.EVT.XInc >> 16;
        PlayPreeKey();
        int Flag2 = 0;
        if (C_MainMenuMemory.BallValPtr[0] == 2) {
            C_MainMenuMemory.BallValPtr[0] = 0;
            this.EVT.XInc = 0;
            this.EVT.XAdc = 0;
            this.EVT.YAdc = 0;
        }
        if (C_MainMenuMemory.BallValPtr[12] == 0) {
            if (C_MainMenuMemory.BallValPtr[7] == 1) {
                this.Angle += 20;
            } else {
                this.Angle += 16;
            }
            if (this.Angle > 360) {
                this.Angle -= 360;
            }
            if (this.Angle < 0) {
                this.Angle += 360;
            }
            if (C_MainMenuMemory.BallValPtr[18] == 1) {
                int[] iArr = C_MainMenuMemory.BallValPtr;
                iArr[16] = iArr[16] + 7;
            }
            if (C_MainMenuMemory.BallValPtr[16] >= 112 && C_MainMenuMemory.BallValPtr[17] == 0) {
                C_MainMenuMemory.BallValPtr[16] = 112;
                C_MainMenuMemory.BallValPtr[21] = 1;
                Flag2 = 1;
            }
            if (C_MainMenuMemory.BallValPtr[16] >= 0 && C_MainMenuMemory.BallValPtr[17] == 0) {
                C_MainMenuMemory.BallValPtr[21] = 1;
            }
            int y = C_MainMenuMemory.BallValPtr[16];
            this.cLib.getGameCanvas().WriteSprite(R.drawable.act_ballenda06, GameEvent.KeepACT, y, 3, (float) this.Angle, 1.0f);
            if (C_MainMenuMemory.BallValPtr[7] == 1) {
                this.cLib.getGameCanvas().WriteSprite(R.drawable.act_ballspeed01, GameEvent.KeepACT, y, 3);
            }
        } else if (C_MainMenuMemory.BallValPtr[12] == 1) {
            if (C_MainMenuMemory.BallValPtr[7] == 1) {
                this.Angle -= 20;
            } else {
                this.Angle -= 16;
            }
            if (this.Angle > 360) {
                this.Angle -= 360;
            }
            if (this.Angle < 0) {
                this.Angle += 360;
            }
            if (C_MainMenuMemory.BallValPtr[18] == 1) {
                int[] iArr2 = C_MainMenuMemory.BallValPtr;
                iArr2[16] = iArr2[16] - 7;
            }
            if (C_MainMenuMemory.BallValPtr[16] <= 368 && C_MainMenuMemory.BallValPtr[17] == 0) {
                C_MainMenuMemory.BallValPtr[16] = 368;
                C_MainMenuMemory.BallValPtr[21] = 1;
                Flag2 = 1;
            }
            if (C_MainMenuMemory.BallValPtr[16] <= 480 && C_MainMenuMemory.BallValPtr[17] == 0) {
                C_MainMenuMemory.BallValPtr[21] = 1;
            }
            int y2 = C_MainMenuMemory.BallValPtr[16];
            this.cLib.getGameCanvas().WriteSprite(R.drawable.act_ballenda06, GameEvent.KeepACT, y2, 3, (float) this.Angle, 1.0f);
            if (C_MainMenuMemory.BallValPtr[7] == 1) {
                this.cLib.getGameCanvas().WriteSprite(R.drawable.act_ballspeed00, GameEvent.KeepACT, y2, 3);
            }
        }
        if (C_MainMenuMemory.BallValPtr[12] == 0) {
            if (Flag2 == 1 && C_MainMenuMemory.BallValPtr[17] == 0) {
                this.EVT.YInc = 458752;
                if (C_MainMenuMemory.BallValPtr[7] == 1) {
                    this.EVT.YInc = 655360;
                }
            } else {
                this.EVT.YInc = 0;
            }
        } else if (Flag2 == 1 && C_MainMenuMemory.BallValPtr[17] == 0) {
            this.EVT.YInc = -458752;
            if (C_MainMenuMemory.BallValPtr[7] == 1) {
                this.EVT.YInc = -655360;
            }
        } else {
            this.EVT.YInc = 0;
        }
        Ball_PlayGod();
        BallToolHit();
        if (C_MainMenuMemory.BallValPtr[11] != 0) {
            this.EVT.XInc = 0;
            this.EVT.XAdc = 0;
            this.EVT.YAdc = 0;
            this.EVT.YAdc = 0;
        } else if (C_MainMenuMemory.BallValPtr[21] == 1) {
            C_MainMenuMemory.GameTotalScore += 1.0f;
            BallRoadHit();
        }
        if (C_MainMenuMemory.BallValPtr[12] == 0) {
            C_MainMenuMemory.BallValPtr[13] = -64;
        } else {
            C_MainMenuMemory.BallValPtr[13] = 544;
        }
    }

    public void MenuIco01EXE() {
        int i = this.EVT.XVal >> 16;
        int CurYVal = this.EVT.YVal >> 16;
        if ((CurYVal <= C_MainMenuMemory.BallValPtr[13] && C_MainMenuMemory.BallValPtr[12] == 0) || (CurYVal >= C_MainMenuMemory.BallValPtr[13] && C_MainMenuMemory.BallValPtr[12] == 1)) {
            this.EVT.Flag = 88;
            EVTCLR();
        }
        PlayJumpSpeed();
    }

    public void MenuIco02EXE() {
        int i = this.EVT.XVal >> 16;
        int CurYVal = this.EVT.YVal >> 16;
        if (this.CURACT == 1) {
            if (this.CurTime == 0) {
                this.CurTime = 1;
                this.EVT.CurFRM = 0;
                this.EVT.CurCNT = 0;
            }
            this.EVT.Flag = 88;
            this.EVT.Attrib = 4;
            this.EVT.MaxCNT = 3;
            this.EVT.MaxFRM = 7;
            this.EVT.ACTPtr = MenuIco02_BOMB;
            if (CHKEVTACTEnd()) {
                EVTCLR();
            }
        }
        if ((CurYVal <= C_MainMenuMemory.BallValPtr[13] && C_MainMenuMemory.BallValPtr[12] == 0) || (CurYVal >= C_MainMenuMemory.BallValPtr[13] && C_MainMenuMemory.BallValPtr[12] == 1)) {
            this.EVT.Flag = 88;
            EVTCLR();
        }
        PlayJumpSpeed();
    }

    public void MenuIco03EXE() {
        int i = this.EVT.XVal >> 16;
        int CurYVal = this.EVT.YVal >> 16;
        switch (this.CURACT) {
            case 0:
                this.EVT.CurFRM = 0;
                break;
            case 1:
                this.EVT.CurFRM = 1;
                break;
            case 2:
                this.EVT.CurFRM = 2;
                break;
            case 3:
                this.EVT.CurFRM = 3;
                break;
            case 4:
                this.EVT.CurFRM = 4;
                break;
        }
        if ((CurYVal <= C_MainMenuMemory.BallValPtr[13] && C_MainMenuMemory.BallValPtr[12] == 0) || (CurYVal >= C_MainMenuMemory.BallValPtr[13] && C_MainMenuMemory.BallValPtr[12] == 1)) {
            this.EVT.Flag = 88;
            EVTCLR();
        }
        if (this.EVT.Flag == 15) {
            PlayJumpSpeed_Fly();
        } else {
            PlayJumpSpeed();
        }
    }

    public void MenuIco04EXE() {
        int CurXVal = this.EVT.XVal >> 16;
        int CurYVal = this.EVT.YVal >> 16;
        PlayJumpSpeed();
        C_MainMenuMemory.BallValPtr[3] = 96 - CurXVal;
        C_MainMenuMemory.BallValPtr[4] = CurYVal;
        if (CurXVal >= 680) {
            EVTCLR();
            C_MainMenuMemory.BallValPtr[0] = 0;
            C_MainMenuMemory.GameCurWinLost = 1;
            C_MainMenuMemory.GameEndFlag = 1;
            C_MainMenu.cMemory.MenuIcoEVT[0].EVTCLR();
        }
    }

    public void MenuIco05EXE() {
        int i = this.EVT.XVal >> 16;
        int CurYVal = this.EVT.YVal >> 16;
        if ((CurYVal <= C_MainMenuMemory.BallValPtr[13] && C_MainMenuMemory.BallValPtr[12] == 0) || (CurYVal >= C_MainMenuMemory.BallValPtr[13] && C_MainMenuMemory.BallValPtr[12] == 1)) {
            this.EVT.Flag = 88;
            EVTCLR();
        }
        if (this.CURACT != 7) {
            this.EVT.CurFRM = 1;
        } else if (C_MainMenuMemory.BallValPtr[12] == 0) {
            this.EVT.CurFRM = 0;
        } else {
            this.EVT.CurFRM = 2;
        }
        PlayJumpSpeed();
    }

    public void MenuIco06EXE() {
        int i = this.EVT.XVal >> 16;
        int CurYVal = this.EVT.YVal >> 16;
        if ((CurYVal <= C_MainMenuMemory.BallValPtr[13] && C_MainMenuMemory.BallValPtr[12] == 0) || (CurYVal >= C_MainMenuMemory.BallValPtr[13] && C_MainMenuMemory.BallValPtr[12] == 1)) {
            this.EVT.Flag = 88;
            EVTCLR();
        }
        PlayJumpSpeed();
    }

    public void MenuIco07EXE() {
        int i = this.EVT.XVal >> 16;
        int CurYVal = this.EVT.YVal >> 16;
        if ((CurYVal <= C_MainMenuMemory.BallValPtr[13] && C_MainMenuMemory.BallValPtr[12] == 0) || (CurYVal >= C_MainMenuMemory.BallValPtr[13] && C_MainMenuMemory.BallValPtr[12] == 1)) {
            this.EVT.Flag = 88;
            EVTCLR();
        }
        PlayJumpSpeed();
    }

    public void MenuIco08EXE() {
        int CurYVal;
        this.EVT.XInc = 0;
        this.EVT.XAdc = 0;
        this.EVT.YInc = 0;
        this.EVT.YAdc = 0;
        C_MainMenuMemory.BallValPtr[19] = 1;
        if (this.CURACT == 0) {
            this.CURACT = 1;
            this.EVT.CurFRM = 0;
            this.EVT.CurCNT = 0;
            this.EVT.MaxCNT = 4;
            this.EVT.MaxFRM = 8;
        }
        if (C_MainMenuMemory.BallValPtr[12] == 0) {
            CurYVal = C_MainMenuMemory.BallValPtr[16];
        } else {
            CurYVal = C_MainMenuMemory.BallValPtr[16];
        }
        this.EVT.XVal = GameEvent.KeepACT << 16;
        this.EVT.YVal = CurYVal << 16;
        this.EVT.ACTPtr = MenuIco08;
        this.EVT.Attrib = 3;
        if (CHKEVTACTEnd()) {
            this.CURACT = 0;
            this.EVT.Flag = 88;
            EVTCLR();
            C_MainMenuMemory.GameCurWinLost = 1;
            C_MainMenuMemory.GameEndFlag = 1;
        }
    }

    public void MenuIco09EXE() {
        int i = this.EVT.XVal >> 16;
        int CurYVal = this.EVT.YVal >> 16;
        if (C_MainMenuMemory.BallValPtr[12] == 0) {
            if (this.CURACT == 0) {
                this.CURACT = 1;
                this.EVT.YInc = 131072;
                this.EVT.YAdc = 40960;
            }
            if (CurYVal >= 240) {
                this.EVT.YVal = 240 << 16;
                this.EVT.YInc = 0;
                this.EVT.YAdc = 0;
            }
        } else {
            if (this.CURACT == 0) {
                this.CURACT = 1;
                this.EVT.YInc = -131072;
                this.EVT.YAdc = -40960;
            }
            if (CurYVal <= 240) {
                this.EVT.YVal = 240 << 16;
                this.EVT.YInc = 0;
                this.EVT.YAdc = 0;
            }
        }
        this.EVT.ACTPtr = MenuIco09;
        this.EVT.Attrib = 3;
        if (this.cLib.getInput().CHKTouchDown()) {
            this.CURACT = 0;
            this.EVT.Flag = 88;
            EVTCLR();
            C_MainMenuMemory.BallValPtr[18] = 1;
            C_MainMenuMemory.BallValPtr[22] = 0;
        }
    }

    private void PlayPreeKey() {
        int z = 0;
        if (this.cLib.getInput().CHKTouchDown() && C_MainMenuMemory.BallValPtr[18] == 1) {
            z = 1;
            C_MainMenuMemory.BallValPtr[22] = 1;
        }
        if (this.cLib.getInput().CHKTouchMove() && C_MainMenuMemory.BallValPtr[18] == 1) {
            z = 1;
            C_MainMenuMemory.BallValPtr[22] = 1;
        }
        if (C_MainMenuMemory.BallValPtr[22] == 1 && C_MainMenuMemory.BallValPtr[18] == 1) {
            z = 1;
        }
        if (z == 1 && C_MainMenuMemory.BallValPtr[0] == 0) {
            C_MainMenuMemory.BallValPtr[0] = 1;
            this.EVT.XInc = 1310720;
            this.EVT.XAdc = -131072;
            if (C_MainMenuMemory.BallValPtr[7] == 1) {
                C_Media.PlaySound(4);
            } else {
                C_Media.PlaySound(3);
            }
        }
        if (this.cLib.getInput().CHKTouchUp() && C_MainMenuMemory.BallValPtr[18] == 1) {
            C_MainMenuMemory.BallValPtr[22] = 0;
        }
        int Speedx = this.EVT.XInc >> 16;
        if (Speedx == 0) {
            z = 0;
        }
        if (Speedx > 0) {
            z = 1;
        }
        if (Speedx < 0) {
            z = 2;
        }
        switch (z) {
            case 0:
                C_MainMenuMemory.BallValPtr[1] = 0;
                return;
            case 1:
                C_MainMenuMemory.BallValPtr[1] = 1;
                return;
            case 2:
                C_MainMenuMemory.BallValPtr[1] = 2;
                return;
            default:
                return;
        }
    }

    private void PlayJumpSpeed() {
        if (C_MainMenuMemory.BallValPtr[11] == 1) {
            this.EVT.XInc = 0;
            this.EVT.XAdc = 0;
            this.EVT.YInc = 0;
            this.EVT.YAdc = 0;
            return;
        }
        this.EVT.XInc = -C_MainMenu.cMemory.MenuIcoEVT[0].EVT.XInc;
        this.EVT.XAdc = -C_MainMenu.cMemory.MenuIcoEVT[0].EVT.XAdc;
        this.EVT.YInc = -C_MainMenu.cMemory.MenuIcoEVT[0].EVT.YInc;
        this.EVT.YAdc = -C_MainMenu.cMemory.MenuIcoEVT[0].EVT.YAdc;
    }

    private void PlayJumpSpeed_Fly() {
        if (this.FlySpeedFlag == 0) {
            this.FlySpeedFlag = 1;
            this.EVT.YInc = 0;
            this.EVT.YAdc = 49152;
        }
        this.EVT.XInc = -C_MainMenu.cMemory.MenuIcoEVT[0].EVT.XInc;
        this.EVT.XAdc = -C_MainMenu.cMemory.MenuIcoEVT[0].EVT.XAdc;
        int CurYVal = this.EVT.YVal >> 16;
        if (CurYVal >= 496 && C_MainMenuMemory.BallValPtr[12] == 0) {
            EVTCLR();
        }
        if (CurYVal <= -16 && C_MainMenuMemory.BallValPtr[12] == 1) {
            EVTCLR();
        }
    }

    private void BallRoadHit() {
        int CmpY;
        int z = 5;
        int x1 = 0;
        if (C_MainMenuMemory.BallValPtr[12] == 0) {
            CmpY = C_MainMenuMemory.BallValPtr[16];
        } else {
            CmpY = C_MainMenuMemory.BallValPtr[16];
        }
        if (C_MainMenu.cMemory.MenuIcoEVT[0].EVT.Valid) {
            int i = C_MainMenu.cMemory.MenuIcoEVT[0].EVT.XVal >> 16;
            int i2 = C_MainMenu.cMemory.MenuIcoEVT[0].EVT.YVal >> 16;
            if (C_MainMenu.cMemory.MenuIcoEVT[0].EVT.Flag == 0) {
                int Total = 2;
                while (true) {
                    if (Total < 62) {
                        if (C_MainMenu.cMemory.MenuIcoEVT[Total].EVT.Valid) {
                            x1 = C_MainMenu.cMemory.MenuIcoEVT[Total].EVT.XVal >> 16;
                            int y1 = C_MainMenu.cMemory.MenuIcoEVT[Total].EVT.YVal >> 16;
                            switch (C_MainMenuMemory.BallValPtr[1]) {
                                case 0:
                                    if (C_MainMenuMemory.BallValPtr[12] != 0) {
                                        if (this.cLib.getGameCanvas().CHKACTTouch(R.drawable.act_ballkt00, GameEvent.KeepACT, CmpY - 16, R.drawable.act_ballkt02, x1, y1)) {
                                            int GetSpriteYHitU = C_OPhoneApp.cLib.getGameCanvas().GetSpriteYHitU(R.drawable.act_ballkt00);
                                            int DYHitU = C_OPhoneApp.cLib.getGameCanvas().GetSpriteYHitD(R.drawable.act_ballkt00);
                                            z = 0;
                                            break;
                                        }
                                    } else if (this.cLib.getGameCanvas().CHKACTTouch(R.drawable.act_ballkt00, GameEvent.KeepACT, CmpY + 16, R.drawable.act_ballkt02, x1, y1)) {
                                        int GetSpriteYHitU2 = C_OPhoneApp.cLib.getGameCanvas().GetSpriteYHitU(R.drawable.act_ballkt00);
                                        int DYHitU2 = C_OPhoneApp.cLib.getGameCanvas().GetSpriteYHitD(R.drawable.act_ballkt00);
                                        z = 0;
                                        break;
                                    }
                                    break;
                                case 1:
                                    if (this.cLib.getGameCanvas().CHKACTTouch(R.drawable.act_ballkt08, 144, CmpY, R.drawable.act_ballkt02, x1, y1)) {
                                        z = 1;
                                        break;
                                    }
                                    break;
                                case 2:
                                    if (this.cLib.getGameCanvas().CHKACTTouch(R.drawable.act_ballkt09, 112, CmpY, R.drawable.act_ballkt02, x1, y1)) {
                                        z = 2;
                                    }
                                    if (118 <= x1 && z == 2) {
                                        z = 0;
                                        break;
                                    }
                            }
                            if (z == 0) {
                                this.CURACT = 0;
                                SetEVTCtrl(8, 0);
                                C_Media.PlaySound(5);
                                C_MainMenuMemory.BallValPtr[19] = 1;
                                C_MainMenuMemory.BallValPtr[0] = 0;
                            } else if (z == 1) {
                                this.CURACT = 0;
                                SetEVTCtrl(8, 0);
                                C_Media.PlaySound(5);
                                C_MainMenuMemory.BallValPtr[19] = 1;
                                C_MainMenuMemory.BallValPtr[0] = 0;
                            } else if (z == 2) {
                                C_MainMenuMemory.BallValPtr[0] = 2;
                                C_Media.PlaySound(8);
                            }
                        }
                        Total++;
                    }
                }
            }
        }
        C_MainMenuMemory.BallValPtr[5] = 0;
        if (z == 5 && C_MainMenuMemory.BallValPtr[0] == 0) {
            int Total2 = 2;
            while (true) {
                if (Total2 < 62) {
                    if (C_MainMenu.cMemory.MenuIcoEVT[Total2].EVT.Valid) {
                        x1 = C_MainMenu.cMemory.MenuIcoEVT[Total2].EVT.XVal >> 16;
                        if (this.cLib.getGameCanvas().CHKACTTouch(R.drawable.act_ballkt0a, GameEvent.KeepACT, CmpY, R.drawable.act_ballkt02, x1, C_MainMenu.cMemory.MenuIcoEVT[Total2].EVT.YVal >> 16)) {
                            C_MainMenuMemory.BallValPtr[5] = 1;
                            z = 6;
                        }
                    }
                    Total2++;
                }
            }
            if (z != 6) {
                C_MainMenuMemory.BallValPtr[0] = 1;
                if (C_MainMenuMemory.BallValPtr[10] == 0) {
                    C_MainMenuMemory.BallValPtr[10] = 1;
                    this.EVT.XInc = -524288;
                    this.EVT.XAdc = -131072;
                }
            }
        }
        if (z == 6 || z == 2) {
            int Addx = (x1 + 32) - GameEvent.KeepACT;
            this.EVT.XVal = ((this.EVT.XVal >> 16) + Addx) << 16;
            this.EVT.XInc = 0;
            this.EVT.XAdc = 0;
            C_MainMenuMemory.BallValPtr[10] = 0;
            for (int Sum = 1; Sum < 124; Sum++) {
                if (C_MainMenu.cMemory.MenuIcoEVT[Sum].EVT.Valid) {
                    C_MainMenu.cMemory.MenuIcoEVT[Sum].EVT.XVal = ((C_MainMenu.cMemory.MenuIcoEVT[Sum].EVT.XVal >> 16) - Addx) << 16;
                    C_MainMenu.cMemory.MenuIcoEVT[Sum].EVT.XInc = 0;
                    C_MainMenu.cMemory.MenuIcoEVT[Sum].EVT.XAdc = 0;
                }
            }
        }
    }

    private int BallToolHit() {
        int CmpY;
        int Cur = 0;
        if (C_MainMenuMemory.BallValPtr[12] == 0) {
            CmpY = C_MainMenuMemory.BallValPtr[16];
        } else {
            CmpY = C_MainMenuMemory.BallValPtr[16];
        }
        if (C_MainMenu.cMemory.MenuIcoEVT[0].EVT.Valid && C_MainMenu.cMemory.MenuIcoEVT[0].EVT.Flag == 0) {
            for (int Total = 62; Total < 92; Total++) {
                if (C_MainMenu.cMemory.MenuIcoEVT[Total].EVT.Valid && C_MainMenu.cMemory.MenuIcoEVT[Total].EVT.Flag == 4) {
                    if (this.cLib.getGameCanvas().CHKACTTouch(R.drawable.act_ballkt09, GameEvent.KeepACT, CmpY, C_MainMenu.cMemory.MenuIcoEVT[Total].EVT.ACTIdx, C_MainMenu.cMemory.MenuIcoEVT[Total].EVT.XVal >> 16, C_MainMenu.cMemory.MenuIcoEVT[Total].EVT.YVal >> 16)) {
                        C_MainMenu.cMemory.MenuIcoEVT[Total].CURACT = 1;
                        C_MainMenu.cMemory.MenuIcoEVT[Total].EVT.CurFRM = 0;
                        C_MainMenu.cMemory.MenuIcoEVT[Total].EVT.CurCNT = 0;
                        C_Media.PlaySound(2);
                        C_MainMenuMemory.GameTotalScore += 5.0f;
                    }
                }
            }
            int Total2 = 92;
            while (true) {
                if (Total2 >= 106) {
                    break;
                }
                if (C_MainMenu.cMemory.MenuIcoEVT[Total2].EVT.Valid) {
                    if (this.cLib.getGameCanvas().CHKACTTouch(R.drawable.act_ballkt09, GameEvent.KeepACT, CmpY, C_MainMenu.cMemory.MenuIcoEVT[Total2].EVT.ACTIdx, C_MainMenu.cMemory.MenuIcoEVT[Total2].EVT.XVal >> 16, C_MainMenu.cMemory.MenuIcoEVT[Total2].EVT.YVal >> 16)) {
                        if (C_MainMenu.cMemory.MenuIcoEVT[Total2].EVT.Flag == 2 || C_MainMenu.cMemory.MenuIcoEVT[Total2].EVT.Flag == 1) {
                            if (C_MainMenuMemory.BallValPtr[8] == 1 && C_MainMenu.cMemory.MenuIcoEVT[Total2].EVT.Flag == 2) {
                                C_MainMenu.cMemory.MenuIcoEVT[Total2].EVT.Flag = 15;
                                C_MainMenu.cMemory.MenuIcoEVT[Total2].FlySpeedFlag = 0;
                            } else {
                                C_MainMenu.cMemory.MenuIcoEVT[Total2].EVT.Flag = 88;
                                this.EVT.XInc = 0;
                                this.EVT.XAdc = 0;
                                this.EVT.YInc = 0;
                                this.EVT.YAdc = 0;
                                Cur = 1;
                                this.CURACT = 0;
                                SetEVTCtrl(8, 0);
                                C_Media.PlaySound(5);
                                C_MainMenuMemory.BallValPtr[19] = 1;
                                C_MainMenuMemory.BallValPtr[0] = 0;
                            }
                        }
                    }
                }
                Total2++;
            }
            int Total3 = 106;
            while (true) {
                if (Total3 >= 108) {
                    break;
                }
                if (C_MainMenu.cMemory.MenuIcoEVT[Total3].EVT.Valid) {
                    if (this.cLib.getGameCanvas().CHKACTTouch(R.drawable.act_ballkt09, GameEvent.KeepACT, CmpY, R.drawable.act_tool06, C_MainMenu.cMemory.MenuIcoEVT[Total3].EVT.XVal >> 16, C_MainMenu.cMemory.MenuIcoEVT[Total3].EVT.YVal >> 16)) {
                        if (C_MainMenu.cMemory.MenuIcoEVT[Total3].EVT.Flag == 7) {
                            C_MainMenu.cMemory.MenuIcoEVT[Total3].EVT.Flag = 88;
                            int[] iArr = C_MainMenuMemory.BallValPtr;
                            iArr[7] = iArr[7] + 1;
                            C_Media.PlaySound(6);
                        }
                        if (C_MainMenu.cMemory.MenuIcoEVT[Total3].EVT.Flag == 8) {
                            C_MainMenu.cMemory.MenuIcoEVT[Total3].EVT.Flag = 88;
                            int[] iArr2 = C_MainMenuMemory.BallValPtr;
                            iArr2[7] = iArr2[7] - 1;
                            C_Media.PlaySound(7);
                        }
                    }
                }
                Total3++;
            }
            for (int Total4 = 108; Total4 < 118; Total4++) {
                if (C_MainMenu.cMemory.MenuIcoEVT[Total4].EVT.Valid) {
                    if (this.cLib.getGameCanvas().CHKACTTouch(R.drawable.act_ballkt09, GameEvent.KeepACT, CmpY, R.drawable.act_ballkt04, C_MainMenu.cMemory.MenuIcoEVT[Total4].EVT.XVal >> 16, C_MainMenu.cMemory.MenuIcoEVT[Total4].EVT.YVal >> 16)) {
                        if (C_MainMenu.cMemory.MenuIcoEVT[Total4].EVT.Flag == 9) {
                            C_MainMenu.cMemory.MenuIcoEVT[Total4].EVT.Flag = 88;
                            C_MainMenu.cMemory.MenuIcoEVT[Total4].EVTCLR();
                            C_MainMenuMemory.BallValPtr[8] = 1;
                            C_MainMenuMemory.BallValPtr[9] = 0;
                        }
                        if (C_MainMenu.cMemory.MenuIcoEVT[Total4].EVT.Flag == 10) {
                            C_MainMenuMemory.BallValPtr[0] = 1;
                            this.EVT.XInc = 2097152;
                            this.EVT.XAdc = -155648;
                        }
                    }
                }
            }
            for (int Total5 = 108; Total5 < 118; Total5++) {
                if (C_MainMenu.cMemory.MenuIcoEVT[Total5].EVT.Valid) {
                    int x1 = C_MainMenu.cMemory.MenuIcoEVT[Total5].EVT.XVal >> 16;
                    int y1 = C_MainMenu.cMemory.MenuIcoEVT[Total5].EVT.YVal >> 16;
                    if (C_MainMenu.cMemory.MenuIcoEVT[Total5].EVT.Flag == 11 && this.cLib.getGameCanvas().CHKACTTouch(R.drawable.act_ballkt09, GameEvent.KeepACT, CmpY, R.drawable.act_tool06, x1, y1)) {
                        C_MainMenuMemory.BallValPtr[2] = 0;
                        C_MainMenuMemory.BallValPtr[17] = 0;
                        C_MainMenu.cMemory.MenuIcoEVT[Total5].EVT.Flag = 88;
                        if (C_MainMenuMemory.BallValPtr[12] == 0) {
                            C_MainMenuMemory.BallValPtr[12] = 1;
                        } else {
                            C_MainMenuMemory.BallValPtr[12] = 0;
                        }
                        C_MainMenuMemory.BallValPtr[24] = 1;
                        GameReturnInit();
                    }
                    if (C_MainMenu.cMemory.MenuIcoEVT[Total5].EVT.Flag == 12 && this.cLib.getGameCanvas().CHKACTTouch(R.drawable.act_ballkt09, GameEvent.KeepACT, CmpY, C_MainMenu.cMemory.MenuIcoEVT[Total5].EVT.ACTIdx, x1, y1)) {
                        C_MainMenu.cMemory.MenuIcoEVT[Total5].EVT.Flag = 88;
                        this.EVT.XInc = 0;
                        this.EVT.XAdc = 0;
                        this.EVT.YInc = 0;
                        this.EVT.YAdc = 0;
                        Cur = 1;
                        this.CURACT = 0;
                        SetEVTCtrl(8, 0);
                        C_Media.PlaySound(5);
                        C_MainMenuMemory.BallValPtr[19] = 1;
                        C_MainMenuMemory.BallValPtr[0] = 0;
                    }
                }
            }
            int Total6 = 108;
            while (true) {
                if (Total6 >= 118) {
                    break;
                }
                if (C_MainMenu.cMemory.MenuIcoEVT[Total6].EVT.Valid) {
                    if (this.cLib.getGameCanvas().CHKACTTouch(R.drawable.act_ballkt09, GameEvent.KeepACT, CmpY, C_MainMenu.cMemory.MenuIcoEVT[Total6].EVT.ACTIdx, C_MainMenu.cMemory.MenuIcoEVT[Total6].EVT.XVal >> 16, C_MainMenu.cMemory.MenuIcoEVT[Total6].EVT.YVal >> 16)) {
                        if (C_MainMenu.cMemory.MenuIcoEVT[Total6].EVT.Flag == 13) {
                            if (C_MainMenuMemory.BallValPtr[8] == 1) {
                                C_MainMenu.cMemory.MenuIcoEVT[Total6].EVT.Flag = 15;
                                C_MainMenu.cMemory.MenuIcoEVT[Total6].FlySpeedFlag = 0;
                            } else {
                                C_MainMenu.cMemory.MenuIcoEVT[Total6].EVT.Flag = 88;
                                this.EVT.XInc = 0;
                                this.EVT.XAdc = 0;
                                this.EVT.YInc = 0;
                                this.EVT.YAdc = 0;
                                Cur = 1;
                                this.CURACT = 0;
                                SetEVTCtrl(8, 0);
                                C_Media.PlaySound(5);
                                C_MainMenuMemory.BallValPtr[19] = 1;
                                C_MainMenuMemory.BallValPtr[0] = 0;
                            }
                        }
                    }
                }
                Total6++;
            }
        }
        if (Cur == 1) {
            C_MainMenuMemory.BallValPtr[11] = 1;
        }
        return Cur;
    }

    private void Ball_PlayGod() {
        if (C_MainMenuMemory.BallValPtr[8] == 1) {
            int[] iArr = C_MainMenuMemory.BallValPtr;
            iArr[9] = iArr[9] + 1;
        } else {
            this.EVT.CurFRM = 0;
            this.EVT.CurCNT = 0;
        }
        int i = C_MainMenuMemory.BallValPtr[9];
        if (C_MainMenuMemory.BallValPtr[9] >= 200) {
            C_MainMenuMemory.BallValPtr[9] = 0;
            C_MainMenuMemory.BallValPtr[8] = 0;
        }
    }

    public void MenuIco0aEXE() {
        this.EVT.Flag = 88;
        this.EVT.ACTPtr = MenuIco0a;
        C_MainMenu.Ball_BlackShow();
        if (C_MainMenuMemory.SaveScoreFlag == 0) {
            C_MainMenuMemory.SaveScoreFlag = 1;
        }
        if (C_MainMenuMemory.GameMenuChoice != 0) {
            Menu_BestScore_Write(2);
        } else if (C_MainMenuMemory.GameCurWinLost != 0) {
            Menu_Crash_Write();
        } else if (C_MainMenuMemory.GameTotalScore > C_MainMenuMemory.GameBestScore) {
            Menu_BestScore_Write(0);
        } else {
            Menu_BestScore_Write(1);
        }
    }

    private void Menu_BestScore_Write(int Flag2) {
        if (this.MenuSmall < 1.0f && this.CurTime == 0) {
            this.MenuSmall = (float) (((double) this.MenuSmall) + 0.1d);
        }
        if (this.CurTime >= 1) {
            this.MenuSmall = (float) (((double) this.MenuSmall) - 0.1d);
            if (((double) this.MenuSmall) <= 0.4d) {
                this.MenuSmall = 0.0f;
                this.CurTime = 0;
                C_MainMenuMemory.mMenuCtrl = 9;
            }
        }
        if (this.CurTime == 0 && this.MenuSmall >= 1.0f) {
            if (C_MainMenuMemory.BestScoreAddFlag == 0) {
                C_MainMenuMemory.BestScoreAddFlag = 1;
            }
            if (C_MainMenuMemory.BestScoreAddFlag >= 3) {
                BsetGameEndTouch();
            }
        }
        int x = (int) ((78.0f * this.MenuSmall) + 167.0f);
        int y = (int) ((11.0f * this.MenuSmall) + 228.0f);
        if (Flag2 != 2) {
            this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt00, x, y, 9, 0.0f, this.MenuSmall);
        } else {
            this.cLib.getGameCanvas().WriteSprite(R.drawable.act_gameover00, x, y, 9, 0.0f, this.MenuSmall);
        }
        this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt01, (int) ((15.0f * this.MenuSmall) + 167.0f), (int) (228.0f - (64.0f * this.MenuSmall)), 9, 0.0f, this.MenuSmall);
        float Sum = C_MainMenuMemory.GameBestScore;
        Best_ScoreShow_JSR(Sum, (int) ((10.0f * this.MenuSmall) + 167.0f), (int) (228.0f - (12.0f * this.MenuSmall)));
        this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt02, (int) (167.0f - (36.0f * this.MenuSmall)), (int) (228.0f - (72.0f * this.MenuSmall)), 9, 0.0f, this.MenuSmall);
        int x2 = (int) (167.0f - (41.0f * this.MenuSmall));
        int y2 = (int) (228.0f - (12.0f * this.MenuSmall));
        if (C_MainMenuMemory.BestScoreAddFlag == 0) {
            Sum = 0.0f;
        }
        if (this.cLib.getInput().CHKTouchDown()) {
            C_MainMenuMemory.BestScorePadAddFlag = 1;
        }
        if (this.cLib.getInput().CHKTouchUp()) {
            C_MainMenuMemory.BestScorePadAddFlag = 0;
        }
        if (C_MainMenuMemory.BestScoreAddFlag >= 1) {
            if (C_MainMenuMemory.BestScoreAddFlag == 1) {
                C_Media.PlaySound(9);
                C_MainMenuMemory.BestScoreAdd += 5.0f;
                if (C_MainMenuMemory.BestScorePadAddFlag >= 1) {
                    C_MainMenuMemory.BestScorePadAddFlag += 6;
                    C_MainMenuMemory.BestScoreAdd += (float) C_MainMenuMemory.BestScorePadAddFlag;
                }
            }
            Sum = C_MainMenuMemory.BestScoreAdd;
            if (Sum >= C_MainMenuMemory.GameTotalScore) {
                Sum = C_MainMenuMemory.GameTotalScore;
                if (C_MainMenuMemory.BestScoreAddFlag < 2) {
                    C_MainMenuMemory.BestScoreAddFlag = 2;
                }
                C_MainMenuMemory.BestScoreAdd = C_MainMenuMemory.GameTotalScore;
            }
        }
        Best_ScoreShow_JSR(Sum, x2, y2);
        int x3 = (int) (167.0f - (39.0f * this.MenuSmall));
        int y3 = (int) ((104.0f * this.MenuSmall) + 228.0f);
        if ((Flag2 != 2 && C_MainMenuMemory.BestScoreAddFlag >= 2) || (C_MainMenuMemory.BestScoreAddFlag >= 2 && C_MainMenuMemory.GameTotalScore >= C_MainMenuMemory.GameBestScore && Flag2 == 2)) {
            if (this.StarSmall > 0.0f) {
                this.StarSmall = (float) (((double) this.StarSmall) - 0.5d);
            } else if (C_MainMenuMemory.BestScoreAddFlag == 2) {
                Ball_BestStarShowInit();
                C_Media.PlaySound(10);
                C_MainMenuMemory.BestScoreAddFlag = 3;
            }
            int i = C_MainMenuMemory.SaveNewAssess;
            if (Flag2 == 2) {
                i = 3;
            }
            this.cLib.getGameCanvas().WriteSprite(Ball_PassStarAct[i], x3, y3, 9, 0.0f, this.MenuSmall + this.StarSmall);
        }
        if (Flag2 == 2 && C_MainMenuMemory.BestScoreAddFlag >= 2 && C_MainMenuMemory.GameTotalScore < C_MainMenuMemory.GameBestScore && C_MainMenuMemory.BestScoreAddFlag == 2) {
            C_MainMenuMemory.BestScoreAddFlag = 3;
        }
        int x4 = (int) (167.0f - (127.0f * this.MenuSmall));
        int y4 = (int) (228.0f - (113.0f * this.MenuSmall));
        if (C_MainMenuMemory.GameEndFlag == 3) {
            this.CurTime++;
            this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt05, x4, y4, 9, 0.0f, this.MenuSmall);
            this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt06, x4, y4, 9, 0.0f, this.MenuSmall);
        } else {
            this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt05, x4, y4, 9, 0.0f, this.MenuSmall);
        }
        int x5 = (int) (167.0f - (127.0f * this.MenuSmall));
        int y5 = (int) ((12.0f * this.MenuSmall) + 228.0f);
        if (Flag2 != 2) {
            if (C_MainMenuMemory.GameEndFlag == 5) {
                this.CurTime++;
                this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt03, x5, y5, 9, 0.0f, this.MenuSmall);
                this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt06, x5, y5, 9, 0.0f, this.MenuSmall);
            } else {
                this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt03, x5, y5, 9, 0.0f, this.MenuSmall);
            }
        }
        int x6 = (int) (167.0f - (127.0f * this.MenuSmall));
        int y6 = (int) ((139.0f * this.MenuSmall) + 228.0f);
        if (C_MainMenuMemory.GameEndFlag == 4) {
            this.CurTime++;
            this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt04, x6, y6, 9, 0.0f, this.MenuSmall);
            this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt06, x6, y6, 9, 0.0f, this.MenuSmall);
            return;
        }
        this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt04, x6, y6, 9, 0.0f, this.MenuSmall);
    }

    private void Menu_Crash_Write() {
        if (this.MenuSmall < 1.0f && this.CurTime == 0) {
            this.MenuSmall = (float) (((double) this.MenuSmall) + 0.1d);
        }
        if (this.CurTime >= 1) {
            this.MenuSmall = (float) (((double) this.MenuSmall) - 0.1d);
            if (((double) this.MenuSmall) <= 0.4d) {
                this.MenuSmall = 0.0f;
                this.CurTime = 0;
                C_MainMenuMemory.mMenuCtrl = 9;
            }
        }
        if (this.CurTime == 0 && this.MenuSmall >= 1.0f) {
            GameEndTouch();
        }
        this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt07, (int) ((57.0f * this.MenuSmall) + 167.0f), (int) ((4.0f * this.MenuSmall) + 228.0f), 9, 0.0f, this.MenuSmall);
        int x = (int) (167.0f - (24.0f * this.MenuSmall));
        int y = (int) ((5.0f * this.MenuSmall) + 228.0f);
        if (C_MainMenuMemory.GameEndFlag == 3) {
            this.CurTime++;
            this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt05, x, y, 9, 0.0f, this.MenuSmall);
            this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt06, x, y, 9, 0.0f, this.MenuSmall);
        } else {
            this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt05, x, y, 9, 0.0f, this.MenuSmall);
        }
        int x2 = (int) (167.0f - (82.0f * this.MenuSmall));
        int y2 = (int) ((3.0f * this.MenuSmall) + 228.0f);
        if (C_MainMenuMemory.GameEndFlag == 4) {
            this.CurTime++;
            this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt04, x2, y2, 9, 0.0f, this.MenuSmall);
            this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt06, x2, y2, 9, 0.0f, this.MenuSmall);
            return;
        }
        this.cLib.getGameCanvas().WriteSprite(R.drawable.act_passkt04, x2, y2, 9, 0.0f, this.MenuSmall);
    }

    private void BsetGameEndTouch() {
        if (this.cLib.getInput().CHKTouchDown()) {
            int mTouchX = this.cLib.getInput().GetTouchDownX();
            int mTouchY = this.cLib.getInput().GetTouchDownY();
            for (int Sum = 0; Sum <= 2; Sum++) {
                int x = BsetGameEnd_TouchX[Sum][0];
                int x1 = BsetGameEnd_TouchX[Sum][1];
                int y = BsetGameEnd_TouchY[Sum][0];
                int y1 = BsetGameEnd_TouchY[Sum][1];
                if (mTouchX >= x && mTouchX <= x1 && mTouchY >= y && mTouchY <= y1) {
                    if (Sum == 0) {
                        C_MainMenuMemory.GameEndFlag = 3;
                    }
                    if (Sum == 1) {
                        C_MainMenuMemory.GameEndFlag = 5;
                    }
                    if (Sum == 2) {
                        C_MainMenuMemory.GameEndFlag = 4;
                    }
                    this.CurTime = 0;
                    C_Media.PlaySound(0);
                }
            }
        }
    }

    private void Menu_Success_Write(int Flag2) {
    }

    private void Best_StarShow_JSR(int Num, int x, int y) {
    }

    private void Best_ScoreShow_JSR(float Total, int x, int y) {
        int z = 0;
        int Sum = 6;
        while (true) {
            if (Sum < 0) {
                break;
            } else if (Total >= C_MainMenu.Plane_ScoreCmp[Sum]) {
                z = Sum;
                break;
            } else {
                Sum--;
            }
        }
        if (Total >= 1000000.0f) {
            this.cLib.getGameCanvas().WriteSprite(Plane_NumberAct_Best[(int) ((Total % 1.0E7f) / 1000000.0f)], x, y, 9, 0.0f, this.MenuSmall);
        }
        if (Total >= 100000.0f) {
            if (z != 5) {
                y += (int) (((float) 18) * this.MenuSmall);
            }
            this.cLib.getGameCanvas().WriteSprite(Plane_NumberAct_Best[(int) ((Total % 1000000.0f) / 100000.0f)], x, y, 9, 0.0f, this.MenuSmall);
        }
        if (Total >= 10000.0f) {
            if (z != 4) {
                y += (int) (((float) 18) * this.MenuSmall);
            }
            this.cLib.getGameCanvas().WriteSprite(Plane_NumberAct_Best[(int) ((Total % 100000.0f) / 10000.0f)], x, y, 9, 0.0f, this.MenuSmall);
        }
        if (Total >= 1000.0f) {
            if (z != 3) {
                y += (int) (((float) 18) * this.MenuSmall);
            }
            this.cLib.getGameCanvas().WriteSprite(Plane_NumberAct_Best[(int) ((Total % 10000.0f) / 1000.0f)], x, y, 9, 0.0f, this.MenuSmall);
        }
        if (Total >= 100.0f) {
            if (z != 2) {
                y += (int) (((float) 18) * this.MenuSmall);
            }
            this.cLib.getGameCanvas().WriteSprite(Plane_NumberAct_Best[(int) ((Total % 1000.0f) / 100.0f)], x, y, 9, 0.0f, this.MenuSmall);
        }
        if (Total >= 10.0f) {
            if (z != 1) {
                y += (int) (((float) 18) * this.MenuSmall);
            }
            this.cLib.getGameCanvas().WriteSprite(Plane_NumberAct_Best[(int) ((Total % 100.0f) / 10.0f)], x, y, 9, 0.0f, this.MenuSmall);
        }
        if (z != 0) {
            y += (int) (((float) 18) * this.MenuSmall);
        }
        this.cLib.getGameCanvas().WriteSprite(Plane_NumberAct_Best[(int) (Total % 10.0f)], x, y, 9, 0.0f, this.MenuSmall);
    }

    private void GameEndTouch() {
        if (this.cLib.getInput().CHKTouchDown()) {
            int mTouchX = this.cLib.getInput().GetTouchDownX();
            int mTouchY = this.cLib.getInput().GetTouchDownY();
            for (int Sum = 0; Sum <= 1; Sum++) {
                int x = GameEnd_TouchX[Sum][0];
                int x1 = GameEnd_TouchX[Sum][1];
                int y = GameEnd_TouchY[Sum][0];
                int y1 = GameEnd_TouchY[Sum][1];
                if (mTouchX >= x && mTouchX <= x1 && mTouchY >= y && mTouchY <= y1) {
                    C_MainMenuMemory.GameEndFlag = Sum + 3;
                    this.CurTime = 0;
                    C_Media.PlaySound(0);
                }
            }
        }
    }

    private void GameEndTouch_OK(int Flag2) {
        int z;
        if (Flag2 == 3) {
            z = 3;
        } else {
            z = 2;
        }
        if (this.cLib.getInput().CHKTouchDown()) {
            int mTouchX = this.cLib.getInput().GetTouchDownX();
            int mTouchY = this.cLib.getInput().GetTouchDownY();
            for (int Sum = 0; Sum <= 0; Sum++) {
                int x = GameEnd_TouchX[Sum + z][0];
                int x1 = GameEnd_TouchX[Sum + z][1];
                int y = GameEnd_TouchY[Sum + z][0];
                int y1 = GameEnd_TouchY[Sum + z][1];
                if (mTouchX >= x && mTouchX <= x1 && mTouchY >= y && mTouchY <= y1) {
                    C_MainMenuMemory.GameEndFlag = 4;
                    this.CurTime = 0;
                    C_Media.PlaySound(0);
                }
            }
        }
    }

    public void MenuIco0bEXE() {
        int i = this.EVT.XVal >> 16;
        int CurYVal = this.EVT.YVal >> 16;
        if ((CurYVal <= C_MainMenuMemory.BallValPtr[13] && C_MainMenuMemory.BallValPtr[12] == 0) || (CurYVal >= C_MainMenuMemory.BallValPtr[13] && C_MainMenuMemory.BallValPtr[12] == 1)) {
            this.EVT.Flag = 88;
            EVTCLR();
        }
        if (this.CURACT == 0) {
            this.EVT.CurFRM = 0;
        } else {
            this.EVT.CurFRM = 1;
        }
        PlayJumpSpeed();
    }

    public void GameReturnInit() {
        int y;
        int i = C_MainMenuMemory.BallValPtr[2];
        C_MainMenuMemory.BallValPtr[6] = 1;
        int i2 = C_MainMenuMemory.BallValPtr[4];
        C_MainMenuMemory.BallValPtr[2] = 0;
        if (C_MainMenuMemory.BallValPtr[12] == 0) {
            y = 112;
        } else {
            y = 368;
        }
        C_MainMenu.cMemory.MenuIcoEVT[0].EVT.YVal = y << 16;
        C_MainMenu.cMemory.MenuIcoEVT[1].EVT.YVal = 0 << 16;
        C_MainMenuMemory.BallValPtr[4] = 0;
    }

    public void MenuIco0cEXE() {
        int i = this.EVT.XVal >> 16;
        int CurYVal = this.EVT.YVal >> 16;
        if ((CurYVal <= C_MainMenuMemory.BallValPtr[13] && C_MainMenuMemory.BallValPtr[12] == 0) || (CurYVal >= C_MainMenuMemory.BallValPtr[13] && C_MainMenuMemory.BallValPtr[12] == 1)) {
            this.EVT.Flag = 88;
            EVTCLR();
        }
        PlayJumpSpeed();
    }

    public void MenuIco0dEXE() {
        int i = this.EVT.XVal >> 16;
        int CurYVal = this.EVT.YVal >> 16;
        if ((CurYVal <= C_MainMenuMemory.BallValPtr[13] && C_MainMenuMemory.BallValPtr[12] == 0) || (CurYVal >= C_MainMenuMemory.BallValPtr[13] && C_MainMenuMemory.BallValPtr[12] == 1)) {
            this.EVT.Flag = 88;
            EVTCLR();
        }
        PlayJumpSpeed();
    }

    public void MenuIco0eEXE() {
        int CurXVal = this.EVT.XVal >> 16;
        int i = this.EVT.YVal >> 16;
        int[] iArr = C_MainMenuMemory.BallValPtr;
        iArr[28] = iArr[28] + 1;
        if (C_MainMenuMemory.BallValPtr[28] >= 10) {
            C_MainMenuMemory.BallValPtr[28] = 10;
            MenuChoiceGamePreeKey(CurXVal);
            MenuShow_Pad();
        }
        int CurXVal2 = this.EVT.XVal >> 16;
        int CurYVal = this.EVT.YVal >> 16;
        if (C_MainMenuMemory.SaveMenuLevelStop == 0 && C_MainMenuMemory.PreeOtherCurKey != 1) {
            this.cLib.getGameCanvas().WriteSprite(R.drawable.act_menudw00, CurXVal2, CurYVal, 3);
        }
        for (int i2 = 1; i2 <= 47; i2++) {
            int CmpX = CurXVal2 - (i2 * 59);
            if (CmpX >= 0 && CmpX <= 320) {
                this.cLib.getGameCanvas().WriteSprite(R.drawable.act_menudw01, CurXVal2 - (i2 * 59), CurYVal, 2);
                if (C_MainMenuMemory.SaveMenuLevelStop == i2 && C_MainMenuMemory.PreeOtherCurKey != 1) {
                    this.cLib.getGameCanvas().WriteSprite(R.drawable.act_menudw00, CurXVal2 - (i2 * 59), CurYVal, 3);
                }
            }
        }
        if (this.AddXSpeed == 1 && (this.EVT.XInc >> 16) <= 0) {
            this.EVT.XInc = 0;
            this.EVT.XAdc = 0;
        }
        if (this.AddXSpeed == 2 && (this.EVT.XInc >> 16) >= 0) {
            this.EVT.XInc = 0;
            this.EVT.XAdc = 0;
        }
        if (this.AddXSpeed == 3 && (this.EVT.XVal >> 16) >= 243) {
            this.EVT.XVal = 253 << 16;
            this.AddXSpeed = 0;
            this.EVT.XInc = 0;
            this.EVT.XAdc = 0;
        }
        if (this.AddXSpeed == 4 && (this.EVT.XVal >> 16) <= 2861) {
            this.EVT.XVal = 2851 << 16;
            this.AddXSpeed = 0;
            this.EVT.XInc = 0;
            this.EVT.XAdc = 0;
        }
        if (this.AddXSpeed == 0) {
            this.EVT.XInc = 0;
            this.EVT.XAdc = 0;
        }
        MenuChoiceGameShow();
    }

    private void MenuChoiceGamePreeKey(int CurXVal) {
        int z = 0;
        if (this.cLib.getInput().CHKTouchDown()) {
            this.NewTouchXVal = this.cLib.getInput().GetTouchDownX();
            this.OldTouchXVal = this.NewTouchXVal;
            this.EVT.XInc = 0;
            this.EVT.XAdc = 0;
            this.AddXSpeed = 0;
            z = 1;
            C_MainMenuMemory.BallValPtr[27] = 0;
            if (this.cLib.getGameCanvas().CHKACTTouch(this.cLib.getInput().GetTouchDownX(), this.cLib.getInput().GetTouchDownY(), 1, 1, 1, 1, R.drawable.act_menukt23, 285, 416)) {
                C_MainMenuMemory.PreeOtherCurKey = 2;
                C_Media.PlaySound(0);
            }
        }
        if (this.cLib.getInput().CHKTouchMove()) {
            z = 1;
            this.NewTouchXVal = this.cLib.getInput().GetTouchMoveX();
            if (this.NewTouchXVal > 26 && this.NewTouchXVal < 255) {
                if (this.NewTouchXVal > this.OldTouchXVal) {
                    int CurXVal2 = this.NewTouchXVal - this.OldTouchXVal;
                    this.EVT.XVal += CurXVal2 << 16;
                    this.AddXSpeed = 1;
                } else {
                    int CurXVal3 = this.OldTouchXVal - this.NewTouchXVal;
                    this.EVT.XVal -= CurXVal3 << 16;
                    this.AddXSpeed = 2;
                }
                if (this.CurTime >= 4) {
                    this.CurTime = 4;
                    for (int i = 1; i <= 3; i++) {
                        C_MainMenuMemory.SaveMenuXStr[i - 1] = C_MainMenuMemory.SaveMenuXStr[i];
                    }
                }
                C_MainMenuMemory.SaveMenuXStr[this.CurTime] = Math.abs(this.NewTouchXVal - this.OldTouchXVal);
                if (C_MainMenuMemory.SaveMenuXStr[this.CurTime] >= 5) {
                    C_MainMenuMemory.BallValPtr[27] = 1;
                }
                int Total = 0;
                for (int i2 = 0; i2 <= this.CurTime; i2++) {
                    Total += Math.abs(C_MainMenuMemory.SaveMenuXStr[i2]);
                }
                if (Total / (this.CurTime + 1) <= 20) {
                    this.AddXSpeed = 0;
                }
                this.CurTime = this.CurTime + 1;
            }
        } else {
            this.CurTime = 0;
        }
        if (this.cLib.getInput().CHKTouchUp()) {
            z = 1;
            int CurXVal4 = this.EVT.XVal >> 16;
            if (this.AddXSpeed == 1) {
                if ((this.EVT.XInc >> 16) > 0) {
                    this.EVT.XInc += 524288;
                } else {
                    this.EVT.XInc = 524288;
                    this.EVT.XAdc = -8192;
                }
                if (CurXVal4 > 2867) {
                    this.EVT.XVal = 187891712;
                    this.EVT.XInc = 0;
                    this.EVT.XAdc = 0;
                }
            }
            if (this.AddXSpeed == 2) {
                if ((this.EVT.XInc >> 16) < 0) {
                    this.EVT.XInc -= 524288;
                } else {
                    this.EVT.XInc = -524288;
                    this.EVT.XAdc = GameEvent.FIXEVT;
                }
                if (CurXVal4 < 237) {
                    this.EVT.XVal = 15532032;
                    this.EVT.XInc = 0;
                    this.EVT.XAdc = 0;
                }
            }
        }
        int CurXVal5 = this.EVT.XVal >> 16;
        if (z == 0) {
            if (CurXVal5 < 253 && this.AddXSpeed != 3) {
                this.AddXSpeed = 3;
                this.EVT.XInc = 1572864;
                this.EVT.XAdc = -8192;
            }
            if (CurXVal5 > 2851 && this.AddXSpeed != 4) {
                this.AddXSpeed = 4;
                this.EVT.XInc = -1572864;
                this.EVT.XAdc = GameEvent.FIXEVT;
            }
        }
        this.OldTouchXVal = this.NewTouchXVal;
    }

    private void MenuChoiceGameShow() {
        int CurXVal = this.EVT.XVal >> 16;
        int i = this.EVT.YVal >> 16;
        for (int i2 = 0; i2 <= 47; i2++) {
            int CmpX = CurXVal - (i2 * 59);
            if (CmpX >= 0 && CmpX <= 320) {
                int x = (CurXVal - 25) - (i2 * 59);
                this.cLib.getGameCanvas().WriteSprite(R.drawable.act_menukt22, x, 36, 3);
                if (i2 <= C_MainMenuMemory.GameCurChoice) {
                    int Score = i2 + 1;
                    if (Score <= 9) {
                        this.cLib.getGameCanvas().WriteSprite(C_MainMenuData.MenuChoiceGameShow_ZT[i2 + 1], x + 2, 36, 3);
                    } else {
                        this.cLib.getGameCanvas().WriteSprite(C_MainMenuData.MenuChoiceGameShow_ZT[Score / 10], x + 2, 36 - 7, 3);
                        this.cLib.getGameCanvas().WriteSprite(C_MainMenuData.MenuChoiceGameShow_ZT[Score % 10], x + 2, 36 + 5, 3);
                    }
                    this.cLib.getGameCanvas().WriteSprite(R.drawable.act_menukt0a, x + 6, 112, 3);
                    MenuBestScoreShow_JSR(C_UserRecordData.mUserScore[i2 + 4], x - 2, 172);
                    this.cLib.getGameCanvas().WriteSprite(R.drawable.act_menukt0c, x + 2, 277, 3);
                    MenuScoreShow_JSR(C_UserRecordData.mUserScore[i2 + 55], x, 331);
                    this.cLib.getGameCanvas().WriteSprite(C_MainMenuData.MenuChoiceGameShow_Star[C_MainMenuMemory.AssessStr[i2]], x - 4, 427, 3);
                } else {
                    this.cLib.getGameCanvas().WriteSprite(R.drawable.act_menukt29, x + 2, 36, 3);
                    this.cLib.getGameCanvas().WriteSprite(R.drawable.act_menukt0a, x + 6, 112, 3);
                    int y = 176;
                    for (int Sum = 0; Sum <= 3; Sum++) {
                        this.cLib.getGameCanvas().WriteSprite(R.drawable.act_menukt0b, x + 2, y, 3);
                        y += 16;
                    }
                    this.cLib.getGameCanvas().WriteSprite(R.drawable.act_menukt0c, x + 2, 277, 3);
                    int y2 = 331;
                    for (int Sum2 = 0; Sum2 <= 3; Sum2++) {
                        this.cLib.getGameCanvas().WriteSprite(R.drawable.act_menukt2a, x, y2, 3);
                        y2 += 13;
                    }
                    this.cLib.getGameCanvas().WriteSprite(C_MainMenuData.MenuChoiceGameShow_Star[0], x - 4, 427, 3);
                }
            }
        }
    }

    private void MenuBestScoreShow_JSR(int Total, int x, int y) {
        int z = 0;
        int Sum = 4;
        while (true) {
            if (Sum < 0) {
                break;
            } else if (((float) Total) >= C_MainMenu.Plane_ScoreCmp[Sum]) {
                z = Sum;
                break;
            } else {
                Sum--;
            }
        }
        if (Total >= 10000) {
            this.cLib.getGameCanvas().WriteSprite(Plane_NumberAct_Best[(Total % 100000) / 10000], x, y, 3);
        }
        if (Total >= 1000) {
            if (z != 3) {
                y += 16;
            }
            this.cLib.getGameCanvas().WriteSprite(Plane_NumberAct_Best[(Total % 10000) / 1000], x, y, 3);
        }
        if (Total >= 100) {
            if (z != 2) {
                y += 16;
            }
            this.cLib.getGameCanvas().WriteSprite(Plane_NumberAct_Best[(Total % 1000) / 100], x, y, 3);
        }
        if (Total >= 10) {
            if (z != 1) {
                y += 16;
            }
            this.cLib.getGameCanvas().WriteSprite(Plane_NumberAct_Best[(Total % 100) / 10], x, y, 3);
        }
        if (z != 0) {
            y += 16;
        }
        this.cLib.getGameCanvas().WriteSprite(Plane_NumberAct_Best[Total % 10], x, y, 3);
    }

    private void MenuScoreShow_JSR(int Total, int x, int y) {
        int z = 0;
        int Sum = 4;
        while (true) {
            if (Sum < 0) {
                break;
            } else if (((float) Total) >= C_MainMenu.Plane_ScoreCmp[Sum]) {
                z = Sum;
                break;
            } else {
                Sum--;
            }
        }
        if (Total >= 10000) {
            this.cLib.getGameCanvas().WriteSprite(C_MainMenuData.MenuChoiceGameShow_Score[(Total % 100000) / 10000], x, y, 3);
        }
        if (Total >= 1000) {
            if (z != 3) {
                y += 13;
            }
            this.cLib.getGameCanvas().WriteSprite(C_MainMenuData.MenuChoiceGameShow_Score[(Total % 10000) / 1000], x, y, 3);
        }
        if (Total >= 100) {
            if (z != 2) {
                y += 13;
            }
            this.cLib.getGameCanvas().WriteSprite(C_MainMenuData.MenuChoiceGameShow_Score[(Total % 1000) / 100], x, y, 3);
        }
        if (Total >= 10) {
            if (z != 1) {
                y += 13;
            }
            this.cLib.getGameCanvas().WriteSprite(C_MainMenuData.MenuChoiceGameShow_Score[(Total % 100) / 10], x, y, 3);
        }
        if (z != 0) {
            y += 13;
        }
        this.cLib.getGameCanvas().WriteSprite(C_MainMenuData.MenuChoiceGameShow_Score[Total % 10], x, y, 3);
    }

    private void MenuShow_Pad() {
        int CurXVal = this.EVT.XVal >> 16;
        int CurYVal = this.EVT.YVal >> 16;
        if (this.cLib.getInput().CHKTouchUp() && C_MainMenuMemory.BallValPtr[27] == 0) {
            int x = this.cLib.getInput().GetTouchUpX();
            int y = this.cLib.getInput().GetTouchUpY();
            if (x > 26 && x < 255) {
                for (int i = 0; i <= 47; i++) {
                    int CmpX = CurXVal - (i * 59);
                    if (CmpX >= 0 && CmpX <= 320 && this.cLib.getGameCanvas().CHKACTTouch(x, y, 1, 1, 1, 1, R.drawable.act_menukt21, CmpX, CurYVal)) {
                        C_MainMenuMemory.PreeOtherCurKey = 1;
                        C_MainMenuMemory.BallValPtr[26] = i;
                        C_Media.PlaySound(0);
                    }
                }
            }
        }
    }

    public void MenuIco0fEXE() {
        this.EVT.ACTPtr = MenuIco0f;
        if (CHKEVTACTEnd()) {
            EVTCLR();
        }
    }

    private void Ball_BestStarShowInit() {
        for (int i = 118; i < 120; i++) {
            if (!C_MainMenu.cMemory.MenuIcoEVT[i].EVT.Valid) {
                C_MainMenu.cMemory.MenuIcoEVT[i].Angle = 0;
                C_MainMenu.cMemory.MenuIcoEVT[i].Flag = 0;
                C_MainMenu.cMemory.MenuIcoEVT[i].CURACT = 0;
                C_MainMenu.cMemory.MenuIcoEVT[i].MakeEVENT(118, 345, 0);
                C_MainMenu.cMemory.MenuIcoEVT[i].SetMenuIcoType(15);
                C_MainMenu.cMemory.MenuIcoEVT[i].EVT.Status |= 9728;
                C_MainMenu.cMemory.MenuIcoEVT[i].EVT.Attrib = 8;
                C_MainMenu.cMemory.MenuIcoEVT[i].EVT.Ctrl = 15;
                C_MainMenu.cMemory.MenuIcoEVT[i].EVT.Flag = i;
                C_MainMenu.cMemory.MenuIcoEVT[i].EVTCur = i;
                return;
            }
        }
    }
}
