package com.game.DodgeBall_AdSence;

public class C_MainMenuMemory {
    public static int ActivityFlag = 0;
    public static int[] AssessStr = new int[50];
    public static final int BALLTOTAL = 150;
    public static final int BallMenuAngle = 29;
    public static final int BallStartFlag = 21;
    public static int[] BallValPtr = new int[BALLTOTAL];
    public static final int BallYVal = 16;
    public static float BestScoreAdd = 0.0f;
    public static int BestScoreAddFlag = 0;
    public static int BestScorePadAddFlag = 0;
    public static int BuyFlag = 0;
    public static final int ChoiceGameCur = 26;
    public static final int ChoiceGameLevelFlag = 14;
    public static final int CurClrYVal = 13;
    public static int EndLessFlag = 0;
    public static final int EnemyReturnFlag = 24;
    public static float GameBestScore = 0.0f;
    public static int GameBestScore_1 = 0;
    public static final int GameBrack = 20;
    public static int GameChoiceBigKey = 0;
    public static int GameCurChoice = 0;
    public static int GameCurWinLost = 0;
    public static int GameEndFlag = 0;
    public static int GameLevel = 0;
    public static final int GameLevel_Game = 15;
    public static int GameMenuChoice = 0;
    public static int GameOpenScrFlag = 0;
    public static float GameTotalScore = 0.0f;
    public static int GameTotalScore_1 = 0;
    public static int[] GreeHitActStr = new int[9];
    public static int HelpFlag = 0;
    public static final int KeyFlag = 22;
    public static final int MakeBallSum = 2;
    public static final int MakeEVTFlag = 5;
    public static final int MakeEVTSecondFlag = 6;
    public static int MakePauseFlag = 0;
    public static int MakePlaneFlag = 0;
    public static int MenuMusicFlag = 0;
    public static final int MenuPadTime = 28;
    public static int MenuUpDownTime = 0;
    public static int MoreGamePreeFlag = 0;
    public static int MusicPreeFlag = 0;
    public static int OpenBjFlag = 0;
    public static final int OverFlag = 19;
    public static final int PadMoveFlag = 27;
    public static int PauseFlag = 0;
    public static float PauseSmail = 0.0f;
    public static int PauseTime = 0;
    public static int PlaneBombFlag = 0;
    public static int PlaneDownFlag = 0;
    public static int[] PlaneHitTypeStr = new int[9];
    public static int PlaneTreeDrmFlag = 0;
    public static final int PlayAngleWay = 12;
    public static final int PlayDownFlag = 10;
    public static final int PlayGodFlag = 8;
    public static final int PlayGodFlagTime = 9;
    public static int PlayPreeFlag = 0;
    public static final int PlaySpeedXFlag = 1;
    public static final int PlayToolHitFlag = 11;
    public static final int PlayUpFlag = 0;
    public static int PreeCurKey = 0;
    public static int PreeFlag = 0;
    public static int PreeOtherCurKey = 0;
    public static int PreeShowTime = 0;
    public static final int PullScrEndFlag = 17;
    public static final int PullXScr = 3;
    public static final int PullYScr = 4;
    public static final int ReadyFlag = 18;
    public static int[] RoadHitStr = new int[34];
    public static int SaveHelpFlag = 0;
    public static int SaveHelpJsr = 0;
    public static int SaveMenuLevelStop = 0;
    public static int[] SaveMenuXStr = new int[10];
    public static int SaveNewAssess = 0;
    public static int SaveOldAssess = 0;
    public static int SavePlaneBombSum = 0;
    public static int SavePlaneLong = 0;
    public static int SaveScoreFlag = 0;
    public static int SongPreeFlag = 0;
    public static final int SpeedLevel = 7;
    public static final int StarActFlag = 25;
    public static final int StopPreeKey = 23;
    public static int mDataHeight;
    public static int mDataWidth;
    public static int[] mDestData;
    public static int mLoadingCount;
    public static int mLoadingInc;
    public static int mMenuCtrl;
    public static int[] mSrcData;
    public C_MenuIco[] MenuIcoEVT;

    public C_MainMenuMemory() {
        mMenuCtrl = 0;
        for (int i = 0; i < 150; i++) {
            BallValPtr[i] = 0;
        }
        this.MenuIcoEVT = null;
        mSrcData = null;
        mDestData = null;
        mDataWidth = 0;
        mDataHeight = 0;
        mLoadingCount = 0;
        mLoadingInc = 0;
        SavePlaneLong = 0;
        SavePlaneBombSum = 20;
        PlaneDownFlag = 0;
        GameCurWinLost = 0;
        for (int i2 = 0; i2 <= 8; i2++) {
            GreeHitActStr[i2] = 0;
            PlaneHitTypeStr[i2] = 0;
        }
        for (int i3 = 0; i3 < 34; i3++) {
            RoadHitStr[i3] = 0;
        }
        GameTotalScore = 0.0f;
        PlaneBombFlag = 0;
        GameEndFlag = 0;
        GameOpenScrFlag = 0;
        MusicPreeFlag = 0;
        SongPreeFlag = 0;
        MoreGamePreeFlag = 0;
        PlayPreeFlag = 0;
        PreeShowTime = 0;
        GameMenuChoice = 0;
        for (int i4 = 0; i4 < 50; i4++) {
            AssessStr[i4] = 0;
        }
        PreeCurKey = 0;
        PreeOtherCurKey = 0;
        GameChoiceBigKey = 0;
        MakePlaneFlag = 0;
        PauseFlag = 0;
        PauseTime = 0;
        PauseSmail = 0.0f;
        MakePauseFlag = 0;
        PlaneTreeDrmFlag = 0;
        SaveScoreFlag = 0;
        SaveOldAssess = 0;
        SaveNewAssess = 0;
        HelpFlag = 0;
        SaveHelpFlag = 0;
        SaveHelpJsr = 0;
        BuyFlag = 0;
        PreeFlag = 0;
        EndLessFlag = 0;
        SaveMenuLevelStop = 0;
        ActivityFlag = 0;
        BestScoreAddFlag = 0;
        BestScorePadAddFlag = 0;
        MenuUpDownTime = 0;
        BestScoreAdd = 0.0f;
        OpenBjFlag = 0;
        MenuMusicFlag = 0;
        for (int i5 = 0; i5 < 10; i5++) {
            SaveMenuXStr[i5] = 0;
        }
        this.MenuIcoEVT = null;
    }
}
