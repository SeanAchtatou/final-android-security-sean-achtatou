package com.MoreGames.API;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.game.DodgeBall_AdSence.MyPocketPlane;
import com.google.ads.AdView;

public class MoreGames extends Activity {
    private final int Web_Http = 1;
    private final int Web_Market = 2;
    private final int Web_Normal = 0;
    private AdView adView;
    private String homeUrl = "http://www.runnergameshk.com/?pd=com.game.DodgeBall_AdSence";
    private ProgressBar mProgressBar;
    private WebView mWebView;
    private RelativeLayout relativeLayout;

    public void onCreate(Bundle savedInstanceState) {
        int screenHeight;
        super.onCreate(savedInstanceState);
        this.relativeLayout = new RelativeLayout(this);
        setContentView(this.relativeLayout);
        this.mProgressBar = new ProgressBar(this);
        RelativeLayout.LayoutParams progressParams = new RelativeLayout.LayoutParams(30, 30);
        progressParams.addRule(13, -1);
        this.mProgressBar.setLayoutParams(progressParams);
        this.relativeLayout.addView(this.mProgressBar);
        this.mWebView = new WebView(this);
        this.relativeLayout.addView(this.mWebView, new RelativeLayout.LayoutParams(-1, -2));
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        if (dm.widthPixels > dm.heightPixels) {
            screenHeight = dm.widthPixels;
        } else {
            screenHeight = dm.heightPixels;
        }
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.getSettings().setSupportZoom(false);
        if (screenHeight <= 576) {
            this.mWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        } else {
            this.mWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        }
        this.mWebView.getSettings().setBuiltInZoomControls(true);
        this.mWebView.setScrollBarStyle(0);
        this.mWebView.requestFocus();
        this.mWebView.loadUrl(this.homeUrl);
        this.mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                switch (MoreGames.this.geUrlType(url)) {
                    case 2:
                        MoreGames.this.loadmarket(url);
                        return true;
                    default:
                        MoreGames.this.loadurl(view, url);
                        return true;
                }
            }
        });
        this.adView = MyPocketPlane.adView;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.adView != null) {
            this.relativeLayout.removeView(this.adView);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.adView != null) {
            RelativeLayout.LayoutParams adViewLayoutParams = new RelativeLayout.LayoutParams(-1, -2);
            adViewLayoutParams.addRule(12, -1);
            this.relativeLayout.addView(this.adView, adViewLayoutParams);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && this.adView != null) {
            this.relativeLayout.removeView(this.adView);
        }
        return super.onKeyDown(keyCode, event);
    }

    public int geUrlType(String url) {
        if (url.contains("http://")) {
            return 1;
        }
        if (url.contains("market:")) {
            return 2;
        }
        return 0;
    }

    public void loadurl(final WebView view, final String url) {
        new Thread() {
            public void run() {
                view.loadUrl(url);
            }
        }.start();
    }

    public void loadmarket(String urladdr) {
        Intent it = new Intent("android.intent.action.VIEW", Uri.parse(urladdr));
        it.addFlags(268435456);
        startActivity(it);
    }
}
