package com.google.ads;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.SystemClock;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import com.google.ads.util.a;
import oms.GameEngine.GameEvent;

public class AdActivity extends Activity implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, View.OnClickListener {
    public static final String BASE_URL_PARAM = "baseurl";
    public static final String HTML_PARAM = "html";
    public static final String INTENT_ACTION_PARAM = "i";
    public static final String ORIENTATION_PARAM = "o";
    public static final String TYPE_PARAM = "m";
    public static final String URL_PARAM = "u";
    private static final Object a = new Object();
    private static AdActivity b = null;
    private static d c = null;
    private static AdActivity d = null;
    private static AdActivity e = null;
    private g f;
    private long g;
    private RelativeLayout h;
    private AdActivity i = null;
    private boolean j;
    private VideoView k;

    private void a(g gVar, boolean z, int i2) {
        requestWindowFeature(1);
        getWindow().setFlags(GameEvent.KeepINC, GameEvent.KeepINC);
        if (gVar.getParent() != null) {
            a("Interstitial created with an AdWebView that has a parent.");
        } else if (gVar.b() != null) {
            a("Interstitial created with an AdWebView that is already in use by another AdActivity.");
        } else {
            setRequestedOrientation(i2);
            gVar.a(this);
            ImageButton imageButton = new ImageButton(getApplicationContext());
            imageButton.setImageResource(17301527);
            imageButton.setBackgroundDrawable(null);
            int applyDimension = (int) TypedValue.applyDimension(1, 1.0f, getResources().getDisplayMetrics());
            imageButton.setPadding(applyDimension, applyDimension, 0, 0);
            imageButton.setOnClickListener(this);
            this.h.addView(gVar, new ViewGroup.LayoutParams(-1, -1));
            this.h.addView(imageButton);
            setContentView(this.h);
            if (z) {
                a.a(gVar);
            }
        }
    }

    private void a(String str) {
        a.b(str);
        finish();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0025, code lost:
        r1 = new android.content.Intent(r0.getApplicationContext(), com.google.ads.AdActivity.class);
        r1.putExtra("com.google.ads.AdOpener", r5.a());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        com.google.ads.util.a.a("Launching AdActivity.");
        r0.startActivity(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0042, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0043, code lost:
        com.google.ads.util.a.a(r0.getMessage(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000a, code lost:
        r0 = r4.e();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000e, code lost:
        if (r0 != null) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        com.google.ads.util.a.e("activity was null while launching an AdActivity.");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void launchAdActivity(defpackage.d r4, defpackage.e r5) {
        /*
            java.lang.Object r0 = com.google.ads.AdActivity.a
            monitor-enter(r0)
            d r1 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0021 }
            if (r1 != 0) goto L_0x0016
            com.google.ads.AdActivity.c = r4     // Catch:{ all -> 0x0021 }
        L_0x0009:
            monitor-exit(r0)
            android.app.Activity r0 = r4.e()
            if (r0 != 0) goto L_0x0025
            java.lang.String r0 = "activity was null while launching an AdActivity."
            com.google.ads.util.a.e(r0)
        L_0x0015:
            return
        L_0x0016:
            d r1 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0021 }
            if (r1 == r4) goto L_0x0009
            java.lang.String r1 = "Tried to launch a new AdActivity with a different AdManager."
            com.google.ads.util.a.b(r1)     // Catch:{ all -> 0x0021 }
            monitor-exit(r0)     // Catch:{ all -> 0x0021 }
            goto L_0x0015
        L_0x0021:
            r1 = move-exception
            r4 = r1
            monitor-exit(r0)
            throw r4
        L_0x0025:
            android.content.Intent r1 = new android.content.Intent
            android.content.Context r2 = r0.getApplicationContext()
            java.lang.Class<com.google.ads.AdActivity> r3 = com.google.ads.AdActivity.class
            r1.<init>(r2, r3)
            java.lang.String r2 = "com.google.ads.AdOpener"
            android.os.Bundle r3 = r5.a()
            r1.putExtra(r2, r3)
            java.lang.String r2 = "Launching AdActivity."
            com.google.ads.util.a.a(r2)     // Catch:{ ActivityNotFoundException -> 0x0042 }
            r0.startActivity(r1)     // Catch:{ ActivityNotFoundException -> 0x0042 }
            goto L_0x0015
        L_0x0042:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            com.google.ads.util.a.a(r1, r0)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdActivity.launchAdActivity(d, e):void");
    }

    public g getOpeningAdWebView() {
        if (this.i != null) {
            return this.i.f;
        }
        synchronized (a) {
            if (c == null) {
                a.e("currentAdManager was null while trying to get the opening AdWebView.");
                return null;
            }
            g i2 = c.i();
            if (i2 != this.f) {
                return i2;
            }
            return null;
        }
    }

    public VideoView getVideoView() {
        return this.k;
    }

    public void onClick(View view) {
        finish();
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        a.d("Video finished playing.");
        if (this.k != null) {
            this.k.setVisibility(8);
        }
        this.f.loadUrl("javascript:AFMA_ReceiveMessage('onVideoEvent', {'event': 'finish'});");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001a, code lost:
        if (r11.i != null) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001e, code lost:
        if (com.google.ads.AdActivity.e == null) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0020, code lost:
        r11.i = com.google.ads.AdActivity.e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0024, code lost:
        com.google.ads.AdActivity.e = r11;
        r11.h = null;
        r11.j = false;
        r11.k = null;
        r1 = getIntent().getBundleExtra("com.google.ads.AdOpener");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0036, code lost:
        if (r1 != null) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0038, code lost:
        a("Could not get the Bundle used to create AdActivity.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0048, code lost:
        r2 = new defpackage.e(r1);
        r1 = r2.b();
        r4 = r2.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0057, code lost:
        if (r11 != com.google.ads.AdActivity.d) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0059, code lost:
        r8.s();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0062, code lost:
        if (r1.equals("intent") == false) goto L_0x00e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0064, code lost:
        r11.f = null;
        r11.g = android.os.SystemClock.elapsedRealtime();
        r11.j = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x006e, code lost:
        if (r4 != null) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0070, code lost:
        a("Could not get the paramMap in launchIntent()");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0076, code lost:
        r1 = r4.get(com.google.ads.AdActivity.URL_PARAM);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x007e, code lost:
        if (r1 != null) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0080, code lost:
        a("Could not get the URL parameter in launchIntent().");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0086, code lost:
        r2 = r4.get(com.google.ads.AdActivity.INTENT_ACTION_PARAM);
        r3 = r4.get(com.google.ads.AdActivity.TYPE_PARAM);
        r1 = android.net.Uri.parse(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x009a, code lost:
        if (r2 != null) goto L_0x00ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x009c, code lost:
        r1 = new android.content.Intent("android.intent.action.VIEW", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a4, code lost:
        r2 = com.google.ads.AdActivity.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00a6, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00a9, code lost:
        if (com.google.ads.AdActivity.b != null) goto L_0x00b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00ab, code lost:
        com.google.ads.AdActivity.b = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00af, code lost:
        if (com.google.ads.AdActivity.c == null) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00b1, code lost:
        com.google.ads.AdActivity.c.t();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00b6, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        com.google.ads.util.a.a("Launching an intent from AdActivity.");
        startActivity(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00c1, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00c2, code lost:
        com.google.ads.util.a.a(r1.getMessage(), r1);
        finish();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00ce, code lost:
        r4 = new android.content.Intent(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00d3, code lost:
        if (r3 == null) goto L_0x00da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00d5, code lost:
        r4.setDataAndType(r1, r3);
        r1 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00da, code lost:
        r4.setData(r1);
        r1 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        com.google.ads.util.a.e("currentAdManager is null while trying to call onLeaveApplication().");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00e8, code lost:
        r11.h = new android.widget.RelativeLayout(getApplicationContext());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00f9, code lost:
        if (r1.equals("webapp") == false) goto L_0x0170;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00fb, code lost:
        r11.f = new defpackage.g(getApplicationContext(), null);
        r1 = new defpackage.h(r8, defpackage.a.b, true, true);
        r1.b();
        r11.f.setWebViewClient(r1);
        r1 = r4.get(com.google.ads.AdActivity.URL_PARAM);
        r2 = r4.get(com.google.ads.AdActivity.BASE_URL_PARAM);
        r3 = r4.get(com.google.ads.AdActivity.HTML_PARAM);
        r7 = r4.get(com.google.ads.AdActivity.ORIENTATION_PARAM);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0137, code lost:
        if (r1 == null) goto L_0x014e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0139, code lost:
        r11.f.loadUrl(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0144, code lost:
        if ("p".equals(r7) == false) goto L_0x0161;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0146, code lost:
        r1 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0147, code lost:
        a(r11.f, false, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x014e, code lost:
        if (r3 == null) goto L_0x015a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0150, code lost:
        r11.f.loadDataWithBaseURL(r2, r3, "text/html", "utf-8", null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x015a, code lost:
        a("Could not get the URL or HTML parameter to show a web app.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0167, code lost:
        if ("l".equals(r7) == false) goto L_0x016b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0169, code lost:
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x016b, code lost:
        r1 = r8.m();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0176, code lost:
        if (r1.equals("interstitial") == false) goto L_0x0189;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0178, code lost:
        r11.f = r8.i();
        a(r11.f, true, r8.m());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0189, code lost:
        a("Unknown AdOpener, <action: " + r1 + ">");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0014, code lost:
        if (com.google.ads.AdActivity.d != null) goto L_0x0018;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0016, code lost:
        com.google.ads.AdActivity.d = r11;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r12) {
        /*
            r11 = this;
            r10 = 0
            r6 = 0
            r9 = 1
            java.lang.String r5 = "u"
            super.onCreate(r12)
            java.lang.Object r1 = com.google.ads.AdActivity.a
            monitor-enter(r1)
            d r2 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0045 }
            if (r2 == 0) goto L_0x003e
            d r8 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0045 }
            monitor-exit(r1)
            com.google.ads.AdActivity r1 = com.google.ads.AdActivity.d
            if (r1 != 0) goto L_0x0018
            com.google.ads.AdActivity.d = r11
        L_0x0018:
            com.google.ads.AdActivity r1 = r11.i
            if (r1 != 0) goto L_0x0024
            com.google.ads.AdActivity r1 = com.google.ads.AdActivity.e
            if (r1 == 0) goto L_0x0024
            com.google.ads.AdActivity r1 = com.google.ads.AdActivity.e
            r11.i = r1
        L_0x0024:
            com.google.ads.AdActivity.e = r11
            r11.h = r6
            r11.j = r10
            r11.k = r6
            android.content.Intent r1 = r11.getIntent()
            java.lang.String r2 = "com.google.ads.AdOpener"
            android.os.Bundle r1 = r1.getBundleExtra(r2)
            if (r1 != 0) goto L_0x0048
            java.lang.String r1 = "Could not get the Bundle used to create AdActivity."
            r11.a(r1)
        L_0x003d:
            return
        L_0x003e:
            java.lang.String r2 = "Could not get currentAdManager."
            r11.a(r2)     // Catch:{ all -> 0x0045 }
            monitor-exit(r1)     // Catch:{ all -> 0x0045 }
            goto L_0x003d
        L_0x0045:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        L_0x0048:
            e r2 = new e
            r2.<init>(r1)
            java.lang.String r1 = r2.b()
            java.util.HashMap r4 = r2.c()
            com.google.ads.AdActivity r2 = com.google.ads.AdActivity.d
            if (r11 != r2) goto L_0x005c
            r8.s()
        L_0x005c:
            java.lang.String r2 = "intent"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x00e8
            r11.f = r6
            long r1 = android.os.SystemClock.elapsedRealtime()
            r11.g = r1
            r11.j = r9
            if (r4 != 0) goto L_0x0076
            java.lang.String r1 = "Could not get the paramMap in launchIntent()"
            r11.a(r1)
            goto L_0x003d
        L_0x0076:
            java.lang.String r1 = "u"
            java.lang.Object r1 = r4.get(r5)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 != 0) goto L_0x0086
            java.lang.String r1 = "Could not get the URL parameter in launchIntent()."
            r11.a(r1)
            goto L_0x003d
        L_0x0086:
            java.lang.String r2 = "i"
            java.lang.Object r2 = r4.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r3 = "m"
            java.lang.Object r3 = r4.get(r3)
            java.lang.String r3 = (java.lang.String) r3
            android.net.Uri r1 = android.net.Uri.parse(r1)
            if (r2 != 0) goto L_0x00ce
            android.content.Intent r2 = new android.content.Intent
            java.lang.String r3 = "android.intent.action.VIEW"
            r2.<init>(r3, r1)
            r1 = r2
        L_0x00a4:
            java.lang.Object r2 = com.google.ads.AdActivity.a
            monitor-enter(r2)
            com.google.ads.AdActivity r3 = com.google.ads.AdActivity.b     // Catch:{ all -> 0x00e5 }
            if (r3 != 0) goto L_0x00b6
            com.google.ads.AdActivity.b = r11     // Catch:{ all -> 0x00e5 }
            d r3 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x00e5 }
            if (r3 == 0) goto L_0x00df
            d r3 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x00e5 }
            r3.t()     // Catch:{ all -> 0x00e5 }
        L_0x00b6:
            monitor-exit(r2)     // Catch:{ all -> 0x00e5 }
            java.lang.String r2 = "Launching an intent from AdActivity."
            com.google.ads.util.a.a(r2)     // Catch:{ ActivityNotFoundException -> 0x00c1 }
            r11.startActivity(r1)     // Catch:{ ActivityNotFoundException -> 0x00c1 }
            goto L_0x003d
        L_0x00c1:
            r1 = move-exception
            java.lang.String r2 = r1.getMessage()
            com.google.ads.util.a.a(r2, r1)
            r11.finish()
            goto L_0x003d
        L_0x00ce:
            android.content.Intent r4 = new android.content.Intent
            r4.<init>(r2)
            if (r3 == 0) goto L_0x00da
            r4.setDataAndType(r1, r3)
            r1 = r4
            goto L_0x00a4
        L_0x00da:
            r4.setData(r1)
            r1 = r4
            goto L_0x00a4
        L_0x00df:
            java.lang.String r3 = "currentAdManager is null while trying to call onLeaveApplication()."
            com.google.ads.util.a.e(r3)     // Catch:{ all -> 0x00e5 }
            goto L_0x00b6
        L_0x00e5:
            r1 = move-exception
            monitor-exit(r2)
            throw r1
        L_0x00e8:
            android.widget.RelativeLayout r2 = new android.widget.RelativeLayout
            android.content.Context r3 = r11.getApplicationContext()
            r2.<init>(r3)
            r11.h = r2
            java.lang.String r2 = "webapp"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0170
            g r1 = new g
            android.content.Context r2 = r11.getApplicationContext()
            r1.<init>(r2, r6)
            r11.f = r1
            h r1 = new h
            java.util.Map<java.lang.String, i> r2 = defpackage.a.b
            r1.<init>(r8, r2, r9, r9)
            r1.b()
            g r2 = r11.f
            r2.setWebViewClient(r1)
            java.lang.String r1 = "u"
            java.lang.Object r1 = r4.get(r5)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r2 = "baseurl"
            java.lang.Object r2 = r4.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r3 = "html"
            java.lang.Object r3 = r4.get(r3)
            java.lang.String r3 = (java.lang.String) r3
            java.lang.String r5 = "o"
            java.lang.Object r4 = r4.get(r5)
            r0 = r4
            java.lang.String r0 = (java.lang.String) r0
            r7 = r0
            if (r1 == 0) goto L_0x014e
            g r2 = r11.f
            r2.loadUrl(r1)
        L_0x013e:
            java.lang.String r1 = "p"
            boolean r1 = r1.equals(r7)
            if (r1 == 0) goto L_0x0161
            r1 = r9
        L_0x0147:
            g r2 = r11.f
            r11.a(r2, r10, r1)
            goto L_0x003d
        L_0x014e:
            if (r3 == 0) goto L_0x015a
            g r1 = r11.f
            java.lang.String r4 = "text/html"
            java.lang.String r5 = "utf-8"
            r1.loadDataWithBaseURL(r2, r3, r4, r5, r6)
            goto L_0x013e
        L_0x015a:
            java.lang.String r1 = "Could not get the URL or HTML parameter to show a web app."
            r11.a(r1)
            goto L_0x003d
        L_0x0161:
            java.lang.String r1 = "l"
            boolean r1 = r1.equals(r7)
            if (r1 == 0) goto L_0x016b
            r1 = r10
            goto L_0x0147
        L_0x016b:
            int r1 = r8.m()
            goto L_0x0147
        L_0x0170:
            java.lang.String r2 = "interstitial"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0189
            g r1 = r8.i()
            r11.f = r1
            int r1 = r8.m()
            g r2 = r11.f
            r11.a(r2, r9, r1)
            goto L_0x003d
        L_0x0189:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Unknown AdOpener, <action: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = ">"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r11.a(r1)
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdActivity.onCreate(android.os.Bundle):void");
    }

    public void onDestroy() {
        if (this.h != null) {
            this.h.removeAllViews();
        }
        if (this.f != null) {
            a.b(this.f);
            this.f.a(null);
        }
        if (isFinishing()) {
            if (this.k != null) {
                this.k.stopPlayback();
                this.k = null;
            }
            synchronized (a) {
                if (!(c == null || this.f == null)) {
                    if (this.f == c.i()) {
                        c.a();
                    }
                    this.f.stopLoading();
                    this.f.destroy();
                }
                if (this == d) {
                    if (c != null) {
                        c.r();
                        c = null;
                    } else {
                        a.e("currentAdManager is null while trying to destroy AdActivity.");
                    }
                    d = null;
                }
            }
            if (this == b) {
                b = null;
            }
            e = this.i;
        }
        a.a("AdActivity is closing.");
        super.onDestroy();
    }

    public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
        a.e("Video threw error! <what:" + what + ", extra:" + extra + ">");
        finish();
        return true;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        a.d("Video is ready to play.");
        this.f.loadUrl("javascript:AFMA_ReceiveMessage('onVideoEvent', {'event': 'load'});");
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        if (this.j && hasFocus && SystemClock.elapsedRealtime() - this.g > 250) {
            a.d("Launcher AdActivity got focus and is closing.");
            finish();
        }
        super.onWindowFocusChanged(hasFocus);
    }

    public void showVideo(VideoView videoView) {
        this.k = videoView;
        if (this.f == null) {
            a("Couldn't get adWebView to show the video.");
            return;
        }
        this.f.setBackgroundColor(0);
        videoView.setOnCompletionListener(this);
        videoView.setOnPreparedListener(this);
        videoView.setOnErrorListener(this);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        LinearLayout linearLayout = new LinearLayout(getApplicationContext());
        linearLayout.setGravity(17);
        linearLayout.addView(videoView, layoutParams);
        this.h.addView(linearLayout, 0, layoutParams);
    }
}
