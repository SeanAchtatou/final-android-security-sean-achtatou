package oms.GameEngine;

import android.content.Context;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class DataAccess {
    public static DataInputStream inputStream = null;
    File hFile = null;
    public int inputFileLen = 0;
    public int outputFileLen = 0;
    public DataOutputStream outputStream = null;

    public boolean OpenInputFile(Context context, String file) {
        try {
            inputStream = new DataInputStream(new BufferedInputStream(context.openFileInput(file)));
            this.inputFileLen = inputStream.available();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e2) {
            e2.printStackTrace();
            return true;
        }
    }

    public void read(byte[] buffer) {
        try {
            inputStream.read(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void CloseInputFile() {
        this.inputFileLen = 0;
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean OpenOutFile(Context context, String file) {
        try {
            this.outputStream = new DataOutputStream(new BufferedOutputStream(context.openFileOutput(file, 1)));
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void write(byte[] buffer) {
        try {
            this.outputStream.write(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void CloseOutputFile() {
        this.outputFileLen = 0;
        try {
            this.outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
