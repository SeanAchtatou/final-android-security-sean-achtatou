package oms.GameEngine;

public class ACTStruct {
    public static final int CENTERX = 2;
    public static final int CENTERY = 2;
    public static final int FILEINDEXADDR = 2;
    public static final int FILENUM = 2;
    public static final int RESID = 4;
    public static final int SPRITEINDEXNUM = 4;
    public static final int SPRITENUM = 4;
    public static final int XHITL = 2;
    public static final int XHITR = 2;
    public static final int YHITD = 2;
    public static final int YHITU = 2;
    public static final int ZHITB = 2;
    public static final int ZHITF = 2;
    public int CenterX = 0;
    public int CenterY = 0;
    public int FileIndexAddr = 2;
    public int FileNum = 2;
    public int ResID = 0;
    public int SpriteIndexAddr = 0;
    public int SpriteNum = 0;
    public int XHitL = 0;
    public int XHitR = 0;
    public int YHitD = 0;
    public int YHitU = 0;
    public int ZHitB = 0;
    public int ZHitF = 0;
}
