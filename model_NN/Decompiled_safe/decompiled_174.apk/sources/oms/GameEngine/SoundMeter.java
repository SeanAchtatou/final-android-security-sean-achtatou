package oms.GameEngine;

import android.media.MediaRecorder;
import android.os.Environment;
import java.io.File;
import java.io.IOException;

public class SoundMeter {
    private static final double EMA_FILTER = 0.6d;
    private static final boolean FILE_W = true;
    private double mEMA = 0.0d;
    private MediaRecorder mRecorder = null;
    private String mStrFilePath = null;

    public void start() {
        if (this.mRecorder == null) {
            this.mRecorder = new MediaRecorder();
            this.mRecorder.setAudioSource(1);
            this.mRecorder.setOutputFormat(1);
            this.mRecorder.setAudioEncoder(1);
            try {
                this.mStrFilePath = File.createTempFile("temp", ".3gp", Environment.getExternalStorageDirectory()).getAbsolutePath();
                this.mRecorder.setOutputFile(this.mStrFilePath);
                try {
                    this.mRecorder.prepare();
                    this.mRecorder.start();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                this.mEMA = 0.0d;
            } catch (IOException e3) {
            }
        }
    }

    public void stop() {
        if (this.mRecorder != null) {
            this.mRecorder.stop();
            this.mRecorder.release();
            this.mRecorder = null;
            if (this.mStrFilePath != null) {
                new File(this.mStrFilePath).delete();
            }
            this.mStrFilePath = null;
        }
    }

    public void reset() {
        stop();
        start();
    }

    public double getAmplitude() {
        if (this.mRecorder != null) {
            return (double) this.mRecorder.getMaxAmplitude();
        }
        return 0.0d;
    }

    public double getAmplitudeEMA() {
        this.mEMA = (EMA_FILTER * getAmplitude()) + (0.4d * this.mEMA);
        return this.mEMA;
    }
}
