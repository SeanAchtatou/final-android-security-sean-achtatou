package oms.GameEngine;

public class SpriteDEF {
    public static final int SPDepth00 = 0;
    public static final int SPDepth01 = 1;
    public static final int SPDepth02 = 2;
    public static final int SPDepth03 = 3;
    public static final int SPDepth04 = 4;
    public static final int SPDepth05 = 5;
    public static final int SPDepth06 = 6;
    public static final int SPDepth07 = 7;
    public static final int SPDepth08 = 8;
    public static final int SPDepth09 = 9;
    public static final int SPDepth0A = 10;
    public static final int SPDepth0B = 11;
    public static final int SPDepth0C = 12;
    public static final int SPDepth0D = 13;
    public static final int SPDepth0E = 14;
    public static final int SPDepth0F = 15;
    public float Rotate = 0.0f;
    public short RotateX;
    public short RotateY;
    public float ScaleX = 1.0f;
    public float ScaleY = 1.0f;
    public int SpriteAttrib;
    public short SpriteCenterX = 0;
    public short SpriteCenterY = 0;
    public short SpriteResID;
    public int SpriteXVal;
    public int SpriteYVal;

    public SpriteDEF() {
        release();
    }

    public void release() {
        this.SpriteResID = -1;
        this.SpriteXVal = 0;
        this.SpriteYVal = 0;
        this.SpriteAttrib = 0;
        this.SpriteCenterX = 0;
        this.SpriteCenterY = 0;
        this.Rotate = 0.0f;
        this.ScaleX = 1.0f;
        this.ScaleY = 1.0f;
        this.RotateX = 0;
        this.RotateY = 0;
    }
}
