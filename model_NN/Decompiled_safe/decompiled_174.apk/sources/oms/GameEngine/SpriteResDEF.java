package oms.GameEngine;

import android.graphics.Bitmap;

public class SpriteResDEF {
    public Bitmap Sprite = null;
    public int SpriteCenterX = 0;
    public int SpriteCenterY = 0;
    public int SpriteResID = -1;
    public int SpriteXHitL = 0;
    public int SpriteXHitR = 0;
    public int SpriteYHitD = 0;
    public int SpriteYHitU = 0;
    public int SpriteZHitB = 0;
    public int SpriteZHitF = 0;

    public void release() {
        if (this.Sprite != null) {
            this.Sprite.recycle();
            this.Sprite = null;
        }
    }

    public Bitmap getSprite() {
        return this.Sprite;
    }

    public void setSprite(Bitmap sprite) {
        this.Sprite = sprite;
    }

    public int getSpriteResID() {
        return this.SpriteResID;
    }

    public void setSpriteResID(int spriteResID) {
        this.SpriteResID = spriteResID;
    }

    public int getSpriteCenterX() {
        return this.SpriteCenterX;
    }

    public void setSpriteCenterX(int spriteCenterX) {
        this.SpriteCenterX = spriteCenterX;
    }

    public int getSpriteCenterY() {
        return this.SpriteCenterY;
    }

    public void setSpriteCenterY(int spriteCenterY) {
        this.SpriteCenterY = spriteCenterY;
    }

    public int getSpriteXHitL() {
        return this.SpriteXHitL;
    }

    public void setSpriteXHitL(int spriteXHitL) {
        this.SpriteXHitL = spriteXHitL;
    }

    public int getSpriteXHitR() {
        return this.SpriteXHitR;
    }

    public void setSpriteXHitR(int spriteXHitR) {
        this.SpriteXHitR = spriteXHitR;
    }

    public int getSpriteYHitU() {
        return this.SpriteYHitU;
    }

    public void setSpriteYHitU(int spriteYHitU) {
        this.SpriteYHitU = spriteYHitU;
    }

    public int getSpriteYHitD() {
        return this.SpriteYHitD;
    }

    public void setSpriteYHitD(int spriteYHitD) {
        this.SpriteYHitD = spriteYHitD;
    }

    public int getSpriteZHitF() {
        return this.SpriteZHitF;
    }

    public void setSpriteZHitF(int spriteZHitF) {
        this.SpriteZHitF = spriteZHitF;
    }

    public int getSpriteZHitB() {
        return this.SpriteZHitB;
    }

    public void setSpriteZHitB(int spriteZHitB) {
        this.SpriteZHitB = spriteZHitB;
    }
}
