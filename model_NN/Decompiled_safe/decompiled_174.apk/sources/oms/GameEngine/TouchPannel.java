package oms.GameEngine;

public class TouchPannel {
    public static final int BUFFMAX = 20;
    public static final int MOVEBUFFMAX = 20;
    public boolean mTouch = false;
    public boolean mTouchDown = false;
    public int[] mTouchDownBuff = new int[1];
    public int mTouchDownReadCount = 0;
    public int mTouchDownWriteCount = 0;
    public boolean mTouchEn = false;
    public boolean mTouchMove = false;
    public int[] mTouchMoveBuff = new int[1];
    public int mTouchMoveReadCount = 0;
    public int mTouchMoveWriteCount = 0;
    public int mTouchMoveX = 0;
    public int[] mTouchMoveXBuff = new int[20];
    public int[] mTouchMoveXReadBuff = new int[20];
    public int mTouchMoveY = 0;
    public int[] mTouchMoveYBuff = new int[20];
    public int[] mTouchMoveYReadBuff = new int[20];
    public boolean mTouchUp = false;
    public int[] mTouchUpBuff = new int[1];
    public int mTouchUpReadCount = 0;
    public int mTouchUpWriteCount = 0;
    public int mTouchUpX = 0;
    public int[] mTouchUpXBuff = new int[20];
    public int[] mTouchUpXReadBuff = new int[20];
    public int mTouchUpY = 0;
    public int[] mTouchUpYBuff = new int[20];
    public int[] mTouchUpYReadBuff = new int[20];
    public int mTouchX = 0;
    public int[] mTouchXBuff = new int[20];
    public int[] mTouchXReadBuff = new int[20];
    public int mTouchY = 0;
    public int[] mTouchYBuff = new int[20];
    public int[] mTouchYReadBuff = new int[20];

    public TouchPannel() {
        this.mTouchDownBuff[0] = 0;
        this.mTouchUpBuff[0] = 0;
        for (int i = 0; i < 20; i++) {
            this.mTouchXBuff[i] = 0;
            this.mTouchYBuff[i] = 0;
            this.mTouchXReadBuff[i] = 0;
            this.mTouchYReadBuff[i] = 0;
            this.mTouchUpXBuff[i] = 0;
            this.mTouchUpYBuff[i] = 0;
            this.mTouchUpXReadBuff[i] = 0;
            this.mTouchUpYReadBuff[i] = 0;
        }
        this.mTouchMoveBuff[0] = 0;
        for (int i2 = 0; i2 < 20; i2++) {
            this.mTouchMoveXBuff[i2] = 0;
            this.mTouchMoveYBuff[i2] = 0;
            this.mTouchMoveXReadBuff[i2] = 0;
            this.mTouchMoveYReadBuff[i2] = 0;
        }
    }

    public void SetTouchEn(boolean touch) {
        this.mTouchEn = touch;
    }

    public boolean GetTouchEn() {
        return this.mTouchEn;
    }

    public void SetTouchDown(int X, int Y) {
        this.mTouchDownBuff[0] = 1;
        this.mTouch = true;
        if (this.mTouchDownWriteCount < 20) {
            this.mTouchXBuff[this.mTouchDownWriteCount] = X;
            int[] iArr = this.mTouchYBuff;
            int i = this.mTouchDownWriteCount;
            this.mTouchDownWriteCount = i + 1;
            iArr[i] = Y;
        }
    }

    public void SetTouchMove(int X, int Y) {
        this.mTouchMoveBuff[0] = 1;
        this.mTouch = true;
        if (this.mTouchMoveWriteCount < 20) {
            this.mTouchMoveXBuff[this.mTouchMoveWriteCount] = X;
            int[] iArr = this.mTouchMoveYBuff;
            int i = this.mTouchMoveWriteCount;
            this.mTouchMoveWriteCount = i + 1;
            iArr[i] = Y;
        }
    }

    public void SetTouchUp(int X, int Y) {
        this.mTouchUpBuff[0] = 1;
        this.mTouch = false;
        if (this.mTouchUpWriteCount < 20) {
            this.mTouchUpXBuff[this.mTouchUpWriteCount] = X;
            int[] iArr = this.mTouchUpYBuff;
            int i = this.mTouchUpWriteCount;
            this.mTouchUpWriteCount = i + 1;
            iArr[i] = Y;
        }
    }

    public void ReadTouch() {
        if (this.mTouchDownBuff[0] == 1) {
            this.mTouchDown = true;
        } else {
            this.mTouchDown = false;
        }
        this.mTouchX = this.mTouchXBuff[0];
        this.mTouchY = this.mTouchYBuff[0];
        this.mTouchDownBuff[0] = 0;
        for (int i = 0; i < this.mTouchDownWriteCount; i++) {
            this.mTouchXReadBuff[i] = this.mTouchXBuff[i];
            this.mTouchYReadBuff[i] = this.mTouchYBuff[i];
        }
        this.mTouchDownReadCount = this.mTouchDownWriteCount;
        this.mTouchDownWriteCount = 0;
        if (this.mTouchMoveBuff[0] == 1) {
            this.mTouchMove = true;
        } else {
            this.mTouchMove = false;
        }
        this.mTouchMoveBuff[0] = 0;
        if (this.mTouchMoveWriteCount > 0) {
            this.mTouchMoveX = this.mTouchMoveXBuff[this.mTouchMoveWriteCount - 1];
            this.mTouchMoveY = this.mTouchMoveYBuff[this.mTouchMoveWriteCount - 1];
        } else {
            this.mTouchMoveX = this.mTouchMoveXBuff[0];
            this.mTouchMoveY = this.mTouchMoveYBuff[0];
        }
        for (int i2 = 0; i2 < this.mTouchMoveWriteCount; i2++) {
            this.mTouchMoveXReadBuff[i2] = this.mTouchMoveXBuff[i2];
            this.mTouchMoveYReadBuff[i2] = this.mTouchMoveYBuff[i2];
            this.mTouchMoveXBuff[i2] = 0;
            this.mTouchMoveYBuff[i2] = 0;
        }
        this.mTouchMoveReadCount = this.mTouchMoveWriteCount;
        this.mTouchMoveWriteCount = 0;
        if (this.mTouchUpBuff[0] == 1) {
            this.mTouchUp = true;
        } else {
            this.mTouchUp = false;
        }
        this.mTouchUpX = this.mTouchUpXBuff[0];
        this.mTouchUpY = this.mTouchUpYBuff[0];
        this.mTouchUpBuff[0] = 0;
        for (int i3 = 0; i3 < this.mTouchUpWriteCount; i3++) {
            this.mTouchUpXReadBuff[i3] = this.mTouchUpXBuff[i3];
            this.mTouchUpYReadBuff[i3] = this.mTouchUpYBuff[i3];
            this.mTouchUpXBuff[i3] = 0;
            this.mTouchUpYBuff[i3] = 0;
        }
        this.mTouchUpReadCount = this.mTouchUpWriteCount;
        this.mTouchUpWriteCount = 0;
    }

    public boolean CHKIsTouch() {
        return this.mTouch;
    }

    public boolean CHKTouchDown() {
        return this.mTouchDown;
    }

    public int GetTouchDownX() {
        return this.mTouchX;
    }

    public int GetTouchDownY() {
        return this.mTouchY;
    }

    public boolean CHKTouchMove() {
        return this.mTouchMove;
    }

    public int GetTouchDownCount() {
        return this.mTouchDownReadCount;
    }

    public int GetTouchDownX(int Count) {
        if (Count >= this.mTouchDownReadCount || Count < 0) {
            return 0;
        }
        return this.mTouchXReadBuff[Count];
    }

    public int GetTouchDownY(int Count) {
        if (Count >= this.mTouchDownReadCount || Count < 0) {
            return 0;
        }
        return this.mTouchYReadBuff[Count];
    }

    public int GetTouchMoveX() {
        return this.mTouchMoveX;
    }

    public int GetTouchMoveY() {
        return this.mTouchMoveY;
    }

    public int GetTouchMoveCount() {
        return this.mTouchMoveReadCount;
    }

    public int GetTouchMoveX(int Count) {
        if (Count < 0 || Count >= this.mTouchMoveReadCount) {
            return 0;
        }
        return this.mTouchMoveXReadBuff[Count];
    }

    public int GetTouchMoveY(int Count) {
        if (Count < 0 || Count >= this.mTouchMoveReadCount) {
            return 0;
        }
        return this.mTouchMoveYReadBuff[Count];
    }

    public boolean CHKTouchUp() {
        return this.mTouchUp;
    }

    public int GetTouchUpX() {
        return this.mTouchUpX;
    }

    public int GetTouchUpY() {
        return this.mTouchUpY;
    }

    public int GetTouchUpCount() {
        return this.mTouchUpReadCount;
    }

    public int GetTouchUpX(int Count) {
        if (Count >= this.mTouchUpReadCount || Count < 0) {
            return 0;
        }
        return this.mTouchUpXReadBuff[Count];
    }

    public int GetTouchUpY(int Count) {
        if (Count >= this.mTouchUpReadCount || Count < 0) {
            return 0;
        }
        return this.mTouchUpYReadBuff[Count];
    }

    public void ClearTouchDown() {
        this.mTouchDownBuff[0] = 0;
        for (int i = 0; i < 20; i++) {
            this.mTouchXBuff[i] = 0;
            this.mTouchYBuff[i] = 0;
        }
    }

    public void ClearTouchMove() {
        this.mTouchMoveBuff[0] = 0;
        for (int i = 0; i < 20; i++) {
            this.mTouchMoveXBuff[i] = 0;
            this.mTouchMoveYBuff[i] = 0;
        }
        this.mTouchMoveReadCount = 0;
        this.mTouchMoveWriteCount = 0;
    }

    public void ClearTouchUp() {
        this.mTouchUpBuff[0] = 0;
        for (int i = 0; i < 20; i++) {
            this.mTouchUpXBuff[i] = 0;
            this.mTouchUpYBuff[i] = 0;
        }
    }
}
