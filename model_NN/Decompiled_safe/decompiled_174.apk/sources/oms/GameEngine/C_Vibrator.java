package oms.GameEngine;

import android.app.Activity;
import android.os.Vibrator;

public class C_Vibrator {
    private Vibrator cVibrator;

    public void InitVibrator(Activity activity) {
        this.cVibrator = (Vibrator) activity.getSystemService("vibrator");
    }

    public void release() {
        if (this.cVibrator != null) {
            Vibrate(0);
            this.cVibrator = null;
        }
    }

    public void Vibrate(long milliseconds) {
        if (this.cVibrator == null) {
            this.cVibrator.vibrate(milliseconds);
        }
    }

    public void Vibrate(long[] pattern) {
        if (this.cVibrator == null) {
            this.cVibrator.vibrate(pattern, 1);
        }
    }

    public void Vibrate(long[] pattern, int loops) {
        if (this.cVibrator == null) {
            this.cVibrator.vibrate(pattern, loops);
        }
    }
}
