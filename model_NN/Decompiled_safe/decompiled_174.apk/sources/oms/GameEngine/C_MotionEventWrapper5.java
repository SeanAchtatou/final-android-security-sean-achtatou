package oms.GameEngine;

import android.view.MotionEvent;

public class C_MotionEventWrapper5 {
    public static final int ACTION_POINTER_1_DOWN = 5;
    public static final int ACTION_POINTER_1_UP = 6;
    public static final int ACTION_POINTER_2_DOWN = 261;
    public static final int ACTION_POINTER_2_UP = 262;
    public static final int ACTION_POINTER_3_DOWN = 517;
    public static final int ACTION_POINTER_3_UP = 518;

    public static int getPointerCount(MotionEvent motionEvent) {
        return motionEvent.getPointerCount();
    }

    public static int getPointerId(MotionEvent motionEvent, int index) {
        return motionEvent.getPointerId(index);
    }

    public static float getX(MotionEvent motionEvent, int index) {
        return motionEvent.getX(index);
    }

    public static float getY(MotionEvent motionEvent, int index) {
        return motionEvent.getY(index);
    }
}
