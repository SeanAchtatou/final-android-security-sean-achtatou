package oms.GameEngine;

import android.app.Activity;

public class VibrateManager {
    private C_Vibrator cVibrator = new C_Vibrator();

    public void OpenVibrate(Activity activity) {
        this.cVibrator.InitVibrator(activity);
    }

    public void Vibrate(long milliseconds) {
        this.cVibrator.Vibrate(milliseconds);
    }

    public void Vibrate(long[] pattern) {
        this.cVibrator.Vibrate(pattern);
    }

    public void Vibrate(long[] pattern, int loops) {
        this.cVibrator.Vibrate(pattern, loops);
    }
}
