package oms.GameEngine;

import android.content.Context;
import android.media.SoundPool;
import android.util.Log;

public class GameSound {
    public static final int PLAY_LOOP = 1;
    public static final int PLAY_ONCE = 0;
    public boolean SoundStopEn = true;
    public CSoundPlay[] cSoundPlay;
    public int mChannelMax;
    public Context mContext;
    public int mCurLoadSoundNum;
    public int[] mPlayChannel;
    public SoundPool soundPool;
    public float volume = 1.0f;

    private class CSoundPlay {
        public int nLoadedID;
        public boolean nLoop;
        public long nPlayStart;
        public int nPlayedTime;
        public boolean nPlaying;
        public int nResID;
        public int nSteamID;

        public CSoundPlay() {
            Reset();
        }

        public void Reset() {
            this.nResID = 1;
            this.nSteamID = -1;
            this.nLoadedID = -1;
            this.nLoop = false;
            this.nPlaying = false;
            this.nPlayedTime = 0;
            this.nPlayStart = 0;
        }

        public void Start() {
            this.nPlayStart = System.currentTimeMillis();
            this.nPlayedTime = 0;
        }

        public void Stop() {
            this.nPlayStart = 0;
            this.nPlayedTime = 0;
        }

        public void Pause() {
            long curTime = System.currentTimeMillis();
            if (curTime < this.nPlayStart) {
                curTime += 86400000;
            }
            this.nPlayedTime = (int) (curTime - this.nPlayStart);
        }

        public void Resume() {
            long curTime = System.currentTimeMillis();
            if (curTime < ((long) this.nPlayedTime)) {
                curTime += 86400000;
            }
            this.nPlayStart = curTime - ((long) this.nPlayedTime);
            this.nPlayedTime = 0;
        }

        public int GetPlayedTime() {
            if (this.nPlayedTime != 0) {
                return this.nPlayedTime;
            }
            long curTime = System.currentTimeMillis();
            if (curTime < this.nPlayStart) {
                curTime += 86400000;
            }
            return (int) (curTime - this.nPlayStart);
        }
    }

    public GameSound(Context context, int size) {
        this.mContext = context;
        this.soundPool = new SoundPool(size + 1, 3, 0);
        this.mChannelMax = size;
        this.mPlayChannel = new int[size];
        this.cSoundPlay = new CSoundPlay[size];
        for (int i = 0; i < size; i++) {
            this.mPlayChannel[i] = -1;
            this.cSoundPlay[i] = new CSoundPlay();
        }
        this.mCurLoadSoundNum = 0;
    }

    public void release() {
        for (int i = 0; i < this.mChannelMax; i++) {
            if (this.cSoundPlay[i].nSteamID != -1 && this.cSoundPlay[i].nLoop) {
                this.soundPool.stop(this.cSoundPlay[i].nSteamID);
                this.soundPool.setLoop(this.cSoundPlay[i].nSteamID, 0);
                this.soundPool.unload(this.cSoundPlay[i].nSteamID);
                this.cSoundPlay[i].nLoop = false;
            }
            this.cSoundPlay[i].nSteamID = -1;
            this.cSoundPlay[i].nResID = -1;
            this.mPlayChannel[i] = -1;
            this.cSoundPlay[i].nPlaying = false;
        }
        this.soundPool.release();
        this.soundPool = null;
        this.mCurLoadSoundNum = 0;
    }

    public void SetSoundStopEn(boolean status) {
        this.SoundStopEn = status;
    }

    private int GetMapIdx(int resID) {
        for (int i = 0; i < this.mChannelMax; i++) {
            if (this.cSoundPlay[i].nResID == resID) {
                return i;
            }
        }
        return -1;
    }

    public void addSound(int resid) {
        if (this.mCurLoadSoundNum < this.mChannelMax && GetMapIdx(resid) == -1) {
            int soundId = this.soundPool.load(this.mContext, resid, 1);
            this.cSoundPlay[this.mCurLoadSoundNum].nResID = resid;
            this.cSoundPlay[this.mCurLoadSoundNum].nLoadedID = soundId;
            CSoundPlay[] cSoundPlayArr = this.cSoundPlay;
            int i = this.mCurLoadSoundNum;
            this.mCurLoadSoundNum = i + 1;
            cSoundPlayArr[i].nLoop = false;
        }
    }

    public void addLoopSound(int resid) {
        if (this.mCurLoadSoundNum < this.mChannelMax && GetMapIdx(resid) == -1) {
            int soundId = this.soundPool.load(this.mContext, resid, 1);
            this.cSoundPlay[this.mCurLoadSoundNum].nResID = resid;
            this.cSoundPlay[this.mCurLoadSoundNum].nLoadedID = soundId;
            CSoundPlay[] cSoundPlayArr = this.cSoundPlay;
            int i = this.mCurLoadSoundNum;
            this.mCurLoadSoundNum = i + 1;
            cSoundPlayArr[i].nLoop = true;
        }
    }

    public void removeSound(int resid) {
        int loadId;
        int MapIdx = GetMapIdx(resid);
        if (MapIdx != -1 && (loadId = this.cSoundPlay[MapIdx].nLoadedID) != -1) {
            if (this.cSoundPlay[MapIdx].nLoop) {
                this.soundPool.stop(this.cSoundPlay[MapIdx].nSteamID);
            }
            this.soundPool.unload(loadId);
            this.cSoundPlay[MapIdx].nLoadedID = -1;
            this.cSoundPlay[MapIdx].nResID = -1;
            this.cSoundPlay[MapIdx].nLoop = false;
            this.cSoundPlay[MapIdx].nSteamID = -1;
            if (this.mCurLoadSoundNum > 0) {
                this.mCurLoadSoundNum--;
            }
        }
    }

    public void removeAll() {
        for (int i = 0; i < this.mChannelMax; i++) {
            if (this.cSoundPlay[i].nSteamID != -1 && this.cSoundPlay[i].nLoop) {
                this.soundPool.stop(this.cSoundPlay[i].nSteamID);
                this.soundPool.setLoop(this.cSoundPlay[i].nSteamID, 0);
                this.soundPool.unload(this.cSoundPlay[i].nSteamID);
                this.cSoundPlay[i].nLoop = false;
            }
            this.cSoundPlay[i].nSteamID = -1;
            this.cSoundPlay[i].nResID = -1;
            this.mPlayChannel[i] = -1;
            this.cSoundPlay[i].nPlaying = false;
        }
        this.mCurLoadSoundNum = 0;
    }

    public void play(int resid) {
        int MapIdx = GetMapIdx(resid);
        if (MapIdx != -1) {
            if (this.SoundStopEn && this.cSoundPlay[MapIdx].nSteamID != -1) {
                this.soundPool.stop(this.cSoundPlay[MapIdx].nSteamID);
                this.soundPool.setVolume(this.cSoundPlay[MapIdx].nSteamID, 0.0f, 0.0f);
                this.soundPool.setLoop(this.cSoundPlay[MapIdx].nSteamID, -1);
                this.cSoundPlay[MapIdx].nPlaying = false;
            }
            int streamID = this.soundPool.play(this.cSoundPlay[MapIdx].nLoadedID, this.volume, this.volume, 0, 0, 1.0f);
            this.cSoundPlay[MapIdx].nSteamID = streamID;
            this.cSoundPlay[MapIdx].nLoop = false;
            this.cSoundPlay[MapIdx].nPlaying = true;
            this.cSoundPlay[MapIdx].Start();
            this.soundPool.setLoop(streamID, 0);
            Log.v("SoundPool", "Cur Stream ID=" + streamID);
        }
    }

    public void playLoop(int resid) {
        int MapIdx = GetMapIdx(resid);
        if (MapIdx != -1) {
            if (this.SoundStopEn && this.cSoundPlay[MapIdx].nSteamID != -1) {
                this.soundPool.stop(this.cSoundPlay[MapIdx].nSteamID);
                this.soundPool.setVolume(this.cSoundPlay[MapIdx].nSteamID, 0.0f, 0.0f);
                this.soundPool.setLoop(this.cSoundPlay[MapIdx].nSteamID, -1);
                this.cSoundPlay[MapIdx].nPlaying = false;
            }
            int streamID = this.soundPool.play(this.cSoundPlay[MapIdx].nLoadedID, this.volume, this.volume, 0, -1, 1.0f);
            this.cSoundPlay[MapIdx].nSteamID = streamID;
            this.cSoundPlay[MapIdx].nLoop = true;
            this.cSoundPlay[MapIdx].nPlaying = true;
            this.cSoundPlay[MapIdx].Start();
            this.soundPool.setLoop(streamID, -1);
        }
    }

    public void stop(int resid) {
        int MapIdx;
        if (this.SoundStopEn && (MapIdx = GetMapIdx(resid)) != -1) {
            this.soundPool.setLoop(this.cSoundPlay[MapIdx].nSteamID, 0);
            this.soundPool.stop(this.cSoundPlay[MapIdx].nSteamID);
            this.cSoundPlay[MapIdx].nSteamID = -1;
            this.cSoundPlay[MapIdx].nLoop = false;
            this.cSoundPlay[MapIdx].nPlaying = false;
            this.cSoundPlay[MapIdx].Stop();
        }
    }

    public void pause() {
        for (int i = 0; i < this.mCurLoadSoundNum; i++) {
            if (this.cSoundPlay[i].nSteamID != -1) {
                this.soundPool.setLoop(this.cSoundPlay[i].nSteamID, 0);
                this.cSoundPlay[i].nPlaying = false;
                this.cSoundPlay[i].Pause();
                for (int j = 0; j < 50; j++) {
                    this.soundPool.pause(this.cSoundPlay[i].nSteamID);
                }
            }
        }
    }

    public void resume() {
        for (int i = 0; i < this.mCurLoadSoundNum; i++) {
            if (this.cSoundPlay[i].nLoop) {
                this.soundPool.setLoop(this.cSoundPlay[i].nSteamID, -1);
                this.soundPool.resume(this.cSoundPlay[i].nSteamID);
                this.cSoundPlay[i].nPlaying = true;
                this.cSoundPlay[i].Resume();
            } else if (this.cSoundPlay[i].GetPlayedTime() < 1000 && this.cSoundPlay[i].nSteamID != -1) {
                this.soundPool.resume(this.cSoundPlay[i].nSteamID);
                this.cSoundPlay[i].Resume();
                this.cSoundPlay[i].nPlaying = true;
            }
        }
    }

    public void pause(int resid) {
        int MapIdx = GetMapIdx(resid);
        if (MapIdx != -1 && this.cSoundPlay[MapIdx].nPlaying) {
            this.soundPool.setLoop(this.cSoundPlay[MapIdx].nSteamID, 0);
            this.cSoundPlay[MapIdx].nPlaying = false;
            this.soundPool.pause(this.cSoundPlay[MapIdx].nSteamID);
            this.cSoundPlay[MapIdx].Pause();
        }
    }

    public void resume(int resid) {
        int MapIdx = GetMapIdx(resid);
        if (MapIdx != -1 && this.cSoundPlay[MapIdx].nSteamID != -1 && !this.cSoundPlay[MapIdx].nPlaying) {
            if (this.cSoundPlay[MapIdx].nLoop) {
                this.soundPool.setLoop(this.cSoundPlay[MapIdx].nSteamID, -1);
            }
            this.soundPool.resume(this.cSoundPlay[MapIdx].nSteamID);
            this.cSoundPlay[MapIdx].Resume();
            this.cSoundPlay[MapIdx].nPlaying = true;
        }
    }

    public void setRate(int streamID, float rate) {
        int MapIdx = GetMapIdx(streamID);
        if (MapIdx != -1) {
            if (((double) rate) < 0.5d) {
                rate = 0.5f;
            } else if (((double) rate) > 2.0d) {
                rate = 2.0f;
            }
            this.soundPool.setRate(this.cSoundPlay[MapIdx].nSteamID, rate);
        }
    }

    public void setSoundVolume(int resid, float Volume) {
        this.volume = Volume;
    }

    public void setAllSoundVolume(float Volume) {
        this.volume = Volume;
    }

    public float getVolume() {
        return this.volume;
    }

    public void CH_Play(int Channel, int Resid) {
        int MapIdx;
        if (Channel <= this.mChannelMax && (MapIdx = GetMapIdx(Resid)) != -1) {
            if (this.mPlayChannel[Channel] != -1) {
                int LastMapIdx = GetMapIdx(this.mPlayChannel[Channel]);
                if (this.SoundStopEn && this.cSoundPlay[LastMapIdx].nSteamID != -1) {
                    this.soundPool.stop(this.cSoundPlay[LastMapIdx].nSteamID);
                    this.soundPool.setVolume(this.cSoundPlay[LastMapIdx].nSteamID, 0.0f, 0.0f);
                    this.soundPool.setLoop(this.cSoundPlay[LastMapIdx].nSteamID, -1);
                    this.cSoundPlay[LastMapIdx].nPlaying = false;
                }
            }
            int streamID = this.soundPool.play(this.cSoundPlay[MapIdx].nLoadedID, this.volume, this.volume, 0, 0, 1.0f);
            this.cSoundPlay[MapIdx].nSteamID = streamID;
            this.cSoundPlay[MapIdx].nLoop = false;
            this.cSoundPlay[MapIdx].nPlaying = true;
            this.cSoundPlay[MapIdx].Start();
            this.soundPool.setLoop(streamID, 0);
            this.mPlayChannel[Channel] = Resid;
        }
    }

    public void CH_PlayLoop(int Channel, int Resid) {
        int MapIdx;
        if (Channel <= this.mChannelMax && (MapIdx = GetMapIdx(Resid)) != -1) {
            if (this.mPlayChannel[Channel] != -1) {
                int LastMapIdx = GetMapIdx(this.mPlayChannel[Channel]);
                if (this.SoundStopEn && this.cSoundPlay[LastMapIdx].nSteamID != -1) {
                    this.soundPool.stop(this.cSoundPlay[LastMapIdx].nSteamID);
                    this.soundPool.setVolume(this.cSoundPlay[LastMapIdx].nSteamID, 0.0f, 0.0f);
                    this.soundPool.setLoop(this.cSoundPlay[LastMapIdx].nSteamID, -1);
                    this.cSoundPlay[LastMapIdx].nPlaying = false;
                }
            }
            int streamID = this.soundPool.play(this.cSoundPlay[MapIdx].nLoadedID, this.volume, this.volume, 0, -1, 1.0f);
            this.cSoundPlay[MapIdx].nSteamID = streamID;
            this.cSoundPlay[MapIdx].nLoop = true;
            this.cSoundPlay[MapIdx].nPlaying = true;
            this.cSoundPlay[MapIdx].Start();
            this.soundPool.setLoop(streamID, -1);
            this.mPlayChannel[Channel] = Resid;
        }
    }

    public void CH_Stop(int Channel) {
        if (Channel <= this.mChannelMax && this.mPlayChannel[Channel] != -1) {
            stop(this.mPlayChannel[Channel]);
            this.mPlayChannel[Channel] = -1;
        }
    }

    public void CH_Pause(int Channel) {
        if (Channel <= this.mChannelMax && this.mPlayChannel[Channel] != -1) {
            pause(this.mPlayChannel[Channel]);
        }
    }

    public void CH_Resume(int Channel) {
        if (Channel <= this.mChannelMax && this.mPlayChannel[Channel] != -1) {
            resume(this.mPlayChannel[Channel]);
        }
    }

    public void CH_SetRate(int Channel, float Rate) {
        if (Channel <= this.mChannelMax && this.mPlayChannel[Channel] != -1) {
            setRate(this.mPlayChannel[Channel], Rate);
        }
    }

    public int CH_GetPlayedTime(int Channel) {
        int MapIdx = GetMapIdx(this.mPlayChannel[Channel]);
        if (MapIdx == -1) {
            return 0;
        }
        return this.cSoundPlay[MapIdx].GetPlayedTime();
    }

    public int GetPlayedTime(int resID) {
        int MapIdx = GetMapIdx(resID);
        if (MapIdx == -1) {
            return 0;
        }
        return this.cSoundPlay[MapIdx].GetPlayedTime();
    }
}
