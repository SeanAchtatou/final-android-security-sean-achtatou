package oms.GameEngine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.game.DodgeBall_AdSence.C_MainMenu;

public class GameCanvas {
    private static SpriteManager SpriteMgr;
    private static TextManager TextMgr;
    private static float fRotate = 0.0f;
    private static int nBackXOff;
    private static int nBackYOff;
    private static int nMaxLogicLayer = 8;
    private static int nScreenHeight;
    private static int nScreenWidth;
    public static int nScreenXOff;
    public static int nScreenYOff;
    private static int nSpriteXOff;
    private static int nSpriteYOff;
    private static Rect nViewRc;
    private static Paint paint = new Paint();
    public boolean bUpdate = false;
    private Context mContext;
    public int nScreenAlpha = 0;

    public GameCanvas(Context context, int TextLayer, int SpriteResNum, int SpriteNum) {
        this.mContext = context;
        nViewRc = null;
        SpriteMgr = null;
        TextMgr = null;
        nBackXOff = 0;
        nBackYOff = 0;
        nSpriteXOff = 0;
        nSpriteYOff = 0;
        nScreenXOff = 0;
        nScreenYOff = 0;
        SetScreenSize(320, 480);
        SetViewRect(-120, -90, C_MainMenu.MAKE_OTHER_END, 90);
        SpriteMgr = new SpriteManager(this.mContext, SpriteResNum, SpriteNum);
        TextMgr = new TextManager(this.mContext, TextLayer);
    }

    public void release() {
        CloseAllText();
        FreeAllACT();
    }

    public void SetLogOut(boolean sprite, boolean text) {
        SpriteMgr.SetBMPSizeOut(sprite);
        TextMgr.SetBMPSizeOut(text);
    }

    public void SetMaxLogicLayer(int Layer) {
        nMaxLogicLayer = Layer;
    }

    public void SetScreenSize(int Width, int Height) {
        nScreenWidth = Width;
        nScreenHeight = Height;
    }

    public static int GetScreenWidth() {
        return nScreenWidth;
    }

    public static int GetScreenHeight() {
        return nScreenHeight;
    }

    public void SetScreenOffset(int XOff, int YOff) {
        nScreenXOff = XOff;
        nScreenYOff = YOff;
    }

    public void SetBackgroundDrawOffset(int XOff, int YOff) {
        nBackXOff = XOff;
        nBackYOff = YOff;
    }

    public void SetSpriteDrawOffset(int XOff, int YOff) {
        nSpriteXOff = XOff;
        nSpriteYOff = YOff;
    }

    public void SetViewRect(int BegX, int BegY, int EndX, int EndY) {
        if (nViewRc == null) {
            nViewRc = new Rect();
        }
        nViewRc.left = BegX;
        nViewRc.top = BegY;
        nViewRc.right = nScreenWidth + EndX;
        nViewRc.bottom = nScreenHeight + EndY;
    }

    public static Rect GetViewRect() {
        return nViewRc;
    }

    public boolean isReflash() {
        return this.bUpdate;
    }

    public static void Rotate(float angle) {
        fRotate = angle;
    }

    public static float getRotate() {
        return fRotate;
    }

    public void onDraw(Canvas canvas) {
        paint.setColor(-16777216);
        boolean showSprite = true;
        int CurDepth = 0;
        for (int i = 0; i < nMaxLogicLayer; i++) {
            Canvas canvas2 = canvas;
            TextMgr.OnDraw(canvas2, CurDepth, nScreenXOff + nBackXOff, nScreenYOff + nBackYOff, paint);
            if (showSprite) {
                if (SpriteMgr.OnDraw(canvas, CurDepth, nScreenXOff + nSpriteXOff, nScreenYOff + nSpriteYOff, paint)) {
                    showSprite = false;
                }
            }
            CurDepth++;
        }
        if (this.nScreenAlpha < 255) {
            canvas.drawARGB(C_MotionEventWrapper8.ACTION_MASK - this.nScreenAlpha, 0, 0, 0);
        }
        this.bUpdate = false;
    }

    public void onDraw(C_Lib cLib, Canvas canvas, int picHeight) {
        paint.setColor(-16777216);
        boolean showSprite = true;
        int CurDepth = 0;
        if (!cLib.mTop && cLib.mBackground != null) {
            canvas.drawBitmap(cLib.mBackground, (float) nScreenXOff, (float) (picHeight - cLib.mBackground.getHeight()), paint);
            canvas.drawBitmap(cLib.mBackground, (float) nScreenXOff, (float) (picHeight + 480), paint);
        }
        for (int i = 0; i < nMaxLogicLayer; i++) {
            Canvas canvas2 = canvas;
            TextMgr.OnDraw(canvas2, CurDepth, nScreenXOff + nBackXOff, nScreenYOff + nBackYOff, paint);
            if (showSprite) {
                if (SpriteMgr.OnDraw(canvas, CurDepth, nScreenXOff + nSpriteXOff, nScreenYOff + nSpriteYOff, paint)) {
                    showSprite = false;
                }
            }
            CurDepth++;
        }
        if (cLib.mTop && cLib.mBackground != null) {
            canvas.drawBitmap(cLib.mBackground, (float) nScreenXOff, (float) (picHeight - cLib.mBackground.getHeight()), paint);
            canvas.drawBitmap(cLib.mBackground, (float) nScreenXOff, (float) (picHeight + 480), paint);
        }
        if (this.nScreenAlpha < 255) {
            canvas.drawARGB(C_MotionEventWrapper8.ACTION_MASK - this.nScreenAlpha, 0, 0, 0);
        }
        this.bUpdate = false;
    }

    public void flush() {
        this.bUpdate = true;
    }

    public boolean getUpdata() {
        return this.bUpdate;
    }

    public void SetACTLibBeg(int beg) {
        SpriteMgr.SetACTLibBeg(beg);
    }

    public void LoadText(int ResTextID, int TextLayer, int TextDepth) {
        TextMgr.LoadText(ResTextID, TextLayer, TextDepth);
    }

    public void LoadText(int Width, int Height, int TextLayer, int TextDepth) {
        TextMgr.LoadText(Width, Height, TextLayer, TextDepth);
    }

    public void LoadPicture(int ResTextID, int TextLayer, int TextDepth) {
        TextMgr.LoadPicture(ResTextID, TextLayer, TextDepth);
    }

    public boolean getTextPixels(int TextLayer, int[] pixels) {
        return TextMgr.getTextPixels(TextLayer, pixels);
    }

    public void setTextPixels(int TextLayer, int[] pixels) {
        TextMgr.setTextPixels(TextLayer, pixels);
    }

    public void setTextPixels(int TextLayer, int[] pixels, int offset, int x, int y, int width, int height) {
        TextMgr.setTextPixels(TextLayer, pixels, offset, x, y, width, height);
    }

    public void setTextPixels(int TextLayer, int[] pixels, int offset, int stride, int x, int y, int width, int height) {
        TextMgr.setTextPixels(TextLayer, pixels, offset, stride, x, y, width, height);
    }

    public void CloseText(int TextLayer) {
        TextMgr.CloseText(TextLayer);
    }

    public void CloseAllText() {
        TextMgr.CloseAllText();
    }

    public void setScreenAlpha(int Alpha) {
        this.nScreenAlpha = Alpha;
    }

    public boolean ViewOpen(int alphaSpeed) {
        this.nScreenAlpha += alphaSpeed;
        if (this.nScreenAlpha <= 255) {
            return false;
        }
        this.nScreenAlpha = C_MotionEventWrapper8.ACTION_MASK;
        return true;
    }

    public boolean ViewDark(int alphaSpeed) {
        this.nScreenAlpha -= alphaSpeed;
        if (this.nScreenAlpha > 0) {
            return false;
        }
        this.nScreenAlpha = 0;
        CloseAllText();
        ClearACT();
        return true;
    }

    public void SetTextInc(int TextLayer, int XInc, int YInc) {
        TextMgr.SetTextInc(TextLayer, XInc, YInc);
    }

    public int GetTextWidth(int TextLayer) {
        return TextMgr.GetTextWidth(TextLayer);
    }

    public int GetTextHeight(int TextLayer) {
        return TextMgr.GetTextHeight(TextLayer);
    }

    public int GetTextXVal(int TextLayer) {
        return TextMgr.GetTextXVal(TextLayer);
    }

    public int GetTextYVal(int TextLayer) {
        return TextMgr.GetTextYVal(TextLayer);
    }

    public void SetTextXVal(int TextLayer, int X) {
        TextMgr.SetTextXVal(TextLayer, X);
    }

    public void SetTextYVal(int TextLayer, int Y) {
        TextMgr.SetTextYVal(TextLayer, Y);
    }

    public void SetTextScale(int TextLayer, float Scale) {
        TextMgr.SetTextScale(TextLayer, Scale);
    }

    public void SetTextRotate(int TextLayer, float Rotate) {
        TextMgr.SetTextRotate(TextLayer, Rotate);
    }

    public void ScrollText(int TextLayer) {
        TextMgr.ScrollText(TextLayer);
    }

    public boolean LoadACT(int ACTId, int ACTFileName) {
        return SpriteMgr.LoadACT(ACTId, ACTFileName);
    }

    public void InitACT(int ACTId, int ACTFileName) {
        SpriteMgr.InitACT(ACTId, ACTFileName);
    }

    public void LoadACT(int ACTLibId, int ACTFrameBeg, int ACTFrameEnd, int ACTFileName) {
        SpriteMgr.LoadACT(ACTLibId, ACTFrameBeg, ACTFrameEnd, ACTFileName);
    }

    public void LoadACT(int ACTLibId, int ACTFrameId, int ACTFileName) {
        SpriteMgr.LoadACT(ACTLibId, ACTFrameId, ACTFileName);
    }

    public int GetACTCount(int ACTId, int ACTFileName) {
        return SpriteMgr.GetACTCount(ACTId, ACTFileName);
    }

    public void FreeAllACT() {
        if (SpriteMgr != null) {
            SpriteMgr.FreeAllACT();
        }
    }

    public void FreeACT(int ACTLibId) {
        if (SpriteMgr != null) {
            SpriteMgr.FreeACT(ACTLibId);
        }
    }

    public void FreeACT(int ACTLibId, int ACTFrameID) {
        if (SpriteMgr != null) {
            SpriteMgr.FreeACT(ACTLibId, ACTFrameID);
        }
    }

    public Bitmap GetSpriteBitmap(int resId) {
        if (SpriteMgr == null) {
            return null;
        }
        return SpriteMgr.GetSpriteBitmap(resId);
    }

    public void ClearACT() {
        if (SpriteMgr != null) {
            SpriteMgr.ClearACT();
        }
    }

    public int WriteSprite(int SpriteResId, int SpriteX, int SpriteY, int SpriteAttr) {
        if (SpriteMgr == null) {
            return -1;
        }
        return SpriteMgr.WriteSprite(SpriteResId, SpriteX, SpriteY, SpriteAttr);
    }

    public int WriteSprite(int SpriteResId, int SpriteX, int SpriteY, int SpriteAttr, float rotate, float scale) {
        return SpriteMgr.WriteSprite(SpriteResId, SpriteX, SpriteY, SpriteAttr, rotate, scale);
    }

    public int WriteSprite(int SpriteResId, int SpriteX, int SpriteY, int SpriteAttr, float rotate, float scale, short RotateX, short RotateY) {
        return SpriteMgr.WriteSprite(SpriteResId, SpriteX, SpriteY, SpriteAttr, rotate, scale, RotateX, RotateY);
    }

    public int GetSpriteXHitL(int SpriteID) {
        if (SpriteMgr == null) {
            return 0;
        }
        return SpriteMgr.GetSpriteXHitL(SpriteID);
    }

    public int GetSpriteXHitR(int SpriteID) {
        if (SpriteMgr == null) {
            return 0;
        }
        return SpriteMgr.GetSpriteXHitR(SpriteID);
    }

    public int GetSpriteYHitU(int SpriteID) {
        if (SpriteMgr == null) {
            return 0;
        }
        return SpriteMgr.GetSpriteYHitU(SpriteID);
    }

    public int GetSpriteYHitD(int SpriteID) {
        if (SpriteMgr == null) {
            return 0;
        }
        return SpriteMgr.GetSpriteYHitD(SpriteID);
    }

    public int GetSpriteZHitF(int SpriteID) {
        if (SpriteMgr == null) {
            return 0;
        }
        return SpriteMgr.GetSpriteZHitF(SpriteID);
    }

    public int GetSpriteZHitB(int SpriteID) {
        if (SpriteMgr == null) {
            return 0;
        }
        return SpriteMgr.GetSpriteZHitB(SpriteID);
    }

    public boolean CHKACTTouch(int SACTName, int SXVal, int SYVal, int DACTName, int DXVal, int DYVal) {
        return SpriteMgr.CHKACTTouch(SACTName, SXVal, SYVal, DACTName, DXVal, DYVal);
    }

    public boolean CHKACTTouch(int SXVal, int SYVal, int SXHitL, int SXHitR, int SYHitU, int SYHitD, int DACTName, int DXVal, int DYVal) {
        return SpriteMgr.CHKACTTouch(SXVal, SYVal, SXHitL, SXHitR, SYHitU, SYHitD, DACTName, DXVal, DYVal);
    }

    public long GetSpriteSegSize(int ACTLibIdx) {
        return SpriteMgr.GetSpriteSegSize(ACTLibIdx);
    }

    public long GetSpriteRamSize() {
        return SpriteMgr.GetBMPRamSize();
    }

    public long GetTextRamSize() {
        return TextMgr.GetBMPRamSize();
    }
}
