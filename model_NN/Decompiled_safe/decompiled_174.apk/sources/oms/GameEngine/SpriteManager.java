package oms.GameEngine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import com.game.DodgeBall_AdSence.R;

public class SpriteManager {
    private ACTStruct ACTStructInfo;
    private SpriteDEF[] Sprite;
    private SpriteResDEF[] SpriteRes;
    private SpriteRESACTINFO[] SpriteResACTInfo;
    private boolean bLoadingSpriteRes;
    private Matrix cMatrix;
    private SpriteACTFILE cSpriteACTFile;
    private int mACTLibBeg = 0;
    private Context mContext;
    private boolean mIsLogOut = true;
    private long nCurBMPRamSize = 0;
    private int nCurFlushSpriteNum;
    private int nCurSegLoadedSpriteNum;
    private int nCurSpriteFileNum;
    private int nMaxSpriteNum;
    private int nMaxSpriteResNum;
    private int nShowSpriteNum;
    private Rect nViewRc;
    public SpriteResSeg[] pSpriteResSegInfo;

    private static class SpriteRESACTINFO {
        short RESACTIdx = -1;
    }

    private static class SpriteACTFILE {
        int nACTBuffLen = 0;
        byte[] pACTBuff = null;

        public void release() {
            if (this.pACTBuff != null) {
                this.pACTBuff = null;
            }
            this.nACTBuffLen = 0;
        }
    }

    public SpriteManager(Context context, int SpriteResNum, int SpriteNum) {
        this.mContext = context;
        Init();
        InitSpriteRes(SpriteResNum);
        InitSprite(SpriteNum);
        InitSpriteACTFile();
        this.nViewRc = GameCanvas.GetViewRect();
        this.cMatrix = new Matrix();
    }

    private void Init() {
        this.SpriteRes = null;
        this.nMaxSpriteResNum = 0;
        this.SpriteResACTInfo = null;
        this.nCurSpriteFileNum = 0;
        this.bLoadingSpriteRes = false;
        this.pSpriteResSegInfo = null;
        this.nCurSegLoadedSpriteNum = 0;
        this.ACTStructInfo = null;
        this.Sprite = null;
        this.nMaxSpriteNum = 0;
        this.nShowSpriteNum = 0;
        this.nCurFlushSpriteNum = 0;
        this.nCurBMPRamSize = 0;
        this.mACTLibBeg = 0;
    }

    public void release() {
        if (this.SpriteRes != null) {
            for (int i = 0; i < this.nMaxSpriteResNum; i++) {
                if (this.SpriteRes[i] != null) {
                    this.SpriteRes[i].release();
                }
            }
        }
        this.SpriteRes = null;
        this.nMaxSpriteResNum = 0;
        this.SpriteResACTInfo = null;
        this.nCurSpriteFileNum = 0;
        this.bLoadingSpriteRes = false;
        this.pSpriteResSegInfo = null;
        this.nCurSegLoadedSpriteNum = 0;
        this.ACTStructInfo = null;
        if (this.Sprite != null) {
            for (int i2 = 0; i2 < this.nMaxSpriteNum; i2++) {
                this.Sprite[i2].release();
            }
        }
        this.Sprite = null;
        this.nMaxSpriteNum = 0;
        this.nShowSpriteNum = 0;
        CloseSpriteACTFile();
    }

    public void SetACTLibBeg(int beg) {
        this.mACTLibBeg = beg;
    }

    private int GetBitmapSize(Bitmap bitmap) {
        int bit = 1;
        if (bitmap.getConfig() == Bitmap.Config.ALPHA_8) {
            bit = 1;
        } else if (bitmap.getConfig() == Bitmap.Config.RGB_565) {
            bit = 2;
        } else if (bitmap.getConfig() == Bitmap.Config.ARGB_4444) {
            bit = 2;
        } else if (bitmap.getConfig() == Bitmap.Config.ARGB_8888) {
            bit = 4;
        }
        return (bitmap.getWidth() * bitmap.getHeight() * bit) + GameEvent.KeepACT;
    }

    public void SetBMPSizeOut(boolean out) {
        this.mIsLogOut = out;
    }

    public long GetBMPRamSize() {
        return this.nCurBMPRamSize;
    }

    public void InitSpriteACTFile() {
        this.cSpriteACTFile = new SpriteACTFILE();
    }

    public boolean OnDraw(Canvas canvas, int Attrib, Paint paint) {
        int SpriteYVal;
        int SpriteXVal;
        if (Attrib == 0) {
            this.nCurFlushSpriteNum = 0;
        }
        if (this.nCurFlushSpriteNum < this.nShowSpriteNum) {
            for (int i = 0; i < this.nShowSpriteNum; i++) {
                int SpriteIdx = this.Sprite[i].SpriteResID;
                if (SpriteIdx != -1 && this.Sprite[i].SpriteAttrib == Attrib && (((SpriteYVal = this.Sprite[i].SpriteYVal) >= this.nViewRc.top || SpriteYVal <= this.nViewRc.bottom) && ((SpriteXVal = this.Sprite[i].SpriteXVal) >= this.nViewRc.left || SpriteXVal <= this.nViewRc.right))) {
                    canvas.drawBitmap(this.SpriteRes[SpriteIdx].Sprite, (float) (SpriteXVal - this.Sprite[i].SpriteCenterX), (float) (SpriteYVal - this.Sprite[i].SpriteCenterY), paint);
                    this.nCurFlushSpriteNum++;
                }
                if (this.nCurFlushSpriteNum == this.nShowSpriteNum) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean OnDraw(Canvas canvas, int Attrib, int XOff, int YOff, Paint paint) {
        int SpriteYVal;
        int SpriteXVal;
        if (Attrib == 0) {
            this.nCurFlushSpriteNum = 0;
        }
        if (this.nCurFlushSpriteNum < this.nShowSpriteNum) {
            for (int i = 0; i < this.nShowSpriteNum; i++) {
                int SpriteIdx = this.Sprite[i].SpriteResID;
                if (SpriteIdx != -1 && this.Sprite[i].SpriteAttrib == Attrib && (((SpriteYVal = this.Sprite[i].SpriteYVal) >= this.nViewRc.top || SpriteYVal <= this.nViewRc.bottom) && ((SpriteXVal = this.Sprite[i].SpriteXVal) >= this.nViewRc.left || SpriteXVal <= this.nViewRc.right))) {
                    if (this.Sprite[i].Rotate == 0.0f && this.Sprite[i].ScaleX == 1.0f && this.Sprite[i].ScaleY == 1.0f) {
                        canvas.drawBitmap(this.SpriteRes[SpriteIdx].Sprite, (float) ((SpriteXVal + XOff) - this.Sprite[i].SpriteCenterX), (float) ((SpriteYVal + YOff) - this.Sprite[i].SpriteCenterY), paint);
                    } else {
                        this.cMatrix.setTranslate((float) ((SpriteXVal + XOff) - this.Sprite[i].SpriteCenterX), (float) ((SpriteYVal + YOff) - this.Sprite[i].SpriteCenterY));
                        this.cMatrix.postScale(this.Sprite[i].ScaleX, this.Sprite[i].ScaleY, (float) (SpriteXVal + XOff), (float) (SpriteYVal + YOff));
                        if (this.Sprite[i].RotateX == -1 || this.Sprite[i].RotateY == -1) {
                            this.cMatrix.postRotate(this.Sprite[i].Rotate, (float) (SpriteXVal + XOff), (float) (SpriteYVal + YOff));
                        } else {
                            this.cMatrix.postRotate(this.Sprite[i].Rotate, (float) (this.Sprite[i].RotateX + XOff), (float) (this.Sprite[i].RotateY + YOff));
                        }
                        canvas.drawBitmap(this.SpriteRes[SpriteIdx].Sprite, this.cMatrix, paint);
                    }
                    this.nCurFlushSpriteNum = this.nCurFlushSpriteNum + 1;
                }
                if (this.nCurFlushSpriteNum == this.nShowSpriteNum) {
                    return true;
                }
            }
        }
        return false;
    }

    public void InitSpriteRes(int SpriteResNum) {
        this.nMaxSpriteResNum = SpriteResNum;
        if (SpriteResNum <= 0) {
            this.nMaxSpriteResNum = 1;
        }
        this.SpriteRes = new SpriteResDEF[this.nMaxSpriteResNum];
        this.SpriteResACTInfo = new SpriteRESACTINFO[this.nMaxSpriteResNum];
        for (int i = 0; i < this.nMaxSpriteResNum; i++) {
            this.SpriteRes[i] = new SpriteResDEF();
            this.SpriteResACTInfo[i] = new SpriteRESACTINFO();
        }
        this.pSpriteResSegInfo = new SpriteResSeg[this.nMaxSpriteResNum];
        for (int i2 = 0; i2 < this.nMaxSpriteResNum; i2++) {
            this.pSpriteResSegInfo[i2] = new SpriteResSeg();
        }
        this.ACTStructInfo = new ACTStruct();
        this.bLoadingSpriteRes = false;
        this.nCurSpriteFileNum = 0;
    }

    public void InitSprite(int SpriteNum) {
        this.nShowSpriteNum = 0;
        this.nMaxSpriteNum = SpriteNum;
        if (this.nMaxSpriteNum == 0) {
            this.nMaxSpriteNum = 1;
        }
        this.Sprite = new SpriteDEF[this.nMaxSpriteNum];
        for (int i = 0; i < this.nMaxSpriteNum; i++) {
            this.Sprite[i] = new SpriteDEF();
        }
    }

    public void InitACT(int ACTLibId, int ACTFileName) {
        int ACTLibId2 = ACTLibId + this.mACTLibBeg;
        if (ACTLibId2 < this.nMaxSpriteResNum && this.pSpriteResSegInfo[ACTLibId2].SegIdx != ACTLibId2) {
            InitSpriteResInfo(ACTFileName, ACTLibId2);
            CloseSpriteACTFile();
            this.pSpriteResSegInfo[ACTLibId2].SegIdx = (short) ACTLibId2;
        }
    }

    public boolean LoadACT(int ACTLibId, int ACTFileName) {
        int ACTLibId2 = ACTLibId + this.mACTLibBeg;
        if (this.pSpriteResSegInfo[ACTLibId2].SegIdx == ACTLibId2) {
            return true;
        }
        boolean result = LoadSpriteResInfo(ACTFileName, ACTLibId2);
        if (result) {
            CloseSpriteACTFile();
            this.pSpriteResSegInfo[ACTLibId2].SegIdx = (short) ACTLibId2;
        }
        return result;
    }

    public void LoadACT(int ACTLibId, int ACTFrameBeg, int ACTFrameEnd, int ACTFileName) {
        int ACTLibId2 = ACTLibId + this.mACTLibBeg;
        if (ACTLibId2 < this.nMaxSpriteResNum) {
            InitSpriteResInfo(ACTFileName, ACTLibId2, ACTFrameBeg, ACTFrameEnd);
            CloseSpriteACTFile();
        }
    }

    public void LoadACT(int ACTLibId, int ACTFrameId, int ACTFileName) {
        int ACTLibId2 = ACTLibId + this.mACTLibBeg;
        if (ACTLibId2 < this.nMaxSpriteResNum) {
            InitSpriteResInfo(ACTFileName, ACTLibId2, ACTFrameId);
            CloseSpriteACTFile();
        }
    }

    public int GetACTCount(int ACTId, int ACTFileName) {
        int ACTId2 = ACTId + this.mACTLibBeg;
        RESApp rs = new RESApp(this.mContext);
        rs.OpenRes(ACTFileName);
        GetFileHead(rs, ACTId2);
        rs.CloseRes();
        return this.ACTStructInfo.FileNum;
    }

    public boolean LoadSpriteACTFile(int ResId) {
        RESApp rs = new RESApp(this.mContext);
        rs.OpenRes(ResId);
        if (rs.getLength() > 0) {
            this.cSpriteACTFile.nACTBuffLen = rs.getLength();
            this.cSpriteACTFile.pACTBuff = new byte[this.cSpriteACTFile.nACTBuffLen];
            rs.ResRead(this.cSpriteACTFile.pACTBuff, 0, this.cSpriteACTFile.nACTBuffLen);
            rs.CloseRes();
            return true;
        }
        rs.CloseRes();
        return false;
    }

    public void CloseSpriteACTFile() {
        this.cSpriteACTFile.release();
    }

    public void InitSpriteResInfo(int ResId, int ACTLibId) {
        this.nCurSegLoadedSpriteNum = 0;
        if (LoadSpriteACTFile(ResId)) {
            GetFileHead(ACTLibId);
            for (int i = 0; i < this.ACTStructInfo.FileNum; i++) {
                GetSpriteHead(i);
                for (int j = 0; j < this.ACTStructInfo.SpriteNum; j++) {
                    if (this.SpriteResACTInfo[GetSpriteResID(j) & 65535].RESACTIdx == -1) {
                        ReadSpriteResInfo(j, ACTLibId);
                    }
                }
                this.nCurSegLoadedSpriteNum += this.ACTStructInfo.SpriteNum;
            }
        }
    }

    public boolean LoadSpriteResInfo(int ResId, int ACTLibId) {
        boolean result = false;
        if (!this.bLoadingSpriteRes) {
            if (ACTLibId >= this.nMaxSpriteResNum) {
                return true;
            }
            if (!LoadSpriteACTFile(ResId)) {
                return true;
            }
            GetFileHead(ACTLibId);
            this.nCurSegLoadedSpriteNum = 0;
            this.nCurSpriteFileNum = 0;
            this.bLoadingSpriteRes = true;
        }
        if (this.nCurSpriteFileNum < this.ACTStructInfo.FileNum) {
            GetSpriteHead(this.nCurSpriteFileNum);
            for (int j = 0; j < this.ACTStructInfo.SpriteNum; j++) {
                if (this.SpriteResACTInfo[GetSpriteResID(j) & 65535].RESACTIdx == -1) {
                    ReadSpriteResInfo(j, ACTLibId);
                }
            }
            this.nCurSegLoadedSpriteNum += this.ACTStructInfo.SpriteNum;
            this.nCurSpriteFileNum++;
        } else {
            this.bLoadingSpriteRes = false;
            result = true;
        }
        return result;
    }

    public void InitSpriteResInfo(int ResId, int ACTLibId, int ACTFrameBeg, int ACTFrameEnd) {
        int End;
        if (LoadSpriteACTFile(ResId)) {
            GetFileHead(ACTLibId);
            GetSpriteHead(0);
            if (ACTFrameEnd < this.ACTStructInfo.SpriteNum) {
                End = ACTFrameEnd;
            } else {
                End = this.ACTStructInfo.SpriteNum;
            }
            for (int j = ACTFrameBeg; j < End; j++) {
                if (this.SpriteResACTInfo[GetSpriteResID(j) & 65535].RESACTIdx == -1) {
                    ReadSpriteResInfo(j, ACTLibId);
                }
            }
        }
    }

    public void InitSpriteResInfo(int ResId, int ACTLibId, int ACTFrameId) {
        boolean ret = false;
        if (LoadSpriteACTFile(ResId)) {
            GetFileHead(ACTLibId);
            int j = 0;
            while (j < this.ACTStructInfo.FileNum) {
                GetSpriteHead(j);
                int i = 0;
                while (true) {
                    if (i >= this.ACTStructInfo.SpriteNum) {
                        break;
                    } else if (GetSpriteResID(i) == ACTFrameId) {
                        ACTFrameId &= 65535;
                        if (this.SpriteResACTInfo[ACTFrameId].RESACTIdx == -1) {
                            ReadSpriteResInfo(i, ACTLibId);
                        }
                        ret = true;
                    } else {
                        i++;
                    }
                }
                if (!ret) {
                    j++;
                } else {
                    return;
                }
            }
        }
    }

    private void GetFileHead(RESApp rs, int ACTId) {
        byte[] buff = new byte[4];
        rs.ResRead(buff, ACTId * 4, 4);
        this.ACTStructInfo.FileNum = 0;
        for (int i = 0; i < 2; i++) {
            this.ACTStructInfo.FileNum += (buff[i] & C_MotionEventWrapper8.ACTION_MASK) << (i * 8);
        }
        this.ACTStructInfo.FileIndexAddr = 0;
        for (int i2 = 0; i2 < 2; i2++) {
            this.ACTStructInfo.FileIndexAddr += (buff[i2 + 2] & C_MotionEventWrapper8.ACTION_MASK) << (i2 * 8);
        }
    }

    private void GetFileHead(int ACTLibId) {
        int nBuffBeg = ACTLibId * 4;
        byte[] buff = this.cSpriteACTFile.pACTBuff;
        this.ACTStructInfo.FileNum = 0;
        for (int i = 0; i < 2; i++) {
            this.ACTStructInfo.FileNum += (buff[nBuffBeg + i] & C_MotionEventWrapper8.ACTION_MASK) << (i * 8);
        }
        this.ACTStructInfo.FileIndexAddr = 0;
        for (int i2 = 0; i2 < 2; i2++) {
            this.ACTStructInfo.FileIndexAddr += (buff[(nBuffBeg + 2) + i2] & C_MotionEventWrapper8.ACTION_MASK) << (i2 * 8);
        }
    }

    private void GetSpriteHead(int ACTLibId) {
        int nBuffBeg = (ACTLibId * 8) + this.ACTStructInfo.FileIndexAddr;
        byte[] buff = this.cSpriteACTFile.pACTBuff;
        this.ACTStructInfo.SpriteNum = 0;
        for (int i = 0; i < 4; i++) {
            this.ACTStructInfo.SpriteNum += (buff[nBuffBeg + i] & C_MotionEventWrapper8.ACTION_MASK) << (i * 8);
        }
        this.ACTStructInfo.SpriteIndexAddr = 0;
        for (int i2 = 0; i2 < 4; i2++) {
            this.ACTStructInfo.SpriteIndexAddr += (buff[(nBuffBeg + i2) + 4] & C_MotionEventWrapper8.ACTION_MASK) << (i2 * 8);
        }
    }

    private int GetSpriteResID(int ACTFileId) {
        byte[] buff = this.cSpriteACTFile.pACTBuff;
        int SpriteInfoOffset = (ACTFileId * 20) + this.ACTStructInfo.SpriteIndexAddr;
        int ResID = 0;
        for (int i = 0; i < 4; i++) {
            ResID += (buff[SpriteInfoOffset + i] & C_MotionEventWrapper8.ACTION_MASK) << (i * 8);
        }
        return ResID;
    }

    private void ReadSpriteResInfo(int ACTFileId, int ACTLibId) {
        int nResId;
        byte[] buff = this.cSpriteACTFile.pACTBuff;
        int SpriteInfoOffset = (ACTFileId * 20) + this.ACTStructInfo.SpriteIndexAddr;
        this.ACTStructInfo.ResID = 0;
        for (int i = 0; i < 4; i++) {
            this.ACTStructInfo.ResID += (buff[SpriteInfoOffset + i] & C_MotionEventWrapper8.ACTION_MASK) << (i * 8);
        }
        int SpriteInfoOffset2 = SpriteInfoOffset + 4;
        this.ACTStructInfo.CenterX = 0;
        for (int i2 = 0; i2 < 2; i2++) {
            this.ACTStructInfo.CenterX += (buff[SpriteInfoOffset2 + i2] & C_MotionEventWrapper8.ACTION_MASK) << (i2 * 8);
        }
        if ((this.ACTStructInfo.CenterX & 32768) == 32768) {
            this.ACTStructInfo.CenterX = -(65536 - this.ACTStructInfo.CenterX);
        }
        int SpriteInfoOffset3 = SpriteInfoOffset2 + 2;
        this.ACTStructInfo.CenterY = 0;
        for (int i3 = 0; i3 < 2; i3++) {
            this.ACTStructInfo.CenterY += (buff[SpriteInfoOffset3 + i3] & C_MotionEventWrapper8.ACTION_MASK) << (i3 * 8);
        }
        if ((this.ACTStructInfo.CenterY & 32768) == 32768) {
            this.ACTStructInfo.CenterY = -(65536 - this.ACTStructInfo.CenterY);
        }
        int SpriteInfoOffset4 = SpriteInfoOffset3 + 2;
        this.ACTStructInfo.XHitL = 0;
        for (int i4 = 0; i4 < 2; i4++) {
            this.ACTStructInfo.XHitL += (buff[SpriteInfoOffset4 + i4] & C_MotionEventWrapper8.ACTION_MASK) << (i4 * 8);
        }
        if ((this.ACTStructInfo.XHitL & 32768) == 32768) {
            this.ACTStructInfo.XHitL = -(65536 - this.ACTStructInfo.XHitL);
        }
        int SpriteInfoOffset5 = SpriteInfoOffset4 + 2;
        this.ACTStructInfo.XHitR = 0;
        for (int i5 = 0; i5 < 2; i5++) {
            this.ACTStructInfo.XHitR += (buff[SpriteInfoOffset5 + i5] & C_MotionEventWrapper8.ACTION_MASK) << (i5 * 8);
        }
        if ((this.ACTStructInfo.XHitR & 32768) == 32768) {
            this.ACTStructInfo.XHitR = -(65536 - this.ACTStructInfo.XHitR);
        }
        int SpriteInfoOffset6 = SpriteInfoOffset5 + 2;
        this.ACTStructInfo.YHitU = 0;
        for (int i6 = 0; i6 < 2; i6++) {
            this.ACTStructInfo.YHitU += (buff[SpriteInfoOffset6 + i6] & C_MotionEventWrapper8.ACTION_MASK) << (i6 * 8);
        }
        if ((this.ACTStructInfo.YHitU & 32768) == 32768) {
            this.ACTStructInfo.YHitU = -(65536 - this.ACTStructInfo.YHitU);
        }
        int SpriteInfoOffset7 = SpriteInfoOffset6 + 2;
        this.ACTStructInfo.YHitD = 0;
        for (int i7 = 0; i7 < 2; i7++) {
            this.ACTStructInfo.YHitD += (buff[SpriteInfoOffset7 + i7] & C_MotionEventWrapper8.ACTION_MASK) << (i7 * 8);
        }
        if ((this.ACTStructInfo.YHitD & 32768) == 32768) {
            this.ACTStructInfo.YHitD = -(65536 - this.ACTStructInfo.YHitD);
        }
        int SpriteInfoOffset8 = SpriteInfoOffset7 + 2;
        this.ACTStructInfo.ZHitF = 0;
        for (int i8 = 0; i8 < 2; i8++) {
            this.ACTStructInfo.ZHitF += (buff[SpriteInfoOffset8 + i8] & C_MotionEventWrapper8.ACTION_MASK) << (i8 * 8);
        }
        if ((this.ACTStructInfo.ZHitF & 32768) == 32768) {
            this.ACTStructInfo.ZHitF = -(65536 - this.ACTStructInfo.ZHitF);
        }
        int SpriteInfoOffset9 = SpriteInfoOffset8 + 2;
        this.ACTStructInfo.ZHitB = 0;
        for (int i9 = 0; i9 < 2; i9++) {
            this.ACTStructInfo.ZHitB += (buff[SpriteInfoOffset9 + i9] & C_MotionEventWrapper8.ACTION_MASK) << (i9 * 8);
        }
        if ((this.ACTStructInfo.ZHitB & 32768) == 32768) {
            this.ACTStructInfo.ZHitB = -(65536 - this.ACTStructInfo.ZHitB);
        }
        int SpriteInfoOffset10 = SpriteInfoOffset9 + 2;
        Bitmap bmp = BitmapFactory.decodeResource(this.mContext.getResources(), this.ACTStructInfo.ResID);
        if (bmp == null) {
            bmp = PackageManager.createBitmap(this.mContext, this.ACTStructInfo.ResID);
        }
        if (bmp != null && (nResId = this.ACTStructInfo.ResID & 65535) < this.nMaxSpriteResNum) {
            if (this.mIsLogOut) {
                Log.v("GameEngine", "ResID: " + this.ACTStructInfo.ResID);
                Log.v("GameEngine", "Width: " + bmp.getWidth());
                Log.v("GameEngine", "Height: " + bmp.getHeight());
                Log.v("GameEngine", "OPtions: " + bmp.getConfig());
                Log.v("GameEngine", "Picture use RAM: " + (this.nCurBMPRamSize / 1024) + " KBytes");
            }
            this.nCurBMPRamSize += (long) GetBitmapSize(bmp);
            this.SpriteRes[nResId].Sprite = bmp;
            this.SpriteRes[nResId].SpriteResID = this.ACTStructInfo.ResID;
            this.SpriteRes[nResId].SpriteCenterX = this.ACTStructInfo.CenterX;
            this.SpriteRes[nResId].SpriteCenterY = this.ACTStructInfo.CenterY;
            this.SpriteRes[nResId].SpriteXHitL = this.ACTStructInfo.XHitL;
            this.SpriteRes[nResId].SpriteXHitR = this.ACTStructInfo.XHitR;
            this.SpriteRes[nResId].SpriteYHitU = this.ACTStructInfo.YHitU;
            this.SpriteRes[nResId].SpriteYHitD = this.ACTStructInfo.YHitD;
            this.SpriteRes[nResId].SpriteZHitF = this.ACTStructInfo.ZHitF;
            this.SpriteRes[nResId].SpriteZHitB = this.ACTStructInfo.ZHitB;
            this.SpriteResACTInfo[nResId].RESACTIdx = (short) ACTLibId;
            this.pSpriteResSegInfo[ACTLibId].SegSize += (bmp.getWidth() * bmp.getHeight() * 4) + GameEvent.KeepACT;
        }
    }

    public void FreeAllACT() {
        for (int i = 0; i < this.nMaxSpriteResNum; i++) {
            if (this.SpriteRes[i].Sprite != null) {
                this.SpriteRes[i].Sprite.recycle();
            }
            this.SpriteRes[i].Sprite = null;
            this.SpriteRes[i].SpriteResID = -1;
            this.SpriteResACTInfo[i].RESACTIdx = -1;
            this.pSpriteResSegInfo[i].SegSize = 0;
            this.pSpriteResSegInfo[i].SegIdx = -1;
        }
        this.nCurBMPRamSize = 0;
        System.gc();
    }

    public void FreeACT(int ACTLibIdx) {
        int ACTLibIdx2 = ACTLibIdx + this.mACTLibBeg;
        for (int i = 0; i < this.nMaxSpriteResNum; i++) {
            if (this.SpriteResACTInfo[i].RESACTIdx == ACTLibIdx2) {
                if (this.SpriteRes[i].Sprite != null) {
                    int Size = GetBitmapSize(this.SpriteRes[i].Sprite);
                    this.nCurBMPRamSize -= (long) Size;
                    this.pSpriteResSegInfo[ACTLibIdx2].SegSize -= Size;
                    this.pSpriteResSegInfo[ACTLibIdx2].SegIdx = -1;
                    this.SpriteRes[i].Sprite.recycle();
                }
                this.SpriteRes[i].Sprite = null;
                this.SpriteRes[i].SpriteResID = -1;
                this.SpriteResACTInfo[i].RESACTIdx = -1;
            }
        }
        if (this.mIsLogOut) {
            Log.v("GameEngine", "Picture use RAM: " + (this.nCurBMPRamSize / 1024) + " KBytes");
        }
        System.gc();
    }

    public void FreeACT(int ACTLibId, int ACTFrameID) {
        if (ACTFrameID >= R.drawable.act_ballbomb00) {
            int ACTLibId2 = ACTLibId + this.mACTLibBeg;
            int ACTFrameID2 = ACTFrameID & 65535;
            if (ACTFrameID2 < this.nMaxSpriteResNum && this.SpriteResACTInfo[ACTFrameID2].RESACTIdx != -1) {
                if (this.SpriteRes[ACTFrameID2].Sprite != null) {
                    int Size = GetBitmapSize(this.SpriteRes[ACTFrameID2].Sprite);
                    this.nCurBMPRamSize -= (long) Size;
                    this.pSpriteResSegInfo[ACTLibId2].SegSize -= Size;
                    this.SpriteRes[ACTFrameID2].Sprite.recycle();
                }
                this.SpriteRes[ACTFrameID2].Sprite = null;
                this.SpriteRes[ACTFrameID2].SpriteResID = -1;
                this.SpriteResACTInfo[ACTFrameID2].RESACTIdx = -1;
            }
            if (this.mIsLogOut) {
                Log.v("GameEngine", "Picture use RAM: " + (this.nCurBMPRamSize / 1024) + " KBytes");
            }
        }
    }

    public Bitmap GetSpriteBitmap(int resId) {
        if (resId < R.drawable.act_ballbomb00) {
            return null;
        }
        return this.SpriteRes[resId & 65535].Sprite;
    }

    public void ClearACT() {
        for (int i = 0; i < this.nShowSpriteNum; i++) {
            this.Sprite[i].SpriteResID = -1;
            this.Sprite[i].Rotate = 0.0f;
            this.Sprite[i].ScaleX = 1.0f;
            this.Sprite[i].ScaleY = 1.0f;
        }
        this.nShowSpriteNum = 0;
    }

    public int WriteSprite(int SpriteResId, int SpriteX, int SpriteY, int SpriteAttr) {
        int SpriteIdx = -1;
        if (this.nShowSpriteNum >= this.nMaxSpriteNum) {
            return -1;
        }
        if (SpriteResId < R.drawable.act_ballbomb00) {
            return -1;
        }
        if (this.SpriteResACTInfo[SpriteResId & 65535].RESACTIdx != -1) {
            SpriteIdx = SpriteResId & 65535;
        }
        if (SpriteIdx != -1) {
            this.Sprite[this.nShowSpriteNum].SpriteResID = (short) SpriteIdx;
            this.Sprite[this.nShowSpriteNum].SpriteXVal = SpriteX;
            this.Sprite[this.nShowSpriteNum].SpriteYVal = SpriteY;
            this.Sprite[this.nShowSpriteNum].Rotate = 0.0f;
            this.Sprite[this.nShowSpriteNum].ScaleX = 1.0f;
            this.Sprite[this.nShowSpriteNum].ScaleY = 1.0f;
            this.Sprite[this.nShowSpriteNum].SpriteCenterX = (short) this.SpriteRes[SpriteIdx].SpriteCenterX;
            this.Sprite[this.nShowSpriteNum].SpriteCenterY = (short) this.SpriteRes[SpriteIdx].SpriteCenterY;
            this.Sprite[this.nShowSpriteNum].RotateX = -1;
            this.Sprite[this.nShowSpriteNum].RotateY = -1;
            SpriteIdx = this.nShowSpriteNum;
            SpriteDEF[] spriteDEFArr = this.Sprite;
            int i = this.nShowSpriteNum;
            this.nShowSpriteNum = i + 1;
            spriteDEFArr[i].SpriteAttrib = SpriteAttr;
        }
        return SpriteIdx;
    }

    public int WriteSprite(int SpriteResId, int SpriteX, int SpriteY, int SpriteAttr, float rotate, float scale) {
        int SpriteIdx = -1;
        if (this.nShowSpriteNum >= this.nMaxSpriteNum) {
            return -1;
        }
        if (SpriteResId < R.drawable.act_ballbomb00) {
            return -1;
        }
        if (this.SpriteResACTInfo[SpriteResId & 65535].RESACTIdx != -1) {
            SpriteIdx = SpriteResId & 65535;
        }
        if (SpriteIdx != -1) {
            this.Sprite[this.nShowSpriteNum].SpriteResID = (short) SpriteIdx;
            this.Sprite[this.nShowSpriteNum].SpriteXVal = SpriteX;
            this.Sprite[this.nShowSpriteNum].SpriteYVal = SpriteY;
            this.Sprite[this.nShowSpriteNum].Rotate = rotate;
            this.Sprite[this.nShowSpriteNum].ScaleX = scale;
            this.Sprite[this.nShowSpriteNum].ScaleY = scale;
            this.Sprite[this.nShowSpriteNum].SpriteCenterX = (short) this.SpriteRes[SpriteIdx].SpriteCenterX;
            this.Sprite[this.nShowSpriteNum].SpriteCenterY = (short) this.SpriteRes[SpriteIdx].SpriteCenterY;
            this.Sprite[this.nShowSpriteNum].RotateX = -1;
            this.Sprite[this.nShowSpriteNum].RotateY = -1;
            SpriteIdx = this.nShowSpriteNum;
            SpriteDEF[] spriteDEFArr = this.Sprite;
            int i = this.nShowSpriteNum;
            this.nShowSpriteNum = i + 1;
            spriteDEFArr[i].SpriteAttrib = SpriteAttr;
        }
        return SpriteIdx;
    }

    public int WriteSprite(int SpriteResId, int SpriteX, int SpriteY, int SpriteAttr, float rotate, float scale, short RotateX, short RotateY) {
        int SpriteIdx = -1;
        if (this.nShowSpriteNum >= this.nMaxSpriteNum) {
            return -1;
        }
        if (SpriteResId < R.drawable.act_ballbomb00) {
            return -1;
        }
        if (this.SpriteResACTInfo[SpriteResId & 65535].RESACTIdx != -1) {
            SpriteIdx = SpriteResId & 65535;
        }
        if (SpriteIdx != -1) {
            this.Sprite[this.nShowSpriteNum].SpriteResID = (short) SpriteIdx;
            this.Sprite[this.nShowSpriteNum].SpriteXVal = SpriteX;
            this.Sprite[this.nShowSpriteNum].SpriteYVal = SpriteY;
            this.Sprite[this.nShowSpriteNum].Rotate = rotate;
            this.Sprite[this.nShowSpriteNum].ScaleX = scale;
            this.Sprite[this.nShowSpriteNum].ScaleY = scale;
            this.Sprite[this.nShowSpriteNum].SpriteCenterX = (short) this.SpriteRes[SpriteIdx].SpriteCenterX;
            this.Sprite[this.nShowSpriteNum].SpriteCenterY = (short) this.SpriteRes[SpriteIdx].SpriteCenterY;
            if (RotateX != 65535) {
                this.Sprite[this.nShowSpriteNum].RotateX = RotateX;
            } else {
                this.Sprite[this.nShowSpriteNum].RotateX = this.Sprite[this.nShowSpriteNum].SpriteCenterX;
            }
            if (RotateX != 65535) {
                this.Sprite[this.nShowSpriteNum].RotateY = RotateY;
            } else {
                this.Sprite[this.nShowSpriteNum].RotateY = this.Sprite[this.nShowSpriteNum].SpriteCenterY;
            }
            SpriteIdx = this.nShowSpriteNum;
            SpriteDEF[] spriteDEFArr = this.Sprite;
            int i = this.nShowSpriteNum;
            this.nShowSpriteNum = i + 1;
            spriteDEFArr[i].SpriteAttrib = SpriteAttr;
        }
        return SpriteIdx;
    }

    public int WriteSprite(int SpriteResId, int SpriteX, int SpriteY, int SpriteAttr, float rotate, float scaleX, float scaleY, short RotateX, short RotateY) {
        int SpriteIdx = -1;
        if (this.nShowSpriteNum >= this.nMaxSpriteNum) {
            return -1;
        }
        if (SpriteResId < R.drawable.act_ballbomb00) {
            return -1;
        }
        if (this.SpriteResACTInfo[SpriteResId & 65535].RESACTIdx != -1) {
            SpriteIdx = SpriteResId & 65535;
        }
        if (SpriteIdx != -1) {
            this.Sprite[this.nShowSpriteNum].SpriteResID = (short) SpriteIdx;
            this.Sprite[this.nShowSpriteNum].SpriteXVal = SpriteX;
            this.Sprite[this.nShowSpriteNum].SpriteYVal = SpriteY;
            this.Sprite[this.nShowSpriteNum].Rotate = rotate;
            this.Sprite[this.nShowSpriteNum].ScaleX = scaleX;
            this.Sprite[this.nShowSpriteNum].ScaleY = scaleY;
            this.Sprite[this.nShowSpriteNum].SpriteCenterX = (short) this.SpriteRes[SpriteIdx].SpriteCenterX;
            this.Sprite[this.nShowSpriteNum].SpriteCenterY = (short) this.SpriteRes[SpriteIdx].SpriteCenterY;
            if (RotateX != 65535) {
                this.Sprite[this.nShowSpriteNum].RotateX = RotateX;
            } else {
                this.Sprite[this.nShowSpriteNum].RotateX = this.Sprite[this.nShowSpriteNum].SpriteCenterX;
            }
            if (RotateX != 65535) {
                this.Sprite[this.nShowSpriteNum].RotateY = RotateY;
            } else {
                this.Sprite[this.nShowSpriteNum].RotateY = this.Sprite[this.nShowSpriteNum].SpriteCenterY;
            }
            SpriteIdx = this.nShowSpriteNum;
            SpriteDEF[] spriteDEFArr = this.Sprite;
            int i = this.nShowSpriteNum;
            this.nShowSpriteNum = i + 1;
            spriteDEFArr[i].SpriteAttrib = SpriteAttr;
        }
        return SpriteIdx;
    }

    public long GetSpriteSegSize(int ACTLibIdx) {
        if (ACTLibIdx >= this.nMaxSpriteResNum) {
            return 0;
        }
        return (long) this.pSpriteResSegInfo[ACTLibIdx].SegSize;
    }

    public int GetSpriteXHitL(int SpriteID) {
        if (this.SpriteResACTInfo[SpriteID & 65535].RESACTIdx == -1) {
            return 0;
        }
        return this.SpriteRes[SpriteID & 65535].SpriteXHitL;
    }

    public int GetSpriteXHitR(int SpriteID) {
        if (this.SpriteResACTInfo[SpriteID & 65535].RESACTIdx == -1) {
            return 0;
        }
        return this.SpriteRes[SpriteID & 65535].SpriteXHitR;
    }

    public int GetSpriteYHitU(int SpriteID) {
        if (this.SpriteResACTInfo[SpriteID & 65535].RESACTIdx == -1) {
            return 0;
        }
        return this.SpriteRes[SpriteID & 65535].SpriteYHitU;
    }

    public int GetSpriteYHitD(int SpriteID) {
        if (this.SpriteResACTInfo[SpriteID & 65535].RESACTIdx == -1) {
            return 0;
        }
        return this.SpriteRes[SpriteID & 65535].SpriteYHitD;
    }

    public int GetSpriteZHitF(int SpriteID) {
        if (this.SpriteResACTInfo[SpriteID & 65535].RESACTIdx == -1) {
            return 0;
        }
        return this.SpriteRes[SpriteID & 65535].SpriteZHitF;
    }

    public int GetSpriteZHitB(int SpriteID) {
        if (this.SpriteResACTInfo[SpriteID & 65535].RESACTIdx == -1) {
            return 0;
        }
        return this.SpriteRes[SpriteID & 65535].SpriteZHitB;
    }

    public boolean CHKACTTouch(int SACTName, int SXVal, int SYVal, int DACTName, int DXVal, int DYVal) {
        if (GameMath.CHKTouch(GetSpriteXHitL(SACTName), SXVal, GetSpriteXHitR(SACTName), GetSpriteXHitL(DACTName), DXVal, GetSpriteXHitR(DACTName))) {
            if (GameMath.CHKTouch(GetSpriteYHitU(SACTName), SYVal, GetSpriteYHitD(SACTName), GetSpriteYHitU(DACTName), DYVal, GetSpriteYHitD(DACTName))) {
                return true;
            }
        }
        return false;
    }

    public boolean CHKACTTouch(int SXVal, int SYVal, int SXHitL, int SXHitR, int SYHitU, int SYHitD, int DACTName, int DXVal, int DYVal) {
        if (GameMath.CHKTouch(SXHitL, SXVal, SXHitR, GetSpriteXHitL(DACTName), DXVal, GetSpriteXHitR(DACTName))) {
            if (GameMath.CHKTouch(SYHitU, SYVal, SYHitD, GetSpriteYHitU(DACTName), DYVal, GetSpriteYHitD(DACTName))) {
                return true;
            }
        }
        return false;
    }
}
