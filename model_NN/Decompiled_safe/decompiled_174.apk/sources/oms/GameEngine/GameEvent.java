package oms.GameEngine;

import android.graphics.Rect;

public class GameEvent {
    public static final int CVTEVT = 32768;
    public static final int FIXEVT = 8192;
    public static final int KeepACT = 128;
    public static final int KeepADC = 512;
    public static final int KeepHIT = 256;
    public static final int KeepINC = 1024;
    public static final int MAPEVT = 4096;
    public static final int RUNEVT = 16384;
    public static final int V3DEVT = 2048;
    public EventDEF EVT;
    public Rect mRc;

    public class EventDEF {
        public int ACTIdx = 0;
        public int[][] ACTPtr = null;
        public int Attrib = 0;
        public int Ctrl = 0;
        public int CurCNT = 0;
        public int CurFRM = 0;
        public int DispX = 0;
        public int DispY = 0;
        public int[][] EVTPtr = null;
        public int Flag = 0;
        public int MaxCNT = 0;
        public int MaxFRM = 0;
        public float Rotate = 0.0f;
        public float Scale = 1.0f;
        public int Status = 0;
        public boolean Touch = false;
        public int TouchEVTIdx = 0;
        public String TouchEVTName = new String();
        public boolean Valid = false;
        public int XAdc = 0;
        public int XHitL = 0;
        public int XHitR = 0;
        public int XInc = 0;
        public int XVal = 0;
        public int YAdc = 0;
        public int YHitD = 0;
        public int YHitU = 0;
        public int YInc = 0;
        public int YVal = 0;
        public int ZAdc = 0;
        public int ZHitB = 0;
        public int ZHitF = 0;
        public int ZInc = 0;
        public int ZVal = 0;

        public EventDEF() {
        }
    }

    public GameEvent() {
        this.EVT = null;
        this.mRc = GameCanvas.GetViewRect();
        this.EVT = new EventDEF();
        this.EVT.Valid = false;
    }

    public void release() {
        try {
            finalize();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public boolean MakeEVENT(int x, int y, int z) {
        if (this.EVT.Valid) {
            return false;
        }
        this.EVT.Valid = true;
        this.EVT.Ctrl = 0;
        this.EVT.MaxCNT = 0;
        this.EVT.MaxFRM = 0;
        this.EVT.CurCNT = 0;
        this.EVT.CurFRM = 0;
        this.EVT.XVal = x << 16;
        this.EVT.YVal = y << 16;
        this.EVT.ZVal = z << 16;
        this.EVT.XInc = 0;
        this.EVT.YInc = 0;
        this.EVT.ZInc = 0;
        this.EVT.XAdc = 0;
        this.EVT.YAdc = 0;
        this.EVT.ZAdc = 0;
        this.EVT.Flag = 0;
        this.EVT.Attrib = 0;
        this.EVT.Status = 32768;
        this.EVT.DispX = x;
        this.EVT.DispY = y;
        this.EVT.XHitL = 0;
        this.EVT.XHitR = 0;
        this.EVT.YHitU = 0;
        this.EVT.YHitD = 0;
        this.EVT.ZHitF = 0;
        this.EVT.ZHitB = 0;
        this.EVT.Touch = false;
        this.EVT.ACTIdx = 0;
        this.EVT.TouchEVTIdx = 0;
        this.EVT.TouchEVTName = "";
        this.EVT.Rotate = 0.0f;
        this.EVT.Scale = 1.0f;
        return true;
    }

    public void ExecEVT(GameCanvas canvas) {
        if (canvas != null && this.EVT.Valid) {
            EVTCVT();
            EVTDIR();
            EVTHIT(canvas);
        }
    }

    public void ExecRUN(GameCanvas canvas) {
        if (canvas != null && this.EVT.Valid) {
            if ((this.EVT.Status & 32768) != 32768) {
                EVTRUN();
            }
            TouchEVTClr();
            CheckEVTFun(canvas);
            EVTACT();
        }
    }

    public void EVTCVT() {
        int Status = this.EVT.Status;
        int Ctrl = this.EVT.Ctrl;
        if ((Status & 32768) == 32768) {
            if ((Status & KeepADC) != 512) {
                this.EVT.XAdc = this.EVT.EVTPtr[Ctrl][3];
                this.EVT.YAdc = this.EVT.EVTPtr[Ctrl][4];
                this.EVT.ZAdc = this.EVT.EVTPtr[Ctrl][5];
            }
            if ((Status & KeepINC) != 1024) {
                this.EVT.XInc = this.EVT.EVTPtr[Ctrl][0];
                this.EVT.YInc = this.EVT.EVTPtr[Ctrl][1];
                this.EVT.ZInc = this.EVT.EVTPtr[Ctrl][2];
            }
            if ((Status & KeepACT) != 128) {
                this.EVT.CurCNT = 0;
                this.EVT.CurFRM = 0;
            }
            this.EVT.MaxCNT = this.EVT.EVTPtr[Ctrl][6];
            this.EVT.MaxFRM = this.EVT.EVTPtr[Ctrl][7];
            if ((Status & KeepHIT) != 256) {
                this.EVT.ACTIdx = this.EVT.ACTPtr[Ctrl][this.EVT.CurFRM];
            }
            this.EVT.Status &= -34689;
        }
    }

    public void EVTHIT(GameCanvas canvas) {
        int ACTIdx;
        if ((this.EVT.Status & KeepHIT) != 256 && (ACTIdx = this.EVT.ACTIdx) != 0) {
            this.EVT.XHitL = canvas.GetSpriteXHitL(ACTIdx);
            this.EVT.XHitR = canvas.GetSpriteXHitR(ACTIdx);
            this.EVT.YHitU = canvas.GetSpriteYHitU(ACTIdx);
            this.EVT.YHitD = canvas.GetSpriteYHitD(ACTIdx);
            this.EVT.ZHitF = canvas.GetSpriteZHitF(ACTIdx);
            this.EVT.ZHitB = canvas.GetSpriteZHitB(ACTIdx);
        }
    }

    public void EVTDIR() {
        int Status = this.EVT.Status;
        if ((Status & KeepADC) != 512) {
            this.EVT.XInc += this.EVT.XAdc;
            this.EVT.YInc += this.EVT.YAdc;
            this.EVT.ZInc += this.EVT.ZAdc;
        }
        if ((Status & KeepINC) != 1024) {
            this.EVT.XVal += this.EVT.XInc;
            this.EVT.YVal += this.EVT.YInc;
            this.EVT.ZVal += this.EVT.ZInc;
        }
    }

    public void TouchEVTClr() {
        this.EVT.Touch = false;
        this.EVT.TouchEVTIdx = 0;
        this.EVT.TouchEVTName = "";
    }

    public void EVTACT() {
        if ((this.EVT.Status & 32768) != 32768) {
            this.EVT.ACTIdx = this.EVT.ACTPtr[this.EVT.Ctrl][this.EVT.CurFRM];
            if ((this.EVT.Status & KeepACT) != 128) {
                EventDEF eventDEF = this.EVT;
                int i = eventDEF.CurCNT + 1;
                eventDEF.CurCNT = i;
                if (i >= this.EVT.MaxCNT) {
                    this.EVT.CurCNT = 0;
                    EventDEF eventDEF2 = this.EVT;
                    int i2 = eventDEF2.CurFRM + 1;
                    eventDEF2.CurFRM = i2;
                    if (i2 >= this.EVT.MaxFRM) {
                        this.EVT.CurFRM = 0;
                    }
                }
            }
        }
    }

    public void ShowEVENT(GameCanvas canvas) {
        if (canvas != null && this.EVT.Valid) {
            if ((this.EVT.Status & MAPEVT) != 0) {
                this.EVT.DispX = (this.EVT.XVal - canvas.GetTextXVal(0)) >> 16;
                this.EVT.DispY = (this.EVT.YVal - canvas.GetTextYVal(0)) >> 16;
            } else if ((this.EVT.Status & V3DEVT) != 2048) {
                this.EVT.DispX = this.EVT.XVal >> 16;
                this.EVT.DispY = this.EVT.YVal >> 16;
            }
            int DispX = this.EVT.DispX;
            int DispY = this.EVT.DispY;
            if (this.EVT.ACTIdx > 0 && DispX < this.mRc.right && DispX >= this.mRc.left && DispY < this.mRc.bottom && DispY >= this.mRc.top) {
                canvas.WriteSprite(this.EVT.ACTIdx, DispX, DispY, this.EVT.Attrib, this.EVT.Rotate, this.EVT.Scale);
            }
        }
    }

    public boolean CHKEVTACTEnd() {
        if (this.EVT.CurCNT + 1 == this.EVT.MaxCNT && this.EVT.CurFRM + 1 == this.EVT.MaxFRM) {
            return true;
        }
        return false;
    }

    public void SetEVTCtrl(int Ctrl, int Mode) {
        this.EVT.Status &= -34689;
        this.EVT.Status |= 32768 | Mode;
        this.EVT.Ctrl = Ctrl;
    }

    public void EVTCLR() {
        this.EVT.Valid = false;
    }

    public void EVTRUN() {
    }

    public void CheckEVTFun(GameCanvas canvas) {
        Rect rc = GameCanvas.GetViewRect();
        if (this.EVT.Valid && (this.EVT.Status & FIXEVT) != 8192) {
            if (this.EVT.DispX >= rc.right || this.EVT.DispX < rc.left || this.EVT.DispY >= rc.bottom || this.EVT.DispY < rc.top) {
                EVTCLR();
            }
        }
    }
}
