package oms.GameEngine;

public class GameMath {
    public static boolean CHKTouch(int SB, int SC, int SE, int TB, int TC, int TE) {
        if (SC + SE < TC - TB) {
            return false;
        }
        if (TC + TE < SC - SB) {
            return false;
        }
        return true;
    }

    public static boolean isPointInLine(int x, int y, int x1, int y1, int x2, int y2) {
        int maxY;
        int minY;
        if (y1 >= y2) {
            maxY = y1;
        } else {
            maxY = y2;
        }
        if (y1 <= y2) {
            minY = y1;
        } else {
            minY = y2;
        }
        if (y >= maxY || y < minY || x > (((x2 - x1) * (y - y1)) / (y2 - y1)) + x1) {
            return false;
        }
        return true;
    }

    public static boolean isPointInPolygon(int x, int y, int[] Polygon, int PolygonPointNum) {
        int crossNum = 0;
        for (int i = 0; i < PolygonPointNum - 1; i++) {
            if (isPointInLine(x, y, Polygon[i << 1], Polygon[(i << 1) + 1], Polygon[(i << 1) + 2], Polygon[(i << 1) + 3])) {
                crossNum++;
            }
        }
        if ((crossNum & 1) == 0) {
            return false;
        }
        return true;
    }

    public static boolean isCollapse(int[] P1, int[] P2, int P1Num, int P2Num) {
        for (int i = 0; i < P1Num - 1; i++) {
            if (isPointInPolygon(P1[i << 1], P1[(i << 1) + 1], P2, P2Num)) {
                return true;
            }
        }
        for (int i2 = 0; i2 < P2Num - 1; i2++) {
            if (isPointInPolygon(P2[i2 << 1], P2[(i2 << 1) + 1], P1, P1Num)) {
                return true;
            }
        }
        return false;
    }

    public static int convertToUIX(int x) {
        return (int) (C_Lib.mCanvasScaleX * ((float) x));
    }

    public static int convertToUIY(int y) {
        return (int) (C_Lib.mCanvasScaleY * ((float) y));
    }

    public static int convertToRealX(int x) {
        return (int) (((float) x) / C_Lib.mCanvasScaleX);
    }

    public static int convertToRealY(int y) {
        return (int) (((float) y) / C_Lib.mCanvasScaleY);
    }
}
