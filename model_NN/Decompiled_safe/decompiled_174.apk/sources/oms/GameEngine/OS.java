package oms.GameEngine;

import android.os.Build;

public class OS {
    public static int API_LEVEL = 0;

    public static void determineAPI() {
        API_LEVEL = Integer.parseInt(Build.VERSION.SDK);
        Debug.print("API LEVEL " + Build.VERSION.SDK + " int=" + API_LEVEL);
    }
}
