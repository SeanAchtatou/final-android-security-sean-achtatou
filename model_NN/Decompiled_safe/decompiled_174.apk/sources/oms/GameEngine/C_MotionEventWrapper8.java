package oms.GameEngine;

import android.view.MotionEvent;

public class C_MotionEventWrapper8 {
    public static final int ACTION_MASK = 255;
    public static final int ACTION_POINTER_DOWN = 5;
    public static final int ACTION_POINTER_INDEX_MASK = 65280;
    public static final int ACTION_POINTER_INDEX_SHIFT = 8;
    public static final int ACTION_POINTER_UP = 6;

    public static int getPointerCount(MotionEvent motionEvent) {
        return motionEvent.getPointerCount();
    }

    public static int getPointerId(MotionEvent motionEvent, int index) {
        return motionEvent.getPointerId(index);
    }

    public static float getX(MotionEvent motionEvent, int index) {
        return motionEvent.getX(index);
    }

    public static float getY(MotionEvent motionEvent, int index) {
        return motionEvent.getY(index);
    }
}
