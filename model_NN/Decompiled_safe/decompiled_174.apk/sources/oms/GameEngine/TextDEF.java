package oms.GameEngine;

import android.graphics.Bitmap;

public class TextDEF {
    public static final int DDIR = 2;
    public static final int HLOOP = 32;
    public static final int LDIR = 4;
    public static final int RDIR = 8;
    public static final int TXDepth00 = 0;
    public static final int TXDepth01 = 1;
    public static final int TXDepth02 = 2;
    public static final int TXDepth03 = 3;
    public static final int TXDepth04 = 4;
    public static final int TXDepth05 = 5;
    public static final int TXDepth06 = 6;
    public static final int TXDepth07 = 7;
    public static final int TXDepth08 = 8;
    public static final int TXDepth09 = 9;
    public static final int TXDepth0A = 10;
    public static final int TXDepth0B = 11;
    public static final int TXDepth0C = 12;
    public static final int TXDepth0D = 13;
    public static final int TXDepth0E = 14;
    public static final int TXDepth0F = 15;
    public static final int TXLayer00 = 0;
    public static final int TXLayer01 = 1;
    public static final int TXLayer02 = 2;
    public static final int TXLayer03 = 3;
    public static final int TXLayer04 = 4;
    public static final int TXLayer05 = 5;
    public static final int TXLayer06 = 6;
    public static final int TXLayer07 = 7;
    public static final int TXLayer08 = 8;
    public static final int TXLayer09 = 9;
    public static final int TXLayer0A = 10;
    public static final int TXLayer0B = 11;
    public static final int TXLayer0C = 12;
    public static final int TXLayer0D = 13;
    public static final int TXLayer0E = 14;
    public static final int TXLayer0F = 15;
    public static final int TXLayerAll = 16;
    public static final int UDIR = 1;
    public static final int VLOOP = 16;
    public float Rotate;
    public float ScaleX;
    public float ScaleY;
    public Bitmap Text;
    public int TextAttrib;
    public int TextCtrl;
    public int TextXInc;
    public int TextXVal;
    public int TextYInc;
    public int TextYVal;

    public TextDEF() {
        this.Rotate = 0.0f;
        this.ScaleX = 1.0f;
        this.ScaleY = 1.0f;
        this.Text = null;
        this.TextAttrib = 0;
        this.TextCtrl = 0;
        this.TextXVal = 0;
        this.TextYVal = 0;
        this.TextXInc = 0;
        this.TextYInc = 0;
        this.Rotate = 0.0f;
        this.ScaleX = 1.0f;
        this.ScaleY = 1.0f;
    }
}
