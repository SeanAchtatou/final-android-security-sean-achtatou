package oms.GameEngine;

import android.content.Context;
import android.media.AudioManager;

public class MediaManager {
    public static float MaxVolume = 0.0f;
    public static float SoundVolume = 0.0f;
    public static final int VOLUMEDOWN = 1;
    public static final int VOLUMEUP = 0;
    public static float VolumeInc;
    public static GameMedia _GameMedia;
    public static GameSound _GameSound;
    public static boolean mMediaEnable;
    public static boolean mSoundEnable;
    private Context mContext;

    public MediaManager(Context context) {
        this.mContext = context;
        _GameMedia = null;
        _GameSound = null;
        mSoundEnable = true;
        mMediaEnable = true;
        AudioManager mgr = (AudioManager) this.mContext.getSystemService("audio");
        SoundVolume = (float) mgr.getStreamVolume(3);
        MaxVolume = (float) mgr.getStreamMaxVolume(3);
        VolumeInc = MaxVolume / 20.0f;
    }

    public void release() {
        if (_GameMedia != null) {
            _GameMedia.release();
        }
        if (_GameSound != null) {
            _GameSound.release();
        }
    }

    public void Init(int soundNum, int mediaNum) {
        _GameMedia = new GameMedia(this.mContext, mediaNum);
        _GameMedia.setAllMediaVolume(SoundVolume / MaxVolume);
        _GameSound = new GameSound(this.mContext, soundNum);
        _GameSound.setAllSoundVolume(SoundVolume / MaxVolume);
    }

    public void SetSystemVolume(int Type) {
        if (Type == 0) {
            SoundVolume += VolumeInc;
            if (SoundVolume >= MaxVolume) {
                SoundVolume = MaxVolume;
            }
        } else {
            SoundVolume -= VolumeInc;
            if (SoundVolume <= 0.0f) {
                SoundVolume = 0.0f;
            }
        }
        setAllSoundVolume(SoundVolume);
        setAllMediaVolume(SoundVolume);
    }

    public void onPause() {
        if (_GameMedia != null) {
            _GameMedia.pause();
        }
        if (_GameSound != null) {
            _GameSound.pause();
        }
    }

    public void onResume() {
        if (_GameMedia != null && mMediaEnable) {
            _GameMedia.resume();
        }
        if (_GameSound != null && mSoundEnable) {
            _GameSound.resume();
        }
    }

    public void SetSoundEnable(boolean enable) {
        mSoundEnable = enable;
    }

    public boolean GetSoundStatus() {
        return mSoundEnable;
    }

    public void SetMediaEnable(boolean enable) {
        mMediaEnable = enable;
    }

    public boolean GetMediaStatus() {
        return mMediaEnable;
    }

    public void setRate(int streamID, float rate) {
        if (_GameSound != null) {
            _GameSound.setRate(streamID, rate);
        }
    }

    public void SetSoundStopEn(boolean status) {
        if (_GameSound != null) {
            _GameSound.SetSoundStopEn(status);
        }
    }

    public void addSound(int resid) {
        if (_GameSound != null) {
            _GameSound.addSound(resid);
        }
    }

    public void addLoopSound(int resid) {
        if (_GameSound != null) {
            _GameSound.addLoopSound(resid);
        }
    }

    public void removeSound(int resid) {
        if (_GameSound != null) {
            _GameSound.removeSound(resid);
        }
    }

    public void soundPlay(int resid) {
        if (mSoundEnable && _GameSound != null) {
            _GameSound.volume = SoundVolume / MaxVolume;
            _GameSound.play(resid);
        }
    }

    public void soundPlayLoop(int resid) {
        if (mSoundEnable && _GameSound != null) {
            _GameSound.volume = SoundVolume / MaxVolume;
            _GameSound.playLoop(resid);
        }
    }

    public void soundStop(int resid) {
        if (mSoundEnable && _GameSound != null) {
            _GameSound.stop(resid);
        }
    }

    public void soundPause(int resid) {
        if (_GameSound != null && mSoundEnable) {
            _GameSound.pause(resid);
        }
    }

    public void soundResume(int resid) {
        if (_GameSound != null && mSoundEnable) {
            _GameSound.resume(resid);
        }
    }

    public void setSoundVolume(int resid, float Volume) {
        SoundVolume = Volume;
        if (_GameSound != null) {
            _GameSound.setSoundVolume(resid, Volume);
        }
    }

    public void setAllSoundVolume(float Volume) {
        if (Volume >= MaxVolume) {
            Volume = MaxVolume;
        }
        SoundVolume = Volume;
        ((AudioManager) this.mContext.getSystemService("audio")).setStreamVolume(3, (int) Volume, 1);
        if (_GameSound != null) {
            _GameSound.setAllSoundVolume(Volume / MaxVolume);
        }
    }

    public void clearSoundPlayMap() {
    }

    public boolean mediaLoad(int StreamID) {
        if (_GameMedia != null) {
            return _GameMedia.load(StreamID);
        }
        return false;
    }

    public void setAudioStreamType(int StreamID, int streamtype) {
        if (_GameMedia != null) {
            _GameMedia.setAudioStreamType(StreamID, streamtype);
        }
    }

    public boolean isMediaPlaying(int StreamID) {
        if (!mMediaEnable) {
            return false;
        }
        if (_GameMedia != null) {
            return _GameMedia.isPlaying(StreamID);
        }
        return false;
    }

    public void setMediaLoop(int StreamID, boolean loop) {
        if (mMediaEnable && _GameMedia != null) {
            _GameMedia.setLoop(StreamID, loop);
        }
    }

    public void mediaPlay(int StreamID, boolean loop) {
        if (mMediaEnable && _GameMedia != null) {
            _GameMedia.play(StreamID, loop);
        }
    }

    public void mediaStop(int StreamID) {
        if (mMediaEnable && _GameMedia != null) {
            _GameMedia.stop(StreamID);
        }
    }

    public void mediaStopAll() {
        if (_GameMedia != null) {
            _GameMedia.stopAll();
        }
    }

    public void clearMediaMap() {
        if (_GameMedia != null) {
            _GameMedia.clearMediaMap();
        }
    }

    public void mediaPause(int StreamID) {
        if (_GameMedia != null && mMediaEnable) {
            _GameMedia.pause(StreamID);
        }
    }

    public void mediaResume(int StreamID) {
        if (_GameMedia != null && mMediaEnable) {
            _GameMedia.resume(StreamID);
        }
    }

    public void setMediaVolume(int StreamID, float Volume) {
        if (_GameMedia != null) {
            _GameMedia.setMediaVolume(StreamID, SoundVolume);
        }
    }

    public void setAllMediaVolume(float Volume) {
        if (Volume >= MaxVolume) {
            Volume = MaxVolume;
        }
        SoundVolume = Volume;
        ((AudioManager) this.mContext.getSystemService("audio")).setStreamVolume(3, (int) Volume, 1);
        if (_GameMedia != null) {
            _GameMedia.setAllMediaVolume(Volume / MaxVolume);
        }
    }

    public float getSoundVolume() {
        return SoundVolume;
    }

    public float getMediaVolume() {
        return SoundVolume;
    }

    public boolean CH_MediaIsPlaying(int Channel) {
        return _GameMedia.CH_isPlaying(Channel);
    }

    public void CH_MediaSetLoop(int Channel, boolean loop) {
        if (mMediaEnable) {
            _GameMedia.CH_SetLoop(Channel, loop);
        }
    }

    public void CH_MediaPlay(int Channel, int StreamID, boolean loop) {
        if (mMediaEnable) {
            _GameMedia.CH_Play(Channel, StreamID, loop);
        }
    }

    public void CH_MediaStop(int Channel) {
        if (mMediaEnable) {
            _GameMedia.CH_Stop(Channel);
        }
    }

    public void CH_MediaPause(int Channel) {
        if (mMediaEnable) {
            _GameMedia.CH_Pause(Channel);
        }
    }

    public void CH_MediaResume(int Channel) {
        if (mMediaEnable) {
            _GameMedia.CH_Resume(Channel);
        }
    }

    public void CH_MediaRelease(int Channel) {
        _GameMedia.CH_Release(Channel);
    }

    public void CH_SoundPlay(int Channel, int Resid) {
        if (mSoundEnable) {
            _GameSound.volume = SoundVolume / MaxVolume;
            _GameSound.CH_Play(Channel, Resid);
        }
    }

    public void CH_SoundPlayLoop(int Channel, int Resid) {
        if (mSoundEnable) {
            _GameSound.CH_PlayLoop(Channel, Resid);
        }
    }

    public void CH_SoundStop(int Channel) {
        if (mSoundEnable) {
            _GameSound.CH_Stop(Channel);
        }
    }

    public void CH_SoundPause(int Channel) {
        if (mSoundEnable) {
            _GameSound.CH_Pause(Channel);
        }
    }

    public void CH_SoundResume(int Channel) {
        if (mSoundEnable) {
            _GameSound.CH_Resume(Channel);
        }
    }

    public void CH_SoundSetRate(int Channel, float Rate) {
        if (mSoundEnable) {
            _GameSound.CH_SetRate(Channel, Rate);
        }
    }

    public int CH_GetPlayedTime(int Channel) {
        return _GameSound.CH_GetPlayedTime(Channel);
    }

    public int GetPlayedTime(int resID) {
        return _GameSound.GetPlayedTime(resID);
    }

    public void RemoveAll() {
        if (_GameMedia != null) {
            _GameMedia.stopAll();
        }
        if (_GameSound != null) {
            _GameSound.removeAll();
        }
    }
}
