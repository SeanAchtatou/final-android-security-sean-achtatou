package oms.GameEngine;

public class InputInterface {
    private KeyBoard cKeyBoard = new KeyBoard();
    private TouchPannel cTouchPannel = new TouchPannel();
    public boolean nIsPause = false;
    public int nScreenXOff = 0;
    public int nScreenYOff = 0;

    public void onKeyDown(int keyCode) {
        if (!this.nIsPause) {
            this.cKeyBoard.onKeyDown(keyCode);
        }
    }

    public void onKeyUp(int keyCode) {
        if (!this.nIsPause) {
            this.cKeyBoard.onKeyUp(keyCode);
        }
    }

    public void ReadKeyBoard() {
        this.cKeyBoard.ReadKeyBoard();
    }

    public boolean CHKSingleKey(int keyCode) {
        return this.cKeyBoard.CHKSingleKey(keyCode);
    }

    public boolean CHKSteadyKey(int keyCode) {
        return this.cKeyBoard.CHKSteadyKey(keyCode);
    }

    public boolean CHKUpKey(int keyCode) {
        return this.cKeyBoard.CHKUpKey(keyCode);
    }

    public void ClearKeyValue() {
        this.cKeyBoard.ClearKeyValue();
    }

    public void SetScreenOffset(int XOff, int YOff) {
        this.nScreenXOff = XOff;
        this.nScreenYOff = YOff;
    }

    public void SetTouchEn(boolean touch) {
        this.cTouchPannel.mTouchEn = touch;
    }

    public boolean GetTouchEn() {
        return this.cTouchPannel.mTouchEn;
    }

    public void SetTouchDown(int X, int Y) {
        this.cTouchPannel.SetTouchDown(GameMath.convertToRealX(X - this.nScreenXOff), GameMath.convertToRealY(Y - this.nScreenYOff));
    }

    public void SetTouchMove(int X, int Y) {
        this.cTouchPannel.SetTouchMove(GameMath.convertToRealX(X - this.nScreenXOff), GameMath.convertToRealY(Y - this.nScreenYOff));
    }

    public void SetTouchUp(int X, int Y) {
        this.cTouchPannel.SetTouchUp(GameMath.convertToRealX(X - this.nScreenXOff), GameMath.convertToRealY(Y - this.nScreenYOff));
    }

    public void ReadTouch() {
        this.cTouchPannel.ReadTouch();
    }

    public boolean CHKIsTouch() {
        return this.cTouchPannel.CHKIsTouch();
    }

    public boolean CHKTouchDown() {
        return this.cTouchPannel.CHKTouchDown();
    }

    public int GetTouchDownX() {
        return this.cTouchPannel.GetTouchDownX();
    }

    public int GetTouchDownY() {
        return this.cTouchPannel.GetTouchDownY();
    }

    public boolean CHKTouchMove() {
        return this.cTouchPannel.CHKTouchMove();
    }

    public int GetTouchMoveX() {
        return this.cTouchPannel.GetTouchMoveX();
    }

    public int GetTouchMoveY() {
        return this.cTouchPannel.GetTouchMoveY();
    }

    public int GetTouchMoveCount() {
        return this.cTouchPannel.GetTouchMoveCount();
    }

    public int GetTouchMoveX(int Count) {
        return this.cTouchPannel.GetTouchMoveX(Count);
    }

    public int GetTouchMoveY(int Count) {
        return this.cTouchPannel.GetTouchMoveY(Count);
    }

    public boolean CHKTouchUp() {
        return this.cTouchPannel.CHKTouchUp();
    }

    public int GetTouchUpX() {
        return this.cTouchPannel.GetTouchUpX();
    }

    public int GetTouchUpY() {
        return this.cTouchPannel.GetTouchUpY();
    }

    public void ClearTouchDown() {
        this.cTouchPannel.ClearTouchDown();
    }

    public void ClearTouchMove() {
        this.cTouchPannel.ClearTouchMove();
    }

    public void ClearTouchUp() {
        this.cTouchPannel.ClearTouchUp();
    }

    public int GetTouchDownCount() {
        return this.cTouchPannel.GetTouchDownCount();
    }

    public int GetTouchDownX(int Count) {
        return this.cTouchPannel.GetTouchDownX();
    }

    public int GetTouchDownY(int Count) {
        return this.cTouchPannel.GetTouchDownY();
    }

    public int GetTouchUpCount() {
        return this.cTouchPannel.GetTouchUpCount();
    }

    public int GetTouchUpX(int Count) {
        return this.cTouchPannel.GetTouchUpX();
    }

    public int GetTouchUpY(int Count) {
        return this.cTouchPannel.GetTouchUpY();
    }
}
