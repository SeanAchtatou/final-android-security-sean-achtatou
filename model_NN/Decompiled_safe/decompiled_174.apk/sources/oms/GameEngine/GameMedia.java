package oms.GameEngine;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import java.io.IOException;
import java.util.HashMap;

public class GameMedia {
    public static final int PLAY_LOOP = 1;
    public static final int PLAY_ONCE = 0;
    private int SOUNDCHANNEL = 0;
    private HashMap<Integer, Integer> gameMediaMap;
    private int loadStreamNum;
    private Context mContext;
    private int[] mPlayChannel;
    private int[] mSavePlayPos;
    private int maxStream;
    private MediaPlayer[] mediaPlayer;
    public float volume;

    public GameMedia(Context context, int StreamNum) {
        this.mContext = context;
        this.maxStream = StreamNum;
        this.gameMediaMap = new HashMap<>();
        this.mediaPlayer = new MediaPlayer[(this.maxStream + this.SOUNDCHANNEL)];
        this.mPlayChannel = new int[(this.maxStream + this.SOUNDCHANNEL)];
        this.mSavePlayPos = new int[(this.maxStream + this.SOUNDCHANNEL)];
        for (int i = 0; i < this.maxStream + this.SOUNDCHANNEL; i++) {
            this.mediaPlayer[i] = new MediaPlayer();
            this.mSavePlayPos[i] = -1;
            this.mPlayChannel[i] = -1;
        }
        this.loadStreamNum = 0;
        this.volume = 1.0f;
    }

    public boolean load(int StreamID) {
        if (this.loadStreamNum >= this.maxStream) {
            return false;
        }
        AssetFileDescriptor afd = this.mContext.getResources().openRawResourceFd(StreamID);
        if (afd == null) {
            return false;
        }
        try {
            this.mediaPlayer[this.loadStreamNum].setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        try {
            afd.close();
        } catch (IOException e4) {
            e4.printStackTrace();
        }
        try {
            this.mediaPlayer[this.loadStreamNum].prepare();
        } catch (IllegalStateException e5) {
            e5.printStackTrace();
        } catch (IOException e6) {
            e6.printStackTrace();
        }
        this.gameMediaMap.put(Integer.valueOf(StreamID), Integer.valueOf(this.loadStreamNum + 1));
        this.loadStreamNum++;
        return true;
    }

    public void setAudioStreamType(int StreamID, int streamtype) {
        int ID = this.gameMediaMap.get(Integer.valueOf(StreamID)).intValue();
        if (ID != 0) {
            this.mediaPlayer[ID - 1].setAudioStreamType(streamtype);
        }
    }

    public boolean isPlaying(int StreamID) {
        if (this.gameMediaMap.isEmpty()) {
            return false;
        }
        int ID = this.gameMediaMap.get(Integer.valueOf(StreamID)).intValue();
        if (ID >= this.maxStream) {
            return false;
        }
        if (ID != 0) {
            return this.mediaPlayer[ID - 1].isPlaying();
        }
        return false;
    }

    public void setLoop(int StreamID, boolean loop) {
        int ID;
        if (!this.gameMediaMap.isEmpty() && (ID = this.gameMediaMap.get(Integer.valueOf(StreamID)).intValue()) < this.maxStream && ID != 0) {
            this.mediaPlayer[ID - 1].setLooping(loop);
        }
    }

    public boolean play(int StreamID, boolean loop) {
        load(StreamID);
        if (this.gameMediaMap.isEmpty()) {
            return false;
        }
        int ID = this.gameMediaMap.get(Integer.valueOf(StreamID)).intValue();
        if (ID >= this.maxStream) {
            return false;
        }
        if (ID == 0) {
            return false;
        }
        this.mediaPlayer[ID - 1].seekTo(0);
        this.mediaPlayer[ID - 1].setLooping(loop);
        this.mediaPlayer[ID - 1].start();
        this.mediaPlayer[ID - 1].setVolume(this.volume, this.volume);
        return true;
    }

    public void stop(int StreamID) {
        int ID;
        if (!this.gameMediaMap.isEmpty() && (ID = this.gameMediaMap.get(Integer.valueOf(StreamID)).intValue()) < this.maxStream) {
            if (ID != 0) {
                this.mediaPlayer[ID - 1].stop();
                this.mSavePlayPos[ID - 1] = -1;
                this.mediaPlayer[ID - 1].release();
                this.mediaPlayer[ID - 1] = new MediaPlayer();
                this.loadStreamNum--;
            }
            this.gameMediaMap.remove(Integer.valueOf(StreamID));
        }
    }

    public void stopAll() {
        if (!this.gameMediaMap.isEmpty()) {
            int Count = this.gameMediaMap.size();
            for (int i = 0; i < Count; i++) {
                this.mediaPlayer[i].stop();
                this.mediaPlayer[i].release();
                this.mediaPlayer[i] = new MediaPlayer();
                this.mSavePlayPos[i] = -1;
            }
            clearMediaMap();
        }
    }

    public void clearMediaMap() {
        this.gameMediaMap.clear();
        this.loadStreamNum = 0;
    }

    public void pause() {
        int Count = this.gameMediaMap.size();
        for (int i = 0; i < Count; i++) {
            if (this.mediaPlayer[i] != null && this.mediaPlayer[i].isPlaying()) {
                this.mediaPlayer[i].pause();
                this.mSavePlayPos[i] = this.mediaPlayer[i].getCurrentPosition();
            }
        }
    }

    public void resume() {
        int Count = this.gameMediaMap.size();
        for (int i = 0; i < Count; i++) {
            if (!(this.mediaPlayer[i] == null || this.mSavePlayPos[i] == -1)) {
                this.mSavePlayPos[i] = -1;
                this.mediaPlayer[i].start();
            }
        }
    }

    public void pause(int StreamID) {
        int ID;
        if (!this.gameMediaMap.isEmpty() && (ID = this.gameMediaMap.get(Integer.valueOf(StreamID)).intValue()) != 0) {
            this.mediaPlayer[ID - 1].pause();
            this.mSavePlayPos[ID - 1] = this.mediaPlayer[ID - 1].getCurrentPosition();
        }
    }

    public void resume(int StreamID) {
        int ID;
        if (!this.gameMediaMap.isEmpty() && (ID = this.gameMediaMap.get(Integer.valueOf(StreamID)).intValue()) != 0) {
            this.mediaPlayer[ID - 1].start();
            if (this.mSavePlayPos[ID - 1] != -1) {
                this.mSavePlayPos[ID - 1] = -1;
                this.mediaPlayer[ID - 1].start();
            }
        }
    }

    public boolean CH_isPlaying(int Channel) {
        if (Channel >= this.maxStream) {
            return false;
        }
        if (this.mPlayChannel[Channel] != -1) {
            return isPlaying(this.mPlayChannel[Channel]);
        }
        return false;
    }

    public void CH_SetLoop(int Channel, boolean loop) {
        if (Channel < this.maxStream && this.mPlayChannel[Channel] != -1) {
            setLoop(this.mPlayChannel[Channel], loop);
        }
    }

    public void CH_Play(int Channel, int StreamID, boolean loop) {
        if (Channel < this.maxStream) {
            CH_Stop(Channel);
            if (play(StreamID, loop)) {
                this.mPlayChannel[Channel] = StreamID;
            }
        }
    }

    public void CH_Stop(int Channel) {
        if (Channel < this.maxStream && this.mPlayChannel[Channel] != -1) {
            stop(this.mPlayChannel[Channel]);
            this.mPlayChannel[Channel] = -1;
        }
    }

    public void CH_Pause(int Channel) {
        if (Channel < this.maxStream && this.mPlayChannel[Channel] != -1) {
            pause(this.mPlayChannel[Channel]);
        }
    }

    public void CH_Resume(int Channel) {
        if (Channel < this.maxStream && this.mPlayChannel[Channel] != -1) {
            resume(this.mPlayChannel[Channel]);
        }
    }

    public void CH_Release(int Channel) {
        if (Channel < this.maxStream && this.mPlayChannel[Channel] != -1) {
            release(this.mPlayChannel[Channel]);
            this.mPlayChannel[Channel] = -1;
        }
    }

    public void release(int StreamID) {
        int ID;
        if (!this.gameMediaMap.isEmpty() && (ID = this.gameMediaMap.get(Integer.valueOf(StreamID)).intValue()) != 0) {
            this.mediaPlayer[ID - 1].stop();
            this.mediaPlayer[ID - 1].release();
            this.gameMediaMap.remove(Integer.valueOf(StreamID));
            if (this.loadStreamNum > 0) {
                this.loadStreamNum--;
            }
        }
    }

    public void release() {
        for (int i = 0; i < this.maxStream + this.SOUNDCHANNEL; i++) {
            this.mediaPlayer[i].stop();
            this.mediaPlayer[i].release();
            this.mPlayChannel[i] = -1;
        }
        this.gameMediaMap.clear();
        this.loadStreamNum = 0;
    }

    public void setMediaVolume(int StreamID, float Volume) {
        this.volume = 1.0f;
    }

    public void setAllMediaVolume(float Volume) {
        this.volume = 1.0f;
    }

    public float getVolume() {
        return this.volume;
    }
}
