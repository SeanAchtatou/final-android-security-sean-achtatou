package oms.GameEngine;

public class C_MSG {
    public static final int ON_COMMAND = 0;
    public static final int ON_WM = 1;
    private int HWND = 0;
    private int nCursorX = 0;
    private int nCursorY = 0;
    private int nMessage = 0;
    private long nTime = 0;
    private String sMessage;
    private int wParam;

    public void SetMessage(int hWnd, int message, int param) {
        this.HWND = hWnd;
        this.nMessage = message;
        this.wParam = param;
    }

    public void SetMessage(int hWnd, String message, int param, int time, int cursorX, int cursorY) {
        this.HWND = hWnd;
        this.sMessage = message;
        this.wParam = param;
        this.nTime = (long) time;
        this.nCursorX = cursorX;
        this.nCursorY = cursorY;
    }

    public void SetMessage(int hWnd, int message, int param, int time, int cursorX, int cursorY) {
        this.HWND = hWnd;
        this.nMessage = message;
        this.wParam = param;
        this.nTime = (long) time;
        this.nCursorX = cursorX;
        this.nCursorY = cursorY;
    }

    public void SetMessage(int hWnd, String message, int param) {
        this.HWND = hWnd;
        this.sMessage = message;
        this.wParam = param;
    }

    public int GetMsgHWnd() {
        return this.HWND;
    }

    public int GetMsgMessage() {
        return this.nMessage;
    }

    public String GetSMsgMessage() {
        return this.sMessage;
    }

    public int GetwParam() {
        return this.wParam;
    }

    public long GetMsgTime() {
        return this.nTime;
    }

    public int GetCursorX() {
        return this.nCursorX;
    }

    public int GetCursorY() {
        return this.nCursorY;
    }
}
