package oms.GameEngine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {
    private static final boolean FOCUSABLE = false;
    private static final boolean THREADEN = true;
    private C_Lib cLib;
    /* access modifiers changed from: private */
    public final PaintFlagsDrawFilter drawFilter = new PaintFlagsDrawFilter(0, 3);
    private SurfaceHolder mHolder = getHolder();
    private int mThreadPriority;
    private RanderThread thread = null;

    public class RanderThread extends Thread {
        private C_Lib cLib;
        private Handler mHandler;
        private boolean mRun = GameView.FOCUSABLE;
        private SurfaceHolder mSurfaceHolder;
        public boolean mThreadEnd = GameView.FOCUSABLE;

        public RanderThread(SurfaceHolder mSurfaceHolder2, Handler handler) {
            this.mSurfaceHolder = mSurfaceHolder2;
            this.mHandler = handler;
            mSurfaceHolder2.setType(2);
            this.cLib = null;
            this.mThreadEnd = GameView.FOCUSABLE;
        }

        public void SetDraw(C_Lib c_lib) {
            this.cLib = c_lib;
        }

        public void run() {
            boolean randerMode;
            while (this.cLib == null) {
                try {
                    Thread.sleep(0, 500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            int rectHeight = (this.cLib.nRefreshHeight - 480) / 2;
            if (C_Lib.mCanvasScaleX == C_Lib.mCanvasScaleY && C_Lib.mCanvasScaleX == 1.0f) {
                randerMode = GameView.THREADEN;
            } else {
                randerMode = GameView.FOCUSABLE;
            }
            Bitmap canvasBMP = null;
            Canvas canvas = null;
            Canvas c = null;
            if (!randerMode) {
                canvasBMP = Bitmap.createBitmap(this.cLib.nRefreshWidth, this.cLib.nRefreshHeight, Bitmap.Config.ARGB_8888);
                canvas = new Canvas();
                canvas.setBitmap(canvasBMP);
            }
            while (this.mRun) {
                long frameTick = System.currentTimeMillis();
                while (this.mRun) {
                    if (this.cLib.isReflash()) {
                        long randerStart = System.currentTimeMillis();
                        try {
                            c = this.mSurfaceHolder.lockCanvas(null);
                            if (!randerMode) {
                                if (c != null) {
                                    c.setDrawFilter(GameView.this.drawFilter);
                                    c.scale(C_Lib.mCanvasScaleX, C_Lib.mCanvasScaleY);
                                }
                                canvas.setDrawFilter(GameView.this.drawFilter);
                                if (rectHeight > 0) {
                                    this.cLib.onDraw(this.cLib, canvas, rectHeight);
                                } else {
                                    this.cLib.OnDraw(canvas);
                                }
                                if (c != null) {
                                    c.drawBitmap(canvasBMP, 0.0f, 0.0f, (Paint) null);
                                }
                            } else {
                                if (c != null) {
                                    c.setDrawFilter(GameView.this.drawFilter);
                                }
                                if (rectHeight > 0) {
                                    if (c != null) {
                                        this.cLib.onDraw(this.cLib, c, rectHeight);
                                    }
                                } else if (c != null) {
                                    this.cLib.OnDraw(c);
                                }
                            }
                            this.cLib.nFPS = (int) (System.currentTimeMillis() - randerStart);
                            long frameElapse = System.currentTimeMillis() - frameTick;
                            if (frameElapse < ((long) this.cLib.getFrameReflashTime())) {
                                try {
                                    sleep((((long) this.cLib.getFrameReflashTime()) - frameElapse) - 1);
                                } catch (InterruptedException e2) {
                                    e2.printStackTrace();
                                }
                            }
                        } finally {
                            if (c != null) {
                                this.mSurfaceHolder.unlockCanvasAndPost(c);
                            }
                        }
                    } else {
                        try {
                            Thread.sleep((long) (this.cLib.getFrameReflashTime() >> 2));
                        } catch (InterruptedException e3) {
                            InterruptedException e4 = e3;
                            e4.printStackTrace();
                            Log.v("Test", e4.toString());
                        }
                    }
                }
                return;
            }
            if (canvasBMP != null) {
                canvasBMP.recycle();
            }
            this.mThreadEnd = GameView.THREADEN;
        }

        public void setRunning(boolean b) {
            this.mRun = b;
        }

        public boolean ismRun() {
            return this.mRun;
        }

        public void setState(int mode) {
            Message msg = this.mHandler.obtainMessage();
            Bundle b = new Bundle();
            b.putString("text", "VISIBLE");
            b.putInt("viz", 0);
            msg.setData(b);
            this.mHandler.sendMessage(msg);
        }
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mHolder.addCallback(this);
        setFocusable((boolean) FOCUSABLE);
        this.mThreadPriority = 10;
    }

    public GameView(Context context) {
        super(context);
        this.mHolder.addCallback(this);
        setFocusable((boolean) FOCUSABLE);
        this.mThreadPriority = 10;
    }

    public void SetThreadPriority(int priority) {
        this.mThreadPriority = priority;
    }

    private void CreateThread() {
        this.thread = new RanderThread(this.mHolder, new Handler() {
            public void handleMessage(Message m) {
            }
        });
    }

    public void SetDraw(C_Lib clib) {
        this.cLib = clib;
        this.cLib.SetGameView(this);
        if (this.thread != null) {
            this.thread.SetDraw(clib);
        }
    }

    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
        synchronized (this) {
            if (this.thread != null) {
                this.thread.setRunning(THREADEN);
                this.thread.setPriority(this.mThreadPriority);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);
    }

    public void surfaceCreated(SurfaceHolder arg0) {
        synchronized (this) {
            if (this.thread == null) {
                CreateThread();
            }
            this.thread.SetDraw(this.cLib);
            this.thread.setRunning(THREADEN);
            this.thread.start();
            setFocusable((boolean) FOCUSABLE);
        }
    }

    public void onPause() {
        if (this.thread != null) {
            this.thread.setRunning(FOCUSABLE);
        }
    }

    public void onResume() {
        if (this.thread != null) {
            this.thread.setRunning(THREADEN);
            if (this.thread.isAlive()) {
                this.thread.resume();
                return;
            }
            try {
                this.thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.thread = null;
            if (this.thread == null) {
                CreateThread();
            }
            this.thread.SetDraw(this.cLib);
            this.thread.setRunning(THREADEN);
            this.thread.setPriority(this.mThreadPriority);
            this.thread.start();
            setFocusable((boolean) FOCUSABLE);
        }
    }

    public void onDestory() {
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
        boolean retry = THREADEN;
        this.thread.setRunning(FOCUSABLE);
        while (retry) {
            try {
                this.thread.join();
                retry = FOCUSABLE;
            } catch (InterruptedException e) {
            }
        }
        this.thread = null;
    }
}
