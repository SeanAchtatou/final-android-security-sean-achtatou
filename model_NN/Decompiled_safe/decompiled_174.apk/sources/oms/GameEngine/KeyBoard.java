package oms.GameEngine;

public class KeyBoard {
    public static final int KEYMAX = 92;
    public boolean[] CurUpKey = new boolean[92];
    public boolean[] DownKey = new boolean[92];
    public boolean[] SingleKey = new boolean[92];
    public boolean[] SteadyKey = new boolean[92];
    public boolean[] UpKey = new boolean[92];

    public void onKeyDown(int keyCode) {
        if (keyCode >= 0 && keyCode < 92) {
            this.DownKey[keyCode] = true;
        }
    }

    public void onKeyUp(int keyCode) {
        if (keyCode >= 0 && keyCode < 92) {
            this.DownKey[keyCode] = false;
            this.CurUpKey[keyCode] = true;
        }
    }

    public void ClearKeyValue() {
        for (int i = 0; i < 92; i++) {
            this.SingleKey[i] = false;
            this.SteadyKey[i] = false;
            this.UpKey[i] = false;
            this.CurUpKey[i] = false;
        }
    }

    public void ReadKeyBoard() {
        for (int i = 0; i < 92; i++) {
            if (!this.DownKey[i] || this.SteadyKey[i]) {
                this.SingleKey[i] = false;
            } else {
                this.SingleKey[i] = true;
            }
            this.SteadyKey[i] = this.DownKey[i];
            this.UpKey[i] = this.CurUpKey[i];
            this.CurUpKey[i] = false;
        }
    }

    public boolean CHKSingleKey(int keyCode) {
        return this.SingleKey[keyCode];
    }

    public boolean CHKSteadyKey(int keyCode) {
        return this.SteadyKey[keyCode];
    }

    public boolean CHKUpKey(int keyCode) {
        return this.UpKey[keyCode];
    }
}
