package oms.GameEngine;

import android.view.MotionEvent;
import java.lang.reflect.Array;

public class C_MultiTouch {
    public static final int MULTIMOVEMAX = 10;
    public static final int MULTITOUCHMAX = 4;
    public static int nScreenXOff;
    public static int nScreenYOff;
    public int mTouchDownReadCount;
    public int[] mTouchDownReadId;
    public int[] mTouchDownReadXVal;
    public int[] mTouchDownReadYVal;
    public int mTouchDownWriteCount;
    public int[] mTouchDownWriteId;
    public int[] mTouchDownWriteXVal;
    public int[] mTouchDownWriteYVal;
    public boolean mTouchMove;
    public int[] mTouchMoveReadCount;
    public int[] mTouchMoveReadId;
    public int[][] mTouchMoveReadXVal;
    public int[][] mTouchMoveReadYVal;
    public int[] mTouchMoveWriteCount;
    public int[] mTouchMoveWriteId;
    public int[][] mTouchMoveWriteXVal;
    public int[][] mTouchMoveWriteYVal;
    public int mTouchUpReadCount;
    public int[] mTouchUpReadId;
    public int[] mTouchUpReadXVal;
    public int[] mTouchUpReadYVal;
    public int mTouchUpWriteCount;
    public int[] mTouchUpWriteId;
    public int[] mTouchUpWriteXVal;
    public int[] mTouchUpWriteYVal;

    public C_MultiTouch() {
        OS.determineAPI();
        initTouch();
        SetScreenOffset(0, 0);
    }

    public static void SetScreenOffset(int XOff, int YOff) {
        nScreenXOff = XOff;
        nScreenYOff = YOff;
    }

    /* access modifiers changed from: protected */
    public void handleSDK8MultiTouch(MotionEvent event) {
        int pointerCount = C_MotionEventWrapper8.getPointerCount(event);
        int action = event.getAction();
        for (int idx = 0; idx < pointerCount; idx++) {
            int id = C_MotionEventWrapper8.getPointerId(event, idx);
            float realX = C_MotionEventWrapper8.getX(event, idx) - ((float) nScreenXOff);
            float realY = C_MotionEventWrapper8.getY(event, idx) - ((float) nScreenYOff);
            switch (action & C_MotionEventWrapper8.ACTION_MASK) {
                case 0:
                    onTouchDown(realX, realY, pointerCount, id);
                    break;
                case 1:
                    onTouchUp(realX, realY, pointerCount, id);
                    break;
                case 2:
                    onTouchMove(realX, realY, pointerCount, id);
                    break;
                case 5:
                    if (((action & C_MotionEventWrapper8.ACTION_POINTER_INDEX_MASK) >> 8) != idx) {
                        break;
                    } else {
                        onTouchDown(realX, realY, pointerCount, id);
                        break;
                    }
                case 6:
                    if (((action & C_MotionEventWrapper8.ACTION_POINTER_INDEX_MASK) >> 8) != idx) {
                        break;
                    } else {
                        onTouchUp(realX, realY, pointerCount, id);
                        break;
                    }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void handleMultiTouch(MotionEvent event) {
        int pointerCount = C_MotionEventWrapper5.getPointerCount(event);
        int action = event.getAction();
        for (int idx = 0; idx < pointerCount; idx++) {
            int id = C_MotionEventWrapper5.getPointerId(event, idx);
            float realX = C_MotionEventWrapper5.getX(event, idx) - ((float) nScreenXOff);
            float realY = C_MotionEventWrapper5.getY(event, idx) - ((float) nScreenYOff);
            switch (action) {
                case 0:
                    onTouchDown(realX, realY, pointerCount, id);
                    break;
                case 1:
                    onTouchUp(realX, realY, pointerCount, id);
                    break;
                case 2:
                    onTouchMove(realX, realY, pointerCount, id);
                    break;
                case 5:
                    if (idx != 0) {
                        break;
                    } else {
                        onTouchDown(realX, realY, pointerCount, id);
                        break;
                    }
                case 6:
                    if (idx != 0) {
                        break;
                    } else {
                        onTouchUp(realX, realY, pointerCount, id);
                        break;
                    }
                case C_MotionEventWrapper5.ACTION_POINTER_2_DOWN:
                    if (idx != 1) {
                        break;
                    } else {
                        onTouchDown(realX, realY, pointerCount, id);
                        break;
                    }
                case C_MotionEventWrapper5.ACTION_POINTER_2_UP:
                    if (idx != 1) {
                        break;
                    } else {
                        onTouchUp(realX, realY, pointerCount, id);
                        break;
                    }
                case C_MotionEventWrapper5.ACTION_POINTER_3_DOWN:
                    if (idx != 2) {
                        break;
                    } else {
                        onTouchDown(realX, realY, pointerCount, id);
                        break;
                    }
                case C_MotionEventWrapper5.ACTION_POINTER_3_UP:
                    if (idx != 2) {
                        break;
                    } else {
                        onTouchUp(realX, realY, pointerCount, id);
                        break;
                    }
            }
        }
    }

    public void handleTouch(MotionEvent event) {
        if (OS.API_LEVEL < 5) {
            int action = event.getAction();
            float realX = event.getX() - ((float) nScreenXOff);
            float realY = event.getY() - ((float) nScreenYOff);
            switch (action) {
                case 0:
                    onTouchDown(realX, realY, 1, 0);
                    return;
                case 1:
                    onTouchUp(realX, realY, 1, 0);
                    return;
                case 2:
                    onTouchMove(realX, realY, 1, 0);
                    return;
                default:
                    return;
            }
        } else if (OS.API_LEVEL >= 8) {
            handleSDK8MultiTouch(event);
        } else {
            handleMultiTouch(event);
        }
    }

    private void initTouch() {
        this.mTouchDownWriteCount = 0;
        this.mTouchDownWriteId = new int[4];
        this.mTouchDownWriteXVal = new int[4];
        this.mTouchDownWriteYVal = new int[4];
        this.mTouchDownReadCount = 0;
        this.mTouchDownReadId = new int[4];
        this.mTouchDownReadXVal = new int[4];
        this.mTouchDownReadYVal = new int[4];
        this.mTouchUpWriteCount = 0;
        this.mTouchUpWriteId = new int[4];
        this.mTouchUpWriteXVal = new int[4];
        this.mTouchUpWriteYVal = new int[4];
        this.mTouchUpReadCount = 0;
        this.mTouchUpReadId = new int[4];
        this.mTouchUpReadXVal = new int[4];
        this.mTouchUpReadYVal = new int[4];
        this.mTouchMoveWriteId = new int[4];
        this.mTouchMoveWriteCount = new int[4];
        this.mTouchMoveWriteXVal = (int[][]) Array.newInstance(Integer.TYPE, 4, 10);
        this.mTouchMoveWriteYVal = (int[][]) Array.newInstance(Integer.TYPE, 4, 10);
        this.mTouchMoveReadId = new int[4];
        this.mTouchMoveReadCount = new int[4];
        this.mTouchMoveReadXVal = (int[][]) Array.newInstance(Integer.TYPE, 4, 10);
        this.mTouchMoveReadYVal = (int[][]) Array.newInstance(Integer.TYPE, 4, 10);
        this.mTouchMove = false;
        for (int i = 0; i < 4; i++) {
            this.mTouchDownWriteId[i] = 0;
            this.mTouchDownWriteXVal[i] = 0;
            this.mTouchDownWriteYVal[i] = 0;
            this.mTouchDownReadId[i] = -1;
            this.mTouchDownReadXVal[i] = 0;
            this.mTouchDownReadYVal[i] = 0;
            this.mTouchUpWriteId[i] = -1;
            this.mTouchUpWriteXVal[i] = 0;
            this.mTouchUpWriteYVal[i] = 0;
            this.mTouchUpReadId[i] = 0;
            this.mTouchUpReadXVal[i] = 0;
            this.mTouchUpReadYVal[i] = 0;
            this.mTouchMoveWriteId[i] = 0;
            this.mTouchMoveWriteCount[i] = 0;
            this.mTouchMoveReadId[i] = 0;
            this.mTouchMoveReadCount[i] = 0;
            for (int j = 0; j < 10; j++) {
                this.mTouchMoveWriteXVal[i][j] = 0;
                this.mTouchMoveWriteYVal[i][j] = 0;
                this.mTouchMoveReadXVal[i][j] = 0;
                this.mTouchMoveReadYVal[i][j] = 0;
            }
        }
    }

    public void ReadTouch() {
        this.mTouchDownReadCount = 0;
        for (int i = 0; i < 4; i++) {
            if (this.mTouchDownWriteId[i] != -1) {
                this.mTouchDownReadCount++;
            }
            this.mTouchDownReadId[i] = this.mTouchDownWriteId[i];
            this.mTouchDownReadXVal[i] = this.mTouchDownWriteXVal[i];
            this.mTouchDownReadYVal[i] = this.mTouchDownWriteYVal[i];
            this.mTouchDownWriteId[i] = -1;
        }
        this.mTouchUpReadCount = 0;
        for (int i2 = 0; i2 < 4; i2++) {
            if (this.mTouchUpWriteId[i2] != -1) {
                this.mTouchUpReadCount++;
            }
            this.mTouchUpReadId[i2] = this.mTouchUpWriteId[i2];
            this.mTouchUpReadXVal[i2] = this.mTouchUpWriteXVal[i2];
            this.mTouchUpReadYVal[i2] = this.mTouchUpWriteYVal[i2];
            this.mTouchUpWriteId[i2] = -1;
        }
        this.mTouchMove = false;
        for (int i3 = 0; i3 < 4; i3++) {
            if (this.mTouchMoveWriteId[i3] != -1) {
                this.mTouchMove = true;
            }
            this.mTouchMoveReadId[i3] = this.mTouchMoveWriteId[i3];
            if (this.mTouchMoveWriteId[i3] != -1) {
                this.mTouchMove = true;
            }
            for (int j = 0; j < 10; j++) {
                this.mTouchMoveReadXVal[i3][j] = this.mTouchMoveWriteXVal[i3][j];
                this.mTouchMoveReadYVal[i3][j] = this.mTouchMoveWriteYVal[i3][j];
            }
            this.mTouchMoveReadCount[i3] = this.mTouchMoveWriteCount[i3];
            this.mTouchMoveWriteCount[i3] = 0;
            this.mTouchMoveWriteId[i3] = -1;
        }
    }

    private void onTouchDown(float x, float y, int pointerCount, int pointerId) {
        if (pointerId < 4) {
            this.mTouchDownWriteId[pointerId] = pointerId;
            this.mTouchDownWriteXVal[pointerId] = GameMath.convertToRealX((int) x);
            this.mTouchDownWriteYVal[pointerId] = GameMath.convertToRealY((int) y);
        }
    }

    public int getTouchDownCount() {
        return this.mTouchDownReadCount;
    }

    public boolean CHKTouchDown() {
        if (this.mTouchDownReadCount > 0) {
            return true;
        }
        return false;
    }

    public int getTouchDownId(int pointerId) {
        if (pointerId < 4) {
            return this.mTouchDownReadId[pointerId];
        }
        return 0;
    }

    public int getTouchDownX(int pointerId) {
        if (pointerId < 4) {
            return this.mTouchDownReadXVal[pointerId];
        }
        return 0;
    }

    public int getTouchDownY(int pointerId) {
        if (pointerId < 4) {
            return this.mTouchDownReadYVal[pointerId];
        }
        return 0;
    }

    private void onTouchUp(float x, float y, int pointerCount, int pointerId) {
        if (pointerId < 4) {
            this.mTouchUpWriteId[pointerId] = pointerId;
            this.mTouchUpWriteXVal[pointerId] = GameMath.convertToRealX((int) x);
            this.mTouchUpWriteYVal[pointerId] = GameMath.convertToRealY((int) y);
        }
    }

    public int getTouchUpCount() {
        return this.mTouchUpReadCount;
    }

    public boolean CHKTouchUp() {
        if (this.mTouchUpReadCount > 0) {
            return true;
        }
        return false;
    }

    public int getTouchUpId(int pointerId) {
        if (pointerId < 4) {
            return this.mTouchUpReadId[pointerId];
        }
        return 0;
    }

    public int getTouchUpX(int pointerId) {
        if (pointerId < 4) {
            return this.mTouchUpReadXVal[pointerId];
        }
        return 0;
    }

    public int getTouchUpY(int pointerId) {
        if (pointerId < 4) {
            return this.mTouchUpReadYVal[pointerId];
        }
        return 0;
    }

    private void onTouchMove(float x, float y, int pointerCount, int pointerId) {
        if (pointerId < 4) {
            this.mTouchMoveWriteId[pointerId] = pointerId;
            if (this.mTouchMoveWriteCount[pointerId] < 10) {
                this.mTouchMoveWriteXVal[pointerId][this.mTouchMoveWriteCount[pointerId]] = GameMath.convertToRealX((int) x);
                int[] iArr = this.mTouchMoveWriteYVal[pointerId];
                int[] iArr2 = this.mTouchMoveWriteCount;
                int i = iArr2[pointerId];
                iArr2[pointerId] = i + 1;
                iArr[i] = GameMath.convertToRealY((int) y);
            }
        }
    }

    public boolean CHKTouchMove() {
        return this.mTouchMove;
    }

    public int getTouchMoveId(int nID) {
        return this.mTouchMoveReadId[nID];
    }

    public int getTouchMoveCount(int nID) {
        return this.mTouchMoveReadCount[nID];
    }

    public int getTouchMoveX(int nID, int pointId) {
        return this.mTouchMoveReadXVal[nID][pointId];
    }

    public int getTouchMoveY(int nID, int pointId) {
        return this.mTouchMoveReadYVal[nID][pointId];
    }
}
