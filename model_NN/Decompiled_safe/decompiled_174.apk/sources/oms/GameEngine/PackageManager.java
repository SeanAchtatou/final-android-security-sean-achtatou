package oms.GameEngine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.IOException;
import java.io.InputStream;

public class PackageManager {
    private static final int PACKAGEHEADSIZE = 16;

    public static Bitmap createBitmap(Context context, int resId) {
        InputStream mRes = context.getResources().openRawResource(resId);
        if (mRes == null) {
            return null;
        }
        int fileLen = 0;
        try {
            fileLen = mRes.available();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (fileLen == 0) {
            return null;
        }
        byte[] buffer = new byte[fileLen];
        try {
            mRes.read(buffer, 0, fileLen);
            mRes.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        byte PackageType = (byte) (GetPackageType(buffer) & 255);
        short PackageFactoryA = (short) (GetPackageFactorA(buffer) & 65535);
        short PackageFactoryB = (short) (GetPackageFactorB(buffer) & 65535);
        int InvalidPackageLeng = GetInvalidLeng(buffer);
        if (InvalidPackageLeng < 0) {
            InvalidPackageLeng = -InvalidPackageLeng;
        }
        for (int i = 16; i < 32; i++) {
            switch (PackageType) {
                case 0:
                    buffer[i] = (byte) (((buffer[i] - PackageFactoryB) - PackageFactoryA) ^ C_MotionEventWrapper8.ACTION_MASK);
                    break;
                case 1:
                    buffer[i] = (byte) (((buffer[i] + PackageFactoryB) - PackageFactoryA) ^ C_MotionEventWrapper8.ACTION_MASK);
                    break;
                case 2:
                    buffer[i] = (byte) (((buffer[i] + PackageFactoryB) + PackageFactoryA) ^ C_MotionEventWrapper8.ACTION_MASK);
                    break;
                default:
                    buffer[i] = (byte) (((buffer[i] - PackageFactoryB) + PackageFactoryA) ^ C_MotionEventWrapper8.ACTION_MASK);
                    break;
            }
        }
        return BitmapFactory.decodeByteArray(buffer, 16, (fileLen - 16) - InvalidPackageLeng, null);
    }

    private static byte GetPackageType(byte[] headBuff) {
        return (byte) ((((headBuff[3] + headBuff[4]) / ((headBuff[1] + headBuff[5]) + headBuff[2])) * (headBuff[0] + headBuff[6] + headBuff[7])) & 3);
    }

    private static short GetPackageFactorA(byte[] headBuff) {
        return (short) ((headBuff[11] + headBuff[9]) * headBuff[8] * headBuff[10]);
    }

    private static short GetPackageFactorB(byte[] headBuff) {
        return (short) (((headBuff[15] + headBuff[12]) * headBuff[14]) / headBuff[13]);
    }

    private static byte GetInvalidLeng(byte[] headBuff) {
        return (byte) (((byte) (headBuff[3] * 128)) + ((byte) (headBuff[8] * 8)) + ((byte) (headBuff[14] * 9)));
    }
}
