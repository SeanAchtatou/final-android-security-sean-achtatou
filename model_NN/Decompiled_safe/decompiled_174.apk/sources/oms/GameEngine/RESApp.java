package oms.GameEngine;

import android.content.Context;
import java.io.IOException;
import java.io.InputStream;

public class RESApp {
    private Context mContext = null;
    private InputStream mRes = null;
    public int nFileLen = 0;

    public RESApp(Context context) {
        this.mContext = context;
    }

    public void OpenRes(int resId) {
        if (this.mRes != null) {
            try {
                this.mRes.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.mRes = this.mContext.getResources().openRawResource(resId);
        try {
            this.nFileLen = this.mRes.available();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public void CloseRes() {
        if (this.mRes != null) {
            try {
                this.mRes.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.mRes = null;
        }
    }

    public void ResRead(byte[] b, int offset, int length) {
        try {
            this.mRes.skip((long) offset);
            this.mRes.read(b, 0, length);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getLength() {
        return this.nFileLen;
    }

    public void Reset() {
        try {
            this.mRes.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
