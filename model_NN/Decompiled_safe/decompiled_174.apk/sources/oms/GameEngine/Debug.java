package oms.GameEngine;

import android.util.Log;

public class Debug {
    protected static boolean debugMode = true;
    private static String tag = "oms";

    public static String getDebugTag() {
        return tag;
    }

    public static void setDebugTag(String tag2) {
        tag = tag2;
    }

    public static void warning(String source, String message) {
        if (debugMode) {
            Log.w(tag, String.valueOf(source) + " - " + message);
            new Exception(String.valueOf(source) + " - " + message).printStackTrace();
        }
    }

    public static void warning(String message) {
        if (debugMode) {
            Log.w(tag, message);
        }
    }

    public static void print(String message) {
        if (debugMode) {
            Log.v(tag, message);
        }
    }

    public static void error(String message) {
        Log.e(tag, message);
        new Exception(message).printStackTrace();
    }

    public static void verbose(String method, String message) {
        if (debugMode) {
            Log.v(tag, String.valueOf(method) + " - " + message);
            if (!debugMode) {
            }
        }
    }

    public static void verbose(String message) {
        if (debugMode) {
            Log.v(tag, message);
        }
    }

    public static void forceExit() {
        System.exit(0);
    }
}
