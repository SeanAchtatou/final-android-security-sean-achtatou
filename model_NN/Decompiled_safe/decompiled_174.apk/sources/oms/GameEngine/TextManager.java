package oms.GameEngine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;

public class TextManager {
    private static TextDEF[] Text;
    private Matrix cMatrix;
    private Context mContext;
    private boolean mIsLogOut = true;
    private long nCurBMPRamSize = 0;
    private int nTextLayerMax;

    public TextManager(Context context, int TextLayer) {
        this.mContext = context;
        Text = null;
        this.nTextLayerMax = TextLayer;
        InitText(this.nTextLayerMax);
        this.cMatrix = new Matrix();
        this.nCurBMPRamSize = 0;
    }

    public void release() {
        CloseAllText();
    }

    private int GetBitmapSize(Bitmap bitmap) {
        int bit = 1;
        if (bitmap.getConfig() == Bitmap.Config.ALPHA_8) {
            bit = 1;
        } else if (bitmap.getConfig() == Bitmap.Config.RGB_565) {
            bit = 2;
        } else if (bitmap.getConfig() == Bitmap.Config.ARGB_4444) {
            bit = 2;
        } else if (bitmap.getConfig() == Bitmap.Config.ARGB_8888) {
            bit = 4;
        }
        return (bitmap.getWidth() * bitmap.getHeight() * bit) + GameEvent.KeepACT;
    }

    public void SetBMPSizeOut(boolean out) {
        this.mIsLogOut = out;
    }

    public long GetBMPRamSize() {
        return this.nCurBMPRamSize;
    }

    public int getTextLayer() {
        return this.nTextLayerMax;
    }

    public void InitText(int TextLayerNum) {
        this.nTextLayerMax = TextLayerNum;
        if (this.nTextLayerMax == 0) {
            this.nTextLayerMax = 1;
        }
        Text = new TextDEF[this.nTextLayerMax];
        for (int i = 0; i < this.nTextLayerMax; i++) {
            Text[i] = new TextDEF();
        }
    }

    public void CloseText(int TextLayer) {
        if (TextLayer < this.nTextLayerMax && Text[TextLayer].Text != null) {
            this.nCurBMPRamSize -= (long) GetBitmapSize(Text[TextLayer].Text);
            Text[TextLayer].Text.recycle();
            Text[TextLayer].Text = null;
            Text[TextLayer].TextAttrib = 0;
            Text[TextLayer].TextCtrl = 0;
            Text[TextLayer].TextXInc = 0;
            Text[TextLayer].TextYInc = 0;
            Text[TextLayer].TextXVal = 0;
            Text[TextLayer].TextYVal = 0;
            if (this.mIsLogOut) {
                Log.v("GameEngine", "Text use RAM: " + (this.nCurBMPRamSize / 1024) + " KBytes");
            }
        }
    }

    public void CloseAllText() {
        for (int i = 0; i < this.nTextLayerMax; i++) {
            CloseText(i);
        }
    }

    public void LoadText(int ResTextID, int TextLayer, int TextDepth) {
        if (TextLayer < this.nTextLayerMax) {
            CloseText(TextLayer);
            Bitmap bmp = PackageManager.createBitmap(this.mContext, ResTextID);
            if (bmp != null) {
                this.nCurBMPRamSize += (long) GetBitmapSize(bmp);
                if (this.mIsLogOut) {
                    Log.v("GameEngine", "ResID: " + ResTextID);
                    Log.v("GameEngine", "Width: " + bmp.getWidth());
                    Log.v("GameEngine", "Height: " + bmp.getHeight());
                    Log.v("GameEngine", "OPtions: " + bmp.getConfig());
                    Log.v("GameEngine", "Text use RAM: " + (this.nCurBMPRamSize / 1024) + " KBytes");
                }
                Text[TextLayer].Text = bmp;
                Text[TextLayer].TextXInc = 0;
                Text[TextLayer].TextYInc = 0;
                Text[TextLayer].TextXVal = 0;
                Text[TextLayer].TextYVal = 0;
                Text[TextLayer].ScaleX = 1.0f;
                Text[TextLayer].ScaleY = 1.0f;
                Text[TextLayer].Rotate = 0.0f;
                Text[TextLayer].TextAttrib = TextDepth;
                Text[TextLayer].TextCtrl = 15;
            }
        }
    }

    public void LoadPicture(int ResTextID, int TextLayer, int TextDepth) {
        if (TextLayer < this.nTextLayerMax) {
            CloseText(TextLayer);
            Bitmap bmp = BitmapFactory.decodeResource(this.mContext.getResources(), ResTextID);
            if (bmp != null) {
                this.nCurBMPRamSize += (long) GetBitmapSize(bmp);
                if (this.mIsLogOut) {
                    Log.v("GameEngine", "ResID: " + ResTextID);
                    Log.v("GameEngine", "Width: " + bmp.getWidth());
                    Log.v("GameEngine", "Height: " + bmp.getHeight());
                    Log.v("GameEngine", "OPtions: " + bmp.getConfig());
                    Log.v("GameEngine", "Text use RAM: " + (this.nCurBMPRamSize / 1024) + " KBytes");
                }
                Text[TextLayer].Text = bmp;
                Text[TextLayer].TextXInc = 0;
                Text[TextLayer].TextYInc = 0;
                Text[TextLayer].TextXVal = 0;
                Text[TextLayer].TextYVal = 0;
                Text[TextLayer].ScaleX = 1.0f;
                Text[TextLayer].ScaleY = 1.0f;
                Text[TextLayer].Rotate = 0.0f;
                Text[TextLayer].TextAttrib = TextDepth;
                Text[TextLayer].TextCtrl = 15;
            }
        }
    }

    public void LoadText(String sResPath, int TextLayer, int TextDepth) {
        if (TextLayer < this.nTextLayerMax) {
            if (Text[TextLayer].Text != null) {
                Text[TextLayer].Text.recycle();
                Text[TextLayer].Text = null;
            }
            InputStream inputStream = null;
            try {
                inputStream = this.mContext.getAssets().open(sResPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (inputStream != null) {
                Bitmap bmp = BitmapFactory.decodeStream(inputStream);
                try {
                    inputStream.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                if (bmp != null) {
                    this.nCurBMPRamSize += (long) GetBitmapSize(bmp);
                    if (this.mIsLogOut) {
                        Log.v("GameEngine", "ResID: 0");
                        Log.v("GameEngine", "Width: " + bmp.getWidth());
                        Log.v("GameEngine", "Height: " + bmp.getHeight());
                        Log.v("GameEngine", "OPtions: " + bmp.getConfig());
                        Log.v("GameEngine", "Text use RAM: " + (this.nCurBMPRamSize / 1024) + " KBytes");
                    }
                    Text[TextLayer].Text = bmp;
                    Text[TextLayer].TextXInc = 0;
                    Text[TextLayer].TextYInc = 0;
                    Text[TextLayer].TextXVal = 0;
                    Text[TextLayer].TextYVal = 0;
                    Text[TextLayer].ScaleX = 1.0f;
                    Text[TextLayer].ScaleY = 1.0f;
                    Text[TextLayer].Rotate = 0.0f;
                    Text[TextLayer].TextAttrib = TextDepth;
                    Text[TextLayer].TextCtrl = 15;
                }
            }
        }
    }

    public void LoadText(int Width, int Height, int TextLayer, int TextDepth) {
        if (Text[TextLayer].Text != null) {
            Text[TextLayer].Text.recycle();
            Text[TextLayer].Text = null;
        }
        Bitmap bmp = Bitmap.createBitmap(Width, Height, Bitmap.Config.ARGB_8888);
        if (TextLayer < this.nTextLayerMax) {
            this.nCurBMPRamSize += (long) GetBitmapSize(bmp);
            if (this.mIsLogOut) {
                Log.v("GameEngine", "ResID: 0");
                Log.v("GameEngine", "Width: " + bmp.getWidth());
                Log.v("GameEngine", "Height: " + bmp.getHeight());
                Log.v("GameEngine", "OPtions: " + bmp.getConfig());
                Log.v("GameEngine", "Text use RAM: " + (this.nCurBMPRamSize / 1024) + " KBytes");
            }
            Text[TextLayer].Text = bmp;
            Text[TextLayer].TextXInc = 0;
            Text[TextLayer].TextYInc = 0;
            Text[TextLayer].TextXVal = 0;
            Text[TextLayer].TextYVal = 0;
            Text[TextLayer].ScaleX = 1.0f;
            Text[TextLayer].ScaleY = 1.0f;
            Text[TextLayer].Rotate = 0.0f;
            Text[TextLayer].TextAttrib = TextDepth;
            Text[TextLayer].TextCtrl = 15;
        }
    }

    public boolean getTextPixels(int TextLayer, int[] pixels) {
        if (TextLayer >= this.nTextLayerMax) {
            return false;
        }
        if (Text[TextLayer].Text == null) {
            return false;
        }
        int width = Text[TextLayer].Text.getWidth();
        Text[TextLayer].Text.getPixels(pixels, 0, width, 0, 0, width, Text[TextLayer].Text.getHeight());
        return true;
    }

    public void setTextPixels(int TextLayer, int[] pixels) {
        if (TextLayer < this.nTextLayerMax && Text[TextLayer].Text != null) {
            int width = Text[TextLayer].Text.getWidth();
            Text[TextLayer].Text.setPixels(pixels, 0, width, 0, 0, width, Text[TextLayer].Text.getHeight());
        }
    }

    public void setTextPixels(int TextLayer, int[] pixels, int offset, int x, int y, int width, int height) {
        if (TextLayer < this.nTextLayerMax && Text[TextLayer].Text != null) {
            Text[TextLayer].Text.setPixels(pixels, offset, Text[TextLayer].Text.getWidth(), x, y, width, height);
        }
    }

    public void setTextPixels(int TextLayer, int[] pixels, int offset, int stride, int x, int y, int width, int height) {
        if (TextLayer < this.nTextLayerMax && Text[TextLayer].Text != null) {
            Text[TextLayer].Text.setPixels(pixels, offset, stride, x, y, width, height);
        }
    }

    public void SetTextInc(int TextLayer, int XInc, int YInc) {
        if (TextLayer < this.nTextLayerMax) {
            Text[TextLayer].TextXInc = XInc;
            Text[TextLayer].TextYInc = YInc;
        }
    }

    public int GetTextWidth(int TextLayer) {
        if (Text[TextLayer].Text != null) {
            return Text[TextLayer].Text.getWidth();
        }
        return 0;
    }

    public int GetTextHeight(int TextLayer) {
        if (Text[TextLayer].Text != null) {
            return Text[TextLayer].Text.getHeight();
        }
        return 0;
    }

    public int GetTextXVal(int TextLayer) {
        return Text[TextLayer].TextXVal;
    }

    public int GetTextYVal(int TextLayer) {
        return Text[TextLayer].TextYVal;
    }

    public void SetTextXVal(int TextLayer, int X) {
        Text[TextLayer].TextXVal = X;
    }

    public void SetTextYVal(int TextLayer, int Y) {
        Text[TextLayer].TextYVal = Y;
    }

    public void SetTextScale(int TextLayer, float Scale) {
        if (Scale > 0.0f) {
            Text[TextLayer].ScaleX = C_Lib.mCanvasScaleX + Scale;
            Text[TextLayer].ScaleY = C_Lib.mCanvasScaleY + Scale;
        }
    }

    public void SetTextRotate(int TextLayer, float Rotate) {
        if (Rotate > 0.0f) {
            Text[TextLayer].Rotate = Rotate;
        }
    }

    public void ScrollText(int TextLayer) {
        if (TextLayer < this.nTextLayerMax && Text[TextLayer].Text != null) {
            int ScreenWidth = GameCanvas.GetScreenWidth();
            int ScreenHeight = GameCanvas.GetScreenHeight();
            int Width = Text[TextLayer].Text.getWidth();
            int Height = Text[TextLayer].Text.getHeight();
            int TextCtrl = Text[TextLayer].TextCtrl;
            if (Text[TextLayer].TextXInc < 0 && (TextCtrl & 4) == 4) {
                Text[TextLayer].TextXVal -= Text[TextLayer].TextXInc;
                if (Text[TextLayer].TextXVal >= 0) {
                    Text[TextLayer].TextXVal = 0;
                }
            }
            if (Text[TextLayer].TextXInc > 0 && (TextCtrl & 8) == 8) {
                Text[TextLayer].TextXVal -= Text[TextLayer].TextXInc;
                if (Text[TextLayer].TextXVal + ScreenWidth >= Width) {
                    Text[TextLayer].TextXVal = ScreenWidth - Width;
                }
            }
            if (Text[TextLayer].TextYInc < 0 && (TextCtrl & 1) == 1) {
                Text[TextLayer].TextYVal -= Text[TextLayer].TextYInc;
                if (Text[TextLayer].TextXVal >= 0) {
                    Text[TextLayer].TextYVal = 0;
                }
            }
            if (Text[TextLayer].TextYInc > 0 && (TextCtrl & 2) == 2) {
                Text[TextLayer].TextYVal -= Text[TextLayer].TextYInc;
                if (Text[TextLayer].TextYVal + ScreenHeight >= Height) {
                    Text[TextLayer].TextYVal = ScreenHeight - Height;
                }
            }
        }
    }

    public void OnDraw(Canvas canvas, int Attrib, Paint paint) {
        for (int i = 0; i < this.nTextLayerMax; i++) {
            if (Text[i].Text != null && Text[i].TextAttrib == Attrib) {
                canvas.drawBitmap(Text[i].Text, (float) GameMath.convertToUIX(Text[i].TextXVal), (float) GameMath.convertToUIY(Text[i].TextYVal), paint);
            }
        }
    }

    public void OnDraw(Canvas canvas, int Attrib, int XOff, int YOff, Paint paint) {
        for (int i = 0; i < this.nTextLayerMax; i++) {
            if (Text[i].Text != null && Text[i].TextAttrib == Attrib) {
                if (Text[i].ScaleX == 1.0f && Text[i].ScaleY == 1.0f && Text[i].Rotate == 0.0f) {
                    canvas.drawBitmap(Text[i].Text, ((float) Text[i].TextXVal) + ((float) XOff), ((float) Text[i].TextYVal) + ((float) YOff), paint);
                } else {
                    this.cMatrix.reset();
                    this.cMatrix.setTranslate((float) GameMath.convertToUIX(Text[i].TextXVal + XOff), (float) GameMath.convertToUIY(Text[i].TextYVal + YOff));
                    this.cMatrix.postRotate(Text[i].Rotate, (float) (Text[i].Text.getWidth() >> 1), (float) (Text[i].Text.getHeight() >> 1));
                    if (!(Text[i].ScaleX == 1.0f && Text[i].ScaleY == 1.0f)) {
                        this.cMatrix.postScale(Text[i].ScaleX, Text[i].ScaleY);
                    }
                    canvas.drawBitmap(Text[i].Text, this.cMatrix, paint);
                }
            }
        }
    }
}
