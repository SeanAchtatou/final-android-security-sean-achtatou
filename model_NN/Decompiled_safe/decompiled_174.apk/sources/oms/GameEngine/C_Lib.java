package oms.GameEngine;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;

public class C_Lib {
    public static final int ADMOB_GONE = 2;
    public static final int ADMOB_INVISIABLE = 0;
    public static final int ADMOB_REFLASH = 32768;
    public static final int ADMOB_VISIABLE = 1;
    public static float mCanvasScaleX = 1.0f;
    public static float mCanvasScaleY = 1.0f;
    private Activity cActivity;
    private MediaManager cMediaManager;
    private C_MessageManager cMessageMgr;
    public Thread cThread;
    private VibrateManager cVibrateManager;
    public GameView cView;
    private GameCanvas gameCanvas;
    private InputInterface input;
    public Bitmap mBackground;
    private Context mContext = null;
    public boolean mTop;
    private int nAdmobStatus;
    public int nFPS;
    private int nFrameFlashTime;
    public int nRefreshHeight;
    public int nRefreshWidth;
    private int nResumeDelay;
    public float nScaledDensity;
    public int nVBLCount;

    public C_Lib(Context context, int TextLayer, int SpriteResNum, int SpriteNum) {
        this.mContext = context;
        this.gameCanvas = null;
        this.cMediaManager = new MediaManager(this.mContext);
        this.input = new InputInterface();
        InitCanvas(TextLayer, SpriteResNum, SpriteNum);
        this.cMessageMgr = new C_MessageManager();
        this.cVibrateManager = new VibrateManager();
        setFrameReflashTime(33);
        this.nFPS = 0;
        this.nVBLCount = 0;
        this.nRefreshWidth = 320;
        this.nRefreshHeight = 480;
        this.nScaledDensity = 1.0f;
        this.cView = null;
        this.cThread = null;
        this.mBackground = null;
        this.mTop = true;
        this.nResumeDelay = 0;
        this.nAdmobStatus = 1;
    }

    public void Release() {
        if (this.gameCanvas != null) {
            this.gameCanvas.release();
            this.gameCanvas = null;
        }
        if (this.cMediaManager != null) {
            this.cMediaManager.release();
            this.cMediaManager = null;
        }
        if (this.mBackground != null) {
            this.mBackground.recycle();
            this.mBackground = null;
        }
    }

    public void SetActivity(Activity activity) {
        this.cActivity = activity;
    }

    public Activity GetActivity() {
        return this.cActivity;
    }

    public void SetAdmobStatus(int status) {
        if (32768 != status) {
            this.nAdmobStatus = status;
        } else {
            this.nAdmobStatus |= 32768;
        }
    }

    public int GetAdmobStatus() {
        return this.nAdmobStatus;
    }

    public void SetGameThread(Thread thread) {
        this.cThread = thread;
    }

    public Thread GetGameThread() {
        return this.cThread;
    }

    public void SetGameView(GameView view) {
        this.cView = view;
    }

    public GameView GetView() {
        return this.cView;
    }

    public void SetBackground(int resID) {
        if (this.nRefreshHeight > 480) {
            if (this.mBackground != null) {
                this.mBackground.recycle();
                this.mBackground = null;
            }
            this.mBackground = PackageManager.createBitmap(this.mContext, resID);
        }
    }

    public void SetBackgroundTop(boolean top) {
        this.mTop = top;
    }

    private void InitCanvas(int TextLayer, int SpriteResNum, int SpriteNum) {
        if (this.gameCanvas == null) {
            this.gameCanvas = new GameCanvas(this.mContext, TextLayer, SpriteResNum, SpriteNum);
        }
    }

    public void InitMedia(int soundNum, int mediaNum) {
        if (this.cMediaManager == null) {
            this.cMediaManager.Init(soundNum, mediaNum);
        }
    }

    public Context getMContext() {
        return this.mContext;
    }

    public void SetCanvaseSize(int width, int height) {
        this.nRefreshWidth = width;
        this.nRefreshHeight = height;
    }

    public void SetReflashSize(int Width, int Height, float scaledDensity) {
        this.nRefreshWidth = Width;
        this.nRefreshHeight = Height;
        this.nScaledDensity = scaledDensity;
        SetScreenOffset((this.nRefreshWidth - 320) / 2, (this.nRefreshHeight - 480) / 2);
        SetCanvasScale(1.0f, 1.0f);
    }

    public void SetCanvasScale(float x, float y) {
        mCanvasScaleX = x;
        mCanvasScaleY = y;
    }

    public int GetReflashWidth() {
        return (int) (((float) this.nRefreshWidth) * this.nScaledDensity);
    }

    public int GetReflashHeight() {
        return (int) (((float) this.nRefreshHeight) * this.nScaledDensity);
    }

    public void setMContext(Context context) {
        this.mContext = context;
    }

    public GameCanvas getGameCanvas() {
        return this.gameCanvas;
    }

    public MediaManager getMediaManager() {
        return this.cMediaManager;
    }

    public C_MessageManager getMessageMgr() {
        return this.cMessageMgr;
    }

    public InputInterface getInput() {
        return this.input;
    }

    public VibrateManager getVibrateManager() {
        return this.cVibrateManager;
    }

    public void SetScreenOffset(int XOff, int YOff) {
        this.input.SetScreenOffset(XOff, YOff);
        this.gameCanvas.SetScreenOffset(XOff, YOff);
        C_MultiTouch.SetScreenOffset(XOff, YOff);
    }

    public void ClearACT() {
        this.gameCanvas.ClearACT();
    }

    public void WaitBLK() {
        if (this.nResumeDelay > 0) {
            this.nResumeDelay--;
        } else {
            this.input.nIsPause = false;
        }
        if (this.gameCanvas != null) {
            this.gameCanvas.flush();
            if (this.cView != null) {
                while (this.gameCanvas != null && this.gameCanvas.getUpdata()) {
                    try {
                        Thread.sleep((long) (this.nFrameFlashTime >> 1));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        this.nVBLCount++;
    }

    public void onPause() {
        this.input.nIsPause = true;
        this.input.ClearKeyValue();
        this.nResumeDelay = 10;
        this.gameCanvas.bUpdate = false;
        if (this.cMediaManager != null) {
            this.cMediaManager.onPause();
        }
    }

    public void onResume() {
        if (this.cMediaManager != null) {
            this.cMediaManager.onResume();
        }
    }

    public void setFrameReflashTime(int time) {
        this.nFrameFlashTime = time;
    }

    public int getFrameReflashTime() {
        return this.nFrameFlashTime;
    }

    public void OnDraw(Canvas canvas) {
        this.gameCanvas.onDraw(canvas);
    }

    /* access modifiers changed from: package-private */
    public void onDraw(C_Lib cLib, Canvas canvas, int picHeight) {
        this.gameCanvas.onDraw(cLib, canvas, picHeight);
    }

    public void ViewOpen(int alphaSpeed) {
        System.gc();
        boolean rt = true;
        while (rt) {
            if (this.gameCanvas.ViewOpen(alphaSpeed)) {
                rt = false;
            }
            WaitBLK();
        }
        this.input.ReadTouch();
        this.input.ReadKeyBoard();
    }

    public void ViewDark(int alphaSpeed) {
        System.gc();
        boolean rt = true;
        while (rt) {
            if (this.gameCanvas.ViewDark(alphaSpeed)) {
                rt = false;
            }
            WaitBLK();
        }
        this.input.ReadTouch();
        this.input.ReadKeyBoard();
    }

    public boolean isReflash() {
        return this.gameCanvas.isReflash();
    }

    public void ReadTouch() {
        this.input.ReadTouch();
    }

    public void setFPS(int FPS) {
        this.nFPS = FPS;
    }

    public int getFPS() {
        return this.nFPS;
    }

    public int getVBLCount() {
        return this.nVBLCount;
    }
}
