package oms.GameEngine;

import java.util.LinkedList;

public class C_MessageManager {
    private int nQueueLen;
    private LinkedList<C_MSG> pMessageQueue = new LinkedList<>();

    public C_MessageManager() {
        RemoveAllMsgQueue();
    }

    public int GetMessageQueueLength() {
        return this.nQueueLen;
    }

    public void SendMessage(int hWnd, int message, int param, int time, int cursorX, int cursorY) {
        C_MSG msg = new C_MSG();
        msg.SetMessage(hWnd, message, param, time, cursorX, cursorY);
        this.pMessageQueue.add(msg);
        this.nQueueLen++;
    }

    public void SendMessage(int hWnd, String message, int param, int time, int cursorX, int cursorY) {
        C_MSG msg = new C_MSG();
        msg.SetMessage(hWnd, message, param, time, cursorX, cursorY);
        this.pMessageQueue.add(msg);
        this.nQueueLen++;
    }

    public void SendMessage(int hWnd, int message, int param) {
        C_MSG msg = new C_MSG();
        msg.SetMessage(hWnd, message, param);
        this.pMessageQueue.add(msg);
        this.nQueueLen++;
    }

    public void SendMessage(int hWnd, String message, int param) {
        C_MSG msg = new C_MSG();
        msg.SetMessage(hWnd, message, param);
        this.pMessageQueue.add(msg);
        this.nQueueLen++;
    }

    public C_MSG GetMessageQueue() {
        if (this.nQueueLen == 0) {
            return null;
        }
        return this.pMessageQueue.get(this.nQueueLen - 1);
    }

    public C_MSG GetMessageQueue(int queueIdx) {
        if (queueIdx >= this.nQueueLen) {
            return null;
        }
        return this.pMessageQueue.get(queueIdx);
    }

    public void RemoveAllMsgQueue() {
        if (this.pMessageQueue != null) {
            for (int i = 0; i < this.nQueueLen; i++) {
                this.pMessageQueue.removeLast();
            }
        }
        this.nQueueLen = 0;
    }
}
