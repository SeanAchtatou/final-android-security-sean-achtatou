package com.derax.rocketboy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.Random;

public class ActionView extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    final int GOAL = 5;
    final int MOVE = 2;
    final int MOVE_R = 3;
    final int NONE = 0;
    final int NORMAL = 1;
    final int ROLE = 4;
    boolean arrive = false;
    boolean batabata = true;
    Boy boy = new Boy();
    private Canvas canvas;
    boolean clear = false;
    int clear_score;
    Context context;
    int gameover = 0;
    private int hiscore;
    private SurfaceHolder holder;
    private Bitmap[] image;
    private int init;
    boolean isFirst = true;
    boolean jump = false;
    double move = 5.0d;
    private Paint paint;
    private boolean playFlag;
    private MediaPlayer player;
    private Random rand;
    private Rect rect;
    private int scene;
    private int score;
    int[] soundId = new int[5];
    SoundPool soundPool;
    double state = 0.0d;
    int[] state_boy = {3, 9, 10, 9, 3, 11, 12, 11};
    int[] state_num = {4, 5, 6, 5, 4, 7, 8, 7};
    Taru[] tarus = new Taru[3];
    private Thread thread;
    private int tick;
    private boolean touchDown;
    int up_dist = 1;

    public ActionView(Context context2) {
        super(context2);
        this.context = context2;
        ((Activity) context2).setVolumeControlStream(3);
        this.soundPool = new SoundPool(10, 3, 0);
        this.soundId[0] = this.soundPool.load(context2, R.raw.death, 1);
        this.soundId[1] = this.soundPool.load(context2, R.raw.go, 1);
        this.soundId[2] = this.soundPool.load(context2, R.raw.death, 1);
        this.rand = new Random();
        this.clear_score = -1;
        this.paint = new Paint();
        this.paint.setAntiAlias(true);
        this.rect = new Rect();
        this.image = new Bitmap[23];
        for (int i = 0; i < 23; i++) {
            this.image[i] = loadImage(i);
        }
        setFocusable(true);
        setFocusableInTouchMode(true);
        this.hiscore = context2.getSharedPreferences("PreferencesEx", 0).getInt("hiscore", 0);
        this.holder = getHolder();
        this.holder.addCallback(this);
        this.holder.setFixedSize(getWidth(), getHeight());
    }

    private Bitmap loadImage(int idx) {
        int resID = R.drawable.pic0;
        if (idx == 1) {
            resID = R.drawable.boy;
        }
        if (idx == 2) {
            resID = R.drawable.boy1;
        }
        if (idx == 3) {
            resID = R.drawable.boy2;
        }
        if (idx == 4) {
            resID = R.drawable.taru;
        }
        if (idx == 5) {
            resID = R.drawable.r_taru2;
        }
        if (idx == 6) {
            resID = R.drawable.r_taru3;
        }
        if (idx == 7) {
            resID = R.drawable.r_taru4;
        }
        if (idx == 8) {
            resID = R.drawable.r_taru5;
        }
        if (idx == 9) {
            resID = R.drawable.boy11;
        }
        if (idx == 10) {
            resID = R.drawable.boy12;
        }
        if (idx == 11) {
            resID = R.drawable.boy13;
        }
        if (idx == 12) {
            resID = R.drawable.boy14;
        }
        if (idx == 13) {
            resID = R.drawable.pic3;
        }
        if (idx == 14) {
            resID = R.drawable.pic4;
        }
        if (idx == 15) {
            resID = R.drawable.pic5;
        }
        if (idx == 16) {
            resID = R.drawable.pic6;
        }
        if (idx == 17) {
            resID = R.drawable.bg2;
        }
        if (idx == 18) {
            resID = R.drawable.smoke;
        }
        if (idx == 19) {
            resID = R.drawable.s_taru;
        }
        if (idx == 20) {
            resID = R.drawable.g_taru;
        }
        if (idx == 21) {
            resID = R.drawable.m_taru;
        }
        if (idx == 22) {
            resID = R.drawable.r_taru;
        }
        return BitmapFactory.decodeResource(getResources(), resID);
    }

    public void surfaceCreated(SurfaceHolder holder2) {
        this.thread = new Thread(this);
        this.thread.start();
    }

    public void surfaceDestroyed(SurfaceHolder holder2) {
        this.thread = null;
    }

    public void surfaceChanged(SurfaceHolder holder2, int format, int w, int h) {
    }

    public void run() {
        long sleepTime = 0;
        while (this.thread != null) {
            this.canvas = this.holder.lockCanvas();
            onTick(this.canvas);
            this.holder.unlockCanvasAndPost(this.canvas);
            do {
            } while (System.currentTimeMillis() < 100 + sleepTime);
            sleepTime = System.currentTimeMillis();
            int i = this.tick + 1;
            this.tick = i;
            if (i >= 999) {
                this.tick = 99;
            }
        }
    }

    private void onTick(Canvas canvas2) {
        if (this.isFirst) {
            for (int i = 0; i < 3; i++) {
                this.tarus[i] = new Taru();
                this.tarus[i].posX = this.rand.nextInt((getWidth() - 60) - this.image[4].getWidth()) + 30;
            }
            this.tarus[0].posY = (getHeight() - (this.image[4].getHeight() / 2)) - 10;
            this.tarus[1].posY = (getHeight() / 2) - (this.image[4].getHeight() / 2);
            this.tarus[2].posY = 10 - (this.image[4].getHeight() / 2);
            this.isFirst = false;
        }
        if (this.arrive && this.boy.alive) {
            for (int i2 = 0; i2 < 3; i2++) {
                this.tarus[i2].posY += 30;
            }
            if (this.tarus[this.boy.type].posY >= getHeight() - this.image[4].getHeight()) {
                this.arrive = false;
                for (int i3 = 0; i3 < 3; i3++) {
                    if (this.tarus[i3].posY > getHeight()) {
                        this.tarus[i3].type = 1;
                        if (this.score >= 100 && this.score < 300) {
                            if (this.score == 150) {
                                this.tarus[i3].type = this.rand.nextInt(3) + 2;
                            }
                            if (this.score == 250) {
                                this.tarus[i3].type = this.rand.nextInt(3) + 2;
                            }
                        }
                        if (this.score >= 300 && this.score < 600) {
                            if (this.score == 330) {
                                this.tarus[i3].type = this.rand.nextInt(3) + 2;
                            }
                            if (this.score == 370) {
                                this.tarus[i3].type = this.rand.nextInt(3) + 2;
                            }
                            if (this.score == 430) {
                                this.tarus[i3].type = this.rand.nextInt(3) + 2;
                            }
                            if (this.score == 470) {
                                this.tarus[i3].type = this.rand.nextInt(3) + 2;
                            }
                            if (this.score == 530) {
                                this.tarus[i3].type = this.rand.nextInt(3) + 2;
                            }
                            if (this.score == 570) {
                                this.tarus[i3].type = this.rand.nextInt(3) + 2;
                            }
                        }
                        if (this.score >= 600 && this.score < 1000 && this.score % 30 == 0) {
                            this.tarus[i3].type = this.rand.nextInt(3) + 2;
                        }
                        if (this.score >= 1000 && this.score % 20 == 0) {
                            this.tarus[i3].type = this.rand.nextInt(3) + 2;
                        }
                        if (this.score == this.clear_score - 20 && this.clear_score != -1) {
                            this.tarus[i3].type = 5;
                        }
                        if (this.score > this.clear_score - 20 && this.clear_score != -1) {
                            this.tarus[i3].type = 0;
                        }
                        this.tarus[i3].posX = this.rand.nextInt((getWidth() - this.image[4].getWidth()) - 20) + 10;
                        this.tarus[i3].posY = 10 - (this.image[4].getHeight() / 2);
                    }
                }
            }
        }
        this.paint.setColor(Color.rgb(0, 0, 0));
        fillRect(0, 0, getWidth(), getHeight());
        for (int i4 = 0; i4 < (getHeight() / 30) + 10; i4++) {
            this.paint.setColor(Color.rgb(255 - (i4 * 5), 0, 0));
            fillRect(0, (getHeight() * i4) / 30, getWidth(), ((i4 + 1) * getHeight()) / 30);
        }
        canvas2.drawBitmap(this.image[17], 0.0f, (float) ((getHeight() - this.image[17].getHeight()) + (this.score * this.up_dist)), (Paint) null);
        if (this.jump || this.arrive) {
            if (!this.arrive) {
                switch (this.boy.direction) {
                    case -60:
                        Boy boy2 = this.boy;
                        boy2.posX = (int) (((double) boy2.posX) - Math.sqrt(Math.pow((double) (getHeight() / 10), 2.0d) - Math.pow((double) (getHeight() / 20), 2.0d)));
                        this.boy.posY -= getHeight() / 20;
                        break;
                    case -30:
                        Boy boy3 = this.boy;
                        boy3.posY = (int) (((double) boy3.posY) - Math.sqrt(Math.pow((double) (getHeight() / 10), 2.0d) - Math.pow((double) (getHeight() / 20), 2.0d)));
                        this.boy.posX -= getHeight() / 20;
                        break;
                    case 0:
                        this.boy.posY -= getHeight() / 10;
                        break;
                    case 30:
                        Boy boy4 = this.boy;
                        boy4.posY = (int) (((double) boy4.posY) - Math.sqrt(Math.pow((double) (getHeight() / 10), 2.0d) - Math.pow((double) (getHeight() / 20), 2.0d)));
                        this.boy.posX += getHeight() / 20;
                        break;
                    case 60:
                        Boy boy5 = this.boy;
                        boy5.posX = (int) (((double) boy5.posX) + Math.sqrt(Math.pow((double) (getHeight() / 10), 2.0d) - Math.pow((double) (getHeight() / 20), 2.0d)));
                        this.boy.posY -= getHeight() / 20;
                        break;
                }
            }
            if (this.boy.posY <= this.tarus[this.boy.type].posY + (this.image[4].getHeight() / 2)) {
                this.arrive = true;
                Log.i("", String.valueOf(this.tarus[this.boy.type].type) + "判定" + this.boy.posX + ":" + this.tarus[this.boy.type].pos2);
                if (Math.abs(this.boy.posX - this.tarus[this.boy.type].posX) <= this.image[4].getHeight() / 2) {
                    this.boy.posX = this.tarus[this.boy.type].posX;
                    this.boy.posY = this.tarus[this.boy.type].posY;
                    this.boy.second = false;
                    if (this.jump) {
                        if (this.boy.alive) {
                            this.score += 10;
                        }
                        this.move += 0.1d;
                        this.boy.direction = 0;
                        this.jump = false;
                    }
                } else if ((this.tarus[this.boy.type].type == 2 || this.tarus[this.boy.type].type == 3) && Math.abs(this.boy.posX - this.tarus[this.boy.type].pos2) <= this.image[4].getHeight() / 2) {
                    this.boy.posX = this.tarus[this.boy.type].pos2;
                    this.boy.posY = this.tarus[this.boy.type].posY;
                    this.boy.second = true;
                    if (this.jump) {
                        if (this.boy.alive) {
                            this.score += 10;
                        }
                        this.move += 0.1d;
                        this.boy.direction = 0;
                        this.jump = false;
                    }
                } else {
                    this.boy.alive = false;
                }
            }
        } else {
            if (this.boy.second) {
                this.boy.posX = this.tarus[this.boy.type].pos2;
            } else {
                this.boy.posX = this.tarus[this.boy.type].posX;
            }
            if (this.boy.direction != 0) {
                this.boy.posY = this.tarus[this.boy.type].posY;
            } else if (((int) this.state) % 2 == 0) {
                this.boy.posY = this.tarus[this.boy.type].posY - 20;
            } else {
                this.boy.posY = this.tarus[this.boy.type].posY - 30;
            }
        }
        if (this.boy.posX < (-this.image[2].getWidth()) / 2 || this.boy.posX > getWidth() - (this.image[2].getWidth() / 2)) {
            this.boy.alive = false;
        }
        if (this.boy.alive) {
            if (!this.arrive && this.jump) {
                switch (this.boy.direction) {
                    case -60:
                        canvas2.drawBitmap(this.image[12], (float) this.boy.posX, (float) this.boy.posY, (Paint) null);
                        break;
                    case -30:
                        canvas2.drawBitmap(this.image[11], (float) this.boy.posX, (float) this.boy.posY, (Paint) null);
                        break;
                    case 0:
                        canvas2.drawBitmap(this.image[18], (float) this.boy.posX, (float) (this.boy.posY + this.image[2].getHeight()), (Paint) null);
                        if (this.batabata) {
                            canvas2.drawBitmap(this.image[2], (float) this.boy.posX, (float) this.boy.posY, (Paint) null);
                        } else {
                            canvas2.drawBitmap(this.image[3], (float) this.boy.posX, (float) this.boy.posY, (Paint) null);
                        }
                        this.batabata = !this.batabata;
                        break;
                    case 30:
                        canvas2.drawBitmap(this.image[9], (float) this.boy.posX, (float) this.boy.posY, (Paint) null);
                        break;
                    case 60:
                        canvas2.drawBitmap(this.image[10], (float) this.boy.posX, (float) this.boy.posY, (Paint) null);
                        break;
                }
            } else {
                switch (this.boy.direction) {
                    case -60:
                        canvas2.drawBitmap(this.image[12], (float) this.boy.posX, (float) this.boy.posY, (Paint) null);
                        break;
                    case -30:
                        canvas2.drawBitmap(this.image[11], (float) this.boy.posX, (float) this.boy.posY, (Paint) null);
                        break;
                    case 0:
                        canvas2.drawBitmap(this.image[2], (float) this.boy.posX, (float) this.boy.posY, (Paint) null);
                        break;
                    case 30:
                        canvas2.drawBitmap(this.image[9], (float) this.boy.posX, (float) this.boy.posY, (Paint) null);
                        break;
                    case 60:
                        canvas2.drawBitmap(this.image[10], (float) this.boy.posX, (float) this.boy.posY, (Paint) null);
                        break;
                }
            }
        } else {
            if (this.gameover == 0) {
                canvas2.drawBitmap(this.image[9], (float) this.boy.posX, (float) this.boy.posY, (Paint) null);
                this.soundPool.play(this.soundId[0], 100.0f, 100.0f, 1, 0, 1.0f);
            }
            if (this.gameover == 1) {
                canvas2.drawBitmap(this.image[14], (float) this.boy.posX, (float) this.boy.posY, (Paint) null);
            }
            if (this.gameover == 2) {
                canvas2.drawBitmap(this.image[15], (float) this.boy.posX, (float) this.boy.posY, (Paint) null);
            }
            if (this.gameover == 3) {
                canvas2.drawBitmap(this.image[16], (float) this.boy.posX, (float) this.boy.posY, (Paint) null);
            }
            if (this.gameover < 4) {
                this.gameover++;
            }
            if (this.score > this.hiscore) {
                this.hiscore = this.score;
                SharedPreferences.Editor editor = getContext().getSharedPreferences("PreferencesEx", 0).edit();
                editor.putInt("hiscore", this.hiscore);
                editor.commit();
            }
        }
        if (this.move > ((double) (getWidth() / 4))) {
            this.move = (double) (getWidth() / 4);
        }
        for (int i5 = 0; i5 < 3; i5++) {
            this.paint.setColor(-16777216);
            if (this.tarus[i5].type == 5) {
                fillRect(0, this.tarus[i5].posY + (this.image[4].getHeight() / 2), getWidth(), 3);
                canvas2.drawBitmap(this.image[20], (float) this.tarus[i5].posX, (float) this.tarus[i5].posY, (Paint) null);
                if (this.tarus[i5].posX > (getWidth() - this.image[4].getWidth()) - 10 || this.tarus[i5].posX < 10) {
                    this.tarus[i5].reverse = !this.tarus[i5].reverse;
                }
                if (!this.tarus[i5].reverse) {
                    this.tarus[i5].posX += ((int) this.move) + i5;
                } else {
                    this.tarus[i5].posX -= ((int) this.move) + i5;
                }
            } else if (this.tarus[i5].type == 1) {
                fillRect(0, this.tarus[i5].posY + (this.image[4].getHeight() / 2), getWidth(), 3);
                canvas2.drawBitmap(this.image[4], (float) this.tarus[i5].posX, (float) this.tarus[i5].posY, (Paint) null);
                if (this.tarus[i5].posX > getWidth() - this.image[4].getWidth() || this.tarus[i5].posX < 0) {
                    this.tarus[i5].reverse = !this.tarus[i5].reverse;
                }
                if (!this.tarus[i5].reverse) {
                    this.tarus[i5].posX += ((int) this.move) + i5;
                } else {
                    this.tarus[i5].posX -= ((int) this.move) + i5;
                }
            } else if (this.tarus[i5].type == 2 || this.tarus[i5].type == 3) {
                fillRect(0, this.tarus[i5].posY + (this.image[4].getHeight() / 2), getWidth(), 3);
                canvas2.drawBitmap(this.image[21], (float) this.tarus[i5].posX, (float) this.tarus[i5].posY, (Paint) null);
                canvas2.drawBitmap(this.image[21], (float) this.tarus[i5].pos2, (float) this.tarus[i5].posY, (Paint) null);
                if (this.tarus[i5].posX > (getWidth() / 2) - this.image[4].getWidth()) {
                    this.tarus[i5].pos2 = (this.tarus[i5].posX - (getWidth() / 2)) - (this.image[4].getWidth() / 2);
                } else {
                    this.tarus[i5].pos2 = this.tarus[i5].posX + (getWidth() / 2) + (this.image[4].getWidth() / 2);
                }
                if (this.tarus[i5].type == 2) {
                    this.tarus[i5].posX -= ((int) this.move) + i5;
                } else {
                    this.tarus[i5].posX += ((int) this.move) + i5;
                }
                if (this.tarus[i5].posX + this.image[4].getWidth() < 0) {
                    this.tarus[i5].posX = getWidth();
                }
                if (this.tarus[i5].posX > getWidth()) {
                    this.tarus[i5].posX = -this.image[4].getWidth();
                }
            } else if (this.tarus[i5].type == 4) {
                fillRect(0, this.tarus[i5].posY + (this.image[4].getHeight() / 2), getWidth(), 3);
                int[] states = {22, 5, 6, 5, 22, 7, 8, 7};
                int num = ((int) this.state) % 8;
                if (!this.jump && i5 == this.boy.type) {
                    switch (states[num]) {
                        case 5:
                            this.boy.direction = 30;
                            break;
                        case 6:
                            this.boy.direction = 60;
                            break;
                        case 7:
                            this.boy.direction = -30;
                            break;
                        case 8:
                            this.boy.direction = -60;
                            break;
                        case 22:
                            this.boy.direction = 0;
                            break;
                    }
                }
                canvas2.drawBitmap(this.image[states[num]], (float) this.tarus[i5].posX, (float) this.tarus[i5].posY, (Paint) null);
                if (this.tarus[i5].posX > getWidth() - this.image[4].getWidth() || this.tarus[i5].posX < 0) {
                    this.tarus[i5].reverse = !this.tarus[i5].reverse;
                }
                if (!this.tarus[i5].reverse) {
                    this.tarus[i5].posX += ((int) this.move) + i5;
                } else {
                    this.tarus[i5].posX -= ((int) this.move) + i5;
                }
            }
        }
        this.state += 0.1d;
        if (this.state > 100.0d) {
            this.state = 0.0d;
        }
        drawScore(20);
        if (this.gameover >= 4) {
            drawMessage("Game Over", getHeight() / 2);
        }
        if (this.score == this.clear_score) {
            drawMessage("Clear", getHeight() / 2);
        }
    }

    private void fillRect(int x, int y, int w, int h) {
        this.rect.left = x;
        this.rect.top = y;
        this.rect.right = x + w;
        this.rect.bottom = y + h;
        this.paint.setStyle(Paint.Style.FILL);
        this.canvas.drawRect(this.rect, this.paint);
    }

    private void drawScore(int y) {
        String str0 = new StringBuilder().append(this.score).toString();
        while (str0.length() < 6) {
            str0 = "0" + str0;
        }
        String str1 = new StringBuilder().append(this.hiscore).toString();
        while (str1.length() < 6) {
            str1 = "0" + str1;
        }
        String str = "SCORE " + str0 + "          HISCORE " + str1;
        this.paint.setTextSize(16.0f);
        drawText(str, ((int) (((float) getWidth()) - this.paint.measureText(str))) / 2, y, -16777216, -1);
    }

    private void drawMessage(String text, int y) {
        this.paint.setTextSize(24.0f);
        drawText(text, ((int) (((float) getWidth()) - this.paint.measureText(text))) / 2, y, -21846, -65536);
    }

    private void drawText(String text, int x, int y, int color0, int color1) {
        this.paint.setColor(color0);
        this.canvas.drawText(text, (float) (x - 1), (float) (y - 1), this.paint);
        this.canvas.drawText(text, (float) x, (float) (y - 1), this.paint);
        this.canvas.drawText(text, (float) (x + 1), (float) (y - 1), this.paint);
        this.canvas.drawText(text, (float) (x - 1), (float) y, this.paint);
        this.canvas.drawText(text, (float) (x + 1), (float) y, this.paint);
        this.canvas.drawText(text, (float) (x - 1), (float) (y + 1), this.paint);
        this.canvas.drawText(text, (float) x, (float) (y + 1), this.paint);
        this.canvas.drawText(text, (float) (x + 1), (float) (y + 1), this.paint);
        this.paint.setColor(color1);
        this.canvas.drawText(text, (float) x, (float) y, this.paint);
    }

    public boolean onTouchEvent(MotionEvent event) {
        int touchAction = event.getAction();
        if (touchAction == 0) {
            if (!this.boy.alive) {
                this.context.startActivity(new Intent(this.context, Title.class));
            } else if (this.score == this.clear_score) {
                this.context.startActivity(new Intent(this.context, Title.class));
            } else if (!this.jump && !this.arrive) {
                this.soundPool.play(this.soundId[1], 100.0f, 100.0f, 1, 0, 1.0f);
                this.jump = true;
                this.boy.type++;
                if (this.boy.type > 2) {
                    this.boy.type = 0;
                }
            }
        } else if (touchAction != 1) {
        }
        return true;
    }
}
