package com.derax.rocketboy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Title extends Activity {
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.main);
        findViewById(R.id.main).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Title.this.startActivity(new Intent(Title.this, ActionGame.class));
            }
        });
    }
}
