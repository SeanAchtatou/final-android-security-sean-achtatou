package com.derax.rocketboy;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int color_aliceblue = 2131034125;
        public static final int color_aqua = 2131034124;
        public static final int color_beige = 2131034128;
        public static final int color_black = 2131034131;
        public static final int color_blue = 2131034123;
        public static final int color_chocolate = 2131034120;
        public static final int color_crimson = 2131034113;
        public static final int color_darkgray = 2131034129;
        public static final int color_deeppink = 2131034115;
        public static final int color_gold = 2131034119;
        public static final int color_limegreen = 2131034122;
        public static final int color_mistyrose = 2131034116;
        public static final int color_orange = 2131034117;
        public static final int color_pink = 2131034114;
        public static final int color_purple = 2131034126;
        public static final int color_red = 2131034112;
        public static final int color_seagreen = 2131034121;
        public static final int color_violet = 2131034127;
        public static final int color_white = 2131034130;
        public static final int color_yellow = 2131034118;
    }

    public static final class drawable {
        public static final int back2 = 2130837504;
        public static final int bg = 2130837505;
        public static final int bg2 = 2130837506;
        public static final int boy = 2130837507;
        public static final int boy1 = 2130837508;
        public static final int boy11 = 2130837509;
        public static final int boy12 = 2130837510;
        public static final int boy13 = 2130837511;
        public static final int boy14 = 2130837512;
        public static final int boy2 = 2130837513;
        public static final int g_taru = 2130837514;
        public static final int m_taru = 2130837515;
        public static final int pic0 = 2130837516;
        public static final int pic1 = 2130837517;
        public static final int pic2 = 2130837518;
        public static final int pic3 = 2130837519;
        public static final int pic4 = 2130837520;
        public static final int pic5 = 2130837521;
        public static final int pic6 = 2130837522;
        public static final int r_taru = 2130837523;
        public static final int r_taru2 = 2130837524;
        public static final int r_taru3 = 2130837525;
        public static final int r_taru4 = 2130837526;
        public static final int r_taru5 = 2130837527;
        public static final int s_taru = 2130837528;
        public static final int smoke = 2130837529;
        public static final int taru = 2130837530;
        public static final int taru2 = 2130837531;
        public static final int taru3 = 2130837532;
        public static final int taru4 = 2130837533;
        public static final int taru5 = 2130837534;
    }

    public static final class id {
        public static final int ad = 2131165187;
        public static final int diarylist = 2131165184;
        public static final int footer = 2131165188;
        public static final int header = 2131165185;
        public static final int imageView1 = 2131165190;
        public static final int main = 2131165189;
        public static final int textView1 = 2131165186;
    }

    public static final class layout {
        public static final int actionview = 2130903040;
        public static final int main = 2130903041;
    }

    public static final class raw {
        public static final int death = 2130968576;
        public static final int go = 2130968577;
    }

    public static final class string {
        public static final int app_name = 2131099648;
    }
}
