package com.derax.rocketboy;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class ActionGame extends Activity {
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.actionview);
        ((LinearLayout) findViewById(R.id.main)).addView(new ActionView(this));
        AdView adView = new AdView(this, AdSize.BANNER, "a14e43c9c5d7b4f");
        ((LinearLayout) findViewById(R.id.ad)).addView(adView);
        adView.loadAd(new AdRequest());
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }
}
