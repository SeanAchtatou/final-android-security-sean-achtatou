package com.vpon.adon.android.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import com.vpon.adon.android.exception.ServiceUnavailableException;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.LinkedList;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public final class IOUtils {
    private static IOUtils instance;
    private String cookie = null;
    private ThreadSafeClientConnManager manager;
    private HttpParams params;

    private IOUtils() {
        HttpParams params2 = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(params2, 30000);
        HttpConnectionParams.setSoTimeout(params2, 30000);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        this.manager = new ThreadSafeClientConnManager(params2, schemeRegistry);
    }

    public static final synchronized IOUtils instance() {
        IOUtils iOUtils;
        synchronized (IOUtils.class) {
            if (instance == null) {
                instance = new IOUtils();
            }
            iOUtils = instance;
        }
        return iOUtils;
    }

    public BufferedInputStream connectHttpServer(String url) throws ClientProtocolException, IOException {
        BufferedInputStream bis = new BufferedInputStream(new DefaultHttpClient(this.manager, this.params).execute(new HttpGet(url)).getEntity().getContent());
        this.manager.closeExpiredConnections();
        return bis;
    }

    public Bitmap downloadBitmap(String url) throws ClientProtocolException, IOException, IllegalStateException, Exception {
        HttpClient httpClient = new DefaultHttpClient(this.manager, this.params);
        HttpGet httpGet = new HttpGet(url);
        HttpResponse resp = httpClient.execute(httpGet);
        if (resp.getStatusLine().getStatusCode() != 200) {
            Log.i("resp.getStatusLine().getStatusCode()", String.valueOf(resp.getStatusLine().getStatusCode()));
            throw new ServiceUnavailableException("getStatusCode() != HttpStatus.SC_OK");
        }
        Bitmap bitmap = BitmapFactory.decodeStream(new BufferedInputStream(resp.getEntity().getContent(), 8192));
        httpGet.abort();
        return bitmap;
    }

    public String getRedirectPackCryptoString(String adRedirectPackString) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, NoSuchProviderException {
        return URLEncoder.encode(Base64.encodeToString(CryptUtils.encryptData(adRedirectPackString.getBytes(XmlConstant.DEFAULT_ENCODING), CryptUtils.getRsaEncryptCipher(CryptUtils.getRsaPubKey(Base64.decode(CryptUtils.decTripleDES("http://www.vpon.com/v/index.jsp88623698333service@vpon.com", "O4+gWecUjeNBCuwRbLprfpbXPUGl3A/Ac8HJxnr8A85UqiTMIJ50RxEkEp/urCSUrkICRuVt0JVss0komwdQQBz94D5XSKE3zlWSMplh+Al5BR99slEzLIZU7TJ7Upk/ZmZlY3UpXmRMX+zLEqybApfAlLEr67QoJBdbEpjHsnWxx5nZwHAdeYXwtgSFVGgtydkZBR+PGEjriyWhWkSTgFbO8yJTMxirdEtldU+604SemOFub2WqVHC86lrN+pnvfO9pfs+d99QgMno8MRVEX0EqTnf+qiVIJrCOJbv0cj7SWws7LTgWrGssmur08FEyY0mPCqUqY0/AEkTnwg4PkiOwny3y3hPVvRipMDxBUiTeOIC9U8UVGPD/T5bX+Qrrb2CV6DkfucIU0Ie5q8iC8KCVI6Uh4wu0SDavbqaZH5Dhtb2zLnmOF4qwBF8We4gkRJ63bosreSm1uGn3UoC2CRvOJTyyf8CcCijLnEm0V7Ob4GXFCJlnPKVMsJxbnlQV+ik9IKny/BGkE5j6Gqq1HQ=="))))), false));
    }

    public HttpResponse connectJSONServer(String json, String uriString, String sdkVersion) throws ParseException, IOException, ServiceUnavailableException, URISyntaxException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, Exception {
        String cliPri = CryptUtils.decTripleDES("http://www.vpon.com/v/index.jsp88623698333service@vpon.com", "a61/A+5uKrsDvf3DuIoLifhEx9ryIUDH5wqYdlut/1a06lYILQQ6J0uxEcQdkPmcgRNrz1jYXZpLGl1cc3bathCcxbM9PxHlXgKzcR6gn2bih9EmPAHQMmU86K3Vi7S5Q011wV6QdRrfLCL4MXRGlHySpk8ER0aEJ0vHRGM9dLtGgqZTAfGNdLVJtcId0JaAzGEZdngBFb+AqySU8SHmqyjDwbVFq0OI6QXZJz6Z27cdJaXYqDdm5FMk0FNbLvrYI+OKUfmn5kJi4yxQDimNJrHWVh7tT6Bo3lAj9H7trZuvKMgNo9opQM7R0tNq2/qU9InYKfqo4/gMBinogkQNtsq+HrC9gm8Ek8H/Re9cKANKVMZvTMN3TuilswjAkdKKzEStYuiWF0grCtkk1bh8/F4C1h9nMvTFJeEn0FlmxZ9K5mT8Y2idP2yiQZL1eHMB1BgYCGKux6nJ1LoZjFKaEWbomzDmpWlPkrWJN97Lw5xkw/7JsiBierbqhPx0UOVj2iaU3XhM6C5Hj7K45mXaOrTkk8vHOM/XP2VJ+pVwflFU48BD5lzE/AuxSRxo0tLxV2OFHyAQ32nLawm1HHNItyl5swv60fqoSDThtRxrPaZTDh9XWFEBjv04bbbgukgBUyMwPGZmP1P18fTn3S1qgiwOYTZwtlRMtdYGerTCwo7Qj7u/BEIfgzNV20BsRFQn71BBOU7XuaWjVL78Ji1oYGoTmAaMJAP2xhJyp8LpU28gPfLu++ZoPxdMfJAuEdlqp/oUHCGlAt3i7EY4Ds3+Y2lDltU0YJvKw1jfaw56ay5OWFemG5tjP3FGwBKNgO9PKkktDP978wgpAC/kFRviryVSKR35zOkvvAtfP8HTLrINI5WdZDDAg/CZqP4K12w3N95gGaJzuO5xBKfxhyG/LfKI40aFH2ITq5zZaKw0mPsRW+vGqvQg8onjrqlEAAZBeR9mDrYtjV6r2fao892i5IG7LUwjELxXQxGjXkdFoHwnRXEhULAY1e3clpNfECzcNyGh2w5quYuFjsq/aKaYCZlMV2ULAbM+f8NZ9GBdXOh1IjwtPqAtZ0MjuHylz/1q1jPmuYaqt8saqLQtwX8HVsAr5jhQFSRqz55+iOVkMvJuevfTw6o6QYarPhv4/IFaWF218DDNvvpg9GrVoZCLtg/3W2DMQ6Zedhc7nBepzah6E9zc+gc0ZyIJHNRTVhApnlL+IcB0522w/Wwgeu40w761UhB+tQRW6H9pUYxBxuz3p2CB1cdvAJaKTrb9kGRoR5pjOSx+ER4z2OaMKosnrPlARzveY7TiaVc1BMd6SZcZMahV40OwfLb4KrdeG9GDq+Hiwy7JL8CECVNfu1e8hBk8GBOC978OmBlsbCAQp5jmhbDKbpBUHhMOInjTTNto8dSXdI1CcV7Tnw7JCM64872UNIGjZfKrEHuNAHwkLDPYOMInuyIFU4BwKXSzEIpfqB8pALWwkgJlZpTsq3tXppAMLbyClHf7RAkmTu+eq6jIvQ5YnX9+ldKhaWqHFXMdMYotWaMCh2kjC3Tzd53CrIfSfb9frEwHQobwIrFDimAwrVcoDWf7A8u279/+JRUTc+YZxYYPtc/jVMbZQAUqfw/o7bUVyCQ8dOfzWt85+aFDsuLQZvGiL3kpquYEyarYJygCXIYd0NR+vp4WN7jONQD54+9Qhw43l5sqcEr0VEXdLIHgJhTXPvk79or8UfEpNiwA8X9GLr7UiK44Oq0oCTr7mOzruZwMK32lCOABAjusqTF6xYFMrj/3xfq7crZA2gluPN5TW2qdYA9D6ROKCferNGc73brV1NtJdtEDlTiTVCq88L1TfFgXxsBJL8Yz0Brp3q7TlNAcUp5BSCJUK5M82aCSK9ofYwJVbizCWOb0xQ9Jk4UUL/6w8Nd7m53jUqTfpDeF9CVQWoTWhkoxfROBosgqUHQRFZUf4xu+DsEj3tbFzF00oTBalXFFoPnQX/dYB4SCbqlaZ4J4YsPbHupB18lPTcjQMybeBxb9NS2oYCbZX56870chIwchmMfxGFLzIhdGC7IP1SAdLXMhVm61yRre8jtmqf33v5m37OgeCA9s3c00oW/b66lOWjvSFiHTqjNIHGOYjJWhn2tuyxqM39hvKvedUWEutZokJ0ygZt7yLsZ6BJgQTwyuJ5P9");
        String json2 = Base64.encodeToString(CryptUtils.encryptData(json.getBytes(XmlConstant.DEFAULT_ENCODING), CryptUtils.getRsaEncryptCipher(CryptUtils.getRsaPubKey(Base64.decode(CryptUtils.decTripleDES("http://www.vpon.com/v/index.jsp88623698333service@vpon.com", "O4+gWecUjeNBCuwRbLprfpbXPUGl3A/Ac8HJxnr8A85UqiTMIJ50RxEkEp/urCSUrkICRuVt0JVss0komwdQQBz94D5XSKE3zlWSMplh+Al5BR99slEzLIZU7TJ7Upk/ZmZlY3UpXmRMX+zLEqybApfAlLEr67QoJBdbEpjHsnWxx5nZwHAdeYXwtgSFVGgtydkZBR+PGEjriyWhWkSTgFbO8yJTMxirdEtldU+604SemOFub2WqVHC86lrN+pnvfO9pfs+d99QgMno8MRVEX0EqTnf+qiVIJrCOJbv0cj7SWws7LTgWrGssmur08FEyY0mPCqUqY0/AEkTnwg4PkiOwny3y3hPVvRipMDxBUiTeOIC9U8UVGPD/T5bX+Qrrb2CV6DkfucIU0Ie5q8iC8KCVI6Uh4wu0SDavbqaZH5Dhtb2zLnmOF4qwBF8We4gkRJ63bosreSm1uGn3UoC2CRvOJTyyf8CcCijLnEm0V7Ob4GXFCJlnPKVMsJxbnlQV+ik9IKny/BGkE5j6Gqq1HQ=="))))), false);
        URI uri = new URI(uriString);
        HttpClient httpClient = new DefaultHttpClient(this.manager, this.params);
        HttpPost httpPost = new HttpPost(uri);
        httpPost.setHeader(new BasicHeader("Content-Type", "application/x-www-form-urlencoded"));
        List<NameValuePair> params2 = new LinkedList<>();
        params2.add(new BasicNameValuePair("data", json2));
        httpPost.setEntity(new UrlEncodedFormEntity(params2, XmlConstant.DEFAULT_ENCODING));
        httpPost.setHeader("X-ADON-SDK_NAME", "Android");
        httpPost.setHeader("X-ADON-SDK_VERSION", sdkVersion);
        if (this.cookie != null) {
            httpPost.setHeader("Cookie", this.cookie);
        }
        HttpResponse resp = httpClient.execute(httpPost);
        if (resp.getStatusLine().getStatusCode() != 200) {
            Log.i("getStatusCode", String.valueOf(resp.getStatusLine().getStatusCode()));
            throw new ServiceUnavailableException("getStatusCode() != HttpStatus.SC_OK");
        }
        Header[] headers = resp.getHeaders("Set-Cookie");
        int length = headers.length;
        for (int i = 0; i < length; i++) {
            this.cookie = headers[i].getValue();
        }
        return resp;
    }
}
