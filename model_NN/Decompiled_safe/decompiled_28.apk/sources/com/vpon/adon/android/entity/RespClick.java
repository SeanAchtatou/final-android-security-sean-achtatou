package com.vpon.adon.android.entity;

import java.io.Serializable;

public class RespClick implements Serializable {
    private static final long serialVersionUID = -7073172895462954975L;
    private String adId;
    private String cellID;
    private int clickType;
    private String lac;
    private double lat;
    private String licenseKey;
    private double lon;
    private String mcc;
    private String mnc;
    private String time;

    public String getLicenseKey() {
        return this.licenseKey;
    }

    public void setLicenseKey(String licenseKey2) {
        this.licenseKey = licenseKey2;
    }

    public double getLat() {
        return this.lat;
    }

    public void setLat(double lat2) {
        this.lat = lat2;
    }

    public double getLon() {
        return this.lon;
    }

    public void setLon(double lon2) {
        this.lon = lon2;
    }

    public String getCellID() {
        return this.cellID;
    }

    public void setCellID(String cellID2) {
        this.cellID = cellID2;
    }

    public String getLac() {
        return this.lac;
    }

    public void setLac(String lac2) {
        this.lac = lac2;
    }

    public String getMnc() {
        return this.mnc;
    }

    public void setMnc(String mnc2) {
        this.mnc = mnc2;
    }

    public String getMcc() {
        return this.mcc;
    }

    public void setMcc(String mcc2) {
        this.mcc = mcc2;
    }

    public String getStrMcc() {
        return this.mcc;
    }

    public void setStrMcc(String strMcc) {
        this.mcc = strMcc;
    }

    public String getTime() {
        return this.time;
    }

    public void setTime(String time2) {
        this.time = time2;
    }

    public String getAdId() {
        return this.adId;
    }

    public void setAdId(String adId2) {
        this.adId = adId2;
    }

    public int getClickType() {
        return this.clickType;
    }

    public void setClickType(int clickType2) {
        this.clickType = clickType2;
    }
}
