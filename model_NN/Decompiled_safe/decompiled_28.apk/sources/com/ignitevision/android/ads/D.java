package com.ignitevision.android.ads;

import android.view.animation.DecelerateInterpolator;

final class D implements Runnable {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ AdView a;
    /* access modifiers changed from: private */
    public h b;
    /* access modifiers changed from: private */
    public h c;

    public D(AdView adView, h hVar) {
        this.a = adView;
        this.b = hVar;
    }

    public void run() {
        this.c = this.a.j;
        if (this.c != null) {
            this.c.setVisibility(8);
        }
        this.b.setVisibility(0);
        G g = new G(90.0f, 0.0f, ((float) this.a.getWidth()) / 2.0f, ((float) this.a.getHeight()) / 2.0f, -0.4f * ((float) this.a.getWidth()), false);
        g.setDuration(700);
        g.setFillAfter(true);
        g.setInterpolator(new DecelerateInterpolator());
        g.setAnimationListener(new E(this));
        this.a.startAnimation(g);
    }
}
