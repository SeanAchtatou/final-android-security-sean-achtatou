package com.ignitevision.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;

final class s {
    protected static Typeface a = Typeface.create(Typeface.SANS_SERIF, 1);
    protected static float b = 13.0f;
    protected static Typeface c = Typeface.create(Typeface.SANS_SERIF, 0);
    protected static float d = 9.5f;
    protected static BitmapDrawable e;
    protected static BitmapDrawable f;
    protected static BitmapDrawable g;
    protected static int h = -1;
    protected static Bitmap i = null;

    private s() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ignitevision.android.ads.s.a(android.graphics.Rect, int, int, boolean):android.graphics.drawable.BitmapDrawable
     arg types: [android.graphics.Rect, int, int, int]
     candidates:
      com.ignitevision.android.ads.s.a(android.graphics.Canvas, android.graphics.Rect, int, int):void
      com.ignitevision.android.ads.s.a(android.graphics.Rect, int, int, boolean):android.graphics.drawable.BitmapDrawable */
    private static BitmapDrawable a(Rect rect, int i2, int i3) {
        return a(rect, i2, i3, false);
    }

    private static BitmapDrawable a(Rect rect, int i2, int i3, boolean z) {
        try {
            Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            a(canvas, rect, i2, i3);
            if (z) {
                a(canvas, rect);
            }
            return new BitmapDrawable(createBitmap);
        } catch (Throwable th) {
            return null;
        }
    }

    private static void a(Canvas canvas, Rect rect) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(-1147097);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3.0f);
        paint.setPathEffect(new CornerPathEffect(3.0f));
        Path path = new Path();
        path.addRoundRect(new RectF(rect), 3.0f, 3.0f, Path.Direction.CW);
        canvas.drawPath(path, paint);
    }

    private static void a(Canvas canvas, Rect rect, int i2, int i3) {
        Paint paint = new Paint();
        paint.setColor(i2);
        paint.setAntiAlias(true);
        canvas.drawRect(rect, paint);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb(127, Color.red(i3), Color.green(i3), Color.blue(i3)), i3});
        int height = ((int) (((double) rect.height()) * 0.4375d)) + rect.top;
        gradientDrawable.setBounds(rect.left, rect.top, rect.right, height);
        gradientDrawable.draw(canvas);
        Rect rect2 = new Rect(rect.left, height, rect.right, rect.bottom);
        Paint paint2 = new Paint();
        paint2.setColor(i3);
        canvas.drawRect(rect2, paint2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ignitevision.android.ads.s.a(android.graphics.Rect, int, int, boolean):android.graphics.drawable.BitmapDrawable
     arg types: [android.graphics.Rect, int, int, int]
     candidates:
      com.ignitevision.android.ads.s.a(android.graphics.Canvas, android.graphics.Rect, int, int):void
      com.ignitevision.android.ads.s.a(android.graphics.Rect, int, int, boolean):android.graphics.drawable.BitmapDrawable */
    public static void a(Rect rect, Context context, int i2) {
        if (rect != null) {
            e = a(rect, -1, i2);
            g = a(rect, -1147097, -19456);
            f = a(rect, -1, i2, true);
        }
    }
}
