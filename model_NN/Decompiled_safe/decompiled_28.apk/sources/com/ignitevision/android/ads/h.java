package com.ignitevision.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.energysource.szj.embeded.AdManager;

class h extends RelativeLayout {
    private Drawable a;
    /* access modifiers changed from: private */
    public C0026a b;
    /* access modifiers changed from: private */
    public long c = 1000;
    /* access modifiers changed from: private */
    public long d = 1000;
    /* access modifiers changed from: private */
    public TextView e;
    private TextView f;
    /* access modifiers changed from: private */
    public ImageView g;
    /* access modifiers changed from: private */
    public ProgressBar h;
    private n i;
    private int j;
    /* access modifiers changed from: private */
    public boolean k;
    private boolean l = false;
    /* access modifiers changed from: private */
    public Context m = null;

    public h(Context context, C0026a aVar, int i2) {
        super(context);
        this.m = context;
        this.b = aVar;
        this.h = null;
        this.k = false;
        this.j = 8;
        if (aVar != null) {
            setFocusable(true);
            setClickable(true);
            if (aVar.a() == C0027b.Banner) {
                this.l = true;
                this.c = (long) (i2 < 1 ? AdManager.AD_FILL_PARENT : i2 * AdManager.AD_FILL_PARENT);
                g();
            } else {
                this.l = false;
                this.c = (long) (i2 < 1 ? AdManager.AD_FILL_PARENT : i2 * AdManager.AD_FILL_PARENT);
                f();
            }
        }
        if (this.l) {
            return;
        }
        if (s.i != null) {
            ImageView imageView = new ImageView(context);
            imageView.setImageBitmap(s.i);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(s.i.getWidth(), s.i.getHeight());
            layoutParams.setMargins(0, 0, this.j, this.j);
            layoutParams.addRule(11);
            layoutParams.addRule(12);
            imageView.setLayoutParams(layoutParams);
            addView(imageView);
            return;
        }
        this.f = new TextView(context);
        this.f.setGravity(5);
        this.f.setText("Tinmoo");
        this.f.setTypeface(s.c);
        this.f.setTextColor(s.h);
        this.f.setTextSize(s.d);
        this.f.setId(3);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.setMargins(0, 0, this.j, this.j);
        layoutParams2.addRule(11);
        layoutParams2.addRule(12);
        this.f.setLayoutParams(layoutParams2);
        addView(this.f);
    }

    /* access modifiers changed from: private */
    public void a(String[] strArr, int i2) {
        if (strArr.length <= 0) {
            return;
        }
        if (i2 >= strArr.length) {
            a(strArr, 0);
        } else if (strArr[i2] == null) {
            a(strArr, i2 + 1);
        } else {
            postDelayed(new o(this, strArr, i2), this.c);
        }
    }

    private void f() {
        if (this.b != null) {
            Bitmap f2 = this.b.f();
            if (f2 != null) {
                this.j = (48 - f2.getHeight()) / 2;
                this.g = new ImageView(this.m);
                this.g.setImageBitmap(f2);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(f2.getWidth(), f2.getHeight());
                layoutParams.setMargins(this.j, this.j, 0, this.j);
                this.g.setLayoutParams(layoutParams);
                this.g.setId(1);
                addView(this.g);
                this.h = new ProgressBar(this.m);
                this.h.setIndeterminate(true);
                this.h.setId(1);
                this.h.setLayoutParams(layoutParams);
                this.h.setVisibility(4);
                addView(this.h);
            }
            this.e = new TextView(this.m);
            this.e.setTypeface(s.a);
            this.e.setTextColor(s.h);
            this.e.setTextSize(s.b);
            this.e.setId(2);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
            if (f2 != null) {
                layoutParams2.addRule(1, 1);
            }
            layoutParams2.setMargins(this.j, this.j, this.j, this.j);
            layoutParams2.addRule(11);
            layoutParams2.addRule(10);
            this.e.setLayoutParams(layoutParams2);
            addView(this.e);
        }
    }

    private void g() {
        if (this.b != null) {
            this.i = new n(this, this.m);
            addView(this.i);
            setBackgroundDrawable(this.b.e());
        }
    }

    private void h() {
        if (this.b != null && isPressed()) {
            setPressed(false);
            if (!this.k) {
                this.k = true;
                if (this.g != null) {
                    AnimationSet animationSet = new AnimationSet(true);
                    float width = ((float) this.g.getWidth()) / 2.0f;
                    float height = ((float) this.g.getHeight()) / 2.0f;
                    ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.2f, 1.0f, 1.2f, width, height);
                    scaleAnimation.setDuration(200);
                    animationSet.addAnimation(scaleAnimation);
                    ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.2f, 0.001f, 1.2f, 0.001f, width, height);
                    scaleAnimation2.setDuration(299);
                    scaleAnimation2.setStartOffset(200);
                    scaleAnimation2.setAnimationListener(new k(this));
                    animationSet.addAnimation(scaleAnimation2);
                    postDelayed(new l(this), 500);
                    this.g.startAnimation(animationSet);
                    return;
                }
                d();
            }
        }
    }

    /* access modifiers changed from: protected */
    public C0026a a() {
        return this.b;
    }

    public void b() {
        post(new i(this));
    }

    public void c() {
        post(new j(this));
    }

    public void d() {
        String c2 = this.b.c();
        if (c2 != null && !c2.equals("")) {
            new m(this, c2).start();
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            setPressed(true);
        } else if (action == 2) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            int left = getLeft();
            int top = getTop();
            int right = getRight();
            int bottom = getBottom();
            if (x < ((float) left) || x > ((float) right) || y < ((float) top) || y > ((float) bottom)) {
                setPressed(false);
            } else {
                setPressed(true);
            }
        } else if (action == 1) {
            if (isPressed()) {
                h();
            }
            setPressed(false);
        } else if (action == 3) {
            setPressed(false);
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            setPressed(true);
        } else if (motionEvent.getAction() == 1) {
            if (hasFocus()) {
                h();
            }
            setPressed(false);
        }
        return super.onTrackballEvent(motionEvent);
    }

    public void e() {
        if (this.b == null) {
            return;
        }
        if (this.b.a() == C0027b.Text) {
            if (this.b.g() != null) {
                int length = this.b.g().length;
                if (length > 0) {
                    String[] strArr = new String[(length + 1)];
                    strArr[0] = this.b.d();
                    for (int i2 = 0; i2 < length; i2++) {
                        strArr[i2 + 1] = this.b.g()[i2];
                    }
                    new Thread(new o(this, strArr, 0)).start();
                    return;
                }
                return;
            }
            new Thread(new q(this)).start();
        } else if (this.b.a() == C0027b.Banner && this.i != null) {
            this.i.a();
        }
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean z, int i2, Rect rect) {
        super.onFocusChanged(z, i2, rect);
        if (z) {
            setBackgroundDrawable(s.f);
        } else {
            setBackgroundDrawable(s.e);
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 66 || i2 == 23) {
            setPressed(true);
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 == 66 || i2 == 23) {
            h();
        }
        setPressed(false);
        return super.onKeyUp(i2, keyEvent);
    }

    public void setPressed(boolean z) {
        Drawable drawable;
        if ((!z || !this.k) && isPressed() != z) {
            BitmapDrawable bitmapDrawable = s.e;
            if (z) {
                this.a = getBackground();
                drawable = s.g;
            } else {
                drawable = this.a;
            }
            setBackgroundDrawable(drawable);
            super.setPressed(z);
            invalidate();
        }
    }
}
