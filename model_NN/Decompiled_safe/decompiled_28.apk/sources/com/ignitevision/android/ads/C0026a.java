package com.ignitevision.android.ads;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import java.util.Arrays;

/* renamed from: com.ignitevision.android.ads.a  reason: case insensitive filesystem */
class C0026a {
    private C0027b a;
    private Long b;
    private Bitmap c;
    private String d;
    private String e;
    private String[] f;
    private Drawable g;
    private Drawable[] h;

    C0026a() {
    }

    public C0027b a() {
        return this.a;
    }

    public void a(Bitmap bitmap) {
        this.c = bitmap;
    }

    public void a(Drawable drawable) {
        this.g = drawable;
    }

    public void a(C0027b bVar) {
        this.a = bVar;
    }

    public void a(Long l) {
        this.b = l;
    }

    public void a(String str) {
        this.d = str;
    }

    public void a(Drawable[] drawableArr) {
        this.h = drawableArr;
    }

    public void a(String[] strArr) {
        this.f = strArr;
    }

    public Long b() {
        return this.b;
    }

    public void b(String str) {
        this.e = str;
    }

    public String c() {
        return this.d;
    }

    public String d() {
        return this.e;
    }

    public Drawable e() {
        return this.g;
    }

    public Bitmap f() {
        return this.c;
    }

    public String[] g() {
        return this.f;
    }

    public Drawable[] h() {
        return this.h;
    }

    public String toString() {
        return "Ad [banner=" + this.g + ", banners=" + Arrays.toString(this.h) + ", bidId=" + this.b + ", icon=" + this.c + ", targetUrl=" + this.d + ", text=" + this.e + ", texts=" + Arrays.toString(this.f) + ", type=" + this.a + "]";
    }
}
