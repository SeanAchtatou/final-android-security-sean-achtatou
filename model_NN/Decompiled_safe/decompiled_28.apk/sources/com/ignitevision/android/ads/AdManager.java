package com.ignitevision.android.ads;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import cn.domob.android.ads.DomobAdManager;

public class AdManager {
    private static boolean a;
    private static String b;
    private static boolean c;
    private static String d;
    private static String e;

    private AdManager() {
    }

    static void a(Context context) {
        if (!c) {
            c = true;
            try {
                ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
                if (applicationInfo != null && applicationInfo.metaData != null) {
                    String string = applicationInfo.metaData.getString("TINMOO_PUBLISHER_ID");
                    if (Log.isLoggable("TinmooSDK", 3)) {
                        Log.d("TinmooSDK", "Publisher Key read from AndroidManifest.xml is " + string);
                    }
                    if (b == null && string != null) {
                        if (!string.equals("1fh3l22kos2l71m8pht1wstoqq19es197axj8i5c4kwzshyczdz1n5yuhrzcuyz") || (!context.getPackageName().equals("com.ignitevision.android.ads") && !context.getPackageName().equals("com.example.ignitevision.simple"))) {
                            setPublisherKey(string);
                            return;
                        }
                        Log.i("TinmooSDK", "This is a sample application so allowing sample publisher Key.");
                        b = string;
                    }
                }
            } catch (Exception e2) {
            }
        }
    }

    protected static void a(String str) {
        throw new IllegalArgumentException(str);
    }

    protected static boolean a() {
        return "unknown".equals(Build.BOARD) && "generic".equals(Build.DEVICE) && "generic".equals(Build.BRAND);
    }

    protected static String b(Context context) {
        if (d == null) {
            String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (string == null || a()) {
                d = DomobAdManager.TEST_EMULATOR;
            } else {
                d = b(string);
            }
        }
        return d;
    }

    private static String b(String str) {
        return str;
    }

    protected static String c(Context context) {
        if (e == null && context != null) {
            if (a()) {
                e = DomobAdManager.TEST_EMULATOR;
            } else {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                if (telephonyManager != null) {
                    e = telephonyManager.getDeviceId();
                    if (e != null) {
                        e = e.toLowerCase();
                    }
                }
            }
        }
        return e;
    }

    public static String getPublisherKey(Context context) {
        if (b == null) {
            a(context);
        }
        if (b == null && Log.isLoggable("TinmooSDK", 6)) {
            Log.e("TinmooSDK", "getPublisherKey returning null publisher id.  Please set the publisher id in AndroidManifest.xml or using AdManager.setPublisherKey(String)");
        }
        return b;
    }

    public static boolean isTest() {
        return a;
    }

    public static void setPublisherKey(String str) {
        if (str == null) {
            a("Publisher key should not be set to null.");
        }
        if (str.equalsIgnoreCase("1fh3l22kos2l71m8pht1wstoqq19es197axj8i5c4kwzshyczdz1n5yuhrzcuyz")) {
            a("This publisher key is use for simple application.");
        }
        b = str;
    }

    public static void setTest(boolean z) {
        a = z;
    }
}
