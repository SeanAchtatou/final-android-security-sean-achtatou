package com.ignitevision.android.ads;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

/* renamed from: com.ignitevision.android.ads.f  reason: case insensitive filesystem */
class C0031f implements LocationListener {
    final /* synthetic */ C0030e a;
    private final /* synthetic */ LocationManager b;

    C0031f(C0030e eVar, LocationManager locationManager) {
        this.a = eVar;
        this.b = locationManager;
    }

    public void onLocationChanged(Location location) {
        this.a.o = location;
        this.a.n = System.currentTimeMillis();
        this.a.g();
        this.b.removeUpdates(this);
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
