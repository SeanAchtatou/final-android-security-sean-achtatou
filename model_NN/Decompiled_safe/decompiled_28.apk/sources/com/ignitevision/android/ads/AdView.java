package com.ignitevision.android.ads;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.RelativeLayout;
import com.energysource.szj.embeded.AdManager;
import org.json.JSONObject;

public class AdView extends RelativeLayout {
    private int a;
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public int g;
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public h j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public boolean l;
    private boolean m;
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public boolean o;
    private long p;
    /* access modifiers changed from: private */
    public Context q;
    /* access modifiers changed from: private */
    public AdListener r;
    /* access modifiers changed from: private */
    public Handler s;
    private C t;
    private final int u;
    private final float v;

    public AdView(Context context) {
        this(context, null, 0);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.g = -1;
        this.h = -16777216;
        this.i = 0;
        this.s = new Handler();
        this.u = 700;
        this.v = -0.4f;
        this.q = context;
        this.n = true;
        setFocusable(true);
        setDescendantFocusability(262144);
        setClickable(true);
        a(attributeSet);
        new Thread(new w(this, null)).start();
    }

    private void a() {
        if (this.t != null) {
            this.t.a = true;
            this.t = null;
        }
    }

    private void a(AttributeSet attributeSet) {
        this.c = -1;
        this.d = -16777216;
        this.b = 0;
        if (attributeSet != null) {
            try {
                String str = "http://schemas.android.com/apk/res/" + this.q.getPackageName();
                this.c = attributeSet.getAttributeUnsignedIntValue(str, "textColor", -1);
                this.d = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", -16777216);
                this.b = attributeSet.getAttributeIntValue(str, "refreshInterval", 0);
            } catch (Exception e2) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(h hVar) {
        this.j = hVar;
        if (this.m) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(233);
            alphaAnimation.startNow();
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new AccelerateInterpolator());
            startAnimation(alphaAnimation);
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        synchronized (this) {
            if (z) {
                if (this.a > 0 && getVisibility() == 0) {
                    a();
                    this.t = new C(this);
                    this.s.postDelayed(this.t, (long) this.a);
                }
            }
            if (!z || this.a == 0) {
                a();
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return false;
        }
        try {
            if (!jSONObject.isNull("textColor")) {
                this.g = Color.parseColor("#" + jSONObject.getString("textColor"));
            }
        } catch (Exception e2) {
        }
        try {
            if (!jSONObject.isNull("backgroundColor")) {
                this.h = Color.parseColor("#" + jSONObject.getString("backgroundColor"));
            }
        } catch (Exception e3) {
        }
        try {
            if (!jSONObject.isNull("refresh")) {
                this.i = jSONObject.getInt("refresh");
            }
        } catch (Exception e4) {
        }
        try {
            if (!jSONObject.isNull("textRefresh")) {
                this.e = jSONObject.getInt("textRefresh");
            }
        } catch (Exception e5) {
        }
        try {
            if (!jSONObject.isNull("imageRefresh")) {
                this.f = jSONObject.getInt("imageRefresh");
            }
        } catch (Exception e6) {
        }
        try {
            if (!jSONObject.isNull("override") && jSONObject.getInt("override") == 1) {
                this.o = true;
            }
        } catch (Exception e7) {
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void b(h hVar) {
        hVar.setVisibility(8);
        G g2 = new G(0.0f, -90.0f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, -0.4f * ((float) getWidth()), true);
        g2.setDuration(700);
        g2.setFillAfter(true);
        g2.setInterpolator(new AccelerateInterpolator());
        g2.setAnimationListener(new v(this, hVar));
        startAnimation(g2);
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        if (super.getVisibility() != 0) {
            Log.d("TinmooSDK", "Cannot requestFreshAd() when the AdView is not visible.");
        } else if (this.l) {
            Log.d("TinmooSDK", "Ignoring requestFreshAd() because we are requesting an ad right now already.");
        } else {
            this.l = true;
            this.p = SystemClock.uptimeMillis();
            synchronized (this) {
                new Thread(new t(this, z)).start();
            }
        }
    }

    public AdListener getAdListener() {
        return this.r;
    }

    public int getBackgroundColor() {
        return this.d;
    }

    public int getRequestInterval() {
        return this.a / AdManager.AD_FILL_PARENT;
    }

    public int getTextColor() {
        return this.c;
    }

    public boolean hasAd() {
        return this.j != null;
    }

    public void load(boolean z) {
        AdManager.setTest(!z);
        requestFreshAd();
        Log.w("TinmooSDK", "Deprecated method load was called. See JavaDoc for instructions to remove.");
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        this.m = true;
        a(true);
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.m = false;
        a(false);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        setMeasuredDimension(getMeasuredWidth(), 48);
    }

    public void onWindowFocusChanged(boolean z) {
        a(z);
    }

    public void requestFreshAd() {
        if (!this.k) {
            Log.d("TinmooSDK", "Ignoring requestFreshAd. AdView is not ready for request.");
        } else if (this.n) {
            Log.d("TinmooSDK", "Ignoring requestFreshAd. Request interval overridden by the system.");
        } else {
            long uptimeMillis = (SystemClock.uptimeMillis() - this.p) / 1000;
            if (uptimeMillis <= 0 || uptimeMillis >= 15) {
                b(false);
            } else {
                Log.d("TinmooSDK", "Ignoring requestFreshAd. Refreshes must be at least 15 apart.");
            }
        }
    }

    public void setAdListener(AdListener adListener) {
        synchronized (this) {
            this.r = adListener;
        }
    }

    public void setBackgroundColor(int i2) {
        this.d = -16777216 | i2;
        s.a(new Rect(0, 0, com.madhouse.android.ads.AdView.AD_MEASURE_320, 48), getContext(), this.d);
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        if (z) {
            setVisibility(0);
        } else {
            setVisibility(8);
        }
    }

    public void setRequestInterval(int i2) {
        int i3;
        if (this.a != i2 * AdManager.AD_FILL_PARENT) {
            if (i2 <= 0) {
                i3 = 0;
            } else if (i2 < 15) {
                Log.d("TinmooSDK", "refreshInterval must be set more than 15s.");
                i3 = 15;
            } else if (i2 > 600) {
                Log.d("TinmooSDK", "refreshInterval must be set less than 600s.");
                i3 = 600;
            } else {
                i3 = i2;
            }
            this.b = i3;
            this.a = i3 * AdManager.AD_FILL_PARENT;
            this.n = true;
            if (i3 <= 0) {
                this.n = false;
                a();
            }
        }
    }

    public void setTextColor(int i2) {
        this.c = -16777216 | i2;
        s.h = this.c;
    }

    public void setVisibility(int i2) {
        if (super.getVisibility() != i2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i3 = 0; i3 < childCount; i3++) {
                    getChildAt(i3).setVisibility(i2);
                }
                super.setVisibility(i2);
                invalidate();
            }
        }
        a(i2 == 0);
    }
}
