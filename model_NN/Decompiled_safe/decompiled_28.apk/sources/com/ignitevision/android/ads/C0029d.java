package com.ignitevision.android.ads;

/* renamed from: com.ignitevision.android.ads.d  reason: case insensitive filesystem */
enum C0029d {
    Tinmoo("tinmoo_cache"),
    Icon("icon"),
    Banner("banner");
    
    private final String d;

    private C0029d(String str) {
        this.d = str;
    }

    public String a() {
        return this.d;
    }
}
