package com.ignitevision.android.ads;

import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import java.lang.ref.WeakReference;

class o implements Runnable {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ h a;
    /* access modifiers changed from: private */
    public String[] b;
    /* access modifiers changed from: private */
    public int c;
    private WeakReference d;

    public o(h hVar, String[] strArr, int i) {
        this.a = hVar;
        this.d = new WeakReference(hVar);
        this.b = strArr;
        this.c = i;
    }

    public void run() {
        try {
            if (((h) this.d.get()) != null) {
                TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, -40.0f, 0.0f);
                translateAnimation.setFillAfter(true);
                translateAnimation.setInterpolator(new LinearInterpolator());
                translateAnimation.setDuration(this.a.d);
                translateAnimation.setAnimationListener(new p(this));
                this.a.e.startAnimation(translateAnimation);
            }
        } catch (Exception e) {
        }
    }
}
