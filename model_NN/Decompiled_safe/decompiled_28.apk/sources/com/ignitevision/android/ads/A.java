package com.ignitevision.android.ads;

import android.util.Log;

class A implements Runnable {
    final /* synthetic */ AdView a;
    private C0026a b;

    public A(AdView adView, C0026a aVar) {
        this.a = adView;
        this.b = aVar;
    }

    public void run() {
        if (this.a.r == null) {
            return;
        }
        if (this.b == null || this.b.a() == C0027b.None) {
            try {
                this.a.r.onFailedToReceiveAd(this.a);
            } catch (Exception e) {
                Log.w("TinmooSDK", "Unhandled exception raised in your AdListener.onFailedToReceiveAd.", e);
            }
        } else {
            try {
                this.a.r.onReceiveAd(this.a);
            } catch (Exception e2) {
                Log.w("TinmooSDK", "Unhandled exception raised in your AdListener.onReceiveAd.", e2);
            }
        }
    }
}
