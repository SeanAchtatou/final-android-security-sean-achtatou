package com.ignitevision.android.ads;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;

class n extends View {
    final /* synthetic */ h a;
    private Drawable[] b;
    private AnimationDrawable c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n(h hVar, Context context) {
        super(context);
        this.a = hVar;
        this.c = null;
        this.c = new AnimationDrawable();
        if (!(hVar.b == null || hVar.b.e() == null)) {
            this.c.addFrame(hVar.b.e(), (int) hVar.c);
            this.b = hVar.b.h();
            if (this.b != null && this.b.length > 0) {
                int length = this.b.length;
                for (int i = 0; i < length; i++) {
                    if (this.b[i] != null) {
                        this.c.addFrame(this.b[i], (int) hVar.c);
                    }
                }
            }
        }
        if (this.c.getNumberOfFrames() > 1) {
            this.c.setOneShot(false);
        } else {
            this.c.setOneShot(true);
        }
        setBackgroundDrawable(this.c);
    }

    public void a() {
        this.a.setBackgroundDrawable(null);
        this.c.start();
    }
}
