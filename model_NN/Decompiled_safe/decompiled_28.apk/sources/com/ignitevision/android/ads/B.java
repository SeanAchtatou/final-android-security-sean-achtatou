package com.ignitevision.android.ads;

import android.util.Log;

class B implements Runnable {
    final /* synthetic */ AdView a;
    private int b;

    public B(AdView adView, int i) {
        this.a = adView;
        this.b = i;
    }

    public void run() {
        if (this.a.r == null) {
            return;
        }
        if (!this.a.hasAd() || this.b != 0) {
            try {
                this.a.r.onFailedToReceiveRefreshedAd(this.a);
            } catch (Exception e) {
                Log.w("TinmooSDK", "Unhandled exception raised in your AdListener.onFailedToReceiveRefreshAd.", e);
            }
        } else {
            try {
                this.a.r.onReceiveRefreshedAd(this.a);
            } catch (Exception e2) {
                Log.w("TinmooSDK", "Unhandled exception raised in your AdListener.onReceiveRefreshAd.", e2);
            }
        }
    }
}
