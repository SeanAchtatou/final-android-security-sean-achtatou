package com.ignitevision.android.ads;

import java.lang.ref.WeakReference;

class C implements Runnable {
    public boolean a;
    final /* synthetic */ AdView b;
    private WeakReference c;

    public C(AdView adView) {
        this.b = adView;
        this.c = new WeakReference(adView);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ignitevision.android.ads.AdView.b(com.ignitevision.android.ads.AdView, boolean):void
     arg types: [com.ignitevision.android.ads.AdView, int]
     candidates:
      com.ignitevision.android.ads.AdView.b(com.ignitevision.android.ads.AdView, int):void
      com.ignitevision.android.ads.AdView.b(com.ignitevision.android.ads.AdView, com.ignitevision.android.ads.h):void
      com.ignitevision.android.ads.AdView.b(com.ignitevision.android.ads.AdView, boolean):void */
    public void run() {
        try {
            AdView adView = (AdView) this.c.get();
            if (!this.a && adView != null) {
                this.b.b(false);
            }
        } catch (Exception e) {
        }
    }
}
