package com.ignitevision.android.ads;

import android.view.animation.Animation;

class E implements Animation.AnimationListener {
    final /* synthetic */ D a;

    E(D d) {
        this.a = d;
    }

    public void onAnimationEnd(Animation animation) {
        if (this.a.c != null) {
            this.a.a.removeView(this.a.c);
        }
        this.a.a.j = this.a.b;
        try {
            this.a.b.e();
        } catch (Exception e) {
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
