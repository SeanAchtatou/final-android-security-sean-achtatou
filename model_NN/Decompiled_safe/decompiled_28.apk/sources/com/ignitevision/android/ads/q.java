package com.ignitevision.android.ads;

import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import java.lang.ref.WeakReference;

class q implements Runnable {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ h a;
    private WeakReference b;

    public q(h hVar) {
        this.a = hVar;
        this.b = new WeakReference(hVar);
    }

    public void run() {
        try {
            if (((h) this.b.get()) != null) {
                TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, -40.0f, 0.0f);
                translateAnimation.setFillAfter(true);
                translateAnimation.setInterpolator(new LinearInterpolator());
                translateAnimation.setDuration(this.a.d);
                translateAnimation.setAnimationListener(new r(this));
                this.a.e.startAnimation(translateAnimation);
            }
        } catch (Exception e) {
        }
    }
}
