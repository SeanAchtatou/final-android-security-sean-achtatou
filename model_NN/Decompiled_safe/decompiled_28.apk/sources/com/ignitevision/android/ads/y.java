package com.ignitevision.android.ads;

class y implements Runnable {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ x a;
    private final /* synthetic */ boolean b;

    y(x xVar, boolean z) {
        this.a = xVar;
        this.b = z;
    }

    public void run() {
        if (this.a.a.o) {
            this.a.a.c = this.a.a.g;
            this.a.a.d = this.a.a.h;
            this.a.a.b = this.a.a.i;
        }
        this.a.a.setTextColor(this.a.a.c);
        this.a.a.setBackgroundColor(this.a.a.d);
        this.a.a.setRequestInterval(this.a.a.b);
        this.a.a.k = this.b;
        new Thread(new z(this)).start();
    }
}
