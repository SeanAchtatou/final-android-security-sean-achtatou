package com.ignitevision.android.ads;

import android.view.animation.Animation;

class v implements Animation.AnimationListener {
    final /* synthetic */ AdView a;
    private final /* synthetic */ h b;

    v(AdView adView, h hVar) {
        this.a = adView;
        this.b = hVar;
    }

    public void onAnimationEnd(Animation animation) {
        this.a.post(new D(this.a, this.b));
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
