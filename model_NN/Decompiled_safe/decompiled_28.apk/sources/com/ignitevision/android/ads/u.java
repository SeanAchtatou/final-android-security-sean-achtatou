package com.ignitevision.android.ads;

import android.widget.RelativeLayout;
import com.madhouse.android.ads.AdView;

class u implements Runnable {
    final /* synthetic */ t a;
    private final /* synthetic */ C0026a b;
    private final /* synthetic */ int c;

    u(t tVar, C0026a aVar, int i) {
        this.a = tVar;
        this.b = aVar;
        this.c = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ignitevision.android.ads.AdView.c(com.ignitevision.android.ads.AdView, boolean):void
     arg types: [com.ignitevision.android.ads.AdView, int]
     candidates:
      com.ignitevision.android.ads.AdView.c(com.ignitevision.android.ads.AdView, int):void
      com.ignitevision.android.ads.AdView.c(com.ignitevision.android.ads.AdView, com.ignitevision.android.ads.h):void
      com.ignitevision.android.ads.AdView.c(com.ignitevision.android.ads.AdView, boolean):void */
    public void run() {
        this.a.a.setBackgroundDrawable(s.e);
        h hVar = new h(this.a.a.q, this.b, this.b.a() == C0027b.Text ? this.a.a.e : this.b.a() == C0027b.Banner ? this.a.a.f : 0);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) AdView.AD_MEASURE_320, 48);
        layoutParams.addRule(14);
        hVar.setLayoutParams(layoutParams);
        if (this.c == 0) {
            this.a.a.addView(hVar);
            if (this.a.a.j == null) {
                this.a.a.a(hVar);
                try {
                    hVar.e();
                } catch (Exception e) {
                }
            } else {
                this.a.a.b(hVar);
            }
        } else {
            this.a.a.j = hVar;
        }
        this.a.a.s.post(new B(this.a.a, this.c));
        this.a.a.l = false;
        if (this.a.a.n) {
            this.a.a.a(true);
        }
    }
}
