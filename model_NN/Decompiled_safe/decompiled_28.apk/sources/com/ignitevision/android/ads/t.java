package com.ignitevision.android.ads;

import android.util.Log;

class t implements Runnable {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ AdView a;
    private final /* synthetic */ boolean b;

    t(AdView adView, boolean z) {
        this.a = adView;
        this.b = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ignitevision.android.ads.AdView.c(com.ignitevision.android.ads.AdView, boolean):void
     arg types: [com.ignitevision.android.ads.AdView, int]
     candidates:
      com.ignitevision.android.ads.AdView.c(com.ignitevision.android.ads.AdView, int):void
      com.ignitevision.android.ads.AdView.c(com.ignitevision.android.ads.AdView, com.ignitevision.android.ads.h):void
      com.ignitevision.android.ads.AdView.c(com.ignitevision.android.ads.AdView, boolean):void */
    public void run() {
        try {
            int l = t.super.getVisibility();
            C0026a b2 = new C0030e(this.a.q).b();
            if (this.b) {
                this.a.s.post(new A(this.a, b2));
            }
            if (b2 != null) {
                boolean z = b2.a() == C0027b.None || b2.a() == C0027b.Unknow;
                if (b2.a() == C0027b.Text && (b2.d() == null || b2.d().length() < 0)) {
                    z = true;
                }
                if (b2.a() == C0027b.Banner && b2.e() == null) {
                    z = true;
                }
                if (!z) {
                    this.a.post(new u(this, b2, l));
                    return;
                }
                this.a.l = false;
                if (this.a.n) {
                    this.a.a(true);
                    return;
                }
                return;
            }
            this.a.l = false;
            if (this.a.n && !this.b) {
                this.a.a(true);
            }
        } catch (Exception e) {
            this.a.l = false;
            Log.e("TinmooSDK", "Unhandled exception requesting a fresh ad.", e);
        }
    }
}
