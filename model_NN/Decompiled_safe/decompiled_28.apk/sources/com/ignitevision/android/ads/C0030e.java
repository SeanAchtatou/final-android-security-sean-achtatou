package com.ignitevision.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.util.Log;
import cn.domob.android.ads.DomobAdManager;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.json.JSONTokener;

/* renamed from: com.ignitevision.android.ads.e  reason: case insensitive filesystem */
class C0030e {
    /* access modifiers changed from: private */
    public Context a;
    private String b;
    private int c;
    private long d;
    private double e;
    private double f;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l = "http://www.tinmoo.com/engine/android/androidSDKTest.do";
    private String m;
    /* access modifiers changed from: private */
    public long n;
    /* access modifiers changed from: private */
    public Location o;
    private F p;

    C0030e(Context context) {
        this.a = context;
        String publisherKey = AdManager.getPublisherKey(context);
        this.b = publisherKey;
        if (publisherKey != null && !AdManager.isTest() && AdManager.b(context) != null && !AdManager.b(context).equals(DomobAdManager.TEST_EMULATOR)) {
            this.l = "http://www.tinmoo.com/engine/android/connect.do";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x008f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.Bitmap a(java.lang.String r11, com.ignitevision.android.ads.C0029d r12) {
        /*
            r10 = this;
            r3 = 0
            r6 = 0
            java.lang.String r0 = "/"
            int r0 = r11.lastIndexOf(r0)
            int r0 = r0 + 1
            java.lang.String r1 = r11.substring(r0)
            if (r12 == 0) goto L_0x0093
            android.content.Context r0 = r10.a
            android.graphics.Bitmap r0 = com.ignitevision.android.ads.C0028c.a(r0, r1, r12)
            if (r0 == 0) goto L_0x0019
        L_0x0018:
            return r0
        L_0x0019:
            r2 = r0
        L_0x001a:
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x006e }
            r0.<init>(r11)     // Catch:{ MalformedURLException -> 0x006e }
        L_0x001f:
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IOException -> 0x0077, all -> 0x0080 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0077, all -> 0x0080 }
            java.lang.String r3 = "GET"
            r0.setRequestMethod(r3)     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            r3 = 6000(0x1770, float:8.408E-42)
            r0.setConnectTimeout(r3)     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            r3 = 6000(0x1770, float:8.408E-42)
            r0.setReadTimeout(r3)     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            r3 = 1
            r0.setDoInput(r3)     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            r0.connect()     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            java.io.InputStream r3 = r0.getInputStream()     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            int r4 = r0.getContentLength()     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            r5 = -1
            if (r4 == r5) goto L_0x0091
            byte[] r4 = new byte[r4]     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            r5 = 512(0x200, float:7.175E-43)
            byte[] r5 = new byte[r5]     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
        L_0x004c:
            int r7 = r3.read(r5)     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            if (r7 > 0) goto L_0x0071
            if (r12 == 0) goto L_0x0061
            java.lang.Thread r3 = new java.lang.Thread     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            com.ignitevision.android.ads.g r5 = new com.ignitevision.android.ads.g     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            r5.<init>(r10, r1, r12, r4)     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            r3.<init>(r5)     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            r3.start()     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
        L_0x0061:
            r1 = 0
            int r3 = r4.length     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeByteArray(r4, r1, r3)     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
        L_0x0067:
            if (r0 == 0) goto L_0x006c
            r0.disconnect()
        L_0x006c:
            r0 = r1
            goto L_0x0018
        L_0x006e:
            r0 = move-exception
            r0 = r3
            goto L_0x001f
        L_0x0071:
            r8 = 0
            java.lang.System.arraycopy(r5, r8, r4, r6, r7)     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            int r6 = r6 + r7
            goto L_0x004c
        L_0x0077:
            r0 = move-exception
            r0 = r3
        L_0x0079:
            if (r0 == 0) goto L_0x008f
            r0.disconnect()
            r0 = r2
            goto L_0x0018
        L_0x0080:
            r0 = move-exception
            r1 = r3
        L_0x0082:
            if (r1 == 0) goto L_0x0087
            r1.disconnect()
        L_0x0087:
            throw r0
        L_0x0088:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0082
        L_0x008d:
            r1 = move-exception
            goto L_0x0079
        L_0x008f:
            r0 = r2
            goto L_0x0018
        L_0x0091:
            r1 = r2
            goto L_0x0067
        L_0x0093:
            r2 = r3
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ignitevision.android.ads.C0030e.a(java.lang.String, com.ignitevision.android.ads.d):android.graphics.Bitmap");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0099, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x009a, code lost:
        r9 = r1;
        r1 = r0;
        r0 = r9;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0099 A[ExcHandler: all (r1v4 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:10:0x0026] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00a0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.drawable.Drawable a(java.lang.String r11, java.lang.String r12, com.ignitevision.android.ads.C0029d r13) {
        /*
            r10 = this;
            r1 = 0
            java.lang.String r0 = "/"
            int r0 = r11.lastIndexOf(r0)
            int r0 = r0 + 1
            java.lang.String r2 = r11.substring(r0)
            if (r13 == 0) goto L_0x00a3
            android.content.Context r0 = r10.a
            android.graphics.drawable.Drawable r0 = com.ignitevision.android.ads.C0028c.b(r0, r2, r13)
            if (r0 == 0) goto L_0x0018
        L_0x0017:
            return r0
        L_0x0018:
            r3 = r0
        L_0x0019:
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x007f }
            r0.<init>(r11)     // Catch:{ MalformedURLException -> 0x007f }
        L_0x001e:
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0089, all -> 0x0092 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0089, all -> 0x0092 }
            java.lang.String r1 = "GET"
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x009e, all -> 0x0099 }
            r1 = 6000(0x1770, float:8.408E-42)
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x009e, all -> 0x0099 }
            r1 = 6000(0x1770, float:8.408E-42)
            r0.setReadTimeout(r1)     // Catch:{ Exception -> 0x009e, all -> 0x0099 }
            r1 = 1
            r0.setDoInput(r1)     // Catch:{ Exception -> 0x009e, all -> 0x0099 }
            r0.connect()     // Catch:{ Exception -> 0x009e, all -> 0x0099 }
            java.io.InputStream r4 = r0.getInputStream()     // Catch:{ Exception -> 0x009e, all -> 0x0099 }
            r1 = 0
            byte[] r1 = (byte[]) r1     // Catch:{ Exception -> 0x009e, all -> 0x0099 }
            java.io.ByteArrayOutputStream r5 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0087, all -> 0x0099 }
            r5.<init>()     // Catch:{ Exception -> 0x0087, all -> 0x0099 }
            r6 = 1024(0x400, float:1.435E-42)
            byte[] r6 = new byte[r6]     // Catch:{ Exception -> 0x0087, all -> 0x0099 }
        L_0x004a:
            int r7 = r4.read(r6)     // Catch:{ Exception -> 0x0087, all -> 0x0099 }
            r8 = -1
            if (r7 != r8) goto L_0x0082
            r4.close()     // Catch:{ Exception -> 0x0087, all -> 0x0099 }
            r5.flush()     // Catch:{ Exception -> 0x0087, all -> 0x0099 }
            r5.close()     // Catch:{ Exception -> 0x0087, all -> 0x0099 }
            byte[] r1 = r5.toByteArray()     // Catch:{ Exception -> 0x0087, all -> 0x0099 }
        L_0x005e:
            if (r13 == 0) goto L_0x006d
            java.lang.Thread r4 = new java.lang.Thread     // Catch:{ Exception -> 0x009e, all -> 0x0099 }
            com.ignitevision.android.ads.g r5 = new com.ignitevision.android.ads.g     // Catch:{ Exception -> 0x009e, all -> 0x0099 }
            r5.<init>(r10, r2, r13, r1)     // Catch:{ Exception -> 0x009e, all -> 0x0099 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x009e, all -> 0x0099 }
            r4.start()     // Catch:{ Exception -> 0x009e, all -> 0x0099 }
        L_0x006d:
            android.graphics.drawable.BitmapDrawable r2 = new android.graphics.drawable.BitmapDrawable     // Catch:{ Exception -> 0x009e, all -> 0x0099 }
            r4 = 0
            int r5 = r1.length     // Catch:{ Exception -> 0x009e, all -> 0x0099 }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeByteArray(r1, r4, r5)     // Catch:{ Exception -> 0x009e, all -> 0x0099 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x009e, all -> 0x0099 }
            if (r0 == 0) goto L_0x007d
            r0.disconnect()
        L_0x007d:
            r0 = r2
            goto L_0x0017
        L_0x007f:
            r0 = move-exception
            r0 = r1
            goto L_0x001e
        L_0x0082:
            r8 = 0
            r5.write(r6, r8, r7)     // Catch:{ Exception -> 0x0087, all -> 0x0099 }
            goto L_0x004a
        L_0x0087:
            r4 = move-exception
            goto L_0x005e
        L_0x0089:
            r0 = move-exception
            r0 = r1
        L_0x008b:
            if (r0 == 0) goto L_0x00a0
            r0.disconnect()
            r0 = r3
            goto L_0x0017
        L_0x0092:
            r0 = move-exception
        L_0x0093:
            if (r1 == 0) goto L_0x0098
            r1.disconnect()
        L_0x0098:
            throw r0
        L_0x0099:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0093
        L_0x009e:
            r1 = move-exception
            goto L_0x008b
        L_0x00a0:
            r0 = r3
            goto L_0x0017
        L_0x00a3:
            r3 = r1
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ignitevision.android.ads.C0030e.a(java.lang.String, java.lang.String, com.ignitevision.android.ads.d):android.graphics.drawable.Drawable");
    }

    /* JADX INFO: finally extract failed */
    private JSONObject c() {
        HttpEntity entity;
        if (this.m == null) {
            return null;
        }
        String str = String.valueOf(this.l) + "?json={" + this.m + "}";
        HttpGet httpGet = new HttpGet();
        try {
            URL url = new URL(str);
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(new BasicHttpParams());
            httpGet.setURI(new URI(url.getProtocol(), null, url.getHost(), url.getPort(), url.getPath(), url.getQuery(), null));
            HttpResponse execute = defaultHttpClient.execute(httpGet);
            JSONObject jSONObject = (200 != execute.getStatusLine().getStatusCode() || (entity = execute.getEntity()) == null || entity.getContentLength() <= 0) ? null : new JSONObject(new JSONTokener(EntityUtils.toString(entity, XmlConstant.DEFAULT_ENCODING)));
            httpGet.abort();
            return jSONObject;
        } catch (Exception e2) {
            httpGet.abort();
            return null;
        } catch (Throwable th) {
            httpGet.abort();
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x007b A[SYNTHETIC, Splitter:B:24:0x007b] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x008a A[SYNTHETIC, Splitter:B:31:0x008a] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00a9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.json.JSONObject d() {
        /*
            r5 = this;
            r4 = 0
            java.lang.String r0 = r5.m
            if (r0 != 0) goto L_0x0007
            r0 = r4
        L_0x0006:
            return r0
        L_0x0007:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = r5.l
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r0.<init>(r1)
            java.lang.String r1 = "?json={"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r5.m
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "}"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x00a1, all -> 0x0085 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x00a1, all -> 0x0085 }
            java.net.URLConnection r5 = r1.openConnection()     // Catch:{ Exception -> 0x00a1, all -> 0x0085 }
            java.net.HttpURLConnection r5 = (java.net.HttpURLConnection) r5     // Catch:{ Exception -> 0x00a1, all -> 0x0085 }
            r0 = 6000(0x1770, float:8.408E-42)
            r5.setConnectTimeout(r0)     // Catch:{ Exception -> 0x00a5, all -> 0x0099 }
            r0 = 6000(0x1770, float:8.408E-42)
            r5.setReadTimeout(r0)     // Catch:{ Exception -> 0x00a5, all -> 0x0099 }
            r5.connect()     // Catch:{ Exception -> 0x00a5, all -> 0x0099 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a5, all -> 0x0099 }
            r0.<init>()     // Catch:{ Exception -> 0x00a5, all -> 0x0099 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00a5, all -> 0x0099 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00a5, all -> 0x0099 }
            java.io.InputStream r3 = r5.getInputStream()     // Catch:{ Exception -> 0x00a5, all -> 0x0099 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x00a5, all -> 0x0099 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x00a5, all -> 0x0099 }
        L_0x0053:
            java.lang.String r2 = r1.readLine()     // Catch:{ Exception -> 0x0077, all -> 0x009d }
            if (r2 != 0) goto L_0x0073
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Exception -> 0x0077, all -> 0x009d }
            org.json.JSONTokener r3 = new org.json.JSONTokener     // Catch:{ Exception -> 0x0077, all -> 0x009d }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0077, all -> 0x009d }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0077, all -> 0x009d }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0077, all -> 0x009d }
            if (r1 == 0) goto L_0x006c
            r1.close()     // Catch:{ Exception -> 0x0097 }
        L_0x006c:
            if (r5 == 0) goto L_0x0071
            r5.disconnect()
        L_0x0071:
            r0 = r2
            goto L_0x0006
        L_0x0073:
            r0.append(r2)     // Catch:{ Exception -> 0x0077, all -> 0x009d }
            goto L_0x0053
        L_0x0077:
            r0 = move-exception
            r0 = r5
        L_0x0079:
            if (r1 == 0) goto L_0x007e
            r1.close()     // Catch:{ Exception -> 0x0093 }
        L_0x007e:
            if (r0 == 0) goto L_0x00a9
            r0.disconnect()
            r0 = r4
            goto L_0x0006
        L_0x0085:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x0088:
            if (r2 == 0) goto L_0x008d
            r2.close()     // Catch:{ Exception -> 0x0095 }
        L_0x008d:
            if (r1 == 0) goto L_0x0092
            r1.disconnect()
        L_0x0092:
            throw r0
        L_0x0093:
            r1 = move-exception
            goto L_0x007e
        L_0x0095:
            r2 = move-exception
            goto L_0x008d
        L_0x0097:
            r0 = move-exception
            goto L_0x006c
        L_0x0099:
            r0 = move-exception
            r1 = r5
            r2 = r4
            goto L_0x0088
        L_0x009d:
            r0 = move-exception
            r2 = r1
            r1 = r5
            goto L_0x0088
        L_0x00a1:
            r0 = move-exception
            r0 = r4
            r1 = r4
            goto L_0x0079
        L_0x00a5:
            r0 = move-exception
            r0 = r5
            r1 = r4
            goto L_0x0079
        L_0x00a9:
            r0 = r4
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ignitevision.android.ads.C0030e.d():org.json.JSONObject");
    }

    private boolean e() {
        if (this.b == null || this.b.equals("")) {
            this.m = null;
            return false;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("\"key\":\"" + this.b + "\",");
        stringBuffer.append("\"actionId\":" + this.c + ",");
        stringBuffer.append("\"bidId\":" + this.d + ",");
        stringBuffer.append("\"latitude\":" + this.e + ",");
        stringBuffer.append("\"longitude\":" + this.f + ",");
        stringBuffer.append("\"deviceId\":" + (this.g == null ? ((Object) null) + "," : XmlConstant.QUOTE + this.g + "\","));
        stringBuffer.append("\"androidId\":" + (this.h == null ? ((Object) null) + "," : XmlConstant.QUOTE + this.h + "\","));
        stringBuffer.append("\"version\":\"" + this.i + "\",");
        stringBuffer.append("\"model\":\"" + this.j + "\",");
        stringBuffer.append("\"osVersion\":\"" + this.k + XmlConstant.QUOTE);
        this.m = stringBuffer.toString();
        return true;
    }

    private Bitmap f(String str) {
        Bitmap bitmap = null;
        try {
            InputStream open = this.a.getAssets().open(str);
            bitmap = BitmapFactory.decodeStream(open);
            open.close();
            return bitmap;
        } catch (Exception e2) {
            return bitmap;
        }
    }

    private void f() {
        LocationManager locationManager;
        String str;
        boolean z;
        if (this.a == null) {
            return;
        }
        if (this.o == null || System.currentTimeMillis() > this.n + 900000) {
            synchronized (this) {
                this.n = System.currentTimeMillis();
                if (this.a.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                    locationManager = (LocationManager) this.a.getSystemService("location");
                    if (locationManager != null) {
                        Criteria criteria = new Criteria();
                        criteria.setAccuracy(2);
                        criteria.setCostAllowed(false);
                        str = locationManager.getBestProvider(criteria, true);
                        z = true;
                    } else {
                        str = null;
                        z = true;
                    }
                } else {
                    locationManager = null;
                    str = null;
                    z = false;
                }
                if (str == null && this.a.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
                    locationManager = (LocationManager) this.a.getSystemService("location");
                    if (locationManager != null) {
                        Criteria criteria2 = new Criteria();
                        criteria2.setAccuracy(1);
                        criteria2.setCostAllowed(false);
                        str = locationManager.getBestProvider(criteria2, true);
                        z = true;
                    } else {
                        z = true;
                    }
                }
                if (!z) {
                    Log.d("TinmooSDK", "Cannot access user's location.  Permissions are not set.");
                } else if (str == null) {
                    Log.d("TinmooSDK", "No location providers are available.  Ads will not be geotargeted.");
                } else {
                    locationManager.requestLocationUpdates(str, 0, 0.0f, new C0031f(this, locationManager), this.a.getMainLooper());
                    if (this.o == null) {
                        try {
                            this.o = locationManager.getLastKnownLocation(str);
                            g();
                        } catch (Exception e2) {
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.o != null) {
            a(this.o.getLatitude());
            b(this.o.getLongitude());
        }
    }

    /* access modifiers changed from: protected */
    public JSONObject a(C0026a aVar) {
        a(3);
        a(aVar.b().longValue());
        a(AdManager.c(this.a));
        b(AdManager.b(this.a));
        c("A2.0.2");
        d(Build.MODEL);
        e(Build.VERSION.RELEASE);
        e();
        return c();
    }

    /* access modifiers changed from: protected */
    public void a() {
        a(1);
        a(AdManager.c(this.a));
        b(AdManager.b(this.a));
        c("A2.0.2");
        d(Build.MODEL);
        e(Build.VERSION.RELEASE);
        e();
        JSONObject d2 = d();
        if (this.p != null) {
            this.p.a(d2);
        }
        if (d2 != null && !d2.isNull("logo")) {
            try {
                Bitmap a2 = a(d2.getString("logo"), C0029d.Icon);
                if (a2 == null) {
                    a2 = f("logo.png");
                }
                s.i = a2;
            } catch (Exception e2) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(double d2) {
        this.e = d2;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        this.c = i2;
    }

    /* access modifiers changed from: package-private */
    public void a(long j2) {
        this.d = j2;
    }

    public void a(F f2) {
        this.p = f2;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.g = str;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:57:0x011d=Splitter:B:57:0x011d, B:19:0x007e=Splitter:B:19:0x007e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.ignitevision.android.ads.C0026a b() {
        /*
            r10 = this;
            r4 = 2
            r5 = 0
            r9 = 0
            java.lang.String r0 = ""
            java.lang.String r0 = "type"
            r10.f()
            r10.a(r4)
            android.content.Context r0 = r10.a
            java.lang.String r0 = com.ignitevision.android.ads.AdManager.c(r0)
            r10.a(r0)
            android.content.Context r0 = r10.a
            java.lang.String r0 = com.ignitevision.android.ads.AdManager.b(r0)
            r10.b(r0)
            java.lang.String r0 = "A2.0.2"
            r10.c(r0)
            java.lang.String r0 = android.os.Build.MODEL
            r10.d(r0)
            java.lang.String r0 = android.os.Build.VERSION.RELEASE
            r10.e(r0)
            r10.e()
            org.json.JSONObject r1 = r10.d()
            if (r1 == 0) goto L_0x015d
            com.ignitevision.android.ads.a r2 = new com.ignitevision.android.ads.a
            r2.<init>()
            java.lang.String r0 = "type"
            boolean r0 = r1.isNull(r0)     // Catch:{ Exception -> 0x0122 }
            if (r0 != 0) goto L_0x0147
            java.lang.String r0 = "type"
            int r0 = r1.getInt(r0)     // Catch:{ Exception -> 0x0122 }
            r3 = 1
            if (r0 != r3) goto L_0x00dc
            com.ignitevision.android.ads.b r0 = com.ignitevision.android.ads.C0027b.Text     // Catch:{ Exception -> 0x0122 }
            r2.a(r0)     // Catch:{ Exception -> 0x0122 }
            java.lang.String r0 = "text"
            boolean r0 = r1.isNull(r0)     // Catch:{ Exception -> 0x0122 }
            if (r0 != 0) goto L_0x0098
            java.lang.String r0 = "text"
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x0122 }
            r2.b(r0)     // Catch:{ Exception -> 0x0122 }
            java.lang.String r0 = "texts"
            boolean r0 = r1.isNull(r0)     // Catch:{ Exception -> 0x0122 }
            if (r0 != 0) goto L_0x0081
            r0 = 0
            java.lang.String[] r0 = (java.lang.String[]) r0     // Catch:{ Exception -> 0x0122 }
            java.lang.String r3 = "texts"
            org.json.JSONArray r3 = r1.getJSONArray(r3)     // Catch:{ Exception -> 0x0157 }
            int r4 = r3.length()     // Catch:{ Exception -> 0x0157 }
            if (r4 <= 0) goto L_0x007e
            java.lang.String[] r0 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0157 }
        L_0x007c:
            if (r5 < r4) goto L_0x00d0
        L_0x007e:
            r2.a(r0)     // Catch:{ Exception -> 0x0122 }
        L_0x0081:
            java.lang.String r0 = "icon"
            boolean r0 = r1.isNull(r0)     // Catch:{ Exception -> 0x0122 }
            if (r0 != 0) goto L_0x0098
            java.lang.String r0 = "icon"
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x0122 }
            com.ignitevision.android.ads.d r3 = com.ignitevision.android.ads.C0029d.Icon     // Catch:{ Exception -> 0x0122 }
            android.graphics.Bitmap r0 = r10.a(r0, r3)     // Catch:{ Exception -> 0x0122 }
            r2.a(r0)     // Catch:{ Exception -> 0x0122 }
        L_0x0098:
            com.ignitevision.android.ads.b r0 = r2.a()     // Catch:{ Exception -> 0x0122 }
            com.ignitevision.android.ads.b r3 = com.ignitevision.android.ads.C0027b.None     // Catch:{ Exception -> 0x0122 }
            if (r0 == r3) goto L_0x015a
            com.ignitevision.android.ads.b r0 = r2.a()     // Catch:{ Exception -> 0x0122 }
            com.ignitevision.android.ads.b r3 = com.ignitevision.android.ads.C0027b.Unknow     // Catch:{ Exception -> 0x0122 }
            if (r0 == r3) goto L_0x015a
            java.lang.String r0 = "bidId"
            boolean r0 = r1.isNull(r0)     // Catch:{ Exception -> 0x0122 }
            if (r0 != 0) goto L_0x00bd
            java.lang.String r0 = "bidId"
            long r3 = r1.getLong(r0)     // Catch:{ Exception -> 0x0122 }
            java.lang.Long r0 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x0122 }
            r2.a(r0)     // Catch:{ Exception -> 0x0122 }
        L_0x00bd:
            java.lang.String r0 = "targetUrl"
            boolean r0 = r1.isNull(r0)     // Catch:{ Exception -> 0x0122 }
            if (r0 != 0) goto L_0x015a
            java.lang.String r0 = "targetUrl"
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x0122 }
            r2.a(r0)     // Catch:{ Exception -> 0x0122 }
            r0 = r2
        L_0x00cf:
            return r0
        L_0x00d0:
            java.lang.String r6 = r3.getString(r5)     // Catch:{ Exception -> 0x00d9 }
        L_0x00d4:
            r0[r5] = r6     // Catch:{ Exception -> 0x0157 }
            int r5 = r5 + 1
            goto L_0x007c
        L_0x00d9:
            r6 = move-exception
            r6 = r9
            goto L_0x00d4
        L_0x00dc:
            java.lang.String r0 = "type"
            int r0 = r1.getInt(r0)     // Catch:{ Exception -> 0x0122 }
            if (r0 != r4) goto L_0x0139
            com.ignitevision.android.ads.b r0 = com.ignitevision.android.ads.C0027b.Banner     // Catch:{ Exception -> 0x0122 }
            r2.a(r0)     // Catch:{ Exception -> 0x0122 }
            java.lang.String r0 = "image"
            boolean r0 = r1.isNull(r0)     // Catch:{ Exception -> 0x0122 }
            if (r0 != 0) goto L_0x0098
            java.lang.String r0 = "image"
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x0122 }
            java.lang.String r3 = ""
            com.ignitevision.android.ads.d r4 = com.ignitevision.android.ads.C0029d.Banner     // Catch:{ Exception -> 0x0122 }
            android.graphics.drawable.Drawable r0 = r10.a(r0, r3, r4)     // Catch:{ Exception -> 0x0122 }
            r2.a(r0)     // Catch:{ Exception -> 0x0122 }
            java.lang.String r0 = "images"
            boolean r0 = r1.isNull(r0)     // Catch:{ Exception -> 0x0122 }
            if (r0 != 0) goto L_0x0098
            r0 = 0
            android.graphics.drawable.Drawable[] r0 = (android.graphics.drawable.Drawable[]) r0     // Catch:{ Exception -> 0x0122 }
            java.lang.String r3 = "images"
            org.json.JSONArray r3 = r1.getJSONArray(r3)     // Catch:{ Exception -> 0x0155 }
            int r4 = r3.length()     // Catch:{ Exception -> 0x0155 }
            if (r4 <= 0) goto L_0x011d
            android.graphics.drawable.Drawable[] r0 = new android.graphics.drawable.Drawable[r4]     // Catch:{ Exception -> 0x0155 }
        L_0x011b:
            if (r5 < r4) goto L_0x0125
        L_0x011d:
            r2.a(r0)     // Catch:{ Exception -> 0x0122 }
            goto L_0x0098
        L_0x0122:
            r0 = move-exception
            r0 = r2
            goto L_0x00cf
        L_0x0125:
            java.lang.String r6 = r3.getString(r5)     // Catch:{ Exception -> 0x0136 }
            java.lang.String r7 = ""
            com.ignitevision.android.ads.d r8 = com.ignitevision.android.ads.C0029d.Banner     // Catch:{ Exception -> 0x0136 }
            android.graphics.drawable.Drawable r6 = r10.a(r6, r7, r8)     // Catch:{ Exception -> 0x0136 }
        L_0x0131:
            r0[r5] = r6     // Catch:{ Exception -> 0x0155 }
            int r5 = r5 + 1
            goto L_0x011b
        L_0x0136:
            r6 = move-exception
            r6 = r9
            goto L_0x0131
        L_0x0139:
            com.ignitevision.android.ads.b r0 = com.ignitevision.android.ads.C0027b.Unknow     // Catch:{ Exception -> 0x0122 }
            r2.a(r0)     // Catch:{ Exception -> 0x0122 }
            java.lang.String r0 = "TinmooSDK"
            java.lang.String r3 = "Ads type is not correct."
            android.util.Log.d(r0, r3)     // Catch:{ Exception -> 0x0122 }
            goto L_0x0098
        L_0x0147:
            com.ignitevision.android.ads.b r0 = com.ignitevision.android.ads.C0027b.None     // Catch:{ Exception -> 0x0122 }
            r2.a(r0)     // Catch:{ Exception -> 0x0122 }
            java.lang.String r0 = "TinmooSDK"
            java.lang.String r3 = "Ads type is not correct."
            android.util.Log.d(r0, r3)     // Catch:{ Exception -> 0x0122 }
            goto L_0x0098
        L_0x0155:
            r3 = move-exception
            goto L_0x011d
        L_0x0157:
            r3 = move-exception
            goto L_0x007e
        L_0x015a:
            r0 = r2
            goto L_0x00cf
        L_0x015d:
            r0 = r9
            goto L_0x00cf
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ignitevision.android.ads.C0030e.b():com.ignitevision.android.ads.a");
    }

    /* access modifiers changed from: package-private */
    public void b(double d2) {
        this.f = d2;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.h = str;
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
        this.i = str;
    }

    /* access modifiers changed from: package-private */
    public void d(String str) {
        this.j = str;
    }

    /* access modifiers changed from: package-private */
    public void e(String str) {
        this.k = str;
    }
}
