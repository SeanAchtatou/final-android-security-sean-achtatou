package com.ignitevision.android.ads;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

class m extends Thread {
    final /* synthetic */ h a;
    private final /* synthetic */ String b;

    m(h hVar, String str) {
        this.a = hVar;
        this.b = str;
    }

    public void run() {
        boolean z;
        this.a.c();
        if (this.b != null) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.b));
                intent.addFlags(268435456);
                this.a.m.startActivity(intent);
                z = true;
            } catch (Exception e) {
                Log.e("TinmooSDK", e.toString());
                z = false;
            }
            if (z) {
                new C0030e(this.a.m).a(this.a.a());
            }
        }
        this.a.b();
    }
}
