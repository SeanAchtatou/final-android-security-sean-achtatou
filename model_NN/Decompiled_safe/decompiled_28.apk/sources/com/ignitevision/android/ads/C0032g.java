package com.ignitevision.android.ads;

/* renamed from: com.ignitevision.android.ads.g  reason: case insensitive filesystem */
class C0032g implements Runnable {
    final /* synthetic */ C0030e a;
    private String b;
    private C0029d c;
    private byte[] d;

    public C0032g(C0030e eVar, String str, C0029d dVar, byte[] bArr) {
        this.a = eVar;
        this.b = str;
        this.c = dVar;
        this.d = bArr;
    }

    public void run() {
        C0028c.a(this.a.a, this.b, this.c, this.d);
    }
}
