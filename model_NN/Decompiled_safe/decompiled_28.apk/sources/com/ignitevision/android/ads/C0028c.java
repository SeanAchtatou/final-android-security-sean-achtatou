package com.ignitevision.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import java.io.File;

/* renamed from: com.ignitevision.android.ads.c  reason: case insensitive filesystem */
class C0028c {
    private C0028c() {
    }

    protected static Bitmap a(Context context, String str, C0029d dVar) {
        File file = new File(a(context, dVar), str);
        if (file.exists()) {
            return BitmapFactory.decodeFile(file.getAbsolutePath());
        }
        return null;
    }

    private static File a(Context context, C0029d dVar) {
        String str = context.getCacheDir() + File.separator + C0029d.Tinmoo.a();
        if (dVar != null) {
            File file = new File(str, dVar.a());
            if (!file.exists()) {
                file.mkdirs();
            }
            return file;
        }
        File file2 = new File(str);
        if (!file2.exists()) {
            file2.mkdirs();
        }
        return file2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0026 A[SYNTHETIC, Splitter:B:12:0x0026] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0032 A[SYNTHETIC, Splitter:B:18:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void a(android.content.Context r4, java.lang.String r5, com.ignitevision.android.ads.C0029d r6, byte[] r7) {
        /*
            if (r7 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            b(r4, r6)
            r0 = 0
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0023, all -> 0x002c }
            java.io.File r2 = a(r4, r6)     // Catch:{ Exception -> 0x0023, all -> 0x002c }
            r1.<init>(r2, r5)     // Catch:{ Exception -> 0x0023, all -> 0x002c }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0023, all -> 0x002c }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0023, all -> 0x002c }
            r2.write(r7)     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            r2.flush()     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            if (r2 == 0) goto L_0x0002
            r2.close()     // Catch:{ IOException -> 0x0021 }
            goto L_0x0002
        L_0x0021:
            r0 = move-exception
            goto L_0x0002
        L_0x0023:
            r1 = move-exception
        L_0x0024:
            if (r0 == 0) goto L_0x0002
            r0.close()     // Catch:{ IOException -> 0x002a }
            goto L_0x0002
        L_0x002a:
            r0 = move-exception
            goto L_0x0002
        L_0x002c:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x0030:
            if (r1 == 0) goto L_0x0035
            r1.close()     // Catch:{ IOException -> 0x0036 }
        L_0x0035:
            throw r0
        L_0x0036:
            r1 = move-exception
            goto L_0x0035
        L_0x0038:
            r0 = move-exception
            r1 = r2
            goto L_0x0030
        L_0x003b:
            r0 = move-exception
            r0 = r2
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ignitevision.android.ads.C0028c.a(android.content.Context, java.lang.String, com.ignitevision.android.ads.d, byte[]):void");
    }

    protected static Drawable b(Context context, String str, C0029d dVar) {
        File file = new File(a(context, dVar), str);
        if (file.exists()) {
            return Drawable.createFromPath(file.getAbsolutePath());
        }
        return null;
    }

    private static void b(Context context, C0029d dVar) {
        File file = new File(context.getCacheDir() + File.separator + C0029d.Tinmoo.a() + File.separator + dVar.a());
        int i = 0;
        for (File length : file.listFiles()) {
            i = (int) (length.length() + ((long) i));
        }
        if (i >= 512000) {
            file.delete();
        }
    }
}
