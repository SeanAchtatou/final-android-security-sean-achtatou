package com.wiyun.ad;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import android.view.View;
import com.adview.util.AdViewUtil;

class t extends View {
    private Paint a;
    private Shader b;
    private float c = 50.0f;
    private long d = System.currentTimeMillis();
    private boolean e = false;
    private String f;
    private int g;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, int, int[], float[], android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
    t(String str, int i, Context context) {
        super(context);
        this.f = str;
        this.g = i;
        this.a = new Paint();
        this.a.setColor(-7829368);
        this.a.setAntiAlias(true);
        this.a.setTextSize(26.0f);
        this.b = new LinearGradient(0.0f, 0.0f, 200.0f, 0.0f, new int[]{Color.argb((int) AdViewUtil.VERSION, 120, 120, 120), Color.argb((int) AdViewUtil.VERSION, 120, 120, 120), Color.argb((int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION)}, new float[]{0.0f, 0.7f, 1.0f}, Shader.TileMode.MIRROR);
        this.a.setShader(this.b);
    }

    private int b(int i) {
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        switch (mode) {
            case Integer.MIN_VALUE:
                return Math.min(50, size);
            case 1073741824:
                return size;
            default:
                return Math.max(50, size);
        }
    }

    private int c(int i) {
        return View.MeasureSpec.getSize(i);
    }

    public void a(float f2) {
        this.a.setTextSize(f2);
    }

    public void a(int i) {
        this.a.setColor(i);
    }

    public void a(boolean z) {
        this.e = z;
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        long currentTimeMillis = System.currentTimeMillis();
        this.c = (((float) (currentTimeMillis - this.d)) / 4.5f) + this.c;
        Matrix matrix = new Matrix();
        if (this.e) {
            matrix.setTranslate(this.c, 0.0f);
            postInvalidateDelayed(50);
        } else {
            matrix.setTranslate(0.0f, 0.0f);
        }
        this.b.setLocalMatrix(matrix);
        int height = getHeight();
        int width = getWidth();
        Paint.FontMetrics fontMetrics = this.a.getFontMetrics();
        int i = (height - ((int) (fontMetrics.descent - fontMetrics.ascent))) / 2;
        int measureText = (int) this.a.measureText(this.f);
        switch (this.g) {
            case 1:
                canvas.drawText(this.f, (float) ((width - measureText) / 2), ((float) i) - fontMetrics.ascent, this.a);
                break;
            case 2:
                canvas.drawText(this.f, (float) (width - measureText), ((float) i) - fontMetrics.ascent, this.a);
                break;
            default:
                canvas.drawText(this.f, 0.0f, ((float) i) - fontMetrics.ascent, this.a);
                break;
        }
        this.d = currentTimeMillis;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = 0;
        int b2 = b(i2);
        if (b2 != 0) {
            i3 = c(i);
        }
        setMeasuredDimension(i3, b2);
    }
}
