package com.wiyun.ad;

import com.wqmobile.sdk.pojoxml.util.XmlConstant;

public class j {
    private static final char[] a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private static final char[] b = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private final String c = XmlConstant.DEFAULT_ENCODING;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.ad.j.a(byte[], boolean):char[]
     arg types: [byte[], int]
     candidates:
      com.wiyun.ad.j.a(byte[], char[]):char[]
      com.wiyun.ad.j.a(byte[], boolean):char[] */
    public static char[] a(byte[] bArr) {
        return a(bArr, true);
    }

    public static char[] a(byte[] bArr, boolean z) {
        return a(bArr, z ? a : b);
    }

    protected static char[] a(byte[] bArr, char[] cArr) {
        int i = 0;
        int length = bArr.length;
        char[] cArr2 = new char[(length << 1)];
        for (int i2 = 0; i2 < length; i2++) {
            int i3 = i + 1;
            cArr2[i] = cArr[(bArr[i2] & 240) >>> 4];
            i = i3 + 1;
            cArr2[i3] = cArr[bArr[i2] & 15];
        }
        return cArr2;
    }

    public static String b(byte[] bArr) {
        return new String(a(bArr));
    }

    public String toString() {
        return String.valueOf(super.toString()) + "[charsetName=" + this.c + "]";
    }
}
