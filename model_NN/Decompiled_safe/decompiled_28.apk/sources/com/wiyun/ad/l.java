package com.wiyun.ad;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.Proxy;
import android.text.TextUtils;
import org.apache.http.HttpHost;

public class l {
    private static l a;
    private Context b;
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public a d;
    private BroadcastReceiver e = new g(this);

    public interface a {
        void a();

        void b();
    }

    private l(Context context, a aVar) {
        this.b = context.getApplicationContext();
        this.d = aVar;
        this.b.registerReceiver(this.e, new IntentFilter("android.net.wifi.STATE_CHANGE"));
        this.c = m.d(context);
    }

    public static void a() {
        if (a != null) {
            a.b.unregisterReceiver(a.e);
            a.b = null;
            a.d = null;
            a = null;
        }
    }

    public static void a(Context context, a aVar) {
        if (a == null) {
            a = new l(context, aVar);
        }
    }

    public static boolean b() {
        if (a == null) {
            return false;
        }
        return a.c;
    }

    public static boolean c() {
        return !TextUtils.isEmpty(Proxy.getDefaultHost());
    }

    public static HttpHost d() {
        return new HttpHost(Proxy.getDefaultHost(), Proxy.getDefaultPort());
    }
}
