package com.mt.airad;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class IlllIllIlllIIIII extends WebViewClient {
    final /* synthetic */ MultiAD _$1;

    IlllIllIlllIIIII(MultiAD multiAD) {
        this._$1 = multiAD;
    }

    public void onPageFinished(WebView webView, String str) {
        this._$1._$4();
        this._$1._$9.setEnabled(true);
        super.onPageFinished(webView, str);
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        this._$1._$3();
        super.onPageStarted(webView, str, bitmap);
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        boolean unused = this._$1._$1 = true;
        super.onReceivedError(webView, i, str, str2);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str.startsWith("mailto:") || str.startsWith("geo:") || str.startsWith("tel:") || str.startsWith("market:") || str.startsWith("smsto:") || str.startsWith("sms:") || str.startsWith("content:")) {
            this._$1.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        }
        webView.loadUrl(str);
        return true;
    }
}
