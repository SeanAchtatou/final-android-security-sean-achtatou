package com.mt.airad;

import android.app.Activity;
import android.content.SharedPreferences;

class IIIlIllllIllIlll {
    private static IIIlIllllIllIlll _$1 = null;
    private SharedPreferences.Editor _$2 = this._$3.edit();
    private SharedPreferences _$3;

    private IIIlIllllIllIlll(Activity activity) {
        this._$3 = activity.getSharedPreferences("information", 2);
    }

    protected static IIIlIllllIllIlll _$1(Activity activity) {
        if (_$1 == null) {
            _$1 = new IIIlIllllIllIlll(activity);
        }
        return _$1;
    }

    /* access modifiers changed from: protected */
    public float _$1(String str) {
        return this._$3.getFloat(str, 0.0f);
    }

    /* access modifiers changed from: protected */
    public boolean _$1(String str, Object obj) {
        if (obj instanceof String) {
            this._$2.putString(str, obj.toString()).commit();
            return true;
        } else if (obj instanceof Integer) {
            this._$2.putInt(str, ((Integer) obj).intValue()).commit();
            return true;
        } else if (obj instanceof Long) {
            this._$2.putLong(str, ((Long) obj).longValue()).commit();
            return true;
        } else if (obj instanceof Float) {
            this._$2.putFloat(str, ((Float) obj).floatValue()).commit();
            return true;
        } else if (!(obj instanceof Integer)) {
            return false;
        } else {
            this._$2.putBoolean(str, ((Boolean) obj).booleanValue()).commit();
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public boolean _$2(String str) {
        return this._$3.getBoolean(str, false);
    }

    /* access modifiers changed from: protected */
    public long _$3(String str) {
        return this._$3.getLong(str, 0);
    }

    /* access modifiers changed from: protected */
    public int _$4(String str) {
        return this._$3.getInt(str, 0);
    }

    /* access modifiers changed from: protected */
    public String _$5(String str) {
        return this._$3.getString(str, "");
    }
}
