package com.mt.airad;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class AirAD extends RelativeLayout {
    private IIlllIllllIIllll _$1;
    protected Activity activity;
    protected AdListener adListener = null;
    protected lIIllIIlIlIIIlll banner;
    protected llIIlllIlIllIIll manager;

    public interface AdListener {
        void onAdBannerClicked();

        void onAdBannerReceive();

        void onAdBannerReceiveFailed();

        void onMultiAdDismiss();
    }

    public AirAD(Context context) {
        super(context);
        _$1(context);
    }

    public AirAD(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        _$1(context);
    }

    private void _$1(Context context) {
        this.activity = (Activity) context;
        this._$1 = IIlllIllllIIllll._$1();
        this._$1._$1(this);
        this.manager = llIIlllIlIllIIll._$1(this);
        this.banner = new lIIllIIlIlIIIlll(this.activity, this);
    }

    public static void setGlobalParameter(String str, boolean z) {
        if (IIlllIllllIIllll._$19 == null) {
            IIlllIllllIIllll._$19 = str;
            if (z) {
                IIlllIllllIIllll._$19 = "123456789";
            }
        }
    }

    public void setAdListener(AdListener adListener2) {
        this.adListener = adListener2;
    }

    public void setLayoutParams(ViewGroup.LayoutParams layoutParams) {
        layoutParams.height = -2;
        layoutParams.width = -2;
        super.setLayoutParams(layoutParams);
    }
}
