package com.mt.airad;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

class IIIIIIllllIIIlIl extends Animation {
    private Configuration _$1;
    private String _$2;
    private Camera _$3;
    private float _$4 = 0.0f;
    private float _$5 = 0.0f;
    private float _$6;
    private float _$7;

    protected IIIIIIllllIIIlIl(Activity activity, String str, float f, float f2) {
        this._$2 = str;
        if (str.equalsIgnoreCase("x")) {
            this._$5 = -10.0f;
            this._$4 = 0.0f;
        } else if (str.equalsIgnoreCase("y")) {
            this._$1 = activity.getResources().getConfiguration();
            if (this._$1.orientation == 2) {
                this._$5 = (float) (activity.getWindowManager().getDefaultDisplay().getHeight() / 2);
            } else if (this._$1.orientation == 1) {
                this._$5 = (float) (activity.getWindowManager().getDefaultDisplay().getWidth() / 2);
            } else if (this._$1.hardKeyboardHidden == 1 || this._$1.hardKeyboardHidden == 2) {
            }
            this._$4 = 0.0f;
        } else if (str.equalsIgnoreCase("z")) {
            this._$5 = 0.0f;
            this._$4 = 0.0f;
        }
        this._$7 = f;
        this._$6 = f2;
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f, Transformation transformation) {
        float f2 = this._$7 + ((this._$6 - this._$7) * f);
        float f3 = this._$5;
        float f4 = this._$4;
        Matrix matrix = transformation.getMatrix();
        if (f2 <= -76.0f) {
            this._$3.save();
            if (this._$2.equalsIgnoreCase("x")) {
                this._$3.rotateX(-90.0f);
            } else if (this._$2.equalsIgnoreCase("y")) {
                this._$3.rotateY(-90.0f);
            } else if (this._$2.equalsIgnoreCase("z")) {
                this._$3.rotateZ(-90.0f);
            }
            this._$3.getMatrix(matrix);
            this._$3.restore();
        } else if (f2 >= 76.0f) {
            this._$3.save();
            if (this._$2.equalsIgnoreCase("x")) {
                this._$3.rotateX(90.0f);
            } else if (this._$2.equalsIgnoreCase("y")) {
                this._$3.rotateY(90.0f);
            } else if (this._$2.equalsIgnoreCase("z")) {
                this._$3.rotateZ(90.0f);
            }
            this._$3.getMatrix(matrix);
            this._$3.restore();
        } else {
            this._$3.save();
            this._$3.translate(0.0f, 0.0f, f3);
            if (this._$2.equalsIgnoreCase("x")) {
                this._$3.rotateX(f2);
            } else if (this._$2.equalsIgnoreCase("y")) {
                this._$3.rotateY(f2);
            } else if (this._$2.equalsIgnoreCase("z")) {
                this._$3.rotateZ(f2);
            }
            this._$3.translate(0.0f, 0.0f, -f3);
            this._$3.getMatrix(matrix);
            this._$3.restore();
        }
        matrix.preTranslate(-f3, -f4);
        matrix.postTranslate(f3, f4);
    }

    public void initialize(int i, int i2, int i3, int i4) {
        super.initialize(i, i2, i3, i4);
        this._$3 = new Camera();
    }
}
