package com.mt.airad;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.StatFs;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

class IIlIlIlIIIllIllI {
    private static IIlIlIlIIIllIllI _$1 = null;
    private IIlllIllllIIllll _$2 = IIlllIllllIIllll._$1();
    private AirAD _$3;
    private Bitmap _$4;
    private IIIlIllllIllIlll _$5;
    private int _$6;
    private Activity _$7;

    private IIlIlIlIIIllIllI(AirAD airAD) {
        this._$3 = airAD;
        this._$7 = airAD.activity;
        this._$5 = IIIlIllllIllIlll._$1(airAD.activity);
    }

    protected static IIlIlIlIIIllIllI _$1(AirAD airAD) {
        if (_$1 == null) {
            _$1 = new IIlIlIlIIIllIllI(airAD);
        }
        return _$1;
    }

    private void _$1(Bitmap bitmap, String str) {
        if (_$1()) {
            try {
                String str2 = Environment.getExternalStorageDirectory() + "/" + this._$2._$15;
                if (!new File(str2 + str).exists()) {
                    new File(str2).mkdirs();
                    BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(new File(str2, str)));
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, bufferedOutputStream);
                    bufferedOutputStream.flush();
                    bufferedOutputStream.close();
                }
            } catch (IOException e) {
            }
        }
    }

    private boolean _$1() {
        if (IIIIlIlIlIIIlIIl._$1(this._$7, "android.permission.WRITE_EXTERNAL_STORAGE") && Environment.getExternalStorageState().equals("mounted")) {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            if (((int) ((((long) statFs.getBlockSize()) * ((long) statFs.getAvailableBlocks())) / 1024)) > 1000) {
                return true;
            }
        }
        return false;
    }

    private boolean _$1(HashMap<String, String> hashMap) {
        try {
            HashMap<String, Bitmap> _$12 = IlIlIIlIIIlIIIIl._$1(this._$3)._$1(hashMap);
            if (_$12.isEmpty()) {
                return false;
            }
            for (String str : this._$2._$14) {
                this._$4 = _$12.get(str);
                if (this._$4 != null) {
                    if (str.equals("close")) {
                        IIlllIllllIIllll._$13 = this._$4;
                    } else if (str.equals("banner_bg")) {
                        IIlllIllllIIllll._$12 = this._$4;
                    } else if (str.equals("banner_fg")) {
                        IIlllIllllIIllll._$11 = this._$4;
                    } else if (str.equals("ad_conner")) {
                        IIlllIllllIIllll._$10 = this._$4;
                    }
                    _$1(this._$4, str);
                }
            }
            this._$3.manager._$3();
            this._$4 = null;
            return true;
        } catch (NullPointerException e) {
            return false;
        }
    }

    private boolean _$2() {
        if (!_$1()) {
            return true;
        }
        String[] strArr = this._$2._$14;
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            if (!new File(Environment.getExternalStorageDirectory() + "/" + this._$2._$15 + strArr[i]).exists()) {
                return true;
            }
        }
        return false;
    }

    private void _$3() {
        FileInputStream fileInputStream;
        for (String str : this._$2._$14) {
            try {
                fileInputStream = new FileInputStream(new File(Environment.getExternalStorageDirectory() + "/" + this._$2._$15 + str));
            } catch (FileNotFoundException e) {
                fileInputStream = null;
            }
            if (str.equals("close")) {
                IIlllIllllIIllll._$13 = BitmapFactory.decodeStream(fileInputStream);
            } else if (str.equals("banner_bg")) {
                IIlllIllllIIllll._$12 = BitmapFactory.decodeStream(fileInputStream);
            } else if (str.equals("banner_fg")) {
                IIlllIllllIIllll._$11 = BitmapFactory.decodeStream(fileInputStream);
            } else if (str.equals("ad_conner")) {
                IIlllIllllIIllll._$10 = BitmapFactory.decodeStream(fileInputStream);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean _$1(String str, String str2, String str3) {
        HashMap hashMap = new HashMap();
        try {
            JSONObject jSONObject = new JSONObject(str2);
            String[] strArr = this._$2._$14;
            if (jSONObject.length() != 0) {
                for (String str4 : strArr) {
                    if (!jSONObject.optString(str4).equals("")) {
                        hashMap.put(str4, str3 + jSONObject.getString(str4));
                    }
                }
            }
        } catch (JSONException e) {
        }
        if (_$1(hashMap) && _$1()) {
            this._$5._$1("FV", Integer.valueOf(Integer.parseInt(str)));
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public String _$4() {
        if (_$2()) {
            this._$6 = 0;
        } else {
            this._$6 = this._$5._$4("FV");
            _$3();
            this._$3.manager._$3();
        }
        return "" + this._$6;
    }
}
