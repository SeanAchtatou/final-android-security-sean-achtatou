package com.mt.airad;

import android.app.Activity;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import com.millennialmedia.android.MMAdView;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class IIlllllIlIIlIlIl {
    private IIlllIllllIIllll _$1;
    private IIIlIllllIllIlll _$2;
    private boolean _$3 = true;
    private lIlIllIIllIllIII _$4;
    private HttpPost _$5 = null;
    private LocationManager _$6;
    private String _$7;
    private Criteria _$8;
    private Activity _$9;

    protected IIlllllIlIIlIlIl(AirAD airAD) {
        this._$9 = airAD.activity;
        this._$6 = (LocationManager) this._$9.getSystemService("location");
        this._$4 = new lIlIllIIllIllIII(airAD);
        this._$2 = IIIlIllllIllIlll._$1(this._$9);
        this._$1 = IIlllIllllIIllll._$1();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private Location _$1() {
        if (!IIIIlIlIlIIIlIIl._$1(this._$9, "android.permission.READ_PHONE_STATE")) {
            return null;
        }
        TelephonyManager telephonyManager = (TelephonyManager) this._$9.getSystemService("phone");
        if (telephonyManager == null) {
            return null;
        }
        GsmCellLocation gsmCellLocation = (GsmCellLocation) telephonyManager.getCellLocation();
        if (gsmCellLocation == null) {
            return null;
        }
        int cid = gsmCellLocation.getCid();
        int lac = gsmCellLocation.getLac();
        int intValue = Integer.valueOf(telephonyManager.getNetworkOperator().substring(0, 3)).intValue();
        int intValue2 = Integer.valueOf(telephonyManager.getNetworkOperator().substring(3, 5)).intValue();
        try {
            JSONObject jSONObject = new JSONObject();
            JSONArray jSONArray = new JSONArray();
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("cell_id", cid);
            jSONObject2.put("location_area_code", lac);
            jSONObject2.put("mobile_country_code", intValue);
            jSONObject2.put("mobile_network_code", intValue2);
            jSONArray.put(jSONObject2);
            jSONObject.put("version", "1.1.0");
            jSONObject.put("host", "maps.google.com");
            jSONObject.put("request_address", true);
            jSONObject.put("cell_towers", jSONArray);
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            this._$5 = new HttpPost("http://www.google.hk/loc/json");
            this._$5.setEntity(new StringEntity(jSONObject.toString()));
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(defaultHttpClient.execute(this._$5).getEntity().getContent()));
            StringBuffer stringBuffer = new StringBuffer();
            for (String readLine = bufferedReader.readLine(); readLine != null; readLine = bufferedReader.readLine()) {
                stringBuffer.append(readLine);
            }
            bufferedReader.close();
            JSONObject jSONObject3 = new JSONObject(stringBuffer.toString());
            Location location = new Location("");
            try {
                location.setLatitude(Double.parseDouble(jSONObject3.getJSONObject("location").getString(MMAdView.KEY_LATITUDE)));
                location.setLongitude(Double.parseDouble(jSONObject3.getJSONObject("location").getString(MMAdView.KEY_LONGITUDE)));
                this._$5 = null;
                return location;
            } catch (IOException | UnsupportedEncodingException | JSONException e) {
                return location;
            }
        } catch (JSONException e2) {
            return null;
        } catch (UnsupportedEncodingException e3) {
            return null;
        } catch (IOException e4) {
            return null;
        }
    }

    private JSONObject _$1(String str) {
        try {
            HttpResponse execute = new DefaultHttpClient().execute(new HttpGet(str));
            if (execute.getStatusLine().getStatusCode() == 200) {
                return new JSONObject(EntityUtils.toString(execute.getEntity()));
            }
            return null;
        } catch (IOException | ClientProtocolException | JSONException e) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.location.Location _$2() {
        /*
            r4 = this;
            r3 = 0
            android.app.Activity r0 = r4._$9
            java.lang.String r1 = "location"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.location.LocationManager r0 = (android.location.LocationManager) r0
            r4._$6 = r0
            android.location.LocationManager r0 = r4._$6
            if (r0 == 0) goto L_0x001b
            android.location.LocationManager r0 = r4._$6
            java.lang.String r1 = "gps"
            boolean r0 = r0.isProviderEnabled(r1)
            if (r0 != 0) goto L_0x001d
        L_0x001b:
            r0 = r3
        L_0x001c:
            return r0
        L_0x001d:
            android.location.Criteria r0 = new android.location.Criteria     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            r0.<init>()     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            r4._$8 = r0     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            android.location.Criteria r0 = r4._$8     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            r1 = 1
            r0.setAccuracy(r1)     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            android.location.Criteria r0 = r4._$8     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            r1 = 0
            r0.setAltitudeRequired(r1)     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            android.location.Criteria r0 = r4._$8     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            r1 = 0
            r0.setBearingRequired(r1)     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            android.location.Criteria r0 = r4._$8     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            r1 = 0
            r0.setCostAllowed(r1)     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            android.location.Criteria r0 = r4._$8     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            r1 = 0
            r0.setSpeedRequired(r1)     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            android.location.Criteria r0 = r4._$8     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            r1 = 1
            r0.setPowerRequirement(r1)     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            android.location.LocationManager r0 = r4._$6     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            android.location.Criteria r1 = r4._$8     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            r2 = 1
            java.lang.String r0 = r0.getBestProvider(r1, r2)     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            r4._$7 = r0     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            android.location.Location r0 = new android.location.Location     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            java.lang.String r1 = r4._$7     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            r0.<init>(r1)     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            r0 = 0
            r4._$8 = r0     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            r0 = 0
            r4._$7 = r0     // Catch:{ IllegalArgumentException -> 0x0062, SecurityException -> 0x0065, all -> 0x0068 }
            r0 = r3
            goto L_0x001c
        L_0x0062:
            r0 = move-exception
            r0 = r3
            goto L_0x001c
        L_0x0065:
            r0 = move-exception
            r0 = r3
            goto L_0x001c
        L_0x0068:
            r0 = move-exception
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mt.airad.IIlllllIlIIlIlIl._$2():android.location.Location");
    }

    private boolean _$3() {
        return IIIIlIlIlIIIlIIl._$1(this._$9, "android.permission.ACCESS_FINE_LOCATION");
    }

    /* access modifiers changed from: protected */
    public String[] _$1(Location location) {
        String[] strArr = {"0", "0", "0"};
        JSONObject _$12 = _$1("http://maps.google.com/maps/api/geocode/json?latlng=" + location.getLatitude() + "," + location.getLongitude() + "&sensor=true&language=en");
        if (_$12 != null) {
            try {
                if (_$12.getString("status").equals("OK")) {
                    List asList = Arrays.asList("administrative_area_level_1", "locality", "sublocality");
                    JSONArray jSONArray = _$12.getJSONArray("results");
                    int length = jSONArray.length();
                    JSONArray jSONArray2 = jSONArray.getJSONObject(length - 1).getJSONArray("address_components");
                    String string = jSONArray2.getJSONObject(jSONArray2.length() - 1).getString("long_name");
                    if (!string.equals("China")) {
                        strArr[0] = string;
                    } else {
                        int i = 0;
                        for (int i2 = 0; i2 < Math.min(length, 4) && i < 3; i2++) {
                            if (asList.contains(jSONArray.getJSONObject((length - i2) - 1).getJSONArray("types").getString(0))) {
                                int i3 = i + 1;
                                strArr[i] = jSONArray.getJSONObject((length - i2) - 1).getJSONArray("address_components").getJSONObject(0).getString("long_name");
                                i = i3;
                            }
                        }
                    }
                }
            } catch (JSONException e) {
            }
        }
        return strArr;
    }

    public Location _$2(String str) {
        Location location = new Location("");
        int indexOf = str.indexOf(44);
        String substring = str.substring(0, indexOf);
        String substring2 = str.substring(indexOf + 1);
        location.setLongitude(Double.parseDouble(substring));
        location.setLatitude(Double.parseDouble(substring2));
        return location;
    }

    /* access modifiers changed from: protected */
    public boolean _$2(Location location) {
        double longitude = location.getLongitude();
        double latitude = location.getLatitude();
        if (Math.abs(this._$2._$1("LO") - ((float) longitude)) < IIlllIllllIIllll._$3 && Math.abs(this._$2._$1("LA") - ((float) latitude)) < IIlllIllllIIllll._$3) {
            return true;
        }
        this._$2._$1("LO", Float.valueOf((float) longitude));
        this._$2._$1("LA", Float.valueOf((float) latitude));
        return false;
    }

    /* access modifiers changed from: protected */
    public void _$4() {
        HashMap<String, String> hashMap = new HashMap<>();
        Location location = null;
        if (_$3() && (location = _$2()) == null) {
            location = _$1();
        }
        if (location != null) {
            if (this._$3) {
                this._$3 = false;
            } else if (_$2(location)) {
                return;
            }
            hashMap.put("LO", Double.toString(location.getLongitude()));
            hashMap.put("LA", Double.toString(location.getLatitude()));
            this._$4._$4(hashMap);
            String[] _$12 = _$1(location);
            hashMap.put("P", _$12[0]);
            hashMap.put("C", _$12[1]);
            hashMap.put("A", _$12[2]);
            this._$1._$20 = hashMap;
            this._$4._$3(hashMap);
        }
    }
}
