package com.mt.airad;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public class MultiAD extends Activity implements Animation.AnimationListener {
    /* access modifiers changed from: private */
    public boolean _$1 = false;
    private String _$10 = null;
    private llIIlllIlIllIIll _$2;
    /* access modifiers changed from: private */
    public ProgressBar _$3;
    private ImageView _$4;
    private ImageButton _$5;
    private RelativeLayout _$6;
    private Animation _$7;
    private Animation _$8;
    /* access modifiers changed from: private */
    public WebView _$9;

    /* access modifiers changed from: private */
    public void _$1() {
        try {
            this._$9.stopLoading();
            if (this._$3 != null) {
                this._$3 = null;
                this._$1 = true;
            }
            this._$5.setClickable(false);
            this._$5.setVisibility(8);
            _$1(this._$6);
        } catch (Exception e) {
            if (this._$3 != null) {
                this._$3 = null;
                this._$1 = true;
            }
            finish();
        }
    }

    private void _$1(View view) {
        if (view.getVisibility() == 0) {
            view.setVisibility(8);
            this._$7 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
            this._$7.setAnimationListener(this);
            this._$7.setDuration(400);
            view.startAnimation(this._$7);
            return;
        }
        finish();
    }

    private void _$10() {
        requestWindowFeature(1);
    }

    /* access modifiers changed from: private */
    public void _$2() {
        if (this._$3 != null) {
            this._$3.setVisibility(8);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(800);
            this._$3.startAnimation(alphaAnimation);
            this._$3 = null;
            if (!this._$1) {
                this._$2._$3(this._$10);
            }
        }
    }

    private void _$2(View view) {
        if (view.getVisibility() == 8) {
            view.setVisibility(0);
            this._$8 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
            this._$8.setDuration(400);
            view.startAnimation(this._$8);
        }
    }

    /* access modifiers changed from: private */
    public void _$3() {
        if (this._$3 != null) {
            this._$3.setVisibility(0);
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(800);
            this._$3.startAnimation(alphaAnimation);
        }
    }

    /* access modifiers changed from: private */
    public void _$4() {
        if (this._$5.getVisibility() != 0 && IIlllIllllIIllll._$13 != null) {
            this._$5.setImageBitmap(IIlllIllllIIllll._$13);
            this._$5.setVisibility(0);
        }
    }

    private void _$5() {
        this._$6 = new RelativeLayout(this);
        this._$6.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this._$6.addView(this._$9, new ViewGroup.LayoutParams(-1, -1));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(40, 40);
        layoutParams.addRule(10, -1);
        layoutParams.addRule(11, -1);
        layoutParams.topMargin = 10;
        layoutParams.rightMargin = 10;
        this._$5.setVisibility(4);
        this._$6.addView(this._$5, layoutParams);
        this._$6.addView(_$8(), new ViewGroup.LayoutParams(-1, -1));
        setContentView(this._$6, new ViewGroup.LayoutParams(-1, -1));
        this._$6.setVisibility(8);
    }

    private void _$6() {
        this._$5 = new ImageButton(this);
        this._$5.setBackgroundColor(Color.argb(0, 0, 0, 0));
        this._$5.setOnClickListener(new lIIIlllIlllIIIII(this));
    }

    private void _$7() {
        this._$9 = new WebView(this);
        this._$9.addView(_$9(), new ViewGroup.LayoutParams(-1, -1));
        this._$9.setEnabled(false);
        this._$9.getSettings().setJavaScriptEnabled(true);
        this._$9.setVerticalScrollBarEnabled(false);
        this._$9.setHorizontalScrollBarEnabled(false);
        this._$9.setWebViewClient(new IlllIllIlllIIIII(this));
        this._$9.setWebChromeClient(new llllIllIlllIIIII(this));
        this._$9.setDownloadListener(new IIIIlllIlllIIIII(this));
    }

    private RelativeLayout _$8() {
        RelativeLayout relativeLayout = new RelativeLayout(this);
        this._$4 = new ImageView(this);
        this._$4.setImageBitmap(IIlllIllllIIllll._$10);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(12);
        layoutParams.addRule(11);
        relativeLayout.addView(this._$4, layoutParams);
        return relativeLayout;
    }

    private RelativeLayout _$9() {
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        RelativeLayout relativeLayout = new RelativeLayout(this);
        this._$3 = new ProgressBar(this, null, 16842872);
        this._$3.setMax(100);
        this._$3.setVisibility(8);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((defaultDisplay.getWidth() * 2) / 3, 10);
        layoutParams.addRule(12);
        layoutParams.setMargins(defaultDisplay.getWidth() / 6, 0, defaultDisplay.getWidth() / 6, defaultDisplay.getHeight() / 3);
        relativeLayout.addView(this._$3, layoutParams);
        return relativeLayout;
    }

    /* access modifiers changed from: protected */
    public void _$1(String str) {
        if (str == null) {
            finish();
            return;
        }
        this._$9.loadUrl(str);
        _$2(this._$6);
        this._$9.setFocusableInTouchMode(true);
        this._$9.requestFocus();
    }

    public void onAnimationEnd(Animation animation) {
        if (animation == this._$7) {
            finish();
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        _$10();
        setRequestedOrientation(1);
        this._$2 = llIIlllIlIllIIll._$15();
        _$7();
        _$6();
        _$5();
        Intent intent = getIntent();
        this._$10 = intent.getStringExtra("adID");
        _$1(intent.getStringExtra("adURL"));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this._$3 == null && !this._$1) {
            this._$2._$2(this._$10);
        }
        this._$9.clearHistory();
        this._$9.clearCache(true);
        this._$9.destroy();
        super.onDestroy();
        this._$2._$1(0);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            _$1();
            return true;
        } else if (i == 82) {
            return true;
        } else {
            super.onKeyDown(i, keyEvent);
            return false;
        }
    }
}
