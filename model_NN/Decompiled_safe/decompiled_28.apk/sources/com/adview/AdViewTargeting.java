package com.adview;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

public class AdViewTargeting {
    private static AdArea adArea;
    private static GregorianCalendar birthDate;
    private static Gender gender;
    private static Set<String> keywordSet;
    private static String keywords;
    private static String postalCode;
    private static RunMode runMode;
    private static UpdateMode updateMode;

    public enum AdArea {
        TOP,
        BOTTOM
    }

    public enum Gender {
        UNKNOWN,
        MALE,
        FEMALE
    }

    public enum RunMode {
        NORMAL,
        TEST
    }

    public enum UpdateMode {
        DEFAULT,
        EVERYTIME
    }

    static {
        resetData();
    }

    private static void resetData() {
        runMode = RunMode.NORMAL;
        updateMode = UpdateMode.DEFAULT;
        adArea = AdArea.BOTTOM;
        gender = Gender.UNKNOWN;
        birthDate = null;
        postalCode = null;
        keywords = null;
        keywordSet = null;
    }

    public static RunMode getRunMode() {
        return runMode;
    }

    public static void setRunMode(RunMode runMode2) {
        if (runMode2 == null) {
            runMode2 = RunMode.NORMAL;
        }
        runMode = runMode2;
    }

    public static UpdateMode getUpdateMode() {
        return updateMode;
    }

    public static void setUpdateMode(UpdateMode updateMode2) {
        if (updateMode2 == null) {
            updateMode2 = UpdateMode.DEFAULT;
        }
        updateMode = updateMode2;
    }

    public static AdArea getAdArea() {
        return adArea;
    }

    public static void setAdArea(AdArea area) {
        if (area == null) {
            area = AdArea.TOP;
        }
        adArea = area;
    }

    public static Gender getGender() {
        return gender;
    }

    public static void setGender(Gender gender2) {
        if (gender2 == null) {
            gender2 = Gender.UNKNOWN;
        }
        gender = gender2;
    }

    public static int getAge() {
        if (birthDate != null) {
            return GregorianCalendar.getInstance().get(1) - birthDate.get(1);
        }
        return -1;
    }

    public static void setAge(int age) {
        birthDate = new GregorianCalendar(Calendar.getInstance().get(1) - age, 0, 1);
    }

    public static GregorianCalendar getBirthDate() {
        return birthDate;
    }

    public static void setBirthDate(GregorianCalendar birthDate2) {
        birthDate = birthDate2;
    }

    public static String getPostalCode() {
        return postalCode;
    }

    public static void setPostalCode(String postalCode2) {
        postalCode = postalCode2;
    }

    public static String getKeywords() {
        return keywords;
    }

    public static void setKeywords(String keywords2) {
        keywords = keywords2;
    }

    public static Set<String> getKeywordSet() {
        return keywordSet;
    }

    public static void setKeywordSet(Set<String> keywords2) {
        keywordSet = keywords2;
    }

    public static void addKeyword(String keyword) {
        if (keywordSet == null) {
            keywordSet = new HashSet();
        }
        keywordSet.add(keyword);
    }
}
