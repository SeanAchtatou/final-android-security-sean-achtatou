package com.adview.adapters;

import android.app.Activity;
import android.util.Log;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.text.SimpleDateFormat;

public class AdMobAdapter extends AdViewAdapter implements AdListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$adview$AdViewTargeting$Gender;

    static /* synthetic */ int[] $SWITCH_TABLE$com$adview$AdViewTargeting$Gender() {
        int[] iArr = $SWITCH_TABLE$com$adview$AdViewTargeting$Gender;
        if (iArr == null) {
            iArr = new int[AdViewTargeting.Gender.values().length];
            try {
                iArr[AdViewTargeting.Gender.FEMALE.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[AdViewTargeting.Gender.MALE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AdViewTargeting.Gender.UNKNOWN.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$adview$AdViewTargeting$Gender = iArr;
        }
        return iArr;
    }

    public AdMobAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
    }

    /* access modifiers changed from: protected */
    public String birthdayForAdViewTargeting() {
        if (AdViewTargeting.getBirthDate() != null) {
            return new SimpleDateFormat("yyyyMMdd").format(AdViewTargeting.getBirthDate().getTime());
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public AdRequest.Gender genderForAdViewTargeting() {
        switch ($SWITCH_TABLE$com$adview$AdViewTargeting$Gender()[AdViewTargeting.getGender().ordinal()]) {
            case 2:
                return AdRequest.Gender.MALE;
            case 3:
                return AdRequest.Gender.FEMALE;
            default:
                return null;
        }
    }

    public void handle() {
        Activity activity;
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Into AdMob");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null && (activity = adViewLayout.activityReference.get()) != null) {
            AdView adView = new AdView(activity, AdSize.BANNER, this.ration.key);
            adView.setAdListener(this);
            adView.loadAd(requestForAdWhirlLayout(adViewLayout));
        }
    }

    /* access modifiers changed from: protected */
    public AdRequest requestForAdWhirlLayout(AdViewLayout layout) {
        AdRequest result = new AdRequest();
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            result.setTesting(true);
        } else if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.NORMAL) {
            result.setTesting(false);
        } else {
            result.setTesting(false);
        }
        result.setGender(genderForAdViewTargeting());
        result.setBirthday(birthdayForAdViewTargeting());
        result.setKeywords(AdViewTargeting.getKeywordSet());
        return result;
    }

    public void onDismissScreen(Ad arg0) {
    }

    public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "AdMob fail");
        }
        arg0.setAdListener(null);
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover_pri();
            adViewLayout.rotateThreadedPri();
        }
    }

    public void onLeaveApplication(Ad arg0) {
    }

    public void onPresentScreen(Ad arg0) {
    }

    public void onReceiveAd(Ad arg0) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "AdMob success");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null && (arg0 instanceof AdView)) {
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.handler.post(new AdViewLayout.ViewAdRunnable(adViewLayout, (AdView) arg0));
            adViewLayout.rotateThreadedDelayed();
        }
    }
}
