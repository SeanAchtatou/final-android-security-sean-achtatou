package com.adview.adapters;

import android.app.Activity;
import android.util.Log;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import com.mt.airad.AirAD;

public class AirAdAdapter extends AdViewAdapter implements AirAD.AdListener {
    private AirAD airAD = null;

    public AirAdAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            AirAD.setGlobalParameter(ration.key, true);
        } else {
            AirAD.setGlobalParameter(ration.key, false);
        }
    }

    public void handle() {
        Activity activity;
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Into AirAD");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null && (activity = adViewLayout.activityReference.get()) != null) {
            this.airAD = new AirAD(activity);
            this.airAD.setAdListener(this);
        }
    }

    public void onAdBannerClicked() {
    }

    public void onAdBannerReceive() {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "AirAD success");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.handler.post(new AdViewLayout.ViewAdRunnable(adViewLayout, this.airAD));
            adViewLayout.rotateThreadedDelayed();
        }
    }

    public void onAdBannerReceiveFailed() {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "AirAD failure");
        }
        this.airAD.setAdListener(null);
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover_pri();
            adViewLayout.rotateThreadedPri();
        }
    }

    public void onMultiAdDismiss() {
    }
}
