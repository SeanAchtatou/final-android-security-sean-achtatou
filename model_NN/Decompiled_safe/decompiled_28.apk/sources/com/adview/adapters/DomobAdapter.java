package com.adview.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import cn.domob.android.ads.DomobAdListener;
import cn.domob.android.ads.DomobAdManager;
import cn.domob.android.ads.DomobAdView;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.obj.Extra;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;

public class DomobAdapter extends AdViewAdapter implements DomobAdListener {
    public DomobAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
    }

    public void handle() {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Into Domob");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            Extra extra = adViewLayout.extra;
            int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
            int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
            Activity activity = adViewLayout.activityReference.get();
            if (activity != null) {
                DomobAdView ad = new DomobAdView(activity);
                DomobAdManager.setPublisherId(this.ration.key);
                if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                    DomobAdManager.setIsTestMode(true);
                } else if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.NORMAL) {
                    DomobAdManager.setIsTestMode(false);
                } else {
                    DomobAdManager.setIsTestMode(false);
                }
                ad.setAdListener(this);
                ad.setBackgroundColor(bgColor);
                ad.setPrimaryTextColor(fgColor);
                ad.requestFreshAd();
            }
        }
    }

    public void onFailedToReceiveFreshAd(DomobAdView arg0) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Domob failure");
        }
        arg0.setAdListener(null);
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover_pri();
            adViewLayout.rotateThreadedPri();
        }
    }

    public void onReceivedFreshAd(DomobAdView arg0) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Domob success");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.handler.post(new AdViewLayout.ViewAdRunnable(adViewLayout, arg0));
            adViewLayout.rotateThreadedDelayed();
        }
    }
}
