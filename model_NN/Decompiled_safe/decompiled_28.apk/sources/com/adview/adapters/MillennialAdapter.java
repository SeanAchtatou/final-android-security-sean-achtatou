package com.adview.adapters;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import com.madhouse.android.ads.AdManager;
import com.millennialmedia.android.MMAdView;
import com.millennialmedia.android.MMAdViewSDK;
import java.util.Hashtable;

public class MillennialAdapter extends AdViewAdapter implements MMAdView.MMAdListener {
    public MillennialAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
    }

    public void handle() {
        String keywords;
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            Hashtable<String, String> map = new Hashtable<>();
            AdViewTargeting.Gender gender = AdViewTargeting.getGender();
            if (gender == AdViewTargeting.Gender.MALE) {
                map.put(MMAdView.KEY_GENDER, AdManager.USER_GENDER_MALE);
            } else if (gender == AdViewTargeting.Gender.FEMALE) {
                map.put(MMAdView.KEY_GENDER, AdManager.USER_GENDER_FEMALE);
            }
            int age = AdViewTargeting.getAge();
            if (age != -1) {
                map.put(MMAdView.KEY_AGE, String.valueOf(age));
            }
            String postalCode = AdViewTargeting.getPostalCode();
            if (!TextUtils.isEmpty(postalCode)) {
                map.put(MMAdView.KEY_ZIP_CODE, postalCode);
            }
            if (AdViewTargeting.getKeywordSet() != null) {
                keywords = TextUtils.join(",", AdViewTargeting.getKeywordSet());
            } else {
                keywords = AdViewTargeting.getKeywords();
            }
            if (!TextUtils.isEmpty(keywords)) {
                map.put(MMAdView.KEY_KEYWORDS, keywords);
            }
            map.put("vendor", "adwhirl");
            MMAdView adView = new MMAdView((Activity) adViewLayout.getContext(), this.ration.key, MMAdView.BANNER_AD_TOP, -1, map);
            adView.setId(MMAdViewSDK.DEFAULT_VIEWID);
            adView.setListener(this);
            adView.callForAd();
            adView.setHorizontalScrollBarEnabled(false);
            adView.setVerticalScrollBarEnabled(false);
        }
    }

    public void MMAdReturned(MMAdView adView) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Millennial success");
        }
        adView.setListener(null);
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.handler.post(new AdViewLayout.ViewAdRunnable(adViewLayout, adView));
            adViewLayout.rotateThreadedDelayed();
        }
    }

    public void MMAdFailed(MMAdView adView) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Millennial failure");
        }
        adView.setListener(null);
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover_pri();
            adViewLayout.rotateThreadedPri();
        }
    }

    public void MMAdClickedToNewBrowser(MMAdView adview) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Millennial Ad clicked, new browser launched");
        }
    }

    public void MMAdClickedToOverlay(MMAdView adview) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Millennial Ad Clicked to overlay");
        }
    }

    public void MMAdOverlayLaunched(MMAdView adview) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Millennial Ad Overlay Launched");
        }
    }

    public void MMAdRequestIsCaching(MMAdView arg0) {
    }
}
