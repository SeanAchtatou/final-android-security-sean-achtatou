package com.adview.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.ViewGroup;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import com.vpon.adon.android.AdListener;
import com.vpon.adon.android.AdOnPlatform;
import com.vpon.adon.android.AdView;

public class VponAdapter extends AdViewAdapter implements AdListener {
    public VponAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
    }

    public void handle() {
        Activity activity;
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Into Vpon");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null && (activity = adViewLayout.activityReference.get()) != null) {
            try {
                AdView adView = new AdView(activity);
                adView.setLicenseKey(this.ration.key, AdOnPlatform.CN, false);
                adView.setAdListener(this);
                adViewLayout.addView(adView, new ViewGroup.LayoutParams(-2, -2));
            } catch (IllegalArgumentException e) {
                adViewLayout.rollover();
            }
        }
    }

    public void onFailedToRecevieAd(AdView arg0) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Vpon fail");
        }
        arg0.setAdListener(null);
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover_pri();
            adViewLayout.rotateThreadedPri();
        }
    }

    public void onRecevieAd(AdView arg0) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Vpon success");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.handler.post(new AdViewLayout.ViewAdRunnable(adViewLayout, arg0));
            adViewLayout.rotateThreadedDelayed();
        }
    }
}
