package com.adview.adapters;

import android.graphics.Color;
import android.util.Log;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.ad.KyAdView;
import com.adview.obj.Extra;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;

public class AdViewHouseAdapter extends AdViewAdapter implements KyAdView.onAdListener {
    public AdViewHouseAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
    }

    public void handle() {
        KyAdView kyAdView;
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Into AdViewHouse");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            Extra extra = adViewLayout.extra;
            int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
            int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
            if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                kyAdView = new KyAdView(adViewLayout.getContext(), this.ration.key, this.ration.key2, this.ration.logo, extra.cycleTime, true, bgColor, fgColor);
            } else {
                kyAdView = new KyAdView(adViewLayout.getContext(), this.ration.key, this.ration.key2, this.ration.logo, extra.cycleTime, false, bgColor, fgColor);
            }
            kyAdView.setAdListener(this);
            kyAdView.setHorizontalScrollBarEnabled(false);
            kyAdView.setVerticalScrollBarEnabled(false);
        }
    }

    public void onConnectFailed(KyAdView view) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "AdViewHouse failure");
        }
        view.setAdListener(null);
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover_pri();
            adViewLayout.rotateThreadedPri();
        }
    }

    public void onReceivedAd(KyAdView view) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "AdViewHouse success");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.handler.post(new AdViewLayout.ViewAdRunnable(adViewLayout, view));
            adViewLayout.rotateThreadedDelayed();
        }
    }
}
