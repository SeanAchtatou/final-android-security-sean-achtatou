package com.adview.adapters;

import android.app.Activity;
import android.util.Log;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import com.energysource.szj.embeded.AdListener;
import com.energysource.szj.embeded.AdManager;
import com.energysource.szj.embeded.AdView;

public class AdTouchAdapter extends AdViewAdapter implements AdListener {
    private int area = 81;

    public AdTouchAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
    }

    public void handle() {
        Activity activity;
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Into AdTouch");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null && (activity = adViewLayout.activityReference.get()) != null) {
            if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                AdManager.openDebug();
            }
            AdManager.openPermissionJudge();
            AdManager.initAd(activity, this.ration.key);
            if (AdViewTargeting.getAdArea() == AdViewTargeting.AdArea.BOTTOM) {
                this.area = 81;
            } else {
                this.area = 49;
            }
            AdManager.addAd(101, AdManager.AD_FILL_PARENT, this.area, 0, 0);
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.rotateThreadedDelayed();
        }
    }

    public void failedReceiveAd(AdView arg0) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "AdTouch fail");
        }
    }

    public void receiveAd(AdView arg0) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "AdTouch success");
        }
    }
}
