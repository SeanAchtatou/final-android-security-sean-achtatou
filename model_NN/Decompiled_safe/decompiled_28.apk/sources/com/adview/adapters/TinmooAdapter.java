package com.adview.adapters;

import android.app.Activity;
import android.util.Log;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import com.ignitevision.android.ads.AdManager;
import com.ignitevision.android.ads.AdView;

public class TinmooAdapter extends AdViewAdapter {
    public TinmooAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
    }

    public void handle() {
        Activity activity;
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Into Tinmoo");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null && (activity = adViewLayout.activityReference.get()) != null) {
            if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                AdManager.setTest(true);
            } else {
                AdManager.setTest(false);
            }
            AdManager.setPublisherKey(this.ration.key);
            AdView adView = new AdView(activity);
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.handler.post(new AdViewLayout.ViewAdRunnable(adViewLayout, adView));
            adViewLayout.rotateThreadedDelayed();
        }
    }
}
