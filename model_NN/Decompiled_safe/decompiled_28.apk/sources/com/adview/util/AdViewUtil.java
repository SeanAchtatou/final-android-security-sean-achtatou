package com.adview.util;

public class AdViewUtil {
    public static final String ADVIEW = "AdView SDK";
    public static final int NETWORK_TYPE_4THSCREEN = 13;
    public static final int NETWORK_TYPE_ADCHINA = 26;
    public static final int NETWORK_TYPE_ADMOB = 1;
    public static final int NETWORK_TYPE_ADSENSE = 14;
    public static final int NETWORK_TYPE_ADTOUCH = 32;
    public static final int NETWORK_TYPE_ADVIEWAD = 28;
    public static final int NETWORK_TYPE_ADWHIRL = 10;
    public static final int NETWORK_TYPE_ADWO = 33;
    public static final int NETWORK_TYPE_AIRAD = 34;
    public static final int NETWORK_TYPE_APPMEDIA = 36;
    public static final int NETWORK_TYPE_CASEE = 24;
    public static final int NETWORK_TYPE_DOMOB = 30;
    public static final int NETWORK_TYPE_DOUBLECLICK = 15;
    public static final int NETWORK_TYPE_EVENT = 17;
    public static final int NETWORK_TYPE_GENERIC = 16;
    public static final int NETWORK_TYPE_GREYSTRIP = 7;
    public static final int NETWORK_TYPE_JUMPTAP = 2;
    public static final int NETWORK_TYPE_KUAIYOU = 23;
    public static final int NETWORK_TYPE_LIVERAIL = 5;
    public static final int NETWORK_TYPE_MDOTM = 12;
    public static final int NETWORK_TYPE_MEDIALETS = 4;
    public static final int NETWORK_TYPE_MILLENNIAL = 6;
    public static final int NETWORK_TYPE_MOBCLIX = 11;
    public static final int NETWORK_TYPE_QUATTRO = 8;
    public static final int NETWORK_TYPE_SMARTAD = 29;
    public static final int NETWORK_TYPE_TINMOO = 37;
    public static final int NETWORK_TYPE_VIDEOEGG = 3;
    public static final int NETWORK_TYPE_VPON = 31;
    public static final int NETWORK_TYPE_WIYUN = 25;
    public static final int NETWORK_TYPE_WOOBOO = 21;
    public static final int NETWORK_TYPE_WQ = 35;
    public static final int NETWORK_TYPE_YOUMI = 22;
    public static final int NETWORK_TYPE_ZESTADZ = 20;
    public static final int VERSION = 255;
    public static String urlClick = "http://www.adview.cn/agent/agent3.php?appid=%s&nid=%s&type=%d&uuid=%s&country_code=%s&appver=%d&client=0";
    public static final String urlConfig = "http://www.adview.cn/agent/agent1_android.php?appid=%s&appver=%d&client=0";
    public static String urlImpression = "http://www.adview.cn/agent/agent2.php?appid=%s&nid=%s&type=%d&uuid=%s&country_code=%s&appver=%d&client=0";

    public static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 15;
            int two_halfs = 0;
            while (true) {
                if (halfbyte < 0 || halfbyte > 9) {
                    buf.append((char) ((halfbyte - 10) + 97));
                } else {
                    buf.append((char) (halfbyte + 48));
                }
                halfbyte = data[i] & 15;
                int two_halfs2 = two_halfs + 1;
                if (two_halfs >= 1) {
                    break;
                }
                two_halfs = two_halfs2;
            }
        }
        return buf.toString();
    }
}
