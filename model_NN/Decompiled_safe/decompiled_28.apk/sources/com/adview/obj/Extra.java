package com.adview.obj;

import com.adview.util.AdViewUtil;

public class Extra {
    public int bgAlpha = 1;
    public int bgBlue = 0;
    public int bgGreen = 0;
    public int bgRed = 0;
    public int cycleTime = 30;
    public int fgAlpha = 1;
    public int fgBlue = AdViewUtil.VERSION;
    public int fgGreen = AdViewUtil.VERSION;
    public int fgRed = AdViewUtil.VERSION;
    public int locationOn = 0;
    public String report = "";
    public int transition = 0;
}
