package com.adview.ad;

public class RetAdBean {
    private String adLink;
    private int adLinkType;
    private String adShowPic;
    private String adShowText;
    private int adShowType;
    private String idAd;
    private String idApp;

    public String getIdApp() {
        return this.idApp;
    }

    public void setIdApp(String s) {
        this.idApp = new String(s);
    }

    public String getIdAd() {
        return this.idAd;
    }

    public void setIdAd(String s) {
        this.idAd = new String(s);
    }

    public int getAdShowType() {
        return this.adShowType;
    }

    public void setAdShowType(int a) {
        this.adShowType = a;
    }

    public String getAdShowText() {
        return this.adShowText;
    }

    public void setAdShowText(String s) {
        this.adShowText = new String(s);
    }

    public String getAdShowPic() {
        return this.adShowPic;
    }

    public void setAdShowPic(String s) {
        this.adShowPic = new String(s);
    }

    public int getAdLinkType() {
        return this.adLinkType;
    }

    public void setAdLinkType(int a) {
        this.adLinkType = a;
    }

    public String getAdLink() {
        return this.adLink;
    }

    public void setAdLink(String s) {
        this.adLink = new String(s);
    }
}
