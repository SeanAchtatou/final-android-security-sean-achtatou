package com.millennialmedia.android;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import com.energysource.szj.embeded.AdManager;

class AccelerometerHelper {
    private static SensorEventListener accelerometerEventListener = new SensorEventListener() {
        private long currentTime = 0;
        private float force = 0.0f;
        private float lastX = 0.0f;
        private float lastY = 0.0f;
        private float lastZ = 0.0f;
        private long prevShakeTime = 0;
        private long prevTime = 0;
        private long timeDifference = 0;
        private float x = 0.0f;
        private float y = 0.0f;
        private float z = 0.0f;

        public void onSensorChanged(SensorEvent event) {
            this.currentTime = event.timestamp;
            this.x = event.values[0];
            this.y = event.values[1];
            this.z = event.values[2];
            this.timeDifference = this.currentTime - this.prevTime;
            if (this.timeDifference > 500) {
                AccelerometerHelper.listener.didAccelerate(this.x, this.y, this.z);
                this.force = Math.abs(((((this.x + this.y) + this.z) - this.lastX) - this.lastY) - this.lastZ) / ((float) this.timeDifference);
                AccelerometerHelper.listener.didAccelerate(this.x, this.y, this.z);
                if (this.force > AccelerometerHelper.threshold) {
                    if (this.currentTime - this.prevShakeTime >= ((long) AccelerometerHelper.interval)) {
                        AccelerometerHelper.listener.didShake(this.force);
                    }
                    this.prevShakeTime = this.currentTime;
                }
                this.lastX = this.x;
                this.lastY = this.y;
                this.lastZ = this.z;
                this.prevTime = this.currentTime;
            }
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };
    /* access modifiers changed from: private */
    public static int interval = AdManager.AD_FILL_PARENT;
    private static boolean isEnabled = false;
    /* access modifiers changed from: private */
    public static AccelerometerListener listener;
    private static SensorManager sensorManager;
    /* access modifiers changed from: private */
    public static float threshold = 0.2f;

    AccelerometerHelper() {
    }

    static void startListening(AccelerometerListener accelerometerListener) {
        if (MMAdViewOverlayActivity.getContext() != null) {
            sensorManager = (SensorManager) MMAdViewOverlayActivity.getContext().getSystemService("sensor");
            if (!Boolean.valueOf(sensorManager.registerListener(accelerometerEventListener, sensorManager.getDefaultSensor(1), 1)).booleanValue()) {
                Log.w(MMAdViewSDK.SDKLOG, "Accelerometer not supported by this device. Unregistering listener.");
                sensorManager.unregisterListener(accelerometerEventListener, sensorManager.getDefaultSensor(1));
                return;
            }
            isEnabled = true;
            listener = accelerometerListener;
            return;
        }
        Log.w(MMAdViewSDK.SDKLOG, "Null context in accelerometer helper");
    }

    static void stopListening() {
        isEnabled = false;
        try {
            if (sensorManager != null && accelerometerEventListener != null) {
                sensorManager.unregisterListener(accelerometerEventListener, sensorManager.getDefaultSensor(1));
            }
        } catch (Exception e) {
        }
    }

    public static boolean isListening() {
        return isEnabled;
    }
}
