package com.millennialmedia.android;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import cn.domob.android.ads.DomobAdDataItem;
import cn.domob.android.ads.DomobAdManager;
import com.energysource.szj.embeded.SZJFrameworkConfig;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class VideoImage implements Parcelable {
    public static final Parcelable.Creator<VideoImage> CREATOR = new Parcelable.Creator<VideoImage>() {
        public VideoImage createFromParcel(Parcel in) {
            return new VideoImage(in);
        }

        public VideoImage[] newArray(int size) {
            return new VideoImage[size];
        }
    };
    String[] activity;
    int anchor;
    int anchor2;
    long appearanceDelay;
    ImageButton button;
    long fadeDuration = 1000;
    float fromAlpha = 1.0f;
    String imageUrl;
    long inactivityTimeout;
    RelativeLayout.LayoutParams layoutParams;
    String linkUrl;
    String name;
    int paddingBottom = 0;
    int paddingLeft = 0;
    int paddingRight = 0;
    int paddingTop = 0;
    int position;
    int position2;
    float toAlpha = 1.0f;

    VideoImage() {
    }

    VideoImage(JSONObject imageObject) throws JSONException {
        deserializeFromObj(imageObject);
    }

    VideoImage(Parcel in) {
        try {
            this.imageUrl = in.readString();
            this.activity = new String[in.readInt()];
            in.readStringArray(this.activity);
            this.linkUrl = in.readString();
            this.name = in.readString();
            this.paddingTop = in.readInt();
            this.paddingBottom = in.readInt();
            this.paddingLeft = in.readInt();
            this.paddingRight = in.readInt();
            this.position = in.readInt();
            this.anchor = in.readInt();
            this.position2 = in.readInt();
            this.anchor2 = in.readInt();
            this.appearanceDelay = in.readLong();
            this.inactivityTimeout = in.readLong();
            this.fromAlpha = in.readFloat();
            this.toAlpha = in.readFloat();
            this.fadeDuration = in.readLong();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public void deserializeFromObj(JSONObject imageObject) throws JSONException {
        try {
            if (imageObject.has(DomobAdDataItem.IMAGE_TYPE)) {
                this.imageUrl = imageObject.getString(DomobAdDataItem.IMAGE_TYPE);
            }
            if (imageObject.has(SZJFrameworkConfig.ACTIVITY)) {
                JSONArray jsonArray = imageObject.getJSONArray(SZJFrameworkConfig.ACTIVITY);
                this.activity = new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    this.activity[i] = jsonArray.getString(i);
                }
            } else {
                this.activity = new String[0];
            }
            if (imageObject.has(DomobAdManager.ACTION_URL)) {
                this.linkUrl = imageObject.getString(DomobAdManager.ACTION_URL);
            }
            if (imageObject.has("android-layout")) {
                this.position = imageObject.getInt("android-layout");
            }
            if (imageObject.has("android-layoutAnchor")) {
                this.anchor = imageObject.getInt("android-layoutAnchor");
            }
            if (imageObject.has("android-layout2")) {
                this.position2 = imageObject.getInt("android-layout2");
            }
            if (imageObject.has("android-layoutAnchor2")) {
                this.anchor2 = imageObject.getInt("android-layoutAnchor2");
            }
            if (imageObject.has("android-paddingTop")) {
                this.paddingTop = imageObject.getInt("android-paddingTop");
            }
            if (imageObject.has("android-paddingLeft")) {
                this.paddingLeft = imageObject.getInt("android-paddingLeft");
            }
            if (imageObject.has("android-paddingRight")) {
                this.paddingRight = imageObject.getInt("android-paddingRight");
            }
            if (imageObject.has("android-paddingBottom")) {
                this.paddingBottom = imageObject.getInt("android-paddingBottom");
            }
            if (imageObject.has("appearanceDelay")) {
                this.appearanceDelay = ((long) imageObject.getDouble("appearanceDelay")) * 1000;
            }
            if (imageObject.has("inactivityTimeout")) {
                this.inactivityTimeout = ((long) imageObject.getDouble("inactivityTimeout")) * 1000;
            }
            if (imageObject.has("opacity")) {
                JSONObject opacityObject = imageObject.getJSONObject("opacity");
                if (opacityObject.has("start")) {
                    this.fromAlpha = (float) opacityObject.getDouble("start");
                }
                if (opacityObject.has("end")) {
                    this.toAlpha = (float) opacityObject.getDouble("end");
                }
                if (opacityObject.has("fadeDuration")) {
                    this.fadeDuration = ((long) opacityObject.getDouble("fadeDuration")) * 1000;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.imageUrl);
        dest.writeInt(this.activity.length);
        dest.writeStringArray(this.activity);
        dest.writeString(this.linkUrl);
        dest.writeString(this.name);
        dest.writeInt(this.paddingTop);
        dest.writeInt(this.paddingBottom);
        dest.writeInt(this.paddingLeft);
        dest.writeInt(this.paddingRight);
        dest.writeInt(this.position);
        dest.writeInt(this.anchor);
        dest.writeInt(this.position2);
        dest.writeInt(this.anchor2);
        dest.writeLong(this.appearanceDelay);
        dest.writeLong(this.inactivityTimeout);
        dest.writeFloat(this.fromAlpha);
        dest.writeFloat(this.toAlpha);
        dest.writeLong(this.fadeDuration);
    }
}
