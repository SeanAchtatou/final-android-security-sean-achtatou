package com.millennialmedia.android;

import android.os.Parcel;
import android.os.Parcelable;
import cn.domob.android.ads.DomobAdManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class VideoAd extends BasicCachedAd implements Parcelable {
    public static final Parcelable.Creator<VideoAd> CREATOR = new Parcelable.Creator<VideoAd>() {
        public VideoAd createFromParcel(Parcel in) {
            return new VideoAd(in);
        }

        public VideoAd[] newArray(int size) {
            return new VideoAd[size];
        }
    };
    ArrayList<VideoLogEvent> activities = new ArrayList<>();
    ArrayList<VideoImage> buttons = new ArrayList<>();
    long duration;
    String[] endActivity;
    String onCompletionUrl;
    boolean showControls;
    boolean showCountdown;
    String[] startActivity;
    boolean stayInPlayer;
    boolean storedOnSdCard;

    VideoAd() {
    }

    VideoAd(Parcel in) {
        try {
            this.id = in.readString();
            this.contentUrl = in.readString();
            this.startActivity = new String[in.readInt()];
            in.readStringArray(this.startActivity);
            this.endActivity = new String[in.readInt()];
            in.readStringArray(this.endActivity);
            boolean[] yo = new boolean[4];
            in.readBooleanArray(yo);
            this.showControls = yo[0];
            this.stayInPlayer = yo[1];
            this.showCountdown = yo[2];
            this.storedOnSdCard = yo[3];
            try {
                this.expiration = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy").parse(in.readString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            this.onCompletionUrl = in.readString();
            this.duration = in.readLong();
            this.buttons = in.readArrayList(VideoImage.class.getClassLoader());
            this.activities = in.readArrayList(VideoLogEvent.class.getClassLoader());
            this.deferredViewStart = in.readLong();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    VideoAd(String jsonString) {
        if (jsonString != null) {
            JSONObject jsonAdObject = null;
            try {
                jsonAdObject = new JSONObject(jsonString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (jsonAdObject != null) {
                try {
                    JSONObject videoObject = jsonAdObject.getJSONObject(DomobAdManager.ACTION_VIDEO);
                    if (videoObject != null) {
                        deserializeFromObj(videoObject);
                    }
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void deserializeFromObj(JSONObject videoObject) throws JSONException {
        try {
            if (videoObject.has("id")) {
                this.id = videoObject.getString("id");
            }
            if (videoObject.has("content-url")) {
                this.contentUrl = videoObject.getString("content-url");
            }
            if (videoObject.has("startActivity")) {
                JSONArray jsonArray = videoObject.getJSONArray("startActivity");
                this.startActivity = new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    this.startActivity[i] = jsonArray.getString(i);
                }
            } else {
                this.startActivity = new String[0];
            }
            if (videoObject.has("endActivity")) {
                JSONArray jsonArray2 = videoObject.getJSONArray("endActivity");
                this.endActivity = new String[jsonArray2.length()];
                for (int i2 = 0; i2 < jsonArray2.length(); i2++) {
                    this.endActivity[i2] = jsonArray2.getString(i2);
                }
            } else {
                this.endActivity = new String[0];
            }
            if (videoObject.has("showVideoPlayerControls")) {
                this.showControls = videoObject.getBoolean("showVideoPlayerControls");
            }
            if (videoObject.has("showCountdownHUD")) {
                this.showCountdown = videoObject.getBoolean("showCountdownHUD");
            }
            if (videoObject.has("expiration")) {
                try {
                    this.expiration = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ZZZZ").parse(videoObject.getString("expiration"));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (videoObject.has("onCompletion")) {
                JSONObject onCompletionObject = videoObject.getJSONObject("onCompletion");
                if (onCompletionObject.has(DomobAdManager.ACTION_URL)) {
                    this.onCompletionUrl = onCompletionObject.getString(DomobAdManager.ACTION_URL);
                }
                if (onCompletionObject.has("stayInPlayer")) {
                    this.stayInPlayer = onCompletionObject.getBoolean("stayInPlayer");
                }
            }
            if (videoObject.has("duration")) {
                this.duration = ((long) videoObject.getDouble("duration")) * 1000;
            }
            if (videoObject.has("buttons")) {
                JSONArray jsonButtons = videoObject.getJSONArray("buttons");
                for (int i3 = 0; i3 < jsonButtons.length(); i3++) {
                    this.buttons.add(new VideoImage(jsonButtons.getJSONObject(i3)));
                }
            }
            if (videoObject.has("log")) {
                JSONArray jsonLogs = videoObject.getJSONArray("log");
                for (int i4 = 0; i4 < jsonLogs.length(); i4++) {
                    this.activities.add(new VideoLogEvent(jsonLogs.getJSONObject(i4)));
                }
            }
            this.deferredViewStart = System.currentTimeMillis();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.contentUrl);
        dest.writeInt(this.startActivity.length);
        dest.writeStringArray(this.startActivity);
        dest.writeInt(this.endActivity.length);
        dest.writeStringArray(this.endActivity);
        dest.writeBooleanArray(new boolean[]{this.showControls, this.stayInPlayer, this.showCountdown, this.storedOnSdCard});
        if (this.expiration != null) {
            dest.writeString(this.expiration.toString());
        }
        dest.writeString(this.onCompletionUrl);
        dest.writeLong(this.duration);
        dest.writeList(this.buttons);
        dest.writeList(this.activities);
        dest.writeLong(this.deferredViewStart);
    }
}
