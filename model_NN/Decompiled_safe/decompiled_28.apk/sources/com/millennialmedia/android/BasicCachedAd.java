package com.millennialmedia.android;

import java.util.Date;

class BasicCachedAd {
    String contentUrl;
    long deferredViewStart;
    boolean downloaded;
    Date expiration;
    String id;
    int type;
    int views;

    BasicCachedAd() {
    }

    BasicCachedAd(String id2) {
        this.id = id2;
    }

    /* access modifiers changed from: package-private */
    public void parseJSON(String JSONString) {
    }
}
