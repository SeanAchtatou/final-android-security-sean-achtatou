package com.madhouse.android.ads;

import I.I;
import java.util.ListResourceBundle;

public class Resource_zh_CN extends ListResourceBundle {
    private Object[][] _ = {new Object[]{I.I(1), "关闭"}, new Object[]{I.I(7), "前一页"}, new Object[]{I.I(16), "后一页"}, new Object[]{I.I(21), "刷新"}};

    /* access modifiers changed from: protected */
    public final Object[][] getContents() {
        return this._;
    }
}
