package com.madhouse.android.ads;

import I.I;
import java.util.ListResourceBundle;

public class Resource_en_US extends ListResourceBundle {
    private Object[][] _ = {new Object[]{I.I(1), I.I(29)}, new Object[]{I.I(7), I.I(35)}, new Object[]{I.I(16), I.I(44)}, new Object[]{I.I(21), I.I(49)}};

    /* access modifiers changed from: protected */
    public final Object[][] getContents() {
        return this._;
    }
}
