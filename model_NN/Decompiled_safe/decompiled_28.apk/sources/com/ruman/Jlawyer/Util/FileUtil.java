package com.ruman.Jlawyer.Util;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.ruman.Jlawyer.Log.Log;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileUtil {
    public static void createFolderIfNotExist(String folder) {
        File modelsFolder = new File(folder);
        if (!modelsFolder.exists()) {
            modelsFolder.mkdir();
        }
    }

    public static BitmapDrawable createFromStream(InputStream stream) {
        return new BitmapDrawable(stream);
    }

    public static Bitmap createBitmapFromStream(InputStream stream) {
        return createFromStream(stream).getBitmap();
    }

    public static void writeFile(String fileName, InputStream stream) throws IOException {
        IOException e;
        File file = new File(fileName);
        BufferedWriter writer = null;
        FileOutputStream output = null;
        try {
            createFolderIfNotExist(fileName.substring(0, fileName.lastIndexOf("/")));
            file.deleteOnExit();
            file.createNewFile();
            Log.WriteLine("start write");
            FileOutputStream output2 = new FileOutputStream(fileName);
            try {
                byte[] buffer = new byte[2048];
                while (true) {
                    int result = stream.read(buffer);
                    if (result == -1) {
                        break;
                    }
                    output2.write(buffer, 0, result);
                }
                output2.close();
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e2) {
                    }
                }
                if (output2 != null) {
                    try {
                        output2.close();
                    } catch (IOException e3) {
                    }
                }
            } catch (IOException e4) {
                e = e4;
                output = output2;
            } catch (Throwable th) {
                th = th;
                output = output2;
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e5) {
                    }
                }
                if (output != null) {
                    try {
                        output.close();
                    } catch (IOException e6) {
                    }
                }
                throw th;
            }
        } catch (IOException e7) {
            e = e7;
            try {
                throw e;
            } catch (Throwable th2) {
                th = th2;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x002a A[SYNTHETIC, Splitter:B:15:0x002a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void writeFile(java.lang.String r6, java.lang.String r7) throws java.io.IOException {
        /*
            java.io.File r1 = new java.io.File
            r1.<init>(r6)
            r2 = 0
            java.lang.String r4 = "start write"
            com.ruman.Jlawyer.Log.Log.WriteLine(r4)     // Catch:{ IOException -> 0x0021 }
            java.io.BufferedWriter r3 = new java.io.BufferedWriter     // Catch:{ IOException -> 0x0021 }
            java.io.FileWriter r4 = new java.io.FileWriter     // Catch:{ IOException -> 0x0021 }
            r4.<init>(r1)     // Catch:{ IOException -> 0x0021 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x0021 }
            r3.write(r7)     // Catch:{ IOException -> 0x0035, all -> 0x0032 }
            r3.close()     // Catch:{ IOException -> 0x0035, all -> 0x0032 }
            if (r3 == 0) goto L_0x0020
            r3.close()     // Catch:{ IOException -> 0x0030 }
        L_0x0020:
            return
        L_0x0021:
            r4 = move-exception
            r0 = r4
        L_0x0023:
            r0.printStackTrace()     // Catch:{ all -> 0x0027 }
            throw r0     // Catch:{ all -> 0x0027 }
        L_0x0027:
            r4 = move-exception
        L_0x0028:
            if (r2 == 0) goto L_0x002d
            r2.close()     // Catch:{ IOException -> 0x002e }
        L_0x002d:
            throw r4
        L_0x002e:
            r5 = move-exception
            goto L_0x002d
        L_0x0030:
            r4 = move-exception
            goto L_0x0020
        L_0x0032:
            r4 = move-exception
            r2 = r3
            goto L_0x0028
        L_0x0035:
            r4 = move-exception
            r0 = r4
            r2 = r3
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ruman.Jlawyer.Util.FileUtil.writeFile(java.lang.String, java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0061 A[SYNTHETIC, Splitter:B:31:0x0061] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0066 A[SYNTHETIC, Splitter:B:34:0x0066] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x006f A[SYNTHETIC, Splitter:B:39:0x006f] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0074 A[SYNTHETIC, Splitter:B:42:0x0074] */
    /* JADX WARNING: Removed duplicated region for block: B:58:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void copyFromResource(int r9, java.lang.String r10, java.lang.Boolean r11) {
        /*
            r6 = 0
            r4 = 0
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x005a }
            r2.<init>(r10)     // Catch:{ Exception -> 0x005a }
            boolean r7 = r2.exists()     // Catch:{ Exception -> 0x005a }
            if (r7 == 0) goto L_0x001e
            boolean r7 = r11.booleanValue()     // Catch:{ Exception -> 0x005a }
            if (r7 != 0) goto L_0x001e
            if (r4 == 0) goto L_0x0018
            r4.close()     // Catch:{ IOException -> 0x0078 }
        L_0x0018:
            if (r6 == 0) goto L_0x001d
            r6.close()     // Catch:{ IOException -> 0x007a }
        L_0x001d:
            return
        L_0x001e:
            r2.deleteOnExit()     // Catch:{ Exception -> 0x005a }
            r2.createNewFile()     // Catch:{ Exception -> 0x005a }
            boolean r7 = r2.canWrite()     // Catch:{ Exception -> 0x005a }
            if (r7 == 0) goto L_0x004d
            com.ruman.Jlawyer.TabView r7 = com.ruman.Jlawyer.Config.Config.Entry     // Catch:{ Exception -> 0x005a }
            android.content.res.Resources r7 = r7.getResources()     // Catch:{ Exception -> 0x005a }
            java.io.InputStream r6 = r7.openRawResource(r9)     // Catch:{ Exception -> 0x005a }
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x005a }
            r5.<init>(r10)     // Catch:{ Exception -> 0x005a }
            int r7 = r6.available()     // Catch:{ Exception -> 0x0087, all -> 0x0084 }
            byte[] r0 = new byte[r7]     // Catch:{ Exception -> 0x0087, all -> 0x0084 }
            int r1 = r6.read(r0)     // Catch:{ Exception -> 0x0087, all -> 0x0084 }
            r5.write(r0)     // Catch:{ Exception -> 0x0087, all -> 0x0084 }
            r5.close()     // Catch:{ Exception -> 0x0087, all -> 0x0084 }
            r6.close()     // Catch:{ Exception -> 0x0087, all -> 0x0084 }
            r4 = r5
        L_0x004d:
            if (r4 == 0) goto L_0x0052
            r4.close()     // Catch:{ IOException -> 0x0082 }
        L_0x0052:
            if (r6 == 0) goto L_0x001d
            r6.close()     // Catch:{ IOException -> 0x0058 }
            goto L_0x001d
        L_0x0058:
            r7 = move-exception
            goto L_0x001d
        L_0x005a:
            r7 = move-exception
            r3 = r7
        L_0x005c:
            com.ruman.Jlawyer.Log.Log.WriteLine(r3)     // Catch:{ all -> 0x006c }
            if (r4 == 0) goto L_0x0064
            r4.close()     // Catch:{ IOException -> 0x007c }
        L_0x0064:
            if (r6 == 0) goto L_0x001d
            r6.close()     // Catch:{ IOException -> 0x006a }
            goto L_0x001d
        L_0x006a:
            r7 = move-exception
            goto L_0x001d
        L_0x006c:
            r7 = move-exception
        L_0x006d:
            if (r4 == 0) goto L_0x0072
            r4.close()     // Catch:{ IOException -> 0x007e }
        L_0x0072:
            if (r6 == 0) goto L_0x0077
            r6.close()     // Catch:{ IOException -> 0x0080 }
        L_0x0077:
            throw r7
        L_0x0078:
            r7 = move-exception
            goto L_0x0018
        L_0x007a:
            r7 = move-exception
            goto L_0x001d
        L_0x007c:
            r7 = move-exception
            goto L_0x0064
        L_0x007e:
            r8 = move-exception
            goto L_0x0072
        L_0x0080:
            r8 = move-exception
            goto L_0x0077
        L_0x0082:
            r7 = move-exception
            goto L_0x0052
        L_0x0084:
            r7 = move-exception
            r4 = r5
            goto L_0x006d
        L_0x0087:
            r7 = move-exception
            r3 = r7
            r4 = r5
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ruman.Jlawyer.Util.FileUtil.copyFromResource(int, java.lang.String, java.lang.Boolean):void");
    }
}
