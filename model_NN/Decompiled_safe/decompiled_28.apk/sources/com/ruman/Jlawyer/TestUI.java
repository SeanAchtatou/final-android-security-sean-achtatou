package com.ruman.Jlawyer;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class TestUI extends Activity {
    private AdView adView;

    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView((int) R.layout.test_ui);
            View findViewById = findViewById(R.id.test_ui_id);
            this.adView = new AdView(this, AdSize.BANNER, "a14e44f0ba030c7");
            AdRequest adRequest = new AdRequest();
            adRequest.setTesting(true);
            ((LinearLayout) findViewById(R.id.test_ui_layout)).addView(this.adView);
            this.adView.loadAd(adRequest);
        } catch (Exception e) {
        }
    }

    public void onDestroy() {
        this.adView.destroy();
        super.onDestroy();
    }
}
