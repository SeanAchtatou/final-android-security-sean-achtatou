package com.ruman.Jlawyer;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.ruman.Jlawyer.Util.FileUtil;
import java.io.File;
import java.io.IOException;

public class ListProvince extends ListActivity {
    public static String[] mProvinces = {"上海", "北京", "天津", "重庆", "浙江", "广东"};

    public enum Provinces {
        Provinces0(ListProvince.mProvinces[0]),
        Provinces1(ListProvince.mProvinces[1]),
        Provinces2(ListProvince.mProvinces[2]),
        Provinces3(ListProvince.mProvinces[3]),
        Provinces4(ListProvince.mProvinces[4]),
        Provinces5(ListProvince.mProvinces[5]);
        
        private String value;

        private Provinces(String v) {
            this.value = v;
        }

        public String getDisplayString() {
            return this.value;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new ArrayAdapter(this, 17367043, mProvinces));
        getListView().setTextFilterEnabled(true);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (position < 6) {
            String province = "Provinces" + String.valueOf(position);
            TabView.setProvince(Provinces.valueOf(province));
            try {
                FileUtil.writeFile(getCacheDir() + File.separator + "config.dat", province);
            } catch (IOException e) {
            }
            finish();
        }
    }
}
