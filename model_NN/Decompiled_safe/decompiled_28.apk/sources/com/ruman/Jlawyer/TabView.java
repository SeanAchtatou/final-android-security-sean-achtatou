package com.ruman.Jlawyer;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;
import com.adview.AdViewLayout;
import com.ruman.Jlawyer.Config.Config;
import com.ruman.Jlawyer.ListProvince;
import com.ruman.Jlawyer.Log.Log;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import com.wqmobile.sdk.protocol.cmd.WQGlobal;
import java.io.File;
import java.io.FileInputStream;
import org.apache.http.util.EncodingUtils;

public class TabView extends TabActivity implements TabHost.TabContentFactory {
    public static ListProvince.Provinces CurrentProvince = ListProvince.Provinces.Provinces0;
    public static final int ID_About = 2;
    public static final int ID_Last = 2;
    public static final int ID_Province = 1;
    public static String xingshiShenCha = "审查起诉阶段";
    public static String xingshiYiShen = "一审审判阶段";
    public static String xingshiZhenCha = "侦查阶段";
    private AdViewLayout adViewLayout1;
    private AdViewLayout adViewLayout2;
    private AdViewLayout adViewLayout3;
    private double mBiaoDiFeiExtendDown = 0.0d;
    /* access modifiers changed from: private */
    public double mBiaoDiFeiExtendDown2 = 0.0d;
    private double mBiaoDiFeiExtendUp = 0.0d;
    /* access modifiers changed from: private */
    public double mBiaoDiFeiExtendUp2 = 0.0d;
    private double mBiaoDiFeiStandardDown = 0.0d;
    /* access modifiers changed from: private */
    public double mBiaoDiFeiStandardDown2 = 0.0d;
    private double mBiaoDiFeiStandardUp = 0.0d;
    /* access modifiers changed from: private */
    public double mBiaoDiFeiStandardUp2 = 0.0d;
    /* access modifiers changed from: private */
    public double mBiaoDiJinE = 0.0d;
    private double mBiaodijine_CaiChan = 0.0d;
    private double mBiaodijine_LiHun = 0.0d;
    private double mBiaodijine_RenGeQing = 0.0d;
    private TabHost tabHost;
    public String tagMinShi = "民事";
    public String tagShenqingfei = "申请费";
    public String tagShouLiFei = "受理费";
    public String tagWeiyuejin = "违约金";
    public String tagXingShi = "刑事";
    public String tagXingZheng = "行政";
    public String tagYanchilixi = "迟延利息";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Config.Entry = this;
            Config.CurrentContext = this;
            this.tabHost = getTabHost();
            createTabSpec_MinShi();
            createTabSpec_XingShi();
            createTabSpec_XingZheng();
            this.adViewLayout1 = createAdViewLayout();
            this.adViewLayout2 = createAdViewLayout();
            this.adViewLayout3 = createAdViewLayout();
            ((LinearLayout) findViewById(R.id.tab_minshi_layout)).addView(this.adViewLayout1);
            ((LinearLayout) findViewById(R.id.tab_xingshi_layout)).addView(this.adViewLayout2);
            ((LinearLayout) findViewById(R.id.tab_xingzheng_layout)).addView(this.adViewLayout3);
            setContentView(this.tabHost);
            try {
                String configFile = getCacheDir() + File.separator + "config.dat";
                if (new File(configFile).exists()) {
                    FileInputStream fin = new FileInputStream(configFile);
                    byte[] buffer = new byte[fin.available()];
                    fin.read(buffer);
                    String res = EncodingUtils.getString(buffer, XmlConstant.DEFAULT_ENCODING);
                    fin.close();
                    CurrentProvince = ListProvince.Provinces.valueOf(res);
                }
            } catch (Exception e) {
                Log.Error(e);
            }
            setProvince(CurrentProvince);
        } catch (Exception e2) {
            Log.Error(e2);
        }
    }

    public void onDestroy() {
        super.onDestroy();
    }

    private AdViewLayout createAdViewLayout() {
        return new AdViewLayout(this, "SDK20111414540836ltqipn4xa5jawgc");
    }

    private View createTabSpec_MinShi() {
        try {
            View view = LayoutInflater.from(this).inflate((int) R.layout.tab_minshi, (ViewGroup) null);
            this.tabHost.getTabContentView().addView(view);
            final EditText editTextBiaoDiFeiStandardDown = (EditText) findViewById(R.id.tab_minshi_editTextBiaoDiFeiStandardDown);
            final EditText editTextBiaoDiFeiStandardUp = (EditText) findViewById(R.id.tab_minshi_editTextBiaoDiFeiStandardUp);
            final EditText editTextBiaoDiFeiExtendDown = (EditText) findViewById(R.id.tab_minshi_editTextBiaoDiFeiExtendDown);
            final EditText editTextBiaoDiFeiExtendUp = (EditText) findViewById(R.id.tab_minshi_editTextBiaoDiFeiExtendUp);
            TextView textView = (TextView) findViewById(R.id.tab_minshi_textViewBiaoDiJinE);
            final EditText editTextBiaodijine = (EditText) findViewById(R.id.tab_minshi_editTextBiaoDiJinE);
            editTextBiaodijine.addTextChangedListener(new TextWatcher() {
                public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
                }

                public void afterTextChanged(Editable arg0) {
                    if (arg0.toString().length() == 0) {
                        TabView.this.mBiaoDiJinE = 0.0d;
                    } else {
                        TabView.this.mBiaoDiJinE = Double.parseDouble(arg0.toString());
                    }
                    TabView.this.caculate_Mingshi();
                }

                public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                }
            });
            ((CheckBox) findViewById(R.id.tab_minshi_checkBoxCaiChan)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        editTextBiaodijine.setEnabled(true);
                        editTextBiaodijine.setText("0.0");
                        editTextBiaoDiFeiStandardDown.setText("0.0");
                        editTextBiaoDiFeiStandardUp.setText("0.0");
                        editTextBiaoDiFeiExtendDown.setText("0.0");
                        editTextBiaoDiFeiExtendUp.setText("0.0");
                        return;
                    }
                    editTextBiaodijine.setEnabled(false);
                    editTextBiaodijine.setText("0.0");
                    editTextBiaoDiFeiStandardDown.setText(String.valueOf(TabView.this.mBiaoDiFeiStandardDown2));
                    editTextBiaoDiFeiStandardUp.setText(String.valueOf(TabView.this.mBiaoDiFeiStandardUp2));
                    editTextBiaoDiFeiExtendDown.setText(String.valueOf(TabView.this.mBiaoDiFeiExtendDown2));
                    editTextBiaoDiFeiExtendUp.setText(String.valueOf(TabView.this.mBiaoDiFeiExtendUp2));
                }
            });
            TabHost.TabSpec tabSpec = this.tabHost.newTabSpec(this.tagMinShi);
            tabSpec.setIndicator(this.tagMinShi, null);
            tabSpec.setContent((int) R.id.tab_minshi_id);
            this.tabHost.addTab(tabSpec);
            return view;
        } catch (Exception e) {
            Log.Error(e);
            return null;
        }
    }

    private View createTabSpec_XingShi() {
        try {
            this.tabHost.getTabContentView().addView(LayoutInflater.from(this).inflate((int) R.layout.tab_xingshi, (ViewGroup) null));
            TabHost.TabSpec tabSpec = this.tabHost.newTabSpec(this.tagXingShi);
            tabSpec.setIndicator(this.tagXingShi, null);
            tabSpec.setContent((int) R.id.tab_xingshi_id);
            this.tabHost.addTab(tabSpec);
            return findViewById(R.id.tab_xingshi_id);
        } catch (Exception e) {
            Log.Error(e);
            return null;
        }
    }

    private View createTabSpec_XingZheng() {
        try {
            LayoutInflater.from(this).inflate((int) R.layout.tab_xingzheng, this.tabHost.getTabContentView());
            final EditText editTextBiaoDiFeiStandardDown = (EditText) findViewById(R.id.tab_xingzheng_editTextBiaoDiFeiStandardDown);
            final EditText editTextBiaoDiFeiStandardUp = (EditText) findViewById(R.id.tab_xingzheng_editTextBiaoDiFeiStandardUp);
            final EditText editTextBiaoDiFeiExtendDown = (EditText) findViewById(R.id.tab_xingzheng_editTextBiaoDiFeiExtendDown);
            final EditText editTextBiaoDiFeiExtendUp = (EditText) findViewById(R.id.tab_xingzheng_editTextBiaoDiFeiExtendUp);
            TextView textView = (TextView) findViewById(R.id.tab_xingzheng_textViewBiaoDiJinE);
            final EditText editTextBiaodijine = (EditText) findViewById(R.id.tab_xingzheng_editTextBiaoDiJinE);
            editTextBiaodijine.addTextChangedListener(new TextWatcher() {
                public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
                }

                public void afterTextChanged(Editable arg0) {
                    if (arg0.toString().length() == 0) {
                        TabView.this.mBiaoDiJinE = 0.0d;
                    } else {
                        TabView.this.mBiaoDiJinE = Double.parseDouble(arg0.toString());
                    }
                    TabView.this.caculate_XingZheng();
                }

                public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                }
            });
            ((CheckBox) findViewById(R.id.tab_xingzheng_checkBoxCaiChan)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        editTextBiaodijine.setEnabled(true);
                        editTextBiaodijine.setText("0.0");
                        editTextBiaoDiFeiStandardDown.setText("0.0");
                        editTextBiaoDiFeiStandardUp.setText("0.0");
                        editTextBiaoDiFeiExtendDown.setText("0.0");
                        editTextBiaoDiFeiExtendUp.setText("0.0");
                        return;
                    }
                    editTextBiaodijine.setEnabled(false);
                    editTextBiaodijine.setText("0.0");
                    editTextBiaoDiFeiStandardDown.setText("2500");
                    editTextBiaoDiFeiStandardUp.setText("10000");
                    editTextBiaoDiFeiExtendDown.setText("2000");
                    editTextBiaoDiFeiExtendUp.setText("12000");
                }
            });
            TabHost.TabSpec tabSpec = this.tabHost.newTabSpec(this.tagXingZheng);
            tabSpec.setIndicator(this.tagXingZheng, null);
            tabSpec.setContent((int) R.id.tab_xingzheng_id);
            this.tabHost.addTab(tabSpec);
            return findViewById(R.id.tab_xingzheng_id);
        } catch (Exception e) {
            Log.Error(e);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void caculate_Mingshi() {
        EditText editTextBiaoDiFeiStandardDown = (EditText) findViewById(R.id.tab_minshi_editTextBiaoDiFeiStandardDown);
        EditText editTextBiaoDiFeiStandardUp = (EditText) findViewById(R.id.tab_minshi_editTextBiaoDiFeiStandardUp);
        EditText editTextBiaoDiFeiExtendDown = (EditText) findViewById(R.id.tab_minshi_editTextBiaoDiFeiExtendDown);
        EditText editTextBiaoDiFeiExtendUp = (EditText) findViewById(R.id.tab_minshi_editTextBiaoDiFeiExtendUp);
        this.mBiaoDiFeiStandardDown = 0.0d;
        this.mBiaoDiFeiStandardUp = 0.0d;
        this.mBiaoDiFeiExtendDown = 0.0d;
        this.mBiaoDiFeiExtendUp = 0.0d;
        if (CurrentProvince == ListProvince.Provinces.Provinces0) {
            caculate_Mingshi_Shanghai();
        } else if (CurrentProvince == ListProvince.Provinces.Provinces1) {
            caculate_Mingshi_Beijing();
        } else if (CurrentProvince == ListProvince.Provinces.Provinces2) {
            caculate_Mingshi_Tianjin();
        } else if (CurrentProvince == ListProvince.Provinces.Provinces3) {
            caculate_Mingshi_Chongqin();
        } else if (CurrentProvince == ListProvince.Provinces.Provinces4) {
            caculate_Mingshi_Zhejiang();
        } else if (CurrentProvince == ListProvince.Provinces.Provinces5) {
            caculate_Mingshi_Guangdong();
        }
        this.mBiaoDiFeiExtendDown2 = this.mBiaoDiFeiStandardDown2 * 0.8d;
        this.mBiaoDiFeiExtendUp2 = this.mBiaoDiFeiStandardUp2 * 1.2d;
        this.mBiaoDiFeiExtendDown = this.mBiaoDiFeiStandardDown * 0.8d;
        this.mBiaoDiFeiExtendUp = this.mBiaoDiFeiStandardUp * 1.2d;
        this.mBiaoDiFeiStandardDown += 1000.0d;
        this.mBiaoDiFeiStandardUp += 1000.0d;
        this.mBiaoDiFeiExtendDown += 1000.0d;
        this.mBiaoDiFeiExtendUp += 1000.0d;
        editTextBiaoDiFeiStandardDown.setText(String.valueOf(this.mBiaoDiFeiStandardDown));
        editTextBiaoDiFeiStandardUp.setText(String.valueOf(this.mBiaoDiFeiStandardUp));
        editTextBiaoDiFeiExtendDown.setText(String.valueOf(this.mBiaoDiFeiExtendDown));
        editTextBiaoDiFeiExtendUp.setText(String.valueOf(this.mBiaoDiFeiExtendUp));
    }

    private void caculate_Mingshi_Zhejiang() {
        double d;
        double d2;
        this.mBiaoDiFeiStandardDown2 = 2500.0d;
        this.mBiaoDiFeiStandardUp2 = 10000.0d;
        if (this.mBiaoDiJinE <= 100000.0d) {
            this.mBiaoDiFeiStandardDown = this.mBiaoDiJinE * 0.06d;
            this.mBiaoDiFeiStandardUp = this.mBiaoDiJinE * 0.08d;
            if (this.mBiaoDiFeiStandardDown < 2500.0d) {
                d = 2500.0d;
            } else {
                d = this.mBiaoDiFeiStandardDown;
            }
            this.mBiaoDiFeiStandardDown = d;
            if (this.mBiaoDiFeiStandardUp < 2500.0d) {
                d2 = 2500.0d;
            } else {
                d2 = this.mBiaoDiFeiStandardUp;
            }
            this.mBiaoDiFeiStandardUp = d2;
            return;
        }
        if (1.0E7d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 1.0E7d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.01d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.02d;
            this.mBiaoDiJinE = 1.0E7d;
        }
        if (5000000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 5000000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.02d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.03d;
            this.mBiaoDiJinE = 5000000.0d;
        }
        if (1000000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 1000000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.03d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.04d;
            this.mBiaoDiJinE = 1000000.0d;
        }
        if (500000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 500000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.04d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.05d;
            this.mBiaoDiJinE = 500000.0d;
        }
        if (100000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 100000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.05d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.06d;
            this.mBiaoDiJinE = 100000.0d;
        }
        if (100000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 100000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.06d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.08d;
        }
    }

    private void caculate_Mingshi_Shanghai() {
        double d;
        double d2;
        this.mBiaoDiFeiStandardDown2 = 3000.0d;
        this.mBiaoDiFeiStandardUp2 = 12000.0d;
        if (this.mBiaoDiJinE <= 100000.0d) {
            this.mBiaoDiFeiStandardDown = this.mBiaoDiJinE * 0.08d;
            this.mBiaoDiFeiStandardUp = this.mBiaoDiJinE * 0.12d;
            if (this.mBiaoDiFeiStandardDown < ((double) WQGlobal.WQConstant.RETRY_TIMEOUT)) {
                d = (double) WQGlobal.WQConstant.RETRY_TIMEOUT;
            } else {
                d = this.mBiaoDiFeiStandardDown;
            }
            this.mBiaoDiFeiStandardDown = d;
            if (this.mBiaoDiFeiStandardUp < ((double) WQGlobal.WQConstant.RETRY_TIMEOUT)) {
                d2 = (double) WQGlobal.WQConstant.RETRY_TIMEOUT;
            } else {
                d2 = this.mBiaoDiFeiStandardUp;
            }
            this.mBiaoDiFeiStandardUp = d2;
            return;
        }
        if (1.0E8d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 1.0E8d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.005d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.01d;
            this.mBiaoDiJinE = 1.0E8d;
        }
        if (1.0E7d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 1.0E7d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.01d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.03d;
            this.mBiaoDiJinE = 1.0E7d;
        }
        if (1000000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 1000000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.03d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.05d;
            this.mBiaoDiJinE = 1000000.0d;
        }
        if (100000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 100000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.05d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.07d;
            this.mBiaoDiJinE = 100000.0d;
        }
        if (100000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 100000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.08d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.12d;
        }
    }

    private void caculate_Mingshi_Beijing() {
        double d;
        double d2;
        this.mBiaoDiFeiStandardDown2 = 2000.0d;
        this.mBiaoDiFeiStandardUp2 = 10000.0d;
        if (this.mBiaoDiJinE <= 100000.0d) {
            this.mBiaoDiFeiStandardDown = this.mBiaoDiJinE * 0.05d;
            this.mBiaoDiFeiStandardUp = this.mBiaoDiJinE * 0.05d;
            if (this.mBiaoDiFeiStandardDown < ((double) 2000)) {
                d = (double) 2000;
            } else {
                d = this.mBiaoDiFeiStandardDown;
            }
            this.mBiaoDiFeiStandardDown = d;
            if (this.mBiaoDiFeiStandardUp < ((double) 2000)) {
                d2 = (double) 2000;
            } else {
                d2 = this.mBiaoDiFeiStandardUp;
            }
            this.mBiaoDiFeiStandardUp = d2;
            return;
        }
        if (1.0E7d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 1.0E7d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.075d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.075d;
            this.mBiaoDiJinE = 1.0E7d;
        }
        if (5000000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 5000000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.01d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.01d;
            this.mBiaoDiJinE = 5000000.0d;
        }
        if (1000000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 1000000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.02d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.02d;
            this.mBiaoDiJinE = 1000000.0d;
        }
        if (500000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 500000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.025d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.025d;
            this.mBiaoDiJinE = 500000.0d;
        }
        if (100000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 100000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.03d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.03d;
            this.mBiaoDiJinE = 100000.0d;
        }
        if (100000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 100000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.05d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.05d;
        }
    }

    private void caculate_Mingshi_Tianjin() {
        this.mBiaoDiFeiStandardDown2 = 0.0d;
        this.mBiaoDiFeiStandardUp2 = 5000.0d;
        if (this.mBiaoDiJinE <= 10000.0d) {
            this.mBiaoDiFeiStandardDown = 0.0d;
            this.mBiaoDiFeiStandardUp = 2000.0d;
            return;
        }
        if (1.0E7d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 1.0E7d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.01d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.01d;
            this.mBiaoDiJinE = 1.0E7d;
        }
        if (5000000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 5000000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.02d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.02d;
            this.mBiaoDiJinE = 5000000.0d;
        }
        if (1000000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 1000000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.03d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.03d;
            this.mBiaoDiJinE = 1000000.0d;
        }
        if (500000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 500000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.04d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.04d;
            this.mBiaoDiJinE = 500000.0d;
        }
        if (100000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 100000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.05d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.05d;
            this.mBiaoDiJinE = 100000.0d;
        }
        if (100000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 100000.0d;
            this.mBiaoDiFeiStandardDown += 0.0d;
            this.mBiaoDiFeiStandardUp += 2000.0d;
        }
    }

    private void caculate_Mingshi_Chongqin() {
        this.mBiaoDiFeiStandardDown2 = 2000.0d;
        this.mBiaoDiFeiStandardUp2 = 6000.0d;
        if (this.mBiaoDiJinE <= 100000.0d) {
            this.mBiaoDiFeiStandardDown = 2000.0d;
            this.mBiaoDiFeiStandardUp = 6000.0d;
            return;
        }
        if (1.0E7d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 1.0E7d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.01d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.02d;
            this.mBiaoDiJinE = 1.0E7d;
        }
        if (5000000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 5000000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.02d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.03d;
            this.mBiaoDiJinE = 5000000.0d;
        }
        if (1000000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 1000000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.03d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.04d;
            this.mBiaoDiJinE = 1000000.0d;
        }
        if (500000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 500000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.04d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.05d;
            this.mBiaoDiJinE = 500000.0d;
        }
        if (100000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 100000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.05d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.06d;
            this.mBiaoDiJinE = 100000.0d;
        }
        this.mBiaoDiFeiStandardDown += 2000.0d;
        this.mBiaoDiFeiStandardUp += 6000.0d;
    }

    private void caculate_Mingshi_Guangdong() {
        this.mBiaoDiFeiStandardDown2 = 3000.0d;
        this.mBiaoDiFeiStandardUp2 = 20000.0d;
        if (this.mBiaoDiJinE <= 50000.0d) {
            this.mBiaoDiFeiStandardDown = 1000.0d;
            this.mBiaoDiFeiStandardUp = 8000.0d;
            return;
        }
        if (5.0E7d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 5.0E7d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.005d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.005d;
            this.mBiaoDiJinE = 5.0E7d;
        }
        if (1.0E7d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 1.0E7d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.01d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.01d;
            this.mBiaoDiJinE = 1.0E7d;
        }
        if (5000000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 5000000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.02d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.02d;
            this.mBiaoDiJinE = 5000000.0d;
        }
        if (1000000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 1000000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.03d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.03d;
            this.mBiaoDiJinE = 1000000.0d;
        }
        if (500000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 500000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.04d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.04d;
            this.mBiaoDiJinE = 500000.0d;
        }
        if (100000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 100000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.05d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.05d;
            this.mBiaoDiJinE = 100000.0d;
        }
        if (50000.0d < this.mBiaoDiJinE) {
            this.mBiaoDiJinE -= 50000.0d;
            this.mBiaoDiFeiStandardDown += this.mBiaoDiJinE * 0.06d;
            this.mBiaoDiFeiStandardUp += this.mBiaoDiJinE * 0.06d;
            this.mBiaoDiJinE = 50000.0d;
        }
        this.mBiaoDiFeiStandardDown += 1000.0d;
        this.mBiaoDiFeiStandardUp += 8000.0d;
    }

    /* access modifiers changed from: private */
    public void caculate_XingZheng() {
        caculate_Mingshi();
    }

    private void caculate_XingShi() {
        EditText editText1 = (EditText) findViewById(R.id.tab_xingshi_editTextZhenCha);
        EditText editText2 = (EditText) findViewById(R.id.tab_xingshi_editTextShenCha);
        EditText editText3 = (EditText) findViewById(R.id.tab_xingshi_editTextYiShen);
        ((TextView) findViewById(R.id.tab_xingshi_textViewZhenCha)).setText(xingshiZhenCha);
        ((TextView) findViewById(R.id.tab_xingshi_textViewShenCha)).setText(xingshiShenCha);
        ((TextView) findViewById(R.id.tab_xingshi_textViewYiShen)).setText(xingshiYiShen);
        String xingshiZhenChaValue = "1500-8000";
        String xingshiShenChaValue = "1500-10000";
        String xingshiYiShenValue = "2500-25000";
        if (CurrentProvince == ListProvince.Provinces.Provinces0) {
            xingshiZhenChaValue = "1500-10000";
            xingshiShenChaValue = "2000-10000";
            xingshiYiShenValue = "3000-30000";
        } else if (CurrentProvince == ListProvince.Provinces.Provinces1) {
            xingshiZhenChaValue = "5000-20000";
            xingshiShenChaValue = "6000-30000";
            xingshiYiShenValue = "8000-50000";
        } else if (CurrentProvince == ListProvince.Provinces.Provinces2) {
            xingshiZhenChaValue = "0-3000";
            xingshiShenChaValue = "0-6000";
            xingshiYiShenValue = "0-6000";
        } else if (CurrentProvince == ListProvince.Provinces.Provinces3) {
            xingshiZhenChaValue = "2000-8000";
            xingshiShenChaValue = "3000-10000";
            xingshiYiShenValue = "5000-30000";
        } else if (CurrentProvince == ListProvince.Provinces.Provinces4) {
            xingshiZhenChaValue = "1500-8000";
            xingshiShenChaValue = "1500-10000";
            xingshiYiShenValue = "2500-25000";
        } else if (CurrentProvince == ListProvince.Provinces.Provinces5) {
            xingshiZhenChaValue = "2000-5000";
            xingshiShenChaValue = "5000-15000";
            xingshiYiShenValue = "6000-30000";
        }
        editText1.setText(xingshiZhenChaValue);
        editText2.setText(xingshiShenChaValue);
        editText3.setText(xingshiYiShenValue);
    }

    public View createTabContent(String tag) {
        return new View(this);
    }

    public static void setProvince(ListProvince.Provinces province) {
        CurrentProvince = province;
        Config.Entry.caculate_Mingshi();
        Config.Entry.caculate_XingZheng();
        Config.Entry.caculate_XingShi();
        Config.Entry.setTitle(String.valueOf(Config.Entry.getString(R.string.app_name)) + " - " + CurrentProvince.getDisplayString());
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, "省份");
        menu.add(0, 2, 0, "关于");
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            switch (item.getItemId()) {
                case 1:
                    startActivity(new Intent(this, ListProvince.class));
                    return true;
                case 2:
                    Log.Messagebox("Ruman Ltc.\r\n\r\n开发团队: \r\nliu422429@hotmail.com\r\n\r\n版本: 1.0.0.0 (Beta)", "关于");
                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        } catch (Exception e) {
            Log.Error(e);
            return true;
        }
    }
}
