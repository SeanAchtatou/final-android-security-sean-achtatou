package com.ruman.Jlawyer.Log;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.ruman.Jlawyer.Config.Config;
import com.ruman.Jlawyer.R;

public class Log {
    public static final String LogFilename = "";

    private enum LogType {
        Comment,
        Warn,
        Error
    }

    private static String GetLogFileName() {
        return "sdcard/JLawyer/Log/" + System.currentTimeMillis() + ".log";
    }

    public static void WriteLine(String message) {
        android.util.Log.i(Config.Entry.getResources().getString(R.string.app_name), message);
    }

    public static void WriteLine(Exception ex) {
        WriteLine(ex.toString());
    }

    private static void Messagebox(String caption, String message, Exception ex, LogType type) {
        String displayMsg = "";
        if (message != null && message.length() > 0) {
            WriteLine(message);
            displayMsg = message;
        }
        if (ex != null) {
            WriteLine(ex.toString());
            if (displayMsg.length() > 0) {
                displayMsg = String.valueOf(displayMsg) + "\r\n";
            }
            displayMsg = String.valueOf(String.valueOf(displayMsg) + ex.toString()) + "\r\n";
        }
        if (Config.CurrentContext == null) {
            WriteLine("Error: Config.CurrentContext is null!");
        } else {
            new AlertDialog.Builder(Config.CurrentContext).setTitle(caption).setMessage(displayMsg).setPositiveButton((int) R.string.alert_dialog_ok, (DialogInterface.OnClickListener) null).create().show();
        }
    }

    public static void Messagebox(String message, String caption) {
        Messagebox(caption, message, null, LogType.Comment);
    }

    public static void Messagebox(String message) {
        Messagebox("Message", message, null, LogType.Comment);
    }

    public static void Warn(String message, String caption) {
        Messagebox(caption, message, null, LogType.Warn);
    }

    public static void Warn(String message) {
        Warn(message, "Warn");
    }

    public static void Error(String message) {
        Messagebox("Error", message, null, LogType.Error);
    }

    public static void Error(Exception ex) {
        Messagebox("Error", null, ex, LogType.Error);
    }

    public static void Error(String message, Exception ex) {
        Messagebox("Error", message, ex, LogType.Error);
    }
}
