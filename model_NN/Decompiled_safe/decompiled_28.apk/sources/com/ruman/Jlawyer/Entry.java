package com.ruman.Jlawyer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class Entry extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(this, TabView.class));
    }
}
