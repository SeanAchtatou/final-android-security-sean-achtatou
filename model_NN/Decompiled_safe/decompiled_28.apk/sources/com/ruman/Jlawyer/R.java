package com.ruman.Jlawyer;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int ajax_loader = 2130837504;
        public static final int ajax_loader_large = 2130837505;
        public static final int calculator = 2130837506;
        public static final int calculator2 = 2130837507;
        public static final int calculator22 = 2130837508;
        public static final int icon = 2130837509;
        public static final int iconentry = 2130837510;
    }

    public static final class id {
        public static final int tab_minshi_checkBoxCaiChan = 2131034114;
        public static final int tab_minshi_editTextBiaoDiFeiExtendDown = 2131034124;
        public static final int tab_minshi_editTextBiaoDiFeiExtendUp = 2131034126;
        public static final int tab_minshi_editTextBiaoDiFeiStandardDown = 2131034120;
        public static final int tab_minshi_editTextBiaoDiFeiStandardUp = 2131034122;
        public static final int tab_minshi_editTextBiaoDiJinE = 2131034116;
        public static final int tab_minshi_editTextShouLiFei = 2131034118;
        public static final int tab_minshi_id = 2131034112;
        public static final int tab_minshi_layout = 2131034113;
        public static final int tab_minshi_textView3 = 2131034117;
        public static final int tab_minshi_textView4 = 2131034119;
        public static final int tab_minshi_textView5 = 2131034121;
        public static final int tab_minshi_textView6 = 2131034123;
        public static final int tab_minshi_textView7 = 2131034125;
        public static final int tab_minshi_textViewBiaoDiJinE = 2131034115;
        public static final int tab_shoulifei_editTextBiaoDiJinE = 2131034130;
        public static final int tab_shoulifei_editTextCaiChan = 2131034132;
        public static final int tab_shoulifei_editTextLiHun = 2131034134;
        public static final int tab_shoulifei_editTextRenGeQuan = 2131034136;
        public static final int tab_shoulifei_id = 2131034127;
        public static final int tab_shoulifei_layout = 2131034128;
        public static final int tab_shoulifei_spinnerDaXiaoXie = 2131034139;
        public static final int tab_shoulifei_spinnerJiaoNaFangShi = 2131034138;
        public static final int tab_shoulifei_textView2 = 2131034137;
        public static final int tab_shoulifei_textView3 = 2131034131;
        public static final int tab_shoulifei_textView4 = 2131034133;
        public static final int tab_shoulifei_textView5 = 2131034135;
        public static final int tab_shoulifei_textViewBiaoDiJinE = 2131034129;
        public static final int tab_xingshi_editTextShenCha = 2131034145;
        public static final int tab_xingshi_editTextYiShen = 2131034147;
        public static final int tab_xingshi_editTextZhenCha = 2131034143;
        public static final int tab_xingshi_id = 2131034140;
        public static final int tab_xingshi_layout = 2131034141;
        public static final int tab_xingshi_textViewShenCha = 2131034144;
        public static final int tab_xingshi_textViewYiShen = 2131034146;
        public static final int tab_xingshi_textViewZhenCha = 2131034142;
        public static final int tab_xingzheng_checkBoxCaiChan = 2131034150;
        public static final int tab_xingzheng_editTextBiaoDiFeiExtendDown = 2131034160;
        public static final int tab_xingzheng_editTextBiaoDiFeiExtendUp = 2131034162;
        public static final int tab_xingzheng_editTextBiaoDiFeiStandardDown = 2131034156;
        public static final int tab_xingzheng_editTextBiaoDiFeiStandardUp = 2131034158;
        public static final int tab_xingzheng_editTextBiaoDiJinE = 2131034152;
        public static final int tab_xingzheng_editTextShouLiFei = 2131034154;
        public static final int tab_xingzheng_id = 2131034148;
        public static final int tab_xingzheng_layout = 2131034149;
        public static final int tab_xingzheng_textView3 = 2131034153;
        public static final int tab_xingzheng_textView4 = 2131034155;
        public static final int tab_xingzheng_textView5 = 2131034157;
        public static final int tab_xingzheng_textView6 = 2131034159;
        public static final int tab_xingzheng_textView7 = 2131034161;
        public static final int tab_xingzheng_textViewBiaoDiJinE = 2131034151;
        public static final int test_ui_id = 2131034163;
        public static final int test_ui_layout = 2131034164;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int tab_minshi = 2130903041;
        public static final int tab_shoulifei = 2130903042;
        public static final int tab_xingshi = 2130903043;
        public static final int tab_xingzheng = 2130903044;
        public static final int test_ui = 2130903045;
    }

    public static final class string {
        public static final int alert_dialog_cancel = 2130968580;
        public static final int alert_dialog_ok = 2130968579;
        public static final int app_desc = 2130968578;
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
    }
}
