package com.ruman.Jlawyer.Config;

public class Version {
    public static int MajorVersion = 0;
    public static int MinorVersion = 0;
    public static int ReleaseVersion = 0;
    public static int RevVersion = 0;

    public Version(int release, int major, int minor, int rev) {
        ReleaseVersion = release;
        MajorVersion = major;
        MinorVersion = minor;
        RevVersion = rev;
    }

    public String toString() {
        return String.valueOf(ReleaseVersion) + "." + MajorVersion + "." + MinorVersion + "." + RevVersion;
    }
}
