package com.ruman.Jlawyer.Config;

import android.content.Context;
import com.ruman.Jlawyer.TabView;

public class Config {
    public static final String CacheFolder = "sdcard/JLawyer/cache";
    public static Context CurrentContext = null;
    public static final String DataFolder = "sdcard/JLawyer";
    public static TabView Entry = null;
    public static final String LocalFolder = "sdcard/JLawyer/cache/local";
    public static final String LogFolder = "sdcard/JLawyer/Log";
    public static final String SDCard = "sdcard";
    public static final boolean UseLog = true;
    public static final Version Version = new Version(1, 0, 0, 0);
}
