package com.wooboo.adlib_android;

import android.content.Context;
import android.util.Log;
import java.util.ArrayList;

final class b {
    /* access modifiers changed from: private */
    public static int a = 5000;
    /* access modifiers changed from: private */
    public Context b;
    private String c = null;
    /* access modifiers changed from: private */
    public String d = null;
    private String e = null;
    /* access modifiers changed from: private */
    public byte f;
    /* access modifiers changed from: private */
    public a g;

    interface a {
        void a();

        void b();
    }

    public static b a(Context context, byte[] bArr) {
        b bVar = new b();
        bVar.b = context;
        ArrayList<Object> a2 = h.a(bArr);
        if (a2 != null) {
            bVar.c = (String) a2.get(0);
            bVar.c = bVar.c;
            bVar.e = (String) a2.get(1);
            bVar.d = (String) a2.get(2);
            bVar.f = ((Byte) a2.get(3)).byteValue();
            if (bVar.c.length() == 0 && bVar.e.length() == 0) {
                Log.w("Wooboo SDK 1.2", "Could not get ad from Wooboo servers (" + (System.currentTimeMillis() - c.b) + " ms);");
                return null;
            }
            Log.d("Wooboo SDK 1.2", "Get an ad from Wooboo servers (" + (System.currentTimeMillis() - c.b) + " ms);");
            return bVar;
        }
        Log.w("Wooboo SDK 1.2", "Could not get ad from Wooboo servers (" + (System.currentTimeMillis() - c.b) + " ms);");
        return null;
    }

    private b() {
    }

    public final void a() {
        if (this.d != null) {
            new Thread() {
                /* JADX WARNING: Removed duplicated region for block: B:13:0x0051 A[SYNTHETIC, Splitter:B:13:0x0051] */
                /* JADX WARNING: Removed duplicated region for block: B:155:? A[RETURN, SYNTHETIC] */
                /* JADX WARNING: Removed duplicated region for block: B:16:0x0057  */
                /* JADX WARNING: Removed duplicated region for block: B:24:0x0077  */
                /* JADX WARNING: Removed duplicated region for block: B:39:0x00f6 A[SYNTHETIC, Splitter:B:39:0x00f6] */
                /* JADX WARNING: Removed duplicated region for block: B:71:0x015d A[SYNTHETIC, Splitter:B:71:0x015d] */
                /* JADX WARNING: Removed duplicated region for block: B:74:0x0162 A[Catch:{ Exception -> 0x0166 }] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final void run() {
                    /*
                        r13 = this;
                        r10 = 0
                        r9 = 1
                        r7 = 0
                        java.lang.String r11 = "|"
                        java.lang.String r8 = "Wooboo SDK 1.2"
                        com.wooboo.adlib_android.b r0 = com.wooboo.adlib_android.b.this
                        byte r1 = r0.f
                        com.wooboo.adlib_android.b r0 = com.wooboo.adlib_android.b.this     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        com.wooboo.adlib_android.b$a r0 = r0.g     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        if (r0 == 0) goto L_0x001e
                        com.wooboo.adlib_android.b r0 = com.wooboo.adlib_android.b.this     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        com.wooboo.adlib_android.b$a r0 = r0.g     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        r0.a()     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                    L_0x001e:
                        java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        com.wooboo.adlib_android.b r2 = com.wooboo.adlib_android.b.this     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        java.lang.String r2 = r2.d     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        r0.<init>(r2)     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        int r2 = com.wooboo.adlib_android.c.a     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        if (r2 != r9) goto L_0x0081
                        java.net.URLConnection r0 = r0.openConnection()     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        r2 = 1
                        java.net.HttpURLConnection.setFollowRedirects(r2)     // Catch:{ MalformedURLException -> 0x00af, IOException -> 0x0320 }
                        int r2 = com.wooboo.adlib_android.b.a     // Catch:{ MalformedURLException -> 0x00af, IOException -> 0x0320 }
                        r0.setConnectTimeout(r2)     // Catch:{ MalformedURLException -> 0x00af, IOException -> 0x0320 }
                        int r2 = com.wooboo.adlib_android.b.a     // Catch:{ MalformedURLException -> 0x00af, IOException -> 0x0320 }
                        r0.setReadTimeout(r2)     // Catch:{ MalformedURLException -> 0x00af, IOException -> 0x0320 }
                    L_0x0045:
                        r0.connect()     // Catch:{ MalformedURLException -> 0x00af, IOException -> 0x0320 }
                        r0.getResponseCode()     // Catch:{ MalformedURLException -> 0x00af, IOException -> 0x0320 }
                        java.net.URL r2 = r0.getURL()     // Catch:{ MalformedURLException -> 0x00af, IOException -> 0x0320 }
                    L_0x004f:
                        if (r1 != r9) goto L_0x00f6
                        java.lang.String r0 = r2.toString()     // Catch:{ NullPointerException -> 0x00eb }
                    L_0x0055:
                        if (r0 == 0) goto L_0x006f
                        android.content.Intent r2 = new android.content.Intent
                        r2.<init>()
                        switch(r1) {
                            case 1: goto L_0x0194;
                            case 2: goto L_0x0194;
                            case 3: goto L_0x01ac;
                            case 4: goto L_0x01ac;
                            case 5: goto L_0x01ac;
                            case 6: goto L_0x01ac;
                            case 7: goto L_0x01ac;
                            case 8: goto L_0x01c8;
                            case 9: goto L_0x01f1;
                            case 10: goto L_0x024f;
                            case 11: goto L_0x01ac;
                            case 12: goto L_0x0268;
                            case 13: goto L_0x02ac;
                            default: goto L_0x005f;
                        }
                    L_0x005f:
                        r1 = r0
                        r0 = r2
                    L_0x0061:
                        r2 = 268435456(0x10000000, float:2.5243549E-29)
                        r0.addFlags(r2)
                        com.wooboo.adlib_android.b r2 = com.wooboo.adlib_android.b.this     // Catch:{ Exception -> 0x02f4 }
                        android.content.Context r2 = r2.b     // Catch:{ Exception -> 0x02f4 }
                        r2.startActivity(r0)     // Catch:{ Exception -> 0x02f4 }
                    L_0x006f:
                        com.wooboo.adlib_android.b r0 = com.wooboo.adlib_android.b.this
                        com.wooboo.adlib_android.b$a r0 = r0.g
                        if (r0 == 0) goto L_0x0080
                        com.wooboo.adlib_android.b r0 = com.wooboo.adlib_android.b.this
                        com.wooboo.adlib_android.b$a r0 = r0.g
                        r0.b()
                    L_0x0080:
                        return
                    L_0x0081:
                        int r2 = com.wooboo.adlib_android.c.a     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        r3 = 2
                        if (r2 != r3) goto L_0x0335
                        java.net.Proxy r2 = new java.net.Proxy     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        java.net.Proxy$Type r3 = java.net.Proxy.Type.HTTP     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        java.net.InetSocketAddress r4 = new java.net.InetSocketAddress     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        java.lang.String r5 = "10.0.0.172"
                        r6 = 80
                        r4.<init>(r5, r6)     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        r2.<init>(r3, r4)     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        java.net.URLConnection r0 = r0.openConnection(r2)     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0323, IOException -> 0x00cc }
                        r2 = 1
                        java.net.HttpURLConnection.setFollowRedirects(r2)     // Catch:{ MalformedURLException -> 0x00af, IOException -> 0x0320 }
                        int r2 = com.wooboo.adlib_android.b.a     // Catch:{ MalformedURLException -> 0x00af, IOException -> 0x0320 }
                        r0.setConnectTimeout(r2)     // Catch:{ MalformedURLException -> 0x00af, IOException -> 0x0320 }
                        int r2 = com.wooboo.adlib_android.b.a     // Catch:{ MalformedURLException -> 0x00af, IOException -> 0x0320 }
                        r0.setReadTimeout(r2)     // Catch:{ MalformedURLException -> 0x00af, IOException -> 0x0320 }
                        goto L_0x0045
                    L_0x00af:
                        r2 = move-exception
                    L_0x00b0:
                        java.lang.String r2 = "Wooboo SDK 1.2"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder
                        java.lang.String r3 = "Malformed click URL.  Will try to follow anyway."
                        r2.<init>(r3)
                        com.wooboo.adlib_android.b r3 = com.wooboo.adlib_android.b.this
                        java.lang.String r3 = r3.d
                        java.lang.StringBuilder r2 = r2.append(r3)
                        java.lang.String r2 = r2.toString()
                        android.util.Log.w(r8, r2)
                        r2 = r7
                        goto L_0x004f
                    L_0x00cc:
                        r0 = move-exception
                        r0 = r7
                    L_0x00ce:
                        java.lang.String r2 = "Wooboo SDK 1.2"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder
                        java.lang.String r3 = "Could not determine final click destination URL.  Will try to follow anyway.  "
                        r2.<init>(r3)
                        com.wooboo.adlib_android.b r3 = com.wooboo.adlib_android.b.this
                        java.lang.String r3 = r3.d
                        java.lang.StringBuilder r2 = r2.append(r3)
                        java.lang.String r2 = r2.toString()
                        android.util.Log.w(r8, r2)
                        r2 = r7
                        goto L_0x004f
                    L_0x00eb:
                        r0 = move-exception
                        java.lang.String r0 = "Wooboo SDK 1.2"
                        java.lang.String r0 = "Could not get ad click url from Telead server."
                        android.util.Log.e(r8, r0)
                        r0 = r7
                        goto L_0x0055
                    L_0x00f6:
                        java.io.InputStream r2 = r0.getInputStream()     // Catch:{ IOException -> 0x011d, all -> 0x0159 }
                        if (r2 == 0) goto L_0x0332
                        int r3 = r2.available()     // Catch:{ IOException -> 0x031d }
                        byte[] r4 = new byte[r3]     // Catch:{ IOException -> 0x031d }
                        r5 = r10
                    L_0x0103:
                        if (r5 < r3) goto L_0x0117
                        java.lang.String r3 = new java.lang.String     // Catch:{ IOException -> 0x031d }
                        r3.<init>(r4)     // Catch:{ IOException -> 0x031d }
                    L_0x010a:
                        if (r2 == 0) goto L_0x010f
                        r2.close()     // Catch:{ Exception -> 0x017c }
                    L_0x010f:
                        if (r0 == 0) goto L_0x0191
                        r0.disconnect()     // Catch:{ Exception -> 0x017c }
                        r0 = r3
                        goto L_0x0055
                    L_0x0117:
                        r2.read(r4)     // Catch:{ IOException -> 0x031d }
                        int r5 = r5 + 1
                        goto L_0x0103
                    L_0x011d:
                        r2 = move-exception
                        r2 = r7
                    L_0x011f:
                        java.lang.String r3 = "Wooboo SDK 1.2"
                        java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x031a }
                        java.lang.String r5 = "Connection off "
                        r4.<init>(r5)     // Catch:{ all -> 0x031a }
                        r5 = 0
                        java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x031a }
                        java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x031a }
                        android.util.Log.e(r3, r4)     // Catch:{ all -> 0x031a }
                        if (r2 == 0) goto L_0x0139
                        r2.close()     // Catch:{ Exception -> 0x0141 }
                    L_0x0139:
                        if (r0 == 0) goto L_0x032f
                        r0.disconnect()     // Catch:{ Exception -> 0x0141 }
                        r0 = r7
                        goto L_0x0055
                    L_0x0141:
                        r0 = move-exception
                        java.lang.String r0 = "Wooboo SDK 1.2"
                        java.lang.StringBuilder r0 = new java.lang.StringBuilder
                        java.lang.String r2 = "Could not close stream"
                        r0.<init>(r2)
                        java.lang.StringBuilder r0 = r0.append(r7)
                        java.lang.String r0 = r0.toString()
                        android.util.Log.e(r8, r0)
                        r0 = r7
                        goto L_0x0055
                    L_0x0159:
                        r1 = move-exception
                        r2 = r7
                    L_0x015b:
                        if (r2 == 0) goto L_0x0160
                        r2.close()     // Catch:{ Exception -> 0x0166 }
                    L_0x0160:
                        if (r0 == 0) goto L_0x0165
                        r0.disconnect()     // Catch:{ Exception -> 0x0166 }
                    L_0x0165:
                        throw r1
                    L_0x0166:
                        r0 = move-exception
                        java.lang.String r0 = "Wooboo SDK 1.2"
                        java.lang.StringBuilder r0 = new java.lang.StringBuilder
                        java.lang.String r2 = "Could not close stream"
                        r0.<init>(r2)
                        java.lang.StringBuilder r0 = r0.append(r7)
                        java.lang.String r0 = r0.toString()
                        android.util.Log.e(r8, r0)
                        goto L_0x0165
                    L_0x017c:
                        r0 = move-exception
                        java.lang.String r0 = "Wooboo SDK 1.2"
                        java.lang.StringBuilder r0 = new java.lang.StringBuilder
                        java.lang.String r2 = "Could not close stream"
                        r0.<init>(r2)
                        java.lang.StringBuilder r0 = r0.append(r3)
                        java.lang.String r0 = r0.toString()
                        android.util.Log.e(r8, r0)
                    L_0x0191:
                        r0 = r3
                        goto L_0x0055
                    L_0x0194:
                        java.lang.String r1 = "android.intent.action.VIEW"
                        r2.setAction(r1)     // Catch:{ NullPointerException -> 0x01a4 }
                        android.net.Uri r1 = android.net.Uri.parse(r0)     // Catch:{ NullPointerException -> 0x01a4 }
                        r2.setData(r1)     // Catch:{ NullPointerException -> 0x01a4 }
                        r1 = r0
                        r0 = r2
                        goto L_0x0061
                    L_0x01a4:
                        r1 = move-exception
                        r1.printStackTrace()
                        r1 = r0
                        r0 = r2
                        goto L_0x0061
                    L_0x01ac:
                        java.lang.String r1 = r0.trim()     // Catch:{ NullPointerException -> 0x01c0 }
                        java.lang.String r3 = "android.intent.action.VIEW"
                        r2.setAction(r3)     // Catch:{ NullPointerException -> 0x01c0 }
                        android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ NullPointerException -> 0x01c0 }
                        r2.setData(r1)     // Catch:{ NullPointerException -> 0x01c0 }
                        r1 = r0
                        r0 = r2
                        goto L_0x0061
                    L_0x01c0:
                        r1 = move-exception
                        r1.printStackTrace()
                        r1 = r0
                        r0 = r2
                        goto L_0x0061
                    L_0x01c8:
                        java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01e7 }
                        java.lang.String r3 = "tel:"
                        r1.<init>(r3)     // Catch:{ Exception -> 0x01e7 }
                        java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ Exception -> 0x01e7 }
                        java.lang.String r0 = r1.toString()     // Catch:{ Exception -> 0x01e7 }
                        java.lang.String r1 = "android.intent.action.DIAL"
                        r2.setAction(r1)     // Catch:{ Exception -> 0x0314 }
                        android.net.Uri r1 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x0314 }
                        r2.setData(r1)     // Catch:{ Exception -> 0x0314 }
                        r1 = r0
                        r0 = r2
                        goto L_0x0061
                    L_0x01e7:
                        r1 = move-exception
                        r12 = r1
                        r1 = r0
                        r0 = r12
                    L_0x01eb:
                        r0.printStackTrace()
                        r0 = r2
                        goto L_0x0061
                    L_0x01f1:
                        java.lang.String r1 = "|"
                        boolean r1 = r0.contains(r11)
                        if (r1 == 0) goto L_0x0242
                        java.lang.String r1 = "|"
                        int r1 = r0.indexOf(r11)
                        java.lang.String r3 = r0.substring(r10, r1)
                        int r1 = r1 + 1
                        java.lang.String r1 = r0.substring(r1)
                        r12 = r3
                        r3 = r1
                        r1 = r12
                    L_0x020c:
                        java.lang.String r4 = "android.intent.action.VIEW"
                        r2.setAction(r4)     // Catch:{ Exception -> 0x0247 }
                        java.lang.String r4 = "com.google.android.apps.maps"
                        java.lang.String r5 = "com.google.android.maps.MapsActivity"
                        r2.setClassName(r4, r5)     // Catch:{ Exception -> 0x0247 }
                        java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0247 }
                        java.lang.String r5 = "http://maps.google.com/maps?q="
                        r4.<init>(r5)     // Catch:{ Exception -> 0x0247 }
                        java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ Exception -> 0x0247 }
                        java.lang.String r4 = "("
                        java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0247 }
                        java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0247 }
                        java.lang.String r3 = ")&z=22"
                        java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0247 }
                        java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0247 }
                        android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ Exception -> 0x0247 }
                        r2.setData(r1)     // Catch:{ Exception -> 0x0247 }
                        r1 = r0
                        r0 = r2
                        goto L_0x0061
                    L_0x0242:
                        java.lang.String r1 = "I am here"
                        r3 = r1
                        r1 = r0
                        goto L_0x020c
                    L_0x0247:
                        r1 = move-exception
                        r1.printStackTrace()
                        r1 = r0
                        r0 = r2
                        goto L_0x0061
                    L_0x024f:
                        android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x0260 }
                        java.lang.String r3 = "android.intent.action.WEB_SEARCH"
                        r1.<init>(r3)     // Catch:{ Exception -> 0x0260 }
                        java.lang.String r2 = "query"
                        r1.putExtra(r2, r0)     // Catch:{ Exception -> 0x030e }
                        r12 = r1
                        r1 = r0
                        r0 = r12
                        goto L_0x0061
                    L_0x0260:
                        r1 = move-exception
                    L_0x0261:
                        r1.printStackTrace()
                        r1 = r0
                        r0 = r2
                        goto L_0x0061
                    L_0x0268:
                        java.lang.String r1 = "|"
                        boolean r1 = r0.contains(r11)
                        if (r1 == 0) goto L_0x032b
                        java.lang.String r1 = "|"
                        int r1 = r0.indexOf(r11)
                        java.lang.String r3 = r0.substring(r10, r1)
                        int r1 = r1 + 1
                        java.lang.String r1 = r0.substring(r1)
                    L_0x0280:
                        java.lang.String r4 = "android.intent.action.SENDTO"
                        r2.setAction(r4)     // Catch:{ Exception -> 0x02a4 }
                        java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02a4 }
                        java.lang.String r5 = "smsto:"
                        r4.<init>(r5)     // Catch:{ Exception -> 0x02a4 }
                        java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x02a4 }
                        java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x02a4 }
                        android.net.Uri r3 = android.net.Uri.parse(r3)     // Catch:{ Exception -> 0x02a4 }
                        r2.setData(r3)     // Catch:{ Exception -> 0x02a4 }
                        java.lang.String r3 = "sms_body"
                        r2.putExtra(r3, r1)     // Catch:{ Exception -> 0x02a4 }
                        r1 = r0
                        r0 = r2
                        goto L_0x0061
                    L_0x02a4:
                        r1 = move-exception
                        r1.printStackTrace()
                        r1 = r0
                        r0 = r2
                        goto L_0x0061
                    L_0x02ac:
                        java.lang.String r1 = "android.intent.action.SEND"
                        r2.setAction(r1)     // Catch:{ Exception -> 0x02ee }
                        java.lang.String r1 = "|"
                        boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x02ee }
                        if (r1 == 0) goto L_0x0327
                        java.lang.String[] r1 = com.wooboo.adlib_android.b.a(r0)     // Catch:{ Exception -> 0x02ee }
                        r3 = 0
                        r3 = r1[r3]     // Catch:{ Exception -> 0x02ee }
                        r4 = 1
                        r4 = r1[r4]     // Catch:{ Exception -> 0x02ee }
                        r5 = 2
                        r1 = r1[r5]     // Catch:{ Exception -> 0x02ee }
                        r12 = r4
                        r4 = r1
                        r1 = r12
                    L_0x02c9:
                        android.net.Uri r5 = android.net.Uri.parse(r3)     // Catch:{ Exception -> 0x02ee }
                        r2.setData(r5)     // Catch:{ Exception -> 0x02ee }
                        r5 = 1
                        java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ Exception -> 0x02ee }
                        r6 = 0
                        r5[r6] = r3     // Catch:{ Exception -> 0x02ee }
                        java.lang.String r3 = "android.intent.extra.EMAIL"
                        r2.putExtra(r3, r5)     // Catch:{ Exception -> 0x02ee }
                        java.lang.String r3 = "android.intent.extra.TEXT"
                        r2.putExtra(r3, r4)     // Catch:{ Exception -> 0x02ee }
                        java.lang.String r3 = "android.intent.extra.SUBJECT"
                        r2.putExtra(r3, r1)     // Catch:{ Exception -> 0x02ee }
                        java.lang.String r1 = "message/rfc882"
                        r2.setType(r1)     // Catch:{ Exception -> 0x02ee }
                        r1 = r0
                        r0 = r2
                        goto L_0x0061
                    L_0x02ee:
                        r1 = move-exception
                        r1.printStackTrace()
                        goto L_0x005f
                    L_0x02f4:
                        r0 = move-exception
                        r0.printStackTrace()
                        java.lang.String r2 = "Wooboo SDK 1.2"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder
                        java.lang.String r3 = "Could not intent to "
                        r2.<init>(r3)
                        java.lang.StringBuilder r1 = r2.append(r1)
                        java.lang.String r1 = r1.toString()
                        android.util.Log.e(r8, r1, r0)
                        goto L_0x006f
                    L_0x030e:
                        r2 = move-exception
                        r12 = r2
                        r2 = r1
                        r1 = r12
                        goto L_0x0261
                    L_0x0314:
                        r1 = move-exception
                        r12 = r1
                        r1 = r0
                        r0 = r12
                        goto L_0x01eb
                    L_0x031a:
                        r1 = move-exception
                        goto L_0x015b
                    L_0x031d:
                        r3 = move-exception
                        goto L_0x011f
                    L_0x0320:
                        r2 = move-exception
                        goto L_0x00ce
                    L_0x0323:
                        r0 = move-exception
                        r0 = r7
                        goto L_0x00b0
                    L_0x0327:
                        r1 = r7
                        r3 = r7
                        r4 = r7
                        goto L_0x02c9
                    L_0x032b:
                        r1 = r7
                        r3 = r7
                        goto L_0x0280
                    L_0x032f:
                        r0 = r7
                        goto L_0x0055
                    L_0x0332:
                        r3 = r7
                        goto L_0x010a
                    L_0x0335:
                        r0 = r7
                        goto L_0x0045
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.wooboo.adlib_android.b.AnonymousClass1.run():void");
                }
            }.start();
            ImpressionAdView.close();
        }
    }

    static /* synthetic */ String[] a(String str) {
        int indexOf = str.indexOf("|");
        String substring = str.substring(indexOf + 1);
        int indexOf2 = substring.indexOf("|");
        return new String[]{str.substring(0, indexOf), substring.substring(0, indexOf2), substring.substring(indexOf2 + 1)};
    }

    public final void a(a aVar) {
        this.g = aVar;
    }

    public final String b() {
        return this.c;
    }

    public final String c() {
        return this.e;
    }

    public final String toString() {
        return this.c;
    }

    public final boolean equals(Object obj) {
        if (obj instanceof b) {
            return toString().equals(((b) obj).toString());
        }
        return false;
    }

    public final int hashCode() {
        return toString().hashCode();
    }
}
