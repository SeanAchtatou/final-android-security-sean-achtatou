package com.wooboo.adlib_android;

public interface AdListener {
    void onFailedToReceiveAd(Object obj);

    void onReceiveAd(Object obj);
}
