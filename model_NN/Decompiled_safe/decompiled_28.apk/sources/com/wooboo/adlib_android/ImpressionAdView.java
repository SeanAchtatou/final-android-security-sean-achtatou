package com.wooboo.adlib_android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import com.energysource.szj.embeded.AdManager;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public final class ImpressionAdView extends PopupWindow {
    /* access modifiers changed from: private */
    public static ImpressionAdView a;
    /* access modifiers changed from: private */
    public static Handler b = null;
    /* access modifiers changed from: private */
    public static Context c;
    /* access modifiers changed from: private */
    public static int d = 0;
    /* access modifiers changed from: private */
    public static int e = 0;
    private static int f;
    /* access modifiers changed from: private */
    public static View g;
    private static int h;
    private static Timer i = null;
    /* access modifiers changed from: private */
    public static ImageButton j;
    /* access modifiers changed from: private */
    public static RelativeLayout k;
    /* access modifiers changed from: private */
    public static Bitmap l = null;
    private static double m = 0.0d;
    private static int n;
    private static int o;
    /* access modifiers changed from: private */
    public static AdListener p;
    private static int q;
    private static int r = 0;
    private static int s = 0;

    protected static int getAdWidth() {
        return o;
    }

    public final void setAdListener(AdListener adListener) {
        synchronized (this) {
            p = adListener;
        }
    }

    protected static void setAdWidth(int i2) {
        o = i2;
    }

    protected static int getDisplayPit() {
        return q;
    }

    protected static void setDisplayPit(int i2) {
        q = i2;
    }

    protected static int getDisplayMetricsPit(Context context) {
        if (context.getResources().getDisplayMetrics().widthPixels <= 728) {
            q = 1;
            return 1;
        }
        q = 7;
        return 7;
    }

    protected static int getAdHeight() {
        return n;
    }

    protected static void setAdHeight(int i2) {
        n = i2;
    }

    protected static double getDen() {
        return m;
    }

    protected static void setDen(double d2) {
        m = d2;
    }

    protected static int getBtnWidth() {
        return r;
    }

    protected static void setBtnWidth(int i2) {
        r = i2;
    }

    protected static int getBtnHeight() {
        return s;
    }

    protected static void setBtnHeight(int i2) {
        s = i2;
    }

    private ImpressionAdView(Context context) {
        super(context);
    }

    public ImpressionAdView(Context context, View view, int i2, int i3, int i4, boolean z) {
        super(context);
        if (view == null) {
            c.c("The parent view that you add is null,please check whether the parent view is initialized or is a real view.");
        }
        new DisplayMetrics();
        setDen((double) context.getResources().getDisplayMetrics().density);
        ImpressionAdView impressionAdView = new ImpressionAdView(context);
        a = impressionAdView;
        impressionAdView.setAnimationStyle(16973827);
        c = context;
        e = i2;
        d = i3;
        c.c(context);
        c.a(z);
        a(i4);
        g = view;
        c.d(c.b(context));
        b(context);
    }

    public ImpressionAdView(Context context, View view, String str, int i2, int i3, int i4, boolean z) {
        super(context);
        if (view == null) {
            c.c("The parent view that you add is null,please check whether the parent view is initialized or is a real view.");
        }
        if (str != null) {
            c.d(str);
        }
        new DisplayMetrics();
        setDen((double) context.getResources().getDisplayMetrics().density);
        ImpressionAdView impressionAdView = new ImpressionAdView(context);
        a = impressionAdView;
        impressionAdView.setAnimationStyle(16973827);
        c = context;
        e = i2;
        d = i3;
        c.c(context);
        c.a(z);
        a(i4);
        g = view;
        b(context);
    }

    public final void show(int i2) {
        int i3;
        if (g.getVisibility() == 0) {
            k();
        }
        if (i2 <= 0) {
            i3 = 0;
        } else if (i2 < 60) {
            Log.w("Wooboo SDK 1.2", "Fresh ads Interval(" + i2 + ") seconds must be >= " + 60);
            i3 = 60;
        } else if (i2 > 600) {
            Log.w("Wooboo SDK 1.2", "Fresh ads Interval(" + i2 + ") seconds must be <= " + 600);
            i3 = 600;
        } else {
            i3 = i2;
        }
        int i4 = i3 * AdManager.AD_FILL_PARENT;
        h = i4;
        if (i4 == 0) {
            a(false);
        } else {
            a(true);
        }
    }

    public final void show() {
        if (g.getVisibility() == 0) {
            k();
        }
    }

    private static void b(Context context) {
        c.c(c.a(context));
        try {
            if (l == null) {
                l = BitmapFactory.decodeStream(c.getAssets().open("wooboo_btn.png"));
            }
        } catch (IOException e2) {
        }
        o = context.getResources().getDisplayMetrics().widthPixels;
        int displayMetricsPit = getDisplayMetricsPit(context);
        c.b(displayMetricsPit);
        if (displayMetricsPit == 7) {
            n = 72;
        } else if (displayMetricsPit == 1) {
            n = (int) (48.0d * m);
        }
        r = (int) (m * ((double) l.getWidth()));
        s = (int) (m * ((double) l.getHeight()));
        c.e(c.e(context));
        c.b(c.d(context));
        c.a(context.getPackageName());
        d.a(context);
        c.d(d.a(Build.MODEL));
        c.a(c.g(context));
    }

    private static void a(boolean z) {
        synchronized (a) {
            if (z) {
                if (h > 0) {
                    if (i == null) {
                        Timer timer = new Timer();
                        i = timer;
                        timer.schedule(new TimerTask() {
                            public final void run() {
                                if (ImpressionAdView.g.isShown()) {
                                    ImpressionAdView.k();
                                }
                            }
                        }, (long) h, (long) h);
                    }
                }
            }
            if ((!z || h == 0) && i != null) {
                i.cancel();
                i = null;
            }
        }
    }

    /* access modifiers changed from: private */
    public static void k() {
        if (b == null) {
            b = new Handler();
        }
        new Thread() {
            public final void run() {
                try {
                    b f = c.f(ImpressionAdView.c);
                    if (f != null) {
                        synchronized (this) {
                            final a aVar = new a(f, ImpressionAdView.c, true, ImpressionAdView.getAdHeight(), ImpressionAdView.getDen());
                            aVar.a(ImpressionAdView.getTextColor());
                            aVar.setVisibility(0);
                            ImpressionAdView.b.post(new Runnable(this) {
                                public final void run() {
                                    ImpressionAdView.close();
                                    aVar.d();
                                    aVar.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
                                    ImpressionAdView.k = new RelativeLayout(ImpressionAdView.c);
                                    ImpressionAdView.k.addView(aVar);
                                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ImpressionAdView.getBtnWidth(), ImpressionAdView.getBtnHeight());
                                    ImpressionAdView.j = new ImageButton(ImpressionAdView.c);
                                    layoutParams.addRule(11);
                                    ImpressionAdView.j.setId(2);
                                    ImpressionAdView.j.setLayoutParams(layoutParams);
                                    try {
                                        ImpressionAdView.j.setBackgroundDrawable(new BitmapDrawable(ImpressionAdView.l));
                                    } catch (Exception e) {
                                    }
                                    ImpressionAdView.j.setOnClickListener(new View.OnClickListener(this) {
                                        public final void onClick(View view) {
                                            ImpressionAdView.close();
                                        }
                                    });
                                    ImpressionAdView.k.addView(ImpressionAdView.j, layoutParams);
                                    try {
                                        ImpressionAdView.a.setContentView(ImpressionAdView.k);
                                        ImpressionAdView.a.showAtLocation(ImpressionAdView.g, 0, ImpressionAdView.e, ImpressionAdView.d);
                                        ImpressionAdView.a.update(ImpressionAdView.e, ImpressionAdView.d, ImpressionAdView.getAdWidth(), ImpressionAdView.getAdHeight());
                                        if (ImpressionAdView.p != null) {
                                            try {
                                                ImpressionAdView.p.onReceiveAd(ImpressionAdView.a);
                                            } catch (Exception e2) {
                                                Log.w("Wooboo SDK 1.2", e2.toString());
                                            }
                                        }
                                    } catch (Exception e3) {
                                        Log.e("Wooboo SDK 1.2", "Can not display an impressionAdView,please check params.");
                                    }
                                }
                            });
                        }
                    } else if (ImpressionAdView.p != null) {
                        try {
                            ImpressionAdView.p.onFailedToReceiveAd(ImpressionAdView.a);
                        } catch (Exception e) {
                            Log.w("Wooboo SDK 1.2", e.toString());
                        }
                    }
                } catch (Exception e2) {
                    Log.e("Wooboo SDK 1.2", "Unhandled exception requesting a fresh ad.", e2);
                }
            }
        }.start();
    }

    public final void onWindowFocusChanged(boolean z) {
        a(z);
    }

    private static void a(int i2) {
        f = -16777216 | i2;
    }

    protected static int getTextColor() {
        return f;
    }

    public static void close() {
        if (a != null && a.isShowing()) {
            try {
                a.dismiss();
                if (k != null) {
                    k.removeAllViews();
                    k = null;
                }
            } catch (Exception e2) {
                a = null;
            }
        }
    }
}
