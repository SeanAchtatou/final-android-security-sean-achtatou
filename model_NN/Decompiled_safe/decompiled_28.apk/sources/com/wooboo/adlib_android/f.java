package com.wooboo.adlib_android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;
import java.io.InputStream;

public final class f extends View implements Runnable {
    private Bitmap a;
    private g b = new g();
    private int c;
    private Thread d = null;
    private Paint e = new Paint();
    private volatile boolean f = true;

    public f(Context context, InputStream inputStream) {
        super(context);
        this.b.a(inputStream);
        this.c = this.b.a();
        this.a = this.b.a(0);
    }

    public final void a() {
        b();
        if (this.a != null && this.c > 0) {
            this.d = new Thread(this);
            this.f = true;
            this.d.start();
        }
    }

    public final void b() {
        if (this.d != null) {
            this.f = false;
            this.d.interrupt();
            this.d = null;
        }
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        if (this.a != null) {
            canvas.drawBitmap(this.a, 0.0f, 0.0f, this.e);
            this.a = this.b.b();
        }
    }

    public final void run() {
        while (this.f) {
            try {
                postInvalidate();
                Thread.sleep(400);
            } catch (Exception e2) {
            }
        }
    }
}
