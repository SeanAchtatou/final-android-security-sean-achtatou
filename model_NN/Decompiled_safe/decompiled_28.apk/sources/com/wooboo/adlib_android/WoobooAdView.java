package com.wooboo.adlib_android;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import com.energysource.szj.embeded.AdManager;
import java.util.Timer;
import java.util.TimerTask;

public final class WoobooAdView extends RelativeLayout {
    private static int b;
    /* access modifiers changed from: private */
    public static Handler i = new Handler();
    /* access modifiers changed from: private */
    public static int j;
    private static double k;
    private static int l;
    /* access modifiers changed from: private */
    public a a;
    private Timer c;
    private int d;
    private int e;
    private boolean f;
    /* access modifiers changed from: private */
    public AdListener g;
    private boolean h;
    protected boolean requestingFreshAd;

    public static int getDisplayPit() {
        return l;
    }

    public static void setDisplayPit(int i2) {
        l = i2;
    }

    protected static int getDisplayMetricsPit(Context context) {
        if (context.getResources().getDisplayMetrics().widthPixels <= 728) {
            l = 1;
            return 1;
        }
        l = 7;
        return 7;
    }

    protected static double getDensity() {
        return k;
    }

    protected static void setDensity(double d2) {
        k = d2;
    }

    protected static int getAdHeight() {
        return j;
    }

    protected static void setAdHeight(int i2) {
        j = i2;
    }

    public WoobooAdView(Context context, String str, int i2, int i3, boolean z, int i4) {
        super(context, null, 0);
        this.h = false;
        setFocusable(true);
        setDescendantFocusability(262144);
        setClickable(true);
        if (z) {
            c.a(z);
        }
        setTextColor(i3);
        setBackgroundColor(i2);
        setRequestInterval(i4);
        setGoneWithoutAd(true);
        c.d(str);
        a(context);
    }

    private void a(Context context) {
        double d2 = (double) getResources().getDisplayMetrics().density;
        k = d2;
        int displayMetricsPit = getDisplayMetricsPit(context);
        c.b(displayMetricsPit);
        if (displayMetricsPit == 7) {
            j = 72;
        } else if (displayMetricsPit == 1) {
            j = (int) (d2 * 48.0d);
        }
        c.c(context);
        c.e(c.e(context));
        String d3 = c.d(context);
        c.a(c.g(context));
        c.b(d3);
        c.a(context.getPackageName());
        d.a(context);
        c.d(d.a(Build.MODEL));
        c.c(c.a(context));
        if (super.getVisibility() == 0) {
            requestFreshAd();
        }
    }

    public WoobooAdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public WoobooAdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        int i3;
        int i4;
        this.h = false;
        setFocusable(true);
        setDescendantFocusability(262144);
        setClickable(true);
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            boolean attributeBooleanValue = attributeSet.getAttributeBooleanValue(str, "testing", false);
            if (attributeBooleanValue) {
                c.a(attributeBooleanValue);
            }
            i4 = attributeSet.getAttributeUnsignedIntValue(str, "textColor", -1);
            int attributeUnsignedIntValue = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", 0);
            setRequestInterval(attributeSet.getAttributeIntValue(str, "refreshInterval", 60));
            i3 = attributeUnsignedIntValue;
        } else {
            i3 = 0;
            i4 = -1;
        }
        setGoneWithoutAd(true);
        setTextColor(i4);
        setBackgroundColor(i3);
        c.d(c.b(context));
        a(context);
    }

    /* access modifiers changed from: protected */
    public final void requestFreshAd() {
        if (super.getVisibility() != 0) {
            Log.w("Wooboo SDK 1.2", "You have set ads view invisible.  You must call ads view.setVisibility(View.VISIBLE).");
            return;
        }
        this.requestingFreshAd = true;
        new Thread() {
            public final void run() {
                try {
                    Context context = WoobooAdView.this.getContext();
                    b f = c.f(context);
                    if (f != null) {
                        synchronized (this) {
                            if (WoobooAdView.this.a == null || !f.equals(WoobooAdView.this.a.f())) {
                                final boolean z = WoobooAdView.this.a == null;
                                final int b = WoobooAdView.super.getVisibility();
                                final a aVar = new a(f, context, false, WoobooAdView.j, WoobooAdView.getDensity());
                                aVar.setBackgroundColor(WoobooAdView.this.getBackgroundColor());
                                aVar.a(WoobooAdView.this.getTextColor());
                                aVar.setVisibility(b);
                                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, WoobooAdView.j);
                                layoutParams.addRule(14);
                                aVar.setLayoutParams(layoutParams);
                                if (WoobooAdView.this.g != null) {
                                    try {
                                        WoobooAdView.this.g.onReceiveAd(WoobooAdView.this);
                                    } catch (Exception e) {
                                        Log.w("Wooboo SDK 1.2", e.toString());
                                    }
                                }
                                WoobooAdView.i.post(new Runnable() {
                                    public final void run() {
                                        try {
                                            WoobooAdView.this.addView(aVar);
                                            if (b == 0) {
                                                aVar.d();
                                                if (z) {
                                                    WoobooAdView.b(WoobooAdView.this, aVar);
                                                } else {
                                                    WoobooAdView.c(WoobooAdView.this, aVar);
                                                }
                                            } else {
                                                WoobooAdView.this.a = aVar;
                                            }
                                        } catch (Exception e) {
                                            Log.e("Wooboo SDK 1.2", e.toString());
                                        } finally {
                                            WoobooAdView.this.requestingFreshAd = false;
                                        }
                                    }
                                });
                            } else {
                                WoobooAdView.this.requestingFreshAd = false;
                            }
                        }
                        return;
                    }
                    if (WoobooAdView.this.g != null) {
                        try {
                            WoobooAdView.this.g.onFailedToReceiveAd(WoobooAdView.this);
                        } catch (Exception e2) {
                            Log.w("Wooboo SDK 1.2", e2.toString());
                        }
                    }
                    WoobooAdView.this.requestingFreshAd = false;
                    return;
                } catch (Exception e3) {
                    WoobooAdView.this.requestingFreshAd = false;
                }
            }
        }.start();
    }

    /* access modifiers changed from: protected */
    public final int getRequestInterval() {
        return b;
    }

    /* access modifiers changed from: protected */
    public final void setRequestInterval(int i2) {
        int i3;
        if (i2 <= 0) {
            i3 = 0;
        } else if (i2 < 60) {
            Log.w("Wooboo SDK 1.2", "Fresh ads Interval(" + i2 + ") seconds must be >= " + 60);
            i3 = 60;
        } else if (i2 > 600) {
            Log.w("Wooboo SDK 1.2", "Fresh ads Interval(" + i2 + ") seconds must be <= " + 600);
            i3 = 600;
        } else {
            i3 = i2;
        }
        int i4 = i3 * AdManager.AD_FILL_PARENT;
        b = i4;
        if (i4 == 0) {
            a(false);
        } else {
            a(true);
        }
    }

    private void a(boolean z) {
        synchronized (this) {
            if (z) {
                if (b > 0) {
                    if (this.c == null) {
                        this.c = new Timer();
                        this.c.schedule(new TimerTask() {
                            public final void run() {
                                WoobooAdView.this.requestFreshAd();
                            }
                        }, (long) b, (long) b);
                    }
                }
            }
            if (!z || b == 0) {
                if (this.c != null) {
                    this.c.cancel();
                    this.c = null;
                }
            } else if (super.getVisibility() == 4 && this.c != null) {
                this.c.cancel();
                this.c = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        if (i2 == 8) {
            a(false);
        } else if (i2 == 0) {
            a(true);
        } else if (i2 == 4) {
            a(false);
        }
    }

    public final void onWindowFocusChanged(boolean z) {
        a(z);
    }

    /* access modifiers changed from: protected */
    public final void setTextColor(int i2) {
        this.e = -16777216 | i2;
        if (this.a != null) {
            this.a.a(i2);
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public final int getTextColor() {
        return this.e;
    }

    public final void setBackgroundColor(int i2) {
        this.d = -16777216 | i2;
        if (this.a != null) {
            this.a.setBackgroundColor(i2);
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public final int getBackgroundColor() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public final void setGoneWithoutAd(boolean z) {
        this.f = z;
    }

    /* access modifiers changed from: protected */
    public final boolean isGoneWithoutAd() {
        return this.f;
    }

    public final void setVisibility(int i2) {
        if (super.getVisibility() != i2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i3 = 0; i3 < childCount; i3++) {
                    getChildAt(i3).setVisibility(i2);
                }
                super.setVisibility(i2);
                if (i2 != 0) {
                    this.a.e();
                    removeView(this.a);
                    this.a.c();
                    this.a = null;
                }
            }
        }
    }

    public final int getVisibility() {
        if (!this.f || hasAd()) {
            return super.getVisibility();
        }
        return 8;
    }

    public final void setAdListener(AdListener adListener) {
        synchronized (this) {
            this.g = adListener;
        }
    }

    public final boolean hasAd() {
        return this.a != null;
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        setMeasuredDimension(getMeasuredWidth(), j);
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        this.h = true;
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        this.h = false;
        a(false);
        super.onDetachedFromWindow();
    }

    static /* synthetic */ void b(WoobooAdView woobooAdView, a aVar) {
        if (woobooAdView.a != null) {
            woobooAdView.a.e();
        }
        woobooAdView.a = aVar;
        if (woobooAdView.h) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(233);
            alphaAnimation.startNow();
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new AccelerateInterpolator());
            woobooAdView.startAnimation(alphaAnimation);
        }
    }

    private final class a implements Runnable {
        /* access modifiers changed from: private */
        public a b;
        /* access modifiers changed from: private */
        public a c;

        public a(a aVar) {
            this.b = aVar;
        }

        public final void run() {
            this.c = WoobooAdView.this.a;
            if (this.c != null) {
                this.c.setVisibility(8);
                this.c.e();
            }
            this.b.setVisibility(0);
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(700);
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new DecelerateInterpolator());
            alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                public final void onAnimationStart(Animation animation) {
                }

                public final void onAnimationEnd(Animation animation) {
                    if (a.this.c != null) {
                        WoobooAdView.this.removeView(a.this.c);
                        a.this.c.c();
                        a.this.c = null;
                    }
                    WoobooAdView.this.a = a.this.b;
                }

                public final void onAnimationRepeat(Animation animation) {
                }
            });
            WoobooAdView.this.startAnimation(alphaAnimation);
        }
    }

    static /* synthetic */ void c(WoobooAdView woobooAdView, final a aVar) {
        aVar.setVisibility(8);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(700);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            public final void onAnimationStart(Animation animation) {
            }

            public final void onAnimationEnd(Animation animation) {
                WoobooAdView.this.post(new a(aVar));
            }

            public final void onAnimationRepeat(Animation animation) {
            }
        });
        woobooAdView.startAnimation(alphaAnimation);
    }
}
