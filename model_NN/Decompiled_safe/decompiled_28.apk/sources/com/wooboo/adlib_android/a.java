package com.wooboo.adlib_android;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.wooboo.adlib_android.b;
import java.io.Closeable;
import java.io.IOException;

final class a extends RelativeLayout implements Animation.AnimationListener, b.a {
    private static final Typeface a = Typeface.create(Typeface.SANS_SERIF, 1);
    private static final Typeface b = Typeface.create(Typeface.SANS_SERIF, 0);
    private int c = -16777216;
    private int d = -1;
    private Drawable e;
    private Drawable f;
    private Drawable g;
    private Drawable h;
    /* access modifiers changed from: private */
    public b i;
    private TextView j;
    private TextView k;
    /* access modifiers changed from: private */
    public f l = null;
    private ImageView m = null;
    /* access modifiers changed from: private */
    public ProgressBar n;
    /* access modifiers changed from: private */
    public boolean o;
    private boolean p;
    private i q;

    /* JADX WARNING: Removed duplicated region for block: B:41:0x0084 A[Catch:{ IOException -> 0x0068, all -> 0x0089 }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0091 A[Catch:{ IOException -> 0x0068, all -> 0x0089 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized byte[] a(java.lang.String r11, boolean r12) {
        /*
            r10 = this;
            r5 = 0
            r8 = 0
            monitor-enter(r10)
            java.lang.String r0 = "/"
            int r0 = r11.lastIndexOf(r0)     // Catch:{ all -> 0x0095 }
            int r0 = r0 + 1
            java.lang.String r1 = "."
            int r1 = r11.lastIndexOf(r1)     // Catch:{ all -> 0x0095 }
            java.lang.String r0 = r11.substring(r0, r1)     // Catch:{ all -> 0x0095 }
            java.lang.String r1 = r0.toLowerCase()     // Catch:{ all -> 0x0095 }
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x005f }
            r0.<init>(r11)     // Catch:{ MalformedURLException -> 0x005f }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IOException -> 0x0068, all -> 0x0089 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0068, all -> 0x0089 }
            r2 = 0
            r0.setConnectTimeout(r2)     // Catch:{ IOException -> 0x00a5, all -> 0x0098 }
            r2 = 0
            r0.setReadTimeout(r2)     // Catch:{ IOException -> 0x00a5, all -> 0x0098 }
            r2 = 1
            r0.setUseCaches(r2)     // Catch:{ IOException -> 0x00a5, all -> 0x0098 }
            r2 = 1
            r0.setDoInput(r2)     // Catch:{ IOException -> 0x00a5, all -> 0x0098 }
            r0.connect()     // Catch:{ IOException -> 0x00a5, all -> 0x0098 }
            java.io.InputStream r2 = r0.getInputStream()     // Catch:{ IOException -> 0x00a5, all -> 0x0098 }
            int r3 = r0.getContentLength()     // Catch:{ IOException -> 0x00ab, all -> 0x009e }
            r4 = -1
            if (r3 == r4) goto L_0x00b0
            byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x00ab, all -> 0x009e }
            r4 = 512(0x200, float:7.175E-43)
            byte[] r4 = new byte[r4]     // Catch:{ IOException -> 0x00ab, all -> 0x009e }
        L_0x0048:
            int r6 = r2.read(r4)     // Catch:{ IOException -> 0x00ab, all -> 0x009e }
            if (r6 > 0) goto L_0x0062
            com.wooboo.adlib_android.i r4 = r10.q     // Catch:{ IOException -> 0x00ab, all -> 0x009e }
            r4.a(r1, r3)     // Catch:{ IOException -> 0x00ab, all -> 0x009e }
            r1 = r3
        L_0x0054:
            a(r2)     // Catch:{ all -> 0x0095 }
            if (r0 == 0) goto L_0x005c
            r0.disconnect()     // Catch:{ all -> 0x0095 }
        L_0x005c:
            r0 = r1
        L_0x005d:
            monitor-exit(r10)
            return r0
        L_0x005f:
            r0 = move-exception
            r0 = r8
            goto L_0x005d
        L_0x0062:
            r7 = 0
            java.lang.System.arraycopy(r4, r7, r3, r5, r6)     // Catch:{ IOException -> 0x00ab, all -> 0x009e }
            int r5 = r5 + r6
            goto L_0x0048
        L_0x0068:
            r0 = move-exception
            r1 = r8
            r2 = r8
        L_0x006b:
            java.lang.String r3 = "Wooboo SDK 1.2"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a3 }
            java.lang.String r5 = "Exception getting image:  "
            r4.<init>(r5)     // Catch:{ all -> 0x00a3 }
            java.lang.StringBuilder r4 = r4.append(r11)     // Catch:{ all -> 0x00a3 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00a3 }
            android.util.Log.w(r3, r4, r0)     // Catch:{ all -> 0x00a3 }
            a(r2)     // Catch:{ all -> 0x0095 }
            if (r1 == 0) goto L_0x0087
            r1.disconnect()     // Catch:{ all -> 0x0095 }
        L_0x0087:
            r0 = r8
            goto L_0x005d
        L_0x0089:
            r0 = move-exception
            r1 = r8
            r2 = r8
        L_0x008c:
            a(r2)     // Catch:{ all -> 0x0095 }
            if (r1 == 0) goto L_0x0094
            r1.disconnect()     // Catch:{ all -> 0x0095 }
        L_0x0094:
            throw r0     // Catch:{ all -> 0x0095 }
        L_0x0095:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x0098:
            r1 = move-exception
            r2 = r8
            r9 = r0
            r0 = r1
            r1 = r9
            goto L_0x008c
        L_0x009e:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x008c
        L_0x00a3:
            r0 = move-exception
            goto L_0x008c
        L_0x00a5:
            r1 = move-exception
            r2 = r8
            r9 = r0
            r0 = r1
            r1 = r9
            goto L_0x006b
        L_0x00ab:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x006b
        L_0x00b0:
            r1 = r8
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wooboo.adlib_android.a.a(java.lang.String, boolean):byte[]");
    }

    private static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e2) {
                Log.e("Wooboo SDK 1.2", "Could not close stream", e2);
            }
        }
    }

    public final void c() {
        g();
        if (!(this.l == null || this.l.getBackground() == null)) {
            this.l.getBackground().setCallback(null);
            this.l.setBackgroundDrawable(null);
        }
        if (this.m != null && this.m.getBackground() != null) {
            this.m.getBackground().setCallback(null);
            this.m.setBackgroundDrawable(null);
        }
    }

    private void g() {
        if (this.e != null) {
            ((BitmapDrawable) this.e).getBitmap().recycle();
        }
        if (this.f != null) {
            ((BitmapDrawable) this.f).getBitmap().recycle();
        }
        if (this.g != null) {
            ((BitmapDrawable) this.g).getBitmap().recycle();
        }
        this.e = null;
        this.g = null;
        this.f = null;
        if (this.h != null) {
            ((BitmapDrawable) this.h).getBitmap().recycle();
        }
        this.h = null;
    }

    public final void d() {
        if (this.l != null) {
            this.l.a();
        }
    }

    public final void e() {
        if (this.l != null) {
            this.l.b();
            if (this.l.getBackground() != null) {
                this.l.getBackground().setCallback(null);
                this.l.setBackgroundDrawable(null);
            }
            this.l = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wooboo.adlib_android.a.a(java.lang.String, boolean):byte[]
     arg types: [java.lang.String, int]
     candidates:
      com.wooboo.adlib_android.a.a(com.wooboo.adlib_android.a, boolean):void
      com.wooboo.adlib_android.a.a(java.lang.String, boolean):byte[] */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x03a0 A[SYNTHETIC, Splitter:B:132:0x03a0] */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x03a4  */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x03f8  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0112  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01b0  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01f0  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0230 A[SYNTHETIC, Splitter:B:68:0x0230] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x024a A[SYNTHETIC, Splitter:B:79:0x024a] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:76:0x0245=Splitter:B:76:0x0245, B:65:0x022b=Splitter:B:65:0x022b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public a(com.wooboo.adlib_android.b r10, android.content.Context r11, boolean r12, int r13, double r14) {
        /*
            r9 = this;
            r9.<init>(r11)
            r0 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r9.c = r0
            r0 = -1
            r9.d = r0
            r0 = 0
            r9.l = r0
            r0 = 0
            r9.m = r0
            com.wooboo.adlib_android.i r0 = r9.q
            if (r0 != 0) goto L_0x001a
            com.wooboo.adlib_android.i r0 = com.wooboo.adlib_android.i.a(r11)
            r9.q = r0
        L_0x001a:
            r9.p = r12
            r9.i = r10
            r10.a(r9)
            r0 = 0
            r9.e = r0
            r0 = 0
            r9.g = r0
            r0 = 0
            r9.f = r0
            r0 = 0
            r9.n = r0
            r0 = 0
            r9.o = r0
            if (r10 == 0) goto L_0x01f5
            r0 = 1
            r9.setFocusable(r0)
            r0 = 1
            r9.setClickable(r0)
            r0 = 0
            java.lang.String r1 = r10.b()
            int r1 = r1.length()
            java.lang.String r2 = r10.c()
            if (r2 == 0) goto L_0x0324
            com.wooboo.adlib_android.f r3 = r9.l     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            if (r3 == 0) goto L_0x0074
            com.wooboo.adlib_android.f r3 = r9.l     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.graphics.drawable.Drawable r3 = r3.getBackground()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            if (r3 == 0) goto L_0x0074
            com.wooboo.adlib_android.f r3 = r9.l     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.graphics.drawable.Drawable r12 = r3.getBackground()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.graphics.drawable.BitmapDrawable r12 = (android.graphics.drawable.BitmapDrawable) r12     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.graphics.Bitmap r3 = r12.getBitmap()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r3.recycle()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            com.wooboo.adlib_android.f r3 = r9.l     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.graphics.drawable.Drawable r3 = r3.getBackground()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r4 = 0
            r3.setCallback(r4)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            com.wooboo.adlib_android.f r3 = r9.l     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r4 = 0
            r3.setBackgroundDrawable(r4)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
        L_0x0074:
            android.widget.ImageView r3 = r9.m     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            if (r3 == 0) goto L_0x009f
            android.widget.ImageView r3 = r9.m     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.graphics.drawable.Drawable r3 = r3.getBackground()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            if (r3 == 0) goto L_0x009f
            android.widget.ImageView r3 = r9.m     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.graphics.drawable.Drawable r12 = r3.getBackground()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.graphics.drawable.BitmapDrawable r12 = (android.graphics.drawable.BitmapDrawable) r12     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.graphics.Bitmap r3 = r12.getBitmap()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r3.recycle()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.widget.ImageView r3 = r9.m     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.graphics.drawable.Drawable r3 = r3.getBackground()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r4 = 0
            r3.setCallback(r4)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.widget.ImageView r3 = r9.m     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r4 = 0
            r3.setBackgroundDrawable(r4)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
        L_0x009f:
            java.lang.String r3 = ".gif"
            boolean r3 = r2.contains(r3)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            if (r3 != 0) goto L_0x00af
            java.lang.String r3 = ".GIF"
            boolean r3 = r2.contains(r3)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            if (r3 == 0) goto L_0x0252
        L_0x00af:
            java.lang.String r3 = "/"
            int r3 = r2.lastIndexOf(r3)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            int r3 = r3 + 1
            java.lang.String r4 = "."
            int r4 = r2.lastIndexOf(r4)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            java.lang.String r3 = r2.substring(r3, r4)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            java.lang.String r3 = r3.toLowerCase()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            com.wooboo.adlib_android.i r4 = r9.q     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            boolean r4 = r4.a(r3)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            if (r4 == 0) goto L_0x01ff
            com.wooboo.adlib_android.i r2 = r9.q     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            byte[] r2 = r2.b(r3)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            if (r2 == 0) goto L_0x00db
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r3.<init>(r2)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r0 = r3
        L_0x00db:
            if (r1 > 0) goto L_0x020e
            android.widget.RelativeLayout$LayoutParams r2 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0227, Error -> 0x0241, all -> 0x0406 }
            r3 = -1
            r4 = -2
            r2.<init>(r3, r4)     // Catch:{ Exception -> 0x0227, Error -> 0x0241, all -> 0x0406 }
        L_0x00e4:
            if (r1 > 0) goto L_0x021a
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r2.setMargins(r3, r4, r5, r6)     // Catch:{ Exception -> 0x0227, Error -> 0x0241, all -> 0x0406 }
        L_0x00ed:
            r3 = 9
            r2.addRule(r3)     // Catch:{ Exception -> 0x0227, Error -> 0x0241, all -> 0x0406 }
            com.wooboo.adlib_android.f r3 = new com.wooboo.adlib_android.f     // Catch:{ Exception -> 0x0227, Error -> 0x0241, all -> 0x0406 }
            r3.<init>(r11, r0)     // Catch:{ Exception -> 0x0227, Error -> 0x0241, all -> 0x0406 }
            r9.l = r3     // Catch:{ Exception -> 0x0227, Error -> 0x0241, all -> 0x0406 }
            com.wooboo.adlib_android.f r3 = r9.l     // Catch:{ Exception -> 0x0227, Error -> 0x0241, all -> 0x0406 }
            r3.setLayoutParams(r2)     // Catch:{ Exception -> 0x0227, Error -> 0x0241, all -> 0x0406 }
            com.wooboo.adlib_android.f r2 = r9.l     // Catch:{ Exception -> 0x0227, Error -> 0x0241, all -> 0x0406 }
            r3 = 1
            r2.setId(r3)     // Catch:{ Exception -> 0x0227, Error -> 0x0241, all -> 0x0406 }
            com.wooboo.adlib_android.f r2 = r9.l     // Catch:{ Exception -> 0x0227, Error -> 0x0241, all -> 0x0406 }
            r9.addView(r2)     // Catch:{ Exception -> 0x0227, Error -> 0x0241, all -> 0x0406 }
        L_0x0109:
            if (r0 == 0) goto L_0x010e
            r0.close()     // Catch:{ IOException -> 0x0403 }
        L_0x010e:
            r0 = 15
            if (r1 <= r0) goto L_0x03a4
            android.widget.TextView r0 = new android.widget.TextView
            r0.<init>(r11)
            r9.j = r0
            android.widget.TextView r0 = r9.j
            java.lang.String r2 = r10.b()
            r0.setText(r2)
            android.widget.TextView r0 = r9.j
            android.graphics.Typeface r2 = com.wooboo.adlib_android.a.a
            r0.setTypeface(r2)
            android.widget.TextView r0 = r9.j
            int r2 = r9.d
            r0.setTextColor(r2)
            android.widget.TextView r0 = r9.j
            r2 = 1096810496(0x41600000, float:14.0)
            r0.setTextSize(r2)
            android.widget.TextView r0 = r9.j
            android.text.TextUtils$TruncateAt r2 = android.text.TextUtils.TruncateAt.END
            r0.setEllipsize(r2)
            android.widget.TextView r0 = r9.j
            r2 = 1
            r0.setSingleLine(r2)
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r2 = -1
            r3 = -1
            r0.<init>(r2, r3)
            android.widget.ImageView r2 = r9.m
            if (r2 == 0) goto L_0x0154
            r2 = 1
            r3 = 1
            r0.addRule(r2, r3)
        L_0x0154:
            r2 = 6
            r3 = 6
            r4 = 0
            r5 = 0
            r0.setMargins(r2, r3, r4, r5)
            android.widget.TextView r2 = r9.j
            r2.setLayoutParams(r0)
            android.widget.TextView r0 = r9.j
            r2 = 2
            r0.setId(r2)
            android.widget.TextView r0 = r9.j
            r9.addView(r0)
        L_0x016b:
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r2 = 44
            r3 = 44
            r0.<init>(r2, r3)
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r0.setMargins(r2, r3, r4, r5)
            android.widget.ProgressBar r2 = new android.widget.ProgressBar
            r2.<init>(r11)
            r9.n = r2
            android.widget.ProgressBar r2 = r9.n
            r3 = 1
            r2.setIndeterminate(r3)
            android.widget.ProgressBar r2 = r9.n
            r2.setLayoutParams(r0)
            android.widget.ProgressBar r0 = r9.n
            r2 = 4
            r0.setVisibility(r2)
            android.widget.ProgressBar r0 = r9.n
            r9.addView(r0)
            android.widget.TextView r0 = new android.widget.TextView
            r0.<init>(r11)
            r9.k = r0
            android.widget.TextView r0 = r9.k
            r2 = 5
            r0.setGravity(r2)
            android.widget.TextView r0 = r9.k
            android.graphics.Typeface r2 = com.wooboo.adlib_android.a.b
            r0.setTypeface(r2)
            boolean r0 = r9.p
            if (r0 == 0) goto L_0x03f8
            android.widget.TextView r0 = r9.k
            int r2 = com.wooboo.adlib_android.ImpressionAdView.getTextColor()
            r0.setTextColor(r2)
        L_0x01b9:
            android.widget.TextView r0 = r9.k
            r2 = 1092616192(0x41200000, float:10.0)
            r0.setTextSize(r2)
            android.widget.TextView r0 = r9.k
            java.lang.String r2 = "哇棒 传媒"
            r0.setText(r2)
            android.widget.TextView r0 = r9.k
            r2 = 3
            r0.setId(r2)
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r2 = -2
            r3 = -2
            r0.<init>(r2, r3)
            r2 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r2 = r2 * r14
            int r2 = (int) r2
            int r2 = r2 + 2
            r3 = 0
            int r2 = r2 * 6
            int r2 = r13 - r2
            r4 = 6
            r5 = 6
            r0.setMargins(r3, r2, r4, r5)
            r2 = 11
            r0.addRule(r2)
            android.widget.TextView r2 = r9.k
            r2.setLayoutParams(r0)
            if (r1 <= 0) goto L_0x01f5
            android.widget.TextView r0 = r9.k
            r9.addView(r0)
        L_0x01f5:
            r0 = -1
            r9.a(r0)
            r0 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r9.setBackgroundColor(r0)
            return
        L_0x01ff:
            r3 = 1
            byte[] r2 = r9.a(r2, r3)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            if (r2 == 0) goto L_0x00db
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r3.<init>(r2)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r0 = r3
            goto L_0x00db
        L_0x020e:
            r2 = 4631952216750555136(0x4048000000000000, double:48.0)
            double r2 = r2 * r14
            int r2 = (int) r2
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0227, Error -> 0x0241, all -> 0x0406 }
            r3.<init>(r2, r2)     // Catch:{ Exception -> 0x0227, Error -> 0x0241, all -> 0x0406 }
            r2 = r3
            goto L_0x00e4
        L_0x021a:
            boolean r3 = r9.p     // Catch:{ Exception -> 0x0227, Error -> 0x0241, all -> 0x0406 }
            if (r3 == 0) goto L_0x0238
            r3 = 1
            r4 = 1
            r5 = 0
            r6 = 2
            r2.setMargins(r3, r4, r5, r6)     // Catch:{ Exception -> 0x0227, Error -> 0x0241, all -> 0x0406 }
            goto L_0x00ed
        L_0x0227:
            r2 = move-exception
            r8 = r2
            r2 = r0
            r0 = r8
        L_0x022b:
            r0.printStackTrace()     // Catch:{ all -> 0x0411 }
            if (r2 == 0) goto L_0x010e
            r2.close()     // Catch:{ IOException -> 0x0235 }
            goto L_0x010e
        L_0x0235:
            r0 = move-exception
            goto L_0x010e
        L_0x0238:
            r3 = 4
            r4 = 4
            r5 = 0
            r6 = 4
            r2.setMargins(r3, r4, r5, r6)     // Catch:{ Exception -> 0x0227, Error -> 0x0241, all -> 0x0406 }
            goto L_0x00ed
        L_0x0241:
            r2 = move-exception
            r8 = r2
            r2 = r0
            r0 = r8
        L_0x0245:
            r0.printStackTrace()     // Catch:{ all -> 0x0411 }
            if (r2 == 0) goto L_0x010e
            r2.close()     // Catch:{ IOException -> 0x024f }
            goto L_0x010e
        L_0x024f:
            r0 = move-exception
            goto L_0x010e
        L_0x0252:
            r3 = 0
            int r4 = r2.length()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            if (r4 == 0) goto L_0x042a
            java.lang.String r3 = "/"
            int r3 = r2.lastIndexOf(r3)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            int r3 = r3 + 1
            java.lang.String r4 = "."
            int r4 = r2.lastIndexOf(r4)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            java.lang.String r3 = r2.substring(r3, r4)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            java.lang.String r3 = r3.toLowerCase()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            com.wooboo.adlib_android.i r4 = r9.q     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            boolean r4 = r4.a(r3)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            if (r4 == 0) goto L_0x02d3
            com.wooboo.adlib_android.i r2 = r9.q     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            byte[] r2 = r2.b(r3)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r3.<init>(r2)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r3)     // Catch:{ Exception -> 0x041f, Error -> 0x0414, all -> 0x040b }
            r2 = r3
        L_0x0287:
            if (r0 != 0) goto L_0x029b
            android.content.Context r0 = r9.getContext()     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            android.content.res.AssetManager r0 = r0.getAssets()     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            java.lang.String r3 = "wooboo_logo.png"
            java.io.InputStream r0 = r0.open(r3)     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
        L_0x029b:
            if (r1 > 0) goto L_0x02fb
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            r4 = -1
            r3.<init>(r4, r13)     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r3.setMargins(r4, r5, r6, r7)     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
        L_0x02aa:
            r4 = 9
            r3.addRule(r4)     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            android.widget.ImageView r4 = new android.widget.ImageView     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            r4.<init>(r11)     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            r9.m = r4     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            android.widget.ImageView r4 = r9.m     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            r4.setLayoutParams(r3)     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            android.widget.ImageView r3 = r9.m     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            android.graphics.drawable.BitmapDrawable r4 = new android.graphics.drawable.BitmapDrawable     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            r3.setBackgroundDrawable(r4)     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            android.widget.ImageView r0 = r9.m     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            r3 = 1
            r0.setId(r3)     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            android.widget.ImageView r0 = r9.m     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            r9.addView(r0)     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            r0 = r2
            goto L_0x0109
        L_0x02d3:
            r3 = 1
            byte[] r2 = r9.a(r2, r3)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            if (r2 == 0) goto L_0x02e5
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r3.<init>(r2)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r3)     // Catch:{ Exception -> 0x0423, Error -> 0x0418, all -> 0x040e }
            r2 = r3
            goto L_0x0287
        L_0x02e5:
            android.content.Context r2 = r9.getContext()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.content.res.AssetManager r2 = r2.getAssets()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            java.lang.String r3 = "wooboo_logo.png"
            java.io.InputStream r2 = r2.open(r3)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r8 = r2
            r2 = r0
            r0 = r8
            goto L_0x0287
        L_0x02fb:
            int r3 = r0.getWidth()     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            int r4 = r0.getHeight()     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            double r5 = (double) r3     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            double r5 = r5 * r14
            int r3 = (int) r5     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            double r4 = (double) r4     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            double r4 = r4 * r14
            int r4 = (int) r4     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            android.widget.RelativeLayout$LayoutParams r5 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            r5.<init>(r3, r4)     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            boolean r3 = r9.p     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            if (r3 == 0) goto L_0x031b
            r3 = 1
            r4 = 1
            r6 = 0
            r7 = 2
            r5.setMargins(r3, r4, r6, r7)     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            r3 = r5
            goto L_0x02aa
        L_0x031b:
            r3 = 4
            r4 = 4
            r6 = 0
            r7 = 4
            r5.setMargins(r3, r4, r6, r7)     // Catch:{ Exception -> 0x0427, Error -> 0x041c }
            r3 = r5
            goto L_0x02aa
        L_0x0324:
            android.content.Context r2 = r9.getContext()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.content.res.AssetManager r2 = r2.getAssets()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            java.lang.String r3 = "wooboo_logo.png"
            java.io.InputStream r2 = r2.open(r3)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            int r3 = r2.getWidth()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            int r4 = r2.getHeight()     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            double r5 = (double) r3     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            double r5 = r5 * r14
            int r3 = (int) r5     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            double r4 = (double) r4     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            double r4 = r4 * r14
            int r4 = (int) r4     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.widget.RelativeLayout$LayoutParams r5 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r5.<init>(r3, r4)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            if (r1 > 0) goto L_0x0380
            r3 = 0
            r4 = 0
            r6 = 0
            r7 = 0
            r5.setMargins(r3, r4, r6, r7)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
        L_0x0352:
            r3 = 9
            r5.addRule(r3)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.widget.ImageView r3 = new android.widget.ImageView     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r3.<init>(r11)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r9.m = r3     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.widget.ImageView r3 = r9.m     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r3.setLayoutParams(r5)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.widget.ImageView r3 = r9.m     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.graphics.drawable.BitmapDrawable r4 = new android.graphics.drawable.BitmapDrawable     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r4.<init>(r2)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r3.setBackgroundDrawable(r4)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.widget.ImageView r2 = r9.m     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r3 = 1
            r2.setId(r3)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            android.widget.ImageView r2 = r9.m     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            r9.addView(r2)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            goto L_0x0109
        L_0x037a:
            r2 = move-exception
            r8 = r2
            r2 = r0
            r0 = r8
            goto L_0x022b
        L_0x0380:
            boolean r3 = r9.p     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            if (r3 == 0) goto L_0x0392
            r3 = 1
            r4 = 1
            r6 = 0
            r7 = 2
            r5.setMargins(r3, r4, r6, r7)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            goto L_0x0352
        L_0x038c:
            r2 = move-exception
            r8 = r2
            r2 = r0
            r0 = r8
            goto L_0x0245
        L_0x0392:
            r3 = 4
            r4 = 4
            r6 = 0
            r7 = 4
            r5.setMargins(r3, r4, r6, r7)     // Catch:{ Exception -> 0x037a, Error -> 0x038c, all -> 0x039a }
            goto L_0x0352
        L_0x039a:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x039e:
            if (r1 == 0) goto L_0x03a3
            r1.close()     // Catch:{ IOException -> 0x0401 }
        L_0x03a3:
            throw r0
        L_0x03a4:
            android.widget.TextView r0 = new android.widget.TextView
            r0.<init>(r11)
            r9.j = r0
            android.widget.TextView r0 = r9.j
            java.lang.String r2 = r10.b()
            r0.setText(r2)
            android.widget.TextView r0 = r9.j
            android.graphics.Typeface r2 = com.wooboo.adlib_android.a.a
            r0.setTypeface(r2)
            android.widget.TextView r0 = r9.j
            int r2 = r9.d
            r0.setTextColor(r2)
            android.widget.TextView r0 = r9.j
            r2 = 1098907648(0x41800000, float:16.0)
            r0.setTextSize(r2)
            android.widget.TextView r0 = r9.j
            r2 = 1
            r0.setSingleLine(r2)
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r2 = -2
            r3 = -2
            r0.<init>(r2, r3)
            android.widget.ImageView r2 = r9.m
            if (r2 == 0) goto L_0x03df
            r2 = 1
            r3 = 1
            r0.addRule(r2, r3)
        L_0x03df:
            r2 = 6
            r3 = 6
            r4 = 0
            r5 = 0
            r0.setMargins(r2, r3, r4, r5)
            android.widget.TextView r2 = r9.j
            r2.setLayoutParams(r0)
            android.widget.TextView r0 = r9.j
            r2 = 2
            r0.setId(r2)
            android.widget.TextView r0 = r9.j
            r9.addView(r0)
            goto L_0x016b
        L_0x03f8:
            android.widget.TextView r0 = r9.k
            int r2 = r9.d
            r0.setTextColor(r2)
            goto L_0x01b9
        L_0x0401:
            r1 = move-exception
            goto L_0x03a3
        L_0x0403:
            r0 = move-exception
            goto L_0x010e
        L_0x0406:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x039e
        L_0x040b:
            r0 = move-exception
            r1 = r3
            goto L_0x039e
        L_0x040e:
            r0 = move-exception
            r1 = r3
            goto L_0x039e
        L_0x0411:
            r0 = move-exception
            r1 = r2
            goto L_0x039e
        L_0x0414:
            r0 = move-exception
            r2 = r3
            goto L_0x0245
        L_0x0418:
            r0 = move-exception
            r2 = r3
            goto L_0x0245
        L_0x041c:
            r0 = move-exception
            goto L_0x0245
        L_0x041f:
            r0 = move-exception
            r2 = r3
            goto L_0x022b
        L_0x0423:
            r0 = move-exception
            r2 = r3
            goto L_0x022b
        L_0x0427:
            r0 = move-exception
            goto L_0x022b
        L_0x042a:
            r2 = r0
            r0 = r3
            goto L_0x0287
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wooboo.adlib_android.a.<init>(com.wooboo.adlib_android.b, android.content.Context, boolean, int, double):void");
    }

    public final void a(int i2) {
        this.d = -16777216 | i2;
        this.j.setTextColor(this.d);
        if (!this.p) {
            this.k.setTextColor(this.d);
        }
        postInvalidate();
    }

    public final void setBackgroundColor(int i2) {
        this.c = -16777216 | i2;
    }

    /* access modifiers changed from: protected */
    public final b f() {
        return this.i;
    }

    /* access modifiers changed from: protected */
    public final void onSizeChanged(int i2, int i3, int i4, int i5) {
        int i6;
        super.onSizeChanged(i2, i3, i4, i5);
        if (!this.p) {
            i6 = this.k.getVisibility();
        } else {
            i6 = 0;
        }
        if (i6 == 0) {
            Typeface typeface = this.j.getTypeface();
            String b2 = this.i.b();
            if (b2 != null) {
                Paint paint = new Paint();
                paint.setTypeface(typeface);
                paint.setTextSize(this.j.getTextSize());
                if (paint.measureText(b2) > ((float) i2)) {
                    i6 = 8;
                }
            }
        }
        this.k.setVisibility(0);
        if (!this.p) {
            this.k.setVisibility(i6);
        }
        if (i2 != 0 && i3 != 0 && !this.p) {
            Rect rect = new Rect(0, 0, i2, i3);
            g();
            this.e = a(rect, -1, this.c);
            this.g = a(rect, -1147097, -19456);
            this.f = a(rect, -1, this.c, true);
            setBackgroundDrawable(this.e);
        }
    }

    private Drawable a(Rect rect, int i2, int i3) {
        return a(rect, i2, i3, false);
    }

    private Drawable a(Rect rect, int i2, int i3, boolean z) {
        Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        if (!this.p) {
            Paint paint = new Paint();
            paint.setColor(i2);
            paint.setAntiAlias(true);
            canvas.drawRect(rect, paint);
            GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb(127, Color.red(i3), Color.green(i3), Color.blue(i3)), i3});
            int height = ((int) (((double) rect.height()) * 0.4375d)) + rect.top;
            gradientDrawable.setBounds(rect.left, rect.top, rect.right, height);
            gradientDrawable.draw(canvas);
            Rect rect2 = new Rect(rect.left, height, rect.right, rect.bottom);
            Paint paint2 = new Paint();
            paint2.setColor(i3);
            canvas.drawRect(rect2, paint2);
        }
        if (z) {
            Paint paint3 = new Paint();
            paint3.setAntiAlias(true);
            paint3.setColor(-1147097);
            paint3.setStyle(Paint.Style.STROKE);
            paint3.setStrokeWidth(3.0f);
            paint3.setPathEffect(new CornerPathEffect(3.0f));
            Path path = new Path();
            path.addRoundRect(new RectF(rect), 3.0f, 3.0f, Path.Direction.CW);
            canvas.drawPath(path, paint3);
        }
        return new BitmapDrawable(createBitmap);
    }

    public final void b() {
        post(new Thread() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.wooboo.adlib_android.a.a(com.wooboo.adlib_android.a, boolean):void
             arg types: [com.wooboo.adlib_android.a, int]
             candidates:
              com.wooboo.adlib_android.a.a(java.lang.String, boolean):byte[]
              com.wooboo.adlib_android.a.a(com.wooboo.adlib_android.a, boolean):void */
            public final void run() {
                try {
                    a.this.n.setVisibility(4);
                    a.this.l.setVisibility(0);
                    a.this.o = false;
                } catch (Exception e) {
                }
            }
        });
    }

    public final void a() {
        post(new Thread() {
            public final void run() {
                try {
                    a.this.n.setVisibility(0);
                } catch (Exception e) {
                }
            }
        });
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 66 || i2 == 23) {
            setPressed(true);
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public final boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 == 66 || i2 == 23) {
            h();
        }
        setPressed(false);
        return super.onKeyUp(i2, keyEvent);
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            setPressed(true);
        } else if (action == 2) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            int left = getLeft();
            int top = getTop();
            int right = getRight();
            int bottom = getBottom();
            if (x < ((float) left) || x > ((float) right) || y < ((float) top) || y > ((float) bottom)) {
                setPressed(false);
            } else {
                setPressed(true);
            }
        } else if (action == 1) {
            if (isPressed()) {
                h();
            }
            setPressed(false);
        } else if (action == 3) {
            setPressed(false);
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public final boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            setPressed(true);
        } else if (motionEvent.getAction() == 1) {
            if (hasFocus()) {
                h();
            }
            setPressed(false);
        }
        return super.onTrackballEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public final void onFocusChanged(boolean z, int i2, Rect rect) {
        super.onFocusChanged(z, i2, rect);
        if (this.p) {
            return;
        }
        if (z) {
            setBackgroundDrawable(this.f);
        } else {
            setBackgroundDrawable(this.e);
        }
    }

    public final void setPressed(boolean z) {
        Drawable drawable;
        if ((!z || !this.o) && isPressed() != z) {
            int i2 = this.d;
            if (z) {
                this.h = getBackground();
                drawable = this.g;
                i2 = -16777216;
            } else {
                drawable = this.h;
            }
            setBackgroundDrawable(drawable);
            if (this.j != null) {
                this.j.setTextColor(i2);
            }
            if (this.k != null) {
                this.k.setTextColor(i2);
            }
            super.setPressed(z);
            invalidate();
        }
    }

    private void h() {
        float f2;
        float f3;
        if (this.i != null && isPressed()) {
            setPressed(false);
            if (!this.o) {
                this.o = true;
                if (this.l == null && this.m == null) {
                    this.i.a();
                    this.o = false;
                    return;
                }
                AnimationSet animationSet = new AnimationSet(true);
                float f4 = 20.0f;
                float f5 = 20.0f;
                if (this.l != null) {
                    f4 = ((float) this.l.getWidth()) / 2.0f;
                    f5 = ((float) this.l.getHeight()) / 2.0f;
                    this.l.b();
                }
                float f6 = f5;
                float f7 = f4;
                float f8 = f6;
                if (this.m != null) {
                    f2 = ((float) this.m.getHeight()) / 2.0f;
                    f3 = ((float) this.m.getWidth()) / 2.0f;
                } else {
                    f2 = f8;
                    f3 = f7;
                }
                ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.2f, 1.0f, 1.2f, f3, f2);
                scaleAnimation.setDuration(200);
                animationSet.addAnimation(scaleAnimation);
                ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.2f, 0.001f, 1.2f, 0.001f, f3, f2);
                scaleAnimation2.setDuration(299);
                scaleAnimation2.setStartOffset(200);
                scaleAnimation2.setAnimationListener(this);
                animationSet.addAnimation(scaleAnimation2);
                postDelayed(new Thread() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.wooboo.adlib_android.a.a(com.wooboo.adlib_android.a, boolean):void
                     arg types: [com.wooboo.adlib_android.a, int]
                     candidates:
                      com.wooboo.adlib_android.a.a(java.lang.String, boolean):byte[]
                      com.wooboo.adlib_android.a.a(com.wooboo.adlib_android.a, boolean):void */
                    public final void run() {
                        a.this.i.a();
                        a.this.o = false;
                    }
                }, 500);
                if (this.l != null) {
                    this.l.startAnimation(animationSet);
                }
                if (this.m != null) {
                    this.m.startAnimation(animationSet);
                }
            }
        }
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void onAnimationEnd(Animation animation) {
    }
}
