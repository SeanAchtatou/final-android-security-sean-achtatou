package com.wooboo.adlib_android;

import android.util.Log;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public final class h {
    private static byte[] a = {20, 10, 1, 20};

    protected static ArrayList<Object> a(byte[] bArr) {
        String str = null;
        if (bArr.length == 7) {
            if (bArr[6] == -32) {
                Log.e("Wooboo SDK 1.2", "Wooboo_PID does not exist");
            } else if (bArr[6] == -31) {
                Log.e("Wooboo SDK 1.2", "Wooboo Server busy");
            } else if (bArr[6] == -30) {
                Log.e("Wooboo SDK 1.2", "Wooboo_PID does not activated");
            } else if (bArr[6] == -29) {
                Log.e("Wooboo SDK 1.2", "Wooboo Server couldnot find the most suitable ad currently");
            } else if (bArr[6] == -28) {
                Log.e("Wooboo SDK 1.2", "Other unknown error");
            }
            return null;
        }
        byte b = 0;
        String str2 = null;
        int length = a.length + 2;
        String str3 = null;
        while (length < bArr.length) {
            byte b2 = bArr[length];
            int i = length + 1;
            short s = (short) (((bArr[i] & 65280) << 8) + (bArr[i + 1] & 255));
            length = i + 2;
            try {
                String str4 = new String(bArr, length, s, XmlConstant.DEFAULT_ENCODING);
                if (b2 == -6) {
                    str = str4;
                } else if (b2 == -5) {
                    str2 = str4;
                } else if (b2 == -4) {
                    str2 = str4.substring(1);
                } else {
                    str3 = str4;
                    b = b2;
                }
                length += s;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        ArrayList<Object> arrayList = new ArrayList<>();
        arrayList.add(str);
        arrayList.add(str2);
        arrayList.add(str3);
        arrayList.add(Byte.valueOf(b));
        return arrayList;
    }
}
