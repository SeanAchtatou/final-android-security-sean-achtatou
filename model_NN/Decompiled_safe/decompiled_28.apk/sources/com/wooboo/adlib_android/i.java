package com.wooboo.adlib_android;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public final class i extends SQLiteOpenHelper {
    private static i a = null;

    protected static i a(Context context) {
        if (a == null) {
            a = new i(context, "woobooad.db", null, 2);
        }
        return a;
    }

    private i(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 2);
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS woobooad( ID integer primary key,imgName varchar,downloadTime date,imgData blob)");
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS woobooad");
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS woobooad( ID integer primary key,imgName varchar,downloadTime date,imgData blob)");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.String r10, byte[] r11) {
        /*
            r9 = this;
            r5 = 0
            java.lang.String r0 = "woobooad"
            android.database.sqlite.SQLiteDatabase r0 = r9.getWritableDatabase()     // Catch:{ Exception -> 0x0091 }
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ Exception -> 0x0091 }
            r1.<init>()     // Catch:{ Exception -> 0x0091 }
            java.lang.String r2 = "imgName"
            r1.put(r2, r10)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r2 = "downloadTime"
            java.text.SimpleDateFormat r3 = new java.text.SimpleDateFormat     // Catch:{ Exception -> 0x0091 }
            java.lang.String r4 = "yyyy-MM-dd hh:mm:ss"
            r3.<init>(r4)     // Catch:{ Exception -> 0x0091 }
            java.util.Date r4 = new java.util.Date     // Catch:{ Exception -> 0x0091 }
            r4.<init>()     // Catch:{ Exception -> 0x0091 }
            java.lang.String r3 = r3.format(r4)     // Catch:{ Exception -> 0x0091 }
            r1.put(r2, r3)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r2 = "imgData"
            r1.put(r2, r11)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r2 = "woobooad"
            java.lang.String r3 = "ID"
            r0.insert(r2, r3, r1)     // Catch:{ Exception -> 0x0091 }
            r0.close()     // Catch:{ Exception -> 0x0091 }
        L_0x0035:
            android.database.sqlite.SQLiteDatabase r0 = r9.getWritableDatabase()     // Catch:{ Exception -> 0x0072, all -> 0x007d }
            java.lang.String r1 = "woobooad"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x008c, all -> 0x0085 }
            int r2 = r1.getCount()     // Catch:{ Exception -> 0x008c, all -> 0x0085 }
            r3 = 30
            if (r2 <= r3) goto L_0x0067
            r1.moveToFirst()     // Catch:{ Exception -> 0x008c, all -> 0x0085 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008c, all -> 0x0085 }
            java.lang.String r3 = "delete from woobooad where ID = "
            r2.<init>(r3)     // Catch:{ Exception -> 0x008c, all -> 0x0085 }
            r3 = 0
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x008c, all -> 0x0085 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x008c, all -> 0x0085 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x008c, all -> 0x0085 }
            r0.execSQL(r2)     // Catch:{ Exception -> 0x008c, all -> 0x0085 }
        L_0x0067:
            if (r1 == 0) goto L_0x006c
            r1.close()     // Catch:{ Exception -> 0x008c, all -> 0x0085 }
        L_0x006c:
            if (r0 == 0) goto L_0x0071
            r0.close()
        L_0x0071:
            return
        L_0x0072:
            r0 = move-exception
            r1 = r5
        L_0x0074:
            r0.printStackTrace()     // Catch:{ all -> 0x008a }
            if (r1 == 0) goto L_0x0071
            r1.close()
            goto L_0x0071
        L_0x007d:
            r0 = move-exception
            r1 = r5
        L_0x007f:
            if (r1 == 0) goto L_0x0084
            r1.close()
        L_0x0084:
            throw r0
        L_0x0085:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x007f
        L_0x008a:
            r0 = move-exception
            goto L_0x007f
        L_0x008c:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0074
        L_0x0091:
            r0 = move-exception
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wooboo.adlib_android.i.a(java.lang.String, byte[]):void");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0040, code lost:
        r1.close();
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0048, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0054, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0055, code lost:
        r11 = r1;
        r1 = r0;
        r0 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x005f, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0060, code lost:
        r11 = r1;
        r1 = r0;
        r0 = r11;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0054 A[ExcHandler: all (r1v10 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:4:0x000f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(java.lang.String r13) {
        /*
            r12 = this;
            r9 = 1
            r8 = 0
            r1 = 0
            java.lang.String r10 = "Wooboo SDK 1.2"
            android.database.sqlite.SQLiteDatabase r0 = r12.getWritableDatabase()     // Catch:{ Exception -> 0x0039 }
            java.lang.String r1 = "woobooad"
            r2 = 0
            java.lang.String r3 = "imgName=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0059, all -> 0x0054 }
            r5 = 0
            r4[r5] = r13     // Catch:{ Exception -> 0x0059, all -> 0x0054 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0059, all -> 0x0054 }
            if (r1 == 0) goto L_0x0064
            int r2 = r1.getCount()     // Catch:{ Exception -> 0x0059, all -> 0x0054 }
            if (r2 == 0) goto L_0x0064
            r2 = r9
        L_0x0024:
            if (r1 == 0) goto L_0x0029
            r1.close()     // Catch:{ Exception -> 0x005f, all -> 0x0054 }
        L_0x0029:
            if (r0 == 0) goto L_0x002e
            r0.close()
        L_0x002e:
            r0 = r2
        L_0x002f:
            if (r0 == 0) goto L_0x004c
            java.lang.String r1 = "Wooboo SDK 1.2"
            java.lang.String r1 = "Ad Image already exists"
            android.util.Log.i(r10, r1)
        L_0x0038:
            return r0
        L_0x0039:
            r0 = move-exception
            r2 = r8
        L_0x003b:
            r0.printStackTrace()     // Catch:{ all -> 0x0045 }
            if (r1 == 0) goto L_0x002e
            r1.close()
            r0 = r2
            goto L_0x002f
        L_0x0045:
            r0 = move-exception
        L_0x0046:
            if (r1 == 0) goto L_0x004b
            r1.close()
        L_0x004b:
            throw r0
        L_0x004c:
            java.lang.String r1 = "Wooboo SDK 1.2"
            java.lang.String r1 = "Ad Image does not exists"
            android.util.Log.i(r10, r1)
            goto L_0x0038
        L_0x0054:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x0046
        L_0x0059:
            r1 = move-exception
            r2 = r8
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x003b
        L_0x005f:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x003b
        L_0x0064:
            r2 = r8
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wooboo.adlib_android.i.a(java.lang.String):boolean");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0045, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x004e, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0052, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0053, code lost:
        r9 = r1;
        r1 = r0;
        r0 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0059, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x005a, code lost:
        r2 = null;
        r9 = r0;
        r0 = r1;
        r1 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        return r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0052 A[ExcHandler: all (r1v7 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:4:0x0008] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final byte[] b(java.lang.String r11) {
        /*
            r10 = this;
            r8 = 0
            android.database.sqlite.SQLiteDatabase r0 = r10.getWritableDatabase()     // Catch:{ Exception -> 0x003d, all -> 0x004a }
            java.lang.String r1 = "woobooad"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0059, all -> 0x0052 }
            r3 = 0
            java.lang.String r4 = "imgData"
            r2[r3] = r4     // Catch:{ Exception -> 0x0059, all -> 0x0052 }
            java.lang.String r3 = "imgName=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0059, all -> 0x0052 }
            r5 = 0
            r4[r5] = r11     // Catch:{ Exception -> 0x0059, all -> 0x0052 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0059, all -> 0x0052 }
            if (r1 == 0) goto L_0x0064
            int r2 = r1.getCount()     // Catch:{ Exception -> 0x0059, all -> 0x0052 }
            if (r2 == 0) goto L_0x0064
            boolean r2 = r1.moveToFirst()     // Catch:{ Exception -> 0x0059, all -> 0x0052 }
            if (r2 == 0) goto L_0x0064
            r2 = 0
            byte[] r2 = r1.getBlob(r2)     // Catch:{ Exception -> 0x0059, all -> 0x0052 }
        L_0x0031:
            if (r1 == 0) goto L_0x0036
            r1.close()     // Catch:{ Exception -> 0x005f, all -> 0x0052 }
        L_0x0036:
            if (r0 == 0) goto L_0x003b
            r0.close()
        L_0x003b:
            r0 = r2
        L_0x003c:
            return r0
        L_0x003d:
            r0 = move-exception
            r1 = r8
            r2 = r8
        L_0x0040:
            r0.printStackTrace()     // Catch:{ all -> 0x0057 }
            if (r1 == 0) goto L_0x003b
            r1.close()
            r0 = r2
            goto L_0x003c
        L_0x004a:
            r0 = move-exception
            r1 = r8
        L_0x004c:
            if (r1 == 0) goto L_0x0051
            r1.close()
        L_0x0051:
            throw r0
        L_0x0052:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x004c
        L_0x0057:
            r0 = move-exception
            goto L_0x004c
        L_0x0059:
            r1 = move-exception
            r2 = r8
            r9 = r0
            r0 = r1
            r1 = r9
            goto L_0x0040
        L_0x005f:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0040
        L_0x0064:
            r2 = r8
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wooboo.adlib_android.i.b(java.lang.String):byte[]");
    }
}
