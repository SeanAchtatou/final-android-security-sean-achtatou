package com.energysource.szj.android;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class ConnManager {
    public static final int NETWORK_TYPE_GPRS = 1;
    public static final int NETWORK_TYPE_NONE = 0;
    public static final int NETWORK_TYPE_WIFI = 2;
    private static final String TAG = "ConnManager";
    private static final ConnManager sConnManager = new ConnManager();
    private OnConnStateChangedListener mListener;

    public interface OnConnStateChangedListener {
        void onDisconnected(NetworkInfo networkInfo);

        void onGprsConnected(NetworkInfo networkInfo);

        void onWifiConnected(NetworkInfo networkInfo);
    }

    private ConnManager() {
    }

    public static ConnManager getInstance() {
        return sConnManager;
    }

    public boolean isConnecting(Context context) {
        return getCurrentNetworkType(context) != 0;
    }

    public boolean isGprsConnected(Context context) {
        return getCurrentNetworkType(context) == 1;
    }

    public boolean isWifiConnected(Context context) {
        return getCurrentNetworkType(context) == 2;
    }

    public int getCurrentNetworkType(Context context) {
        return getNetworkType(((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo());
    }

    private int getNetworkType(NetworkInfo info) {
        if (info == null) {
            return 0;
        }
        NetworkInfo.State state = info.getState();
        if (state != NetworkInfo.State.CONNECTED && state != NetworkInfo.State.CONNECTING) {
            return 0;
        }
        if (info.getType() == 1) {
            return 2;
        }
        return 1;
    }

    public void handleConnStateChanged(NetworkInfo info, Context context) {
        Log.v(TAG, info.toString());
        OnConnStateChangedListener listener = this.mListener;
        if (listener == null) {
            context.startService(new Intent("SZJService"));
            return;
        }
        switch (getNetworkType(info)) {
            case 0:
                Log.w(TAG, "Lost connection!");
                if (listener != null) {
                    listener.onDisconnected(info);
                    return;
                }
                return;
            case 1:
                Log.i(TAG, "GPRS is connecting...");
                if (listener != null) {
                    listener.onGprsConnected(info);
                    return;
                }
                return;
            case 2:
                Log.i(TAG, "Wi-Fi is connecting...");
                if (listener != null) {
                    listener.onWifiConnected(info);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void setOnConnStateChangedListener(OnConnStateChangedListener listener) {
        this.mListener = listener;
    }
}
