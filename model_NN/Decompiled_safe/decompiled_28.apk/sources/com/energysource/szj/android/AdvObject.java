package com.energysource.szj.android;

public class AdvObject {
    private int height;
    private String resContent;
    private String resTitle;
    private int showtype;
    private String tid;
    private long time;
    private String url;
    private int width;

    public AdvObject(String advUrl, long advTime, int advWidth, int advHeight, String advResTitle, String advResContent, String advTid, int advShowType) {
        this.url = advUrl;
        this.time = advTime;
        this.width = advWidth;
        this.height = advHeight;
        this.resTitle = advResTitle;
        this.resContent = advResContent;
        this.tid = advTid;
        this.showtype = advShowType;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(long time2) {
        this.time = time2;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width2) {
        this.width = width2;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height2) {
        this.height = height2;
    }

    public String getResTitle() {
        return this.resTitle;
    }

    public void setResTitle(String resTitle2) {
        this.resTitle = resTitle2;
    }

    public String getResContent() {
        return this.resContent;
    }

    public void setResContent(String resContent2) {
        this.resContent = resContent2;
    }

    public String getTid() {
        return this.tid;
    }

    public void setTid(String tid2) {
        this.tid = tid2;
    }

    public int getShowtype() {
        return this.showtype;
    }

    public void setShowtype(int showtype2) {
        this.showtype = showtype2;
    }
}
