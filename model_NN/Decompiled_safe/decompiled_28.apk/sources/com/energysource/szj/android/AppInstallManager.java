package com.energysource.szj.android;

import android.content.Context;
import android.content.Intent;

public class AppInstallManager {
    private static AppInstallManager instance = new AppInstallManager();
    private AppListener appListener;

    public interface AppListener {
        void installApp(String str);

        void unInstallApp(String str);
    }

    public static AppInstallManager getInstance() {
        return instance;
    }

    public void handlerListener(Intent intent, Context context) {
        String pkg = intent.getDataString();
        if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
            if (this.appListener == null) {
                context.startService(new Intent("SZJService"));
            } else {
                this.appListener.installApp(pkg);
            }
        } else if (!intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {
        } else {
            if (this.appListener == null) {
                context.startService(new Intent("SZJService"));
            } else {
                this.appListener.unInstallApp(pkg);
            }
        }
    }

    public void setOnAppListener(AppListener listener) {
        this.appListener = listener;
    }
}
