package com.energysource.szj.embeded;

import android.app.Activity;
import android.content.Context;
import com.energysource.szj.android.DebugListener;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class SZJServiceInstance {
    private static SZJServiceInstance szjServiceInstance = new SZJServiceInstance();
    private Activity activity;
    private String appsec;
    private Context context;
    private boolean debug = false;
    private DebugListener debugListener;
    private HashMap defaultSizeMap;
    private boolean destoryFlag = false;
    private Thread frameworkThread;
    private boolean framworkThreadFlag = false;
    private boolean loadClassFlag = false;
    private ModuleEntity me;
    private ConcurrentHashMap modulesMap;
    private String oldVersion;
    private int phoneHeight;
    private int phoneWidht;
    private boolean startLoadFlag = false;

    public DebugListener getDebugListener() {
        return this.debugListener;
    }

    public void setDebugListener(DebugListener debugListener2) {
        this.debugListener = debugListener2;
    }

    private SZJServiceInstance() {
    }

    public boolean isDestoryFlag() {
        return this.destoryFlag;
    }

    public void setDestoryFlag(boolean destoryFlag2) {
        this.destoryFlag = destoryFlag2;
    }

    public boolean isDebug() {
        return this.debug;
    }

    public void setDebug(boolean debug2) {
        this.debug = debug2;
    }

    public Activity getActivity() {
        return this.activity;
    }

    public void setActivity(Activity activity2) {
        this.activity = activity2;
    }

    public boolean isStartLoadFlag() {
        return this.startLoadFlag;
    }

    public void setStartLoadFlag(boolean startLoadFlag2) {
        this.startLoadFlag = startLoadFlag2;
        if (!startLoadFlag2) {
            AdManager.clearMemory();
        }
    }

    public String getAppsec() {
        return this.appsec;
    }

    public void setAppsec(String appsec2) {
        this.appsec = appsec2;
    }

    public ModuleEntity getMe() {
        return this.me;
    }

    public void setMe(ModuleEntity me2) {
        this.me = me2;
    }

    public boolean isLoadClassFlag() {
        return this.loadClassFlag;
    }

    public void setLoadClassFlag(boolean loadClassFlag2) {
        this.loadClassFlag = loadClassFlag2;
    }

    public String getOldVersion() {
        return this.oldVersion;
    }

    public void setOldVersion(String oldVersion2) {
        this.oldVersion = oldVersion2;
    }

    public int getPhoneWidht() {
        return this.phoneWidht;
    }

    public void setPhoneWidht(int phoneWidht2) {
        this.phoneWidht = phoneWidht2;
    }

    public int getPhoneHeight() {
        return this.phoneHeight;
    }

    public void setPhoneHeight(int phoneHeight2) {
        this.phoneHeight = phoneHeight2;
    }

    public boolean isFramworkThreadFlag() {
        return this.framworkThreadFlag;
    }

    public void setFramworkThreadFlag(boolean framworkThreadFlag2) {
        this.framworkThreadFlag = framworkThreadFlag2;
    }

    public Thread getFrameworkThread() {
        return this.frameworkThread;
    }

    public void setFrameworkThread(Thread frameworkThread2) {
        this.frameworkThread = frameworkThread2;
    }

    public Context getContext() {
        return this.context;
    }

    public void setContext(Context context2) {
        this.context = context2;
    }

    public ConcurrentHashMap getModulesMap() {
        return this.modulesMap;
    }

    public void setModulesMap(ConcurrentHashMap modulesMap2) {
        this.modulesMap = modulesMap2;
    }

    public static SZJServiceInstance getInstance() {
        return szjServiceInstance;
    }
}
