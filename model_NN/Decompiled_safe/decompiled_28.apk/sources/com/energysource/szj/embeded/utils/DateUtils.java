package com.energysource.szj.embeded.utils;

import android.util.Log;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {
    static String TAG = "DateUtils.java";

    public static Date formateStringToDate(String format, String date) {
        try {
            return new SimpleDateFormat(format).parse(date);
        } catch (Exception e) {
            Log.e(TAG, "formateStringToDateException:", e);
            return new Date();
        }
    }

    /* JADX INFO: Multiple debug info for r0v1 int: [D('cal' java.util.GregorianCalendar), D('iSecond' int)] */
    /* JADX INFO: Multiple debug info for r4v2 java.lang.String: [D('iYear' int), D('sYear' java.lang.String)] */
    public static String DateToString(Date date, String dateDev, int dateStyle, int timeStyle) {
        String result;
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        int iYear = cal.get(1);
        int iMonth = cal.get(2) + 1;
        int iDate = cal.get(5);
        int iHour = cal.get(11);
        int iMinute = cal.get(12);
        int iSecond = cal.get(13);
        String sYear = "" + iYear;
        switch (dateStyle) {
            case 0:
                result = "";
                break;
            case 1:
                result = sYear + dateDev + addZero(iMonth) + dateDev + addZero(iDate);
                break;
            case 2:
                result = addZero(iMonth) + dateDev + addZero(iDate) + dateDev + sYear;
                break;
            case 3:
                result = addZero(iMonth) + dateDev + addZero(iDate);
                break;
            default:
                result = sYear + dateDev + addZero(iMonth) + dateDev + addZero(iDate);
                break;
        }
        switch (timeStyle) {
            case 0:
            default:
                return result;
            case 1:
                return result + XmlConstant.SINGLE_SPACE + addZero(iHour) + ":" + addZero(iMinute) + ":" + addZero(iSecond);
            case 2:
                return result + XmlConstant.SINGLE_SPACE + addZero(iHour) + ":" + addZero(iMinute);
            case 3:
                return result + "_" + addZero(iHour) + addZero(iMinute) + addZero(iSecond);
        }
    }

    public static String addZero(int number) {
        if (number < 10) {
            return "0" + number;
        }
        return "" + number;
    }

    public static String getDateNow() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
    }

    public static String getDatebyLong(long cur) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(cur));
    }

    public static long getDistanceDays(String str1, String str2) throws Exception {
        long diff;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date one = df.parse(str1);
            Date two = df.parse(str2);
            long time1 = one.getTime();
            long time2 = two.getTime();
            if (time1 < time2) {
                diff = time2 - time1;
            } else {
                diff = time1 - time2;
            }
            return diff / 86400000;
        } catch (ParseException e) {
            Log.e(TAG, "getDistanceDays:", e);
            return 0;
        }
    }

    /* JADX INFO: Multiple debug info for r19v8 java.util.Date: [D('one' java.util.Date), D('str1' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r4v3 java.util.Date: [D('df' java.text.DateFormat), D('two' java.util.Date)] */
    /* JADX INFO: Multiple debug info for r19v9 long: [D('time1' long), D('one' java.util.Date)] */
    /* JADX INFO: Multiple debug info for r4v4 long: [D('two' java.util.Date), D('time2' long)] */
    /* JADX INFO: Multiple debug info for r19v10 long: [D('time1' long), D('diff' long)] */
    /* JADX INFO: Multiple debug info for r19v13 long: [D('time1' long), D('diff' long)] */
    public static long[] getDistanceTimes(String str1, String str2) {
        long day;
        long min;
        long hour;
        ParseException e;
        long min2;
        long hour2;
        long sec;
        long time2;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date one = simpleDateFormat.parse(str1);
            Date two = simpleDateFormat.parse(str2);
            long time1 = one.getTime();
            long time22 = two.getTime();
            if (time1 < time22) {
                time2 = time22 - time1;
            } else {
                time2 = time1 - time22;
            }
            day = time2 / 86400000;
            try {
                hour2 = (time2 / 3600000) - (24 * day);
                try {
                    long min3 = ((time2 / 60000) - ((24 * day) * 60)) - (60 * hour2);
                    try {
                        long sec2 = (((time2 / 1000) - (((24 * day) * 60) * 60)) - ((60 * hour2) * 60)) - (60 * min3);
                        sec = min3;
                        min2 = sec2;
                    } catch (ParseException e2) {
                        min = min3;
                        hour = hour2;
                        e = e2;
                        e.printStackTrace();
                        min2 = 0;
                        hour2 = hour;
                        sec = min;
                        return new long[]{day, hour2, sec, min2};
                    }
                } catch (ParseException e3) {
                    min = 0;
                    hour = hour2;
                    e = e3;
                    e.printStackTrace();
                    min2 = 0;
                    hour2 = hour;
                    sec = min;
                    return new long[]{day, hour2, sec, min2};
                }
            } catch (ParseException e4) {
                e = e4;
                hour = 0;
                min = 0;
                e.printStackTrace();
                min2 = 0;
                hour2 = hour;
                sec = min;
                return new long[]{day, hour2, sec, min2};
            }
        } catch (ParseException e5) {
            ParseException parseException = e5;
            day = 0;
            hour = 0;
            e = parseException;
            min = 0;
            e.printStackTrace();
            min2 = 0;
            hour2 = hour;
            sec = min;
            return new long[]{day, hour2, sec, min2};
        }
        return new long[]{day, hour2, sec, min2};
    }

    /* JADX INFO: Multiple debug info for r20v14 java.util.Date: [D('one' java.util.Date), D('str1' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v3 java.util.Date: [D('df' java.text.DateFormat), D('two' java.util.Date)] */
    /* JADX INFO: Multiple debug info for r20v15 long: [D('time1' long), D('one' java.util.Date)] */
    /* JADX INFO: Multiple debug info for r5v4 long: [D('two' java.util.Date), D('time2' long)] */
    /* JADX INFO: Multiple debug info for r20v16 long: [D('time1' long), D('diff' long)] */
    /* JADX INFO: Multiple debug info for r20v19 long: [D('time1' long), D('diff' long)] */
    public static String getDistanceTime(String str1, String str2) {
        long day;
        long min;
        long hour;
        ParseException e;
        long min2;
        long hour2;
        long sec;
        long time2;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date one = simpleDateFormat.parse(str1);
            Date two = simpleDateFormat.parse(str2);
            long time1 = one.getTime();
            long time22 = two.getTime();
            if (time1 < time22) {
                time2 = time22 - time1;
            } else {
                time2 = time1 - time22;
            }
            day = time2 / 86400000;
            try {
                hour2 = (time2 / 3600000) - (24 * day);
                try {
                    long min3 = ((time2 / 60000) - ((24 * day) * 60)) - (60 * hour2);
                    try {
                        long sec2 = (((time2 / 1000) - (((24 * day) * 60) * 60)) - ((60 * hour2) * 60)) - (60 * min3);
                        sec = min3;
                        min2 = sec2;
                    } catch (ParseException e2) {
                        min = min3;
                        hour = hour2;
                        e = e2;
                        Log.e(TAG, "getDistanceTime:", e);
                        min2 = 0;
                        hour2 = hour;
                        sec = min;
                        return day + "天" + hour2 + "小时" + sec + "分" + min2 + "秒";
                    }
                } catch (ParseException e3) {
                    min = 0;
                    hour = hour2;
                    e = e3;
                    Log.e(TAG, "getDistanceTime:", e);
                    min2 = 0;
                    hour2 = hour;
                    sec = min;
                    return day + "天" + hour2 + "小时" + sec + "分" + min2 + "秒";
                }
            } catch (ParseException e4) {
                e = e4;
                hour = 0;
                min = 0;
                Log.e(TAG, "getDistanceTime:", e);
                min2 = 0;
                hour2 = hour;
                sec = min;
                return day + "天" + hour2 + "小时" + sec + "分" + min2 + "秒";
            }
        } catch (ParseException e5) {
            ParseException parseException = e5;
            day = 0;
            hour = 0;
            e = parseException;
            min = 0;
            Log.e(TAG, "getDistanceTime:", e);
            min2 = 0;
            hour2 = hour;
            sec = min;
            return day + "天" + hour2 + "小时" + sec + "分" + min2 + "秒";
        }
        return day + "天" + hour2 + "小时" + sec + "分" + min2 + "秒";
    }
}
