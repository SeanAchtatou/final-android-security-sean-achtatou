package com.energysource.szj.embeded.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipUtil {
    public static void doExtract(File zipFile, File extractDir) throws IOException {
        ZipInputStream zipIn = null;
        try {
            ZipInputStream zipIn2 = new ZipInputStream(new FileInputStream(zipFile));
            try {
                byte[] buf = new byte[8196];
                while (true) {
                    ZipEntry ze = zipIn2.getNextEntry();
                    if (ze == null) {
                        break;
                    }
                    FileOutputStream zipOut = new FileOutputStream(new File(extractDir, ze.getName()));
                    while (true) {
                        int len = zipIn2.read(buf);
                        if (len == -1) {
                            break;
                        }
                        zipOut.write(buf, 0, len);
                    }
                    zipOut.close();
                    zipIn2.closeEntry();
                }
                if (zipIn2 != null) {
                    zipIn2.close();
                }
            } catch (Throwable th) {
                th = th;
                zipIn = zipIn2;
            }
        } catch (Throwable th2) {
            th = th2;
            if (zipIn != null) {
                zipIn.close();
            }
            throw th;
        }
    }

    public static void doExtract(InputStream zipFile, File extractDir) throws IOException {
        ZipInputStream zipIn = null;
        try {
            ZipInputStream zipIn2 = new ZipInputStream(zipFile);
            try {
                byte[] buf = new byte[8196];
                while (true) {
                    ZipEntry ze = zipIn2.getNextEntry();
                    if (ze == null) {
                        break;
                    }
                    FileOutputStream zipOut = new FileOutputStream(new File(extractDir, ze.getName()));
                    while (true) {
                        int len = zipIn2.read(buf);
                        if (len == -1) {
                            break;
                        }
                        zipOut.write(buf, 0, len);
                    }
                    zipOut.close();
                    zipIn2.closeEntry();
                }
                if (zipIn2 != null) {
                    zipIn2.close();
                }
            } catch (Throwable th) {
                th = th;
                zipIn = zipIn2;
            }
        } catch (Throwable th2) {
            th = th2;
            if (zipIn != null) {
                zipIn.close();
            }
            throw th;
        }
    }

    public static void copyFile(File sourceFile, File targetFile) throws Exception {
        FileInputStream in = new FileInputStream(sourceFile);
        FileOutputStream out = new FileOutputStream(targetFile);
        byte[] buffer = new byte[2097152];
        while (true) {
            int ins = in.read(buffer);
            if (ins == -1) {
                in.close();
                out.flush();
                out.close();
            } else {
                out.write(buffer, 0, ins);
            }
        }
    }
}
