package com.energysource.szj.embeded;

public class ModuleEntity {
    private String loadClassPath;
    private String name;
    private long size;
    private String verify;

    public String getLoadClassPath() {
        return this.loadClassPath;
    }

    public void setLoadClassPath(String loadClassPath2) {
        this.loadClassPath = loadClassPath2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public long getSize() {
        return this.size;
    }

    public void setSize(long size2) {
        this.size = size2;
    }

    public String getVerify() {
        return this.verify;
    }

    public void setVerify(String verify2) {
        this.verify = verify2;
    }
}
