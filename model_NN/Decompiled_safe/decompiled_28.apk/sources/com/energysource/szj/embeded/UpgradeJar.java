package com.energysource.szj.embeded;

import android.util.Log;
import com.energysource.szj.embeded.utils.FileUtils;
import java.io.File;

public class UpgradeJar {
    public void startUpgradeJar() {
        String pkg = SZJServiceInstance.getInstance().getContext().getPackageName();
        if (FileUtils.existsFile(new File("/data/data/" + pkg + "/tempZip/"))) {
            boolean flag = FileUtils.deleteDirectory("/data/data/" + pkg + "/bakjar/");
            Log.d("newZip", "flag===" + flag);
            if (flag) {
                renameFile("/data/data/" + pkg + "/loadjar/", "/data/data/" + pkg + "/bakjar/");
                renameFile("/data/data/" + pkg + "/tempZip/", "/data/data/" + pkg + "/loadjar/");
            }
        }
    }

    public void renameFile(String oldPath, String newPath) {
        File oldFile = new File(oldPath);
        File newFile = new File(newPath);
        if (!oldFile.exists() || newFile.exists() || oldFile.renameTo(newFile)) {
        }
    }
}
