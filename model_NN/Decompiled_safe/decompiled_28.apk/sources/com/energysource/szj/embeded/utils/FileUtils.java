package com.energysource.szj.embeded.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Xml;
import com.adchina.android.ads.CookieDB;
import com.energysource.szj.android.Log;
import com.energysource.szj.embeded.ModuleEntity;
import com.energysource.szj.embeded.SZJFrameworkConfig;
import com.energysource.szj.embeded.SZJServiceInstance;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import org.xmlpull.v1.XmlPullParser;

public class FileUtils {
    private static final String TAG = "FileUtils.java";

    public static boolean existsFile(File f) {
        if (f.exists()) {
            return true;
        }
        return false;
    }

    public static String jarEncoder(String str) {
        try {
            BufferedInputStream br = new BufferedInputStream(new FileInputStream(str));
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            byte[] buf = new byte[1024];
            for (int rcount = br.read(buf); rcount > 0; rcount = br.read(buf)) {
                messageDigest.update(buf);
            }
            byte[] byteArray = messageDigest.digest();
            StringBuffer md5StrBuff = new StringBuffer();
            for (int i = 0; i < byteArray.length; i++) {
                if (Integer.toHexString(byteArray[i] & 255).length() == 1) {
                    md5StrBuff.append("0").append(Integer.toHexString(byteArray[i] & 255));
                } else {
                    md5StrBuff.append(Integer.toHexString(byteArray[i] & 255));
                }
            }
            return md5StrBuff.toString();
        } catch (Exception e) {
            return "";
        }
    }

    public static ModuleEntity readXml(File f) {
        Exception e;
        ModuleEntity me = null;
        try {
            InputStream in = new FileInputStream(f);
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(in, "utf-8");
            SZJServiceInstance instance = SZJServiceInstance.getInstance();
            int event = parser.getEventType();
            while (true) {
                ModuleEntity me2 = me;
                if (event == 1) {
                    return me2;
                }
                switch (event) {
                    case 2:
                        try {
                            String tag = parser.getName();
                            if ("version".equals(tag)) {
                                instance.setOldVersion(parser.nextText());
                                me = me2;
                                break;
                            } else if ("module".equals(tag)) {
                                me = new ModuleEntity();
                                break;
                            } else if (CookieDB.KEY_NAME.equals(tag)) {
                                me2.setName(parser.nextText());
                                me = me2;
                                break;
                            } else if ("size".equals(tag)) {
                                me2.setSize(Long.parseLong(parser.nextText()));
                                me = me2;
                                break;
                            } else if ("verify".equals(tag)) {
                                me2.setVerify(parser.nextText());
                                me = me2;
                                break;
                            } else if ("loadpath".equals(tag)) {
                                me2.setLoadClassPath(parser.nextText());
                            }
                        } catch (Exception e2) {
                            e = e2;
                            me = me2;
                            Log.e(TAG, "readXmlException:", e);
                            return me;
                        }
                    default:
                        me = me2;
                        break;
                }
                event = parser.next();
            }
        } catch (Exception e3) {
            e = e3;
            Log.e(TAG, "readXmlException:", e);
            return me;
        }
    }

    public static ConcurrentHashMap readXmlByMap(File f) {
        Exception e;
        ConcurrentHashMap chm = new ConcurrentHashMap(5);
        List<ModuleEntity> list = new ArrayList<>();
        ModuleEntity me = null;
        try {
            InputStream in = new FileInputStream(f);
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(in, "utf-8");
            int event = parser.getEventType();
            while (true) {
                ModuleEntity me2 = me;
                if (event != 1) {
                    switch (event) {
                        case 2:
                            try {
                                String tag = parser.getName();
                                if ("version".equals(tag)) {
                                    me = me2;
                                    break;
                                } else if ("module".equals(tag)) {
                                    me = new ModuleEntity();
                                    list.add(me);
                                    break;
                                } else if (CookieDB.KEY_NAME.equals(tag)) {
                                    me2.setName(parser.nextText());
                                    me = me2;
                                    break;
                                } else if ("size".equals(tag)) {
                                    me2.setSize(Long.parseLong(parser.nextText()));
                                    me = me2;
                                    break;
                                } else if ("verify".equals(tag)) {
                                    me2.setVerify(parser.nextText());
                                }
                            } catch (Exception e2) {
                                e = e2;
                                Log.e(TAG, "readXmlException:", e);
                                return chm;
                            }
                        default:
                            me = me2;
                            break;
                    }
                    event = parser.next();
                } else {
                    for (ModuleEntity mo : list) {
                        chm.put(mo.getName(), mo);
                    }
                    return chm;
                }
            }
        } catch (Exception e3) {
            e = e3;
        }
    }

    public static boolean checkJarFile(String fileName, ModuleEntity me, String pkg) {
        boolean flag = false;
        try {
            String md5 = jarEncoder("/data/data/" + pkg + "/loadjar/" + fileName + SZJFrameworkConfig.POSTFIX);
            if (me == null || md5 == null) {
                Log.d(TAG, "me==null or md5==null");
                return false;
            }
            if (me.getVerify().equals(md5)) {
                flag = true;
            }
            return flag;
        } catch (Exception e) {
            Log.e(TAG, "checkJarFile", e);
            try {
                if (me.getVerify().equals(jarEncoder("/data/data/" + pkg + "/loadjar/" + fileName + SZJFrameworkConfig.POSTFIX))) {
                    flag = true;
                }
            } catch (Exception e2) {
                Log.e(TAG, "checkJarFile", e2);
            }
        }
    }

    public static Bitmap readImageResource(File f) {
        try {
            return BitmapFactory.decodeStream(new FileInputStream(f));
        } catch (Exception e) {
            Log.e(TAG, "readImageResourceException:", e);
            return null;
        }
    }

    public static boolean createDire(String str) {
        File file = new File(str);
        try {
            if (!file.exists()) {
                return file.mkdir();
            }
            return true;
        } catch (Exception e) {
            Log.e(TAG, "createDireException", e);
            return false;
        }
    }

    public static boolean deleteDirectory(String dir) {
        if (!dir.endsWith(File.separator)) {
            dir = dir + File.separator;
        }
        File dirFile = new File(dir);
        if (!dirFile.exists() || !dirFile.isDirectory()) {
            Log.d("newZip", "===don't exist==");
            return false;
        }
        boolean flag = true;
        File[] files = dirFile.listFiles();
        for (int i = 0; i < files.length; i++) {
            if (!files[i].isFile()) {
                flag = deleteDirectory(files[i].getAbsolutePath());
                if (!flag) {
                    break;
                }
            } else {
                flag = deleteFile(files[i].getAbsolutePath());
                if (!flag) {
                    break;
                }
            }
        }
        if (!flag) {
            return false;
        }
        return dirFile.delete();
    }

    public static boolean deleteFile(String fileName) {
        File file = new File(fileName);
        if (!file.isFile() || !file.exists()) {
            return false;
        }
        file.delete();
        return true;
    }
}
