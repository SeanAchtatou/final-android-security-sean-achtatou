package com.energysource.szj.embeded;

public class SZJFrameworkConfig {
    public static final String ACTIVITY = "activity";
    public static final String APPSEC = "appsec";
    public static String BAKJARPATH = "/data/data/com.energysource.szjframework.android/bakjar/";
    public static final String BOOTABLEMAPKEY = "bootablemodule";
    public static final String BOOTABLEMODULE = "bootableModule";
    public static final String CONFIGJARNAME = "moduleconfig";
    public static final String CONTEXT = "context";
    public static final String DEBUGFLAG = "debugFlag";
    public static final String DEBUGLISTENER = "debugListener";
    public static String JARPATH = "data/data/com.energysource.szjframework.android/loadjar/";
    public static String JAVAPATH = "data/data/com.energysource.szjframework.android/";
    public static final String MODULEXMLNAME = "module.xml";
    public static final String POSTFIX = ".jar";
    public static final String PROJECTTYPE = "projecttype";
    public static final int PROJECTTYPE_EMBEDED = 2;
    public static final int PROJECTTYPE_SERVICE = 1;
    public static final String WATCHERTYPE = "watchertype";
    public static final String WIFIMANAGER = "wifiManager";
    public static final String XMLNAME = "bootablemodule.xml";
    public static final String ZIPTEMPPATH = "/data/data/com.energysource.szjframework.android/tempZip/";
}
