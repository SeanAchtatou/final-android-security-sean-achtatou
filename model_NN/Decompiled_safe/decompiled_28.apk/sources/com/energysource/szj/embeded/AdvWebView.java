package com.energysource.szj.embeded;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Message;
import android.view.KeyEvent;
import android.webkit.HttpAuthHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import cn.domob.android.ads.DomobAdManager;
import com.energysource.szj.android.Log;
import com.energysource.szj.android.SZJModule;
import java.util.concurrent.ConcurrentHashMap;

public class AdvWebView extends WebView {
    static final String TAG = "AdvWebView";
    private boolean isLast;
    private boolean isTransparent;
    private int resHeight;
    private int resWidth;
    private int showType;
    private String tid;
    private long time;

    public AdvWebView(Context context) {
        super(context);
    }

    public boolean isLast() {
        return this.isLast;
    }

    public void setLast(boolean isLast2) {
        this.isLast = isLast2;
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(long time2) {
        this.time = time2;
    }

    public boolean isTransparent() {
        return this.isTransparent;
    }

    public void setTransparent(boolean isTransparent2) {
        this.isTransparent = isTransparent2;
    }

    public int getResWidth() {
        return this.resWidth;
    }

    public void setResWidth(int resWidth2) {
        this.resWidth = resWidth2;
    }

    public int getResHeight() {
        return this.resHeight;
    }

    public void setResHeight(int resHeight2) {
        this.resHeight = resHeight2;
    }

    public String getTid() {
        return this.tid;
    }

    public void setTid(String tid2) {
        this.tid = tid2;
    }

    public int getShowType() {
        return this.showType;
    }

    public void setShowType(int showType2) {
        this.showType = showType2;
    }

    static class MyWebViewClient extends WebViewClient {
        String resContent;
        String resTitle;
        int showtype;
        String tid;

        MyWebViewClient() {
        }

        public String getTid() {
            return this.tid;
        }

        public void setTid(String tid2) {
            this.tid = tid2;
        }

        public String getResTitle() {
            return this.resTitle;
        }

        public void setResTitle(String resTitle2) {
            this.resTitle = resTitle2;
        }

        public String getResContent() {
            return this.resContent;
        }

        public void setResContent(String resContent2) {
            this.resContent = resContent2;
        }

        public int getShowtype() {
            return this.showtype;
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            Log.e(AdvWebView.TAG, "=====什么情况：viewid:" + view.getId() + ",errorCode:" + errorCode + ",description：" + description + ",failingUrl:" + failingUrl);
        }

        public void onFormResubmission(WebView view, Message dontResend, Message resend) {
            super.onFormResubmission(view, dontResend, resend);
            Log.i(AdvWebView.TAG, "===检测：onFormResubmission");
        }

        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
            Log.i(AdvWebView.TAG, "===检测：onLoadResource");
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.i(AdvWebView.TAG, "===检测：onPageFinished");
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.i(AdvWebView.TAG, "===检测：onPageStarted");
        }

        public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
            super.onReceivedHttpAuthRequest(view, handler, host, realm);
            Log.i(AdvWebView.TAG, "===检测：onReceivedHttpAuthRequest");
        }

        public void onScaleChanged(WebView view, float oldScale, float newScale) {
            super.onScaleChanged(view, oldScale, newScale);
            Log.i(AdvWebView.TAG, "===检测：onScaleChanged");
        }

        public void onTooManyRedirects(WebView view, Message cancelMsg, Message continueMsg) {
            super.onTooManyRedirects(view, cancelMsg, continueMsg);
            Log.i(AdvWebView.TAG, "===检测：onTooManyRedirects");
        }

        public void onUnhandledKeyEvent(WebView view, KeyEvent event) {
            super.onUnhandledKeyEvent(view, event);
            Log.i(AdvWebView.TAG, "===检测：onUnhandledKeyEvent");
        }

        public void setShowtype(int showtype2) {
            this.showtype = showtype2;
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            try {
                Log.d(AdvWebView.TAG, "shouldOverrideUrlLoading========url:" + url);
                String[] urls = url.toLowerCase().split(":");
                if (urls.length >= 2) {
                    ConcurrentHashMap chm = SZJServiceInstance.getInstance().getModulesMap();
                    if (chm != null) {
                        ((SZJModule) chm.get(SZJFrameworkConfig.BOOTABLEMODULE)).saveClickOP(this.tid, this.showtype);
                    }
                    if ("cmd".equals(urls[0])) {
                        Log.d(AdvWebView.TAG, "shouldOverrideUrlLoading======cmd:" + url);
                        return true;
                    } else if ("tel".equals(urls[0])) {
                        Intent intent = new Intent("android.intent.action.DIAL", Uri.parse("tel:" + urls[1]));
                        intent.setFlags(268435456);
                        AdManager.AD_MANAGER.getContextFromActivity().startActivity(intent);
                        return true;
                    } else if ("smsto".equals(urls[0])) {
                        Intent intent2 = new Intent("android.intent.action.SENDTO", Uri.parse(url));
                        intent2.putExtra("sms_body", getResContent());
                        intent2.setFlags(268435456);
                        AdManager.AD_MANAGER.getContextFromActivity().startActivity(intent2);
                        return true;
                    } else if ("mailto".equals(urls[0])) {
                        Intent intent3 = new Intent("android.intent.action.SEND");
                        intent3.setType("text/plain");
                        intent3.putExtra("android.intent.extra.EMAIL", new String[]{urls[1]});
                        intent3.putExtra("android.intent.extra.SUBJECT", getResTitle());
                        intent3.putExtra("android.intent.extra.TEXT", getResContent());
                        intent3.setFlags(268435456);
                        AdManager.AD_MANAGER.getContextFromActivity().startActivity(intent3);
                        return true;
                    } else if ("geo".equals(urls[0])) {
                        String[] t = urls[1].split(",");
                        Intent intent4 = new Intent("android.intent.action.VIEW", Uri.parse("http://maps.google.com/maps?q=" + t[0] + "," + t[1]));
                        intent4.setFlags(268435456);
                        AdManager.AD_MANAGER.getContextFromActivity().startActivity(intent4);
                        return true;
                    } else if ("pkg".equals(urls[0])) {
                        if (url.endsWith("mp4")) {
                            Intent intent5 = new Intent("android.intent.action.VIEW", Uri.parse(urls[1] + ":" + urls[2] + ":" + urls[3]));
                            intent5.setDataAndType(Uri.parse(urls[1] + ":" + urls[2] + ":" + urls[3]), "video/*");
                            intent5.setFlags(268435456);
                            AdManager.AD_MANAGER.getContextFromActivity().startActivity(intent5);
                            Log.d(AdvWebView.TAG, "====shouldOverrideUrlLoading==pkg==下载的mp4:" + url);
                            return true;
                        }
                        Intent intent6 = new Intent("android.intent.action.VIEW", Uri.parse(url.replace("pkg:", "")));
                        intent6.setFlags(268435456);
                        AdManager.AD_MANAGER.getContextFromActivity().startActivity(intent6);
                        Log.d(AdvWebView.TAG, "====shouldOverrideUrlLoading==pkg==下载非mp4文件:" + url);
                        return true;
                    } else if (!DomobAdManager.ACTION_VIDEO.equals(urls[0]) || urls.length != 4) {
                        Uri uri = Uri.parse(url);
                        Log.d(AdvWebView.TAG, "====shouldOverrideUrlLoading==else:" + url);
                        Intent intent7 = new Intent("android.intent.action.VIEW", uri);
                        intent7.setFlags(268435456);
                        AdManager.AD_MANAGER.getContextFromActivity().startActivity(intent7);
                        return true;
                    } else {
                        Log.d(AdvWebView.TAG, "====shouldOverrideUrlLoading==video==在线播放的mp4:" + url);
                        Intent intent8 = new Intent("android.intent.action.VIEW", Uri.parse(urls[1] + ":" + urls[2] + ":" + urls[3]));
                        intent8.setFlags(268435456);
                        intent8.setDataAndType(Uri.parse(urls[1] + ":" + urls[2] + ":" + urls[3]), "video/*");
                        AdManager.AD_MANAGER.getContextFromActivity().startActivity(intent8);
                        return true;
                    }
                } else {
                    Log.d(AdvWebView.TAG, "===shouldOverrideUrlLoading==urls.length>=2===" + urls.length);
                    return true;
                }
            } catch (Exception e) {
                Log.e(AdvWebView.TAG, "shouldOverrideUrlLoading Exception:", e);
                return true;
            }
        }
    }
}
