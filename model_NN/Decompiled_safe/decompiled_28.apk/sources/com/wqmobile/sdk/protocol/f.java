package com.wqmobile.sdk.protocol;

import java.util.TimerTask;

final class f extends TimerTask {
    private /* synthetic */ WQSDKEngineThread a;

    f(WQSDKEngineThread wQSDKEngineThread) {
        this.a = wQSDKEngineThread;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wqmobile.sdk.protocol.WQSDKEngineThread.a(com.wqmobile.sdk.protocol.WQSDKEngineThread, boolean):void
     arg types: [com.wqmobile.sdk.protocol.WQSDKEngineThread, int]
     candidates:
      com.wqmobile.sdk.protocol.WQSDKEngineThread.a(com.wqmobile.sdk.protocol.WQSDKEngineThread, int):void
      com.wqmobile.sdk.protocol.WQSDKEngineThread.a(com.wqmobile.sdk.protocol.WQSDKEngineThread, android.os.Handler):void
      com.wqmobile.sdk.protocol.WQSDKEngineThread.a(com.wqmobile.sdk.protocol.WQSDKEngineThread, android.os.Message):void
      com.wqmobile.sdk.protocol.WQSDKEngineThread.a(com.wqmobile.sdk.protocol.WQSDKEngineThread, com.wqmobile.sdk.protocol.socket.IMySocket):void
      com.wqmobile.sdk.protocol.WQSDKEngineThread.a(com.wqmobile.sdk.protocol.WQSDKEngineThread, java.util.TimerTask):void
      com.wqmobile.sdk.protocol.WQSDKEngineThread.a(com.wqmobile.sdk.protocol.WQSDKEngineThread, byte[]):boolean
      com.wqmobile.sdk.protocol.WQSDKEngineThread.a(com.wqmobile.sdk.protocol.WQSDKEngineThread, boolean):void */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:19:0x0082=Splitter:B:19:0x0082, B:10:0x0033=Splitter:B:10:0x0033, B:28:0x00a3=Splitter:B:28:0x00a3} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r5 = this;
            r4 = 2
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x007d }
            com.wqmobile.sdk.protocol.WQHandler r0 = r0.h     // Catch:{ Exception -> 0x007d }
            boolean r0 = r0.isReceiveReply()     // Catch:{ Exception -> 0x007d }
            if (r0 != 0) goto L_0x00c4
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x007d }
            boolean r0 = r0.k     // Catch:{ Exception -> 0x007d }
            if (r0 != 0) goto L_0x0082
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x007d }
            com.wqmobile.sdk.protocol.WQHandler r0 = r0.h     // Catch:{ Exception -> 0x007d }
            int r0 = r0.iTryTimes     // Catch:{ Exception -> 0x007d }
            if (r0 != r4) goto L_0x0082
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x0075 }
            java.lang.String r1 = "udp"
            java.lang.String r2 = "adwaken.com"
            java.net.InetAddress r2 = java.net.InetAddress.getByName(r2)     // Catch:{ Exception -> 0x0075 }
            r3 = 62539(0xf44b, float:8.7636E-41)
            com.wqmobile.sdk.protocol.socket.IMySocket r1 = com.wqmobile.sdk.protocol.socket.WQSocketFactory.getSocket(r1, r2, r3)     // Catch:{ Exception -> 0x0075 }
            r0.i = r1     // Catch:{ Exception -> 0x0075 }
        L_0x0033:
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x007d }
            r1 = 1
            r0.k = r1     // Catch:{ Exception -> 0x007d }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x007d }
            com.wqmobile.sdk.protocol.WQHandler r0 = r0.h     // Catch:{ Exception -> 0x007d }
            r1 = 0
            r0.iTryTimes = r1     // Catch:{ Exception -> 0x007d }
        L_0x0042:
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x007d }
            com.wqmobile.sdk.protocol.WQHandler r0 = r0.h     // Catch:{ Exception -> 0x007d }
            int r0 = r0.iTryTimes     // Catch:{ Exception -> 0x007d }
            if (r0 >= r4) goto L_0x0074
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x007d }
            android.os.Handler r0 = r0.f     // Catch:{ Exception -> 0x007d }
            android.os.Message r0 = r0.obtainMessage()     // Catch:{ Exception -> 0x007d }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r1 = r5.a     // Catch:{ Exception -> 0x007d }
            android.os.Message r1 = r1.j     // Catch:{ Exception -> 0x007d }
            r0.copyFrom(r1)     // Catch:{ Exception -> 0x007d }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r1 = r5.a     // Catch:{ Exception -> 0x007d }
            android.os.Handler r1 = r1.f     // Catch:{ Exception -> 0x007d }
            r1.sendMessage(r0)     // Catch:{ Exception -> 0x007d }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x007d }
            com.wqmobile.sdk.protocol.WQHandler r0 = r0.h     // Catch:{ Exception -> 0x007d }
            int r1 = r0.iTryTimes     // Catch:{ Exception -> 0x007d }
            int r1 = r1 + 1
            r0.iTryTimes = r1     // Catch:{ Exception -> 0x007d }
        L_0x0074:
            return
        L_0x0075:
            r0 = move-exception
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x007d }
            r1 = 0
            r0.i = r1     // Catch:{ Exception -> 0x007d }
            goto L_0x0033
        L_0x007d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0074
        L_0x0082:
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x007d }
            boolean r0 = r0.k     // Catch:{ Exception -> 0x007d }
            if (r0 == 0) goto L_0x0042
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x007d }
            com.wqmobile.sdk.protocol.WQHandler r0 = r0.h     // Catch:{ Exception -> 0x007d }
            int r0 = r0.iTryTimes     // Catch:{ Exception -> 0x007d }
            if (r0 != r4) goto L_0x0042
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x007d }
            com.wqmobile.sdk.protocol.WQHandler r0 = r0.h     // Catch:{ Exception -> 0x007d }
            boolean r0 = r0.mNetworkInvalid     // Catch:{ Exception -> 0x007d }
            if (r0 != 0) goto L_0x0042
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x00bc }
            r0.a()     // Catch:{ Exception -> 0x00bc }
        L_0x00a3:
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x007d }
            com.wqmobile.sdk.protocol.WQHandler r0 = r0.h     // Catch:{ Exception -> 0x007d }
            r1 = 1
            r0.mNetworkInvalid = r1     // Catch:{ Exception -> 0x007d }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x007d }
            r1 = 0
            r0.k = r1     // Catch:{ Exception -> 0x007d }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x007d }
            com.wqmobile.sdk.protocol.WQHandler r0 = r0.h     // Catch:{ Exception -> 0x007d }
            r0.doFail()     // Catch:{ Exception -> 0x007d }
            goto L_0x0074
        L_0x00bc:
            r0 = move-exception
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x007d }
            r1 = 0
            r0.i = r1     // Catch:{ Exception -> 0x007d }
            goto L_0x00a3
        L_0x00c4:
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x007d }
            r0.initialState()     // Catch:{ Exception -> 0x007d }
            goto L_0x0074
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wqmobile.sdk.protocol.f.run():void");
    }
}
