package com.wqmobile.sdk.protocol.socket;

import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.Socket;

public class WQTcpSocketImp implements IMySocket {
    private static WQTcpSocketImp a;
    private Socket b = new Socket(this.c, this.d);
    private InetAddress c;
    private int d;

    private WQTcpSocketImp(InetAddress inetAddress, int i) {
        this.c = inetAddress;
        this.d = i;
    }

    public static IMySocket getInstance() {
        if (a == null) {
            return null;
        }
        return a;
    }

    public static IMySocket getInstance(InetAddress inetAddress, int i) {
        if (a == null) {
            a = new WQTcpSocketImp(inetAddress, i);
        }
        return a;
    }

    public void close() {
        if (this.b != null) {
            try {
                this.b.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        if (this.b != null) {
            try {
                this.b.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void recv(byte[] bArr) {
        int length = bArr.length;
        if (length > 0) {
            if (this.b != null) {
                new DatagramPacket(bArr, length);
                this.b.getInputStream().read(bArr);
                return;
            }
            throw new NullPointerException("Null socket.");
        }
    }

    public void send(byte[] bArr) {
        if (this.b != null) {
            OutputStream outputStream = this.b.getOutputStream();
            outputStream.write(bArr);
            outputStream.flush();
            return;
        }
        throw new NullPointerException("Null socket.");
    }
}
