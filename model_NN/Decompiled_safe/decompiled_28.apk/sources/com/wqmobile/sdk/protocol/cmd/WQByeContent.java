package com.wqmobile.sdk.protocol.cmd;

import com.wqmobile.sdk.protocol.WQHandler;
import com.wqmobile.sdk.protocol.cmd.WQGlobal;

public class WQByeContent extends WQCmdContentBase {
    private byte[] a = new byte[1];
    private byte[] b = new byte[2];
    private byte[] c = new byte[64];

    public WQByeContent() {
        this.m_GenCMDCode = WQGlobal.WQ_NET_CMD_CODE.enum_BYE.getByteValue();
        this.m_SplitCode = 63;
        initiParameterList();
    }

    public WQCmdContentBase doAction(WQHandler wQHandler) {
        return this;
    }

    public String getDetailMessage() {
        return WQGlobal.convertFromByte2String(this.c);
    }

    public byte[] getDetailStatusCode() {
        return this.b;
    }

    public byte getStatusCode() {
        return this.a[0];
    }

    /* access modifiers changed from: protected */
    public void initiParameterList() {
        this.m_ParameterList.add(this.a);
        this.m_ParameterList.add(this.b);
        this.m_ParameterList.add(this.c);
    }

    public void setDetailMessage(String str) {
        WQGlobal.copyString2Bytes(this.c, str);
    }

    public void setDetailStatusCode(byte[] bArr) {
        this.b[0] = bArr[0];
        if (bArr.length > 1) {
            this.b[1] = bArr[1];
        } else {
            this.b[1] = 0;
        }
    }

    public void setStatusCode(byte b2) {
        this.a[0] = b2;
    }
}
