package com.wqmobile.sdk.protocol.cmd;

import com.wqmobile.sdk.protocol.cmd.WQGlobal;

public class WQUnknownContent extends WQCmdContentBase {
    public WQUnknownContent() {
        this.m_GenCMDCode = WQGlobal.WQ_NET_CMD_CODE.enum_UNKNOW.getByteValue();
        this.m_SplitCode = 63;
        initiParameterList();
    }

    public int getTest() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void initiParameterList() {
    }
}
