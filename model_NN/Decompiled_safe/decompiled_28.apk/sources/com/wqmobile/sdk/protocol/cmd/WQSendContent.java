package com.wqmobile.sdk.protocol.cmd;

import com.wqmobile.sdk.protocol.WQHandler;
import com.wqmobile.sdk.protocol.cmd.WQGlobal;

public class WQSendContent extends WQCmdContentBase {
    private byte[] a = new byte[1];
    private byte[] b = new byte[2];
    private byte[] c = new byte[4];
    private byte[] d;

    public WQSendContent() {
        this.m_GenCMDCode = WQGlobal.WQ_NET_CMD_CODE.enum_SEND.getByteValue();
        this.m_SplitCode = 63;
        initiParameterList();
    }

    public boolean checkContentIntegrality() {
        return WQGlobal.isZeroByte(this.c) || WQGlobal.checkCRC(this.d, this.c);
    }

    public boolean checkFormat(byte[] bArr, int i, int i2) {
        return checkFormat(bArr, this.m_ParameterList.size() + 2, i, i2);
    }

    public WQCmdContentBase doAction(WQHandler wQHandler) {
        if (wQHandler.mCurrAdEntity.getExpectedSeq() != getFileSeqNum()) {
            return null;
        }
        wQHandler.receiveReply();
        int fileSeqNum = getFileSeqNum();
        if (checkContentIntegrality()) {
            synchronized (wQHandler.mCurrAdEntity) {
                wQHandler.mCurrAdEntity.addAdImageBuffer(this.d);
                fileSeqNum++;
            }
        }
        wQHandler.needReply();
        return WQCMDGenerator.GenRdrcv(fileSeqNum);
    }

    public long getCRCCode() {
        return WQGlobal.byteArray2LongCRC(this.c);
    }

    public byte[] getCmd() {
        int paramListCMDLength = super.getParamListCMDLength();
        byte[] cmd = super.getCmd();
        if (this.d != null) {
            WQGlobal.copyFromBytes2Bytes(cmd, paramListCMDLength, this.d, 0, this.d.length);
        }
        cmd[cmd.length - 1] = this.m_SplitCode;
        return cmd;
    }

    public byte[] getContent() {
        return this.d;
    }

    public int getContentSize() {
        return WQGlobal.byteArray2Int(this.b);
    }

    public String getContentString() {
        return WQGlobal.convertFromByte2String(this.d);
    }

    public int getFileSeqNum() {
        return this.a[0];
    }

    /* access modifiers changed from: protected */
    public int getParamListCMDLength() {
        int paramListCMDLength = super.getParamListCMDLength();
        if (this.d != null) {
            paramListCMDLength += this.d.length;
        }
        return paramListCMDLength + 1;
    }

    public int importCmd(byte[] bArr, int i, int i2) {
        int i3 = (i2 - i) + 1;
        int importCmd = super.importCmd(bArr, i, i2);
        if (importCmd == 0) {
            this.m_IsValid = false;
            return 0;
        }
        int i4 = i2;
        while (bArr[i4] != this.m_SplitCode && i4 > 0) {
            i4--;
        }
        if (importCmd >= i4 || i4 == 0) {
            this.m_IsValid = false;
            return 0;
        }
        int i5 = i4 - importCmd;
        this.d = new byte[i5];
        WQGlobal.copyFromBytes2Bytes(this.d, 0, bArr, importCmd, i5);
        return i3;
    }

    /* access modifiers changed from: protected */
    public void initiParameterList() {
        this.m_ParameterList.add(this.a);
        this.m_ParameterList.add(this.b);
        this.m_ParameterList.add(this.c);
    }

    public void setCRCCode(long j) {
        WQGlobal.copyFromBytes2Bytes(this.c, 0, WQGlobal.longCRC2ByteArray(j), 0, 4);
    }

    public void setContent(String str) {
        setContent(str.getBytes());
    }

    public void setContent(byte[] bArr) {
        int length = bArr.length;
        WQGlobal.copyFromBytes2Bytes(this.b, 0, WQGlobal.int2ByteArray(bArr.length, 2), 0, 2);
        this.d = new byte[length];
        WQGlobal.copyFromBytes2Bytes(this.d, 0, bArr, 0, length);
    }

    public void setFileSeqNum(int i) {
        this.a[0] = (byte) i;
    }
}
