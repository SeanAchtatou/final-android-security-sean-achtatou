package com.wqmobile.sdk.protocol;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.wqmobile.sdk.protocol.cmd.WQCMDGenerator;
import com.wqmobile.sdk.protocol.cmd.WQCmdContentBase;
import com.wqmobile.sdk.protocol.cmd.WQCommandContent;
import com.wqmobile.sdk.protocol.cmd.WQErrorContent;
import com.wqmobile.sdk.protocol.cmd.WQGlobal;
import com.wqmobile.sdk.protocol.cmd.WQTransferData;
import com.wqmobile.sdk.protocol.entity.WQAdEntity;
import java.util.Timer;

public class WQHandler {
    private static /* synthetic */ int[] j;
    public static byte[] lock = new byte[0];
    public static byte[] mAppSettingLock = new byte[0];
    private Handler a;
    /* access modifiers changed from: private */
    public WQCommunicator b;
    private Boolean c = false;
    private Boolean d = new Boolean(true);
    private String e = "";
    private String f = "";
    /* access modifiers changed from: private */
    public String g = "";
    private Timer h;
    private int i;
    public int iTryTimes;
    public WQAdEntity mCurrAdEntity = null;
    public Handler mGetAdHandler;
    public HANDLER_STATUS mHandlerStatus;
    public Handler mHelloHandler;
    public boolean mNetworkInvalid;
    public String mSrvAppSetting = "";
    public WQTransferData mTransferData = new WQTransferData();
    public Timer m_RetryTimer = null;
    public int m_TimerCount = -1;

    public enum HANDLER_STATUS {
        START,
        HELLO,
        CONNECTED,
        RP_ACK,
        APPSETTING,
        FIRST_AD,
        NEXT_AD,
        OFFLINE_AD_COUNTING,
        IDLE
    }

    public class WQGetAdArg {
        private WQGlobal.WQ_NET_CMD_CMD_CODE a;
        private String b;
        private int c;

        public WQGetAdArg(WQHandler wQHandler, WQGlobal.WQ_NET_CMD_CMD_CODE wq_net_cmd_cmd_code, String str, int i) {
            setClientProfile(str);
            setCode(wq_net_cmd_cmd_code);
            setCount(i);
        }

        public String getClientProfile() {
            return this.b;
        }

        public WQGlobal.WQ_NET_CMD_CMD_CODE getCode() {
            return this.a;
        }

        public int getCount() {
            return this.c;
        }

        public void setClientProfile(String str) {
            this.b = str;
        }

        public void setCode(WQGlobal.WQ_NET_CMD_CMD_CODE wq_net_cmd_cmd_code) {
            this.a = wq_net_cmd_cmd_code;
        }

        public void setCount(int i) {
            this.c = i;
        }
    }

    public class WQGetAdThread extends Thread {
        public WQGetAdThread(String str) {
            super(str);
        }

        public void run() {
            Looper.prepare();
            WQHandler.this.mGetAdHandler = new c(this);
            Looper.loop();
        }
    }

    public WQHandler() {
        new WQSDKEngineThread(this).start();
        this.mHelloHandler = null;
        this.mNetworkInvalid = false;
        this.i = 0;
        new WQGetAdThread("GetAdThread").start();
        this.mHandlerStatus = HANDLER_STATUS.START;
    }

    /* access modifiers changed from: private */
    public void a(WQCmdContentBase wQCmdContentBase) {
        a(wQCmdContentBase, WQGlobal.WQ_NET_COMM_TYPE.enum_SEND_ASYNC.getValue());
    }

    private void a(WQCmdContentBase wQCmdContentBase, int i2) {
        if (this.a != null) {
            WQTransferData wQTransferData = new WQTransferData();
            wQTransferData.copyInfo(this.mTransferData);
            wQTransferData.importCMDContent(wQCmdContentBase);
            Message obtainMessage = this.a.obtainMessage();
            obtainMessage.obj = wQTransferData.getCmd();
            obtainMessage.what = i2;
            this.a.sendMessage(obtainMessage);
        }
    }

    private boolean a() {
        if (this.mTransferData.getSessionID().length() > 0) {
            return true;
        }
        throw new Exception("Call startRequest before this method");
    }

    private static /* synthetic */ int[] b() {
        int[] iArr = j;
        if (iArr == null) {
            iArr = new int[HANDLER_STATUS.values().length];
            try {
                iArr[HANDLER_STATUS.APPSETTING.ordinal()] = 5;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[HANDLER_STATUS.CONNECTED.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[HANDLER_STATUS.FIRST_AD.ordinal()] = 6;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[HANDLER_STATUS.HELLO.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[HANDLER_STATUS.IDLE.ordinal()] = 9;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[HANDLER_STATUS.NEXT_AD.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[HANDLER_STATUS.OFFLINE_AD_COUNTING.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[HANDLER_STATUS.RP_ACK.ordinal()] = 4;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[HANDLER_STATUS.START.ordinal()] = 1;
            } catch (NoSuchFieldError e10) {
            }
            j = iArr;
        }
        return iArr;
    }

    public byte[] RP_CMD(byte[] bArr) {
        WQTransferData wQTransferData = new WQTransferData(bArr);
        if (!this.mTransferData.isSameConversation(wQTransferData)) {
            return null;
        }
        if (wQTransferData.getCMDCode() == WQGlobal.WQ_NET_CMD_CODE.enum_ERROR.getValue() && ((WQErrorContent) wQTransferData.getCmdContent()).getStatusCode() == 33) {
            this.mTransferData.setSessionID("");
            this.c = false;
        }
        WQCmdContentBase doAction = wQTransferData.getCmdContent().doAction(this);
        if (doAction == null) {
            return null;
        }
        wQTransferData.importCMDContent(doAction);
        return wQTransferData.getCmd();
    }

    public boolean SAY_HELLO() {
        this.mTransferData.setSessionID("");
        if (this.e.length() > 0) {
            return SAY_HELLO(this.e);
        }
        return false;
    }

    /*  JADX ERROR: UnsupportedOperationException in pass: MethodInvokeVisitor
        java.lang.UnsupportedOperationException: ArgType.getObject(), call class: class jadx.core.dex.instructions.args.ArgType$ArrayArg
        	at jadx.core.dex.instructions.args.ArgType.getObject(ArgType.java:513)
        	at jadx.core.clsp.ClspGraph.getClsDetails(ClspGraph.java:76)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:100)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean SAY_HELLO(java.lang.String r5) {
        /*
            r4 = this;
            r4.e = r5
            com.wqmobile.sdk.protocol.WQHandler$HANDLER_STATUS r0 = r4.mHandlerStatus
            com.wqmobile.sdk.protocol.WQHandler$HANDLER_STATUS r1 = com.wqmobile.sdk.protocol.WQHandler.HANDLER_STATUS.START
            if (r0 != r1) goto L_0x001b
            com.wqmobile.sdk.protocol.WQHandler$HANDLER_STATUS r0 = com.wqmobile.sdk.protocol.WQHandler.HANDLER_STATUS.HELLO
            r4.mHandlerStatus = r0
            com.wqmobile.sdk.protocol.cmd.WQHelloContent r0 = com.wqmobile.sdk.protocol.cmd.WQCMDGenerator.GenHello(r5)
            r4.needReply()
            com.wqmobile.sdk.protocol.cmd.WQTransferData r1 = r4.mTransferData
            r1.moveToNextConversation()
            r4.a(r0)
        L_0x001b:
            com.wqmobile.sdk.protocol.cmd.WQTransferData r0 = r4.mTransferData
            byte[] r0 = r0.m_SessionID
            monitor-enter(r0)
            com.wqmobile.sdk.protocol.cmd.WQTransferData r1 = r4.mTransferData     // Catch:{ all -> 0x003d }
            byte[] r1 = r1.m_SessionID     // Catch:{ all -> 0x003d }
            r2 = 25000(0x61a8, double:1.23516E-319)
            r1.wait(r2)     // Catch:{ all -> 0x003d }
            monitor-exit(r0)     // Catch:{ all -> 0x003d }
            r4.receiveReply()
            com.wqmobile.sdk.protocol.cmd.WQTransferData r0 = r4.mTransferData
            byte[] r0 = r0.m_SessionID
            boolean r0 = com.wqmobile.sdk.protocol.cmd.WQGlobal.isZeroByte(r0)
            if (r0 != 0) goto L_0x0040
            com.wqmobile.sdk.protocol.WQHandler$HANDLER_STATUS r0 = com.wqmobile.sdk.protocol.WQHandler.HANDLER_STATUS.CONNECTED
            r4.mHandlerStatus = r0
            r0 = 1
        L_0x003c:
            return r0
        L_0x003d:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x0040:
            r0 = 0
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wqmobile.sdk.protocol.WQHandler.SAY_HELLO(java.lang.String):boolean");
    }

    public void SetRefused(Boolean bool) {
        this.c = bool;
    }

    public Boolean WasRefused() {
        return this.c;
    }

    public void cancelRetryTimer() {
        if (this.m_RetryTimer != null) {
            this.m_RetryTimer.cancel();
            this.m_RetryTimer = null;
        }
    }

    public void doFail() {
        receiveReply();
        switch (b()[this.mHandlerStatus.ordinal()]) {
            case 2:
                this.mHandlerStatus = HANDLER_STATUS.START;
                return;
            case 3:
            case 4:
            default:
                return;
            case 5:
                this.mHandlerStatus = HANDLER_STATUS.CONNECTED;
                return;
            case 6:
            case 7:
            case 8:
                this.mHandlerStatus = HANDLER_STATUS.IDLE;
                return;
        }
    }

    public void getAd(WQGlobal.WQ_NET_CMD_CMD_CODE wq_net_cmd_cmd_code, String str, int i2) {
        a();
        if (this.mGetAdHandler != null) {
            Message obtainMessage = this.mGetAdHandler.obtainMessage();
            obtainMessage.obj = new WQGetAdArg(this, wq_net_cmd_cmd_code, str, i2);
            this.mGetAdHandler.sendMessage(obtainMessage);
        }
    }

    public void getFirstAd(String str, int i2) {
        getAd(WQGlobal.WQ_NET_CMD_CMD_CODE.enum_CMD_GET_AD, str, i2);
    }

    public void getNextAd(String str, int i2) {
        getAd(WQGlobal.WQ_NET_CMD_CMD_CODE.enum_CMD_GET_NEXT_AD, str, i2);
    }

    public void getOfflineAd(int i2) {
        getAd(WQGlobal.WQ_NET_CMD_CMD_CODE.enum_CMD_GET_OFFLINE_AD, "<?xml version=\"1.0\" encoding=\"utf-8\"?><ViewSettings><Width>320</Width><Height>48</Height><TopLeft-x>0</TopLeft-x><TopLeft-y>0</TopLeft-y></ViewSettings>", i2);
    }

    public boolean isReceiveReply() {
        return this.d.booleanValue();
    }

    public void needReply() {
        synchronized (this.d) {
            this.d = false;
        }
    }

    public void reSayHello() {
        if (this.mHelloHandler == null) {
            HandlerThread handlerThread = new HandlerThread("HelloThread");
            handlerThread.start();
            this.mHelloHandler = new b(this, handlerThread.getLooper());
        }
        this.mHelloHandler.sendMessage(this.mHelloHandler.obtainMessage());
    }

    public void receiveReply() {
        synchronized (this.d) {
            this.iTryTimes = 0;
            this.mNetworkInvalid = false;
            this.d = true;
            cancelRetryTimer();
        }
    }

    public String refreshAppSetting() {
        return this.f.length() > 0 ? refreshAppSetting(this.f) : "";
    }

    /*  JADX ERROR: UnsupportedOperationException in pass: MethodInvokeVisitor
        java.lang.UnsupportedOperationException: ArgType.getObject(), call class: class jadx.core.dex.instructions.args.ArgType$ArrayArg
        	at jadx.core.dex.instructions.args.ArgType.getObject(ArgType.java:513)
        	at jadx.core.clsp.ClspGraph.getClsDetails(ClspGraph.java:76)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:100)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public java.lang.String refreshAppSetting(java.lang.String r5) {
        /*
            r4 = this;
            r4.a()
            com.wqmobile.sdk.protocol.WQHandler$HANDLER_STATUS r0 = r4.mHandlerStatus
            com.wqmobile.sdk.protocol.WQHandler$HANDLER_STATUS r1 = com.wqmobile.sdk.protocol.WQHandler.HANDLER_STATUS.CONNECTED
            if (r0 != r1) goto L_0x003b
            com.wqmobile.sdk.protocol.WQHandler$HANDLER_STATUS r0 = com.wqmobile.sdk.protocol.WQHandler.HANDLER_STATUS.APPSETTING
            r4.mHandlerStatus = r0
            r4.f = r5
            com.wqmobile.sdk.protocol.cmd.WQGlobal$WQ_NET_CMD_CMD_CODE r0 = com.wqmobile.sdk.protocol.cmd.WQGlobal.WQ_NET_CMD_CMD_CODE.enum_CMD_CLIENT_APP_SETTING
            com.wqmobile.sdk.protocol.cmd.WQCommandContent r0 = com.wqmobile.sdk.protocol.cmd.WQCMDGenerator.GenCMD(r0, r5)
            r4.needReply()
            r4.a(r0)
            byte[] r0 = com.wqmobile.sdk.protocol.WQHandler.mAppSettingLock
            monitor-enter(r0)
            byte[] r1 = com.wqmobile.sdk.protocol.WQHandler.mAppSettingLock     // Catch:{ all -> 0x0038 }
            r2 = 25000(0x61a8, double:1.23516E-319)
            r1.wait(r2)     // Catch:{ all -> 0x0038 }
            monitor-exit(r0)     // Catch:{ all -> 0x0038 }
            r4.receiveReply()
            java.lang.String r0 = r4.mSrvAppSetting
            int r0 = r0.length()
            if (r0 <= 0) goto L_0x003b
            com.wqmobile.sdk.protocol.WQHandler$HANDLER_STATUS r0 = com.wqmobile.sdk.protocol.WQHandler.HANDLER_STATUS.IDLE
            r4.mHandlerStatus = r0
            java.lang.String r0 = r4.mSrvAppSetting
        L_0x0037:
            return r0
        L_0x0038:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x003b:
            java.lang.String r0 = ""
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wqmobile.sdk.protocol.WQHandler.refreshAppSetting(java.lang.String):java.lang.String");
    }

    public void sendOfflineADCounting(String str) {
        if (this.h != null) {
            this.h.cancel();
            this.h = null;
        }
        if (this.mHandlerStatus == HANDLER_STATUS.IDLE) {
            this.mHandlerStatus = HANDLER_STATUS.OFFLINE_AD_COUNTING;
            WQCommandContent GenCMD = WQCMDGenerator.GenCMD(WQGlobal.WQ_NET_CMD_CMD_CODE.enum_CMD_OFFLINE_AD_COUNT, str);
            needReply();
            a(GenCMD);
        } else if (this.i < 200) {
            this.h = new Timer();
            this.h.schedule(new a(this, str), 50000);
            this.i++;
        }
    }

    public void sendUserResponse(String str) {
        a(WQCMDGenerator.GenCMD(WQGlobal.WQ_NET_CMD_CMD_CODE.enum_CMD_USER_ACTION, str), WQGlobal.WQ_NET_COMM_TYPE.enum_SEND_NO_RECV.getValue());
    }

    public void setGetAdCallBack(WQCommunicator wQCommunicator) {
        this.b = wQCommunicator;
    }

    public void setRecvBuf(byte[] bArr) {
    }

    public void setSendHandler(Handler handler) {
        this.a = handler;
    }
}
