package com.wqmobile.sdk.protocol.cmd;

import com.wqmobile.sdk.protocol.cmd.WQGlobal;

public class WQCMDGenerator {
    public static WQAckEofContent GenAckEof(int i, int i2, long j) {
        WQAckEofContent wQAckEofContent = new WQAckEofContent();
        wQAckEofContent.setCRCCode(j);
        wQAckEofContent.setFileSeqNum(i);
        wQAckEofContent.setTotalPartNum(i2);
        return wQAckEofContent;
    }

    public static WQByeContent GenBye(int i, byte[] bArr, String str) {
        WQByeContent wQByeContent = new WQByeContent();
        wQByeContent.setStatusCode((byte) i);
        wQByeContent.setDetailStatusCode(bArr);
        wQByeContent.setDetailMessage(str);
        return wQByeContent;
    }

    public static WQCommandContent GenCMD(WQGlobal.WQ_NET_CMD_CMD_CODE wq_net_cmd_cmd_code, String str) {
        return GenCMD(wq_net_cmd_cmd_code, str.getBytes());
    }

    public static WQCommandContent GenCMD(WQGlobal.WQ_NET_CMD_CMD_CODE wq_net_cmd_cmd_code, byte[] bArr) {
        WQCommandContent wQCommandContent = new WQCommandContent();
        wQCommandContent.setCMDContentCode(wq_net_cmd_cmd_code);
        wQCommandContent.setContent(bArr);
        return wQCommandContent;
    }

    public static WQErrorContent GenErr(int i, byte[] bArr, String str) {
        WQErrorContent wQErrorContent = new WQErrorContent();
        wQErrorContent.setStatusCode((byte) i);
        wQErrorContent.setDetailStatusCode(bArr);
        wQErrorContent.setDetailMessage(str);
        return wQErrorContent;
    }

    public static WQHelloContent GenHello(String str) {
        WQHelloContent wQHelloContent = new WQHelloContent();
        wQHelloContent.setPublisherID(str);
        wQHelloContent.setVersionNum(1, 0);
        return wQHelloContent;
    }

    public static WQRDRCVContent GenRdrcv(int i) {
        WQRDRCVContent wQRDRCVContent = new WQRDRCVContent();
        wQRDRCVContent.setFileSeqNum(i);
        return wQRDRCVContent;
    }

    public static WQResendContent GenResend(int i) {
        WQResendContent wQResendContent = new WQResendContent();
        wQResendContent.setFileSeqNum(i);
        return wQResendContent;
    }

    public static WQUnknownContent GenUnknow() {
        return new WQUnknownContent();
    }
}
