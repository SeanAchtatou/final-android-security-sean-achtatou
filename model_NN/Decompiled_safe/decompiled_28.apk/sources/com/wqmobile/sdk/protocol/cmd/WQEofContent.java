package com.wqmobile.sdk.protocol.cmd;

import com.wqmobile.sdk.protocol.WQHandler;
import com.wqmobile.sdk.protocol.cmd.WQGlobal;
import com.wqmobile.sdk.protocol.entity.WQAdEntity;

public class WQEofContent extends WQCmdContentBase {
    private byte[] a = new byte[1];
    private byte[] b = new byte[2];
    private byte[] c = new byte[4];

    public WQEofContent() {
        this.m_GenCMDCode = WQGlobal.WQ_NET_CMD_CODE.enum_EOF.getByteValue();
        this.m_SplitCode = 63;
        initiParameterList();
    }

    public boolean checkContentIntegrality(WQAdEntity wQAdEntity) {
        return WQGlobal.isZeroByte(this.c) || WQGlobal.checkCRC(wQAdEntity.getCurImageBuffer(), 0, wQAdEntity.getCurImageLength(), this.c);
    }

    public WQCmdContentBase doAction(WQHandler wQHandler) {
        wQHandler.receiveReply();
        synchronized (wQHandler.mCurrAdEntity) {
            if (checkContentIntegrality(wQHandler.mCurrAdEntity)) {
                wQHandler.mCurrAdEntity.completeImage();
                wQHandler.mCurrAdEntity.setIsValid(true);
                wQHandler.mCurrAdEntity.notify();
                return null;
            }
            wQHandler.mCurrAdEntity.clearCurImageBuffer();
            wQHandler.needReply();
            WQRDRCVContent GenRdrcv = WQCMDGenerator.GenRdrcv(1);
            return GenRdrcv;
        }
    }

    public byte[] getCRCCode() {
        return this.c;
    }

    public int getFileSeqNum() {
        return this.a[0];
    }

    public int getTotalPartNum() {
        return WQGlobal.byteArray2Int(this.b);
    }

    /* access modifiers changed from: protected */
    public void initiParameterList() {
        this.m_ParameterList.add(this.a);
        this.m_ParameterList.add(this.b);
        this.m_ParameterList.add(this.c);
    }

    public void setCRCCode(long j) {
        WQGlobal.copyFromBytes2Bytes(this.c, 0, WQGlobal.longCRC2ByteArray(j), 0, 4);
    }

    public void setFileSeqNum(int i) {
        this.a[0] = (byte) i;
    }

    public void setTotalPartNum(int i) {
        WQGlobal.copyFromBytes2Bytes(this.b, 0, WQGlobal.int2ByteArray(i, 2), 0, 2);
    }
}
