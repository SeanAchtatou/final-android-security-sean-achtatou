package com.wqmobile.sdk.protocol.cmd;

import com.wqmobile.sdk.protocol.WQHandler;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

public abstract class WQCmdContentBase {
    protected static final int BYE_STATUS_MESSAGE_LENGTH = 64;
    protected static final int CMD_CODE_LENGTH = 1;
    protected static final byte CMD_CONTENT_SPLIT_CODE = 63;
    protected static final int CONTENT_LENGTH_LENGTH = 2;
    protected static final int CRC_CODE_LENGTH = 4;
    protected static final int DETAIL_STATUS_CODE_LENGTH = 2;
    protected static final int ERROR_STATUS_MESSAGE_LENGTH = 256;
    protected static final int FILE_SEQ_NUM_LENGTH = 1;
    protected static final byte GEN_CMD_SPLIT_CODE = 33;
    protected static final int HASH_CODE_LENGTH = 32;
    protected static final int PUBLISHER_ID_LENGTH = 32;
    protected static final int SESSION_ID_LENGTH = 32;
    protected static final int STATUS_CODE_LENGTH = 1;
    protected static final int TOTAL_PART_NUM_LENGTH = 2;
    protected static final int VARIABLE_CONTENT_LENGTH = 16;
    protected static final int VERSION_NUMBER_LENGTH = 2;
    protected byte m_GenCMDCode;
    protected boolean m_IsValid = true;
    protected List m_ParameterList = new LinkedList();
    protected byte m_SplitCode;
    protected WQTransferData m_TransferDate;

    WQCmdContentBase() {
    }

    public boolean checkFormat(byte[] bArr) {
        return checkFormat(bArr, this.m_ParameterList.size() + 1, 0, bArr.length - 1);
    }

    public boolean checkFormat(byte[] bArr, int i, int i2) {
        return checkFormat(bArr, this.m_ParameterList.size() + 1, i, i2);
    }

    /* access modifiers changed from: protected */
    public boolean checkFormat(byte[] bArr, int i, int i2, int i3) {
        if (bArr == null) {
            return false;
        }
        int i4 = (i3 - i2) + 1;
        if (i4 < 2 || bArr[i2] != this.m_SplitCode || bArr[i3] != this.m_SplitCode) {
            return false;
        }
        int i5 = 0;
        for (int i6 = i2; i6 < i2 + i4; i6++) {
            if (bArr[i6] == this.m_SplitCode) {
                i5++;
            }
            if (i5 == i) {
                break;
            }
        }
        return i <= i5;
    }

    public WQCmdContentBase doAction(WQHandler wQHandler) {
        return null;
    }

    public int getCMDCode() {
        return WQGlobal.converFromByte2UnsignInt(this.m_GenCMDCode);
    }

    public byte[] getCmd() {
        byte[] bArr = new byte[getParamListCMDLength()];
        int size = this.m_ParameterList.size();
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            int i3 = i + 1;
            bArr[i] = this.m_SplitCode;
            byte[] bArr2 = (byte[]) this.m_ParameterList.get(i2);
            int length = bArr2.length;
            WQGlobal.copyFromBytes2Bytes(bArr, i3, bArr2, 0, length);
            i = i3 + length;
        }
        bArr[i] = this.m_SplitCode;
        return bArr;
    }

    /* access modifiers changed from: protected */
    public int getParamListCMDLength() {
        int i = 0;
        int i2 = 0;
        while (true) {
            int i3 = i;
            if (i2 >= this.m_ParameterList.size()) {
                return this.m_ParameterList.size() + i3 + 1;
            }
            i = ((byte[]) this.m_ParameterList.get(i2)).length + i3;
            i2++;
        }
    }

    public int getTest() {
        return 0;
    }

    public int importCmd(byte[] bArr) {
        return importCmd(bArr, 0, bArr.length - 1);
    }

    public int importCmd(byte[] bArr, int i, int i2) {
        int i3 = (i2 - i) + 1;
        if (bArr == null || i3 <= 0 || i2 >= bArr.length || i < 0 || i2 <= 0) {
            this.m_IsValid = false;
            return 0;
        }
        int size = this.m_ParameterList.size();
        int i4 = 0;
        int i5 = i;
        while (i4 < size) {
            if (bArr[i5] != this.m_SplitCode) {
                this.m_IsValid = false;
                return 0;
            }
            byte[] bArr2 = (byte[]) this.m_ParameterList.get(i4);
            int length = bArr2.length;
            if (WQGlobal.copyFromBytes2Bytes(bArr2, 0, bArr, i5 + 1, length) == 0) {
                WQGlobal.setBytesZero(bArr2);
                this.m_IsValid = false;
                return 0;
            }
            i4++;
            i5 = i5 + length + 1;
        }
        int i6 = i5 + 1;
        if (i4 == size) {
            return i6;
        }
        this.m_IsValid = false;
        return 0;
    }

    /* access modifiers changed from: protected */
    public abstract void initiParameterList();

    /* access modifiers changed from: protected */
    public boolean isValidCMD() {
        return this.m_IsValid;
    }

    public void setTransferDate(WQTransferData wQTransferData) {
        this.m_TransferDate = wQTransferData;
    }

    public BigInteger testHash(String str) {
        return wqHashString(str);
    }

    /* access modifiers changed from: protected */
    public BigInteger wqHashString(String str) {
        byte[] bytes = str.getBytes();
        BigInteger bigInteger = new BigInteger("0");
        for (byte b : bytes) {
            bigInteger = bigInteger.multiply(BigInteger.valueOf(5)).add(BigInteger.valueOf((long) b));
        }
        return bigInteger;
    }
}
