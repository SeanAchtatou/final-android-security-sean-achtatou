package com.wqmobile.sdk.protocol;

import com.wqmobile.sdk.protocol.entity.WQAdEntity;
import java.util.Observable;

public class WQCommunicator extends Observable {
    private static WQCommunicator b;
    private WQHandler a = new WQHandler();

    private WQCommunicator() {
        this.a.setGetAdCallBack(this);
    }

    public static WQCommunicator getInstance() {
        if (b == null) {
            b = new WQCommunicator();
        }
        return b;
    }

    public Boolean WasRefused() {
        return this.a.WasRefused();
    }

    public void getAdComplete(WQAdEntity wQAdEntity) {
        setChanged();
        notifyObservers(wQAdEntity);
    }

    public void getFirstAds(String str, int i) {
        this.a.getFirstAd(str, i);
    }

    public void getNextAds(String str, int i) {
        this.a.getNextAd(str, i);
    }

    public void getOfflineAds(int i) {
        this.a.getOfflineAd(i);
    }

    public String refreshAppSetting(String str) {
        return this.a.refreshAppSetting(str);
    }

    public void sendOfflineADCounting(String str) {
        this.a.sendOfflineADCounting(str);
    }

    public void sendUserResponse(String str) {
        this.a.sendUserResponse(str);
    }

    public boolean startRequest(String str) {
        return this.a.SAY_HELLO(str);
    }
}
