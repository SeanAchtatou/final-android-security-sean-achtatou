package com.wqmobile.sdk.protocol.cmd;

import com.adview.util.AdViewUtil;
import java.util.Hashtable;
import java.util.zip.CRC32;

public final class WQGlobal {
    private static Hashtable a;
    /* access modifiers changed from: private */
    public static String b = "com.wqmobile.sdk.protocol.cmd.";

    public class WQConstant {
        public static final boolean CHECK_CRC = true;
        public static final boolean DEBUG = false;
        public static final int DEBUG_LOCAL = 1;
        public static final int ERROR_INVALID_PUBID_HASHCODE = 17;
        public static final int ERROR_INVALID_SESSION = 33;
        public static final int ERROR_SERVERBUSY = 1;
        public static final int GET_AD_TIME_OUT = 28000;
        public static final String LOG_TAG = "WQMobile";
        public static final int MAX_IMAGE_SIZE = 65000;
        public static final int MAX_OFFLINEAD_RETRY_TIMES = 200;
        public static final int M_MAINVERSION = 1;
        public static final int M_MINORVERSION = 0;
        public static final int OFFLINEAD_RETRY_TIMEOUT = 50000;
        public static final int RETRY_TIMEOUT = 3000;
        public static final int RETRY_TIMES = 2;
        public static final String SOCKET_TCP = "tcp";
        public static final String SOCKET_UDP = "udp";
        public static final String STR_RESPONSE_CLICK1 = "<responsetype>clicked</responsetype>";
        public static final String STR_RESPONSE_CLICK2 = "<responsetype>click</responsetype>";
        public static final int SYNCHRONIZED_WAIT_TIME = 25000;

        public WQConstant(WQGlobal wQGlobal) {
        }
    }

    public enum WQ_NET_CMD_CMD_CODE {
        enum_CMD_SERVER_APP_SETTING(1),
        enum_CMD_CLIENT_APP_SETTING(2),
        enum_CMD_CLIENT_DEVICE_PROFILE(3),
        enum_CMD_AD_PROPERTIES(4),
        enum_CMD_OFFLINE_AD_COUNT(5),
        enum_CMD_GET_AD(16),
        enum_CMD_GET_NEXT_AD(17),
        enum_CMD_GET_PREVIOUS_AD(18),
        enum_CMD_REFRESH_ADS(19),
        enum_CMD_GET_NEXT_PORTION(20),
        enum_CMD_GET_OFFLINE_AD(21),
        enum_CMD_USER_ACTION(32),
        enum_CMD_USER_VIEW(33),
        enum_CMD_USER_CLICK(34),
        enum_CMD_GET_PUBLISHING_STATUS(112),
        enum_CMD_GET_SERVER_STATUS(113),
        enum_CMD_GET_LAST_STATUS(114),
        enum_CMD_QUIT(128);
        
        private int a;

        private WQ_NET_CMD_CMD_CODE(int i) {
            this.a = i;
        }

        /* access modifiers changed from: package-private */
        public final byte a() {
            return (byte) this.a;
        }
    }

    public enum WQ_NET_CMD_CODE {
        enum_HELLO(1, String.valueOf(WQGlobal.b) + "WQHelloContent"),
        enum_ACK(254, String.valueOf(WQGlobal.b) + "WQAckContent"),
        enum_CMD(2, String.valueOf(WQGlobal.b) + "WQCommandContent"),
        enum_RDRCV(3, String.valueOf(WQGlobal.b) + "WQRDRCVContent"),
        enum_RDSEND(4, String.valueOf(WQGlobal.b) + "WQRDSendContent"),
        enum_SEND(16, String.valueOf(WQGlobal.b) + "WQSendContent"),
        enum_RESEND(17, String.valueOf(WQGlobal.b) + "WQResendContent"),
        enum_BYE(225, String.valueOf(WQGlobal.b) + "WQByeContent"),
        enum_EOF(18, String.valueOf(WQGlobal.b) + "WQEofContent"),
        enum_ACKEOF(29, String.valueOf(WQGlobal.b) + "WQAckEofContent"),
        enum_UNKNOW(AdViewUtil.VERSION, String.valueOf(WQGlobal.b) + "WQUnknownContent"),
        enum_ERROR(241, String.valueOf(WQGlobal.b) + "WQErrorContent");
        
        private int a;
        private String b;

        private WQ_NET_CMD_CODE(int i, String str) {
            this.a = i;
            this.b = str;
        }

        public final byte getByteValue() {
            return (byte) this.a;
        }

        public final String getCMDClassName() {
            return this.b;
        }

        public final int getValue() {
            return this.a;
        }
    }

    public enum WQ_NET_COMM_TYPE {
        enum_SEND_SYNC(1),
        enum_SEND_ASYNC(2),
        enum_SEND_NO_RECV(3);
        
        private int a;

        private WQ_NET_COMM_TYPE(int i) {
            this.a = i;
        }

        public final int getValue() {
            return this.a;
        }
    }

    public static int byteArray2Int(byte[] bArr) {
        if (bArr.length == 0) {
            return 0;
        }
        int length = bArr.length > 2 ? 2 : bArr.length;
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            i += (bArr[(bArr.length - 1) - i2] & 255) << (i2 * 8);
        }
        return i;
    }

    public static long byteArray2LongCRC(byte[] bArr) {
        int length = bArr.length > 4 ? 4 : bArr.length;
        long j = 0;
        for (int i = 0; i < length; i++) {
            j += ((long) (bArr[i] & 255)) << (i * 8);
        }
        return j;
    }

    public static boolean checkCRC(byte[] bArr, int i, int i2, byte[] bArr2) {
        return byteArray2LongCRC(bArr2) == getCRC32Value(bArr, i, i2);
    }

    public static boolean checkCRC(byte[] bArr, byte[] bArr2) {
        return checkCRC(bArr, 0, bArr.length, bArr2);
    }

    public static int converFromByte2UnsignInt(byte b2) {
        return b2 & 255;
    }

    public static String convertFromByte2String(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < bArr.length && bArr[i] != 0) {
            sb.append((char) bArr[i]);
            i++;
        }
        return sb.toString();
    }

    public static int copyFromBytes2Bytes(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        if (bArr == null || bArr2 == null || i3 <= 0 || i < 0 || i2 < 0) {
            return 0;
        }
        int length = bArr.length;
        int length2 = bArr2.length;
        if (length < i + i3 || length2 < i2 + i3) {
            return 0;
        }
        int i4 = 0;
        while (i4 < i3) {
            bArr[i + i4] = bArr2[i2 + i4];
            i4++;
        }
        return i4;
    }

    public static void copyNumber2Bytes(byte[] bArr, String str, boolean z) {
        if (z) {
            try {
                int length = (bArr.length << 1) > str.length() ? str.length() / 2 : bArr.length;
                for (int i = 0; i < length; i++) {
                    String substring = str.substring(i * 2, (i * 2) + 2);
                    if (substring.startsWith("0")) {
                        substring = substring.substring(1);
                    }
                    bArr[i] = Integer.valueOf(substring, 16).byteValue();
                }
            } catch (NumberFormatException e) {
            }
        } else {
            int length2 = bArr.length > str.length() ? str.length() : bArr.length;
            for (int i2 = 0; i2 < length2; i2++) {
                bArr[i2] = Byte.valueOf(str.substring(i2, i2 + 1)).byteValue();
            }
        }
    }

    public static int copyString2Bytes(byte[] bArr, String str) {
        if (bArr == null || str == null) {
            return 0;
        }
        byte[] bytes = str.getBytes();
        return copyFromBytes2Bytes(bArr, 0, bytes, 0, bArr.length > bytes.length ? bytes.length : bArr.length);
    }

    public static String getCMDNameByCode(byte b2) {
        int converFromByte2UnsignInt = converFromByte2UnsignInt(b2);
        if (a == null) {
            a = new Hashtable();
            for (WQ_NET_CMD_CODE wq_net_cmd_code : WQ_NET_CMD_CODE.values()) {
                a.put(String.valueOf(wq_net_cmd_code.getValue()), wq_net_cmd_code.getCMDClassName());
            }
        }
        String str = (String) a.get(String.valueOf(converFromByte2UnsignInt));
        return str == null ? "" : str;
    }

    public static long getCRC32Value(byte[] bArr) {
        return getCRC32Value(bArr, 0, bArr.length);
    }

    public static long getCRC32Value(byte[] bArr, int i, int i2) {
        CRC32 crc32 = new CRC32();
        crc32.update(bArr, i, i2);
        return crc32.getValue();
    }

    public static String getTest() {
        return "test";
    }

    public static byte[] int2ByteArray(int i, int i2) {
        int i3 = 2;
        if (i2 <= 0) {
            return new byte[0];
        }
        if (i2 <= 2) {
            i3 = i2;
        }
        byte[] bArr = new byte[i3];
        for (int i4 = 0; i4 < i3; i4++) {
            bArr[(i3 - 1) - i4] = (byte) (i >> (i4 * 8));
        }
        return bArr;
    }

    public static boolean isZeroByte(byte[] bArr) {
        for (byte b2 : bArr) {
            if (b2 > 0) {
                return false;
            }
        }
        return true;
    }

    public static byte[] longCRC2ByteArray(long j) {
        byte[] bArr = new byte[4];
        for (int i = 0; i < 4; i++) {
            bArr[i] = (byte) ((int) ((j >> (i * 8)) & 255));
        }
        return bArr;
    }

    public static void setBytesZero(byte[] bArr) {
        if (bArr != null && bArr.length != 0) {
            for (int i = 0; i < bArr.length; i++) {
                bArr[i] = 0;
            }
        }
    }
}
