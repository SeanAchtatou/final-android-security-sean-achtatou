package com.wqmobile.sdk.protocol.cmd;

import com.wqmobile.sdk.protocol.WQHandler;
import com.wqmobile.sdk.protocol.cmd.WQGlobal;

public class WQAckContent extends WQCmdContentBase {
    private byte[] a = new byte[1];
    private byte[] b = new byte[16];

    public WQAckContent() {
        this.m_GenCMDCode = WQGlobal.WQ_NET_CMD_CODE.enum_ACK.getByteValue();
        this.m_SplitCode = 63;
        initiParameterList();
    }

    public void SetVariableContent(int i) {
        WQGlobal.copyNumber2Bytes(this.b, String.valueOf(i), false);
    }

    public void SetVariableContent(String str) {
        WQGlobal.copyString2Bytes(this.b, str);
    }

    public WQCmdContentBase doAction(WQHandler wQHandler) {
        if (wQHandler.mHandlerStatus == WQHandler.HANDLER_STATUS.HELLO) {
            if (this.m_TransferDate.getSessionID().length() <= 0) {
                return null;
            }
            wQHandler.receiveReply();
            synchronized (wQHandler.mTransferData.m_SessionID) {
                wQHandler.mTransferData.copyInfo(this.m_TransferDate);
                wQHandler.mTransferData.m_SessionID.notify();
            }
            return null;
        } else if (wQHandler.mHandlerStatus != WQHandler.HANDLER_STATUS.OFFLINE_AD_COUNTING) {
            return null;
        } else {
            wQHandler.mHandlerStatus = WQHandler.HANDLER_STATUS.IDLE;
            wQHandler.receiveReply();
            return null;
        }
    }

    public byte getACKCode() {
        return this.a[0];
    }

    public byte[] getVariableContent() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void initiParameterList() {
        this.m_ParameterList.add(this.a);
        this.m_ParameterList.add(this.b);
    }

    public void setACKCode(WQGlobal.WQ_NET_CMD_CODE wq_net_cmd_code) {
        this.a[0] = wq_net_cmd_code.getByteValue();
    }
}
