package com.wqmobile.sdk.protocol.socket;

import com.wqmobile.sdk.protocol.cmd.WQGlobal;
import java.net.InetAddress;

public class WQSocketFactory {
    public static IMySocket getSocket(String str) {
        if (str.toLowerCase() == WQGlobal.WQConstant.SOCKET_UDP) {
            return WQUdpSocketImp.getInstance();
        }
        if (str.toLowerCase() == WQGlobal.WQConstant.SOCKET_TCP) {
            return WQTcpSocketImp.getInstance();
        }
        return null;
    }

    public static IMySocket getSocket(String str, InetAddress inetAddress, int i) {
        if (str.toLowerCase() == WQGlobal.WQConstant.SOCKET_UDP) {
            return WQUdpSocketImp.getInstance(inetAddress, i);
        }
        if (str.toLowerCase() == WQGlobal.WQConstant.SOCKET_TCP) {
            return WQTcpSocketImp.getInstance(inetAddress, i);
        }
        return null;
    }
}
