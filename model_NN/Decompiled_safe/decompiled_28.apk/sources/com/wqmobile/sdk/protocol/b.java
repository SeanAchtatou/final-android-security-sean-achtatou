package com.wqmobile.sdk.protocol;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.wqmobile.sdk.protocol.WQHandler;

final class b extends Handler {
    private /* synthetic */ WQHandler a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    b(WQHandler wQHandler, Looper looper) {
        super(looper);
        this.a = wQHandler;
    }

    public final void handleMessage(Message message) {
        boolean z = false;
        if (this.a.mHandlerStatus == WQHandler.HANDLER_STATUS.NEXT_AD || this.a.mHandlerStatus == WQHandler.HANDLER_STATUS.FIRST_AD) {
            z = true;
        }
        try {
            this.a.mHandlerStatus = WQHandler.HANDLER_STATUS.START;
            if (this.a.SAY_HELLO()) {
                this.a.refreshAppSetting();
                if (z) {
                    synchronized (this.a.mCurrAdEntity) {
                        this.a.mCurrAdEntity.setDump(true);
                        this.a.mCurrAdEntity.notify();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
