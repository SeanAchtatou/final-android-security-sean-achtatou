package com.wqmobile.sdk.protocol;

import android.os.Handler;
import com.wqmobile.sdk.protocol.WQHandler;

final class c extends Handler {
    private /* synthetic */ WQHandler.WQGetAdThread a;

    c(WQHandler.WQGetAdThread wQGetAdThread) {
        this.a = wQGetAdThread;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void handleMessage(android.os.Message r10) {
        /*
            r9 = this;
            r7 = 0
            com.wqmobile.sdk.protocol.WQHandler$WQGetAdThread r0 = r9.a     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler r0 = com.wqmobile.sdk.protocol.WQHandler.this     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler$HANDLER_STATUS r0 = r0.mHandlerStatus     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler$HANDLER_STATUS r1 = com.wqmobile.sdk.protocol.WQHandler.HANDLER_STATUS.IDLE     // Catch:{ Exception -> 0x0101 }
            if (r0 != r1) goto L_0x002d
            java.lang.Object r0 = r10.obj     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler$WQGetAdArg r0 = (com.wqmobile.sdk.protocol.WQHandler.WQGetAdArg) r0     // Catch:{ Exception -> 0x0101 }
            java.lang.String r1 = r0.getClientProfile()     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.cmd.WQGlobal$WQ_NET_CMD_CMD_CODE r2 = r0.getCode()     // Catch:{ Exception -> 0x0101 }
            int r0 = r0.getCount()     // Catch:{ Exception -> 0x0101 }
            r3 = r2
            r4 = r0
            r0 = r1
            r2 = r7
        L_0x0021:
            if (r4 > 0) goto L_0x002e
            com.wqmobile.sdk.protocol.WQHandler$WQGetAdThread r0 = r9.a     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler r0 = com.wqmobile.sdk.protocol.WQHandler.this     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler$HANDLER_STATUS r1 = com.wqmobile.sdk.protocol.WQHandler.HANDLER_STATUS.IDLE     // Catch:{ Exception -> 0x0101 }
            r0.mHandlerStatus = r1     // Catch:{ Exception -> 0x0101 }
        L_0x002d:
            return
        L_0x002e:
            com.wqmobile.sdk.protocol.WQHandler$WQGetAdThread r5 = r9.a     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler r5 = com.wqmobile.sdk.protocol.WQHandler.this     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler$HANDLER_STATUS r6 = com.wqmobile.sdk.protocol.WQHandler.HANDLER_STATUS.NEXT_AD     // Catch:{ Exception -> 0x0101 }
            r5.mHandlerStatus = r6     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler$WQGetAdThread r5 = r9.a     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler r5 = com.wqmobile.sdk.protocol.WQHandler.this     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.cmd.WQTransferData r5 = r5.mTransferData     // Catch:{ Exception -> 0x0101 }
            r5.moveToNextConversation()     // Catch:{ Exception -> 0x0101 }
            if (r2 == 0) goto L_0x00be
            com.wqmobile.sdk.protocol.cmd.WQGlobal$WQ_NET_CMD_CMD_CODE r5 = com.wqmobile.sdk.protocol.cmd.WQGlobal.WQ_NET_CMD_CMD_CODE.enum_CMD_GET_AD     // Catch:{ Exception -> 0x0101 }
            if (r3 != r5) goto L_0x00be
            com.wqmobile.sdk.protocol.cmd.WQGlobal$WQ_NET_CMD_CMD_CODE r2 = com.wqmobile.sdk.protocol.cmd.WQGlobal.WQ_NET_CMD_CMD_CODE.enum_CMD_GET_NEXT_AD     // Catch:{ Exception -> 0x0101 }
        L_0x004b:
            com.wqmobile.sdk.protocol.WQHandler$WQGetAdThread r5 = r9.a     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler r5 = com.wqmobile.sdk.protocol.WQHandler.this     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.entity.WQAdEntity r6 = new com.wqmobile.sdk.protocol.entity.WQAdEntity     // Catch:{ Exception -> 0x0101 }
            r6.<init>()     // Catch:{ Exception -> 0x0101 }
            r5.mCurrAdEntity = r6     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.cmd.WQCommandContent r0 = com.wqmobile.sdk.protocol.cmd.WQCMDGenerator.GenCMD(r2, r0)     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler$WQGetAdThread r2 = r9.a     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler r2 = com.wqmobile.sdk.protocol.WQHandler.this     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.entity.WQAdEntity r2 = r2.mCurrAdEntity     // Catch:{ Exception -> 0x0101 }
            monitor-enter(r2)     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler$WQGetAdThread r5 = r9.a     // Catch:{ all -> 0x00fe }
            com.wqmobile.sdk.protocol.WQHandler r5 = com.wqmobile.sdk.protocol.WQHandler.this     // Catch:{ all -> 0x00fe }
            r5.needReply()     // Catch:{ all -> 0x00fe }
            com.wqmobile.sdk.protocol.WQHandler$WQGetAdThread r5 = r9.a     // Catch:{ all -> 0x00fe }
            com.wqmobile.sdk.protocol.WQHandler r5 = com.wqmobile.sdk.protocol.WQHandler.this     // Catch:{ all -> 0x00fe }
            r5.a(r0)     // Catch:{ all -> 0x00fe }
            com.wqmobile.sdk.protocol.WQHandler$WQGetAdThread r0 = r9.a     // Catch:{ all -> 0x00fe }
            com.wqmobile.sdk.protocol.WQHandler r0 = com.wqmobile.sdk.protocol.WQHandler.this     // Catch:{ all -> 0x00fe }
            com.wqmobile.sdk.protocol.entity.WQAdEntity r0 = r0.mCurrAdEntity     // Catch:{ all -> 0x00fe }
            r5 = 28000(0x6d60, double:1.3834E-319)
            r0.wait(r5)     // Catch:{ all -> 0x00fe }
            com.wqmobile.sdk.protocol.WQHandler$WQGetAdThread r0 = r9.a     // Catch:{ all -> 0x00fe }
            com.wqmobile.sdk.protocol.WQHandler r0 = com.wqmobile.sdk.protocol.WQHandler.this     // Catch:{ all -> 0x00fe }
            com.wqmobile.sdk.protocol.entity.WQAdEntity r0 = r0.mCurrAdEntity     // Catch:{ all -> 0x00fe }
            boolean r0 = r0.isDump()     // Catch:{ all -> 0x00fe }
            if (r0 != 0) goto L_0x00ee
            com.wqmobile.sdk.protocol.WQHandler$WQGetAdThread r0 = r9.a     // Catch:{ all -> 0x00fe }
            com.wqmobile.sdk.protocol.WQHandler r0 = com.wqmobile.sdk.protocol.WQHandler.this     // Catch:{ all -> 0x00fe }
            com.wqmobile.sdk.protocol.WQCommunicator r0 = r0.b     // Catch:{ all -> 0x00fe }
            com.wqmobile.sdk.protocol.WQHandler$WQGetAdThread r5 = r9.a     // Catch:{ all -> 0x00fe }
            com.wqmobile.sdk.protocol.WQHandler r5 = com.wqmobile.sdk.protocol.WQHandler.this     // Catch:{ all -> 0x00fe }
            com.wqmobile.sdk.protocol.entity.WQAdEntity r5 = r5.mCurrAdEntity     // Catch:{ all -> 0x00fe }
            r0.getAdComplete(r5)     // Catch:{ all -> 0x00fe }
            int r0 = r4 + -1
            r4 = 1
            r5 = r0
            r0 = r1
            r8 = r3
            r3 = r4
            r4 = r8
        L_0x00af:
            com.wqmobile.sdk.protocol.WQHandler$WQGetAdThread r6 = r9.a     // Catch:{ all -> 0x00fe }
            com.wqmobile.sdk.protocol.WQHandler r6 = com.wqmobile.sdk.protocol.WQHandler.this     // Catch:{ all -> 0x00fe }
            r6.receiveReply()     // Catch:{ all -> 0x00fe }
            monitor-exit(r2)     // Catch:{ all -> 0x00fe }
            r2 = r3
            r3 = r4
            r4 = r5
            goto L_0x0021
        L_0x00be:
            if (r2 != 0) goto L_0x00eb
            com.wqmobile.sdk.protocol.cmd.WQGlobal$WQ_NET_CMD_CMD_CODE r2 = com.wqmobile.sdk.protocol.cmd.WQGlobal.WQ_NET_CMD_CMD_CODE.enum_CMD_GET_AD     // Catch:{ Exception -> 0x0101 }
            if (r3 != r2) goto L_0x00eb
            com.wqmobile.sdk.protocol.WQHandler$WQGetAdThread r2 = r9.a     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler r2 = com.wqmobile.sdk.protocol.WQHandler.this     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler$HANDLER_STATUS r5 = com.wqmobile.sdk.protocol.WQHandler.HANDLER_STATUS.FIRST_AD     // Catch:{ Exception -> 0x0101 }
            r2.mHandlerStatus = r5     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler$WQGetAdThread r2 = r9.a     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler r2 = com.wqmobile.sdk.protocol.WQHandler.this     // Catch:{ Exception -> 0x0101 }
            java.lang.String r2 = r2.g     // Catch:{ Exception -> 0x0101 }
            java.lang.String r2 = r2.trim()     // Catch:{ Exception -> 0x0101 }
            int r2 = r2.length()     // Catch:{ Exception -> 0x0101 }
            if (r2 != 0) goto L_0x00eb
            com.wqmobile.sdk.protocol.WQHandler$WQGetAdThread r2 = r9.a     // Catch:{ Exception -> 0x0101 }
            com.wqmobile.sdk.protocol.WQHandler r2 = com.wqmobile.sdk.protocol.WQHandler.this     // Catch:{ Exception -> 0x0101 }
            r2.g = r1     // Catch:{ Exception -> 0x0101 }
        L_0x00eb:
            r2 = r3
            goto L_0x004b
        L_0x00ee:
            com.wqmobile.sdk.protocol.WQHandler$WQGetAdThread r0 = r9.a     // Catch:{ all -> 0x00fe }
            com.wqmobile.sdk.protocol.WQHandler r0 = com.wqmobile.sdk.protocol.WQHandler.this     // Catch:{ all -> 0x00fe }
            java.lang.String r0 = r0.g     // Catch:{ all -> 0x00fe }
            com.wqmobile.sdk.protocol.cmd.WQGlobal$WQ_NET_CMD_CMD_CODE r3 = com.wqmobile.sdk.protocol.cmd.WQGlobal.WQ_NET_CMD_CMD_CODE.enum_CMD_GET_AD     // Catch:{ all -> 0x00fe }
            r5 = r4
            r4 = r3
            r3 = r7
            goto L_0x00af
        L_0x00fe:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ Exception -> 0x0101 }
            throw r0     // Catch:{ Exception -> 0x0101 }
        L_0x0101:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wqmobile.sdk.protocol.c.handleMessage(android.os.Message):void");
    }
}
