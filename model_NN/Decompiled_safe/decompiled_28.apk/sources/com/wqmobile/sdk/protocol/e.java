package com.wqmobile.sdk.protocol;

import android.os.Handler;
import com.wqmobile.sdk.protocol.WQSDKEngineThread;

final class e extends Handler {
    private /* synthetic */ WQSDKEngineThread.WQRecvThread a;

    e(WQSDKEngineThread.WQRecvThread wQRecvThread) {
        this.a = wQRecvThread;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void handleMessage(android.os.Message r4) {
        /*
            r3 = this;
            com.wqmobile.sdk.protocol.WQSDKEngineThread$WQRecvThread r0 = r3.a     // Catch:{ Exception -> 0x0077 }
            com.wqmobile.sdk.protocol.WQSDKEngineThread.WQRecvThread.a(r0)     // Catch:{ Exception -> 0x0077 }
            int r0 = r4.what     // Catch:{ Exception -> 0x0077 }
            com.wqmobile.sdk.protocol.cmd.WQGlobal$WQ_NET_COMM_TYPE r1 = com.wqmobile.sdk.protocol.cmd.WQGlobal.WQ_NET_COMM_TYPE.enum_SEND_ASYNC     // Catch:{ Exception -> 0x0077 }
            int r1 = r1.getValue()     // Catch:{ Exception -> 0x0077 }
            if (r0 != r1) goto L_0x0054
            com.wqmobile.sdk.protocol.WQSDKEngineThread$WQRecvThread r0 = r3.a     // Catch:{ Exception -> 0x0077 }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = com.wqmobile.sdk.protocol.WQSDKEngineThread.this     // Catch:{ Exception -> 0x0077 }
            com.wqmobile.sdk.protocol.WQHandler r0 = r0.h     // Catch:{ Exception -> 0x0077 }
            com.wqmobile.sdk.protocol.WQSDKEngineThread$WQRecvThread r1 = r3.a     // Catch:{ Exception -> 0x0077 }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r1 = com.wqmobile.sdk.protocol.WQSDKEngineThread.this     // Catch:{ Exception -> 0x0077 }
            byte[] r1 = r1.a     // Catch:{ Exception -> 0x0077 }
            byte[] r0 = r0.RP_CMD(r1)     // Catch:{ Exception -> 0x0077 }
            if (r0 == 0) goto L_0x0053
            r1 = 1
            byte r1 = r0[r1]     // Catch:{ Exception -> 0x0077 }
            com.wqmobile.sdk.protocol.cmd.WQGlobal$WQ_NET_CMD_CODE r2 = com.wqmobile.sdk.protocol.cmd.WQGlobal.WQ_NET_CMD_CODE.enum_UNKNOW     // Catch:{ Exception -> 0x0077 }
            int r2 = r2.getValue()     // Catch:{ Exception -> 0x0077 }
            if (r1 == r2) goto L_0x0053
            com.wqmobile.sdk.protocol.WQSDKEngineThread$WQRecvThread r1 = r3.a     // Catch:{ Exception -> 0x0077 }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r1 = com.wqmobile.sdk.protocol.WQSDKEngineThread.this     // Catch:{ Exception -> 0x0077 }
            android.os.Handler r1 = r1.f     // Catch:{ Exception -> 0x0077 }
            android.os.Message r1 = r1.obtainMessage()     // Catch:{ Exception -> 0x0077 }
            r1.obj = r0     // Catch:{ Exception -> 0x0077 }
            int r0 = r4.what     // Catch:{ Exception -> 0x0077 }
            r1.what = r0     // Catch:{ Exception -> 0x0077 }
            com.wqmobile.sdk.protocol.WQSDKEngineThread$WQRecvThread r0 = r3.a     // Catch:{ Exception -> 0x0077 }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = com.wqmobile.sdk.protocol.WQSDKEngineThread.this     // Catch:{ Exception -> 0x0077 }
            android.os.Handler r0 = r0.f     // Catch:{ Exception -> 0x0077 }
            r0.sendMessage(r1)     // Catch:{ Exception -> 0x0077 }
        L_0x0053:
            return
        L_0x0054:
            int r0 = r4.what     // Catch:{ Exception -> 0x0077 }
            com.wqmobile.sdk.protocol.cmd.WQGlobal$WQ_NET_COMM_TYPE r1 = com.wqmobile.sdk.protocol.cmd.WQGlobal.WQ_NET_COMM_TYPE.enum_SEND_SYNC     // Catch:{ Exception -> 0x0077 }
            int r1 = r1.getValue()     // Catch:{ Exception -> 0x0077 }
            if (r0 != r1) goto L_0x0053
            com.wqmobile.sdk.protocol.WQSDKEngineThread$WQRecvThread r0 = r3.a     // Catch:{ Exception -> 0x0077 }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = com.wqmobile.sdk.protocol.WQSDKEngineThread.this     // Catch:{ Exception -> 0x0077 }
            byte[] r0 = r0.b     // Catch:{ Exception -> 0x0077 }
            monitor-enter(r0)     // Catch:{ Exception -> 0x0077 }
            com.wqmobile.sdk.protocol.WQSDKEngineThread$WQRecvThread r1 = r3.a     // Catch:{ all -> 0x0074 }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r1 = com.wqmobile.sdk.protocol.WQSDKEngineThread.this     // Catch:{ all -> 0x0074 }
            byte[] r1 = r1.b     // Catch:{ all -> 0x0074 }
            r1.notify()     // Catch:{ all -> 0x0074 }
            monitor-exit(r0)     // Catch:{ all -> 0x0074 }
            goto L_0x0053
        L_0x0074:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ Exception -> 0x0077 }
            throw r1     // Catch:{ Exception -> 0x0077 }
        L_0x0077:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wqmobile.sdk.protocol.e.handleMessage(android.os.Message):void");
    }
}
