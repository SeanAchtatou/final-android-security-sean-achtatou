package com.wqmobile.sdk.protocol;

public final class R {

    public final class attr {
    }

    public final class layout {
        public static final int wqprotocol = 2130837504;
    }

    public final class string {
        public static final int wqprotocol_app_name = 2130903041;
        public static final int wqprotocolhello = 2130903040;
    }
}
