package com.wqmobile.sdk.protocol;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.wqmobile.sdk.protocol.cmd.WQGlobal;
import com.wqmobile.sdk.protocol.socket.IMySocket;
import com.wqmobile.sdk.protocol.socket.WQSocketFactory;
import java.net.InetAddress;
import java.util.TimerTask;

public class WQSDKEngineThread extends Thread {
    private static final byte[] c = {-64, -88, 1, -125};
    private static final byte[] d = {59, 41, 39, 93};
    private static final byte[] e = {-46, 21, 63, -77};
    byte[] a = new byte[65535];
    byte[] b = new byte[0];
    /* access modifiers changed from: private */
    public Handler f;
    /* access modifiers changed from: private */
    public Handler g;
    /* access modifiers changed from: private */
    public WQHandler h;
    /* access modifiers changed from: private */
    public IMySocket i;
    /* access modifiers changed from: private */
    public Message j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public TimerTask l;
    /* access modifiers changed from: private */
    public int m;

    public class WQRecvThread extends Thread {
        public WQRecvThread() {
        }

        static /* synthetic */ void a(WQRecvThread wQRecvThread) {
            WQGlobal.setBytesZero(WQSDKEngineThread.this.a);
            if (WQSDKEngineThread.this.i != null) {
                WQSDKEngineThread.this.i.recv(WQSDKEngineThread.this.a);
            }
        }

        public void run() {
            Looper.prepare();
            WQSDKEngineThread.this.g = new e(this);
            Looper.loop();
        }
    }

    static {
        byte[] bArr = {59, 41, 39, 19};
        byte[] bArr2 = {-46, 21, 63, -88};
    }

    public WQSDKEngineThread(WQHandler wQHandler) {
        this.h = wQHandler;
        this.k = false;
        this.m = 0;
        a();
        new WQRecvThread().start();
    }

    /* access modifiers changed from: private */
    public void a() {
        this.i = WQSocketFactory.getSocket(WQGlobal.WQConstant.SOCKET_UDP, InetAddress.getByName("adwaken.cn"), 23925);
    }

    static /* synthetic */ boolean a(WQSDKEngineThread wQSDKEngineThread, byte[] bArr) {
        if (wQSDKEngineThread.i == null) {
            return false;
        }
        wQSDKEngineThread.i.send(bArr);
        return true;
    }

    public void initialState() {
        this.h.iTryTimes = 0;
    }

    public void run() {
        Looper.prepare();
        this.f = new d(this);
        this.h.setSendHandler(this.f);
        Looper.loop();
    }
}
