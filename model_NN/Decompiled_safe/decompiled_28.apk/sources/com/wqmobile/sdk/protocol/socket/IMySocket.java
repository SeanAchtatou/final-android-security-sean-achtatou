package com.wqmobile.sdk.protocol.socket;

public interface IMySocket {
    void close();

    void recv(byte[] bArr);

    void send(byte[] bArr);
}
