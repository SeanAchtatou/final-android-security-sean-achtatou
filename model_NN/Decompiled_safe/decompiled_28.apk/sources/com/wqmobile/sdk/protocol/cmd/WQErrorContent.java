package com.wqmobile.sdk.protocol.cmd;

import com.wqmobile.sdk.protocol.WQHandler;
import com.wqmobile.sdk.protocol.cmd.WQGlobal;

public class WQErrorContent extends WQCmdContentBase {
    private byte[] a = new byte[1];
    private byte[] b = new byte[2];
    private byte[] c = new byte[256];

    public WQErrorContent() {
        this.m_GenCMDCode = WQGlobal.WQ_NET_CMD_CODE.enum_ERROR.getByteValue();
        this.m_SplitCode = 63;
        initiParameterList();
    }

    public WQCmdContentBase doAction(WQHandler wQHandler) {
        wQHandler.receiveReply();
        if (getStatusCode() != 33) {
            return null;
        }
        try {
            wQHandler.reSayHello();
        } catch (Exception e) {
        }
        return null;
    }

    public String getDetailMessage() {
        return WQGlobal.convertFromByte2String(this.c);
    }

    public byte[] getDetailStatusCode() {
        return this.b;
    }

    public byte getStatusCode() {
        return this.a[0];
    }

    /* access modifiers changed from: protected */
    public void initiParameterList() {
        this.m_ParameterList.add(this.a);
        this.m_ParameterList.add(this.b);
        this.m_ParameterList.add(this.c);
    }

    public void setDetailMessage(String str) {
        WQGlobal.copyString2Bytes(this.c, str);
    }

    public void setDetailStatusCode(byte[] bArr) {
        this.b[0] = bArr[0];
        if (bArr.length > 1) {
            this.b[1] = bArr[1];
        } else {
            this.b[1] = 0;
        }
    }

    public void setStatusCode(byte b2) {
        this.a[0] = b2;
    }
}
