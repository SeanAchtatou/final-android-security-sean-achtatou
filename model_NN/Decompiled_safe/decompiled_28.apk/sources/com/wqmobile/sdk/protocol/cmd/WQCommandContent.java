package com.wqmobile.sdk.protocol.cmd;

import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import com.wqmobile.sdk.protocol.WQHandler;
import com.wqmobile.sdk.protocol.cmd.WQGlobal;
import com.wqmobile.sdk.protocol.entity.WQAdEntity;
import java.io.UnsupportedEncodingException;

public class WQCommandContent extends WQCmdContentBase {
    private byte[] a = new byte[1];
    private byte[] b = new byte[2];
    private byte[] c = new byte[4];
    private byte[] d;

    public WQCommandContent() {
        this.m_GenCMDCode = WQGlobal.WQ_NET_CMD_CODE.enum_CMD.getByteValue();
        this.m_SplitCode = 63;
        initiParameterList();
    }

    private int a() {
        int i;
        String lowerCase = getContentString().toLowerCase();
        int indexOf = lowerCase.indexOf("<size>");
        int indexOf2 = lowerCase.indexOf("</size>");
        if (indexOf <= 0 || indexOf >= lowerCase.length() || indexOf2 <= indexOf) {
            i = 0;
        } else {
            try {
                i = Integer.parseInt(lowerCase.substring("<size>".length() + indexOf, indexOf2));
            } catch (Exception e) {
                i = 0;
            }
        }
        return i <= 0 ? WQGlobal.WQConstant.MAX_IMAGE_SIZE : i;
    }

    private void a(int i) {
        WQGlobal.copyFromBytes2Bytes(this.b, 0, WQGlobal.int2ByteArray(i, 2), 0, 2);
    }

    public boolean checkContentIntegrality() {
        return WQGlobal.checkCRC(this.d, this.c);
    }

    public boolean checkFormat(byte[] bArr, int i, int i2) {
        return checkFormat(bArr, this.m_ParameterList.size() + 2, i, i2);
    }

    public WQCmdContentBase doAction(WQHandler wQHandler) {
        if (getCMDContentCode() == WQGlobal.WQ_NET_CMD_CMD_CODE.enum_CMD_AD_PROPERTIES.a()) {
            if (wQHandler.mHandlerStatus == WQHandler.HANDLER_STATUS.FIRST_AD || wQHandler.mHandlerStatus == WQHandler.HANDLER_STATUS.NEXT_AD) {
                wQHandler.receiveReply();
                if (this.d.length > 0) {
                    synchronized (wQHandler.mCurrAdEntity) {
                        if (wQHandler.mCurrAdEntity == null) {
                            wQHandler.mCurrAdEntity = new WQAdEntity();
                        }
                        WQAdEntity wQAdEntity = wQHandler.mCurrAdEntity;
                        String contentString = getContentString();
                        String str = String.valueOf("File") + String.valueOf(1);
                        if (contentString.length() > 0) {
                            String str2 = contentString;
                            String str3 = str;
                            int i = 1;
                            while (str2.indexOf(str3) > 0) {
                                i++;
                                str2 = str2.replaceAll(str3, "File");
                                str3 = String.valueOf("File") + String.valueOf(i);
                            }
                            contentString = str2;
                        }
                        wQAdEntity.setAdProperty(contentString);
                        wQHandler.mCurrAdEntity.addAdImage(new byte[a()]);
                    }
                    wQHandler.needReply();
                    return WQCMDGenerator.GenRdrcv(1);
                }
                wQHandler.SetRefused(true);
                synchronized (wQHandler.mCurrAdEntity) {
                    if (wQHandler.mCurrAdEntity == null) {
                        wQHandler.mCurrAdEntity = new WQAdEntity();
                    }
                    wQHandler.mCurrAdEntity.setIsValid(false);
                    wQHandler.mCurrAdEntity.notify();
                }
                return null;
            }
        } else if (getCMDContentCode() == WQGlobal.WQ_NET_CMD_CMD_CODE.enum_CMD_SERVER_APP_SETTING.a() && wQHandler.mHandlerStatus == WQHandler.HANDLER_STATUS.APPSETTING) {
            wQHandler.receiveReply();
            synchronized (WQHandler.mAppSettingLock) {
                wQHandler.mSrvAppSetting = getContentString();
                WQHandler.mAppSettingLock.notifyAll();
            }
        }
        return null;
    }

    public byte getCMDContentCode() {
        return this.a[0];
    }

    public long getCRCCode() {
        return WQGlobal.byteArray2LongCRC(this.c);
    }

    public byte[] getCmd() {
        int paramListCMDLength = super.getParamListCMDLength();
        byte[] cmd = super.getCmd();
        if (this.d != null) {
            WQGlobal.copyFromBytes2Bytes(cmd, paramListCMDLength, this.d, 0, this.d.length);
        }
        cmd[cmd.length - 1] = this.m_SplitCode;
        return cmd;
    }

    public byte[] getContent() {
        return this.d;
    }

    public int getContentSize() {
        return WQGlobal.byteArray2Int(this.b);
    }

    public String getContentString() {
        try {
            return new String(this.d, XmlConstant.DEFAULT_ENCODING);
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    /* access modifiers changed from: protected */
    public int getParamListCMDLength() {
        int paramListCMDLength = super.getParamListCMDLength();
        if (this.d != null) {
            paramListCMDLength += this.d.length;
        }
        return paramListCMDLength + 1;
    }

    public int importCmd(byte[] bArr, int i, int i2) {
        int i3 = (i2 - i) + 1;
        int importCmd = super.importCmd(bArr, i, i2);
        if (importCmd == 0) {
            this.m_IsValid = false;
            return 0;
        }
        int i4 = i2;
        while (bArr[i4] != this.m_SplitCode && i4 > 0) {
            i4--;
        }
        if (importCmd > i4 || i4 == 0) {
            this.m_IsValid = false;
            return 0;
        }
        int i5 = i4 - importCmd;
        this.d = new byte[i5];
        WQGlobal.copyFromBytes2Bytes(this.d, 0, bArr, importCmd, i5);
        return i3;
    }

    /* access modifiers changed from: protected */
    public void initiParameterList() {
        this.m_ParameterList.add(this.a);
        this.m_ParameterList.add(this.b);
        this.m_ParameterList.add(this.c);
    }

    public void setCMDContentCode(WQGlobal.WQ_NET_CMD_CMD_CODE wq_net_cmd_cmd_code) {
        this.a[0] = wq_net_cmd_cmd_code.a();
    }

    public void setContent(String str) {
        setContent(str.getBytes());
    }

    public void setContent(byte[] bArr) {
        if (bArr != null) {
            int length = bArr.length;
            a(length);
            this.d = new byte[length];
            WQGlobal.copyFromBytes2Bytes(this.d, 0, bArr, 0, length);
            WQGlobal.copyFromBytes2Bytes(this.c, 0, WQGlobal.longCRC2ByteArray(WQGlobal.getCRC32Value(bArr)), 0, 4);
            return;
        }
        a(0);
    }
}
