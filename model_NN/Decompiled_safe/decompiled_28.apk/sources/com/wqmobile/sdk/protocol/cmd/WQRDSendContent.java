package com.wqmobile.sdk.protocol.cmd;

import com.wqmobile.sdk.protocol.WQHandler;
import com.wqmobile.sdk.protocol.cmd.WQGlobal;

public class WQRDSendContent extends WQCmdContentBase {
    private byte[] a = new byte[1];

    public WQRDSendContent() {
        this.m_GenCMDCode = WQGlobal.WQ_NET_CMD_CODE.enum_RDSEND.getByteValue();
        this.m_SplitCode = 63;
        initiParameterList();
    }

    public WQCmdContentBase doAction(WQHandler wQHandler) {
        return this;
    }

    public int getFileSeqNum() {
        return this.a[0];
    }

    /* access modifiers changed from: protected */
    public void initiParameterList() {
        this.m_ParameterList.add(this.a);
    }

    public void setFileSeqNum(int i) {
        this.a[0] = (byte) i;
    }
}
