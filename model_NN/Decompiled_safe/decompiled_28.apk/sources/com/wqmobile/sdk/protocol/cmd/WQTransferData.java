package com.wqmobile.sdk.protocol.cmd;

public class WQTransferData extends WQCmdContentBase {
    private byte[] a = new byte[1];
    private byte[] b = new byte[2];
    private WQCmdContentBase c;
    public byte[] m_SessionID = new byte[32];

    public WQTransferData() {
        this.m_SplitCode = 33;
        initiParameterList();
        setConversationID(0);
    }

    public WQTransferData(byte[] bArr) {
        this.m_SplitCode = 33;
        initiParameterList();
        importCmd(bArr);
    }

    public boolean checkFormat(byte[] bArr) {
        if (bArr.length == 0) {
            return false;
        }
        int size = this.m_ParameterList.size() + 2;
        int length = bArr.length - 1;
        while (bArr[length] == 0 && length > 0) {
            length--;
        }
        if (length == 0) {
            return false;
        }
        return checkFormat(bArr, size, 0, length);
    }

    public void copyInfo(WQTransferData wQTransferData) {
        setSessionID(wQTransferData.getSessionID());
        setConversationID(wQTransferData.getConversationID());
    }

    public int getCMDCode() {
        return (this.a[0] != 0 || this.c == null) ? WQGlobal.converFromByte2UnsignInt(this.a[0]) : this.c.getCMDCode();
    }

    public byte[] getCmd() {
        int paramListCMDLength = super.getParamListCMDLength();
        byte[] cmd = super.getCmd();
        if (this.c != null) {
            byte[] cmd2 = this.c.getCmd();
            WQGlobal.copyFromBytes2Bytes(cmd, paramListCMDLength, cmd2, 0, cmd2.length);
        }
        cmd[cmd.length - 1] = this.m_SplitCode;
        return cmd;
    }

    public WQCmdContentBase getCmdContent() {
        return this.c;
    }

    public int getConversationID() {
        return WQGlobal.byteArray2Int(this.b);
    }

    /* access modifiers changed from: protected */
    public int getParamListCMDLength() {
        int paramListCMDLength = super.getParamListCMDLength();
        if (this.c != null) {
            paramListCMDLength += this.c.getParamListCMDLength();
        }
        return paramListCMDLength + 1;
    }

    public String getSessionID() {
        return WQGlobal.convertFromByte2String(this.m_SessionID);
    }

    public void importCMDContent(WQCmdContentBase wQCmdContentBase) {
        this.c = wQCmdContentBase;
        this.a[0] = (byte) wQCmdContentBase.getCMDCode();
        this.c.setTransferDate(this);
    }

    public int importCmd(byte[] bArr) {
        int length = bArr.length;
        if (checkFormat(bArr)) {
            int importCmd = super.importCmd(bArr);
            if (importCmd == 0) {
                importCMDContent(new WQUnknownContent());
                return 0;
            }
            int i = length - 1;
            while (bArr[i] != this.m_SplitCode && i > 0) {
                i--;
            }
            if (importCmd >= i || i == 0) {
                importCMDContent(new WQUnknownContent());
                return 0;
            }
            int i2 = i - 1;
            try {
                WQCmdContentBase wQCmdContentBase = (WQCmdContentBase) Class.forName(WQGlobal.getCMDNameByCode(this.a[0])).newInstance();
                if (wQCmdContentBase.checkFormat(bArr, importCmd, i2) && wQCmdContentBase.importCmd(bArr, importCmd, i2) > 0) {
                    importCMDContent(wQCmdContentBase);
                    return length;
                }
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            }
        }
        importCMDContent(new WQUnknownContent());
        return 0;
    }

    /* access modifiers changed from: protected */
    public void initiParameterList() {
        this.m_ParameterList.add(this.a);
        this.m_ParameterList.add(this.m_SessionID);
        this.m_ParameterList.add(this.b);
    }

    public boolean isSameConversation(WQTransferData wQTransferData) {
        return getConversationID() == wQTransferData.getConversationID();
    }

    public void moveToNextConversation() {
        setConversationID(getConversationID() + 1);
    }

    public void setConversationID(int i) {
        byte[] int2ByteArray = WQGlobal.int2ByteArray(i, 2);
        WQGlobal.copyFromBytes2Bytes(this.b, 0, int2ByteArray, 0, int2ByteArray.length);
    }

    public void setSessionID(String str) {
        if (str.length() > 0) {
            WQGlobal.copyString2Bytes(this.m_SessionID, str);
        } else {
            WQGlobal.setBytesZero(this.m_SessionID);
        }
    }
}
