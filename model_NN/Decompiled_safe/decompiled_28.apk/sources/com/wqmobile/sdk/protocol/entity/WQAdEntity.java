package com.wqmobile.sdk.protocol.entity;

import com.wqmobile.sdk.protocol.cmd.WQGlobal;
import java.util.ArrayList;
import java.util.List;

public class WQAdEntity {
    private List a = new ArrayList();
    private String b = "";
    private int c = 0;
    private int d = 0;
    private int e = 1;
    private boolean f = false;
    private boolean g = false;

    public void addAdImage(byte[] bArr) {
        this.a.add(bArr);
        this.d = 0;
    }

    public boolean addAdImageBuffer(byte[] bArr) {
        byte[] bArr2 = (byte[]) this.a.get(this.c);
        this.e++;
        if (this.d + bArr.length > bArr2.length) {
            byte[] bArr3 = new byte[(bArr2.length << 1)];
            WQGlobal.copyFromBytes2Bytes(bArr3, 0, bArr2, 0, bArr2.length);
            this.a.remove(this.c);
            this.a.add(this.c, bArr3);
            bArr2 = bArr3;
        }
        WQGlobal.copyFromBytes2Bytes(bArr2, this.d, bArr, 0, bArr.length);
        this.d += bArr.length;
        return true;
    }

    public void clearCurImageBuffer() {
        WQGlobal.setBytesZero((byte[]) this.a.get(this.c));
        this.d = 0;
        this.e = 1;
    }

    public void completeImage() {
        this.c++;
        this.d = 0;
    }

    public void finalize() {
    }

    public List getAdImageList() {
        return this.a;
    }

    public String getAdProperty() {
        return this.b;
    }

    public byte[] getCurImageBuffer() {
        return (byte[]) this.a.get(this.c);
    }

    public int getCurImageLength() {
        return this.d;
    }

    public int getExpectedSeq() {
        return this.e;
    }

    public int getTotalNum() {
        return this.e - 1;
    }

    public boolean isDump() {
        return this.g;
    }

    public boolean isValidAd() {
        return this.f;
    }

    public void setAdProperty(String str) {
        this.b = str;
    }

    public void setDump(boolean z) {
        this.g = z;
    }

    public void setIsValid(boolean z) {
        this.f = z;
    }
}
