package com.wqmobile.sdk.protocol.cmd;

import com.wqmobile.sdk.protocol.cmd.WQGlobal;
import java.math.BigInteger;

public class WQHelloContent extends WQCmdContentBase {
    private byte[] a = new byte[2];
    private byte[] b = new byte[32];
    private byte[] c = new byte[32];

    public WQHelloContent() {
        this.m_GenCMDCode = WQGlobal.WQ_NET_CMD_CODE.enum_HELLO.getByteValue();
        this.m_SplitCode = 63;
        initiParameterList();
    }

    public String getPublisherID() {
        return WQGlobal.convertFromByte2String(this.b);
    }

    public byte[] getVersionNum() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public void initiParameterList() {
        this.m_ParameterList.add(this.a);
        this.m_ParameterList.add(this.b);
        this.m_ParameterList.add(this.c);
    }

    public void setPublisherID(String str) {
        if (str.length() > this.b.length) {
            throw new Exception("The length of Publisher ID must be less than " + String.valueOf(this.b.length));
        }
        WQGlobal.setBytesZero(this.b);
        WQGlobal.copyString2Bytes(this.b, str);
        BigInteger wqHashString = wqHashString(str);
        WQGlobal.setBytesZero(this.c);
        WQGlobal.copyString2Bytes(this.c, wqHashString.toString(16));
    }

    public void setVersionNum(int i, int i2) {
        this.a[0] = (byte) i;
        this.a[1] = (byte) i2;
    }
}
