package com.wqmobile.sdk.protocol;

import android.os.Handler;

final class d extends Handler {
    private /* synthetic */ WQSDKEngineThread a;

    d(WQSDKEngineThread wQSDKEngineThread) {
        this.a = wQSDKEngineThread;
    }

    /*  JADX ERROR: UnsupportedOperationException in pass: MethodInvokeVisitor
        java.lang.UnsupportedOperationException: ArgType.getObject(), call class: class jadx.core.dex.instructions.args.ArgType$ArrayArg
        	at jadx.core.dex.instructions.args.ArgType.getObject(ArgType.java:513)
        	at jadx.core.clsp.ClspGraph.getClsDetails(ClspGraph.java:76)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:100)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public final void handleMessage(android.os.Message r6) {
        /*
            r5 = this;
            java.lang.Object r0 = r6.obj     // Catch:{ Exception -> 0x009e }
            byte[] r0 = (byte[]) r0     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r1 = r5.a     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread.a(r1, r0)     // Catch:{ Exception -> 0x009e }
            int r0 = r6.what     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.cmd.WQGlobal$WQ_NET_COMM_TYPE r1 = com.wqmobile.sdk.protocol.cmd.WQGlobal.WQ_NET_COMM_TYPE.enum_SEND_NO_RECV     // Catch:{ Exception -> 0x009e }
            int r1 = r1.getValue()     // Catch:{ Exception -> 0x009e }
            if (r0 != r1) goto L_0x0014
        L_0x0013:
            return
        L_0x0014:
            int r0 = r6.what     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.cmd.WQGlobal$WQ_NET_COMM_TYPE r1 = com.wqmobile.sdk.protocol.cmd.WQGlobal.WQ_NET_COMM_TYPE.enum_SEND_ASYNC     // Catch:{ Exception -> 0x009e }
            int r1 = r1.getValue()     // Catch:{ Exception -> 0x009e }
            if (r0 != r1) goto L_0x00a4
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x009e }
            android.os.Message r1 = new android.os.Message     // Catch:{ Exception -> 0x009e }
            r1.<init>()     // Catch:{ Exception -> 0x009e }
            r0.j = r1     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x009e }
            android.os.Message r0 = r0.j     // Catch:{ Exception -> 0x009e }
            r0.copyFrom(r6)     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x009e }
            android.os.Handler r0 = r0.g     // Catch:{ Exception -> 0x009e }
            if (r0 == 0) goto L_0x0013
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQHandler r0 = r0.h     // Catch:{ Exception -> 0x009e }
            r0.cancelRetryTimer()     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQHandler r0 = r0.h     // Catch:{ Exception -> 0x009e }
            java.util.Timer r1 = new java.util.Timer     // Catch:{ Exception -> 0x009e }
            r1.<init>()     // Catch:{ Exception -> 0x009e }
            r0.m_RetryTimer = r1     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQHandler r0 = r0.h     // Catch:{ Exception -> 0x009e }
            int r1 = r0.m_TimerCount     // Catch:{ Exception -> 0x009e }
            int r1 = r1 + 1
            r0.m_TimerCount = r1     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x009e }
            int r1 = r0.m     // Catch:{ Exception -> 0x009e }
            int r1 = r1 + 1
            r0.m = r1     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.f r1 = new com.wqmobile.sdk.protocol.f     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r2 = r5.a     // Catch:{ Exception -> 0x009e }
            r1.<init>(r2)     // Catch:{ Exception -> 0x009e }
            r0.l = r1     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQHandler r0 = r0.h     // Catch:{ Exception -> 0x009e }
            java.util.Timer r0 = r0.m_RetryTimer     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r1 = r5.a     // Catch:{ Exception -> 0x009e }
            java.util.TimerTask r1 = r1.l     // Catch:{ Exception -> 0x009e }
            r2 = 3000(0xbb8, double:1.482E-320)
            r0.schedule(r1, r2)     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x009e }
            android.os.Handler r0 = r0.g     // Catch:{ Exception -> 0x009e }
            android.os.Message r0 = r0.obtainMessage()     // Catch:{ Exception -> 0x009e }
            int r1 = r6.what     // Catch:{ Exception -> 0x009e }
            r0.what = r1     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r1 = r5.a     // Catch:{ Exception -> 0x009e }
            android.os.Handler r1 = r1.g     // Catch:{ Exception -> 0x009e }
            r1.sendMessage(r0)     // Catch:{ Exception -> 0x009e }
            goto L_0x0013
        L_0x009e:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0013
        L_0x00a4:
            int r0 = r6.what     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.cmd.WQGlobal$WQ_NET_COMM_TYPE r1 = com.wqmobile.sdk.protocol.cmd.WQGlobal.WQ_NET_COMM_TYPE.enum_SEND_SYNC     // Catch:{ Exception -> 0x009e }
            int r1 = r1.getValue()     // Catch:{ Exception -> 0x009e }
            if (r0 != r1) goto L_0x0013
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x009e }
            android.os.Message r1 = new android.os.Message     // Catch:{ Exception -> 0x009e }
            r1.<init>()     // Catch:{ Exception -> 0x009e }
            r0.j = r1     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x009e }
            android.os.Message r0 = r0.j     // Catch:{ Exception -> 0x009e }
            r0.copyFrom(r6)     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x009e }
            android.os.Handler r0 = r0.g     // Catch:{ Exception -> 0x009e }
            if (r0 == 0) goto L_0x0013
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQHandler r0 = r0.h     // Catch:{ Exception -> 0x009e }
            r0.cancelRetryTimer()     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQHandler r0 = r0.h     // Catch:{ Exception -> 0x009e }
            java.util.Timer r1 = new java.util.Timer     // Catch:{ Exception -> 0x009e }
            r1.<init>()     // Catch:{ Exception -> 0x009e }
            r0.m_RetryTimer = r1     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQHandler r0 = r0.h     // Catch:{ Exception -> 0x009e }
            int r1 = r0.m_TimerCount     // Catch:{ Exception -> 0x009e }
            int r1 = r1 + 1
            r0.m_TimerCount = r1     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.f r1 = new com.wqmobile.sdk.protocol.f     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r2 = r5.a     // Catch:{ Exception -> 0x009e }
            r1.<init>(r2)     // Catch:{ Exception -> 0x009e }
            r0.l = r1     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r0 = r5.a     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQHandler r0 = r0.h     // Catch:{ Exception -> 0x009e }
            java.util.Timer r0 = r0.m_RetryTimer     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r1 = r5.a     // Catch:{ Exception -> 0x009e }
            java.util.TimerTask r1 = r1.l     // Catch:{ Exception -> 0x009e }
            r2 = 3000(0xbb8, double:1.482E-320)
            r0.schedule(r1, r2)     // Catch:{ Exception -> 0x009e }
            byte[] r0 = com.wqmobile.sdk.protocol.WQHandler.lock     // Catch:{ Exception -> 0x009e }
            monitor-enter(r0)     // Catch:{ Exception -> 0x009e }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r1 = r5.a     // Catch:{ all -> 0x0158 }
            byte[] r1 = r1.b     // Catch:{ all -> 0x0158 }
            monitor-enter(r1)     // Catch:{ all -> 0x0158 }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r2 = r5.a     // Catch:{ InterruptedException -> 0x015b }
            android.os.Handler r2 = r2.g     // Catch:{ InterruptedException -> 0x015b }
            android.os.Message r2 = r2.obtainMessage()     // Catch:{ InterruptedException -> 0x015b }
            int r3 = r6.what     // Catch:{ InterruptedException -> 0x015b }
            r2.what = r3     // Catch:{ InterruptedException -> 0x015b }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r3 = r5.a     // Catch:{ InterruptedException -> 0x015b }
            android.os.Handler r3 = r3.g     // Catch:{ InterruptedException -> 0x015b }
            r3.sendMessage(r2)     // Catch:{ InterruptedException -> 0x015b }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r2 = r5.a     // Catch:{ InterruptedException -> 0x015b }
            byte[] r2 = r2.b     // Catch:{ InterruptedException -> 0x015b }
            r3 = 2900(0xb54, double:1.433E-320)
            r2.wait(r3)     // Catch:{ InterruptedException -> 0x015b }
        L_0x0132:
            monitor-exit(r1)     // Catch:{ all -> 0x0160 }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r1 = r5.a     // Catch:{ all -> 0x0158 }
            byte[] r1 = r1.a     // Catch:{ all -> 0x0158 }
            boolean r1 = com.wqmobile.sdk.protocol.cmd.WQGlobal.isZeroByte(r1)     // Catch:{ all -> 0x0158 }
            if (r1 != 0) goto L_0x0155
            com.wqmobile.sdk.protocol.WQSDKEngineThread r1 = r5.a     // Catch:{ all -> 0x0158 }
            com.wqmobile.sdk.protocol.WQHandler r1 = r1.h     // Catch:{ all -> 0x0158 }
            com.wqmobile.sdk.protocol.WQSDKEngineThread r2 = r5.a     // Catch:{ all -> 0x0158 }
            byte[] r2 = r2.a     // Catch:{ all -> 0x0158 }
            java.lang.Object r5 = r2.clone()     // Catch:{ all -> 0x0158 }
            byte[] r5 = (byte[]) r5     // Catch:{ all -> 0x0158 }
            r1.setRecvBuf(r5)     // Catch:{ all -> 0x0158 }
            byte[] r1 = com.wqmobile.sdk.protocol.WQHandler.lock     // Catch:{ all -> 0x0158 }
            r1.notify()     // Catch:{ all -> 0x0158 }
        L_0x0155:
            monitor-exit(r0)     // Catch:{ all -> 0x0158 }
            goto L_0x0013
        L_0x0158:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ Exception -> 0x009e }
            throw r1     // Catch:{ Exception -> 0x009e }
        L_0x015b:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x0160 }
            goto L_0x0132
        L_0x0160:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0158 }
            throw r2     // Catch:{ all -> 0x0158 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wqmobile.sdk.protocol.d.handleMessage(android.os.Message):void");
    }
}
