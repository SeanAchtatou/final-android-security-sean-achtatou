package com.wqmobile.sdk.protocol.socket;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class WQUdpSocketImp implements IMySocket {
    private static WQUdpSocketImp a;
    private DatagramSocket b = new DatagramSocket();
    private InetAddress c;
    private int d;

    public WQUdpSocketImp(InetAddress inetAddress, int i) {
        this.c = inetAddress;
        this.d = i;
    }

    public static IMySocket getInstance() {
        if (a == null) {
            return null;
        }
        return a;
    }

    public static IMySocket getInstance(InetAddress inetAddress, int i) {
        if (a == null) {
            a = new WQUdpSocketImp(inetAddress, i);
        } else {
            WQUdpSocketImp wQUdpSocketImp = a;
            wQUdpSocketImp.c = inetAddress;
            wQUdpSocketImp.d = i;
        }
        return a;
    }

    public void close() {
        if (this.b != null) {
            this.b.close();
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        if (this.b != null) {
            this.b.close();
        }
    }

    public void recv(byte[] bArr) {
        int length = bArr.length;
        if (length > 0) {
            if (this.b != null) {
                this.b.receive(new DatagramPacket(bArr, length));
                return;
            }
            throw new NullPointerException("Null socket.");
        }
    }

    public void send(byte[] bArr) {
        if (this.b != null) {
            this.b.send(new DatagramPacket(bArr, bArr.length, this.c, this.d));
            return;
        }
        throw new NullPointerException("Null socket.");
    }
}
