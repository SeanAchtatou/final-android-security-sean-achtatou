package com.wqmobile.sdk.service;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;
import com.wqmobile.sdk.model.AD;
import com.wqmobile.sdk.model.AdvertisementProperties;
import com.wqmobile.sdk.model.AppSettings;
import com.wqmobile.sdk.model.CacheOfflineADs;
import com.wqmobile.sdk.model.ClientProfile;
import com.wqmobile.sdk.model.DeviceProfile;
import com.wqmobile.sdk.model.DynClientProfile;
import com.wqmobile.sdk.model.DynamicClientProfile;
import com.wqmobile.sdk.model.GeographicInfo;
import com.wqmobile.sdk.model.HardwareProfile;
import com.wqmobile.sdk.model.NetworkDevice;
import com.wqmobile.sdk.model.NetworkDevices;
import com.wqmobile.sdk.model.OfflineADs;
import com.wqmobile.sdk.model.Platform;
import com.wqmobile.sdk.model.Screen;
import com.wqmobile.sdk.model.SystemProfile;
import com.wqmobile.sdk.model.UserResponse;
import com.wqmobile.sdk.model.ViewSettings;
import com.wqmobile.sdk.pojoxml.core.PojoXml;
import com.wqmobile.sdk.pojoxml.core.PojoXmlFactory;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import com.wqmobile.sdk.protocol.WQCommunicator;
import com.wqmobile.sdk.protocol.entity.WQAdEntity;
import com.wqmobile.sdk.util.FileUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class AdsService {
    private static AppSettings appSettings;
    public static CacheOfflineADs cacheOffline = new CacheOfflineADs();
    private static Boolean hasSendOfflineAdCount = false;
    private static AdsService instance;
    private static OfflineADs offlineADs;
    private Boolean SHOW_MSG = false;
    /* access modifiers changed from: private */
    public ADCallback adCallback;
    private String appStr;
    private WQCommunicator comm;
    private String commAPP = "";
    private Boolean commFlag = false;
    private Context context;
    /* access modifiers changed from: private */
    public PojoXmlFactory factory = PojoXmlFactory.getInstance();
    private Integer fristLoad = 0;
    private GetAdObserverImp imp;
    private int mHeight = 0;
    private int mWidth = 0;
    /* access modifiers changed from: private */
    public Boolean offlineChange = false;
    private Timestamp orgTime;
    PojoXml pojo;
    private String sdkVersion = "1.0.1";
    public String viewTag = "wqadsdk view tag";

    public void setSize(int width, int height) {
        this.mWidth = width;
        this.mHeight = height;
    }

    public static synchronized AdsService getInstance(Context context2) {
        AdsService adsService;
        synchronized (AdsService.class) {
            if (instance == null) {
                instance = new AdsService(context2);
            } else {
                instance.setContext(context2);
            }
            adsService = instance;
        }
        return adsService;
    }

    public AdsService(Context context2) {
        cacheOffline = FileUtil.LoadCacheADs(context2);
        offlineADs = FileUtil.LoadOfflineConfig(context2);
        this.imp = new GetAdObserverImp();
        try {
            this.comm = WQCommunicator.getInstance();
            this.comm.addObserver(this.imp);
        } catch (Exception e) {
            this.comm = null;
            showMsg("method:AdsService:" + e.toString());
        }
        this.context = context2;
    }

    public void refreshAppSetting(String appsettings) {
        Class<AppSettings> cls = AppSettings.class;
        this.appStr = appsettings;
        this.pojo = this.factory.createPojoXml();
        Class<AppSettings> cls2 = AppSettings.class;
        appSettings = (AppSettings) this.pojo.getPojo(appsettings, cls);
        if (isNetworkOnline(this.context).booleanValue() && appsettings != null && this.comm != null) {
            int i = 0;
            while (true) {
                if (i > 3) {
                    break;
                }
                this.commFlag = Boolean.valueOf(this.comm.startRequest(appSettings.getPublisherID()));
                if (this.commFlag.booleanValue()) {
                    int j = 0;
                    while (true) {
                        if (i > 3) {
                            break;
                        }
                        this.commAPP = this.comm.refreshAppSetting(appsettings);
                        if (this.commAPP.length() > 0) {
                            Class<AppSettings> cls3 = AppSettings.class;
                            appSettings = (AppSettings) this.pojo.getPojo(this.commAPP, cls);
                            if (appSettings.getTestMode().equals("N") && appSettings.getRefreshRate().intValue() < 20) {
                                appSettings.setRefreshRate(20);
                            }
                        } else {
                            j++;
                        }
                    }
                } else {
                    i++;
                }
            }
        }
        showMsg(this.commAPP);
    }

    public void refreshAppSetting() {
        if (isNetworkOnline(this.context).booleanValue() && appSettings != null && this.comm != null) {
            int i = 0;
            while (true) {
                if (i > 3) {
                    break;
                }
                this.commFlag = Boolean.valueOf(this.comm.startRequest(appSettings.getPublisherID()));
                if (this.commFlag.booleanValue()) {
                    int j = 0;
                    while (true) {
                        if (i > 3) {
                            break;
                        }
                        showTimes("begin refreshAppSetting");
                        this.commAPP = this.comm.refreshAppSetting(this.appStr);
                        showTimes("end refreshAppSetting");
                        if (this.commAPP.length() > 0) {
                            appSettings = (AppSettings) this.pojo.getPojo(this.commAPP, AppSettings.class);
                            if (appSettings.getTestMode().equals("N") && appSettings.getRefreshRate().intValue() < 20) {
                                appSettings.setRefreshRate(20);
                            }
                            showMsg("LoopTimes:" + appSettings.getLoopTimes() + "NextADCount:" + appSettings.getNextADCount() + "RefreshRate:" + appSettings.getRefreshRate());
                        } else {
                            j++;
                        }
                    }
                } else {
                    i++;
                }
            }
        }
        showMsg(this.commAPP);
        showMsg(appSettings.getAppID());
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.wqmobile.sdk.model.AppSettings} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.wqmobile.sdk.model.AppSettings getAppSettings(java.lang.String r7) {
        /*
            r6 = this;
            r5 = 20
            r2 = 0
            r6.appStr = r7
            com.wqmobile.sdk.pojoxml.core.PojoXmlFactory r3 = r6.factory     // Catch:{ Exception -> 0x003b }
            com.wqmobile.sdk.pojoxml.core.PojoXml r3 = r3.createPojoXml()     // Catch:{ Exception -> 0x003b }
            r6.pojo = r3     // Catch:{ Exception -> 0x003b }
            com.wqmobile.sdk.pojoxml.core.PojoXml r3 = r6.pojo     // Catch:{ Exception -> 0x003b }
            java.lang.Class<com.wqmobile.sdk.model.AppSettings> r4 = com.wqmobile.sdk.model.AppSettings.class
            java.lang.Object r3 = r3.getPojo(r7, r4)     // Catch:{ Exception -> 0x003b }
            r0 = r3
            com.wqmobile.sdk.model.AppSettings r0 = (com.wqmobile.sdk.model.AppSettings) r0     // Catch:{ Exception -> 0x003b }
            r2 = r0
            java.lang.String r3 = r2.getTestMode()     // Catch:{ Exception -> 0x003b }
            java.lang.String r4 = "N"
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x003b }
            if (r3 == 0) goto L_0x0038
            java.lang.Integer r3 = r2.getRefreshRate()     // Catch:{ Exception -> 0x003b }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x003b }
            if (r3 >= r5) goto L_0x0038
            r3 = 20
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x003b }
            r2.setRefreshRate(r3)     // Catch:{ Exception -> 0x003b }
        L_0x0038:
            com.wqmobile.sdk.service.AdsService.appSettings = r2
            return r2
        L_0x003b:
            r3 = move-exception
            r1 = r3
            java.lang.String r3 = r1.toString()
            r6.showMsg(r3)
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wqmobile.sdk.service.AdsService.getAppSettings(java.lang.String):com.wqmobile.sdk.model.AppSettings");
    }

    public String getAppSettings(AppSettings app) {
        String result = null;
        appSettings = app;
        try {
            this.pojo = this.factory.createPojoXml();
            result = this.pojo.getXml(app);
        } catch (Exception e) {
            showMsg("method getAppSettings:" + e.toString());
        }
        this.appStr = result;
        return result;
    }

    public void refreshAppSetting(InputStream appsettings) {
        refreshAppSetting(getStringByInputStream(appsettings));
    }

    public Boolean isNetworkOnline(Context context2) {
        NetworkInfo info;
        try {
            ConnectivityManager conn = (ConnectivityManager) context2.getSystemService("connectivity");
            if (conn == null || (info = conn.getActiveNetworkInfo()) == null) {
                return false;
            }
            return Boolean.valueOf(info.isAvailable());
        } catch (Exception e) {
            showMsg(e.toString());
            return false;
        }
    }

    public void getAds(Context context2, ADCallback callback) {
        int nextAdNum;
        this.adCallback = callback;
        this.pojo = this.factory.createPojoXml();
        if (this.comm != null && isNetworkOnline(context2).booleanValue()) {
            String xml1 = null;
            if (this.commAPP.length() <= 0 || !this.commFlag.booleanValue()) {
                refreshAppSetting();
                xml1 = this.pojo.getXml(getClientProfile());
            }
            if (this.commAPP.length() > 0 && this.commFlag.booleanValue()) {
                if (this.fristLoad.intValue() == 0) {
                    showTimes("begin getFirstAds");
                    this.comm.getFirstAds(xml1, appSettings.getIntNextADCount().intValue() + 1);
                    showTimes("end getFirstAds");
                    if (appSettings.getAcceptsOfflineAD() != null && appSettings.getAcceptsOfflineAD().equals("Y")) {
                        int count = appSettings.getIntOfflineADCount();
                        if (cacheOffline == null || cacheOffline.getOfflineADs().size() <= count) {
                            showMsg("get offline ads");
                            this.comm.getOfflineAds(count);
                        }
                    }
                    this.fristLoad = Integer.valueOf(this.fristLoad.intValue() + 1);
                } else {
                    String xml = this.pojo.getXml(getDynamicClientProfile());
                    if (WasRefused().booleanValue()) {
                        nextAdNum = 1;
                    } else {
                        nextAdNum = Integer.valueOf(appSettings.getNextADCount()).intValue();
                    }
                    this.comm.getNextAds(xml, nextAdNum);
                    showMsg("get next ADs:" + appSettings.getNextADCount());
                }
                if (offlineADs.getAD().length > 0 && !hasSendOfflineAdCount.booleanValue()) {
                    this.comm.sendOfflineADCounting(this.pojo.getXml(offlineADs));
                    offlineADs = new OfflineADs();
                    hasSendOfflineAdCount = true;
                }
            }
        } else if (!(cacheOffline == null || cacheOffline.getOfflineADs() == null || cacheOffline.getOfflineADs().size() <= 0)) {
            for (AdvertisementProperties pro : cacheOffline.getOfflineADs()) {
                callback.ADLoaded(pro);
            }
        }
        showTimes("get ads after");
    }

    private ClientProfile getClientProfile() {
        ClientProfile result = new ClientProfile();
        result.setAppID(appSettings.getAppID());
        result.setSDKVersion(this.sdkVersion);
        DeviceProfile dp = result.getDeviceProfile();
        dp.setPhoneInfo(GetPhoneInfo(this.context));
        dp.setNetworkDevices(GetNetworkDevices());
        dp.setSystemProfile(GetSystemProfile());
        dp.setHardwareProfile(GetHardwareProfile());
        result.setGeographicInfo(getGeographicInfo());
        return result;
    }

    private DynamicClientProfile getDynamicClientProfile() {
        DynamicClientProfile result = new DynamicClientProfile();
        DynClientProfile dpf = new DynClientProfile();
        dpf.setGeographicInfo(getGeographicInfo());
        result.setViewSettings(getViewSettings());
        result.setClientProfile(dpf);
        return result;
    }

    private ViewSettings getViewSettings() {
        return new ViewSettings(Integer.valueOf(this.mWidth), Integer.valueOf(this.mHeight));
    }

    private GeographicInfo getGeographicInfo() {
        GeographicInfo gi = new GeographicInfo();
        try {
            LocationManager locationManager = (LocationManager) this.context.getSystemService("location");
            Criteria criteria = new Criteria();
            criteria.setAccuracy(1);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setCostAllowed(false);
            criteria.setPowerRequirement(1);
            String provider = locationManager.getBestProvider(criteria, true);
            Location location = locationManager.getLastKnownLocation(provider);
            if (location != null) {
                String latitude = Double.toString(location.getLatitude());
                String longitude = Double.toString(location.getLongitude());
                gi.setLatitude(latitude);
                gi.setLongitude(longitude);
                gi.setObtainMethod(provider);
            }
        } catch (Exception e) {
            gi.setLatitude("0");
            gi.setLongitude("0");
            gi.setObtainMethod("");
            showMsg("Err in getting location");
        }
        return gi;
    }

    public Boolean sendSMS(Context context2, String telNumber, String telMessage) {
        SmsManager smsMgr = SmsManager.getDefault();
        try {
            for (String text : smsMgr.divideMessage(telMessage)) {
                smsMgr.sendTextMessage(telNumber, null, text, null, null);
            }
            return true;
        } catch (Exception e) {
            Exception e2 = e;
            Log.e("SMS Error", e2.getMessage());
            showMsg("method sendSMS:" + e2.toString());
            return false;
        }
    }

    public void callPhone(Context context2, String telNumber) {
        try {
            context2.startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:" + telNumber)));
        } catch (Exception e) {
            showMsg(e.toString());
        }
    }

    public void openUrl(Context context2, String url) {
        context2.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
    }

    private NetworkDevices GetNetworkDevices() {
        String str;
        NetworkDevices result = new NetworkDevices();
        List<NetworkDevice> devices = new ArrayList<>();
        NetworkDevice gg = null;
        NetworkDevice edge = null;
        NetworkDevice gprs = null;
        TelephonyManager tm = (TelephonyManager) this.context.getSystemService("phone");
        if (((ConnectivityManager) this.context.getSystemService("connectivity")).getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
            str = "Connected";
        } else {
            str = "Disconnected";
        }
        devices.add(GetNetworkDevice("WIFI", str, "", appSettings.getIP(), getLocalMacAddress()));
        switch (tm.getNetworkType()) {
            case 1:
                gprs = GetNetworkDevice("gprs", "Connected", "", "", "");
                break;
            case 2:
                edge = GetNetworkDevice("gprs_edge", "Connected", "", "", "");
                break;
            case 3:
            case 4:
            case 5:
            case 6:
            case 8:
                gg = GetNetworkDevice("3G", "Connected", "", "", "");
                break;
        }
        if (gg == null) {
            gg = GetNetworkDevice("3G", "Disconnected", "", "", "");
        }
        if (edge == null) {
            edge = GetNetworkDevice("gprs_edge", "Disconnected", "", "", "");
        }
        if (gprs == null) {
            gprs = GetNetworkDevice("gprs", "Disconnected", "", "", "");
        }
        devices.add(gg);
        devices.add(edge);
        devices.add(gprs);
        NetworkDevice[] dds = new NetworkDevice[devices.size()];
        devices.toArray(dds);
        result.setDevice(dds);
        return result;
    }

    private NetworkDevice GetNetworkDevice(String type, String status, String speed, String ip, String mac) {
        NetworkDevice result = new NetworkDevice();
        result.setConnectionType(type);
        result.setStatus(status);
        result.setIP(ip);
        result.setMAC(mac);
        result.setSpeed(speed);
        return result;
    }

    private SystemProfile GetSystemProfile() {
        SystemProfile result = new SystemProfile();
        Platform pf = new Platform();
        pf.setType("Android");
        pf.setVersion(Build.VERSION.SDK);
        result.setPlatform(pf);
        return result;
    }

    private HardwareProfile GetHardwareProfile() {
        HardwareProfile result = new HardwareProfile();
        Screen sc = result.getScreen();
        Display display = ((Activity) this.context).getWindowManager().getDefaultDisplay();
        sc.setHorizontalResolution(Integer.valueOf(display.getWidth()));
        sc.setVerticalResolution(Integer.valueOf(display.getHeight()));
        return result;
    }

    /* JADX WARN: Type inference failed for: r13v16, types: [android.telephony.CellLocation] */
    /* JADX WARN: Type inference failed for: r13v25, types: [android.telephony.CellLocation] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.wqmobile.sdk.model.PhoneInfo GetPhoneInfo(android.content.Context r17) {
        /*
            r16 = this;
            r15 = 3
            com.wqmobile.sdk.model.PhoneInfo r11 = new com.wqmobile.sdk.model.PhoneInfo
            r11.<init>()
            com.wqmobile.sdk.model.DeviceID r4 = r11.getDeviceID()
            com.wqmobile.sdk.model.BaseStation r2 = r11.getBaseStation()
            com.wqmobile.sdk.model.Network r7 = r11.getNetwork()
            com.wqmobile.sdk.model.Operator r9 = r7.getOperator()
            r6 = 0
            r3 = 0
            r10 = 0
            java.lang.String r13 = "phone"
            r0 = r17
            r1 = r13
            java.lang.Object r12 = r0.getSystemService(r1)
            android.telephony.TelephonyManager r12 = (android.telephony.TelephonyManager) r12
            int r13 = r12.getPhoneType()
            switch(r13) {
                case 1: goto L_0x00b5;
                case 2: goto L_0x00b1;
                default: goto L_0x002b;
            }
        L_0x002b:
            java.lang.String r13 = r12.getLine1Number()
            r11.setPhoneNumber(r13)
            r11.setRadioType(r10)
            java.lang.String r13 = android.os.Build.MANUFACTURER
            r11.setManufacturer(r13)
            java.lang.String r13 = android.os.Build.MODEL
            r11.setModel(r13)
            java.lang.String r13 = r12.getDeviceId()
            r4.setIMEI(r13)
            java.lang.String r13 = r12.getSubscriberId()
            r4.setIMSI(r13)
            android.telephony.CellLocation r13 = r12.getCellLocation()     // Catch:{ Exception -> 0x00b9 }
            r0 = r13
            android.telephony.gsm.GsmCellLocation r0 = (android.telephony.gsm.GsmCellLocation) r0     // Catch:{ Exception -> 0x00b9 }
            r6 = r0
        L_0x0055:
            if (r6 == 0) goto L_0x006d
            int r13 = r6.getCid()
            java.lang.String r13 = java.lang.String.valueOf(r13)
            r2.setGSMCellID(r13)
            int r13 = r6.getLac()
            java.lang.String r13 = java.lang.String.valueOf(r13)
            r2.setGSMLocationAreCode(r13)
        L_0x006d:
            java.lang.String r13 = r12.getNetworkCountryIso()
            r7.setCountryISO(r13)
            java.lang.String r8 = r12.getNetworkOperator()
            if (r8 == 0) goto L_0x0090
            int r13 = r8.length()
            r14 = 2
            if (r13 <= r14) goto L_0x0090
            r13 = 0
            java.lang.String r13 = r8.substring(r13, r15)
            r9.setMCC(r13)
            java.lang.String r13 = r8.substring(r15)
            r9.setMNC(r13)
        L_0x0090:
            android.telephony.CellLocation r13 = r12.getCellLocation()     // Catch:{ Exception -> 0x00c6 }
            r0 = r13
            android.telephony.cdma.CdmaCellLocation r0 = (android.telephony.cdma.CdmaCellLocation) r0     // Catch:{ Exception -> 0x00c6 }
            r3 = r0
        L_0x0098:
            if (r3 == 0) goto L_0x00b0
            int r13 = r3.getSystemId()
            java.lang.String r13 = java.lang.String.valueOf(r13)
            r9.setSID(r13)
            int r13 = r3.getNetworkId()
            java.lang.String r13 = java.lang.String.valueOf(r13)
            r9.setNID(r13)
        L_0x00b0:
            return r11
        L_0x00b1:
            java.lang.String r10 = "CDMA"
            goto L_0x002b
        L_0x00b5:
            java.lang.String r10 = "GSM"
            goto L_0x002b
        L_0x00b9:
            r13 = move-exception
            r5 = r13
            java.lang.String r13 = r5.toString()
            r0 = r16
            r1 = r13
            r0.showMsg(r1)
            goto L_0x0055
        L_0x00c6:
            r13 = move-exception
            r5 = r13
            java.lang.String r13 = r5.toString()
            r0 = r16
            r1 = r13
            r0.showMsg(r1)
            goto L_0x0098
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wqmobile.sdk.service.AdsService.GetPhoneInfo(android.content.Context):com.wqmobile.sdk.model.PhoneInfo");
    }

    private String getLocalMacAddress() {
        return ((WifiManager) this.context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
    }

    public String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (enumIpAddr.hasMoreElements()) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e) {
            showMsg(e.toString());
        }
        return null;
    }

    public Timestamp getOrgTime() {
        return this.orgTime;
    }

    public void setOrgTime(Timestamp orgTime2) {
        this.orgTime = orgTime2;
    }

    public void saveOfflineAds() {
        if (cacheOffline != null && cacheOffline.getOfflineADs().size() > 0 && this.offlineChange.booleanValue()) {
            FileUtil.SaveCacheADs(this.context, cacheOffline);
            FileUtil.LoadCacheADs(this.context);
            this.offlineChange = false;
        }
        FileUtil.SaveOfflineConfig(this.context, offlineADs);
    }

    /* access modifiers changed from: private */
    public void checkOutofOfflineAds() {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        Iterator<AdvertisementProperties> itr = cacheOffline.getOfflineADs().iterator();
        while (itr.hasNext()) {
            AdvertisementProperties pro = itr.next();
            if (pro.getOfflineAdExpireDate() != null && pro.getOfflineAdExpireDateTime().before(now)) {
                showMsg("time out adID : " + pro.getAdID());
                FileUtil.DelOfflineAd(pro.getAdID(), this.context);
                itr.remove();
            }
        }
    }

    class GetAdObserverImp implements Observer {
        GetAdObserverImp() {
        }

        public void update(Observable arg0, Object arg1) {
            AdsService.this.showTimes("got the ads ");
            if (arg0 instanceof WQCommunicator) {
                WQAdEntity entity = (WQAdEntity) arg1;
                if (entity.isValidAd() && entity.getAdProperty().length() > 0) {
                    AdsService.this.pojo = AdsService.this.factory.CreateAdvertisementProperties();
                    AdvertisementProperties result = (AdvertisementProperties) AdsService.this.pojo.getPojo(entity.getAdProperty(), AdvertisementProperties.class);
                    result.setImageList(entity.getAdImageList());
                    AdsService.this.showMsg("AdID:" + result.getAdID() + " Type:" + result.getType() + "  ActionType:" + result.getAction().getActionType() + "  offline:" + result.getIsOffline() + "  ImageList count:" + entity.getAdImageList().size());
                    if (result.getIsOffline().equals("Y")) {
                        if (AdsService.cacheOffline == null) {
                            AdsService.cacheOffline = new CacheOfflineADs();
                        }
                        AdsService.cacheOffline.addOfflineAD(result);
                        AdsService.this.checkOutofOfflineAds();
                        AdsService.this.offlineChange = true;
                    } else if (AdsService.this.adCallback != null) {
                        AdsService.this.adCallback.ADLoaded(result);
                    }
                }
            }
        }
    }

    public AppSettings getAppSettings() {
        return appSettings;
    }

    public Drawable getDrawableByByte(byte[] bytes) {
        if (bytes != null) {
            return new BitmapDrawable(BitmapFactory.decodeByteArray(bytes, 0, bytes.length));
        }
        return null;
    }

    public Bitmap getBitmapByByte(byte[] bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    public long getDiffTime(Timestamp before, Timestamp now) {
        if (now == null || before == null) {
            return 0;
        }
        return now.getTime() - before.getTime();
    }

    public String getStringByInputStream(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                        showMsg(e.toString());
                    }
                } else {
                    sb.append(String.valueOf(line) + XmlConstant.NL);
                }
            } catch (IOException e2) {
                showMsg(e2.toString());
                try {
                    is.close();
                } catch (IOException e3) {
                    showMsg(e3.toString());
                }
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e4) {
                    showMsg(e4.toString());
                }
                throw th;
            }
        }
        is.close();
        return sb.toString();
    }

    public void sendMail(String mailAddress, String title, String content) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.EMAIL", new String[]{mailAddress});
        intent.putExtra("android.intent.extra.SUBJECT", new String[]{title});
        intent.putExtra("android.intent.extra.TEXT", content);
        intent.setType("text/plain");
        this.context.startActivity(intent);
    }

    private String getXmlUserResponse(String adId, String responseTime, String responseType, String responseViewingTime, Integer responseViewingTimes) {
        UserResponse ur = new UserResponse();
        ur.setAdID(adId);
        ur.setAppID(appSettings.getAppID());
        ur.setResponseTime(responseTime);
        ur.setResponseType(responseType);
        ur.setResponseViewingTime(responseViewingTime);
        ur.setResponseViewingTimes(responseViewingTimes);
        this.pojo = this.factory.createPojoXml();
        return this.pojo.getXml(ur);
    }

    public void sendUserRespone(String adId, String responseType, String responseViewingTime, Integer responseViewingTimes) {
        showMsg("adId:" + adId + "," + "type:" + responseType);
        if (isNetworkOnline(this.context).booleanValue() && this.comm != null) {
            String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Timestamp(System.currentTimeMillis()));
            if (this.commAPP.length() <= 0 || !this.commFlag.booleanValue()) {
                refreshAppSetting();
                this.comm.sendUserResponse(getXmlUserResponse(adId, time, responseType, responseViewingTime, responseViewingTimes));
                return;
            }
            this.comm.sendUserResponse(getXmlUserResponse(adId, time, responseType, responseViewingTime, responseViewingTimes));
        }
    }

    public void viewOfflineAD(AdvertisementProperties pro) {
        List<AD> list;
        Boolean is = false;
        for (AD temp : offlineADs.getAD()) {
            if (temp.getID().equals(pro.getAdID())) {
                temp.setViewCount(Integer.valueOf(temp.getClickCount().intValue() + 1));
                is = true;
            }
        }
        if (!is.booleanValue()) {
            AD ad = new AD();
            ad.setID(pro.getAdID());
            ad.setViewCount(pro.getRunningCount());
            ad.setClickCount(1);
            AD[] ads = offlineADs.getAD();
            if (ads.length > 0) {
                list = new ArrayList<>(Arrays.asList(ads));
            } else {
                list = new ArrayList<>();
            }
            list.add(ad);
            AD[] add = new AD[list.size()];
            list.toArray(add);
            offlineADs.setAD(add);
        }
    }

    public void doOfflineEvent(AdvertisementProperties pro) {
        List<AD> list;
        Boolean is = false;
        for (AD temp : offlineADs.getAD()) {
            if (temp.getID().equals(pro.getAdID())) {
                temp.setClickCount(Integer.valueOf(temp.getClickCount().intValue() + 1));
                is = true;
            }
        }
        if (!is.booleanValue()) {
            AD ad = new AD();
            ad.setID(pro.getAdID());
            ad.setViewCount(pro.getRunningCount());
            ad.setClickCount(1);
            AD[] ads = offlineADs.getAD();
            if (ads.length > 0) {
                list = new ArrayList<>(Arrays.asList(ads));
            } else {
                list = new ArrayList<>();
            }
            list.add(ad);
            AD[] add = new AD[list.size()];
            list.toArray(add);
            offlineADs.setAD(add);
        }
    }

    public void showTimes(String tag) {
        if (this.SHOW_MSG.booleanValue()) {
            Log.d("Times", String.valueOf(tag) + new SimpleDateFormat("hh:mm:ss SSS").format(new Date(System.currentTimeMillis())));
        }
    }

    public void showMsg(String msg) {
        if (this.SHOW_MSG.booleanValue()) {
            Log.d(this.viewTag, msg);
        }
    }

    public void setContext(Context context2) {
        this.context = context2;
    }

    public void setAppSettings(AppSettings appSettings2) {
        appSettings = appSettings2;
    }

    public Boolean WasRefused() {
        return this.comm.WasRefused();
    }

    public void RefreshRunningTime() {
        if (offlineADs != null && appSettings != null) {
            offlineADs.setRunningTime(Integer.valueOf(offlineADs.getRunningTime().intValue() + appSettings.getRefreshRate().intValue()));
        }
    }
}
