package com.wqmobile.sdk.util;

import android.content.Context;
import android.os.Environment;
import com.wqmobile.sdk.model.AdvertisementProperties;
import com.wqmobile.sdk.model.CacheOfflineADs;
import com.wqmobile.sdk.model.OfflineADs;
import com.wqmobile.sdk.pojoxml.core.PojoXmlFactory;
import com.wqmobile.sdk.pojoxml.util.StringUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileUtil {
    private static PojoXmlFactory factory = new PojoXmlFactory();
    private static String offlineAdsName = "ads.wq";
    private static String offlineConfig = "cc.wq";
    private static String sdcardPath = "/wqmobile/";

    public static void SaveCacheADs(Context context, CacheOfflineADs ads) {
        try {
            for (AdvertisementProperties pro : ads.getOfflineADs()) {
                SaveCacheAdver(context, pro);
            }
            SaveFile(context, offlineAdsName, factory.createPojoXml().getXml(ads).getBytes());
        } catch (Exception e) {
            StringUtil.PrintExceptionStatck(e);
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v4, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.wqmobile.sdk.model.CacheOfflineADs} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.wqmobile.sdk.model.CacheOfflineADs LoadCacheADs(android.content.Context r13) {
        /*
            com.wqmobile.sdk.model.CacheOfflineADs r9 = new com.wqmobile.sdk.model.CacheOfflineADs
            r9.<init>()
            r5 = 0
            r6 = 0
            byte[] r6 = (byte[]) r6     // Catch:{ Exception -> 0x0068 }
            java.lang.String r10 = com.wqmobile.sdk.util.FileUtil.offlineAdsName     // Catch:{ Exception -> 0x0068 }
            byte[] r2 = LoadFile(r13, r10)     // Catch:{ Exception -> 0x0068 }
            if (r2 == 0) goto L_0x0036
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x0068 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0068 }
            com.wqmobile.sdk.pojoxml.core.PojoXmlFactory r10 = com.wqmobile.sdk.util.FileUtil.factory     // Catch:{ Exception -> 0x0068 }
            com.wqmobile.sdk.pojoxml.core.PojoXml r7 = r10.CreateCacheOfflineAds()     // Catch:{ Exception -> 0x0068 }
            java.lang.Class<com.wqmobile.sdk.model.CacheOfflineADs> r10 = com.wqmobile.sdk.model.CacheOfflineADs.class
            java.lang.Object r10 = r7.getPojo(r1, r10)     // Catch:{ Exception -> 0x0068 }
            r0 = r10
            com.wqmobile.sdk.model.CacheOfflineADs r0 = (com.wqmobile.sdk.model.CacheOfflineADs) r0     // Catch:{ Exception -> 0x0068 }
            r9 = r0
            if (r9 == 0) goto L_0x0036
            java.util.List r10 = r9.getOfflineADs()     // Catch:{ Exception -> 0x0068 }
            java.util.Iterator r10 = r10.iterator()     // Catch:{ Exception -> 0x0068 }
        L_0x0030:
            boolean r11 = r10.hasNext()     // Catch:{ Exception -> 0x0068 }
            if (r11 != 0) goto L_0x0037
        L_0x0036:
            return r9
        L_0x0037:
            java.lang.Object r8 = r10.next()     // Catch:{ Exception -> 0x0068 }
            com.wqmobile.sdk.model.AdvertisementProperties r8 = (com.wqmobile.sdk.model.AdvertisementProperties) r8     // Catch:{ Exception -> 0x0068 }
            java.lang.String r11 = r8.getAdID()     // Catch:{ Exception -> 0x0068 }
            java.util.List r5 = SearchFile(r13, r11)     // Catch:{ Exception -> 0x0068 }
            java.util.ArrayList r11 = new java.util.ArrayList     // Catch:{ Exception -> 0x0068 }
            r11.<init>()     // Catch:{ Exception -> 0x0068 }
            r8.setImageList(r11)     // Catch:{ Exception -> 0x0068 }
            java.util.Iterator r11 = r5.iterator()     // Catch:{ Exception -> 0x0068 }
        L_0x0051:
            boolean r12 = r11.hasNext()     // Catch:{ Exception -> 0x0068 }
            if (r12 == 0) goto L_0x0030
            java.lang.Object r4 = r11.next()     // Catch:{ Exception -> 0x0068 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ Exception -> 0x0068 }
            byte[] r6 = LoadFile(r13, r4)     // Catch:{ Exception -> 0x0068 }
            int r12 = r6.length     // Catch:{ Exception -> 0x0068 }
            if (r12 <= 0) goto L_0x0051
            r8.addImage(r6)     // Catch:{ Exception -> 0x0068 }
            goto L_0x0051
        L_0x0068:
            r10 = move-exception
            r3 = r10
            com.wqmobile.sdk.pojoxml.util.StringUtil.PrintExceptionStatck(r3)
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wqmobile.sdk.util.FileUtil.LoadCacheADs(android.content.Context):com.wqmobile.sdk.model.CacheOfflineADs");
    }

    public static void DelOfflineAd(String adId, Context context) {
        for (String fileName : SearchFile(context, adId)) {
            DeleteDataFile(fileName, context);
        }
    }

    public static OfflineADs LoadOfflineConfig(Context context) {
        OfflineADs result = new OfflineADs();
        try {
            byte[] bytes = LoadFile(context, offlineConfig);
            if (bytes == null) {
                return result;
            }
            return (OfflineADs) factory.createPojoXml().getPojo(new String(bytes), OfflineADs.class);
        } catch (Exception e) {
            StringUtil.PrintExceptionStatck(e);
            return result;
        }
    }

    public static void SaveOfflineConfig(Context context, OfflineADs ads) {
        if (ads != null) {
            try {
                if (ads.getAD().length > 0) {
                    SaveFile(context, offlineConfig, factory.createPojoXml().getXml(ads).getBytes());
                }
            } catch (Exception e) {
                StringUtil.PrintExceptionStatck(e);
            }
        }
    }

    private static List<String> SearchFile(Context context, String startName) {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return SearchSDCardFile(startName);
        }
        return SearchDataFile(context, startName);
    }

    private static List<String> SearchSDCardFile(String startName) {
        List<String> result = new ArrayList<>();
        for (String temp : new File(Environment.getExternalStorageDirectory(), sdcardPath).list()) {
            if (temp.startsWith(startName)) {
                result.add(temp);
            }
        }
        return result;
    }

    private static List<String> SearchDataFile(Context context, String startName) {
        List<String> result = new ArrayList<>();
        for (String temp : context.fileList()) {
            if (temp.startsWith(startName)) {
                result.add(temp);
            }
        }
        return result;
    }

    private static byte[] LoadFile(Context context, String fileName) {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return LoadSDCardFile(String.valueOf(sdcardPath) + fileName);
        }
        return LoadDataFile(context, fileName);
    }

    private static void SaveFile(Context context, String fileName, byte[] bytes) {
        if (Environment.getExternalStorageState().equals("mounted")) {
            new File(Environment.getExternalStorageDirectory(), sdcardPath).mkdirs();
            SaveSDCardFile(String.valueOf(sdcardPath) + fileName, bytes);
            return;
        }
        SaveDataFile(context, fileName, bytes);
    }

    private static void SaveDataFile(Context context, String fileName, byte[] bytes) {
        try {
            FileOutputStream outStream = context.openFileOutput(fileName, 0);
            outStream.write(bytes);
            outStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private static byte[] LoadDataFile(Context context, String fileName) {
        byte[] buffer = null;
        try {
            FileInputStream fileInputStream = context.openFileInput(fileName);
            buffer = new byte[fileInputStream.available()];
            fileInputStream.read(buffer);
            fileInputStream.close();
            return buffer;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return buffer;
        } catch (IOException e2) {
            e2.printStackTrace();
            return buffer;
        }
    }

    private static Boolean DeleteDataFile(String fileName, Context context) {
        return Boolean.valueOf(new File(fileName).delete());
    }

    private static void SaveSDCardFile(String fileName, byte[] bytes) {
        try {
            FileOutputStream outputStream = new FileOutputStream(new File(Environment.getExternalStorageDirectory(), fileName));
            outputStream.write(bytes);
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private static byte[] LoadSDCardFile(String fileName) {
        File file = new File(Environment.getExternalStorageDirectory(), fileName);
        byte[] buffer = null;
        try {
            if (!file.exists()) {
                return buffer;
            }
            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] buffer2 = new byte[fileInputStream.available()];
            fileInputStream.read(buffer2);
            fileInputStream.close();
            return buffer2;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return buffer;
        } catch (IOException e2) {
            e2.printStackTrace();
            return buffer;
        }
    }

    private static void SaveCacheAdver(Context context, AdvertisementProperties pro) {
        try {
            if (pro.getImageList() != null) {
                synchronized (pro) {
                    for (byte[] img : pro.getImageList()) {
                        SaveFile(context, String.valueOf(pro.getAdID()) + 0 + ".cache", img);
                    }
                }
            }
        } catch (Exception e) {
            StringUtil.PrintExceptionStatck(e);
        }
    }
}
