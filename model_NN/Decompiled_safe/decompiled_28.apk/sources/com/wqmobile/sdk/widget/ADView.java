package com.wqmobile.sdk.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import android.widget.ViewFlipper;
import android.widget.ViewSwitcher;
import com.energysource.szj.embeded.AdManager;
import com.millennialmedia.android.MMAdView;
import com.wqmobile.sdk.model.AdvertisementAction;
import com.wqmobile.sdk.model.AdvertisementDetail;
import com.wqmobile.sdk.model.AdvertisementProperties;
import com.wqmobile.sdk.pojoxml.util.StringUtil;
import com.wqmobile.sdk.service.ADCallback;
import com.wqmobile.sdk.service.AdsService;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ADView extends RelativeLayout implements ViewSwitcher.ViewFactory {
    private static final String IMAGE = "P";
    private static final int IMAGE_SWITCH_ID = 608131;
    private static final String MEDIA = "M";
    private static final String TEXT = "T";
    /* access modifiers changed from: private */
    public static Integer adIndex = 0;
    private static Integer currentShowIndex = -1;
    private static Boolean hasError = false;
    /* access modifiers changed from: private */
    public static Boolean hasGetFristAds = false;
    /* access modifiers changed from: private */
    public static List<AdvertisementProperties> props = new ArrayList();
    /* access modifiers changed from: private */
    public static AdsService service;
    /* access modifiers changed from: private */
    public static Integer showCount = 1;
    private Activity activity;
    private String appString = null;
    /* access modifiers changed from: private */
    public Boolean backToForeground = false;
    private Timestamp beginTime;
    private ADCallback callback = null;
    private View.OnClickListener clickLis = null;
    private ViewFlipper filpper = new ViewFlipper(getContext());
    /* access modifiers changed from: private */
    public int imageLoopIndex = 0;
    /* access modifiers changed from: private */
    public ImageSwitcher imageswitcher = new ImageSwitcher(getContext());
    private ImageView imgIcon = new ImageView(getContext());
    /* access modifiers changed from: private */
    public Boolean isFrist = false;
    private final LinearLayout layout = new LinearLayout(getContext());
    private LoadAdTask loadTask;
    private int mHeight = 0;
    private int mWidth = 0;
    /* access modifiers changed from: private */
    public Boolean running = true;
    private ShowAdTask showTask;
    private Boolean showed = false;
    /* access modifiers changed from: private */
    public Boolean tag = true;
    /* access modifiers changed from: private */
    public LinearLayout videoAd;
    private VideoView videoView = new VideoView(getContext());

    public ADView(Context context) {
        super(context);
        this.activity = (Activity) context;
        try {
            service = AdsService.getInstance(this.activity);
            setOnClickListener(getClick());
            this.imageswitcher.setId(IMAGE_SWITCH_ID);
            this.imageswitcher.setFactory(this);
        } catch (Exception e) {
            service.showMsg(e.toString());
        }
    }

    public ADView(Context context, AttributeSet attributeset) {
        super(context, attributeset);
        this.activity = (Activity) context;
        service = AdsService.getInstance(this.activity);
        setOnClickListener(getClick());
        this.imageswitcher.setId(IMAGE_SWITCH_ID);
        this.imageswitcher.setFactory(this);
    }

    public void Init(InputStream appsettings) {
        try {
            if (this.appString == null && service.getAppSettings() == null) {
                if (service.getAppSettings() == null) {
                    this.appString = service.getStringByInputStream(appsettings);
                    service.getAppSettings(this.appString);
                }
                if (service.getAppSettings().getTestMode().equals("Y")) {
                    checkDisplay();
                }
                setLayout();
            }
            loadDefImage();
        } catch (Exception e) {
            service.showMsg(e.toString());
        }
    }

    public void Init(String appsettings) {
        try {
            if (this.appString == null) {
                this.appString = appsettings;
                if (service.getAppSettings() == null) {
                    service.getAppSettings(this.appString);
                }
                if (service.getAppSettings().getTestMode().equals("Y")) {
                    checkDisplay();
                }
                setLayout();
            }
            loadDefImage();
        } catch (Exception e) {
            service.showMsg(e.toString());
        }
    }

    private void loadDefImage() {
        if (props.size() <= 0) {
            removeAllViews();
            this.layout.removeAllViews();
            ImageView iv = new ImageView(this.activity);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(-1, -1);
            InputStream is = getClass().getResourceAsStream("bb.png");
            iv.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeStream(is)));
            iv.setScaleType(ImageView.ScaleType.FIT_XY);
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.layout.addView(iv, params);
            addView(this.layout, new RelativeLayout.LayoutParams(-1, -1));
        }
    }

    private void checkDisplay() {
        int width;
        int height;
        String msg = null;
        Display display = this.activity.getWindowManager().getDefaultDisplay();
        if (service.getAppSettings().getWidth() == null) {
            width = 1;
        } else {
            width = service.getAppSettings().getWidth().intValue();
        }
        if (service.getAppSettings().getHeight() == null) {
            height = 1;
        } else {
            height = service.getAppSettings().getHeight().intValue();
        }
        if ((width > display.getWidth() && width != 1) || (height > display.getHeight() && height != 1)) {
            msg = "The non test version may not be able to get advertisement due to the width or height you set exceeds the actual screen.";
            hasError = true;
        } else if (!(width == 1 || height == 1 || width * height >= 2500)) {
            msg = "The non test version may not be able to get advertisement due to  the width*height you set is less than 2500.";
            hasError = true;
        }
        if (msg != null) {
            new AlertDialog.Builder(this.activity).setTitle("Message").setMessage(msg).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            }).create().show();
        }
    }

    /* access modifiers changed from: protected */
    public void setBackgroundColor() {
        if (service.getAppSettings().getBackgroundColor() != null) {
            setBackgroundColor(Color.parseColor("#" + service.getAppSettings().getBackgroundColor()));
            this.layout.setBackgroundColor(Color.parseColor("#" + service.getAppSettings().getBackgroundColor()));
        }
        if (service.getAppSettings().getBackgroundTransparency() != null) {
            getBackground().setAlpha(Integer.valueOf(service.getAppSettings().getBackgroundTransparency()).intValue());
            this.layout.getBackground().setAlpha(Integer.valueOf(service.getAppSettings().getBackgroundTransparency()).intValue());
        }
    }

    /* access modifiers changed from: protected */
    public final void LoadAds() {
        this.loadTask = new LoadAdTask();
        this.showTask = new ShowAdTask();
        this.loadTask.execute(null);
        this.showTask.execute(null);
    }

    private void setLayout() {
    }

    /* access modifiers changed from: private */
    public void showAds() {
        if (props.size() > 0) {
            service.showTimes("show ads before");
            AdvertisementProperties pro = getAdsAndRemove();
            if (pro != null) {
                this.showed = true;
                service.showMsg(pro.getAdID());
                if (pro.getType().equals(TEXT)) {
                    addTextAds(pro);
                    setBackgroundColor();
                } else if (pro.getType().equals(IMAGE)) {
                    addImageAds(pro);
                    setBackgroundColor();
                } else if (pro.getType().equals(MEDIA)) {
                    addMediaAds(pro);
                    setBackgroundColor();
                }
                setVisibility(0);
                this.beginTime = new Timestamp(System.currentTimeMillis());
            } else {
                loadDefImage();
            }
        } else {
            loadDefImage();
        }
        service.RefreshRunningTime();
    }

    private AdvertisementProperties getAdsAndRemove() {
        int rcount;
        AdvertisementProperties result = null;
        removeAllViews();
        Integer tempIndex = adIndex;
        if (adIndex.intValue() < 0) {
            adIndex = 0;
        }
        if (props.size() > 0) {
            if (adIndex.intValue() >= props.size()) {
                result = props.get(0);
                showCount = Integer.valueOf(showCount.intValue() + 1);
            } else if (adIndex.intValue() < service.getAppSettings().getIntNextADCount().intValue()) {
                result = props.get(adIndex.intValue());
            } else {
                adIndex = 0;
                result = props.get(0);
                showCount = Integer.valueOf(showCount.intValue() + 1);
            }
        }
        if (showCount.intValue() > Integer.valueOf(service.getAppSettings().getLoopTimes()).intValue()) {
            if (tempIndex.intValue() + 1 <= service.getAppSettings().getIntNextADCount().intValue()) {
                rcount = tempIndex.intValue() + 1;
            } else {
                rcount = service.getAppSettings().getIntNextADCount().intValue();
            }
            service.showMsg("del count:" + rcount);
            synchronized (props) {
                for (int i = 0; i < rcount; i++) {
                    if (props.size() > 0) {
                        service.showMsg("del" + props.get(0).getAdID() + "rcount:" + rcount + "index" + tempIndex);
                        props.remove(0);
                    }
                }
            }
            showCount = 1;
            adIndex = 0;
            if (props.size() > 0) {
                result = props.get(0);
            } else {
                result = null;
            }
        }
        if (result != null) {
            if (result.getIsOffline() == null || result.getIsOffline().equals("N")) {
                long dif = service.getDiffTime(this.beginTime, new Timestamp(System.currentTimeMillis()));
                service.showMsg("CurShowIndex:" + currentShowIndex.toString() + " AdIndex:" + adIndex.toString());
                if (currentShowIndex != adIndex) {
                    service.sendUserRespone(result.getAdID(), "view", String.valueOf(dif), result.getRunningCount());
                }
                result.setRunningCount(Integer.valueOf(result.getRunningCount().intValue() + 1));
            } else {
                service.viewOfflineAD(result);
            }
        }
        currentShowIndex = adIndex;
        adIndex = Integer.valueOf(adIndex.intValue() + 1);
        service.showMsg("adIndex=" + adIndex + " size=" + props.size() + " loopsize=" + service.getAppSettings().getIntNextADCount() + " show=" + showCount + " appCount=" + service.getAppSettings().getLoopTimes() + " id=" + (result == null ? "" : result.getAdID()));
        return result;
    }

    private void addTextAds(AdvertisementProperties pro) {
        removeAllViews();
        this.layout.removeAllViews();
        initText();
        AdvertisementDetail detail = pro.getText();
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1, 5.0f);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -1, 1.0f);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -1);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams3.addRule(11);
        layoutParams4.addRule(11);
        layoutParams4.addRule(12);
        RelativeLayout relativeLayout = new RelativeLayout(getContext());
        FrameLayout frameLayout = new FrameLayout(getContext());
        RelativeLayout relativeLayout2 = new RelativeLayout(getContext());
        TextView tvText = new TextView(getContext());
        ImageView imageView = new ImageView(getContext());
        relativeLayout2.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        relativeLayout.setLayoutParams(layoutParams2);
        frameLayout.setLayoutParams(layoutParams3);
        if (pro.getImageList() != null && pro.getImageList().size() > 0) {
            byte[] img = pro.getImageList().get(0);
            this.imgIcon.setImageBitmap(BitmapFactory.decodeByteArray(img, 0, img.length));
            this.imgIcon.setLayoutParams(layoutParams);
            this.imgIcon.setPadding(3, 3, 3, 3);
            this.layout.addView(this.imgIcon);
        }
        tvText.setText(detail.getText());
        tvText.setTextColor(Color.parseColor("#" + service.getAppSettings().getTextColor()));
        tvText.setLayoutParams(layoutParams5);
        tvText.setGravity(16);
        tvText.setPadding(3, 3, 3, 3);
        imageView.setImageBitmap(BitmapFactory.decodeStream(getClass().getResourceAsStream("l.png")));
        imageView.setLayoutParams(layoutParams4);
        relativeLayout2.addView(imageView);
        frameLayout.addView(relativeLayout2);
        relativeLayout.addView(tvText);
        relativeLayout.addView(frameLayout);
        this.layout.addView(relativeLayout);
        addView(this.layout);
    }

    private void initText() {
        this.layout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.layout.setOrientation(0);
        this.layout.setGravity(17);
    }

    private void addImageAds(AdvertisementProperties pro) {
        removeAllViews();
        this.layout.removeAllViews();
        this.filpper.removeAllViews();
        this.filpper.setAutoStart(true);
        if (!(pro == null || pro.getImageList() == null)) {
            for (byte[] b : pro.getImageList()) {
                Drawable d = service.getDrawableByByte(b);
                ImageView image = new ImageView(getContext());
                image.setImageDrawable(d);
                setImageViewLayoutByAppSetting(image);
                this.filpper.addView(image);
            }
        }
        this.filpper.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.filpper.setBackgroundColor(-16776961);
        this.layout.addView(this.filpper);
        this.layout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        addView(this.layout);
    }

    private void setImageViewLayoutByAppSetting(ImageView image) {
        String fit = service.getAppSettings().getFittingStrategy();
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(-1, -1);
        if (fit == null || fit.length() <= 0) {
            image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        } else {
            service.showMsg(fit);
            if (fit.equals(MMAdView.KEY_WIDTH)) {
                image.setScaleType(ImageView.ScaleType.FIT_XY);
            } else if (fit.equals(MMAdView.KEY_HEIGHT)) {
                image.setScaleType(ImageView.ScaleType.FIT_CENTER);
            } else if (fit.equals("size")) {
                image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            } else if (fit.equals("stretch")) {
                image.setScaleType(ImageView.ScaleType.FIT_XY);
            }
        }
        image.setLayoutParams(params);
    }

    private void addMediaAds(AdvertisementProperties pro) {
        this.layout.removeAllViews();
        this.videoAd = new LinearLayout(this.activity);
        this.videoAd.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        ImageView image = new ImageView(this.activity);
        image.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.videoAd.addView(image);
        byte[] img = pro.getImageList().get(0);
        image.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeByteArray(img, 0, img.length)));
        this.videoAd.setOnClickListener(getClick());
        addView(this.videoAd);
    }

    class ImageSwitchAdTask extends AsyncTask<Integer, Integer, Void> {
        ImageSwitchAdTask() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Integer... params) {
            for (int i = 0; i <= params[0].intValue(); i++) {
                publishProgress(params[0]);
                SystemClock.sleep((long) ((ADView.service.getAppSettings().getRefreshRate().intValue() / params[0].intValue()) * AdManager.AD_FILL_PARENT));
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... values) {
            super.onProgressUpdate((Object[]) values);
            if (ADView.this.imageLoopIndex < values[0].intValue()) {
                ADView.this.imageswitcher.setImageDrawable(ADView.service.getDrawableByByte(((AdvertisementProperties) ADView.props.get(0)).getImageList().get(ADView.this.imageLoopIndex)));
            }
            ADView aDView = ADView.this;
            aDView.imageLoopIndex = aDView.imageLoopIndex + 1;
        }
    }

    class ShowAdTask extends AsyncTask<Void, Integer, Void> {
        ShowAdTask() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            ADView.service.showTimes("begin show refresh tag:" + ADView.this.tag + " running:" + ADView.this.running);
            while (ADView.this.tag.booleanValue()) {
                if (!ADView.this.running.booleanValue()) {
                    SystemClock.sleep(500);
                } else if (ADView.props.size() == 0) {
                    SystemClock.sleep(500);
                } else {
                    publishProgress(1);
                    int i = 0;
                    while (i <= ADView.service.getAppSettings().getRefreshRate().intValue()) {
                        SystemClock.sleep(1000);
                        if (!ADView.this.running.booleanValue()) {
                            continue;
                            break;
                        } else if (!ADView.this.tag.booleanValue()) {
                            ADView.service.showTimes("end show refresh tag:" + ADView.this.tag + " running:" + ADView.this.running);
                            return null;
                        } else {
                            i++;
                        }
                    }
                    continue;
                }
                ADView.service.showTimes("running:" + ADView.this.running + " size:" + ADView.props.size() + "aindex:" + ADView.adIndex + "showCount:" + ADView.showCount + ";");
            }
            ADView.service.showTimes("end show refresh tag:" + ADView.this.tag + " running:" + ADView.this.running);
            return null;
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... values) {
            super.onProgressUpdate((Object[]) values);
            ADView.this.showAds();
        }
    }

    class LoadAdTask extends AsyncTask<Void, Integer, Void> {
        LoadAdTask() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            ADView.service.showTimes("begin load refresh tag:" + ADView.this.tag + " running:" + ADView.this.running);
            int i = 1;
            while (ADView.this.tag.booleanValue()) {
                if (ADView.this.running.booleanValue() && ADView.this.isFrist.booleanValue()) {
                    if (ADView.service.getAppSettings() != null) {
                        try {
                            if (!ADView.service.WasRefused().booleanValue()) {
                                if (!ADView.hasGetFristAds.booleanValue()) {
                                    ADView.this.getAds();
                                    ADView.hasGetFristAds = true;
                                } else if (ADView.hasGetFristAds.booleanValue() && ADView.props.size() <= 2) {
                                    ADView.this.getAds();
                                }
                            } else if (ADView.this.backToForeground.booleanValue()) {
                                ADView.this.backToForeground = false;
                                ADView.this.getAds();
                            }
                            if (ADView.props.size() == 0 && AdsService.cacheOffline != null) {
                                synchronized (ADView.props) {
                                    for (AdvertisementProperties ad : AdsService.cacheOffline.getOfflineADs()) {
                                        ADView.props.add(ad);
                                    }
                                }
                            }
                            for (int j = 0; j <= ADView.service.getAppSettings().getRefreshRate().intValue(); j++) {
                                SystemClock.sleep(950);
                                if (!ADView.this.tag.booleanValue()) {
                                    ADView.service.showTimes("break show refresh tag:" + ADView.this.tag + " running:" + ADView.this.running);
                                    return null;
                                }
                            }
                        } catch (Exception e) {
                            Exception ex = e;
                            StringUtil.PrintExceptionStatck(ex);
                            ADView.service.showMsg("method loadDoing:" + ex.toString());
                        }
                        i++;
                    } else {
                        SystemClock.sleep(900);
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... values) {
            super.onProgressUpdate((Object[]) values);
        }
    }

    /* access modifiers changed from: private */
    public void getAds() {
        if (this.callback == null) {
            this.callback = new ADCallback() {
                public void ADLoaded(AdvertisementProperties pros) {
                    if (ADView.props.size() < 1) {
                        ADView.props.add(pros);
                    } else {
                        ADView.props.add(pros);
                    }
                }
            };
        }
        service.getAds(getContext(), this.callback);
    }

    /* access modifiers changed from: private */
    public void openVideo(String strPath) {
        this.layout.removeAllViews();
        FrameLayout layout2 = new FrameLayout(this.activity);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(-1, -1);
        layout2.setForegroundGravity(17);
        layout2.setBackgroundColor(-16777216);
        LinearLayout inner = new LinearLayout(this.activity);
        inner.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        inner.setGravity(17);
        layout2.addView(inner);
        this.activity.addContentView(layout2, params);
        this.videoView.setVideoPath(strPath);
        inner.addView(this.videoView);
        layout2.setOnClickListener(getClick());
        layout2.setForegroundGravity(17);
        this.videoView.start();
    }

    private void openActionVideo(String strPath) {
        FrameLayout layout2 = new FrameLayout(this.activity);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(-1, -1);
        layout2.setForegroundGravity(17);
        layout2.setBackgroundColor(-16777216);
        LinearLayout inner = new LinearLayout(this.activity);
        inner.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        inner.setGravity(17);
        layout2.addView(inner);
        this.activity.addContentView(layout2, params);
        this.videoView.setVideoPath(strPath);
        inner.addView(this.videoView);
        layout2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((FrameLayout) v).removeAllViews();
                v.setVisibility(8);
                ADView.this.running = true;
            }
        });
        layout2.setForegroundGravity(17);
        this.running = false;
        this.videoView.start();
    }

    /* access modifiers changed from: private */
    public void doEvent() {
        Integer index;
        if (props.size() > 0) {
            if (adIndex.intValue() <= 0) {
                index = 0;
            } else {
                index = Integer.valueOf(adIndex.intValue() - 1);
            }
            AdvertisementProperties pro = props.get(index.intValue());
            AdvertisementAction action = pro.getAction();
            if (pro.getIsOffline().equals("Y")) {
                service.doOfflineEvent(pro);
                return;
            }
            service.sendUserRespone(pro.getAdID(), "click", String.valueOf(service.getDiffTime(this.beginTime, new Timestamp(System.currentTimeMillis()))), pro.getRunningCount());
            if (pro.getAction().getActionType().equals("1")) {
                openUrl(pro.getAction().getClickToURL().getURL());
            } else if (!pro.getAction().getActionType().equals("2") && !pro.getAction().getActionType().equals("3")) {
                if (pro.getAction().getActionType().equals("4")) {
                    service.openUrl(this.activity, pro.getAction().getDownloadAndroid().getTarget());
                } else if (pro.getAction().getActionType().equals("5")) {
                    openActionVideo(action.getClickToMedia().getTarget());
                } else if (pro.getAction().getActionType().equals("6")) {
                    service.callPhone(getContext(), action.getClickToCall().getPhoneNumber());
                } else if (pro.getAction().getActionType().equals("7")) {
                    service.openUrl(this.activity, action.getClickToMap().getMapLink());
                } else if (pro.getAction().getActionType().equals("8")) {
                    openUrl(action.getClickToSearch().getSearchLink());
                } else if (pro.getAction().getActionType().equals("9")) {
                    this.running = false;
                    sendSMS(pro.getAction().getClickToSMS().getTarget(), pro.getAction().getClickToSMS().getContent());
                } else if (pro.getAction().getActionType().equals("10")) {
                    this.running = false;
                    sendMail(action.getClickToMail().getTarget(), action.getClickToMail().getContent().getTitle(), action.getClickToMail().getContent().getBody());
                }
            }
        }
    }

    private void sendMail(final String mailAddress, final String title, final String content) {
        DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case -2:
                        ADView.adIndex = Integer.valueOf(ADView.adIndex.intValue() - 1);
                        ADView.this.running = true;
                        return;
                    case MMAdView.REFRESH_INTERVAL_OFF:
                        ADView.service.sendMail(mailAddress, title, content);
                        return;
                    default:
                        return;
                }
            }
        };
        new AlertDialog.Builder(getContext()).setMessage("你要向" + mailAddress + "发送邮件吗？").setNegativeButton("否", ocl).setPositiveButton("是", ocl).create().show();
    }

    private void sendSMS(final String telNumber, final String content) {
        DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case -2:
                        ADView.adIndex = Integer.valueOf(ADView.adIndex.intValue() - 1);
                        ADView.this.running = true;
                        return;
                    case MMAdView.REFRESH_INTERVAL_OFF:
                        ADView.service.sendSMS(ADView.this.getContext(), telNumber, content);
                        return;
                    default:
                        return;
                }
            }
        };
        new AlertDialog.Builder(getContext()).setMessage("你要向" + telNumber + "发送短信吗？").setNegativeButton("否", ocl).setPositiveButton("是", ocl).create().show();
    }

    private void openUrl(String url) {
        if (!url.startsWith("http:")) {
            url = "http://" + url;
        }
        if (service.getAppSettings().getUseInternalBrowser() == null || !service.getAppSettings().getUseInternalBrowser().booleanValue()) {
            service.openUrl(this.activity, url);
            return;
        }
        this.running = false;
        openBuildInWebView(url);
    }

    private void openBuildInWebView(String url) {
        final FrameLayout layout2 = new FrameLayout(this.activity);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(-1, -1);
        params.gravity = 5;
        layout2.setLayoutParams(params);
        layout2.setBackgroundColor(-1);
        LinearLayout container = new LinearLayout(this.activity);
        new LinearLayout.LayoutParams(-1, -1);
        container.setGravity(5);
        container.setPadding(5, 5, 5, 5);
        Button btn = new Button(this.activity);
        btn.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        btn.setText(" X ");
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                layout2.setVisibility(8);
                ADView.this.running = true;
            }
        });
        WebView webView = new WebView(this.activity);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        layout2.addView(webView);
        container.addView(btn);
        layout2.addView(container);
        webView.loadUrl(url);
        this.activity.addContentView(layout2, params);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);
        if (!hasError.booleanValue()) {
            if (visibility == 0) {
                this.running = true;
                if (!this.tag.booleanValue()) {
                    this.backToForeground = true;
                    this.tag = true;
                    LoadAds();
                } else {
                    this.backToForeground = false;
                }
            } else if (visibility == 8) {
                this.backToForeground = false;
                this.running = false;
                this.tag = false;
                if (this.loadTask != null) {
                    this.loadTask.cancel(true);
                }
                if (this.showTask != null) {
                    this.showTask.cancel(true);
                }
                this.layout.removeAllViews();
                service.saveOfflineAds();
                loadDefImage();
                adIndex = Integer.valueOf(adIndex.intValue() - 1);
            }
        }
        service.showMsg("visibility=" + visibility);
    }

    public View makeView() {
        return new ImageView(this.activity);
    }

    private View.OnClickListener getClick() {
        if (this.clickLis == null) {
            this.clickLis = new View.OnClickListener() {
                public void onClick(View v) {
                    if (ADView.props.size() > 0) {
                        AdvertisementProperties pro = (AdvertisementProperties) ADView.props.get(0);
                        if (v == ADView.this.videoAd) {
                            String path = pro.getMedia().getUrl();
                            Log.d("Video path", path);
                            ADView.this.openVideo(path);
                            return;
                        }
                        if (v.getClass() == FrameLayout.class) {
                            ((FrameLayout) v).removeAllViews();
                            v.setVisibility(8);
                            ADView.this.running = true;
                        }
                        ADView.this.doEvent();
                    }
                }
            };
        }
        return this.clickLis;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        if (!hasError.booleanValue() && !this.isFrist.booleanValue()) {
            service.getAppSettings().setWidth(Integer.valueOf(r));
            service.getAppSettings().setHeight(Integer.valueOf(b));
            this.isFrist = true;
            service.getAppSettings(service.getAppSettings());
            LoadAds();
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (service.getAppSettings() != null) {
            service.setSize(w, h);
        }
    }
}
