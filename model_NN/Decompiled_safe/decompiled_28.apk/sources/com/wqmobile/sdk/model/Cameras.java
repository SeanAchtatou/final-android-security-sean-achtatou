package com.wqmobile.sdk.model;

import java.util.ArrayList;
import java.util.List;

public class Cameras {
    private List<Camera> Cameras = new ArrayList();
    private Integer DeviceCount;

    public Integer getDeviceCount() {
        return this.DeviceCount;
    }

    public void setDeviceCount(Integer deviceCount) {
        this.DeviceCount = deviceCount;
    }

    public List<Camera> getCameras() {
        return this.Cameras;
    }

    public void setCameras(List<Camera> cameras) {
        this.Cameras = cameras;
    }
}
