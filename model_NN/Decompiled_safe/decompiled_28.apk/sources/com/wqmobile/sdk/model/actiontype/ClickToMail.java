package com.wqmobile.sdk.model.actiontype;

public class ClickToMail {
    private MailContent Content;
    private String Target;

    public String getTarget() {
        return this.Target;
    }

    public void setTarget(String target) {
        this.Target = target;
    }

    public MailContent getContent() {
        return this.Content;
    }

    public void setContent(MailContent content) {
        this.Content = content;
    }
}
