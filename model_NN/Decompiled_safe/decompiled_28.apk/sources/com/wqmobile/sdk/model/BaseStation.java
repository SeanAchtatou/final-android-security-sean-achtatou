package com.wqmobile.sdk.model;

public class BaseStation {
    private String BaseStationLatitude;
    private String BaseStationLongitude;
    private String GSMCellID;
    private String GSMLocationAreaCode;
    private String ID;
    private String SignalStrength;

    public String getID() {
        return this.ID;
    }

    public void setID(String iD) {
        this.ID = iD;
    }

    public String getSignalStrength() {
        return this.SignalStrength;
    }

    public void setSignalStrength(String signalStrength) {
        this.SignalStrength = signalStrength;
    }

    public String getBaseStationLatitude() {
        return this.BaseStationLatitude;
    }

    public void setBaseStationLatitude(String baseStationLatitude) {
        this.BaseStationLatitude = baseStationLatitude;
    }

    public String getBaseStationLongitude() {
        return this.BaseStationLongitude;
    }

    public void setBaseStationLongitude(String baseStationLongitude) {
        this.BaseStationLongitude = baseStationLongitude;
    }

    public String getGSMCellID() {
        return this.GSMCellID;
    }

    public void setGSMCellID(String gSMCellID) {
        this.GSMCellID = gSMCellID;
    }

    public String getGSMLocationAreCode() {
        return this.GSMLocationAreaCode;
    }

    public void setGSMLocationAreCode(String gSMLocationAreCode) {
        this.GSMLocationAreaCode = gSMLocationAreCode;
    }
}
