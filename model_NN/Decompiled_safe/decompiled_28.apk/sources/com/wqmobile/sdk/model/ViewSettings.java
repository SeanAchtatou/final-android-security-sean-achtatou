package com.wqmobile.sdk.model;

public class ViewSettings {
    private Integer Height;
    private Integer Width;

    public ViewSettings() {
    }

    public ViewSettings(Integer width, Integer height) {
        this.Width = width;
        this.Height = height;
    }

    public Integer getWidth() {
        return this.Width;
    }

    public void setWidth(Integer width) {
        this.Width = width;
    }

    public Integer getHeight() {
        return this.Height;
    }

    public void setHeight(Integer height) {
        this.Height = height;
    }
}
