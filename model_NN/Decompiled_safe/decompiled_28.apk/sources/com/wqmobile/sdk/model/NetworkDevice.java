package com.wqmobile.sdk.model;

public class NetworkDevice {
    private String ConnectionType;
    private String IP;
    private String MAC;
    private String Speed;
    private String Status;

    public String getConnectionType() {
        return this.ConnectionType;
    }

    public void setConnectionType(String connectionType) {
        this.ConnectionType = connectionType;
    }

    public String getStatus() {
        return this.Status;
    }

    public void setStatus(String status) {
        this.Status = status;
    }

    public String getSpeed() {
        return this.Speed;
    }

    public void setSpeed(String speed) {
        this.Speed = speed;
    }

    public String getIP() {
        return this.IP;
    }

    public void setIP(String iP) {
        this.IP = iP;
    }

    public String getMAC() {
        return this.MAC;
    }

    public void setMAC(String mAC) {
        this.MAC = mAC;
    }
}
