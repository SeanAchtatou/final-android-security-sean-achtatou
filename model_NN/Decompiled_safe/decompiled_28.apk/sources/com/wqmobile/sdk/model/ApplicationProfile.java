package com.wqmobile.sdk.model;

public class ApplicationProfile {
    private Applications InstalledApplications = new Applications();
    private Applications RunningApplications = new Applications();

    public Applications getRunningApplications() {
        return this.RunningApplications;
    }

    public void setRunningApplications(Applications runningApplications) {
        this.RunningApplications = runningApplications;
    }

    public Applications getInstalledApplications() {
        return this.InstalledApplications;
    }

    public void setInstalledApplications(Applications installedApplications) {
        this.InstalledApplications = installedApplications;
    }
}
