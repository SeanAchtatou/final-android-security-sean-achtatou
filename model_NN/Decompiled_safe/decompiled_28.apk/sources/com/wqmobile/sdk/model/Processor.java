package com.wqmobile.sdk.model;

public class Processor {
    private String Frequency;
    private String ID;
    private String Manufacturer;
    private String Model;

    public String getManufacturer() {
        return this.Manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.Manufacturer = manufacturer;
    }

    public String getModel() {
        return this.Model;
    }

    public void setModel(String model) {
        this.Model = model;
    }

    public String getFrequency() {
        return this.Frequency;
    }

    public void setFrequency(String frequency) {
        this.Frequency = frequency;
    }

    public String getID() {
        return this.ID;
    }

    public void setID(String iD) {
        this.ID = iD;
    }
}
