package com.wqmobile.sdk.model;

public class ClientProfile {
    private String AppID;
    private ApplicationProfile ApplicationProfile;
    private DemographicInfo DemographicInfo;
    private DeviceProfile DeviceProfile = new DeviceProfile();
    private DeviceStatus DeviceStatus = new DeviceStatus();
    private GeographicInfo GeographicInfo = new GeographicInfo();
    private String SDKVersion;

    public String getAppID() {
        return this.AppID;
    }

    public void setAppID(String appID) {
        this.AppID = appID;
    }

    public DeviceProfile getDeviceProfile() {
        return this.DeviceProfile;
    }

    public void setDeviceProfile(DeviceProfile deviceProfile) {
        this.DeviceProfile = deviceProfile;
    }

    public DeviceStatus getDeviceStatus() {
        return this.DeviceStatus;
    }

    public void setDeviceStatus(DeviceStatus deviceStatus) {
        this.DeviceStatus = deviceStatus;
    }

    public ApplicationProfile getApplicationProfile() {
        return this.ApplicationProfile;
    }

    public void setApplicationProfile(ApplicationProfile applicationProfile) {
        this.ApplicationProfile = applicationProfile;
    }

    public GeographicInfo getGeographicInfo() {
        return this.GeographicInfo;
    }

    public void setGeographicInfo(GeographicInfo geographicInfo) {
        this.GeographicInfo = geographicInfo;
    }

    public DemographicInfo getDemographicInfo() {
        return this.DemographicInfo;
    }

    public void setDemographicInfo(DemographicInfo demographicInfo) {
        this.DemographicInfo = demographicInfo;
    }

    public String getSDKVersion() {
        return this.SDKVersion;
    }

    public void setSDKVersion(String sDKVersion) {
        this.SDKVersion = sDKVersion;
    }
}
