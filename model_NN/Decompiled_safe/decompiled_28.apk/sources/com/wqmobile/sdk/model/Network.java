package com.wqmobile.sdk.model;

public class Network {
    private String CountryISO;
    private Operator Operator = new Operator();

    public String getCountryISO() {
        return this.CountryISO;
    }

    public void setCountryISO(String countryISO) {
        this.CountryISO = countryISO;
    }

    public Operator getOperator() {
        return this.Operator;
    }

    public void setOperator(Operator operator) {
        this.Operator = operator;
    }
}
