package com.wqmobile.sdk.model;

public class InputMethods {
    private Boolean Keyboard;
    private Boolean Keypad;
    private Boolean Keys;
    private Boolean Mouse;
    private Boolean MultiTouch;
    private Boolean SingleTouch;

    public Boolean getKeys() {
        return this.Keys;
    }

    public void setKeys(Boolean keys) {
        this.Keys = keys;
    }

    public Boolean getKeypad() {
        return this.Keypad;
    }

    public void setKeypad(Boolean keypad) {
        this.Keypad = keypad;
    }

    public Boolean getKeyboard() {
        return this.Keyboard;
    }

    public void setKeyboard(Boolean keyboard) {
        this.Keyboard = keyboard;
    }

    public Boolean getSingleTouch() {
        return this.SingleTouch;
    }

    public void setSingleTouch(Boolean singleTouch) {
        this.SingleTouch = singleTouch;
    }

    public Boolean getMultiTouch() {
        return this.MultiTouch;
    }

    public void setMultiTouch(Boolean multiTouch) {
        this.MultiTouch = multiTouch;
    }

    public Boolean getMouse() {
        return this.Mouse;
    }

    public void setMouse(Boolean mouse) {
        this.Mouse = mouse;
    }
}
