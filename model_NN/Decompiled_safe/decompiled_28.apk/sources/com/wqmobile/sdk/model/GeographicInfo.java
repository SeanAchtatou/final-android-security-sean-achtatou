package com.wqmobile.sdk.model;

public class GeographicInfo {
    private String Latitude;
    private String Longitude;
    private String ObtainMethod;

    public String getObtainMethod() {
        return this.ObtainMethod;
    }

    public void setObtainMethod(String obtainMethod) {
        this.ObtainMethod = obtainMethod;
    }

    public String getLatitude() {
        return this.Latitude;
    }

    public void setLatitude(String latitude) {
        this.Latitude = latitude;
    }

    public String getLongitude() {
        return this.Longitude;
    }

    public void setLongitude(String longitude) {
        this.Longitude = longitude;
    }
}
