package com.wqmobile.sdk.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AdvertisementProperties {
    private AdvertisementAction Action = new AdvertisementAction();
    private String AdID;
    private AdvertisementDetail Image = new AdvertisementDetail();
    private String IsOffline;
    private AdvertisementDetail Media = new AdvertisementDetail();
    private String OfflineAdExpireDate;
    private AdvertisementDetail Text = new AdvertisementDetail();
    private String Type;
    private List<byte[]> imageList = new ArrayList();
    private Integer runningCount = 0;

    public String getAdID() {
        return this.AdID;
    }

    public void setAdID(String adID) {
        this.AdID = adID;
    }

    public String getType() {
        return this.Type;
    }

    public void setType(String type) {
        this.Type = type;
    }

    public AdvertisementDetail getText() {
        return this.Text;
    }

    public void setText(AdvertisementDetail text) {
        this.Text = text;
    }

    public AdvertisementDetail getImage() {
        return this.Image;
    }

    public void setImage(AdvertisementDetail image) {
        this.Image = image;
    }

    public AdvertisementDetail getMedia() {
        return this.Media;
    }

    public void setMedia(AdvertisementDetail media) {
        this.Media = media;
    }

    public AdvertisementAction getAction() {
        return this.Action;
    }

    public void setAction(AdvertisementAction action) {
        this.Action = action;
    }

    public List<byte[]> getImageList() {
        return this.imageList;
    }

    public void setImageList(List<byte[]> imageList2) {
        this.imageList = imageList2;
    }

    public Integer getRunningCount() {
        return this.runningCount;
    }

    public void setRunningCount(Integer runningCount2) {
        this.runningCount = runningCount2;
    }

    public void addImage(byte[] img) {
        this.imageList.add(img);
    }

    public String getOfflineAdExpireDate() {
        return this.OfflineAdExpireDate;
    }

    public void setOfflineAdExpireDate(String offlineAdExpireDate) {
        this.OfflineAdExpireDate = offlineAdExpireDate;
    }

    public Date getOfflineAdExpireDateTime() {
        try {
            return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(this.OfflineAdExpireDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getIsOffline() {
        return this.IsOffline;
    }

    public void setIsOffline(String isOffline) {
        this.IsOffline = isOffline;
    }
}
