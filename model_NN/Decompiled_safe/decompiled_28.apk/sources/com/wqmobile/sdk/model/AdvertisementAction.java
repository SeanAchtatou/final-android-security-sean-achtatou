package com.wqmobile.sdk.model;

import com.wqmobile.sdk.model.actiontype.ClickToCall;
import com.wqmobile.sdk.model.actiontype.ClickToMail;
import com.wqmobile.sdk.model.actiontype.ClickToMap;
import com.wqmobile.sdk.model.actiontype.ClickToMedia;
import com.wqmobile.sdk.model.actiontype.ClickToSMS;
import com.wqmobile.sdk.model.actiontype.ClickToSearch;
import com.wqmobile.sdk.model.actiontype.ClickToURL;
import com.wqmobile.sdk.model.actiontype.DownloadAndroid;

public class AdvertisementAction {
    private String ActionType = "";
    private ClickToCall ClickToCall = new ClickToCall();
    private ClickToMail ClickToMail = new ClickToMail();
    private ClickToMap ClickToMap = new ClickToMap();
    private ClickToMedia ClickToMedia = new ClickToMedia();
    private ClickToSMS ClickToSMS = new ClickToSMS();
    private ClickToSearch ClickToSearch = new ClickToSearch();
    private ClickToURL ClickToURL = new ClickToURL();
    private DownloadAndroid DownloadAndroid = new DownloadAndroid();

    public String getActionType() {
        return this.ActionType;
    }

    public void setActionType(String actionType) {
        this.ActionType = actionType;
    }

    public ClickToSearch getClickToSearch() {
        return this.ClickToSearch;
    }

    public void setClickToSearch(ClickToSearch clickToSearch) {
        this.ClickToSearch = clickToSearch;
    }

    public ClickToMap getClickToMap() {
        return this.ClickToMap;
    }

    public void setClickToMap(ClickToMap clickToMap) {
        this.ClickToMap = clickToMap;
    }

    public ClickToURL getClickToURL() {
        return this.ClickToURL;
    }

    public void setClickToURL(ClickToURL clickToURL) {
        this.ClickToURL = clickToURL;
    }

    public ClickToCall getClickToCall() {
        return this.ClickToCall;
    }

    public void setClickToCall(ClickToCall clickToCall) {
        this.ClickToCall = clickToCall;
    }

    public ClickToMail getClickToMail() {
        return this.ClickToMail;
    }

    public void setClickToMail(ClickToMail clickToMail) {
        this.ClickToMail = clickToMail;
    }

    public ClickToMedia getClickToMedia() {
        return this.ClickToMedia;
    }

    public void setClickToMedia(ClickToMedia clickToMedia) {
        this.ClickToMedia = clickToMedia;
    }

    public ClickToSMS getClickToSMS() {
        return this.ClickToSMS;
    }

    public void setClickToSMS(ClickToSMS clickToSMS) {
        this.ClickToSMS = clickToSMS;
    }

    public DownloadAndroid getDownloadAndroid() {
        return this.DownloadAndroid;
    }

    public void setDownloadAndroid(DownloadAndroid downloadAndroid) {
        this.DownloadAndroid = downloadAndroid;
    }
}
