package com.wqmobile.sdk.model;

public class Profiles {
    private String[] Profile = new String[0];
    private Integer ProfileCount;

    public Integer getProfileCount() {
        return this.ProfileCount;
    }

    public void setProfileCount(Integer profileCount) {
        this.ProfileCount = profileCount;
    }

    public String[] getProfile() {
        return this.Profile;
    }

    public void setProfile(String[] profile) {
        this.Profile = profile;
    }
}
