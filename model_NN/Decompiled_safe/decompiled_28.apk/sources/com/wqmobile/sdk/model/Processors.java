package com.wqmobile.sdk.model;

import java.util.ArrayList;
import java.util.List;

public class Processors {
    private Integer Count;
    private List<Processor> Processors = new ArrayList();

    public Integer getCount() {
        return this.Count;
    }

    public void setCount(Integer count) {
        this.Count = count;
    }

    public List<Processor> getProcessors() {
        return this.Processors;
    }

    public void setProcessors(List<Processor> processors) {
        this.Processors = processors;
    }
}
