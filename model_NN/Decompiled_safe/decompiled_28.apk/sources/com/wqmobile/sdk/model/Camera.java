package com.wqmobile.sdk.model;

public class Camera {
    private String Effects;
    private String FlashMode;
    private String FocusMode;
    private String Manufacturer;
    private String PictureFormat;
    private String PictureSize;
    private String Position;
    private String Type;
    private String ZoomValue;

    public String getPosition() {
        return this.Position;
    }

    public void setPosition(String position) {
        this.Position = position;
    }

    public String getManufacturer() {
        return this.Manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.Manufacturer = manufacturer;
    }

    public String getType() {
        return this.Type;
    }

    public void setType(String type) {
        this.Type = type;
    }

    public String getPictureSize() {
        return this.PictureSize;
    }

    public void setPictureSize(String pictureSize) {
        this.PictureSize = pictureSize;
    }

    public String getZoomValue() {
        return this.ZoomValue;
    }

    public void setZoomValue(String zoomValue) {
        this.ZoomValue = zoomValue;
    }

    public String getFlashMode() {
        return this.FlashMode;
    }

    public void setFlashMode(String flashMode) {
        this.FlashMode = flashMode;
    }

    public String getFocusMode() {
        return this.FocusMode;
    }

    public void setFocusMode(String focusMode) {
        this.FocusMode = focusMode;
    }

    public String getPictureFormat() {
        return this.PictureFormat;
    }

    public void setPictureFormat(String pictureFormat) {
        this.PictureFormat = pictureFormat;
    }

    public String getEffects() {
        return this.Effects;
    }

    public void setEffects(String effects) {
        this.Effects = effects;
    }
}
