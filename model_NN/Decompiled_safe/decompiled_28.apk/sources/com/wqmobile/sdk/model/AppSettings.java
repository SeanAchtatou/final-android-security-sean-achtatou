package com.wqmobile.sdk.model;

public class AppSettings {
    private String AcceptsOfflineAD;
    private String AppID;
    private String AppStoreURL;
    private String BackgroundColor;
    private String BackgroundTransparency;
    private String FittingStrategy;
    private Integer Height;
    private String IP;
    private String LoopTimes;
    private String NextADCount;
    private String OfflineADCount;
    private String PublisherID;
    private Integer RefreshRate;
    private String TestMode;
    private String TextColor;
    private Integer TopLeftX;
    private Integer TopLeftY;
    private String URL;
    private Boolean UseInternalBrowser;
    private String UseLocationInfo;
    private Integer Width;

    public String getAppID() {
        return this.AppID;
    }

    public void setAppID(String appID) {
        this.AppID = appID;
    }

    public String getPublisherID() {
        return this.PublisherID;
    }

    public void setPublisherID(String publisherID) {
        this.PublisherID = publisherID;
    }

    public String getURL() {
        return this.URL;
    }

    public void setURL(String uRL) {
        this.URL = uRL;
    }

    public String getAppStoreURL() {
        return this.AppStoreURL;
    }

    public void setAppStoreURL(String appStoreURL) {
        this.AppStoreURL = appStoreURL;
    }

    public String getUseLocationInfo() {
        return this.UseLocationInfo;
    }

    public void setUseLocationInfo(String useLocationInfo) {
        this.UseLocationInfo = useLocationInfo;
    }

    public Integer getRefreshRate() {
        return this.RefreshRate;
    }

    public void setRefreshRate(Integer refreshRate) {
        this.RefreshRate = refreshRate;
    }

    public String getTestMode() {
        return this.TestMode;
    }

    public void setTestMode(String testMode) {
        this.TestMode = testMode;
    }

    public String getNextADCount() {
        return this.NextADCount;
    }

    public Integer getIntNextADCount() {
        int count = 1;
        try {
            return Integer.valueOf(this.NextADCount);
        } catch (Exception e) {
            return count;
        }
    }

    public void setNextADCount(String nextADCount) {
        this.NextADCount = nextADCount;
    }

    public String getLoopTimes() {
        return this.LoopTimes;
    }

    public void setLoopTimes(String loopTimes) {
        this.LoopTimes = loopTimes;
    }

    public String getAcceptsOfflineAD() {
        return this.AcceptsOfflineAD;
    }

    public void setAcceptsOfflineAD(String acceptsOfflineAD) {
        this.AcceptsOfflineAD = acceptsOfflineAD;
    }

    public String getBackgroundColor() {
        return this.BackgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.BackgroundColor = backgroundColor;
    }

    public String getTextColor() {
        return this.TextColor;
    }

    public void setTextColor(String textColor) {
        this.TextColor = textColor;
    }

    public String getBackgroundTransparency() {
        return this.BackgroundTransparency;
    }

    public void setBackgroundTransparency(String backgroundTransparency) {
        this.BackgroundTransparency = backgroundTransparency;
    }

    public Integer getWidth() {
        return this.Width;
    }

    public void setWidth(Integer width) {
        this.Width = width;
    }

    public Integer getHeight() {
        return this.Height;
    }

    public void setHeight(Integer height) {
        this.Height = height;
    }

    public Integer getTopLeftX() {
        return this.TopLeftX;
    }

    public void setTopLeftX(Integer topLeftX) {
        this.TopLeftX = topLeftX;
    }

    public Integer getTopLeftY() {
        return this.TopLeftY;
    }

    public void setTopLeftY(Integer topLeftY) {
        this.TopLeftY = topLeftY;
    }

    public Boolean getUseInternalBrowser() {
        return this.UseInternalBrowser;
    }

    public void setUseInternalBrowser(Boolean useInternalBrowser) {
        this.UseInternalBrowser = useInternalBrowser;
    }

    public String getIP() {
        return this.IP;
    }

    public void setIP(String iP) {
        this.IP = iP;
    }

    public String getOfflineADCount() {
        return this.OfflineADCount;
    }

    public void setOfflineADCount(String offlineADCount) {
        this.OfflineADCount = offlineADCount;
    }

    public int getIntOfflineADCount() {
        try {
            return Integer.valueOf(this.OfflineADCount).intValue();
        } catch (Exception e) {
            return 1;
        }
    }

    public String getFittingStrategy() {
        return this.FittingStrategy;
    }

    public void setFittingStrategy(String fittingStrategy) {
        this.FittingStrategy = fittingStrategy;
    }
}
