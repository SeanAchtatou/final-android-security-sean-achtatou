package com.wqmobile.sdk.model;

public class Services {
    private String Configuration;
    private Integer ServiceCount;
    Service[] Services = new Service[0];

    public Integer getServiceCount() {
        return this.ServiceCount;
    }

    public void setServiceCount(Integer serviceCount) {
        this.ServiceCount = serviceCount;
    }

    public Service[] getServices() {
        return this.Services;
    }

    public void setServices(Service[] services) {
        this.Services = services;
    }

    public String getConfiguration() {
        return this.Configuration;
    }

    public void setConfiguration(String configuration) {
        this.Configuration = configuration;
    }
}
