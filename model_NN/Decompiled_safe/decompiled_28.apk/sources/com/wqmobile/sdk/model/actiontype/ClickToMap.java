package com.wqmobile.sdk.model.actiontype;

public class ClickToMap {
    private String MapLink;

    public String getMapLink() {
        return this.MapLink;
    }

    public void setMapLink(String mapLink) {
        this.MapLink = mapLink;
    }
}
