package com.wqmobile.sdk.model;

public class Screen {
    private Integer ColorBits;
    private Integer HorizontalResolution;
    private Integer HorizontalSize;
    private String Manufacturer;
    private String Model;
    private String PartNumber;
    private String Type;
    private Integer VerticalResolution;
    private Integer VerticalSize;

    public String getType() {
        return this.Type;
    }

    public void setType(String type) {
        this.Type = type;
    }

    public String getManufacturer() {
        return this.Manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.Manufacturer = manufacturer;
    }

    public String getModel() {
        return this.Model;
    }

    public void setModel(String model) {
        this.Model = model;
    }

    public String getPartNumber() {
        return this.PartNumber;
    }

    public void setPartNumber(String partNumber) {
        this.PartNumber = partNumber;
    }

    public Integer getHorizontalSize() {
        return this.HorizontalSize;
    }

    public void setHorizontalSize(Integer horizontalSize) {
        this.HorizontalSize = horizontalSize;
    }

    public Integer getVerticalSize() {
        return this.VerticalSize;
    }

    public void setVerticalSize(Integer verticalSize) {
        this.VerticalSize = verticalSize;
    }

    public Integer getHorizontalResolution() {
        return this.HorizontalResolution;
    }

    public void setHorizontalResolution(Integer horizontalResolution) {
        this.HorizontalResolution = horizontalResolution;
    }

    public Integer getVerticalResolution() {
        return this.VerticalResolution;
    }

    public void setVerticalResolution(Integer verticalResolution) {
        this.VerticalResolution = verticalResolution;
    }

    public Integer getColorBits() {
        return this.ColorBits;
    }

    public void setColorBits(Integer colorBits) {
        this.ColorBits = colorBits;
    }
}
