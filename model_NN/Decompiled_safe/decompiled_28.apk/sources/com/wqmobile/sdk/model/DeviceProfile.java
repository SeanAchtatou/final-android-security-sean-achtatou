package com.wqmobile.sdk.model;

public class DeviceProfile {
    private HardwareProfile HardwareProfile;
    private NetworkDevices NetworkDevices;
    private PhoneInfo PhoneInfo;
    private SystemProfile SystemProfile;

    public PhoneInfo getPhoneInfo() {
        return this.PhoneInfo;
    }

    public void setPhoneInfo(PhoneInfo phoneInfo) {
        this.PhoneInfo = phoneInfo;
    }

    public HardwareProfile getHardwareProfile() {
        return this.HardwareProfile;
    }

    public void setHardwareProfile(HardwareProfile hardwareProfile) {
        this.HardwareProfile = hardwareProfile;
    }

    public SystemProfile getSystemProfile() {
        return this.SystemProfile;
    }

    public void setSystemProfile(SystemProfile systemProfile) {
        this.SystemProfile = systemProfile;
    }

    public NetworkDevices getNetworkDevices() {
        return this.NetworkDevices;
    }

    public void setNetworkDevices(NetworkDevices networkDevices) {
        this.NetworkDevices = networkDevices;
    }
}
