package com.wqmobile.sdk.model;

import java.util.ArrayList;
import java.util.List;

public class Sensors {
    private Integer SensorCount;
    private List<Sensor> Sensors = new ArrayList();

    public Integer getSensorCount() {
        return this.SensorCount;
    }

    public void setSensorCount(Integer sensorCount) {
        this.SensorCount = sensorCount;
    }

    public List<Sensor> getSensors() {
        return this.Sensors;
    }

    public void setSensors(List<Sensor> sensors) {
        this.Sensors = sensors;
    }
}
