package com.wqmobile.sdk.model;

import java.util.ArrayList;
import java.util.List;

public class CacheOfflineADs {
    private List<AdvertisementProperties> offlineADs = new ArrayList();

    public List<AdvertisementProperties> getOfflineADs() {
        return this.offlineADs;
    }

    public void setOfflineADs(List<AdvertisementProperties> offlineADs2) {
        this.offlineADs = offlineADs2;
    }

    public void addOfflineAD(AdvertisementProperties ad) {
        this.offlineADs.add(ad);
    }

    public void delOfflineAd(AdvertisementProperties ad) {
        this.offlineADs.remove(ad);
    }
}
