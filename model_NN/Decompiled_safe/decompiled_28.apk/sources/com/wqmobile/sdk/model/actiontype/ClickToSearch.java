package com.wqmobile.sdk.model.actiontype;

public class ClickToSearch {
    private String SearchLink;

    public String getSearchLink() {
        return this.SearchLink;
    }

    public void setSearchLink(String searchLink) {
        this.SearchLink = searchLink;
    }
}
