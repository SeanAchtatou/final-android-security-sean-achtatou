package com.wqmobile.sdk.model;

public class Device {
    private String FreeSize;
    private String Manufacturer;
    private String StorageType;
    private String TotalSize;

    public String getStorageType() {
        return this.StorageType;
    }

    public void setStorageType(String storageType) {
        this.StorageType = storageType;
    }

    public String getManufacturer() {
        return this.Manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.Manufacturer = manufacturer;
    }

    public String getTotalSize() {
        return this.TotalSize;
    }

    public void setTotalSize(String totalSize) {
        this.TotalSize = totalSize;
    }

    public String getFreeSize() {
        return this.FreeSize;
    }

    public void setFreeSize(String freeSize) {
        this.FreeSize = freeSize;
    }
}
