package com.wqmobile.sdk.model;

public class Operator {
    private String MCC;
    private String MNC;
    private String NID;
    private String SID;

    public String getMCC() {
        return this.MCC;
    }

    public void setMCC(String mCC) {
        this.MCC = mCC;
    }

    public String getMNC() {
        return this.MNC;
    }

    public void setMNC(String mNC) {
        this.MNC = mNC;
    }

    public String getNID() {
        return this.NID;
    }

    public void setNID(String nID) {
        this.NID = nID;
    }

    public String getSID() {
        return this.SID;
    }

    public void setSID(String sID) {
        this.SID = sID;
    }
}
