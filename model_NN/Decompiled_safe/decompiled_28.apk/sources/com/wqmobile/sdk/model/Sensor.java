package com.wqmobile.sdk.model;

public class Sensor {
    private String Capabilities;
    private String CurrentState;
    private String Manufacturer;
    private String Model;
    private String Name;
    private Integer PartNumber;
    private String SubModel;
    private String Type;
    private String Version;

    public String getName() {
        return this.Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getManufacturer() {
        return this.Manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.Manufacturer = manufacturer;
    }

    public String getModel() {
        return this.Model;
    }

    public void setModel(String model) {
        this.Model = model;
    }

    public String getSubModel() {
        return this.SubModel;
    }

    public void setSubModel(String subModel) {
        this.SubModel = subModel;
    }

    public String getType() {
        return this.Type;
    }

    public void setType(String type) {
        this.Type = type;
    }

    public String getVersion() {
        return this.Version;
    }

    public void setVersion(String version) {
        this.Version = version;
    }

    public Integer getPartNumber() {
        return this.PartNumber;
    }

    public void setPartNumber(Integer partNumber) {
        this.PartNumber = partNumber;
    }

    public String getCurrentState() {
        return this.CurrentState;
    }

    public void setCurrentState(String currentState) {
        this.CurrentState = currentState;
    }

    public String getCapabilities() {
        return this.Capabilities;
    }

    public void setCapabilities(String capabilities) {
        this.Capabilities = capabilities;
    }
}
