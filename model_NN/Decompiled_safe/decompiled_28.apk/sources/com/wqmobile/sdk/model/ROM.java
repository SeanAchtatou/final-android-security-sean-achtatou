package com.wqmobile.sdk.model;

public class ROM {
    private String FreeSize;
    private String TotalSize;

    public String getTotalSize() {
        return this.TotalSize;
    }

    public void setTotalSize(String totalSize) {
        this.TotalSize = totalSize;
    }

    public String getFreeSize() {
        return this.FreeSize;
    }

    public void setFreeSize(String freeSize) {
        this.FreeSize = freeSize;
    }
}
