package com.wqmobile.sdk.model;

public class AdvertisementDetail {
    private AdvertisementFiles ContentFiles = new AdvertisementFiles();
    private String Effect;
    private String Text;
    private String Url;

    public AdvertisementFiles getContentFiles() {
        return this.ContentFiles;
    }

    public void setContentFiles(AdvertisementFiles contentFiles) {
        this.ContentFiles = contentFiles;
    }

    public String getText() {
        return this.Text;
    }

    public void setText(String text) {
        this.Text = text;
    }

    public String getEffect() {
        return this.Effect;
    }

    public void setEffect(String effect) {
        this.Effect = effect;
    }

    public String getUrl() {
        return this.Url;
    }

    public void setUrl(String url) {
        this.Url = url;
    }
}
