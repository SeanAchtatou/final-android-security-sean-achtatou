package com.wqmobile.sdk.model;

public class Battery {
    private String RemainingCapacity;
    private String RemainingTime;
    private String Status;
    private String TotalCapacity;

    public String getStatus() {
        return this.Status;
    }

    public void setStatus(String status) {
        this.Status = status;
    }

    public String getTotalCapacity() {
        return this.TotalCapacity;
    }

    public void setTotalCapacity(String totalCapacity) {
        this.TotalCapacity = totalCapacity;
    }

    public String getRemainingCapacity() {
        return this.RemainingCapacity;
    }

    public void setRemainingCapacity(String remainingCapacity) {
        this.RemainingCapacity = remainingCapacity;
    }

    public String getRemainingTime() {
        return this.RemainingTime;
    }

    public void setRemainingTime(String remainingTime) {
        this.RemainingTime = remainingTime;
    }
}
