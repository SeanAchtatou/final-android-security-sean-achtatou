package com.wqmobile.sdk.model.actiontype;

public class ClickToCall {
    private String PhoneNumber;

    public String getPhoneNumber() {
        return this.PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.PhoneNumber = phoneNumber;
    }
}
