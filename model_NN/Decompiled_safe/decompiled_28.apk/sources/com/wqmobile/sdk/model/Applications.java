package com.wqmobile.sdk.model;

public class Applications {
    private Application[] Application;
    private Integer ApplicationCount;

    public Integer getApplicationCount() {
        return this.ApplicationCount;
    }

    public void setApplicationCount(Integer applicationCount) {
        this.ApplicationCount = applicationCount;
    }

    public Application[] getApplication() {
        return this.Application;
    }

    public void setApplication(Application[] application) {
        this.Application = application;
    }
}
