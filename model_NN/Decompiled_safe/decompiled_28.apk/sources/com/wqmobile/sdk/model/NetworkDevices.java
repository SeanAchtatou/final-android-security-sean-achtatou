package com.wqmobile.sdk.model;

public class NetworkDevices {
    private NetworkDevice[] Device = new NetworkDevice[0];
    private Integer DeviceCount;

    public Integer getDeviceCount() {
        return this.DeviceCount;
    }

    public void setDeviceCount(Integer deviceCount) {
        this.DeviceCount = deviceCount;
    }

    public NetworkDevice[] getDevice() {
        return this.Device;
    }

    public void setDevice(NetworkDevice[] device) {
        this.Device = device;
    }
}
