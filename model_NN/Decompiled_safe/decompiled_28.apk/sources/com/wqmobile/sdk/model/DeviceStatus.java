package com.wqmobile.sdk.model;

public class DeviceStatus {
    private String CurrentUserProfile;
    private String Locked;
    private String Volumn;

    public String getCurrentUserProfile() {
        return this.CurrentUserProfile;
    }

    public void setCurrentUserProfile(String currentUserProfile) {
        this.CurrentUserProfile = currentUserProfile;
    }

    public String getLocked() {
        return this.Locked;
    }

    public void setLocked(String locked) {
        this.Locked = locked;
    }

    public String getVolumn() {
        return this.Volumn;
    }

    public void setVolumn(String volumn) {
        this.Volumn = volumn;
    }
}
