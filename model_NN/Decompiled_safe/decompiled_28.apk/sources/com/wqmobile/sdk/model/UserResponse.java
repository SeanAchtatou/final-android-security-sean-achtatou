package com.wqmobile.sdk.model;

public class UserResponse {
    private String AdID;
    private String AppID;
    private String ResponseParameters;
    private String ResponseTime;
    private String ResponseType;
    private String ResponseViewingTime;
    private Integer ResponseViewingTimes;

    public String getAppID() {
        return this.AppID;
    }

    public void setAppID(String appID) {
        this.AppID = appID;
    }

    public String getAdID() {
        return this.AdID;
    }

    public void setAdID(String adID) {
        this.AdID = adID;
    }

    public String getResponseType() {
        return this.ResponseType;
    }

    public void setResponseType(String responseType) {
        this.ResponseType = responseType;
    }

    public String getResponseTime() {
        return this.ResponseTime;
    }

    public void setResponseTime(String responseTime) {
        this.ResponseTime = responseTime;
    }

    public String getResponseViewingTime() {
        return this.ResponseViewingTime;
    }

    public void setResponseViewingTime(String responseViewingTime) {
        this.ResponseViewingTime = responseViewingTime;
    }

    public Integer getResponseViewingTimes() {
        return this.ResponseViewingTimes;
    }

    public void setResponseViewingTimes(Integer responseViewingTimes) {
        this.ResponseViewingTimes = responseViewingTimes;
    }

    public String getResponseParameters() {
        return this.ResponseParameters;
    }

    public void setResponseParameters(String responseParameters) {
        this.ResponseParameters = responseParameters;
    }
}
