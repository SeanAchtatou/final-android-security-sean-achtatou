package com.wqmobile.sdk.model;

public class DynamicClientProfile {
    private DynClientProfile ClientProfile = new DynClientProfile();
    private ViewSettings ViewSettings = new ViewSettings();

    public ViewSettings getViewSettings() {
        return this.ViewSettings;
    }

    public void setViewSettings(ViewSettings viewSettings) {
        this.ViewSettings = viewSettings;
    }

    public DynClientProfile getClientProfile() {
        return this.ClientProfile;
    }

    public void setClientProfile(DynClientProfile clientProfile) {
        this.ClientProfile = clientProfile;
    }
}
