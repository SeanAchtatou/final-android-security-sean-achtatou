package com.wqmobile.sdk.model;

import java.util.ArrayList;
import java.util.List;

public class Storage {
    private Integer DeviceCount;
    private List<Device> Devices = new ArrayList();

    public Integer getDeviceCount() {
        return this.DeviceCount;
    }

    public void setDeviceCount(Integer deviceCount) {
        this.DeviceCount = deviceCount;
    }

    public List<Device> getDevices() {
        return this.Devices;
    }

    public void setDevices(List<Device> devices) {
        this.Devices = devices;
    }
}
