package com.wqmobile.sdk.model.actiontype;

public class MailContent {
    private String Body;
    private String Title;

    public String getTitle() {
        return this.Title;
    }

    public void setTitle(String title) {
        this.Title = title;
    }

    public String getBody() {
        return this.Body;
    }

    public void setBody(String body) {
        this.Body = body;
    }
}
