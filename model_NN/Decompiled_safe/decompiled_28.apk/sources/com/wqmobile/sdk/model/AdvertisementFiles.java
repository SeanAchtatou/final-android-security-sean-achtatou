package com.wqmobile.sdk.model;

public class AdvertisementFiles {
    private Integer Count;
    AdvertisementFile[] File = new AdvertisementFile[0];

    public Integer getCount() {
        return this.Count;
    }

    public void setCount(Integer count) {
        this.Count = count;
    }

    public AdvertisementFile[] getFile() {
        return this.File;
    }

    public void setFile(AdvertisementFile[] files) {
        this.File = files;
    }
}
