package com.wqmobile.sdk.model;

public class Application {
    private String Description;
    private String ID;
    private String Location;
    private String MemoryUsage;
    private String Name;
    private String NetworkUsage;
    private String RunTime;
    private String State;
    private String Version;

    public String getID() {
        return this.ID;
    }

    public void setID(String iD) {
        this.ID = iD;
    }

    public String getName() {
        return this.Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getDescription() {
        return this.Description;
    }

    public void setDescription(String description) {
        this.Description = description;
    }

    public String getLocation() {
        return this.Location;
    }

    public void setLocation(String location) {
        this.Location = location;
    }

    public String getState() {
        return this.State;
    }

    public void setState(String state) {
        this.State = state;
    }

    public String getVersion() {
        return this.Version;
    }

    public void setVersion(String version) {
        this.Version = version;
    }

    public String getRunTime() {
        return this.RunTime;
    }

    public void setRunTime(String runTime) {
        this.RunTime = runTime;
    }

    public String getMemoryUsage() {
        return this.MemoryUsage;
    }

    public void setMemoryUsage(String memoryUsage) {
        this.MemoryUsage = memoryUsage;
    }

    public String getNetworkUsage() {
        return this.NetworkUsage;
    }

    public void setNetworkUsage(String networkUsage) {
        this.NetworkUsage = networkUsage;
    }
}
