package com.wqmobile.sdk.model;

public class SystemProfile {
    private String Configuration;
    private Platform Platform;
    private Profiles Profiles;
    private Services[] Services;

    public Platform getPlatform() {
        return this.Platform;
    }

    public void setPlatform(Platform platform) {
        this.Platform = platform;
    }

    public Services[] getServices() {
        return this.Services;
    }

    public void setServices(Services[] services) {
        this.Services = services;
    }

    public String getConfiguration() {
        return this.Configuration;
    }

    public void setConfiguration(String configuration) {
        this.Configuration = configuration;
    }

    public Profiles getProfiles() {
        return this.Profiles;
    }

    public void setProfiles(Profiles profiles) {
        this.Profiles = profiles;
    }
}
