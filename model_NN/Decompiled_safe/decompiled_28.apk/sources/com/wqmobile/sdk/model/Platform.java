package com.wqmobile.sdk.model;

public class Platform {
    private String Type;
    private String Variation;
    private String Version;

    public String getType() {
        return this.Type;
    }

    public void setType(String type) {
        this.Type = type;
    }

    public String getVersion() {
        return this.Version;
    }

    public void setVersion(String version) {
        this.Version = version;
    }

    public String getVariation() {
        return this.Variation;
    }

    public void setVariation(String variation) {
        this.Variation = variation;
    }
}
