package com.wqmobile.sdk.model;

public class AdvertisementFile {
    private String FileName;
    private String Portion;
    private Integer Size;
    private String Type;

    public String getFileName() {
        return this.FileName;
    }

    public void setFileName(String fileName) {
        this.FileName = fileName;
    }

    public String getType() {
        return this.Type;
    }

    public void setType(String type) {
        this.Type = type;
    }

    public String getPortion() {
        return this.Portion;
    }

    public void setPortion(String portion) {
        this.Portion = portion;
    }

    public Integer getSize() {
        return this.Size;
    }

    public void setSize(Integer size) {
        this.Size = size;
    }
}
