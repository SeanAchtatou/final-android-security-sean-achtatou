package com.wqmobile.sdk.model.actiontype;

public class ClickToSMS {
    private String Content;
    private String Target;

    public String getTarget() {
        return this.Target;
    }

    public void setTarget(String target) {
        this.Target = target;
    }

    public String getContent() {
        return this.Content;
    }

    public void setContent(String content) {
        this.Content = content;
    }
}
