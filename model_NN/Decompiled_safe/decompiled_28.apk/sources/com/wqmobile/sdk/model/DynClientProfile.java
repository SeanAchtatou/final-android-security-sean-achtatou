package com.wqmobile.sdk.model;

public class DynClientProfile {
    private DemographicInfo DemographicInfo = new DemographicInfo();
    private GeographicInfo GeographicInfo = new GeographicInfo();

    public GeographicInfo getGeographicInfo() {
        return this.GeographicInfo;
    }

    public void setGeographicInfo(GeographicInfo geographicInfo) {
        this.GeographicInfo = geographicInfo;
    }

    public DemographicInfo getDemographicInfo() {
        return this.DemographicInfo;
    }

    public void setDemographicInfo(DemographicInfo demographicInfo) {
        this.DemographicInfo = demographicInfo;
    }
}
