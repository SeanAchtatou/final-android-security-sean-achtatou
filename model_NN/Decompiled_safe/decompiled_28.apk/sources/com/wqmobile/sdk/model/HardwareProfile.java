package com.wqmobile.sdk.model;

public class HardwareProfile {
    private Screen Screen = new Screen();

    public Screen getScreen() {
        return this.Screen;
    }

    public void setScreen(Screen screen) {
        this.Screen = screen;
    }
}
