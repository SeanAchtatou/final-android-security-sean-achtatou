package com.wqmobile.sdk.model;

public class OfflineADs {
    private AD[] AD = new AD[0];
    private Integer RunningTime = 0;

    public AD[] getAD() {
        return this.AD;
    }

    public void setAD(AD[] aD) {
        this.AD = aD;
    }

    public Integer getRunningTime() {
        return this.RunningTime;
    }

    public void setRunningTime(Integer runningTime) {
        this.RunningTime = runningTime;
    }
}
