package com.wqmobile.sdk.model;

public class SimCardState {
    private String Description;
    private String StateCode;

    public String getStateCode() {
        return this.StateCode;
    }

    public void setStateCode(String stateCode) {
        this.StateCode = stateCode;
    }

    public String getDescription() {
        return this.Description;
    }

    public void setDescription(String description) {
        this.Description = description;
    }
}
