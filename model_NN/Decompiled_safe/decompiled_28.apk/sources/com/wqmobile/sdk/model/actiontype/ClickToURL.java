package com.wqmobile.sdk.model.actiontype;

public class ClickToURL {
    private String URL;

    public String getURL() {
        return this.URL;
    }

    public void setURL(String uRL) {
        this.URL = uRL;
    }
}
