package com.wqmobile.sdk.model;

public class PhoneInfo {
    private BaseStation BaseStation = new BaseStation();
    private DeviceID DeviceID = new DeviceID();
    private String Manufacturer;
    private String Model;
    private Network Network = new Network();
    private String PhoneNumber;
    private String ProductName;
    private String RadioType;
    private String RegistrationStatus;
    private SimCardState SimcardState = new SimCardState();

    public DeviceID getDeviceID() {
        return this.DeviceID;
    }

    public void setDeviceID(DeviceID deviceID) {
        this.DeviceID = deviceID;
    }

    public String getManufacturer() {
        return this.Manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.Manufacturer = manufacturer;
    }

    public String getModel() {
        return this.Model;
    }

    public void setModel(String model) {
        this.Model = model;
    }

    public String getProductName() {
        return this.ProductName;
    }

    public void setProductName(String productName) {
        this.ProductName = productName;
    }

    public String getRadioType() {
        return this.RadioType;
    }

    public void setRadioType(String radioType) {
        this.RadioType = radioType;
    }

    public SimCardState getSimcardState() {
        return this.SimcardState;
    }

    public void setSimcardState(SimCardState simcardState) {
        this.SimcardState = simcardState;
    }

    public BaseStation getBaseStation() {
        return this.BaseStation;
    }

    public void setBaseStation(BaseStation baseStation) {
        this.BaseStation = baseStation;
    }

    public String getRegistrationStatus() {
        return this.RegistrationStatus;
    }

    public void setRegistrationStatus(String registrationStatus) {
        this.RegistrationStatus = registrationStatus;
    }

    public Network getNetwork() {
        return this.Network;
    }

    public void setNetwork(Network network) {
        this.Network = network;
    }

    public String getPhoneNumber() {
        return this.PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.PhoneNumber = phoneNumber;
    }
}
