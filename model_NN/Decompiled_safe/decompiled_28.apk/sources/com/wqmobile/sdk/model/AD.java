package com.wqmobile.sdk.model;

public class AD {
    private Integer ClickCount;
    private String ID;
    private Integer ViewCount;

    public String getID() {
        return this.ID;
    }

    public void setID(String iD) {
        this.ID = iD;
    }

    public Integer getViewCount() {
        return this.ViewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.ViewCount = viewCount;
    }

    public Integer getClickCount() {
        return this.ClickCount;
    }

    public void setClickCount(Integer clickCount) {
        this.ClickCount = clickCount;
    }
}
