package com.wqmobile.sdk.model;

public class Service {
    private String Description;
    private String ID;
    private String Name;
    private String RunTime;
    private String State;

    public String getID() {
        return this.ID;
    }

    public void setID(String iD) {
        this.ID = iD;
    }

    public String getName() {
        return this.Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getDescription() {
        return this.Description;
    }

    public void setDescription(String description) {
        this.Description = description;
    }

    public String getState() {
        return this.State;
    }

    public void setState(String state) {
        this.State = state;
    }

    public String getRunTime() {
        return this.RunTime;
    }

    public void setRunTime(String runTime) {
        this.RunTime = runTime;
    }
}
