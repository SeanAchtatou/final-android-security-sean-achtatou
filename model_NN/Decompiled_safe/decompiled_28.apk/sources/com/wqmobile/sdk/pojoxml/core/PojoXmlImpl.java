package com.wqmobile.sdk.pojoxml.core;

import com.wqmobile.sdk.pojoxml.core.processor.PojoToXml;
import com.wqmobile.sdk.pojoxml.core.processor.XmlToPojo;
import com.wqmobile.sdk.pojoxml.util.StringUtil;
import java.io.InputStream;

class PojoXmlImpl implements PojoXml {
    private String ecoding;
    private boolean enableCDATA;
    private PojoXmlInitInfo pojoXmlInitInfo = new PojoXmlInitInfo();

    public Object getPojo(String xmlContent, Class objectClass) {
        try {
            return new XmlToPojo(objectClass).parseXMLFromXmlData(xmlContent, this.pojoXmlInitInfo);
        } catch (Exception e) {
            StringUtil.PrintExceptionStatck(e);
            return null;
        }
    }

    public Object getPojoFromFile(String fileName, Class objectClass) {
        return new XmlToPojo(objectClass).parseXMLFromFile(fileName, this.pojoXmlInitInfo);
    }

    public String getXml(Object object) {
        if (object == null) {
            try {
                throw new PojoXmlException("null value can not convert as Xml");
            } catch (Exception e) {
                StringUtil.PrintExceptionStatck(e);
                return null;
            }
        } else {
            PojoToXml pojoToXml = new PojoToXml(this.pojoXmlInitInfo);
            pojoToXml.setEncoding(getEcoding());
            pojoToXml.setCDataEnabled(isEnableCDATA());
            return pojoToXml.getXml(object);
        }
    }

    public void saveXml(Object object, String fileName) {
        if (object == null) {
            throw new PojoXmlException("null value can not convert as Xml");
        }
        PojoToXml pojoToXml = new PojoToXml(this.pojoXmlInitInfo);
        pojoToXml.setEncoding(getEcoding());
        pojoToXml.setCDataEnabled(isEnableCDATA());
        pojoToXml.saveXml(object, fileName);
    }

    public void setEncoding(String encoding) {
        setEcoding(encoding);
    }

    public void setEcoding(String ecoding2) {
        this.ecoding = ecoding2;
    }

    public String getEcoding() {
        return this.ecoding;
    }

    public boolean isEnableCDATA() {
        return this.enableCDATA;
    }

    public void enableCDATA(boolean enableCDATA2) {
        this.enableCDATA = enableCDATA2;
    }

    public void addCollectionClass(String elementName, Class clas) {
        if (clas == null) {
            throw new PojoXmlException("Provide Class type for the field alias");
        }
        this.pojoXmlInitInfo.addCollectionClass(elementName, clas);
    }

    public void addFieldAlias(Class cls, String fieldName, String aliasName) {
        if (cls == null) {
            throw new PojoXmlException("Provide Class type for the field alias");
        }
        this.pojoXmlInitInfo.addFieldAlias(String.valueOf(cls.getName()) + "." + fieldName, aliasName);
    }

    public void addClassAlias(Class cls, String aliasName) {
        if (cls == null) {
            throw new PojoXmlException("Provide Class type for the field alias");
        }
        this.pojoXmlInitInfo.addFieldAlias(cls.getName(), aliasName);
    }

    public Object getPojo(InputStream xmlStream, Class objectClass) {
        try {
            return new XmlToPojo(objectClass).parseXMLFromInputStream(xmlStream, this.pojoXmlInitInfo);
        } catch (Exception e) {
            StringUtil.PrintExceptionStatck(e);
            return null;
        }
    }
}
