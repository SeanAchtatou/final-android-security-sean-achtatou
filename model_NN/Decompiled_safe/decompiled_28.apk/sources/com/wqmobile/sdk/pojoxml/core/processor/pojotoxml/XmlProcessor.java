package com.wqmobile.sdk.pojoxml.core.processor.pojotoxml;

import com.wqmobile.sdk.pojoxml.core.PojoXmlInitInfo;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import com.wqmobile.sdk.pojoxml.util.XmlUtil;

abstract class XmlProcessor {
    private boolean attribute;
    private String attributes;
    private boolean cdataEnabled;
    private String className;
    private String elementName;
    private PojoXmlInitInfo pojoXmlInitInfo;
    private int space;
    private boolean stringFound;
    private StringBuffer xmlContent;

    public abstract String process(Object obj);

    public boolean isCdataEnabled() {
        return this.cdataEnabled;
    }

    public void setCdataEnabled(boolean cdataEnabled2) {
        this.cdataEnabled = cdataEnabled2;
    }

    public boolean isStringFound() {
        return this.stringFound;
    }

    public void setStringFound(boolean stringFound2) {
        this.stringFound = stringFound2;
    }

    public XmlProcessor() {
        this.attributes = "";
        this.attribute = false;
        this.cdataEnabled = false;
        this.stringFound = false;
        this.xmlContent = new StringBuffer();
    }

    public XmlProcessor(String elementName2, int space2, PojoXmlInitInfo pojoXmlInitInfo2) {
        this(elementName2, "", space2, pojoXmlInitInfo2);
    }

    public XmlProcessor(String elementName2, String attrib, int space2, PojoXmlInitInfo pojoXmlInitInfo2) {
        this.attributes = "";
        this.attribute = false;
        this.cdataEnabled = false;
        this.stringFound = false;
        setPojoXmlInitInfo(pojoXmlInitInfo2);
        this.elementName = elementName2;
        this.attributes = attrib;
        this.space = space2;
        this.xmlContent = new StringBuffer();
    }

    public boolean isEmptyElement(Object object, String elementName2) {
        if (object != null) {
            return false;
        }
        writeXmlContent(createEmptyElementWithNL(elementName2, this.attributes, getSpace()));
        return true;
    }

    public void processCollection(Object collectionEelement) {
        CollectionToXmlProcessor collectionToXmlProcessor = new CollectionToXmlProcessor(getSpace());
        collectionToXmlProcessor.setCdataEnabled(isCdataEnabled());
        writeXmlContent(collectionToXmlProcessor.process(collectionEelement));
    }

    public void processObject(Object object) {
        ClassToXmlProcessor classToXmlProcessor = new ClassToXmlProcessor(getSpace(), getPojoXmlInitInfo());
        classToXmlProcessor.setCdataEnabled(isCdataEnabled());
        writeXmlContent(classToXmlProcessor.process(object));
    }

    public void processObject(Object object, String elementName2) {
        System.out.println("ELM: " + elementName2 + XmlConstant.SINGLE_SPACE + getAliasName(elementName2));
        ClassToXmlProcessor classToXmlProcessor = new ClassToXmlProcessor(getSpace(), getAliasName(elementName2), getPojoXmlInitInfo());
        classToXmlProcessor.setCdataEnabled(isCdataEnabled());
        writeXmlContent(classToXmlProcessor.process(object));
    }

    public void processObject(Object otherObj, String classElementName, String attrib) {
        ClassToXmlProcessor processor = new ClassToXmlProcessor(getAliasName(classElementName), attrib, getSpace(), getPojoXmlInitInfo());
        processor.setAttributes(attrib);
        processor.setCdataEnabled(isCdataEnabled());
        writeXmlContent(processor.process(otherObj));
    }

    public boolean isEmptyElement(Object object, String elementName2, String attrib) {
        if (object == null) {
            return true;
        }
        return false;
    }

    public void writeXmlContent(String xml) {
        if (xml != null) {
            this.xmlContent.append(xml);
        }
    }

    public String getXmlContent() {
        return this.xmlContent.toString();
    }

    public void setXmlContent(StringBuffer xmlContent2) {
        this.xmlContent = xmlContent2;
    }

    public String toString() {
        return this.xmlContent.toString();
    }

    public void incrementSpace() {
        this.space++;
    }

    public void decrementSpace() {
        this.space--;
    }

    public int getSpace() {
        return this.space;
    }

    public void setSpace(int space2) {
        this.space = space2;
    }

    public String getElementName() {
        return this.elementName;
    }

    public void setElementName(String elementName2) {
        this.elementName = elementName2;
    }

    public void setAttribute(boolean attribute2) {
        this.attribute = attribute2;
    }

    public boolean isAttribute() {
        return this.attribute;
    }

    public void setAttributes(String attributes2) {
        this.attributes = attributes2;
    }

    public String getAttributes() {
        return this.attributes;
    }

    public void setPojoXmlInitInfo(PojoXmlInitInfo pojoXmlInitInfo2) {
        this.pojoXmlInitInfo = pojoXmlInitInfo2;
    }

    public PojoXmlInitInfo getPojoXmlInitInfo() {
        return this.pojoXmlInitInfo;
    }

    public String getAliasName(String fieldName) {
        Object aliasName = this.pojoXmlInitInfo.getAliasName(String.valueOf(getClassName()) + "." + fieldName);
        if (aliasName == null) {
            return fieldName;
        }
        return aliasName.toString();
    }

    public String getClassAliasName(String className2) {
        Object aliasName = this.pojoXmlInitInfo.getAliasName(className2);
        if (aliasName == null) {
            return null;
        }
        return aliasName.toString();
    }

    /* access modifiers changed from: protected */
    public String getElementWithNL(String elementName2, String elementAttributes, String elementValue, int spac) {
        return XmlUtil.getElementWithNL(getAliasName(elementName2), elementAttributes, elementValue, spac);
    }

    /* access modifiers changed from: protected */
    public String getStartTagWithNL(String rootName, String attributes2, int space2) {
        return XmlUtil.getStartTagWithNL(getAliasName(rootName), attributes2, space2);
    }

    /* access modifiers changed from: protected */
    public String getCloseTag(String rootName, int space2) {
        return XmlUtil.getCloseTag(getAliasName(rootName), space2);
    }

    /* access modifiers changed from: protected */
    public String getCloseTagWithNL(String rootName, int space2) {
        return XmlUtil.getCloseTagWithNL(getAliasName(rootName), space2);
    }

    /* access modifiers changed from: protected */
    public String createEmptyElementWithNL(String elementName2, String attributes2, int space2) {
        return XmlUtil.createEmptyElementWithNL(getAliasName(elementName2), attributes2, space2);
    }

    /* access modifiers changed from: protected */
    public String getStartTag(String elementName2, String attribute2, int space2) {
        return XmlUtil.getStartTag(getAliasName(elementName2), attribute2, space2);
    }

    public void setClassName(String className2) {
        this.className = className2;
    }

    public String getClassName() {
        return this.className;
    }
}
