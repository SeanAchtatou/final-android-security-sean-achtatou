package com.wqmobile.sdk.pojoxml.core;

public class PojoXmlException extends RuntimeException {
    private static final long serialVersionUID = 123456789102134L;

    public PojoXmlException() {
    }

    public PojoXmlException(String message) {
        super(message);
    }

    public PojoXmlException(Throwable cause) {
        super(cause);
    }
}
