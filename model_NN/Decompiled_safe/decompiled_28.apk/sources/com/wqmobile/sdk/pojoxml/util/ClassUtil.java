package com.wqmobile.sdk.pojoxml.util;

import com.wqmobile.sdk.pojoxml.exeception.ClassHandlerException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

public abstract class ClassUtil {
    private static final String GET = "get";
    private static final String SET = "set";
    private static final Set collectionType = new HashSet();
    private static final Map primWraperMap = new HashMap();
    private static final Set primitiveType = new HashSet();
    private static final Set wrapperType = new HashSet();

    static {
        Class<Float> cls = Float.class;
        Class<Double> cls2 = Double.class;
        Class<Character> cls3 = Character.class;
        Class<Byte> cls4 = Byte.class;
        Class<Boolean> cls5 = Boolean.class;
        primitiveType.add(Byte.TYPE);
        primitiveType.add(byte[].class);
        primitiveType.add(Integer.TYPE);
        primitiveType.add(int[].class);
        primitiveType.add(Short.TYPE);
        primitiveType.add(short[].class);
        primitiveType.add(Long.TYPE);
        primitiveType.add(long[].class);
        primitiveType.add(Float.TYPE);
        primitiveType.add(float[].class);
        primitiveType.add(Double.TYPE);
        primitiveType.add(double[].class);
        primitiveType.add(Character.TYPE);
        primitiveType.add(char[].class);
        primitiveType.add(Boolean.TYPE);
        primitiveType.add(boolean[].class);
        wrapperType.add(String.class);
        wrapperType.add(Integer.class);
        wrapperType.add(Long.class);
        wrapperType.add(Short.class);
        Class<Character> cls6 = Character.class;
        wrapperType.add(cls3);
        Class<Boolean> cls7 = Boolean.class;
        wrapperType.add(cls5);
        Class<Byte> cls8 = Byte.class;
        wrapperType.add(cls4);
        Class<Double> cls9 = Double.class;
        wrapperType.add(cls2);
        Class<Float> cls10 = Float.class;
        wrapperType.add(cls);
        wrapperType.add(BigInteger.class);
        wrapperType.add(BigDecimal.class);
        wrapperType.add(Date.class);
        wrapperType.add(java.sql.Date.class);
        collectionType.add(List.class);
        collectionType.add(Set.class);
        collectionType.add(ArrayList.class);
        collectionType.add(LinkedList.class);
        collectionType.add(Vector.class);
        collectionType.add(TreeSet.class);
        collectionType.add(HashSet.class);
        collectionType.add(LinkedHashSet.class);
        primWraperMap.put(Integer.TYPE, Integer.class);
        primWraperMap.put(Short.TYPE, Short.class);
        Class<Byte> cls11 = Byte.class;
        primWraperMap.put(Byte.TYPE, cls4);
        primWraperMap.put(Long.TYPE, Long.class);
        Class<Character> cls12 = Character.class;
        primWraperMap.put(Character.TYPE, cls3);
        Class<Boolean> cls13 = Boolean.class;
        primWraperMap.put(Boolean.TYPE, cls5);
        Class<Double> cls14 = Double.class;
        primWraperMap.put(Double.TYPE, cls2);
        Class<Float> cls15 = Float.class;
        primWraperMap.put(Float.TYPE, cls);
    }

    public static Field[] getFields(Class clas) {
        Assert.isNull(clas, "Class should not be Null");
        return clas.getDeclaredFields();
    }

    public static String[] getInstanceVariables(Class clas) {
        Field[] fields = clas.getDeclaredFields();
        int length = fields.length;
        String[] vars = new String[length];
        for (int i = 0; i < length; i++) {
            vars[i] = fields[i].getName();
        }
        return vars;
    }

    public static String getClassName(Class clas) {
        String name = clas.getName();
        return name.substring(name.lastIndexOf(46) + 1);
    }

    private static Object handleCharacterDefault(Object charObj) {
        if (charObj == null || charObj.getClass() != Character.class) {
            return charObj;
        }
        if (((Character) charObj).charValue() == 0) {
            charObj = new Character(' ');
        }
        return charObj;
    }

    public static Object invokeGetter(String fieldName, Class clas, Object object) throws ClassHandlerException {
        try {
            return handleCharacterDefault(clas.getMethod(getGetterMethod(fieldName), new Class[0]).invoke(object, new Object[0]));
        } catch (SecurityException e) {
            throw new ClassHandlerException("Security Problem: " + e);
        } catch (NoSuchMethodException e2) {
            try {
                return clas.getMethod("is" + StringUtil.initCap(fieldName), new Class[0]).invoke(object, new Object[0]);
            } catch (Exception e3) {
                System.out.println("Warning: No getter Method available for field :" + fieldName + XmlConstant.NL);
                return null;
            }
        } catch (IllegalArgumentException e4) {
            throw new ClassHandlerException("Illegal argument exception :" + e4);
        } catch (IllegalAccessException e5) {
            throw new ClassHandlerException("Illegal access exception :" + e5);
        } catch (InvocationTargetException e6) {
            throw new ClassHandlerException("Invocation Target exception :" + e6);
        }
    }

    public static Iterator getIterator(Object object) throws ClassHandlerException {
        Class clas = null;
        if (object instanceof List) {
            clas = List.class;
        } else if (object instanceof Set) {
            clas = Set.class;
        }
        try {
            return (Iterator) clas.getMethod("iterator", new Class[0]).invoke(object, new Object[0]);
        } catch (IllegalArgumentException e) {
            throw new ClassHandlerException(e);
        } catch (IllegalAccessException e2) {
            throw new ClassHandlerException(e2);
        } catch (InvocationTargetException e3) {
            throw new ClassHandlerException(e3);
        } catch (SecurityException e4) {
            throw new ClassHandlerException(e4);
        } catch (NoSuchMethodException e5) {
            throw new ClassHandlerException(e5);
        }
    }

    public static String getGetterMethod(String fieldName) {
        return GET + StringUtil.initCap(fieldName);
    }

    public static boolean isPrimitiveOrWrapper(Class clas, String fieldName) throws ClassHandlerException {
        try {
            Class typeClass = clas.getDeclaredField(fieldName).getType();
            if (typeClass.isArray()) {
                return false;
            }
            return primitiveType.contains(typeClass) || wrapperType.contains(typeClass);
        } catch (SecurityException e) {
            throw new ClassHandlerException(e);
        } catch (NoSuchFieldException e2) {
            throw new ClassHandlerException("No fields available: " + e2);
        }
    }

    public static boolean isCollection(Class clas, Object object, String fieldName) throws ClassHandlerException {
        boolean retVal = false;
        try {
            Class listClass = Class.forName("java.util.List");
            Class setClass = Class.forName("java.util.Set");
            Object retObject = invokeGetter(fieldName, clas, object);
            if (listClass.isInstance(retObject)) {
                retVal = true;
            }
            if (setClass.isInstance(retObject)) {
                return true;
            }
            return retVal;
        } catch (ClassNotFoundException e) {
            throw new ClassHandlerException(e);
        }
    }

    public static boolean isCollection(Object collectionObject) {
        boolean retVal = false;
        try {
            Class listClass = Class.forName("java.util.List");
            Class setClass = Class.forName("java.util.Set");
            if (listClass.isInstance(collectionObject)) {
                retVal = true;
            }
            if (setClass.isInstance(collectionObject)) {
                return true;
            }
            return retVal;
        } catch (ClassNotFoundException e) {
            throw new ClassHandlerException(e);
        }
    }

    public static Class getClassFromProperty(String property, Class clas) {
        try {
            return clas.getDeclaredField(property).getType();
        } catch (SecurityException e) {
            throw new ClassHandlerException(e);
        } catch (NoSuchFieldException e2) {
            throw new ClassHandlerException(e2);
        }
    }

    public static Map getFieldsAndType(Class clas) {
        Map map = new HashMap();
        Field[] fields = clas.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            map.put(fields[i].getName(), fields[i].getType());
        }
        return map;
    }

    public static Map getSetterAndClass(Class clas) {
        Map setterMap = new HashMap();
        for (Method setMethod : clas.getDeclaredMethods()) {
            if (setMethod.getName().startsWith(SET)) {
                setterMap.put(StringUtil.subString(setMethod.getName(), 3), setMethod.getParameterTypes()[0]);
            }
        }
        return setterMap;
    }

    public static void invokeSetter(Class clas, Object object, String fieldName, Object paramValue, Class paramClass) {
        try {
            clas.getMethod(getSetterMethod(fieldName), paramClass).invoke(object, paramValue);
        } catch (SecurityException e) {
            throw new ClassHandlerException("Security Problem: " + e);
        } catch (NoSuchMethodException e2) {
            System.out.print("Warning: No setter Method available for field :" + fieldName + XmlConstant.NL + e2);
        } catch (IllegalArgumentException e3) {
            throw new ClassHandlerException("Illegal argument exception :" + e3);
        } catch (IllegalAccessException e4) {
            throw new ClassHandlerException("Illegal access exception :" + e4);
        } catch (InvocationTargetException e5) {
            throw new ClassHandlerException("Invocation Target exception :" + e5);
        }
    }

    public static String getSetterMethod(String fieldName) {
        return SET + StringUtil.initCap(fieldName);
    }

    public static boolean isArray(Class clas, String fieldName) {
        try {
            return clas.getDeclaredField(fieldName).getType().isArray();
        } catch (SecurityException e) {
            throw new ClassHandlerException(e);
        } catch (NoSuchFieldException e2) {
            throw new ClassHandlerException(e2);
        }
    }

    public static int getArrayLength(Object arrayObject) {
        return Array.getLength(arrayObject);
    }

    public static boolean isWrapperArray(Object oneArrayObject) {
        Class clas = null;
        if (oneArrayObject != null) {
            clas = oneArrayObject.getClass();
        }
        return wrapperType.contains(clas);
    }

    public static boolean isCollectionArray(Object firstObject) throws ClassHandlerException {
        boolean deriveCollection = false;
        if (firstObject != null) {
            firstObject.getClass();
        }
        boolean baseCollection = collectionType.contains(null);
        try {
            Class listClass = Class.forName("java.util.List");
            Class setClass = Class.forName("java.util.Set");
            if (listClass.isInstance(firstObject)) {
                deriveCollection = true;
            }
            if (setClass.isInstance(firstObject)) {
                deriveCollection = true;
            }
            return baseCollection || deriveCollection;
        } catch (ClassNotFoundException e) {
            throw new ClassHandlerException(e);
        }
    }

    public static Object createObject(Class clas) throws ClassHandlerException {
        try {
            return clas.newInstance();
        } catch (InstantiationException e) {
            try {
                return clas.getComponentType().newInstance();
            } catch (InstantiationException e2) {
                e2.printStackTrace();
                return null;
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
                return null;
            }
        } catch (IllegalAccessException e4) {
            throw new ClassHandlerException(e4);
        }
    }

    public static Object ObjectFromValue(Object value, Class valueClass) {
        Class<String> cls = String.class;
        if (valueClass == Integer.TYPE) {
            return new Integer(value.toString());
        }
        if (valueClass == Short.TYPE) {
            return new Short(value.toString());
        }
        if (valueClass == Byte.TYPE) {
            return new Byte(value.toString());
        }
        if (valueClass == Boolean.TYPE) {
            return new Boolean(value.toString());
        }
        if (valueClass == Float.TYPE) {
            return new Float(value.toString());
        }
        if (valueClass == Long.TYPE) {
            return new Long(value.toString());
        }
        if (valueClass == Character.TYPE || valueClass == Character.class) {
            return new Character(value.toString().charAt(0));
        }
        if (valueClass == Double.TYPE) {
            return new Double(value.toString());
        }
        Class<String> cls2 = String.class;
        if (valueClass == cls) {
            return value;
        }
        if (!wrapperType.contains(valueClass)) {
            return value.toString();
        }
        try {
            return valueClass.getConstructor(String.class).newInstance(value);
        } catch (SecurityException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchMethodException e2) {
            e2.printStackTrace();
            return null;
        } catch (IllegalArgumentException e3) {
            e3.printStackTrace();
            return null;
        } catch (InstantiationException e4) {
            e4.printStackTrace();
            return null;
        } catch (IllegalAccessException e5) {
            e5.printStackTrace();
            return null;
        } catch (InvocationTargetException e6) {
            e6.printStackTrace();
            return null;
        }
    }

    public static boolean isPrimitiveOrWrapper(Class clas) {
        Class componentType;
        if (!isArray(clas) || (componentType = clas.getComponentType()) == null) {
            return primitiveType.contains(clas) || wrapperType.contains(clas);
        }
        if (primitiveType.contains(componentType) || wrapperType.contains(componentType)) {
            return true;
        }
        return false;
    }

    public static boolean isArray(Class clas) {
        if (clas != null) {
            return clas.isArray();
        }
        return false;
    }

    public static boolean isPrimitiveArray(Class clas) {
        return primitiveType.contains(clas);
    }

    public static Object createArray(List valueList, Class clas) throws ClassHandlerException {
        Object setObject;
        if (!isPrimitiveOrWrapper(clas) && !clas.equals(Character.TYPE) && clas.getComponentType() != null) {
            clas = clas.getComponentType();
        }
        Object arrayObject = Array.newInstance(clas, valueList.size());
        int i = 0;
        Class wrapperClass = (Class) primWraperMap.get(clas);
        if (wrapperClass == null) {
            wrapperClass = clas;
        }
        for (Object value : valueList) {
            try {
                if (clas.equals(Character.TYPE) || clas.equals(Character.class)) {
                    if (value == null) {
                        setObject = null;
                    } else {
                        setObject = new Character(value.toString().charAt(0));
                    }
                } else if (isPrimitiveOrWrapper(clas)) {
                    Constructor construtor = wrapperClass.getConstructor(String.class);
                    if (value == null) {
                        setObject = null;
                    } else {
                        setObject = construtor.newInstance(value.toString());
                    }
                } else {
                    setObject = value;
                }
                Array.set(arrayObject, i, setObject);
            } catch (SecurityException e) {
                throw new ClassHandlerException(e);
            } catch (NoSuchMethodException e2) {
                e2.printStackTrace();
            } catch (IllegalArgumentException e3) {
                e3.printStackTrace();
            } catch (InstantiationException e4) {
                e4.printStackTrace();
            } catch (IllegalAccessException e5) {
                e5.printStackTrace();
            } catch (InvocationTargetException e6) {
                e6.printStackTrace();
            }
            i++;
        }
        return arrayObject;
    }

    public static boolean isCollection(Class clas) {
        return collectionType.contains(clas);
    }

    public static Class getPrimitiveClass(Class clas) {
        if (primWraperMap.containsKey(clas)) {
        }
        return (Class) primWraperMap.get(clas);
    }
}
