package com.wqmobile.sdk.pojoxml.core;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.w3c.dom.Node;

public class PojoXmlInitInfo {
    private Map collectionClassMap = Collections.synchronizedMap(new HashMap());
    private Map fieldAliasMap = Collections.synchronizedMap(new HashMap());
    private Node rootNode;

    public Map getCollectionClassMap() {
        return this.collectionClassMap;
    }

    public void setCollectionClassMap(Map collectionClassMap2) {
        this.collectionClassMap = collectionClassMap2;
    }

    public void addCollectionClass(String elementName, Class cls) {
        this.collectionClassMap.put(elementName, cls);
    }

    public void addFieldAlias(String fieldName, String aliasName) {
        this.fieldAliasMap.put(fieldName, aliasName);
    }

    public Class getCollectionClass(String elementName) {
        return (Class) this.collectionClassMap.get(elementName);
    }

    public void setRootNode(Node rootNode2) {
        this.rootNode = rootNode2;
    }

    public Node getRootNode() {
        return this.rootNode;
    }

    public Object getAliasName(String fieldName) {
        if (this.fieldAliasMap == null) {
            return null;
        }
        return this.fieldAliasMap.get(fieldName);
    }

    public Object classForCollection(String elementName) {
        if (this.collectionClassMap == null) {
            return null;
        }
        return this.collectionClassMap.get(elementName);
    }

    public void setFieldAliasMap(Map fieldAliasMap2) {
        this.fieldAliasMap = fieldAliasMap2;
    }

    public Map getFieldAliasMap() {
        return this.fieldAliasMap;
    }
}
