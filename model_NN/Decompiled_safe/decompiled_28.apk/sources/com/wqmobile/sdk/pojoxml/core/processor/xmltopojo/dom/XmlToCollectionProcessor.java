package com.wqmobile.sdk.pojoxml.core.processor.xmltopojo.dom;

import com.wqmobile.sdk.pojoxml.core.PojoXmlException;
import com.wqmobile.sdk.pojoxml.util.ClassUtil;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;

public class XmlToCollectionProcessor {
    private Class collectionClass;
    private Object collectionData;
    private String setterName;

    public Object getCollectionData() {
        return this.collectionData;
    }

    public void setCollectionData(Object collectionData2) {
        this.collectionData = collectionData2;
    }

    public XmlToCollectionProcessor(String setterName2, Class valueClass) {
        try {
            this.setterName = setterName2;
            this.collectionClass = valueClass;
            Class listClass = Class.forName("java.util.List");
            Class setClass = Class.forName("java.util.Set");
            if (valueClass == listClass) {
                this.collectionData = new ArrayList();
            } else if (valueClass == setClass) {
                this.collectionData = new HashSet();
            } else if (listClass.isInstance(valueClass.newInstance()) || setClass.isInstance(valueClass.newInstance())) {
                this.collectionData = valueClass.newInstance();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addCollection(Object value) throws PojoXmlException {
        Class<Object> cls = Object.class;
        if (value == null) {
            try {
                this.collectionData.getClass().getMethod("add", Object.class).invoke(this.collectionData, new Object[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Class clas = value.getClass();
            Object setObject = null;
            Class wrapperClass = ClassUtil.getPrimitiveClass(clas);
            if (wrapperClass == null) {
                wrapperClass = clas;
            }
            if (clas.equals(Character.TYPE)) {
                setObject = new Character(value.toString().charAt(0));
            } else if (ClassUtil.isPrimitiveOrWrapper(clas)) {
                try {
                    setObject = wrapperClass.getConstructor(String.class).newInstance(value.toString());
                } catch (SecurityException e2) {
                    e2.printStackTrace();
                } catch (NoSuchMethodException e3) {
                    e3.printStackTrace();
                } catch (IllegalArgumentException e4) {
                    e4.printStackTrace();
                } catch (InstantiationException e5) {
                    e5.printStackTrace();
                } catch (IllegalAccessException e6) {
                    e6.printStackTrace();
                } catch (InvocationTargetException e7) {
                    e7.printStackTrace();
                }
            } else {
                setObject = value;
            }
            try {
                this.collectionData.getClass().getMethod("add", Object.class).invoke(this.collectionData, setObject);
            } catch (Exception e8) {
                e8.printStackTrace();
            }
        }
    }

    public void setSetterName(String setterName2) {
        this.setterName = setterName2;
    }

    public String getSetterName() {
        return this.setterName;
    }

    public void setCollectionClass(Class collectionClass2) {
        this.collectionClass = collectionClass2;
    }

    public Class getCollectionClass() {
        return this.collectionClass;
    }
}
