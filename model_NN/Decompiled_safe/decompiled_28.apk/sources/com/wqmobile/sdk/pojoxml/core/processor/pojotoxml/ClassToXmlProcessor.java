package com.wqmobile.sdk.pojoxml.core.processor.pojotoxml;

import com.wqmobile.sdk.pojoxml.core.PojoXmlInitInfo;
import com.wqmobile.sdk.pojoxml.util.ClassUtil;
import com.wqmobile.sdk.pojoxml.util.StringUtil;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import java.util.List;
import java.util.Map;

public class ClassToXmlProcessor extends XmlProcessor {
    private Map attributeMap;
    private Class clas;
    private Object object;
    private String rootName;
    private String[] tagNames;

    public /* bridge */ /* synthetic */ void decrementSpace() {
        super.decrementSpace();
    }

    public /* bridge */ /* synthetic */ String getAliasName(String str) {
        return super.getAliasName(str);
    }

    public /* bridge */ /* synthetic */ String getAttributes() {
        return super.getAttributes();
    }

    public /* bridge */ /* synthetic */ String getClassAliasName(String str) {
        return super.getClassAliasName(str);
    }

    public /* bridge */ /* synthetic */ String getClassName() {
        return super.getClassName();
    }

    public /* bridge */ /* synthetic */ String getElementName() {
        return super.getElementName();
    }

    public /* bridge */ /* synthetic */ PojoXmlInitInfo getPojoXmlInitInfo() {
        return super.getPojoXmlInitInfo();
    }

    public /* bridge */ /* synthetic */ int getSpace() {
        return super.getSpace();
    }

    public /* bridge */ /* synthetic */ String getXmlContent() {
        return super.getXmlContent();
    }

    public /* bridge */ /* synthetic */ void incrementSpace() {
        super.incrementSpace();
    }

    public /* bridge */ /* synthetic */ boolean isAttribute() {
        return super.isAttribute();
    }

    public /* bridge */ /* synthetic */ boolean isCdataEnabled() {
        return super.isCdataEnabled();
    }

    public /* bridge */ /* synthetic */ boolean isEmptyElement(Object obj, String str) {
        return super.isEmptyElement(obj, str);
    }

    public /* bridge */ /* synthetic */ boolean isEmptyElement(Object obj, String str, String str2) {
        return super.isEmptyElement(obj, str, str2);
    }

    public /* bridge */ /* synthetic */ boolean isStringFound() {
        return super.isStringFound();
    }

    public /* bridge */ /* synthetic */ void processCollection(Object obj) {
        super.processCollection(obj);
    }

    public /* bridge */ /* synthetic */ void processObject(Object obj) {
        super.processObject(obj);
    }

    public /* bridge */ /* synthetic */ void processObject(Object obj, String str) {
        super.processObject(obj, str);
    }

    public /* bridge */ /* synthetic */ void processObject(Object obj, String str, String str2) {
        super.processObject(obj, str, str2);
    }

    public /* bridge */ /* synthetic */ void setAttribute(boolean z) {
        super.setAttribute(z);
    }

    public /* bridge */ /* synthetic */ void setAttributes(String str) {
        super.setAttributes(str);
    }

    public /* bridge */ /* synthetic */ void setCdataEnabled(boolean z) {
        super.setCdataEnabled(z);
    }

    public /* bridge */ /* synthetic */ void setClassName(String str) {
        super.setClassName(str);
    }

    public /* bridge */ /* synthetic */ void setElementName(String str) {
        super.setElementName(str);
    }

    public /* bridge */ /* synthetic */ void setPojoXmlInitInfo(PojoXmlInitInfo pojoXmlInitInfo) {
        super.setPojoXmlInitInfo(pojoXmlInitInfo);
    }

    public /* bridge */ /* synthetic */ void setSpace(int i) {
        super.setSpace(i);
    }

    public /* bridge */ /* synthetic */ void setStringFound(boolean z) {
        super.setStringFound(z);
    }

    public /* bridge */ /* synthetic */ void setXmlContent(StringBuffer stringBuffer) {
        super.setXmlContent(stringBuffer);
    }

    public /* bridge */ /* synthetic */ String toString() {
        return super.toString();
    }

    public /* bridge */ /* synthetic */ void writeXmlContent(String str) {
        super.writeXmlContent(str);
    }

    public ClassToXmlProcessor(int space, PojoXmlInitInfo pojoXmlInitInfo) {
        this(null, "", space, pojoXmlInitInfo);
    }

    public ClassToXmlProcessor(int space, String elementName, PojoXmlInitInfo pojoXmlInitInfo) {
        this(elementName, "", space, pojoXmlInitInfo);
    }

    public ClassToXmlProcessor(String elementName, String attributes, int space, PojoXmlInitInfo pojoXmlInitInfo) {
        super(elementName, attributes, space, pojoXmlInitInfo);
    }

    private void init() {
        XmlProcessorInitInfo initInfo = XmlProcessorIntializer.getInstanceVariablesAndAttributes(getClas());
        setTagNames(initInfo.elements);
        if (initInfo.isAttribute()) {
            setAttribute(initInfo.isAttribute());
            setAttributeMap(initInfo.getAttributes());
        }
        if (getElementName() == null) {
            String rootName2 = getClassAliasName(getClas().getName());
            if (rootName2 == null) {
                setRootName(ClassUtil.getClassName(getClas()));
            } else {
                setRootName(rootName2);
            }
        } else {
            setRootName(getElementName());
        }
        writeXmlContent(getStartTagWithNL(getRootName(), getAttributes(), getSpace()));
        incrementSpace();
    }

    public String process(Object object2) {
        if (object2 == null) {
            writeXmlContent(getStartTagWithNL(getRootName(), getAttributes(), getSpace()));
            return getXmlContent();
        }
        this.clas = object2.getClass();
        setClassName(this.clas.getName());
        this.object = object2;
        init();
        toXml();
        return getXmlContent();
    }

    private void toXml() {
        for (String classElementName : this.tagNames) {
            if (ClassUtil.isPrimitiveOrWrapper(getClas(), classElementName)) {
                readPrimitiveOrWrapperGetters(classElementName);
            } else if (ClassUtil.isArray(getClas(), classElementName)) {
                readArray(classElementName);
            } else if (ClassUtil.isCollection(getClas(), getObject(), classElementName)) {
                readCollection(classElementName);
            } else {
                readObjectGetters(classElementName);
            }
        }
        decrementSpace();
        writeXmlContent(getCloseTag(getRootName(), getSpace()));
    }

    public void readPrimitiveOrWrapperGetters(String elementName) {
        String elementValue = StringUtil.getActualValue(ClassUtil.invokeGetter(elementName, this.clas, this.object), isCdataEnabled());
        if (elementValue != null && elementValue.length() > 0) {
            String elementName2 = String.valueOf(elementName.substring(0, 1).toUpperCase()) + elementName.substring(1);
            writeXmlContent(getElementWithNL(elementName2, elementAttributes(elementName2), elementValue, getSpace()));
        }
    }

    public String elementAttributes(String elementName) {
        if (!isAttribute() || this.attributeMap == null || !this.attributeMap.containsKey(elementName)) {
            return "";
        }
        String attributeNameAndValues = "";
        for (Object obj : (List) this.attributeMap.get(elementName)) {
            String attribProperty = obj.toString();
            attributeNameAndValues = String.valueOf(attributeNameAndValues) + (XmlConstant.SINGLE_SPACE + StringUtil.initSmall(attribProperty.substring(9).substring(elementName.length()))) + XmlConstant.EQUAL + XmlConstant.QUOTE + StringUtil.getActualValue(ClassUtil.invokeGetter(attribProperty, this.clas, this.object), isCdataEnabled()) + XmlConstant.QUOTE;
        }
        return attributeNameAndValues;
    }

    public void readArray(String elementName) {
        ArrayToXmlProcessor arrayToXmlProcessor = new ArrayToXmlProcessor(elementName, elementAttributes(elementName), getSpace(), getPojoXmlInitInfo());
        arrayToXmlProcessor.setCdataEnabled(isCdataEnabled());
        arrayToXmlProcessor.setClassName(getClassName());
        Object arrayObject = ClassUtil.invokeGetter(elementName, getClas(), getObject());
        if (arrayObject != null) {
            writeXmlContent(arrayToXmlProcessor.process(arrayObject));
        }
    }

    /* access modifiers changed from: package-private */
    public void readCollection(String elementName) {
        CollectionToXmlProcessor collectionToXmlProcessor = new CollectionToXmlProcessor(elementName, getSpace(), getPojoXmlInitInfo());
        collectionToXmlProcessor.setCdataEnabled(isCdataEnabled());
        collectionToXmlProcessor.setClassName(getClassName());
        if (isAttribute()) {
            collectionToXmlProcessor.setAttributes(elementAttributes(elementName));
        }
        Object collectionObject = ClassUtil.invokeGetter(elementName, getClas(), getObject());
        if (collectionObject != null) {
            writeXmlContent(collectionToXmlProcessor.process(collectionObject));
        }
    }

    /* access modifiers changed from: package-private */
    public void readObjectGetters(String classElementName) {
        Object otherObj = ClassUtil.invokeGetter(classElementName, this.clas, this.object);
        String attrib = elementAttributes(classElementName);
        if (!isEmptyElement(otherObj, classElementName, attrib)) {
            processObject(otherObj, classElementName, attrib);
        }
    }

    public String[] getTagNames() {
        return this.tagNames;
    }

    public void setTagNames(String[] tagNames2) {
        this.tagNames = tagNames2;
    }

    public void setRootName(String rootName2) {
        this.rootName = rootName2;
    }

    public String getRootName() {
        return this.rootName;
    }

    public Object getObject() {
        return this.object;
    }

    public void setObject(Object object2) {
        this.object = object2;
    }

    public Class getClas() {
        return this.clas;
    }

    public void setClas(Class clas2) {
        this.clas = clas2;
    }

    public void setAttributeMap(Map attributeMap2) {
        this.attributeMap = attributeMap2;
    }

    public Map getAttributeMap() {
        return this.attributeMap;
    }
}
