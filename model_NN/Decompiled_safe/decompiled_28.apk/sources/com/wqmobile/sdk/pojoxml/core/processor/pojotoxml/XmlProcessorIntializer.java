package com.wqmobile.sdk.pojoxml.core.processor.pojotoxml;

import com.wqmobile.sdk.pojoxml.util.ClassUtil;
import com.wqmobile.sdk.pojoxml.util.StringUtil;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class XmlProcessorIntializer {
    XmlProcessorIntializer() {
    }

    /* JADX INFO: Multiple debug info for r7v0 java.util.Iterator: [D('maybeItr' java.util.Iterator), D('name' java.lang.String)] */
    public static XmlProcessorInitInfo getInstanceVariablesAndAttributes(Class clas) {
        Field[] fields = ClassUtil.getFields(clas);
        int length = fields.length;
        List<Object> elementList = new ArrayList<>();
        List<Object> attributeMayBeList = new ArrayList<>();
        Map attributes = new HashMap();
        XmlProcessorInitInfo initInfo = new XmlProcessorInitInfo();
        boolean attribute = false;
        for (int i = 0; i < length; i++) {
            if (fields[i].getName().startsWith(XmlConstant.ATTRIBUTE)) {
                attribute = true;
                attributeMayBeList.add(fields[i].getName().substring(9));
            }
            elementList.add(fields[i].getName());
        }
        if (!attribute) {
            initInfo.setAttribute(false);
            initInfo.setElements(StringUtil.toStringArray(elementList.toArray()));
            return initInfo;
        }
        for (Object obj : attributeMayBeList) {
            String attributeName = obj.toString();
            String attributeNameToCheck = StringUtil.initSmall(attributeName);
            boolean attributeFound = false;
            for (Object obj2 : elementList) {
                String elementName = obj2.toString();
                if (attributeNameToCheck.startsWith(elementName) && attributeNameToCheck.length() > elementName.length()) {
                    if (!attributes.containsKey(elementName)) {
                        List attribList = new ArrayList();
                        attribList.add(XmlConstant.ATTRIBUTE + attributeName);
                        attributes.put(elementName, attribList);
                    } else {
                        ((List) attributes.get(elementName)).add(XmlConstant.ATTRIBUTE + attributeName);
                    }
                    attributeFound = true;
                }
            }
            if (attributeFound) {
                elementList.remove(XmlConstant.ATTRIBUTE + attributeName);
                initInfo.setAttribute(true);
            }
        }
        initInfo.setAttributes(attributes);
        initInfo.setElements(StringUtil.toStringArray(elementList.toArray()));
        return initInfo;
    }
}
