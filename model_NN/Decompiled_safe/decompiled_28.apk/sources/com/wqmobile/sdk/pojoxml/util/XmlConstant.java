package com.wqmobile.sdk.pojoxml.util;

public interface XmlConstant {
    public static final String ATTRIBUTE = "attribute";
    public static final int ATTRIBUTE_LENGTH = 9;
    public static final String CLOSE_TAG = ">";
    public static final String DEFAULT_ENCODING = "UTF-8";
    public static final String EMPTY_TAG = "/>";
    public static final String END_TAG = "</";
    public static final String EQUAL = "=";
    public static final String HEAD_CLOSE = "\"?>";
    public static final String HEAD_OPEN = "<?xml version=\"1.0\" encoding=\"";
    public static final String NL = "\n";
    public static final String NOTHING = "";
    public static final int NO_SPACE = 0;
    public static final String QUOTE = "\"";
    public static final String SINGLE_SPACE = " ";
    public static final String SPACE = "  ";
    public static final String START_TAG = "<";
}
