package com.wqmobile.sdk.pojoxml.core;

import java.io.InputStream;

public interface PojoXml {
    void addClassAlias(Class cls, String str);

    void addCollectionClass(String str, Class cls);

    void addFieldAlias(Class cls, String str, String str2);

    void enableCDATA(boolean z);

    Object getPojo(InputStream inputStream, Class cls);

    Object getPojo(String str, Class cls);

    Object getPojoFromFile(String str, Class cls);

    String getXml(Object obj);

    void saveXml(Object obj, String str);

    void setEncoding(String str);
}
