package com.wqmobile.sdk.pojoxml.core.processor.xmltopojo.dom;

import com.wqmobile.sdk.pojoxml.core.PojoXmlInitInfo;
import com.wqmobile.sdk.pojoxml.util.ClassUtil;
import com.wqmobile.sdk.pojoxml.util.StringUtil;
import java.util.List;
import java.util.Map;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlToObjectProcessor extends ObjectProcessor {
    private static final boolean ARRAY_NOT_READING = false;
    private static final boolean ARRAY_READING = true;
    private static final boolean CHILD_EMPTY = true;
    private static final boolean CHILD_NOT_EMPTY = false;
    private static final boolean COLLECTION_NOT_READING = false;
    private static final boolean COLLECTION_READING = true;
    private XmlToArrayProcessor arrayProcessor;
    private boolean arrayReading = false;
    private Class clas;
    private XmlToCollectionProcessor collectionProcessor;
    private boolean collectionReading = false;
    private String currentNodeName;
    private Object currentNodeValue;
    private boolean isChildEmpty = true;
    private Object object;
    private Map setterMethodMap;

    public /* bridge */ /* synthetic */ String getElementName(Node node) {
        return super.getElementName(node);
    }

    public /* bridge */ /* synthetic */ PojoXmlInitInfo getInitInfo() {
        return super.getInitInfo();
    }

    public /* bridge */ /* synthetic */ Node getRootNode() {
        return super.getRootNode();
    }

    public /* bridge */ /* synthetic */ void setInitInfo(PojoXmlInitInfo pojoXmlInitInfo) {
        super.setInitInfo(pojoXmlInitInfo);
    }

    public /* bridge */ /* synthetic */ void setRootNode(Node node) {
        super.setRootNode(node);
    }

    public XmlToObjectProcessor(Class clas2, PojoXmlInitInfo initInfo) {
        super(initInfo);
        this.clas = clas2;
        if (clas2 != null) {
            init();
            readChildren(this.rootNode);
            checkPendingOperation();
        }
    }

    private void init() {
        if (this.clas != null) {
            setSetterMethodMap(ClassUtil.getSetterAndClass(this.clas));
            setObject(ClassUtil.createObject(this.clas));
        }
    }

    private void checkPendingOperation() {
        if (isArrayReading()) {
            processArray();
        }
        if (isCollectionReading()) {
            processCollection();
        }
    }

    private void readNode(Node node) {
        readChildren(node);
    }

    private void readChildren(Node node) {
        if (node.hasChildNodes()) {
            setCurrentNodeName(node.getNodeName());
            NodeList childNodes = node.getChildNodes();
            this.isChildEmpty = false;
            for (int i = 0; i < childNodes.getLength(); i++) {
                Node child = childNodes.item(i);
                if (child.getNodeType() == 1) {
                    if (!processElementNodeContinue(child)) {
                        readNode(child);
                    }
                } else if (child.getNodeType() == 3 || child.getNodeType() == 4) {
                    setCurrentNodeValue(child.getNodeValue());
                }
            }
            if (getCurrentNodeName() != null) {
                setValuesToCurrentObject(getCurrentNodeName(), getCurrentNodeValue());
                setCurrentNodeName(null);
            }
        }
    }

    private boolean processElementNodeContinue(Node child) {
        Class valueClass = (Class) this.setterMethodMap.get(getElementName(child));
        if (valueClass == null) {
            return true;
        }
        String setterName = getActualValueFromAleasName(child.getNodeName());
        if (ClassUtil.isArray(valueClass)) {
            if (ClassUtil.isPrimitiveArray(valueClass) && child.getNodeValue().trim().length() == 0) {
                return true;
            }
            setArrayReading(true);
            if (child.getNodeValue() != null && child.getNodeValue().trim().length() == 0) {
                addArray(setterName, valueClass, null);
            }
        }
        if (ClassUtil.isCollection(valueClass)) {
            setCollectionReading(true);
            PojoXmlInitInfo init = new PojoXmlInitInfo();
            init.setCollectionClassMap(getInitInfo().getCollectionClassMap());
            init.setRootNode(child);
            addCollection(setterName, valueClass, new XmlToCollectionHandler(init, setterName).processCollection());
            processCollection();
            return true;
        } else if (ClassUtil.isPrimitiveOrWrapper(valueClass)) {
            return false;
        } else {
            if (ClassUtil.isArray(valueClass)) {
                Class valClass = valueClass.getComponentType();
                PojoXmlInitInfo initInfo = new PojoXmlInitInfo();
                initInfo.setRootNode(child);
                int length = -1;
                if (child.getChildNodes().getLength() == 0) {
                    length = 0;
                }
                if (length == 0) {
                    return true;
                }
                addArray(setterName, valueClass, new XmlToObjectProcessor(valClass, initInfo).getObject());
                return true;
            }
            System.out.print(String.valueOf(valueClass.getName()) + "===" + child.getNodeValue());
            setInnerObject(valueClass, child);
            return true;
        }
    }

    private void setValuesToCurrentObject(String setterName, Object value) {
        Class valueClass = (Class) this.setterMethodMap.get(StringUtil.initCap(setterName));
        if (this.arrayReading) {
            if (ClassUtil.isArray(valueClass)) {
                addArray(setterName, (Class) this.setterMethodMap.get(StringUtil.initCap(setterName)), value);
                return;
            }
            processArray();
        }
        if (ClassUtil.isPrimitiveOrWrapper(valueClass)) {
            ClassUtil.invokeSetter(this.clas, this.object, setterName, ClassUtil.ObjectFromValue(value, valueClass), valueClass);
        }
    }

    private void setInnerObject(Class innerClas, Node node) {
        int length = -1;
        String nodeName = getActualValueFromAleasName(node.getNodeName());
        if (node.getChildNodes().getLength() == 0) {
            length = 0;
        }
        getInitInfo().setRootNode(node);
        Object innerObject = new XmlToObjectProcessor(innerClas, getInitInfo()).getObject();
        if (length == 0) {
            innerObject = null;
        }
        if (!isArrayReading()) {
            ClassUtil.invokeSetter(this.clas, this.object, nodeName, innerObject, innerClas);
        } else if (!addArray(nodeName, innerClas, innerObject)) {
            ClassUtil.invokeSetter(this.clas, this.object, nodeName, innerObject, innerClas);
        }
    }

    private boolean addArray(String setterName, Class valueClass, Object value) {
        if (this.arrayProcessor == null) {
            this.arrayProcessor = new XmlToArrayProcessor(setterName, valueClass);
            setArrayReading(true);
        }
        if (this.arrayProcessor.getSetterName().equals(setterName)) {
            this.arrayProcessor.addArrayValue(value);
        } else {
            processArray();
            Class valueClass2 = (Class) this.setterMethodMap.get(StringUtil.initCap(setterName));
            if (!ClassUtil.isArray(valueClass2)) {
                return false;
            }
            addArray(setterName, valueClass2, value);
        }
        return true;
    }

    private boolean addCollection(String setterName, Class valueClass, Object value) {
        if (this.collectionProcessor == null) {
            this.collectionProcessor = new XmlToCollectionProcessor(setterName, valueClass);
        }
        if (checkCollectionContainsObject(setterName, valueClass, value)) {
            return true;
        }
        Object alreadyPresentedValue = ClassUtil.invokeGetter(this.collectionProcessor.getSetterName(), this.clas, this.object);
        if (alreadyPresentedValue != null) {
            this.collectionProcessor.setCollectionData(alreadyPresentedValue);
        }
        this.collectionProcessor.addCollection(value);
        return true;
    }

    private boolean checkCollectionContainsObject(String setterName, Class valueClass, Object value) {
        if (!ClassUtil.isCollection(value)) {
            return false;
        }
        for (Object addCollection : (List) value) {
            addCollection(setterName, valueClass, addCollection);
        }
        return true;
    }

    private void processArray() {
        ClassUtil.invokeSetter(this.clas, this.object, this.arrayProcessor.getSetterName(), this.arrayProcessor.processArray(), this.arrayProcessor.getArrayClass());
        setArrayReading(false);
        this.arrayProcessor = null;
    }

    private void processCollection() {
        ClassUtil.invokeSetter(this.clas, this.object, this.collectionProcessor.getSetterName(), this.collectionProcessor.getCollectionData(), this.collectionProcessor.getCollectionClass());
        setCollectionReading(false);
        this.collectionProcessor = null;
    }

    public String getCurrentNodeName() {
        return getActualValueFromAleasName(this.currentNodeName);
    }

    public void setCurrentNodeName(String currentNodeName2) {
        this.currentNodeName = currentNodeName2;
    }

    public Map getSetterMethodMap() {
        return this.setterMethodMap;
    }

    public void setSetterMethodMap(Map setterMethodMap2) {
        this.setterMethodMap = setterMethodMap2;
    }

    public Class getClas() {
        return this.clas;
    }

    public void setClas(Class clas2) {
        this.clas = clas2;
    }

    public void setObject(Object object2) {
        this.object = object2;
    }

    public Object getObject() {
        return this.object;
    }

    public void setCurrentNodeValue(Object currentNodeValue2) {
        this.currentNodeValue = currentNodeValue2;
    }

    public Object getCurrentNodeValue() {
        return this.currentNodeValue;
    }

    public Object getParsedObject() {
        if (this.isChildEmpty) {
            return null;
        }
        return this.object;
    }

    public void setArrayReading(boolean arrayReading2) {
        this.arrayReading = arrayReading2;
    }

    public boolean isArrayReading() {
        return this.arrayReading;
    }

    public void setCollectionReading(boolean collectionReading2) {
        this.collectionReading = collectionReading2;
    }

    public boolean isCollectionReading() {
        return this.collectionReading;
    }
}
