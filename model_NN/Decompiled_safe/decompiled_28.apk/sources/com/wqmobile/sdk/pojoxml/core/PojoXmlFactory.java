package com.wqmobile.sdk.pojoxml.core;

import com.wqmobile.sdk.model.AdvertisementFile;
import com.wqmobile.sdk.model.AdvertisementProperties;
import com.wqmobile.sdk.model.DynClientProfile;
import com.wqmobile.sdk.model.actiontype.ClickToMail;
import com.wqmobile.sdk.model.actiontype.ClickToMedia;
import com.wqmobile.sdk.model.actiontype.ClickToSMS;
import com.wqmobile.sdk.model.actiontype.DownloadAndroid;
import com.wqmobile.sdk.model.actiontype.MailContent;

public class PojoXmlFactory {
    private static PojoXmlFactory instance = null;

    public static synchronized PojoXmlFactory getInstance() {
        PojoXmlFactory pojoXmlFactory;
        synchronized (PojoXmlFactory.class) {
            if (instance == null) {
                instance = new PojoXmlFactory();
            }
            pojoXmlFactory = instance;
        }
        return pojoXmlFactory;
    }

    public PojoXml createPojoXml() {
        return new PojoXmlImpl();
    }

    public PojoXml CreateCacheOfflineAds() {
        PojoXml result = CreateAdvertisementProperties();
        result.addCollectionClass("AdvertisementProperties", AdvertisementProperties.class);
        return result;
    }

    public PojoXml CreateAdvertisementProperties() {
        Class<AdvertisementFile> cls = AdvertisementFile.class;
        PojoXmlImpl result = new PojoXmlImpl();
        result.enableCDATA(true);
        Class<AdvertisementFile> cls2 = AdvertisementFile.class;
        result.addClassAlias(cls, "File");
        result.addClassAlias(MailContent.class, "content");
        Class<AdvertisementFile> cls3 = AdvertisementFile.class;
        result.addCollectionClass("File", cls);
        Class<AdvertisementFile> cls4 = AdvertisementFile.class;
        result.addFieldAlias(cls, "File", "File");
        result.addFieldAlias(ClickToMail.class, "Target", "target");
        result.addFieldAlias(ClickToMail.class, "content", "content");
        result.addFieldAlias(ClickToSMS.class, "Content", "content");
        result.addFieldAlias(MailContent.class, "Title", "title");
        result.addFieldAlias(MailContent.class, "Body", "body");
        result.addFieldAlias(ClickToMedia.class, "Target", "target");
        result.addFieldAlias(DownloadAndroid.class, "Target", "target");
        return result;
    }

    public PojoXml CreateDynamicClientProfile() {
        Class<DynClientProfile> cls = DynClientProfile.class;
        PojoXmlImpl result = new PojoXmlImpl();
        result.enableCDATA(true);
        Class<DynClientProfile> cls2 = DynClientProfile.class;
        result.addClassAlias(cls, "ClientProfile");
        Class<DynClientProfile> cls3 = DynClientProfile.class;
        result.addFieldAlias(cls, "ClientProfile", "ClientProfile");
        return result;
    }
}
