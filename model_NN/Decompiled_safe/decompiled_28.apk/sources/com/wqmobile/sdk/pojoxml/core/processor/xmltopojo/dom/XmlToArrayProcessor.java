package com.wqmobile.sdk.pojoxml.core.processor.xmltopojo.dom;

import com.wqmobile.sdk.pojoxml.util.ClassUtil;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XmlToArrayProcessor {
    private static Map arrayClassMap = new HashMap();
    private Class arrayClass;
    private List arrayHandler = new ArrayList();
    private String setterName;

    static {
        arrayClassMap.put(byte[].class, Byte.TYPE);
        arrayClassMap.put(Byte[].class, Byte.class);
        arrayClassMap.put(short[].class, Short.TYPE);
        arrayClassMap.put(Short[].class, Short.class);
        arrayClassMap.put(int[].class, Integer.TYPE);
        arrayClassMap.put(Integer[].class, Integer.class);
        arrayClassMap.put(long[].class, Long.TYPE);
        arrayClassMap.put(Long[].class, Long.class);
        arrayClassMap.put(float[].class, Float.TYPE);
        arrayClassMap.put(Float[].class, Float.class);
        arrayClassMap.put(double[].class, Double.TYPE);
        arrayClassMap.put(Double[].class, Double.class);
        arrayClassMap.put(String[].class, String.class);
        arrayClassMap.put(char[].class, Character.TYPE);
        arrayClassMap.put(Character[].class, Character.class);
        arrayClassMap.put(boolean[].class, Boolean.TYPE);
        arrayClassMap.put(Boolean[].class, Boolean.class);
        arrayClassMap.put(BigDecimal[].class, BigDecimal.class);
        arrayClassMap.put(BigInteger[].class, BigInteger.class);
    }

    public XmlToArrayProcessor(String setterName2, Class arrayClass2) {
        this.arrayClass = arrayClass2;
        setSetterName(setterName2);
    }

    public void addArrayValue(Object arrayValue) {
        if (arrayValue instanceof List) {
            this.arrayHandler = (List) arrayValue;
        } else {
            this.arrayHandler.add(arrayValue);
        }
    }

    public Object processArray() {
        Class cls = (Class) arrayClassMap.get(this.arrayClass);
        if (cls == null) {
            cls = this.arrayClass;
        }
        if (this.arrayHandler == null) {
            return null;
        }
        return ClassUtil.createArray(this.arrayHandler, cls);
    }

    public void setArrayHandler(List arrayHandler2) {
        this.arrayHandler = arrayHandler2;
    }

    public List getArrayHandler() {
        return this.arrayHandler;
    }

    public void setArrayClass(Class arrayClass2) {
        this.arrayClass = arrayClass2;
    }

    public Class getArrayClass() {
        return this.arrayClass;
    }

    public void setSetterName(String setterName2) {
        this.setterName = setterName2;
    }

    public String getSetterName() {
        return this.setterName;
    }
}
