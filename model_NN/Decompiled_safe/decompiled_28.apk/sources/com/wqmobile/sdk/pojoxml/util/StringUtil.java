package com.wqmobile.sdk.pojoxml.util;

public class StringUtil {
    public static String[] toStringArray(Object[] objects) {
        String[] stringArr = new String[objects.length];
        for (int i = 0; i < objects.length; i++) {
            stringArr[i] = objects[i].toString();
        }
        return stringArr;
    }

    public static String initCap(String str) {
        char[] chars = str.toCharArray();
        chars[0] = Character.toUpperCase(chars[0]);
        return new String(chars);
    }

    public static String initSmall(String str) {
        char[] chars = str.toCharArray();
        chars[0] = Character.toLowerCase(chars[0]);
        return new String(chars);
    }

    public static String getActualValue(Object object, boolean cdataEnabled) {
        if (object == null) {
            return "";
        }
        if (!cdataEnabled || !object.getClass().equals(String.class)) {
            return object.toString();
        }
        return "<![CDATA[" + object.toString() + "]]>";
    }

    public static String subString(String string, int index) {
        if (string == null) {
            return null;
        }
        return string.substring(index);
    }

    public static void PrintExceptionStatck(Exception ex) {
    }
}
