package com.wqmobile.sdk.pojoxml.core.processor.pojotoxml;

import java.util.Map;

class XmlProcessorInitInfo {
    private boolean attribute = false;
    private Map attributes;
    public String[] elements;

    XmlProcessorInitInfo() {
    }

    public String[] getElements() {
        return this.elements;
    }

    public void setElements(String[] elements2) {
        this.elements = elements2;
    }

    public Map getAttributes() {
        return this.attributes;
    }

    public void setAttributes(Map attributes2) {
        this.attributes = attributes2;
    }

    public boolean isAttribute() {
        return this.attribute;
    }

    public void setAttribute(boolean attribute2) {
        this.attribute = attribute2;
    }
}
