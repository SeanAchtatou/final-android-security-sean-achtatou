package com.wqmobile.sdk.pojoxml.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class XmlWriter {
    private BufferedWriter buffer;
    private File file;
    private String fileName;
    private FileWriter writer;

    public XmlWriter(String fileName2) {
        this.fileName = fileName2;
        init();
    }

    private void init() {
        this.file = new File(this.fileName);
        try {
            this.writer = new FileWriter(this.file);
            this.buffer = new BufferedWriter(this.writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(String content) {
        try {
            this.writer.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        Assert.isNull(this.writer, "Intialise before closing");
        try {
            this.buffer.flush();
            this.writer.close();
            this.fileName = null;
            this.file = null;
            this.writer = null;
            this.buffer = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
