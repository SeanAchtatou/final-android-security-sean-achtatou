package com.wqmobile.sdk.pojoxml.exeception;

import com.wqmobile.sdk.pojoxml.core.PojoXmlException;

public class ClassHandlerException extends PojoXmlException {
    private static final long serialVersionUID = 7866025573012654251L;

    public ClassHandlerException() {
    }

    public ClassHandlerException(String message) {
        super(message);
    }

    public ClassHandlerException(Throwable cause) {
        super(cause);
    }
}
