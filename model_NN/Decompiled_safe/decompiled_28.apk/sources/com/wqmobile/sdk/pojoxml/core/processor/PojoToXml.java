package com.wqmobile.sdk.pojoxml.core.processor;

import com.wqmobile.sdk.pojoxml.core.PojoXmlInitInfo;
import com.wqmobile.sdk.pojoxml.core.processor.pojotoxml.ClassToXmlProcessor;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import com.wqmobile.sdk.pojoxml.util.XmlUtil;
import com.wqmobile.sdk.pojoxml.util.XmlWriter;

public class PojoToXml {
    private boolean cDataEnabled;
    private String encoding;
    private boolean fileWriter = false;
    private PojoXmlInitInfo initInfo;
    private XmlWriter writer;
    private StringBuffer xmlContent;

    public PojoToXml(PojoXmlInitInfo pojoInitInfo) {
        this.initInfo = pojoInitInfo;
    }

    public String getXml(Object object) {
        ClassToXmlProcessor processor = new ClassToXmlProcessor(0, this.initInfo);
        processor.setCdataEnabled(this.cDataEnabled);
        writeXmlContent(processor.process(object));
        return this.xmlContent.toString();
    }

    public void saveXml(Object object, String fileName) {
        ClassToXmlProcessor processor = new ClassToXmlProcessor(0, this.initInfo);
        processor.setCdataEnabled(this.cDataEnabled);
        this.fileWriter = true;
        this.writer = new XmlWriter(fileName);
        writeXmlContent(processor.process(object));
        close();
    }

    private void close() {
        this.writer.close();
    }

    public void writeXmlContent(String xml) {
        if (this.xmlContent == null) {
            this.xmlContent = new StringBuffer(XmlConstant.HEAD_OPEN + XmlUtil.getEncoding(this.encoding) + XmlConstant.HEAD_CLOSE);
        }
        this.xmlContent.append(xml);
        if (this.fileWriter) {
            this.writer.write(this.xmlContent.toString());
        }
    }

    public void setEncoding(String encoding2) {
        this.encoding = encoding2;
    }

    public String getEncoding() {
        return this.encoding;
    }

    public void setCDataEnabled(boolean cDataEnabled2) {
        this.cDataEnabled = cDataEnabled2;
    }

    public boolean isCDataEnabled() {
        return this.cDataEnabled;
    }

    public void setInitInfo(PojoXmlInitInfo initInfo2) {
        this.initInfo = initInfo2;
    }

    public PojoXmlInitInfo getInitInfo() {
        return this.initInfo;
    }
}
