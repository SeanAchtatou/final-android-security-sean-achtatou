package com.wqmobile.sdk.pojoxml.core.processor;

import com.wqmobile.sdk.pojoxml.core.PojoXmlException;
import com.wqmobile.sdk.pojoxml.core.PojoXmlInitInfo;
import com.wqmobile.sdk.pojoxml.core.processor.xmltopojo.dom.XmlToObjectProcessor;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XmlToPojo {
    private Class clas;

    public XmlToPojo(Class clas2) {
        this.clas = clas2;
    }

    public Object parseXMLFromFile(String fileName, PojoXmlInitInfo initInfo) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setCoalescing(true);
        factory.setIgnoringComments(true);
        factory.setIgnoringElementContentWhitespace(true);
        factory.setNamespaceAware(true);
        factory.setValidating(false);
        try {
            try {
                initInfo.setRootNode(factory.newDocumentBuilder().parse(new File(fileName)).getDocumentElement());
                return new XmlToObjectProcessor(this.clas, initInfo).getParsedObject();
            } catch (SAXException e) {
                throw new PojoXmlException("Parser Initialization issue : " + e);
            } catch (IOException e2) {
                throw new PojoXmlException("Parser Initialization issue : " + e2);
            }
        } catch (ParserConfigurationException e3) {
            throw new PojoXmlException("Configuration issue : " + e3);
        }
    }

    public Object parseXMLFromInputStream(InputStream input, PojoXmlInitInfo initInfo) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setCoalescing(true);
        factory.setIgnoringComments(true);
        factory.setIgnoringElementContentWhitespace(true);
        factory.setNamespaceAware(true);
        factory.setValidating(false);
        try {
            try {
                initInfo.setRootNode(factory.newDocumentBuilder().parse(new InputSource(input)).getDocumentElement());
                return new XmlToObjectProcessor(this.clas, initInfo).getParsedObject();
            } catch (SAXException e) {
                throw new PojoXmlException("Parser Initialization issue : " + e);
            } catch (IOException e2) {
                throw new PojoXmlException("Parser Initialization issue : " + e2);
            }
        } catch (ParserConfigurationException e3) {
            throw new PojoXmlException("Configuration issue : " + e3);
        }
    }

    public Object parseXMLFromXmlData(String xmlData, PojoXmlInitInfo initInfo) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setCoalescing(false);
        factory.setIgnoringComments(true);
        factory.setIgnoringElementContentWhitespace(true);
        factory.setNamespaceAware(true);
        factory.setValidating(false);
        try {
            try {
                initInfo.setRootNode(factory.newDocumentBuilder().parse(new ByteArrayInputStream(xmlData.getBytes())).getDocumentElement());
                return new XmlToObjectProcessor(this.clas, initInfo).getParsedObject();
            } catch (SAXException e) {
                throw new PojoXmlException("Parser Initialization issue : " + e);
            } catch (IOException e2) {
                throw new PojoXmlException("Parser Initialization issue : " + e2);
            }
        } catch (ParserConfigurationException e3) {
            throw new PojoXmlException("Configuration issue : " + e3);
        }
    }

    public void setClas(Class clas2) {
        this.clas = clas2;
    }

    public Class getClas() {
        return this.clas;
    }
}
