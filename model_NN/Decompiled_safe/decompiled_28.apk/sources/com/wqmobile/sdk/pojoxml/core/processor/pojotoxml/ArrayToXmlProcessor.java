package com.wqmobile.sdk.pojoxml.core.processor.pojotoxml;

import com.wqmobile.sdk.pojoxml.core.PojoXmlInitInfo;
import com.wqmobile.sdk.pojoxml.util.ClassUtil;
import com.wqmobile.sdk.pojoxml.util.StringUtil;
import java.lang.reflect.Array;

class ArrayToXmlProcessor extends XmlProcessor {
    public ArrayToXmlProcessor(String elementName, String attrib, int space, PojoXmlInitInfo pojoXmlInitInfo) {
        super(elementName, attrib, space, pojoXmlInitInfo);
    }

    public String process(Object arrayObject) {
        if (isEmptyElement(arrayObject, getElementName())) {
            return getXmlContent();
        }
        int length = ClassUtil.getArrayLength(arrayObject);
        if (length == 0) {
            return createEmptyElementWithNL(getElementName(), getAttributes(), getSpace());
        }
        boolean isPrimitive = ClassUtil.isPrimitiveOrWrapper(arrayObject.getClass());
        for (int i = 0; i < length; i++) {
            Object arrayElement = Array.get(arrayObject, i);
            boolean isWrapperArray = ClassUtil.isWrapperArray(arrayElement);
            boolean isCollectionArray = ClassUtil.isCollectionArray(arrayElement);
            if (!isNullArrayObject(arrayElement, arrayObject.getClass(), isPrimitive, isWrapperArray, isCollectionArray, i, length)) {
                if (isPrimitive || isWrapperArray) {
                    writeXmlContent(getElementWithNL(getElementName(), getAttributes(), StringUtil.getActualValue(arrayElement, isCdataEnabled()), getSpace()));
                } else if (isCollectionArray) {
                    writeXmlContent(getStartTagWithNL(getElementName(), getAttributes(), getSpace()));
                    incrementSpace();
                    processCollection(arrayElement);
                    decrementSpace();
                    writeXmlContent(getCloseTagWithNL(getElementName(), getSpace()));
                } else {
                    processObject(arrayElement, getElementName());
                }
            }
        }
        return getXmlContent();
    }

    private boolean isNullArrayObject(Object arrayElement, Class arrayType, boolean isPrimitve, boolean isWrapper, boolean isCollection, int start, int length) {
        if (arrayElement != null) {
            return false;
        }
        if (isPrimitve || isWrapper) {
            writeXmlContent(createEmptyElementWithNL(getElementName(), getAttributes(), getSpace()));
            return true;
        }
        writeXmlContent(createEmptyElementWithNL(getElementName(), getAttributes(), getSpace()));
        return true;
    }
}
