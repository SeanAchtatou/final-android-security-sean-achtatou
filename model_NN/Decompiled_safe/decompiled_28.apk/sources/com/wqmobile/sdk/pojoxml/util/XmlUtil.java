package com.wqmobile.sdk.pojoxml.util;

public class XmlUtil {
    public static String getElement(String elementName, String elementValue, int space) {
        return XmlConstant.START_TAG + elementName + XmlConstant.CLOSE_TAG + elementValue + XmlConstant.END_TAG + elementName + XmlConstant.CLOSE_TAG;
    }

    public static String getElementWithNL(String elementName, String attributes, String elementValue, int space) {
        if (elementValue.equals("")) {
            return createEmptyElementWithNL(elementName, space);
        }
        return XmlConstant.START_TAG + elementName + attributes + XmlConstant.CLOSE_TAG + elementValue + XmlConstant.END_TAG + elementName + XmlConstant.CLOSE_TAG;
    }

    public static String getCloseTag(String elementName, int space) {
        return XmlConstant.END_TAG + elementName + XmlConstant.CLOSE_TAG;
    }

    public static String getCloseTagWithNL(String elementName, int space) {
        return XmlConstant.END_TAG + elementName + XmlConstant.CLOSE_TAG;
    }

    public static String getStartTag(String elementName, int space) {
        return XmlConstant.START_TAG + elementName + XmlConstant.CLOSE_TAG;
    }

    public static String getStartTag(String elementName, String attributes, int space) {
        return XmlConstant.START_TAG + elementName + attributes + XmlConstant.CLOSE_TAG;
    }

    public static String getStartTagWithNL(String elementName, String attributes, int space) {
        return XmlConstant.START_TAG + elementName + attributes + XmlConstant.CLOSE_TAG;
    }

    public static String createEmptyElement(String elementName, int space) {
        return String.valueOf(getSpace(space)) + XmlConstant.START_TAG + elementName + XmlConstant.EMPTY_TAG;
    }

    public static String createEmptyElementWithNL(String elementName, int space) {
        return XmlConstant.START_TAG + elementName + XmlConstant.EMPTY_TAG;
    }

    public static String createEmptyElementWithNL(String elementName, String attributes, int space) {
        return XmlConstant.START_TAG + elementName + attributes + XmlConstant.EMPTY_TAG;
    }

    public static String getSpace(int space) {
        String spc = "";
        for (int i = 0; i < space; i++) {
            spc = String.valueOf(spc) + XmlConstant.SPACE;
        }
        return spc;
    }

    public static String getEncoding(String encoding) {
        if (encoding == null) {
            return XmlConstant.DEFAULT_ENCODING;
        }
        return encoding;
    }
}
