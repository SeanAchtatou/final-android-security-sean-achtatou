package com.wqmobile.sdk.pojoxml.core.processor.xmltopojo.dom;

import com.wqmobile.sdk.pojoxml.core.PojoXmlInitInfo;
import com.wqmobile.sdk.pojoxml.util.MapUtils;
import com.wqmobile.sdk.pojoxml.util.StringUtil;
import java.util.List;
import org.w3c.dom.Node;

abstract class ObjectProcessor {
    private PojoXmlInitInfo initInfo;
    Node rootNode;

    public ObjectProcessor(PojoXmlInitInfo initInfo2) {
        this.initInfo = initInfo2;
        this.rootNode = initInfo2.getRootNode();
    }

    /* access modifiers changed from: protected */
    public String getActualValueFromAleasName(String tagName) {
        if (this.initInfo.getFieldAliasMap() == null) {
            return tagName;
        }
        List matchedValueList = MapUtils.getKeysFromValue(this.initInfo.getFieldAliasMap(), tagName);
        if (matchedValueList == null || matchedValueList.size() != 1) {
            return tagName;
        }
        String actualTag = matchedValueList.iterator().next().toString();
        return actualTag.substring(actualTag.lastIndexOf(46) + 1);
    }

    public String getElementName(Node child) {
        return StringUtil.initCap(getActualValueFromAleasName(child.getNodeName()));
    }

    public void setInitInfo(PojoXmlInitInfo initInfo2) {
        this.initInfo = initInfo2;
    }

    public PojoXmlInitInfo getInitInfo() {
        return this.initInfo;
    }

    public void setRootNode(Node rootNode2) {
        this.rootNode = rootNode2;
    }

    public Node getRootNode() {
        return this.rootNode;
    }
}
