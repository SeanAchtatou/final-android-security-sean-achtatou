package com.wqmobile.sdk.pojoxml.util;

public class Assert {
    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void checkIndex(int index, String message) {
        if (index < 0) {
            throw new IndexOutOfBoundsException(message);
        }
    }
}
