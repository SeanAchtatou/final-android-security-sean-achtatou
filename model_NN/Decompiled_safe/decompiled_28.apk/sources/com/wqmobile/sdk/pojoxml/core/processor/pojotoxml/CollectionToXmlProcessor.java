package com.wqmobile.sdk.pojoxml.core.processor.pojotoxml;

import com.wqmobile.sdk.pojoxml.core.PojoXmlException;
import com.wqmobile.sdk.pojoxml.core.PojoXmlInitInfo;
import com.wqmobile.sdk.pojoxml.util.ClassUtil;
import com.wqmobile.sdk.pojoxml.util.StringUtil;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import java.util.Collection;
import java.util.Iterator;

class CollectionToXmlProcessor extends XmlProcessor {
    private Class collectionObjectClass;
    private boolean collectionObjectFound = false;

    public CollectionToXmlProcessor(int space) {
        setSpace(space);
    }

    public CollectionToXmlProcessor(String elementName, int space, PojoXmlInitInfo pojoXmlInit) {
        super(elementName, space, pojoXmlInit);
    }

    public String process(Object collectionObject) {
        if (isEmptyElement(collectionObject, getElementName())) {
            return createEmptyElementWithNL(getElementName(), getAttributes(), getSpace());
        }
        processCollectionObject(collectionObject);
        return getXmlContent();
    }

    public void processCollectionObject(Object collectionObject) {
        collectionObjectInterospect(collectionObject);
        Iterator itr = ClassUtil.getIterator(collectionObject);
        int index = 0;
        int length = ((Collection) collectionObject).size();
        while (itr.hasNext()) {
            Object listOrSet = itr.next();
            if (listOrSet == null) {
                processNullObjectInCollection(index, length, getElementName(), listOrSet);
                index++;
            } else {
                if (ClassUtil.isPrimitiveOrWrapper(listOrSet.getClass())) {
                    processCollectionPrimitive(index, length, getElementName(), listOrSet);
                } else if (ClassUtil.isCollection(listOrSet)) {
                    throw new PojoXmlException("Inner Collection object is not supported.");
                } else {
                    if (index == 0) {
                        writeXmlContent(getStartTag(getElementName(), getAttributes(), getSpace()));
                        incrementSpace();
                    }
                    processObject(listOrSet);
                    writeXmlContent(XmlConstant.NL);
                    if (index == length - 1) {
                        decrementSpace();
                        writeXmlContent(getCloseTagWithNL(getElementName(), getSpace()));
                    }
                }
                index++;
            }
        }
    }

    public void collectionObjectInterospect(Object collectionObject) {
        Iterator itr = ClassUtil.getIterator(collectionObject);
        while (itr.hasNext()) {
            Object listOrSet = itr.next();
            if (listOrSet != null && !ClassUtil.isPrimitiveOrWrapper(listOrSet.getClass())) {
                if (ClassUtil.isCollection(listOrSet)) {
                    throw new PojoXmlException("Inner Collection object is not supported.");
                }
                setCollectionObjectClass(listOrSet.getClass());
                setCollectionObjectFound(true);
            }
        }
    }

    private void processCollectionPrimitive(int startIndex, int length, String elementName, Object object) {
        writeXmlContent(getStartTag(elementName, getAttributes(), getSpace()));
        writeXmlContent(StringUtil.getActualValue(object, isCdataEnabled()));
        writeXmlContent(getCloseTagWithNL(getElementName(), 0));
    }

    private void processNullObjectInCollection(int index, int length, String elementName, Object listOrSet) {
        if (listOrSet != null) {
            return;
        }
        if (isCollectionObjectFound()) {
            if (index == 0) {
                writeXmlContent(getStartTag(getElementName(), getAttributes(), getSpace()));
                incrementSpace();
            }
            writeXmlContent(createEmptyElementWithNL(ClassUtil.getClassName(getCollectionObjectClass()), getAttributes(), getSpace()));
            if (index == length - 1) {
                decrementSpace();
                writeXmlContent(getCloseTagWithNL(getElementName(), getSpace()));
                return;
            }
            return;
        }
        writeXmlContent(createEmptyElementWithNL(elementName, getAttributes(), getSpace()));
    }

    public void setCollectionObjectFound(boolean collectionObjectFound2) {
        this.collectionObjectFound = collectionObjectFound2;
    }

    public boolean isCollectionObjectFound() {
        return this.collectionObjectFound;
    }

    public void setCollectionObjectClass(Class collectionObjectClass2) {
        this.collectionObjectClass = collectionObjectClass2;
    }

    public Class getCollectionObjectClass() {
        return this.collectionObjectClass;
    }
}
