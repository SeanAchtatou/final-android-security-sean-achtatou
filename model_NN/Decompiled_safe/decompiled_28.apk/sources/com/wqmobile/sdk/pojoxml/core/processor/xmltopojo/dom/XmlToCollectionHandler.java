package com.wqmobile.sdk.pojoxml.core.processor.xmltopojo.dom;

import com.wqmobile.sdk.pojoxml.core.PojoXmlException;
import com.wqmobile.sdk.pojoxml.core.PojoXmlInitInfo;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlToCollectionHandler {
    private String currentNodeName;
    private Object currentNodeValue;
    private String currentSetterName;
    PojoXmlInitInfo initInfo;
    private List objects = new ArrayList();

    public XmlToCollectionHandler(PojoXmlInitInfo initInfo2, String setterName) {
        this.initInfo = initInfo2;
        this.currentSetterName = setterName;
    }

    public Object processCollection() {
        readChildren(this.initInfo.getRootNode());
        if (this.objects.size() > 0) {
            return this.objects;
        }
        return getCurrentNodeValue();
    }

    private void readChildren(Node node) {
        if (node.hasChildNodes()) {
            setCurrentNodeName(node.getNodeName());
            NodeList childNodes = node.getChildNodes();
            for (int i = 0; i < childNodes.getLength(); i++) {
                Node child = childNodes.item(i);
                if (child.getNodeType() == 1) {
                    Class clas = this.initInfo.getCollectionClass(child.getNodeName());
                    if (clas == null) {
                        String fieldName = child.getNodeName();
                        throw new PojoXmlException("Class for " + fieldName + " is not specified## Specify it using PojoXmlObj.addCollectionClass(\"" + fieldName + "\"," + fieldName + ".class) method");
                    } else if (child.getChildNodes().getLength() == 0) {
                        this.objects.add(null);
                    } else {
                        this.initInfo.setRootNode(child);
                        this.objects.add(new XmlToObjectProcessor(clas, this.initInfo).getObject());
                    }
                } else if (child.getNodeType() == 3) {
                    setCurrentNodeValue(child.getNodeValue());
                }
            }
        }
    }

    public void setCurrentNodeName(String currentNodeName2) {
        this.currentNodeName = currentNodeName2;
    }

    public String getCurrentNodeName() {
        return this.currentNodeName;
    }

    public void setCurrentNodeValue(Object currentNodeValue2) {
        this.currentNodeValue = currentNodeValue2;
    }

    public Object getCurrentNodeValue() {
        return this.currentNodeValue;
    }

    public void setCurrentSetterName(String currentSetterName2) {
        this.currentSetterName = currentSetterName2;
    }

    public String getCurrentSetterName() {
        return this.currentSetterName;
    }
}
