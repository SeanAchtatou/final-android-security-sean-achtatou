package com.adchina.android.test;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int adchina = 2130837504;
        public static final int adchina_close = 2130837505;
        public static final int fswallpaper = 2130837506;
        public static final int test_ad = 2130837507;
    }

    public static final class id {
        public static final int adsapceidET = 2131034115;
        public static final int adview = 2131034117;
        public static final int closeBtn = 2131034119;
        public static final int frame = 2131034112;
        public static final int fsadview = 2131034118;
        public static final int mainLayout = 2131034113;
        public static final int startBtn = 2131034116;
        public static final int textview = 2131034114;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
        public static final int main1 = 2130968579;
        public static final int main2 = 2130968578;
    }
}
