package com.adchina.android.test;

import android.app.Activity;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;
import com.adchina.android.ads.AdListener;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.AdView;
import com.adchina.android.ads.FullScreenAdView;
import com.ruman.Jlawyer.R;

public class AdChinaTest extends Activity implements AdListener {
    private boolean hasFullScreenAd = true;
    /* access modifiers changed from: private */
    public AdView mAdView;
    /* access modifiers changed from: private */
    public EditText mAdspaceidET;
    /* access modifiers changed from: private */
    public ImageButton mCloseButton;
    /* access modifiers changed from: private */
    public FrameLayout mFrameLayout;
    /* access modifiers changed from: private */
    public FullScreenAdView mFullScreenAdView;
    private Button mStartBtn;

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        String packageName = getPackageName();
        String string = getResources().getString(R.string.app_name);
        setContentView((int) R.layout.main);
        this.mAdspaceidET = (EditText) findViewById(R.id.tab_minshi_textViewBiaoDiJinE);
        this.mStartBtn = (Button) findViewById(R.id.tab_minshi_editTextBiaoDiJinE);
        this.mAdView = (AdView) findViewById(R.id.tab_minshi_textView3);
        this.mAdView.setAdListener(this);
        Display display = getWindowManager().getDefaultDisplay();
        AdManager.setResolution(String.valueOf(display.getWidth()) + "x" + display.getHeight());
        AdManager.setDebugMode(true);
        AdManager.setAdspaceId(AdManager.DEFAULT_ADSPACE_ID);
        if (this.hasFullScreenAd) {
            createFullScreenAdView(AdManager.DEFAULT_FULLSCREEN_ADSPACE_ID);
            this.mFullScreenAdView.start();
        } else {
            this.mAdView.start();
        }
        this.mStartBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AdManager.setAdspaceId(AdChinaTest.this.mAdspaceidET.getText().toString());
                AdChinaTest.this.mAdView.start();
            }
        });
    }

    private void createFullScreenAdView(String fsAdSpaceId) {
        AdManager.setFullScreenAdspaceId(fsAdSpaceId);
        this.mFrameLayout = (FrameLayout) findViewById(R.id.tab_minshi_id);
        this.mFullScreenAdView = (FullScreenAdView) findViewById(R.id.tab_minshi_editTextShouLiFei);
        this.mFullScreenAdView.setImageResource(R.drawable.calculator);
        this.mAdView.setFullScreenAd(this.mFullScreenAdView);
        this.mCloseButton = (ImageButton) findViewById(R.id.tab_minshi_textView4);
        this.mCloseButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                AdChinaTest.this.mFrameLayout.removeView(AdChinaTest.this.mFullScreenAdView);
                AdChinaTest.this.mFrameLayout.removeView(AdChinaTest.this.mCloseButton);
                return true;
            }
        });
    }

    public void onFailedToReceiveAd(AdView view) {
        displayText("onFailedToRecvAd");
    }

    public void onReceiveAd(AdView view) {
        displayText("onRecvAd");
    }

    public void onRefreshAd(AdView view) {
        displayText("onRefreshAd");
    }

    public void onFailedToRefreshAd(AdView view) {
        displayText("onFailedToRefreshAd");
    }

    private void displayText(String str) {
        Toast.makeText(this, str, 0).show();
    }

    public boolean OnRecvSms(AdView view, String promptText) {
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        System.exit(0);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public void onReceiveFullScreenAd(AdView view) {
        displayText("onRecvFullScreenAd");
    }

    public void onFailedToReceiveFullScreenAd(AdView view) {
        displayText("onFailedToReceiveFullScreenAd");
    }
}
