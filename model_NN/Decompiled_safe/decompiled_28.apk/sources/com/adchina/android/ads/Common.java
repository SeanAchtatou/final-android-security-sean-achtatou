package com.adchina.android.ads;

public final class Common {
    protected static final String KAdSvrUrlFormat = "http://amob.acs86.com/a.htm?pv=1&sp=%s,0,0,0,0,1,1,10&ec=utf-8&cb=%d&fc=%s&ts=1262152690.23&stt=4&mpid=100&muid=%s";
    protected static final String KBTNCLK = "200";
    protected static final String KCLK = "1";
    protected static final String KCookieFilename = "adchinaCookie.ce";
    protected static final String KEnc = "utf-8";
    protected static final String KFSCLK = "204";
    protected static final String KFSIMP = "203";
    protected static final String KFcFilename = "adchinaFC.fc";
    protected static final int KGifInterval = 2000;
    protected static final String KIMP = "2";
    protected static final String KLogTag = "AdChinaError";
    protected static final String KMaParamsFormat = "&ma=%s,%s,%s,%s,%s,%s,%s,%s";
    protected static final int KMsgDisplayAd = 1;
    protected static final int KMsgDisplayFullScreenAd = 4;
    protected static final int KMsgFailedToReceiveAd = 2;
    protected static final int KMsgFailedToReceiveFullScreenAd = 5;
    protected static final int KMsgRefreshAd = 3;
    protected static final String KSplitTag = "|||";
    protected static final int KThdIdle = 50;
    protected static final int KTimerDelay = 3000;
    protected static final int KTimerInterval = 15000;

    public enum EAdModel {
        EAdTXT,
        EAdJPG,
        EAdPNG,
        EAdGIF,
        EAdNONE
    }
}
