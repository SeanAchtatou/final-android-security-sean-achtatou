package com.adchina.android.ads;

public interface AdListener {
    boolean OnRecvSms(AdView adView, String str);

    void onFailedToReceiveAd(AdView adView);

    void onFailedToReceiveFullScreenAd(AdView adView);

    void onFailedToRefreshAd(AdView adView);

    void onReceiveAd(AdView adView);

    void onReceiveFullScreenAd(AdView adView);

    void onRefreshAd(AdView adView);
}
