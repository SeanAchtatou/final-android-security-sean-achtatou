package com.adchina.android.ads;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MotionEvent;
import cn.domob.android.ads.DomobAdManager;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.Common;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import org.xmlpull.v1.XmlPullParserException;

public class AdEngine implements Handler.Callback {
    private StringBuffer mAdClickUrl = new StringBuffer();
    private AdListener mAdListener = null;
    private Common.EAdModel mAdModel = Common.EAdModel.EAdNONE;
    private StringBuffer mAdType = new StringBuffer();
    private AdView mAdView;
    private StringBuffer mAdspaceId = new StringBuffer();
    private StringBuffer mBtnCallNumber = new StringBuffer();
    private StringBuffer mBtnDownloadUrl = new StringBuffer();
    private StringBuffer mBtnH = new StringBuffer();
    private StringBuffer mBtnSmsContent = new StringBuffer();
    private StringBuffer mBtnSmsNumber = new StringBuffer();
    private StringBuffer mBtnThdClkTrack = new StringBuffer();
    private LinkedList<String> mBtnThdClkTrackList = new LinkedList<>();
    private StringBuffer mBtnType = new StringBuffer();
    private StringBuffer mBtnW = new StringBuffer();
    private StringBuffer mBtnX = new StringBuffer();
    private StringBuffer mBtnY = new StringBuffer();
    /* access modifiers changed from: private */
    public EClkTrack mClickStatus = EClkTrack.EIdle;
    private ContentView mContentView;
    private Context mContext;
    private boolean mDebugMode = false;
    private StringBuffer mFC = new StringBuffer();
    private StringBuffer mFullScreenClkUrl = new StringBuffer();
    private StringBuffer mFullScreenImgAddr = new StringBuffer();
    private boolean mFullScreenStart = false;
    private StringBuffer mFullScreenThdClkTrack = new StringBuffer();
    private LinkedList<String> mFullScreenThdClkTrackList = new LinkedList<>();
    private StringBuffer mFullScreenThdImpTrack = new StringBuffer();
    private LinkedList<String> mFullScreenThdImpTrackList = new LinkedList<>();
    private FullScreenAdView mFullScreenView;
    private GifEngine mGifEngine = null;
    private boolean mHasAd = false;
    private HttpEngine mHttpEngine = null;
    private StringBuffer mIMEI = new StringBuffer();
    private StringBuffer mImgAddr = new StringBuffer();
    private Handler mMsgHandler;
    private StringBuffer mPhoneModel = new StringBuffer();
    private StringBuffer mRefTime = new StringBuffer();
    private boolean mRefreshAd = false;
    private StringBuffer mReportBaseUrl = new StringBuffer();
    /* access modifiers changed from: private */
    public ERecvAdStatus mRunState = ERecvAdStatus.EReceiveAd;
    private StringBuffer mSmsAddr = new StringBuffer();
    private StringBuffer mSmsContent = new StringBuffer();
    private StringBuffer mSmsType = new StringBuffer();
    private boolean mStart = false;
    /* access modifiers changed from: private */
    public boolean mStop = false;
    private StringBuffer mThdClkTrack = new StringBuffer();
    private LinkedList<String> mThdClkTrackList = new LinkedList<>();
    private StringBuffer mThdImpTrack = new StringBuffer();
    private LinkedList<String> mThdImpTrackList = new LinkedList<>();
    private StringBuffer mTxtLnkText = new StringBuffer();
    private StringBuffer mXmlVer = new StringBuffer();

    public enum EClkTrack {
        ESendClkTrack,
        ESendThdClkTrack,
        ESendBtnClkTrack,
        ESendBtnThdClkTrack,
        ESendFullScreenClkTrack,
        ESendFullScreenThdClkTrack,
        EIdle
    }

    public enum ERecvAdStatus {
        EReceiveAd,
        EGetFullScreenImgMaterial,
        EGetImgMaterial,
        ESendImpTrack,
        ESendThdImpTrack,
        ERefreshAd,
        ESendFullScreenImpTrack,
        ESendFullScreenThdImpTrack,
        EIdle
    }

    public AdEngine(Context context, AdView view, ContentView contentView) {
        this.mContext = context;
        this.mAdView = view;
        this.mContentView = contentView;
        initView(context);
    }

    public void setFullScreenAdView(FullScreenAdView fsAdView) {
        this.mFullScreenView = fsAdView;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        AdLog.uninitLog();
        AdLog.uninitDebugLog();
    }

    private void initView(Context context) {
        this.mHttpEngine = new HttpEngine(context);
        this.mMsgHandler = new Handler(this);
    }

    public void setAdListener(AdListener adListener) {
        this.mAdListener = adListener;
    }

    public void setPenColor(int aColor) {
        if (this.mContentView != null) {
            this.mContentView.setPenColor(aColor);
        }
    }

    public void setBackgroundColor(int aColor) {
        if (this.mContentView != null) {
            this.mContentView.setBackgroundColor(aColor);
        }
    }

    public void setDefaultImage(Bitmap aDefImage) {
        if (this.mContentView != null) {
            this.mContentView.setDefaultImage(aDefImage);
        }
    }

    public void setDefaultUrl(String aUrl) {
        this.mAdClickUrl.setLength(0);
        this.mAdClickUrl.append(aUrl);
        this.mSmsType.setLength(0);
        this.mSmsType.append("2");
    }

    public void setFont(Typeface aFont) {
        if (this.mContentView != null) {
            this.mContentView.setFont(aFont);
        }
    }

    public void startFullScreenAd() {
        if (!this.mFullScreenStart) {
            setAdSpaceIdAsFullScreen(true);
            String imei = ((TelephonyManager) this.mContext.getSystemService("phone")).getDeviceId();
            if (imei != null) {
                this.mIMEI.append(imei);
            }
            this.mPhoneModel.append(Build.MODEL.replaceAll(XmlConstant.SINGLE_SPACE, "_"));
            AdLog.initLog("AdChina_" + ((Object) this.mAdspaceId) + ".txt");
            AdLog.initDebugLog("AdChinaOutput_" + ((Object) this.mAdspaceId) + ".txt");
            new RecvAdThread().start();
            new ClkTrackThread().start();
            this.mFullScreenStart = true;
        }
    }

    /* access modifiers changed from: private */
    public void setAdSpaceIdAsFullScreen(boolean isFullScreenAd) {
        this.mAdspaceId.setLength(0);
        if (isFullScreenAd) {
            this.mAdspaceId.append(AdManager.getFullScreenAdspaceId());
        } else {
            this.mAdspaceId.append(AdManager.getAdspaceId());
        }
    }

    public void start() {
        if (!this.mStart) {
            setAdSpaceIdAsFullScreen(false);
            String imei = ((TelephonyManager) this.mContext.getSystemService("phone")).getDeviceId();
            if (imei != null) {
                this.mIMEI.append(imei);
            }
            this.mPhoneModel.append(Build.MODEL.replaceAll(XmlConstant.SINGLE_SPACE, "_"));
            AdLog.initLog("AdChina_" + ((Object) this.mAdspaceId) + ".txt");
            AdLog.initDebugLog("AdChinaOutput_" + ((Object) this.mAdspaceId) + ".txt");
            new RecvAdThread().start();
            new ClkTrackThread().start();
            this.mStart = true;
        }
    }

    public void stop() {
        this.mStop = true;
    }

    public boolean hasAd() {
        return this.mHasAd;
    }

    private void setRecvAdStatus(ERecvAdStatus status) {
        this.mRunState = status;
    }

    private void setClkTrackStatus(EClkTrack status) {
        this.mClickStatus = status;
    }

    private void sendMessage(int what, Object obj) {
        Message msg = new Message();
        msg.what = what;
        msg.obj = obj;
        this.mMsgHandler.sendMessage(msg);
    }

    private void resetAdserverInfo() {
        this.mXmlVer.setLength(0);
        this.mAdType.setLength(0);
        this.mImgAddr.setLength(0);
        this.mAdClickUrl.setLength(0);
        this.mReportBaseUrl.setLength(0);
        this.mThdImpTrack.setLength(0);
        this.mThdClkTrack.setLength(0);
        this.mFC.setLength(0);
        this.mRefTime.setLength(0);
        this.mTxtLnkText.setLength(0);
        this.mSmsType.setLength(0);
        this.mThdClkTrackList.clear();
        this.mThdImpTrackList.clear();
        this.mBtnThdClkTrackList.clear();
        this.mBtnType.setLength(0);
        this.mBtnX.setLength(0);
        this.mBtnY.setLength(0);
        this.mBtnW.setLength(0);
        this.mBtnH.setLength(0);
        this.mBtnThdClkTrack.setLength(0);
        this.mBtnDownloadUrl.setLength(0);
        this.mBtnSmsNumber.setLength(0);
        this.mBtnSmsContent.setLength(0);
        this.mBtnCallNumber.setLength(0);
        this.mFullScreenThdImpTrackList.clear();
        this.mFullScreenThdClkTrackList.clear();
        this.mFullScreenImgAddr.setLength(0);
        this.mFullScreenClkUrl.setLength(0);
        this.mFullScreenThdImpTrack.setLength(0);
        this.mFullScreenThdClkTrack.setLength(0);
    }

    private void readAdserverInfo(InputStream is) throws XmlPullParserException, IOException {
        AdLog.writeLog("++ readAdserverInfo");
        XmlEngine xmlEngine = new XmlEngine();
        xmlEngine.parseXmlStream(is);
        xmlEngine.readXmlInfo(this.mXmlVer, this.mAdType, this.mAdClickUrl, this.mReportBaseUrl, this.mThdImpTrack, this.mThdClkTrack, this.mFC, this.mRefTime, this.mSmsAddr, this.mSmsContent, this.mSmsType, this.mImgAddr, this.mTxtLnkText, this.mBtnType, this.mBtnX, this.mBtnY, this.mBtnW, this.mBtnH, this.mBtnThdClkTrack, this.mBtnDownloadUrl, this.mBtnSmsNumber, this.mBtnSmsContent, this.mBtnCallNumber, this.mFullScreenImgAddr, this.mFullScreenClkUrl, this.mFullScreenThdImpTrack, this.mFullScreenThdClkTrack);
        if (this.mAdType.toString().compareToIgnoreCase("txt") == 0) {
            this.mAdModel = Common.EAdModel.EAdTXT;
        } else if (this.mAdType.toString().compareToIgnoreCase("jpg") == 0) {
            this.mAdModel = Common.EAdModel.EAdJPG;
        } else if (this.mAdType.toString().compareToIgnoreCase("png") == 0) {
            this.mAdModel = Common.EAdModel.EAdPNG;
        } else if (this.mAdType.toString().compareToIgnoreCase("gif") == 0) {
            this.mAdModel = Common.EAdModel.EAdGIF;
        } else {
            this.mAdModel = Common.EAdModel.EAdNONE;
        }
        writeFcParams(this.mFC.toString());
        AdLog.writeLog(((Object) this.mAdType) + " : ");
        AdLog.writeLog("ImpUrl:" + this.mThdImpTrack.toString());
        AdLog.writeLog("ClkUrl:" + this.mAdClickUrl.toString());
        AdLog.writeLog("FC:" + this.mFC.toString());
        AdLog.writeLog("FullScreenImgAddr:" + this.mFullScreenImgAddr.toString());
        if (!(Common.EAdModel.EAdTXT == this.mAdModel || Common.EAdModel.EAdNONE == this.mAdModel)) {
            AdLog.writeLog("ImgAddr:" + this.mImgAddr.toString());
        }
        AdLog.writeLog("-- readAdserverInfo");
    }

    private String setupMaParams() {
        try {
            StringBuilder sb = new StringBuilder(String.valueOf(""));
            Object[] objArr = new Object[8];
            objArr[0] = "2.1.1";
            objArr[1] = "Android_" + Build.VERSION.RELEASE;
            objArr[2] = "";
            objArr[3] = this.mPhoneModel.toString();
            objArr[4] = AdManager.getResolution();
            objArr[5] = AdManager.getGender() == AdManager.EGender.EFemale ? "2" : "1";
            objArr[6] = AdManager.getPostalCode();
            objArr[7] = AdManager.getBirthday();
            return sb.append(String.format("&ma=%s,%s,%s,%s,%s,%s,%s,%s", objArr)).toString();
        } catch (Exception e) {
            String err = "Failed to getMaParams, err = " + e.toString();
            AdLog.writeLog(err);
            Log.e("AdChinaError", err);
            return "";
        }
    }

    /* JADX INFO: Multiple debug info for r2v2 java.io.IOException: [D('e' java.lang.Exception), D('e' java.io.IOException)] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0052 A[SYNTHETIC, Splitter:B:16:0x0052] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0057 A[Catch:{ IOException -> 0x005b }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0077 A[SYNTHETIC, Splitter:B:24:0x0077] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x007c A[Catch:{ IOException -> 0x0080 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String readFcParams() {
        /*
            r12 = this;
            java.lang.String r8 = "adchinaFC.fc"
            java.lang.String r11 = "Exceptions in getFcParams 2, err = "
            r3 = 0
            r6 = 0
            r0 = 0
            android.content.Context r8 = r12.mContext     // Catch:{ Exception -> 0x0038 }
            java.lang.String r9 = "adchinaFC.fc"
            java.io.File r4 = r8.getFileStreamPath(r9)     // Catch:{ Exception -> 0x0038 }
            long r8 = r4.length()     // Catch:{ Exception -> 0x0038 }
            int r8 = (int) r8     // Catch:{ Exception -> 0x0038 }
            char[] r5 = new char[r8]     // Catch:{ Exception -> 0x0038 }
            android.content.Context r8 = r12.mContext     // Catch:{ Exception -> 0x0038 }
            java.lang.String r9 = "adchinaFC.fc"
            java.io.FileInputStream r3 = r8.openFileInput(r9)     // Catch:{ Exception -> 0x0038 }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0038 }
            r7.<init>(r3)     // Catch:{ Exception -> 0x0038 }
            r7.read(r5)     // Catch:{ Exception -> 0x00b7, all -> 0x00b4 }
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x00b7, all -> 0x00b4 }
            r1.<init>(r5)     // Catch:{ Exception -> 0x00b7, all -> 0x00b4 }
            if (r7 == 0) goto L_0x0030
            r7.close()     // Catch:{ IOException -> 0x0099 }
        L_0x0030:
            if (r3 == 0) goto L_0x00b1
            r3.close()     // Catch:{ IOException -> 0x0099 }
            r0 = r1
            r6 = r7
        L_0x0037:
            return r0
        L_0x0038:
            r8 = move-exception
            r2 = r8
        L_0x003a:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0074 }
            java.lang.String r9 = "Exceptions in getFcParams 1, err = "
            r8.<init>(r9)     // Catch:{ all -> 0x0074 }
            java.lang.String r9 = r2.toString()     // Catch:{ all -> 0x0074 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0074 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0074 }
            com.adchina.android.ads.AdLog.writeLog(r8)     // Catch:{ all -> 0x0074 }
            if (r6 == 0) goto L_0x0055
            r6.close()     // Catch:{ IOException -> 0x005b }
        L_0x0055:
            if (r3 == 0) goto L_0x0037
            r3.close()     // Catch:{ IOException -> 0x005b }
            goto L_0x0037
        L_0x005b:
            r8 = move-exception
            r2 = r8
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = "Exceptions in getFcParams 2, err = "
            r8.<init>(r11)
            java.lang.String r9 = r2.toString()
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r8 = r8.toString()
            com.adchina.android.ads.AdLog.writeLog(r8)
            goto L_0x0037
        L_0x0074:
            r8 = move-exception
        L_0x0075:
            if (r6 == 0) goto L_0x007a
            r6.close()     // Catch:{ IOException -> 0x0080 }
        L_0x007a:
            if (r3 == 0) goto L_0x007f
            r3.close()     // Catch:{ IOException -> 0x0080 }
        L_0x007f:
            throw r8
        L_0x0080:
            r9 = move-exception
            r2 = r9
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "Exceptions in getFcParams 2, err = "
            r9.<init>(r11)
            java.lang.String r10 = r2.toString()
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r9 = r9.toString()
            com.adchina.android.ads.AdLog.writeLog(r9)
            goto L_0x007f
        L_0x0099:
            r8 = move-exception
            r2 = r8
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = "Exceptions in getFcParams 2, err = "
            r8.<init>(r11)
            java.lang.String r9 = r2.toString()
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r8 = r8.toString()
            com.adchina.android.ads.AdLog.writeLog(r8)
        L_0x00b1:
            r0 = r1
            r6 = r7
            goto L_0x0037
        L_0x00b4:
            r8 = move-exception
            r6 = r7
            goto L_0x0075
        L_0x00b7:
            r8 = move-exception
            r2 = r8
            r6 = r7
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.AdEngine.readFcParams():java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x004e A[SYNTHETIC, Splitter:B:16:0x004e] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0053 A[Catch:{ IOException -> 0x0057 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x005c A[SYNTHETIC, Splitter:B:23:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0061 A[Catch:{ IOException -> 0x0068 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void writeFcParams(java.lang.String r8) {
        /*
            r7 = this;
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "saveFcParams : "
            r4.<init>(r5)
            java.lang.StringBuilder r4 = r4.append(r8)
            java.lang.String r4 = r4.toString()
            com.adchina.android.ads.AdLog.writeLog(r4)
            r1 = 0
            r2 = 0
            android.content.Context r4 = r7.mContext     // Catch:{ Exception -> 0x0034 }
            java.lang.String r5 = "adchinaFC.fc"
            r6 = 0
            java.io.FileOutputStream r1 = r4.openFileOutput(r5, r6)     // Catch:{ Exception -> 0x0034 }
            java.io.OutputStreamWriter r3 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x0034 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0034 }
            r3.write(r8)     // Catch:{ Exception -> 0x006d, all -> 0x006a }
            r3.flush()     // Catch:{ Exception -> 0x006d, all -> 0x006a }
            if (r3 == 0) goto L_0x002d
            r3.close()     // Catch:{ IOException -> 0x0065 }
        L_0x002d:
            if (r1 == 0) goto L_0x0071
            r1.close()     // Catch:{ IOException -> 0x0065 }
            r2 = r3
        L_0x0033:
            return
        L_0x0034:
            r4 = move-exception
            r0 = r4
        L_0x0036:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0059 }
            java.lang.String r5 = "Exceptions in saveFcParams, err = "
            r4.<init>(r5)     // Catch:{ all -> 0x0059 }
            java.lang.String r5 = r0.toString()     // Catch:{ all -> 0x0059 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0059 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0059 }
            com.adchina.android.ads.AdLog.writeLog(r4)     // Catch:{ all -> 0x0059 }
            if (r2 == 0) goto L_0x0051
            r2.close()     // Catch:{ IOException -> 0x0057 }
        L_0x0051:
            if (r1 == 0) goto L_0x0033
            r1.close()     // Catch:{ IOException -> 0x0057 }
            goto L_0x0033
        L_0x0057:
            r4 = move-exception
            goto L_0x0033
        L_0x0059:
            r4 = move-exception
        L_0x005a:
            if (r2 == 0) goto L_0x005f
            r2.close()     // Catch:{ IOException -> 0x0068 }
        L_0x005f:
            if (r1 == 0) goto L_0x0064
            r1.close()     // Catch:{ IOException -> 0x0068 }
        L_0x0064:
            throw r4
        L_0x0065:
            r4 = move-exception
            r2 = r3
            goto L_0x0033
        L_0x0068:
            r5 = move-exception
            goto L_0x0064
        L_0x006a:
            r4 = move-exception
            r2 = r3
            goto L_0x005a
        L_0x006d:
            r4 = move-exception
            r0 = r4
            r2 = r3
            goto L_0x0036
        L_0x0071:
            r2 = r3
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.AdEngine.writeFcParams(java.lang.String):void");
    }

    private String setupAdserverUrl() {
        String fcParams = readFcParams();
        if (!(fcParams == null || -1 == fcParams.indexOf(XmlConstant.SINGLE_SPACE))) {
            fcParams = "";
        }
        AdLog.writeLog("FC:" + fcParams);
        setAdSpaceIdAsFullScreen(this.mAdspaceId.toString().equals(AdManager.getFullScreenAdspaceId()));
        return String.valueOf(String.format("http://amob.acs86.com/a.htm?pv=1&sp=%s,0,0,0,0,1,1,10&ec=utf-8&cb=%d&fc=%s&ts=1262152690.23&stt=4&mpid=100&muid=%s", this.mAdspaceId, Integer.valueOf(Utils.GetRandomNumber()), fcParams, this.mIMEI.toString())) + setupMaParams();
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0034 A[SYNTHETIC, Splitter:B:15:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0039 A[Catch:{ Exception -> 0x003d }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0042 A[SYNTHETIC, Splitter:B:22:0x0042] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0047 A[Catch:{ Exception -> 0x0050 }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String setupAdserverUrlFromConfig() {
        /*
            r10 = this;
            r5 = 0
            r3 = 0
            r1 = 0
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0031, all -> 0x003f }
            java.lang.String r8 = "/sdcard/AdChinaConfig.txt"
            r0.<init>(r8)     // Catch:{ Exception -> 0x0031, all -> 0x003f }
            long r8 = r0.length()     // Catch:{ Exception -> 0x0031, all -> 0x003f }
            int r8 = (int) r8     // Catch:{ Exception -> 0x0031, all -> 0x003f }
            char[] r7 = new char[r8]     // Catch:{ Exception -> 0x0031, all -> 0x003f }
            java.io.FileInputStream r6 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0031, all -> 0x003f }
            r6.<init>(r0)     // Catch:{ Exception -> 0x0031, all -> 0x003f }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0059, all -> 0x0052 }
            r4.<init>(r6)     // Catch:{ Exception -> 0x0059, all -> 0x0052 }
            r4.read(r7)     // Catch:{ Exception -> 0x005c, all -> 0x0055 }
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x005c, all -> 0x0055 }
            r2.<init>(r7)     // Catch:{ Exception -> 0x005c, all -> 0x0055 }
            if (r4 == 0) goto L_0x0028
            r4.close()     // Catch:{ Exception -> 0x004b }
        L_0x0028:
            if (r6 == 0) goto L_0x0060
            r6.close()     // Catch:{ Exception -> 0x004b }
            r1 = r2
            r3 = r4
            r5 = r6
        L_0x0030:
            return r1
        L_0x0031:
            r8 = move-exception
        L_0x0032:
            if (r3 == 0) goto L_0x0037
            r3.close()     // Catch:{ Exception -> 0x003d }
        L_0x0037:
            if (r5 == 0) goto L_0x0030
            r5.close()     // Catch:{ Exception -> 0x003d }
            goto L_0x0030
        L_0x003d:
            r8 = move-exception
            goto L_0x0030
        L_0x003f:
            r8 = move-exception
        L_0x0040:
            if (r3 == 0) goto L_0x0045
            r3.close()     // Catch:{ Exception -> 0x0050 }
        L_0x0045:
            if (r5 == 0) goto L_0x004a
            r5.close()     // Catch:{ Exception -> 0x0050 }
        L_0x004a:
            throw r8
        L_0x004b:
            r8 = move-exception
            r1 = r2
            r3 = r4
            r5 = r6
            goto L_0x0030
        L_0x0050:
            r9 = move-exception
            goto L_0x004a
        L_0x0052:
            r8 = move-exception
            r5 = r6
            goto L_0x0040
        L_0x0055:
            r8 = move-exception
            r3 = r4
            r5 = r6
            goto L_0x0040
        L_0x0059:
            r8 = move-exception
            r5 = r6
            goto L_0x0032
        L_0x005c:
            r8 = move-exception
            r3 = r4
            r5 = r6
            goto L_0x0032
        L_0x0060:
            r1 = r2
            r3 = r4
            r5 = r6
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.AdEngine.setupAdserverUrlFromConfig():java.lang.String");
    }

    /* access modifiers changed from: private */
    public void onReceiveAd() {
        AdLog.writeLog("++ onReceiveAd");
        setRecvAdStatus(ERecvAdStatus.EIdle);
        InputStream inputStream = null;
        try {
            if (this.mDebugMode) {
                String data = setupAdserverUrlFromConfig();
                if (data != null) {
                    inputStream = this.mHttpEngine.requestGet(data, this.mAdspaceId.toString());
                }
            } else {
                String adserverUrl = setupAdserverUrl();
                AdLog.writeLog("AdserverUlr:" + adserverUrl);
                inputStream = this.mHttpEngine.requestGet(adserverUrl, this.mAdspaceId.toString());
            }
            resetAdserverInfo();
            readAdserverInfo(inputStream);
            if (this.mAdspaceId.toString().equals(AdManager.getFullScreenAdspaceId())) {
                setRecvAdStatus(ERecvAdStatus.EGetFullScreenImgMaterial);
            } else if (Common.EAdModel.EAdTXT == this.mAdModel) {
                sendMessage(1, this.mTxtLnkText.toString());
            } else if (Common.EAdModel.EAdJPG == this.mAdModel || Common.EAdModel.EAdPNG == this.mAdModel || Common.EAdModel.EAdGIF == this.mAdModel) {
                setRecvAdStatus(ERecvAdStatus.EGetImgMaterial);
            } else {
                throw new XmlPullParserException("Invalidate mAdModel");
            }
            Utils.splitTrackUrl(this.mThdClkTrack.toString(), this.mThdClkTrackList);
            Utils.splitTrackUrl(this.mThdImpTrack.toString(), this.mThdImpTrackList);
            Utils.splitTrackUrl(this.mBtnThdClkTrack.toString(), this.mBtnThdClkTrackList);
            Utils.splitTrackUrl(this.mFullScreenThdImpTrack.toString(), this.mFullScreenThdImpTrackList);
            Utils.splitTrackUrl(this.mFullScreenThdClkTrack.toString(), this.mFullScreenThdClkTrackList);
        } catch (Exception e) {
            String err = "Exceptions in onReceiveAd, err = " + e.toString();
            if (this.mAdspaceId.equals(AdManager.getFullScreenAdspaceId())) {
                sendMessage(5, err);
            } else {
                sendMessage(2, err);
            }
            AdLog.writeLog(err);
            Log.e("AdChinaError", err);
        } finally {
            this.mHttpEngine.closeStream(inputStream);
            AdLog.writeLog("-- onReceiveAd");
        }
    }

    /* access modifiers changed from: private */
    public void onGetFullScreenImgMaterial() {
        AdLog.writeLog("++ onGetFullScreenImgMaterial");
        setRecvAdStatus(ERecvAdStatus.EIdle);
        InputStream is = null;
        try {
            is = this.mHttpEngine.requestGet(this.mFullScreenImgAddr.toString());
            Bitmap bmp = Utils.convertStreamToBitmap(is);
            if (bmp != null) {
                sendMessage(4, bmp);
            } else {
                sendMessage(5, "Full Screen AdMaterial is null");
            }
        } catch (Exception e) {
            String err = "Failed to get full-screen ad material, err = " + e.toString();
            sendMessage(5, err);
            AdLog.writeLog(err);
            Log.e("AdChinaError", err);
        } finally {
            this.mHttpEngine.closeStream(is);
            AdLog.writeLog("-- onGetFullScreenImgMaterial");
        }
    }

    /* access modifiers changed from: private */
    public void onGetImgMaterial() {
        AdLog.writeLog("++ onGetImgMaterial");
        setRecvAdStatus(ERecvAdStatus.EIdle);
        InputStream is = null;
        try {
            is = this.mHttpEngine.requestGet(this.mImgAddr.toString());
            if (Common.EAdModel.EAdJPG == this.mAdModel) {
                Bitmap bmp = Utils.convertStreamToBitmap(is);
                if (bmp != null) {
                    sendMessage(1, bmp);
                } else {
                    sendMessage(2, "JPG AdMaterial is null");
                }
            } else if (Common.EAdModel.EAdPNG == this.mAdModel) {
                Bitmap bmp2 = Utils.convertStreamToBitmap(is);
                if (bmp2 != null) {
                    sendMessage(1, bmp2);
                } else {
                    sendMessage(2, "PNG AdMaterial is null");
                }
            } else if (Common.EAdModel.EAdGIF == this.mAdModel) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                while (true) {
                    int ch = is.read();
                    if (ch == -1) {
                        break;
                    }
                    stream.write(ch);
                }
                this.mGifEngine = GifEngine.CreateGifImage(stream.toByteArray());
                if (this.mGifEngine != null) {
                    sendMessage(1, this.mGifEngine);
                }
                stream.close();
            }
        } catch (Exception e) {
            String err = "Failed to get ad material, err = " + e.toString();
            sendMessage(2, err);
            AdLog.writeLog(err);
            Log.e("AdChinaError", err);
        } finally {
            this.mHttpEngine.closeStream(is);
            AdLog.writeLog("-- onGetImgMaterial, AdModel = " + this.mAdModel);
        }
    }

    private void onDisplayFullScreenAd(Message msg) {
        AdLog.writeLog("++ onDisplayFullScreenAd");
        this.mFullScreenView.setImageBitmap((Bitmap) msg.obj);
        setRecvAdStatus(ERecvAdStatus.ESendFullScreenImpTrack);
        AdLog.writeLog("-- onDisplayFullScreenAd");
    }

    private void onDisplayAd(Message msg) {
        AdLog.writeLog("++ onDisplayAd, mAdModel = " + this.mAdModel);
        this.mRefreshAd = true;
        if (Common.EAdModel.EAdTXT == this.mAdModel) {
            this.mContentView.setContent(this.mTxtLnkText.toString());
        } else if (Common.EAdModel.EAdJPG == this.mAdModel || Common.EAdModel.EAdPNG == this.mAdModel) {
            this.mContentView.setContent((Bitmap) msg.obj);
        } else if (Common.EAdModel.EAdGIF == this.mAdModel) {
            this.mContentView.setContent((GifEngine) msg.obj);
        }
        this.mHasAd = true;
        setRecvAdStatus(ERecvAdStatus.ESendImpTrack);
        AdLog.writeLog("-- onDisplayAd");
    }

    /* access modifiers changed from: private */
    public void onSendFullScreenImpTrack() {
        AdLog.writeLog("++ onSendFullScreenImpTrack");
        try {
            this.mHttpEngine.requestSend(String.valueOf(this.mReportBaseUrl.toString()) + "203" + ",0,0,0" + setupMaParams());
        } catch (Exception e) {
            String err = "Exceptions in onSendFullScreenImpTrack, err = " + e.toString();
            AdLog.writeLog(err);
            Log.e("AdChinaError", err);
        } finally {
            setRecvAdStatus(ERecvAdStatus.EGetImgMaterial);
            AdLog.writeLog("-- onSendFullScreenImpTrack");
        }
    }

    /* access modifiers changed from: private */
    public void onSendImpTrack() {
        AdLog.writeLog("++ onSendImpTrack");
        try {
            this.mHttpEngine.requestSend(String.valueOf(this.mReportBaseUrl.toString()) + "2" + ",0,0,0" + setupMaParams());
        } catch (Exception e) {
            String err = "Exceptions in onSendImpTrack, err = " + e.toString();
            AdLog.writeLog(err);
            Log.e("AdChinaError", err);
        } finally {
            setRecvAdStatus(ERecvAdStatus.ESendThdImpTrack);
            AdLog.writeLog("-- onSendImpTrack");
        }
    }

    /* access modifiers changed from: private */
    public void onSendFullScreenThdImpTrack() {
        setRecvAdStatus(ERecvAdStatus.EIdle);
        int size = this.mFullScreenThdImpTrackList.size();
        AdLog.writeLog("++ onSendFullScreenThdImpTrack, Size of list is " + size);
        if (size > 0) {
            try {
                this.mThdImpTrackList.removeLast();
                this.mHttpEngine.requestSend(this.mThdImpTrackList.get(size - 1));
            } catch (Exception e) {
                String err = "Failed to onSendFullScreenThdImpTrack, err = " + e.toString();
                AdLog.writeLog(err);
                Log.e("AdChinaError", err);
            } finally {
                setRecvAdStatus(ERecvAdStatus.ESendFullScreenThdImpTrack);
            }
        } else {
            setRecvAdStatus(ERecvAdStatus.EGetImgMaterial);
        }
        AdLog.writeLog("-- onSendFullScreenThdImpTrack");
    }

    /* access modifiers changed from: private */
    public void onSendThdImpTrack() {
        setRecvAdStatus(ERecvAdStatus.EIdle);
        int size = this.mThdImpTrackList.size();
        AdLog.writeLog("++ onSendThdImpTrack, Size of list is " + size);
        if (size > 0) {
            try {
                this.mThdImpTrackList.removeLast();
                this.mHttpEngine.requestSend(this.mThdImpTrackList.get(size - 1));
            } catch (Exception e) {
                String err = "Failed to onSendThdImpTrack, err = " + e.toString();
                AdLog.writeLog(err);
                Log.e("AdChinaError", err);
            } finally {
                setRecvAdStatus(ERecvAdStatus.ESendThdImpTrack);
            }
        } else {
            sendMessage(3, "RefreshAd");
        }
        AdLog.writeLog("-- onSendThdImpTrack");
    }

    /* access modifiers changed from: private */
    public void onSendFullScreenClkTrack() {
        AdLog.writeLog("++ onSendFullScreenClkTrack");
        setClkTrackStatus(EClkTrack.EIdle);
        try {
            this.mHttpEngine.requestSend(String.valueOf(this.mReportBaseUrl.toString()) + "204" + ",0,0,0" + setupMaParams());
        } catch (Exception e) {
            String err = "Exceptions in onSendFullScreenClkTrack, err = " + e.toString();
            AdLog.writeLog(err);
            Log.e("AdChinaError", err);
        } finally {
            setClkTrackStatus(EClkTrack.ESendFullScreenThdClkTrack);
            AdLog.writeLog("-- onSendFullScreenClkTrack");
        }
    }

    /* access modifiers changed from: private */
    public void onSendClkTrack() {
        AdLog.writeLog("++ onSendClkTrack");
        setClkTrackStatus(EClkTrack.EIdle);
        try {
            this.mHttpEngine.requestSend(String.valueOf(this.mReportBaseUrl.toString()) + "1" + ",0,0,0" + setupMaParams());
        } catch (Exception e) {
            String err = "Exceptions in onSendClkTrack, err = " + e.toString();
            AdLog.writeLog(err);
            Log.e("AdChinaError", err);
        } finally {
            setClkTrackStatus(EClkTrack.ESendThdClkTrack);
            AdLog.writeLog("-- onSendClkTrack");
        }
    }

    /* access modifiers changed from: private */
    public void onSendFullScreenThdClkTrack() {
        setClkTrackStatus(EClkTrack.EIdle);
        int size = this.mFullScreenThdClkTrackList.size();
        AdLog.writeLog("++ onSendFullScreenThdClkTrack, Size of list is " + size);
        if (size > 0) {
            try {
                this.mFullScreenThdClkTrackList.removeLast();
                this.mHttpEngine.requestSend(this.mFullScreenThdClkTrackList.get(size - 1));
            } catch (Exception e) {
                String err = "Failed to onSendFullScreenThdClkTrack, err = " + e.toString();
                AdLog.writeLog(err);
                Log.e("AdChinaError", err);
            } finally {
                setClkTrackStatus(EClkTrack.ESendFullScreenThdClkTrack);
                AdLog.writeLog("-- onSendFullScreenThdClkTrack");
            }
        }
    }

    /* access modifiers changed from: private */
    public void onSendThdClkTrack() {
        setClkTrackStatus(EClkTrack.EIdle);
        int size = this.mThdClkTrackList.size();
        AdLog.writeLog("++ onSendThdClkTrack, Size of list is " + size);
        if (size > 0) {
            try {
                this.mThdClkTrackList.removeLast();
                this.mHttpEngine.requestSend(this.mThdClkTrackList.get(size - 1));
            } catch (Exception e) {
                String err = "Failed to onSendThdClkTrack, err = " + e.toString();
                AdLog.writeLog(err);
                Log.e("AdChinaError", err);
            } finally {
                setClkTrackStatus(EClkTrack.ESendThdClkTrack);
                AdLog.writeLog("-- onSendThdClkTrack");
            }
        }
    }

    /* access modifiers changed from: private */
    public void onSendBtnClkTrack() {
        AdLog.writeLog("++ onSendBtnClkTrack");
        setClkTrackStatus(EClkTrack.EIdle);
        try {
            this.mHttpEngine.requestSend(String.valueOf(this.mReportBaseUrl.toString()) + "200" + ",0,0,0" + setupMaParams());
        } catch (Exception e) {
            String err = "Exceptions in onSendBtnClkTrack, err = " + e.toString();
            AdLog.writeLog(err);
            Log.e("AdChinaError", err);
        } finally {
            setClkTrackStatus(EClkTrack.ESendBtnThdClkTrack);
            AdLog.writeLog("-- onSendBtnClkTrack");
        }
    }

    /* access modifiers changed from: private */
    public void onSendBtnThdClkTrack() {
        setClkTrackStatus(EClkTrack.EIdle);
        int size = this.mBtnThdClkTrackList.size();
        AdLog.writeLog("++ onSendBtnThdClkTrack, Size of list is " + size);
        if (size > 0) {
            try {
                this.mBtnThdClkTrackList.removeLast();
                this.mHttpEngine.requestSend(this.mBtnThdClkTrackList.get(size - 1));
            } catch (Exception e) {
                String err = "Failed to onSendBtnThdClkTrack, err = " + e.toString();
                AdLog.writeLog(err);
                Log.e("AdChinaError", err);
            } finally {
                setClkTrackStatus(EClkTrack.ESendBtnThdClkTrack);
                AdLog.writeLog("-- onSendBtnThdClkTrack");
            }
        }
    }

    /* access modifiers changed from: private */
    public void onRefreshAd() {
        AdLog.writeLog("++ onRefreshAd");
        try {
            int delay = Integer.valueOf(this.mRefTime.toString()).intValue();
            try {
                Thread.sleep((long) (delay * com.energysource.szj.embeded.AdManager.AD_FILL_PARENT));
            } catch (Exception e) {
            } finally {
                setRecvAdStatus(ERecvAdStatus.EReceiveAd);
                AdLog.writeLog("-- onRefreshAd, delay = " + delay);
            }
        } catch (Exception e2) {
            try {
                Thread.sleep((long) (15 * com.energysource.szj.embeded.AdManager.AD_FILL_PARENT));
            } catch (Exception e3) {
            } finally {
                setRecvAdStatus(ERecvAdStatus.EReceiveAd);
                AdLog.writeLog("-- onRefreshAd, delay = " + 15);
            }
        } catch (Throwable th) {
            try {
                Thread.sleep((long) (15 * com.energysource.szj.embeded.AdManager.AD_FILL_PARENT));
            } catch (Exception e4) {
            } finally {
                setRecvAdStatus(ERecvAdStatus.EReceiveAd);
                AdLog.writeLog("-- onRefreshAd, delay = " + 15);
            }
            throw th;
        }
    }

    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case 1:
                if (this.mAdListener != null) {
                    if (this.mRefreshAd) {
                        this.mAdListener.onRefreshAd(this.mAdView);
                    } else {
                        this.mAdListener.onReceiveAd(this.mAdView);
                    }
                }
                onDisplayAd(msg);
                return true;
            case 2:
                if (this.mAdListener != null) {
                    if (this.mRefreshAd) {
                        this.mAdListener.onFailedToRefreshAd(this.mAdView);
                    } else {
                        this.mAdListener.onFailedToReceiveAd(this.mAdView);
                    }
                }
                this.mHasAd = false;
                setRecvAdStatus(ERecvAdStatus.ERefreshAd);
                return true;
            case 3:
                setRecvAdStatus(ERecvAdStatus.ERefreshAd);
                return true;
            case 4:
                if (this.mAdListener != null) {
                    this.mAdListener.onReceiveFullScreenAd(this.mAdView);
                }
                onDisplayFullScreenAd(msg);
                return true;
            case 5:
                if (this.mAdListener != null) {
                    this.mAdListener.onFailedToReceiveFullScreenAd(this.mAdView);
                }
                setRecvAdStatus(ERecvAdStatus.EGetImgMaterial);
                return true;
            default:
                return true;
        }
    }

    class RecvAdThread extends Thread {
        private static /* synthetic */ int[] $SWITCH_TABLE$com$adchina$android$ads$AdEngine$ERecvAdStatus;

        static /* synthetic */ int[] $SWITCH_TABLE$com$adchina$android$ads$AdEngine$ERecvAdStatus() {
            int[] iArr = $SWITCH_TABLE$com$adchina$android$ads$AdEngine$ERecvAdStatus;
            if (iArr == null) {
                iArr = new int[ERecvAdStatus.values().length];
                try {
                    iArr[ERecvAdStatus.EGetFullScreenImgMaterial.ordinal()] = 2;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[ERecvAdStatus.EGetImgMaterial.ordinal()] = 3;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[ERecvAdStatus.EIdle.ordinal()] = 9;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[ERecvAdStatus.EReceiveAd.ordinal()] = 1;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[ERecvAdStatus.ERefreshAd.ordinal()] = 6;
                } catch (NoSuchFieldError e5) {
                }
                try {
                    iArr[ERecvAdStatus.ESendFullScreenImpTrack.ordinal()] = 7;
                } catch (NoSuchFieldError e6) {
                }
                try {
                    iArr[ERecvAdStatus.ESendFullScreenThdImpTrack.ordinal()] = 8;
                } catch (NoSuchFieldError e7) {
                }
                try {
                    iArr[ERecvAdStatus.ESendImpTrack.ordinal()] = 4;
                } catch (NoSuchFieldError e8) {
                }
                try {
                    iArr[ERecvAdStatus.ESendThdImpTrack.ordinal()] = 5;
                } catch (NoSuchFieldError e9) {
                }
                $SWITCH_TABLE$com$adchina$android$ads$AdEngine$ERecvAdStatus = iArr;
            }
            return iArr;
        }

        public RecvAdThread() {
        }

        public void run() {
            while (!AdEngine.this.mStop) {
                switch ($SWITCH_TABLE$com$adchina$android$ads$AdEngine$ERecvAdStatus()[AdEngine.this.mRunState.ordinal()]) {
                    case 1:
                        AdEngine.this.onReceiveAd();
                        break;
                    case 2:
                        AdEngine.this.onGetFullScreenImgMaterial();
                        AdEngine.this.setAdSpaceIdAsFullScreen(false);
                        break;
                    case 3:
                        AdEngine.this.onGetImgMaterial();
                        break;
                    case 4:
                        AdEngine.this.onSendImpTrack();
                        break;
                    case 5:
                        AdEngine.this.onSendThdImpTrack();
                        break;
                    case 6:
                        AdEngine.this.onRefreshAd();
                        break;
                    case 7:
                        AdEngine.this.onSendFullScreenImpTrack();
                        break;
                    case 8:
                        AdEngine.this.onSendFullScreenThdImpTrack();
                        break;
                }
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    class ClkTrackThread extends Thread {
        private static /* synthetic */ int[] $SWITCH_TABLE$com$adchina$android$ads$AdEngine$EClkTrack;

        static /* synthetic */ int[] $SWITCH_TABLE$com$adchina$android$ads$AdEngine$EClkTrack() {
            int[] iArr = $SWITCH_TABLE$com$adchina$android$ads$AdEngine$EClkTrack;
            if (iArr == null) {
                iArr = new int[EClkTrack.values().length];
                try {
                    iArr[EClkTrack.EIdle.ordinal()] = 7;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[EClkTrack.ESendBtnClkTrack.ordinal()] = 3;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[EClkTrack.ESendBtnThdClkTrack.ordinal()] = 4;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[EClkTrack.ESendClkTrack.ordinal()] = 1;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[EClkTrack.ESendFullScreenClkTrack.ordinal()] = 5;
                } catch (NoSuchFieldError e5) {
                }
                try {
                    iArr[EClkTrack.ESendFullScreenThdClkTrack.ordinal()] = 6;
                } catch (NoSuchFieldError e6) {
                }
                try {
                    iArr[EClkTrack.ESendThdClkTrack.ordinal()] = 2;
                } catch (NoSuchFieldError e7) {
                }
                $SWITCH_TABLE$com$adchina$android$ads$AdEngine$EClkTrack = iArr;
            }
            return iArr;
        }

        public ClkTrackThread() {
        }

        public void run() {
            while (!AdEngine.this.mStop) {
                switch ($SWITCH_TABLE$com$adchina$android$ads$AdEngine$EClkTrack()[AdEngine.this.mClickStatus.ordinal()]) {
                    case 1:
                        AdEngine.this.onSendClkTrack();
                        break;
                    case 2:
                        AdEngine.this.onSendThdClkTrack();
                        break;
                    case 3:
                        AdEngine.this.onSendBtnClkTrack();
                        break;
                    case 4:
                        AdEngine.this.onSendBtnThdClkTrack();
                        break;
                    case 5:
                        AdEngine.this.onSendFullScreenClkTrack();
                        break;
                    case 6:
                        AdEngine.this.onSendFullScreenThdClkTrack();
                        break;
                }
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public void onClickBackground() {
        AdLog.writeLog("++ onClick");
        if (this.mSmsType.length() > 0) {
            openBrowser(this.mAdClickUrl.toString());
            setClkTrackStatus(EClkTrack.ESendThdClkTrack);
            AdLog.writeLog("-- onClick");
        }
    }

    public void onClickFullScreenAd() {
        AdLog.writeLog("++ onFullScreenClick");
        openBrowser(this.mFullScreenClkUrl.toString());
        setClkTrackStatus(EClkTrack.ESendFullScreenClkTrack);
        AdLog.writeLog("-- onFullScreenClick");
    }

    public void onTouchEvent(MotionEvent event) {
        float fX = event.getX();
        float fY = event.getY();
        if (this.mBtnType.length() > 0) {
            int x = Integer.valueOf(this.mBtnX.toString()).intValue();
            int y = Integer.valueOf(this.mBtnY.toString()).intValue();
            if (new Rect(x, y, x + Integer.valueOf(this.mBtnW.toString()).intValue(), y + Integer.valueOf(this.mBtnH.toString()).intValue()).contains((int) fX, (int) fY)) {
                setClkTrackStatus(EClkTrack.ESendBtnClkTrack);
                if (this.mBtnType.toString().equalsIgnoreCase("down")) {
                    openBrowser(this.mBtnDownloadUrl.toString());
                } else if (this.mBtnType.toString().equalsIgnoreCase(DomobAdManager.ACTION_SMS)) {
                    if (this.mAdListener.OnRecvSms(this.mAdView, "SMS:" + ((Object) this.mBtnSmsContent) + " To:" + ((Object) this.mBtnSmsNumber))) {
                        sendSms(this.mBtnSmsNumber.toString(), this.mBtnSmsContent.toString());
                    }
                } else if (this.mBtnType.toString().equalsIgnoreCase(DomobAdManager.ACTION_CALL)) {
                    makeCall(this.mBtnCallNumber.toString());
                }
            } else {
                onClickBackground();
            }
        } else {
            onClickBackground();
        }
    }

    private void sendSms(String number, String content) {
        AdLog.writeLog("++ sendSms<" + number + "," + content + XmlConstant.CLOSE_TAG);
        Intent retIntent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + number));
        retIntent.putExtra("sms_body", content);
        this.mContext.startActivity(retIntent);
        AdLog.writeLog("-- sendSms");
    }

    private void makeCall(String number) {
        this.mContext.startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:" + number)));
    }

    private void openBrowser(String targetUrl) {
        if (this.mAdClickUrl.length() > 0) {
            this.mContext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(targetUrl)));
        }
    }
}
