package com.adchina.android.ads;

import cn.domob.android.ads.DomobAdManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public final class XmlEngine {
    private String mAdClickUrl = "";
    private String mAdType = "";
    private String mBtnCallNumber = "";
    private String mBtnClkUrl = "";
    private String mBtnDownloadUrl = "";
    private String mBtnH = "";
    private String mBtnSmsContent = "";
    private String mBtnSmsNumber = "";
    private String mBtnType = "";
    private String mBtnW = "";
    private String mBtnX = "";
    private String mBtnY = "";
    private String mFC = "";
    private String mFullScreenClkUrl = "";
    private String mFullScreenImgAddr = "";
    private String mFullscreenThdClkTrack = "";
    private String mFullscreenThdImpTrack = "";
    private String mImgAddr = "";
    private String mImgType = "";
    private String mRT = "";
    private String mReportBaseUrl = "";
    private String mSmsAddr = "";
    private String mSmsContent = "";
    private String mSmsType = "";
    private String mThdClkTrack = "";
    private String mThdImpTrack = "";
    private String mTxtLnkText = "";
    private String mXmlVer = "";

    /* access modifiers changed from: protected */
    public void readXmlInfo(StringBuffer xmlVer, StringBuffer adType, StringBuffer adClickUrl, StringBuffer reportBaseUrl, StringBuffer thdImpTrack, StringBuffer thdClkTrack, StringBuffer fc, StringBuffer rt, StringBuffer smsAddr, StringBuffer smsContent, StringBuffer smsType, StringBuffer imgAddr, StringBuffer txtLnkText, StringBuffer btnType, StringBuffer btnX, StringBuffer btnY, StringBuffer btnW, StringBuffer btnH, StringBuffer btnClkUrl, StringBuffer btnDownloadUrl, StringBuffer btnSmsNumber, StringBuffer btnSmsContent, StringBuffer btnCallNumber, StringBuffer fullscreenImgAddr, StringBuffer fullScreenClkUrl, StringBuffer fullscreenThdImpTrack, StringBuffer fullscreenThdClkTrack) throws UnsupportedEncodingException {
        xmlVer.append(this.mXmlVer);
        adType.append(this.mAdType);
        adClickUrl.append(URLDecoder.decode(this.mAdClickUrl, "utf-8"));
        reportBaseUrl.append(URLDecoder.decode(this.mReportBaseUrl, "utf-8"));
        thdImpTrack.append(URLDecoder.decode(this.mThdImpTrack, "utf-8"));
        thdClkTrack.append(URLDecoder.decode(this.mThdClkTrack, "utf-8"));
        fc.append(this.mFC);
        rt.append(this.mRT);
        smsAddr.append(URLDecoder.decode(this.mSmsAddr, "utf-8"));
        smsContent.append(URLDecoder.decode(this.mSmsContent, "utf-8"));
        smsType.append(URLDecoder.decode(this.mSmsType, "utf-8"));
        txtLnkText.append(URLDecoder.decode(this.mTxtLnkText, "utf-8"));
        imgAddr.append(URLDecoder.decode(this.mImgAddr, "utf-8"));
        if (adType.toString().compareToIgnoreCase("img") == 0) {
            adType.setLength(0);
            adType.append(URLDecoder.decode(this.mImgType, "utf-8"));
        }
        btnType.append(this.mBtnType);
        btnX.append(this.mBtnX);
        btnY.append(this.mBtnY);
        btnW.append(this.mBtnW);
        btnH.append(this.mBtnH);
        btnClkUrl.append(URLDecoder.decode(this.mBtnClkUrl, "utf-8"));
        btnDownloadUrl.append(URLDecoder.decode(this.mBtnDownloadUrl, "utf-8"));
        btnSmsNumber.append(this.mBtnSmsNumber);
        btnSmsContent.append(this.mBtnSmsContent);
        btnCallNumber.append(this.mBtnCallNumber);
        fullscreenImgAddr.append(URLDecoder.decode(this.mFullScreenImgAddr, "utf-8"));
        fullScreenClkUrl.append(URLDecoder.decode(this.mFullScreenClkUrl, "utf-8"));
        fullscreenThdImpTrack.append(URLDecoder.decode(this.mFullscreenThdImpTrack, "utf-8"));
        fullscreenThdClkTrack.append(URLDecoder.decode(this.mFullscreenThdClkTrack, "utf-8"));
        AdLog.writeDebugLog("Xml from Adserver:");
        AdLog.writeDebugLog("<?xml version=\"1.0\"?>");
        AdLog.writeDebugLog("<ac ver=\"" + this.mXmlVer + "\">");
        AdLog.writeDebugLog("    <type>" + adType.toString() + "</type>");
        AdLog.writeDebugLog("    <cu>" + adClickUrl.toString() + "</cu>");
        AdLog.writeDebugLog("    <rbu>" + reportBaseUrl.toString() + "</rbu>");
        AdLog.writeDebugLog("    <itu>" + thdImpTrack.toString() + "</itu>");
        AdLog.writeDebugLog("    <ctu>" + thdClkTrack.toString() + "</ctu>");
        AdLog.writeDebugLog("    <fc>" + fc.toString() + "</fc>");
        AdLog.writeDebugLog("    <rt>" + rt.toString() + "</rt>");
        AdLog.writeDebugLog("    <sa>" + smsAddr.toString() + "</sa>");
        AdLog.writeDebugLog("    <sc>" + smsContent.toString() + "</sc>");
        AdLog.writeDebugLog("    <sptype>" + smsType.toString() + "</sptype>");
        AdLog.writeDebugLog("    <txt>" + txtLnkText.toString() + "</txt>");
        AdLog.writeDebugLog("    <img type=\"" + this.mImgType.toString() + "\">" + imgAddr.toString() + "</img>");
        AdLog.writeDebugLog("    <video type=\"\"></video>");
        AdLog.writeDebugLog("    <btn type=\"" + this.mBtnType + "\" x=\"" + this.mBtnX + "\" y=\"" + this.mBtnY + "\" w=\"" + this.mBtnW + "\" h=\"" + this.mBtnH + "\">");
        AdLog.writeDebugLog("    <curl>" + ((Object) btnClkUrl) + "</curl>");
        AdLog.writeDebugLog("    <down>" + ((Object) btnDownloadUrl) + "</down>");
        AdLog.writeDebugLog("    <sms>" + ((Object) btnSmsNumber) + ":" + ((Object) btnSmsContent) + "</sms>");
        AdLog.writeDebugLog("    <call>" + ((Object) btnCallNumber) + "</call>");
        AdLog.writeDebugLog("    </btn>");
        AdLog.writeDebugLog("    <fsad>");
        AdLog.writeDebugLog("    <fsurl>" + fullscreenImgAddr.toString() + "</fsurl>");
        AdLog.writeDebugLog("    <fscurl>" + fullScreenClkUrl.toString() + "</fscurl>");
        AdLog.writeDebugLog("    <fsimp>" + fullscreenThdImpTrack.toString() + "</fsimp>");
        AdLog.writeDebugLog("    <fsclk>" + fullscreenThdClkTrack.toString() + "</fsclk>");
        AdLog.writeDebugLog("    </fsad>");
        AdLog.writeDebugLog("</ac>");
    }

    /* access modifiers changed from: protected */
    public void parseXmlStream(InputStream inputStream) throws XmlPullParserException, IOException {
        XmlPullParser xmlPull = XmlPullParserFactory.newInstance().newPullParser();
        xmlPull.setInput(inputStream, "utf-8");
        for (int eventCode = xmlPull.getEventType(); eventCode != 1; eventCode = xmlPull.next()) {
            switch (eventCode) {
                case 2:
                    parseTag(xmlPull, xmlPull.getName());
                    break;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void parseTag(XmlPullParser xmlPull, String name) throws XmlPullParserException, IndexOutOfBoundsException, IOException {
        if (name.equalsIgnoreCase("ac")) {
            this.mXmlVer = String.valueOf(this.mXmlVer) + xmlPull.getAttributeValue(0);
        } else if (name.equalsIgnoreCase("type")) {
            this.mAdType = String.valueOf(this.mAdType) + xmlPull.nextText();
        } else if (name.equalsIgnoreCase("cu")) {
            this.mAdClickUrl = String.valueOf(this.mAdClickUrl) + xmlPull.nextText();
        } else if (name.equalsIgnoreCase("rbu")) {
            this.mReportBaseUrl = String.valueOf(this.mReportBaseUrl) + xmlPull.nextText();
        } else if (name.equalsIgnoreCase("itu")) {
            this.mThdImpTrack = String.valueOf(this.mThdImpTrack) + xmlPull.nextText();
        } else if (name.equalsIgnoreCase("ctu")) {
            this.mThdClkTrack = String.valueOf(this.mThdClkTrack) + xmlPull.nextText();
        } else if (name.equalsIgnoreCase("fc")) {
            this.mFC = String.valueOf(this.mFC) + xmlPull.nextText();
        } else if (name.equalsIgnoreCase("rt")) {
            this.mRT = String.valueOf(this.mRT) + xmlPull.nextText();
        } else if (name.equalsIgnoreCase("sa")) {
            this.mSmsAddr = String.valueOf(this.mSmsAddr) + xmlPull.nextText();
        } else if (name.equalsIgnoreCase("sc")) {
            this.mSmsContent = String.valueOf(this.mSmsContent) + xmlPull.nextText();
        } else if (name.equalsIgnoreCase("sptype")) {
            this.mSmsType = String.valueOf(this.mSmsType) + xmlPull.nextText();
        } else if (name.equalsIgnoreCase("txt")) {
            this.mTxtLnkText = String.valueOf(this.mTxtLnkText) + xmlPull.nextText();
        } else if (name.equalsIgnoreCase("img")) {
            this.mImgType = String.valueOf(this.mImgType) + xmlPull.getAttributeValue(0);
            this.mImgAddr = String.valueOf(this.mImgAddr) + xmlPull.nextText();
        } else if (name.equalsIgnoreCase("btn")) {
            this.mBtnType = String.valueOf(this.mBtnType) + xmlPull.getAttributeValue(0);
            this.mBtnX = String.valueOf(this.mBtnX) + xmlPull.getAttributeValue(1);
            this.mBtnY = String.valueOf(this.mBtnY) + xmlPull.getAttributeValue(2);
            this.mBtnW = String.valueOf(this.mBtnW) + xmlPull.getAttributeValue(3);
            this.mBtnH = String.valueOf(this.mBtnH) + xmlPull.getAttributeValue(4);
        } else if (name.equalsIgnoreCase("curl")) {
            this.mBtnClkUrl = String.valueOf(this.mBtnClkUrl) + xmlPull.nextText();
        } else if (name.equalsIgnoreCase("down")) {
            this.mBtnDownloadUrl = String.valueOf(this.mBtnDownloadUrl) + xmlPull.nextText();
        } else if (name.equalsIgnoreCase(DomobAdManager.ACTION_SMS)) {
            String sms = xmlPull.nextText();
            int tag = sms.indexOf(":");
            if (-1 != tag) {
                this.mBtnSmsNumber = String.valueOf(this.mBtnSmsNumber) + sms.substring(0, tag);
                this.mBtnSmsContent = String.valueOf(this.mBtnSmsContent) + sms.substring(tag + 1);
            }
        } else if (name.equalsIgnoreCase(DomobAdManager.ACTION_CALL)) {
            this.mBtnCallNumber = String.valueOf(this.mBtnCallNumber) + xmlPull.nextText();
        } else if (name.equalsIgnoreCase("fsurl")) {
            this.mFullScreenImgAddr = String.valueOf(this.mFullScreenImgAddr) + xmlPull.nextText();
        } else if (name.equalsIgnoreCase("fscurl")) {
            this.mFullScreenClkUrl = String.valueOf(this.mFullScreenClkUrl) + xmlPull.nextText();
        } else if (name.equalsIgnoreCase("fsimp")) {
            this.mFullscreenThdImpTrack = String.valueOf(this.mFullscreenThdImpTrack) + xmlPull.nextText();
        } else if (name.equalsIgnoreCase("fsclk")) {
            this.mFullscreenThdClkTrack = String.valueOf(this.mFullscreenThdClkTrack) + xmlPull.nextText();
        }
    }
}
