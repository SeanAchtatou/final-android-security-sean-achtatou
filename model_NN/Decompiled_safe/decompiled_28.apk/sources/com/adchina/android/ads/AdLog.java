package com.adchina.android.ads;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Date;
import java.text.SimpleDateFormat;

public final class AdLog {
    private static boolean mDebugInit = false;
    private static File mDebugLogFile = null;
    private static FileOutputStream mDebugLogStream = null;
    private static boolean mInit = false;
    private static File mLogFile = null;
    private static FileOutputStream mLogStream = null;
    private static boolean mOpenDebugLog = false;
    private static boolean mOpenLog = false;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    protected static void initLog(String aFileName) {
        if (mOpenLog && !mInit) {
            mInit = true;
            mLogFile = new File("/sdcard/" + aFileName);
            if (mLogFile.exists()) {
                mLogFile.delete();
            }
            try {
                mLogFile.createNewFile();
                mLogStream = new FileOutputStream(mLogFile, true);
            } catch (Exception e) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    protected static void initDebugLog(String aFileName) {
        if (mOpenDebugLog && !mDebugInit) {
            mDebugInit = true;
            mDebugLogFile = new File("/sdcard/" + aFileName);
            if (mDebugLogFile.exists()) {
                mDebugLogFile.delete();
            }
            try {
                mDebugLogFile.createNewFile();
                mDebugLogStream = new FileOutputStream(mDebugLogFile, true);
            } catch (Exception e) {
            }
        }
    }

    protected static void uninitLog() {
        if (mOpenLog) {
            try {
                mLogStream.close();
            } catch (Exception e) {
            }
        }
    }

    protected static void uninitDebugLog() {
        if (mOpenDebugLog) {
            try {
                mDebugLogStream.close();
            } catch (Exception e) {
            }
        }
    }

    protected static void writeLog(String strLog) {
        if (mOpenLog) {
            try {
                mLogStream.write((String.valueOf(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss --- ").format(new Date(System.currentTimeMillis()))) + strLog + "\r\n").getBytes());
                mLogStream.flush();
            } catch (Exception e) {
            }
        }
    }

    protected static void writeDebugLog(String strLog) {
        if (mOpenDebugLog) {
            try {
                mDebugLogStream.write((String.valueOf(strLog) + "\r\n").getBytes());
                mDebugLogStream.flush();
            } catch (Exception e) {
            }
        }
    }
}
