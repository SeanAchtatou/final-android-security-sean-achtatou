package com.adchina.android.ads;

import android.graphics.Bitmap;
import java.util.Vector;

public class GifEngine {
    private Vector<Bitmap> frames = new Vector<>(1);
    private int index = 0;

    public void addImage(Bitmap image) {
        this.frames.addElement(image);
    }

    public int size() {
        return this.frames.size();
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public Bitmap getImage() {
        if (size() == 0) {
            return null;
        }
        return this.frames.elementAt(this.index);
    }

    public void nextFrame() {
        if (this.index + 1 < size()) {
            this.index++;
        } else {
            this.index = 0;
        }
    }

    public static GifEngine CreateGifImage(byte[] abyte0) {
        try {
            GifEngine GF = new GifEngine();
            GifDecoder gifdecoder = new GifDecoder(abyte0);
            while (gifdecoder.moreFrames()) {
                try {
                    Bitmap image = gifdecoder.decodeImage();
                    if (!(GF == null || image == null)) {
                        GF.addImage(image);
                    }
                    gifdecoder.nextFrame();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            gifdecoder.clear();
            return GF;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    static class GifDecoder {
        private static final int[] F6 = {8, 8, 4, 2};
        private static final int[] F8;
        byte[] C2;
        private int E0 = -1;
        private int[] E1 = new int[280];
        private int E2 = -1;
        private int E6 = 0;
        private boolean E7 = false;
        private int[] E8 = null;
        private int ED = 0;
        private boolean EE = false;
        private boolean EF = false;
        private int[] F0 = null;
        private int F1 = 0;
        private boolean F2;
        private int F3;
        private long F4;
        private int F5 = 0;
        int FA;
        int FB;
        int FC;
        int FD;
        int curFrame = 0;
        private int height = 0;
        int poolsize;
        private int width = 0;

        static {
            int[] iArr = new int[4];
            iArr[1] = 4;
            iArr[2] = 2;
            iArr[3] = 1;
            F8 = iArr;
        }

        public GifDecoder(byte[] abyte0) {
            this.C2 = abyte0;
            this.poolsize = this.C2.length;
            this.FA = 0;
        }

        public boolean moreFrames() {
            return this.poolsize - this.FA >= 16;
        }

        public void nextFrame() {
            this.curFrame++;
        }

        public Bitmap decodeImage() {
            return decodeImage(this.curFrame);
        }

        /* JADX WARNING: CFG modification limit reached, blocks count: 138 */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0046, code lost:
            if (E7() != false) goto L_0x002a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0048, code lost:
            r2 = null;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.graphics.Bitmap decodeImage(int r6) {
            /*
                r5 = this;
                r4 = 0
                int r2 = r5.E0
                if (r6 > r2) goto L_0x0007
                r2 = r4
            L_0x0006:
                return r2
            L_0x0007:
                int r2 = r5.E0
                if (r2 >= 0) goto L_0x002a
                boolean r2 = r5.E3()
                if (r2 != 0) goto L_0x0013
                r2 = r4
                goto L_0x0006
            L_0x0013:
                boolean r2 = r5.E4()
                if (r2 != 0) goto L_0x002a
                r2 = r4
                goto L_0x0006
            L_0x001b:
                android.graphics.Bitmap r0 = r5.createImage()
                int r2 = r5.E0
                int r2 = r2 + 1
                r5.E0 = r2
                int r2 = r5.E0
                if (r2 >= r6) goto L_0x0056
                r0 = 0
            L_0x002a:
                r2 = 1
                boolean r2 = r5.E9(r2)
                if (r2 != 0) goto L_0x0033
                r2 = r4
                goto L_0x0006
            L_0x0033:
                int[] r2 = r5.E1
                r3 = 0
                r1 = r2[r3]
                r2 = 59
                if (r1 != r2) goto L_0x003e
                r2 = r4
                goto L_0x0006
            L_0x003e:
                r2 = 33
                if (r1 != r2) goto L_0x004a
                boolean r2 = r5.E7()
                if (r2 != 0) goto L_0x002a
                r2 = r4
                goto L_0x0006
            L_0x004a:
                r2 = 44
                if (r1 != r2) goto L_0x002a
                boolean r2 = r5.E5()
                if (r2 != 0) goto L_0x001b
                r2 = r4
                goto L_0x0006
            L_0x0056:
                r2 = r0
                goto L_0x0006
            */
            throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.GifEngine.GifDecoder.decodeImage(int):android.graphics.Bitmap");
        }

        public void clear() {
            this.C2 = null;
            this.E1 = null;
            this.E8 = null;
            this.F0 = null;
        }

        private Bitmap createImage() {
            int j2;
            int i = this.width;
            int j = this.height;
            int k1 = 0;
            int[] ai = new int[4096];
            int[] ai1 = new int[4096];
            int[] ai2 = new int[8192];
            if (!E9(1)) {
                return null;
            }
            int k = this.E1[0];
            int[] image = new int[(this.width * this.height)];
            int[] ai3 = this.E8;
            if (this.EE) {
                ai3 = this.F0;
            }
            if (this.E2 >= 0) {
                ai3[this.E2] = 16777215;
            }
            int l2 = 1 << k;
            int j3 = l2 + 1;
            int k2 = k + 1;
            int l3 = l2 + 2;
            int k3 = -1;
            int j4 = -1;
            for (int l1 = 0; l1 < l2; l1++) {
                ai1[l1] = l1;
            }
            int j22 = 0;
            E2();
            int j1 = 0;
            int i2 = 0;
            while (i2 < j) {
                int i1 = 0;
                int j23 = j22;
                while (i1 < i) {
                    if (j23 == 0) {
                        int i4 = E1(k2);
                        if (i4 < 0) {
                            return Bitmap.createBitmap(image, this.width, this.height, Bitmap.Config.RGB_565);
                        } else if (i4 > l3 || i4 == j3) {
                            return Bitmap.createBitmap(image, this.width, this.height, Bitmap.Config.RGB_565);
                        } else if (i4 == l2) {
                            k2 = k + 1;
                            l3 = l2 + 2;
                            k3 = -1;
                        } else if (k3 == -1) {
                            ai2[j23] = ai1[i4];
                            k3 = i4;
                            j4 = i4;
                            j23++;
                        } else {
                            int i3 = i4;
                            if (i4 == l3) {
                                ai2[j23] = j4;
                                i4 = k3;
                                j23++;
                            }
                            while (i4 > l2) {
                                ai2[j23] = ai1[i4];
                                i4 = ai[i4];
                                j23++;
                            }
                            j4 = ai1[i4];
                            if (l3 >= 4096) {
                                return Bitmap.createBitmap(image, this.width, this.height, Bitmap.Config.RGB_565);
                            }
                            j2 = j23 + 1;
                            ai2[j23] = j4;
                            ai[l3] = k3;
                            ai1[l3] = j4;
                            l3++;
                            if (l3 >= (1 << k2) && l3 < 4096) {
                                k2++;
                            }
                            k3 = i3;
                        }
                    } else {
                        j2 = j23;
                    }
                    int j24 = j2 - 1;
                    int l = ai2[j24];
                    if (l < 0) {
                        return Bitmap.createBitmap(image, this.width, this.height, Bitmap.Config.RGB_565);
                    }
                    if (i1 == 0) {
                        this.FC = 0;
                        this.FB = ai3[l];
                        this.FD = 0;
                    } else if (this.FB != ai3[l]) {
                        for (int mm = this.FD; mm <= this.FD + this.FC; mm++) {
                            image[(this.width * j1) + mm] = this.FB;
                        }
                        this.FC = 0;
                        this.FB = ai3[l];
                        this.FD = i1;
                        if (i1 == i - 1) {
                            image[(this.width * j1) + i1] = ai3[l];
                        }
                    } else {
                        this.FC = this.FC + 1;
                        if (i1 == i - 1) {
                            for (int mm2 = this.FD; mm2 <= this.FD + this.FC; mm2++) {
                                image[(this.width * j1) + mm2] = this.FB;
                            }
                        }
                    }
                    i1++;
                    j23 = j24;
                }
                if (this.EF) {
                    j1 += F6[k1];
                    while (j1 >= j) {
                        k1++;
                        if (k1 > 3) {
                            return Bitmap.createBitmap(image, this.width, this.height, Bitmap.Config.RGB_565);
                        }
                        j1 = F8[k1];
                    }
                    continue;
                } else {
                    j1++;
                }
                i2++;
                j22 = j23;
            }
            return Bitmap.createBitmap(image, this.width, this.height, Bitmap.Config.RGB_565);
        }

        private int E1(int i) {
            while (true) {
                if (this.F5 >= i) {
                    break;
                } else if (this.F2) {
                    return -1;
                } else {
                    if (this.F1 == 0) {
                        this.F1 = E8();
                        this.F3 = 0;
                        if (this.F1 <= 0) {
                            this.F2 = true;
                            break;
                        }
                    }
                    this.F4 += (long) (this.E1[this.F3] << this.F5);
                    this.F3++;
                    this.F5 += 8;
                    this.F1--;
                }
            }
            int j = ((int) this.F4) & ((1 << i) - 1);
            this.F4 >>= i;
            this.F5 -= i;
            return j;
        }

        private void E2() {
            this.F5 = 0;
            this.F1 = 0;
            this.F4 = 0;
            this.F2 = false;
            this.F3 = -1;
        }

        private boolean E3() {
            if (!E9(6)) {
                return false;
            }
            return this.E1[0] == 71 && this.E1[1] == 73 && this.E1[2] == 70 && this.E1[3] == 56 && (this.E1[4] == 55 || this.E1[4] == 57) && this.E1[5] == 97;
        }

        private boolean E4() {
            if (!E9(7)) {
                return false;
            }
            int i = this.E1[4];
            this.E6 = 2 << (i & 7);
            this.E7 = EB(i, 128);
            this.E8 = null;
            return !this.E7 || E6(this.E6, true);
        }

        private boolean E5() {
            if (!E9(9)) {
                return false;
            }
            this.width = EA(this.E1[4], this.E1[5]);
            this.height = EA(this.E1[6], this.E1[7]);
            int i = this.E1[8];
            this.EE = EB(i, 128);
            this.ED = 2 << (i & 7);
            this.EF = EB(i, 64);
            this.F0 = null;
            return !this.EE || E6(this.ED, false);
        }

        private boolean E6(int i, boolean flag) {
            int[] ai = new int[i];
            for (int j = 0; j < i; j++) {
                if (!E9(3)) {
                    return false;
                }
                ai[j] = (this.E1[0] << 16) | (this.E1[1] << 8) | this.E1[2] | -16777216;
            }
            if (flag) {
                this.E8 = ai;
            } else {
                this.F0 = ai;
            }
            return true;
        }

        private boolean E7() {
            if (!E9(1)) {
                return false;
            }
            switch (this.E1[0]) {
                case 249:
                    if (E8() >= 0) {
                        if ((this.E1[0] & 1) == 0) {
                            this.E2 = -1;
                            break;
                        } else {
                            this.E2 = this.E1[3];
                            break;
                        }
                    } else {
                        return true;
                    }
            }
            do {
            } while (E8() > 0);
            return true;
        }

        private int E8() {
            if (!E9(1)) {
                return -1;
            }
            int i = this.E1[0];
            if (i == 0 || E9(i)) {
                return i;
            }
            return -1;
        }

        /* JADX WARN: Failed to insert an additional move for type inference into block B:12:0x001e */
        /* JADX WARN: Failed to insert an additional move for type inference into block B:11:0x001e */
        /* JADX WARN: Type inference failed for: r2v5, types: [byte[]] */
        /* JADX WARN: Type inference failed for: r1v0, types: [int, byte] */
        /* JADX WARN: Type inference failed for: r1v1 */
        /* JADX WARN: Type inference failed for: r2v6, types: [int[]] */
        /* JADX WARN: Type inference failed for: r1v2, types: [int] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private boolean E9(int r5) {
            /*
                r4 = this;
                int r2 = r4.FA
                int r2 = r2 + r5
                int r3 = r4.poolsize
                if (r2 < r3) goto L_0x0009
                r2 = 0
            L_0x0008:
                return r2
            L_0x0009:
                r0 = 0
            L_0x000a:
                if (r0 < r5) goto L_0x0013
                int r2 = r4.FA
                int r2 = r2 + r5
                r4.FA = r2
                r2 = 1
                goto L_0x0008
            L_0x0013:
                byte[] r2 = r4.C2
                int r3 = r4.FA
                int r3 = r3 + r0
                byte r1 = r2[r3]
                if (r1 >= 0) goto L_0x001e
                int r1 = r1 + 256
            L_0x001e:
                int[] r2 = r4.E1
                r2[r0] = r1
                int r0 = r0 + 1
                goto L_0x000a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.GifEngine.GifDecoder.E9(int):boolean");
        }

        private static final int EA(int i, int j) {
            return (j << 8) | i;
        }

        private static final boolean EB(int i, int j) {
            return (i & j) == j;
        }
    }
}
