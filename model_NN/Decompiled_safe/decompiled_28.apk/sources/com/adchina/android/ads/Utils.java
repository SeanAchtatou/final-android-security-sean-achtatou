package com.adchina.android.ads;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;

public class Utils {
    protected static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                sb.append(line);
            } catch (IOException e) {
                Log.e("AdChinaError", "convertStreamToString:" + e.toString());
            }
        }
        return sb.toString();
    }

    protected static Bitmap convertStreamToBitmap(InputStream is) throws NullPointerException, IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        while (true) {
            int ch = is.read();
            if (ch == -1) {
                byte[] imgdata = stream.toByteArray();
                return BitmapFactory.decodeByteArray(imgdata, 0, imgdata.length);
            }
            stream.write(ch);
        }
    }

    protected static void splitTrackUrl(String urls, LinkedList<String> list) {
        list.clear();
        int start = 0;
        while (true) {
            int index = urls.indexOf("|||", start);
            if (-1 == index) {
                list.add(urls.substring(start));
                return;
            } else {
                list.add(urls.substring(start, index));
                start = index + "|||".length();
            }
        }
    }

    protected static int GetRandomNumber() {
        return (int) (Math.random() * 10000.0d);
    }
}
