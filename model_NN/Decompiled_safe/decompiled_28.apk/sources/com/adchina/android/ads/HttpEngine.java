package com.adchina.android.ads;

import android.content.Context;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;

public final class HttpEngine {
    private static final String KLOGTAG = "AdChinaError";
    private Context mContext = null;
    private List<BasicClientCookie> mNewList = new LinkedList();
    private List<BasicClientCookie> mOldList = new LinkedList();
    private boolean mReadCookie = false;

    public HttpEngine(Context context) {
        this.mContext = context;
    }

    /* access modifiers changed from: protected */
    public boolean requestSend(String httpUrl) throws ClientProtocolException, IOException {
        boolean ret = false;
        if (httpUrl.length() <= 0) {
            return false;
        }
        if (new DefaultHttpClient().execute(new HttpGet(httpUrl)).getStatusLine().getStatusCode() == 200) {
            ret = true;
        }
        return ret;
    }

    private void addCookies(DefaultHttpClient httpClient) {
        if (!this.mOldList.isEmpty()) {
            CookieStore cookieStore = httpClient.getCookieStore();
            cookieStore.clear();
            for (int i = 0; i < this.mOldList.size(); i++) {
                BasicClientCookie oldItem = this.mOldList.get(i);
                BasicClientCookie newItem = new BasicClientCookie(oldItem.getName(), oldItem.getValue());
                newItem.setDomain(oldItem.getDomain());
                cookieStore.addCookie(newItem);
            }
            httpClient.setCookieStore(cookieStore);
        }
    }

    private void saveCookies(DefaultHttpClient httpClient, String aAdspaceId) {
        AdLog.writeDebugLog("Cookies:");
        List<Cookie> listCookies = httpClient.getCookieStore().getCookies();
        if (!listCookies.isEmpty()) {
            this.mNewList.clear();
            for (int i = 0; i < listCookies.size(); i++) {
                Cookie oldItem = (Cookie) listCookies.get(i);
                BasicClientCookie newItem = new BasicClientCookie(oldItem.getName(), oldItem.getValue());
                newItem.setDomain(oldItem.getDomain());
                if (newItem.getValue() != null) {
                    AdLog.writeDebugLog(String.valueOf(newItem.getName()) + ":" + newItem.getValue());
                }
                this.mNewList.add(newItem);
            }
            for (int i2 = 0; i2 < this.mOldList.size(); i2++) {
                boolean bFound = false;
                int k = 0;
                while (true) {
                    if (k >= listCookies.size()) {
                        break;
                    } else if (this.mOldList.get(i2).getName().compareToIgnoreCase(((Cookie) listCookies.get(k)).getName()) == 0) {
                        bFound = true;
                        break;
                    } else {
                        k++;
                    }
                }
                if (!bFound && this.mOldList.get(i2).getValue() != null) {
                    this.mNewList.add(this.mOldList.get(i2));
                }
            }
            writeCookieToFile(aAdspaceId);
        }
    }

    /* access modifiers changed from: protected */
    public String getCookieFilename(String aAdspaceId) {
        return "adchinaCookie.ce" + aAdspaceId;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x011d A[SYNTHETIC, Splitter:B:30:0x011d] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0122 A[Catch:{ IOException -> 0x012a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void writeCookieToFile(java.lang.String r10) {
        /*
            r9 = this;
            java.lang.String r7 = "value:"
            java.lang.String r7 = "name:"
            java.lang.String r7 = "domain:"
            java.lang.String r7 = "\r\n"
            java.lang.String r7 = "++ writeCookieToFile"
            com.adchina.android.ads.AdLog.writeLog(r7)
            java.util.List<org.apache.http.impl.cookie.BasicClientCookie> r7 = r9.mOldList
            r7.clear()
            r4 = 0
            r5 = 0
            java.lang.String r0 = r9.getCookieFilename(r10)     // Catch:{ Exception -> 0x00f3 }
            android.content.Context r7 = r9.mContext     // Catch:{ Exception -> 0x00f3 }
            r7.deleteFile(r0)     // Catch:{ Exception -> 0x00f3 }
            android.content.Context r7 = r9.mContext     // Catch:{ Exception -> 0x00f3 }
            r8 = 0
            java.io.FileOutputStream r4 = r7.openFileOutput(r0, r8)     // Catch:{ Exception -> 0x00f3 }
            java.io.OutputStreamWriter r6 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x00f3 }
            r6.<init>(r4)     // Catch:{ Exception -> 0x00f3 }
            r3 = 0
        L_0x002a:
            java.util.List<org.apache.http.impl.cookie.BasicClientCookie> r7 = r9.mNewList     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            int r7 = r7.size()     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            if (r3 < r7) goto L_0x0043
            if (r6 == 0) goto L_0x0037
            r6.close()     // Catch:{ IOException -> 0x0126 }
        L_0x0037:
            if (r4 == 0) goto L_0x0133
            r4.close()     // Catch:{ IOException -> 0x0126 }
            r5 = r6
        L_0x003d:
            java.lang.String r7 = "-- writeCookieToFile"
            com.adchina.android.ads.AdLog.writeLog(r7)
            return
        L_0x0043:
            java.util.List<org.apache.http.impl.cookie.BasicClientCookie> r7 = r9.mNewList     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.Object r1 = r7.get(r3)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            org.apache.http.impl.cookie.BasicClientCookie r1 = (org.apache.http.impl.cookie.BasicClientCookie) r1     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.util.List<org.apache.http.impl.cookie.BasicClientCookie> r7 = r9.mOldList     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            r7.add(r1)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r8 = "name:"
            r7.<init>(r8)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r8 = r1.getName()     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r8 = "\r\n"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            r6.write(r7)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            r6.flush()     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r8 = "value:"
            r7.<init>(r8)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r8 = r1.getValue()     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r8 = "\r\n"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            r6.write(r7)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            r6.flush()     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r8 = "domain:"
            r7.<init>(r8)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r8 = r1.getDomain()     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r8 = "\r\n"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            r6.write(r7)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            r6.flush()     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r8 = "name:"
            r7.<init>(r8)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r8 = r1.getName()     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            com.adchina.android.ads.AdLog.writeLog(r7)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r8 = "value:"
            r7.<init>(r8)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r8 = r1.getValue()     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            com.adchina.android.ads.AdLog.writeLog(r7)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r8 = "domain:"
            r7.<init>(r8)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r8 = r1.getDomain()     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            com.adchina.android.ads.AdLog.writeLog(r7)     // Catch:{ Exception -> 0x012f, all -> 0x012c }
            int r3 = r3 + 1
            goto L_0x002a
        L_0x00f3:
            r7 = move-exception
            r2 = r7
        L_0x00f5:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x011a }
            java.lang.String r8 = "Exceptions in writeCookieToFile, err = "
            r7.<init>(r8)     // Catch:{ all -> 0x011a }
            java.lang.String r8 = r2.toString()     // Catch:{ all -> 0x011a }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x011a }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x011a }
            com.adchina.android.ads.AdLog.writeLog(r7)     // Catch:{ all -> 0x011a }
            if (r5 == 0) goto L_0x0110
            r5.close()     // Catch:{ IOException -> 0x0117 }
        L_0x0110:
            if (r4 == 0) goto L_0x003d
            r4.close()     // Catch:{ IOException -> 0x0117 }
            goto L_0x003d
        L_0x0117:
            r7 = move-exception
            goto L_0x003d
        L_0x011a:
            r7 = move-exception
        L_0x011b:
            if (r5 == 0) goto L_0x0120
            r5.close()     // Catch:{ IOException -> 0x012a }
        L_0x0120:
            if (r4 == 0) goto L_0x0125
            r4.close()     // Catch:{ IOException -> 0x012a }
        L_0x0125:
            throw r7
        L_0x0126:
            r7 = move-exception
            r5 = r6
            goto L_0x003d
        L_0x012a:
            r8 = move-exception
            goto L_0x0125
        L_0x012c:
            r7 = move-exception
            r5 = r6
            goto L_0x011b
        L_0x012f:
            r7 = move-exception
            r2 = r7
            r5 = r6
            goto L_0x00f5
        L_0x0133:
            r5 = r6
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.HttpEngine.writeCookieToFile(java.lang.String):void");
    }

    /* JADX INFO: Multiple debug info for r4v2 java.io.IOException: [D('e' java.lang.Exception), D('e' java.io.IOException)] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00fd A[SYNTHETIC, Splitter:B:28:0x00fd] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0102 A[Catch:{ IOException -> 0x0107 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0124 A[SYNTHETIC, Splitter:B:36:0x0124] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0129 A[Catch:{ IOException -> 0x012d }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void readCookieFromFile(java.lang.String r19) {
        /*
            r18 = this;
            java.lang.String r15 = "++ readCookieFromFile"
            com.adchina.android.ads.AdLog.writeLog(r15)
            r0 = r18
            java.util.List<org.apache.http.impl.cookie.BasicClientCookie> r0 = r0.mOldList
            r15 = r0
            r15.clear()
            r9 = 0
            r7 = 0
            java.lang.String r1 = r18.getCookieFilename(r19)     // Catch:{ Exception -> 0x00e3 }
            r0 = r18
            android.content.Context r0 = r0.mContext     // Catch:{ Exception -> 0x00e3 }
            r15 = r0
            java.io.File r6 = r15.getFileStreamPath(r1)     // Catch:{ Exception -> 0x00e3 }
            long r15 = r6.length()     // Catch:{ Exception -> 0x00e3 }
            int r15 = (int) r15     // Catch:{ Exception -> 0x00e3 }
            char[] r10 = new char[r15]     // Catch:{ Exception -> 0x00e3 }
            r0 = r18
            android.content.Context r0 = r0.mContext     // Catch:{ Exception -> 0x00e3 }
            r15 = r0
            java.io.FileInputStream r9 = r15.openFileInput(r1)     // Catch:{ Exception -> 0x00e3 }
            java.io.InputStreamReader r8 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00e3 }
            r8.<init>(r9)     // Catch:{ Exception -> 0x00e3 }
            r8.read(r10)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            r2.<init>(r10)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
        L_0x0039:
            java.lang.String r15 = "name:"
            int r13 = r2.indexOf(r15)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            r15 = -1
            if (r15 != r13) goto L_0x0053
        L_0x0042:
            if (r8 == 0) goto L_0x0047
            r8.close()     // Catch:{ IOException -> 0x0147 }
        L_0x0047:
            if (r9 == 0) goto L_0x015f
            r9.close()     // Catch:{ IOException -> 0x0147 }
            r7 = r8
        L_0x004d:
            java.lang.String r15 = "-- readCookieFromFile"
            com.adchina.android.ads.AdLog.writeLog(r15)
            return
        L_0x0053:
            int r13 = r13 + 5
            java.lang.String r15 = "\r\n"
            int r5 = r2.indexOf(r15)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            java.lang.String r11 = r2.substring(r13, r5)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            int r15 = r5 + 2
            java.lang.String r2 = r2.substring(r15)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            java.lang.String r15 = "value:"
            int r13 = r2.indexOf(r15)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            r15 = -1
            if (r15 == r13) goto L_0x0042
            int r13 = r13 + 6
            java.lang.String r15 = "\r\n"
            int r5 = r2.indexOf(r15)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            java.lang.String r14 = r2.substring(r13, r5)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            int r15 = r5 + 2
            java.lang.String r2 = r2.substring(r15)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            java.lang.String r15 = "domain:"
            int r13 = r2.indexOf(r15)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            r15 = -1
            if (r15 == r13) goto L_0x0042
            int r13 = r13 + 7
            java.lang.String r15 = "\r\n"
            int r5 = r2.indexOf(r15)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            java.lang.String r3 = r2.substring(r13, r5)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            org.apache.http.impl.cookie.BasicClientCookie r12 = new org.apache.http.impl.cookie.BasicClientCookie     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            r12.<init>(r11, r14)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            r12.setDomain(r3)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            r0 = r18
            java.util.List<org.apache.http.impl.cookie.BasicClientCookie> r0 = r0.mOldList     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            r15 = r0
            r15.add(r12)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            java.lang.String r16 = "name:"
            r15.<init>(r16)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            java.lang.StringBuilder r15 = r15.append(r11)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            com.adchina.android.ads.AdLog.writeLog(r15)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            java.lang.String r16 = "value:"
            r15.<init>(r16)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            java.lang.StringBuilder r15 = r15.append(r14)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            com.adchina.android.ads.AdLog.writeLog(r15)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            java.lang.String r16 = "domain:"
            r15.<init>(r16)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            java.lang.StringBuilder r15 = r15.append(r3)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            com.adchina.android.ads.AdLog.writeLog(r15)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            int r15 = r5 + 2
            java.lang.String r2 = r2.substring(r15)     // Catch:{ Exception -> 0x0165, all -> 0x0162 }
            goto L_0x0039
        L_0x00e3:
            r15 = move-exception
            r4 = r15
        L_0x00e5:
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ all -> 0x0121 }
            java.lang.String r16 = "Exceptions in readCookieFromFile , err = "
            r15.<init>(r16)     // Catch:{ all -> 0x0121 }
            java.lang.String r16 = r4.toString()     // Catch:{ all -> 0x0121 }
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ all -> 0x0121 }
            java.lang.String r15 = r15.toString()     // Catch:{ all -> 0x0121 }
            com.adchina.android.ads.AdLog.writeLog(r15)     // Catch:{ all -> 0x0121 }
            if (r7 == 0) goto L_0x0100
            r7.close()     // Catch:{ IOException -> 0x0107 }
        L_0x0100:
            if (r9 == 0) goto L_0x004d
            r9.close()     // Catch:{ IOException -> 0x0107 }
            goto L_0x004d
        L_0x0107:
            r15 = move-exception
            r4 = r15
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            java.lang.String r16 = "Exceptions in readCookieFromFile release resource, err = "
            r15.<init>(r16)
            java.lang.String r16 = r4.toString()
            java.lang.StringBuilder r15 = r15.append(r16)
            java.lang.String r15 = r15.toString()
            com.adchina.android.ads.AdLog.writeLog(r15)
            goto L_0x004d
        L_0x0121:
            r15 = move-exception
        L_0x0122:
            if (r7 == 0) goto L_0x0127
            r7.close()     // Catch:{ IOException -> 0x012d }
        L_0x0127:
            if (r9 == 0) goto L_0x012c
            r9.close()     // Catch:{ IOException -> 0x012d }
        L_0x012c:
            throw r15
        L_0x012d:
            r16 = move-exception
            r4 = r16
            java.lang.StringBuilder r16 = new java.lang.StringBuilder
            java.lang.String r17 = "Exceptions in readCookieFromFile release resource, err = "
            r16.<init>(r17)
            java.lang.String r17 = r4.toString()
            java.lang.StringBuilder r16 = r16.append(r17)
            java.lang.String r16 = r16.toString()
            com.adchina.android.ads.AdLog.writeLog(r16)
            goto L_0x012c
        L_0x0147:
            r15 = move-exception
            r4 = r15
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            java.lang.String r16 = "Exceptions in readCookieFromFile release resource, err = "
            r15.<init>(r16)
            java.lang.String r16 = r4.toString()
            java.lang.StringBuilder r15 = r15.append(r16)
            java.lang.String r15 = r15.toString()
            com.adchina.android.ads.AdLog.writeLog(r15)
        L_0x015f:
            r7 = r8
            goto L_0x004d
        L_0x0162:
            r15 = move-exception
            r7 = r8
            goto L_0x0122
        L_0x0165:
            r15 = move-exception
            r4 = r15
            r7 = r8
            goto L_0x00e5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.HttpEngine.readCookieFromFile(java.lang.String):void");
    }

    /* access modifiers changed from: protected */
    public InputStream requestGet(String httpUrl, String aAdspaceId) throws ClientProtocolException, NullPointerException, IOException {
        HttpEntity entity;
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(httpUrl);
        httpget.setHeader("Connection", "close");
        if (!this.mReadCookie) {
            readCookieFromFile(aAdspaceId);
            this.mReadCookie = true;
        }
        addCookies(httpClient);
        HttpResponse response = httpClient.execute(httpget);
        if (response.getStatusLine().getStatusCode() != 200 || (entity = response.getEntity()) == null) {
            return null;
        }
        InputStream is = entity.getContent();
        saveCookies(httpClient, aAdspaceId);
        return is;
    }

    /* access modifiers changed from: protected */
    public InputStream requestGet(String httpUrl) throws ClientProtocolException, NullPointerException, IOException {
        HttpEntity entity;
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(httpUrl);
        httpget.setHeader("Connection", "close");
        HttpResponse response = httpClient.execute(httpget);
        if (response.getStatusLine().getStatusCode() != 200 || (entity = response.getEntity()) == null) {
            return null;
        }
        return entity.getContent();
    }

    /* access modifiers changed from: protected */
    public void closeStream(InputStream is) {
        if (is != null) {
            try {
                is.close();
            } catch (IOException e) {
                Log.e(KLOGTAG, "closeStream:" + e.toString());
            }
        }
    }
}
