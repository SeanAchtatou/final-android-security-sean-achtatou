package com.adchina.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

public class AdView extends RelativeLayout implements View.OnClickListener {
    private AdEngine mAdEngine = null;
    private ContentView mContentView = null;
    private FullScreenAdView mFsAdView = null;

    public AdView(Context context) {
        super(context);
        initView(context);
    }

    public AdView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public AdView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context);
    }

    private void initView(Context context) {
        this.mContentView = new ContentView(context);
        this.mAdEngine = new AdEngine(context, this, this.mContentView);
        this.mContentView.setAdEngine(this.mAdEngine);
        this.mContentView.setOnClickListener(this);
        addView(this.mContentView);
    }

    public void setAdListener(AdListener adListener) {
        if (this.mAdEngine != null) {
            this.mAdEngine.setAdListener(adListener);
        }
    }

    public void setFullScreenAd(FullScreenAdView fsAdView) {
        this.mFsAdView = fsAdView;
        this.mFsAdView.setAdEngine(this.mAdEngine);
        this.mAdEngine.setFullScreenAdView(this.mFsAdView);
    }

    public void setBackgroundColor(int aColor) {
        if (this.mAdEngine != null) {
            this.mAdEngine.setBackgroundColor(aColor);
        }
    }

    public void setDefaultImage(int aDrawableId) {
        Bitmap defImage = new BitmapDrawable(getContext().getResources().openRawResource(aDrawableId)).getBitmap();
        if (this.mAdEngine != null) {
            this.mAdEngine.setDefaultImage(defImage);
        }
    }

    public void setDefaultUrl(String aUrl) {
        if (this.mAdEngine != null) {
            this.mAdEngine.setDefaultUrl(aUrl);
        }
    }

    public void start() {
        if (this.mAdEngine != null) {
            this.mAdEngine.start();
        }
    }

    public void stop() {
        if (this.mAdEngine != null) {
            this.mAdEngine.stop();
        }
    }

    public boolean hasAd() {
        if (this.mAdEngine != null) {
            return this.mAdEngine.hasAd();
        }
        return false;
    }

    public void onClick(View view) {
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                if (this.mAdEngine == null) {
                    return true;
                }
                this.mAdEngine.onTouchEvent(event);
                return true;
            default:
                return true;
        }
    }
}
