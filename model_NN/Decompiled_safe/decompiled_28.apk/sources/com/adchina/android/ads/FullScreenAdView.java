package com.adchina.android.ads;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class FullScreenAdView extends ImageView implements View.OnClickListener {
    private AdEngine mAdEngine = null;

    public FullScreenAdView(Context context) {
        super(context);
        initFullScreenAd();
    }

    public FullScreenAdView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initFullScreenAd();
    }

    public FullScreenAdView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initFullScreenAd();
    }

    public void initFullScreenAd() {
        setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        setScaleType(ImageView.ScaleType.FIT_XY);
        setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void setAdEngine(AdEngine adEngine) {
        this.mAdEngine = adEngine;
    }

    public void start() {
        if (this.mAdEngine != null) {
            this.mAdEngine.startFullScreenAd();
        }
    }

    public void onClick(View v) {
        if (this.mAdEngine != null) {
            this.mAdEngine.onClickFullScreenAd();
        }
    }
}
