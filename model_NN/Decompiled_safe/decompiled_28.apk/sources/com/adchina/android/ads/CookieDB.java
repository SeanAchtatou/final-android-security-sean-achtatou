package com.adchina.android.ads;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CookieDB {
    private static final String DB_CREATE = "CREATE TABLE adcookie (_id INTEGER PRIMARY KEY,name TEXT,value TEXT,domain TEXT)";
    private static final String DB_NAME = "adcookie.db";
    private static final String DB_TABLE = "adcookie";
    private static final int DB_VERSION = 1;
    public static final String KEY_DOMAIN = "domain";
    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_VALUE = "value";
    private Context mContext = null;
    private DatabaseHelper mDatabaseHelper = null;
    private SQLiteDatabase mReadDB = null;
    private SQLiteDatabase mWriteDB = null;

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, CookieDB.DB_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CookieDB.DB_CREATE);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE adcookie");
            onCreate(db);
        }
    }

    public CookieDB(Context context) {
        this.mContext = context;
        this.mDatabaseHelper = new DatabaseHelper(this.mContext);
    }

    public void openWrite() throws SQLException {
        this.mWriteDB = this.mDatabaseHelper.getWritableDatabase();
    }

    public void openRead() throws SQLException {
        this.mReadDB = this.mDatabaseHelper.getReadableDatabase();
    }

    public void closeWrite() {
        this.mWriteDB.close();
    }

    public void closeRead() {
        this.mReadDB.close();
    }

    public long insertData(int i, String name, String value, String domain) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_NAME, name);
        initialValues.put(KEY_VALUE, value);
        initialValues.put(KEY_DOMAIN, domain);
        return this.mWriteDB.insert(DB_TABLE, KEY_ID, initialValues);
    }

    public boolean IsLocked() {
        return this.mWriteDB.isDbLockedByCurrentThread() || this.mWriteDB.isDbLockedByOtherThreads();
    }

    public void deleteAll() {
        this.mWriteDB.execSQL("DELETE FROM adcookie");
    }

    public Cursor readAll() {
        return this.mReadDB.rawQuery("SELECT * FROM adcookie", null);
    }
}
