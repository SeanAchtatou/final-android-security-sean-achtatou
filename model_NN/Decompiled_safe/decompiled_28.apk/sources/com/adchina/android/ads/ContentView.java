package com.adchina.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import java.util.Timer;
import java.util.TimerTask;

public final class ContentView extends LinearLayout implements Handler.Callback {
    private static final int KDURATION = 750;
    /* access modifiers changed from: private */
    public AdEngine mAdEngine = null;
    private int mBackgroundColor = -16777216;
    private Bitmap mBmp = null;
    private Typeface mFont = null;
    private GifEngine mGifEngine = null;
    /* access modifiers changed from: private */
    public Handler mGifMsgHandler = null;
    private Timer mGifTimer = null;
    /* access modifiers changed from: private */
    public ImageView mImageView = null;
    private Matrix mMatrix = new Matrix();
    private Paint mPaint = null;
    private int mPenColor = -1;
    private StringBuffer mTxtLnkText = new StringBuffer();
    private EMaterialType mType = EMaterialType.ENONE;

    enum EMaterialType {
        ETXT,
        EIMG,
        EGIF,
        ENONE
    }

    public ContentView(Context context) {
        super(context);
        init(context);
    }

    public ContentView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.mFont = Typeface.create("宋体", 1);
        this.mGifMsgHandler = new Handler(this);
        this.mImageView = new ImageView(context);
        this.mImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        addView(this.mImageView, new LinearLayout.LayoutParams(-1, -2, 0.0f));
        this.mImageView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                if (ContentView.this.mAdEngine == null) {
                    return false;
                }
                ContentView.this.mAdEngine.onTouchEvent(arg1);
                return false;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void setAdEngine(AdEngine adEngine) {
        this.mAdEngine = adEngine;
    }

    /* access modifiers changed from: protected */
    public void setDefaultImage(Bitmap aDefImage) {
        cancelGifUpdateTimer();
        this.mBmp = aDefImage;
        this.mType = EMaterialType.EIMG;
        applyRotation(this.mBmp);
    }

    /* access modifiers changed from: protected */
    public void setContent(Bitmap bmp) {
        cancelGifUpdateTimer();
        this.mBmp = bmp;
        this.mType = EMaterialType.EIMG;
        this.mImageView.setImageBitmap(this.mBmp);
        applyRotation(this.mBmp);
    }

    /* access modifiers changed from: protected */
    public void setContent(String txt) {
        cancelGifUpdateTimer();
        this.mTxtLnkText.setLength(0);
        this.mTxtLnkText.append(txt);
        this.mType = EMaterialType.ETXT;
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void setContent(GifEngine gif) {
        this.mGifEngine = gif;
        cancelGifUpdateTimer();
        createGifUpdateTimer();
        this.mType = EMaterialType.EGIF;
    }

    public void setPenColor(int aColor) {
        this.mPenColor = aColor;
    }

    public void setBackgroundColor(int aColor) {
        this.mBackgroundColor = aColor;
    }

    public void setFont(Typeface aFont) {
        this.mFont = aFont;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private void onDrawBitmap(Canvas canvas, Bitmap bmp) {
        int i;
        int i2 = 0;
        Rect rcView = new Rect();
        getHitRect(rcView);
        this.mMatrix.reset();
        this.mMatrix.postScale(1.0f, 1.0f);
        Bitmap newBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), this.mMatrix, true);
        int w = newBmp.getWidth();
        int h = newBmp.getHeight();
        int xOffset = (rcView.width() - w) / 2;
        int yOffset = (rcView.height() - h) / 2;
        if (xOffset > 0) {
            i = xOffset;
        } else {
            i = 0;
        }
        float f = (float) i;
        if (yOffset > 0) {
            i2 = yOffset;
        }
        canvas.drawBitmap(newBmp, f, (float) i2, (Paint) null);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.mPaint == null) {
            this.mPaint = new Paint();
        }
        Rect rcView = new Rect();
        getHitRect(rcView);
        if (EMaterialType.EIMG == this.mType) {
            this.mPaint.setColor(-16777216);
            this.mPaint.setStyle(Paint.Style.FILL);
            canvas.drawRect(rcView, this.mPaint);
            if (this.mBmp != null) {
                onDrawBitmap(canvas, this.mBmp);
            }
        } else if (EMaterialType.EGIF == this.mType) {
            this.mGifEngine.nextFrame();
            Bitmap bmp = this.mGifEngine.getImage();
            if (bmp != null) {
                onDrawBitmap(canvas, bmp);
            } else {
                AdLog.writeLog("onDraw, Gif bmp is null");
            }
        } else if (EMaterialType.ETXT == this.mType) {
            this.mPaint.setColor(this.mBackgroundColor);
            this.mPaint.setStyle(Paint.Style.FILL);
            canvas.drawRect(rcView, this.mPaint);
            this.mPaint.setColor(this.mPenColor);
            this.mPaint.setTypeface(this.mFont);
            canvas.drawText(this.mTxtLnkText.toString(), 0.0f, (float) (rcView.height() / 2), this.mPaint);
        }
    }

    public void createGifUpdateTimer() {
        this.mGifTimer = new Timer();
        this.mGifTimer.schedule(new GifTimerTask(), 0, 2000);
    }

    public void cancelGifUpdateTimer() {
        if (this.mGifTimer != null) {
            this.mGifTimer.cancel();
        }
    }

    public class GifTimerTask extends TimerTask {
        public GifTimerTask() {
        }

        public void run() {
            Message msg = new Message();
            msg.what = 1;
            ContentView.this.mGifMsgHandler.sendMessage(msg);
        }
    }

    public boolean handleMessage(Message arg0) {
        this.mBmp = this.mGifEngine.getImage();
        if (this.mBmp == null) {
            return true;
        }
        this.mImageView.setImageBitmap(this.mBmp);
        this.mGifEngine.nextFrame();
        return true;
    }

    /* access modifiers changed from: protected */
    public void applyRotation(Bitmap bmp) {
        Rotate3dAnimation rotation = new Rotate3dAnimation(0.0f, 90.0f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, 310.0f, true);
        rotation.setDuration(750);
        rotation.setFillAfter(true);
        rotation.setInterpolator(new AccelerateInterpolator());
        rotation.setAnimationListener(new DisplayNextImage(this, bmp, null));
        startAnimation(rotation);
    }

    private final class DisplayNextImage implements Animation.AnimationListener {
        private final Bitmap mNewBmp;

        /* synthetic */ DisplayNextImage(ContentView contentView, Bitmap bitmap, DisplayNextImage displayNextImage) {
            this(bitmap);
        }

        private DisplayNextImage(Bitmap bmp) {
            this.mNewBmp = bmp;
        }

        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
            ContentView.this.post(new SwapImage(this.mNewBmp));
        }

        public void onAnimationRepeat(Animation animation) {
        }
    }

    private final class SwapImage implements Runnable {
        private final Bitmap mNewBmp;

        public SwapImage(Bitmap bmp) {
            this.mNewBmp = bmp;
        }

        public void run() {
            float centerX = ((float) ContentView.this.getWidth()) / 2.0f;
            float centerY = ((float) ContentView.this.getHeight()) / 2.0f;
            ContentView.this.mImageView.setVisibility(0);
            ContentView.this.mImageView.requestFocus();
            if (this.mNewBmp != null) {
                ContentView.this.mImageView.setImageBitmap(this.mNewBmp);
            }
            Rotate3dAnimation rotation = new Rotate3dAnimation(-90.0f, 0.0f, centerX, centerY, 310.0f, false);
            rotation.setDuration(750);
            rotation.setFillAfter(true);
            rotation.setInterpolator(new DecelerateInterpolator());
            ContentView.this.startAnimation(rotation);
        }
    }
}
