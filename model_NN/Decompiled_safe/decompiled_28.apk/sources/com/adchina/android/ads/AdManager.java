package com.adchina.android.ads;

public class AdManager {
    public static final String DEFAULT_ADSPACE_ID = "69327";
    public static final String DEFAULT_FULLSCREEN_ADSPACE_ID = "70213";
    public static final String SDK_DATE = "20100926";
    public static final String SDK_NAME = "AdChina_Android_SDK";
    public static final String SDK_VERSION = "2.1.1";
    private static StringBuffer mAdspaceId = new StringBuffer();
    private static StringBuffer mBirthday = new StringBuffer("");
    private static boolean mDebugMode = false;
    private static StringBuffer mFullScreenAdspaceId = new StringBuffer();
    private static EGender mGender = EGender.EFemale;
    private static StringBuffer mPostalCode = new StringBuffer("");
    private static StringBuffer mResolution = new StringBuffer();
    private static StringBuffer mTelephoneNumber = new StringBuffer();

    public enum EGender {
        EFemale,
        EMale
    }

    public static void setDebugMode(boolean isDebugMode) {
        mDebugMode = isDebugMode;
        if (mDebugMode) {
            setAdspaceId(DEFAULT_ADSPACE_ID);
        }
    }

    public static boolean getDebugMode() {
        return mDebugMode;
    }

    public static String getAdspaceId() {
        if (mDebugMode) {
            return DEFAULT_ADSPACE_ID;
        }
        return mAdspaceId.toString();
    }

    public static void setAdspaceId(String adspaceId) {
        mAdspaceId.setLength(0);
        mAdspaceId.append(adspaceId);
    }

    public static String getFullScreenAdspaceId() {
        if (mDebugMode) {
            return DEFAULT_FULLSCREEN_ADSPACE_ID;
        }
        return mFullScreenAdspaceId.toString();
    }

    public static void setFullScreenAdspaceId(String adspaceId) {
        mFullScreenAdspaceId.setLength(0);
        mFullScreenAdspaceId.append(adspaceId);
    }

    public static String getTelephoneNumber() {
        return mTelephoneNumber.toString();
    }

    public static void setTelephoneNumber(String number) {
        mTelephoneNumber.setLength(0);
        mTelephoneNumber.append(number);
    }

    public static String getResolution() {
        return mResolution.toString();
    }

    public static void setResolution(String resolution) {
        mResolution.setLength(0);
        mResolution.append(resolution);
    }

    public static void setBirthday(String birthday) {
        mBirthday.setLength(0);
        mBirthday.append(birthday);
    }

    public static String getBirthday() {
        return mBirthday.toString();
    }

    public static void setPostalCode(String postalCode) {
        mPostalCode.setLength(0);
        mPostalCode.append(postalCode);
    }

    public static String getPostalCode() {
        return mPostalCode.toString();
    }

    public static void setGender(EGender gender) {
        mGender = gender;
    }

    public static EGender getGender() {
        return mGender;
    }
}
