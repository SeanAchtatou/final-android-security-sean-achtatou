package com.adwo.adsdk;

import android.content.Context;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.adwo.adsdk.f  reason: case insensitive filesystem */
final class C0009f {
    Context a;
    protected int b = -1;
    protected String c = null;
    protected String d = null;
    protected String e = null;
    protected byte f;
    protected List g = new ArrayList();
    protected List h = new ArrayList();
    protected String i = null;
    C0011h j;

    public static C0009f a(Context context, byte[] bArr) {
        C0009f b2 = O.b(bArr);
        if (b2 == null) {
            return null;
        }
        b2.a = context;
        if (b2.c == null && b2.e == null) {
            return null;
        }
        if (b2.c != null && b2.c.length() == 0) {
            return null;
        }
        if (b2.e != null && b2.e.length() == 0) {
            return null;
        }
        Log.d("Adwo SDK", "Get an ad from Adwo servers.");
        return b2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.d != null) {
            new C0010g(this).start();
        }
    }

    public final String b() {
        return this.c;
    }

    public final String c() {
        return this.e;
    }

    public final String toString() {
        return this.c;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof C0009f)) {
            return false;
        }
        C0009f fVar = (C0009f) obj;
        if (this.c != null && fVar.c != null && this.c.equals(fVar.c)) {
            return true;
        }
        if (this.e != null && fVar.e != null && this.e.equals(fVar.e)) {
            return true;
        }
        if (this.i == null || fVar.i == null || !this.i.equals(fVar.i)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return toString().hashCode();
    }
}
