package com.adwo.adsdk;

/* renamed from: com.adwo.adsdk.g  reason: case insensitive filesystem */
final class C0010g extends Thread {
    private /* synthetic */ C0009f a;

    C0010g(C0009f fVar) {
        this.a = fVar;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0073 A[SYNTHETIC, Splitter:B:16:0x0073] */
    /* JADX WARNING: Removed duplicated region for block: B:192:? A[ADDED_TO_REGION, ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0167 A[SYNTHETIC, Splitter:B:55:0x0167] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x01bb A[SYNTHETIC, Splitter:B:84:0x01bb] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r14 = this;
            r9 = 0
            r8 = 0
            java.lang.String r12 = "Adwo SDK"
            java.lang.String r11 = "|"
            java.lang.String r10 = "android.intent.action.VIEW"
            com.adwo.adsdk.f r0 = r14.a
            byte r0 = r0.f
            com.adwo.adsdk.f r1 = r14.a     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            com.adwo.adsdk.h r1 = r1.j     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            if (r1 == 0) goto L_0x0019
            com.adwo.adsdk.f r1 = r14.a     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            com.adwo.adsdk.h r1 = r1.j     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            r1.f()     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
        L_0x0019:
            com.adwo.adsdk.f r1 = r14.a     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.lang.String r1 = r1.d     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            com.adwo.adsdk.f r2 = r14.a     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.lang.String r2 = r2.d     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.lang.String r3 = "clk?p0"
            int r2 = r2.indexOf(r3)     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            if (r2 <= 0) goto L_0x0054
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            com.adwo.adsdk.f r2 = r14.a     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.lang.String r2 = r2.d     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            r1.<init>(r2)     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.lang.String r2 = "&p1="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.lang.String r3 = "yyyyMMddHHmmss"
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.util.Date r3 = new java.util.Date     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            r3.<init>()     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.lang.String r2 = r2.format(r3)     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.lang.String r1 = r1.toString()     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
        L_0x0054:
            java.net.URL r2 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            r2.<init>(r1)     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            int r1 = com.adwo.adsdk.C0012i.f     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            int r3 = com.adwo.adsdk.C0012i.b     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            if (r1 != r3) goto L_0x00c4
            java.net.URLConnection r1 = r2.openConnection()     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            int r3 = com.adwo.adsdk.P.b     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0140 }
            r1.setConnectTimeout(r3)     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0140 }
            int r3 = com.adwo.adsdk.P.b     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0140 }
            r1.setReadTimeout(r3)     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0140 }
        L_0x006d:
            r1.connect()     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0140 }
        L_0x0070:
            r3 = 1
            if (r0 != r3) goto L_0x0167
            java.lang.String r1 = r2.toString()     // Catch:{ Exception -> 0x015c }
        L_0x0077:
            com.adwo.adsdk.f r2 = r14.a
            com.adwo.adsdk.h r2 = r2.j
            if (r2 == 0) goto L_0x0084
            com.adwo.adsdk.f r2 = r14.a
            com.adwo.adsdk.h r2 = r2.j
            r2.e()
        L_0x0084:
            if (r1 == 0) goto L_0x00a8
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            switch(r0) {
                case 1: goto L_0x01bf;
                case 2: goto L_0x01bf;
                case 3: goto L_0x0227;
                case 4: goto L_0x0227;
                case 5: goto L_0x0227;
                case 6: goto L_0x0227;
                case 7: goto L_0x0227;
                case 8: goto L_0x0241;
                case 9: goto L_0x0266;
                case 10: goto L_0x02be;
                case 11: goto L_0x008e;
                case 12: goto L_0x02d6;
                case 13: goto L_0x0318;
                case 14: goto L_0x038a;
                case 15: goto L_0x03aa;
                default: goto L_0x008e;
            }
        L_0x008e:
            r0 = r2
        L_0x008f:
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            r0.addFlags(r2)
            com.adwo.adsdk.f r2 = r14.a     // Catch:{ ActivityNotFoundException -> 0x03d4 }
            android.content.Context r2 = r2.a     // Catch:{ ActivityNotFoundException -> 0x03d4 }
            r2.startActivity(r0)     // Catch:{ ActivityNotFoundException -> 0x03d4 }
            java.util.Set r0 = com.adwo.adsdk.C0012i.h     // Catch:{ ActivityNotFoundException -> 0x03d4 }
            com.adwo.adsdk.f r2 = r14.a     // Catch:{ ActivityNotFoundException -> 0x03d4 }
            int r2 = r2.b     // Catch:{ ActivityNotFoundException -> 0x03d4 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ ActivityNotFoundException -> 0x03d4 }
            r0.add(r2)     // Catch:{ ActivityNotFoundException -> 0x03d4 }
        L_0x00a8:
            com.adwo.adsdk.f r0 = r14.a
            java.util.List r0 = r0.g
            if (r0 == 0) goto L_0x00c3
            com.adwo.adsdk.f r0 = r14.a
            java.util.List r0 = r0.g
            int r0 = r0.size()
            if (r0 == 0) goto L_0x00c3
            r1 = r9
        L_0x00b9:
            com.adwo.adsdk.f r0 = r14.a
            java.util.List r0 = r0.g
            int r0 = r0.size()
            if (r1 < r0) goto L_0x041f
        L_0x00c3:
            return
        L_0x00c4:
            int r1 = com.adwo.adsdk.C0012i.f     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            int r3 = com.adwo.adsdk.C0012i.c     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            if (r1 == r3) goto L_0x00d0
            int r1 = com.adwo.adsdk.C0012i.f     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            int r3 = com.adwo.adsdk.C0012i.d     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            if (r1 != r3) goto L_0x0113
        L_0x00d0:
            java.net.Proxy r1 = new java.net.Proxy     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.net.Proxy$Type r3 = java.net.Proxy.Type.HTTP     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.net.InetSocketAddress r4 = new java.net.InetSocketAddress     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.lang.String r5 = new java.lang.String     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            byte[] r6 = com.adwo.adsdk.P.j     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.lang.String r7 = "utf-8"
            r5.<init>(r6, r7)     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            r6 = 80
            r4.<init>(r5, r6)     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            r1.<init>(r3, r4)     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.net.URLConnection r1 = r2.openConnection(r1)     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            int r3 = com.adwo.adsdk.P.b     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0140 }
            r1.setConnectTimeout(r3)     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0140 }
            int r3 = com.adwo.adsdk.P.b     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0140 }
            r1.setReadTimeout(r3)     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0140 }
            goto L_0x006d
        L_0x00f7:
            r2 = move-exception
        L_0x00f8:
            java.lang.String r2 = "Adwo SDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Malformed click URL.Will try to follow anyway."
            r2.<init>(r3)
            com.adwo.adsdk.f r3 = r14.a
            java.lang.String r3 = r3.d
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.util.Log.w(r12, r2)
            r2 = r8
            goto L_0x0070
        L_0x0113:
            int r1 = com.adwo.adsdk.C0012i.f     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            int r3 = com.adwo.adsdk.C0012i.e     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            if (r1 != r3) goto L_0x0459
            java.net.Proxy r1 = new java.net.Proxy     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.net.Proxy$Type r3 = java.net.Proxy.Type.HTTP     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.net.InetSocketAddress r4 = new java.net.InetSocketAddress     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.lang.String r5 = new java.lang.String     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            byte[] r6 = com.adwo.adsdk.P.i     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.lang.String r7 = "utf-8"
            r5.<init>(r6, r7)     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            r6 = 80
            r4.<init>(r5, r6)     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            r1.<init>(r3, r4)     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            java.net.URLConnection r1 = r2.openConnection(r1)     // Catch:{ MalformedURLException -> 0x0443, IOException -> 0x043f }
            int r3 = com.adwo.adsdk.P.b     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0140 }
            r1.setConnectTimeout(r3)     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0140 }
            int r3 = com.adwo.adsdk.P.b     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0140 }
            r1.setReadTimeout(r3)     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0140 }
            goto L_0x006d
        L_0x0140:
            r2 = move-exception
        L_0x0141:
            java.lang.String r2 = "Adwo SDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Could not determine final click destination URL.  Will try to follow anyway.  "
            r2.<init>(r3)
            com.adwo.adsdk.f r3 = r14.a
            java.lang.String r3 = r3.d
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.util.Log.w(r12, r2)
            r2 = r8
            goto L_0x0070
        L_0x015c:
            r1 = move-exception
            java.lang.String r1 = "Adwo SDK"
            java.lang.String r1 = "Could not get ad click url from  server."
            android.util.Log.e(r12, r1)
            r1 = r8
            goto L_0x0077
        L_0x0167:
            java.io.InputStream r2 = r1.getInputStream()     // Catch:{ Exception -> 0x043b, all -> 0x01b7 }
            if (r2 == 0) goto L_0x0456
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0193 }
            r3.<init>()     // Catch:{ Exception -> 0x0193 }
        L_0x0172:
            int r4 = r2.read()     // Catch:{ Exception -> 0x0193 }
            r5 = -1
            if (r4 != r5) goto L_0x018f
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x0193 }
            byte[] r3 = r3.toByteArray()     // Catch:{ Exception -> 0x0193 }
            java.lang.String r5 = "UTF-8"
            r4.<init>(r3, r5)     // Catch:{ Exception -> 0x0193 }
            r1 = r4
        L_0x0185:
            if (r2 == 0) goto L_0x0077
            r2.close()     // Catch:{ Exception -> 0x018c }
            goto L_0x0077
        L_0x018c:
            r2 = move-exception
            goto L_0x0077
        L_0x018f:
            r3.write(r4)     // Catch:{ Exception -> 0x0193 }
            goto L_0x0172
        L_0x0193:
            r3 = move-exception
        L_0x0194:
            java.lang.String r3 = "Adwo SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0437 }
            java.lang.String r5 = "Connection off "
            r4.<init>(r5)     // Catch:{ all -> 0x0437 }
            r5 = 0
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0437 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0437 }
            android.util.Log.e(r3, r4)     // Catch:{ all -> 0x0437 }
            if (r2 == 0) goto L_0x01ae
            r2.close()     // Catch:{ Exception -> 0x01b3 }
        L_0x01ae:
            if (r1 == 0) goto L_0x0453
            r1 = r8
            goto L_0x0077
        L_0x01b3:
            r1 = move-exception
            r1 = r8
            goto L_0x0077
        L_0x01b7:
            r0 = move-exception
            r1 = r8
        L_0x01b9:
            if (r1 == 0) goto L_0x01be
            r1.close()     // Catch:{ Exception -> 0x0431 }
        L_0x01be:
            throw r0
        L_0x01bf:
            com.adwo.adsdk.f r0 = r14.a     // Catch:{ ActivityNotFoundException -> 0x01f5 }
            android.content.Context r0 = r0.a     // Catch:{ ActivityNotFoundException -> 0x01f5 }
            java.lang.Class<com.adwo.adsdk.AdwoAdBrowserActivity> r3 = com.adwo.adsdk.AdwoAdBrowserActivity.class
            r2.setClass(r0, r3)     // Catch:{ ActivityNotFoundException -> 0x01f5 }
            java.lang.String r0 = "android.intent.action.VIEW"
            r2.setAction(r0)     // Catch:{ ActivityNotFoundException -> 0x01f5 }
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            r2.addFlags(r0)     // Catch:{ ActivityNotFoundException -> 0x01f5 }
            java.lang.String r0 = "url"
            r2.putExtra(r0, r1)     // Catch:{ ActivityNotFoundException -> 0x01f5 }
        L_0x01d7:
            com.adwo.adsdk.f r0 = r14.a
            java.util.List r0 = r0.g
            if (r0 == 0) goto L_0x0450
            com.adwo.adsdk.f r0 = r14.a
            java.util.List r0 = r0.g
            int r0 = r0.size()
            if (r0 == 0) goto L_0x0450
            r3 = r9
        L_0x01e8:
            com.adwo.adsdk.f r0 = r14.a
            java.util.List r0 = r0.g
            int r0 = r0.size()
            if (r3 < r0) goto L_0x0216
            r0 = r2
            goto L_0x008f
        L_0x01f5:
            r0 = move-exception
            android.content.Intent r0 = new android.content.Intent
            r0.<init>()
            java.lang.String r2 = "android.intent.action.VIEW"
            r0.setAction(r2)     // Catch:{ Exception -> 0x0213 }
            android.net.Uri r2 = android.net.Uri.parse(r1)     // Catch:{ Exception -> 0x0213 }
            r0.setData(r2)     // Catch:{ Exception -> 0x0213 }
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            r0.addFlags(r2)     // Catch:{ Exception -> 0x0213 }
            java.lang.String r2 = "CONFIGURATION ERROR:  com.adwo.adsdk.AdwoAdBrowserActivity must be registered in your AndroidManifest.xml file. "
            com.adwo.adsdk.C0012i.a(r2)     // Catch:{ Exception -> 0x0213 }
            r2 = r0
            goto L_0x01d7
        L_0x0213:
            r2 = move-exception
            r2 = r0
            goto L_0x01d7
        L_0x0216:
            com.adwo.adsdk.f r0 = r14.a
            java.util.List r0 = r0.g
            java.lang.Object r0 = r0.get(r3)
            java.lang.String r0 = (java.lang.String) r0
            com.adwo.adsdk.C0012i.c(r0)
            int r0 = r3 + 1
            r3 = r0
            goto L_0x01e8
        L_0x0227:
            java.lang.String r0 = r1.trim()     // Catch:{ NullPointerException -> 0x023a }
            java.lang.String r3 = "android.intent.action.VIEW"
            r2.setAction(r3)     // Catch:{ NullPointerException -> 0x023a }
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ NullPointerException -> 0x023a }
            r2.setData(r0)     // Catch:{ NullPointerException -> 0x023a }
            r0 = r2
            goto L_0x008f
        L_0x023a:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x008f
        L_0x0241:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x025f }
            java.lang.String r3 = "tel:"
            r0.<init>(r3)     // Catch:{ Exception -> 0x025f }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x025f }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x025f }
            java.lang.String r3 = "android.intent.action.DIAL"
            r2.setAction(r3)     // Catch:{ Exception -> 0x025f }
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x025f }
            r2.setData(r0)     // Catch:{ Exception -> 0x025f }
            r0 = r2
            goto L_0x008f
        L_0x025f:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x008f
        L_0x0266:
            java.lang.String r0 = "|"
            boolean r0 = r1.contains(r11)
            if (r0 == 0) goto L_0x02b3
            java.lang.String r0 = "|"
            int r0 = r1.indexOf(r11)
            java.lang.String r3 = r1.substring(r9, r0)
            int r0 = r0 + 1
            java.lang.String r0 = r1.substring(r0)
        L_0x027e:
            java.lang.String r4 = "android.intent.action.VIEW"
            r2.setAction(r4)     // Catch:{ Exception -> 0x02b7 }
            java.lang.String r4 = "com.google.android.apps.maps"
            java.lang.String r5 = "com.google.android.maps.MapsActivity"
            r2.setClassName(r4, r5)     // Catch:{ Exception -> 0x02b7 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02b7 }
            java.lang.String r5 = "http://maps.google.com/maps?q="
            r4.<init>(r5)     // Catch:{ Exception -> 0x02b7 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x02b7 }
            java.lang.String r4 = "("
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x02b7 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x02b7 }
            java.lang.String r3 = ")&z=22"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x02b7 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x02b7 }
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x02b7 }
            r2.setData(r0)     // Catch:{ Exception -> 0x02b7 }
            r0 = r2
            goto L_0x008f
        L_0x02b3:
            java.lang.String r0 = ""
            r3 = r1
            goto L_0x027e
        L_0x02b7:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x008f
        L_0x02be:
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Exception -> 0x0434 }
            java.lang.String r3 = "android.intent.action.WEB_SEARCH"
            r0.<init>(r3)     // Catch:{ Exception -> 0x0434 }
            java.lang.String r2 = "query"
            r0.putExtra(r2, r1)     // Catch:{ Exception -> 0x02cc }
            goto L_0x008f
        L_0x02cc:
            r2 = move-exception
            r13 = r2
            r2 = r0
            r0 = r13
        L_0x02d0:
            r0.printStackTrace()
            r0 = r2
            goto L_0x008f
        L_0x02d6:
            java.lang.String r0 = "|"
            boolean r0 = r1.contains(r11)
            if (r0 == 0) goto L_0x044c
            java.lang.String r0 = "|"
            int r0 = r1.indexOf(r11)
            java.lang.String r3 = r1.substring(r9, r0)
            int r0 = r0 + 1
            java.lang.String r0 = r1.substring(r0)
        L_0x02ee:
            java.lang.String r4 = "android.intent.action.SENDTO"
            r2.setAction(r4)     // Catch:{ Exception -> 0x0311 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0311 }
            java.lang.String r5 = "smsto:"
            r4.<init>(r5)     // Catch:{ Exception -> 0x0311 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x0311 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0311 }
            android.net.Uri r3 = android.net.Uri.parse(r3)     // Catch:{ Exception -> 0x0311 }
            r2.setData(r3)     // Catch:{ Exception -> 0x0311 }
            java.lang.String r3 = "sms_body"
            r2.putExtra(r3, r0)     // Catch:{ Exception -> 0x0311 }
            r0 = r2
            goto L_0x008f
        L_0x0311:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x008f
        L_0x0318:
            java.lang.String r0 = "android.intent.action.SEND"
            r2.setAction(r0)     // Catch:{ Exception -> 0x0383 }
            java.lang.String r0 = "|"
            boolean r0 = r1.contains(r0)     // Catch:{ Exception -> 0x0383 }
            if (r0 == 0) goto L_0x0447
            r0 = 3
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x0383 }
            java.lang.String r3 = "|"
            int r3 = r1.indexOf(r3)     // Catch:{ Exception -> 0x0383 }
            r4 = 0
            r5 = 0
            java.lang.String r5 = r1.substring(r5, r3)     // Catch:{ Exception -> 0x0383 }
            r0[r4] = r5     // Catch:{ Exception -> 0x0383 }
            int r3 = r3 + 1
            java.lang.String r3 = r1.substring(r3)     // Catch:{ Exception -> 0x0383 }
            java.lang.String r4 = "|"
            int r4 = r3.indexOf(r4)     // Catch:{ Exception -> 0x0383 }
            r5 = 1
            r6 = 0
            java.lang.String r6 = r3.substring(r6, r4)     // Catch:{ Exception -> 0x0383 }
            r0[r5] = r6     // Catch:{ Exception -> 0x0383 }
            r5 = 2
            int r4 = r4 + 1
            java.lang.String r3 = r3.substring(r4)     // Catch:{ Exception -> 0x0383 }
            r0[r5] = r3     // Catch:{ Exception -> 0x0383 }
            r3 = 0
            r3 = r0[r3]     // Catch:{ Exception -> 0x0383 }
            r4 = 1
            r4 = r0[r4]     // Catch:{ Exception -> 0x0383 }
            r5 = 2
            r0 = r0[r5]     // Catch:{ Exception -> 0x0383 }
            r13 = r4
            r4 = r0
            r0 = r13
        L_0x035f:
            android.net.Uri r5 = android.net.Uri.parse(r3)     // Catch:{ Exception -> 0x0383 }
            r2.setData(r5)     // Catch:{ Exception -> 0x0383 }
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ Exception -> 0x0383 }
            r6 = 0
            r5[r6] = r3     // Catch:{ Exception -> 0x0383 }
            java.lang.String r3 = "android.intent.extra.EMAIL"
            r2.putExtra(r3, r5)     // Catch:{ Exception -> 0x0383 }
            java.lang.String r3 = "android.intent.extra.TEXT"
            r2.putExtra(r3, r4)     // Catch:{ Exception -> 0x0383 }
            java.lang.String r3 = "android.intent.extra.SUBJECT"
            r2.putExtra(r3, r0)     // Catch:{ Exception -> 0x0383 }
            java.lang.String r0 = "message/rfc882"
            r2.setType(r0)     // Catch:{ Exception -> 0x0383 }
            r0 = r2
            goto L_0x008f
        L_0x0383:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x008f
        L_0x038a:
            java.lang.String r0 = ".mp3"
            boolean r0 = r1.endsWith(r0)
            if (r0 != 0) goto L_0x039a
            java.lang.String r0 = ".wav"
            boolean r0 = r1.endsWith(r0)
            if (r0 == 0) goto L_0x03aa
        L_0x039a:
            java.lang.String r0 = "android.intent.action.VIEW"
            r2.setAction(r10)
            java.lang.String r0 = com.adwo.adsdk.P.a(r1)
            android.net.Uri r3 = android.net.Uri.parse(r1)
            r2.setDataAndType(r3, r0)
        L_0x03aa:
            java.lang.String r0 = ".3gp"
            boolean r0 = r1.endsWith(r0)
            if (r0 != 0) goto L_0x03c2
            java.lang.String r0 = ".mp4"
            boolean r0 = r1.endsWith(r0)
            if (r0 != 0) goto L_0x03c2
            java.lang.String r0 = ".mpeg"
            boolean r0 = r1.endsWith(r0)
            if (r0 == 0) goto L_0x008e
        L_0x03c2:
            java.lang.String r0 = "android.intent.action.VIEW"
            r2.setAction(r10)
            java.lang.String r0 = com.adwo.adsdk.P.a(r1)
            android.net.Uri r3 = android.net.Uri.parse(r1)
            r2.setDataAndType(r3, r0)
            goto L_0x008e
        L_0x03d4:
            r0 = move-exception
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            java.lang.String r3 = "android.intent.action.VIEW"
            r2.setAction(r3)     // Catch:{ NullPointerException -> 0x0415, ActivityNotFoundException -> 0x041a }
            android.net.Uri r3 = android.net.Uri.parse(r1)     // Catch:{ NullPointerException -> 0x0415, ActivityNotFoundException -> 0x041a }
            r2.setData(r3)     // Catch:{ NullPointerException -> 0x0415, ActivityNotFoundException -> 0x041a }
            r3 = 268435456(0x10000000, float:2.5243549E-29)
            r2.addFlags(r3)     // Catch:{ NullPointerException -> 0x0415, ActivityNotFoundException -> 0x041a }
            com.adwo.adsdk.f r3 = r14.a     // Catch:{ NullPointerException -> 0x0415, ActivityNotFoundException -> 0x041a }
            android.content.Context r3 = r3.a     // Catch:{ NullPointerException -> 0x0415, ActivityNotFoundException -> 0x041a }
            r3.startActivity(r2)     // Catch:{ NullPointerException -> 0x0415, ActivityNotFoundException -> 0x041a }
            java.util.Set r2 = com.adwo.adsdk.C0012i.h     // Catch:{ NullPointerException -> 0x0415, ActivityNotFoundException -> 0x041a }
            com.adwo.adsdk.f r3 = r14.a     // Catch:{ NullPointerException -> 0x0415, ActivityNotFoundException -> 0x041a }
            int r3 = r3.b     // Catch:{ NullPointerException -> 0x0415, ActivityNotFoundException -> 0x041a }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ NullPointerException -> 0x0415, ActivityNotFoundException -> 0x041a }
            r2.add(r3)     // Catch:{ NullPointerException -> 0x0415, ActivityNotFoundException -> 0x041a }
        L_0x03ff:
            java.lang.String r2 = "Adwo SDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Could not intent to "
            r2.<init>(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r12, r1, r0)
            goto L_0x00a8
        L_0x0415:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x03ff
        L_0x041a:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x03ff
        L_0x041f:
            com.adwo.adsdk.f r0 = r14.a
            java.util.List r0 = r0.g
            java.lang.Object r0 = r0.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            com.adwo.adsdk.C0012i.c(r0)
            int r0 = r1 + 1
            r1 = r0
            goto L_0x00b9
        L_0x0431:
            r1 = move-exception
            goto L_0x01be
        L_0x0434:
            r0 = move-exception
            goto L_0x02d0
        L_0x0437:
            r0 = move-exception
            r1 = r2
            goto L_0x01b9
        L_0x043b:
            r2 = move-exception
            r2 = r8
            goto L_0x0194
        L_0x043f:
            r1 = move-exception
            r1 = r8
            goto L_0x0141
        L_0x0443:
            r1 = move-exception
            r1 = r8
            goto L_0x00f8
        L_0x0447:
            r0 = r8
            r3 = r8
            r4 = r8
            goto L_0x035f
        L_0x044c:
            r0 = r8
            r3 = r8
            goto L_0x02ee
        L_0x0450:
            r0 = r2
            goto L_0x008f
        L_0x0453:
            r1 = r8
            goto L_0x0077
        L_0x0456:
            r1 = r8
            goto L_0x0185
        L_0x0459:
            r1 = r8
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0010g.run():void");
    }
}
