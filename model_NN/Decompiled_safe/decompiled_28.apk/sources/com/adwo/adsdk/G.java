package com.adwo.adsdk;

import android.util.Log;

final class G implements Runnable {
    private /* synthetic */ F a;
    private final /* synthetic */ C0005b b;
    private final /* synthetic */ int c;
    private final /* synthetic */ boolean d;

    G(F f, C0005b bVar, int i, boolean z) {
        this.a = f;
        this.b = bVar;
        this.c = i;
        this.d = z;
    }

    public final void run() {
        try {
            this.a.a.addView(this.b);
            if (this.c == 0) {
                this.b.b();
                if (this.d) {
                    AdwoAdView.b(this.a.a, this.b);
                } else {
                    AdwoAdView.c(this.a.a, this.b);
                }
            } else {
                this.a.a.c = this.b;
            }
        } catch (Exception e) {
            Log.e("Adwo SDK", e.toString());
        } finally {
            this.a.a.a = false;
        }
    }
}
