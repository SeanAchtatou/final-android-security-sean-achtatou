package com.adwo.adsdk;

import android.content.Context;
import android.os.AsyncTask;

final class R extends AsyncTask {
    private byte a = 0;
    private boolean b;
    private /* synthetic */ FSAdUtil c;

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        Context[] contextArr = (Context[]) objArr;
        FSAd a2 = this.b ? FSAdUtil.a(contextArr[0], this.a, (byte) 3) : FSAdUtil.a(contextArr[0], this.a, (byte) 19);
        if (a2 == null) {
            if (this.c.z != null) {
                this.c.z.onFailedToReceiveAd();
            }
            return false;
        }
        if (this.c.z != null) {
            this.c.z.onReceiveAd(a2);
        }
        return true;
    }

    public R(FSAdUtil fSAdUtil, byte b2, boolean z) {
        this.c = fSAdUtil;
        this.a = b2;
        this.b = z;
    }
}
