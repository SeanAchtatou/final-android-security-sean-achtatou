package com.adwo.adsdk;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Set;

/* renamed from: com.adwo.adsdk.i  reason: case insensitive filesystem */
public final class C0012i {
    private static byte[] A = null;
    private static String B = null;
    private static int C = 0;
    protected static String a = null;
    protected static int b = 1;
    protected static int c = 2;
    protected static int d = 3;
    protected static int e = 4;
    protected static volatile int f = 1;
    protected static String g = "";
    protected static Set h = new HashSet();
    private static TelephonyManager i;
    private static boolean j = false;
    private static byte[] k = null;
    private static byte[] l = null;
    private static String m;
    private static String n = null;
    private static Context o = null;
    private static volatile boolean p = false;
    private static byte q = 0;
    private static byte r = 0;
    private static byte[] s = null;
    private static byte[] t = null;
    private static byte u = 0;
    private static short v = 0;
    private static byte[] w = null;
    private static byte x = 2;
    private static byte y = 2;
    private static byte z = 2;

    protected static String a(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                return applicationInfo.metaData.getString("Adwo_PID");
            }
            return null;
        } catch (Exception e2) {
            Log.e("Adwo SDK", "Could not read Adwo_PID meta-data from AndroidManifest.xml.");
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x003a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void b(android.content.Context r10) {
        /*
            r9 = 6
            r8 = 9
            r7 = 3
            r6 = 0
            r5 = 1
            com.adwo.adsdk.C0012i.o = r10
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r10.getSystemService(r0)
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
            com.adwo.adsdk.C0012i.i = r0
            c(r10)
            android.content.res.Resources r0 = r10.getResources()
            android.content.res.Configuration r0 = r0.getConfiguration()
            java.util.Locale r0 = r0.locale
            java.lang.String r0 = r0.getLanguage()
            java.lang.String r1 = "en"
            boolean r1 = r0.contains(r1)
            if (r1 != 0) goto L_0x0196
            java.lang.String r1 = "zh"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x013d
            r0 = r6
        L_0x0034:
            com.adwo.adsdk.C0012i.x = r0
            java.lang.String r0 = com.adwo.adsdk.C0012i.n
            if (r0 != 0) goto L_0x0048
            android.telephony.TelephonyManager r0 = com.adwo.adsdk.C0012i.i
            if (r0 == 0) goto L_0x0199
            android.telephony.TelephonyManager r0 = com.adwo.adsdk.C0012i.i
            java.lang.String r0 = r0.getSimSerialNumber()
            com.adwo.adsdk.C0012i.m = r0
        L_0x0046:
            com.adwo.adsdk.C0012i.n = r0
        L_0x0048:
            byte[] r0 = com.adwo.adsdk.C0012i.A
            if (r0 != 0) goto L_0x004f
            a()
        L_0x004f:
            java.lang.String r0 = r10.getPackageName()
            java.lang.String r1 = "UTF-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ UnsupportedEncodingException -> 0x019c }
            com.adwo.adsdk.C0012i.w = r0     // Catch:{ UnsupportedEncodingException -> 0x019c }
        L_0x005b:
            d(r10)
            java.util.Properties r0 = java.lang.System.getProperties()
            java.lang.String r1 = "os.version"
            java.lang.Object r10 = r0.get(r1)
            java.lang.String r10 = (java.lang.String) r10
            if (r10 == 0) goto L_0x0082
            int r0 = r10.length()
            if (r0 < r7) goto L_0x0082
            java.lang.String r0 = r10.substring(r6, r7)
            r1 = 4621819117588971520(0x4024000000000000, double:10.0)
            double r3 = java.lang.Double.parseDouble(r0)     // Catch:{ Exception -> 0x09c0 }
            double r0 = r1 * r3
            int r0 = (int) r0     // Catch:{ Exception -> 0x09c0 }
            byte r0 = (byte) r0     // Catch:{ Exception -> 0x09c0 }
            com.adwo.adsdk.C0012i.u = r0     // Catch:{ Exception -> 0x09c0 }
        L_0x0082:
            java.lang.Class<android.os.Build> r0 = android.os.Build.class
            java.lang.String r1 = android.os.Build.MODEL     // Catch:{ SecurityException -> 0x09bd, NoSuchFieldException -> 0x09ba, IllegalArgumentException -> 0x09b7, IllegalAccessException -> 0x09b4, UnsupportedEncodingException -> 0x09b1 }
            java.lang.String r1 = r1.toLowerCase()     // Catch:{ SecurityException -> 0x09bd, NoSuchFieldException -> 0x09ba, IllegalArgumentException -> 0x09b7, IllegalAccessException -> 0x09b4, UnsupportedEncodingException -> 0x09b1 }
            java.lang.String r2 = "UTF-8"
            byte[] r1 = r1.getBytes(r2)     // Catch:{ SecurityException -> 0x09bd, NoSuchFieldException -> 0x09ba, IllegalArgumentException -> 0x09b7, IllegalAccessException -> 0x09b4, UnsupportedEncodingException -> 0x09b1 }
            com.adwo.adsdk.C0012i.t = r1     // Catch:{ SecurityException -> 0x09bd, NoSuchFieldException -> 0x09ba, IllegalArgumentException -> 0x09b7, IllegalAccessException -> 0x09b4, UnsupportedEncodingException -> 0x09b1 }
            java.lang.String r1 = "MANUFACTURER"
            java.lang.reflect.Field r0 = r0.getField(r1)     // Catch:{ SecurityException -> 0x09bd, NoSuchFieldException -> 0x09ba, IllegalArgumentException -> 0x09b7, IllegalAccessException -> 0x09b4, UnsupportedEncodingException -> 0x09b1 }
            android.os.Build r1 = new android.os.Build     // Catch:{ SecurityException -> 0x09bd, NoSuchFieldException -> 0x09ba, IllegalArgumentException -> 0x09b7, IllegalAccessException -> 0x09b4, UnsupportedEncodingException -> 0x09b1 }
            r1.<init>()     // Catch:{ SecurityException -> 0x09bd, NoSuchFieldException -> 0x09ba, IllegalArgumentException -> 0x09b7, IllegalAccessException -> 0x09b4, UnsupportedEncodingException -> 0x09b1 }
            java.lang.Object r10 = r0.get(r1)     // Catch:{ SecurityException -> 0x09bd, NoSuchFieldException -> 0x09ba, IllegalArgumentException -> 0x09b7, IllegalAccessException -> 0x09b4, UnsupportedEncodingException -> 0x09b1 }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ SecurityException -> 0x09bd, NoSuchFieldException -> 0x09ba, IllegalArgumentException -> 0x09b7, IllegalAccessException -> 0x09b4, UnsupportedEncodingException -> 0x09b1 }
            if (r10 == 0) goto L_0x00b7
            int r0 = r10.length()     // Catch:{ SecurityException -> 0x09bd, NoSuchFieldException -> 0x09ba, IllegalArgumentException -> 0x09b7, IllegalAccessException -> 0x09b4, UnsupportedEncodingException -> 0x09b1 }
            if (r0 <= 0) goto L_0x00b7
            java.lang.String r0 = r10.toLowerCase()     // Catch:{ SecurityException -> 0x09bd, NoSuchFieldException -> 0x09ba, IllegalArgumentException -> 0x09b7, IllegalAccessException -> 0x09b4, UnsupportedEncodingException -> 0x09b1 }
            java.lang.String r1 = "UTF-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ SecurityException -> 0x09bd, NoSuchFieldException -> 0x09ba, IllegalArgumentException -> 0x09b7, IllegalAccessException -> 0x09b4, UnsupportedEncodingException -> 0x09b1 }
            com.adwo.adsdk.C0012i.s = r0     // Catch:{ SecurityException -> 0x09bd, NoSuchFieldException -> 0x09ba, IllegalArgumentException -> 0x09b7, IllegalAccessException -> 0x09b4, UnsupportedEncodingException -> 0x09b1 }
        L_0x00b7:
            android.telephony.TelephonyManager r0 = com.adwo.adsdk.C0012i.i     // Catch:{ Exception -> 0x09ae }
            java.lang.String r0 = r0.getNetworkOperator()     // Catch:{ Exception -> 0x09ae }
            if (r0 == 0) goto L_0x00da
            int r1 = r0.length()     // Catch:{ Exception -> 0x09ae }
            r2 = 4
            if (r1 < r2) goto L_0x00da
            r1 = 0
            r2 = 3
            java.lang.String r1 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x09ae }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ Exception -> 0x09ae }
            com.adwo.adsdk.C0012i.C = r1     // Catch:{ Exception -> 0x09ae }
            r1 = 3
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x09ae }
            java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x09ae }
        L_0x00da:
            int r0 = com.adwo.adsdk.C0012i.C
            r1 = 460(0x1cc, float:6.45E-43)
            if (r0 != r1) goto L_0x013c
            java.lang.String r0 = com.adwo.adsdk.C0012i.n
            if (r0 == 0) goto L_0x013c
            java.lang.String r0 = com.adwo.adsdk.C0012i.n
            r1 = 2
            byte[] r1 = new byte[r1]
            int r2 = r0.length()
            if (r2 < r9) goto L_0x0134
            r3 = 4
            java.lang.String r3 = r0.substring(r3, r9)
            java.lang.String r4 = "0"
            boolean r4 = r3.endsWith(r4)
            if (r4 != 0) goto L_0x010c
            java.lang.String r4 = "2"
            boolean r4 = r3.endsWith(r4)
            if (r4 != 0) goto L_0x010c
            java.lang.String r4 = "7"
            boolean r4 = r3.endsWith(r4)
            if (r4 == 0) goto L_0x03f8
        L_0x010c:
            r3 = 7
            if (r2 < r3) goto L_0x0134
            r3 = 7
            java.lang.String r3 = r0.substring(r9, r3)
            java.lang.String r4 = "0"
            boolean r4 = r3.equals(r4)
            if (r4 == 0) goto L_0x01a6
            r1[r6] = r6
        L_0x011e:
            r3 = 10
            if (r2 < r3) goto L_0x0134
            r2 = 8
            r3 = 10
            java.lang.String r0 = r0.substring(r2, r3)
            java.lang.String r2 = "01"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x025e
            r1[r5] = r5
        L_0x0134:
            byte r0 = r1[r6]
            com.adwo.adsdk.C0012i.q = r0
            byte r0 = r1[r5]
            com.adwo.adsdk.C0012i.r = r0
        L_0x013c:
            return
        L_0x013d:
            java.lang.String r1 = "ko"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x0148
            r0 = 5
            goto L_0x0034
        L_0x0148:
            java.lang.String r1 = "fr"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x0153
            r0 = r7
            goto L_0x0034
        L_0x0153:
            java.lang.String r1 = "es"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x015f
            r0 = 8
            goto L_0x0034
        L_0x015f:
            java.lang.String r1 = "de"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x016a
            r0 = r9
            goto L_0x0034
        L_0x016a:
            java.lang.String r1 = "it"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x0175
            r0 = 7
            goto L_0x0034
        L_0x0175:
            java.lang.String r1 = "ja"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x0180
            r0 = 4
            goto L_0x0034
        L_0x0180:
            java.lang.String r1 = "ru"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x018b
            r0 = r8
            goto L_0x0034
        L_0x018b:
            java.lang.String r1 = "pt"
            boolean r0 = r0.contains(r1)
            if (r0 == 0) goto L_0x0196
            r0 = r5
            goto L_0x0034
        L_0x0196:
            r0 = 2
            goto L_0x0034
        L_0x0199:
            r0 = 0
            goto L_0x0046
        L_0x019c:
            r0 = move-exception
            java.lang.String r0 = "Package Name ERROR:  Incorrect application pakage name.  "
            java.lang.String r1 = "Adwo SDK"
            android.util.Log.e(r1, r0)
            goto L_0x005b
        L_0x01a6:
            java.lang.String r4 = "1"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x01b2
            r1[r6] = r5
            goto L_0x011e
        L_0x01b2:
            java.lang.String r4 = "2"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x01bf
            r3 = 2
            r1[r6] = r3
            goto L_0x011e
        L_0x01bf:
            java.lang.String r4 = "3"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x01cb
            r1[r6] = r7
            goto L_0x011e
        L_0x01cb:
            java.lang.String r4 = "4"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x01d8
            r3 = 4
            r1[r6] = r3
            goto L_0x011e
        L_0x01d8:
            java.lang.String r4 = "5"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x01e5
            r3 = 5
            r1[r6] = r3
            goto L_0x011e
        L_0x01e5:
            java.lang.String r4 = "6"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x01f1
            r1[r6] = r9
            goto L_0x011e
        L_0x01f1:
            java.lang.String r4 = "7"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x01fe
            r3 = 7
            r1[r6] = r3
            goto L_0x011e
        L_0x01fe:
            java.lang.String r4 = "8"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x020c
            r3 = 8
            r1[r6] = r3
            goto L_0x011e
        L_0x020c:
            java.lang.String r4 = "9"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0218
            r1[r6] = r8
            goto L_0x011e
        L_0x0218:
            java.lang.String r4 = "A"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0226
            r3 = 10
            r1[r6] = r3
            goto L_0x011e
        L_0x0226:
            java.lang.String r4 = "B"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0234
            r3 = 11
            r1[r6] = r3
            goto L_0x011e
        L_0x0234:
            java.lang.String r4 = "C"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0242
            r3 = 12
            r1[r6] = r3
            goto L_0x011e
        L_0x0242:
            java.lang.String r4 = "D"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0250
            r3 = 13
            r1[r6] = r3
            goto L_0x011e
        L_0x0250:
            java.lang.String r4 = "E"
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x011e
            r3 = 14
            r1[r6] = r3
            goto L_0x011e
        L_0x025e:
            java.lang.String r2 = "02"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x026a
            r1[r5] = r7
            goto L_0x0134
        L_0x026a:
            java.lang.String r2 = "03"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0276
            r1[r5] = r8
            goto L_0x0134
        L_0x0276:
            java.lang.String r2 = "04"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0284
            r0 = 12
            r1[r5] = r0
            goto L_0x0134
        L_0x0284:
            java.lang.String r2 = "05"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0292
            r0 = 8
            r1[r5] = r0
            goto L_0x0134
        L_0x0292:
            java.lang.String r2 = "06"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x029f
            r0 = 7
            r1[r5] = r0
            goto L_0x0134
        L_0x029f:
            java.lang.String r2 = "07"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x02ab
            r1[r5] = r9
            goto L_0x0134
        L_0x02ab:
            java.lang.String r2 = "08"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x02b8
            r0 = 5
            r1[r5] = r0
            goto L_0x0134
        L_0x02b8:
            java.lang.String r2 = "09"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x02c5
            r0 = 2
            r1[r5] = r0
            goto L_0x0134
        L_0x02c5:
            java.lang.String r2 = "10"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x02d3
            r0 = 14
            r1[r5] = r0
            goto L_0x0134
        L_0x02d3:
            java.lang.String r2 = "11"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x02e1
            r0 = 18
            r1[r5] = r0
            goto L_0x0134
        L_0x02e1:
            java.lang.String r2 = "12"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x02ef
            r0 = 13
            r1[r5] = r0
            goto L_0x0134
        L_0x02ef:
            java.lang.String r2 = "13"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x02fd
            r0 = 19
            r1[r5] = r0
            goto L_0x0134
        L_0x02fd:
            java.lang.String r2 = "14"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x030b
            r0 = 15
            r1[r5] = r0
            goto L_0x0134
        L_0x030b:
            java.lang.String r2 = "15"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0319
            r0 = 11
            r1[r5] = r0
            goto L_0x0134
        L_0x0319:
            java.lang.String r2 = "16"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0327
            r0 = 10
            r1[r5] = r0
            goto L_0x0134
        L_0x0327:
            java.lang.String r2 = "17"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0335
            r0 = 17
            r1[r5] = r0
            goto L_0x0134
        L_0x0335:
            java.lang.String r2 = "18"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0343
            r0 = 16
            r1[r5] = r0
            goto L_0x0134
        L_0x0343:
            java.lang.String r2 = "19"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0351
            r0 = 20
            r1[r5] = r0
            goto L_0x0134
        L_0x0351:
            java.lang.String r2 = "20"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x035f
            r0 = 29
            r1[r5] = r0
            goto L_0x0134
        L_0x035f:
            java.lang.String r2 = "21"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x036d
            r0 = 27
            r1[r5] = r0
            goto L_0x0134
        L_0x036d:
            java.lang.String r2 = "22"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x037b
            r0 = 24
            r1[r5] = r0
            goto L_0x0134
        L_0x037b:
            java.lang.String r2 = "23"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0389
            r0 = 25
            r1[r5] = r0
            goto L_0x0134
        L_0x0389:
            java.lang.String r2 = "24"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0397
            r0 = 26
            r1[r5] = r0
            goto L_0x0134
        L_0x0397:
            java.lang.String r2 = "25"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03a5
            r0 = 30
            r1[r5] = r0
            goto L_0x0134
        L_0x03a5:
            java.lang.String r2 = "26"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03b3
            r0 = 21
            r1[r5] = r0
            goto L_0x0134
        L_0x03b3:
            java.lang.String r2 = "27"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03c1
            r0 = 22
            r1[r5] = r0
            goto L_0x0134
        L_0x03c1:
            java.lang.String r2 = "28"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03cf
            r0 = 23
            r1[r5] = r0
            goto L_0x0134
        L_0x03cf:
            java.lang.String r2 = "29"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03dd
            r0 = 28
            r1[r5] = r0
            goto L_0x0134
        L_0x03dd:
            java.lang.String r2 = "30"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03eb
            r0 = 31
            r1[r5] = r0
            goto L_0x0134
        L_0x03eb:
            java.lang.String r2 = "31"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0134
            r0 = 4
            r1[r5] = r0
            goto L_0x0134
        L_0x03f8:
            java.lang.String r4 = "1"
            boolean r4 = r3.endsWith(r4)
            if (r4 == 0) goto L_0x06cc
            if (r2 < r8) goto L_0x0134
            r3 = 8
            java.lang.String r3 = r0.substring(r3, r8)
            java.lang.String r4 = "0"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x042c
            r3 = 24
            r1[r6] = r3
        L_0x0414:
            r3 = 13
            if (r2 < r3) goto L_0x0134
            r2 = 10
            r3 = 13
            java.lang.String r0 = r0.substring(r2, r3)
            java.lang.String r2 = "010"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x0460
            r1[r5] = r5
            goto L_0x0134
        L_0x042c:
            java.lang.String r4 = "1"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0439
            r3 = 15
            r1[r6] = r3
            goto L_0x0414
        L_0x0439:
            java.lang.String r4 = "2"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0446
            r3 = 16
            r1[r6] = r3
            goto L_0x0414
        L_0x0446:
            java.lang.String r4 = "5"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0453
            r3 = 19
            r1[r6] = r3
            goto L_0x0414
        L_0x0453:
            java.lang.String r4 = "6"
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x0414
            r3 = 20
            r1[r6] = r3
            goto L_0x0414
        L_0x0460:
            java.lang.String r2 = "022"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x046c
            r1[r5] = r7
            goto L_0x0134
        L_0x046c:
            java.lang.String r2 = "31"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x047c
            java.lang.String r2 = "33"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0480
        L_0x047c:
            r1[r5] = r8
            goto L_0x0134
        L_0x0480:
            java.lang.String r2 = "35"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0490
            java.lang.String r2 = "34"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0496
        L_0x0490:
            r0 = 12
            r1[r5] = r0
            goto L_0x0134
        L_0x0496:
            java.lang.String r2 = "47"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x04a6
            java.lang.String r2 = "48"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x04ac
        L_0x04a6:
            r0 = 8
            r1[r5] = r0
            goto L_0x0134
        L_0x04ac:
            java.lang.String r2 = "024"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x04c4
            java.lang.String r2 = "41"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x04c4
            java.lang.String r2 = "42"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x04c9
        L_0x04c4:
            r0 = 7
            r1[r5] = r0
            goto L_0x0134
        L_0x04c9:
            java.lang.String r2 = "43"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x04d5
            r1[r5] = r9
            goto L_0x0134
        L_0x04d5:
            java.lang.String r2 = "45"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x04e5
            java.lang.String r2 = "46"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x04ea
        L_0x04e5:
            r0 = 5
            r1[r5] = r0
            goto L_0x0134
        L_0x04ea:
            java.lang.String r2 = "021"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x04f7
            r0 = 2
            r1[r5] = r0
            goto L_0x0134
        L_0x04f7:
            java.lang.String r2 = "025"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x050f
            java.lang.String r2 = "51"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x050f
            java.lang.String r2 = "52"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0515
        L_0x050f:
            r0 = 14
            r1[r5] = r0
            goto L_0x0134
        L_0x0515:
            java.lang.String r2 = "57"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0523
            r0 = 18
            r1[r5] = r0
            goto L_0x0134
        L_0x0523:
            java.lang.String r2 = "55"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0533
            java.lang.String r2 = "56"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0539
        L_0x0533:
            r0 = 13
            r1[r5] = r0
            goto L_0x0134
        L_0x0539:
            java.lang.String r2 = "59"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0547
            r0 = 19
            r1[r5] = r0
            goto L_0x0134
        L_0x0547:
            java.lang.String r2 = "79"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0557
            java.lang.String r2 = "70"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x055d
        L_0x0557:
            r0 = 15
            r1[r5] = r0
            goto L_0x0134
        L_0x055d:
            java.lang.String r2 = "53"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0575
            java.lang.String r2 = "54"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0575
            java.lang.String r2 = "63"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x057b
        L_0x0575:
            r0 = 11
            r1[r5] = r0
            goto L_0x0134
        L_0x057b:
            java.lang.String r2 = "37"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x058b
            java.lang.String r2 = "39"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0591
        L_0x058b:
            r0 = 10
            r1[r5] = r0
            goto L_0x0134
        L_0x0591:
            java.lang.String r2 = "027"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x05a9
            java.lang.String r2 = "71"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x05a9
            java.lang.String r2 = "72"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x05af
        L_0x05a9:
            r0 = 17
            r1[r5] = r0
            goto L_0x0134
        L_0x05af:
            java.lang.String r2 = "73"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x05bf
            java.lang.String r2 = "74"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x05c5
        L_0x05bf:
            r0 = 16
            r1[r5] = r0
            goto L_0x0134
        L_0x05c5:
            java.lang.String r2 = "020"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x05e5
            java.lang.String r2 = "75"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x05e5
            java.lang.String r2 = "76"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x05e5
            java.lang.String r2 = "66"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x05eb
        L_0x05e5:
            r0 = 20
            r1[r5] = r0
            goto L_0x0134
        L_0x05eb:
            java.lang.String r2 = "77"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x05f9
            r0 = 29
            r1[r5] = r0
            goto L_0x0134
        L_0x05f9:
            java.lang.String r2 = "898"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x0607
            r0 = 27
            r1[r5] = r0
            goto L_0x0134
        L_0x0607:
            java.lang.String r2 = "028"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x0627
            java.lang.String r2 = "81"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0627
            java.lang.String r2 = "82"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0627
            java.lang.String r2 = "83"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x062d
        L_0x0627:
            r0 = 24
            r1[r5] = r0
            goto L_0x0134
        L_0x062d:
            java.lang.String r2 = "85"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0134
            java.lang.String r2 = "87"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x064d
            java.lang.String r2 = "88"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x064d
            java.lang.String r2 = "69"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0653
        L_0x064d:
            r0 = 26
            r1[r5] = r0
            goto L_0x0134
        L_0x0653:
            java.lang.String r2 = "89"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0661
            r0 = 30
            r1[r5] = r0
            goto L_0x0134
        L_0x0661:
            java.lang.String r2 = "029"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x0671
            java.lang.String r2 = "91"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0677
        L_0x0671:
            r0 = 21
            r1[r5] = r0
            goto L_0x0134
        L_0x0677:
            java.lang.String r2 = "93"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0687
            java.lang.String r2 = "94"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x068d
        L_0x0687:
            r0 = 22
            r1[r5] = r0
            goto L_0x0134
        L_0x068d:
            java.lang.String r2 = "97"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x069b
            r0 = 23
            r1[r5] = r0
            goto L_0x0134
        L_0x069b:
            java.lang.String r2 = "95"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x06a9
            r0 = 28
            r1[r5] = r0
            goto L_0x0134
        L_0x06a9:
            java.lang.String r2 = "90"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x06b9
            java.lang.String r2 = "99"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x06bf
        L_0x06b9:
            r0 = 31
            r1[r5] = r0
            goto L_0x0134
        L_0x06bf:
            java.lang.String r2 = "023"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0134
            r0 = 4
            r1[r5] = r0
            goto L_0x0134
        L_0x06cc:
            java.lang.String r4 = "3"
            boolean r3 = r3.endsWith(r4)
            if (r3 == 0) goto L_0x0134
            if (r2 < r8) goto L_0x0134
            r3 = 8
            java.lang.String r3 = r0.substring(r3, r8)
            java.lang.String r4 = "3"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0700
            r3 = 17
            r1[r6] = r3
        L_0x06e8:
            r3 = 13
            if (r2 < r3) goto L_0x0134
            r2 = 10
            r3 = 13
            java.lang.String r0 = r0.substring(r2, r3)
            java.lang.String r2 = "010"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0734
            r1[r5] = r5
            goto L_0x0134
        L_0x0700:
            java.lang.String r4 = "4"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x070d
            r3 = 18
            r1[r6] = r3
            goto L_0x06e8
        L_0x070d:
            java.lang.String r4 = "7"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x071a
            r3 = 21
            r1[r6] = r3
            goto L_0x06e8
        L_0x071a:
            java.lang.String r4 = "8"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0727
            r3 = 22
            r1[r6] = r3
            goto L_0x06e8
        L_0x0727:
            java.lang.String r4 = "9"
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x06e8
            r3 = 23
            r1[r6] = r3
            goto L_0x06e8
        L_0x0734:
            java.lang.String r2 = "022"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0740
            r1[r5] = r7
            goto L_0x0134
        L_0x0740:
            java.lang.String r2 = "31"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0750
            java.lang.String r2 = "33"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0754
        L_0x0750:
            r1[r5] = r8
            goto L_0x0134
        L_0x0754:
            java.lang.String r2 = "35"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0764
            java.lang.String r2 = "34"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x076a
        L_0x0764:
            r0 = 12
            r1[r5] = r0
            goto L_0x0134
        L_0x076a:
            java.lang.String r2 = "47"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x077a
            java.lang.String r2 = "48"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0780
        L_0x077a:
            r0 = 8
            r1[r5] = r0
            goto L_0x0134
        L_0x0780:
            java.lang.String r2 = "024"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x0798
            java.lang.String r2 = "41"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0798
            java.lang.String r2 = "42"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x079d
        L_0x0798:
            r0 = 7
            r1[r5] = r0
            goto L_0x0134
        L_0x079d:
            java.lang.String r2 = "43"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x07a9
            r1[r5] = r9
            goto L_0x0134
        L_0x07a9:
            java.lang.String r2 = "45"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x07b9
            java.lang.String r2 = "46"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x07be
        L_0x07b9:
            r0 = 5
            r1[r5] = r0
            goto L_0x0134
        L_0x07be:
            java.lang.String r2 = "021"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x07cb
            r0 = 2
            r1[r5] = r0
            goto L_0x0134
        L_0x07cb:
            java.lang.String r2 = "025"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x07e3
            java.lang.String r2 = "51"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x07e3
            java.lang.String r2 = "52"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x07e9
        L_0x07e3:
            r0 = 14
            r1[r5] = r0
            goto L_0x0134
        L_0x07e9:
            java.lang.String r2 = "57"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x07f9
            java.lang.String r2 = "58"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x07ff
        L_0x07f9:
            r0 = 18
            r1[r5] = r0
            goto L_0x0134
        L_0x07ff:
            java.lang.String r2 = "55"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x080f
            java.lang.String r2 = "56"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0815
        L_0x080f:
            r0 = 13
            r1[r5] = r0
            goto L_0x0134
        L_0x0815:
            java.lang.String r2 = "59"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0823
            r0 = 19
            r1[r5] = r0
            goto L_0x0134
        L_0x0823:
            java.lang.String r2 = "79"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0833
            java.lang.String r2 = "70"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0839
        L_0x0833:
            r0 = 15
            r1[r5] = r0
            goto L_0x0134
        L_0x0839:
            java.lang.String r2 = "53"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0851
            java.lang.String r2 = "54"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0851
            java.lang.String r2 = "63"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0857
        L_0x0851:
            r0 = 11
            r1[r5] = r0
            goto L_0x0134
        L_0x0857:
            java.lang.String r2 = "37"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0867
            java.lang.String r2 = "39"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x086d
        L_0x0867:
            r0 = 10
            r1[r5] = r0
            goto L_0x0134
        L_0x086d:
            java.lang.String r2 = "027"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x0885
            java.lang.String r2 = "71"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0885
            java.lang.String r2 = "72"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x088b
        L_0x0885:
            r0 = 17
            r1[r5] = r0
            goto L_0x0134
        L_0x088b:
            java.lang.String r2 = "73"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x089b
            java.lang.String r2 = "74"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x08a1
        L_0x089b:
            r0 = 16
            r1[r5] = r0
            goto L_0x0134
        L_0x08a1:
            java.lang.String r2 = "020"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x08c1
            java.lang.String r2 = "75"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x08c1
            java.lang.String r2 = "76"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x08c1
            java.lang.String r2 = "66"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x08c7
        L_0x08c1:
            r0 = 20
            r1[r5] = r0
            goto L_0x0134
        L_0x08c7:
            java.lang.String r2 = "77"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x08d5
            r0 = 29
            r1[r5] = r0
            goto L_0x0134
        L_0x08d5:
            java.lang.String r2 = "898"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x08e3
            r0 = 27
            r1[r5] = r0
            goto L_0x0134
        L_0x08e3:
            java.lang.String r2 = "028"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x0903
            java.lang.String r2 = "81"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0903
            java.lang.String r2 = "82"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0903
            java.lang.String r2 = "83"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0909
        L_0x0903:
            r0 = 24
            r1[r5] = r0
            goto L_0x0134
        L_0x0909:
            java.lang.String r2 = "85"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0917
            r0 = 25
            r1[r5] = r0
            goto L_0x0134
        L_0x0917:
            java.lang.String r2 = "87"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x092f
            java.lang.String r2 = "88"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x092f
            java.lang.String r2 = "69"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0935
        L_0x092f:
            r0 = 26
            r1[r5] = r0
            goto L_0x0134
        L_0x0935:
            java.lang.String r2 = "89"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0943
            r0 = 30
            r1[r5] = r0
            goto L_0x0134
        L_0x0943:
            java.lang.String r2 = "029"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x0953
            java.lang.String r2 = "91"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0959
        L_0x0953:
            r0 = 21
            r1[r5] = r0
            goto L_0x0134
        L_0x0959:
            java.lang.String r2 = "93"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0969
            java.lang.String r2 = "94"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x096f
        L_0x0969:
            r0 = 22
            r1[r5] = r0
            goto L_0x0134
        L_0x096f:
            java.lang.String r2 = "97"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x097d
            r0 = 23
            r1[r5] = r0
            goto L_0x0134
        L_0x097d:
            java.lang.String r2 = "95"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x098b
            r0 = 28
            r1[r5] = r0
            goto L_0x0134
        L_0x098b:
            java.lang.String r2 = "90"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x099b
            java.lang.String r2 = "99"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x09a1
        L_0x099b:
            r0 = 31
            r1[r5] = r0
            goto L_0x0134
        L_0x09a1:
            java.lang.String r2 = "023"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0134
            r0 = 4
            r1[r5] = r0
            goto L_0x0134
        L_0x09ae:
            r0 = move-exception
            goto L_0x00da
        L_0x09b1:
            r0 = move-exception
            goto L_0x00b7
        L_0x09b4:
            r0 = move-exception
            goto L_0x00b7
        L_0x09b7:
            r0 = move-exception
            goto L_0x00b7
        L_0x09ba:
            r0 = move-exception
            goto L_0x00b7
        L_0x09bd:
            r0 = move-exception
            goto L_0x00b7
        L_0x09c0:
            r0 = move-exception
            goto L_0x0082
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0012i.b(android.content.Context):void");
    }

    protected static void a(String str) {
        Log.e("Adwo SDK", str);
    }

    private static byte[] a() {
        String line1Number;
        if (!(i == null || (line1Number = i.getLine1Number()) == null)) {
            try {
                A = line1Number.getBytes(XmlConstant.DEFAULT_ENCODING);
                return line1Number.getBytes(XmlConstant.DEFAULT_ENCODING);
            } catch (UnsupportedEncodingException e2) {
            }
        }
        return null;
    }

    protected static void b(String str) {
        if (str == null || str.length() != 32) {
            Log.e("Adwo SDK", "CONFIGURATION ERROR:  Incorrect Adwo publisher ID.  Should 32 [a-z,0-9] characters:  " + k);
        }
        Log.d("Adwo SDK", "Your Adwo PID is " + str);
        try {
            k = str.getBytes(XmlConstant.DEFAULT_ENCODING);
        } catch (UnsupportedEncodingException e2) {
            Log.e("Adwo SDK", "CONFIGURATION ERROR:  Incorrect Adwo publisher ID.  Should 32 [a-z,0-9] characters:  " + k);
        }
    }

    protected static void a(boolean z2) {
        j = z2;
    }

    private static byte[] c(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == -1) {
            Log.e("Adwo SDK", "Cannot request an ad without READ_PHONE_STATE permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.READ_PHONE_STATE\" />");
        }
        if (a == null && i != null) {
            a = i.getDeviceId();
        }
        if (a == null) {
            a = Settings.System.getString(context.getContentResolver(), "android_id");
        }
        if (a == null) {
            a = "00000000";
        }
        try {
            if (a != null) {
                l = a.getBytes(XmlConstant.DEFAULT_ENCODING);
                return a.getBytes(XmlConstant.DEFAULT_ENCODING);
            }
        } catch (UnsupportedEncodingException e2) {
        }
        return "00000000".getBytes();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0252, code lost:
        if (r34 == null) goto L_0x025b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:?, code lost:
        com.adwo.adsdk.C0012i.v = (short) (com.adwo.adsdk.C0012i.v + 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x025b, code lost:
        if (r3 == null) goto L_0x0260;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0260, code lost:
        if (r36 == null) goto L_0x0265;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0262, code lost:
        r36.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0265, code lost:
        if (r35 == null) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0267, code lost:
        r35.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0043, code lost:
        r36 = com.adwo.adsdk.O.a(com.adwo.adsdk.C0012i.y, com.adwo.adsdk.C0012i.z, com.adwo.adsdk.C0012i.u, r35, com.adwo.adsdk.C0012i.x, r8, com.adwo.adsdk.C0012i.k, (byte) 0, 0, com.adwo.adsdk.C0012i.l, com.adwo.adsdk.C0012i.s, com.adwo.adsdk.C0012i.t, 0, 0, (short) com.adwo.adsdk.C0012i.C, com.adwo.adsdk.C0012i.q, com.adwo.adsdk.C0012i.r, com.adwo.adsdk.C0012i.A, com.adwo.adsdk.C0012i.w, false, 0.0d, 0.0d, (byte) 0, r28, r29, com.adwo.adsdk.C0012i.v);
        r3 = null;
        com.adwo.adsdk.C0012i.f = com.adwo.adsdk.C0012i.b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:?, code lost:
        r35.setRequestProperty("Content-Length", java.lang.Integer.toString(r36.length));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:?, code lost:
        r4.write(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0285, code lost:
        r34 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0286, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x028a, code lost:
        android.util.Log.w("Adwo SDK", "Could not get ad from Adwo servers.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x0294, code lost:
        r34 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0298, code lost:
        r34 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x02a2, code lost:
        r34 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x02a5, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x02a6, code lost:
        r33 = r4;
        r4 = r34;
        r34 = r33;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x02ae, code lost:
        r34 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x02b2, code lost:
        r35 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0084, code lost:
        if (com.adwo.adsdk.C0012i.f != com.adwo.adsdk.C0012i.b) goto L_0x010a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0088, code lost:
        if (com.adwo.adsdk.C0012i.j == false) goto L_0x00d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x008a, code lost:
        r0 = new java.net.URL(new java.lang.String(com.adwo.adsdk.P.e, com.wqmobile.sdk.pojoxml.util.XmlConstant.DEFAULT_ENCODING));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x009b, code lost:
        r35 = (java.net.HttpURLConnection) r35.openConnection();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r35.setConnectTimeout(com.adwo.adsdk.P.a);
        r35.setReadTimeout(com.adwo.adsdk.P.a);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00b1, code lost:
        if (r35 != null) goto L_0x01f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00b3, code lost:
        if (r35 == null) goto L_0x00b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r35.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r0 = new java.net.URL(new java.lang.String(com.adwo.adsdk.P.c, com.wqmobile.sdk.pojoxml.util.XmlConstant.DEFAULT_ENCODING));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00e4, code lost:
        r34 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00e5, code lost:
        r35 = null;
        r36 = null;
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r34.printStackTrace();
        android.util.Log.w("Adwo SDK", "Could not get ad from Adwo servers,Network Error!");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00f8, code lost:
        if (r3 != null) goto L_0x00fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00fd, code lost:
        if (r36 != null) goto L_0x00ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ff, code lost:
        r36.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0102, code lost:
        if (r35 == null) goto L_0x02ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0104, code lost:
        r35.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0107, code lost:
        r34 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0111, code lost:
        if (com.adwo.adsdk.C0012i.f == com.adwo.adsdk.C0012i.c) goto L_0x011c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x011a, code lost:
        if (com.adwo.adsdk.C0012i.f != com.adwo.adsdk.C0012i.d) goto L_0x018f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x011e, code lost:
        if (com.adwo.adsdk.C0012i.j == false) goto L_0x0165;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0120, code lost:
        r0 = new java.net.URL(new java.lang.String(com.adwo.adsdk.P.g, "utf-8"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0131, code lost:
        r35 = (java.net.HttpURLConnection) r35.openConnection();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        r35.setRequestProperty("X-Online-Host", new java.lang.String(com.adwo.adsdk.P.f, "utf-8"));
        r35.setConnectTimeout(com.adwo.adsdk.P.a * 2);
        r35.setReadTimeout(com.adwo.adsdk.P.a * 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x015f, code lost:
        r34 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0160, code lost:
        r36 = null;
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        r0 = new java.net.URL(new java.lang.String(com.adwo.adsdk.P.h, "utf-8"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0177, code lost:
        r34 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0178, code lost:
        r35 = null;
        r36 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x017c, code lost:
        if (r3 != null) goto L_0x017e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0181, code lost:
        if (r36 != null) goto L_0x0183;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0183, code lost:
        r36.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0186, code lost:
        if (r35 != null) goto L_0x0188;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0188, code lost:
        r35.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
        throw r34;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0196, code lost:
        if (com.adwo.adsdk.C0012i.f != com.adwo.adsdk.C0012i.e) goto L_0x02b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x019a, code lost:
        if (com.adwo.adsdk.C0012i.j == false) goto L_0x01e7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x019c, code lost:
        r0 = new java.net.URL(new java.lang.String(com.adwo.adsdk.P.e, com.wqmobile.sdk.pojoxml.util.XmlConstant.DEFAULT_ENCODING));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01ad, code lost:
        r35 = (java.net.HttpURLConnection) r35.openConnection(new java.net.Proxy(java.net.Proxy.Type.HTTP, new java.net.InetSocketAddress(new java.lang.String(com.adwo.adsdk.P.i, "utf-8"), 80)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:?, code lost:
        r35.setConnectTimeout(com.adwo.adsdk.P.a * 2);
        r35.setReadTimeout(com.adwo.adsdk.P.a * 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01e3, code lost:
        r34 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01e4, code lost:
        r36 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:?, code lost:
        r0 = new java.net.URL(new java.lang.String(com.adwo.adsdk.P.c, com.wqmobile.sdk.pojoxml.util.XmlConstant.DEFAULT_ENCODING));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        r35.setRequestMethod("POST");
        r35.setDoOutput(true);
        r35.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0215, code lost:
        if (com.adwo.adsdk.C0012i.j == false) goto L_0x026f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0217, code lost:
        r35.setRequestProperty("Content-Length", java.lang.Integer.toString(r36.length));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0228, code lost:
        r3 = r35.getOutputStream();
        r5 = com.adwo.adsdk.C0012i.j;
        r3.write(r36);
        r36 = r35.getInputStream();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:?, code lost:
        r4 = new java.io.ByteArrayOutputStream();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x023d, code lost:
        r5 = r36.read();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0242, code lost:
        if (r5 != -1) goto L_0x0281;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0244, code lost:
        r4 = r4.toByteArray();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0249, code lost:
        if (r4.length <= 0) goto L_0x028a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x024b, code lost:
        r34 = com.adwo.adsdk.C0009f.a(r34, r4);
     */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x02ae  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00fa A[SYNTHETIC, Splitter:B:37:0x00fa] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00ff A[Catch:{ Exception -> 0x0297 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0104 A[Catch:{ Exception -> 0x0297 }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x017e A[SYNTHETIC, Splitter:B:63:0x017e] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0183 A[Catch:{ Exception -> 0x029f }] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0188 A[Catch:{ Exception -> 0x029f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static synchronized com.adwo.adsdk.C0009f a(android.content.Context r34, byte r35, byte r36) {
        /*
            java.lang.Class<com.adwo.adsdk.i> r31 = com.adwo.adsdk.C0012i.class
            monitor-enter(r31)
            java.lang.String r36 = "android.permission.INTERNET"
            r0 = r34
            r1 = r36
            int r36 = r0.checkCallingOrSelfPermission(r1)     // Catch:{ all -> 0x018c }
            r3 = -1
            r0 = r36
            r1 = r3
            if (r0 != r1) goto L_0x001d
            java.lang.String r36 = "Cannot request an ad without INTERNET permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />"
            java.lang.String r3 = "Adwo SDK"
            r0 = r3
            r1 = r36
            android.util.Log.e(r0, r1)     // Catch:{ all -> 0x018c }
        L_0x001d:
            r32 = 0
            boolean r8 = com.adwo.adsdk.C0012i.j     // Catch:{ all -> 0x018c }
            r10 = 0
            java.util.Set r36 = com.adwo.adsdk.C0012i.h     // Catch:{ all -> 0x018c }
            int r36 = r36.size()     // Catch:{ all -> 0x018c }
            r0 = r36
            short r0 = (short) r0     // Catch:{ all -> 0x018c }
            r28 = r0
            r0 = r28
            java.lang.Integer[] r0 = new java.lang.Integer[r0]     // Catch:{ all -> 0x018c }
            r29 = r0
            r36 = 0
            java.util.Set r3 = com.adwo.adsdk.C0012i.h     // Catch:{ all -> 0x018c }
            java.util.Iterator r3 = r3.iterator()     // Catch:{ all -> 0x018c }
            r4 = r36
        L_0x003d:
            boolean r36 = r3.hasNext()     // Catch:{ all -> 0x018c }
            if (r36 != 0) goto L_0x00bc
            byte r3 = com.adwo.adsdk.C0012i.y     // Catch:{ all -> 0x018c }
            byte r4 = com.adwo.adsdk.C0012i.z     // Catch:{ all -> 0x018c }
            byte r5 = com.adwo.adsdk.C0012i.u     // Catch:{ all -> 0x018c }
            byte r7 = com.adwo.adsdk.C0012i.x     // Catch:{ all -> 0x018c }
            byte[] r9 = com.adwo.adsdk.C0012i.k     // Catch:{ all -> 0x018c }
            r11 = 0
            byte[] r12 = com.adwo.adsdk.C0012i.l     // Catch:{ all -> 0x018c }
            byte[] r13 = com.adwo.adsdk.C0012i.s     // Catch:{ all -> 0x018c }
            byte[] r14 = com.adwo.adsdk.C0012i.t     // Catch:{ all -> 0x018c }
            r15 = 0
            r16 = 0
            int r36 = com.adwo.adsdk.C0012i.C     // Catch:{ all -> 0x018c }
            r0 = r36
            short r0 = (short) r0     // Catch:{ all -> 0x018c }
            r17 = r0
            byte r18 = com.adwo.adsdk.C0012i.q     // Catch:{ all -> 0x018c }
            byte r19 = com.adwo.adsdk.C0012i.r     // Catch:{ all -> 0x018c }
            byte[] r20 = com.adwo.adsdk.C0012i.A     // Catch:{ all -> 0x018c }
            byte[] r21 = com.adwo.adsdk.C0012i.w     // Catch:{ all -> 0x018c }
            r22 = 0
            r23 = 0
            r25 = 0
            r27 = 0
            short r30 = com.adwo.adsdk.C0012i.v     // Catch:{ all -> 0x018c }
            r6 = r35
            byte[] r36 = com.adwo.adsdk.O.a(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r25, r27, r28, r29, r30)     // Catch:{ all -> 0x018c }
            r3 = 0
            r4 = 0
            r5 = 0
            int r35 = com.adwo.adsdk.C0012i.b     // Catch:{ all -> 0x018c }
            com.adwo.adsdk.C0012i.f = r35     // Catch:{ all -> 0x018c }
            int r35 = com.adwo.adsdk.C0012i.f     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            int r6 = com.adwo.adsdk.C0012i.b     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            r0 = r35
            r1 = r6
            if (r0 != r1) goto L_0x010a
            boolean r35 = com.adwo.adsdk.C0012i.j     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            if (r35 == 0) goto L_0x00d2
            java.net.URL r35 = new java.net.URL     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            byte[] r7 = com.adwo.adsdk.P.e     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.lang.String r8 = "UTF-8"
            r6.<init>(r7, r8)     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            r0 = r35
            r1 = r6
            r0.<init>(r1)     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
        L_0x009b:
            java.net.URLConnection r35 = r35.openConnection()     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.net.HttpURLConnection r35 = (java.net.HttpURLConnection) r35     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            int r5 = com.adwo.adsdk.P.a     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            r0 = r35
            r1 = r5
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            int r5 = com.adwo.adsdk.P.a     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            r0 = r35
            r1 = r5
            r0.setReadTimeout(r1)     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
        L_0x00b1:
            if (r35 != 0) goto L_0x01f9
            if (r35 == 0) goto L_0x00b8
            r35.disconnect()     // Catch:{ Exception -> 0x029c }
        L_0x00b8:
            r34 = 0
        L_0x00ba:
            monitor-exit(r31)
            return r34
        L_0x00bc:
            java.lang.Object r36 = r3.next()     // Catch:{ all -> 0x018c }
            java.lang.Integer r36 = (java.lang.Integer) r36     // Catch:{ all -> 0x018c }
            int r36 = r36.intValue()     // Catch:{ all -> 0x018c }
            java.lang.Integer r36 = java.lang.Integer.valueOf(r36)     // Catch:{ all -> 0x018c }
            r29[r4] = r36     // Catch:{ all -> 0x018c }
            int r36 = r4 + 1
            r4 = r36
            goto L_0x003d
        L_0x00d2:
            java.net.URL r35 = new java.net.URL     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            byte[] r7 = com.adwo.adsdk.P.c     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.lang.String r8 = "UTF-8"
            r6.<init>(r7, r8)     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            r0 = r35
            r1 = r6
            r0.<init>(r1)     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            goto L_0x009b
        L_0x00e4:
            r34 = move-exception
            r35 = r5
            r36 = r4
            r4 = r32
        L_0x00eb:
            r34.printStackTrace()     // Catch:{ all -> 0x02a2 }
            java.lang.String r34 = "Adwo SDK"
            java.lang.String r5 = "Could not get ad from Adwo servers,Network Error!"
            r0 = r34
            r1 = r5
            android.util.Log.w(r0, r1)     // Catch:{ all -> 0x02a2 }
            if (r3 == 0) goto L_0x00fd
            r3.close()     // Catch:{ Exception -> 0x0297 }
        L_0x00fd:
            if (r36 == 0) goto L_0x0102
            r36.close()     // Catch:{ Exception -> 0x0297 }
        L_0x0102:
            if (r35 == 0) goto L_0x02ae
            r35.disconnect()     // Catch:{ Exception -> 0x0297 }
            r34 = r4
            goto L_0x00ba
        L_0x010a:
            int r35 = com.adwo.adsdk.C0012i.f     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            int r6 = com.adwo.adsdk.C0012i.c     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            r0 = r35
            r1 = r6
            if (r0 == r1) goto L_0x011c
            int r35 = com.adwo.adsdk.C0012i.f     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            int r6 = com.adwo.adsdk.C0012i.d     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            r0 = r35
            r1 = r6
            if (r0 != r1) goto L_0x018f
        L_0x011c:
            boolean r35 = com.adwo.adsdk.C0012i.j     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            if (r35 == 0) goto L_0x0165
            java.net.URL r35 = new java.net.URL     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            byte[] r7 = com.adwo.adsdk.P.g     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.lang.String r8 = "utf-8"
            r6.<init>(r7, r8)     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            r0 = r35
            r1 = r6
            r0.<init>(r1)     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
        L_0x0131:
            java.net.URLConnection r35 = r35.openConnection()     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.net.HttpURLConnection r35 = (java.net.HttpURLConnection) r35     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.lang.String r5 = "X-Online-Host"
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            byte[] r7 = com.adwo.adsdk.P.f     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            java.lang.String r8 = "utf-8"
            r6.<init>(r7, r8)     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            r0 = r35
            r1 = r5
            r2 = r6
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            int r5 = com.adwo.adsdk.P.a     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            int r5 = r5 * 2
            r0 = r35
            r1 = r5
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            int r5 = com.adwo.adsdk.P.a     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            int r5 = r5 * 2
            r0 = r35
            r1 = r5
            r0.setReadTimeout(r1)     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            goto L_0x00b1
        L_0x015f:
            r34 = move-exception
            r36 = r4
            r4 = r32
            goto L_0x00eb
        L_0x0165:
            java.net.URL r35 = new java.net.URL     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            byte[] r7 = com.adwo.adsdk.P.h     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.lang.String r8 = "utf-8"
            r6.<init>(r7, r8)     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            r0 = r35
            r1 = r6
            r0.<init>(r1)     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            goto L_0x0131
        L_0x0177:
            r34 = move-exception
            r35 = r5
            r36 = r4
        L_0x017c:
            if (r3 == 0) goto L_0x0181
            r3.close()     // Catch:{ Exception -> 0x029f }
        L_0x0181:
            if (r36 == 0) goto L_0x0186
            r36.close()     // Catch:{ Exception -> 0x029f }
        L_0x0186:
            if (r35 == 0) goto L_0x018b
            r35.disconnect()     // Catch:{ Exception -> 0x029f }
        L_0x018b:
            throw r34     // Catch:{ all -> 0x018c }
        L_0x018c:
            r34 = move-exception
            monitor-exit(r31)
            throw r34
        L_0x018f:
            int r35 = com.adwo.adsdk.C0012i.f     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            int r6 = com.adwo.adsdk.C0012i.e     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            r0 = r35
            r1 = r6
            if (r0 != r1) goto L_0x02b2
            boolean r35 = com.adwo.adsdk.C0012i.j     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            if (r35 == 0) goto L_0x01e7
            java.net.URL r35 = new java.net.URL     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            byte[] r7 = com.adwo.adsdk.P.e     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.lang.String r8 = "UTF-8"
            r6.<init>(r7, r8)     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            r0 = r35
            r1 = r6
            r0.<init>(r1)     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
        L_0x01ad:
            java.net.Proxy r6 = new java.net.Proxy     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.net.Proxy$Type r7 = java.net.Proxy.Type.HTTP     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.net.InetSocketAddress r8 = new java.net.InetSocketAddress     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.lang.String r9 = new java.lang.String     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            byte[] r10 = com.adwo.adsdk.P.i     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.lang.String r11 = "utf-8"
            r9.<init>(r10, r11)     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            r10 = 80
            r8.<init>(r9, r10)     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            r6.<init>(r7, r8)     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            r0 = r35
            r1 = r6
            java.net.URLConnection r35 = r0.openConnection(r1)     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.net.HttpURLConnection r35 = (java.net.HttpURLConnection) r35     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            int r5 = com.adwo.adsdk.P.a     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            int r5 = r5 * 2
            r0 = r35
            r1 = r5
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            int r5 = com.adwo.adsdk.P.a     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            int r5 = r5 * 2
            r0 = r35
            r1 = r5
            r0.setReadTimeout(r1)     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            goto L_0x00b1
        L_0x01e3:
            r34 = move-exception
            r36 = r4
            goto L_0x017c
        L_0x01e7:
            java.net.URL r35 = new java.net.URL     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            byte[] r7 = com.adwo.adsdk.P.c     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            java.lang.String r8 = "UTF-8"
            r6.<init>(r7, r8)     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            r0 = r35
            r1 = r6
            r0.<init>(r1)     // Catch:{ Exception -> 0x00e4, all -> 0x0177 }
            goto L_0x01ad
        L_0x01f9:
            java.lang.String r5 = "POST"
            r0 = r35
            r1 = r5
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            r5 = 1
            r0 = r35
            r1 = r5
            r0.setDoOutput(r1)     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            java.lang.String r5 = "Content-Type"
            java.lang.String r6 = "application/x-www-form-urlencoded"
            r0 = r35
            r1 = r5
            r2 = r6
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            boolean r5 = com.adwo.adsdk.C0012i.j     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            if (r5 == 0) goto L_0x026f
            java.lang.String r5 = "Content-Length"
            r0 = r36
            int r0 = r0.length     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            r6 = r0
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            r0 = r35
            r1 = r5
            r2 = r6
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
        L_0x0228:
            java.io.OutputStream r3 = r35.getOutputStream()     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            boolean r5 = com.adwo.adsdk.C0012i.j     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            r0 = r3
            r1 = r36
            r0.write(r1)     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            java.io.InputStream r36 = r35.getInputStream()     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0285 }
            r4.<init>()     // Catch:{ Exception -> 0x0285 }
        L_0x023d:
            int r5 = r36.read()     // Catch:{ Exception -> 0x0285 }
            r6 = -1
            if (r5 != r6) goto L_0x0281
            byte[] r4 = r4.toByteArray()     // Catch:{ Exception -> 0x0285 }
            int r5 = r4.length     // Catch:{ Exception -> 0x0285 }
            if (r5 <= 0) goto L_0x028a
            r0 = r34
            r1 = r4
            com.adwo.adsdk.f r34 = com.adwo.adsdk.C0009f.a(r0, r1)     // Catch:{ Exception -> 0x0285 }
            if (r34 == 0) goto L_0x025b
            short r4 = com.adwo.adsdk.C0012i.v     // Catch:{ Exception -> 0x02a5 }
            int r4 = r4 + 1
            short r4 = (short) r4     // Catch:{ Exception -> 0x02a5 }
            com.adwo.adsdk.C0012i.v = r4     // Catch:{ Exception -> 0x02a5 }
        L_0x025b:
            if (r3 == 0) goto L_0x0260
            r3.close()     // Catch:{ Exception -> 0x026c }
        L_0x0260:
            if (r36 == 0) goto L_0x0265
            r36.close()     // Catch:{ Exception -> 0x026c }
        L_0x0265:
            if (r35 == 0) goto L_0x00ba
            r35.disconnect()     // Catch:{ Exception -> 0x026c }
            goto L_0x00ba
        L_0x026c:
            r35 = move-exception
            goto L_0x00ba
        L_0x026f:
            java.lang.String r5 = "Content-Length"
            r0 = r36
            int r0 = r0.length     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            r6 = r0
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            r0 = r35
            r1 = r5
            r2 = r6
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x015f, all -> 0x01e3 }
            goto L_0x0228
        L_0x0281:
            r4.write(r5)     // Catch:{ Exception -> 0x0285 }
            goto L_0x023d
        L_0x0285:
            r34 = move-exception
            r4 = r32
            goto L_0x00eb
        L_0x028a:
            java.lang.String r34 = "Adwo SDK"
            java.lang.String r4 = "Could not get ad from Adwo servers."
            r0 = r34
            r1 = r4
            android.util.Log.w(r0, r1)     // Catch:{ Exception -> 0x0285 }
            r34 = r32
            goto L_0x025b
        L_0x0297:
            r34 = move-exception
            r34 = r4
            goto L_0x00ba
        L_0x029c:
            r34 = move-exception
            goto L_0x00b8
        L_0x029f:
            r35 = move-exception
            goto L_0x018b
        L_0x02a2:
            r34 = move-exception
            goto L_0x017c
        L_0x02a5:
            r4 = move-exception
            r33 = r4
            r4 = r34
            r34 = r33
            goto L_0x00eb
        L_0x02ae:
            r34 = r4
            goto L_0x00ba
        L_0x02b2:
            r35 = r5
            goto L_0x00b1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0012i.a(android.content.Context, byte, byte):com.adwo.adsdk.f");
    }

    protected static byte a(int i2, int i3, boolean z2) {
        if (i2 < 240 || i2 >= 320) {
            if (i2 < 320 || i2 >= 480) {
                if (i2 < 480 || i2 >= 640) {
                    if (i2 < 640 || i2 >= 720) {
                        if (i2 < 720 || i2 > 1024) {
                            if (i2 > 1024 && i3 > 768) {
                                return 9;
                            }
                        } else if (i3 < 320 && i3 >= 240) {
                            return 9;
                        } else {
                            if ((i3 >= 320 && i3 < 640) || i3 >= 640) {
                                return 9;
                            }
                        }
                    } else if (i3 < 320 && i3 >= 240) {
                        return 8;
                    } else {
                        if (i3 >= 320 && i3 < 640) {
                            return 8;
                        }
                        if (i3 >= 640) {
                            return 8;
                        }
                    }
                } else if (i3 < 320 && i3 >= 240) {
                    return 6;
                } else {
                    if ((i3 >= 320 && i3 < 640) || i3 >= 640) {
                        return 7;
                    }
                }
            } else if (i3 < 320 && i3 >= 240) {
                return 6;
            } else {
                if (i3 >= 320 && i3 < 640) {
                    return 6;
                }
                if (i3 >= 640) {
                    return 8;
                }
            }
        } else if (i3 < 320 && i3 >= 240) {
            return 5;
        } else {
            if (i3 >= 320 && i3 < 480) {
                return 6;
            }
        }
        return 6;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String d(android.content.Context r4) {
        /*
            java.lang.Class<android.webkit.WebSettings> r0 = android.webkit.WebSettings.class
            r1 = 2
            java.lang.Class[] r1 = new java.lang.Class[r1]     // Catch:{ Exception -> 0x0038 }
            r2 = 0
            java.lang.Class<android.content.Context> r3 = android.content.Context.class
            r1[r2] = r3     // Catch:{ Exception -> 0x0038 }
            r2 = 1
            java.lang.Class<android.webkit.WebView> r3 = android.webkit.WebView.class
            r1[r2] = r3     // Catch:{ Exception -> 0x0038 }
            java.lang.reflect.Constructor r1 = r0.getDeclaredConstructor(r1)     // Catch:{ Exception -> 0x0038 }
            r0 = 1
            r1.setAccessible(r0)     // Catch:{ Exception -> 0x0038 }
            r0 = 2
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x0032 }
            r2 = 0
            r0[r2] = r4     // Catch:{ all -> 0x0032 }
            r2 = 1
            r3 = 0
            r0[r2] = r3     // Catch:{ all -> 0x0032 }
            java.lang.Object r0 = r1.newInstance(r0)     // Catch:{ all -> 0x0032 }
            android.webkit.WebSettings r0 = (android.webkit.WebSettings) r0     // Catch:{ all -> 0x0032 }
            java.lang.String r0 = r0.getUserAgentString()     // Catch:{ all -> 0x0032 }
            com.adwo.adsdk.C0012i.g = r0     // Catch:{ all -> 0x0032 }
            r2 = 0
            r1.setAccessible(r2)     // Catch:{ Exception -> 0x0038 }
        L_0x0031:
            return r0
        L_0x0032:
            r0 = move-exception
            r2 = 0
            r1.setAccessible(r2)     // Catch:{ Exception -> 0x0038 }
            throw r0     // Catch:{ Exception -> 0x0038 }
        L_0x0038:
            r0 = move-exception
            android.webkit.WebView r0 = new android.webkit.WebView
            r0.<init>(r4)
            android.webkit.WebSettings r0 = r0.getSettings()
            java.lang.String r0 = r0.getUserAgentString()
            com.adwo.adsdk.C0012i.g = r0
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0012i.d(android.content.Context):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0046 A[SYNTHETIC, Splitter:B:16:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0052 A[SYNTHETIC, Splitter:B:22:0x0052] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void c(java.lang.String r6) {
        /*
            r3 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0041, all -> 0x004d }
            r0.<init>(r6)     // Catch:{ Exception -> 0x0041, all -> 0x004d }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0041, all -> 0x004d }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0041, all -> 0x004d }
            java.lang.String r1 = "GET"
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x0069, all -> 0x005f }
            int r1 = com.adwo.adsdk.P.a     // Catch:{ Exception -> 0x0069, all -> 0x005f }
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x0069, all -> 0x005f }
            java.lang.String r1 = "User-Agent"
            java.lang.String r2 = com.adwo.adsdk.C0012i.g     // Catch:{ Exception -> 0x0069, all -> 0x005f }
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x0069, all -> 0x005f }
            r0.connect()     // Catch:{ Exception -> 0x0069, all -> 0x005f }
            java.io.InputStream r1 = r0.getInputStream()     // Catch:{ Exception -> 0x0069, all -> 0x005f }
            java.lang.String r2 = "adwo"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            java.lang.String r4 = "connect to beacon: "
            r3.<init>(r4)     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            android.util.Log.v(r2, r3)     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            if (r1 == 0) goto L_0x003d
            r1.close()     // Catch:{ IOException -> 0x005d }
        L_0x003d:
            r0.disconnect()
        L_0x0040:
            return
        L_0x0041:
            r0 = move-exception
            r0 = r3
            r1 = r3
        L_0x0044:
            if (r0 == 0) goto L_0x0049
            r0.close()     // Catch:{ IOException -> 0x0059 }
        L_0x0049:
            r1.disconnect()
            goto L_0x0040
        L_0x004d:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0050:
            if (r1 == 0) goto L_0x0055
            r1.close()     // Catch:{ IOException -> 0x005b }
        L_0x0055:
            r2.disconnect()
            throw r0
        L_0x0059:
            r0 = move-exception
            goto L_0x0049
        L_0x005b:
            r1 = move-exception
            goto L_0x0055
        L_0x005d:
            r1 = move-exception
            goto L_0x003d
        L_0x005f:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x0050
        L_0x0064:
            r2 = move-exception
            r5 = r2
            r2 = r0
            r0 = r5
            goto L_0x0050
        L_0x0069:
            r1 = move-exception
            r1 = r0
            r0 = r3
            goto L_0x0044
        L_0x006d:
            r2 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0012i.c(java.lang.String):void");
    }
}
