package com.adwo.adsdk;

import android.webkit.WebView;
import android.webkit.WebViewClient;

final class B extends WebViewClient {
    /* access modifiers changed from: private */
    public /* synthetic */ C0013j a;

    B(C0013j jVar) {
        this.a = jVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:125:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01d8, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01d9, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01df, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01e0, code lost:
        r11 = r2;
        r2 = null;
        r1 = r11;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x01a8  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01d8 A[ExcHandler: MalformedURLException (r0v3 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:5:0x0015] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x01e8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean shouldOverrideUrlLoading(android.webkit.WebView r13, java.lang.String r14) {
        /*
            r12 = this;
            r8 = 0
            r6 = 1
            java.lang.String r10 = "http"
            java.lang.String r9 = "Adwo AdSDK"
            java.lang.String r7 = "android.intent.action.VIEW"
            if (r14 == 0) goto L_0x01d5
            r1 = 0
            android.content.Context r0 = r13.getContext()
            android.app.Activity r0 = (android.app.Activity) r0
            if (r0 != 0) goto L_0x0015
            r0 = r8
        L_0x0014:
            return r0
        L_0x0015:
            android.net.Uri r2 = android.net.Uri.parse(r14)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r3 = r2.getScheme()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r4 = "tel"
            boolean r3 = r3.equalsIgnoreCase(r4)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r3 == 0) goto L_0x0033
            android.content.Intent r3 = new android.content.Intent     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r4 = "android.intent.action.DIAL"
            r3.<init>(r4, r2)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r0 == 0) goto L_0x0031
            r0.startActivity(r3)     // Catch:{ ActivityNotFoundException -> 0x027b }
        L_0x0031:
            r0 = r6
            goto L_0x0014
        L_0x0033:
            java.lang.String r3 = r2.getScheme()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r4 = "sms"
            boolean r3 = r3.equalsIgnoreCase(r4)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r3 == 0) goto L_0x004d
            android.content.Intent r3 = new android.content.Intent     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r4 = "android.intent.action.VIEW"
            r3.<init>(r4, r2)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r0 == 0) goto L_0x004b
            r0.startActivity(r3)     // Catch:{ ActivityNotFoundException -> 0x027e }
        L_0x004b:
            r0 = r6
            goto L_0x0014
        L_0x004d:
            java.lang.String r3 = r2.getScheme()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r4 = "market"
            boolean r3 = r3.equalsIgnoreCase(r4)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r3 != 0) goto L_0x0071
            java.lang.String r3 = r14.toLowerCase()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r4 = "http://market.android.com"
            boolean r3 = r3.startsWith(r4)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r3 != 0) goto L_0x0071
            java.lang.String r3 = r14.toLowerCase()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r4 = "https://market.android.com"
            boolean r3 = r3.startsWith(r4)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r3 == 0) goto L_0x007f
        L_0x0071:
            android.content.Intent r3 = new android.content.Intent     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r4 = "android.intent.action.VIEW"
            r3.<init>(r4, r2)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r0 == 0) goto L_0x007d
            r0.startActivity(r3)     // Catch:{ ActivityNotFoundException -> 0x0281 }
        L_0x007d:
            r0 = r6
            goto L_0x0014
        L_0x007f:
            java.lang.String r3 = r2.getScheme()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r4 = "http"
            boolean r3 = r3.equalsIgnoreCase(r4)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r3 == 0) goto L_0x00a7
            java.lang.String r3 = r14.toLowerCase()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r4 = ".apk"
            boolean r3 = r3.endsWith(r4)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r3 == 0) goto L_0x00a7
            java.lang.Thread r2 = new java.lang.Thread     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            com.adwo.adsdk.C r3 = new com.adwo.adsdk.C     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            r3.<init>(r12, r14, r0)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            r2.start()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            r0 = r6
            goto L_0x0014
        L_0x00a7:
            java.lang.String r2 = r2.getScheme()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r3 = "http"
            boolean r2 = r2.equalsIgnoreCase(r3)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r2 == 0) goto L_0x00bf
            java.lang.String r2 = r14.toLowerCase()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r3 = ".jpeg"
            boolean r2 = r2.endsWith(r3)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r2 != 0) goto L_0x00ef
        L_0x00bf:
            java.lang.String r2 = r14.toLowerCase()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r3 = ".gif"
            boolean r2 = r2.endsWith(r3)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r2 != 0) goto L_0x00ef
            java.lang.String r2 = r14.toLowerCase()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r3 = ".png"
            boolean r2 = r2.endsWith(r3)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r2 != 0) goto L_0x00ef
            java.lang.String r2 = r14.toLowerCase()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r3 = ".jpg"
            boolean r2 = r2.endsWith(r3)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r2 != 0) goto L_0x00ef
            java.lang.String r2 = r14.toLowerCase()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r3 = ".bmp"
            boolean r2 = r2.endsWith(r3)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r2 == 0) goto L_0x00ff
        L_0x00ef:
            java.lang.Thread r2 = new java.lang.Thread     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            com.adwo.adsdk.D r3 = new com.adwo.adsdk.D     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            r3.<init>(r12, r14, r0)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            r2.start()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            r0 = r6
            goto L_0x0014
        L_0x00ff:
            java.lang.String r2 = ".3gp"
            boolean r2 = r14.endsWith(r2)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r2 != 0) goto L_0x0127
            java.lang.String r2 = ".mp3"
            boolean r2 = r14.endsWith(r2)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r2 != 0) goto L_0x0127
            java.lang.String r2 = ".mp4"
            boolean r2 = r14.endsWith(r2)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r2 != 0) goto L_0x0127
            java.lang.String r2 = ".mpeg"
            boolean r2 = r14.endsWith(r2)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r2 != 0) goto L_0x0127
            java.lang.String r2 = ".wav"
            boolean r2 = r14.endsWith(r2)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r2 == 0) goto L_0x0146
        L_0x0127:
            android.content.Intent r2 = new android.content.Intent     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r3 = "android.intent.action.VIEW"
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            r3 = 268435456(0x10000000, float:2.5243549E-29)
            r2.addFlags(r3)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r3 = com.adwo.adsdk.P.a(r14)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            android.net.Uri r4 = android.net.Uri.parse(r14)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            r2.setDataAndType(r4, r3)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            if (r0 == 0) goto L_0x0143
            r0.startActivity(r2)     // Catch:{ ActivityNotFoundException -> 0x0284 }
        L_0x0143:
            r0 = r6
            goto L_0x0014
        L_0x0146:
            java.net.URL r2 = new java.net.URL     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            r2.<init>(r14)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            r3 = 0
            java.net.HttpURLConnection.setFollowRedirects(r3)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.net.URLConnection r12 = r2.openConnection()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.net.HttpURLConnection r12 = (java.net.HttpURLConnection) r12     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r2 = "GET"
            r12.setRequestMethod(r2)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            r12.connect()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r2 = "Location"
            java.lang.String r2 = r12.getHeaderField(r2)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            java.lang.String r3 = "Content-Type"
            java.lang.String r1 = r12.getHeaderField(r3)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x01df }
            r12.getResponseCode()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x0287 }
            java.lang.String r3 = "Adwo AdSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x0287 }
            java.lang.String r5 = "Response Code:"
            r4.<init>(r5)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x0287 }
            int r5 = r12.getResponseCode()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x0287 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x0287 }
            java.lang.String r5 = " Response Message:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x0287 }
            java.lang.String r5 = r12.getResponseMessage()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x0287 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x0287 }
            java.lang.String r4 = r4.toString()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x0287 }
            android.util.Log.d(r3, r4)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x0287 }
            java.lang.String r3 = "Adwo AdSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x0287 }
            java.lang.String r5 = "locurlString: "
            r4.<init>(r5)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x0287 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x0287 }
            java.lang.String r2 = r2.toString()     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x0287 }
            android.util.Log.i(r3, r2)     // Catch:{ MalformedURLException -> 0x01d8, IOException -> 0x0287 }
        L_0x01a6:
            if (r14 == 0) goto L_0x01e8
            android.net.Uri r2 = android.net.Uri.parse(r14)
            if (r1 != 0) goto L_0x01b0
            java.lang.String r1 = ""
        L_0x01b0:
            if (r2 == 0) goto L_0x01d5
            java.lang.String r3 = r2.getScheme()
            if (r3 == 0) goto L_0x01d5
            java.lang.String r3 = r2.getScheme()
            java.lang.String r4 = "market"
            boolean r3 = r3.equalsIgnoreCase(r4)
            if (r3 == 0) goto L_0x01eb
            java.lang.String r1 = "Adwo AdSDK"
            java.lang.String r1 = "Android Market URL, launch the Market Application"
            android.util.Log.i(r9, r1)
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r3 = "android.intent.action.VIEW"
            r1.<init>(r7, r2)
            r0.startActivity(r1)
        L_0x01d5:
            r0 = r6
            goto L_0x0014
        L_0x01d8:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r8
            goto L_0x0014
        L_0x01df:
            r2 = move-exception
            r11 = r2
            r2 = r1
            r1 = r11
        L_0x01e3:
            r1.printStackTrace()
            r1 = r2
            goto L_0x01a6
        L_0x01e8:
            r0 = r8
            goto L_0x0014
        L_0x01eb:
            java.lang.String r3 = r2.getScheme()
            java.lang.String r4 = "rtsp"
            boolean r3 = r3.equalsIgnoreCase(r4)
            if (r3 != 0) goto L_0x01d5
            java.lang.String r3 = r2.getScheme()
            java.lang.String r4 = "http"
            boolean r3 = r3.equalsIgnoreCase(r10)
            if (r3 == 0) goto L_0x0213
            java.lang.String r3 = "video/mp4"
            boolean r3 = r1.equalsIgnoreCase(r3)
            if (r3 != 0) goto L_0x01d5
            java.lang.String r3 = "video/3gpp"
            boolean r1 = r1.equalsIgnoreCase(r3)
            if (r1 != 0) goto L_0x01d5
        L_0x0213:
            java.lang.String r1 = r2.getScheme()
            java.lang.String r3 = "tel"
            boolean r1 = r1.equalsIgnoreCase(r3)
            if (r1 == 0) goto L_0x0231
            java.lang.String r1 = "Adwo AdSDK"
            java.lang.String r1 = "Telephone Number, launch the phone"
            android.util.Log.i(r9, r1)
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r3 = "android.intent.action.DIAL"
            r1.<init>(r3, r2)
            r0.startActivity(r1)
            goto L_0x01d5
        L_0x0231:
            java.lang.String r1 = r2.getScheme()
            java.lang.String r3 = "sms"
            boolean r1 = r1.equalsIgnoreCase(r3)
            if (r1 == 0) goto L_0x0248
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r3 = "android.intent.action.VIEW"
            r1.<init>(r7, r2)
            r0.startActivity(r1)
            goto L_0x01d5
        L_0x0248:
            java.lang.String r0 = r2.getScheme()
            java.lang.String r1 = "http"
            boolean r0 = r0.equalsIgnoreCase(r10)
            if (r0 == 0) goto L_0x0272
            java.lang.String r0 = r2.getLastPathSegment()
            if (r0 == 0) goto L_0x0272
            java.lang.String r0 = r2.getLastPathSegment()
            java.lang.String r1 = ".mp4"
            boolean r0 = r0.endsWith(r1)
            if (r0 != 0) goto L_0x01d5
            java.lang.String r0 = r2.getLastPathSegment()
            java.lang.String r1 = ".3gp"
            boolean r0 = r0.endsWith(r1)
            if (r0 != 0) goto L_0x01d5
        L_0x0272:
            java.lang.String r0 = r2.toString()
            r13.loadUrl(r0)
            goto L_0x01d5
        L_0x027b:
            r0 = move-exception
            goto L_0x0031
        L_0x027e:
            r0 = move-exception
            goto L_0x004b
        L_0x0281:
            r0 = move-exception
            goto L_0x007d
        L_0x0284:
            r0 = move-exception
            goto L_0x0143
        L_0x0287:
            r2 = move-exception
            r11 = r2
            r2 = r1
            r1 = r11
            goto L_0x01e3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.B.shouldOverrideUrlLoading(android.webkit.WebView, java.lang.String):boolean");
    }

    public final void onPageFinished(WebView webView, String str) {
        if (this.a.a.canGoBack()) {
            if (this.a.f != null) {
                this.a.f.setVisibility(0);
            }
        } else if (this.a.f != null) {
            this.a.f.setVisibility(4);
        }
        if (this.a.a.canGoForward()) {
            if (this.a.e != null) {
                this.a.e.setVisibility(0);
            }
        } else if (this.a.e != null) {
            this.a.e.setVisibility(4);
        }
        C0013j jVar = this.a;
        C0013j.b(this.a.a());
    }
}
