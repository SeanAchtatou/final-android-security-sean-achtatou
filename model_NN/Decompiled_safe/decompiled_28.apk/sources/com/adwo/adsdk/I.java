package com.adwo.adsdk;

import android.view.animation.Animation;

final class I implements Animation.AnimationListener {
    private /* synthetic */ AdwoAdView a;
    private final /* synthetic */ C0005b b;
    private final /* synthetic */ N c;

    I(AdwoAdView adwoAdView, C0005b bVar, N n) {
        this.a = adwoAdView;
        this.b = bVar;
        this.c = n;
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.post(new K(this.a, this.b, this.c));
    }

    public final void onAnimationRepeat(Animation animation) {
    }
}
