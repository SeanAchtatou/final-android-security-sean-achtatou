package com.adwo.adsdk;

import android.graphics.Bitmap;
import java.io.InputStream;
import java.util.Vector;

public final class S {
    private byte[] A = new byte[256];
    private int B = 0;
    private int C = 0;
    private int D = 0;
    private boolean E = false;
    private int F = 0;
    private int G;
    private short[] H;

    /* renamed from: I  reason: collision with root package name */
    private byte[] f1I;
    private byte[] J;
    private byte[] K;
    private Vector L;
    private int M;
    private InputStream a;
    private int b;
    private int c;
    private int d;
    private boolean e;
    private int f;
    private int[] g;
    private int[] h;
    private int[] i;
    private int j;
    private int k;
    private int l;
    private boolean m;
    private boolean n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;
    private int u;
    private int v;
    private int w;
    private Bitmap x;
    private Bitmap y;
    private int z = 0;

    public final int a(int i2) {
        this.F = -1;
        if (i2 >= 0 && i2 < this.M) {
            this.F = ((T) this.L.elementAt(i2)).b;
        }
        return this.F;
    }

    public final int a() {
        return this.M;
    }

    public final Bitmap b(int i2) {
        if (i2 < 0 || i2 >= this.M) {
            return null;
        }
        return ((T) this.L.elementAt(i2)).a;
    }

    public final Bitmap b() {
        this.z++;
        if (this.z > this.L.size() - 1) {
            this.z = 0;
        }
        return ((T) this.L.elementAt(this.z)).a;
    }

    public final int a(InputStream inputStream) {
        this.b = 0;
        this.M = 0;
        this.L = new Vector();
        this.g = null;
        this.h = null;
        if (inputStream != null) {
            try {
                this.a = inputStream;
                if (!g()) {
                    throw new Exception();
                }
                if (!c()) {
                    f();
                    if (this.M < 0) {
                        this.b = 1;
                    }
                }
                if (c()) {
                    throw new Exception();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
                return this.b;
            } catch (Exception e2) {
                throw e2;
            }
        } else {
            this.b = 2;
            throw new Exception();
        }
    }

    private boolean c() {
        return this.b != 0;
    }

    private int d() {
        try {
            return this.a.read();
        } catch (Exception e2) {
            this.b = 1;
            return 0;
        }
    }

    private int e() {
        this.B = d();
        int i2 = 0;
        if (this.B > 0) {
            while (i2 < this.B) {
                try {
                    int read = this.a.read(this.A, i2, this.B - i2);
                    if (read == -1) {
                        break;
                    }
                    i2 += read;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            if (i2 < this.B) {
                this.b = 1;
            }
        }
        return i2;
    }

    private int[] c(int i2) {
        int i3;
        int i4 = i2 * 3;
        byte[] bArr = new byte[i4];
        try {
            i3 = this.a.read(bArr);
        } catch (Exception e2) {
            e2.printStackTrace();
            i3 = 0;
        }
        if (i3 < i4) {
            this.b = 1;
            return null;
        }
        int[] iArr = new int[256];
        int i5 = 0;
        int i6 = 0;
        while (i5 < i2) {
            int i7 = i6 + 1;
            int i8 = i7 + 1;
            iArr[i5] = ((bArr[i6] & 255) << 16) | -16777216 | ((bArr[i7] & 255) << 8) | (bArr[i8] & 255);
            i5++;
            i6 = i8 + 1;
        }
        return iArr;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r8v10, types: [int] */
    /* JADX WARN: Type inference failed for: r16v7, types: [int] */
    /* JADX WARN: Type inference failed for: r17v6, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x03d2 A[LOOP:3: B:44:0x0132->B:109:0x03d2, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x0002 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x013d  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void f() {
        /*
            r26 = this;
            r2 = 0
            r10 = r2
        L_0x0002:
            if (r10 != 0) goto L_0x000a
            boolean r2 = r26.c()
            if (r2 == 0) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            int r2 = r26.d()
            switch(r2) {
                case 0: goto L_0x0002;
                case 33: goto L_0x0488;
                case 44: goto L_0x0019;
                case 59: goto L_0x0513;
                default: goto L_0x0012;
            }
        L_0x0012:
            r2 = 1
            r0 = r2
            r1 = r26
            r1.b = r0
            goto L_0x0002
        L_0x0019:
            int r2 = r26.i()
            r0 = r2
            r1 = r26
            r1.p = r0
            int r2 = r26.i()
            r0 = r2
            r1 = r26
            r1.q = r0
            int r2 = r26.i()
            r0 = r2
            r1 = r26
            r1.r = r0
            int r2 = r26.i()
            r0 = r2
            r1 = r26
            r1.s = r0
            int r2 = r26.d()
            r3 = r2 & 128(0x80, float:1.794E-43)
            if (r3 == 0) goto L_0x0284
            r3 = 1
        L_0x0046:
            r0 = r3
            r1 = r26
            r1.m = r0
            r3 = r2 & 64
            if (r3 == 0) goto L_0x0287
            r3 = 1
        L_0x0050:
            r0 = r3
            r1 = r26
            r1.n = r0
            r3 = 2
            r2 = r2 & 7
            int r2 = r3 << r2
            r0 = r2
            r1 = r26
            r1.o = r0
            r0 = r26
            boolean r0 = r0.m
            r2 = r0
            if (r2 == 0) goto L_0x028a
            r0 = r26
            int r0 = r0.o
            r2 = r0
            r0 = r26
            r1 = r2
            int[] r2 = r0.c(r1)
            r0 = r2
            r1 = r26
            r1.h = r0
            r0 = r26
            int[] r0 = r0.h
            r2 = r0
            r0 = r2
            r1 = r26
            r1.i = r0
        L_0x0081:
            r2 = 0
            r0 = r26
            boolean r0 = r0.E
            r3 = r0
            if (r3 == 0) goto L_0x00a2
            r0 = r26
            int[] r0 = r0.i
            r2 = r0
            r0 = r26
            int r0 = r0.G
            r3 = r0
            r2 = r2[r3]
            r0 = r26
            int[] r0 = r0.i
            r3 = r0
            r0 = r26
            int r0 = r0.G
            r4 = r0
            r5 = 0
            r3[r4] = r5
        L_0x00a2:
            r11 = r2
            r0 = r26
            int[] r0 = r0.i
            r2 = r0
            if (r2 != 0) goto L_0x00b0
            r2 = 1
            r0 = r2
            r1 = r26
            r1.b = r0
        L_0x00b0:
            boolean r2 = r26.c()
            if (r2 != 0) goto L_0x0002
            r0 = r26
            int r0 = r0.r
            r2 = r0
            r0 = r26
            int r0 = r0.s
            r3 = r0
            int r2 = r2 * r3
            r0 = r26
            byte[] r0 = r0.K
            r3 = r0
            if (r3 == 0) goto L_0x00d0
            r0 = r26
            byte[] r0 = r0.K
            r3 = r0
            int r3 = r3.length
            if (r3 >= r2) goto L_0x00d7
        L_0x00d0:
            byte[] r3 = new byte[r2]
            r0 = r3
            r1 = r26
            r1.K = r0
        L_0x00d7:
            r0 = r26
            short[] r0 = r0.H
            r3 = r0
            if (r3 != 0) goto L_0x00e7
            r3 = 4096(0x1000, float:5.74E-42)
            short[] r3 = new short[r3]
            r0 = r3
            r1 = r26
            r1.H = r0
        L_0x00e7:
            r0 = r26
            byte[] r0 = r0.f1I
            r3 = r0
            if (r3 != 0) goto L_0x00f7
            r3 = 4096(0x1000, float:5.74E-42)
            byte[] r3 = new byte[r3]
            r0 = r3
            r1 = r26
            r1.f1I = r0
        L_0x00f7:
            r0 = r26
            byte[] r0 = r0.J
            r3 = r0
            if (r3 != 0) goto L_0x0107
            r3 = 4097(0x1001, float:5.741E-42)
            byte[] r3 = new byte[r3]
            r0 = r3
            r1 = r26
            r1.J = r0
        L_0x0107:
            int r3 = r26.d()
            r4 = 1
            int r4 = r4 << r3
            int r5 = r4 + 1
            int r6 = r4 + 2
            r7 = -1
            int r8 = r3 + 1
            r9 = 1
            int r9 = r9 << r8
            r12 = 1
            int r9 = r9 - r12
            r12 = 0
        L_0x0119:
            if (r12 < r4) goto L_0x02a8
            r12 = 0
            r13 = 0
            r14 = r12
            r15 = r7
            r16 = r8
            r17 = r9
            r18 = r6
            r19 = r13
            r7 = r12
            r8 = r12
            r9 = r12
            r13 = r12
            r6 = r12
        L_0x012c:
            r0 = r19
            r1 = r2
            if (r0 < r1) goto L_0x02bc
        L_0x0131:
            r3 = r6
        L_0x0132:
            if (r3 < r2) goto L_0x03d2
            r26.j()
            boolean r2 = r26.c()
            if (r2 != 0) goto L_0x0002
            r0 = r26
            int r0 = r0.M
            r2 = r0
            int r2 = r2 + 1
            r0 = r2
            r1 = r26
            r1.M = r0
            r0 = r26
            int r0 = r0.c
            r2 = r0
            r0 = r26
            int r0 = r0.d
            r3 = r0
            android.graphics.Bitmap$Config r4 = android.graphics.Bitmap.Config.RGB_565
            android.graphics.Bitmap r2 = android.graphics.Bitmap.createBitmap(r2, r3, r4)
            r0 = r2
            r1 = r26
            r1.x = r0
            r0 = r26
            int r0 = r0.c
            r2 = r0
            r0 = r26
            int r0 = r0.d
            r3 = r0
            int r2 = r2 * r3
            int[] r3 = new int[r2]
            r0 = r26
            int r0 = r0.D
            r2 = r0
            if (r2 <= 0) goto L_0x01cf
            r0 = r26
            int r0 = r0.D
            r2 = r0
            r4 = 3
            if (r2 != r4) goto L_0x0191
            r0 = r26
            int r0 = r0.M
            r2 = r0
            r4 = 2
            int r2 = r2 - r4
            if (r2 <= 0) goto L_0x03de
            r4 = 1
            int r2 = r2 - r4
            r0 = r26
            r1 = r2
            android.graphics.Bitmap r2 = r0.b(r1)
            r0 = r2
            r1 = r26
            r1.y = r0
        L_0x0191:
            r0 = r26
            android.graphics.Bitmap r0 = r0.y
            r2 = r0
            if (r2 == 0) goto L_0x01cf
            r0 = r26
            android.graphics.Bitmap r0 = r0.y
            r2 = r0
            r4 = 0
            r0 = r26
            int r0 = r0.c
            r5 = r0
            r6 = 0
            r7 = 0
            r0 = r26
            int r0 = r0.c
            r8 = r0
            r0 = r26
            int r0 = r0.d
            r9 = r0
            r2.getPixels(r3, r4, r5, r6, r7, r8, r9)
            r0 = r26
            int r0 = r0.D
            r2 = r0
            r4 = 2
            if (r2 != r4) goto L_0x01cf
            r2 = 0
            r0 = r26
            boolean r0 = r0.E
            r4 = r0
            if (r4 != 0) goto L_0x01c7
            r0 = r26
            int r0 = r0.l
            r2 = r0
        L_0x01c7:
            r4 = 0
        L_0x01c8:
            r0 = r26
            int r0 = r0.w
            r5 = r0
            if (r4 < r5) goto L_0x03e6
        L_0x01cf:
            r2 = 1
            r4 = 8
            r5 = 0
            r6 = 0
            r24 = r6
            r6 = r2
            r2 = r24
            r25 = r4
            r4 = r5
            r5 = r25
        L_0x01de:
            r0 = r26
            int r0 = r0.s
            r7 = r0
            if (r2 < r7) goto L_0x0409
            r0 = r26
            int r0 = r0.c
            r2 = r0
            r0 = r26
            int r0 = r0.d
            r4 = r0
            android.graphics.Bitmap$Config r5 = android.graphics.Bitmap.Config.RGB_565
            android.graphics.Bitmap r2 = android.graphics.Bitmap.createBitmap(r3, r2, r4, r5)
            r0 = r2
            r1 = r26
            r1.x = r0
            r0 = r26
            java.util.Vector r0 = r0.L
            r2 = r0
            com.adwo.adsdk.T r3 = new com.adwo.adsdk.T
            r0 = r26
            android.graphics.Bitmap r0 = r0.x
            r4 = r0
            r0 = r26
            int r0 = r0.F
            r5 = r0
            r3.<init>(r4, r5)
            r2.addElement(r3)
            r0 = r26
            boolean r0 = r0.E
            r2 = r0
            if (r2 == 0) goto L_0x0224
            r0 = r26
            int[] r0 = r0.i
            r2 = r0
            r0 = r26
            int r0 = r0.G
            r3 = r0
            r2[r3] = r11
        L_0x0224:
            r0 = r26
            int r0 = r0.C
            r2 = r0
            r0 = r2
            r1 = r26
            r1.D = r0
            r0 = r26
            int r0 = r0.p
            r2 = r0
            r0 = r2
            r1 = r26
            r1.t = r0
            r0 = r26
            int r0 = r0.q
            r2 = r0
            r0 = r2
            r1 = r26
            r1.u = r0
            r0 = r26
            int r0 = r0.r
            r2 = r0
            r0 = r2
            r1 = r26
            r1.v = r0
            r0 = r26
            int r0 = r0.s
            r2 = r0
            r0 = r2
            r1 = r26
            r1.w = r0
            r0 = r26
            android.graphics.Bitmap r0 = r0.x
            r2 = r0
            r0 = r2
            r1 = r26
            r1.y = r0
            r0 = r26
            int r0 = r0.k
            r2 = r0
            r0 = r2
            r1 = r26
            r1.l = r0
            r2 = 0
            r0 = r2
            r1 = r26
            r1.C = r0
            r2 = 0
            r0 = r2
            r1 = r26
            r1.E = r0
            r2 = 0
            r0 = r2
            r1 = r26
            r1.F = r0
            r2 = 0
            r0 = r2
            r1 = r26
            r1.h = r0
            goto L_0x0002
        L_0x0284:
            r3 = 0
            goto L_0x0046
        L_0x0287:
            r3 = 0
            goto L_0x0050
        L_0x028a:
            r0 = r26
            int[] r0 = r0.g
            r2 = r0
            r0 = r2
            r1 = r26
            r1.i = r0
            r0 = r26
            int r0 = r0.j
            r2 = r0
            r0 = r26
            int r0 = r0.G
            r3 = r0
            if (r2 != r3) goto L_0x0081
            r2 = 0
            r0 = r2
            r1 = r26
            r1.k = r0
            goto L_0x0081
        L_0x02a8:
            r0 = r26
            short[] r0 = r0.H
            r13 = r0
            r14 = 0
            r13[r12] = r14
            r0 = r26
            byte[] r0 = r0.f1I
            r13 = r0
            byte r14 = (byte) r12
            r13[r12] = r14
            int r12 = r12 + 1
            goto L_0x0119
        L_0x02bc:
            if (r8 != 0) goto L_0x0398
            r0 = r14
            r1 = r16
            if (r0 >= r1) goto L_0x02eb
            if (r13 != 0) goto L_0x02d1
            int r7 = r26.e()
            if (r7 <= 0) goto L_0x0131
            r13 = 0
            r24 = r13
            r13 = r7
            r7 = r24
        L_0x02d1:
            r0 = r26
            byte[] r0 = r0.A
            r20 = r0
            byte r20 = r20[r7]
            r0 = r20
            r0 = r0 & 255(0xff, float:3.57E-43)
            r20 = r0
            int r20 = r20 << r14
            int r12 = r12 + r20
            int r14 = r14 + 8
            int r7 = r7 + 1
            int r13 = r13 + -1
            goto L_0x012c
        L_0x02eb:
            r20 = r12 & r17
            int r12 = r12 >> r16
            int r14 = r14 - r16
            r0 = r20
            r1 = r18
            if (r0 > r1) goto L_0x0131
            r0 = r20
            r1 = r5
            if (r0 == r1) goto L_0x0131
            r0 = r20
            r1 = r4
            if (r0 != r1) goto L_0x031b
            int r15 = r3 + 1
            r16 = 1
            int r16 = r16 << r15
            r17 = 1
            int r16 = r16 - r17
            int r17 = r4 + 2
            r18 = -1
            r24 = r18
            r18 = r17
            r17 = r16
            r16 = r15
            r15 = r24
            goto L_0x012c
        L_0x031b:
            r21 = -1
            r0 = r15
            r1 = r21
            if (r0 != r1) goto L_0x033a
            r0 = r26
            byte[] r0 = r0.J
            r9 = r0
            int r15 = r8 + 1
            r0 = r26
            byte[] r0 = r0.f1I
            r21 = r0
            byte r21 = r21[r20]
            r9[r8] = r21
            r8 = r15
            r9 = r20
            r15 = r20
            goto L_0x012c
        L_0x033a:
            r0 = r20
            r1 = r18
            if (r0 != r1) goto L_0x0526
            r0 = r26
            byte[] r0 = r0.J
            r21 = r0
            int r22 = r8 + 1
            byte r9 = (byte) r9
            r21[r8] = r9
            r8 = r22
            r9 = r15
        L_0x034e:
            if (r9 > r4) goto L_0x03b4
            r0 = r26
            byte[] r0 = r0.f1I
            r21 = r0
            byte r9 = r21[r9]
            r9 = r9 & 255(0xff, float:3.57E-43)
            r21 = 4096(0x1000, float:5.74E-42)
            r0 = r18
            r1 = r21
            if (r0 >= r1) goto L_0x0131
            r0 = r26
            byte[] r0 = r0.J
            r21 = r0
            int r22 = r8 + 1
            r0 = r9
            byte r0 = (byte) r0
            r23 = r0
            r21[r8] = r23
            r0 = r26
            short[] r0 = r0.H
            r8 = r0
            short r15 = (short) r15
            r8[r18] = r15
            r0 = r26
            byte[] r0 = r0.f1I
            r8 = r0
            byte r15 = (byte) r9
            r8[r18] = r15
            int r8 = r18 + 1
            r15 = r8 & r17
            if (r15 != 0) goto L_0x0520
            r15 = 4096(0x1000, float:5.74E-42)
            if (r8 >= r15) goto L_0x0520
            int r15 = r16 + 1
            int r16 = r17 + r8
        L_0x038e:
            r17 = r16
            r18 = r8
            r16 = r15
            r8 = r22
            r15 = r20
        L_0x0398:
            int r8 = r8 + -1
            r0 = r26
            byte[] r0 = r0.K
            r20 = r0
            int r21 = r6 + 1
            r0 = r26
            byte[] r0 = r0.J
            r22 = r0
            byte r22 = r22[r8]
            r20[r6] = r22
            int r6 = r19 + 1
            r19 = r6
            r6 = r21
            goto L_0x012c
        L_0x03b4:
            r0 = r26
            byte[] r0 = r0.J
            r21 = r0
            int r22 = r8 + 1
            r0 = r26
            byte[] r0 = r0.f1I
            r23 = r0
            byte r23 = r23[r9]
            r21[r8] = r23
            r0 = r26
            short[] r0 = r0.H
            r8 = r0
            short r8 = r8[r9]
            r9 = r8
            r8 = r22
            goto L_0x034e
        L_0x03d2:
            r0 = r26
            byte[] r0 = r0.K
            r4 = r0
            r5 = 0
            r4[r3] = r5
            int r3 = r3 + 1
            goto L_0x0132
        L_0x03de:
            r2 = 0
            r0 = r2
            r1 = r26
            r1.y = r0
            goto L_0x0191
        L_0x03e6:
            r0 = r26
            int r0 = r0.u
            r5 = r0
            int r5 = r5 + r4
            r0 = r26
            int r0 = r0.c
            r6 = r0
            int r5 = r5 * r6
            r0 = r26
            int r0 = r0.t
            r6 = r0
            int r5 = r5 + r6
            r0 = r26
            int r0 = r0.v
            r6 = r0
            int r6 = r6 + r5
        L_0x03fe:
            if (r5 < r6) goto L_0x0404
            int r4 = r4 + 1
            goto L_0x01c8
        L_0x0404:
            r3[r5] = r2
            int r5 = r5 + 1
            goto L_0x03fe
        L_0x0409:
            r0 = r26
            boolean r0 = r0.n
            r7 = r0
            if (r7 == 0) goto L_0x051a
            r0 = r26
            int r0 = r0.s
            r7 = r0
            if (r4 < r7) goto L_0x041c
            int r6 = r6 + 1
            switch(r6) {
                case 2: goto L_0x0465;
                case 3: goto L_0x0467;
                case 4: goto L_0x046a;
                default: goto L_0x041c;
            }
        L_0x041c:
            int r7 = r4 + r5
            r24 = r7
            r7 = r6
            r6 = r5
            r5 = r24
        L_0x0424:
            r0 = r26
            int r0 = r0.q
            r8 = r0
            int r4 = r4 + r8
            r0 = r26
            int r0 = r0.d
            r8 = r0
            if (r4 >= r8) goto L_0x045e
            r0 = r26
            int r0 = r0.c
            r8 = r0
            int r4 = r4 * r8
            r0 = r26
            int r0 = r0.p
            r8 = r0
            int r8 = r8 + r4
            r0 = r26
            int r0 = r0.r
            r9 = r0
            int r9 = r9 + r8
            r0 = r26
            int r0 = r0.c
            r12 = r0
            int r12 = r12 + r4
            if (r12 >= r9) goto L_0x0517
            r0 = r26
            int r0 = r0.c
            r9 = r0
            int r4 = r4 + r9
        L_0x0451:
            r0 = r26
            int r0 = r0.r
            r9 = r0
            int r9 = r9 * r2
            r24 = r9
            r9 = r8
            r8 = r24
        L_0x045c:
            if (r9 < r4) goto L_0x046d
        L_0x045e:
            int r2 = r2 + 1
            r4 = r5
            r5 = r6
            r6 = r7
            goto L_0x01de
        L_0x0465:
            r4 = 4
            goto L_0x041c
        L_0x0467:
            r4 = 2
            r5 = 4
            goto L_0x041c
        L_0x046a:
            r4 = 1
            r5 = 2
            goto L_0x041c
        L_0x046d:
            r0 = r26
            byte[] r0 = r0.K
            r12 = r0
            int r13 = r8 + 1
            byte r8 = r12[r8]
            r8 = r8 & 255(0xff, float:3.57E-43)
            r0 = r26
            int[] r0 = r0.i
            r12 = r0
            r8 = r12[r8]
            if (r8 == 0) goto L_0x0483
            r3[r9] = r8
        L_0x0483:
            int r8 = r9 + 1
            r9 = r8
            r8 = r13
            goto L_0x045c
        L_0x0488:
            int r2 = r26.d()
            switch(r2) {
                case 249: goto L_0x0494;
                case 255: goto L_0x04d6;
                default: goto L_0x048f;
            }
        L_0x048f:
            r26.j()
            goto L_0x0002
        L_0x0494:
            r26.d()
            int r2 = r26.d()
            r3 = r2 & 28
            int r3 = r3 >> 2
            r0 = r3
            r1 = r26
            r1.C = r0
            r0 = r26
            int r0 = r0.C
            r3 = r0
            if (r3 != 0) goto L_0x04b1
            r3 = 1
            r0 = r3
            r1 = r26
            r1.C = r0
        L_0x04b1:
            r2 = r2 & 1
            if (r2 == 0) goto L_0x04d4
            r2 = 1
        L_0x04b6:
            r0 = r2
            r1 = r26
            r1.E = r0
            int r2 = r26.i()
            int r2 = r2 * 10
            r0 = r2
            r1 = r26
            r1.F = r0
            int r2 = r26.d()
            r0 = r2
            r1 = r26
            r1.G = r0
            r26.d()
            goto L_0x0002
        L_0x04d4:
            r2 = 0
            goto L_0x04b6
        L_0x04d6:
            r26.e()
            java.lang.String r2 = ""
            r3 = 0
            r24 = r3
            r3 = r2
            r2 = r24
        L_0x04e1:
            r4 = 11
            if (r2 < r4) goto L_0x04f2
            java.lang.String r2 = "NETSCAPE2.0"
            boolean r2 = r3.equals(r2)
            if (r2 == 0) goto L_0x050e
            r26.h()
            goto L_0x0002
        L_0x04f2:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r4.<init>(r3)
            r0 = r26
            byte[] r0 = r0.A
            r3 = r0
            byte r3 = r3[r2]
            char r3 = (char) r3
            java.lang.StringBuilder r3 = r4.append(r3)
            java.lang.String r3 = r3.toString()
            int r2 = r2 + 1
            goto L_0x04e1
        L_0x050e:
            r26.j()
            goto L_0x0002
        L_0x0513:
            r2 = 1
            r10 = r2
            goto L_0x0002
        L_0x0517:
            r4 = r9
            goto L_0x0451
        L_0x051a:
            r7 = r6
            r6 = r5
            r5 = r4
            r4 = r2
            goto L_0x0424
        L_0x0520:
            r15 = r16
            r16 = r17
            goto L_0x038e
        L_0x0526:
            r9 = r20
            goto L_0x034e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.S.f():void");
    }

    private boolean g() {
        String str = "";
        for (int i2 = 0; i2 < 6; i2++) {
            str = String.valueOf(str) + ((char) d());
        }
        if (!str.startsWith("GIF")) {
            this.b = 1;
            return false;
        }
        this.c = i();
        this.d = i();
        int d2 = d();
        this.e = (d2 & 128) != 0;
        this.f = 2 << (d2 & 7);
        this.j = d();
        d();
        if (this.e && !c()) {
            this.g = c(this.f);
            this.k = this.g[this.j];
        }
        return true;
    }

    private void h() {
        do {
            e();
            if (this.A[0] == 1) {
                byte[] bArr = this.A;
                byte[] bArr2 = this.A;
            }
            if (this.B <= 0) {
                return;
            }
        } while (!c());
    }

    private int i() {
        return d() | (d() << 8);
    }

    private void j() {
        do {
            e();
            if (this.B <= 0) {
                return;
            }
        } while (!c());
    }
}
