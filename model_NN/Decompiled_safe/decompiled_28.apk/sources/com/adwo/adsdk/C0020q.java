package com.adwo.adsdk;

import android.view.MotionEvent;
import android.view.View;

/* renamed from: com.adwo.adsdk.q  reason: case insensitive filesystem */
final class C0020q implements View.OnTouchListener {
    private /* synthetic */ C0013j a;

    C0020q(C0013j jVar) {
        this.a = jVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.a.r.onTouchEvent(motionEvent);
    }
}
