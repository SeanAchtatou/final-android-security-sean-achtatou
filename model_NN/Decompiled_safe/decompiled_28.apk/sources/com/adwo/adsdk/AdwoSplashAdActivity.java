package com.adwo.adsdk;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;

public class AdwoSplashAdActivity extends Activity implements y {
    private static String[] b = {"explode", "toptobottom", "bottomtotop"};
    /* access modifiers changed from: private */
    public static boolean c;
    C0013j a;
    private Handler d = new M(this);

    public void onCreate(Bundle bundle) {
        boolean z;
        boolean z2;
        boolean z3;
        long j;
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        try {
            FSAd fSAd = (FSAd) getIntent().getParcelableExtra("FSAd");
            int i = 0;
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                long j2 = extras.getLong("transitionTime", 600);
                extras.getString("overlayTransition");
                i = extras.getInt("shouldResizeOverlay", 0);
                boolean z4 = extras.getBoolean("shouldShowTitlebar", false);
                extras.getString("overlayTitle");
                extras.getBoolean("shouldShowBottomBar", true);
                boolean z5 = extras.getBoolean("shouldEnableBottomBar", true);
                z = extras.getBoolean("shouldMakeOverlayTransparent", false);
                z2 = z5;
                z3 = z4;
                j = j2;
            } else {
                z = false;
                z2 = true;
                z3 = true;
                j = 600;
            }
            this.a = new C0013j(this, i, j, b[(int) (Math.random() * 3.0d)], z3, z2, z);
            setContentView(this.a);
            if (fSAd != null) {
                if (fSAd.i != null) {
                    if (fSAd.i.startsWith("http")) {
                        this.a.c("fsAd.htmlContent");
                    } else {
                        this.a.d(fSAd.i);
                    }
                }
                if (fSAd.d != null) {
                    this.a.a(fSAd.d);
                }
            } else {
                this.a.c("http://www.adwo.com");
            }
            this.a.a(this);
            if (FSAdUtil.a()) {
                setRequestedOrientation(1);
            } else {
                setRequestedOrientation(0);
            }
            c = false;
            this.d.sendEmptyMessageDelayed(1, 5000);
        } catch (Throwable th) {
            th.printStackTrace();
            finish();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        if (c) {
            return super.onKeyDown(i, keyEvent);
        }
        return false;
    }

    public final void a() {
        setResult(998);
        finish();
    }
}
