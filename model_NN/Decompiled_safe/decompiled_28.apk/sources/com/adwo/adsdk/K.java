package com.adwo.adsdk;

import android.view.animation.DecelerateInterpolator;

final class K implements Runnable {
    /* access modifiers changed from: private */
    public C0005b a;
    /* access modifiers changed from: private */
    public C0005b b;
    private N c;
    /* access modifiers changed from: private */
    public /* synthetic */ AdwoAdView d;

    public K(AdwoAdView adwoAdView, C0005b bVar, N n) {
        this.d = adwoAdView;
        this.a = bVar;
        this.c = n;
    }

    public final void run() {
        this.b = this.d.c;
        if (this.b != null) {
            this.b.setVisibility(8);
            this.b.c();
        }
        this.a.setVisibility(0);
        V v = new V(90.0f, 0.0f, ((float) this.d.getWidth()) / 2.0f, ((float) this.d.getHeight()) / 2.0f, 0.0f, 0.0f, -0.4f * ((float) this.d.getWidth()), false, this.c);
        v.setDuration(500);
        v.setFillAfter(true);
        v.setInterpolator(new DecelerateInterpolator());
        v.setAnimationListener(new L(this));
        this.d.startAnimation(v);
    }
}
