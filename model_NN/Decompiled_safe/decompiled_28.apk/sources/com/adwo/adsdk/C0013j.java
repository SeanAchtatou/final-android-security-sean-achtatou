package com.adwo.adsdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.animation.AlphaAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

/* renamed from: com.adwo.adsdk.j  reason: case insensitive filesystem */
final class C0013j extends FrameLayout implements GestureDetector.OnGestureListener {
    private static String q;
    protected WebView a;
    private NotificationManager b;
    private Notification c = null;
    private volatile boolean d = false;
    /* access modifiers changed from: private */
    public Button e;
    /* access modifiers changed from: private */
    public Button f;
    private Button g;
    private RelativeLayout h;
    private RelativeLayout i;
    private Drawable j;
    private Drawable k;
    private Drawable l;
    private ImageView m;
    private WeakReference n;
    /* access modifiers changed from: private */
    public y o;
    private String p;
    /* access modifiers changed from: private */
    public GestureDetector r;

    public final void a(y yVar) {
        this.o = yVar;
    }

    public final String a() {
        return this.p;
    }

    public final void a(String str) {
        if (str.indexOf("/a/") > 1) {
            this.p = str.replace("/a/", "/");
        } else {
            this.p = str;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean b(java.lang.String r4) {
        /*
            r0 = 0
            r1 = 0
            java.net.URL r2 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0029, IOException -> 0x0030, all -> 0x003d }
            r2.<init>(r4)     // Catch:{ MalformedURLException -> 0x0029, IOException -> 0x0030, all -> 0x003d }
            java.net.URLConnection r4 = r2.openConnection()     // Catch:{ MalformedURLException -> 0x0029, IOException -> 0x0030, all -> 0x003d }
            java.net.HttpURLConnection r4 = (java.net.HttpURLConnection) r4     // Catch:{ MalformedURLException -> 0x0029, IOException -> 0x0030, all -> 0x003d }
            int r1 = com.adwo.adsdk.P.b     // Catch:{ MalformedURLException -> 0x004d, IOException -> 0x004a, all -> 0x0044 }
            r4.setConnectTimeout(r1)     // Catch:{ MalformedURLException -> 0x004d, IOException -> 0x004a, all -> 0x0044 }
            int r1 = com.adwo.adsdk.P.b     // Catch:{ MalformedURLException -> 0x004d, IOException -> 0x004a, all -> 0x0044 }
            r4.setReadTimeout(r1)     // Catch:{ MalformedURLException -> 0x004d, IOException -> 0x004a, all -> 0x0044 }
            r4.connect()     // Catch:{ MalformedURLException -> 0x004d, IOException -> 0x004a, all -> 0x0044 }
            int r1 = r4.getResponseCode()     // Catch:{ MalformedURLException -> 0x004d, IOException -> 0x004a, all -> 0x0044 }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 != r2) goto L_0x0023
            r0 = 1
        L_0x0023:
            if (r4 == 0) goto L_0x0028
            r4.disconnect()
        L_0x0028:
            return r0
        L_0x0029:
            r2 = move-exception
        L_0x002a:
            if (r1 == 0) goto L_0x0028
            r1.disconnect()
            goto L_0x0028
        L_0x0030:
            r2 = move-exception
            r3 = r2
            r2 = r1
            r1 = r3
        L_0x0034:
            r1.printStackTrace()     // Catch:{ all -> 0x0047 }
            if (r2 == 0) goto L_0x0028
            r2.disconnect()
            goto L_0x0028
        L_0x003d:
            r0 = move-exception
        L_0x003e:
            if (r1 == 0) goto L_0x0043
            r1.disconnect()
        L_0x0043:
            throw r0
        L_0x0044:
            r0 = move-exception
            r1 = r4
            goto L_0x003e
        L_0x0047:
            r0 = move-exception
            r1 = r2
            goto L_0x003e
        L_0x004a:
            r1 = move-exception
            r2 = r4
            goto L_0x0034
        L_0x004d:
            r1 = move-exception
            r1 = r4
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0013j.b(java.lang.String):boolean");
    }

    C0013j(Activity activity, int i2, long j2, String str, boolean z, boolean z2, boolean z3) {
        super(activity);
        new C0014k(this);
        this.p = null;
        this.r = null;
        this.n = new WeakReference(activity);
        setId(15062);
        Activity activity2 = (Activity) this.n.get();
        if (activity2 != null) {
            this.b = (NotificationManager) activity2.getSystemService("notification");
            this.c = new Notification(17301598, "程序下载完成", System.currentTimeMillis());
            this.c.defaults = 1;
            activity2.setTheme(16973840);
            setWillNotDraw(false);
            float f2 = activity2.getResources().getDisplayMetrics().density;
            Integer valueOf = Integer.valueOf((int) (0.0625f * f2 * ((float) i2)));
            setPadding(valueOf.intValue(), valueOf.intValue(), valueOf.intValue(), valueOf.intValue());
            this.i = new RelativeLayout(activity2);
            this.i.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
            Activity activity3 = (Activity) this.n.get();
            if (activity3 != null) {
                this.a = new WebView(activity3);
                this.a.setId(200);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
                this.a.setLayoutParams(layoutParams);
                this.a.setWebViewClient(new B(this));
                this.a.setWebChromeClient(new A(this));
                this.a.addJavascriptInterface(new z(this, activity3), "interface");
                this.a.requestFocusFromTouch();
                this.a.requestFocusFromTouch();
                this.r = new GestureDetector(this);
                this.a.setOnTouchListener(new C0020q(this));
                CookieManager.getInstance().setAcceptCookie(true);
                WebSettings settings = this.a.getSettings();
                settings.setJavaScriptEnabled(true);
                settings.setDefaultTextEncodingName("utf-8");
                settings.setJavaScriptCanOpenWindowsAutomatically(true);
                settings.setGeolocationEnabled(true);
                settings.setBuiltInZoomControls(true);
                settings.setAllowFileAccess(true);
                settings.setSupportZoom(true);
                settings.setBuiltInZoomControls(true);
                settings.setPluginsEnabled(true);
                settings.setSaveFormData(true);
                settings.setLightTouchEnabled(true);
                q = settings.getUserAgentString();
                if (z) {
                    layoutParams.addRule(3, 100);
                }
                this.i.addView(this.a);
                if (z3) {
                    this.a.setBackgroundColor(0);
                    this.i.setBackgroundColor(0);
                } else {
                    this.a.setBackgroundColor(-1);
                    this.i.setBackgroundColor(-1);
                }
                this.h = new RelativeLayout(activity3);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, (int) (40.0f * f2));
                layoutParams2.addRule(12);
                this.h.setBackgroundColor(-3355444);
                this.h.setId(300);
                this.g = new Button(activity3);
                this.g.setBackgroundColor(-16777216);
                AssetManager assets = activity3.getAssets();
                try {
                    this.l = Drawable.createFromStream(assets.open("adwo_x.png"), "adwo_x.png");
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                c(z2);
                this.m = new ImageView(activity3);
                try {
                    this.m.setBackgroundDrawable(Drawable.createFromStream(assets.open("adwo_close.png"), "adwo_close.png"));
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams((int) (31.0f * f2), (int) (31.0f * f2));
                layoutParams3.addRule(11);
                layoutParams3.addRule(10);
                layoutParams3.setMargins(0, (int) (10.0f * f2), (int) (10.0f * f2), 0);
                this.m.setOnClickListener(new C0021r(this));
                this.i.addView(this.m, layoutParams3);
                a(4);
                RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams((int) (35.0f * f2), (int) (35.0f * f2));
                layoutParams4.addRule(11);
                layoutParams4.addRule(15);
                this.h.addView(this.g, layoutParams4);
                this.e = new Button(activity3);
                try {
                    this.j = Drawable.createFromStream(assets.open("adwo_right_arrow.png"), "adwo_right_arrow.png");
                } catch (IOException e4) {
                    this.e.setBackgroundColor(-3355444);
                    this.e.setText(">>");
                    this.e.setTextColor(-16777216);
                    e4.printStackTrace();
                }
                a(z2);
                this.f = new Button(activity3);
                try {
                    this.k = Drawable.createFromStream(assets.open("adwo_left_arrow.png"), "adwo_left_arrow.png");
                } catch (IOException e5) {
                    this.f.setBackgroundColor(-3355444);
                    this.f.setText("<<");
                    this.f.setTextColor(-16777216);
                    e5.printStackTrace();
                }
                this.f.setId(301);
                b(z2);
                this.f.setId(302);
                RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams((int) (48.0f * f2), (int) (31.0f * f2));
                layoutParams5.addRule(15);
                this.h.addView(this.f, layoutParams5);
                this.e.setId(303);
                RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams((int) (48.0f * f2), (int) (f2 * 31.0f));
                layoutParams6.addRule(1, this.f.getId());
                layoutParams6.addRule(15);
                this.h.addView(this.e, layoutParams6);
                this.f.setVisibility(4);
                this.e.setVisibility(4);
                this.i.addView(this.h, layoutParams2);
                addView(this.i);
                String str2 = str == null ? "toptobottom" : str;
                if (str2.equals("toptobottom")) {
                    TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
                    translateAnimation.setDuration(j2);
                    translateAnimation.setAnimationListener(new C0022s(this));
                    startAnimation(translateAnimation);
                } else if (str2.equals("explode")) {
                    ScaleAnimation scaleAnimation = new ScaleAnimation(1.1f, 0.9f, 0.1f, 0.9f, 1, 0.5f, 1, 0.5f);
                    scaleAnimation.setDuration(j2);
                    scaleAnimation.setAnimationListener(new C0023t(this));
                    startAnimation(scaleAnimation);
                } else {
                    TranslateAnimation translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
                    translateAnimation2.setDuration(j2);
                    translateAnimation2.setAnimationListener(new C0024u(this));
                    startAnimation(translateAnimation2);
                }
                this.h.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(int i2) {
        this.m.setVisibility(i2);
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        new Rect(new Rect(canvas.getClipBounds())).inset(0, 0);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
    }

    /* access modifiers changed from: package-private */
    public final void c(String str) {
        new Thread(new C0025v(this, str)).start();
    }

    /* access modifiers changed from: package-private */
    public final void d(String str) {
        new Thread(new w(this, str)).start();
    }

    private void a(boolean z) {
        if (this.e != null && z) {
            this.e.setBackgroundDrawable(this.j);
            this.e.setOnClickListener(new x(this));
            this.e.setEnabled(true);
        }
    }

    private void b(boolean z) {
        if (this.f != null && z) {
            this.f.setBackgroundDrawable(this.k);
            this.f.setOnClickListener(new C0015l(this));
            this.f.setEnabled(true);
        }
    }

    private void c(boolean z) {
        if (this.g != null && z) {
            this.g.setBackgroundDrawable(this.l);
            this.g.setOnClickListener(new C0016m(this));
            this.g.setEnabled(true);
        }
    }

    static /* synthetic */ void a(C0013j jVar, boolean z) {
        Activity activity = (Activity) jVar.n.get();
        if (activity == null) {
            return;
        }
        if (1 != 0) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(200);
            activity.finish();
            jVar.startAnimation(alphaAnimation);
            return;
        }
        activity.finish();
    }

    /* access modifiers changed from: package-private */
    public final Activity b() {
        return (Activity) this.n.get();
    }

    /* access modifiers changed from: protected */
    public final void c() {
        if (this.h != null) {
            this.h.setVisibility(0);
            TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
            translateAnimation.setDuration(300);
            translateAnimation.setAnimationListener(new C0017n(this));
            this.h.startAnimation(translateAnimation);
            c(true);
            a(true);
            b(true);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, Activity activity) {
        String str2;
        CharSequence charSequence;
        String substring = str.substring(str.lastIndexOf("/") + 1);
        String externalStorageState = Environment.getExternalStorageState();
        if (!externalStorageState.equals("mounted")) {
            if (externalStorageState.equals("shared")) {
                str2 = "SD 卡正忙。要允许下载，请在通知中选择\"关闭USB 存储\"";
                charSequence = "SD 卡不可用";
            } else {
                str2 = "需要有 SD 卡才能下载" + substring;
                charSequence = "无 SD 卡";
            }
            new AlertDialog.Builder(b()).setTitle(charSequence).setIcon(17301543).setMessage(str2).setPositiveButton("OK", (DialogInterface.OnClickListener) null).show();
            return;
        }
        activity.runOnUiThread(new C0018o(this, activity, substring));
        if (!this.d) {
            this.d = true;
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(str);
            httpGet.setHeader("User-Agent", q);
            try {
                HttpResponse execute = defaultHttpClient.execute(httpGet);
                if (execute.getStatusLine().getStatusCode() == 200) {
                    InputStream content = execute.getEntity().getContent();
                    File file = new File(Environment.getExternalStorageDirectory() + "/adwo/");
                    if (!file.exists()) {
                        file.mkdir();
                    }
                    File file2 = new File(Environment.getExternalStorageDirectory() + "/adwo/", substring);
                    if (file2.exists() || file2.createNewFile()) {
                        FileOutputStream fileOutputStream = new FileOutputStream(file2);
                        while (true) {
                            int read = content.read();
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream.write(read);
                        }
                        content.close();
                        fileOutputStream.close();
                        Uri fromFile = Uri.fromFile(file2);
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setFlags(268435456);
                        intent.setDataAndType(fromFile, "application/vnd.android.package-archive");
                        PendingIntent activity2 = PendingIntent.getActivity(activity, 0, intent, 268435456);
                        if (!(this.c == null || activity2 == null)) {
                            this.c.setLatestEventInfo(activity, String.valueOf(substring) + "下载完成", String.valueOf(substring) + "下载完成,请安装使用", activity2);
                        }
                        this.b.notify(0, this.c);
                    } else {
                        return;
                    }
                }
                this.d = false;
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (Exception e3) {
            } finally {
                this.d = false;
            }
        }
    }

    static /* synthetic */ void a(C0013j jVar, String str, Activity activity) {
        String str2;
        CharSequence charSequence;
        String substring = str.substring(str.lastIndexOf("/") + 1);
        String externalStorageState = Environment.getExternalStorageState();
        if (!externalStorageState.equals("mounted")) {
            if (externalStorageState.equals("shared")) {
                str2 = "SD 卡正忙。要允许下载，请在通知中选择\"关闭USB 存储\"";
                charSequence = "SD 卡不可用";
            } else {
                str2 = "需要有 SD 卡才能下载" + substring;
                charSequence = "无 SD 卡";
            }
            new AlertDialog.Builder(jVar.b()).setTitle(charSequence).setIcon(17301543).setMessage(str2).setPositiveButton("OK", (DialogInterface.OnClickListener) null).show();
            return;
        }
        activity.runOnUiThread(new C0019p(jVar, activity, substring));
        if (!jVar.d) {
            jVar.d = true;
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(str);
            httpGet.setHeader("User-Agent", q);
            try {
                HttpResponse execute = defaultHttpClient.execute(httpGet);
                if (execute.getStatusLine().getStatusCode() == 200) {
                    InputStream content = execute.getEntity().getContent();
                    File file = new File(Environment.getExternalStorageDirectory() + "/adwo/");
                    if (!file.exists()) {
                        file.mkdir();
                    }
                    File file2 = new File(Environment.getExternalStorageDirectory() + "/adwo/", substring);
                    if (file2.exists() || file2.createNewFile()) {
                        FileOutputStream fileOutputStream = new FileOutputStream(file2);
                        while (true) {
                            int read = content.read();
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream.write(read);
                        }
                        content.close();
                        fileOutputStream.close();
                        Uri fromFile = Uri.fromFile(file2);
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setFlags(268435456);
                        intent.setDataAndType(fromFile, "image/bmp");
                        intent.setDataAndType(fromFile, "image/gif");
                        intent.setDataAndType(fromFile, "image/jpeg");
                        PendingIntent activity2 = PendingIntent.getActivity(activity, 0, intent, 268435456);
                        if (!(jVar.c == null || activity2 == null)) {
                            jVar.c.setLatestEventInfo(activity, String.valueOf(substring) + "下载完成", String.valueOf(substring) + "下载完成,请安装使用", activity2);
                        }
                        jVar.b.notify(0, jVar.c);
                    } else {
                        return;
                    }
                }
                jVar.d = false;
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (Exception e3) {
            } finally {
                jVar.d = false;
            }
        }
    }

    public final boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        if (motionEvent.getX() - motionEvent2.getX() > 120.0f && Math.abs(f2) > 200.0f) {
            this.a.goBack();
            return false;
        } else if (motionEvent2.getX() - motionEvent.getX() <= 120.0f || Math.abs(f2) <= 200.0f) {
            return false;
        } else {
            this.a.goForward();
            return false;
        }
    }

    public final void onLongPress(MotionEvent motionEvent) {
    }

    public final boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    public final void onShowPress(MotionEvent motionEvent) {
    }

    public final boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }
}
