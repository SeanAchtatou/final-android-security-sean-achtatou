package com.casee.apptrack;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.zip.Adler32;
import java.util.zip.CheckedInputStream;

public class NotifyUtil {
    Context a;
    AssetManager b;

    public NotifyUtil(Context context) {
        this.a = context;
        this.b = context.getAssets();
    }

    private String a(Map map) {
        Iterator it = map.keySet().iterator();
        StringBuffer stringBuffer = new StringBuffer();
        boolean z = true;
        while (true) {
            boolean z2 = z;
            if (!it.hasNext()) {
                return stringBuffer.toString();
            }
            String str = (String) it.next();
            String str2 = (String) map.get(str);
            if (str2 != null) {
                String encode = URLEncoder.encode(str2);
                if (z2) {
                    z2 = false;
                } else {
                    stringBuffer.append("&");
                }
                stringBuffer.append(str);
                stringBuffer.append(XmlConstant.EQUAL);
                stringBuffer.append(encode);
            }
            z = z2;
        }
    }

    private HttpURLConnection a(String str, String str2) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setRequestProperty("User-Agent", "Android-CASEE-PTSDK");
        httpURLConnection.setConnectTimeout(5000);
        httpURLConnection.setReadTimeout(5000);
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setDoOutput(true);
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(httpURLConnection.getOutputStream()));
        try {
            bufferedWriter.write(str2);
            return httpURLConnection;
        } finally {
            bufferedWriter.close();
        }
    }

    private File c(String str) {
        return new File(this.a.getDir("casee", 0), str);
    }

    private long d(String str) {
        try {
            CheckedInputStream checkedInputStream = new CheckedInputStream(new ByteArrayInputStream(str.getBytes("utf-8")), new Adler32());
            byte[] bArr = new byte[128];
            long j = 0;
            while (checkedInputStream.read(bArr) >= 0) {
                j = checkedInputStream.getChecksum().getValue();
            }
            return j;
        } catch (Exception e) {
            Log.e("CASEE-TA", e.getMessage());
            return 0;
        }
    }

    public synchronized Properties a(String str) throws IOException {
        Properties properties;
        properties = new Properties();
        File c = c(str);
        if (c.exists()) {
            FileInputStream fileInputStream = new FileInputStream(c);
            try {
                properties.load(fileInputStream);
            } finally {
                fileInputStream.close();
            }
        }
        return properties;
    }

    public synchronized void a(String str, Properties properties) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(c(str));
        try {
            properties.save(fileOutputStream, null);
            fileOutputStream.close();
        } catch (Throwable th) {
            fileOutputStream.close();
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x004c A[SYNTHETIC, Splitter:B:25:0x004c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r7, java.util.Map r8) {
        /*
            r6 = this;
            java.lang.String r0 = r6.a(r8)
            long r1 = r6.d(r0)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r3 = r3.append(r7)
            java.lang.String r4 = "?cs="
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r2 = 0
            r3 = 1
            java.net.HttpURLConnection r0 = r6.a(r1, r0)     // Catch:{ Throwable -> 0x0032, all -> 0x0048 }
            r0.getResponseCode()     // Catch:{ Throwable -> 0x0059, all -> 0x0052 }
            if (r0 == 0) goto L_0x0060
            r0.disconnect()     // Catch:{ Throwable -> 0x002f }
            r0 = r3
        L_0x002e:
            return r0
        L_0x002f:
            r0 = move-exception
            r0 = r3
            goto L_0x002e
        L_0x0032:
            r0 = move-exception
            r1 = r2
        L_0x0034:
            r2 = 0
            java.lang.String r3 = "CASEE-TA"
            java.lang.String r4 = r0.getMessage()     // Catch:{ all -> 0x0057 }
            android.util.Log.e(r3, r4, r0)     // Catch:{ all -> 0x0057 }
            if (r1 == 0) goto L_0x005e
            r1.disconnect()     // Catch:{ Throwable -> 0x0045 }
            r0 = r2
            goto L_0x002e
        L_0x0045:
            r0 = move-exception
            r0 = r2
            goto L_0x002e
        L_0x0048:
            r0 = move-exception
            r1 = r2
        L_0x004a:
            if (r1 == 0) goto L_0x004f
            r1.disconnect()     // Catch:{ Throwable -> 0x0050 }
        L_0x004f:
            throw r0
        L_0x0050:
            r1 = move-exception
            goto L_0x004f
        L_0x0052:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x004a
        L_0x0057:
            r0 = move-exception
            goto L_0x004a
        L_0x0059:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0034
        L_0x005e:
            r0 = r2
            goto L_0x002e
        L_0x0060:
            r0 = r3
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.casee.apptrack.NotifyUtil.a(java.lang.String, java.util.Map):boolean");
    }

    public Properties b(String str) throws IOException {
        InputStream open = this.a.getAssets().open(str);
        Properties properties = new Properties();
        try {
            properties.load(open);
            return properties;
        } finally {
            open.close();
        }
    }
}
