package com.casee.adsdk;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Handler;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.domob.android.ads.DomobAdManager;
import com.madhouse.android.ads.AdView;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class i implements View.OnClickListener {
    /* access modifiers changed from: private */
    public b A;
    /* access modifiers changed from: private */
    public b B;
    /* access modifiers changed from: private */
    public a C;
    /* access modifiers changed from: private */
    public String D;
    private int E;
    /* access modifiers changed from: private */
    public boolean F = true;
    /* access modifiers changed from: private */
    public ArrayList G;
    /* access modifiers changed from: private */
    public RelativeLayout H;

    /* renamed from: I  reason: collision with root package name */
    private ImageButton f3I;
    /* access modifiers changed from: private */
    public int J = 0;
    /* access modifiers changed from: private */
    public Handler K = new t(this);
    /* access modifiers changed from: private */
    public Context a;
    /* access modifiers changed from: private */
    public o b;
    /* access modifiers changed from: private */
    public CaseeAdView c;
    private final int d = 1;
    private final int e = 2;
    private final String f = "http://myapk.cn/sdkapi.php";
    /* access modifiers changed from: private */
    public int g = 700;
    /* access modifiers changed from: private */
    public WebView h = null;
    private RelativeLayout i = null;
    /* access modifiers changed from: private */
    public ImageView j;
    private ImageView k;
    private ImageView l;
    private ImageView m;
    private ImageView n;
    private ImageView o;
    private ImageView p;
    /* access modifiers changed from: private */
    public ImageView q;
    /* access modifiers changed from: private */
    public RelativeLayout r;
    /* access modifiers changed from: private */
    public RelativeLayout s;
    /* access modifiers changed from: private */
    public RelativeLayout t;
    /* access modifiers changed from: private */
    public b u;
    /* access modifiers changed from: private */
    public b v;
    /* access modifiers changed from: private */
    public b w;
    /* access modifiers changed from: private */
    public b x;
    /* access modifiers changed from: private */
    public b y;
    /* access modifiers changed from: private */
    public b z;

    class a extends Dialog {
        private Activity b;
        private Point c;
        /* access modifiers changed from: private */
        public int d;
        private int e;
        private int f = 4;
        private boolean g;
        /* access modifiers changed from: private */
        public boolean h = false;

        public a(Context context) {
            super(context, 16973840);
            setOwnerActivity((Activity) context);
            this.b = (Activity) context;
        }

        private void a() {
            if (this.d == 0) {
                i.this.r.setVisibility(8);
                i.this.s.setVisibility(0);
            } else if (this.d == 4) {
                i.this.r.setVisibility(0);
                i.this.s.setVisibility(8);
            } else {
                i.this.r.setVisibility(0);
                i.this.s.setVisibility(0);
            }
        }

        private void a(int i, int i2) {
            int c2 = i.this.c((Activity) i.this.a);
            TranslateAnimation translateAnimation = new TranslateAnimation((float) ((-i) * c2), (float) (c2 * (-i2)), 0.0f, 0.0f);
            translateAnimation.setDuration(300);
            translateAnimation.setInterpolator(new AccelerateInterpolator(0.5f));
            translateAnimation.setFillAfter(true);
            i.this.H.startAnimation(translateAnimation);
        }

        private void b() {
            if (i.this.H.getVisibility() != 0) {
                i.this.H.setVisibility(0);
                i.this.s.setVisibility(0);
                this.d = 0;
                i.this.h.setFocusable(false);
                i.this.h.setClickable(false);
                return;
            }
            i.this.H.clearAnimation();
            i.this.t.setVisibility(8);
            i.this.t.clearAnimation();
            i.this.H.setVisibility(8);
            i.this.r.setVisibility(8);
            i.this.s.setVisibility(8);
            i.this.h.setFocusable(true);
            i.this.h.setClickable(true);
        }

        /* access modifiers changed from: protected */
        public void onStop() {
            super.onStop();
            RelativeLayout unused = i.this.H = (RelativeLayout) null;
            e.a = false;
            this.h = false;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public boolean onTouchEvent(MotionEvent motionEvent) {
            boolean z;
            Point point = new Point((int) motionEvent.getX(), (int) motionEvent.getY());
            switch (motionEvent.getAction()) {
                case 0:
                    this.g = false;
                    this.c = point;
                    if (i.this.u.a(point)) {
                        if (i.this.h.canGoBack()) {
                            i.this.h.goBack();
                            i.this.f();
                            z = true;
                            break;
                        }
                    } else if (i.this.v.a(point)) {
                        if (i.this.h.canGoForward()) {
                            i.this.h.goForward();
                            i.this.f();
                            z = true;
                            break;
                        }
                    } else if (i.this.w.a(point)) {
                        i.this.h.reload();
                        z = true;
                        break;
                    } else if (i.this.x.a(point)) {
                        try {
                            Intent intent = new Intent("android.intent.action.SEARCH", Uri.parse(i.this.D));
                            intent.putExtra("query", i.this.D);
                            i.this.a.startActivity(intent);
                            z = true;
                            break;
                        } catch (ActivityNotFoundException e2) {
                            try {
                                i.this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(i.this.D)));
                            } catch (ActivityNotFoundException e3) {
                            }
                            z = true;
                            break;
                        }
                    } else if (i.this.y.a(point)) {
                        i.this.e(this.b);
                        i.this.K.sendEmptyMessageDelayed(1, (long) i.this.g);
                        z = true;
                        break;
                    } else if (i.this.z.a(point)) {
                        if (this.h && i.this.q != null) {
                            i.this.q.clearAnimation();
                            if (i.this.H == null) {
                                if (i.this.G != null && i.this.G.size() > 0) {
                                    i.this.b();
                                    z = true;
                                    break;
                                } else {
                                    ArrayList unused = i.this.G = (ArrayList) null;
                                    i.this.d();
                                    z = true;
                                    break;
                                }
                            } else {
                                b();
                                z = true;
                                break;
                            }
                        }
                        z = false;
                        break;
                    } else {
                        if (!i.this.A.a(point) && i.this.H != null && i.this.H.getVisibility() == 0 && i.this.B.a(point)) {
                            b();
                            z = true;
                            break;
                        }
                        z = false;
                    }
                    z = true;
                    break;
                case 1:
                    if (!this.g && i.this.H != null && i.this.H.getVisibility() == 0 && i.this.A.a(point)) {
                        i.this.c();
                        z = false;
                        break;
                    }
                    z = false;
                    break;
                case 2:
                    if (!(this.c == null || point == null || (Math.abs(this.c.x - point.x) <= 10 && Math.abs(this.c.y - point.y) <= 10))) {
                        this.g = true;
                    }
                    if (i.this.H != null && i.this.H.getVisibility() == 0 && i.this.A.a(point)) {
                        if (this.c.x - point.x <= 50) {
                            z = false;
                        } else if (this.d >= this.f) {
                            z = false;
                            break;
                        } else {
                            this.e = this.d + 1;
                            a(this.d, this.e);
                            this.d++;
                            a();
                            this.c = point;
                            z = true;
                        }
                        if (point.x - this.c.x > 50 && this.d > 0) {
                            this.e = this.d - 1;
                            a(this.d, this.e);
                            this.d--;
                            a();
                            this.c = point;
                            z = true;
                            break;
                        }
                    }
                    z = false;
                    break;
                default:
                    z = false;
                    break;
            }
            return z ? z : super.onTouchEvent(motionEvent);
        }
    }

    class b {
        public int a;
        public int b;
        public int c;
        public int d;
        public Point e;

        public b(int i, int i2, int i3, int i4) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
            this.e = new Point(i, i2);
        }

        public boolean a(Point point) {
            return this.a <= point.x && this.b <= point.y && point.x <= this.a + this.c && point.y <= this.b + this.d;
        }
    }

    i(Context context, o oVar, CaseeAdView caseeAdView) {
        this.a = context;
        this.b = oVar;
        this.c = caseeAdView;
        e.a = false;
        a(context, caseeAdView);
    }

    private void a(int i2, int i3, int i4, int i5) {
        ImageView imageView = new ImageView(this.a);
        imageView.setBackgroundDrawable(new BitmapDrawable((Bitmap) ((HashMap) this.G.get(i4)).get("bitmap")));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((this.E * 150) / 100, (this.E * 150) / 100);
        layoutParams.setMargins((i2 * i4) + (this.E / 3), (this.E * 40) / 100, 0, 0);
        this.H.addView(imageView, layoutParams);
        TextView textView = new TextView(this.a);
        String str = (String) ((HashMap) this.G.get(i4)).get("title");
        if (str.length() > 8) {
            str = str.substring(0, 7);
        }
        if (str.indexOf(XmlConstant.SINGLE_SPACE) != -1) {
            str = str.substring(0, str.indexOf(XmlConstant.SINGLE_SPACE));
        }
        textView.setText(str);
        textView.setTextSize(18.0f);
        textView.setTextColor(-16777216);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.setMargins((i2 * i4) + (this.E * 2), this.E / 4, 0, 0);
        this.H.addView(textView, layoutParams2);
        TextView textView2 = new TextView(this.a);
        String str2 = (String) ((HashMap) this.G.get(i4)).get("size");
        if (str2.length() > 10) {
            str2 = str2.substring(0, 9);
        }
        textView2.setText(str2);
        textView2.setTextSize(15.0f);
        textView2.setTextColor(-16777216);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.setMargins(((i4 + 1) * i2) - (this.E * 4), this.E / 4, 0, 0);
        this.H.addView(textView2, layoutParams3);
        ImageButton imageButton = new ImageButton(this.a);
        if (i4 == 0) {
            this.f3I = imageButton;
        }
        Bitmap decodeStream = BitmapFactory.decodeStream(e.class.getResourceAsStream("webview_download_icon_up.png"));
        imageButton.setBackgroundDrawable(new BitmapDrawable(decodeStream));
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(this.E, this.E);
        layoutParams4.setMargins((((i4 + 1) * i2) - decodeStream.getWidth()) - (this.E / 2), this.E / 2, 0, 0);
        this.H.addView(imageButton, layoutParams4);
        imageButton.setOnClickListener(new q(this));
        TextView textView3 = new TextView(this.a);
        String str3 = (String) ((HashMap) this.G.get(i4)).get("author");
        if (str3.length() > 10) {
            str3 = str3.substring(0, 9);
        }
        textView3.setText(str3);
        textView3.setTextSize(15.0f);
        textView3.setTextColor(-16777216);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams5.setMargins((i2 * i4) + (this.E * 2), (this.E * 120) / 100, 0, 0);
        this.H.addView(textView3, layoutParams5);
        TextView textView4 = new TextView(this.a);
        String str4 = (String) ((HashMap) this.G.get(i4)).get("fee_type");
        if (str4.length() > 10) {
            str4 = str4.substring(0, 9);
        }
        textView4.setText(str4);
        textView4.setTextSize(15.0f);
        textView4.setTextColor(-16777216);
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams6.setMargins(((i4 + 1) * i2) - (this.E * 4), (this.E * 120) / 100, 0, 0);
        this.H.addView(textView4, layoutParams6);
        int i6 = i2 > i3 ? 120 : 70;
        TextView textView5 = new TextView(this.a);
        String replaceAll = ((String) ((HashMap) this.G.get(i4)).get("content")).replaceAll("<[^>]*>", "");
        if (replaceAll.length() > i6) {
            replaceAll = replaceAll.substring(0, i6 - 1);
        }
        textView5.setText(Html.fromHtml("    " + replaceAll + "...." + "<font color='red' ><u>点击下载</u></font> " + "!"));
        textView5.setTextSize(15.0f);
        textView5.setTextColor(-16777216);
        RelativeLayout.LayoutParams layoutParams7 = new RelativeLayout.LayoutParams(i2, (this.E * 325) / 100);
        layoutParams7.setMargins((i2 * i4) + i5, this.E * 2, ((4 - i4) * i2) + i5, 0);
        this.H.addView(textView5, layoutParams7);
    }

    private void a(Context context, CaseeAdView caseeAdView) {
        this.E = (int) (30.0f * caseeAdView.getResources().getDisplayMetrics().density);
        int b2 = b((Activity) context);
        int c2 = c((Activity) context);
        this.z = new b(0, b2 - ((this.E * 6) / 4), c2 / 6, (this.E * 6) / 4);
        this.u = new b(c2 / 6, b2 - ((this.E * 6) / 4), c2 / 6, (this.E * 6) / 4);
        this.v = new b((c2 * 2) / 6, b2 - ((this.E * 6) / 4), c2 / 6, (this.E * 6) / 4);
        this.w = new b((c2 * 3) / 6, b2 - ((this.E * 6) / 4), c2 / 6, (this.E * 6) / 4);
        this.x = new b((c2 * 4) / 6, b2 - ((this.E * 6) / 4), c2 / 6, (this.E * 6) / 4);
        this.y = new b((c2 * 5) / 6, b2 - ((this.E * 6) / 4), c2 / 6, (this.E * 6) / 4);
        Rect rect = new Rect();
        ((Activity) context).getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        int i2 = rect.top;
        this.A = new b(0, (b2 - ((this.E * 625) / 100)) - i2, c2, (this.E * AdView.AD_MEASURE_480) / 100);
        this.B = new b(0, i2, c2, b2 - ((this.E * 625) / 100));
    }

    private void a(View view, int i2, int i3) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.E, this.E);
        layoutParams.addRule(12);
        layoutParams.setMargins(i2, 0, 0, i3);
        this.i.addView(view, layoutParams);
    }

    private boolean a() {
        Intent intent;
        try {
            String g2 = this.b.g();
            if (g2 == null || "".equals(g2)) {
                return false;
            }
            if (g2.startsWith("tel")) {
                Uri parse = Uri.parse(g2);
                intent = this.a.getPackageManager().checkPermission("android.permission.CALL_PHONE", this.a.getPackageName()) == 0 ? new Intent("android.intent.action.CALL", parse) : new Intent("android.intent.action.DIAL", parse);
            } else if (g2.startsWith("smsto")) {
                Intent intent2 = new Intent("android.intent.action.SENDTO", Uri.parse(g2));
                intent2.putExtra("sms_body", this.b.c());
                intent = intent2;
            } else if (g2.startsWith("mailto")) {
                intent = new Intent("android.intent.action.SENDTO", Uri.parse(g2));
            } else if (g2.startsWith("geo")) {
                intent = new Intent("android.intent.action.VIEW", Uri.parse(g2));
            } else {
                if (!b(this.a, this.b.m())) {
                    g2 = this.b.n();
                }
                if (g2 != null && !"".equals(g2)) {
                    g2 = g2.replace("$(ssq)", this.b.k()).replace("$(sid)", this.b.j()).replace("$(asq)", this.b.l()).replace("$(aid)", this.b.i());
                }
                if (e.a) {
                    return false;
                }
                if (a(this.a, g2) != null) {
                    return true;
                }
                intent = null;
            }
            if (intent == null) {
                return false;
            }
            intent.addFlags(268435456);
            this.a.startActivity(intent);
            return true;
        } catch (Exception e2) {
            Log.e("CASEE-AD", "open new activity error:" + this.b.g(), e2);
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* access modifiers changed from: private */
    public void b() {
        Class<e> cls = e.class;
        int b2 = b((Activity) this.a);
        int c2 = c((Activity) this.a);
        this.H = new RelativeLayout(this.a);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(c2 * 5, (this.E * 625) / 100);
        layoutParams.addRule(12);
        layoutParams.setMargins(0, 0, (-c2) * 4, (this.E * 145) / 100);
        this.H.setBackgroundColor(Color.argb(250, 77, 118, 189));
        this.i.addView(this.H, layoutParams);
        Class<e> cls2 = e.class;
        Bitmap decodeStream = BitmapFactory.decodeStream(cls.getResourceAsStream("webview_side_page.png"));
        Matrix matrix = new Matrix();
        matrix.postRotate(180.0f);
        Bitmap createBitmap = Bitmap.createBitmap(decodeStream, 0, 0, decodeStream.getWidth(), decodeStream.getHeight(), matrix, true);
        int width = decodeStream.getWidth();
        int height = decodeStream.getHeight();
        for (int i2 = 0; i2 < this.G.size(); i2++) {
            a(c2, b2, i2, width);
        }
        this.s = new RelativeLayout(this.a);
        this.s.setBackgroundDrawable(new BitmapDrawable(decodeStream));
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(width, height);
        layoutParams2.setMargins(c2 - decodeStream.getWidth(), b2 - ((this.E * 465) / 100), 0, 0);
        this.s.setVisibility(0);
        this.s.setFocusable(false);
        this.s.setClickable(false);
        this.s.getBackground().setAlpha(100);
        this.i.addView(this.s, layoutParams2);
        this.r = new RelativeLayout(this.a);
        this.r.setBackgroundDrawable(new BitmapDrawable(createBitmap));
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(width, height);
        layoutParams3.setMargins(0, b2 - ((this.E * 465) / 100), 0, 0);
        this.r.setVisibility(8);
        this.r.getBackground().setAlpha(100);
        this.r.setFocusable(false);
        this.r.setClickable(false);
        this.i.addView(this.r, layoutParams3);
        Class<e> cls3 = e.class;
        Bitmap decodeStream2 = BitmapFactory.decodeStream(cls.getResourceAsStream("webview_hand.png"));
        this.t = new RelativeLayout(this.a);
        this.t.setBackgroundDrawable(new BitmapDrawable(decodeStream2));
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams4.setMargins((c2 * 3) / 4, b2 - ((this.E * 465) / 100), 0, 0);
        this.t.setVisibility(0);
        this.t.setFocusable(false);
        this.t.setClickable(false);
        this.i.addView(this.t, layoutParams4);
        this.H.setVisibility(0);
        this.h.setFocusable(false);
        this.h.setClickable(false);
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (this.E / 4));
        translateAnimation.setDuration(500);
        translateAnimation.setRepeatCount(3);
        translateAnimation.setInterpolator(new AccelerateInterpolator(0.8f));
        this.f3I.startAnimation(translateAnimation);
        TranslateAnimation translateAnimation2 = new TranslateAnimation(0.0f, (float) (((-c2) * 2) / 4), 0.0f, 0.0f);
        translateAnimation2.setDuration(1000);
        translateAnimation2.setStartOffset(3000);
        translateAnimation2.setInterpolator(new AccelerateInterpolator(2.0f));
        translateAnimation2.setAnimationListener(new j(this));
        this.t.startAnimation(translateAnimation2);
    }

    private boolean b(Context context, String str) {
        if (str == null || "".equals(str)) {
            return true;
        }
        List<ApplicationInfo> installedApplications = context.getPackageManager().getInstalledApplications(128);
        new HashMap();
        for (int i2 = 0; i2 < installedApplications.size(); i2++) {
            ApplicationInfo applicationInfo = installedApplications.get(i2);
            if (applicationInfo != null && applicationInfo.packageName.equals(str)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void c() {
        String str = (String) ((HashMap) this.G.get(this.C.d)).get(DomobAdManager.ACTION_URL);
        if (str != null || str.trim() != "") {
            try {
                Log.i("result", "---55---" + str);
                ((Activity) this.a).startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            } catch (ActivityNotFoundException e2) {
                try {
                    Intent intent = new Intent("android.intent.action.WEB_SEARCH", Uri.parse(str));
                    intent.putExtra("query", str);
                    intent.addFlags(268435456);
                    this.a.startActivity(intent);
                } catch (ActivityNotFoundException e3) {
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        Uri parse = Uri.parse(str);
        try {
            ((Activity) this.a).startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } catch (ActivityNotFoundException e2) {
            try {
                Intent intent = new Intent("android.intent.action.WEB_SEARCH", parse);
                intent.putExtra("query", str);
                intent.addFlags(268435456);
                this.a.startActivity(intent);
            } catch (ActivityNotFoundException e3) {
            }
        }
    }

    private ImageView d(String str) {
        ImageView imageView = new ImageView(this.a);
        imageView.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeStream(e.class.getResourceAsStream(str))));
        return imageView;
    }

    /* access modifiers changed from: private */
    public void d() {
        new Thread(new p(this)).start();
    }

    private void d(Activity activity) {
        this.J = 0;
        this.G = null;
        this.F = true;
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) b(activity), 0.0f);
        translateAnimation.setDuration(500);
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(new AccelerateInterpolator(0.8f));
        this.i.startAnimation(translateAnimation);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.casee.adsdk.i.a.a(com.casee.adsdk.i$a, boolean):boolean
     arg types: [com.casee.adsdk.i$a, int]
     candidates:
      com.casee.adsdk.i.a.a(int, int):void
      com.casee.adsdk.i.a.a(com.casee.adsdk.i$a, boolean):boolean */
    /* access modifiers changed from: private */
    public void e() {
        if (this.C != null || !this.C.h) {
            Bitmap bitmap = (Bitmap) ((HashMap) this.G.get(0)).get("bitmap");
            boolean unused = this.C.h = true;
            if (bitmap != null) {
                this.q.setBackgroundDrawable(new BitmapDrawable(bitmap));
            }
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(200);
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new AccelerateInterpolator(0.5f));
            this.q.startAnimation(alphaAnimation);
        }
    }

    /* access modifiers changed from: private */
    public void e(Activity activity) {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) b(activity));
        translateAnimation.setDuration(500);
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(new AccelerateInterpolator(0.8f));
        this.i.startAnimation(translateAnimation);
    }

    /* access modifiers changed from: private */
    public void f() {
        Class<e> cls = e.class;
        if (this.h.canGoBack()) {
            Class<e> cls2 = e.class;
            this.l.setImageBitmap(BitmapFactory.decodeStream(cls.getResourceAsStream("webview_last_on.png")));
            this.l.invalidate();
        } else {
            Class<e> cls3 = e.class;
            this.l.setImageBitmap(BitmapFactory.decodeStream(cls.getResourceAsStream("webview_last_off.png")));
            this.l.invalidate();
        }
        if (this.h.canGoForward()) {
            Class<e> cls4 = e.class;
            this.m.setImageBitmap(BitmapFactory.decodeStream(cls.getResourceAsStream("webview_next_on.png")));
            this.m.invalidate();
            return;
        }
        Class<e> cls5 = e.class;
        this.m.setImageBitmap(BitmapFactory.decodeStream(cls.getResourceAsStream("webview_next_off.png")));
        this.m.invalidate();
    }

    /* access modifiers changed from: private */
    public void g() {
        RotateAnimation rotateAnimation = new RotateAnimation(0.0f, 3600.0f, 1, 0.5f, 1, 0.5f);
        rotateAnimation.setDuration(6000);
        rotateAnimation.setRepeatCount(-1);
        this.j.startAnimation(rotateAnimation);
    }

    static /* synthetic */ int r(i iVar) {
        int i2 = iVar.J;
        iVar.J = i2 + 1;
        return i2;
    }

    /* access modifiers changed from: package-private */
    public RelativeLayout a(Context context, String str) {
        this.D = str;
        Log.i("result", "-----" + str);
        this.C = new a(context);
        Activity activity = (Activity) context;
        this.i = new RelativeLayout(activity);
        this.i.setVisibility(0);
        this.C.addContentView(this.i, new RelativeLayout.LayoutParams(-1, -1));
        this.C.show();
        e.a = true;
        this.h = new WebView(activity);
        this.h.getSettings().setJavaScriptEnabled(true);
        this.h.getSettings().setCacheMode(1);
        this.h.setWebViewClient(new m(this));
        this.h.setDownloadListener(new l(this));
        this.k = new ImageView(activity);
        this.k.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeStream(e.class.getResourceAsStream("webview_background.png"))));
        Rect rect = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        this.i.addView(this.h, new RelativeLayout.LayoutParams(-1, (b(activity) - rect.top) - ((this.E * 6) / 4)));
        a(activity);
        d(activity);
        this.F = true;
        new Thread(new k(this, str)).start();
        return this.i;
    }

    public String a(String str) {
        BufferedReader bufferedReader;
        try {
            HttpURLConnection b2 = b(str);
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(b2.getInputStream()));
                StringBuilder sb = new StringBuilder();
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    sb.append(readLine);
                }
                String sb2 = sb.toString();
                bufferedReader.close();
                if (b2 != null) {
                    b2.disconnect();
                }
                return sb2;
            } catch (Throwable th) {
                if (b2 != null) {
                    b2.disconnect();
                }
                throw th;
            }
        } catch (Exception e2) {
            Log.e("result", e2.toString());
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.casee.adsdk.i.a.a(com.casee.adsdk.i$a, boolean):boolean
     arg types: [com.casee.adsdk.i$a, int]
     candidates:
      com.casee.adsdk.i.a.a(int, int):void
      com.casee.adsdk.i.a.a(com.casee.adsdk.i$a, boolean):boolean */
    /* access modifiers changed from: protected */
    public void a(Activity activity) {
        int b2 = b((Activity) this.a);
        int c2 = c((Activity) this.a);
        int i2 = b2 < c2 ? (this.E * 9) / 12 : (this.E * 5) / 12;
        this.q = d("webview_introduce_logo.png");
        this.l = d("webview_last_off.png");
        this.m = d("webview_next_off.png");
        this.n = d("webview_web_refresh.png");
        this.o = d("webview_open_web.png");
        this.p = d("webview_exit.png");
        this.j = d("webview_loading.png");
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        this.i.addView(this.j, layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, (this.E * 6) / 4);
        layoutParams2.addRule(12);
        this.i.addView(this.k, layoutParams2);
        a(this.q, i2, (this.E * 28) / 100);
        boolean unused = this.C.h = false;
        a(this.l, (c2 / 6) + i2, this.E / 4);
        a(this.m, ((c2 * 2) / 6) + i2, this.E / 4);
        a(this.n, ((c2 * 3) / 6) + i2, this.E / 4);
        a(this.o, ((c2 * 4) / 6) + i2, this.E / 4);
        a(this.p, ((c2 * 5) / 6) + i2, this.E / 4);
    }

    public int b(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public HttpURLConnection b(String str) {
        HttpURLConnection httpURLConnection = null;
        if (str == null) {
            return null;
        }
        try {
            HttpURLConnection httpURLConnection2 = (HttpURLConnection) new URL(str).openConnection();
            try {
                httpURLConnection2.setConnectTimeout(5000);
                httpURLConnection2.setReadTimeout(5000);
                httpURLConnection2.setRequestMethod("POST");
                httpURLConnection2.setDoOutput(true);
                return httpURLConnection2;
            } catch (Exception e2) {
                e = e2;
                httpURLConnection = httpURLConnection2;
                Log.e("result", e.toString());
                return httpURLConnection;
            }
        } catch (Exception e3) {
            e = e3;
            Log.e("result", e.toString());
            return httpURLConnection;
        }
    }

    public int c(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public void onClick(View view) {
        Log.i("CASEE-AD", "click ad.");
        if (a()) {
            new s(this).start();
        }
    }
}
