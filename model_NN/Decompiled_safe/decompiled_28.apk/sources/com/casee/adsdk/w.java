package com.casee.adsdk;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public class w extends Thread {
    Location a;
    Context b;

    class a implements LocationListener {
        a() {
        }

        public void onLocationChanged(Location location) {
            w.this.a = location;
        }

        public void onProviderDisabled(String str) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }
    }

    public w(Context context) {
        this.b = context;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.a != null ? "" + this.a.getLatitude() + "," + this.a.getLongitude() : "";
    }

    public void run() {
        String str;
        LocationManager locationManager;
        LocationManager locationManager2;
        String str2;
        if (this.b.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            LocationManager locationManager3 = (LocationManager) this.b.getSystemService("location");
            Criteria criteria = new Criteria();
            criteria.setAccuracy(2);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setCostAllowed(true);
            String bestProvider = locationManager3.getBestProvider(criteria, true);
            locationManager = locationManager3;
            str = bestProvider;
        } else {
            str = null;
            locationManager = null;
        }
        if (str == null && this.b.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
            locationManager2 = (LocationManager) this.b.getSystemService("location");
            Criteria criteria2 = new Criteria();
            criteria2.setAccuracy(1);
            criteria2.setAltitudeRequired(false);
            criteria2.setBearingRequired(false);
            criteria2.setCostAllowed(true);
            str2 = locationManager2.getBestProvider(criteria2, true);
        } else {
            String str3 = str;
            locationManager2 = locationManager;
            str2 = str3;
        }
        if (str2 == null) {
            Log.w("CASEE-AD", "cannot get location provider.");
            return;
        }
        this.a = locationManager2.getLastKnownLocation(str2);
        locationManager2.requestLocationUpdates(str2, 900000, 700.0f, new a(), this.b.getMainLooper());
    }
}
