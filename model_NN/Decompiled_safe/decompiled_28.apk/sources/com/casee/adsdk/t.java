package com.casee.adsdk;

import android.os.Handler;
import android.os.Message;
import com.casee.adsdk.i;
import java.util.ArrayList;

class t extends Handler {
    final /* synthetic */ i a;

    t(i iVar) {
        this.a = iVar;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                if (this.a.C != null) {
                    this.a.C.cancel();
                    i.a unused = this.a.C = (i.a) null;
                }
                e.a = false;
                return;
            case 2:
                if (!e.a) {
                    return;
                }
                if (this.a.G == null || this.a.G.size() < 5) {
                    ArrayList unused2 = this.a.G = (ArrayList) null;
                    this.a.d();
                    return;
                }
                this.a.e();
                return;
            default:
                return;
        }
    }
}
