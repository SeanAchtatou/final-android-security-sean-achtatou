package com.casee.adsdk;

import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import cn.domob.android.ads.DomobAdManager;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;

class p implements Runnable {
    final /* synthetic */ i a;

    p(i iVar) {
        this.a = iVar;
    }

    public void run() {
        try {
            String a2 = this.a.a("http://myapk.cn/sdkapi.php");
            if (e.a && a2 != null) {
                String substring = a2.substring(a2.indexOf("{"), a2.lastIndexOf("}") + 1);
                ArrayList arrayList = new ArrayList();
                JSONObject jSONObject = new JSONObject(substring);
                if (!jSONObject.isNull("list")) {
                    JSONArray jSONArray = jSONObject.getJSONArray("list");
                    int i = 0;
                    while (jSONArray != null && i < jSONArray.length()) {
                        HashMap hashMap = new HashMap();
                        JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                        hashMap.put("m", jSONObject2.getString("m"));
                        hashMap.put("cid", jSONObject2.getString("cid"));
                        hashMap.put("id", jSONObject2.getString("id"));
                        hashMap.put("title", jSONObject2.getString("title"));
                        String string = jSONObject2.getString("logo");
                        hashMap.put("logo", string);
                        if (e.a) {
                            hashMap.put("bitmap", ((BitmapDrawable) f.a(this.a.a, string)).getBitmap());
                            if (e.a) {
                                hashMap.put("author", jSONObject2.getString("author"));
                                hashMap.put("size", jSONObject2.getString("size"));
                                hashMap.put("fee_type", jSONObject2.getString("fee_type"));
                                hashMap.put("content", jSONObject2.getString("content"));
                                String string2 = jSONObject2.getString(DomobAdManager.ACTION_URL);
                                hashMap.put(DomobAdManager.ACTION_URL, string2);
                                Log.i("result", "-----" + string2);
                                arrayList.add(hashMap);
                                if (e.a) {
                                    i++;
                                } else {
                                    return;
                                }
                            } else {
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                }
                if (arrayList.size() >= 5) {
                    if (e.a) {
                        ArrayList unused = this.a.G = arrayList;
                        this.a.K.sendEmptyMessageDelayed(2, 0);
                    }
                } else if (this.a.J < 3 && e.a) {
                    this.a.d();
                    i.r(this.a);
                    Log.i("result", "-----" + this.a.J);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
