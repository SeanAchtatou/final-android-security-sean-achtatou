package com.casee.adsdk;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import java.util.Random;
import java.util.Timer;

public class CaseeAdView extends RelativeLayout {
    static w a;
    private static String j;
    private static boolean k;
    /* access modifiers changed from: private */
    public Handler b;
    /* access modifiers changed from: private */
    public e c;
    private Timer d;
    /* access modifiers changed from: private */
    public AdListener e;
    /* access modifiers changed from: private */
    public Boolean f;
    private Random g;
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public int i;
    private int l;
    private int m;
    private float n;
    /* access modifiers changed from: private */
    public int o;
    /* access modifiers changed from: private */
    public int p;
    private boolean q;
    /* access modifiers changed from: private */
    public o r;
    /* access modifiers changed from: private */
    public boolean s;
    private boolean t;
    private float u;
    private float[][] v;
    private float[][] w;
    private float[][] x;

    public interface AdListener {
        void onFailedToReceiveAd(CaseeAdView caseeAdView);

        void onFailedToReceiveRefreshAd(CaseeAdView caseeAdView);

        void onReceiveAd(CaseeAdView caseeAdView);

        void onReceiveRefreshAd(CaseeAdView caseeAdView);
    }

    class a extends Thread {
        a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.casee.adsdk.CaseeAdView.a(com.casee.adsdk.CaseeAdView, boolean):boolean
         arg types: [com.casee.adsdk.CaseeAdView, int]
         candidates:
          com.casee.adsdk.CaseeAdView.a(com.casee.adsdk.CaseeAdView, int):int
          com.casee.adsdk.CaseeAdView.a(com.casee.adsdk.CaseeAdView, com.casee.adsdk.e):com.casee.adsdk.e
          com.casee.adsdk.CaseeAdView.a(com.casee.adsdk.CaseeAdView, com.casee.adsdk.o):com.casee.adsdk.o
          com.casee.adsdk.CaseeAdView.a(com.casee.adsdk.CaseeAdView, java.lang.Boolean):java.lang.Boolean
          com.casee.adsdk.CaseeAdView.a(com.casee.adsdk.e, com.casee.adsdk.e):void
          com.casee.adsdk.CaseeAdView.a(com.casee.adsdk.CaseeAdView, boolean):boolean */
        public void run() {
            o unused = CaseeAdView.this.r = b.a(CaseeAdView.this.getContext(), CaseeAdView.this);
            if (CaseeAdView.this.r == null) {
                boolean unused2 = CaseeAdView.this.s = false;
                if (CaseeAdView.this.e != null) {
                    if (CaseeAdView.this.f == null) {
                        Boolean unused3 = CaseeAdView.this.f = new Boolean(false);
                        CaseeAdView.this.e.onFailedToReceiveAd(CaseeAdView.this);
                    } else if (CaseeAdView.this.f.booleanValue()) {
                        Boolean unused4 = CaseeAdView.this.f = new Boolean(false);
                        CaseeAdView.this.e.onFailedToReceiveRefreshAd(CaseeAdView.this);
                    }
                }
                Log.i("CASEE-AD", "cannot fetch ad.");
                return;
            }
            if (CaseeAdView.this.e != null) {
                if (CaseeAdView.this.f == null || !CaseeAdView.this.f.booleanValue()) {
                    Boolean unused5 = CaseeAdView.this.f = new Boolean(true);
                    CaseeAdView.this.e.onReceiveAd(CaseeAdView.this);
                } else if (CaseeAdView.this.f.booleanValue()) {
                    Boolean unused6 = CaseeAdView.this.f = new Boolean(true);
                    CaseeAdView.this.e.onReceiveRefreshAd(CaseeAdView.this);
                }
            }
            Log.i("CASEE-AD", "fetch an ad successfully.");
            if (CaseeAdView.this.r.a() > 0 && CaseeAdView.this.o != CaseeAdView.this.r.a()) {
                int unused7 = CaseeAdView.this.o = CaseeAdView.this.r.a();
                CaseeAdView.this.n();
                CaseeAdView.this.m();
            } else if (CaseeAdView.this.r.a() == -1 && CaseeAdView.this.o != CaseeAdView.this.p) {
                int unused8 = CaseeAdView.this.o = CaseeAdView.this.p;
                CaseeAdView.this.n();
                CaseeAdView.this.m();
            }
            CaseeAdView.this.r.e();
            CaseeAdView.this.b.post(new u(this));
        }
    }

    public CaseeAdView(Context context) {
        this(context, null, 0);
    }

    public CaseeAdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CaseeAdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f = null;
        this.g = new Random(System.currentTimeMillis());
        this.h = 0;
        this.i = 0;
        this.l = -16777216;
        this.m = -1;
        this.n = 18.0f;
        this.o = 30000;
        this.p = 30000;
        this.q = false;
        this.u = getResources().getDisplayMetrics().density;
        this.v = new float[][]{new float[]{0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f}, new float[]{0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f}, new float[]{0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f}, new float[]{0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f}};
        this.w = new float[][]{new float[]{1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f}, new float[]{1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f}, new float[]{1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f}, new float[]{1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f}, new float[]{1.0f, 0.0f, 1.0f, 0.0f, 0.5f, 0.5f}};
        this.x = new float[][]{new float[]{0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f}, new float[]{0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f}, new float[]{0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f}, new float[]{0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f}, new float[]{0.0f, 1.0f, 0.0f, 1.0f, 0.5f, 0.5f}};
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            if (applicationInfo != null) {
                j = applicationInfo.metaData.getString("com.casee.adsdk.siteId");
                k = applicationInfo.metaData.getBoolean("com.casee.adsdk.isTesting", false);
            }
            if (j == null || j.length() != 32) {
                throw new IllegalArgumentException("Site ID must be a string which contains 32 chars.");
            }
            if (attributeSet != null) {
                this.o = attributeSet.getAttributeIntValue(str, "refreshInterval", this.o);
                if (this.o < 10000 || this.o > 900000) {
                    throw new IllegalArgumentException("Fetch interval must be between 15 secs and 15 mins.");
                }
                this.p = this.o;
                this.m = attributeSet.getAttributeUnsignedIntValue(str, "textColor", this.m);
                this.l = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", this.l);
                this.q = attributeSet.getAttributeBooleanValue(str, "vertical", this.q);
            }
            new Timer().schedule(new af(this), 9000);
            o();
        } catch (Exception e2) {
            Log.e("CASEE-AD", e2.getMessage(), e2);
        }
    }

    public CaseeAdView(Context context, AttributeSet attributeSet, int i2, String str, boolean z, int i3, int i4, int i5) {
        this(context, attributeSet, i2, str, z, i3, i4, i5, false);
    }

    public CaseeAdView(Context context, AttributeSet attributeSet, int i2, String str, boolean z, int i3, int i4, int i5, float f2, boolean z2) {
        this(context, attributeSet, i2, str, z, i3, i4, i5, z2);
    }

    public CaseeAdView(Context context, AttributeSet attributeSet, int i2, String str, boolean z, int i3, int i4, int i5, boolean z2) {
        super(context, attributeSet, i2);
        this.f = null;
        this.g = new Random(System.currentTimeMillis());
        this.h = 0;
        this.i = 0;
        this.l = -16777216;
        this.m = -1;
        this.n = 18.0f;
        this.o = 30000;
        this.p = 30000;
        this.q = false;
        this.u = getResources().getDisplayMetrics().density;
        this.v = new float[][]{new float[]{0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f}, new float[]{0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f}, new float[]{0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f}, new float[]{0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f}};
        this.w = new float[][]{new float[]{1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f}, new float[]{1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f}, new float[]{1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f}, new float[]{1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f}, new float[]{1.0f, 0.0f, 1.0f, 0.0f, 0.5f, 0.5f}};
        this.x = new float[][]{new float[]{0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f}, new float[]{0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f}, new float[]{0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f}, new float[]{0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f}, new float[]{0.0f, 1.0f, 0.0f, 1.0f, 0.5f, 0.5f}};
        if (str == null || str.length() != 32) {
            throw new IllegalArgumentException("Site ID must be a string which contains 32 chars.");
        }
        j = str;
        k = z;
        if (i3 < 10000 || i3 > 900000) {
            throw new IllegalArgumentException("Fetch interval must be between 15 secs and 15 mins.");
        }
        this.o = i3;
        this.p = this.o;
        this.l = i4;
        this.m = i5;
        this.q = z2;
        new Timer().schedule(new ae(this), 9000);
        o();
    }

    public CaseeAdView(Context context, String str, boolean z, int i2, int i3, int i4, boolean z2) {
        this(context, null, 0, str, z, i2, i3, i4, z2);
    }

    /* access modifiers changed from: private */
    public void a(e eVar, e eVar2) {
        if (!this.t) {
            Log.i("CASEE-AD", "View is not shown, skip swap ad.");
            this.s = false;
        } else if (getVisibility() != 0) {
            Log.i("CASEE-AD", "View is invisible, pass swap ad.");
            this.s = false;
        } else {
            super.setBackgroundColor(this.l);
            int nextInt = this.g.nextInt(100);
            if (nextInt < 30) {
                a(eVar, eVar2, this.g.nextInt(this.v.length));
            } else if (nextInt < 50) {
                if (eVar != null) {
                    d(eVar, eVar2);
                } else {
                    e(eVar, eVar2);
                }
            } else if (nextInt <= 80) {
                if (eVar != null) {
                    b(eVar, eVar2, this.g.nextInt(this.w.length));
                } else {
                    c(eVar, eVar2, this.g.nextInt(this.w.length));
                }
            } else if (eVar != null) {
                b(eVar, eVar2);
            } else {
                c(eVar, eVar2);
            }
        }
    }

    private void a(e eVar, e eVar2, int i2) {
        if (eVar != null) {
            eVar.setClickable(false);
            TranslateAnimation translateAnimation = new TranslateAnimation(1, this.v[i2][0], 1, this.v[i2][1], 1, this.v[i2][2], 1, this.v[i2][3]);
            translateAnimation.setDuration(800);
            translateAnimation.setAnimationListener(new ab(this, eVar));
            eVar.startAnimation(translateAnimation);
        }
        eVar2.setClickable(false);
        eVar2.setVisibility(0);
        TranslateAnimation translateAnimation2 = new TranslateAnimation(1, this.v[i2][4], 1, this.v[i2][5], 1, this.v[i2][6], 1, this.v[i2][7]);
        translateAnimation2.setDuration(800);
        eVar2.startAnimation(translateAnimation2);
        eVar2.setClickable(true);
        Log.i("CASEE-AD", "display the ad successfully.");
        this.s = false;
    }

    private void b(e eVar, e eVar2) {
        eVar2.setVisibility(8);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(800);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        alphaAnimation.startNow();
        alphaAnimation.setAnimationListener(new ad(this, eVar, eVar2));
        startAnimation(alphaAnimation);
    }

    private void b(e eVar, e eVar2, int i2) {
        eVar2.setVisibility(8);
        ScaleAnimation scaleAnimation = new ScaleAnimation(this.w[i2][0], this.w[i2][1], this.w[i2][2], this.w[i2][3], 1, this.w[i2][4], 1, this.w[i2][5]);
        scaleAnimation.setDuration(600);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setInterpolator(new AccelerateInterpolator());
        scaleAnimation.startNow();
        scaleAnimation.setAnimationListener(new x(this, eVar, eVar2, i2));
        eVar.startAnimation(scaleAnimation);
    }

    static String c() {
        return j;
    }

    /* access modifiers changed from: private */
    public void c(e eVar, e eVar2) {
        boolean z;
        if (eVar != null) {
            eVar.setVisibility(8);
            removeView(eVar);
            z = false;
        } else {
            z = true;
        }
        eVar2.setVisibility(0);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        if (z) {
            alphaAnimation.setDuration(300);
        } else {
            alphaAnimation.setDuration(600);
        }
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        alphaAnimation.setAnimationListener(new aa(this));
        alphaAnimation.startNow();
        startAnimation(alphaAnimation);
        Log.i("CASEE-AD", "display the ad successfully.");
        this.s = false;
    }

    /* access modifiers changed from: private */
    public void c(e eVar, e eVar2, int i2) {
        if (eVar != null) {
            eVar.setVisibility(8);
            removeView(eVar);
        }
        eVar2.setVisibility(0);
        ScaleAnimation scaleAnimation = new ScaleAnimation(this.x[i2][0], this.x[i2][1], this.x[i2][2], this.x[i2][3], 1, this.x[i2][4], 1, this.x[i2][5]);
        scaleAnimation.setDuration(600);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setInterpolator(new AccelerateInterpolator());
        scaleAnimation.setAnimationListener(new d(this));
        scaleAnimation.startNow();
        eVar2.startAnimation(scaleAnimation);
        Log.i("CASEE-AD", "display the ad successfully.");
        this.s = false;
    }

    private void d(e eVar, e eVar2) {
        eVar2.setVisibility(8);
        a aVar = new a(0.0f, 90.0f, (float) (getWidth() / 2), (float) (getWidth() / 2));
        aVar.setDuration(600);
        aVar.setFillAfter(true);
        aVar.setInterpolator(new AccelerateInterpolator());
        aVar.startNow();
        aVar.setAnimationListener(new z(this, eVar, eVar2));
        eVar.startAnimation(aVar);
    }

    static boolean d() {
        return k;
    }

    static String e() {
        return Build.VERSION.RELEASE;
    }

    /* access modifiers changed from: private */
    public void e(e eVar, e eVar2) {
        if (eVar != null) {
            eVar.setVisibility(8);
            removeView(eVar);
        }
        eVar2.setVisibility(0);
        a aVar = new a(-90.0f, 0.0f, (float) (getWidth() / 2), (float) (getWidth() / 2));
        aVar.setDuration(600);
        aVar.setFillAfter(true);
        aVar.setInterpolator(new AccelerateInterpolator());
        aVar.setAnimationListener(new y(this));
        aVar.startNow();
        eVar2.startAnimation(aVar);
        Log.i("CASEE-AD", "display the ad successfully.");
        this.s = false;
    }

    static String f() {
        return Build.MODEL;
    }

    static String g() {
        return Build.ID;
    }

    /* access modifiers changed from: private */
    public void m() {
        if (j != null && "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA".equalsIgnoreCase(j)) {
            Log.w("CASEE-AD", "You are using the sample site ID. Before release your application, please use a formal site id which you get from CASEE.");
        }
        this.t = true;
        Log.i("CASEE-AD", "start to show ad: siteid - " + j + "; isTesting - " + k + "; interval - " + this.o);
        if (a == null) {
            a = new w(getContext());
            a.start();
        }
        if (this.o > 0 && this.d == null) {
            this.d = new Timer();
            this.d.schedule(new ac(this), 1000, (long) this.o);
        }
    }

    /* access modifiers changed from: private */
    public void n() {
        if (this.d != null) {
            this.d.cancel();
            this.d = null;
        }
    }

    /* access modifiers changed from: private */
    public void o() {
        if (!this.s) {
            Log.d("CASEE-AD", "==================showAd()================");
            if (this.b == null) {
                this.b = new Handler();
            }
            if (!this.t) {
                Log.i("CASEE-AD", "View is not shown, skip show ad.");
            } else if (getVisibility() != 0) {
                Log.i("CASEE-AD", "View is invisible, pass show ad.");
            } else {
                this.s = true;
                new a().start();
            }
        }
    }

    public float a() {
        return this.n;
    }

    public int b() {
        return this.l;
    }

    public int h() {
        return this.m;
    }

    public o i() {
        return this.r;
    }

    public boolean j() {
        return this.q;
    }

    public int k() {
        return this.h;
    }

    public int l() {
        return this.i;
    }

    public void onDetachedFromWindow() {
        n();
    }

    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int i4 = this.u < 1.0f ? 24 : this.u == 1.0f ? 48 : 72;
        if (this.q) {
            this.h = i4;
            this.i = getMeasuredHeight();
        } else {
            this.h = getMeasuredWidth();
            this.i = i4;
            int i5 = getLayoutParams().height;
            if (i5 > 0) {
                this.i = Math.min(this.i, i5);
            }
            if (this.i < 48 && this.i > 1) {
                this.n = 13.0f;
            }
        }
        setMeasuredDimension(this.h, this.i);
    }

    public void onShown() {
        this.t = true;
    }

    public void onUnshown() {
        this.t = false;
    }

    public void onWindowFocusChanged(boolean z) {
        if (z) {
            Log.i("CASEE-AD", "Adview got focus, start ...");
            m();
            return;
        }
        Log.i("CASEE-AD", "Adview lost focus, cancel timer.");
        n();
    }

    public void setAdViewHeight(int i2) {
        this.i = i2;
    }

    public void setAdViewWidth(int i2) {
        this.h = i2;
    }

    public void setBackgroundColor(int i2) {
        this.l = i2;
        super.setBackgroundColor(i2);
    }

    public void setListener(AdListener adListener) {
        synchronized (this) {
            this.e = adListener;
        }
    }

    public void setTextColor(int i2) {
        this.m = i2;
        if (this.c != null) {
            this.c.a(i2);
        }
    }

    public void setVisibility(int i2) {
        if (i2 != super.getVisibility()) {
            if (i2 == 8 || this.c == null) {
                super.setBackgroundColor(0);
            } else {
                super.setBackgroundColor(this.l);
            }
            if (this.c != null) {
                this.c.setVisibility(i2);
            }
            super.setVisibility(i2);
            if (i2 == 0 && this.c == null) {
                o();
            }
        }
    }
}
