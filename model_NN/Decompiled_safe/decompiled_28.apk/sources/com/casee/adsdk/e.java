package com.casee.adsdk;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.adview.util.AdViewUtil;
import com.casee.adsdk.gifview.b;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;

class e extends RelativeLayout {
    static boolean a = false;
    static final Typeface b = Typeface.create(Typeface.SANS_SERIF, 1);
    static final Typeface c = Typeface.create(Typeface.SANS_SERIF, 0);
    private BitmapDrawable d;
    private Context e;
    private o f;
    private ImageView g;
    private TextView h;
    private View i;
    private TextView j;
    private ScrollView k;
    private int l;
    private float m = 13.0f;
    private int n = -1;
    private boolean o = false;
    private boolean p = true;
    private float q = getResources().getDisplayMetrics().density;
    private CaseeAdView r;
    private b s;
    private i t;
    private final int u = 0;
    private final int v = 1;
    private Handler w = new v(this);
    private boolean x = true;
    private int y = 0;
    private int z = 0;

    public e(Context context, o oVar, CaseeAdView caseeAdView) {
        super(context, null, 0);
        this.f = oVar;
        this.e = context;
        this.o = caseeAdView.j();
        this.p = CaseeAdView.d();
        this.n = caseeAdView.h();
        this.l = caseeAdView.b();
        this.m = caseeAdView.a();
        this.r = caseeAdView;
        this.t = new i(context, oVar, caseeAdView);
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.casee.adsdk.e.a(android.graphics.Rect, int, int, boolean):android.graphics.drawable.BitmapDrawable
     arg types: [android.graphics.Rect, int, int, int]
     candidates:
      com.casee.adsdk.e.a(android.graphics.Canvas, android.graphics.Rect, int, int):void
      com.casee.adsdk.e.a(android.graphics.Rect, int, int, boolean):android.graphics.drawable.BitmapDrawable */
    private BitmapDrawable a(Rect rect, int i2, int i3) {
        return a(rect, i2, i3, false);
    }

    private BitmapDrawable a(Rect rect, int i2, int i3, boolean z2) {
        try {
            Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
            a(new Canvas(createBitmap), rect, i2, i3);
            return new BitmapDrawable(createBitmap);
        } catch (Throwable th) {
            return null;
        }
    }

    private void a() {
        if (this.f != null) {
            setClickable(true);
            setFocusable(true);
            setOnClickListener(this.t);
            if (this.f.b() == 0) {
                a(this.f.b(), this.f.e());
                c();
            } else if (this.f.o()) {
                d();
            } else {
                a(this.f.b(), this.f.e());
            }
            b(this.f.b());
            b();
        }
    }

    private void a(int i2, Drawable drawable) {
        RelativeLayout.LayoutParams layoutParams;
        this.g = new ImageView(this.e);
        this.g.setImageDrawable(drawable);
        if (i2 != 0) {
            this.g.setScaleType(ImageView.ScaleType.FIT_CENTER);
            layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        } else {
            layoutParams = new RelativeLayout.LayoutParams((int) (this.q * 38.0f), (int) (this.q * 38.0f));
            if (this.o) {
                layoutParams.setMargins((int) (this.q * 5.0f), 5, 0, (int) (this.q * 5.0f));
                layoutParams.addRule(14);
            } else {
                layoutParams.setMargins(1, 1, 0, 1);
                layoutParams.addRule(9);
                layoutParams.addRule(15);
            }
        }
        this.g.setId(1);
        this.g.setLayoutParams(layoutParams);
        addView(this.g);
    }

    private static void a(Canvas canvas, Rect rect, int i2, int i3) {
        Paint paint = new Paint();
        paint.setColor(i2);
        paint.setAntiAlias(true);
        canvas.drawRect(rect, paint);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb(127, Color.red(i3), Color.green(i3), Color.blue(i3)), i3});
        int height = ((int) (((double) rect.height()) * 0.5d)) + rect.top;
        gradientDrawable.setBounds(rect.left, rect.top, rect.right, height);
        gradientDrawable.draw(canvas);
        Rect rect2 = new Rect(rect.left, height, rect.right, rect.bottom);
        Paint paint2 = new Paint();
        paint2.setColor(i3);
        canvas.drawRect(rect2, paint2);
    }

    private void a(String str) {
        this.k = new ScrollView(this.e);
        this.k.setId(2);
        this.k.setVerticalScrollBarEnabled(false);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        if (this.q < 1.0f) {
            layoutParams.setMargins(2, 0, 5, 5);
        } else {
            layoutParams.setMargins(((int) ((38.0f - this.m) / 2.0f)) + 5, 0, 5, 5);
        }
        layoutParams.addRule(3, 1);
        layoutParams.addRule(2, 3);
        this.k.setLayoutParams(layoutParams);
        LinearLayout linearLayout = new LinearLayout(this.e);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(1);
        for (int i2 = 0; i2 < str.length(); i2++) {
            TextView textView = new TextView(this.e);
            textView.setTextSize(this.m);
            textView.setTextColor(this.n);
            String valueOf = String.valueOf(str.charAt(i2));
            textView.setTypeface(b);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams2.addRule(13);
            layoutParams2.setMargins(0, 0, 0, 0);
            textView.setText(valueOf);
            textView.setClickable(true);
            textView.setFocusable(true);
            textView.setOnClickListener(this.t);
            textView.setLayoutParams(layoutParams2);
            linearLayout.addView(textView, i2);
        }
        linearLayout.setClickable(true);
        linearLayout.setFocusable(true);
        linearLayout.setOnClickListener(this.t);
        this.k.addView(linearLayout);
    }

    private void b() {
        if (this.p || "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA".equals(CaseeAdView.c())) {
            this.j = new TextView(this.e);
            this.j.setGravity(17);
            this.j.setText(this.f.b);
            this.j.setId(4);
            if (this.f.b() != 0) {
                this.j.setBackgroundColor(Color.argb((int) AdViewUtil.VERSION, 0, 0, 0));
            }
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(14);
            if (!this.o) {
                layoutParams.addRule(12);
            } else {
                layoutParams.addRule(15);
            }
            this.j.setLayoutParams(layoutParams);
            this.j.setTextSize(10.0f);
            this.j.setTextColor(this.n);
            addView(this.j);
        }
    }

    private void b(int i2) {
        this.i = a(this.q);
        addView(this.i);
        if (i2 == 1) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(7000);
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new AccelerateInterpolator());
            alphaAnimation.startNow();
            this.i.startAnimation(alphaAnimation);
        }
    }

    private void c() {
        if (this.o) {
            a(this.f.d() + XmlConstant.SINGLE_SPACE);
            this.k.setSmoothScrollingEnabled(true);
            this.k.setClickable(true);
            this.k.setFocusable(true);
            this.k.addStatesFromChildren();
            this.k.setOnClickListener(this.t);
            addView(this.k);
            e();
            return;
        }
        this.h = new n(this.e);
        this.h.setText(this.f.d());
        this.h.setId(2);
        this.h.setTextSize(this.m);
        this.h.setTextColor(this.n);
        this.h.setTypeface(b);
        this.h.setSingleLine(true);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        if (this.m < 16.0f) {
            layoutParams.setMargins(5, 0, 5, 5);
        } else {
            layoutParams.setMargins(5, 4, 5, 5);
        }
        layoutParams.addRule(1, 1);
        this.h.setLayoutParams(layoutParams);
        this.h.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        this.h.setMarqueeRepeatLimit(-1);
        this.h.setClickable(true);
        this.h.setFocusable(true);
        this.h.setOnClickListener(this.t);
        addView(this.h);
    }

    private void d() {
        this.s = this.f.a(this.o);
        if (this.s == null) {
            a(0, this.f.f());
            c();
            return;
        }
        this.s.setId(1);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        addView(this.s, layoutParams);
    }

    private void e() {
        this.w.sendEmptyMessageDelayed(0, 100);
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.k == null) {
            return;
        }
        if (this.z > 2) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            this.w.sendEmptyMessageDelayed(1, 2000);
        } else if (this.x) {
            this.w.sendEmptyMessageDelayed(0, 2000);
            this.k.scrollTo(this.k.getScrollX(), 0);
            this.x = false;
            this.z++;
        } else {
            this.k.smoothScrollBy(0, 2);
            if (this.y != this.k.getScrollY() || this.y == 0) {
                this.y = this.k.getScrollY();
                this.w.sendEmptyMessageDelayed(0, 100);
                return;
            }
            this.x = true;
            this.y = this.k.getScrollY();
            this.w.sendEmptyMessageDelayed(0, 2000);
        }
    }

    /* access modifiers changed from: protected */
    public View a(float f2) {
        ImageView imageView = new ImageView(this.e);
        Bitmap a2 = this.f.a(f2);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageView.setImageBitmap(a2);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(11);
        layoutParams.addRule(12);
        imageView.setLayoutParams(layoutParams);
        return imageView;
    }

    public void a(int i2) {
        this.n = i2;
        if (this.h != null) {
            this.h.setTextColor(i2);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.s != null) {
            this.s.a();
        }
    }

    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int i4 = this.q < 1.0f ? 24 : this.q == 1.0f ? 48 : 72;
        if (this.o) {
            setMeasuredDimension(i4, getMeasuredHeight());
        } else {
            setMeasuredDimension(getMeasuredWidth(), i4);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != 0 && i3 != 0) {
            this.d = a(new Rect(0, 0, i2, i3), -1, this.l);
            setBackgroundDrawable(this.d);
        }
    }

    public void setBackgroundColor(int i2) {
        this.l = i2;
    }
}
