package I;

import com.adview.util.AdViewUtil;
import java.io.InputStream;

public class I {
    private static byte[] _;
    private static String[] __ = new String[256];
    private static int[] ___ = new int[256];

    static {
        try {
            InputStream resourceAsStream = new I().getClass().getResourceAsStream(new StringBuffer().append('I').append('.').append('g').append('i').append('f').toString());
            if (resourceAsStream != null) {
                int read = (resourceAsStream.read() << 16) | (resourceAsStream.read() << 8) | resourceAsStream.read();
                _ = new byte[read];
                byte b = (byte) read;
                byte[] bArr = _;
                int i = read;
                int i2 = 0;
                while (i != 0) {
                    int read2 = resourceAsStream.read(bArr, i2, i);
                    if (read2 == -1) {
                        break;
                    }
                    i -= read2;
                    int i3 = read2 + i2;
                    while (i2 < i3) {
                        bArr[i2] = (byte) (bArr[i2] ^ b);
                        i2++;
                    }
                }
                resourceAsStream.close();
            }
        } catch (Exception e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.<init>(byte[], int, int):void}
     arg types: [byte[], int, byte]
     candidates:
      ClspMth{java.lang.String.<init>(int[], int, int):void}
      ClspMth{java.lang.String.<init>(char[], int, int):void}
      ClspMth{java.lang.String.<init>(byte[], int, int):void} */
    public static final synchronized String I(int i) {
        String str;
        synchronized (I.class) {
            int i2 = i & AdViewUtil.VERSION;
            if (___[i2] != i) {
                ___[i2] = i;
                int i3 = i < 0 ? 65535 & i : i;
                __[i2] = new String(_, i3, (int) (_[i3 - 1] & 255)).intern();
            }
            str = __[i2];
        }
        return str;
    }
}
