package cn.appmedia.ad.g;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import cn.appmedia.ad.a.d;
import cn.appmedia.ad.a.f;
import cn.appmedia.ad.b.k;
import cn.appmedia.ad.c.b;
import cn.appmedia.ad.c.c;
import cn.appmedia.ad.c.h;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;

public class a {
    private static final String a = Build.VERSION.RELEASE;
    private static final String b = Build.MODEL;
    private static long c = Calendar.getInstance().getTimeInMillis();
    private static a d;
    private String e;
    private String f;
    private String g;
    private final String h = "1A2df@g9&S*#eRF4";
    private Context i;

    private a() {
    }

    private c a(String str, String str2) {
        c c2 = c();
        c2.d("getad");
        c2.e().put("loc", str);
        c2.e().put("res", str2);
        c2.e().put("model", b);
        c2.e().put("loc_lon", String.valueOf(k.f));
        c2.e().put("loc_lat", String.valueOf(k.g));
        c2.e().put("loc_pro", String.valueOf(k.h));
        return c2;
    }

    private c a(String str, ArrayList arrayList, ArrayList arrayList2) {
        c c2 = c();
        c2.d("info");
        c2.e().put("it", str);
        c2.e().put("model", b);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("sys:");
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            stringBuffer.append((String) arrayList.get(i2));
            if (i2 < size - 1) {
                stringBuffer.append("|");
            }
        }
        stringBuffer.append(",third:");
        int size2 = arrayList2.size();
        for (int i3 = 0; i3 < size2; i3++) {
            stringBuffer.append((String) arrayList2.get(i3));
            if (i3 < size2 - 1) {
                stringBuffer.append("|");
            }
        }
        c2.e().put("ic", stringBuffer.toString());
        return c2;
    }

    public static a a() {
        if (d == null) {
            d = new a();
        }
        return d;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0031 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0056 A[SYNTHETIC, Splitter:B:25:0x0056] */
    /* JADX WARNING: Removed duplicated region for block: B:39:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String b() {
        /*
            r6 = this;
            java.lang.String r4 = ""
            r0 = 0
            java.lang.String r1 = ""
            java.util.Properties r1 = new java.util.Properties
            r1.<init>()
            android.content.Context r2 = r6.i     // Catch:{ IOException -> 0x0034, all -> 0x0050 }
            android.content.res.Resources r2 = r2.getResources()     // Catch:{ IOException -> 0x0034, all -> 0x0050 }
            android.content.res.AssetManager r2 = r2.getAssets()     // Catch:{ IOException -> 0x0034, all -> 0x0050 }
            java.lang.String r3 = "config"
            java.io.InputStream r0 = r2.open(r3)     // Catch:{ IOException -> 0x0034, all -> 0x0050 }
            r1.load(r0)     // Catch:{ IOException -> 0x0074, all -> 0x006d }
            java.lang.String r2 = "channelid"
            java.lang.String r1 = r1.getProperty(r2)     // Catch:{ IOException -> 0x0074, all -> 0x006d }
            if (r0 == 0) goto L_0x006b
            r0.close()     // Catch:{ IOException -> 0x0063 }
            r0 = r1
        L_0x0029:
            java.lang.String r1 = ""
            boolean r1 = r0.equals(r4)
            if (r1 == 0) goto L_0x0033
            java.lang.String r0 = "DE51F"
        L_0x0033:
            return r0
        L_0x0034:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0038:
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0072 }
            cn.appmedia.ad.a.d.c(r0)     // Catch:{ all -> 0x0072 }
            if (r1 == 0) goto L_0x0079
            r1.close()     // Catch:{ IOException -> 0x0046 }
            r0 = r4
            goto L_0x0029
        L_0x0046:
            r0 = move-exception
            java.lang.String r0 = r0.toString()
            cn.appmedia.ad.a.d.c(r0)
            r0 = r4
            goto L_0x0029
        L_0x0050:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0054:
            if (r1 == 0) goto L_0x0059
            r1.close()     // Catch:{ IOException -> 0x005a }
        L_0x0059:
            throw r0
        L_0x005a:
            r1 = move-exception
            java.lang.String r1 = r1.toString()
            cn.appmedia.ad.a.d.c(r1)
            goto L_0x0059
        L_0x0063:
            r0 = move-exception
            java.lang.String r0 = r0.toString()
            cn.appmedia.ad.a.d.c(r0)
        L_0x006b:
            r0 = r1
            goto L_0x0029
        L_0x006d:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0054
        L_0x0072:
            r0 = move-exception
            goto L_0x0054
        L_0x0074:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0038
        L_0x0079:
            r0 = r4
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.appmedia.ad.g.a.b():java.lang.String");
    }

    private String b(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        String subscriberId = telephonyManager.getSubscriberId();
        if (subscriberId == null) {
            subscriberId = "0";
        }
        String str = "0" == 0 ? "0" : "0";
        String deviceId = telephonyManager.getDeviceId();
        if (deviceId == null) {
            deviceId = "0";
        }
        return String.valueOf(subscriberId) + "-" + "0" + "-" + str + "-" + deviceId;
    }

    private c c() {
        c cVar = new c();
        cVar.b(this.g);
        cVar.a(this.f);
        cVar.c("A-1.0.3");
        cVar.e(b());
        return cVar;
    }

    private c c(Context context) {
        this.i = context;
        c c2 = c();
        c2.d("login");
        c2.e().put("os", a);
        c2.e().put("model", b);
        c2.e().put("t", b(context));
        return c2;
    }

    /* access modifiers changed from: private */
    public c f(String str) {
        c c2 = c();
        c2.d("data");
        c2.e().put("did", str);
        c2.e().put("model", b);
        return c2;
    }

    public b a(Context context) {
        c c2 = c(context);
        d.b("application id:" + c2.b());
        c = Calendar.getInstance().getTimeInMillis();
        d.b("login time:" + c);
        try {
            String a2 = e.a(c2);
            this.e = String.valueOf(String.valueOf(k.e())) + "x" + String.valueOf(k.f());
            String a3 = g.a().a("http://www.appmedia.cn/add/adrequest/login.do", a2);
            if (a3.equalsIgnoreCase("fail")) {
                return null;
            }
            return (b) e.b(a3);
        } catch (ConnectTimeoutException e2) {
            d.c(e2.toString());
            return null;
        } catch (JSONException e3) {
            d.c(e3.toString());
            return null;
        } catch (IOException e4) {
            d.c(e4.toString());
            return null;
        }
    }

    public void a(String str) {
        this.g = str;
    }

    public boolean a(ArrayList arrayList, ArrayList arrayList2) {
        if (arrayList == null) {
            return false;
        }
        try {
            String a2 = e.a(a("app", arrayList, arrayList2));
            d.b(a2);
            if (!g.a().a("http://www.appmedia.cn/add/adrequest/info.do", f.a("1A2df@g9&S*#eRF4", a2)).equalsIgnoreCase("fail")) {
                return true;
            }
            d.b("send info fail");
            return false;
        } catch (Exception e2) {
            d.c(e2.toString());
            return false;
        }
    }

    public void b(String str) {
        this.f = str;
    }

    public h c(String str) {
        try {
            String a2 = g.a().a("http://www.appmedia.cn/add/adrequest/request.do", e.a(a(str, this.e)));
            if (!a2.equalsIgnoreCase("fail")) {
                return e.a(a2);
            }
        } catch (ConnectTimeoutException e2) {
            d.c(e2.toString());
            return null;
        } catch (JSONException e3) {
            d.c(e3.toString());
            return null;
        } catch (IOException e4) {
            d.c(e4.toString());
        }
        return null;
    }

    public void d(String str) {
        new h(this, str).start();
    }

    public void e(String str) {
        new f(this, str).start();
    }
}
