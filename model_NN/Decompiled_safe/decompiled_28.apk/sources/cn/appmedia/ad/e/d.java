package cn.appmedia.ad.e;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import cn.appmedia.ad.b.k;
import cn.appmedia.ad.c.e;
import cn.appmedia.ad.c.g;
import cn.appmedia.ad.d.i;
import cn.appmedia.ad.d.j;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import java.io.IOException;
import java.util.Vector;

public final class d extends e {
    protected Vector a = new Vector();
    private c i;
    private float j = k.d();
    private Drawable k;
    private Context l;
    private ImageView m;
    private RelativeLayout n;

    public d(Context context, String str, int i2, int i3) {
        super(context, str, i2, i3);
    }

    public void a() {
        int size = this.g.size();
        for (int i2 = 0; i2 < size; i2++) {
            g gVar = (g) this.g.get(i2);
            j a2 = i.a(gVar.a());
            if (a2 != null) {
                try {
                    a2.a(this.f, gVar.b());
                } catch (Exception e) {
                    cn.appmedia.ad.a.d.c(e.toString());
                }
                a2.a(gVar.c());
                this.a.add(a2);
            }
        }
        this.g.clear();
    }

    public void a(Context context, ViewGroup viewGroup) {
        int i2;
        int i3;
        int i4;
        int i5;
        this.l = context;
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -2);
        linearLayout.setLayoutParams(layoutParams);
        AbsoluteLayout absoluteLayout = new AbsoluteLayout(context);
        absoluteLayout.setLayoutParams(layoutParams);
        int size = this.a.size();
        for (int i6 = 0; i6 < size; i6++) {
            j jVar = (j) this.a.get(i6);
            View a2 = jVar.a(context);
            if (a2 != null) {
                int[] a3 = jVar.a();
                if (k.g()) {
                    int i7 = (int) (((float) a3[0]) / this.j);
                    i2 = (int) (((float) a3[1]) / this.j);
                    i3 = ((int) (((float) a3[3]) / this.j)) - i2;
                    i4 = i7;
                    i5 = ((int) (((float) a3[2]) / this.j)) - i7;
                } else {
                    int i8 = a3[0];
                    i2 = a3[1];
                    i3 = a3[3] - i2;
                    i4 = i8;
                    i5 = a3[2] - i8;
                }
                cn.appmedia.ad.a.d.b("rect:" + i4 + XmlConstant.SINGLE_SPACE + i2 + XmlConstant.SINGLE_SPACE + i5 + XmlConstant.SINGLE_SPACE + i3);
                AbsoluteLayout.LayoutParams layoutParams2 = new AbsoluteLayout.LayoutParams(i5, i3, i4, i2);
                e eVar = (e) this.h.get("click");
                if (eVar == null || !eVar.b()[0].a().equals("open") || !((String) eVar.b()[0].b().get("target")).equals("current")) {
                    a2.setLayoutParams(layoutParams2);
                    absoluteLayout.addView(a2);
                    if (i6 == 0) {
                        linearLayout.addView(absoluteLayout);
                    }
                } else if (i6 == 0) {
                    RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(i5, 15);
                    this.m = new ImageView(context);
                    this.m.setLayoutParams(layoutParams3);
                    this.m.setAdjustViewBounds(true);
                    this.m.setScaleType(ImageView.ScaleType.FIT_XY);
                    this.n = new RelativeLayout(context);
                    this.n.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
                    if (viewGroup.getTop() > 0) {
                        try {
                            this.k = new BitmapDrawable(this.l.getResources().getAssets().open("upflag.png"));
                            this.m.setImageDrawable(this.k);
                            linearLayout.addView(this.m);
                            a2.setLayoutParams(layoutParams2);
                            absoluteLayout.addView(a2);
                            linearLayout.addView(absoluteLayout);
                            linearLayout.addView(this.n);
                        } catch (IOException e) {
                            cn.appmedia.ad.a.d.b(e.toString());
                        }
                    } else {
                        try {
                            linearLayout.addView(this.n);
                            a2.setLayoutParams(layoutParams2);
                            absoluteLayout.addView(a2);
                            linearLayout.addView(absoluteLayout);
                            this.k = new BitmapDrawable(this.l.getResources().getAssets().open("downflag.png"));
                            this.m.setImageDrawable(this.k);
                            linearLayout.addView(this.m);
                        } catch (IOException e2) {
                            cn.appmedia.ad.a.d.b(e2.toString());
                        }
                    }
                } else {
                    absoluteLayout.addView(a2);
                }
            }
        }
        this.i = new c();
        this.i.a(linearLayout, viewGroup);
    }
}
