package cn.appmedia.ad.e;

import android.content.Context;
import android.view.ViewGroup;
import cn.appmedia.ad.c.g;
import java.util.Hashtable;
import java.util.Vector;

public abstract class e {
    protected String b;
    protected int c;
    protected int d;
    protected int e;
    protected Context f;
    protected Vector g = new Vector();
    protected Hashtable h = new Hashtable();

    public e(Context context, String str, int i, int i2) {
        this.f = context;
        this.b = str;
        this.c = i;
        this.d = i2;
        this.e = 0;
    }

    public cn.appmedia.ad.c.e a(String str) {
        if (this.h.containsKey(str)) {
            return (cn.appmedia.ad.c.e) this.h.get(str);
        }
        return null;
    }

    public void a() {
    }

    public void a(int i) {
        this.e = i;
    }

    public void a(Context context, ViewGroup viewGroup) {
    }

    public void a(g gVar) {
        this.g.add(gVar);
    }

    public int b() {
        return this.e;
    }

    public Hashtable c() {
        return this.h;
    }
}
