package cn.appmedia.ad;

import android.os.Handler;
import android.os.Message;
import android.view.ViewGroup;
import cn.appmedia.ad.a.d;
import cn.appmedia.ad.e.e;

class a implements Handler.Callback {
    final /* synthetic */ BannerAdView a;
    private ViewGroup b;

    public a(BannerAdView bannerAdView, ViewGroup viewGroup) {
        this.a = bannerAdView;
        this.b = viewGroup;
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 1:
                d.a("handleMessage MSG_RENDER_AD");
                this.a.setVisibility(0);
                if (this.a.mListener != null) {
                    this.a.mListener.onReceiveAdSuccess((BannerAdView) this.b);
                }
                ((e) message.obj).a(this.a.getContext(), this.b);
                return true;
            case 2:
                d.a("handleMessage MSG_LOAD_AD");
                int i = message.arg1;
                if (i == 0) {
                    d.a("load ad success");
                    this.a.mDefaultListener.onReceiveAdSuccess((BannerAdView) this.b);
                    if (this.a.mListener != null) {
                        this.a.mListener.onReceiveAdSuccess((BannerAdView) this.b);
                    }
                } else if (i == 2) {
                    d.a("load ad null");
                    this.a.mDefaultListener.onReceiveAdFailure((BannerAdView) this.b);
                } else if (i == 1) {
                    d.c("load ad failure");
                    this.a.mDefaultListener.onReceiveAdFailure((BannerAdView) this.b);
                    if (this.a.mListener != null) {
                        this.a.mListener.onReceiveAdFailure((BannerAdView) this.b);
                    }
                }
                return true;
            case 3:
                d.a("handleMessage MSG_CLEAR_AD");
                return true;
            case 4:
                d.a("stop show ad");
                this.a.mAdClient.m();
                return true;
            case 5:
                d.a("start show ad");
                this.a.mAdClient.j();
                return true;
            default:
                return false;
        }
    }
}
