package cn.appmedia.ad;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import cn.appmedia.ad.a.d;
import cn.appmedia.ad.b.k;
import cn.appmedia.ad.f.a;

public class BannerAdView extends RelativeLayout {
    /* access modifiers changed from: private */
    public k mAdClient;
    /* access modifiers changed from: private */
    public AdViewListener mDefaultListener = new c(this, null);
    /* access modifiers changed from: private */
    public AdViewListener mListener;

    public BannerAdView(Context context) {
        super(context);
        init(context);
    }

    public BannerAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    private void init(Context context) {
        Handler handler = new Handler(new a(this, this));
        a aVar = new a();
        this.mAdClient = k.a();
        this.mAdClient.a(context);
        this.mAdClient.a("1");
        this.mAdClient.a(aVar);
        this.mAdClient.a(handler);
        this.mAdClient.h();
        setOnClickListener(new b(this));
        setBackgroundColor(k.c);
        setClickable(true);
        setLongClickable(false);
        setVisibility(8);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (!z) {
            this.mAdClient.k();
            d.a("lose windowFocus");
        } else if (getVisibility() == 0) {
            this.mAdClient.j();
        }
    }

    public void setADVisibility(int i) {
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            getChildAt(i2).setVisibility(i);
        }
        super.setVisibility(i);
        invalidate();
    }

    public void setAdListener(AdViewListener adViewListener) {
        this.mListener = adViewListener;
    }

    public void setAdTextColor(int i) {
        k.b = i;
    }

    public void setVisibility(int i) {
        if (getVisibility() != i) {
            synchronized (this) {
                if (i == 0) {
                    this.mAdClient.j();
                } else {
                    this.mAdClient.k();
                }
            }
            setADVisibility(i);
        }
    }
}
