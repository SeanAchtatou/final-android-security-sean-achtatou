package cn.appmedia.ad.b;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public final class f {
    private static f a;
    private SQLiteDatabase b = this.c.getWritableDatabase();
    private SQLiteOpenHelper c;

    private f(Context context) {
        this.c = new g(context, "appmedia.db", null, 1);
    }

    public static f a(Context context) {
        if (a == null) {
            a = new f(context);
        }
        return a;
    }

    public long a() {
        long j = 0;
        Cursor rawQuery = this.b.rawQuery("SELECT time FROM scan", null);
        if (rawQuery.moveToNext()) {
            j = rawQuery.getLong(rawQuery.getColumnIndex("time"));
        }
        rawQuery.close();
        return j;
    }

    public void a(long j) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("time", Long.valueOf(j));
        int i = 0;
        Cursor rawQuery = this.b.rawQuery("SELECT COUNT(*) AS count FROM scan", null);
        int columnIndex = rawQuery.getColumnIndex("count");
        if (columnIndex > -1 && rawQuery.moveToNext()) {
            i = rawQuery.getInt(columnIndex);
        }
        rawQuery.close();
        if (i == 0) {
            this.b.insert("scan", "time", contentValues);
        } else {
            this.b.update("scan", contentValues, null, null);
        }
    }
}
