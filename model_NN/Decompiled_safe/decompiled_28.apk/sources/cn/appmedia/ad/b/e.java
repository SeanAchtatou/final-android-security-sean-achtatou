package cn.appmedia.ad.b;

import cn.appmedia.ad.a.d;
import java.util.Vector;

public final class e {
    private final Vector a = new Vector();

    public synchronized int a() {
        return this.a.size();
    }

    public synchronized void a(Vector vector) {
        this.a.addAll(vector);
    }

    public synchronized cn.appmedia.ad.e.e b() {
        cn.appmedia.ad.e.e eVar;
        if (this.a.isEmpty()) {
            eVar = null;
        } else {
            d.b("mAdPoolSize:" + this.a.size());
            eVar = (cn.appmedia.ad.e.e) this.a.remove(0);
            d.b("mAdPoolSize:" + this.a.size());
        }
        return eVar;
    }
}
