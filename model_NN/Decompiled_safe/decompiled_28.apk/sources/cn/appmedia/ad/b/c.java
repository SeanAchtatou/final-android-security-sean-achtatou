package cn.appmedia.ad.b;

import cn.appmedia.ad.a.d;
import cn.appmedia.ad.e.e;
import cn.appmedia.ad.f.b;

public class c extends Thread {
    final /* synthetic */ k a;
    private boolean b = true;
    private b c;
    private l d;
    private String e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(k kVar, String str, b bVar, l lVar, String str2) {
        super(str);
        this.a = kVar;
        synchronized (this) {
            this.c = bVar;
            this.d = lVar;
            this.e = str2;
        }
    }

    private int a() {
        e a2;
        synchronized (k.t) {
            a2 = this.a.i.a(this.e);
            if (this.a.i.b(this.e)) {
                this.a.i.b((k) null);
            }
        }
        if (a2 == null) {
            return 0;
        }
        d.a("show ad");
        k.e = a2;
        synchronized (k.t) {
            this.c.a(k.e);
            this.d.a("view", k.e, this.a.s, null);
        }
        return k.e.b();
    }

    public void a(b bVar, l lVar, String str) {
        synchronized (k.t) {
            this.c = bVar;
            this.d = lVar;
            this.e = str;
        }
    }

    public void a(boolean z) {
        this.b = z;
    }

    public void run() {
        while (this.b) {
            a();
            try {
                Thread.sleep(20000);
            } catch (InterruptedException e2) {
                d.c(e2.toString());
            }
        }
    }
}
