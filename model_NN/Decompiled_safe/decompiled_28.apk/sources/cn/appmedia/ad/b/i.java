package cn.appmedia.ad.b;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Message;
import cn.appmedia.ad.a.d;
import cn.appmedia.ad.c.b;
import cn.appmedia.ad.c.h;
import cn.appmedia.ad.e.e;
import cn.appmedia.ad.g.a;
import com.energysource.szj.embeded.SZJFrameworkConfig;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

public class i {
    private static i a;
    private final long b = 86400000;
    private Context c;
    private Timer d;
    private j e;
    private h f;
    private k g;

    private i(k kVar) {
        this.g = kVar;
        this.f = new h();
    }

    public static synchronized i a(k kVar) {
        i iVar;
        synchronized (i.class) {
            if (a == null) {
                a = new i(kVar);
            }
            iVar = a;
        }
        return iVar;
    }

    private ArrayList a(int i) {
        switch (i) {
            case 0:
                return c();
            case 1:
                return f();
            case 2:
                return e();
            case 3:
                return d();
            default:
                return null;
        }
    }

    private void a(k kVar, int i) {
        if (kVar != null) {
            Message.obtain(kVar.i(), 2, i, 0).sendToTarget();
        }
    }

    private void a(Timer timer) {
        timer.cancel();
        timer.purge();
        d.b("stop timer");
    }

    private void a(TimerTask timerTask) {
        timerTask.cancel();
        d.b("stop timerTask");
    }

    private ArrayList c() {
        ArrayList arrayList = new ArrayList();
        for (PackageInfo next : this.c.getPackageManager().getInstalledPackages(0)) {
            arrayList.add(String.valueOf(next.applicationInfo.loadLabel(this.c.getPackageManager()).toString()) + "-" + next.versionName);
        }
        return arrayList;
    }

    private boolean c(k kVar) {
        b a2 = a.a().a(kVar.b());
        if (a2 == null) {
            return false;
        }
        switch (a2.b()) {
            case 0:
                d.a("login success");
                a.a().b(a2.a());
                b();
                return true;
            default:
                d.c("login failure");
                return false;
        }
    }

    private ArrayList d() {
        ArrayList arrayList = new ArrayList();
        for (PackageInfo next : this.c.getPackageManager().getInstalledPackages(1)) {
            if ((next.applicationInfo.flags & 1) > 0) {
                arrayList.add(String.valueOf(next.applicationInfo.loadLabel(this.c.getPackageManager()).toString()) + "-" + next.versionName);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void d(k kVar) {
        e a2 = this.f.a(kVar.c());
        if (!k.a) {
            k.a = c(kVar);
        }
        if (k.a) {
            d.a("start load ad");
            h c2 = a.a().c(kVar.c());
            if (c2 == null || c2.b() != 0) {
                a(kVar, 1);
                return;
            }
            cn.appmedia.ad.c.i[] a3 = c2.a();
            if (a3.length == 0) {
                a.a().d("did_view_0");
                a(kVar, 2);
                return;
            }
            Vector vector = new Vector();
            for (cn.appmedia.ad.c.i iVar : a3) {
                if (iVar != null) {
                    try {
                        vector.add(a.a(this.c, iVar));
                    } catch (Exception e2) {
                        d.c(e2.toString());
                    }
                }
            }
            if (!vector.isEmpty()) {
                a2.a(vector);
                a(kVar, 0);
                return;
            }
            return;
        }
        a(kVar, 1);
    }

    private ArrayList e() {
        ArrayList arrayList = new ArrayList();
        for (PackageInfo next : this.c.getPackageManager().getInstalledPackages(0)) {
            if ((next.applicationInfo.flags & 1) <= 0) {
                arrayList.add(String.valueOf(next.applicationInfo.loadLabel(this.c.getPackageManager()).toString()) + "-" + next.versionName);
            }
        }
        return arrayList;
    }

    private ArrayList f() {
        ArrayList arrayList = new ArrayList();
        List<ActivityManager.RecentTaskInfo> recentTasks = ((ActivityManager) this.c.getSystemService(SZJFrameworkConfig.ACTIVITY)).getRecentTasks(10, 1);
        for (int i = 0; i < recentTasks.size(); i++) {
            PackageManager packageManager = this.c.getPackageManager();
            String packageName = recentTasks.get(i).origActivity.getPackageName();
            try {
                arrayList.add(String.valueOf(packageManager.getApplicationLabel(packageManager.getApplicationInfo(packageName, 0)).toString()) + "-" + packageManager.getPackageInfo(packageName, 1).versionName);
            } catch (PackageManager.NameNotFoundException e2) {
                d.c(e2.toString());
            }
        }
        return arrayList;
    }

    public e a(String str) {
        return this.f.a(str).b();
    }

    public void a() {
        if (this.e != null) {
            a(this.e);
        }
        if (this.d != null) {
            a(this.d);
        }
        d.a("stop get ad thread");
    }

    public void a(Context context) {
        this.c = context;
    }

    public void b() {
        f a2 = f.a(this.c);
        long a3 = a2.a();
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - a3 > 86400000) {
            a2.a(currentTimeMillis);
            new m(this, a(3), a(2)).start();
        }
    }

    public void b(k kVar) {
        if (kVar == null) {
            new b(this, this.g).start();
        } else {
            new b(this, kVar).start();
        }
    }

    public boolean b(String str) {
        return this.f.a(str).a() <= 2;
    }
}
