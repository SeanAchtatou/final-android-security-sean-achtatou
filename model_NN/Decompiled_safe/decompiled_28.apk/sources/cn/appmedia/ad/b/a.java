package cn.appmedia.ad.b;

import android.content.Context;
import cn.appmedia.ad.c.g;
import cn.appmedia.ad.c.i;
import cn.appmedia.ad.e.e;
import cn.appmedia.ad.e.f;

public final class a {
    public static e a(Context context, i iVar) {
        e a = f.a(context, iVar.a(), iVar.b(), iVar.f()[0], iVar.f()[1]);
        a.a(iVar.c());
        for (g a2 : iVar.d()) {
            a.a(a2);
        }
        for (int i = 0; i < iVar.e().length; i++) {
            a.c().put(iVar.e()[i].a(), iVar.e()[i]);
        }
        a.a();
        return a;
    }
}
