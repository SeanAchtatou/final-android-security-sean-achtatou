package cn.appmedia.ad.b;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import cn.appmedia.ad.action.b;
import cn.appmedia.ad.e.e;
import cn.appmedia.ad.g.a;

public final class l {
    public void a(String str, e eVar, Context context, View view) {
        cn.appmedia.ad.c.e a = eVar.a(str);
        if (a != null) {
            if (!TextUtils.isEmpty(a.c())) {
                a.a().d(a.c());
            }
            cn.appmedia.ad.c.a[] b = a.b();
            if (b != null) {
                for (cn.appmedia.ad.c.a aVar : b) {
                    b a2 = cn.appmedia.ad.action.e.a(aVar.a());
                    if (a2 != null) {
                        a2.a(aVar.b(), context, view);
                        if (!TextUtils.isEmpty(aVar.c())) {
                            a.a().d(aVar.c());
                        }
                    }
                }
            }
        }
    }
}
