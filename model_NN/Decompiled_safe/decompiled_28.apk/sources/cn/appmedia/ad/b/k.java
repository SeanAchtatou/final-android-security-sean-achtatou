package cn.appmedia.ad.b;

import android.content.Context;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import cn.appmedia.ad.a.d;
import cn.appmedia.ad.e.e;
import cn.appmedia.ad.f.b;

public class k {
    public static boolean a = false;
    public static int b = -1;
    public static int c = 0;
    public static e e;
    public static double f = 0.0d;
    public static double g = 0.0d;
    public static String h = "";
    public static k j;
    private static c k;
    private static float n;
    private static int o;
    private static int p;
    private static boolean q = false;
    /* access modifiers changed from: private */
    public static Object t = new Object();
    public b d;
    public i i;
    private l l = new l();
    private Handler m;
    private String r = "";
    /* access modifiers changed from: private */
    public Context s;
    private LocationListener u = new d(this);

    public static k a() {
        if (j == null) {
            j = new k();
        }
        return j;
    }

    public static float d() {
        return n;
    }

    public static int e() {
        return o;
    }

    public static int f() {
        return p;
    }

    public static boolean g() {
        return q;
    }

    public static e l() {
        return e;
    }

    private void o() {
        LocationManager locationManager = (LocationManager) this.s.getSystemService("location");
        locationManager.removeUpdates(this.u);
        locationManager.requestLocationUpdates("gps", 0, 0.0f, this.u);
        locationManager.requestLocationUpdates("network", 0, 0.0f, this.u);
    }

    private void p() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) this.s.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        n = displayMetrics.density;
        int i2 = displayMetrics.widthPixels;
        int i3 = displayMetrics.heightPixels;
        if (i2 < i3) {
            if (i2 <= 320) {
                o = (int) Math.ceil((double) (((float) i2) * n));
                p = (int) Math.ceil((double) (((float) i3) * n));
                q = true;
            } else {
                o = i2;
                p = i3;
                q = false;
            }
        } else if (i3 <= 320) {
            o = (int) Math.ceil((double) (((float) i2) * n));
            p = (int) Math.ceil((double) (((float) i3) * n));
            q = true;
        } else {
            o = i2;
            p = i3;
            q = false;
        }
        d.a("screen: " + o + "*" + p);
    }

    private void q() {
        if (k == null || !k.isAlive()) {
            k = new c(this, "ShowAdThread", this.d, this.l, this.r);
            k.start();
            d.a("start show thread");
            return;
        }
        k.a(this.d, this.l, this.r);
        d.a("update show thread");
    }

    public void a(Context context) {
        this.s = context;
        this.i = i.a(this);
        this.i.a(context.getApplicationContext());
        p();
        o();
    }

    public void a(Handler handler) {
        this.m = handler;
    }

    public void a(View view) {
        this.l.a("click", e, this.s, view);
    }

    public void a(b bVar) {
        this.d = bVar;
    }

    public void a(String str) {
        this.r = str;
    }

    public Context b() {
        return this.s;
    }

    public String c() {
        return this.r;
    }

    public void h() {
        this.i.b(this);
    }

    public Handler i() {
        return this.m;
    }

    public void j() {
        q();
    }

    public void k() {
        m();
        this.i.a();
    }

    public void m() {
        if (k != null) {
            k.a(false);
            k = null;
            d.a("stop show ad thread");
        }
    }
}
