package cn.appmedia.ad.b;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

class d implements LocationListener {
    final /* synthetic */ k a;

    d(k kVar) {
        this.a = kVar;
    }

    public void onLocationChanged(Location location) {
        k.f = location.getLongitude();
        k.g = location.getLatitude();
        k.h = location.getProvider();
        cn.appmedia.ad.a.d.b("location provide:" + k.h + " longitude:" + k.f + " latitude:" + k.g);
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
