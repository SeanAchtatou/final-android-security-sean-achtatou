package cn.appmedia.ad.a;

import java.io.UnsupportedEncodingException;

public class a {
    static final /* synthetic */ boolean a = (!a.class.desiredAssertionStatus());

    public static String a(byte[] bArr, int i) {
        try {
            return new String(b(bArr, i), "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[] a(byte[] bArr, int i, int i2, int i3) {
        e eVar = new e(i3, null);
        int i4 = (i2 / 3) * 4;
        if (!eVar.d) {
            switch (i2 % 3) {
                case 1:
                    i4 += 2;
                    break;
                case 2:
                    i4 += 3;
                    break;
            }
        } else if (i2 % 3 > 0) {
            i4 += 4;
        }
        if (eVar.e && i2 > 0) {
            i4 += (((i2 - 1) / 57) + 1) * (eVar.f ? 2 : 1);
        }
        eVar.a = new byte[i4];
        eVar.a(bArr, i, i2, true);
        if (a || eVar.b == i4) {
            return eVar.a;
        }
        throw new AssertionError();
    }

    public static byte[] b(byte[] bArr, int i) {
        return a(bArr, 0, bArr.length, i);
    }
}
