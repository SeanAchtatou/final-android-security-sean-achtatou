package cn.appmedia.ad.a;

import android.content.Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public final class b {
    private static b a;

    public static b a() {
        if (a == null) {
            a = new b();
        }
        return a;
    }

    public void a(Context context, String str, InputStream inputStream) {
        FileOutputStream fileOutputStream;
        Exception e;
        Throwable th;
        d.b("cacheImage start");
        try {
            fileOutputStream = new FileOutputStream(new File(context.getCacheDir(), str));
            try {
                byte[] bArr = new byte[128];
                while (inputStream.read(bArr) != -1) {
                    fileOutputStream.write(bArr);
                }
                try {
                    inputStream.close();
                    fileOutputStream.close();
                } catch (IOException e2) {
                    d.c(e2.toString());
                }
            } catch (Exception e3) {
                e = e3;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            fileOutputStream = null;
            e = exc;
            try {
                d.c(e.toString());
                try {
                    inputStream.close();
                    fileOutputStream.close();
                } catch (IOException e5) {
                    d.c(e5.toString());
                }
                d.b("cacheImage end");
            } catch (Throwable th2) {
                th = th2;
                try {
                    inputStream.close();
                    fileOutputStream.close();
                } catch (IOException e6) {
                    d.c(e6.toString());
                }
                throw th;
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            fileOutputStream = null;
            th = th4;
            inputStream.close();
            fileOutputStream.close();
            throw th;
        }
        d.b("cacheImage end");
    }

    public boolean a(Context context, String str) {
        return new File(context.getCacheDir(), str).exists();
    }

    public InputStream b(Context context, String str) {
        try {
            return new FileInputStream(String.valueOf(context.getCacheDir().getPath()) + "/" + str);
        } catch (FileNotFoundException e) {
            d.c(e.toString());
            return null;
        }
    }
}
