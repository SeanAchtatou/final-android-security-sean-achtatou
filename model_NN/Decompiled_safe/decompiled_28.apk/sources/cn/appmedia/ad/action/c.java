package cn.appmedia.ad.action;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import java.util.Map;

public class c implements b {
    private Context a;

    public void a(Map map, Context context, View view) {
        this.a = context;
        Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + ((String) map.get("msisdn"))));
        intent.putExtra("sms_body", (String) map.get("msg"));
        this.a.startActivity(intent);
    }
}
