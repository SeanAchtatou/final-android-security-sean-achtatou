package cn.appmedia.ad.action;

import android.content.Context;
import android.os.Handler;
import cn.appmedia.ad.b.k;

public class JSAction {
    /* access modifiers changed from: private */
    public String a = "";
    /* access modifiers changed from: private */
    public Context b;
    private Handler c = new Handler();

    public JSAction(Context context) {
        this.b = context;
    }

    /* access modifiers changed from: private */
    public String a() {
        String c2 = k.l().a("click").c();
        return c2.replace(c2.substring(c2.indexOf("_") + 1, c2.lastIndexOf("_")), this.a);
    }

    public void sms(String str, String str2) {
        this.c.post(new h(this, str, str2));
    }

    public void tel(String str) {
        this.c.post(new g(this, str));
    }
}
