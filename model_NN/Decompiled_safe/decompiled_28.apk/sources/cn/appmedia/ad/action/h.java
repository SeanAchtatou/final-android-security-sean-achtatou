package cn.appmedia.ad.action;

import android.content.Intent;
import android.net.Uri;
import cn.appmedia.ad.g.a;
import cn.domob.android.ads.DomobAdManager;

class h implements Runnable {
    final /* synthetic */ JSAction a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;

    h(JSAction jSAction, String str, String str2) {
        this.a = jSAction;
        this.b = str;
        this.c = str2;
    }

    public void run() {
        this.a.a = DomobAdManager.ACTION_SMS;
        a.a().e(this.a.a());
        Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + this.b));
        intent.putExtra("sms_body", this.c);
        this.a.b.startActivity(intent);
    }
}
