package cn.appmedia.ad.action;

import android.content.Intent;
import android.net.Uri;
import cn.appmedia.ad.g.a;

class g implements Runnable {
    final /* synthetic */ JSAction a;
    private final /* synthetic */ String b;

    g(JSAction jSAction, String str) {
        this.a = jSAction;
        this.b = str;
    }

    public void run() {
        this.a.a = "phone";
        a.a().e(this.a.a());
        this.a.b.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + this.b)));
    }
}
