package cn.appmedia.ad.action;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Message;
import android.view.View;
import cn.appmedia.ad.a.d;
import cn.appmedia.ad.b.k;
import cn.appmedia.ad.d.g;
import cn.appmedia.ad.d.h;
import cn.domob.android.ads.DomobAdManager;
import java.util.Map;

public class f implements b {
    public void a(Map map, Context context, View view) {
        String str = (String) map.get(DomobAdManager.ACTION_URL);
        String str2 = (String) map.get("target");
        if (str2 == null || str2.length() == 0) {
            str2 = "browser";
        }
        String str3 = !str.toLowerCase().startsWith("http://") ? "http://" + str : str;
        if (str2.equals("browser")) {
            d.b("open browser");
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str3)));
        } else if (str2.equals("current")) {
            d.b("browser:open webview");
            d.b("browser:" + str3);
            Message.obtain(k.a().i(), 4, 0, 0).sendToTarget();
            if (k.f() > k.e()) {
                new h(context, str3, view).a();
            } else {
                new g(context, str3, view).a();
            }
        } else {
            ((String) map.get("target")).equals("none");
        }
    }
}
