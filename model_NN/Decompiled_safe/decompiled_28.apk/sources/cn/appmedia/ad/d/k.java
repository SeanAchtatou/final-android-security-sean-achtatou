package cn.appmedia.ad.d;

import android.webkit.WebChromeClient;
import android.webkit.WebView;

class k extends WebChromeClient {
    final /* synthetic */ h a;

    k(h hVar) {
        this.a = hVar;
    }

    public void onProgressChanged(WebView webView, int i) {
        super.onProgressChanged(webView, i);
        if (i < 100) {
            this.a.j.setVisibility(0);
        }
        if (i == 100) {
            this.a.j.setVisibility(8);
            h.k.setVisibility(0);
            this.a.a.setVisibility(0);
        }
    }
}
