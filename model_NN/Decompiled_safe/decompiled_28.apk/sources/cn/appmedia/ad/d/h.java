package cn.appmedia.ad.d;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.appmedia.ad.a.d;
import cn.appmedia.ad.action.JSAction;
import cn.appmedia.ad.b.k;
import java.io.IOException;

public class h extends RelativeLayout {
    /* access modifiers changed from: private */
    public static RelativeLayout k;
    /* access modifiers changed from: private */
    public static Boolean m = true;
    /* access modifiers changed from: private */
    public static int n;
    /* access modifiers changed from: private */
    public static ViewGroup q = null;
    /* access modifiers changed from: private */
    public WebView a;
    private String b;
    /* access modifiers changed from: private */
    public Context c;
    private Animation d;
    private Animation e;
    private View f;
    /* access modifiers changed from: private */
    public RelativeLayout g;
    private TextView h;
    private ImageView i;
    /* access modifiers changed from: private */
    public ProgressBar j;
    private RelativeLayout.LayoutParams l;
    private Drawable o;
    private String p = "";

    public h(Context context, String str, View view) {
        super(context);
        this.b = str;
        this.c = context;
        this.f = view;
    }

    private void a(int i2) {
        this.e = new TranslateAnimation(0.0f, 0.0f, (float) i2, 0.0f);
        this.e.setDuration(1200);
        this.d = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) i2);
        this.d.setDuration(1200);
    }

    private void e() {
        this.l = new RelativeLayout.LayoutParams(-1, n);
        k = new RelativeLayout(this.c);
        k.setLayoutParams(this.l);
        k.setId(1);
        k.setBackgroundColor(-1);
    }

    private void f() {
        try {
            this.o = new BitmapDrawable(this.c.getResources().getAssets().open(this.p));
        } catch (IOException e2) {
            d.b(e2.toString());
        }
    }

    private void g() {
        this.a = new WebView(this.c);
        this.a.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.a.loadUrl(this.b);
        this.a.setVisibility(8);
        k.addView(this.a);
    }

    private void h() {
        AnimationDrawable animationDrawable = new AnimationDrawable();
        BitmapDrawable bitmapDrawable = new BitmapDrawable(this.c.getAssets().open("refresh1.png"));
        BitmapDrawable bitmapDrawable2 = new BitmapDrawable(this.c.getAssets().open("refresh2.png"));
        BitmapDrawable bitmapDrawable3 = new BitmapDrawable(this.c.getAssets().open("refresh3.png"));
        BitmapDrawable bitmapDrawable4 = new BitmapDrawable(this.c.getAssets().open("refresh4.png"));
        BitmapDrawable bitmapDrawable5 = new BitmapDrawable(this.c.getAssets().open("refresh5.png"));
        BitmapDrawable bitmapDrawable6 = new BitmapDrawable(this.c.getAssets().open("refresh6.png"));
        BitmapDrawable bitmapDrawable7 = new BitmapDrawable(this.c.getAssets().open("refresh7.png"));
        BitmapDrawable bitmapDrawable8 = new BitmapDrawable(this.c.getAssets().open("refresh8.png"));
        animationDrawable.addFrame(bitmapDrawable, 200);
        animationDrawable.addFrame(bitmapDrawable2, 200);
        animationDrawable.addFrame(bitmapDrawable3, 200);
        animationDrawable.addFrame(bitmapDrawable4, 200);
        animationDrawable.addFrame(bitmapDrawable5, 200);
        animationDrawable.addFrame(bitmapDrawable6, 200);
        animationDrawable.addFrame(bitmapDrawable7, 200);
        animationDrawable.addFrame(bitmapDrawable8, 200);
        animationDrawable.setOneShot(false);
        this.j.setIndeterminate(false);
        this.j.setIndeterminateDrawable(animationDrawable);
        animationDrawable.start();
    }

    private void i() {
        this.j = new ProgressBar(this.c);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(30, 30);
        layoutParams.addRule(13);
        this.j.setLayoutParams(layoutParams);
        k.addView(this.j);
        try {
            h();
        } catch (IOException e2) {
            d.c(e2.getMessage());
        }
    }

    private void j() {
        this.h = new TextView(this.c);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, 2);
        this.h.setBackgroundColor(-16777216);
        if (this.g.getTop() <= 0 || this.g.getTop() == n) {
            layoutParams.addRule(12);
        } else {
            layoutParams.addRule(10);
        }
        this.h.setLayoutParams(layoutParams);
        k.addView(this.h);
    }

    public void a() {
        if (k.f() < 600) {
            n = ((k.f() * 3) / 4) - 80;
        } else {
            n = ((k.f() * 1) / 2) - 80;
        }
        this.g = (RelativeLayout) this.f;
        q = (ViewGroup) this.g.getChildAt(0);
        if (this.g.getTop() <= 0 || this.g.getTop() == n) {
            if (m.booleanValue()) {
                e();
                ((ViewGroup) q.getChildAt(0)).addView(k);
            }
            a(-n);
            this.p = "upflag.png";
            f();
            this.i = (ImageView) q.getChildAt(2);
        } else {
            if (m.booleanValue()) {
                e();
                ((ViewGroup) q.getChildAt(2)).addView(k);
            }
            a(n);
            this.p = "downflag.png";
            f();
            this.i = (ImageView) q.getChildAt(0);
        }
        if (m.booleanValue()) {
            g();
            j();
            i();
            this.a.getSettings().setJavaScriptEnabled(true);
            this.a.addJavascriptInterface(new JSAction(this.c), "appmedia");
            this.a.setWebViewClient(new l(this));
            this.a.setWebChromeClient(new k(this));
            this.g.startAnimation(this.e);
            this.i.setImageDrawable(this.o);
        } else if (this.g.getBottom() - this.g.getTop() == 65) {
            m = true;
            ((this.g.getTop() <= 0 || this.g.getTop() == n) ? (ViewGroup) q.getChildAt(0) : (ViewGroup) q.getChildAt(2)).removeView(k);
            a();
            return;
        } else {
            if (this.g.getTop() <= 0 || this.g.getTop() == n) {
                this.p = "downflag.png";
                f();
                this.i = (ImageView) q.getChildAt(2);
            } else {
                this.p = "upflag.png";
                f();
                this.i = (ImageView) q.getChildAt(0);
            }
            this.i.setImageDrawable(this.o);
            this.g.startAnimation(this.d);
        }
        this.e.setAnimationListener(new p(this));
        this.d.setAnimationListener(new n(this));
    }
}
