package cn.appmedia.ad.d;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import cn.appmedia.ad.a.d;
import cn.appmedia.ad.action.JSAction;
import cn.appmedia.ad.b.k;
import java.io.IOException;

public class g extends RelativeLayout {
    /* access modifiers changed from: private */
    public static RelativeLayout k;
    private static int m;
    /* access modifiers changed from: private */
    public WebView a;
    private String b;
    /* access modifiers changed from: private */
    public Context c;
    private Animation d;
    /* access modifiers changed from: private */
    public Animation e;
    private View f;
    /* access modifiers changed from: private */
    public RelativeLayout g;
    /* access modifiers changed from: private */
    public RelativeLayout h;
    /* access modifiers changed from: private */
    public ImageView i;
    /* access modifiers changed from: private */
    public ProgressBar j;
    private RelativeLayout.LayoutParams l;
    /* access modifiers changed from: private */
    public Drawable n;
    /* access modifiers changed from: private */
    public String o = "";

    public g(Context context, String str, View view) {
        super(context);
        this.b = str;
        this.c = context;
        this.f = view;
    }

    private void a(float f2) {
        this.d = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, f2, 1, 0.0f);
        this.d.setDuration(1200);
        this.e = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, f2);
        this.e.setDuration(1200);
    }

    private void c() {
        this.l = new RelativeLayout.LayoutParams(-1, m);
        if (this.g.getTop() > 0) {
            this.l.addRule(12);
        } else {
            this.l.addRule(10);
        }
        k = new RelativeLayout(this.c);
        k.setLayoutParams(this.l);
        k.setId(1);
        k.setBackgroundColor(-1);
    }

    /* access modifiers changed from: private */
    public void d() {
        try {
            this.n = new BitmapDrawable(this.c.getResources().getAssets().open(this.o));
        } catch (IOException e2) {
            d.b(e2.toString());
        }
    }

    private void e() {
        this.a = new WebView(this.c);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        if (this.g.getTop() > 0) {
            layoutParams.addRule(3, 10001);
        } else {
            layoutParams.addRule(2, 10001);
        }
        this.a.setLayoutParams(layoutParams);
        this.a.loadUrl(this.b);
        this.a.setVisibility(8);
        k.addView(this.a);
    }

    private void f() {
        AnimationDrawable animationDrawable = new AnimationDrawable();
        BitmapDrawable bitmapDrawable = new BitmapDrawable(this.c.getAssets().open("refresh1.png"));
        BitmapDrawable bitmapDrawable2 = new BitmapDrawable(this.c.getAssets().open("refresh2.png"));
        BitmapDrawable bitmapDrawable3 = new BitmapDrawable(this.c.getAssets().open("refresh3.png"));
        BitmapDrawable bitmapDrawable4 = new BitmapDrawable(this.c.getAssets().open("refresh4.png"));
        BitmapDrawable bitmapDrawable5 = new BitmapDrawable(this.c.getAssets().open("refresh5.png"));
        BitmapDrawable bitmapDrawable6 = new BitmapDrawable(this.c.getAssets().open("refresh6.png"));
        BitmapDrawable bitmapDrawable7 = new BitmapDrawable(this.c.getAssets().open("refresh7.png"));
        BitmapDrawable bitmapDrawable8 = new BitmapDrawable(this.c.getAssets().open("refresh8.png"));
        animationDrawable.addFrame(bitmapDrawable, 200);
        animationDrawable.addFrame(bitmapDrawable2, 200);
        animationDrawable.addFrame(bitmapDrawable3, 200);
        animationDrawable.addFrame(bitmapDrawable4, 200);
        animationDrawable.addFrame(bitmapDrawable5, 200);
        animationDrawable.addFrame(bitmapDrawable6, 200);
        animationDrawable.addFrame(bitmapDrawable7, 200);
        animationDrawable.addFrame(bitmapDrawable8, 200);
        animationDrawable.setOneShot(false);
        this.j.setIndeterminate(false);
        this.j.setIndeterminateDrawable(animationDrawable);
        animationDrawable.start();
    }

    private void g() {
        this.j = new ProgressBar(this.c);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(30, 30);
        layoutParams.addRule(13);
        this.j.setLayoutParams(layoutParams);
        k.addView(this.j);
        try {
            f();
        } catch (IOException e2) {
            d.c(e2.getMessage());
        }
    }

    private void h() {
        this.i = new ImageView(this.c);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, 30);
        if (this.o.equals("downflag.png")) {
            layoutParams.addRule(10);
        } else if (this.o.equals("upflag.png")) {
            layoutParams.addRule(12);
        }
        this.i.setLayoutParams(layoutParams);
        this.i.setId(10001);
        k.addView(this.i);
        this.i.setAdjustViewBounds(true);
        this.i.setScaleType(ImageView.ScaleType.FIT_XY);
        this.i.setImageDrawable(this.n);
        this.i.setOnClickListener(new f(this));
    }

    public void a() {
        if (k.f() < 600) {
            m = (k.f() * 3) / 4;
        }
        this.g = (RelativeLayout) this.f;
        this.h = (RelativeLayout) this.f.getParent();
        c();
        this.h.addView(k);
        if (this.g.getTop() > 0) {
            a(1.0f);
            this.o = "downflag.png";
            d();
        } else {
            a(-1.0f);
            this.o = "upflag.png";
        }
        d();
        h();
        e();
        g();
        this.g.setVisibility(8);
        k.startAnimation(this.d);
        this.i.setImageDrawable(this.n);
        this.a.getSettings().setJavaScriptEnabled(true);
        this.a.addJavascriptInterface(new JSAction(this.c), "appmedia");
        this.a.setWebViewClient(new e(this));
        this.a.setWebChromeClient(new d(this));
        this.d.setAnimationListener(new c(this));
        this.e.setAnimationListener(new b(this));
    }
}
