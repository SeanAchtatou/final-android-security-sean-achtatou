package cn.appmedia.ad.d;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.ImageView;
import cn.appmedia.ad.a.b;
import cn.appmedia.ad.a.d;
import cn.appmedia.ad.g.g;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

public final class o extends j {
    private String b;

    private void a(InputStream inputStream, OutputStream outputStream) {
        if (inputStream != null && outputStream != null) {
            byte[] bArr = new byte[256];
            while (true) {
                int read = inputStream.read(bArr);
                if (read > 0) {
                    outputStream.write(bArr, 0, read);
                } else {
                    return;
                }
            }
        }
    }

    public View a(Context context) {
        if (this.b == null) {
            return null;
        }
        Bitmap decodeStream = BitmapFactory.decodeStream(b.a().b(context, this.b));
        if (decodeStream == null) {
            return null;
        }
        ImageView imageView = new ImageView(context);
        imageView.setImageBitmap(decodeStream);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        return imageView;
    }

    public void a(Context context, Map map) {
        int lastIndexOf;
        b a = b.a();
        String str = (String) map.get("s");
        if (str != null && (lastIndexOf = str.lastIndexOf("/")) != -1) {
            this.b = str.substring(lastIndexOf + 1);
            if (!a.a(context, this.b)) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                try {
                    a(g.a().a(str), byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    if (byteArray.length > 0) {
                        a.a(context, this.b, new ByteArrayInputStream(byteArray));
                    }
                    if (byteArrayOutputStream != null) {
                        try {
                            byteArrayOutputStream.close();
                        } catch (IOException e) {
                            d.c(e.toString());
                        }
                    }
                } catch (Exception e2) {
                    d.c(e2.toString());
                    if (byteArrayOutputStream != null) {
                        try {
                            byteArrayOutputStream.close();
                        } catch (IOException e3) {
                            d.c(e3.toString());
                        }
                    }
                } catch (Throwable th) {
                    if (byteArrayOutputStream != null) {
                        try {
                            byteArrayOutputStream.close();
                        } catch (IOException e4) {
                            d.c(e4.toString());
                        }
                    }
                    throw th;
                }
            }
        }
    }
}
