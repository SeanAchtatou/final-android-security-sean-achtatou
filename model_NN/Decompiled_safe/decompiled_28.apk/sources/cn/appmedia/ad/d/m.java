package cn.appmedia.ad.d;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.appmedia.ad.b.k;
import com.google.ads.AdActivity;
import com.madhouse.android.ads.AdView;
import java.util.Map;

public final class m extends j {
    public static int b = -1;
    private String c;
    private int d;
    private boolean e;
    private boolean f;

    private TextView b(Context context) {
        TextView textView = new TextView(context);
        textView.setTypeface(textView.getTypeface(), (!this.e || !this.f) ? this.e ? 1 : this.f ? 2 : 0 : 3);
        textView.setTextColor(b);
        textView.setTextSize((float) this.d);
        textView.setText(this.c);
        textView.setPadding(10, 0, 10, 0);
        return textView;
    }

    private TextView c(Context context) {
        TextView textView = new TextView(context);
        textView.setTextColor(b);
        textView.setTextSize(10.0f);
        textView.setTypeface(textView.getTypeface(), 2);
        textView.setText("Ads by AppMedia");
        textView.setPadding(0, 0, 3, 3);
        return textView;
    }

    public View a(Context context) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        TextView b2 = b(context);
        layoutParams.addRule(13);
        b2.setGravity(3);
        b2.setLayoutParams(layoutParams);
        relativeLayout.addView(b2);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        TextView c2 = c(context);
        layoutParams2.addRule(12);
        layoutParams2.addRule(11);
        c2.setLayoutParams(layoutParams2);
        relativeLayout.addView(c2);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1, -7829368});
        gradientDrawable.setBounds(0, 0, AdView.AD_MEASURE_320, 50);
        gradientDrawable.setGradientType(0);
        relativeLayout.setBackgroundDrawable(gradientDrawable);
        relativeLayout.getBackground().setAlpha(80);
        return relativeLayout;
    }

    public void a(Context context, Map map) {
        if (map.get("c") == null) {
            this.c = "";
        } else {
            this.c = ((String) map.get("c")).trim().length() <= 20 ? ((String) map.get("c")).trim() : ((String) map.get("c")).trim().substring(0, 20);
        }
        String str = (String) map.get("fs");
        if (str != null) {
            this.d = Integer.parseInt(str);
            if (this.d == 0) {
                this.d = 14;
            }
        } else {
            this.d = 14;
        }
        if (k.b == 0) {
            b = -1;
        } else {
            b = k.b;
        }
        String str2 = (String) map.get("fa");
        if (str2 != null && str2.length() > 0) {
            this.e = str2.indexOf("b") > -1;
            this.f = str2.indexOf(AdActivity.INTENT_ACTION_PARAM) > -1;
        }
    }
}
