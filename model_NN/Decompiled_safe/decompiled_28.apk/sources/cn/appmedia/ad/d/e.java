package cn.appmedia.ad.d;

import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class e extends WebViewClient {
    final /* synthetic */ g a;

    e(g gVar) {
        this.a = gVar;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str.endsWith(".apk")) {
            this.a.c.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        }
        this.a.a.loadUrl(str);
        return false;
    }
}
