package cn.domob.android.ads;

final class b {
    b() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static boolean a(cn.domob.android.ads.DBHelper r7, java.lang.String r8, java.lang.String r9, java.util.Hashtable<java.lang.String, byte[]> r10) {
        /*
            java.lang.String r0 = "DomobSDK"
            r0 = 0
            r1 = 0
            android.net.Uri r2 = cn.domob.android.ads.DBHelper.b     // Catch:{ Exception -> 0x0072 }
            android.database.Cursor r1 = r7.b(r2, r9)     // Catch:{ Exception -> 0x0072 }
            if (r1 == 0) goto L_0x004b
            int r2 = r1.getCount()     // Catch:{ Exception -> 0x007c }
            if (r2 <= 0) goto L_0x004b
            r1.moveToFirst()     // Catch:{ Exception -> 0x007c }
            java.lang.String r2 = "_image"
            int r2 = r1.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x007c }
            byte[] r2 = r1.getBlob(r2)     // Catch:{ Exception -> 0x007c }
            if (r2 == 0) goto L_0x0081
            java.lang.String r3 = "DomobSDK"
            r4 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r4)     // Catch:{ Exception -> 0x007c }
            if (r3 == 0) goto L_0x003e
            java.lang.String r3 = "DomobSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007c }
            java.lang.String r5 = "load image from cache:"
            r4.<init>(r5)     // Catch:{ Exception -> 0x007c }
            java.lang.StringBuilder r4 = r4.append(r9)     // Catch:{ Exception -> 0x007c }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x007c }
            android.util.Log.d(r3, r4)     // Catch:{ Exception -> 0x007c }
        L_0x003e:
            r10.put(r8, r2)     // Catch:{ Exception -> 0x007c }
            r0 = 1
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0045:
            if (r0 == 0) goto L_0x004a
            r0.close()
        L_0x004a:
            return r1
        L_0x004b:
            java.lang.String r2 = "DomobSDK"
            r3 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r3)     // Catch:{ Exception -> 0x007c }
            if (r2 == 0) goto L_0x0081
            java.lang.String r2 = "DomobSDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007c }
            java.lang.String r4 = "fail to load image:"
            r3.<init>(r4)     // Catch:{ Exception -> 0x007c }
            java.lang.StringBuilder r3 = r3.append(r9)     // Catch:{ Exception -> 0x007c }
            java.lang.String r4 = " from cache"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x007c }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x007c }
            android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x007c }
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0045
        L_0x0072:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
        L_0x0076:
            r1.printStackTrace()
            r1 = r0
            r0 = r2
            goto L_0x0045
        L_0x007c:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x0076
        L_0x0081:
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.b.a(cn.domob.android.ads.DBHelper, java.lang.String, java.lang.String, java.util.Hashtable):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void a(cn.domob.android.ads.DBHelper r6, java.lang.String r7, byte[] r8) {
        /*
            java.lang.String r0 = "DomobSDK"
            r0 = 0
            android.net.Uri r1 = cn.domob.android.ads.DBHelper.b     // Catch:{ Exception -> 0x008b }
            android.database.Cursor r0 = r6.b(r1, r7)     // Catch:{ Exception -> 0x008b }
            if (r0 == 0) goto L_0x0011
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x0082 }
            if (r1 != 0) goto L_0x0060
        L_0x0011:
            android.net.Uri r1 = cn.domob.android.ads.DBHelper.b     // Catch:{ Exception -> 0x0082 }
            r6.a(r1)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r1 = "DomobSDK"
            r2 = 3
            boolean r1 = android.util.Log.isLoggable(r1, r2)     // Catch:{ Exception -> 0x0082 }
            if (r1 == 0) goto L_0x0039
            java.lang.String r1 = "DomobSDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = "save image:"
            r2.<init>(r3)     // Catch:{ Exception -> 0x0082 }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = " to cache!"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0082 }
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x0082 }
        L_0x0039:
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ Exception -> 0x0082 }
            r1.<init>()     // Catch:{ Exception -> 0x0082 }
            java.lang.String r2 = "_name"
            r1.put(r2, r7)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r2 = "_image"
            r1.put(r2, r8)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r2 = "_date"
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0082 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x0082 }
            r1.put(r2, r3)     // Catch:{ Exception -> 0x0082 }
            android.net.Uri r2 = cn.domob.android.ads.DBHelper.b     // Catch:{ Exception -> 0x0082 }
            r6.a(r2, r1)     // Catch:{ Exception -> 0x0082 }
        L_0x005a:
            if (r0 == 0) goto L_0x005f
            r0.close()
        L_0x005f:
            return
        L_0x0060:
            java.lang.String r1 = "DomobSDK"
            r2 = 3
            boolean r1 = android.util.Log.isLoggable(r1, r2)     // Catch:{ Exception -> 0x0082 }
            if (r1 == 0) goto L_0x005a
            java.lang.String r1 = "DomobSDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = java.lang.String.valueOf(r7)     // Catch:{ Exception -> 0x0082 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = " is already in cache."
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0082 }
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x0082 }
            goto L_0x005a
        L_0x0082:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0086:
            r0.printStackTrace()
            r0 = r1
            goto L_0x005a
        L_0x008b:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0086
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.b.a(cn.domob.android.ads.DBHelper, java.lang.String, byte[]):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x003a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static boolean b(cn.domob.android.ads.DBHelper r11, java.lang.String r12, java.lang.String r13, java.util.Hashtable<java.lang.String, byte[]> r14) {
        /*
            r9 = 1
            r8 = 3
            java.lang.String r7 = "DomobSDK"
            r0 = 0
            r1 = 0
            android.net.Uri r2 = cn.domob.android.ads.DBHelper.a     // Catch:{ Exception -> 0x0082 }
            android.database.Cursor r1 = r11.b(r2, r13)     // Catch:{ Exception -> 0x0082 }
            if (r1 == 0) goto L_0x003e
            int r2 = r1.getCount()     // Catch:{ Exception -> 0x0082 }
            if (r2 <= 0) goto L_0x003e
            r1.moveToFirst()     // Catch:{ Exception -> 0x0082 }
            java.lang.String r2 = "_image"
            int r2 = r1.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0082 }
            byte[] r2 = r1.getBlob(r2)     // Catch:{ Exception -> 0x0082 }
            if (r2 == 0) goto L_0x009b
            java.lang.String r3 = "DomobSDK"
            r4 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r4)     // Catch:{ Exception -> 0x0082 }
            if (r3 == 0) goto L_0x0033
            java.lang.String r3 = "DomobSDK"
            java.lang.String r4 = "success to load from resources DB."
            android.util.Log.d(r3, r4)     // Catch:{ Exception -> 0x0082 }
        L_0x0033:
            r14.put(r12, r2)     // Catch:{ Exception -> 0x0082 }
            r0 = r1
            r1 = r9
        L_0x0038:
            if (r0 == 0) goto L_0x003d
            r0.close()
        L_0x003d:
            return r1
        L_0x003e:
            android.content.Context r2 = r11.a()     // Catch:{ Exception -> 0x0082 }
            if (r2 == 0) goto L_0x009b
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0082 }
            r3.<init>()     // Catch:{ Exception -> 0x0082 }
            android.content.res.AssetManager r2 = r2.getAssets()     // Catch:{ Exception -> 0x0082 }
            java.io.InputStream r2 = r2.open(r13)     // Catch:{ Exception -> 0x0082 }
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ Exception -> 0x0082 }
            android.graphics.Bitmap$CompressFormat r4 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ Exception -> 0x0082 }
            r5 = 100
            r2.compress(r4, r5, r3)     // Catch:{ Exception -> 0x0082 }
            byte[] r4 = r3.toByteArray()     // Catch:{ Exception -> 0x0082 }
            if (r4 == 0) goto L_0x0076
            java.lang.String r5 = "DomobSDK"
            r6 = 3
            boolean r5 = android.util.Log.isLoggable(r5, r6)     // Catch:{ Exception -> 0x0082 }
            if (r5 == 0) goto L_0x0072
            java.lang.String r5 = "DomobSDK"
            java.lang.String r6 = "success to load from preload resources."
            android.util.Log.d(r5, r6)     // Catch:{ Exception -> 0x0082 }
        L_0x0072:
            r14.put(r12, r4)     // Catch:{ Exception -> 0x0082 }
            r0 = r9
        L_0x0076:
            r3.close()     // Catch:{ Exception -> 0x0082 }
            if (r2 == 0) goto L_0x009b
            r2.recycle()     // Catch:{ Exception -> 0x0082 }
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x0038
        L_0x0082:
            r2 = move-exception
            r10 = r2
            r2 = r0
            r0 = r10
            java.lang.String r3 = "DomobSDK"
            java.lang.String r3 = "cannot load it from res this time."
            android.util.Log.i(r7, r3)
            java.lang.String r3 = "DomobSDK"
            boolean r3 = android.util.Log.isLoggable(r7, r8)
            if (r3 == 0) goto L_0x0098
            r0.printStackTrace()
        L_0x0098:
            r0 = r1
            r1 = r2
            goto L_0x0038
        L_0x009b:
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.b.b(cn.domob.android.ads.DBHelper, java.lang.String, java.lang.String, java.util.Hashtable):boolean");
    }
}
