package cn.domob.android.ads;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.SystemClock;
import android.util.Log;
import com.google.ads.AdActivity;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import java.util.Iterator;
import org.json.JSONObject;

class f {
    private Context a;
    private String b;
    private String c;
    private int d;
    private boolean e;
    private long f;
    private long g;
    private String h;
    private boolean i;

    f() {
    }

    private f(Context context) {
        this.e = false;
        this.i = true;
        this.a = context;
        this.g = 0;
    }

    protected static f a(Context context, JSONObject jSONObject) {
        f fVar = new f(context);
        if (fVar.a(jSONObject)) {
            return fVar;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public final long c() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public final long d() {
        return this.g;
    }

    private boolean a(JSONObject jSONObject) {
        int lastIndexOf;
        boolean z;
        if (!jSONObject.optBoolean("detection")) {
            Log.e(Constants.LOG, "invalide detector response");
            return false;
        }
        this.c = jSONObject.optString("lm[config]", null);
        if (this.c != null && Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "lm[config]:" + this.c);
        }
        this.b = jSONObject.optString("lm[res]", null);
        if (this.b != null && Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "lm[res]:" + this.b);
        }
        this.d = jSONObject.optInt("refresh_interval", -1);
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "interval:" + this.d);
        }
        this.e = jSONObject.optBoolean("disabled", false);
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "disabled:" + this.e);
        }
        this.f = jSONObject.optLong("dis_time", 0);
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "disabled time:" + this.f);
        }
        this.i = jSONObject.optBoolean("allow_tm", true);
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "allow test:" + this.i);
        }
        this.h = jSONObject.optString("cid", null);
        if (this.h != null && Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "CID:" + this.h);
        }
        JSONObject optJSONObject = jSONObject.optJSONObject("markup");
        if (optJSONObject != null) {
            try {
                JSONObject optJSONObject2 = optJSONObject.optJSONObject("$");
                if (optJSONObject2 != null) {
                    DBHelper a2 = DBHelper.a(this.a);
                    Iterator<String> keys = optJSONObject2.keys();
                    while (keys.hasNext()) {
                        String next = keys.next();
                        String string = optJSONObject2.getJSONObject(next).getString(AdActivity.URL_PARAM);
                        if (next != null && string != null && (lastIndexOf = string.lastIndexOf(47)) > 0 && lastIndexOf + 1 < string.length()) {
                            String substring = string.substring(lastIndexOf + 1);
                            Cursor b2 = a2.b(DBHelper.a, substring);
                            if (b2 == null || b2.getCount() <= 0) {
                                z = true;
                            } else {
                                b2.moveToFirst();
                                a2.a(DBHelper.a, "_name=\"" + b2.getString(b2.getColumnIndexOrThrow("_name")) + XmlConstant.QUOTE);
                                z = false;
                            }
                            if (b2 != null) {
                                b2.close();
                            }
                            if (!z && Log.isLoggable(Constants.LOG, 3)) {
                                Log.d(Constants.LOG, String.valueOf(substring) + ":old version existed, delete it.");
                            }
                        }
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                return false;
            }
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "update config");
        }
        DBHelper a3 = DBHelper.a(this.a);
        ContentValues contentValues = new ContentValues();
        if (this.c != null) {
            contentValues.put("_conf_ver", this.c);
        }
        if (this.b != null) {
            contentValues.put("_res_ver", this.b);
        }
        if (this.d != -1) {
            contentValues.put("_interval", Integer.valueOf(this.d));
        }
        contentValues.put("_dis_flag", Boolean.valueOf(this.e));
        contentValues.put("_dis_time", Long.valueOf(this.f));
        if (this.e) {
            this.g = SystemClock.uptimeMillis();
            contentValues.put("_dis_timestamp", Long.valueOf(this.g));
        }
        contentValues.put("_test_flag", Boolean.valueOf(this.i));
        if (this.h != null) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "update cid:" + this.h);
            }
            contentValues.put("_cid", this.h);
        }
        a3.a(contentValues);
        return true;
    }
}
