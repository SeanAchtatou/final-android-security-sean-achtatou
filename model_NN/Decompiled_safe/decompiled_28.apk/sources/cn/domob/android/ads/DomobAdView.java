package cn.domob.android.ads;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.RelativeLayout;
import cn.domob.android.ads.DomobAdEngine;
import com.energysource.szj.embeded.AdManager;
import com.millennialmedia.android.MMAdView;
import java.util.ArrayList;

public class DomobAdView extends RelativeLayout {
    public static final int ANIMATION_ALPHA = 3;
    public static final int ANIMATION_FRAGMENT = 9;
    public static final int ANIMATION_ROTATE = 1;
    public static final int ANIMATION_SCALE_FROM_MIDDLE = 5;
    public static final int ANIMATION_TRANSLATE = 7;
    private static int D = 0;
    static final Handler b = new Handler();
    private static Boolean c = null;
    private c A;
    private ArrayList<Integer> B;
    private boolean C;
    private int E;
    protected boolean a;
    /* access modifiers changed from: private */
    public DomobAdBuilder d;
    private String e;
    private DomobAdEngine.RecvHandler f;
    private int g;
    private int h;
    private long i;
    private long j;
    private long k;
    private String l;
    private DomobAdListener m;
    private boolean n;
    private boolean o;
    private boolean p;
    /* access modifiers changed from: private */
    public boolean q;
    private long r;
    private long s;
    /* access modifiers changed from: private */
    public int t;
    /* access modifiers changed from: private */
    public boolean u;
    /* access modifiers changed from: private */
    public Context v;
    private boolean w;
    private boolean x;
    private c y;
    private boolean z;

    public DomobAdView(Activity activity) {
        this(activity, null, 0);
    }

    public DomobAdView(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0);
    }

    public DomobAdView(Context context, AttributeSet attributeset, int defStyle) {
        super(context, attributeset, defStyle);
        this.j = -16777216;
        this.k = -1;
        this.a = true;
        this.w = false;
        this.x = false;
        this.y = new c();
        this.z = false;
        this.B = new ArrayList<>();
        this.C = false;
        Log.i(Constants.LOG, "current version is 20110701");
        if (-1 != context.checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") || (l.b(Build.VERSION.SDK) && Integer.parseInt(Build.VERSION.SDK) == 3)) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "DomobAdView!");
            }
            int i2 = D;
            D = i2 + 1;
            this.E = i2;
            setDescendantFocusability(262144);
            setClickable(true);
            setLongClickable(false);
            setGravity(17);
            this.g = 20000;
            if (attributeset != null) {
                String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
                int attributeUnsignedIntValue = attributeset.getAttributeUnsignedIntValue(str, "backgroundColor", -1);
                if (attributeUnsignedIntValue != -1) {
                    setBackgroundColor(attributeUnsignedIntValue);
                }
                int attributeUnsignedIntValue2 = attributeset.getAttributeUnsignedIntValue(str, "primaryTextColor", -1);
                if (attributeUnsignedIntValue2 != -1) {
                    setPrimaryTextColor(attributeUnsignedIntValue2);
                }
                this.e = attributeset.getAttributeValue(str, MMAdView.KEY_KEYWORDS);
                this.l = attributeset.getAttributeValue(str, "spots");
                this.g = a(attributeset.getAttributeIntValue(str, "refreshInterval", 20)) * AdManager.AD_FILL_PARENT;
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "setDefaultInterval:" + this.g);
                }
            }
            this.v = context;
            this.d = null;
            this.f = null;
            if (c == null) {
                c = Boolean.valueOf(b(context));
            }
            if (!a(context)) {
                this.q = false;
                this.r = 0;
                this.s = 0;
                setRequestInterval(1);
            }
            this.n = false;
            this.p = true;
            this.i = 0;
            this.u = true;
            this.t = 0;
            if (this.q) {
                long uptimeMillis = SystemClock.uptimeMillis();
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "mDisabled = " + this.q + " | mDisabledTime = " + this.r + " | mDisabledTimestamp = " + this.s);
                    Log.d(Constants.LOG, "curr timestamp = " + uptimeMillis);
                }
                if (uptimeMillis - this.s >= this.r * 1000) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "send detector because ad is disabled.");
                    }
                    j();
                }
            }
            PackageManager packageManager = this.v.getPackageManager();
            Intent intent = new Intent();
            intent.setClass(this.v, DomobActivity.class);
            if (packageManager.resolveActivity(intent, -1) == null) {
                Log.e(Constants.LOG, "please declare DomobActivity in AndroidManifest !");
                throw new RuntimeException("can't find declare of DomobActivity in AndroidManifest !");
            }
            return;
        }
        Log.e(Constants.LOG, "you must have android.permission.WRITE_EXTERNAL_STORAGE permission !");
        throw new RuntimeException("you must have android.permission.WRITE_EXTERNAL_STORAGE permission !");
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(android.content.Context r7) {
        /*
            r6 = this;
            r3 = 1
            r4 = 0
            java.lang.String r0 = "DomobSDK"
            r0 = 0
            cn.domob.android.ads.DBHelper r1 = cn.domob.android.ads.DBHelper.a(r7)     // Catch:{ Exception -> 0x0076 }
            android.database.Cursor r0 = r1.b()     // Catch:{ Exception -> 0x0076 }
            if (r0 == 0) goto L_0x0015
            int r2 = r0.getCount()     // Catch:{ Exception -> 0x006c }
            if (r2 != 0) goto L_0x002f
        L_0x0015:
            java.lang.String r2 = "DomobSDK"
            r3 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r3)     // Catch:{ Exception -> 0x006c }
            if (r2 == 0) goto L_0x0025
            java.lang.String r2 = "DomobSDK"
            java.lang.String r3 = "no data in conf db, initialize now."
            android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x006c }
        L_0x0025:
            r1.c()     // Catch:{ Exception -> 0x006c }
            r1 = r4
        L_0x0029:
            if (r0 == 0) goto L_0x002e
            r0.close()
        L_0x002e:
            return r1
        L_0x002f:
            r0.moveToFirst()     // Catch:{ Exception -> 0x006c }
            java.lang.String r1 = "_dis_flag"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x006c }
            int r1 = r0.getInt(r1)     // Catch:{ Exception -> 0x006c }
            if (r1 != 0) goto L_0x0068
            r1 = 0
            r6.q = r1     // Catch:{ Exception -> 0x006c }
        L_0x0041:
            java.lang.String r1 = "_dis_time"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x006c }
            long r1 = r0.getLong(r1)     // Catch:{ Exception -> 0x006c }
            r6.r = r1     // Catch:{ Exception -> 0x006c }
            java.lang.String r1 = "_dis_timestamp"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x006c }
            long r1 = r0.getLong(r1)     // Catch:{ Exception -> 0x006c }
            r6.s = r1     // Catch:{ Exception -> 0x006c }
            java.lang.String r1 = "_interval"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x006c }
            int r1 = r0.getInt(r1)     // Catch:{ Exception -> 0x006c }
            r6.setRequestInterval(r1)     // Catch:{ Exception -> 0x006c }
            r1 = r3
            goto L_0x0029
        L_0x0068:
            r1 = 1
            r6.q = r1     // Catch:{ Exception -> 0x006c }
            goto L_0x0041
        L_0x006c:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0070:
            r0.printStackTrace()
            r0 = r1
            r1 = r4
            goto L_0x0029
        L_0x0076:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0070
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.DomobAdView.a(android.content.Context):boolean");
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int i2;
        DomobAdEngine b2;
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "DomobAdView onMeasure");
        }
        int size = View.MeasureSpec.getSize(widthMeasureSpec);
        int mode = View.MeasureSpec.getMode(widthMeasureSpec);
        int size2 = View.MeasureSpec.getSize(heightMeasureSpec);
        int mode2 = View.MeasureSpec.getMode(heightMeasureSpec);
        if (!(mode == Integer.MIN_VALUE || mode == 1073741824)) {
            size = DomobAdManager.e(getContext());
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "DomobAdView width is :" + size);
        }
        if (mode2 == 1073741824) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "heightMeasureSpec is android.view.View.MeasureSpec.EXACTLY!");
            }
            i2 = size2;
        } else if (this.d == null || (b2 = this.d.b()) == null) {
            i2 = 0;
        } else {
            i2 = b2.a(b2.c());
            if (mode2 == Integer.MIN_VALUE && size2 < i2) {
                Log.e(Constants.LOG, "Cannot display ad because its container is too small.  The ad is " + i2 + " pixels tall but is only given " + size2 + " at most to draw into.  Please make your view containing DomobAdView taller.");
                i2 = 0;
            }
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "DomobAdView height is :" + i2);
        }
        setMeasuredDimension(size, i2);
        if (this.a && !c.booleanValue()) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "request the first ad!");
            }
            i();
        }
    }

    protected static void a(DomobAdView domobAdView, boolean z2) {
        domobAdView.p = z2;
    }

    public void requestFreshAd() {
        this.a = true;
        this.C = true;
        i();
    }

    private boolean i() {
        long uptimeMillis = (SystemClock.uptimeMillis() - this.i) / 1000;
        if (uptimeMillis < 20) {
            Log.w(Constants.LOG, "Ignoring requestFreshAd.  Called " + uptimeMillis + " seconds since last refresh.  " + "Refreshes must be at least " + 20 + " apart.");
            return false;
        } else if (this.n) {
            Log.w(Constants.LOG, "Ignoring doRefresh() because we are requesting an ad right now already.");
            return false;
        } else {
            k();
            return true;
        }
    }

    protected static void a(DomobAdView domobAdView) {
        if (domobAdView == null) {
            return;
        }
        if (!domobAdView.C || domobAdView.p || domobAdView.a) {
            domobAdView.k();
        }
    }

    private void j() {
        new h(this).start();
    }

    private void k() {
        if (this.q && (!DomobAdManager.isTestMode(this.v) || !DomobAdManager.isTestAllowed(this.v))) {
            Log.w(Constants.LOG, "AD has been disabled on web server, ignore doRefresh()");
        } else if (!this.a && super.getVisibility() != 0) {
            Log.w(Constants.LOG, "Cannot doRefresh() when the DomobAdView is not visible. Call DomobAdView.setVisibility(View.VISIBLE) first.");
        } else if (this.n) {
            Log.w(Constants.LOG, "Ignoring doRefresh() because we are requesting an ad right now already.");
        } else {
            this.n = true;
            this.i = SystemClock.uptimeMillis();
            if (this.p) {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "send detector!");
                }
                j();
                return;
            }
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "doRefresh now!");
            }
            Log.v(Constants.LOG, "GetAd by view:" + String.valueOf(this.E));
            new g(this).start();
        }
    }

    public int getRequestInterval() {
        return this.h / AdManager.AD_FILL_PARENT;
    }

    public void setRequestInterval(int requestInterval) {
        if (requestInterval == 1) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "use the default interval(from xml).");
            }
            this.h = this.g;
        } else if (requestInterval == 0) {
            Log.w(Constants.LOG, "refreshInterval is 0, AD will not be refreshed any more.");
            this.h = requestInterval * AdManager.AD_FILL_PARENT;
            l();
        } else {
            this.h = a(requestInterval) * AdManager.AD_FILL_PARENT;
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "setRequestInterval:" + this.h);
        }
    }

    private static int a(int i2) {
        if (i2 < 20) {
            return 20;
        }
        if (i2 > 600) {
            return 600;
        }
        return i2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00ee, code lost:
        if (r4.h == 0) goto L_0x00f0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(boolean r5) {
        /*
            r4 = this;
            java.lang.String r0 = "DomobSDK"
            monitor-enter(r4)
            java.lang.String r0 = "DomobSDK"
            r1 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ all -> 0x00e7 }
            if (r0 == 0) goto L_0x007a
            java.lang.String r0 = "DomobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e7 }
            java.lang.String r2 = "schedule for a fresh ad?"
            r1.<init>(r2)     // Catch:{ all -> 0x00e7 }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ all -> 0x00e7 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00e7 }
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x00e7 }
            java.lang.String r0 = "DomobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e7 }
            java.lang.String r2 = "check WindowsVisibility:"
            r1.<init>(r2)     // Catch:{ all -> 0x00e7 }
            int r2 = r4.t     // Catch:{ all -> 0x00e7 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00e7 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00e7 }
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x00e7 }
            java.lang.String r0 = "DomobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e7 }
            java.lang.String r2 = "check WindowsFocus:"
            r1.<init>(r2)     // Catch:{ all -> 0x00e7 }
            boolean r2 = r4.u     // Catch:{ all -> 0x00e7 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00e7 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00e7 }
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x00e7 }
            java.lang.String r0 = "DomobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e7 }
            java.lang.String r2 = "getVisibility:"
            r1.<init>(r2)     // Catch:{ all -> 0x00e7 }
            int r2 = r4.getVisibility()     // Catch:{ all -> 0x00e7 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00e7 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00e7 }
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x00e7 }
            java.lang.String r0 = "DomobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e7 }
            java.lang.String r2 = "mInterval:"
            r1.<init>(r2)     // Catch:{ all -> 0x00e7 }
            int r2 = r4.h     // Catch:{ all -> 0x00e7 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00e7 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00e7 }
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x00e7 }
        L_0x007a:
            r0 = 0
            int r1 = r4.h     // Catch:{ all -> 0x00e7 }
            if (r1 > 0) goto L_0x0087
            int r1 = r4.h     // Catch:{ all -> 0x00e7 }
            if (r1 != 0) goto L_0x0088
            boolean r1 = r4.a     // Catch:{ all -> 0x00e7 }
            if (r1 == 0) goto L_0x0088
        L_0x0087:
            r0 = 1
        L_0x0088:
            if (r5 == 0) goto L_0x00ea
            if (r0 == 0) goto L_0x00ea
            int r0 = r4.getVisibility()     // Catch:{ all -> 0x00e7 }
            if (r0 != 0) goto L_0x00ea
            int r0 = r4.t     // Catch:{ all -> 0x00e7 }
            if (r0 != 0) goto L_0x00ea
            boolean r0 = r4.u     // Catch:{ all -> 0x00e7 }
            if (r0 == 0) goto L_0x00ea
            r4.l()     // Catch:{ all -> 0x00e7 }
            cn.domob.android.ads.DomobAdView$c r0 = new cn.domob.android.ads.DomobAdView$c     // Catch:{ all -> 0x00e7 }
            r0.<init>(r4)     // Catch:{ all -> 0x00e7 }
            r4.A = r0     // Catch:{ all -> 0x00e7 }
            int r0 = r4.h     // Catch:{ all -> 0x00e7 }
            if (r0 != 0) goto L_0x00dc
            android.os.Handler r0 = cn.domob.android.ads.DomobAdView.b     // Catch:{ all -> 0x00e7 }
            cn.domob.android.ads.DomobAdView$c r1 = r4.A     // Catch:{ all -> 0x00e7 }
            r2 = 20000(0x4e20, double:9.8813E-320)
            r0.postDelayed(r1, r2)     // Catch:{ all -> 0x00e7 }
        L_0x00b1:
            java.lang.String r0 = "DomobSDK"
            r1 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ all -> 0x00e7 }
            if (r0 == 0) goto L_0x00da
            java.lang.String r0 = "DomobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e7 }
            r1.<init>()     // Catch:{ all -> 0x00e7 }
            java.lang.String r2 = "Ad refresh scheduled for "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00e7 }
            int r2 = r4.h     // Catch:{ all -> 0x00e7 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00e7 }
            java.lang.String r2 = " from now."
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00e7 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00e7 }
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x00e7 }
        L_0x00da:
            monitor-exit(r4)     // Catch:{ all -> 0x00e7 }
            return
        L_0x00dc:
            android.os.Handler r0 = cn.domob.android.ads.DomobAdView.b     // Catch:{ all -> 0x00e7 }
            cn.domob.android.ads.DomobAdView$c r1 = r4.A     // Catch:{ all -> 0x00e7 }
            int r2 = r4.h     // Catch:{ all -> 0x00e7 }
            long r2 = (long) r2     // Catch:{ all -> 0x00e7 }
            r0.postDelayed(r1, r2)     // Catch:{ all -> 0x00e7 }
            goto L_0x00b1
        L_0x00e7:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x00ea:
            if (r5 == 0) goto L_0x00f0
            int r0 = r4.h     // Catch:{ all -> 0x00e7 }
            if (r0 != 0) goto L_0x00da
        L_0x00f0:
            java.lang.String r0 = "DomobSDK"
            r1 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ all -> 0x00e7 }
            if (r0 == 0) goto L_0x0100
            java.lang.String r0 = "DomobSDK"
            java.lang.String r1 = "just cancel the previous request!"
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x00e7 }
        L_0x0100:
            r4.l()     // Catch:{ all -> 0x00e7 }
            goto L_0x00da
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.DomobAdView.a(boolean):void");
    }

    private void l() {
        if (b != null) {
            if (this.A != null) {
                this.A.a = true;
                b.removeCallbacks(this.A);
                this.A = null;
            }
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "Cancelled an ad refresh scheduled.");
            }
        }
    }

    class c implements Runnable {
        boolean a;

        /* synthetic */ c(DomobAdView domobAdView) {
            this((byte) 0);
        }

        private c(byte b2) {
        }

        public final void run() {
            try {
                DomobAdView domobAdView = DomobAdView.this;
                if (!this.a && domobAdView != null) {
                    int b2 = DomobAdView.b(domobAdView) / AdManager.AD_FILL_PARENT;
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "Requesting a fresh ad because a request interval passed (" + b2 + " seconds).");
                    }
                    DomobAdView.a(domobAdView);
                }
            } catch (Exception e) {
                Log.e(Constants.LOG, "exception caught in RefreshThread.run()!");
                e.printStackTrace();
            }
        }
    }

    public void onWindowFocusChanged(boolean flag) {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "onWindowFocusChanged:" + flag);
        }
        this.u = flag;
        a(flag);
        super.onWindowFocusChanged(flag);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int v2) {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "onWindowVisibilityChanged:" + v2);
        }
        this.t = v2;
        a(v2 == 0);
        super.onWindowVisibilityChanged(v2);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "onAttachedToWindow");
        }
        this.o = true;
        a(true);
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "onDetachedFromWindow");
        }
        this.o = false;
        this.a = true;
        if (this.v != null) {
            long c2 = j.c();
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "The avg req time is:" + c2);
            }
            if (c2 != -1) {
                DBHelper a2 = DBHelper.a(this.v);
                ContentValues contentValues = new ContentValues();
                contentValues.put("_avg_time", Long.valueOf(c2));
                a2.a(contentValues);
            }
        }
        a(false);
        if (this.d != null) {
            this.d.c();
        }
        j.b();
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public final long a() {
        return this.k;
    }

    public void setPrimaryTextColor(int color) {
        this.k = (long) (-16777216 | color);
        this.x = true;
    }

    /* access modifiers changed from: protected */
    public final long b() {
        return this.j;
    }

    public void setBackgroundColor(int color) {
        this.j = (long) color;
        invalidate();
        this.w = true;
    }

    public String getKeywords() {
        return this.e;
    }

    public void setKeywords(String s2) {
        if (s2 == null || s2.length() <= 200) {
            this.e = s2;
        } else {
            Log.e(Constants.LOG, "The length of keywords cannot exceed 200!");
        }
    }

    public String getSpots() {
        return this.l;
    }

    public void setSpots(String s2) {
        if (s2 == null || s2.length() <= 200) {
            this.l = s2;
        } else {
            Log.e(Constants.LOG, "The length of spots cannot exceed 200!");
        }
    }

    public void setVisibility(int v2) {
        if (super.getVisibility() != v2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    getChildAt(i2).setVisibility(v2);
                }
                super.setVisibility(v2);
                invalidate();
            }
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "setVisibility:" + v2);
        }
        a(v2 == 0);
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z2, long j2, long j3) {
        if (this.q != z2) {
            this.q = z2;
            if (this.q) {
                Log.w(Constants.LOG, "AD is disabled on web server.");
                this.r = j2;
                this.s = j3;
            } else {
                this.r = 0;
                this.s = 0;
            }
            if (b != null) {
                b.post(new e(this));
            }
        }
    }

    public void setAdListener(DomobAdListener adlistener) {
        synchronized (this) {
            this.m = adlistener;
        }
    }

    public DomobAdListener getAdListener() {
        return this.m;
    }

    public boolean hasAd() {
        return (this.d == null || this.d.b() == null) ? false : true;
    }

    public void cleanup() {
        if (this.d != null) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "clear the ad.");
            }
            this.d.c();
            this.d = null;
        }
    }

    private static boolean b(Context context) {
        try {
            if (Class.forName("org.json.JSONException") != null) {
                return false;
            }
        } catch (ClassNotFoundException e2) {
            e2.printStackTrace();
        }
        return DomobAdManager.b(context) == null;
    }

    protected static DomobAdBuilder a(DomobAdView domobAdView, DomobAdBuilder domobAdBuilder) {
        domobAdView.d = domobAdBuilder;
        return domobAdBuilder;
    }

    protected static int b(DomobAdView domobAdView) {
        return domobAdView.h;
    }

    protected static DomobAdListener c(DomobAdView domobAdView) {
        return domobAdView.m;
    }

    protected static String d(DomobAdView domobAdView) {
        if (domobAdView.e == null || domobAdView.e.length() <= 200) {
            return domobAdView.e;
        }
        Log.e(Constants.LOG, "The length of keywords cannot exceed 200!");
        return null;
    }

    protected static String e(DomobAdView domobAdView) {
        if (domobAdView.l == null || domobAdView.l.length() <= 200) {
            return domobAdView.l;
        }
        Log.e(Constants.LOG, "The length of spots cannot exceed 200!");
        return null;
    }

    protected static void b(DomobAdView domobAdView, boolean z2) {
        if (domobAdView != null) {
            domobAdView.n = false;
        }
    }

    protected static void c(DomobAdView domobAdView, boolean z2) {
        if (domobAdView != null) {
            domobAdView.a(true);
        }
    }

    protected static DomobAdEngine.RecvHandler f(DomobAdView domobAdView) {
        if (domobAdView == null) {
            return null;
        }
        if (domobAdView.f == null) {
            domobAdView.f = new DomobAdEngine.RecvHandler(domobAdView);
        }
        return domobAdView.f;
    }

    protected static void g(DomobAdView domobAdView) {
        if (domobAdView != null && domobAdView.m != null && b != null) {
            Handler handler = b;
            domobAdView.getClass();
            handler.post(new b(domobAdView));
        }
    }

    class b implements Runnable {
        private DomobAdView a;

        public b(DomobAdView domobAdView) {
            this.a = domobAdView;
        }

        public final void run() {
            if (this.a != null) {
                try {
                    DomobAdView.c(this.a).onFailedToReceiveFreshAd(this.a);
                } catch (Exception e) {
                    Log.e(Constants.LOG, "Unhandled exception raised in onFailedToReceiveRefreshedAd.");
                    e.printStackTrace();
                }
            }
        }
    }

    protected static void h(DomobAdView domobAdView) {
        if (domobAdView != null && domobAdView.m != null) {
            try {
                domobAdView.m.onReceivedFreshAd(domobAdView);
            } catch (Exception e2) {
                Log.e(Constants.LOG, "Unhandled exception raised in onReceivedFreshAd.");
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(DomobAdEngine domobAdEngine, DomobAdBuilder domobAdBuilder) {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "construct ad view");
        }
        if (domobAdEngine == null || domobAdBuilder == null) {
            Log.w(Constants.LOG, "failed to construct view!");
            return;
        }
        boolean z2 = this.a;
        this.a = false;
        domobAdBuilder.a(domobAdEngine);
        int visibility = getVisibility();
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "this.getVisibility():" + visibility);
        }
        domobAdBuilder.setVisibility(visibility);
        domobAdBuilder.setGravity(17);
        domobAdEngine.a(domobAdBuilder);
        domobAdBuilder.setLayoutParams(new RelativeLayout.LayoutParams(domobAdEngine.a(domobAdEngine.b()), domobAdEngine.a(domobAdEngine.c())));
        b.post(new d(domobAdBuilder, visibility, z2));
    }

    class e implements Runnable {
        /* synthetic */ e(DomobAdView domobAdView) {
            this((byte) 0);
        }

        private e(byte b) {
        }

        public final void run() {
            if (DomobAdView.this == null) {
                return;
            }
            if (!DomobAdView.this.q) {
                DomobAdView.this.setVisibility(0);
            } else if (!DomobAdManager.isTestMode(DomobAdView.this.v) || !DomobAdManager.isTestAllowed(DomobAdView.this.v)) {
                DomobAdView.this.setVisibility(8);
            }
        }
    }

    class d implements Runnable {
        private DomobAdView a;
        private DomobAdBuilder b;
        private int c;
        private boolean d;

        public d(DomobAdBuilder domobAdBuilder, int i, boolean z) {
            this.a = DomobAdView.this;
            this.b = domobAdBuilder;
            this.c = i;
            this.d = z;
        }

        public final void run() {
            try {
                if (!(this.a == null || this.b == null)) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "show ad!");
                        Log.d(Constants.LOG, "WindowVisibility:" + DomobAdView.this.t);
                        Log.d(Constants.LOG, "WindowFocus:" + DomobAdView.this.u);
                    }
                    if (DomobAdView.this.t == 0 && DomobAdView.this.u) {
                        this.b.setVisibility(4);
                        this.a.addView(this.b);
                        DomobAdView domobAdView = this.a;
                        this.b.b();
                        DomobAdView.h(domobAdView);
                        if (this.d) {
                            DomobAdView.b(this.a, this.b);
                            return;
                        } else if (this.c == 0) {
                            String a2 = this.b.a();
                            if (a2 == null || a2.equals("fr2l")) {
                                DomobAdView.a(this.a, this.b, DomobReport.a(this.a));
                                return;
                            }
                            return;
                        } else {
                            return;
                        }
                    }
                }
            } catch (Exception e2) {
                Log.e(Constants.LOG, "Unhandled exception in showAdThread.run() " + e2.getMessage());
                e2.printStackTrace();
            }
            if (this.b != null) {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "error or view is invisible, clear resources!");
                }
                this.a.removeView(this.b);
                this.b.c();
                this.b = null;
                if (this.d) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "reset mNoAd");
                    }
                    DomobAdView.this.a = true;
                }
            }
        }
    }

    protected static void b(DomobAdView domobAdView, DomobAdBuilder domobAdBuilder) {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "startAlphaAnimation");
        }
        DomobAdBuilder domobAdBuilder2 = domobAdView.d;
        domobAdBuilder.setVisibility(0);
        domobAdView.d = domobAdBuilder;
        if (domobAdBuilder2 != null) {
            domobAdView.removeView(domobAdBuilder2);
            domobAdBuilder2.c();
        }
        if (domobAdView.o) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(233);
            alphaAnimation.startNow();
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new AccelerateInterpolator());
            domobAdView.startAnimation(alphaAnimation);
        }
    }

    protected static void a(DomobAdView domobAdView, DomobAdBuilder domobAdBuilder, int i2) {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "start ad switch Animation");
        }
        if (i2 == 9) {
            domobAdBuilder.setVisibility(4);
            new n().a(domobAdView, domobAdBuilder, domobAdView.d);
            domobAdView.d.setVisibility(8);
            return;
        }
        Animation a2 = DomobReport.a(i2, domobAdView);
        domobAdView.getClass();
        a2.setAnimationListener(new a(domobAdView, domobAdView, domobAdBuilder, i2));
        if (i2 == 5 || i2 == 7) {
            domobAdBuilder.setVisibility(0);
            domobAdBuilder.startAnimation(a2);
            return;
        }
        domobAdView.startAnimation(a2);
    }

    class a implements Animation.AnimationListener {
        private DomobAdBuilder a;
        private DomobAdView b;
        private int c;

        a(DomobAdView domobAdView, DomobAdView domobAdView2, DomobAdBuilder domobAdBuilder, int i) {
            this.b = domobAdView2;
            this.a = domobAdBuilder;
            this.c = i;
        }

        public final void onAnimationStart(Animation animation) {
        }

        public final void onAnimationEnd(Animation animation) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "DomobAdView onAnimationEnd.");
            }
            if (this.a != null) {
                DomobAdBuilder m = this.b.d;
                this.a.setVisibility(0);
                DomobAdView.a(this.b, this.a);
                if (m != null) {
                    this.b.removeView(m);
                    m.c();
                }
                if (this.c != 5 && this.c != 7) {
                    this.b.startAnimation(DomobReport.a(this.c + 1, this.b));
                }
            }
        }

        public final void onAnimationRepeat(Animation animation) {
        }
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        return this.w;
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return this.x;
    }

    /* access modifiers changed from: protected */
    public final boolean e() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final c f() {
        return this.y;
    }

    protected static void a(DomobAdView domobAdView, c cVar) {
    }

    protected static void b(DomobAdView domobAdView, c cVar) {
    }

    /* access modifiers changed from: protected */
    public final e g() {
        return null;
    }

    public void SetAnimList(int[] animArray) {
        for (int valueOf : animArray) {
            this.B.add(Integer.valueOf(valueOf));
        }
    }

    /* access modifiers changed from: protected */
    public final ArrayList<Integer> h() {
        return this.B;
    }
}
