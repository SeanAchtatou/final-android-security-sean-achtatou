package cn.domob.android.ads;

import android.util.Log;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

final class q extends d {
    private HttpURLConnection m;

    protected q(String str, String str2, String str3, String str4, a aVar, int i, Map<String, String> map, String str5) {
        super(str2, str3, str4, aVar, i, null, str5);
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "connect url=" + str);
        }
        try {
            this.a = new URL(str);
        } catch (Exception e) {
            this.a = null;
            e.printStackTrace();
        }
        this.m = null;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0082  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a() {
        /*
            r8 = this;
            r1 = 3
            r7 = 0
            r6 = 1
            java.lang.String r5 = "DomobSDK"
            java.lang.String r0 = "DomobSDK"
            boolean r0 = android.util.Log.isLoggable(r5, r1)
            if (r0 == 0) goto L_0x0014
            java.lang.String r0 = "DomobSDK"
            java.lang.String r0 = "initConn"
            android.util.Log.d(r5, r0)
        L_0x0014:
            java.net.URL r0 = r8.a
            if (r0 == 0) goto L_0x016a
            r0 = 1
            java.net.HttpURLConnection.setFollowRedirects(r0)     // Catch:{ Exception -> 0x006d }
            r8.g()     // Catch:{ Exception -> 0x006d }
            java.net.Proxy r0 = r8.b     // Catch:{ Exception -> 0x006d }
            if (r0 == 0) goto L_0x008b
            java.net.URL r0 = r8.a     // Catch:{ Exception -> 0x006d }
            java.net.Proxy r1 = r8.b     // Catch:{ Exception -> 0x006d }
            java.net.URLConnection r0 = r0.openConnection(r1)     // Catch:{ Exception -> 0x006d }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x006d }
            r8.m = r0     // Catch:{ Exception -> 0x006d }
        L_0x002f:
            java.net.HttpURLConnection r0 = r8.m     // Catch:{ Exception -> 0x006d }
            if (r0 == 0) goto L_0x00db
            java.net.HttpURLConnection r0 = r8.m     // Catch:{ Exception -> 0x006d }
            int r1 = r8.e     // Catch:{ Exception -> 0x006d }
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x006d }
            java.net.HttpURLConnection r0 = r8.m     // Catch:{ Exception -> 0x006d }
            int r1 = r8.e     // Catch:{ Exception -> 0x006d }
            r0.setReadTimeout(r1)     // Catch:{ Exception -> 0x006d }
            java.util.Map r0 = r8.f     // Catch:{ Exception -> 0x006d }
            if (r0 == 0) goto L_0x0096
            java.util.Map r0 = r8.f     // Catch:{ Exception -> 0x006d }
            java.util.Set r0 = r0.keySet()     // Catch:{ Exception -> 0x006d }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ Exception -> 0x006d }
        L_0x004f:
            boolean r0 = r2.hasNext()     // Catch:{ Exception -> 0x006d }
            if (r0 == 0) goto L_0x0096
            java.lang.Object r0 = r2.next()     // Catch:{ Exception -> 0x006d }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x006d }
            if (r0 == 0) goto L_0x004f
            java.util.Map r1 = r8.f     // Catch:{ Exception -> 0x006d }
            java.lang.Object r1 = r1.get(r0)     // Catch:{ Exception -> 0x006d }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x006d }
            if (r1 == 0) goto L_0x004f
            java.net.HttpURLConnection r3 = r8.m     // Catch:{ Exception -> 0x006d }
            r3.addRequestProperty(r0, r1)     // Catch:{ Exception -> 0x006d }
            goto L_0x004f
        L_0x006d:
            r0 = move-exception
            java.lang.String r1 = "DomobSDK"
            java.lang.String r1 = "Error happened in connection."
            android.util.Log.e(r5, r1)
            r0.printStackTrace()
        L_0x0078:
            r0 = r7
        L_0x0079:
            if (r0 != 0) goto L_0x007e
            r1 = 0
            r8.h = r1
        L_0x007e:
            cn.domob.android.ads.a r1 = r8.c
            if (r1 == 0) goto L_0x0087
            cn.domob.android.ads.a r1 = r8.c
            r1.a(r8)
        L_0x0087:
            r8.g()
        L_0x008a:
            return r0
        L_0x008b:
            java.net.URL r0 = r8.a     // Catch:{ Exception -> 0x006d }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x006d }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x006d }
            r8.m = r0     // Catch:{ Exception -> 0x006d }
            goto L_0x002f
        L_0x0096:
            java.lang.String r0 = r8.g     // Catch:{ Exception -> 0x006d }
            if (r0 == 0) goto L_0x015e
            java.net.HttpURLConnection r0 = r8.m     // Catch:{ Exception -> 0x006d }
            java.lang.String r1 = "POST"
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x006d }
            java.net.HttpURLConnection r0 = r8.m     // Catch:{ Exception -> 0x006d }
            r1 = 1
            r0.setDoOutput(r1)     // Catch:{ Exception -> 0x006d }
            java.net.HttpURLConnection r0 = r8.m     // Catch:{ Exception -> 0x006d }
            java.lang.String r1 = "Content-Type"
            java.lang.String r2 = r8.d     // Catch:{ Exception -> 0x006d }
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x006d }
            java.net.HttpURLConnection r0 = r8.m     // Catch:{ Exception -> 0x006d }
            java.lang.String r1 = "Content-Length"
            java.lang.String r2 = r8.g     // Catch:{ Exception -> 0x006d }
            int r2 = r2.length()     // Catch:{ Exception -> 0x006d }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x006d }
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x006d }
            java.net.HttpURLConnection r0 = r8.m     // Catch:{ Exception -> 0x006d }
            java.io.OutputStream r0 = r0.getOutputStream()     // Catch:{ Exception -> 0x006d }
            java.io.BufferedWriter r1 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x006d }
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x006d }
            r2.<init>(r0)     // Catch:{ Exception -> 0x006d }
            r0 = 4096(0x1000, float:5.74E-42)
            r1.<init>(r2, r0)     // Catch:{ Exception -> 0x006d }
            java.lang.String r0 = r8.g     // Catch:{ Exception -> 0x006d }
            r1.write(r0)     // Catch:{ Exception -> 0x006d }
            r1.close()     // Catch:{ Exception -> 0x006d }
        L_0x00db:
            java.net.HttpURLConnection r0 = r8.m     // Catch:{ Exception -> 0x006d }
            int r0 = r0.getResponseCode()     // Catch:{ Exception -> 0x006d }
            r8.j = r0     // Catch:{ Exception -> 0x006d }
            int r0 = r8.j     // Catch:{ Exception -> 0x006d }
            r1 = 200(0xc8, float:2.8E-43)
            if (r0 < r1) goto L_0x0078
            int r0 = r8.j     // Catch:{ Exception -> 0x006d }
            r1 = 300(0x12c, float:4.2E-43)
            if (r0 >= r1) goto L_0x0078
            java.net.HttpURLConnection r0 = r8.m     // Catch:{ Exception -> 0x006d }
            java.lang.String r0 = r0.getContentType()     // Catch:{ Exception -> 0x006d }
            r8.k = r0     // Catch:{ Exception -> 0x006d }
            java.net.HttpURLConnection r0 = r8.m     // Catch:{ Exception -> 0x006d }
            java.lang.String r0 = r0.getContentEncoding()     // Catch:{ Exception -> 0x006d }
            r8.l = r0     // Catch:{ Exception -> 0x006d }
            java.lang.String r0 = "DomobSDK"
            r1 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ Exception -> 0x006d }
            if (r0 == 0) goto L_0x012a
            java.lang.String r0 = "DomobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006d }
            java.lang.String r2 = "resp type:"
            r1.<init>(r2)     // Catch:{ Exception -> 0x006d }
            java.lang.String r2 = r8.k     // Catch:{ Exception -> 0x006d }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x006d }
            java.lang.String r2 = ",mRespEncoding:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x006d }
            java.lang.String r2 = r8.l     // Catch:{ Exception -> 0x006d }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x006d }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x006d }
            android.util.Log.v(r0, r1)     // Catch:{ Exception -> 0x006d }
        L_0x012a:
            java.net.HttpURLConnection r0 = r8.m     // Catch:{ Exception -> 0x006d }
            java.net.URL r0 = r0.getURL()     // Catch:{ Exception -> 0x006d }
            r8.a = r0     // Catch:{ Exception -> 0x006d }
            boolean r0 = r8.i     // Catch:{ Exception -> 0x006d }
            if (r0 == 0) goto L_0x015b
            java.io.BufferedInputStream r0 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x006d }
            java.net.HttpURLConnection r1 = r8.m     // Catch:{ Exception -> 0x006d }
            java.io.InputStream r1 = r1.getInputStream()     // Catch:{ Exception -> 0x006d }
            r2 = 4096(0x1000, float:5.74E-42)
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x006d }
            r1 = 4096(0x1000, float:5.74E-42)
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x006d }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x006d }
            r3 = 4096(0x1000, float:5.74E-42)
            r2.<init>(r3)     // Catch:{ Exception -> 0x006d }
        L_0x014e:
            int r3 = r0.read(r1)     // Catch:{ Exception -> 0x006d }
            r4 = -1
            if (r3 != r4) goto L_0x0165
            byte[] r0 = r2.toByteArray()     // Catch:{ Exception -> 0x006d }
            r8.h = r0     // Catch:{ Exception -> 0x006d }
        L_0x015b:
            r0 = r6
            goto L_0x0079
        L_0x015e:
            java.net.HttpURLConnection r0 = r8.m     // Catch:{ Exception -> 0x006d }
            r0.connect()     // Catch:{ Exception -> 0x006d }
            goto L_0x00db
        L_0x0165:
            r4 = 0
            r2.write(r1, r4, r3)     // Catch:{ Exception -> 0x006d }
            goto L_0x014e
        L_0x016a:
            r0 = r7
            goto L_0x008a
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.q.a():boolean");
    }

    private void g() {
        if (this.m != null) {
            this.m.disconnect();
            this.m = null;
        }
    }

    public final void run() {
        try {
            a();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
