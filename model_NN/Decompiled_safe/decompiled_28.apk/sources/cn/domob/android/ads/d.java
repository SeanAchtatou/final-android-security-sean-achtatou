package cn.domob.android.ads;

import android.util.Log;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

abstract class d implements Runnable {
    private static Executor m = null;
    protected URL a;
    protected Proxy b;
    protected a c;
    protected String d;
    protected int e;
    protected Map<String, String> f;
    protected String g;
    protected byte[] h;
    protected boolean i;
    protected int j = 0;
    protected String k;
    protected String l;
    private String n;

    /* access modifiers changed from: package-private */
    public abstract boolean a();

    protected d(String str, String str2, String str3, a aVar, int i2, Map<String, String> map, String str4) {
        this.n = str;
        this.c = aVar;
        this.f = map;
        this.e = i2;
        if (str4 != null) {
            this.g = str4;
            this.d = "application/x-www-form-urlencoded";
        } else {
            this.g = null;
            this.d = null;
        }
        this.b = null;
        this.i = true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00d5 A[Catch:{ all -> 0x013b }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00e1 A[SYNTHETIC, Splitter:B:45:0x00e1] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00f0 A[Catch:{ all -> 0x013b }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00fc A[SYNTHETIC, Splitter:B:56:0x00fc] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x010b A[Catch:{ all -> 0x013e }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0125 A[SYNTHETIC, Splitter:B:66:0x0125] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x012d A[Catch:{ Exception -> 0x00bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.content.Context r10) {
        /*
            r9 = this;
            r7 = 0
            java.lang.String r0 = "DomobSDK"
            if (r10 != 0) goto L_0x0006
        L_0x0005:
            return
        L_0x0006:
            android.database.Cursor r0 = cn.domob.android.ads.DomobAdManager.j(r10)     // Catch:{ Exception -> 0x0135 }
            if (r0 == 0) goto L_0x00c3
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x00bb }
            if (r1 <= 0) goto L_0x00c3
            r0.moveToFirst()     // Catch:{ Exception -> 0x00bb }
            java.lang.String r1 = "proxy"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r2 = "port"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00bb }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r3 = "apn"
            int r3 = r0.getColumnIndexOrThrow(r3)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r3 = r0.getString(r3)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r4 = "DomobSDK"
            r5 = 3
            boolean r4 = android.util.Log.isLoggable(r4, r5)     // Catch:{ Exception -> 0x00bb }
            if (r4 == 0) goto L_0x0050
            java.lang.String r4 = "DomobSDK"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bb }
            java.lang.String r6 = "current apnType is "
            r5.<init>(r6)     // Catch:{ Exception -> 0x00bb }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00bb }
            android.util.Log.d(r4, r3)     // Catch:{ Exception -> 0x00bb }
        L_0x0050:
            java.lang.String r3 = "DomobSDK"
            r4 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r4)     // Catch:{ Exception -> 0x00bb }
            if (r3 == 0) goto L_0x0077
            java.lang.String r3 = "DomobSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bb }
            java.lang.String r5 = "proxy:"
            r4.<init>(r5)     // Catch:{ Exception -> 0x00bb }
            java.lang.StringBuilder r4 = r4.append(r1)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r5 = "| port:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00bb }
            java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00bb }
            android.util.Log.d(r3, r4)     // Catch:{ Exception -> 0x00bb }
        L_0x0077:
            if (r1 == 0) goto L_0x00c3
            java.lang.String r3 = ""
            boolean r3 = r1.equals(r3)     // Catch:{ Exception -> 0x00bb }
            if (r3 != 0) goto L_0x00c3
            java.lang.String r3 = "r.domob.cn"
            java.net.InetAddress r3 = java.net.InetAddress.getByName(r3)     // Catch:{ UnknownHostException -> 0x00ca, SocketTimeoutException -> 0x00e5, Exception -> 0x0100, all -> 0x0129 }
            java.net.Socket r4 = new java.net.Socket     // Catch:{ UnknownHostException -> 0x00ca, SocketTimeoutException -> 0x00e5, Exception -> 0x0100, all -> 0x0129 }
            r4.<init>()     // Catch:{ UnknownHostException -> 0x00ca, SocketTimeoutException -> 0x00e5, Exception -> 0x0100, all -> 0x0129 }
            java.net.InetSocketAddress r5 = new java.net.InetSocketAddress     // Catch:{ UnknownHostException -> 0x0146, SocketTimeoutException -> 0x0143, Exception -> 0x0140, all -> 0x0138 }
            r6 = 80
            r5.<init>(r3, r6)     // Catch:{ UnknownHostException -> 0x0146, SocketTimeoutException -> 0x0143, Exception -> 0x0140, all -> 0x0138 }
            r3 = 5000(0x1388, float:7.006E-42)
            r4.connect(r5, r3)     // Catch:{ UnknownHostException -> 0x0146, SocketTimeoutException -> 0x0143, Exception -> 0x0140, all -> 0x0138 }
            boolean r3 = r4.isConnected()     // Catch:{ UnknownHostException -> 0x0146, SocketTimeoutException -> 0x0143, Exception -> 0x0140, all -> 0x0138 }
            if (r3 == 0) goto L_0x0131
            java.lang.String r3 = "DomobSDK"
            r5 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r5)     // Catch:{ UnknownHostException -> 0x0146, SocketTimeoutException -> 0x0143, Exception -> 0x0140, all -> 0x0138 }
            if (r3 == 0) goto L_0x00ae
            java.lang.String r3 = "DomobSDK"
            java.lang.String r5 = "connected  not need use proxy"
            android.util.Log.d(r3, r5)     // Catch:{ UnknownHostException -> 0x0146, SocketTimeoutException -> 0x0143, Exception -> 0x0140, all -> 0x0138 }
        L_0x00ae:
            if (r0 == 0) goto L_0x00b3
            r0.close()     // Catch:{ UnknownHostException -> 0x0146, SocketTimeoutException -> 0x0143, Exception -> 0x0140, all -> 0x0138 }
        L_0x00b3:
            r4.close()     // Catch:{ UnknownHostException -> 0x0146, SocketTimeoutException -> 0x0143, Exception -> 0x0140, all -> 0x0138 }
            r4.close()     // Catch:{ Exception -> 0x00bb }
            goto L_0x0005
        L_0x00bb:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x00bf:
            r0.printStackTrace()
            r0 = r1
        L_0x00c3:
            if (r0 == 0) goto L_0x0005
            r0.close()
            goto L_0x0005
        L_0x00ca:
            r3 = move-exception
            r3 = r7
        L_0x00cc:
            java.lang.String r4 = "DomobSDK"
            r5 = 3
            boolean r4 = android.util.Log.isLoggable(r4, r5)     // Catch:{ all -> 0x013b }
            if (r4 == 0) goto L_0x00dc
            java.lang.String r4 = "DomobSDK"
            java.lang.String r5 = "socket connect  is  throws UnknownHostException "
            android.util.Log.i(r4, r5)     // Catch:{ all -> 0x013b }
        L_0x00dc:
            r9.a(r1, r2)     // Catch:{ all -> 0x013b }
            if (r3 == 0) goto L_0x00c3
            r3.close()     // Catch:{ Exception -> 0x00bb }
            goto L_0x00c3
        L_0x00e5:
            r3 = move-exception
            r3 = r7
        L_0x00e7:
            java.lang.String r4 = "DomobSDK"
            r5 = 3
            boolean r4 = android.util.Log.isLoggable(r4, r5)     // Catch:{ all -> 0x013b }
            if (r4 == 0) goto L_0x00f7
            java.lang.String r4 = "DomobSDK"
            java.lang.String r5 = "socket connect  is  throws SocketTimeoutException "
            android.util.Log.i(r4, r5)     // Catch:{ all -> 0x013b }
        L_0x00f7:
            r9.a(r1, r2)     // Catch:{ all -> 0x013b }
            if (r3 == 0) goto L_0x00c3
            r3.close()     // Catch:{ Exception -> 0x00bb }
            goto L_0x00c3
        L_0x0100:
            r1 = move-exception
            r2 = r7
        L_0x0102:
            java.lang.String r3 = "DomobSDK"
            r4 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r4)     // Catch:{ all -> 0x013e }
            if (r3 == 0) goto L_0x0123
            java.lang.String r3 = "DomobSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x013e }
            java.lang.String r5 = "socket exception "
            r4.<init>(r5)     // Catch:{ all -> 0x013e }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x013e }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x013e }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x013e }
            android.util.Log.e(r3, r1)     // Catch:{ all -> 0x013e }
        L_0x0123:
            if (r2 == 0) goto L_0x00c3
            r2.close()     // Catch:{ Exception -> 0x00bb }
            goto L_0x00c3
        L_0x0129:
            r1 = move-exception
            r2 = r7
        L_0x012b:
            if (r2 == 0) goto L_0x0130
            r2.close()     // Catch:{ Exception -> 0x00bb }
        L_0x0130:
            throw r1     // Catch:{ Exception -> 0x00bb }
        L_0x0131:
            r4.close()     // Catch:{ Exception -> 0x00bb }
            goto L_0x00c3
        L_0x0135:
            r0 = move-exception
            r1 = r7
            goto L_0x00bf
        L_0x0138:
            r1 = move-exception
            r2 = r4
            goto L_0x012b
        L_0x013b:
            r1 = move-exception
            r2 = r3
            goto L_0x012b
        L_0x013e:
            r1 = move-exception
            goto L_0x012b
        L_0x0140:
            r1 = move-exception
            r2 = r4
            goto L_0x0102
        L_0x0143:
            r3 = move-exception
            r3 = r4
            goto L_0x00e7
        L_0x0146:
            r3 = move-exception
            r3 = r4
            goto L_0x00cc
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.d.a(android.content.Context):void");
    }

    private final void a(String str, int i2) {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "setProxy -- proxy:" + str + "| port:" + i2);
        }
        this.b = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(str, i2));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (m == null) {
            m = Executors.newCachedThreadPool();
        }
        m.execute(this);
    }

    /* access modifiers changed from: protected */
    public final String c() {
        return this.n;
    }

    /* access modifiers changed from: protected */
    public final byte[] d() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public final int e() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public final String f() {
        if (this.l == null) {
            return XmlConstant.DEFAULT_ENCODING;
        }
        return this.l;
    }
}
