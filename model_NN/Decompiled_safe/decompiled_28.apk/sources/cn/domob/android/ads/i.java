package cn.domob.android.ads;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import cn.domob.android.ads.DomobReport;
import cn.domob.android.download.AppExchangeDownloader;
import cn.domob.android.download.AppExchangeDownloaderListener;
import com.adchina.android.ads.CookieDB;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

final class i {
    private ProgressDialog a = null;
    private WebView b;
    /* access modifiers changed from: private */
    public String c = null;
    private String d = null;
    /* access modifiers changed from: private */
    public Context e = null;
    /* access modifiers changed from: private */
    public Dialog f = null;
    private HashMap<String, String> g;
    private RelativeLayout h;
    private RelativeLayout i;
    private Animation j;
    private Animation k;
    private Animation l;
    private Animation m;
    /* access modifiers changed from: private */
    public Handler n = new Handler();
    /* access modifiers changed from: private */
    public int o = 0;
    private int p;
    private int q;
    private float r = 1.0f;
    private ImageButton s;
    private ImageButton t;
    /* access modifiers changed from: private */
    public ImageView u;
    /* access modifiers changed from: private */
    public RotateAnimation v;
    /* access modifiers changed from: private */
    public int w;
    private DomobAdView x;
    /* access modifiers changed from: private */
    public boolean y = false;
    /* access modifiers changed from: private */
    public boolean z = false;

    i() {
    }

    /* access modifiers changed from: protected */
    public final Dialog a(Context context, String str, String str2, ProgressDialog progressDialog, DomobAdView domobAdView, HashMap<String, String> hashMap) throws Exception {
        this.e = context;
        this.g = hashMap;
        this.x = domobAdView;
        Context context2 = this.e;
        this.r = DomobAdManager.a(domobAdView);
        this.c = a(str);
        this.d = str2;
        this.a = progressDialog;
        this.h = new RelativeLayout(context);
        this.h.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        this.u = new ImageView(this.e);
        this.u.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeStream(this.e.getAssets().open("loading.png"))));
        this.u.setVisibility(8);
        this.v = new RotateAnimation(0.0f, 360.0f, 1, 0.5f, 1, 0.5f);
        this.v.setDuration(1000);
        this.v.setInterpolator(new LinearInterpolator());
        this.v.setRepeatCount(-1);
        RelativeLayout relativeLayout = this.h;
        this.b = new WebView(this.e);
        if (this.d == null || this.d.length() <= 0) {
            this.b.loadUrl(this.c);
        } else {
            this.b.loadDataWithBaseURL(this.c, this.d, "text/html", "utf-8", "");
        }
        this.u.setVisibility(0);
        this.u.startAnimation(this.v);
        this.b.getSettings().setJavaScriptEnabled(true);
        this.b.setDownloadListener(new DownloadListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: cn.domob.android.ads.i.a(cn.domob.android.ads.i, java.lang.String, android.content.Context, boolean):void
             arg types: [cn.domob.android.ads.i, java.lang.String, android.content.Context, int]
             candidates:
              cn.domob.android.ads.i.a(cn.domob.android.ads.DomobAdView, java.lang.String, android.app.ProgressDialog, java.util.HashMap<java.lang.String, java.lang.String>):android.app.Dialog
              cn.domob.android.ads.i.a(cn.domob.android.ads.i, java.lang.String, android.content.Context, boolean):void */
            public final void onDownloadStart(String url, String str, String str2, String str3, long j) {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.i(Constants.LOG, "inapp download url:" + url);
                }
                i.a(i.this, url, i.this.e, false);
            }
        });
        this.b.setWebViewClient(new WebViewClient() {
            public final boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (!url.startsWith("wtai://wp/") || !url.startsWith("wtai://wp/mc;")) {
                    try {
                        if (url.startsWith("market://search?q=")) {
                            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(url));
                            intent.addCategory("android.intent.category.BROWSABLE");
                            i.this.e.startActivity(intent);
                            return true;
                        }
                    } catch (Exception e) {
                        if (Log.isLoggable(Constants.LOG, 3)) {
                            Log.e(Constants.LOG, "failed to intent market " + url);
                        }
                    }
                    if (url.startsWith("sms:") || url.startsWith("mailto:")) {
                        try {
                            i.this.e.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                            return true;
                        } catch (Exception e2) {
                            if (Log.isLoggable(Constants.LOG, 3)) {
                                Log.e(Constants.LOG, "failed to intent " + url);
                            }
                        }
                    }
                    view.loadUrl(url);
                    return true;
                }
                Log.e("in", "tel");
                i.this.e.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("tel:" + url.substring("wtai://wp/mc;".length()))));
                return true;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: cn.domob.android.ads.i.a(cn.domob.android.ads.i, boolean):void
             arg types: [cn.domob.android.ads.i, int]
             candidates:
              cn.domob.android.ads.i.a(java.lang.String, android.content.Context):android.graphics.drawable.Drawable
              cn.domob.android.ads.i.a(java.lang.String, android.widget.ImageButton):android.widget.LinearLayout
              cn.domob.android.ads.i.a(cn.domob.android.ads.i, int):void
              cn.domob.android.ads.i.a(int, int):boolean
              cn.domob.android.ads.i.a(cn.domob.android.ads.i, boolean):void */
            public final void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (!i.this.y) {
                    i.this.y = true;
                    i.a(i.this, 4);
                }
                i.this.u.clearAnimation();
                i.this.u.setVisibility(8);
                i.this.b();
            }

            public final void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                i.this.u.setVisibility(0);
                i.this.u.startAnimation(i.this.v);
                i.this.b();
            }

            public final void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                if (!i.this.z) {
                    i.this.z = true;
                    i.a(i.this, 5);
                }
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.e(Constants.LOG, "WebView ReceivedError:" + description);
                }
                super.onReceivedError(view, errorCode, description, failingUrl);
            }
        });
        this.b.setWebChromeClient(new WebChromeClient() {
            public final void onProgressChanged(WebView view, int newProgress) {
                i.this.b();
                super.onProgressChanged(view, newProgress);
            }
        });
        this.b.clearCache(false);
        this.b.getSettings().setCacheMode(1);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
        this.b.setScrollBarStyle(0);
        layoutParams2.addRule(12);
        layoutParams2.bottomMargin = (int) (40.0f * this.r);
        this.b.setLayoutParams(layoutParams2);
        relativeLayout.addView(this.b);
        this.h.addView(this.u, layoutParams);
        LinearLayout linearLayout = new LinearLayout(this.e);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(17);
        linearLayout.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeStream(this.e.getAssets().open("banner.png"))));
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, (int) (40.0f * this.r));
        layoutParams3.addRule(12);
        this.s = new ImageButton(this.e);
        this.s.setLayoutParams(new LinearLayout.LayoutParams((int) (40.0f * this.r), (int) (40.0f * this.r)));
        String str3 = this.b.canGoBack() ? "preview.png" : "preview_off.png";
        this.s.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                i.l(i.this);
            }
        });
        linearLayout.addView(a(str3, this.s));
        this.t = new ImageButton(this.e);
        this.t.setLayoutParams(new LinearLayout.LayoutParams((int) (40.0f * this.r), (int) (40.0f * this.r)));
        this.t.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                i.m(i.this);
            }
        });
        linearLayout.addView(a("next_off.png", this.t));
        ImageButton imageButton = new ImageButton(this.e);
        imageButton.setLayoutParams(new LinearLayout.LayoutParams((int) (40.0f * this.r), (int) (40.0f * this.r)));
        imageButton.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                try {
                    i.p(i.this);
                } catch (Exception e) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.e(Constants.LOG, "intent " + i.this.c + " error");
                    }
                }
            }
        });
        linearLayout.addView(a("refresh.png", imageButton));
        ImageButton imageButton2 = new ImageButton(this.e);
        imageButton2.setLayoutParams(new LinearLayout.LayoutParams((int) (40.0f * this.r), (int) (40.0f * this.r)));
        imageButton2.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                try {
                    i.this.e.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(i.this.c)));
                } catch (Exception e) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.e(Constants.LOG, "intent " + i.this.c + " error");
                    }
                }
            }
        });
        linearLayout.addView(a("out.png", imageButton2));
        linearLayout.addView(a());
        this.h.addView(linearLayout, layoutParams3);
        boolean z2 = false;
        if (context instanceof Activity) {
            z2 = a(((Activity) context).getWindow().getAttributes().flags, 1024);
        }
        if (z2) {
            this.f = new Dialog(context, 16973841);
        } else {
            this.f = new Dialog(context, 16973840);
        }
        this.j = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
        this.j.setDuration(500);
        this.h.startAnimation(this.j);
        WindowManager.LayoutParams attributes = this.f.getWindow().getAttributes();
        attributes.width = -1;
        attributes.height = -1;
        attributes.dimAmount = 0.5f;
        this.f.getWindow().setAttributes(attributes);
        this.f.getWindow().setFlags(2, 2);
        this.f.getWindow().requestFeature(2);
        this.f.setContentView(this.h);
        this.f.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public final void onDismiss(DialogInterface dialogInterface) {
                i.a(i.this, 6);
            }
        });
        return this.f;
    }

    /* access modifiers changed from: protected */
    public final Dialog a(DomobAdView domobAdView, String str, ProgressDialog progressDialog, HashMap<String, String> hashMap) throws Exception {
        RelativeLayout.LayoutParams layoutParams;
        this.o = 1;
        this.g = hashMap;
        this.e = domobAdView.getContext();
        Context context = this.e;
        this.r = DomobAdManager.a(domobAdView);
        this.c = a(str);
        this.a = progressDialog;
        this.p = DomobAdManager.a(this.e, domobAdView);
        this.q = DomobAdManager.b(this.e, domobAdView);
        WindowManager.LayoutParams attributes = this.a.getWindow().getAttributes();
        attributes.gravity = 80;
        this.a.getWindow().setAttributes(attributes);
        this.a.setProgressStyle(0);
        this.i = new RelativeLayout(this.e);
        this.h = new RelativeLayout(this.e);
        this.h.setPadding(10, 10, 10, 10);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(13);
        this.u = new ImageView(this.e);
        this.u.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeStream(this.e.getAssets().open("loading.png"))));
        this.u.setVisibility(8);
        this.v = new RotateAnimation(0.0f, 360.0f, 1, 0.5f, 1, 0.5f);
        this.v.setDuration(1000);
        this.v.setInterpolator(new AccelerateInterpolator(0.5f));
        this.v.setRepeatCount(-1);
        RelativeLayout relativeLayout = this.h;
        this.b = new WebView(this.e);
        this.b.loadUrl(this.c);
        this.u.setVisibility(0);
        this.u.startAnimation(this.v);
        this.b.getSettings().setJavaScriptEnabled(true);
        this.b.setWebViewClient(new WebViewClient() {
            public final void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                i.this.u.clearAnimation();
                i.this.u.setVisibility(8);
            }

            public final void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                i.this.u.setVisibility(0);
                i.this.u.startAnimation(i.this.v);
            }
        });
        this.b.setWebChromeClient(new WebChromeClient(this) {
            public final void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }
        });
        this.b.clearCache(true);
        this.b.getSettings().setCacheMode(1);
        if (DomobAdManager.d(this.e) != "v") {
            ((Activity) this.e).setRequestedOrientation(0);
            layoutParams = this.p / this.q > 0 ? ((float) (this.q - 20)) >= 300.0f * this.r ? new RelativeLayout.LayoutParams((int) (400.0f * this.r), (int) (300.0f * this.r)) : new RelativeLayout.LayoutParams((int) (((float) ((((int) (((float) (this.q - 20)) * this.r)) << 2) / 3)) * this.r), (int) (((float) (this.q - 20)) * this.r)) : this.q - 20 >= 400 ? new RelativeLayout.LayoutParams((int) (300.0f * this.r), (int) (400.0f * this.r)) : new RelativeLayout.LayoutParams((int) (((float) ((((int) (((float) (this.q - 20)) * this.r)) << 2) / 3)) * this.r), (int) (((float) (this.q - 20)) * this.r));
        } else if (this.q / this.p > 0) {
            ((Activity) this.e).setRequestedOrientation(1);
            layoutParams = ((float) (this.p - 20)) >= 300.0f * this.r ? new RelativeLayout.LayoutParams((int) (300.0f * this.r), (int) (400.0f * this.r)) : new RelativeLayout.LayoutParams((int) (((float) (this.p - 20)) * this.r), (int) (((float) ((((int) (((float) (this.p - 20)) * this.r)) << 2) / 3)) * this.r));
        } else {
            layoutParams = this.q - 20 >= 400 ? new RelativeLayout.LayoutParams((int) (300.0f * this.r), (int) (400.0f * this.r)) : new RelativeLayout.LayoutParams((int) (((float) ((((int) (((float) (this.q - 20)) * this.r)) * 3) / 4)) * this.r), (int) (((float) (this.q - 20)) * this.r));
        }
        layoutParams.addRule(15);
        this.b.setScrollBarStyle(0);
        this.b.setLayoutParams(layoutParams);
        relativeLayout.addView(this.b);
        this.h.addView(this.u, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(11);
        boolean z2 = false;
        if (this.e instanceof Activity) {
            z2 = a(((Activity) this.e).getWindow().getAttributes().flags, 1024);
        }
        if (z2) {
            this.f = new Dialog(this.e, 16973841);
        } else {
            this.f = new Dialog(this.e, 16973840);
        }
        this.f.setCancelable(true);
        WindowManager.LayoutParams attributes2 = this.f.getWindow().getAttributes();
        this.w = ((Activity) this.e).getRequestedOrientation();
        if (DomobAdManager.d(this.e) == "v") {
            ((Activity) this.e).setRequestedOrientation(1);
            if (this.q / this.p > 0) {
                if (this.p - 20 >= 300) {
                    attributes2.width = (int) (320.0f * this.r);
                    attributes2.height = (int) (420.0f * this.r);
                } else {
                    attributes2.width = (int) (((float) ((this.p - 20) + 20)) * this.r);
                    attributes2.height = (int) (((float) ((((this.p - 20) + 20) << 2) / 3)) * this.r);
                }
            } else if (this.q - 20 >= 400) {
                attributes2.width = (int) (300.0f * this.r);
                attributes2.height = (int) (400.0f * this.r);
            } else {
                attributes2.height = (int) (((float) (this.q - 20)) * this.r);
                attributes2.width = (int) (((float) ((attributes2.height * 3) / 4)) * this.r);
            }
        } else {
            ((Activity) this.e).setRequestedOrientation(0);
            if (this.p / this.q > 0) {
                if (this.q - 20 >= 400) {
                    attributes2.height = (int) (300.0f * this.r);
                    attributes2.width = (int) (400.0f * this.r);
                } else {
                    attributes2.width = (int) (((float) (((this.q - 20) << 2) / 3)) * this.r);
                    attributes2.height = (int) (((float) (this.q - 20)) * this.r);
                }
            } else if (this.q - 20 >= 400) {
                attributes2.width = (int) (300.0f * this.r);
                attributes2.height = (int) (400.0f * this.r);
            } else {
                attributes2.height = (int) (((float) (this.q - 20)) * this.r);
                attributes2.width = (int) (((float) ((attributes2.height * 3) / 4)) * this.r);
            }
        }
        attributes2.dimAmount = 0.7f;
        this.f.getWindow().setAttributes(attributes2);
        this.f.getWindow().setFlags(2, 2);
        this.h.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.i.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        RelativeLayout relativeLayout2 = this.i;
        Button button = new Button(this.e);
        button.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        button.setBackgroundColor(Color.argb(100, 0, 0, 0));
        relativeLayout2.addView(button);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams4.addRule(15);
        this.i.addView(this.h, layoutParams4);
        this.i.addView(a(), layoutParams3);
        this.f.setContentView(this.i);
        this.l = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, (float) (this.f.getWindow().getAttributes().width / 2), (float) (this.f.getWindow().getAttributes().height / 2));
        this.l.setDuration(500);
        this.i.startAnimation(this.l);
        this.f.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public final void onDismiss(DialogInterface dialogInterface) {
                i.a(i.this, 6);
            }
        });
        return this.f;
    }

    private static boolean a(int i2, int i3) {
        int length = Integer.toBinaryString(1024).length() - 1;
        if ((i2 >>> length) % 2 == (1024 >>> length) % 2) {
            return true;
        }
        return false;
    }

    private static String a(String str) {
        String trim = str.trim();
        int indexOf = trim.indexOf(58);
        boolean z2 = true;
        String str2 = trim;
        for (int i2 = 0; i2 < indexOf; i2++) {
            char charAt = str2.charAt(i2);
            if (!Character.isLetter(charAt)) {
                break;
            }
            z2 &= Character.isLowerCase(charAt);
            if (i2 == indexOf - 1 && !z2) {
                str2 = String.valueOf(str2.substring(0, indexOf).toLowerCase()) + str2.substring(indexOf);
            }
        }
        if (str2.startsWith("http://") || str2.startsWith("https://")) {
            return str2;
        }
        if (!str2.startsWith("http:") && !str2.startsWith("https:")) {
            return str2;
        }
        if (str2.startsWith("http:/") || str2.startsWith("https:/")) {
            return str2.replaceFirst("/", "//");
        }
        return str2.replaceFirst(":", "://");
    }

    private static Drawable a(String str, Context context) {
        InputStream inputStream;
        InputStream inputStream2;
        try {
            inputStream2 = context.getAssets().open(str);
            try {
                BitmapDrawable bitmapDrawable = new BitmapDrawable(BitmapFactory.decodeStream(inputStream2));
                try {
                    inputStream2.close();
                    return bitmapDrawable;
                } catch (Exception e2) {
                    Log.d(Constants.LOG, "asset stream close error");
                    return bitmapDrawable;
                }
            } catch (Exception e3) {
                try {
                    Log.d(Constants.LOG, "load " + str + " error");
                    try {
                        inputStream2.close();
                        return null;
                    } catch (Exception e4) {
                        Log.d(Constants.LOG, "asset stream close error");
                        return null;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    inputStream = inputStream2;
                    th = th2;
                    try {
                        inputStream.close();
                    } catch (Exception e5) {
                        Log.d(Constants.LOG, "asset stream close error");
                    }
                    throw th;
                }
            }
        } catch (Exception e6) {
            inputStream2 = null;
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
            inputStream.close();
            throw th;
        }
    }

    private LinearLayout a(String str, ImageButton imageButton) {
        LinearLayout.LayoutParams layoutParams;
        LinearLayout linearLayout = new LinearLayout(this.e);
        imageButton.setBackgroundDrawable(a(str, this.e));
        if (this.o == 1) {
            layoutParams = new LinearLayout.LayoutParams((int) (this.r * 30.0f), (int) (this.r * 30.0f));
        } else {
            layoutParams = new LinearLayout.LayoutParams((int) (this.r * 30.0f), (int) (this.r * 30.0f));
        }
        imageButton.setLayoutParams(layoutParams);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 1.0f));
        linearLayout.setGravity(17);
        linearLayout.addView(imageButton);
        return linearLayout;
    }

    class a implements Runnable {
        a() {
        }

        public final void run() {
            i.this.f.dismiss();
            if (i.this.o == 1) {
                ((Activity) i.this.e).setRequestedOrientation(i.this.w);
            }
        }
    }

    private LinearLayout a() {
        String str;
        LinearLayout.LayoutParams layoutParams;
        ImageButton imageButton = new ImageButton(this.e);
        if (this.o == 1) {
            str = "close.png";
            layoutParams = new LinearLayout.LayoutParams((int) (this.r * 30.0f), (int) (this.r * 30.0f));
        } else {
            str = "exit.png";
            layoutParams = new LinearLayout.LayoutParams((int) (this.r * 30.0f), (int) (this.r * 30.0f));
        }
        imageButton.setLayoutParams(layoutParams);
        imageButton.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                i.j(i.this);
                i.this.n.postDelayed(new a(), 500);
            }
        });
        return a(str, imageButton);
    }

    static /* synthetic */ void j(i iVar) {
        if (iVar.o == 0) {
            iVar.k = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
            iVar.k.setDuration(500);
            iVar.h.startAnimation(iVar.k);
        } else if (iVar.o == 1) {
            iVar.m = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, (float) (iVar.f.getWindow().getAttributes().width / 2), (float) (iVar.f.getWindow().getAttributes().height / 2));
            iVar.m.setDuration(500);
            iVar.i.startAnimation(iVar.m);
        }
    }

    static /* synthetic */ void l(i iVar) {
        if (iVar.b != null && iVar.b.canGoBack()) {
            iVar.b.goBack();
            iVar.b();
        }
    }

    static /* synthetic */ void m(i iVar) {
        if (iVar.b != null && iVar.b.canGoForward()) {
            iVar.b.goForward();
            iVar.b();
        }
    }

    static /* synthetic */ void p(i iVar) {
        if (iVar.b == null) {
            return;
        }
        if (iVar.b.canGoBack()) {
            iVar.b.reload();
        } else {
            iVar.b.loadUrl(iVar.c);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.b.canGoBack()) {
            try {
                this.s.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeStream(this.e.getAssets().open("preview.png"))));
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        } else {
            try {
                this.s.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeStream(this.e.getAssets().open("preview_off.png"))));
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        }
        if (this.b.canGoForward()) {
            try {
                this.t.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeStream(this.e.getAssets().open("next.png"))));
            } catch (IOException e4) {
                e4.printStackTrace();
            }
        } else {
            try {
                this.t.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeStream(this.e.getAssets().open("next_off.png"))));
            } catch (IOException e5) {
                e5.printStackTrace();
            }
        }
    }

    static /* synthetic */ void a(i iVar, int i2) {
        DomobReport domobReport = new DomobReport(iVar.e);
        domobReport.getClass();
        DomobReport.ReportInfo reportInfo = new DomobReport.ReportInfo(domobReport);
        reportInfo.a = i2;
        reportInfo.b = 1;
        reportInfo.c = new ArrayList<>();
        reportInfo.c.add(iVar.g.get("id"));
        reportInfo.d = iVar.g.get("rpurlmd5");
        reportInfo.e = iVar.x.getSpots();
        domobReport.a(reportInfo);
    }

    static /* synthetic */ void a(i iVar, String str, final Context context, boolean z2) {
        final Intent appIsDownload = AppExchangeDownloader.appIsDownload(context, str, iVar.g.get(CookieDB.KEY_NAME));
        if (appIsDownload != null) {
            new AlertDialog.Builder(context).setTitle("安装").setMessage(String.valueOf(iVar.g.get(CookieDB.KEY_NAME)) + "已经下载是否现在安装?").setNegativeButton("否", new DialogInterface.OnClickListener(iVar) {
                public final void onClick(DialogInterface dialogInterface, int i) {
                }
            }).setPositiveButton("是", new DialogInterface.OnClickListener(iVar) {
                public final void onClick(DialogInterface dialogInterface, int i) {
                    DomobActivity.initReceiver(context);
                    context.startActivity(appIsDownload);
                }
            }).show();
            return;
        }
        Toast.makeText(context, "开始下载", 0).show();
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.i(Constants.LOG, "startDownload url:" + str);
        }
        AppExchangeDownloader.downloadApp(str, iVar.g.get(CookieDB.KEY_NAME), context, new AppExchangeDownloaderListener() {
            public final void onStartDownload() {
                i.a(i.this, 0);
            }

            public final void onDownloadSuccess(String str) {
                i.a(i.this, 1);
                DomobActivity.initReceiver(context);
            }

            public final void onDownloadFailed(int i, String str) {
                i.a(i.this, 2);
            }

            public final void onDownloadCanceled() {
                i.a(i.this, 3);
            }
        });
    }
}
