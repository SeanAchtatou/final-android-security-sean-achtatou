package cn.domob.android.ads;

import android.content.Context;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import cn.domob.android.ads.DomobAdEngine;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class DomobReport {
    /* access modifiers changed from: private */
    public Context a;

    DomobReport() {
    }

    public class ActionType {
        public ActionType(DomobReport domobReport) {
        }
    }

    public class IDType {
        public IDType(DomobReport domobReport) {
        }
    }

    public static Animation a(int i, DomobAdView domobAdView) {
        switch (i) {
            case 1:
                p pVar = new p(0.0f, -90.0f, ((float) domobAdView.getWidth()) / 2.0f, ((float) domobAdView.getHeight()) / 2.0f, 0.0f, true);
                pVar.setDuration(500);
                pVar.setFillAfter(true);
                pVar.setInterpolator(new AccelerateInterpolator());
                return pVar;
            case 2:
                p pVar2 = new p(90.0f, 0.0f, ((float) domobAdView.getWidth()) / 2.0f, ((float) domobAdView.getHeight()) / 2.0f, 0.0f, false);
                pVar2.setDuration(500);
                pVar2.setFillAfter(true);
                pVar2.setInterpolator(new DecelerateInterpolator());
                return pVar2;
            case 3:
                AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.2f);
                alphaAnimation.setDuration(1000);
                alphaAnimation.setFillAfter(true);
                alphaAnimation.setInterpolator(new DecelerateInterpolator());
                return alphaAnimation;
            case 4:
                AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.3f, 1.0f);
                alphaAnimation2.setDuration(1600);
                alphaAnimation2.setFillAfter(true);
                alphaAnimation2.setInterpolator(new DecelerateInterpolator());
                return alphaAnimation2;
            case 5:
                ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 0.5f);
                scaleAnimation.setDuration(1000);
                scaleAnimation.setFillAfter(false);
                scaleAnimation.setInterpolator(new DecelerateInterpolator());
                return scaleAnimation;
            case 6:
            default:
                return null;
            case 7:
                TranslateAnimation translateAnimation = new Random().nextInt(3) == 0 ? new TranslateAnimation(1, -1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f) : new TranslateAnimation(1, 1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                translateAnimation.setDuration(1500);
                translateAnimation.setFillAfter(false);
                translateAnimation.setInterpolator(new DecelerateInterpolator());
                return translateAnimation;
        }
    }

    public class ReportInfo {
        int a;
        int b;
        ArrayList<String> c;
        String d;
        String e;

        public ReportInfo(DomobReport domobReport) {
        }
    }

    public DomobReport(Context context) {
        this.a = context;
    }

    public static int a(DomobAdView domobAdView) {
        Random random = new Random();
        int nextInt = (random.nextInt(5) << 1) + 1;
        ArrayList<Integer> h = domobAdView.h();
        while (h.size() > 0 && !h.contains(Integer.valueOf(nextInt))) {
            nextInt = (random.nextInt(5) << 1) + 1;
        }
        if (nextInt == 9) {
            return 7;
        }
        return nextInt;
    }

    /* access modifiers changed from: private */
    public static String a(int i) {
        switch (i) {
            case 0:
                return "download_start";
            case 1:
                return "download_finish";
            case 2:
                return "download_failed";
            case 3:
                return "download_cancel";
            case 4:
                return "load_success";
            case 5:
                return "load_failed";
            case 6:
                return "close_lp";
            case 7:
                return "install_success";
            case 8:
                return "run";
            case 9:
                return "confirm_download";
            default:
                return "";
        }
    }

    public final void a(final ReportInfo reportInfo) {
        new Thread() {
            public final void run() {
                d a2 = DomobAdEngine.a.a("http://e.domob.cn/report", DomobReport.a(DomobReport.this.b(reportInfo)));
                a2.a(DomobReport.this.a);
                if (a2.a()) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "send report:" + DomobReport.a(reportInfo.a) + " success");
                    }
                } else if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "send report:" + DomobReport.a(reportInfo.a) + " failed");
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public static String a(HashMap<String, String> hashMap) {
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (Map.Entry next : hashMap.entrySet()) {
            String str = (String) next.getKey();
            String str2 = (String) next.getValue();
            if (!(str == null || str.length() <= 0 || str2 == null)) {
                if (z) {
                    try {
                        sb.append(URLEncoder.encode(str, XmlConstant.DEFAULT_ENCODING)).append(XmlConstant.EQUAL).append(URLEncoder.encode(str2, XmlConstant.DEFAULT_ENCODING));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    sb.append("&").append(URLEncoder.encode(str, XmlConstant.DEFAULT_ENCODING)).append(XmlConstant.EQUAL).append(URLEncoder.encode(str2, XmlConstant.DEFAULT_ENCODING));
                }
            }
            if (z) {
                z = false;
            }
        }
        String sb2 = sb.toString();
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "post params string:" + sb2);
        }
        return sb2;
    }

    /* access modifiers changed from: private */
    public HashMap<String, String> b(ReportInfo reportInfo) {
        String str;
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("rt", "1");
        String a2 = l.a(this.a);
        if (a2 == null) {
            a2 = "Android,,,,,,,,";
        }
        hashMap.put("ua", a2);
        String g = DomobAdManager.g(this.a);
        if (g != null) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "CID:" + g);
            }
            hashMap.put("cid", g);
        }
        String publisherId = DomobAdManager.getPublisherId(this.a);
        if (publisherId == null || publisherId.length() <= 0) {
            Log.e(Constants.LOG, "publisher id is null or empty!");
        } else {
            hashMap.put("ipb", publisherId);
        }
        String str2 = reportInfo.d;
        if (str2 != null) {
            hashMap.put("rp_md5", str2);
        }
        String str3 = reportInfo.e;
        if (str3 != null && str3.length() > 0) {
            if (str3.length() > 256) {
                str3 = str3.substring(0, 256);
            }
            hashMap.put("spot", str3);
        }
        if (reportInfo.c != null) {
            try {
                JSONArray jSONArray = new JSONArray();
                for (int i = 0; i < reportInfo.c.size(); i++) {
                    jSONArray.put(reportInfo.c.get(i));
                }
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("type", a(reportInfo.a));
                jSONObject.put("ts", System.currentTimeMillis());
                switch (reportInfo.b) {
                    case 0:
                        str = "app";
                        break;
                    case 1:
                        str = "ad";
                        break;
                    case 2:
                        str = "pkg";
                        break;
                    default:
                        str = "";
                        break;
                }
                jSONObject.put("idtype", str);
                jSONObject.put("id", jSONArray);
                JSONArray jSONArray2 = new JSONArray();
                jSONArray2.put(jSONObject);
                hashMap.put("jstr", jSONArray2.toString());
            } catch (JSONException e) {
            }
        }
        return hashMap;
    }
}
