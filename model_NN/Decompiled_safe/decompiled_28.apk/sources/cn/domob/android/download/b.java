package cn.domob.android.download;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import cn.domob.android.ads.Constants;
import cn.domob.android.download.AppExchangeDownloader;
import com.wqmobile.sdk.pojoxml.util.XmlConstant;
import java.io.File;

final class b extends Thread {
    private AppExchangeDownloader.b a = null;
    private Context b = null;
    private long c = 0;
    private String d = "";
    private String e = "";
    private boolean f = false;
    private String g = "";

    protected b(Context context, String str, String str2, AppExchangeDownloader.b bVar) {
        this.a = bVar;
        this.b = context;
        this.g = str2;
        this.d = String.valueOf(str) + ".apk";
    }

    public final void run() {
        if (c() || !b()) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x016a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b() {
        /*
            r15 = this;
            r11 = 3
            r10 = 1
            r9 = 0
            java.lang.String r12 = "/DomobAppDownload/"
            java.lang.String r8 = "DomobSDK"
            r1 = 0
            r2 = 0
            java.lang.String r0 = r15.g     // Catch:{ Exception -> 0x00eb }
            java.net.URL r4 = new java.net.URL     // Catch:{ Exception -> 0x00eb }
            r4.<init>(r0)     // Catch:{ Exception -> 0x00eb }
            java.net.URLConnection r0 = r4.openConnection()     // Catch:{ Exception -> 0x00eb }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00eb }
            int r4 = r0.getResponseCode()     // Catch:{ Exception -> 0x00eb }
            r5 = 200(0xc8, float:2.8E-43)
            if (r4 < r5) goto L_0x0023
            r5 = 300(0x12c, float:4.2E-43)
            if (r4 < r5) goto L_0x00e4
        L_0x0023:
            r4 = -1
        L_0x0025:
            r15.c = r4     // Catch:{ Exception -> 0x00eb }
        L_0x0027:
            java.lang.String r0 = android.os.Environment.getExternalStorageState()
            java.lang.String r4 = "mounted"
            boolean r0 = r0.equals(r4)
            if (r0 == 0) goto L_0x016c
            android.os.StatFs r0 = new android.os.StatFs
            java.io.File r2 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r2 = r2.getPath()
            r0.<init>(r2)
            int r2 = r0.getBlockSize()
            long r2 = (long) r2
            int r0 = r0.getAvailableBlocks()
            long r4 = (long) r0
            long r2 = r2 * r4
            java.lang.String r0 = "DomobSDK"
            boolean r0 = android.util.Log.isLoggable(r8, r11)
            if (r0 == 0) goto L_0x0073
            java.lang.String r0 = "DomobSDK"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r4 = "sd availaSize="
            r0.<init>(r4)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r4 = "softsize="
            java.lang.StringBuilder r0 = r0.append(r4)
            long r4 = r15.c
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r8, r0)
        L_0x0073:
            long r4 = r15.c
            r6 = 2097152(0x200000, double:1.0361308E-317)
            long r4 = r4 + r6
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x016c
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "/DomobAppDownload/"
            java.lang.StringBuilder r0 = r0.append(r12)
            java.lang.String r0 = r0.toString()
            b(r0)
            r13 = r2
            r2 = r10
            r3 = r0
            r0 = r13
        L_0x009b:
            if (r2 != 0) goto L_0x016a
            java.io.File r2 = android.os.Environment.getDataDirectory()
            android.os.StatFs r3 = new android.os.StatFs
            java.lang.String r2 = r2.getPath()
            r3.<init>(r2)
            int r2 = r3.getBlockSize()
            long r4 = (long) r2
            int r2 = r3.getAvailableBlocks()
            long r2 = (long) r2
            long r2 = r2 * r4
            java.lang.String r4 = "DomobSDK"
            boolean r4 = android.util.Log.isLoggable(r8, r11)
            if (r4 == 0) goto L_0x00d1
            java.lang.String r4 = "DomobSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "rom"
            r4.<init>(r5)
            java.lang.StringBuilder r4 = r4.append(r2)
            java.lang.String r4 = r4.toString()
            android.util.Log.d(r8, r4)
        L_0x00d1:
            long r4 = r15.c
            r6 = 5242880(0x500000, double:2.590327E-317)
            long r4 = r4 + r6
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 >= 0) goto L_0x00f5
            cn.domob.android.download.AppExchangeDownloader$b r4 = r15.a
            long r5 = r15.c
            r4.a(r0, r2)
            r0 = r9
        L_0x00e3:
            return r0
        L_0x00e4:
            int r0 = r0.getContentLength()     // Catch:{ Exception -> 0x00eb }
            long r4 = (long) r0
            goto L_0x0025
        L_0x00eb:
            r0 = move-exception
            cn.domob.android.download.AppExchangeDownloader$b r0 = r15.a
            java.lang.String r4 = "连接下载地址错误"
            r0.d(r4)
            goto L_0x0027
        L_0x00f5:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            android.content.Context r1 = r15.b
            java.io.File r1 = r1.getFilesDir()
            java.lang.String r1 = r1.getAbsolutePath()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r0.<init>(r1)
            java.lang.String r1 = "/DomobAppDownload/"
            java.lang.StringBuilder r0 = r0.append(r12)
            java.lang.String r0 = r0.toString()
            b(r0)
            java.io.File r1 = new java.io.File
            r1.<init>(r0)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "chmod 777 "
            r2.<init>(r3)
            java.lang.String r1 = r1.getAbsolutePath()
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r1 = r1.toString()
            java.lang.Runtime r2 = java.lang.Runtime.getRuntime()
            java.lang.Process r1 = r2.exec(r1)     // Catch:{ IOException -> 0x0160, InterruptedException -> 0x0165 }
            int r1 = r1.waitFor()     // Catch:{ IOException -> 0x0160, InterruptedException -> 0x0165 }
            if (r1 == 0) goto L_0x0140
            cn.domob.android.download.AppExchangeDownloader$b r1 = r15.a     // Catch:{ IOException -> 0x0160, InterruptedException -> 0x0165 }
            r1.a()     // Catch:{ IOException -> 0x0160, InterruptedException -> 0x0165 }
        L_0x0140:
            cn.domob.android.download.AppExchangeDownloader$b r1 = r15.a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r2.<init>(r0)
            java.lang.String r0 = r15.d
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = ".temp"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r1.b(r0)
            r0 = r10
            goto L_0x00e3
        L_0x0160:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0140
        L_0x0165:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0140
        L_0x016a:
            r0 = r3
            goto L_0x0140
        L_0x016c:
            r13 = r2
            r2 = r9
            r3 = r1
            r0 = r13
            goto L_0x009b
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.download.b.b():boolean");
    }

    private boolean c() {
        this.e = Environment.getExternalStorageDirectory() + "/DomobAppDownload/" + this.d;
        if (a(this.e)) {
            return true;
        }
        this.e = String.valueOf(this.b.getFilesDir().getAbsolutePath()) + "/DomobAppDownload/" + this.d;
        if (a(this.e)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final String a() {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "app is download:" + c() + XmlConstant.SINGLE_SPACE + "is complete:" + this.f);
        }
        if (!c()) {
            return null;
        }
        if (this.f) {
            return null;
        }
        return this.e;
    }

    private boolean a(String str) {
        File file = new File(str);
        if (!file.exists()) {
            return false;
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, String.valueOf(str) + "download size=" + file.length() + XmlConstant.SINGLE_SPACE + "softSize:" + this.c);
        }
        if (file.length() == this.c) {
            if (this.a != null) {
                this.a.a(str);
                return true;
            }
        } else if (new File(String.valueOf(str) + ".temp").exists()) {
            this.f = true;
            if (this.a != null) {
                this.a.c(String.valueOf(str) + ".temp");
            }
        }
        return true;
    }

    private static boolean b(String str) {
        File file = new File(str);
        if (!file.exists()) {
            return file.mkdirs();
        }
        return true;
    }
}
