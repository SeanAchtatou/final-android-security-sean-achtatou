package defpackage;

import android.webkit.WebView;
import cn.domob.android.ads.DomobAdManager;
import com.google.ads.util.a;
import java.util.HashMap;

/* renamed from: p  reason: default package */
public final class p implements i {
    public final void a(d dVar, HashMap<String, String> hashMap, WebView webView) {
        String str = hashMap.get(DomobAdManager.ACTION_URL);
        a.c("Received ad url: <\"url\": \"" + str + "\", \"afmaNotifyDt\": \"" + hashMap.get("afma_notify_dt") + "\">");
        c g = dVar.g();
        if (g != null) {
            g.b(str);
        }
    }
}
