package net.youmi.android;

import android.content.Context;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

abstract class bi implements ec {
    protected String c;
    protected String d;
    protected long e;
    protected String f;
    protected final String g = "utf-8";
    protected Context h;
    protected DefaultHttpClient i;
    protected InputStream j;
    protected OutputStream k;
    protected List l;
    protected int m = 4;

    bi(Context context, String str) {
        this.h = context;
        this.c = str;
        this.d = str;
    }

    private int a(int i2) {
        HttpPost httpGet;
        try {
            if (!Cdo.c(this.h)) {
                try {
                    if (this.k != null) {
                        this.k.close();
                    }
                } catch (Exception e2) {
                }
                try {
                    if (this.j != null) {
                        this.j.close();
                    }
                } catch (Exception e3) {
                }
                try {
                    if (this.i != null) {
                        this.i.getConnectionManager().shutdown();
                    }
                } catch (Exception e4) {
                }
                return 0;
            } else if (!r.b(this.h)) {
                try {
                    if (this.k != null) {
                        this.k.close();
                    }
                } catch (Exception e5) {
                }
                try {
                    if (this.j != null) {
                        this.j.close();
                    }
                } catch (Exception e6) {
                }
                try {
                    if (this.i != null) {
                        this.i.getConnectionManager().shutdown();
                    }
                } catch (Exception e7) {
                }
                return 1;
            } else {
                this.i = r.a(this.h, this);
                if (i2 == 1) {
                    httpGet = new HttpPost(this.c);
                    if (this.l != null && this.l.size() > 0) {
                        httpGet.setEntity(new UrlEncodedFormEntity(this.l, "utf-8"));
                    }
                } else {
                    httpGet = new HttpGet(this.c);
                }
                HttpResponse execute = this.i.execute(httpGet);
                if (execute.getStatusLine().getStatusCode() != 200) {
                    try {
                        if (this.k != null) {
                            this.k.close();
                        }
                    } catch (Exception e8) {
                    }
                    try {
                        if (this.j != null) {
                            this.j.close();
                        }
                    } catch (Exception e9) {
                    }
                    try {
                        if (this.i != null) {
                            this.i.getConnectionManager().shutdown();
                        }
                    } catch (Exception e10) {
                    }
                    return 2;
                } else if (!a(execute)) {
                    int i3 = this.m;
                    try {
                        if (this.k != null) {
                            this.k.close();
                        }
                    } catch (Exception e11) {
                    }
                    try {
                        if (this.j != null) {
                            this.j.close();
                        }
                    } catch (Exception e12) {
                    }
                    try {
                        if (this.i == null) {
                            return i3;
                        }
                        this.i.getConnectionManager().shutdown();
                        return i3;
                    } catch (Exception e13) {
                        return i3;
                    }
                } else {
                    HttpEntity entity = execute.getEntity();
                    if (entity != null) {
                        this.e = entity.getContentLength();
                        this.j = entity.getContent();
                        if (this.j != null) {
                            if (b()) {
                                byte[] bArr = new byte[1024];
                                while (true) {
                                    int read = this.j.read(bArr);
                                    if (read <= 0) {
                                        break;
                                    }
                                    this.k.write(bArr, 0, read);
                                }
                                if (c()) {
                                    try {
                                        if (this.k != null) {
                                            this.k.close();
                                        }
                                    } catch (Exception e14) {
                                    }
                                    try {
                                        if (this.j != null) {
                                            this.j.close();
                                        }
                                    } catch (Exception e15) {
                                    }
                                    try {
                                        if (this.i != null) {
                                            this.i.getConnectionManager().shutdown();
                                        }
                                    } catch (Exception e16) {
                                    }
                                    return 3;
                                }
                                int i4 = this.m;
                                try {
                                    if (this.k != null) {
                                        this.k.close();
                                    }
                                } catch (Exception e17) {
                                }
                                try {
                                    if (this.j != null) {
                                        this.j.close();
                                    }
                                } catch (Exception e18) {
                                }
                                try {
                                    if (this.i == null) {
                                        return i4;
                                    }
                                    this.i.getConnectionManager().shutdown();
                                    return i4;
                                } catch (Exception e19) {
                                    return i4;
                                }
                            } else {
                                int i5 = this.m;
                                try {
                                    if (this.k != null) {
                                        this.k.close();
                                    }
                                } catch (Exception e20) {
                                }
                                try {
                                    if (this.j != null) {
                                        this.j.close();
                                    }
                                } catch (Exception e21) {
                                }
                                try {
                                    if (this.i == null) {
                                        return i5;
                                    }
                                    this.i.getConnectionManager().shutdown();
                                    return i5;
                                } catch (Exception e22) {
                                    return i5;
                                }
                            }
                        }
                    }
                    try {
                        if (this.k != null) {
                            this.k.close();
                        }
                    } catch (Exception e23) {
                    }
                    try {
                        if (this.j != null) {
                            this.j.close();
                        }
                    } catch (Exception e24) {
                    }
                    try {
                        if (this.i != null) {
                            this.i.getConnectionManager().shutdown();
                        }
                    } catch (Exception e25) {
                    }
                    return 2;
                }
            }
        } catch (Exception e26) {
            try {
                if (this.k != null) {
                    this.k.close();
                }
            } catch (Exception e27) {
            }
            try {
                if (this.j != null) {
                    this.j.close();
                }
            } catch (Exception e28) {
            }
            try {
                if (this.i != null) {
                    this.i.getConnectionManager().shutdown();
                }
            } catch (Exception e29) {
            }
            return 2;
        } catch (Throwable th) {
            try {
                if (this.k != null) {
                    this.k.close();
                }
            } catch (Exception e30) {
            }
            try {
                if (this.j != null) {
                    this.j.close();
                }
            } catch (Exception e31) {
            }
            try {
                if (this.i != null) {
                    this.i.getConnectionManager().shutdown();
                }
            } catch (Exception e32) {
            }
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public int a(List list) {
        this.l = list;
        return a(1);
    }

    public void a(String str) {
        this.d = str;
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(HttpResponse httpResponse);

    /* access modifiers changed from: protected */
    public abstract boolean b();

    /* access modifiers changed from: protected */
    public abstract boolean c();
}
