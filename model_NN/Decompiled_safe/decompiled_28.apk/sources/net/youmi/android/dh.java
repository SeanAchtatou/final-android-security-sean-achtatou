package net.youmi.android;

import com.madhouse.android.ads.AdView;

class dh {
    final /* synthetic */ bz a;
    private float b = 9.0f;
    private float c = 16.0f;
    private float d = 11.0f;

    dh(bz bzVar, bz bzVar2) {
        this.a = bzVar;
        if (!bzVar2.c()) {
            switch (bzVar2.e()) {
                case 120:
                    this.b = 7.0f;
                    this.c = 16.0f;
                    this.d = 11.0f;
                    return;
                case 160:
                    this.b = 9.0f;
                    this.c = 16.0f;
                    this.d = 11.0f;
                    return;
                case AdView.AD_MEASURE_240:
                    this.b = 14.0f;
                    this.c = 16.0f;
                    this.d = 11.0f;
                    return;
                case AdView.AD_MEASURE_320:
                    this.b = 18.0f;
                    this.c = 16.0f;
                    this.d = 11.0f;
                    return;
                default:
                    this.b = 9.0f;
                    this.c = 16.0f;
                    this.d = 11.0f;
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public float a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public float b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public float c() {
        return this.d;
    }
}
