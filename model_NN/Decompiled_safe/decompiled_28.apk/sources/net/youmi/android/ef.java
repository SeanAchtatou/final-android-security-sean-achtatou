package net.youmi.android;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import java.util.Locale;

class ef {
    private static int a = -1;
    private static int b = -1;
    private static int c = -1;
    private static int d = -1;
    private static int e = -1;
    private static String f;
    private static String g;
    private static int h = -1;
    private static String i;
    private static String j;
    private static String k = "";
    private static ej l = null;
    private static int m = 0;
    /* access modifiers changed from: private */
    public static long n = 0;
    /* access modifiers changed from: private */
    public static long o = 0;
    /* access modifiers changed from: private */
    public static boolean p = false;

    ef() {
    }

    static String a() {
        if (g == null) {
            Locale locale = Locale.getDefault();
            g = String.format("%s-%s", locale.getLanguage(), locale.getCountry());
        }
        return g;
    }

    static ej a(Context context) {
        if (l == null) {
            l = new ej(context);
        }
        return l;
    }

    static boolean a(int i2) {
        return h == i2;
    }

    static boolean a(long j2) {
        if (j2 - o <= g()) {
            return false;
        }
        if (!p) {
            return true;
        }
        if (j2 - n <= 35000) {
            return false;
        }
        p = false;
        return true;
    }

    static String b() {
        return Build.MODEL;
    }

    static void b(int i2) {
        h = i2;
        m = 0;
    }

    static void b(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager != null) {
                try {
                    String networkOperatorName = telephonyManager.getNetworkOperatorName();
                    if (networkOperatorName == null) {
                        i = "";
                    } else {
                        i = networkOperatorName;
                    }
                } catch (Exception e2) {
                }
                try {
                    String line1Number = telephonyManager.getLine1Number();
                    if (line1Number == null) {
                        j = "";
                        return;
                    }
                    String trim = line1Number.trim();
                    if (trim.length() > 11) {
                        int length = trim.length() - 11;
                        j = trim.substring(length, length + 7);
                    } else if (trim.length() == 11) {
                        j = trim.substring(0, 7);
                    } else {
                        j = "";
                    }
                } catch (Exception e3) {
                }
            }
        } catch (Exception e4) {
        }
    }

    static String c() {
        return "android " + Build.VERSION.RELEASE;
    }

    static String c(Context context) {
        if (j == null) {
            b(context);
        }
        return j == null ? "" : j;
    }

    static String d() {
        return Build.BRAND;
    }

    static String d(Context context) {
        if (i == null) {
            b(context);
        }
        return i == null ? "" : i;
    }

    static void e() {
        m++;
    }

    static boolean e(Context context) {
        if (eo.a()) {
            return true;
        }
        return a(context).f();
    }

    static int f() {
        return m;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0053 A[Catch:{ Exception -> 0x00b5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0071 A[Catch:{ Exception -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0111 A[Catch:{ Exception -> 0x0202 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static synchronized java.lang.String f(android.content.Context r12) {
        /*
            r3 = 3
            r6 = 0
            r9 = -1
            java.lang.String r10 = ""
            java.lang.String r0 = "|"
            java.lang.Class<net.youmi.android.ef> r0 = net.youmi.android.ef.class
            monitor-enter(r0)
            java.lang.String r1 = net.youmi.android.ef.f     // Catch:{ all -> 0x0227 }
            if (r1 == 0) goto L_0x001a
            java.lang.String r1 = net.youmi.android.ef.f     // Catch:{ all -> 0x0227 }
            int r1 = r1.length()     // Catch:{ all -> 0x0227 }
            if (r1 <= 0) goto L_0x001a
            java.lang.String r1 = net.youmi.android.ef.f     // Catch:{ all -> 0x0227 }
        L_0x0018:
            monitor-exit(r0)
            return r1
        L_0x001a:
            java.lang.String r1 = "phone"
            java.lang.Object r12 = r12.getSystemService(r1)     // Catch:{ Exception -> 0x0202 }
            android.telephony.TelephonyManager r12 = (android.telephony.TelephonyManager) r12     // Catch:{ Exception -> 0x0202 }
            if (r12 == 0) goto L_0x0206
            java.lang.String r1 = ""
            java.lang.String r1 = ""
            java.lang.String r1 = r12.getNetworkOperator()     // Catch:{ Exception -> 0x00b0 }
            java.lang.String r1 = r1.trim()     // Catch:{ Exception -> 0x00b0 }
            int r2 = r1.length()     // Catch:{ Exception -> 0x00b0 }
            if (r2 <= r3) goto L_0x0236
            r2 = 0
            r3 = 3
            java.lang.String r2 = r1.substring(r2, r3)     // Catch:{ Exception -> 0x00b0 }
            r3 = 3
            java.lang.String r1 = r1.substring(r3)     // Catch:{ Exception -> 0x0232 }
        L_0x0041:
            int r3 = r2.length()     // Catch:{ Exception -> 0x0202 }
            if (r3 == 0) goto L_0x004d
            int r3 = r1.length()     // Catch:{ Exception -> 0x0202 }
            if (r3 != 0) goto L_0x006a
        L_0x004d:
            java.lang.String r3 = r12.getSubscriberId()     // Catch:{ Exception -> 0x00b5 }
            if (r3 == 0) goto L_0x006a
            java.lang.String r3 = r3.trim()     // Catch:{ Exception -> 0x00b5 }
            int r4 = r3.length()     // Catch:{ Exception -> 0x00b5 }
            r5 = 5
            if (r4 <= r5) goto L_0x006a
            r4 = 0
            r5 = 3
            java.lang.String r2 = r3.substring(r4, r5)     // Catch:{ Exception -> 0x00b5 }
            r4 = 3
            r5 = 5
            java.lang.String r1 = r3.substring(r4, r5)     // Catch:{ Exception -> 0x00b5 }
        L_0x006a:
            int r3 = r12.getPhoneType()     // Catch:{ Exception -> 0x0202 }
            r4 = 1
            if (r3 != r4) goto L_0x0111
            int r3 = net.youmi.android.ef.a     // Catch:{ Exception -> 0x0202 }
            if (r3 <= r9) goto L_0x00be
            int r3 = net.youmi.android.ef.b     // Catch:{ Exception -> 0x0202 }
            if (r3 <= r9) goto L_0x00be
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0202 }
            java.lang.String r4 = "0|"
            r3.<init>(r4)     // Catch:{ Exception -> 0x0202 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ Exception -> 0x0202 }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0202 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Exception -> 0x0202 }
            java.lang.String r2 = "|"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            int r2 = net.youmi.android.ef.a     // Catch:{ Exception -> 0x0202 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            java.lang.String r2 = "|"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            int r2 = net.youmi.android.ef.b     // Catch:{ Exception -> 0x0202 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0202 }
            net.youmi.android.ef.f = r1     // Catch:{ Exception -> 0x0202 }
            java.lang.String r1 = net.youmi.android.ef.f     // Catch:{ Exception -> 0x0202 }
            goto L_0x0018
        L_0x00b0:
            r1 = move-exception
            r1 = r10
        L_0x00b2:
            r2 = r1
            r1 = r10
            goto L_0x0041
        L_0x00b5:
            r3 = move-exception
            r11 = r3
            r3 = r2
            r2 = r11
            net.youmi.android.f.a(r2)     // Catch:{ Exception -> 0x0202 }
            r2 = r3
            goto L_0x006a
        L_0x00be:
            android.telephony.CellLocation r12 = r12.getCellLocation()     // Catch:{ Exception -> 0x0202 }
            android.telephony.gsm.GsmCellLocation r12 = (android.telephony.gsm.GsmCellLocation) r12     // Catch:{ Exception -> 0x0202 }
            if (r12 == 0) goto L_0x00d2
            int r3 = r12.getCid()     // Catch:{ Exception -> 0x0202 }
            net.youmi.android.ef.a = r3     // Catch:{ Exception -> 0x0202 }
            int r3 = r12.getLac()     // Catch:{ Exception -> 0x0202 }
            net.youmi.android.ef.b = r3     // Catch:{ Exception -> 0x0202 }
        L_0x00d2:
            int r3 = net.youmi.android.ef.a     // Catch:{ Exception -> 0x0202 }
            if (r3 <= r9) goto L_0x0206
            int r3 = net.youmi.android.ef.b     // Catch:{ Exception -> 0x0202 }
            if (r3 <= r9) goto L_0x0206
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0202 }
            java.lang.String r4 = "0|"
            r3.<init>(r4)     // Catch:{ Exception -> 0x0202 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ Exception -> 0x0202 }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0202 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Exception -> 0x0202 }
            java.lang.String r2 = "|"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            int r2 = net.youmi.android.ef.a     // Catch:{ Exception -> 0x0202 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            java.lang.String r2 = "|"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            int r2 = net.youmi.android.ef.b     // Catch:{ Exception -> 0x0202 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0202 }
            net.youmi.android.ef.f = r1     // Catch:{ Exception -> 0x0202 }
            java.lang.String r1 = net.youmi.android.ef.f     // Catch:{ Exception -> 0x0202 }
            goto L_0x0018
        L_0x0111:
            r4 = 2
            if (r3 != r4) goto L_0x022a
            int r3 = net.youmi.android.ef.c     // Catch:{ Exception -> 0x0202 }
            if (r3 <= r9) goto L_0x0163
            int r3 = net.youmi.android.ef.e     // Catch:{ Exception -> 0x0202 }
            if (r3 <= r9) goto L_0x0163
            int r3 = net.youmi.android.ef.d     // Catch:{ Exception -> 0x0202 }
            if (r3 <= r9) goto L_0x0163
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0202 }
            java.lang.String r4 = "1|"
            r3.<init>(r4)     // Catch:{ Exception -> 0x0202 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ Exception -> 0x0202 }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0202 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Exception -> 0x0202 }
            java.lang.String r2 = "|"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            int r2 = net.youmi.android.ef.c     // Catch:{ Exception -> 0x0202 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            java.lang.String r2 = "|"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            int r2 = net.youmi.android.ef.e     // Catch:{ Exception -> 0x0202 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            java.lang.String r2 = "|"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            int r2 = net.youmi.android.ef.d     // Catch:{ Exception -> 0x0202 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0202 }
            net.youmi.android.ef.f = r1     // Catch:{ Exception -> 0x0202 }
            java.lang.String r1 = net.youmi.android.ef.f     // Catch:{ Exception -> 0x0202 }
            goto L_0x0018
        L_0x0163:
            android.telephony.CellLocation r3 = r12.getCellLocation()     // Catch:{ Exception -> 0x0202 }
            java.lang.Class r4 = r3.getClass()     // Catch:{ Exception -> 0x0202 }
            java.lang.reflect.Method[] r4 = r4.getMethods()     // Catch:{ Exception -> 0x0202 }
            if (r4 == 0) goto L_0x0175
            r5 = r6
        L_0x0172:
            int r6 = r4.length     // Catch:{ Exception -> 0x0202 }
            if (r5 < r6) goto L_0x01c4
        L_0x0175:
            int r3 = net.youmi.android.ef.c     // Catch:{ Exception -> 0x0202 }
            if (r3 <= r9) goto L_0x0206
            int r3 = net.youmi.android.ef.e     // Catch:{ Exception -> 0x0202 }
            if (r3 <= r9) goto L_0x0206
            int r3 = net.youmi.android.ef.d     // Catch:{ Exception -> 0x0202 }
            if (r3 <= r9) goto L_0x0206
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0202 }
            java.lang.String r4 = "1|"
            r3.<init>(r4)     // Catch:{ Exception -> 0x0202 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ Exception -> 0x0202 }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0202 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Exception -> 0x0202 }
            java.lang.String r2 = "|"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            int r2 = net.youmi.android.ef.c     // Catch:{ Exception -> 0x0202 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            java.lang.String r2 = "|"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            int r2 = net.youmi.android.ef.e     // Catch:{ Exception -> 0x0202 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            java.lang.String r2 = "|"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            int r2 = net.youmi.android.ef.d     // Catch:{ Exception -> 0x0202 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0202 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0202 }
            net.youmi.android.ef.f = r1     // Catch:{ Exception -> 0x0202 }
            java.lang.String r1 = net.youmi.android.ef.f     // Catch:{ Exception -> 0x0202 }
            goto L_0x0018
        L_0x01c4:
            r6 = r4[r5]     // Catch:{ Exception -> 0x0202 }
            if (r6 == 0) goto L_0x01e3
            java.lang.String r7 = r6.getName()     // Catch:{ Exception -> 0x0202 }
            java.lang.String r8 = "getBaseStationId"
            boolean r7 = r7.equals(r8)     // Catch:{ Exception -> 0x0202 }
            if (r7 == 0) goto L_0x01e6
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x0202 }
            java.lang.Object r12 = r6.invoke(r3, r7)     // Catch:{ Exception -> 0x0202 }
            java.lang.Integer r12 = (java.lang.Integer) r12     // Catch:{ Exception -> 0x0202 }
            int r6 = r12.intValue()     // Catch:{ Exception -> 0x0202 }
            net.youmi.android.ef.c = r6     // Catch:{ Exception -> 0x0202 }
        L_0x01e3:
            int r5 = r5 + 1
            goto L_0x0172
        L_0x01e6:
            java.lang.String r7 = r6.getName()     // Catch:{ Exception -> 0x0202 }
            java.lang.String r8 = "getNetworkId"
            boolean r7 = r7.equals(r8)     // Catch:{ Exception -> 0x0202 }
            if (r7 == 0) goto L_0x020b
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x0202 }
            java.lang.Object r12 = r6.invoke(r3, r7)     // Catch:{ Exception -> 0x0202 }
            java.lang.Integer r12 = (java.lang.Integer) r12     // Catch:{ Exception -> 0x0202 }
            int r6 = r12.intValue()     // Catch:{ Exception -> 0x0202 }
            net.youmi.android.ef.e = r6     // Catch:{ Exception -> 0x0202 }
            goto L_0x01e3
        L_0x0202:
            r1 = move-exception
            net.youmi.android.f.a(r1)     // Catch:{ all -> 0x0227 }
        L_0x0206:
            java.lang.String r1 = ""
            r1 = r10
            goto L_0x0018
        L_0x020b:
            java.lang.String r7 = r6.getName()     // Catch:{ Exception -> 0x0202 }
            java.lang.String r8 = "getSystemId"
            boolean r7 = r7.equals(r8)     // Catch:{ Exception -> 0x0202 }
            if (r7 == 0) goto L_0x01e3
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x0202 }
            java.lang.Object r12 = r6.invoke(r3, r7)     // Catch:{ Exception -> 0x0202 }
            java.lang.Integer r12 = (java.lang.Integer) r12     // Catch:{ Exception -> 0x0202 }
            int r6 = r12.intValue()     // Catch:{ Exception -> 0x0202 }
            net.youmi.android.ef.d = r6     // Catch:{ Exception -> 0x0202 }
            goto L_0x01e3
        L_0x0227:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x022a:
            java.lang.String r1 = ""
            net.youmi.android.ef.f = r1     // Catch:{ Exception -> 0x0202 }
            java.lang.String r1 = net.youmi.android.ef.f     // Catch:{ Exception -> 0x0202 }
            goto L_0x0018
        L_0x0232:
            r1 = move-exception
            r1 = r2
            goto L_0x00b2
        L_0x0236:
            r1 = r10
            r2 = r10
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.ef.f(android.content.Context):java.lang.String");
    }

    static long g() {
        long g2 = (long) bp.g();
        if (g2 < eo.g()) {
            g2 = eo.g();
        }
        return g2 < bj.a() ? bj.a() : g2;
    }
}
