package net.youmi.android;

import com.madhouse.android.ads.AdView;

final class dt {
    static final dt a = new dt(AdView.AD_MEASURE_240, 38);
    static final dt b = new dt(AdView.AD_MEASURE_320, 50);
    static final dt c = new dt(AdView.AD_MEASURE_480, 75);
    static final dt d = new dt(AdView.AD_MEASURE_640, 100);
    private int e;
    private int f;

    dt(int i, int i2) {
        this.e = i;
        this.f = i2;
    }

    public int a() {
        return this.e;
    }

    public int b() {
        return this.f;
    }
}
