package net.youmi.android;

import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.PathShape;
import android.graphics.drawable.shapes.RectShape;
import com.adview.util.AdViewUtil;

class bx {
    bx() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    static Drawable a(bz bzVar) {
        try {
            ShapeDrawable shapeDrawable = new ShapeDrawable(new RectShape());
            shapeDrawable.getPaint().setShader(new LinearGradient(0.0f, 0.0f, 0.0f, (float) bzVar.b().a(), Color.rgb(60, 60, 60), Color.rgb(0, 0, 0), Shader.TileMode.CLAMP));
            return shapeDrawable;
        } catch (Exception e) {
            return new ColorDrawable(Color.argb(160, 0, 0, 0));
        }
    }

    static Drawable a(bz bzVar, int[] iArr, int[] iArr2) {
        try {
            float b = (float) bzVar.b().b();
            float b2 = (float) bzVar.b().b();
            float c = (float) bzVar.b().c();
            float c2 = (float) bzVar.b().c();
            float b3 = ((float) bzVar.b().b()) + c;
            float b4 = ((float) bzVar.b().b()) + c2;
            float f = b2 / 6.0f;
            float f2 = b2 / 5.0f;
            float f3 = b / 5.0f;
            float f4 = b / 6.0f;
            Path path = new Path();
            path.moveTo(c + f3, c2);
            path.lineTo(c + b, c2);
            path.lineTo(c + b, c2 + b2);
            path.lineTo(c + f3, c2 + b2);
            path.lineTo(c + f3, (c2 + b2) - f);
            path.lineTo((c + b) - f4, (c2 + b2) - f);
            path.lineTo((c + b) - f4, c2 + f);
            path.lineTo(c + f3, c2 + f);
            path.lineTo(f3 + c, c2);
            path.close();
            path.moveTo(c, (f2 * 2.0f) + c2);
            path.lineTo(c, (f2 * 3.0f) + c2);
            path.lineTo((f4 * 2.0f) + c, (f2 * 3.0f) + c2);
            path.lineTo((f4 * 2.0f) + c, (c2 + b2) - f);
            path.lineTo((b + c) - f4, (b2 / 2.0f) + c2);
            path.lineTo((f4 * 2.0f) + c, c2 + f);
            path.lineTo((f4 * 2.0f) + c, (f2 * 2.0f) + c2);
            path.lineTo(c, (f2 * 2.0f) + c2);
            path.close();
            PathShape pathShape = new PathShape(path, (float) bzVar.b().a(), (float) bzVar.b().a());
            ShapeDrawable shapeDrawable = new ShapeDrawable(pathShape);
            shapeDrawable.getPaint().setColor(Color.rgb(245, 245, 245));
            ShapeDrawable shapeDrawable2 = new ShapeDrawable(pathShape);
            shapeDrawable2.getPaint().setColor(Color.argb(200, 0, 191, (int) AdViewUtil.VERSION));
            return a(iArr, shapeDrawable, iArr2, shapeDrawable2);
        } catch (Exception e) {
            return null;
        }
    }

    static Drawable a(bz bzVar, int[] iArr, int[] iArr2, int[] iArr3) {
        try {
            float b = (float) bzVar.b().b();
            float b2 = (float) bzVar.b().b();
            float c = (float) bzVar.b().c();
            float c2 = (float) bzVar.b().c();
            float b3 = ((float) bzVar.b().b()) + c;
            Path path = new Path();
            path.moveTo(c, (b2 / 2.0f) + c2);
            path.lineTo(b3, c2);
            path.lineTo(b3, ((float) bzVar.b().b()) + c2);
            path.lineTo(c, (b2 / 2.0f) + c2);
            path.close();
            PathShape pathShape = new PathShape(path, (float) bzVar.b().a(), (float) bzVar.b().a());
            ShapeDrawable shapeDrawable = new ShapeDrawable(pathShape);
            shapeDrawable.getPaint().setColor(-12303292);
            ShapeDrawable shapeDrawable2 = new ShapeDrawable(pathShape);
            shapeDrawable2.getPaint().setColor(Color.rgb(245, 245, 245));
            ShapeDrawable shapeDrawable3 = new ShapeDrawable(pathShape);
            shapeDrawable3.getPaint().setColor(Color.argb(200, 0, 191, (int) AdViewUtil.VERSION));
            return a(iArr, shapeDrawable, iArr2, shapeDrawable2, iArr3, shapeDrawable3);
        } catch (Exception e) {
            return null;
        }
    }

    static Drawable a(int[] iArr, Drawable drawable, int[] iArr2, Drawable drawable2) {
        try {
            StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.addState(iArr2, drawable2);
            stateListDrawable.addState(iArr, drawable);
            return stateListDrawable;
        } catch (Exception e) {
            return null;
        }
    }

    static Drawable a(int[] iArr, Drawable drawable, int[] iArr2, Drawable drawable2, int[] iArr3, Drawable drawable3) {
        try {
            StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.addState(iArr3, drawable3);
            stateListDrawable.addState(iArr2, drawable2);
            stateListDrawable.addState(iArr, drawable);
            return stateListDrawable;
        } catch (Exception e) {
            return null;
        }
    }

    static Drawable b(bz bzVar, int[] iArr, int[] iArr2) {
        try {
            float b = (float) bzVar.b().b();
            float b2 = (float) bzVar.b().b();
            float c = (float) bzVar.b().c();
            float c2 = (float) bzVar.b().c();
            float b3 = ((float) bzVar.b().b()) + c;
            float b4 = ((float) bzVar.b().b()) + c2;
            Path path = new Path();
            path.moveTo(c, b4);
            path.lineTo(b3, b4);
            path.lineTo(b3, (0.75f * b2) + c2);
            path.lineTo(c, (0.75f * b2) + c2);
            path.lineTo(c, b4);
            path.close();
            path.moveTo((b / 2.0f) + c, (b2 * 0.7f) + c2);
            path.lineTo((0.9f * b) + c, (b2 * 0.35f) + c2);
            path.lineTo((b * 0.6f) + c, (b2 * 0.35f) + c2);
            path.lineTo((b * 0.6f) + c, c2);
            path.lineTo((b * 0.4f) + c, c2);
            path.lineTo((b * 0.4f) + c, (b2 * 0.35f) + c2);
            path.lineTo((0.1f * b) + c, (b2 * 0.35f) + c2);
            path.lineTo((b / 2.0f) + c, (b2 * 0.7f) + c2);
            path.close();
            PathShape pathShape = new PathShape(path, (float) bzVar.b().a(), (float) bzVar.b().a());
            ShapeDrawable shapeDrawable = new ShapeDrawable(pathShape);
            shapeDrawable.getPaint().setColor(Color.rgb(245, 245, 245));
            ShapeDrawable shapeDrawable2 = new ShapeDrawable(pathShape);
            shapeDrawable2.getPaint().setColor(Color.argb(200, 0, 191, (int) AdViewUtil.VERSION));
            return a(iArr, shapeDrawable, iArr2, shapeDrawable2);
        } catch (Exception e) {
            return null;
        }
    }

    static Drawable b(bz bzVar, int[] iArr, int[] iArr2, int[] iArr3) {
        try {
            bzVar.b().b();
            int b = bzVar.b().b();
            int c = bzVar.b().c();
            int c2 = bzVar.b().c();
            Path path = new Path();
            path.moveTo((float) c, (float) c2);
            path.lineTo((float) (bzVar.b().b() + c), (float) ((b / 2) + c2));
            path.lineTo((float) c, (float) (bzVar.b().b() + c2));
            path.lineTo((float) c, (float) c2);
            path.close();
            PathShape pathShape = new PathShape(path, (float) bzVar.b().a(), (float) bzVar.b().a());
            ShapeDrawable shapeDrawable = new ShapeDrawable(pathShape);
            shapeDrawable.getPaint().setColor(-12303292);
            ShapeDrawable shapeDrawable2 = new ShapeDrawable(pathShape);
            shapeDrawable2.getPaint().setColor(Color.rgb(245, 245, 245));
            ShapeDrawable shapeDrawable3 = new ShapeDrawable(pathShape);
            shapeDrawable3.getPaint().setColor(Color.argb(200, 0, 191, (int) AdViewUtil.VERSION));
            return a(iArr, shapeDrawable, iArr2, shapeDrawable2, iArr3, shapeDrawable3);
        } catch (Exception e) {
            return null;
        }
    }

    static Drawable c(bz bzVar, int[] iArr, int[] iArr2, int[] iArr3) {
        try {
            float b = (float) bzVar.b().b();
            float b2 = (float) bzVar.b().b();
            float c = (float) bzVar.b().c();
            float c2 = (float) bzVar.b().c();
            float f = 0.16f * b2;
            Path path = new Path();
            RectF rectF = new RectF(c, c2, ((float) bzVar.b().b()) + c, ((float) bzVar.b().b()) + c2);
            RectF rectF2 = new RectF(c + f, c2 + f, (c + b) - f, (c2 + b2) - f);
            path.moveTo(c + b, (0.5f * b2) + c2);
            path.arcTo(rectF, 0.0f, -150.0f);
            path.lineTo(c, (0.15f * b2) + c2);
            path.lineTo(c, (0.5f * b2) + c2);
            path.lineTo((0.4f * b) + c, (0.5f * b2) + c2);
            path.lineTo((0.2f * b) + c, (0.4f * b2) + c2);
            path.arcTo(rectF2, 190.0f, 160.0f);
            path.lineTo(c + b, (0.5f * b2) + c2);
            path.close();
            path.moveTo(c, (0.5f * b2) + c2);
            path.arcTo(rectF, 180.0f, -150.0f);
            path.lineTo(c + b, (0.85f * b2) + c2);
            path.lineTo(c + b, (0.5f * b2) + c2);
            path.lineTo((0.6f * b) + c, (0.5f * b2) + c2);
            path.lineTo((b * 0.8f) + c, (0.6f * b2) + c2);
            path.arcTo(rectF2, 10.0f, 160.0f);
            path.lineTo(c, (0.5f * b2) + c2);
            path.close();
            PathShape pathShape = new PathShape(path, (float) bzVar.b().a(), (float) bzVar.b().a());
            ShapeDrawable shapeDrawable = new ShapeDrawable(pathShape);
            shapeDrawable.getPaint().setColor(-12303292);
            ShapeDrawable shapeDrawable2 = new ShapeDrawable(pathShape);
            shapeDrawable2.getPaint().setColor(Color.rgb(245, 245, 245));
            shapeDrawable2.getPaint().setAntiAlias(true);
            ShapeDrawable shapeDrawable3 = new ShapeDrawable(pathShape);
            shapeDrawable3.getPaint().setColor(Color.argb(200, 0, 191, (int) AdViewUtil.VERSION));
            return a(iArr, shapeDrawable, iArr2, shapeDrawable2, iArr3, shapeDrawable3);
        } catch (Exception e) {
            return null;
        }
    }
}
