package net.youmi.android;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

abstract class dg extends df {
    protected eb h;

    dg(eb ebVar) {
        this.h = ebVar;
    }

    /* access modifiers changed from: protected */
    public int a() {
        if (!Cdo.c(this.e)) {
            return 1;
        }
        return !r.b(this.e) ? 2 : 0;
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(File file);

    /* access modifiers changed from: protected */
    public abstract boolean a(byte[] bArr);

    /* access modifiers changed from: protected */
    public int b() {
        a(0);
        try {
            if (this.h != null) {
                File file = new File(this.h.c(this.a));
                if (file.exists() && a(file)) {
                    this.f = 6;
                    h();
                    return this.f;
                }
            }
        } catch (Exception e) {
            f.a(e);
        }
        HttpResponse execute = f().execute(new HttpGet(this.a));
        if (execute.getStatusLine().getStatusCode() == 200) {
            HttpEntity entity = execute.getEntity();
            this.c = entity.getContentLength();
            InputStream content = entity.getContent();
            if (content == null) {
                this.f = 7;
                h();
                return this.f;
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream((int) this.c);
            byte[] bArr = new byte[1024];
            while (true) {
                int read = content.read(bArr);
                if (read <= 0) {
                    break;
                }
                this.f = 5;
                this.d += (long) read;
                byteArrayOutputStream.write(bArr, 0, read);
            }
            this.f = 7;
            if (byteArrayOutputStream.size() > 0) {
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                try {
                    if (this.h != null) {
                        this.h.a(this.e, this.a, byteArray);
                    }
                } catch (Exception e2) {
                }
                try {
                    if (a(byteArray)) {
                        this.f = 6;
                    } else {
                        this.f = 7;
                    }
                } catch (Exception e3) {
                    this.f = 4;
                    h();
                    return this.f;
                }
            }
            try {
                content.close();
            } catch (Exception e4) {
            }
            try {
                byteArrayOutputStream.close();
            } catch (Exception e5) {
            }
            h();
            return this.f;
        }
        this.f = 7;
        h();
        return this.f;
    }
}
