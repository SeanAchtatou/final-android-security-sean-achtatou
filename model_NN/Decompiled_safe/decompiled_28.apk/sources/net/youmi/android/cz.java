package net.youmi.android;

import android.app.Activity;
import android.text.TextUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

class cz extends RelativeLayout {
    TextView a;
    TextView b;
    final /* synthetic */ cw c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cz(cw cwVar, Activity activity, int i, bz bzVar, int i2) {
        super(activity);
        this.c = cwVar;
        this.a = new TextView(activity);
        this.a.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        this.a.setFocusable(true);
        this.a.setFocusableInTouchMode(true);
        this.a.setMarqueeRepeatLimit(-1);
        this.a.setTextColor(i);
        this.a.setId(1018);
        this.a.setTextSize(bzVar.i().b());
        this.a.setSingleLine(true);
        this.a.setWidth(i2);
        this.b = new TextView(activity);
        this.b.setSingleLine(true);
        this.b.setTextColor(i);
        this.b.setTextSize(bzVar.i().c());
        this.b.setId(1019);
        RelativeLayout.LayoutParams b2 = h.b(i2);
        RelativeLayout.LayoutParams b3 = h.b(i2);
        b2.addRule(10);
        b2.addRule(9);
        b3.addRule(3, this.a.getId());
        b3.addRule(9);
        addView(this.a, b2);
        addView(this.b, b3);
    }

    /* access modifiers changed from: package-private */
    public boolean a(ct ctVar) {
        if (ctVar == null) {
            return false;
        }
        try {
            if (ctVar.b() != 1 && ctVar.b() != 0) {
                return false;
            }
            String c2 = ctVar.c();
            String d = ctVar.d();
            if (c2 != null) {
                c2 = c2.trim();
            }
            if (c2.length() == 0) {
                c2 = null;
            }
            if (d != null) {
                d = d.trim();
            }
            if (d.length() == 0) {
                d = null;
            }
            if (c2 == null && d == null) {
                return false;
            }
            if (c2 == null || d == null) {
                if (c2 == null) {
                    c2 = d;
                }
                this.a.setMaxLines(2);
                this.a.setSingleLine(false);
                this.a.setGravity(17);
                this.a.setText(c2);
                this.b.setVisibility(8);
                return true;
            }
            this.a.setSingleLine(true);
            this.b.setLines(1);
            this.b.setVisibility(0);
            this.a.setText(c2);
            this.b.setText(d);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
