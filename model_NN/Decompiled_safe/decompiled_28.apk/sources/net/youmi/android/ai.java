package net.youmi.android;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;

class ai {
    private static Animation a;
    private static Animation b;
    private static Animation c;
    private static Animation d;

    ai() {
    }

    private static Animation a(int i) {
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) i);
        translateAnimation.setDuration(300);
        animationSet.addAnimation(translateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(300);
        animationSet.addAnimation(alphaAnimation);
        return animationSet;
    }

    static Animation a(bz bzVar) {
        if (c == null) {
            c = e(bzVar);
        }
        return c;
    }

    static Animation b(bz bzVar) {
        if (d == null) {
            d = a(bzVar.a().a());
        }
        return d;
    }

    static Animation c(bz bzVar) {
        if (a == null) {
            a = f(bzVar);
        }
        return a;
    }

    static Animation d(bz bzVar) {
        if (b == null) {
            b = g(bzVar);
        }
        return b;
    }

    private static Animation e(bz bzVar) {
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) (-bzVar.a().a()), 0.0f);
        translateAnimation.setDuration(300);
        animationSet.addAnimation(translateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(300);
        animationSet.addAnimation(alphaAnimation);
        return animationSet;
    }

    private static Animation f(bz bzVar) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(600);
        return alphaAnimation;
    }

    private static Animation g(bz bzVar) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(600);
        return alphaAnimation;
    }
}
