package net.youmi.android;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;

class c extends AsyncTask {
    Context a;

    c(Context context) {
        this.a = context;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public dk doInBackground(String... strArr) {
        try {
            return au.a(this.a);
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(dk dkVar) {
        super.onPostExecute(dkVar);
        if (dkVar != null) {
            try {
                if (dkVar.e == null) {
                    if (dkVar.b == null) {
                        dkVar.e = "是否更新到最新版本?";
                    } else {
                        dkVar.e = "是否将版本更新到最新的" + dkVar.b + "?";
                    }
                }
                new AlertDialog.Builder(this.a).setTitle("应用程序有新版本更新").setMessage(dkVar.e).setCancelable(false).setNegativeButton("以后再说", new da(this)).setPositiveButton("立即更新", new db(this, dkVar)).create().show();
            } catch (Exception e) {
            }
        }
    }
}
