package net.youmi.android;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;
import com.adview.util.AdViewUtil;

class di extends View {
    RectF a = new RectF();
    int b;
    int c;
    RectF d = new RectF();
    Paint e = new Paint();
    float f = 3.0f;
    int g = 0;
    bz h;
    float i = 1.0f;

    di(Context context, bz bzVar) {
        super(context);
        this.h = bzVar;
        this.f = this.h.a(this.f);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (i2 >= 0 && i2 <= 100) {
            this.g = i2;
            postInvalidate();
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        try {
            int i2 = getLayoutParams().width;
            int a2 = this.h.a(10);
            getLayoutParams().height = a2;
            if (i2 <= 0) {
                i2 = this.h.d();
            }
            if (i2 > this.h.d()) {
                i2 = this.h.d();
            }
            this.b = i2;
            this.c = a2;
            this.a.set(0.0f, 0.0f, (float) i2, (float) a2);
            this.i = ((float) this.b) / 100.0f;
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.e.reset();
        this.e.setARGB(100, 180, 180, 180);
        this.e.setStyle(Paint.Style.STROKE);
        this.e.setStrokeWidth(this.f);
        canvas.drawRect(this.a, this.e);
        this.d.set(this.f, this.f, (this.i * ((float) this.g)) - this.f, ((float) this.c) - this.f);
        this.e.reset();
        this.e.setARGB(AdViewUtil.VERSION, 65, 105, 225);
        canvas.drawRect(this.d, this.e);
    }
}
