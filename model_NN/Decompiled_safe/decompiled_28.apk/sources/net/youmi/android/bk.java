package net.youmi.android;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

class bk extends df {
    private String h;
    private String i;
    private List j;
    private boolean k;

    bk() {
        this.k = false;
        this.k = false;
    }

    /* access modifiers changed from: protected */
    public int a() {
        if (!Cdo.c(this.e)) {
            return 1;
        }
        return !r.b(this.e) ? 2 : 0;
    }

    /* access modifiers changed from: protected */
    public int b() {
        HttpPost httpGet;
        DefaultHttpClient defaultHttpClient = null;
        try {
            defaultHttpClient = f();
        } catch (Exception e) {
        }
        if (defaultHttpClient == null) {
            this.f = 4;
            h();
            return this.f;
        }
        try {
            a(0);
            if (this.k) {
                httpGet = new HttpPost(this.a);
                httpGet.setEntity(new UrlEncodedFormEntity(this.j, "utf-8"));
            } else {
                httpGet = new HttpGet(this.a);
            }
            HttpResponse execute = defaultHttpClient.execute(httpGet);
            if (execute.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = execute.getEntity();
                this.c = entity.getContentLength();
                this.i = EntityUtils.getContentCharSet(entity);
                InputStream content = entity.getContent();
                byte[] bArr = new byte[1024];
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                while (true) {
                    int read = content.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                    a((long) byteArrayOutputStream.size());
                }
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                if (this.i == null) {
                    this.i = "utf-8";
                    this.h = new String(byteArray, "utf-8");
                } else {
                    this.h = new String(byteArray, this.i);
                }
                try {
                    content.close();
                } catch (Exception e2) {
                }
                try {
                    byteArrayOutputStream.close();
                } catch (Exception e3) {
                }
                try {
                    defaultHttpClient.getConnectionManager().shutdown();
                } catch (Exception e4) {
                }
                this.f = 6;
                h();
                return this.f;
            }
            try {
                defaultHttpClient.getConnectionManager().shutdown();
            } catch (Exception e5) {
            }
            this.f = 7;
            h();
            return this.f;
        } catch (Exception e6) {
            f.a(e6);
            try {
                defaultHttpClient.getConnectionManager().shutdown();
            } catch (Exception e7) {
                f.a(e7);
            }
            this.f = 4;
            h();
            return this.f;
        }
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.h;
    }
}
