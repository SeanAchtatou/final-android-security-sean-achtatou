package net.youmi.android;

import android.app.Activity;
import android.os.Handler;
import android.widget.FrameLayout;

class cw extends FrameLayout {
    /* access modifiers changed from: private */
    public ct a;
    /* access modifiers changed from: private */
    public af b;
    /* access modifiers changed from: private */
    public dy c;
    /* access modifiers changed from: private */
    public as d;
    /* access modifiers changed from: private */
    public l e;
    /* access modifiers changed from: private */
    public bl f;
    /* access modifiers changed from: private */
    public ba g;
    private AdView h;
    /* access modifiers changed from: private */
    public fb i;
    /* access modifiers changed from: private */
    public bz j;
    private Activity k;
    /* access modifiers changed from: private */
    public int l = this.h.getAdWidth();
    /* access modifiers changed from: private */
    public int m = this.h.getAdHeight();

    public cw(Activity activity, AdView adView, fb fbVar, bz bzVar, int i2, int i3) {
        super(activity);
        this.j = bzVar;
        this.k = activity;
        this.h = adView;
        this.h = adView;
        this.i = fbVar;
        a(i2, i3, adView.d());
    }

    private void a(int i2, int i3, int i4) {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.l, this.m);
        this.b = new af(this, this.k, this.j, i2, i3);
        addView(this.b);
        this.b.setVisibility(8);
        this.c = new dy(this, this.k, this.j, false);
        this.c.setBackgroundColor(-16776961);
        addView(this.c, layoutParams);
        this.c.setVisibility(8);
        this.d = new as(this, this.k, this.j, i2, i3);
        addView(this.d, layoutParams);
        this.d.setVisibility(8);
        this.e = new l(this, this.k, i4);
        addView(this.e, layoutParams);
        this.e.setVisibility(8);
        this.f = new bl(this, this.k, this.h);
        addView(this.f, layoutParams);
        this.f.setVisibility(8);
    }

    /* access modifiers changed from: package-private */
    public void a(ct ctVar) {
        try {
            this.a = ctVar;
            Handler handler = getHandler();
            if (handler != null) {
                handler.post(new e(this));
            }
        } catch (Exception e2) {
            f.a(e2);
        }
    }
}
