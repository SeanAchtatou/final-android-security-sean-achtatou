package net.youmi.android;

import android.app.Activity;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import java.util.ArrayList;

class cn extends LinearLayout {
    Activity a;
    bz b;
    boolean c = false;
    boolean d = false;
    ey e;
    dn f;
    ArrayList g;
    ArrayList h;
    ExpandableListView i;
    Runnable j = new ab(this);

    public cn(Activity activity, bz bzVar) {
        super(activity);
        this.a = activity;
        this.b = bzVar;
        this.h = q.b();
        this.g = q.a();
        b();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.h = q.b();
        this.g = q.a();
        if (this.g == null) {
            q.e(this.a);
        } else if (this.g.size() == 0) {
            q.e(this.a);
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.i = new ExpandableListView(this.a);
        this.f = new dn(this, this.a, this, this.b, this.i, this.h, this.g);
        this.i.setAdapter(this.f);
        addView(this.i, new ViewGroup.LayoutParams(-1, -1));
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.e = new ey(new ac(this), -1);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        try {
            this.e.c();
        } catch (Exception e2) {
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            try {
                this.e.b();
            } catch (Exception e2) {
            }
        } else {
            this.e.a();
        }
    }
}
