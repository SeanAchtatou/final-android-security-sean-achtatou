package net.youmi.android;

import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.ArcShape;
import com.adview.util.AdViewUtil;

class dp {
    final /* synthetic */ cc a;
    private Drawable b;
    private Drawable c;
    private Drawable d;
    private Drawable e;
    private Drawable f;

    dp(cc ccVar) {
        this.a = ccVar;
    }

    /* access modifiers changed from: package-private */
    public Drawable a() {
        if (this.b == null) {
            this.b = a(a("last.png"), a("last-enable.png"));
        }
        return this.b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    /* access modifiers changed from: package-private */
    public Drawable a(Drawable drawable, Drawable drawable2) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        ShapeDrawable shapeDrawable = new ShapeDrawable();
        shapeDrawable.setShape(new ArcShape(0.0f, 360.0f));
        shapeDrawable.getPaint().setShader(new RadialGradient(35.0f, 25.0f, 25.0f, Color.argb(80, (int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION), Color.argb(0, (int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION), Shader.TileMode.CLAMP));
        stateListDrawable.addState(cc.PRESSED_ENABLED_STATE_SET, new LayerDrawable(new Drawable[]{shapeDrawable, drawable}));
        if (drawable != null) {
            stateListDrawable.addState(cc.ENABLED_STATE_SET, drawable);
        }
        if (drawable2 != null) {
            stateListDrawable.addState(cc.EMPTY_STATE_SET, drawable2);
        }
        return stateListDrawable;
    }

    /* access modifiers changed from: package-private */
    public Drawable a(String str) {
        try {
            return new BitmapDrawable(BitmapFactory.decodeStream(getClass().getResourceAsStream(str)));
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public Drawable b() {
        if (this.c == null) {
            this.c = a(a("next.png"), a("next-enable.png"));
        }
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public Drawable c() {
        if (this.f == null) {
            this.f = a(a("download.png"), null);
        }
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public Drawable d() {
        if (this.e == null) {
            this.e = a(a("quit.png"), null);
        }
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public Drawable e() {
        if (this.d == null) {
            this.d = a(a("reflesh.png"), a("reflesh.png"));
        }
        return this.d;
    }
}
