package net.youmi.android;

import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import java.util.Hashtable;

class k {
    static Hashtable a;

    k() {
    }

    static bv a(Context context, String str, String str2, String str3, String str4, String str5) {
        try {
            PackageInfo packageArchiveInfo = context.getPackageManager().getPackageArchiveInfo(str, 1);
            if (packageArchiveInfo != null) {
                bv bvVar = new bv();
                bvVar.a = str5;
                bvVar.b = str2;
                bvVar.d = str4;
                bvVar.c = str3;
                cs csVar = new cs();
                csVar.a = packageArchiveInfo.packageName;
                csVar.c = packageArchiveInfo.versionCode;
                csVar.b = packageArchiveInfo.versionName;
                bvVar.f = csVar;
                try {
                    PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageArchiveInfo.packageName, 0);
                    if (packageInfo != null) {
                        cs csVar2 = new cs();
                        csVar2.a = packageInfo.packageName;
                        csVar2.c = packageInfo.versionCode;
                        csVar2.b = packageInfo.versionName;
                        bvVar.e = csVar2;
                    }
                } catch (Exception e) {
                }
                if (a == null) {
                    a = new Hashtable(20);
                }
                if (!a.containsKey(packageArchiveInfo.packageName)) {
                    a.put(packageArchiveInfo.packageName, bvVar);
                }
                return bvVar;
            }
        } catch (Exception e2) {
        }
        return null;
    }

    static void a(Context context) {
        try {
            IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
            intentFilter.addAction("android.intent.action.PACKAGE_INSTALL");
            intentFilter.addDataScheme("package");
            context.registerReceiver(new eq(), intentFilter);
        } catch (Exception e) {
        }
    }

    static void a(Context context, String str, String str2) {
        bv bvVar;
        if (str != null && str2 != null) {
            try {
                if (a != null && a.containsKey(str) && (bvVar = (bv) a.get(str)) != null && str2.equals("android.intent.action.PACKAGE_ADDED")) {
                    try {
                        PackageInfo packageInfo = context.getPackageManager().getPackageInfo(str, 0);
                        if (packageInfo != null) {
                            cs csVar = new cs();
                            csVar.a = packageInfo.packageName;
                            csVar.c = packageInfo.versionCode;
                            csVar.b = packageInfo.versionName;
                            bvVar.g = csVar;
                            a(context, bvVar, 4);
                        }
                    } catch (Exception e) {
                        f.a(e);
                    }
                }
            } catch (Exception e2) {
                try {
                    f.a(e2);
                } catch (Exception e3) {
                    f.a(e3);
                }
            }
        }
    }

    static void a(Context context, bv bvVar, int i) {
        try {
            new Thread(new ep(context, bvVar, i)).start();
        } catch (Exception e) {
            f.a(e);
        }
    }

    static void b(Context context, bv bvVar, int i) {
        String str;
        int i2;
        String str2;
        int i3;
        String str3;
        String str4;
        int i4;
        if (bvVar != null) {
            int i5 = bvVar.f != null ? bvVar.e != null ? 1 : 0 : 2;
            String str5 = null;
            if (bvVar.e != null) {
                int i6 = bvVar.e.c;
                str = bvVar.e.b;
                int i7 = i6;
                str5 = bvVar.e.a;
                i2 = i7;
            } else {
                str = null;
                i2 = 0;
            }
            if (bvVar.f != null) {
                int i8 = bvVar.f.c;
                str2 = bvVar.f.b;
                int i9 = i8;
                str5 = bvVar.f.a;
                i3 = i9;
            } else {
                str2 = null;
                i3 = 0;
            }
            if (bvVar.g != null) {
                int i10 = bvVar.g.c;
                str3 = bvVar.g.b;
                str4 = bvVar.g.a;
                i4 = i10;
            } else {
                str3 = null;
                str4 = str5;
                i4 = 0;
            }
            au.a(context, i, i5, i2, i3, i4, bvVar.a, str4, str, str2, str3, bvVar.c, bvVar.d, bvVar.b);
        }
    }
}
