package net.youmi.android;

import android.app.Activity;
import android.view.animation.Animation;
import android.widget.FrameLayout;

class de extends FrameLayout implements ba {
    cz a;
    cz b;
    cz c;
    Animation d;
    Animation e;
    int f;
    final /* synthetic */ cw g;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public de(cw cwVar, Activity activity, int i, bz bzVar, int i2) {
        super(activity);
        this.g = cwVar;
        this.f = i2;
        this.a = new cz(cwVar, activity, i, bzVar, i2);
        this.a.setGravity(17);
        this.b = new cz(cwVar, activity, i, bzVar, i2);
        this.b.setGravity(17);
        this.a.setVisibility(8);
        this.b.setVisibility(8);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(i2, -2);
        layoutParams.gravity = 16;
        layoutParams.leftMargin = bzVar.a(10);
        addView(this.a, layoutParams);
        addView(this.b, layoutParams);
        this.d = ai.a(bzVar);
        this.e = ai.b(bzVar);
    }

    public void a() {
        setVisibility(8);
    }

    public void a(Animation animation) {
        try {
            a(animation);
        } catch (Exception e2) {
        }
    }

    public boolean a(ct ctVar) {
        try {
            cz e2 = e();
            if (e2 != null) {
                return e2.a(ctVar);
            }
            return false;
        } catch (Exception e3) {
            return false;
        }
    }

    public void b() {
        try {
            cz e2 = e();
            if (e2 != null) {
                if (this.c != null) {
                    this.c.setVisibility(8);
                }
                e2.setVisibility(0);
                this.c = e2;
                c();
            }
        } catch (Exception e3) {
            f.a(e3);
        }
    }

    public void c() {
        setVisibility(0);
    }

    public void d() {
        cz e2 = e();
        if (e2 != null) {
            if (this.c != null) {
                this.c.startAnimation(this.e);
                this.c.setVisibility(8);
            }
            e2.setVisibility(0);
            e2.startAnimation(this.d);
            this.c = e2;
        }
    }

    /* access modifiers changed from: package-private */
    public cz e() {
        return this.a == this.c ? this.b : this.a;
    }
}
