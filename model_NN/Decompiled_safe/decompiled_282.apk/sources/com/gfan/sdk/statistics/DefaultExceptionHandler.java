package com.gfan.sdk.statistics;

import android.content.Context;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread;

public class DefaultExceptionHandler implements Thread.UncaughtExceptionHandler {
    private Context context;
    private Thread.UncaughtExceptionHandler defaultExceptionHandler;

    public DefaultExceptionHandler(Thread.UncaughtExceptionHandler pDefaultExceptionHandler, Context context2) {
        this.defaultExceptionHandler = pDefaultExceptionHandler;
        this.context = context2;
    }

    public void uncaughtException(Thread t, Throwable e) {
        Writer result = new StringWriter();
        e.printStackTrace(new PrintWriter(result));
        try {
            BufferedWriter bos = new BufferedWriter(new FileWriter(String.valueOf(this.context.getFilesDir().getAbsolutePath()) + "/" + new StringBuilder(String.valueOf(System.currentTimeMillis())).toString() + ".stacktrace"));
            bos.write(result.toString());
            bos.flush();
            bos.close();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.defaultExceptionHandler.uncaughtException(t, e);
    }
}
