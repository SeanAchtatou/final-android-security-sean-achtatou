package com.mobclix.android.sdk;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import com.mobclix.android.sdk.MobclixLocation;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

public final class Mobclix {
    static final boolean DEBUG = false;
    public static final int LOG_LEVEL_DEBUG = 1;
    public static final int LOG_LEVEL_ERROR = 8;
    public static final int LOG_LEVEL_FATAL = 16;
    public static final int LOG_LEVEL_INFO = 2;
    public static final int LOG_LEVEL_WARN = 4;
    /* access modifiers changed from: private */
    public static final String[] MC_AD_SIZES = {"320x50", "300x250"};
    /* access modifiers changed from: private */
    public static String MC_ANALYTICS_DIRECTORY = "analytics";
    static final String MC_CUSTOM_AD_FILENAME = "_mc_cached_custom_ad.png";
    static final String MC_CUSTOM_AD_PREF = "CustomAdUrl";
    /* access modifiers changed from: private */
    public static String MC_DIRECTORY = "mobclix";
    private static final String MC_KEY_APPLICATION_ID = "a";
    private static final String MC_KEY_APPLICATION_VERSION = "v";
    private static final String MC_KEY_CONNECTION_TYPE = "g";
    private static final String MC_KEY_DEVICE_HARDWARE_MODEL = "hwdm";
    private static final String MC_KEY_DEVICE_ID = "d";
    private static final String MC_KEY_DEVICE_MODEL = "dm";
    private static final String MC_KEY_DEVICE_SYSTEM_VERSION = "dv";
    private static final String MC_KEY_EVENT_DESCRIPTION = "ed";
    private static final String MC_KEY_EVENT_LOG_LEVEL = "el";
    private static final String MC_KEY_EVENT_NAME = "en";
    private static final String MC_KEY_EVENT_PROCESS_NAME = "ep";
    private static final String MC_KEY_EVENT_STOP = "es";
    private static final String MC_KEY_EVENT_THREAD_ID = "et";
    private static final String MC_KEY_LATITUDE_LONGITUDE = "ll";
    private static final String MC_KEY_MOBCLIX_LIBRARY_VERSION = "m";
    private static final String MC_KEY_PLATFORM_ID = "p";
    private static final String MC_KEY_SESSION_ID = "id";
    private static final String MC_KEY_TIMESTAMP = "ts";
    private static final String MC_KEY_USER_LANGUAGE_PREFERENCE = "lg";
    private static final String MC_KEY_USER_LOCALE_PREFERENCE = "lo";
    private static final String MC_LIBRARY_VERSION = "2.1.0";
    private static int MC_MAX_ANALYTICS_FILES = 100;
    /* access modifiers changed from: private */
    public static int MC_MAX_EVENTS_PER_FILE = 5;
    private static final String MC_TAG = "mobclix-controller";
    static final String PREFS_CONFIG = ".MCConfig";
    /* access modifiers changed from: private */
    public static int SYNC_ERROR = -1;
    /* access modifiers changed from: private */
    public static int SYNC_READY = 0;
    /* access modifiers changed from: private */
    public static int SYNC_RUNNING = 1;
    /* access modifiers changed from: private */
    public static final Mobclix controller = new Mobclix();
    /* access modifiers changed from: private */
    public static File currentFile = null;
    /* access modifiers changed from: private */
    public static boolean fileCreated = DEBUG;
    private static boolean isInitialized = DEBUG;
    /* access modifiers changed from: private */
    public static boolean loggingEvent = DEBUG;
    /* access modifiers changed from: private */
    public static int numLinesWritten = 0;
    /* access modifiers changed from: private */
    public static String syncContents = null;
    /* access modifiers changed from: private */
    public static int syncStatus = 0;
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public String adServer = "http://ads.mobclix.com/";
    /* access modifiers changed from: private */
    public String analyticsServer = "http://data.mobclix.com/post/sendData";
    private String androidId;
    /* access modifiers changed from: private */
    public String androidVersion;
    private String applicationId = "null";
    /* access modifiers changed from: private */
    public String applicationVersion;
    /* access modifiers changed from: private */
    public HashMap<String, Boolean> autoplay = new HashMap<>();
    /* access modifiers changed from: private */
    public String configServer = "http://data.mobclix.com/post/config";
    /* access modifiers changed from: private */
    public String connectionType = "null";
    /* access modifiers changed from: private */
    public HashMap<String, Boolean> customAdSet = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<String, String> customAdUrl = new HashMap<>();
    private String deviceHardwareModel;
    /* access modifiers changed from: private */
    public String deviceId;
    private String deviceModel;
    /* access modifiers changed from: private */
    public HashMap<String, Boolean> enabled = new HashMap<>();
    /* access modifiers changed from: private */
    public String feedbackServer = "http://data.mobclix.com/post/feedback";
    /* access modifiers changed from: private */
    public boolean haveLocationPermission = DEBUG;
    /* access modifiers changed from: private */
    public boolean haveNetworkStatePermission = DEBUG;
    /* access modifiers changed from: private */
    public int idleTimeout = 120000;
    private boolean isInSession = DEBUG;
    /* access modifiers changed from: private */
    public boolean isNewUser = DEBUG;
    /* access modifiers changed from: private */
    public boolean isOfflineSession = DEBUG;
    private boolean isTopTask = DEBUG;
    /* access modifiers changed from: private */
    public String language = "null";
    /* access modifiers changed from: private */
    public String latitude = "null";
    /* access modifiers changed from: private */
    public String locale = "null";
    private MobclixLocation location = new MobclixLocation();
    /* access modifiers changed from: private */
    public Criteria locationCriteria;
    private Handler locationHandler;
    private int logLevel = 16;
    /* access modifiers changed from: private */
    public String longitude = "null";
    /* access modifiers changed from: private */
    public String mcc = "null";
    /* access modifiers changed from: private */
    public String mnc = "null";
    /* access modifiers changed from: private */
    public int pollTime = 30000;
    /* access modifiers changed from: private */
    public String previousDeviceId = null;
    /* access modifiers changed from: private */
    public HashMap<String, Long> refreshTime = new HashMap<>();
    /* access modifiers changed from: private */
    public int remoteConfigSet = 0;
    private JSONObject session = new JSONObject();
    private long sessionEndTime = 0;
    private Timer sessionPollingTimer = new Timer();
    private long sessionStartTime = 0;
    private SharedPreferences sharedPrefs = null;
    private long totalIdleTime = 0;
    private String userAgent = "";
    /* access modifiers changed from: private */
    public String vcServer = "http://vc.mobclix.com";

    /* access modifiers changed from: package-private */
    public void setContext(Activity a) {
        this.activity = a;
    }

    /* access modifiers changed from: package-private */
    public Activity getContext() {
        return this.activity;
    }

    /* access modifiers changed from: package-private */
    public String getApplicationId() {
        return this.applicationId == null ? "null" : this.applicationId;
    }

    /* access modifiers changed from: package-private */
    public String getAndroidVersion() {
        return this.androidVersion == null ? "null" : this.androidVersion;
    }

    /* access modifiers changed from: package-private */
    public String getApplicationVersion() {
        return this.applicationVersion == null ? "null" : this.applicationVersion;
    }

    /* access modifiers changed from: package-private */
    public String getDeviceId() {
        return this.deviceId == null ? "null" : this.deviceId;
    }

    /* access modifiers changed from: package-private */
    public String getAndroidId() {
        return this.androidId == null ? "null" : this.androidId;
    }

    /* access modifiers changed from: package-private */
    public String getDeviceModel() {
        return this.deviceModel == null ? "null" : this.deviceModel;
    }

    /* access modifiers changed from: package-private */
    public String getDeviceHardwareModel() {
        return this.deviceHardwareModel == null ? "null" : this.deviceHardwareModel;
    }

    /* access modifiers changed from: package-private */
    public String getConnectionType() {
        return this.connectionType == null ? "null" : this.connectionType;
    }

    /* access modifiers changed from: package-private */
    public String getLatitude() {
        return this.latitude == null ? "null" : this.latitude;
    }

    /* access modifiers changed from: package-private */
    public String getLongitude() {
        return this.longitude == null ? "null" : this.longitude;
    }

    /* access modifiers changed from: package-private */
    public String getGPS() {
        if (getLatitude().equals("null") || getLongitude().equals("null")) {
            return "null";
        }
        return String.valueOf(getLatitude()) + "," + getLongitude();
    }

    /* access modifiers changed from: package-private */
    public String getLanguage() {
        return this.language == null ? "null" : this.language;
    }

    /* access modifiers changed from: package-private */
    public String getLocale() {
        return this.locale == null ? "null" : this.locale;
    }

    /* access modifiers changed from: package-private */
    public String getMobclixVersion() {
        return MC_LIBRARY_VERSION;
    }

    /* access modifiers changed from: package-private */
    public int getLogLevel() {
        return this.logLevel;
    }

    /* access modifiers changed from: package-private */
    public boolean isTopTask() {
        return this.isTopTask;
    }

    /* access modifiers changed from: package-private */
    public boolean isEnabled(String size) {
        try {
            return this.enabled.get(size).booleanValue();
        } catch (Exception e) {
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public long getRefreshTime(String size) {
        try {
            return this.refreshTime.get(size).longValue();
        } catch (Exception e) {
            return -1;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean shouldAutoplay(String size) {
        try {
            return this.autoplay.get(size).booleanValue();
        } catch (Exception e) {
            return DEBUG;
        }
    }

    /* access modifiers changed from: package-private */
    public String getCustomAdUrl(String size) {
        try {
            return this.customAdUrl.get(size);
        } catch (Exception e) {
            return "";
        }
    }

    /* access modifiers changed from: package-private */
    public String getAdServer() {
        return this.adServer;
    }

    /* access modifiers changed from: package-private */
    public String getConfigServer() {
        return this.configServer;
    }

    /* access modifiers changed from: package-private */
    public String getAnalyticsServer() {
        return this.analyticsServer;
    }

    /* access modifiers changed from: package-private */
    public String getVcServer() {
        return this.vcServer;
    }

    /* access modifiers changed from: package-private */
    public String getFeedbackServer() {
        return this.feedbackServer;
    }

    /* access modifiers changed from: package-private */
    public int isRemoteConfigSet() {
        return this.remoteConfigSet;
    }

    /* access modifiers changed from: package-private */
    public String getUserAgent() {
        if (this.userAgent.equals("") && hasPref("UserAgent")) {
            this.userAgent = getPref("UserAgent");
        }
        return this.userAgent;
    }

    /* access modifiers changed from: package-private */
    public void setUserAgent(String u) {
        this.userAgent = u;
        addPref("UserAgent", u);
    }

    static HashMap<String, String> getAllPref() {
        try {
            return (HashMap) controller.sharedPrefs.getAll();
        } catch (Exception e) {
            return new HashMap<>();
        }
    }

    static String getPref(String k) {
        try {
            return controller.sharedPrefs.getString(k, "");
        } catch (Exception e) {
            return "";
        }
    }

    static boolean hasPref(String k) {
        try {
            return controller.sharedPrefs.contains(k);
        } catch (Exception e) {
            return DEBUG;
        }
    }

    static void addPref(String k, String v) {
        try {
            SharedPreferences.Editor spe = controller.sharedPrefs.edit();
            spe.putString(k, v);
            spe.commit();
        } catch (Exception e) {
        }
    }

    static void addPref(Map<String, String> m) {
        try {
            SharedPreferences.Editor spe = controller.sharedPrefs.edit();
            for (Map.Entry<String, String> pair : m.entrySet()) {
                spe.putString((String) pair.getKey(), (String) pair.getValue());
            }
            spe.commit();
        } catch (Exception e) {
        }
    }

    static void removePref(String k) {
        try {
            SharedPreferences.Editor spe = controller.sharedPrefs.edit();
            spe.remove(k);
            spe.commit();
        } catch (Exception e) {
        }
    }

    static void clearPref() {
        try {
            SharedPreferences.Editor spe = controller.sharedPrefs.edit();
            spe.clear();
            spe.commit();
        } catch (Exception e) {
        }
    }

    private static String sha1(String string) {
        byte[] bArr = new byte[40];
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(string.getBytes(), 0, string.length());
            byte[] shaHash = md.digest();
            StringBuffer hexString = new StringBuffer();
            for (byte b : shaHash) {
                hexString.append(Integer.toHexString(b & 255));
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    /* access modifiers changed from: package-private */
    public void updateSession() {
        updateConnectivity();
        if (this.haveLocationPermission) {
            this.locationHandler.sendEmptyMessage(0);
        }
        try {
            this.session.put(MC_KEY_TIMESTAMP, System.currentTimeMillis());
            String loc = getGPS();
            if (!loc.equals("null")) {
                this.session.put(MC_KEY_LATITUDE_LONGITUDE, loc);
            } else {
                this.session.remove(MC_KEY_LATITUDE_LONGITUDE);
            }
            this.session.put(MC_KEY_CONNECTION_TYPE, this.connectionType);
        } catch (JSONException e) {
        }
    }

    /* access modifiers changed from: private */
    public void updateLocation() {
        this.location.getLocation(this.activity, new MobclixLocation.LocationResult() {
            public void gotLocation(Location location) {
                try {
                    Mobclix.this.latitude = Double.toString(location.getLatitude());
                    Mobclix.this.longitude = Double.toString(location.getLongitude());
                } catch (Exception e) {
                }
            }
        });
    }

    private void updateConnectivity() {
        NetworkInfo network_info;
        ConnectivityManager connectivityManager = (ConnectivityManager) this.activity.getSystemService("connectivity");
        String network_type = "u";
        if (this.haveNetworkStatePermission && (network_info = connectivityManager.getActiveNetworkInfo()) != null) {
            network_type = network_info.getTypeName();
        }
        if (network_type.equals("WI_FI") || network_type.equals("WIFI")) {
            this.connectionType = "wifi";
        } else if (network_type.equals("MOBILE")) {
            this.connectionType = Integer.toString(((TelephonyManager) this.activity.getSystemService("phone")).getNetworkType());
        } else {
            this.connectionType = "null";
        }
        if (this.connectionType == null) {
            this.connectionType = "null";
        }
    }

    private Mobclix() {
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v0, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: android.telephony.TelephonyManager} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void initialize(android.app.Activity r10, java.lang.String r11, int r12) {
        /*
            r9 = this;
            r4 = 0
            r3 = 0
            r9.activity = r10
            r9.applicationId = r11
            java.lang.String r1 = android.os.Build.VERSION.RELEASE
            r9.androidVersion = r1
            r7 = 0
            android.app.Activity r1 = r9.activity     // Catch:{ Exception -> 0x00d4 }
            java.lang.String r2 = "phone"
            java.lang.Object r8 = r1.getSystemService(r2)     // Catch:{ Exception -> 0x00d4 }
            r0 = r8
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ Exception -> 0x00d4 }
            r7 = r0
        L_0x0017:
            android.content.ContentResolver r1 = r10.getContentResolver()
            java.lang.String r2 = "android_id"
            java.lang.String r1 = android.provider.Settings.System.getString(r1, r2)
            r9.androidId = r1
            java.lang.String r1 = r9.androidId
            if (r1 == 0) goto L_0x0031
            java.lang.String r1 = r9.androidId
            java.lang.String r2 = ""
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0035
        L_0x0031:
            java.lang.String r1 = "null"
            r9.androidId = r1
        L_0x0035:
            java.lang.String r1 = r7.getDeviceId()
            r9.deviceId = r1
            java.lang.String r1 = r9.deviceId
            if (r1 == 0) goto L_0x0049
            java.lang.String r1 = r9.deviceId
            java.lang.String r2 = ""
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x004d
        L_0x0049:
            java.lang.String r1 = r9.androidId
            r9.deviceId = r1
        L_0x004d:
            java.lang.String r1 = android.os.Build.MODEL
            r9.deviceModel = r1
            java.lang.String r1 = r9.deviceModel
            if (r1 == 0) goto L_0x005f
            java.lang.String r1 = r9.deviceModel
            java.lang.String r2 = ""
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0063
        L_0x005f:
            java.lang.String r1 = "null"
            r9.deviceModel = r1
        L_0x0063:
            java.lang.String r1 = android.os.Build.DEVICE
            r9.deviceHardwareModel = r1
            java.lang.String r1 = r9.deviceHardwareModel
            if (r1 == 0) goto L_0x0075
            java.lang.String r1 = r9.deviceHardwareModel
            java.lang.String r2 = ""
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0079
        L_0x0075:
            java.lang.String r1 = "null"
            r9.deviceHardwareModel = r1
        L_0x0079:
            r9.logLevel = r12
            java.lang.String r1 = "null"
            r9.applicationVersion = r1
            java.lang.String r1 = "null"
            r9.longitude = r1
            java.lang.String r1 = "null"
            r9.latitude = r1
            r9.haveLocationPermission = r3
            r9.locationCriteria = r4
            r9.haveNetworkStatePermission = r3
            java.lang.String r1 = "null"
            r9.locale = r1
            android.app.Activity r1 = r9.activity
            android.webkit.CookieSyncManager.createInstance(r1)
            com.mobclix.android.sdk.Mobclix$2 r1 = new com.mobclix.android.sdk.Mobclix$2
            r1.<init>()
            r9.locationHandler = r1
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = r10.getPackageName()
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r1.<init>(r2)
            java.lang.String r2 = ".MCConfig"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.content.SharedPreferences r1 = r10.getSharedPreferences(r1, r3)
            r9.sharedPrefs = r1
            int r1 = r9.pollTime
            int r2 = r9.idleTimeout
            int r1 = java.lang.Math.min(r1, r2)
            r9.pollTime = r1
            java.util.Timer r1 = r9.sessionPollingTimer
            com.mobclix.android.sdk.Mobclix$SessionPolling r2 = new com.mobclix.android.sdk.Mobclix$SessionPolling
            r2.<init>(r9, r4)
            r3 = 0
            int r5 = r9.pollTime
            long r5 = (long) r5
            r1.scheduleAtFixedRate(r2, r3, r5)
            return
        L_0x00d4:
            r1 = move-exception
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.Mobclix.initialize(android.app.Activity, java.lang.String, int):void");
    }

    public static Mobclix getInstance() {
        return controller;
    }

    public static final synchronized void onCreate(Activity a) {
        String packageName;
        synchronized (Mobclix.class) {
            if (a != null) {
                try {
                    controller.activity = a;
                } catch (Exception e) {
                }
            }
            if (!isInitialized) {
                try {
                    packageName = a.getPackageName();
                } catch (Exception e2) {
                }
                ApplicationInfo applicationInfo = null;
                try {
                    applicationInfo = a.getPackageManager().getApplicationInfo(packageName, 128);
                } catch (NullPointerException e3) {
                    NullPointerException nullPointerException = e3;
                    throw new Resources.NotFoundException("com.mobclix.APPLICATION_ID not found in the Android Manifest xml.");
                } catch (PackageManager.NameNotFoundException e4) {
                    Log.e(MC_TAG, "Application Key Started");
                }
                String applicationId2 = applicationInfo.metaData.getString("com.mobclix.APPLICATION_ID");
                if (applicationId2 == null) {
                    throw new Resources.NotFoundException("com.mobclix.APPLICATION_ID not found in the Android Manifest xml.");
                }
                String logLevelString = null;
                try {
                    logLevelString = applicationInfo.metaData.getString("com.mobclix.LOG_LEVEL");
                } catch (Exception e5) {
                }
                int logLevelSetting = 16;
                if (logLevelString != null) {
                    if (logLevelString.equalsIgnoreCase("debug")) {
                        logLevelSetting = 1;
                    } else if (logLevelString.equalsIgnoreCase("info")) {
                        logLevelSetting = 2;
                    } else if (logLevelString.equalsIgnoreCase("warn")) {
                        logLevelSetting = 4;
                    } else if (logLevelString.equalsIgnoreCase("error")) {
                        logLevelSetting = 8;
                    } else if (logLevelString.equalsIgnoreCase("fatal")) {
                        logLevelSetting = 16;
                    }
                }
                try {
                    controller.initialize(a, applicationId2, logLevelSetting);
                    controller.updateSession();
                    isInitialized = true;
                } catch (Exception e6) {
                }
            }
            controller.handleSessionStatus(true);
        }
    }

    public static final synchronized void onStop(Activity a) {
        synchronized (Mobclix.class) {
            controller.handleSessionStatus(DEBUG);
        }
    }

    public static final void logEvent(int eventLogLevel, String processName, String eventName, String description, boolean stopProcess) {
        if (!isInitialized) {
            Log.v(MC_TAG, "logEvent failed - You must initialize Mobclix by calling Mobclix.onCreate(this).");
        } else if (eventLogLevel >= controller.logLevel) {
            String logString = String.valueOf(processName) + ", " + eventName + ": " + description;
            switch (eventLogLevel) {
                case 1:
                    Log.d("Mobclix", logString);
                    break;
                case 2:
                    Log.i("Mobclix", logString);
                    break;
                case 4:
                    Log.w("Mobclix", logString);
                    break;
                case LOG_LEVEL_ERROR /*8*/:
                    Log.e("Mobclix", logString);
                    break;
                case LOG_LEVEL_FATAL /*16*/:
                    Log.e("Mobclix", logString);
                    break;
            }
            try {
                JSONObject event = new JSONObject(controller.session, new String[]{MC_KEY_TIMESTAMP, MC_KEY_LATITUDE_LONGITUDE, MC_KEY_CONNECTION_TYPE, MC_KEY_SESSION_ID});
                event.put(MC_KEY_EVENT_LOG_LEVEL, Integer.toString(eventLogLevel));
                event.put(MC_KEY_EVENT_PROCESS_NAME, URLEncoder.encode(processName, "UTF-8"));
                event.put(MC_KEY_EVENT_NAME, URLEncoder.encode(eventName, "UTF-8"));
                event.put(MC_KEY_EVENT_DESCRIPTION, URLEncoder.encode(description, "UTF-8"));
                event.put(MC_KEY_EVENT_THREAD_ID, Long.toString(Thread.currentThread().getId()));
                event.put(MC_KEY_EVENT_STOP, stopProcess ? "1" : "0");
                Mobclix mobclix = controller;
                mobclix.getClass();
                new Thread(new LogEvent(event)).start();
            } catch (Exception e) {
            }
        }
    }

    private class LogEvent implements Runnable {
        JSONObject event;

        public LogEvent(JSONObject e) {
            this.event = e;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
         arg types: [java.io.File, int]
         candidates:
          ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
          ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
        public void run() {
            while (true) {
                try {
                    if (!Mobclix.loggingEvent && Mobclix.syncStatus == Mobclix.SYNC_READY) {
                        break;
                    }
                } catch (Exception e) {
                    Mobclix.loggingEvent = Mobclix.DEBUG;
                    return;
                }
            }
            Mobclix.loggingEvent = true;
            if (Mobclix.fileCreated || Mobclix.this.OpenAnalyticsFile()) {
                do {
                } while (!Mobclix.fileCreated);
                Mobclix.controller.updateSession();
                try {
                    FileOutputStream fos = new FileOutputStream(Mobclix.currentFile, true);
                    if (Mobclix.numLinesWritten > 1) {
                        fos.write(",".getBytes());
                    }
                    fos.write(this.event.toString().getBytes());
                    fos.close();
                    Mobclix.numLinesWritten = Mobclix.numLinesWritten + 1;
                    if (Mobclix.numLinesWritten > Mobclix.MC_MAX_EVENTS_PER_FILE) {
                        Mobclix.fileCreated = Mobclix.DEBUG;
                        boolean unused = Mobclix.this.OpenAnalyticsFile();
                    }
                } catch (Exception e2) {
                }
                Mobclix.loggingEvent = Mobclix.DEBUG;
                return;
            }
            Mobclix.loggingEvent = Mobclix.DEBUG;
        }
    }

    /* access modifiers changed from: private */
    public boolean OpenAnalyticsFile() {
        numLinesWritten = 1;
        controller.updateSession();
        try {
            JSONObject header = new JSONObject(controller.session, new String[]{MC_KEY_LATITUDE_LONGITUDE, MC_KEY_CONNECTION_TYPE, MC_KEY_SESSION_ID});
            header.put(MC_KEY_APPLICATION_ID, URLEncoder.encode(getApplicationId(), "UTF-8"));
            header.put(MC_KEY_PLATFORM_ID, "android");
            header.put(MC_KEY_MOBCLIX_LIBRARY_VERSION, URLEncoder.encode(getMobclixVersion()));
            header.put(MC_KEY_APPLICATION_VERSION, URLEncoder.encode(getApplicationVersion(), "UTF-8"));
            header.put(MC_KEY_DEVICE_ID, URLEncoder.encode(getDeviceId(), "UTF-8"));
            header.put(MC_KEY_DEVICE_MODEL, URLEncoder.encode(getDeviceModel(), "UTF-8"));
            header.put(MC_KEY_DEVICE_SYSTEM_VERSION, URLEncoder.encode(getAndroidVersion(), "UTF-8"));
            header.put(MC_KEY_DEVICE_HARDWARE_MODEL, URLEncoder.encode(getDeviceHardwareModel(), "UTF-8"));
            header.put(MC_KEY_MOBCLIX_LIBRARY_VERSION, URLEncoder.encode(MC_LIBRARY_VERSION, "UTF-8"));
            header.put(MC_KEY_USER_LANGUAGE_PREFERENCE, URLEncoder.encode(getLanguage(), "UTF-8"));
            header.put(MC_KEY_USER_LOCALE_PREFERENCE, URLEncoder.encode(getLocale(), "UTF-8"));
            File anDir = new File(String.valueOf(controller.activity.getDir(MC_DIRECTORY, 0).getAbsolutePath()) + "/" + MC_ANALYTICS_DIRECTORY);
            anDir.mkdir();
            try {
                if (anDir.listFiles().length >= MC_MAX_ANALYTICS_FILES) {
                    return DEBUG;
                }
            } catch (Exception e) {
            }
            currentFile = new File(anDir.getAbsoluteFile() + "/" + System.currentTimeMillis() + ".log");
            currentFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(currentFile);
            fos.write("[{\"hb\":".getBytes());
            fos.write(header.toString().getBytes());
            fos.write(",\"ev\":[".getBytes());
            fos.close();
            fileCreated = true;
            return true;
        } catch (Exception e2) {
            return DEBUG;
        }
    }

    public static final void sync() {
        if (!isInitialized) {
            Log.v(MC_TAG, "sync failed - You must initialize Mobclix by calling Mobclix.onCreate(this).");
        } else if (syncStatus == SYNC_READY) {
            Mobclix mobclix = controller;
            mobclix.getClass();
            new Thread(new Sync(mobclix, null)).start();
        }
    }

    private class Sync implements Runnable {
        private Sync() {
        }

        /* synthetic */ Sync(Mobclix mobclix, Sync sync) {
            this();
        }

        /* JADX WARNING: Removed duplicated region for block: B:49:0x01dd A[Catch:{ Exception -> 0x01b1 }] */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x01a8 A[SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public synchronized void run() {
            /*
                r22 = this;
                monitor-enter(r22)
            L_0x0001:
                boolean r19 = com.mobclix.android.sdk.Mobclix.loggingEvent     // Catch:{ all -> 0x01c0 }
                if (r19 != 0) goto L_0x0001
                int r19 = com.mobclix.android.sdk.Mobclix.SYNC_RUNNING     // Catch:{ all -> 0x01c0 }
                com.mobclix.android.sdk.Mobclix.syncStatus = r19     // Catch:{ all -> 0x01c0 }
                com.mobclix.android.sdk.Mobclix r19 = com.mobclix.android.sdk.Mobclix.controller     // Catch:{ Exception -> 0x01b1 }
                android.app.Activity r19 = r19.activity     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r20 = com.mobclix.android.sdk.Mobclix.MC_DIRECTORY     // Catch:{ Exception -> 0x01b1 }
                r21 = 0
                java.io.File r12 = r19.getDir(r20, r21)     // Catch:{ Exception -> 0x01b1 }
                java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x01b1 }
                java.lang.StringBuilder r19 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r20 = r12.getAbsolutePath()     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r20 = java.lang.String.valueOf(r20)     // Catch:{ Exception -> 0x01b1 }
                r19.<init>(r20)     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r20 = "/"
                java.lang.StringBuilder r19 = r19.append(r20)     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r20 = com.mobclix.android.sdk.Mobclix.MC_ANALYTICS_DIRECTORY     // Catch:{ Exception -> 0x01b1 }
                java.lang.StringBuilder r19 = r19.append(r20)     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r19 = r19.toString()     // Catch:{ Exception -> 0x01b1 }
                r0 = r2
                r1 = r19
                r0.<init>(r1)     // Catch:{ Exception -> 0x01b1 }
                r2.mkdir()     // Catch:{ Exception -> 0x01b1 }
                java.io.File[] r8 = r2.listFiles()     // Catch:{ Exception -> 0x01b1 }
                r10 = 0
            L_0x004f:
                r0 = r8
                int r0 = r0.length     // Catch:{ Exception -> 0x01b1 }
                r19 = r0
                r0 = r10
                r1 = r19
                if (r0 < r1) goto L_0x0070
            L_0x0058:
                r19 = 0
                com.mobclix.android.sdk.Mobclix.syncContents = r19     // Catch:{ all -> 0x01c0 }
                r19 = 0
                com.mobclix.android.sdk.Mobclix.numLinesWritten = r19     // Catch:{ all -> 0x01c0 }
                r19 = 0
                com.mobclix.android.sdk.Mobclix.fileCreated = r19     // Catch:{ all -> 0x01c0 }
                int r19 = com.mobclix.android.sdk.Mobclix.SYNC_READY     // Catch:{ all -> 0x01c0 }
                com.mobclix.android.sdk.Mobclix.syncStatus = r19     // Catch:{ all -> 0x01c0 }
            L_0x006e:
                monitor-exit(r22)
                return
            L_0x0070:
                java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x01b1 }
                r5.<init>()     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r19 = "p=android"
                r0 = r5
                r1 = r19
                r0.append(r1)     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r19 = "&a="
                r0 = r5
                r1 = r19
                java.lang.StringBuffer r19 = r0.append(r1)     // Catch:{ Exception -> 0x01b1 }
                com.mobclix.android.sdk.Mobclix r20 = com.mobclix.android.sdk.Mobclix.controller     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r20 = r20.getApplicationId()     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r21 = "UTF-8"
                java.lang.String r20 = java.net.URLEncoder.encode(r20, r21)     // Catch:{ Exception -> 0x01b1 }
                r19.append(r20)     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r19 = "&m="
                r0 = r5
                r1 = r19
                java.lang.StringBuffer r19 = r0.append(r1)     // Catch:{ Exception -> 0x01b1 }
                com.mobclix.android.sdk.Mobclix r20 = com.mobclix.android.sdk.Mobclix.controller     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r20 = r20.getMobclixVersion()     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r20 = java.net.URLEncoder.encode(r20)     // Catch:{ Exception -> 0x01b1 }
                r19.append(r20)     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r19 = "&d="
                r0 = r5
                r1 = r19
                java.lang.StringBuffer r19 = r0.append(r1)     // Catch:{ Exception -> 0x01b1 }
                com.mobclix.android.sdk.Mobclix r20 = com.mobclix.android.sdk.Mobclix.controller     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r20 = r20.getDeviceId()     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r21 = "UTF-8"
                java.lang.String r20 = java.net.URLEncoder.encode(r20, r21)     // Catch:{ Exception -> 0x01b1 }
                r19.append(r20)     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r19 = "&v="
                r0 = r5
                r1 = r19
                java.lang.StringBuffer r19 = r0.append(r1)     // Catch:{ Exception -> 0x01b1 }
                r0 = r22
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x01b1 }
                r20 = r0
                java.lang.String r20 = r20.applicationVersion     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r21 = "UTF-8"
                java.lang.String r20 = java.net.URLEncoder.encode(r20, r21)     // Catch:{ Exception -> 0x01b1 }
                r19.append(r20)     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r19 = "&j="
                r0 = r5
                r1 = r19
                r0.append(r1)     // Catch:{ Exception -> 0x01b1 }
                java.io.FileInputStream r9 = new java.io.FileInputStream     // Catch:{ Exception -> 0x01b1 }
                r19 = r8[r10]     // Catch:{ Exception -> 0x01b1 }
                r0 = r9
                r1 = r19
                r0.<init>(r1)     // Catch:{ Exception -> 0x01b1 }
                java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x01b1 }
                r3.<init>(r9)     // Catch:{ Exception -> 0x01b1 }
                java.io.DataInputStream r6 = new java.io.DataInputStream     // Catch:{ Exception -> 0x01b1 }
                r6.<init>(r3)     // Catch:{ Exception -> 0x01b1 }
            L_0x0101:
                int r19 = r6.available()     // Catch:{ Exception -> 0x01b1 }
                if (r19 != 0) goto L_0x01b4
                r6.close()     // Catch:{ Exception -> 0x01b1 }
                r3.close()     // Catch:{ Exception -> 0x01b1 }
                r9.close()     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r19 = "]}]"
                r0 = r5
                r1 = r19
                r0.append(r1)     // Catch:{ Exception -> 0x01b1 }
                java.lang.String r19 = r5.toString()     // Catch:{ Exception -> 0x01b1 }
                com.mobclix.android.sdk.Mobclix.syncContents = r19     // Catch:{ Exception -> 0x01b1 }
                r17 = 0
                r4 = 0
                java.net.URL r18 = new java.net.URL     // Catch:{ Exception -> 0x01e6 }
                r0 = r22
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x01e6 }
                r19 = r0
                java.lang.String r19 = r19.analyticsServer     // Catch:{ Exception -> 0x01e6 }
                r18.<init>(r19)     // Catch:{ Exception -> 0x01e6 }
                java.net.URLConnection r5 = r18.openConnection()     // Catch:{ Exception -> 0x01d0 }
                r0 = r5
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x01d0 }
                r4 = r0
                r19 = 1
                r0 = r4
                r1 = r19
                r0.setDoOutput(r1)     // Catch:{ Exception -> 0x01d0 }
                r19 = 1
                r0 = r4
                r1 = r19
                r0.setDoInput(r1)     // Catch:{ Exception -> 0x01d0 }
                r19 = 0
                r0 = r4
                r1 = r19
                r0.setUseCaches(r1)     // Catch:{ Exception -> 0x01d0 }
                java.lang.String r19 = "POST"
                r0 = r4
                r1 = r19
                r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x01d0 }
                java.io.OutputStream r13 = r4.getOutputStream()     // Catch:{ Exception -> 0x01d0 }
                java.io.PrintWriter r14 = new java.io.PrintWriter     // Catch:{ Exception -> 0x01d0 }
                r14.<init>(r13)     // Catch:{ Exception -> 0x01d0 }
                java.lang.String r19 = com.mobclix.android.sdk.Mobclix.syncContents     // Catch:{ Exception -> 0x01d0 }
                r0 = r14
                r1 = r19
                r0.print(r1)     // Catch:{ Exception -> 0x01d0 }
                r14.flush()     // Catch:{ Exception -> 0x01d0 }
                r14.close()     // Catch:{ Exception -> 0x01d0 }
                java.io.BufferedReader r15 = new java.io.BufferedReader     // Catch:{ Exception -> 0x01d0 }
                java.io.InputStreamReader r19 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x01d0 }
                java.io.InputStream r20 = r4.getInputStream()     // Catch:{ Exception -> 0x01d0 }
                r19.<init>(r20)     // Catch:{ Exception -> 0x01d0 }
                r0 = r15
                r1 = r19
                r0.<init>(r1)     // Catch:{ Exception -> 0x01d0 }
                r11 = 0
            L_0x0184:
                java.lang.String r16 = r15.readLine()     // Catch:{ Exception -> 0x01d0 }
                if (r16 != 0) goto L_0x01c3
                java.lang.String r19 = "1"
                r0 = r11
                r1 = r19
                boolean r19 = r0.equals(r1)     // Catch:{ Exception -> 0x01d0 }
                if (r19 == 0) goto L_0x01c8
            L_0x0195:
                r15.close()     // Catch:{ Exception -> 0x01d0 }
                r17 = r18
            L_0x019a:
                int r19 = com.mobclix.android.sdk.Mobclix.syncStatus     // Catch:{ Exception -> 0x01b1 }
                int r20 = com.mobclix.android.sdk.Mobclix.SYNC_ERROR     // Catch:{ Exception -> 0x01b1 }
                r0 = r19
                r1 = r20
                if (r0 != r1) goto L_0x01dd
                int r19 = com.mobclix.android.sdk.Mobclix.SYNC_READY     // Catch:{ Exception -> 0x01b1 }
                com.mobclix.android.sdk.Mobclix.syncStatus = r19     // Catch:{ Exception -> 0x01b1 }
                goto L_0x006e
            L_0x01b1:
                r19 = move-exception
                goto L_0x0058
            L_0x01b4:
                java.lang.String r19 = r6.readLine()     // Catch:{ Exception -> 0x01b1 }
                r0 = r5
                r1 = r19
                r0.append(r1)     // Catch:{ Exception -> 0x01b1 }
                goto L_0x0101
            L_0x01c0:
                r19 = move-exception
                monitor-exit(r22)
                throw r19
            L_0x01c3:
                if (r11 != 0) goto L_0x0184
                r11 = r16
                goto L_0x0184
            L_0x01c8:
                int r19 = com.mobclix.android.sdk.Mobclix.SYNC_ERROR     // Catch:{ Exception -> 0x01d0 }
                com.mobclix.android.sdk.Mobclix.syncStatus = r19     // Catch:{ Exception -> 0x01d0 }
                goto L_0x0195
            L_0x01d0:
                r19 = move-exception
                r7 = r19
                r17 = r18
            L_0x01d5:
                int r19 = com.mobclix.android.sdk.Mobclix.SYNC_ERROR     // Catch:{ Exception -> 0x01b1 }
                com.mobclix.android.sdk.Mobclix.syncStatus = r19     // Catch:{ Exception -> 0x01b1 }
                goto L_0x019a
            L_0x01dd:
                r19 = r8[r10]     // Catch:{ Exception -> 0x01b1 }
                r19.delete()     // Catch:{ Exception -> 0x01b1 }
                int r10 = r10 + 1
                goto L_0x004f
            L_0x01e6:
                r19 = move-exception
                r7 = r19
                goto L_0x01d5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.Mobclix.Sync.run():void");
        }
    }

    private void createNewSession() {
        long ts = System.currentTimeMillis();
        String sessionId = sha1(String.valueOf(this.deviceId) + ts);
        this.isTopTask = true;
        this.sessionStartTime = ts;
        this.sessionEndTime = 0;
        this.totalIdleTime = 0;
        this.isInSession = true;
        try {
            this.session.put(MC_KEY_SESSION_ID, URLEncoder.encode(sessionId, "UTF-8"));
        } catch (Exception e) {
        }
        this.remoteConfigSet = 0;
        new Thread(new FetchRemoteConfig(this, null)).start();
    }

    private class SessionPolling extends TimerTask {
        private SessionPolling() {
        }

        /* synthetic */ SessionPolling(Mobclix mobclix, SessionPolling sessionPolling) {
            this();
        }

        public synchronized void run() {
            Mobclix.this.handleSessionStatus(isTopTask());
        }

        public boolean isTopTask() {
            try {
                return ((ActivityManager) Mobclix.this.activity.getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getPackageName().equals(Mobclix.this.activity.getPackageName());
            } catch (Exception e) {
                return Mobclix.DEBUG;
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void handleSessionStatus(boolean r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0015, all -> 0x0024 }
            if (r7 == 0) goto L_0x0027
            boolean r2 = r6.isTopTask     // Catch:{ Exception -> 0x0015, all -> 0x0024 }
            if (r2 == 0) goto L_0x000d
        L_0x000b:
            monitor-exit(r6)
            return
        L_0x000d:
            boolean r2 = r6.isInSession     // Catch:{ Exception -> 0x0015, all -> 0x0024 }
            if (r2 != 0) goto L_0x0017
            r6.createNewSession()     // Catch:{ Exception -> 0x0015, all -> 0x0024 }
            goto L_0x000b
        L_0x0015:
            r2 = move-exception
            goto L_0x000b
        L_0x0017:
            long r2 = r6.totalIdleTime     // Catch:{ Exception -> 0x0015, all -> 0x0024 }
            long r4 = r6.sessionEndTime     // Catch:{ Exception -> 0x0015, all -> 0x0024 }
            long r4 = r0 - r4
            long r2 = r2 + r4
            r6.totalIdleTime = r2     // Catch:{ Exception -> 0x0015, all -> 0x0024 }
            r2 = 1
            r6.isTopTask = r2     // Catch:{ Exception -> 0x0015, all -> 0x0024 }
            goto L_0x000b
        L_0x0024:
            r2 = move-exception
            monitor-exit(r6)
            throw r2
        L_0x0027:
            boolean r2 = r6.isTopTask     // Catch:{ Exception -> 0x0015, all -> 0x0024 }
            if (r2 != 0) goto L_0x003e
            long r2 = r6.sessionEndTime     // Catch:{ Exception -> 0x0015, all -> 0x0024 }
            long r2 = r0 - r2
            int r4 = r6.idleTimeout     // Catch:{ Exception -> 0x0015, all -> 0x0024 }
            long r4 = (long) r4     // Catch:{ Exception -> 0x0015, all -> 0x0024 }
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x000b
            boolean r2 = r6.isInSession     // Catch:{ Exception -> 0x0015, all -> 0x0024 }
            if (r2 == 0) goto L_0x000b
            r6.endSession()     // Catch:{ Exception -> 0x0015, all -> 0x0024 }
            goto L_0x000b
        L_0x003e:
            r6.sessionEndTime = r0     // Catch:{ Exception -> 0x0015, all -> 0x0024 }
            r2 = 0
            r6.isTopTask = r2     // Catch:{ Exception -> 0x0015, all -> 0x0024 }
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.Mobclix.handleSessionStatus(boolean):void");
    }

    private void endSession() {
        try {
            if (this.isInSession) {
                long sessionTime = this.sessionEndTime - this.sessionStartTime;
                if (hasPref("totalSessionTime")) {
                    try {
                        sessionTime += Long.parseLong(getPref("totalSessionTime"));
                    } catch (Exception e) {
                    }
                }
                if (hasPref("totalIdleTime")) {
                    try {
                        this.totalIdleTime += Long.parseLong(getPref("totalIdleTime"));
                    } catch (Exception e2) {
                    }
                }
                HashMap<String, String> sessionStats = new HashMap<>();
                sessionStats.put("totalSessionTime", Long.toString(sessionTime));
                sessionStats.put("totalIdleTime", Long.toString(this.totalIdleTime));
                if (this.isOfflineSession) {
                    int offlineSessions = 1;
                    if (hasPref("offlineSessions")) {
                        try {
                            offlineSessions = 1 + Integer.parseInt(getPref("offlineSessions"));
                        } catch (Exception e3) {
                        }
                    }
                    sessionStats.put("offlineSessions", Long.toString((long) offlineSessions));
                }
                addPref(sessionStats);
                this.isInSession = DEBUG;
                this.isTopTask = DEBUG;
                this.sessionStartTime = 0;
                this.sessionEndTime = 0;
                this.totalIdleTime = 0;
            }
        } catch (Exception e4) {
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        endSession();
    }

    private class FetchRemoteConfig implements Runnable {
        private String url;

        private FetchRemoteConfig() {
        }

        /* synthetic */ FetchRemoteConfig(Mobclix mobclix, FetchRemoteConfig fetchRemoteConfig) {
            this();
        }

        /* JADX WARN: Type inference failed for: r10v0, types: [java.net.URLConnection] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:132:0x06a7 A[LOOP:1: B:43:0x0191->B:132:0x06a7, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:172:0x0889  */
        /* JADX WARNING: Removed duplicated region for block: B:195:0x01a3 A[EDGE_INSN: B:195:0x01a3->B:45:0x01a3 ?: BREAK  , SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r32 = this;
                java.lang.String r5 = ""
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x08d2 }
                r26 = r0
                android.app.Activity r26 = r26.activity     // Catch:{ Exception -> 0x08d2 }
                java.lang.String r5 = r26.getPackageName()     // Catch:{ Exception -> 0x08d2 }
            L_0x0010:
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0252 }
                r26 = r0
                android.app.Activity r26 = r26.activity     // Catch:{ Exception -> 0x0252 }
                android.content.pm.PackageManager r16 = r26.getPackageManager()     // Catch:{ Exception -> 0x0252 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ NameNotFoundException -> 0x0242 }
                r26 = r0
                r27 = 0
                r0 = r16
                r1 = r5
                r2 = r27
                android.content.pm.PackageInfo r27 = r0.getPackageInfo(r1, r2)     // Catch:{ NameNotFoundException -> 0x0242 }
                r0 = r27
                java.lang.String r0 = r0.versionName     // Catch:{ NameNotFoundException -> 0x0242 }
                r27 = r0
                r26.applicationVersion = r27     // Catch:{ NameNotFoundException -> 0x0242 }
            L_0x0038:
                java.lang.String r26 = "android.permission.ACCESS_FINE_LOCATION"
                r0 = r16
                r1 = r26
                r2 = r5
                int r26 = r0.checkPermission(r1, r2)     // Catch:{ Exception -> 0x0252 }
                if (r26 != 0) goto L_0x0255
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0252 }
                r26 = r0
                android.location.Criteria r27 = new android.location.Criteria     // Catch:{ Exception -> 0x0252 }
                r27.<init>()     // Catch:{ Exception -> 0x0252 }
                r26.locationCriteria = r27     // Catch:{ Exception -> 0x0252 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0252 }
                r26 = r0
                android.location.Criteria r26 = r26.locationCriteria     // Catch:{ Exception -> 0x0252 }
                r27 = 1
                r26.setAccuracy(r27)     // Catch:{ Exception -> 0x0252 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0252 }
                r26 = r0
                r27 = 1
                r26.haveLocationPermission = r27     // Catch:{ Exception -> 0x0252 }
            L_0x006d:
                java.lang.String r26 = "android.permission.ACCESS_NETWORK_STATE"
                r0 = r16
                r1 = r26
                r2 = r5
                int r26 = r0.checkPermission(r1, r2)     // Catch:{ Exception -> 0x0252 }
                if (r26 != 0) goto L_0x0299
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0252 }
                r26 = r0
                r27 = 1
                r26.haveNetworkStatePermission = r27     // Catch:{ Exception -> 0x0252 }
            L_0x0085:
                java.util.Locale r12 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x08cf }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x08cf }
                r26 = r0
                java.lang.String r27 = r12.toString()     // Catch:{ Exception -> 0x08cf }
                r26.locale = r27     // Catch:{ Exception -> 0x08cf }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x08cf }
                r26 = r0
                java.lang.String r27 = r12.getLanguage()     // Catch:{ Exception -> 0x08cf }
                r26.language = r27     // Catch:{ Exception -> 0x08cf }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x08cf }
                r26 = r0
                android.app.Activity r26 = r26.activity     // Catch:{ Exception -> 0x08cf }
                java.lang.String r27 = "phone"
                java.lang.Object r23 = r26.getSystemService(r27)     // Catch:{ Exception -> 0x08cf }
                android.telephony.TelephonyManager r23 = (android.telephony.TelephonyManager) r23     // Catch:{ Exception -> 0x08cf }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x08cf }
                r26 = r0
                java.lang.String r26 = r26.locale     // Catch:{ Exception -> 0x08cf }
                java.lang.String r27 = ""
                boolean r26 = r26.equals(r27)     // Catch:{ Exception -> 0x08cf }
                if (r26 == 0) goto L_0x00eb
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x08cf }
                r26 = r0
                java.lang.String r27 = r23.getSimCountryIso()     // Catch:{ Exception -> 0x08cf }
                r26.locale = r27     // Catch:{ Exception -> 0x08cf }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x08cf }
                r26 = r0
                java.lang.String r26 = r26.locale     // Catch:{ Exception -> 0x08cf }
                if (r26 != 0) goto L_0x00eb
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x08cf }
                r26 = r0
                java.lang.String r27 = "null"
                r26.locale = r27     // Catch:{ Exception -> 0x08cf }
            L_0x00eb:
                java.lang.String r15 = r23.getNetworkOperator()     // Catch:{ Exception -> 0x08cf }
                if (r15 == 0) goto L_0x0119
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x08cf }
                r26 = r0
                r27 = 0
                r28 = 3
                r0 = r15
                r1 = r27
                r2 = r28
                java.lang.String r27 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x08cf }
                r26.mcc = r27     // Catch:{ Exception -> 0x08cf }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x08cf }
                r26 = r0
                r27 = 3
                r0 = r15
                r1 = r27
                java.lang.String r27 = r0.substring(r1)     // Catch:{ Exception -> 0x08cf }
                r26.mnc = r27     // Catch:{ Exception -> 0x08cf }
            L_0x0119:
                java.lang.String r26 = "deviceId"
                boolean r26 = com.mobclix.android.sdk.Mobclix.hasPref(r26)     // Catch:{ Exception -> 0x02c2 }
                if (r26 == 0) goto L_0x02a6
                java.lang.String r26 = "deviceId"
                java.lang.String r17 = com.mobclix.android.sdk.Mobclix.getPref(r26)     // Catch:{ Exception -> 0x02c2 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x02c2 }
                r26 = r0
                java.lang.String r26 = r26.deviceId     // Catch:{ Exception -> 0x02c2 }
                r0 = r17
                r1 = r26
                boolean r26 = r0.equals(r1)     // Catch:{ Exception -> 0x02c2 }
                if (r26 != 0) goto L_0x0148
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x02c2 }
                r26 = r0
                r0 = r26
                r1 = r17
                r0.previousDeviceId = r1     // Catch:{ Exception -> 0x02c2 }
            L_0x0148:
                java.lang.String r26 = "idleTimeout"
                boolean r26 = com.mobclix.android.sdk.Mobclix.hasPref(r26)
                if (r26 == 0) goto L_0x0163
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r26 = r0
                java.lang.String r27 = "idleTimeout"
                java.lang.String r27 = com.mobclix.android.sdk.Mobclix.getPref(r27)
                int r27 = java.lang.Integer.parseInt(r27)
                r26.idleTimeout = r27
            L_0x0163:
                java.lang.String r26 = "pollTime"
                boolean r26 = com.mobclix.android.sdk.Mobclix.hasPref(r26)
                if (r26 == 0) goto L_0x017e
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r26 = r0
                java.lang.String r27 = "pollTime"
                java.lang.String r27 = com.mobclix.android.sdk.Mobclix.getPref(r27)
                int r27 = java.lang.Integer.parseInt(r27)
                r26.pollTime = r27
            L_0x017e:
                java.lang.String[] r26 = com.mobclix.android.sdk.Mobclix.MC_AD_SIZES
                r0 = r26
                int r0 = r0.length
                r27 = r0
                r28 = 0
            L_0x0189:
                r0 = r28
                r1 = r27
                if (r0 < r1) goto L_0x02c5
                r19 = 1
            L_0x0191:
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r26 = r0
                int r26 = r26.remoteConfigSet
                r27 = 1
                r0 = r26
                r1 = r27
                if (r0 != r1) goto L_0x0459
            L_0x01a3:
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r26 = r0
                int r26 = r26.remoteConfigSet
                r27 = 1
                r0 = r26
                r1 = r27
                if (r0 == r1) goto L_0x023e
                java.lang.String r26 = "ConfigServer"
                boolean r26 = com.mobclix.android.sdk.Mobclix.hasPref(r26)
                if (r26 == 0) goto L_0x01cc
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r26 = r0
                java.lang.String r27 = "ConfigServer"
                java.lang.String r27 = com.mobclix.android.sdk.Mobclix.getPref(r27)
                r26.configServer = r27
            L_0x01cc:
                java.lang.String r26 = "AdServer"
                boolean r26 = com.mobclix.android.sdk.Mobclix.hasPref(r26)
                if (r26 == 0) goto L_0x01e3
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r26 = r0
                java.lang.String r27 = "AdServer"
                java.lang.String r27 = com.mobclix.android.sdk.Mobclix.getPref(r27)
                r26.adServer = r27
            L_0x01e3:
                java.lang.String r26 = "AnalyticsServer"
                boolean r26 = com.mobclix.android.sdk.Mobclix.hasPref(r26)
                if (r26 == 0) goto L_0x01fa
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r26 = r0
                java.lang.String r27 = "AnalyticsServer"
                java.lang.String r27 = com.mobclix.android.sdk.Mobclix.getPref(r27)
                r26.analyticsServer = r27
            L_0x01fa:
                java.lang.String r26 = "VcServer"
                boolean r26 = com.mobclix.android.sdk.Mobclix.hasPref(r26)
                if (r26 == 0) goto L_0x0211
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r26 = r0
                java.lang.String r27 = "VcServer"
                java.lang.String r27 = com.mobclix.android.sdk.Mobclix.getPref(r27)
                r26.vcServer = r27
            L_0x0211:
                java.lang.String r26 = "FeedbackServer"
                boolean r26 = com.mobclix.android.sdk.Mobclix.hasPref(r26)
                if (r26 == 0) goto L_0x0228
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r26 = r0
                java.lang.String r27 = "FeedbackServer"
                java.lang.String r27 = com.mobclix.android.sdk.Mobclix.getPref(r27)
                r26.feedbackServer = r27
            L_0x0228:
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r26 = r0
                r27 = 1
                r26.remoteConfigSet = r27
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r26 = r0
                r27 = 1
                r26.isOfflineSession = r27
            L_0x023e:
                com.mobclix.android.sdk.Mobclix.sync()
                return
            L_0x0242:
                r26 = move-exception
                r13 = r26
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0252 }
                r26 = r0
                java.lang.String r27 = "null"
                r26.applicationVersion = r27     // Catch:{ Exception -> 0x0252 }
                goto L_0x0038
            L_0x0252:
                r26 = move-exception
                goto L_0x0085
            L_0x0255:
                java.lang.String r26 = "android.permission.ACCESS_COARSE_LOCATION"
                r0 = r16
                r1 = r26
                r2 = r5
                int r26 = r0.checkPermission(r1, r2)     // Catch:{ Exception -> 0x0252 }
                if (r26 != 0) goto L_0x028c
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0252 }
                r26 = r0
                android.location.Criteria r27 = new android.location.Criteria     // Catch:{ Exception -> 0x0252 }
                r27.<init>()     // Catch:{ Exception -> 0x0252 }
                r26.locationCriteria = r27     // Catch:{ Exception -> 0x0252 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0252 }
                r26 = r0
                android.location.Criteria r26 = r26.locationCriteria     // Catch:{ Exception -> 0x0252 }
                r27 = 2
                r26.setAccuracy(r27)     // Catch:{ Exception -> 0x0252 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0252 }
                r26 = r0
                r27 = 1
                r26.haveLocationPermission = r27     // Catch:{ Exception -> 0x0252 }
                goto L_0x006d
            L_0x028c:
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0252 }
                r26 = r0
                r27 = 0
                r26.haveLocationPermission = r27     // Catch:{ Exception -> 0x0252 }
                goto L_0x006d
            L_0x0299:
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0252 }
                r26 = r0
                r27 = 0
                r26.haveNetworkStatePermission = r27     // Catch:{ Exception -> 0x0252 }
                goto L_0x0085
            L_0x02a6:
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x02c2 }
                r26 = r0
                r27 = 1
                r26.isNewUser = r27     // Catch:{ Exception -> 0x02c2 }
                java.lang.String r26 = "deviceId"
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x02c2 }
                r27 = r0
                java.lang.String r27 = r27.deviceId     // Catch:{ Exception -> 0x02c2 }
                com.mobclix.android.sdk.Mobclix.addPref(r26, r27)     // Catch:{ Exception -> 0x02c2 }
                goto L_0x0148
            L_0x02c2:
                r26 = move-exception
                goto L_0x0148
            L_0x02c5:
                r22 = r26[r28]
                boolean r29 = com.mobclix.android.sdk.Mobclix.hasPref(r22)
                if (r29 == 0) goto L_0x03f5
                java.lang.String r8 = com.mobclix.android.sdk.Mobclix.getPref(r22)
                java.lang.String r29 = ","
                r0 = r8
                r1 = r29
                java.lang.String[] r11 = r0.split(r1)
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x039b }
                r29 = r0
                java.util.HashMap r29 = r29.enabled     // Catch:{ Exception -> 0x039b }
                r30 = 0
                r30 = r11[r30]     // Catch:{ Exception -> 0x039b }
                java.lang.String r31 = "true"
                boolean r30 = r30.equals(r31)     // Catch:{ Exception -> 0x039b }
                java.lang.Boolean r30 = java.lang.Boolean.valueOf(r30)     // Catch:{ Exception -> 0x039b }
                r0 = r29
                r1 = r22
                r2 = r30
                r0.put(r1, r2)     // Catch:{ Exception -> 0x039b }
            L_0x02fb:
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x03b9 }
                r29 = r0
                java.util.HashMap r29 = r29.refreshTime     // Catch:{ Exception -> 0x03b9 }
                r30 = 1
                r30 = r11[r30]     // Catch:{ Exception -> 0x03b9 }
                long r30 = java.lang.Long.parseLong(r30)     // Catch:{ Exception -> 0x03b9 }
                java.lang.Long r30 = java.lang.Long.valueOf(r30)     // Catch:{ Exception -> 0x03b9 }
                r0 = r29
                r1 = r22
                r2 = r30
                r0.put(r1, r2)     // Catch:{ Exception -> 0x03b9 }
            L_0x031a:
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x03d7 }
                r29 = r0
                java.util.HashMap r29 = r29.autoplay     // Catch:{ Exception -> 0x03d7 }
                r30 = 2
                r30 = r11[r30]     // Catch:{ Exception -> 0x03d7 }
                java.lang.String r31 = "true"
                boolean r30 = r30.equals(r31)     // Catch:{ Exception -> 0x03d7 }
                java.lang.Boolean r30 = java.lang.Boolean.valueOf(r30)     // Catch:{ Exception -> 0x03d7 }
                r0 = r29
                r1 = r22
                r2 = r30
                r0.put(r1, r2)     // Catch:{ Exception -> 0x03d7 }
            L_0x033b:
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r29 = r0
                java.util.HashMap r29 = r29.customAdSet
                r30 = 0
                java.lang.Boolean r30 = java.lang.Boolean.valueOf(r30)
                r0 = r29
                r1 = r22
                r2 = r30
                r0.put(r1, r2)
                java.lang.StringBuilder r29 = new java.lang.StringBuilder
                java.lang.String r30 = java.lang.String.valueOf(r22)
                r29.<init>(r30)
                java.lang.String r30 = "CustomAdUrl"
                java.lang.StringBuilder r29 = r29.append(r30)
                java.lang.String r29 = r29.toString()
                boolean r29 = com.mobclix.android.sdk.Mobclix.hasPref(r29)
                if (r29 == 0) goto L_0x0442
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r29 = r0
                java.util.HashMap r29 = r29.customAdUrl
                java.lang.StringBuilder r30 = new java.lang.StringBuilder
                java.lang.String r31 = java.lang.String.valueOf(r22)
                r30.<init>(r31)
                java.lang.String r31 = "CustomAdUrl"
                java.lang.StringBuilder r30 = r30.append(r31)
                java.lang.String r30 = r30.toString()
                java.lang.String r30 = com.mobclix.android.sdk.Mobclix.getPref(r30)
                r0 = r29
                r1 = r22
                r2 = r30
                r0.put(r1, r2)
            L_0x0397:
                int r28 = r28 + 1
                goto L_0x0189
            L_0x039b:
                r29 = move-exception
                r13 = r29
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r29 = r0
                java.util.HashMap r29 = r29.enabled
                r30 = 1
                java.lang.Boolean r30 = java.lang.Boolean.valueOf(r30)
                r0 = r29
                r1 = r22
                r2 = r30
                r0.put(r1, r2)
                goto L_0x02fb
            L_0x03b9:
                r29 = move-exception
                r13 = r29
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r29 = r0
                java.util.HashMap r29 = r29.refreshTime
                r30 = 30000(0x7530, double:1.4822E-319)
                java.lang.Long r30 = java.lang.Long.valueOf(r30)
                r0 = r29
                r1 = r22
                r2 = r30
                r0.put(r1, r2)
                goto L_0x031a
            L_0x03d7:
                r29 = move-exception
                r13 = r29
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r29 = r0
                java.util.HashMap r29 = r29.autoplay
                r30 = 0
                java.lang.Boolean r30 = java.lang.Boolean.valueOf(r30)
                r0 = r29
                r1 = r22
                r2 = r30
                r0.put(r1, r2)
                goto L_0x033b
            L_0x03f5:
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r29 = r0
                java.util.HashMap r29 = r29.enabled
                r30 = 1
                java.lang.Boolean r30 = java.lang.Boolean.valueOf(r30)
                r0 = r29
                r1 = r22
                r2 = r30
                r0.put(r1, r2)
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r29 = r0
                java.util.HashMap r29 = r29.refreshTime
                r30 = 30000(0x7530, double:1.4822E-319)
                java.lang.Long r30 = java.lang.Long.valueOf(r30)
                r0 = r29
                r1 = r22
                r2 = r30
                r0.put(r1, r2)
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r29 = r0
                java.util.HashMap r29 = r29.autoplay
                r30 = 0
                java.lang.Boolean r30 = java.lang.Boolean.valueOf(r30)
                r0 = r29
                r1 = r22
                r2 = r30
                r0.put(r1, r2)
                goto L_0x033b
            L_0x0442:
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r29 = r0
                java.util.HashMap r29 = r29.customAdUrl
                java.lang.String r30 = ""
                r0 = r29
                r1 = r22
                r2 = r30
                r0.put(r1, r2)
                goto L_0x0397
            L_0x0459:
                r0 = r32
                r1 = r19
                java.lang.String r26 = r0.getConfigUrl(r1)
                r0 = r26
                r1 = r32
                r1.url = r0
                java.lang.String r20 = ""
                r9 = 0
                r7 = 0
                java.net.URL r26 = new java.net.URL     // Catch:{ Exception -> 0x08c6, all -> 0x08c3 }
                r0 = r32
                java.lang.String r0 = r0.url     // Catch:{ Exception -> 0x08c6, all -> 0x08c3 }
                r27 = r0
                r26.<init>(r27)     // Catch:{ Exception -> 0x08c6, all -> 0x08c3 }
                java.net.URLConnection r10 = r26.openConnection()     // Catch:{ Exception -> 0x08c6, all -> 0x08c3 }
                r0 = r10
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x08c6, all -> 0x08c3 }
                r9 = r0
                java.lang.String r26 = "GET"
                r0 = r9
                r1 = r26
                r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x08c6, all -> 0x08c3 }
                java.lang.String r26 = "User-Agent"
                com.mobclix.android.sdk.Mobclix r27 = com.mobclix.android.sdk.Mobclix.controller     // Catch:{ Exception -> 0x08c6, all -> 0x08c3 }
                java.lang.String r27 = r27.getUserAgent()     // Catch:{ Exception -> 0x08c6, all -> 0x08c3 }
                r0 = r9
                r1 = r26
                r2 = r27
                r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x08c6, all -> 0x08c3 }
                r9.connect()     // Catch:{ Exception -> 0x08c6, all -> 0x08c3 }
                int r21 = r9.getResponseCode()     // Catch:{ Exception -> 0x08c6, all -> 0x08c3 }
                r26 = 200(0xc8, float:2.8E-43)
                r0 = r21
                r1 = r26
                if (r0 != r1) goto L_0x088d
                java.io.BufferedReader r6 = new java.io.BufferedReader     // Catch:{ Exception -> 0x08c6, all -> 0x08c3 }
                java.io.InputStreamReader r26 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x08c6, all -> 0x08c3 }
                java.io.InputStream r27 = r9.getInputStream()     // Catch:{ Exception -> 0x08c6, all -> 0x08c3 }
                r26.<init>(r27)     // Catch:{ Exception -> 0x08c6, all -> 0x08c3 }
                r27 = 8000(0x1f40, float:1.121E-41)
                r0 = r6
                r1 = r26
                r2 = r27
                r0.<init>(r1, r2)     // Catch:{ Exception -> 0x08c6, all -> 0x08c3 }
                java.lang.String r24 = r6.readLine()     // Catch:{ Exception -> 0x0828 }
            L_0x04c0:
                if (r24 != 0) goto L_0x06ab
                java.lang.String r26 = ""
                r0 = r20
                r1 = r26
                boolean r26 = r0.equals(r1)     // Catch:{ Exception -> 0x0828 }
                if (r26 != 0) goto L_0x069d
                org.json.JSONObject r10 = new org.json.JSONObject     // Catch:{ Exception -> 0x0818 }
                r0 = r10
                r1 = r20
                r0.<init>(r1)     // Catch:{ Exception -> 0x0818 }
                java.lang.String r26 = "urls"
                r0 = r10
                r1 = r26
                org.json.JSONObject r25 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x0818 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r26 = r0
                java.lang.String r27 = "config"
                r0 = r25
                r1 = r27
                java.lang.String r27 = r0.getString(r1)     // Catch:{ Exception -> 0x0818 }
                r26.configServer = r27     // Catch:{ Exception -> 0x0818 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r26 = r0
                java.lang.String r27 = "ads"
                r0 = r25
                r1 = r27
                java.lang.String r27 = r0.getString(r1)     // Catch:{ Exception -> 0x0818 }
                r26.adServer = r27     // Catch:{ Exception -> 0x0818 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r26 = r0
                java.lang.String r27 = "analytics"
                r0 = r25
                r1 = r27
                java.lang.String r27 = r0.getString(r1)     // Catch:{ Exception -> 0x0818 }
                r26.analyticsServer = r27     // Catch:{ Exception -> 0x0818 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r26 = r0
                java.lang.String r27 = "vc"
                r0 = r25
                r1 = r27
                java.lang.String r27 = r0.getString(r1)     // Catch:{ Exception -> 0x0818 }
                r26.vcServer = r27     // Catch:{ Exception -> 0x0818 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r26 = r0
                java.lang.String r27 = "feedback"
                r0 = r25
                r1 = r27
                java.lang.String r27 = r0.getString(r1)     // Catch:{ Exception -> 0x0818 }
                r26.feedbackServer = r27     // Catch:{ Exception -> 0x0818 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r26 = r0
                java.lang.String r27 = "idle_timeout"
                r0 = r10
                r1 = r27
                int r27 = r0.getInt(r1)     // Catch:{ Exception -> 0x0818 }
                r0 = r27
                int r0 = r0 * 1000
                r27 = r0
                r26.idleTimeout = r27     // Catch:{ Exception -> 0x0818 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x08cc }
                r26 = r0
                java.lang.String r27 = "poll_time"
                r0 = r10
                r1 = r27
                int r27 = r0.getInt(r1)     // Catch:{ Exception -> 0x08cc }
                r0 = r27
                int r0 = r0 * 1000
                r27 = r0
                r26.pollTime = r27     // Catch:{ Exception -> 0x08cc }
            L_0x056e:
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r26 = r0
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r27 = r0
                int r27 = r27.pollTime     // Catch:{ Exception -> 0x0818 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r28 = r0
                int r28 = r28.idleTimeout     // Catch:{ Exception -> 0x0818 }
                int r27 = java.lang.Math.min(r27, r28)     // Catch:{ Exception -> 0x0818 }
                r26.pollTime = r27     // Catch:{ Exception -> 0x0818 }
                java.util.HashMap r18 = new java.util.HashMap     // Catch:{ Exception -> 0x0818 }
                r18.<init>()     // Catch:{ Exception -> 0x0818 }
                java.lang.String r26 = "ConfigServer"
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r27 = r0
                java.lang.String r27 = r27.configServer     // Catch:{ Exception -> 0x0818 }
                r0 = r18
                r1 = r26
                r2 = r27
                r0.put(r1, r2)     // Catch:{ Exception -> 0x0818 }
                java.lang.String r26 = "AdServer"
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r27 = r0
                java.lang.String r27 = r27.adServer     // Catch:{ Exception -> 0x0818 }
                r0 = r18
                r1 = r26
                r2 = r27
                r0.put(r1, r2)     // Catch:{ Exception -> 0x0818 }
                java.lang.String r26 = "AnalyticsServer"
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r27 = r0
                java.lang.String r27 = r27.analyticsServer     // Catch:{ Exception -> 0x0818 }
                r0 = r18
                r1 = r26
                r2 = r27
                r0.put(r1, r2)     // Catch:{ Exception -> 0x0818 }
                java.lang.String r26 = "VcServer"
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r27 = r0
                java.lang.String r27 = r27.vcServer     // Catch:{ Exception -> 0x0818 }
                r0 = r18
                r1 = r26
                r2 = r27
                r0.put(r1, r2)     // Catch:{ Exception -> 0x0818 }
                java.lang.String r26 = "FeedbackServer"
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r27 = r0
                java.lang.String r27 = r27.feedbackServer     // Catch:{ Exception -> 0x0818 }
                r0 = r18
                r1 = r26
                r2 = r27
                r0.put(r1, r2)     // Catch:{ Exception -> 0x0818 }
                java.lang.String r26 = "idleTimeout"
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r27 = r0
                int r27 = r27.idleTimeout     // Catch:{ Exception -> 0x0818 }
                java.lang.String r27 = java.lang.Integer.toString(r27)     // Catch:{ Exception -> 0x0818 }
                r0 = r18
                r1 = r26
                r2 = r27
                r0.put(r1, r2)     // Catch:{ Exception -> 0x0818 }
                java.lang.String r26 = "pollTime"
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r27 = r0
                int r27 = r27.pollTime     // Catch:{ Exception -> 0x0818 }
                java.lang.String r27 = java.lang.Integer.toString(r27)     // Catch:{ Exception -> 0x0818 }
                r0 = r18
                r1 = r26
                r2 = r27
                r0.put(r1, r2)     // Catch:{ Exception -> 0x0818 }
                java.lang.String r26 = "ad_units"
                r0 = r10
                r1 = r26
                org.json.JSONArray r4 = r0.getJSONArray(r1)     // Catch:{ Exception -> 0x0818 }
                r14 = 0
            L_0x0639:
                int r26 = r4.length()     // Catch:{ Exception -> 0x0818 }
                r0 = r14
                r1 = r26
                if (r0 < r1) goto L_0x06c6
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r26 = r0
                java.lang.String r26 = r26.previousDeviceId     // Catch:{ Exception -> 0x0818 }
                if (r26 == 0) goto L_0x065d
                java.lang.String r26 = "deviceId"
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r27 = r0
                java.lang.String r27 = r27.deviceId     // Catch:{ Exception -> 0x0818 }
                com.mobclix.android.sdk.Mobclix.addPref(r26, r27)     // Catch:{ Exception -> 0x0818 }
            L_0x065d:
                java.lang.String r26 = "offlineSessions"
                java.lang.String r27 = "0"
                r0 = r18
                r1 = r26
                r2 = r27
                r0.put(r1, r2)     // Catch:{ Exception -> 0x0818 }
                java.lang.String r26 = "totalSessionTime"
                java.lang.String r27 = "0"
                r0 = r18
                r1 = r26
                r2 = r27
                r0.put(r1, r2)     // Catch:{ Exception -> 0x0818 }
                java.lang.String r26 = "totalIdleTime"
                java.lang.String r27 = "0"
                r0 = r18
                r1 = r26
                r2 = r27
                r0.put(r1, r2)     // Catch:{ Exception -> 0x0818 }
                com.mobclix.android.sdk.Mobclix.addPref(r18)     // Catch:{ Exception -> 0x0818 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r26 = r0
                r27 = 0
                r26.isOfflineSession = r27     // Catch:{ Exception -> 0x0818 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r26 = r0
                r27 = 1
                r26.remoteConfigSet = r27     // Catch:{ Exception -> 0x0818 }
            L_0x069d:
                r6.close()     // Catch:{ Exception -> 0x08b5 }
            L_0x06a0:
                if (r9 == 0) goto L_0x06a5
                r9.disconnect()
            L_0x06a5:
                if (r19 == 0) goto L_0x01a3
                r19 = 0
                goto L_0x0191
            L_0x06ab:
                java.lang.StringBuilder r26 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0828 }
                java.lang.String r27 = java.lang.String.valueOf(r20)     // Catch:{ Exception -> 0x0828 }
                r26.<init>(r27)     // Catch:{ Exception -> 0x0828 }
                r0 = r26
                r1 = r24
                java.lang.StringBuilder r26 = r0.append(r1)     // Catch:{ Exception -> 0x0828 }
                java.lang.String r20 = r26.toString()     // Catch:{ Exception -> 0x0828 }
                java.lang.String r24 = r6.readLine()     // Catch:{ Exception -> 0x0828 }
                goto L_0x04c0
            L_0x06c6:
                org.json.JSONObject r3 = r4.getJSONObject(r14)     // Catch:{ Exception -> 0x0818 }
                java.lang.String r26 = "size"
                r0 = r3
                r1 = r26
                java.lang.String r22 = r0.getString(r1)     // Catch:{ Exception -> 0x0818 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r26 = r0
                java.util.HashMap r26 = r26.enabled     // Catch:{ Exception -> 0x0818 }
                java.lang.String r27 = "enabled"
                r0 = r3
                r1 = r27
                boolean r27 = r0.getBoolean(r1)     // Catch:{ Exception -> 0x0818 }
                java.lang.Boolean r27 = java.lang.Boolean.valueOf(r27)     // Catch:{ Exception -> 0x0818 }
                r0 = r26
                r1 = r22
                r2 = r27
                r0.put(r1, r2)     // Catch:{ Exception -> 0x0818 }
                java.lang.String r26 = "refresh"
                r0 = r3
                r1 = r26
                long r26 = r0.getLong(r1)     // Catch:{ Exception -> 0x0818 }
                r28 = -1
                int r26 = (r26 > r28 ? 1 : (r26 == r28 ? 0 : -1))
                if (r26 != 0) goto L_0x07f2
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r26 = r0
                java.util.HashMap r26 = r26.refreshTime     // Catch:{ Exception -> 0x0818 }
                r27 = -1
                java.lang.Long r27 = java.lang.Long.valueOf(r27)     // Catch:{ Exception -> 0x0818 }
                r0 = r26
                r1 = r22
                r2 = r27
                r0.put(r1, r2)     // Catch:{ Exception -> 0x0818 }
            L_0x071b:
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r26 = r0
                java.util.HashMap r26 = r26.autoplay     // Catch:{ Exception -> 0x0818 }
                java.lang.String r27 = "autoplay"
                r0 = r3
                r1 = r27
                boolean r27 = r0.getBoolean(r1)     // Catch:{ Exception -> 0x0818 }
                java.lang.Boolean r27 = java.lang.Boolean.valueOf(r27)     // Catch:{ Exception -> 0x0818 }
                r0 = r26
                r1 = r22
                r2 = r27
                r0.put(r1, r2)     // Catch:{ Exception -> 0x0818 }
                java.lang.StringBuilder r26 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0818 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r27 = r0
                java.util.HashMap r27 = r27.enabled     // Catch:{ Exception -> 0x0818 }
                r0 = r27
                r1 = r22
                java.lang.Object r5 = r0.get(r1)     // Catch:{ Exception -> 0x0818 }
                java.lang.Boolean r5 = (java.lang.Boolean) r5     // Catch:{ Exception -> 0x0818 }
                boolean r27 = r5.booleanValue()     // Catch:{ Exception -> 0x0818 }
                java.lang.String r27 = java.lang.Boolean.toString(r27)     // Catch:{ Exception -> 0x0818 }
                java.lang.String r27 = java.lang.String.valueOf(r27)     // Catch:{ Exception -> 0x0818 }
                r26.<init>(r27)     // Catch:{ Exception -> 0x0818 }
                java.lang.String r27 = ","
                java.lang.StringBuilder r26 = r26.append(r27)     // Catch:{ Exception -> 0x0818 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r27 = r0
                java.util.HashMap r27 = r27.refreshTime     // Catch:{ Exception -> 0x0818 }
                r0 = r27
                r1 = r22
                java.lang.Object r5 = r0.get(r1)     // Catch:{ Exception -> 0x0818 }
                java.lang.Long r5 = (java.lang.Long) r5     // Catch:{ Exception -> 0x0818 }
                java.lang.String r27 = r5.toString()     // Catch:{ Exception -> 0x0818 }
                java.lang.StringBuilder r26 = r26.append(r27)     // Catch:{ Exception -> 0x0818 }
                java.lang.String r27 = ","
                java.lang.StringBuilder r26 = r26.append(r27)     // Catch:{ Exception -> 0x0818 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r27 = r0
                java.util.HashMap r27 = r27.autoplay     // Catch:{ Exception -> 0x0818 }
                r0 = r27
                r1 = r22
                java.lang.Object r5 = r0.get(r1)     // Catch:{ Exception -> 0x0818 }
                java.lang.Boolean r5 = (java.lang.Boolean) r5     // Catch:{ Exception -> 0x0818 }
                boolean r27 = r5.booleanValue()     // Catch:{ Exception -> 0x0818 }
                java.lang.String r27 = java.lang.Boolean.toString(r27)     // Catch:{ Exception -> 0x0818 }
                java.lang.StringBuilder r26 = r26.append(r27)     // Catch:{ Exception -> 0x0818 }
                java.lang.String r26 = r26.toString()     // Catch:{ Exception -> 0x0818 }
                r0 = r18
                r1 = r22
                r2 = r26
                r0.put(r1, r2)     // Catch:{ Exception -> 0x0818 }
                java.lang.String r26 = "customAdUrl"
                r0 = r3
                r1 = r26
                java.lang.String r8 = r0.getString(r1)     // Catch:{ Exception -> 0x0869 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0869 }
                r26 = r0
                java.util.HashMap r26 = r26.customAdUrl     // Catch:{ Exception -> 0x0869 }
                r0 = r26
                r1 = r22
                java.lang.Object r26 = r0.get(r1)     // Catch:{ Exception -> 0x0869 }
                r0 = r8
                r1 = r26
                boolean r26 = r0.equals(r1)     // Catch:{ Exception -> 0x0869 }
                if (r26 == 0) goto L_0x0840
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0869 }
                r26 = r0
                java.util.HashMap r26 = r26.customAdUrl     // Catch:{ Exception -> 0x0869 }
                java.lang.String r27 = ""
                r0 = r26
                r1 = r22
                r2 = r27
                r0.put(r1, r2)     // Catch:{ Exception -> 0x0869 }
            L_0x07ee:
                int r14 = r14 + 1
                goto L_0x0639
            L_0x07f2:
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r26 = r0
                java.util.HashMap r26 = r26.refreshTime     // Catch:{ Exception -> 0x0818 }
                java.lang.String r27 = "refresh"
                r0 = r3
                r1 = r27
                long r27 = r0.getLong(r1)     // Catch:{ Exception -> 0x0818 }
                r29 = 1000(0x3e8, double:4.94E-321)
                long r27 = r27 * r29
                java.lang.Long r27 = java.lang.Long.valueOf(r27)     // Catch:{ Exception -> 0x0818 }
                r0 = r26
                r1 = r22
                r2 = r27
                r0.put(r1, r2)     // Catch:{ Exception -> 0x0818 }
                goto L_0x071b
            L_0x0818:
                r26 = move-exception
                r13 = r26
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0828 }
                r26 = r0
                r27 = -1
                r26.remoteConfigSet = r27     // Catch:{ Exception -> 0x0828 }
                goto L_0x069d
            L_0x0828:
                r26 = move-exception
                r13 = r26
            L_0x082b:
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ all -> 0x0883 }
                r26 = r0
                r27 = -1
                r26.remoteConfigSet = r27     // Catch:{ all -> 0x0883 }
                r6.close()     // Catch:{ Exception -> 0x089b }
            L_0x0839:
                if (r9 == 0) goto L_0x06a5
                r9.disconnect()
                goto L_0x06a5
            L_0x0840:
                java.lang.StringBuilder r26 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0869 }
                java.lang.String r27 = java.lang.String.valueOf(r22)     // Catch:{ Exception -> 0x0869 }
                r26.<init>(r27)     // Catch:{ Exception -> 0x0869 }
                java.lang.String r27 = "CustomAdUrl"
                java.lang.StringBuilder r26 = r26.append(r27)     // Catch:{ Exception -> 0x0869 }
                java.lang.String r26 = r26.toString()     // Catch:{ Exception -> 0x0869 }
                com.mobclix.android.sdk.Mobclix.removePref(r26)     // Catch:{ Exception -> 0x0869 }
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0869 }
                r26 = r0
                java.util.HashMap r26 = r26.customAdUrl     // Catch:{ Exception -> 0x0869 }
                r0 = r26
                r1 = r22
                r2 = r8
                r0.put(r1, r2)     // Catch:{ Exception -> 0x0869 }
                goto L_0x07ee
            L_0x0869:
                r26 = move-exception
                r13 = r26
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x0818 }
                r26 = r0
                java.util.HashMap r26 = r26.customAdUrl     // Catch:{ Exception -> 0x0818 }
                java.lang.String r27 = ""
                r0 = r26
                r1 = r22
                r2 = r27
                r0.put(r1, r2)     // Catch:{ Exception -> 0x0818 }
                goto L_0x07ee
            L_0x0883:
                r26 = move-exception
            L_0x0884:
                r6.close()     // Catch:{ Exception -> 0x08a8 }
            L_0x0887:
                if (r9 == 0) goto L_0x088c
                r9.disconnect()
            L_0x088c:
                throw r26
            L_0x088d:
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this     // Catch:{ Exception -> 0x08c6, all -> 0x08c3 }
                r26 = r0
                r27 = -1
                r26.remoteConfigSet = r27     // Catch:{ Exception -> 0x08c6, all -> 0x08c3 }
                r6 = r7
                goto L_0x069d
            L_0x089b:
                r13 = move-exception
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r26 = r0
                r27 = -1
                r26.remoteConfigSet = r27
                goto L_0x0839
            L_0x08a8:
                r13 = move-exception
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r27 = r0
                r28 = -1
                r27.remoteConfigSet = r28
                goto L_0x0887
            L_0x08b5:
                r13 = move-exception
                r0 = r32
                com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.this
                r26 = r0
                r27 = -1
                r26.remoteConfigSet = r27
                goto L_0x06a0
            L_0x08c3:
                r26 = move-exception
                r6 = r7
                goto L_0x0884
            L_0x08c6:
                r26 = move-exception
                r13 = r26
                r6 = r7
                goto L_0x082b
            L_0x08cc:
                r26 = move-exception
                goto L_0x056e
            L_0x08cf:
                r26 = move-exception
                goto L_0x0119
            L_0x08d2:
                r26 = move-exception
                goto L_0x0010
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.Mobclix.FetchRemoteConfig.run():void");
        }

        private String getConfigUrl(boolean usePref) {
            String c = Mobclix.this.configServer;
            StringBuffer data = new StringBuffer();
            try {
                if (Mobclix.hasPref("ConfigServer") && usePref) {
                    c = Mobclix.getPref("ConfigServer");
                }
                data.append(c);
                data.append("?p=android");
                data.append("&a=").append(URLEncoder.encode(Mobclix.controller.getApplicationId(), "UTF-8"));
                data.append("&m=").append(URLEncoder.encode(Mobclix.controller.getMobclixVersion()));
                data.append("&v=").append(URLEncoder.encode(Mobclix.this.applicationVersion, "UTF-8"));
                data.append("&d=").append(URLEncoder.encode(Mobclix.controller.getDeviceId(), "UTF-8"));
                data.append("&dm=").append(URLEncoder.encode(Mobclix.controller.getDeviceModel(), "UTF-8"));
                data.append("&dv=").append(URLEncoder.encode(Mobclix.this.androidVersion, "UTF-8"));
                data.append("&hwdm=").append(URLEncoder.encode(Mobclix.controller.getDeviceHardwareModel(), "UTF-8"));
                data.append("&g=").append(URLEncoder.encode(Mobclix.this.connectionType, "UTF-8"));
                if (!Mobclix.this.getGPS().equals("null")) {
                    data.append("&ll=").append(URLEncoder.encode(Mobclix.this.getGPS(), "UTF-8"));
                }
                if (Mobclix.hasPref("offlineSessions")) {
                    try {
                        data.append("&off=").append(Mobclix.getPref("offlineSessions"));
                    } catch (Exception e) {
                    }
                }
                if (Mobclix.hasPref("totalSessionTime")) {
                    try {
                        data.append("&st=").append(Mobclix.getPref("totalSessionTime"));
                    } catch (Exception e2) {
                    }
                }
                if (Mobclix.hasPref("totalIdleTime")) {
                    try {
                        data.append("&it=").append(Mobclix.getPref("totalIdleTime"));
                    } catch (Exception e3) {
                    }
                }
                if (Mobclix.this.previousDeviceId != null) {
                    data.append("&pd=").append(URLEncoder.encode(Mobclix.this.previousDeviceId, "UTF-8"));
                }
                data.append("&mcc=").append(URLEncoder.encode(Mobclix.this.mcc, "UTF-8"));
                data.append("&mnc=").append(URLEncoder.encode(Mobclix.this.mnc, "UTF-8"));
                if (Mobclix.this.isNewUser) {
                    data.append("&new=true");
                }
                return data.toString();
            } catch (Exception e4) {
                return "";
            }
        }

        private void downloadCustomImages() {
            for (String s : Mobclix.MC_AD_SIZES) {
                String c = (String) Mobclix.this.customAdUrl.get(s);
                if (!c.equals("")) {
                    try {
                        DefaultHttpClient httpClient = new DefaultHttpClient();
                        HttpGet httpGet = new HttpGet(c);
                        httpGet.setHeader("Cookie", Mobclix.getCookieStringFromCookieManager(c));
                        HttpEntity httpEntity = httpClient.execute(httpGet).getEntity();
                        Mobclix.syncCookiesToCookieManager(httpClient.getCookieStore(), c);
                        Bitmap bmImg = BitmapFactory.decodeStream(httpEntity.getContent());
                        FileOutputStream fos = Mobclix.this.activity.openFileOutput(String.valueOf(s) + Mobclix.MC_CUSTOM_AD_FILENAME, 0);
                        bmImg.compress(Bitmap.CompressFormat.PNG, 90, fos);
                        fos.close();
                        Mobclix.addPref(String.valueOf(s) + Mobclix.MC_CUSTOM_AD_PREF, (String) Mobclix.this.customAdUrl.get(s));
                        Mobclix.this.customAdSet.put(s, true);
                    } catch (Exception e) {
                    }
                } else {
                    Mobclix.this.customAdSet.put(s, true);
                }
            }
        }
    }

    static String getCookieStringFromCookieManager(String url) {
        try {
            return CookieManager.getInstance().getCookie(url);
        } catch (Exception e) {
            return "";
        }
    }

    static void syncCookiesToCookieManager(CookieStore cs, String url) {
        try {
            CookieManager cookieManager = CookieManager.getInstance();
            List<Cookie> cookies = cs.getCookies();
            StringBuffer cookieStringBuffer = new StringBuffer();
            if (!cookies.isEmpty()) {
                for (int i = 0; i < cookies.size(); i++) {
                    Cookie c = (Cookie) cookies.get(i);
                    cookieStringBuffer.append(c.getName()).append("=").append(c.getValue());
                    if (c.getExpiryDate() != null) {
                        cookieStringBuffer.append("; expires=").append(new SimpleDateFormat("E, dd-MMM-yyyy HH:mm:ss").format(c.getExpiryDate())).append(" GMT");
                    }
                    if (c.getPath() != null) {
                        cookieStringBuffer.append("; path=").append(c.getPath());
                    }
                    if (c.getDomain() != null) {
                        cookieStringBuffer.append("; domain=").append(c.getDomain());
                    }
                    cookieManager.setCookie(url, cookieStringBuffer.toString());
                }
                CookieSyncManager.getInstance().sync();
            }
        } catch (Exception e) {
        }
    }

    static class BitmapHandler extends Handler {
        protected Bitmap bmImg = null;
        protected Object state = null;

        BitmapHandler() {
        }

        public void setBitmap(Bitmap bm) {
            this.bmImg = bm;
        }

        public void setState(Object o) {
            this.state = o;
        }
    }

    static class FetchImageThread implements Runnable {
        private Bitmap bmImg;
        private BitmapHandler handler;
        private String imageUrl;

        FetchImageThread(String url, BitmapHandler h) {
            this.imageUrl = url;
            this.handler = h;
        }

        public void run() {
            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(this.imageUrl);
                httpGet.setHeader("Cookie", Mobclix.getCookieStringFromCookieManager(this.imageUrl));
                HttpEntity httpEntity = httpClient.execute(httpGet).getEntity();
                Mobclix.syncCookiesToCookieManager(httpClient.getCookieStore(), this.imageUrl);
                this.bmImg = BitmapFactory.decodeStream(httpEntity.getContent());
                httpEntity.consumeContent();
                this.handler.setBitmap(this.bmImg);
            } catch (Exception e) {
            }
            this.handler.sendEmptyMessage(0);
        }
    }

    static class FetchResponseThread extends TimerTask implements Runnable {
        private Handler handler;
        private String url;

        FetchResponseThread(String u, Handler h) {
            this.url = u;
            this.handler = h;
        }

        public void run() {
            int errorCode;
            Mobclix.controller.updateSession();
            if (this.url.equals("")) {
                sendErrorCode(MobclixAdViewListener.UNAVAILABLE);
            }
            String response = "";
            BufferedReader br = null;
            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(this.url);
                httpGet.setHeader("Cookie", Mobclix.getCookieStringFromCookieManager(this.url));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                int responseCode = httpResponse.getStatusLine().getStatusCode();
                Mobclix.syncCookiesToCookieManager(httpClient.getCookieStore(), this.url);
                if ((responseCode != 200 && responseCode != 251) || httpEntity == null) {
                    switch (responseCode) {
                        case 251:
                            String sub = httpResponse.getFirstHeader("X-Mobclix-Suballocation").getValue();
                            if (sub == null) {
                                errorCode = MobclixAdViewListener.UNAVAILABLE;
                            } else {
                                errorCode = Integer.parseInt(sub);
                            }
                            sendErrorCode(errorCode);
                            break;
                        default:
                            errorCode = MobclixAdViewListener.UNAVAILABLE;
                            sendErrorCode(errorCode);
                            break;
                    }
                } else {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpEntity.getContent()), 8000);
                    try {
                        for (String tmp = bufferedReader.readLine(); tmp != null; tmp = bufferedReader.readLine()) {
                            response = String.valueOf(response) + tmp;
                        }
                        httpEntity.consumeContent();
                        if (!response.equals("")) {
                            Message msg = new Message();
                            Bundle bundle = new Bundle();
                            bundle.putString("type", "success");
                            bundle.putString("response", response);
                            msg.setData(bundle);
                            this.handler.sendMessage(msg);
                            br = bufferedReader;
                        } else {
                            br = bufferedReader;
                        }
                    } catch (Exception e) {
                        br = bufferedReader;
                    } catch (Throwable th) {
                        th = th;
                        br = bufferedReader;
                        try {
                            br.close();
                        } catch (Exception e2) {
                        }
                        throw th;
                    }
                }
                try {
                    br.close();
                    return;
                } catch (Exception e3) {
                    return;
                }
            } catch (Exception e4) {
            }
            try {
                sendErrorCode(MobclixAdViewListener.UNAVAILABLE);
                try {
                    br.close();
                } catch (Exception e5) {
                }
            } catch (Throwable th2) {
                th = th2;
                br.close();
                throw th;
            }
        }

        /* access modifiers changed from: package-private */
        public void setUrl(String u) {
            this.url = u;
        }

        private void sendErrorCode(int errorCode) {
            Message msg = new Message();
            Bundle bundle = new Bundle();
            bundle.putString("type", "failure");
            bundle.putInt("errorCode", errorCode);
            msg.setData(bundle);
            this.handler.sendMessage(msg);
        }
    }
}
