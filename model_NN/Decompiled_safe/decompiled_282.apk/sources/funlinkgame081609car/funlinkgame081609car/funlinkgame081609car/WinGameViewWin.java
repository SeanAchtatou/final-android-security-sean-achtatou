package funlinkgame081609car.funlinkgame081609car.funlinkgame081609car;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import funlinkgame081609car.funlinkgame081609car.funlinkgame081609car.GameCtrlViewWin;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class WinGameViewWin extends View {
    public final int H_LINE = 1;
    public final int ONE_C_LINE = 2;
    public final int TWO_C_LINE = 3;
    public final int V_LINE = 1;
    public final int col = 10;
    public int[][] grid = ((int[][]) Array.newInstance(Integer.TYPE, 10, 10));
    public float height;
    public Bitmap[] image;
    public int[] imageType = {R.drawable.p01, R.drawable.p02, R.drawable.p03, R.drawable.p04, R.drawable.p05, R.drawable.p06, R.drawable.p07, R.drawable.p08, R.drawable.p09, R.drawable.p10, R.drawable.p11, R.drawable.p12, R.drawable.p13, R.drawable.p14, R.drawable.p15, R.drawable.p16};
    public boolean isLine = false;
    public int lineType = 0;
    public int much = 0;
    GameCtrlViewWin.Point[] p;
    public final int row = 10;
    private Rect selRect = new Rect();
    private int selX;
    private int selY;
    public List<Integer> type = new ArrayList();
    public float width;

    public WinGameViewWin(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    public WinGameViewWin(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setBackgroundResource(R.drawable.background);
        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    public void reset() {
    }

    public void fillImage(Context context) {
        int lth = this.imageType.length;
        this.image = new Bitmap[lth];
        for (int i = 0; i < lth; i++) {
            Bitmap bitmap = Bitmap.createBitmap((int) this.width, (int) this.height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            Drawable drw = context.getResources().getDrawable(this.imageType[i]);
            drw.setBounds(1, 1, 30, 45);
            drw.draw(canvas);
            this.image[i] = bitmap;
        }
    }

    public void initType() {
        int count = 64 / this.imageType.length;
        for (int j = 0; j < this.imageType.length; j++) {
            for (int i = 0; i < count; i++) {
                this.type.add(Integer.valueOf(this.imageType[j]));
            }
        }
    }

    public void select(int x, int y) {
        invalidate(this.selRect);
        this.selX = Math.min(Math.max(x, 0), 9);
        this.selY = Math.min(Math.max(y, 0), 9);
        getRect(this.selX, this.selY, this.selRect);
        invalidate(this.selRect);
    }

    private void getRect(int x, int y, Rect rect) {
        rect.set((int) (((float) x) * this.width), (int) (((float) y) * this.height), (int) ((((float) x) * this.width) + this.width), (int) ((((float) y) * this.height) + this.height));
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Paint background = new Paint();
        background.setColor(0);
        canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), background);
        Paint hilite = new Paint();
        hilite.setColor(getResources().getColor(R.color.hilite));
        Paint light = new Paint();
        light.setColor(getResources().getColor(R.color.light));
        for (int i = 0; i <= 9; i++) {
            canvas.drawLine(0.0f, ((float) i) * this.height, (float) getWidth(), ((float) i) * this.height, light);
            canvas.drawLine(0.0f, (((float) i) * this.height) + 1.0f, (float) getWidth(), (((float) i) * this.height) + 1.0f, hilite);
            canvas.drawLine(((float) i) * this.width, 0.0f, ((float) i) * this.width, (float) getHeight(), light);
            canvas.drawLine((((float) i) * this.width) + 1.0f, 0.0f, (((float) i) * this.width) + 1.0f, (float) getHeight(), hilite);
        }
        if (GameCtrlViewWin.CURRENT_CH) {
            Paint selected = new Paint();
            selected.setColor(getResources().getColor(R.color.puzzle_selected));
            canvas.drawRect(this.selRect, selected);
        }
        for (int i2 = 0; i2 < 9; i2++) {
            for (int j = 0; j < 9; j++) {
                if (this.grid[i2][j] != 0) {
                    canvas.drawBitmap(this.image[Arrays.binarySearch(this.imageType, this.grid[i2][j])], ((float) i2) * this.width, ((float) j) * this.height, (Paint) null);
                }
            }
        }
        if (this.isLine) {
            Paint lineColor = new Paint();
            lineColor.setColor(-65536);
            switch (this.lineType) {
                case 1:
                    canvas.drawLine((((float) this.p[0].x) * this.width) + (this.width / 2.0f), (((float) this.p[0].y) * this.height) + (this.height / 2.0f), (((float) this.p[1].x) * this.width) + (this.width / 2.0f), (((float) this.p[1].y) * this.height) + (this.height / 2.0f), lineColor);
                    break;
                case 2:
                    canvas.drawLine((((float) this.p[0].x) * this.width) + (this.width / 2.0f), (((float) this.p[0].y) * this.height) + (this.height / 2.0f), (((float) this.p[1].x) * this.width) + (this.width / 2.0f), (((float) this.p[1].y) * this.height) + (this.height / 2.0f), lineColor);
                    canvas.drawLine((((float) this.p[1].x) * this.width) + (this.width / 2.0f), (((float) this.p[1].y) * this.height) + (this.height / 2.0f), (((float) this.p[2].x) * this.width) + (this.width / 2.0f), (((float) this.p[2].y) * this.height) + (this.height / 2.0f), lineColor);
                    break;
                case 3:
                    canvas.drawLine((((float) this.p[0].x) * this.width) + (this.width / 2.0f), (((float) this.p[0].y) * this.height) + (this.height / 2.0f), (((float) this.p[1].x) * this.width) + (this.width / 2.0f), (((float) this.p[1].y) * this.height) + (this.height / 2.0f), lineColor);
                    canvas.drawLine((((float) this.p[1].x) * this.width) + (this.width / 2.0f), (((float) this.p[1].y) * this.height) + (this.height / 2.0f), (((float) this.p[2].x) * this.width) + (this.width / 2.0f), (((float) this.p[2].y) * this.height) + (this.height / 2.0f), lineColor);
                    canvas.drawLine((((float) this.p[3].x) * this.width) + (this.width / 2.0f), (((float) this.p[3].y) * this.height) + (this.height / 2.0f), (((float) this.p[2].x) * this.width) + (this.width / 2.0f), (((float) this.p[2].y) * this.height) + (this.height / 2.0f), lineColor);
                    break;
            }
        }
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.width = (float) (w / 10);
        this.height = (float) (h / 10);
        fillImage(getContext());
        super.onSizeChanged(w, h, oldw, oldh);
    }

    public void initGrid() {
        Random ad = new Random();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (i == 0 || i == 9 || j == 0 || j == 9) {
                    this.grid[i][j] = 0;
                } else if (this.type != null && this.type.size() > 0) {
                    int index = ad.nextInt(this.type.size());
                    this.grid[i][j] = this.type.get(index).intValue();
                    this.type.remove(index);
                }
            }
        }
    }
}
