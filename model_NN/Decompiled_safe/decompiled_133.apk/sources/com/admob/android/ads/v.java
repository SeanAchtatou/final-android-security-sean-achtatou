package com.admob.android.ads;

import android.content.Context;
import android.media.AudioManager;

/* compiled from: AudioManagerWrapper */
public class v {
    public AudioManager a;

    public v(Context context) {
        this.a = (AudioManager) context.getSystemService("audio");
    }

    public int a() {
        return this.a.getMode();
    }

    public int d() {
        return this.a.getRingerMode();
    }

    public boolean b() {
        return this.a.isMusicActive();
    }

    public boolean c() {
        return this.a.isSpeakerphoneOn();
    }
}
