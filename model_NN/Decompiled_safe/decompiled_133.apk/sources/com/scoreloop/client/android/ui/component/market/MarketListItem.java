package com.scoreloop.client.android.ui.component.market;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import com.skyd.bestpuzzle.n1669.R;

public class MarketListItem extends StandardListItem<Void> {
    private Integer _counter;
    private String _imageUrl;

    public static class MarketViewHolder extends StandardListItem.StandardViewHolder {
        TextView number;
    }

    public MarketListItem(ComponentActivity activity, Drawable drawable, String title, String subTitle) {
        super(activity, drawable, title, subTitle, null);
    }

    public void setCounter(Integer counter) {
        this._counter = counter;
    }

    /* access modifiers changed from: protected */
    public StandardListItem.StandardViewHolder createViewHolder() {
        return new MarketViewHolder();
    }

    /* access modifiers changed from: protected */
    public void fillViewHolder(View view, StandardListItem.StandardViewHolder holder) {
        super.fillViewHolder(view, holder);
        ((MarketViewHolder) holder).number = (TextView) view.findViewById(R.id.sl_number);
    }

    /* access modifiers changed from: protected */
    public void updateViews(StandardListItem.StandardViewHolder holder) {
        super.updateViews(holder);
        MarketViewHolder marketHolder = (MarketViewHolder) holder;
        if (marketHolder.number == null) {
            return;
        }
        if (this._counter != null) {
            marketHolder.number.setText(this._counter.toString());
        } else {
            marketHolder.number.setText((CharSequence) null);
        }
    }

    /* access modifiers changed from: protected */
    public String getImageUrl() {
        return this._imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this._imageUrl = imageUrl;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.sl_list_item_market;
    }
}
