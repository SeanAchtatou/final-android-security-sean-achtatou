package com.scoreloop.client.android.ui.framework;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import com.scoreloop.client.android.ui.framework.BaseListAdapter;
import com.scoreloop.client.android.ui.framework.BaseListItem;

public class PagingListAdapter<T extends BaseListItem> extends BaseListAdapter<BaseListItem> {
    private final int _listOffset;
    private PagingListItem _nextPagingItem;
    private int _pagingFlags;
    private PagingListItem _prevPagingItem;
    private PagingListItem _topPagingItem;

    public interface OnListItemClickListener<T extends BaseListItem> extends BaseListAdapter.OnListItemClickListener<T> {
        void onPagingListItemClick(PagingDirection pagingDirection);
    }

    public PagingListAdapter(Context context) {
        super(context);
        this._pagingFlags = 0;
        this._listOffset = 0;
    }

    public PagingListAdapter(Context context, int listOffset) {
        super(context);
        this._pagingFlags = 0;
        this._listOffset = listOffset;
    }

    public void addPagingItems(boolean showTop, boolean showPrev, boolean showNext) {
        this._pagingFlags = 0;
        if (showTop) {
            insert(getTopPagingItem(), this._listOffset);
            this._pagingFlags = PagingDirection.PAGE_TO_TOP.combine(this._pagingFlags);
        }
        if (showPrev) {
            insert(getPrevPagingItem(), showTop ? this._listOffset + 1 : this._listOffset);
            this._pagingFlags = PagingDirection.PAGE_TO_PREV.combine(this._pagingFlags);
        }
        if (showNext) {
            add(getNextPagingItem());
            this._pagingFlags = PagingDirection.PAGE_TO_NEXT.combine(this._pagingFlags);
        }
    }

    public T getContentItem(int position) {
        return (BaseListItem) getItem(getFirstContentPosition() + position);
    }

    public int getFirstContentPosition() {
        int offset = this._listOffset;
        if (PagingDirection.PAGE_TO_TOP.isPresentIn(this._pagingFlags)) {
            offset++;
        }
        if (PagingDirection.PAGE_TO_PREV.isPresentIn(this._pagingFlags)) {
            return offset + 1;
        }
        return offset;
    }

    public int getLastContentPosition() {
        int offset = getCount() - 1;
        if (PagingDirection.PAGE_TO_NEXT.isPresentIn(this._pagingFlags)) {
            offset--;
        }
        return Math.max(0, offset);
    }

    private BaseListItem getNextPagingItem() {
        if (this._nextPagingItem == null) {
            this._nextPagingItem = new PagingListItem(getContext(), PagingDirection.PAGE_TO_NEXT);
        }
        return this._nextPagingItem;
    }

    private BaseListItem getPrevPagingItem() {
        if (this._prevPagingItem == null) {
            this._prevPagingItem = new PagingListItem(getContext(), PagingDirection.PAGE_TO_PREV);
        }
        return this._prevPagingItem;
    }

    private BaseListItem getTopPagingItem() {
        if (this._topPagingItem == null) {
            this._topPagingItem = new PagingListItem(getContext(), PagingDirection.PAGE_TO_TOP);
        }
        return this._topPagingItem;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        if (this._listItemClickListener != null) {
            BaseListItem item = (BaseListItem) getItem(position);
            if (item.getType() == 0) {
                ((OnListItemClickListener) this._listItemClickListener).onPagingListItemClick(((PagingListItem) item).getPagingDirection());
            } else {
                this._listItemClickListener.onListItemClick(item);
            }
        }
    }
}
