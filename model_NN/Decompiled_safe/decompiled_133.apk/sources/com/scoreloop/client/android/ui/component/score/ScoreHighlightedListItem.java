package com.scoreloop.client.android.ui.component.score;

import android.view.View;
import android.widget.TextView;
import com.scoreloop.client.android.core.model.Ranking;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import com.skyd.bestpuzzle.n1669.R;

public class ScoreHighlightedListItem extends ScoreListItem {
    private static final int ROUND_TO_PERCENT = 5;
    private Ranking _ranking;

    static class HighlightedViewHolder extends StandardListItem.StandardViewHolder {
        TextView percentText;

        HighlightedViewHolder() {
        }
    }

    public ScoreHighlightedListItem(ComponentActivity activity, Score score, Ranking ranking) {
        super(activity, score);
        this._ranking = ranking;
    }

    /* access modifiers changed from: protected */
    public StandardListItem.StandardViewHolder createViewHolder() {
        return new HighlightedViewHolder();
    }

    /* access modifiers changed from: protected */
    public void fillViewHolder(View view, StandardListItem.StandardViewHolder holder) {
        super.fillViewHolder(view, holder);
        ((HighlightedViewHolder) holder).percentText = (TextView) view.findViewById(R.id.sl_list_item_score_percent);
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.sl_list_item_score_highlighted;
    }

    private String getPercentString() {
        int total;
        if (this._ranking == null || (total = this._ranking.getTotal().intValue()) == 0) {
            return "";
        }
        return String.format(getContext().getString(R.string.sl_format_leaderboards_percent), Integer.valueOf((((((this._ranking.getRank().intValue() * 100) / total) + 5) - 1) / 5) * 5));
    }

    public int getType() {
        return 21;
    }

    public void setRanking(Ranking ranking) {
        this._ranking = ranking;
    }

    /* access modifiers changed from: protected */
    public void updateViews(StandardListItem.StandardViewHolder holder) {
        super.updateViews(holder);
        ((HighlightedViewHolder) holder).percentText.setText(getPercentString());
    }
}
