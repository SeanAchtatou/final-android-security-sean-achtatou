package com.scoreloop.client.android.ui.util;

import android.os.Handler;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class Cache<K, V> {
    private static final int DEFAULT_HARD_CACHE_CAPACITY = 100;
    private HashMap<K, Cache<K, V>.CacheEntry> _hardCache;
    /* access modifiers changed from: private */
    public int _hardCacheCapacity;
    private long _minPurgeInterval;
    private Handler _purgeHandler;
    private final Runnable _purger;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<K, SoftReference<Cache<K, V>.CacheEntry>> _softCache;

    class CacheEntry {
        private long _lastAccess;
        private long _timeToLive;
        private V _value;

        CacheEntry(V value, long lastAccess, long timeToLive) {
            this._value = value;
            this._lastAccess = lastAccess;
            this._timeToLive = timeToLive;
        }

        /* access modifiers changed from: package-private */
        public long getLastAccess() {
            return this._lastAccess;
        }

        /* access modifiers changed from: package-private */
        public void setLastAccess(long lastAccess) {
            this._lastAccess = lastAccess;
        }

        /* access modifiers changed from: package-private */
        public long getTimeToLive() {
            return this._timeToLive;
        }

        /* access modifiers changed from: package-private */
        public V getValue() {
            return this._value;
        }
    }

    public Cache() {
        this(DEFAULT_HARD_CACHE_CAPACITY);
    }

    public Cache(int hardCacheCapacity) {
        this._purger = new Runnable() {
            public void run() {
                Cache.this.purgeCache();
            }
        };
        this._hardCacheCapacity = hardCacheCapacity;
        this._minPurgeInterval = 0;
        this._purgeHandler = new Handler();
        initHardCache();
        initSoftCache();
    }

    private void initHardCache() {
        this._hardCache = new LinkedHashMap<K, Cache<K, V>.CacheEntry>(this._hardCacheCapacity / 2, 0.75f, true) {
            private static final long serialVersionUID = 1;

            /* Debug info: failed to restart local var, previous not found, register: 3 */
            /* access modifiers changed from: protected */
            public boolean removeEldestEntry(Map.Entry<K, Cache<K, V>.CacheEntry> eldest) {
                if (size() <= Cache.this._hardCacheCapacity) {
                    return false;
                }
                Cache.this._softCache.put(eldest.getKey(), new SoftReference(eldest.getValue()));
                return true;
            }
        };
    }

    private void initSoftCache() {
        this._softCache = new ConcurrentHashMap<>(this._hardCacheCapacity / 2);
    }

    public void purgeCache() {
        long now = System.currentTimeMillis();
        Set<K> hardKeys = new HashSet<>(this._hardCache.keySet());
        this._softCache.clear();
        for (K k : hardKeys) {
            Cache<K, V>.CacheEntry e = this._hardCache.get(k);
            if (e.getLastAccess() + e.getTimeToLive() < now) {
                synchronized (this._hardCache) {
                    this._softCache.put(k, new SoftReference(e));
                    this._hardCache.remove(k);
                }
            }
        }
        resetPurgeTimer(this._minPurgeInterval);
    }

    private void resetPurgeTimer(long purgeInterval) {
        if (purgeInterval > 0) {
            if (purgeInterval < this._minPurgeInterval) {
                this._minPurgeInterval = purgeInterval;
            } else if (this._minPurgeInterval == 0) {
                this._minPurgeInterval = purgeInterval;
            }
        }
        this._purgeHandler.removeCallbacks(this._purger);
        if (this._minPurgeInterval > 0) {
            this._purgeHandler.postDelayed(this._purger, this._minPurgeInterval);
        }
    }

    public void put(K key, V value, long timeToLive) {
        long now = System.currentTimeMillis();
        synchronized (this._hardCache) {
            this._hardCache.put(key, new CacheEntry(value, now, timeToLive));
        }
        resetPurgeTimer(timeToLive);
    }

    public V get(K key) {
        Cache<K, V>.CacheEntry cacheEntry = getCacheEntry(key);
        if (cacheEntry != null) {
            return cacheEntry.getValue();
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002a, code lost:
        if (r1 == null) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002c, code lost:
        r0 = r1.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0032, code lost:
        if (r0 == null) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0034, code lost:
        r4 = r6._hardCache;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0036, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r0.setLastAccess(r2);
        r6._hardCache.put(r7, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003f, code lost:
        monitor-exit(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0040, code lost:
        r6._softCache.remove(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x004d, code lost:
        r6._softCache.remove(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0052, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0022, code lost:
        r1 = r6._softCache.get(r7);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.scoreloop.client.android.ui.util.Cache<K, V>.CacheEntry getCacheEntry(K r7) {
        /*
            r6 = this;
            long r2 = java.lang.System.currentTimeMillis()
            java.util.HashMap<K, com.scoreloop.client.android.ui.util.Cache<K, V>$CacheEntry> r4 = r6._hardCache
            monitor-enter(r4)
            java.util.HashMap<K, com.scoreloop.client.android.ui.util.Cache<K, V>$CacheEntry> r5 = r6._hardCache     // Catch:{ all -> 0x0047 }
            java.lang.Object r0 = r5.get(r7)     // Catch:{ all -> 0x0047 }
            com.scoreloop.client.android.ui.util.Cache$CacheEntry r0 = (com.scoreloop.client.android.ui.util.Cache.CacheEntry) r0     // Catch:{ all -> 0x0047 }
            if (r0 == 0) goto L_0x0021
            r0.setLastAccess(r2)     // Catch:{ all -> 0x0047 }
            java.util.HashMap<K, com.scoreloop.client.android.ui.util.Cache<K, V>$CacheEntry> r5 = r6._hardCache     // Catch:{ all -> 0x0047 }
            r5.remove(r7)     // Catch:{ all -> 0x0047 }
            java.util.HashMap<K, com.scoreloop.client.android.ui.util.Cache<K, V>$CacheEntry> r5 = r6._hardCache     // Catch:{ all -> 0x0047 }
            r5.put(r7, r0)     // Catch:{ all -> 0x0047 }
            monitor-exit(r4)     // Catch:{ all -> 0x0047 }
            r4 = r0
        L_0x0020:
            return r4
        L_0x0021:
            monitor-exit(r4)     // Catch:{ all -> 0x0047 }
            java.util.concurrent.ConcurrentHashMap<K, java.lang.ref.SoftReference<com.scoreloop.client.android.ui.util.Cache<K, V>$CacheEntry>> r4 = r6._softCache
            java.lang.Object r1 = r4.get(r7)
            java.lang.ref.SoftReference r1 = (java.lang.ref.SoftReference) r1
            if (r1 == 0) goto L_0x0052
            java.lang.Object r0 = r1.get()
            com.scoreloop.client.android.ui.util.Cache$CacheEntry r0 = (com.scoreloop.client.android.ui.util.Cache.CacheEntry) r0
            if (r0 == 0) goto L_0x004d
            java.util.HashMap<K, com.scoreloop.client.android.ui.util.Cache<K, V>$CacheEntry> r4 = r6._hardCache
            monitor-enter(r4)
            r0.setLastAccess(r2)     // Catch:{ all -> 0x004a }
            java.util.HashMap<K, com.scoreloop.client.android.ui.util.Cache<K, V>$CacheEntry> r5 = r6._hardCache     // Catch:{ all -> 0x004a }
            r5.put(r7, r0)     // Catch:{ all -> 0x004a }
            monitor-exit(r4)     // Catch:{ all -> 0x004a }
            java.util.concurrent.ConcurrentHashMap<K, java.lang.ref.SoftReference<com.scoreloop.client.android.ui.util.Cache<K, V>$CacheEntry>> r4 = r6._softCache
            r4.remove(r0)
            r4 = r0
            goto L_0x0020
        L_0x0047:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0047 }
            throw r5
        L_0x004a:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x004a }
            throw r5
        L_0x004d:
            java.util.concurrent.ConcurrentHashMap<K, java.lang.ref.SoftReference<com.scoreloop.client.android.ui.util.Cache<K, V>$CacheEntry>> r4 = r6._softCache
            r4.remove(r1)
        L_0x0052:
            r4 = 0
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scoreloop.client.android.ui.util.Cache.getCacheEntry(java.lang.Object):com.scoreloop.client.android.ui.util.Cache$CacheEntry");
    }
}
