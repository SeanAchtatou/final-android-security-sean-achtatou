package com.scoreloop.client.android.ui.component.base;

import android.os.Bundle;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.ui.framework.ValueStore;

public interface ComponentActivityHooks {
    void onCreate(Bundle bundle);

    void onRefresh(int i);

    void onValueChanged(ValueStore valueStore, String str, Object obj, Object obj2);

    void onValueSetDirty(ValueStore valueStore, String str);

    void requestControllerDidReceiveResponseSafe(RequestController requestController);
}
