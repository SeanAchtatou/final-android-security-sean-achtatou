package com.scoreloop.client.android.ui.component.base;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.scoreloop.client.android.ui.util.ImageDownloader;
import com.skyd.bestpuzzle.n1669.R;

public class StandardListItem<T> extends BaseListItem {
    private StandardViewHolder _holder;
    private String _subTitel;
    private String _subTitle2;
    private final T _target;

    public static class StandardViewHolder {
        public ImageView icon;
        public TextView subTitle;
        public TextView subTitle2;
        public TextView title;
    }

    public StandardListItem(ComponentActivity activity, Drawable drawable, String title, String subTitle, T target) {
        super(activity, drawable, title);
        this._subTitel = subTitle;
        this._target = target;
    }

    public StandardListItem(ComponentActivity activity, T target) {
        super(activity, null, null);
        this._target = target;
    }

    /* access modifiers changed from: protected */
    public StandardViewHolder createViewHolder() {
        return new StandardViewHolder();
    }

    /* access modifiers changed from: protected */
    public void fillViewHolder(View view, StandardViewHolder holder) {
        int iconId = getIconId();
        if (iconId != 0) {
            holder.icon = (ImageView) view.findViewById(iconId);
        }
        holder.title = (TextView) view.findViewById(getTitleId());
        int subTitleId = getSubTitleId();
        if (subTitleId != 0) {
            holder.subTitle = (TextView) view.findViewById(subTitleId);
        }
        int subTitle2Id = getSubTitle2Id();
        if (subTitle2Id != 0) {
            holder.subTitle2 = (TextView) view.findViewById(subTitle2Id);
        }
    }

    public ComponentActivity getComponentActivity() {
        return (ComponentActivity) getContext();
    }

    /* access modifiers changed from: protected */
    public int getIconId() {
        return R.id.sl_icon;
    }

    /* access modifiers changed from: protected */
    public String getImageUrl() {
        return null;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.sl_list_item_icon_title_subtitle;
    }

    public String getSubTitle() {
        return this._subTitel;
    }

    public String getSubTitle2() {
        return this._subTitle2;
    }

    public void setSubTitle2(String subTitle2) {
        this._subTitle2 = subTitle2;
    }

    /* access modifiers changed from: protected */
    public int getSubTitleId() {
        return R.id.sl_subtitle;
    }

    /* access modifiers changed from: protected */
    public int getSubTitle2Id() {
        return 0;
    }

    public T getTarget() {
        return this._target;
    }

    /* access modifiers changed from: protected */
    public int getTitleId() {
        return R.id.sl_title;
    }

    public int getType() {
        return 22;
    }

    public View getView(View view, ViewGroup parent) {
        if (view == null) {
            view = getLayoutInflater().inflate(getLayoutId(), (ViewGroup) null);
            this._holder = createViewHolder();
            fillViewHolder(view, this._holder);
            view.setTag(this._holder);
        } else {
            this._holder = (StandardViewHolder) view.getTag();
        }
        updateViews(this._holder);
        return view;
    }

    public boolean isEnabled() {
        return true;
    }

    public void setSubTitle(String subTitle) {
        this._subTitel = subTitle;
    }

    /* access modifiers changed from: protected */
    public void updateViews(StandardViewHolder holder) {
        String imageUrl = getImageUrl();
        if (imageUrl != null) {
            ImageDownloader.downloadImage(imageUrl, getDrawableLoading(), holder.icon, getDrawableLoadingError());
        } else {
            Drawable drawable = getDrawable();
            if (drawable != null) {
                holder.icon.setImageDrawable(drawable);
            }
        }
        holder.title.setText(getTitle());
        TextView subTitle = holder.subTitle;
        if (subTitle != null) {
            subTitle.setText(getSubTitle());
        }
        TextView subTitle2 = holder.subTitle2;
        if (subTitle2 != null) {
            subTitle2.setText(getSubTitle2());
        }
    }

    /* access modifiers changed from: protected */
    public Drawable getDrawableLoading() {
        return getContext().getResources().getDrawable(R.drawable.sl_icon_games_loading);
    }

    /* access modifiers changed from: protected */
    public Drawable getDrawableLoadingError() {
        return null;
    }
}
