package com.scoreloop.client.android.ui.framework;

public interface ScreenManager {

    public interface Delegate {

        public enum Direction {
            FORWARD,
            BACKWARD,
            NONE
        }

        void screenManagerDidLeaveFramework(ScreenManager screenManager);

        boolean screenManagerWantsNewScreen(ScreenManager screenManager, ScreenDescription screenDescription, ScreenDescription screenDescription2);

        void screenManagerWillEnterFramework(ScreenManager screenManager);

        void screenManagerWillShowOptionsMenu();

        void screenManagerWillShowScreenDescription(ScreenDescription screenDescription, Direction direction);
    }

    void display(ScreenDescription screenDescription);

    void displayInScreen(ScreenDescription screenDescription, ScreenActivityProtocol screenActivityProtocol, boolean z);

    void displayPreviousDescription();

    void displayReferencedStackEntryInScreen(int i, ScreenActivityProtocol screenActivityProtocol);

    void displayStoredDescriptionInScreen(ScreenActivityProtocol screenActivityProtocol);

    void displayStoredDescriptionInTabs(TabsActivityProtocol tabsActivityProtocol);

    void displayWithEmptyStack(ScreenDescription screenDescription);

    void finishDisplay();

    ActivityDescription getActivityDescription(String str);

    ScreenDescription getCurrentDescription();

    int getCurrentStackEntryReference();

    void onShowedTab(ScreenDescription screenDescription);

    void onWillShowOptionsMenu();

    void setDelegate(Delegate delegate);
}
