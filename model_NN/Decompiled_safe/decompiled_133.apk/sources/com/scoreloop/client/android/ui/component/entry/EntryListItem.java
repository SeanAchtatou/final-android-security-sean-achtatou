package com.scoreloop.client.android.ui.component.entry;

import android.graphics.drawable.Drawable;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import com.skyd.bestpuzzle.n1669.R;

public class EntryListItem extends StandardListItem<Void> {
    public EntryListItem(ComponentActivity context, Drawable drawable, String title, String subTitle) {
        super(context, drawable, title, subTitle, null);
    }

    /* access modifiers changed from: protected */
    public int getIconId() {
        return R.id.sl_list_item_main_icon;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.sl_list_item_main;
    }

    /* access modifiers changed from: protected */
    public int getSubTitleId() {
        return R.id.sl_list_item_main_subtitle;
    }

    /* access modifiers changed from: protected */
    public int getTitleId() {
        return R.id.sl_list_item_main_title;
    }

    public int getType() {
        return 10;
    }
}
