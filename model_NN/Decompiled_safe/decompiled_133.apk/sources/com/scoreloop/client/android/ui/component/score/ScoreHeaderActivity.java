package com.scoreloop.client.android.ui.component.score;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.scoreloop.client.android.ui.component.base.ComponentHeaderActivity;
import com.scoreloop.client.android.ui.framework.ValueStore;
import com.skyd.bestpuzzle.n1669.R;

public class ScoreHeaderActivity extends ComponentHeaderActivity implements DialogInterface.OnClickListener {
    public void onClick(DialogInterface dialog, int position) {
        getScreenValues().putValue("mode", Integer.valueOf(getModeForPosition(position)));
    }

    public void onClick(View view) {
        showDialogSafe(18, true);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.sl_header_default);
        setCaption(getGame().getName());
        getImageView().setImageDrawable(getResources().getDrawable(R.drawable.sl_header_icon_leaderboards));
        setTitle(getString(R.string.sl_leaderboards));
        if (getGame().hasModes()) {
            showControlIcon(R.drawable.sl_button_more);
            updateUI();
            addObservedKeys("mode");
        }
    }

    private void showControlIcon(int resId) {
        ImageView icon = (ImageView) findViewById(R.id.sl_control_icon);
        icon.setImageResource(resId);
        icon.setEnabled(true);
        icon.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 18:
                AlertDialog dialog = new AlertDialog.Builder(this).setItems(getConfiguration().getModesResId(), this).create();
                dialog.setOnDismissListener(this);
                return dialog;
            default:
                return super.onCreateDialog(id);
        }
    }

    private void updateUI() {
        setSubTitle(getModeString(((Integer) getScreenValues().getValue("mode")).intValue()));
    }

    public void onValueChanged(ValueStore valueStore, String key, Object oldValue, Object newValue) {
        if (key.equals("mode") && newValue != null && !newValue.equals(oldValue)) {
            updateUI();
        }
    }
}
