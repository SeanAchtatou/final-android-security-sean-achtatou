package com.scoreloop.client.android.ui.component.market;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.CaptionListItem;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.Factory;
import com.scoreloop.client.android.ui.framework.BaseListAdapter;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.scoreloop.client.android.ui.framework.ValueStore;
import com.skyd.bestpuzzle.n1669.R;

public class MarketListActivity extends ComponentListActivity<BaseListItem> implements ValueStore.Observer {
    /* access modifiers changed from: private */
    public MarketListItem _buddiesGamesItem;
    /* access modifiers changed from: private */
    public MarketListItem _myGamesItem;
    /* access modifiers changed from: private */
    public MarketListItem _newGamesItem;
    /* access modifiers changed from: private */
    public MarketListItem _popularGamesItem;

    class SocialMarketListAdapter extends BaseListAdapter<BaseListItem> {
        public SocialMarketListAdapter(Context context) {
            super(context);
            add(new CaptionListItem(context, null, context.getString(R.string.sl_market)));
            add(MarketListActivity.this._newGamesItem);
            add(MarketListActivity.this._popularGamesItem);
            add(MarketListActivity.this._buddiesGamesItem);
            add(new CaptionListItem(context, null, context.getString(R.string.sl_playing)));
            add(MarketListActivity.this._myGamesItem);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Resources res = getResources();
        this._myGamesItem = new MarketListItem(this, res.getDrawable(R.drawable.sl_icon_games), getString(R.string.sl_my_games), getString(R.string.sl_games_subtitle));
        this._myGamesItem.setCounter(getSessionUser().getGamesCounter());
        this._popularGamesItem = new MarketListItem(this, res.getDrawable(R.drawable.sl_icon_market), getString(R.string.sl_popular_games), getString(R.string.sl_popular_games_subtitle));
        this._newGamesItem = new MarketListItem(this, res.getDrawable(R.drawable.sl_icon_market), getString(R.string.sl_new_games), getString(R.string.sl_new_games_subtitle));
        this._buddiesGamesItem = new MarketListItem(this, res.getDrawable(R.drawable.sl_icon_market), getString(R.string.sl_friends_games), getString(R.string.sl_friends_games_subtitle));
        setListAdapter(new SocialMarketListAdapter(this));
    }

    public void onListItemClick(BaseListItem item) {
        Factory factory = getFactory();
        User user = getUser();
        if (item == this._myGamesItem) {
            display(factory.createGameScreenDescription(user, 0));
        } else if (item == this._popularGamesItem) {
            display(factory.createGameScreenDescription(user, 1));
        } else if (item == this._newGamesItem) {
            display(factory.createGameScreenDescription(user, 2));
        } else if (item == this._buddiesGamesItem) {
            display(factory.createGameScreenDescription(user, 3));
        }
    }
}
