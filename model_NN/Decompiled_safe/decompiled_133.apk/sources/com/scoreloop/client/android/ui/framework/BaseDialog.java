package com.scoreloop.client.android.ui.framework;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.skyd.bestpuzzle.n1669.R;

public abstract class BaseDialog extends Dialog implements View.OnClickListener {
    protected OnActionListener _listener;
    private Button _okButton;
    private String _okButtonText;
    private Object _target;
    private String _text;
    private TextView _textView;

    public interface OnActionListener {
        void onAction(BaseDialog baseDialog, int i);
    }

    /* access modifiers changed from: protected */
    public abstract int getContentViewLayoutId();

    protected BaseDialog(Context context) {
        super(context, R.style.sl_dialog);
        setCancelable(false);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentViewLayoutId());
        this._textView = (TextView) findViewById(R.id.sl_text);
        this._okButton = (Button) findViewById(R.id.sl_button_ok);
        refresh();
    }

    public void setOnActionListener(OnActionListener listener) {
        this._listener = listener;
    }

    public void setText(String text) {
        this._text = text;
        refresh();
    }

    public void setOkButtonText(String okButtonText) {
        this._okButtonText = okButtonText;
        refresh();
    }

    private void refresh() {
        if (this._textView != null) {
            this._textView.setText(this._text);
        }
        if (this._textView != null && this._okButtonText != null) {
            this._okButton.setText(this._okButtonText);
        }
    }

    public void setTarget(Object target) {
        this._target = target;
    }

    public <T> T getTarget() {
        return this._target;
    }
}
