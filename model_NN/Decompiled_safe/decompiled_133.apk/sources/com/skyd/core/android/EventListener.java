package com.skyd.core.android;

import android.os.Bundle;

public interface EventListener {
    void OnEvent(Object obj, Bundle bundle);
}
