package com.skyd.core.android.game.crosswisewar;

import com.skyd.core.android.game.GameAnimationSpirit;
import com.skyd.core.game.crosswisewar.ICooling;
import com.skyd.core.game.crosswisewar.IMagic;
import com.skyd.core.game.crosswisewar.IWarrior;

public abstract class Magic extends GameAnimationSpirit implements IMagic {
    private IWarrior _Target = null;

    public IWarrior getTarget() {
        return this._Target;
    }

    public void setTarget(IWarrior value) {
        this._Target = value;
    }

    public void setTargetToDefault() {
        setTarget(null);
    }

    public void useTo(IWarrior target) {
        getAnimation().start();
    }

    public ICooling getCooling() {
        return CoolingMaster.getCooling(getClass());
    }
}
