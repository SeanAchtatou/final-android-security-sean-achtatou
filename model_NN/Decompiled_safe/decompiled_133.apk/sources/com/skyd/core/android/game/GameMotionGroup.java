package com.skyd.core.android.game;

import java.util.ArrayList;
import java.util.Iterator;

public class GameMotionGroup {
    private ArrayList<GameMotion> _MotionList = new ArrayList<>();

    public ArrayList<GameMotion> getMotionList() {
        return this._MotionList;
    }

    /* access modifiers changed from: protected */
    public void setMotionList(ArrayList<GameMotion> value) {
        this._MotionList = value;
    }

    /* access modifiers changed from: protected */
    public void setMotionListToDefault() {
        setMotionList(new ArrayList());
    }

    public void start() {
        Iterator<GameMotion> it = getMotionList().iterator();
        while (it.hasNext()) {
            it.next().start();
        }
    }

    public void stop() {
        Iterator<GameMotion> it = getMotionList().iterator();
        while (it.hasNext()) {
            it.next().stop();
        }
    }

    public void update(GameObject obj) {
        Iterator<GameMotion> it = getMotionList().iterator();
        while (it.hasNext()) {
            it.next().update(obj);
        }
    }
}
