package com.skyd.core.android.game;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import com.skyd.core.android.game.GameObjectList;
import com.skyd.core.android.game.GameSpirit;
import com.skyd.core.vector.Vector2DF;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Vector;

public class GameSpiritGroup extends GameSpirit {
    GameSpirit.OnLevelChangedListener Spirit_OnLevelChangedListener = new GameSpirit.OnLevelChangedListener() {
        public void OnLevelChangedEvent(Object sender, float value) {
            GameSpiritGroup.this.sortSpiritList();
        }
    };
    private GameObjectList<GameSpirit> _SpiritList = new GameObjectList<>(this);

    public GameSpiritGroup() {
        this._SpiritList.addOnAddItemListener(new GameObjectList.OnAddItemListener<GameSpirit>() {
            public void OnAddItemEvent(Object sender, GameSpirit newItem) {
                newItem.addOnLevelChangedListener(GameSpiritGroup.this.Spirit_OnLevelChangedListener);
            }
        });
        this._SpiritList.addOnRemoveItemListener(new GameObjectList.OnRemoveItemListener<GameSpirit>() {
            public void OnRemoveItemEvent(Object sender, GameSpirit oldItem) {
                oldItem.removeOnLevelChangedListener(GameSpiritGroup.this.Spirit_OnLevelChangedListener);
            }
        });
        this._SpiritList.addOnChangedListener(new GameObjectList.OnChangedListener() {
            public void OnChangedEvent(Object sender, int newSize) {
                GameSpiritGroup.this.sortSpiritList();
            }
        });
    }

    public Vector<GameSpirit> getSpiritsByName(String name) {
        Vector<GameSpirit> l = new Vector<>();
        Iterator<GameSpirit> it = this._SpiritList.iterator();
        while (it.hasNext()) {
            GameSpirit f = it.next();
            if (f.getName() == name) {
                l.add(f);
            }
        }
        return l;
    }

    /* access modifiers changed from: protected */
    public void sortSpiritList() {
        this._SpiritList.sort(new Comparator<GameSpirit>() {
            public int compare(GameSpirit arg0, GameSpirit arg1) {
                if (arg0.getLevel() == arg1.getLevel()) {
                    return 0;
                }
                if (arg0.getLevel() > arg1.getLevel()) {
                    return 1;
                }
                return -1;
            }
        });
    }

    public GameObject getDisplayContentChild() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void drawChilds(Canvas c, Rect drawArea) {
        super.drawChilds(c, drawArea);
        Iterator<GameSpirit> it = this._SpiritList.iterator();
        while (it.hasNext()) {
            it.next().draw(c, drawArea);
        }
    }

    /* access modifiers changed from: protected */
    public void updateChilds() {
        super.updateChilds();
        Iterator<GameSpirit> it = this._SpiritList.iterator();
        while (it.hasNext()) {
            it.next().update();
        }
    }

    public GameObjectList<GameSpirit> getSpiritList() {
        return this._SpiritList;
    }

    public void setSpiritList(GameObjectList<GameSpirit> value) {
        this._SpiritList = value;
    }

    public void setSpiritListToDefault() {
        setSpiritList(new GameObjectList(this));
    }

    public float getDrawHeight() {
        return 0.0f;
    }

    public Vector2DF getDrawSize() {
        return new Vector2DF();
    }

    public float getDrawWidth() {
        return 0.0f;
    }

    public void separate(GameSpirit s) {
        Matrix m = s.getMatrix();
        s.getPosition().resetWith(s.getDisplayPosition(m).minus(s.getPositionOffset()));
        s.setRotation(s.getDisplayRotation(m));
        s.getScale().resetWith(s.getDisplayScale(m));
        s.setOpacity(s.getOpacity() * getOpacity());
        this._SpiritList.remove(s);
    }
}
