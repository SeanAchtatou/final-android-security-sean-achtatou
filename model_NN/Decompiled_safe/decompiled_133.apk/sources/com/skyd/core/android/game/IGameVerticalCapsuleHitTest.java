package com.skyd.core.android.game;

public interface IGameVerticalCapsuleHitTest {
    float getVerticalCapsuleHitTestHeight();

    float getVerticalCapsuleHitTestRadius();

    float getVerticalCapsuleHitTestX();

    float getVerticalCapsuleHitTestY();
}
