package com.skyd.core.android.game;

import java.lang.Number;

public abstract class GameNumberBufferStepMotion<T extends Number> extends GameNumberStepMotion<T> {
    private float _LessenBuffer = 0.1f;

    /* access modifiers changed from: protected */
    public abstract T mul(Number number, float f);

    public GameNumberBufferStepMotion(T targetValue, T stepLength, T tolerance, float buffer) {
        super(targetValue, stepLength, tolerance);
        setLessenBuffer(buffer);
    }

    /* access modifiers changed from: protected */
    public void updateSelf(GameObject obj) {
        super.updateSelf(obj);
        setStepLength(mul(getStepLength(), 1.0f - getLessenBuffer()));
    }

    public float getLessenBuffer() {
        return this._LessenBuffer;
    }

    public void setLessenBuffer(float value) {
        this._LessenBuffer = value;
    }

    public void setLessenBufferToDefault() {
        setLessenBuffer(0.1f);
    }
}
