package com.skyd.core.android;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public final class Serialization {
    public static String toXMLString(Object obj) {
        return new XStream().toXML(obj);
    }

    public static void toXMLFile(Object obj, String path) throws FileNotFoundException {
        toXMLStream(obj, new FileOutputStream(path));
    }

    public static void toXMLStream(Object obj, OutputStream stream) {
        new XStream().toXML(obj, stream);
    }

    public static <T> T formXMLFile(String path, T rootobj) throws FileNotFoundException {
        return formXMLStream(new FileInputStream(path), rootobj);
    }

    public static <T> T formXMLStream(InputStream stream, T rootobj) {
        new XStream(new DomDriver()).fromXML(stream, rootobj);
        return rootobj;
    }

    public static Object formXMLFile(String path) throws FileNotFoundException {
        return formXMLStream(new FileInputStream(path));
    }

    public static Object formXMLStream(InputStream stream) {
        return new XStream(new DomDriver()).fromXML(stream);
    }

    public static <T> T formXMLString(String xmlstring, T rootobj) {
        new XStream(new DomDriver()).fromXML(xmlstring, rootobj);
        return rootobj;
    }

    public static Object formXMLString(String xmlstring) {
        return new XStream(new DomDriver()).fromXML(xmlstring);
    }
}
