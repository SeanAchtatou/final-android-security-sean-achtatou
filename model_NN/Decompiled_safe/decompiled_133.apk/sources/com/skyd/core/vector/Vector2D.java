package com.skyd.core.vector;

import android.graphics.Rect;
import android.graphics.RectF;
import com.skyd.core.math.MathEx;
import java.util.ArrayList;
import java.util.Iterator;

public class Vector2D {
    private ArrayList<OnValueChangedListener> _ValueChangedListenerList = null;
    protected double _X = 0.0d;
    private ArrayList<OnXChangingListener> _XChangingListenerList = null;
    protected double _Y = 0.0d;
    private ArrayList<OnYChangingListener> _YChangingListenerList = null;

    public interface OnValueChangedListener {
        void OnValueChangedEvent(Object obj, double d, double d2);
    }

    public interface OnXChangingListener {
        boolean OnXChangingEvent(Object obj, double d, double d2);
    }

    public interface OnYChangingListener {
        boolean OnYChangingEvent(Object obj, double d, double d2);
    }

    public Vector2D reset(double x, double y) {
        setX(x);
        setY(y);
        return this;
    }

    public Vector2D resetWith(Vector2D value) {
        setX(value.getX());
        setY(value.getY());
        return this;
    }

    public int getIntX() {
        return (int) Math.ceil(getX());
    }

    public int getIntY() {
        return (int) Math.ceil(getY());
    }

    public double getX() {
        return this._X;
    }

    public Vector2D setX(double value) {
        if (onXChanging(this._X, value)) {
            this._X = value;
            onValueChanged(this._X, this._Y);
        }
        return this;
    }

    public Vector2D setXToDefault() {
        setX(0.0d);
        return this;
    }

    public double getY() {
        return this._Y;
    }

    public Vector2D setY(double value) {
        if (onYChanging(this._Y, value)) {
            this._Y = value;
            onValueChanged(this._X, this._Y);
        }
        return this;
    }

    public Vector2D setYToDefault() {
        setY(0.0d);
        return this;
    }

    public Vector2D(double x, double y) {
        setX(x);
        setY(y);
    }

    public Vector2D() {
    }

    public Vector2D(double[] array) {
        setX(array[0]);
        setY(array[1]);
    }

    public double[] toArray() {
        return new double[]{getX(), getY()};
    }

    /* access modifiers changed from: protected */
    public Object clone() throws CloneNotSupportedException {
        return getClone();
    }

    public Vector2D getClone() {
        return new Vector2D(this._X, this._Y);
    }

    public String toString() {
        return "[" + (Math.round(this._X * 1000.0d) / 1000) + "," + (Math.round(this._Y * 1000.0d) / 1000) + "]";
    }

    public Vector2D plus(Vector2D value) {
        setX(this._X + value._X);
        setY(this._Y + value._Y);
        return this;
    }

    public Vector2D plusNew(Vector2D value) {
        return new Vector2D(this._X + value._X, this._Y + value._Y);
    }

    public Vector2D minus(Vector2D value) {
        setX(this._X - value._X);
        setY(this._Y - value._Y);
        return this;
    }

    public Vector2D minusNew(Vector2D value) {
        return new Vector2D(this._X - value._X, this._Y - value._Y);
    }

    public Vector2D negate() {
        setX(-this._X);
        setY(-this._Y);
        return this;
    }

    public Vector2D negateNew() {
        return new Vector2D(-this._X, -this._Y);
    }

    public Vector2D scale(double value) {
        scale(value, value);
        return this;
    }

    public Vector2D scaleNew(double value) {
        return scaleNew(value, value);
    }

    public Vector2D scale(double xValue, double yValue) {
        setX(this._X * xValue);
        setY(this._Y * yValue);
        return this;
    }

    public Vector2D scale(Vector2D value) {
        return scale(value.getX(), value.getY());
    }

    public Vector2D scaleNew(double xValue, double yValue) {
        return new Vector2D(this._X * xValue, this._Y * yValue);
    }

    public Vector2D scaleNew(Vector2D value) {
        return scaleNew(value.getX(), getY());
    }

    public Vector2D scale(double xValue, double yValue, Vector2D center) {
        minus(center);
        setX(this._X * xValue);
        setY(this._Y * yValue);
        plus(center);
        return this;
    }

    public Vector2D scale(Vector2D value, Vector2D center) {
        return scale(value.getX(), value.getY(), center);
    }

    public Vector2D scaleNew(double xValue, double yValue, Vector2D center) {
        Vector2D v = getClone();
        v.scale(xValue, yValue, center);
        return v;
    }

    public Vector2D scaleNew(Vector2D value, Vector2D center) {
        return scaleNew(value.getX(), value.getY(), center);
    }

    public double getLength() {
        return Math.sqrt((this._X * this._X) + (this._Y * this._Y));
    }

    public Vector2D setLength(double value) {
        double r = getLength();
        if (r != 0.0d) {
            scale(value / r);
        } else {
            setX(value);
        }
        return this;
    }

    public double getAngle() {
        return MathEx.atan2Angle(this._Y, this._X);
    }

    public Vector2D setAngle(double angle) {
        double r = getLength();
        setX(MathEx.cosAngle(angle) * r);
        setY(MathEx.sinAngle(angle) * r);
        return this;
    }

    public Vector2D rotate(double angle) {
        double ca = MathEx.cosAngle(angle);
        double sa = MathEx.sinAngle(angle);
        setX((this._X * ca) - (this._Y * sa));
        setY((this._X * sa) + (this._Y * ca));
        return this;
    }

    public Vector2D rotateNew(double angle) {
        Vector2D v = getClone();
        v.rotate(angle);
        return v;
    }

    public Vector2D rotate(double angle, Vector2D center) {
        minus(center);
        rotate(angle);
        plus(center);
        return this;
    }

    public Vector2D rotateNew(double angle, Vector2D center) {
        Vector2D v = getClone();
        v.rotate(angle, center);
        return v;
    }

    public double dot(Vector2D v) {
        return (this._X * v._X) + (this._Y * v._Y);
    }

    public boolean isPerpTo(Vector2D v) {
        return dot(v) == 0.0d;
    }

    public Vector2D getNormalCW() {
        return new Vector2D(this._Y, -this._X);
    }

    public Vector2D getNormalCCW() {
        return new Vector2D(-this._Y, this._X);
    }

    public double angleBetween(Vector2D v) {
        return MathEx.acosAngle(dot(v) / (getLength() * v.getLength()));
    }

    public boolean addOnXChangingListener(OnXChangingListener listener) {
        if (this._XChangingListenerList == null) {
            this._XChangingListenerList = new ArrayList<>();
        } else if (this._XChangingListenerList.contains(listener)) {
            return false;
        }
        this._XChangingListenerList.add(listener);
        return true;
    }

    public boolean removeOnXChangingListener(OnXChangingListener listener) {
        if (this._XChangingListenerList == null || !this._XChangingListenerList.contains(listener)) {
            return false;
        }
        this._XChangingListenerList.remove(listener);
        return true;
    }

    public void clearOnXChangingListeners() {
        if (this._XChangingListenerList != null) {
            this._XChangingListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public boolean onXChanging(double currentValue, double newValue) {
        if (this._XChangingListenerList != null) {
            Iterator<OnXChangingListener> it = this._XChangingListenerList.iterator();
            while (it.hasNext()) {
                if (!it.next().OnXChangingEvent(this, currentValue, newValue)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean addOnYChangingListener(OnYChangingListener listener) {
        if (this._YChangingListenerList == null) {
            this._YChangingListenerList = new ArrayList<>();
        } else if (this._YChangingListenerList.contains(listener)) {
            return false;
        }
        this._YChangingListenerList.add(listener);
        return true;
    }

    public boolean removeOnYChangingListener(OnYChangingListener listener) {
        if (this._YChangingListenerList == null || !this._YChangingListenerList.contains(listener)) {
            return false;
        }
        this._YChangingListenerList.remove(listener);
        return true;
    }

    public void clearOnYChangingListeners() {
        if (this._YChangingListenerList != null) {
            this._YChangingListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public boolean onYChanging(double currentValue, double newValue) {
        if (this._YChangingListenerList != null) {
            Iterator<OnYChangingListener> it = this._YChangingListenerList.iterator();
            while (it.hasNext()) {
                if (!it.next().OnYChangingEvent(this, currentValue, newValue)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean addOnValueChangedListener(OnValueChangedListener listener) {
        if (this._ValueChangedListenerList == null) {
            this._ValueChangedListenerList = new ArrayList<>();
        } else if (this._ValueChangedListenerList.contains(listener)) {
            return false;
        }
        this._ValueChangedListenerList.add(listener);
        return true;
    }

    public boolean removeOnValueChangedListener(OnValueChangedListener listener) {
        if (this._ValueChangedListenerList == null || !this._ValueChangedListenerList.contains(listener)) {
            return false;
        }
        this._ValueChangedListenerList.remove(listener);
        return true;
    }

    public void clearOnValueChangedListeners() {
        if (this._ValueChangedListenerList != null) {
            this._ValueChangedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onValueChanged(double newX, double newY) {
        if (this._ValueChangedListenerList != null) {
            Iterator<OnValueChangedListener> it = this._ValueChangedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnValueChangedEvent(this, newX, newY);
            }
        }
    }

    public boolean isIn(Rect r) {
        return this._X >= ((double) r.left) && this._X <= ((double) r.right) && this._Y >= ((double) r.top) && this._Y <= ((double) r.bottom);
    }

    public boolean isIn(RectF r) {
        return this._X >= ((double) r.left) && this._X <= ((double) r.right) && this._Y >= ((double) r.top) && this._Y <= ((double) r.bottom);
    }

    public Vector2D plusX(double v) {
        setX(this._X + v);
        return this;
    }

    public Vector2D plusY(double v) {
        setY(this._Y + v);
        return this;
    }

    public Vector2D scaleX(double v) {
        setX(this._X * v);
        return this;
    }

    public Vector2D scaleY(double v) {
        setY(this._Y * v);
        return this;
    }

    public Vector2D restrainLength(double v) {
        if (getLength() > v) {
            setLength(v);
        }
        return this;
    }

    public Vector2D averageWith(Vector2D v) {
        setX((this._X + v._X) / 2.0d);
        setY((this._Y + v._Y) / 2.0d);
        return this;
    }

    public Vector2D averageNewWith(Vector2D v) {
        return getClone().averageWith(v);
    }
}
