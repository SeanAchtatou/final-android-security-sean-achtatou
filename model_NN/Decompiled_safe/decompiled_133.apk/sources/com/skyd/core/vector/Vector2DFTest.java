package com.skyd.core.vector;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

public class Vector2DFTest {
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testGetAngle() {
        Assert.assertTrue(new Vector2DF(10.0f, -10.0f).getAngle() == -45.0f);
    }

    @Test
    public void testSetAngle() {
        Vector2DF v = new Vector2DF(10.0f, -10.0f);
        v.setAngle(45.0f);
        Assert.assertTrue(v.getAngle() == 45.0f);
    }
}
