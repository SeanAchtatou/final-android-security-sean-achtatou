package com.skyd.core.game.crosswisewar;

public interface ICooling {
    int getBaseCoolingTime();

    int getCoolingTime();

    boolean getIsCanUse();

    void resetCoolingTime();

    void setCoolingTime(int i);

    void update();
}
