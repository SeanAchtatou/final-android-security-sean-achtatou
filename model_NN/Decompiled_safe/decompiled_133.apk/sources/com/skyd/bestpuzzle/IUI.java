package com.skyd.bestpuzzle;

import com.skyd.core.vector.Vector2DF;

public interface IUI {
    void executive(Vector2DF vector2DF);

    boolean isInArea(Vector2DF vector2DF);

    void reset();
}
