package com.skyd.bestpuzzle;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.widget.Toast;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.skyd.bestpuzzle.n1669.R;
import com.skyd.core.android.game.GameImageSpirit;
import com.skyd.core.android.game.GameMaster;
import com.skyd.core.android.game.GameObject;
import com.skyd.core.android.game.GameScene;
import com.skyd.core.android.game.GameSpirit;
import com.skyd.core.draw.DrawHelper;
import com.skyd.core.vector.Vector2DF;
import com.skyd.core.vector.VectorRect2DF;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class PuzzleScene extends GameScene {
    public Controller Controller;
    public Desktop Desktop;
    boolean IsDrawingPuzzle;
    boolean IsDrawnSelectRect;
    public GameImageSpirit MoveButton;
    public UI MoveButtonSlot;
    public OriginalImage OriginalImage;
    public long PastTime;
    public long StartTime;
    public UI SubmitScoreButton;
    /* access modifiers changed from: private */
    public RectF SubmitScoreButtonRect;
    public GameSpirit Time;
    public UI ViewFullButton;
    /* access modifiers changed from: private */
    public RectF ViewFullButtonRect;
    public UI ZoomInButton;
    public UI ZoomOutButton;
    private boolean _IsFinished = false;
    private ArrayList<OnIsFinishedChangedListener> _IsFinishedChangedListenerList = null;
    long savetime = Long.MIN_VALUE;
    RectF selectRect;

    public interface OnIsFinishedChangedListener {
        void OnIsFinishedChangedEvent(Object obj, boolean z);
    }

    public PuzzleScene() {
        setMaxDrawCacheLayer(25.0f);
        this.StartTime = new Date().getTime();
        loadGameState();
    }

    public void loadGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        this.PastTime = c.getPastTime().longValue();
        this._IsFinished = c.getIsFinished().booleanValue();
    }

    public void saveGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        if (getIsFinished()) {
            c.setPastTime(Long.valueOf(this.PastTime));
        } else {
            c.setPastTime(Long.valueOf(this.PastTime + (new Date().getTime() - this.StartTime)));
        }
        c.setIsFinished(Boolean.valueOf(getIsFinished()));
    }

    public void setFinished() {
        if (!getIsFinished()) {
            this.SubmitScoreButton.show();
        }
        setIsFinished(true);
        this.PastTime += new Date().getTime() - this.StartTime;
    }

    public boolean getIsFinished() {
        return this._IsFinished;
    }

    private void setIsFinished(boolean value) {
        onIsFinishedChanged(value);
        this._IsFinished = value;
    }

    private void setIsFinishedToDefault() {
        setIsFinished(false);
    }

    public boolean addOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null) {
            this._IsFinishedChangedListenerList = new ArrayList<>();
        } else if (this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.add(listener);
        return true;
    }

    public boolean removeOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null || !this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.remove(listener);
        return true;
    }

    public void clearOnIsFinishedChangedListeners() {
        if (this._IsFinishedChangedListenerList != null) {
            this._IsFinishedChangedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onIsFinishedChanged(boolean newValue) {
        if (this._IsFinishedChangedListenerList != null) {
            Iterator<OnIsFinishedChangedListener> it = this._IsFinishedChangedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnIsFinishedChangedEvent(this, newValue);
            }
        }
    }

    public int getTargetCacheID() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void drawChilds(Canvas c, Rect drawArea) {
        this.IsDrawingPuzzle = false;
        this.IsDrawnSelectRect = false;
        super.drawChilds(c, drawArea);
    }

    /* access modifiers changed from: protected */
    public boolean onDrawingChild(GameObject child, Canvas c, Rect drawArea) {
        float lvl = ((GameSpirit) child).getLevel();
        if (!this.IsDrawingPuzzle && lvl <= 10.0f) {
            this.IsDrawingPuzzle = true;
            c.setMatrix(this.Desktop.getMatrix());
        } else if (lvl > 10.0f && this.IsDrawingPuzzle) {
            this.IsDrawingPuzzle = false;
            c.setMatrix(new Matrix());
        }
        if (lvl > 25.0f && this.selectRect != null && !this.IsDrawnSelectRect) {
            this.IsDrawnSelectRect = true;
            Paint p = new Paint();
            p.setARGB(100, 180, 225, 255);
            c.drawRect(this.selectRect, p);
        }
        return super.onDrawingChild(child, c, drawArea);
    }

    public void load(Context c) {
        loadOriginalImage(c);
        loadDesktop(c);
        loadInterface(c);
        loadPuzzle(c);
        loadPuzzleState();
        this.Desktop.updateCurrentObserveAreaRectF();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF
     arg types: [int, int, float, float, int]
     candidates:
      com.skyd.core.draw.DrawHelper.calculateScaleSize(double, double, double, double, boolean):com.skyd.core.vector.Vector2D
      com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF */
    public void loadOriginalImage(Context c) {
        this.OriginalImage = new OriginalImage() {
            /* access modifiers changed from: protected */
            public boolean onDrawing(Canvas c, Rect drawArea) {
                c.drawARGB(180, 0, 0, 0);
                return super.onDrawing(c, drawArea);
            }
        };
        this.OriginalImage.setLevel(50.0f);
        this.OriginalImage.setName("OriginalImage");
        this.OriginalImage.getSize().resetWith(DrawHelper.calculateScaleSize(384.0f, 240.0f, (float) (GameMaster.getScreenWidth() - 40), (float) (GameMaster.getScreenHeight() - 40), true));
        this.OriginalImage.setTotalSeparateColumn(8);
        this.OriginalImage.setTotalSeparateRow(5);
        this.OriginalImage.getOriginalSize().reset(800.0f, 500.0f);
        this.OriginalImage.hide();
        this.OriginalImage.setIsUseAbsolutePosition(true);
        this.OriginalImage.setIsUseAbsoluteSize(true);
        this.OriginalImage.getImage().setIsUseAbsolutePosition(true);
        this.OriginalImage.getImage().setIsUseAbsoluteSize(true);
        this.OriginalImage.getPosition().reset(((float) (GameMaster.getScreenWidth() / 2)) - (this.OriginalImage.getSize().getX() / 2.0f), ((float) (GameMaster.getScreenHeight() / 2)) - (this.OriginalImage.getSize().getY() / 2.0f));
        getSpiritList().add(this.OriginalImage);
    }

    public void loadDesktop(Context c) {
        this.Desktop = new Desktop();
        this.Desktop.setLevel(-9999.0f);
        this.Desktop.setName("Desktop");
        this.Desktop.getSize().reset(1600.0f, 1000.0f);
        this.Desktop.getCurrentObservePosition().reset(800.0f, 500.0f);
        this.Desktop.setOriginalImage(this.OriginalImage);
        this.Desktop.setIsUseAbsolutePosition(true);
        this.Desktop.setIsUseAbsoluteSize(true);
        this.Desktop.ColorAPaint = new Paint();
        this.Desktop.ColorAPaint.setColor(Color.argb(255, 32, 30, 43));
        this.Desktop.ColorBPaint = new Paint();
        this.Desktop.ColorBPaint.setColor(Color.argb(255, 26, 24, 37));
        getSpiritList().add(this.Desktop);
    }

    public void loadPuzzle(Context c) {
        this.Controller = new Controller();
        this.Controller.setLevel(10.0f);
        this.Controller.setName("Controller");
        this.Controller.setDesktop(this.Desktop);
        this.Controller.setIsUseAbsolutePosition(true);
        this.Controller.setIsUseAbsoluteSize(true);
        getSpiritList().add(this.Controller);
        Puzzle.getRealitySize().reset(100.0f, 100.0f);
        Puzzle.getMaxRadius().reset(76.66666f, 76.66666f);
        c1139265004(c);
        c1595960395(c);
        c29639842(c);
        c849721877(c);
        c2106307501(c);
        c31808220(c);
        c635623542(c);
        c1804482621(c);
        c87355417(c);
        c2061097774(c);
        c315788164(c);
        c1958884097(c);
        c1349705658(c);
        c305893668(c);
        c24207297(c);
        c2106038466(c);
        c698311935(c);
        c1247890733(c);
        c802542692(c);
        c2050260507(c);
        c2023439901(c);
        c1382041943(c);
        c517003624(c);
        c430906672(c);
        c449859470(c);
        c451329701(c);
        c305154650(c);
        c1729751336(c);
        c892374211(c);
        c763230019(c);
        c1972244829(c);
        c1109760194(c);
        c928289068(c);
        c85982790(c);
        c1115224980(c);
        c1262102120(c);
        c1821292(c);
        c676570845(c);
        c2020327835(c);
        c1349325407(c);
    }

    public void loadInterface(Context c) {
        this.MoveButton = new GameImageSpirit();
        this.MoveButton.setLevel(22.0f);
        this.MoveButton.getImage().loadImageFromResource(c, R.drawable.movebutton);
        this.MoveButton.getSize().reset(155.0f, 155.0f);
        this.MoveButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButton);
        final Vector2DF mvds = this.MoveButton.getDisplaySize();
        mvds.scale(0.5f);
        this.MoveButton.getPosition().reset(5.0f + mvds.getX(), ((float) (GameMaster.getScreenHeight() - 5)) - mvds.getY());
        this.MoveButton.getPositionOffset().resetWith(mvds.negateNew());
        this.MoveButtonSlot = new UI() {
            Vector2DF movevector;
            boolean needmove;

            public void executive(Vector2DF point) {
                this.movevector = point.minusNew(getPosition()).restrainLength(mvds.getX() * 0.4f);
                this.needmove = true;
            }

            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.needmove) {
                    PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition().plusNew(this.movevector));
                    PuzzleScene.this.Desktop.getCurrentObservePosition().plus(this.movevector);
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < mvds.getX() + 5.0f;
            }

            public void reset() {
                this.needmove = false;
                PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition());
                PuzzleScene.this.refreshDrawCacheBitmap();
            }
        };
        this.MoveButtonSlot.setLevel(21.0f);
        this.MoveButtonSlot.getImage().loadImageFromResource(c, R.drawable.movebuttonslot);
        this.MoveButtonSlot.getSize().reset(155.0f, 155.0f);
        this.MoveButtonSlot.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButtonSlot);
        this.MoveButtonSlot.getPosition().resetWith(this.MoveButton.getPosition());
        this.MoveButtonSlot.getPositionOffset().resetWith(this.MoveButton.getPositionOffset());
        this.ZoomInButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.min(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.min(double, double):double}
              ClspMth{java.lang.Math.min(long, long):long}
              ClspMth{java.lang.Math.min(int, int):int}
              ClspMth{java.lang.Math.min(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.min(1.3f, PuzzleScene.this.Desktop.getZoom() * 1.05f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }
        };
        this.ZoomInButton.setLevel(21.0f);
        this.ZoomInButton.getImage().loadImageFromResource(c, R.drawable.zoombutton1);
        this.ZoomInButton.getSize().reset(77.0f, 77.0f);
        this.ZoomInButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomInButton);
        Vector2DF zibds = this.ZoomInButton.getDisplaySize().scale(0.5f);
        this.ZoomInButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 50)) - zibds.getX(), ((float) (GameMaster.getScreenHeight() - 10)) - zibds.getY());
        this.ZoomInButton.getPositionOffset().resetWith(zibds.negateNew());
        this.ZoomOutButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.max(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.max(double, double):double}
              ClspMth{java.lang.Math.max(int, int):int}
              ClspMth{java.lang.Math.max(long, long):long}
              ClspMth{java.lang.Math.max(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.max(0.3f, PuzzleScene.this.Desktop.getZoom() * 0.95f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                    GameMaster.log(this, Float.valueOf(PuzzleScene.this.Desktop.getCurrentObserveAreaRectF().width()));
                }
                super.updateSelf();
            }
        };
        this.ZoomOutButton.setLevel(21.0f);
        this.ZoomOutButton.getImage().loadImageFromResource(c, R.drawable.zoombutton2);
        this.ZoomOutButton.getSize().reset(77.0f, 77.0f);
        this.ZoomOutButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomOutButton);
        Vector2DF zobds = this.ZoomOutButton.getDisplaySize().scale(0.5f);
        this.ZoomOutButton.getPosition().reset((((float) (GameMaster.getScreenWidth() - 100)) - zobds.getX()) - (zibds.getX() * 2.0f), ((float) (GameMaster.getScreenHeight() - 10)) - zobds.getY());
        this.ZoomOutButton.getPositionOffset().resetWith(zobds.negateNew());
        this.ViewFullButton = new UI() {
            public void executive(Vector2DF point) {
                if (PuzzleScene.this.OriginalImage.getImage().getImage() == null) {
                    PuzzleScene.this.OriginalImage.getImage().loadImageFromResource(GameMaster.getContext(), R.drawable.oim);
                }
                PuzzleScene.this.OriginalImage.show();
            }

            public boolean isInArea(Vector2DF point) {
                return point.isIn(PuzzleScene.this.ViewFullButtonRect);
            }

            public void reset() {
                PuzzleScene.this.OriginalImage.hide();
            }
        };
        this.ViewFullButton.getImage().loadImageFromResource(c, R.drawable.viewfull);
        this.ViewFullButton.setLevel(21.0f);
        this.ViewFullButton.setIsUseAbsolutePosition(true);
        this.ViewFullButton.getSize().reset(83.0f, 38.0f);
        getSpiritList().add(this.ViewFullButton);
        this.ViewFullButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 15)) - this.ViewFullButton.getDisplaySize().getX(), 15.0f);
        this.ViewFullButtonRect = new VectorRect2DF(this.ViewFullButton.getPosition(), this.ViewFullButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton = new UI() {
            public void executive(Vector2DF point) {
                Score score = new Score(Double.valueOf(Math.ceil((double) (PuzzleScene.this.PastTime / 1000))), null);
                score.setMode(17);
                ScoreloopManagerSingleton.get().onGamePlayEnded(score);
                hide();
                Toast.makeText(GameMaster.getContext(), (int) R.string.Submitting, 1).show();
            }

            public boolean isInArea(Vector2DF point) {
                return getVisibleOriginalValue() && point.isIn(PuzzleScene.this.SubmitScoreButtonRect);
            }

            public void reset() {
            }
        };
        this.SubmitScoreButton.getImage().loadImageFromResource(c, R.drawable.submitscore);
        this.SubmitScoreButton.setLevel(21.0f);
        this.SubmitScoreButton.setIsUseAbsolutePosition(true);
        this.SubmitScoreButton.getSize().reset(114.0f, 40.0f);
        getSpiritList().add(this.SubmitScoreButton);
        this.SubmitScoreButton.getPosition().reset(15.0f, 60.0f);
        this.SubmitScoreButtonRect = new VectorRect2DF(this.SubmitScoreButton.getPosition(), this.SubmitScoreButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton.hide();
        this.Time = new GameSpirit() {
            DecimalFormat FMT = new DecimalFormat("00");
            Paint p1;
            Paint p2;

            public GameObject getDisplayContentChild() {
                return null;
            }

            /* access modifiers changed from: protected */
            public void drawSelf(Canvas c, Rect drawArea) {
                if (this.p1 == null) {
                    this.p1 = new Paint();
                    this.p1.setARGB(160, 255, 255, 255);
                    this.p1.setAntiAlias(true);
                    this.p1.setTextSize(28.0f);
                    this.p1.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                if (this.p2 == null) {
                    this.p2 = new Paint();
                    this.p2.setARGB(100, 0, 0, 0);
                    this.p2.setAntiAlias(true);
                    this.p2.setTextSize(28.0f);
                    this.p2.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                long t = PuzzleScene.this.getIsFinished() ? PuzzleScene.this.PastTime : PuzzleScene.this.PastTime + (new Date().getTime() - PuzzleScene.this.StartTime);
                long h = t / 3600000;
                long m = (t - (((1000 * h) * 60) * 60)) / 60000;
                String str = String.valueOf(this.FMT.format(h)) + ":" + this.FMT.format(m) + ":" + this.FMT.format(((t - (((1000 * h) * 60) * 60)) - ((1000 * m) * 60)) / 1000) + (PuzzleScene.this.getIsFinished() ? " Finished" : "");
                c.drawText(str, 16.0f, 44.0f, this.p2);
                c.drawText(str, 15.0f, 43.0f, this.p1);
                super.drawSelf(c, drawArea);
            }
        };
        this.Time.setLevel(40.0f);
        getSpiritList().add(this.Time);
    }

    public float getMaxPuzzleLevel() {
        float maxlvl = 0.0f;
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getLevel() <= 10.0f) {
                maxlvl = Math.max(f.getLevel(), maxlvl);
            }
        }
        return maxlvl;
    }

    public Puzzle getTouchPuzzle(Vector2DF v) {
        Vector2DF rv = this.Desktop.reMapPoint(v);
        Puzzle o = null;
        float olen = 99999.0f;
        for (int i = getSpiritList().size() - 1; i >= 0; i--) {
            if (getSpiritList().get(i) instanceof Puzzle) {
                Puzzle p = (Puzzle) getSpiritList().get(i);
                float len = p.getPositionInDesktop().minusNew(rv).getLength();
                if (len <= Puzzle.getRealitySize().getX() * 0.4f) {
                    return p;
                }
                if (len <= Puzzle.getRealitySize().getX() && olen > len) {
                    o = p;
                    olen = len;
                }
            }
        }
        return o;
    }

    public void startDrawSelectRect(Vector2DF startDragPoint, Vector2DF v) {
        this.selectRect = new VectorRect2DF(startDragPoint, v.minusNew(startDragPoint)).getFixedRectF();
    }

    public void stopDrawSelectRect() {
        this.selectRect = null;
    }

    public ArrayList<Puzzle> getSelectPuzzle(Vector2DF startDragPoint, Vector2DF v) {
        Vector2DF rs = this.Desktop.reMapPoint(startDragPoint);
        ArrayList<Puzzle> l = new ArrayList<>();
        RectF rect = new VectorRect2DF(rs, this.Desktop.reMapPoint(v).minusNew(rs)).getFixedRectF();
        float xo = Puzzle.getRealitySize().getX() / 3.0f;
        float yo = Puzzle.getRealitySize().getY() / 3.0f;
        RectF sr = new RectF(rect.left - xo, rect.top - yo, rect.right + xo, rect.bottom + yo);
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getPositionInDesktop().isIn(sr)) {
                l.add(f);
            }
        }
        return l;
    }

    public void loadPuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences s = c.getSharedPreferences();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().load(c, s);
        }
    }

    public void savePuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences.Editor e = c.getSharedPreferences().edit();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().save(c, e);
        }
        e.commit();
    }

    public void reset() {
        this.Controller.reset();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            f.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
            this.Desktop.RandomlyPlaced(f);
        }
        refreshDrawCacheBitmap();
        setIsFinishedToDefault();
        this.StartTime = new Date().getTime();
        this.PastTime = 0;
        this.SubmitScoreButton.hide();
        saveGameState();
        savePuzzleState();
    }

    /* access modifiers changed from: package-private */
    public void c1139265004(Context c) {
        Puzzle p1139265004 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1139265004(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1139265004(editor, this);
            }
        };
        p1139265004.setID(1139265004);
        p1139265004.setName("1139265004");
        p1139265004.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1139265004);
        this.Desktop.RandomlyPlaced(p1139265004);
        p1139265004.setTopEdgeType(EdgeType.Flat);
        p1139265004.setBottomEdgeType(EdgeType.Concave);
        p1139265004.setLeftEdgeType(EdgeType.Flat);
        p1139265004.setRightEdgeType(EdgeType.Concave);
        p1139265004.setTopExactPuzzleID(-1);
        p1139265004.setBottomExactPuzzleID(1595960395);
        p1139265004.setLeftExactPuzzleID(-1);
        p1139265004.setRightExactPuzzleID(31808220);
        p1139265004.getDisplayImage().loadImageFromResource(c, R.drawable.p1139265004h);
        p1139265004.setExactRow(0);
        p1139265004.setExactColumn(0);
        p1139265004.getSize().reset(100.0f, 100.0f);
        p1139265004.getPositionOffset().reset(-50.0f, -50.0f);
        p1139265004.setIsUseAbsolutePosition(true);
        p1139265004.setIsUseAbsoluteSize(true);
        p1139265004.getImage().setIsUseAbsolutePosition(true);
        p1139265004.getImage().setIsUseAbsoluteSize(true);
        p1139265004.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1139265004.resetPosition();
        getSpiritList().add(p1139265004);
    }

    /* access modifiers changed from: package-private */
    public void c1595960395(Context c) {
        Puzzle p1595960395 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1595960395(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1595960395(editor, this);
            }
        };
        p1595960395.setID(1595960395);
        p1595960395.setName("1595960395");
        p1595960395.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1595960395);
        this.Desktop.RandomlyPlaced(p1595960395);
        p1595960395.setTopEdgeType(EdgeType.Convex);
        p1595960395.setBottomEdgeType(EdgeType.Convex);
        p1595960395.setLeftEdgeType(EdgeType.Flat);
        p1595960395.setRightEdgeType(EdgeType.Concave);
        p1595960395.setTopExactPuzzleID(1139265004);
        p1595960395.setBottomExactPuzzleID(29639842);
        p1595960395.setLeftExactPuzzleID(-1);
        p1595960395.setRightExactPuzzleID(635623542);
        p1595960395.getDisplayImage().loadImageFromResource(c, R.drawable.p1595960395h);
        p1595960395.setExactRow(1);
        p1595960395.setExactColumn(0);
        p1595960395.getSize().reset(100.0f, 153.3333f);
        p1595960395.getPositionOffset().reset(-50.0f, -76.66666f);
        p1595960395.setIsUseAbsolutePosition(true);
        p1595960395.setIsUseAbsoluteSize(true);
        p1595960395.getImage().setIsUseAbsolutePosition(true);
        p1595960395.getImage().setIsUseAbsoluteSize(true);
        p1595960395.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1595960395.resetPosition();
        getSpiritList().add(p1595960395);
    }

    /* access modifiers changed from: package-private */
    public void c29639842(Context c) {
        Puzzle p29639842 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load29639842(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save29639842(editor, this);
            }
        };
        p29639842.setID(29639842);
        p29639842.setName("29639842");
        p29639842.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p29639842);
        this.Desktop.RandomlyPlaced(p29639842);
        p29639842.setTopEdgeType(EdgeType.Concave);
        p29639842.setBottomEdgeType(EdgeType.Concave);
        p29639842.setLeftEdgeType(EdgeType.Flat);
        p29639842.setRightEdgeType(EdgeType.Concave);
        p29639842.setTopExactPuzzleID(1595960395);
        p29639842.setBottomExactPuzzleID(849721877);
        p29639842.setLeftExactPuzzleID(-1);
        p29639842.setRightExactPuzzleID(1804482621);
        p29639842.getDisplayImage().loadImageFromResource(c, R.drawable.p29639842h);
        p29639842.setExactRow(2);
        p29639842.setExactColumn(0);
        p29639842.getSize().reset(100.0f, 100.0f);
        p29639842.getPositionOffset().reset(-50.0f, -50.0f);
        p29639842.setIsUseAbsolutePosition(true);
        p29639842.setIsUseAbsoluteSize(true);
        p29639842.getImage().setIsUseAbsolutePosition(true);
        p29639842.getImage().setIsUseAbsoluteSize(true);
        p29639842.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p29639842.resetPosition();
        getSpiritList().add(p29639842);
    }

    /* access modifiers changed from: package-private */
    public void c849721877(Context c) {
        Puzzle p849721877 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load849721877(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save849721877(editor, this);
            }
        };
        p849721877.setID(849721877);
        p849721877.setName("849721877");
        p849721877.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p849721877);
        this.Desktop.RandomlyPlaced(p849721877);
        p849721877.setTopEdgeType(EdgeType.Convex);
        p849721877.setBottomEdgeType(EdgeType.Convex);
        p849721877.setLeftEdgeType(EdgeType.Flat);
        p849721877.setRightEdgeType(EdgeType.Convex);
        p849721877.setTopExactPuzzleID(29639842);
        p849721877.setBottomExactPuzzleID(2106307501);
        p849721877.setLeftExactPuzzleID(-1);
        p849721877.setRightExactPuzzleID(87355417);
        p849721877.getDisplayImage().loadImageFromResource(c, R.drawable.p849721877h);
        p849721877.setExactRow(3);
        p849721877.setExactColumn(0);
        p849721877.getSize().reset(126.6667f, 153.3333f);
        p849721877.getPositionOffset().reset(-50.0f, -76.66666f);
        p849721877.setIsUseAbsolutePosition(true);
        p849721877.setIsUseAbsoluteSize(true);
        p849721877.getImage().setIsUseAbsolutePosition(true);
        p849721877.getImage().setIsUseAbsoluteSize(true);
        p849721877.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p849721877.resetPosition();
        getSpiritList().add(p849721877);
    }

    /* access modifiers changed from: package-private */
    public void c2106307501(Context c) {
        Puzzle p2106307501 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2106307501(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2106307501(editor, this);
            }
        };
        p2106307501.setID(2106307501);
        p2106307501.setName("2106307501");
        p2106307501.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2106307501);
        this.Desktop.RandomlyPlaced(p2106307501);
        p2106307501.setTopEdgeType(EdgeType.Concave);
        p2106307501.setBottomEdgeType(EdgeType.Flat);
        p2106307501.setLeftEdgeType(EdgeType.Flat);
        p2106307501.setRightEdgeType(EdgeType.Convex);
        p2106307501.setTopExactPuzzleID(849721877);
        p2106307501.setBottomExactPuzzleID(-1);
        p2106307501.setLeftExactPuzzleID(-1);
        p2106307501.setRightExactPuzzleID(2061097774);
        p2106307501.getDisplayImage().loadImageFromResource(c, R.drawable.p2106307501h);
        p2106307501.setExactRow(4);
        p2106307501.setExactColumn(0);
        p2106307501.getSize().reset(126.6667f, 100.0f);
        p2106307501.getPositionOffset().reset(-50.0f, -50.0f);
        p2106307501.setIsUseAbsolutePosition(true);
        p2106307501.setIsUseAbsoluteSize(true);
        p2106307501.getImage().setIsUseAbsolutePosition(true);
        p2106307501.getImage().setIsUseAbsoluteSize(true);
        p2106307501.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2106307501.resetPosition();
        getSpiritList().add(p2106307501);
    }

    /* access modifiers changed from: package-private */
    public void c31808220(Context c) {
        Puzzle p31808220 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load31808220(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save31808220(editor, this);
            }
        };
        p31808220.setID(31808220);
        p31808220.setName("31808220");
        p31808220.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p31808220);
        this.Desktop.RandomlyPlaced(p31808220);
        p31808220.setTopEdgeType(EdgeType.Flat);
        p31808220.setBottomEdgeType(EdgeType.Concave);
        p31808220.setLeftEdgeType(EdgeType.Convex);
        p31808220.setRightEdgeType(EdgeType.Concave);
        p31808220.setTopExactPuzzleID(-1);
        p31808220.setBottomExactPuzzleID(635623542);
        p31808220.setLeftExactPuzzleID(1139265004);
        p31808220.setRightExactPuzzleID(315788164);
        p31808220.getDisplayImage().loadImageFromResource(c, R.drawable.p31808220h);
        p31808220.setExactRow(0);
        p31808220.setExactColumn(1);
        p31808220.getSize().reset(126.6667f, 100.0f);
        p31808220.getPositionOffset().reset(-76.66666f, -50.0f);
        p31808220.setIsUseAbsolutePosition(true);
        p31808220.setIsUseAbsoluteSize(true);
        p31808220.getImage().setIsUseAbsolutePosition(true);
        p31808220.getImage().setIsUseAbsoluteSize(true);
        p31808220.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p31808220.resetPosition();
        getSpiritList().add(p31808220);
    }

    /* access modifiers changed from: package-private */
    public void c635623542(Context c) {
        Puzzle p635623542 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load635623542(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save635623542(editor, this);
            }
        };
        p635623542.setID(635623542);
        p635623542.setName("635623542");
        p635623542.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p635623542);
        this.Desktop.RandomlyPlaced(p635623542);
        p635623542.setTopEdgeType(EdgeType.Convex);
        p635623542.setBottomEdgeType(EdgeType.Convex);
        p635623542.setLeftEdgeType(EdgeType.Convex);
        p635623542.setRightEdgeType(EdgeType.Concave);
        p635623542.setTopExactPuzzleID(31808220);
        p635623542.setBottomExactPuzzleID(1804482621);
        p635623542.setLeftExactPuzzleID(1595960395);
        p635623542.setRightExactPuzzleID(1958884097);
        p635623542.getDisplayImage().loadImageFromResource(c, R.drawable.p635623542h);
        p635623542.setExactRow(1);
        p635623542.setExactColumn(1);
        p635623542.getSize().reset(126.6667f, 153.3333f);
        p635623542.getPositionOffset().reset(-76.66666f, -76.66666f);
        p635623542.setIsUseAbsolutePosition(true);
        p635623542.setIsUseAbsoluteSize(true);
        p635623542.getImage().setIsUseAbsolutePosition(true);
        p635623542.getImage().setIsUseAbsoluteSize(true);
        p635623542.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p635623542.resetPosition();
        getSpiritList().add(p635623542);
    }

    /* access modifiers changed from: package-private */
    public void c1804482621(Context c) {
        Puzzle p1804482621 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1804482621(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1804482621(editor, this);
            }
        };
        p1804482621.setID(1804482621);
        p1804482621.setName("1804482621");
        p1804482621.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1804482621);
        this.Desktop.RandomlyPlaced(p1804482621);
        p1804482621.setTopEdgeType(EdgeType.Concave);
        p1804482621.setBottomEdgeType(EdgeType.Concave);
        p1804482621.setLeftEdgeType(EdgeType.Convex);
        p1804482621.setRightEdgeType(EdgeType.Convex);
        p1804482621.setTopExactPuzzleID(635623542);
        p1804482621.setBottomExactPuzzleID(87355417);
        p1804482621.setLeftExactPuzzleID(29639842);
        p1804482621.setRightExactPuzzleID(1349705658);
        p1804482621.getDisplayImage().loadImageFromResource(c, R.drawable.p1804482621h);
        p1804482621.setExactRow(2);
        p1804482621.setExactColumn(1);
        p1804482621.getSize().reset(153.3333f, 100.0f);
        p1804482621.getPositionOffset().reset(-76.66666f, -50.0f);
        p1804482621.setIsUseAbsolutePosition(true);
        p1804482621.setIsUseAbsoluteSize(true);
        p1804482621.getImage().setIsUseAbsolutePosition(true);
        p1804482621.getImage().setIsUseAbsoluteSize(true);
        p1804482621.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1804482621.resetPosition();
        getSpiritList().add(p1804482621);
    }

    /* access modifiers changed from: package-private */
    public void c87355417(Context c) {
        Puzzle p87355417 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load87355417(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save87355417(editor, this);
            }
        };
        p87355417.setID(87355417);
        p87355417.setName("87355417");
        p87355417.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p87355417);
        this.Desktop.RandomlyPlaced(p87355417);
        p87355417.setTopEdgeType(EdgeType.Convex);
        p87355417.setBottomEdgeType(EdgeType.Concave);
        p87355417.setLeftEdgeType(EdgeType.Concave);
        p87355417.setRightEdgeType(EdgeType.Concave);
        p87355417.setTopExactPuzzleID(1804482621);
        p87355417.setBottomExactPuzzleID(2061097774);
        p87355417.setLeftExactPuzzleID(849721877);
        p87355417.setRightExactPuzzleID(305893668);
        p87355417.getDisplayImage().loadImageFromResource(c, R.drawable.p87355417h);
        p87355417.setExactRow(3);
        p87355417.setExactColumn(1);
        p87355417.getSize().reset(100.0f, 126.6667f);
        p87355417.getPositionOffset().reset(-50.0f, -76.66666f);
        p87355417.setIsUseAbsolutePosition(true);
        p87355417.setIsUseAbsoluteSize(true);
        p87355417.getImage().setIsUseAbsolutePosition(true);
        p87355417.getImage().setIsUseAbsoluteSize(true);
        p87355417.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p87355417.resetPosition();
        getSpiritList().add(p87355417);
    }

    /* access modifiers changed from: package-private */
    public void c2061097774(Context c) {
        Puzzle p2061097774 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2061097774(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2061097774(editor, this);
            }
        };
        p2061097774.setID(2061097774);
        p2061097774.setName("2061097774");
        p2061097774.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2061097774);
        this.Desktop.RandomlyPlaced(p2061097774);
        p2061097774.setTopEdgeType(EdgeType.Convex);
        p2061097774.setBottomEdgeType(EdgeType.Flat);
        p2061097774.setLeftEdgeType(EdgeType.Concave);
        p2061097774.setRightEdgeType(EdgeType.Concave);
        p2061097774.setTopExactPuzzleID(87355417);
        p2061097774.setBottomExactPuzzleID(-1);
        p2061097774.setLeftExactPuzzleID(2106307501);
        p2061097774.setRightExactPuzzleID(24207297);
        p2061097774.getDisplayImage().loadImageFromResource(c, R.drawable.p2061097774h);
        p2061097774.setExactRow(4);
        p2061097774.setExactColumn(1);
        p2061097774.getSize().reset(100.0f, 126.6667f);
        p2061097774.getPositionOffset().reset(-50.0f, -76.66666f);
        p2061097774.setIsUseAbsolutePosition(true);
        p2061097774.setIsUseAbsoluteSize(true);
        p2061097774.getImage().setIsUseAbsolutePosition(true);
        p2061097774.getImage().setIsUseAbsoluteSize(true);
        p2061097774.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2061097774.resetPosition();
        getSpiritList().add(p2061097774);
    }

    /* access modifiers changed from: package-private */
    public void c315788164(Context c) {
        Puzzle p315788164 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load315788164(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save315788164(editor, this);
            }
        };
        p315788164.setID(315788164);
        p315788164.setName("315788164");
        p315788164.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p315788164);
        this.Desktop.RandomlyPlaced(p315788164);
        p315788164.setTopEdgeType(EdgeType.Flat);
        p315788164.setBottomEdgeType(EdgeType.Convex);
        p315788164.setLeftEdgeType(EdgeType.Convex);
        p315788164.setRightEdgeType(EdgeType.Concave);
        p315788164.setTopExactPuzzleID(-1);
        p315788164.setBottomExactPuzzleID(1958884097);
        p315788164.setLeftExactPuzzleID(31808220);
        p315788164.setRightExactPuzzleID(2106038466);
        p315788164.getDisplayImage().loadImageFromResource(c, R.drawable.p315788164h);
        p315788164.setExactRow(0);
        p315788164.setExactColumn(2);
        p315788164.getSize().reset(126.6667f, 126.6667f);
        p315788164.getPositionOffset().reset(-76.66666f, -50.0f);
        p315788164.setIsUseAbsolutePosition(true);
        p315788164.setIsUseAbsoluteSize(true);
        p315788164.getImage().setIsUseAbsolutePosition(true);
        p315788164.getImage().setIsUseAbsoluteSize(true);
        p315788164.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p315788164.resetPosition();
        getSpiritList().add(p315788164);
    }

    /* access modifiers changed from: package-private */
    public void c1958884097(Context c) {
        Puzzle p1958884097 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1958884097(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1958884097(editor, this);
            }
        };
        p1958884097.setID(1958884097);
        p1958884097.setName("1958884097");
        p1958884097.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1958884097);
        this.Desktop.RandomlyPlaced(p1958884097);
        p1958884097.setTopEdgeType(EdgeType.Concave);
        p1958884097.setBottomEdgeType(EdgeType.Convex);
        p1958884097.setLeftEdgeType(EdgeType.Convex);
        p1958884097.setRightEdgeType(EdgeType.Concave);
        p1958884097.setTopExactPuzzleID(315788164);
        p1958884097.setBottomExactPuzzleID(1349705658);
        p1958884097.setLeftExactPuzzleID(635623542);
        p1958884097.setRightExactPuzzleID(698311935);
        p1958884097.getDisplayImage().loadImageFromResource(c, R.drawable.p1958884097h);
        p1958884097.setExactRow(1);
        p1958884097.setExactColumn(2);
        p1958884097.getSize().reset(126.6667f, 126.6667f);
        p1958884097.getPositionOffset().reset(-76.66666f, -50.0f);
        p1958884097.setIsUseAbsolutePosition(true);
        p1958884097.setIsUseAbsoluteSize(true);
        p1958884097.getImage().setIsUseAbsolutePosition(true);
        p1958884097.getImage().setIsUseAbsoluteSize(true);
        p1958884097.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1958884097.resetPosition();
        getSpiritList().add(p1958884097);
    }

    /* access modifiers changed from: package-private */
    public void c1349705658(Context c) {
        Puzzle p1349705658 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1349705658(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1349705658(editor, this);
            }
        };
        p1349705658.setID(1349705658);
        p1349705658.setName("1349705658");
        p1349705658.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1349705658);
        this.Desktop.RandomlyPlaced(p1349705658);
        p1349705658.setTopEdgeType(EdgeType.Concave);
        p1349705658.setBottomEdgeType(EdgeType.Convex);
        p1349705658.setLeftEdgeType(EdgeType.Concave);
        p1349705658.setRightEdgeType(EdgeType.Convex);
        p1349705658.setTopExactPuzzleID(1958884097);
        p1349705658.setBottomExactPuzzleID(305893668);
        p1349705658.setLeftExactPuzzleID(1804482621);
        p1349705658.setRightExactPuzzleID(1247890733);
        p1349705658.getDisplayImage().loadImageFromResource(c, R.drawable.p1349705658h);
        p1349705658.setExactRow(2);
        p1349705658.setExactColumn(2);
        p1349705658.getSize().reset(126.6667f, 126.6667f);
        p1349705658.getPositionOffset().reset(-50.0f, -50.0f);
        p1349705658.setIsUseAbsolutePosition(true);
        p1349705658.setIsUseAbsoluteSize(true);
        p1349705658.getImage().setIsUseAbsolutePosition(true);
        p1349705658.getImage().setIsUseAbsoluteSize(true);
        p1349705658.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1349705658.resetPosition();
        getSpiritList().add(p1349705658);
    }

    /* access modifiers changed from: package-private */
    public void c305893668(Context c) {
        Puzzle p305893668 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load305893668(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save305893668(editor, this);
            }
        };
        p305893668.setID(305893668);
        p305893668.setName("305893668");
        p305893668.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p305893668);
        this.Desktop.RandomlyPlaced(p305893668);
        p305893668.setTopEdgeType(EdgeType.Concave);
        p305893668.setBottomEdgeType(EdgeType.Concave);
        p305893668.setLeftEdgeType(EdgeType.Convex);
        p305893668.setRightEdgeType(EdgeType.Convex);
        p305893668.setTopExactPuzzleID(1349705658);
        p305893668.setBottomExactPuzzleID(24207297);
        p305893668.setLeftExactPuzzleID(87355417);
        p305893668.setRightExactPuzzleID(802542692);
        p305893668.getDisplayImage().loadImageFromResource(c, R.drawable.p305893668h);
        p305893668.setExactRow(3);
        p305893668.setExactColumn(2);
        p305893668.getSize().reset(153.3333f, 100.0f);
        p305893668.getPositionOffset().reset(-76.66666f, -50.0f);
        p305893668.setIsUseAbsolutePosition(true);
        p305893668.setIsUseAbsoluteSize(true);
        p305893668.getImage().setIsUseAbsolutePosition(true);
        p305893668.getImage().setIsUseAbsoluteSize(true);
        p305893668.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p305893668.resetPosition();
        getSpiritList().add(p305893668);
    }

    /* access modifiers changed from: package-private */
    public void c24207297(Context c) {
        Puzzle p24207297 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load24207297(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save24207297(editor, this);
            }
        };
        p24207297.setID(24207297);
        p24207297.setName("24207297");
        p24207297.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p24207297);
        this.Desktop.RandomlyPlaced(p24207297);
        p24207297.setTopEdgeType(EdgeType.Convex);
        p24207297.setBottomEdgeType(EdgeType.Flat);
        p24207297.setLeftEdgeType(EdgeType.Convex);
        p24207297.setRightEdgeType(EdgeType.Concave);
        p24207297.setTopExactPuzzleID(305893668);
        p24207297.setBottomExactPuzzleID(-1);
        p24207297.setLeftExactPuzzleID(2061097774);
        p24207297.setRightExactPuzzleID(2050260507);
        p24207297.getDisplayImage().loadImageFromResource(c, R.drawable.p24207297h);
        p24207297.setExactRow(4);
        p24207297.setExactColumn(2);
        p24207297.getSize().reset(126.6667f, 126.6667f);
        p24207297.getPositionOffset().reset(-76.66666f, -76.66666f);
        p24207297.setIsUseAbsolutePosition(true);
        p24207297.setIsUseAbsoluteSize(true);
        p24207297.getImage().setIsUseAbsolutePosition(true);
        p24207297.getImage().setIsUseAbsoluteSize(true);
        p24207297.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p24207297.resetPosition();
        getSpiritList().add(p24207297);
    }

    /* access modifiers changed from: package-private */
    public void c2106038466(Context c) {
        Puzzle p2106038466 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2106038466(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2106038466(editor, this);
            }
        };
        p2106038466.setID(2106038466);
        p2106038466.setName("2106038466");
        p2106038466.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2106038466);
        this.Desktop.RandomlyPlaced(p2106038466);
        p2106038466.setTopEdgeType(EdgeType.Flat);
        p2106038466.setBottomEdgeType(EdgeType.Concave);
        p2106038466.setLeftEdgeType(EdgeType.Convex);
        p2106038466.setRightEdgeType(EdgeType.Convex);
        p2106038466.setTopExactPuzzleID(-1);
        p2106038466.setBottomExactPuzzleID(698311935);
        p2106038466.setLeftExactPuzzleID(315788164);
        p2106038466.setRightExactPuzzleID(2023439901);
        p2106038466.getDisplayImage().loadImageFromResource(c, R.drawable.p2106038466h);
        p2106038466.setExactRow(0);
        p2106038466.setExactColumn(3);
        p2106038466.getSize().reset(153.3333f, 100.0f);
        p2106038466.getPositionOffset().reset(-76.66666f, -50.0f);
        p2106038466.setIsUseAbsolutePosition(true);
        p2106038466.setIsUseAbsoluteSize(true);
        p2106038466.getImage().setIsUseAbsolutePosition(true);
        p2106038466.getImage().setIsUseAbsoluteSize(true);
        p2106038466.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2106038466.resetPosition();
        getSpiritList().add(p2106038466);
    }

    /* access modifiers changed from: package-private */
    public void c698311935(Context c) {
        Puzzle p698311935 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load698311935(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save698311935(editor, this);
            }
        };
        p698311935.setID(698311935);
        p698311935.setName("698311935");
        p698311935.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p698311935);
        this.Desktop.RandomlyPlaced(p698311935);
        p698311935.setTopEdgeType(EdgeType.Convex);
        p698311935.setBottomEdgeType(EdgeType.Convex);
        p698311935.setLeftEdgeType(EdgeType.Convex);
        p698311935.setRightEdgeType(EdgeType.Convex);
        p698311935.setTopExactPuzzleID(2106038466);
        p698311935.setBottomExactPuzzleID(1247890733);
        p698311935.setLeftExactPuzzleID(1958884097);
        p698311935.setRightExactPuzzleID(1382041943);
        p698311935.getDisplayImage().loadImageFromResource(c, R.drawable.p698311935h);
        p698311935.setExactRow(1);
        p698311935.setExactColumn(3);
        p698311935.getSize().reset(153.3333f, 153.3333f);
        p698311935.getPositionOffset().reset(-76.66666f, -76.66666f);
        p698311935.setIsUseAbsolutePosition(true);
        p698311935.setIsUseAbsoluteSize(true);
        p698311935.getImage().setIsUseAbsolutePosition(true);
        p698311935.getImage().setIsUseAbsoluteSize(true);
        p698311935.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p698311935.resetPosition();
        getSpiritList().add(p698311935);
    }

    /* access modifiers changed from: package-private */
    public void c1247890733(Context c) {
        Puzzle p1247890733 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1247890733(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1247890733(editor, this);
            }
        };
        p1247890733.setID(1247890733);
        p1247890733.setName("1247890733");
        p1247890733.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1247890733);
        this.Desktop.RandomlyPlaced(p1247890733);
        p1247890733.setTopEdgeType(EdgeType.Concave);
        p1247890733.setBottomEdgeType(EdgeType.Convex);
        p1247890733.setLeftEdgeType(EdgeType.Concave);
        p1247890733.setRightEdgeType(EdgeType.Concave);
        p1247890733.setTopExactPuzzleID(698311935);
        p1247890733.setBottomExactPuzzleID(802542692);
        p1247890733.setLeftExactPuzzleID(1349705658);
        p1247890733.setRightExactPuzzleID(517003624);
        p1247890733.getDisplayImage().loadImageFromResource(c, R.drawable.p1247890733h);
        p1247890733.setExactRow(2);
        p1247890733.setExactColumn(3);
        p1247890733.getSize().reset(100.0f, 126.6667f);
        p1247890733.getPositionOffset().reset(-50.0f, -50.0f);
        p1247890733.setIsUseAbsolutePosition(true);
        p1247890733.setIsUseAbsoluteSize(true);
        p1247890733.getImage().setIsUseAbsolutePosition(true);
        p1247890733.getImage().setIsUseAbsoluteSize(true);
        p1247890733.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1247890733.resetPosition();
        getSpiritList().add(p1247890733);
    }

    /* access modifiers changed from: package-private */
    public void c802542692(Context c) {
        Puzzle p802542692 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load802542692(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save802542692(editor, this);
            }
        };
        p802542692.setID(802542692);
        p802542692.setName("802542692");
        p802542692.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p802542692);
        this.Desktop.RandomlyPlaced(p802542692);
        p802542692.setTopEdgeType(EdgeType.Concave);
        p802542692.setBottomEdgeType(EdgeType.Concave);
        p802542692.setLeftEdgeType(EdgeType.Concave);
        p802542692.setRightEdgeType(EdgeType.Concave);
        p802542692.setTopExactPuzzleID(1247890733);
        p802542692.setBottomExactPuzzleID(2050260507);
        p802542692.setLeftExactPuzzleID(305893668);
        p802542692.setRightExactPuzzleID(430906672);
        p802542692.getDisplayImage().loadImageFromResource(c, R.drawable.p802542692h);
        p802542692.setExactRow(3);
        p802542692.setExactColumn(3);
        p802542692.getSize().reset(100.0f, 100.0f);
        p802542692.getPositionOffset().reset(-50.0f, -50.0f);
        p802542692.setIsUseAbsolutePosition(true);
        p802542692.setIsUseAbsoluteSize(true);
        p802542692.getImage().setIsUseAbsolutePosition(true);
        p802542692.getImage().setIsUseAbsoluteSize(true);
        p802542692.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p802542692.resetPosition();
        getSpiritList().add(p802542692);
    }

    /* access modifiers changed from: package-private */
    public void c2050260507(Context c) {
        Puzzle p2050260507 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2050260507(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2050260507(editor, this);
            }
        };
        p2050260507.setID(2050260507);
        p2050260507.setName("2050260507");
        p2050260507.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2050260507);
        this.Desktop.RandomlyPlaced(p2050260507);
        p2050260507.setTopEdgeType(EdgeType.Convex);
        p2050260507.setBottomEdgeType(EdgeType.Flat);
        p2050260507.setLeftEdgeType(EdgeType.Convex);
        p2050260507.setRightEdgeType(EdgeType.Convex);
        p2050260507.setTopExactPuzzleID(802542692);
        p2050260507.setBottomExactPuzzleID(-1);
        p2050260507.setLeftExactPuzzleID(24207297);
        p2050260507.setRightExactPuzzleID(449859470);
        p2050260507.getDisplayImage().loadImageFromResource(c, R.drawable.p2050260507h);
        p2050260507.setExactRow(4);
        p2050260507.setExactColumn(3);
        p2050260507.getSize().reset(153.3333f, 126.6667f);
        p2050260507.getPositionOffset().reset(-76.66666f, -76.66666f);
        p2050260507.setIsUseAbsolutePosition(true);
        p2050260507.setIsUseAbsoluteSize(true);
        p2050260507.getImage().setIsUseAbsolutePosition(true);
        p2050260507.getImage().setIsUseAbsoluteSize(true);
        p2050260507.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2050260507.resetPosition();
        getSpiritList().add(p2050260507);
    }

    /* access modifiers changed from: package-private */
    public void c2023439901(Context c) {
        Puzzle p2023439901 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2023439901(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2023439901(editor, this);
            }
        };
        p2023439901.setID(2023439901);
        p2023439901.setName("2023439901");
        p2023439901.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2023439901);
        this.Desktop.RandomlyPlaced(p2023439901);
        p2023439901.setTopEdgeType(EdgeType.Flat);
        p2023439901.setBottomEdgeType(EdgeType.Concave);
        p2023439901.setLeftEdgeType(EdgeType.Concave);
        p2023439901.setRightEdgeType(EdgeType.Concave);
        p2023439901.setTopExactPuzzleID(-1);
        p2023439901.setBottomExactPuzzleID(1382041943);
        p2023439901.setLeftExactPuzzleID(2106038466);
        p2023439901.setRightExactPuzzleID(451329701);
        p2023439901.getDisplayImage().loadImageFromResource(c, R.drawable.p2023439901h);
        p2023439901.setExactRow(0);
        p2023439901.setExactColumn(4);
        p2023439901.getSize().reset(100.0f, 100.0f);
        p2023439901.getPositionOffset().reset(-50.0f, -50.0f);
        p2023439901.setIsUseAbsolutePosition(true);
        p2023439901.setIsUseAbsoluteSize(true);
        p2023439901.getImage().setIsUseAbsolutePosition(true);
        p2023439901.getImage().setIsUseAbsoluteSize(true);
        p2023439901.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2023439901.resetPosition();
        getSpiritList().add(p2023439901);
    }

    /* access modifiers changed from: package-private */
    public void c1382041943(Context c) {
        Puzzle p1382041943 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1382041943(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1382041943(editor, this);
            }
        };
        p1382041943.setID(1382041943);
        p1382041943.setName("1382041943");
        p1382041943.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1382041943);
        this.Desktop.RandomlyPlaced(p1382041943);
        p1382041943.setTopEdgeType(EdgeType.Convex);
        p1382041943.setBottomEdgeType(EdgeType.Convex);
        p1382041943.setLeftEdgeType(EdgeType.Concave);
        p1382041943.setRightEdgeType(EdgeType.Convex);
        p1382041943.setTopExactPuzzleID(2023439901);
        p1382041943.setBottomExactPuzzleID(517003624);
        p1382041943.setLeftExactPuzzleID(698311935);
        p1382041943.setRightExactPuzzleID(305154650);
        p1382041943.getDisplayImage().loadImageFromResource(c, R.drawable.p1382041943h);
        p1382041943.setExactRow(1);
        p1382041943.setExactColumn(4);
        p1382041943.getSize().reset(126.6667f, 153.3333f);
        p1382041943.getPositionOffset().reset(-50.0f, -76.66666f);
        p1382041943.setIsUseAbsolutePosition(true);
        p1382041943.setIsUseAbsoluteSize(true);
        p1382041943.getImage().setIsUseAbsolutePosition(true);
        p1382041943.getImage().setIsUseAbsoluteSize(true);
        p1382041943.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1382041943.resetPosition();
        getSpiritList().add(p1382041943);
    }

    /* access modifiers changed from: package-private */
    public void c517003624(Context c) {
        Puzzle p517003624 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load517003624(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save517003624(editor, this);
            }
        };
        p517003624.setID(517003624);
        p517003624.setName("517003624");
        p517003624.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p517003624);
        this.Desktop.RandomlyPlaced(p517003624);
        p517003624.setTopEdgeType(EdgeType.Concave);
        p517003624.setBottomEdgeType(EdgeType.Concave);
        p517003624.setLeftEdgeType(EdgeType.Convex);
        p517003624.setRightEdgeType(EdgeType.Convex);
        p517003624.setTopExactPuzzleID(1382041943);
        p517003624.setBottomExactPuzzleID(430906672);
        p517003624.setLeftExactPuzzleID(1247890733);
        p517003624.setRightExactPuzzleID(1729751336);
        p517003624.getDisplayImage().loadImageFromResource(c, R.drawable.p517003624h);
        p517003624.setExactRow(2);
        p517003624.setExactColumn(4);
        p517003624.getSize().reset(153.3333f, 100.0f);
        p517003624.getPositionOffset().reset(-76.66666f, -50.0f);
        p517003624.setIsUseAbsolutePosition(true);
        p517003624.setIsUseAbsoluteSize(true);
        p517003624.getImage().setIsUseAbsolutePosition(true);
        p517003624.getImage().setIsUseAbsoluteSize(true);
        p517003624.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p517003624.resetPosition();
        getSpiritList().add(p517003624);
    }

    /* access modifiers changed from: package-private */
    public void c430906672(Context c) {
        Puzzle p430906672 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load430906672(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save430906672(editor, this);
            }
        };
        p430906672.setID(430906672);
        p430906672.setName("430906672");
        p430906672.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p430906672);
        this.Desktop.RandomlyPlaced(p430906672);
        p430906672.setTopEdgeType(EdgeType.Convex);
        p430906672.setBottomEdgeType(EdgeType.Concave);
        p430906672.setLeftEdgeType(EdgeType.Convex);
        p430906672.setRightEdgeType(EdgeType.Convex);
        p430906672.setTopExactPuzzleID(517003624);
        p430906672.setBottomExactPuzzleID(449859470);
        p430906672.setLeftExactPuzzleID(802542692);
        p430906672.setRightExactPuzzleID(892374211);
        p430906672.getDisplayImage().loadImageFromResource(c, R.drawable.p430906672h);
        p430906672.setExactRow(3);
        p430906672.setExactColumn(4);
        p430906672.getSize().reset(153.3333f, 126.6667f);
        p430906672.getPositionOffset().reset(-76.66666f, -76.66666f);
        p430906672.setIsUseAbsolutePosition(true);
        p430906672.setIsUseAbsoluteSize(true);
        p430906672.getImage().setIsUseAbsolutePosition(true);
        p430906672.getImage().setIsUseAbsoluteSize(true);
        p430906672.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p430906672.resetPosition();
        getSpiritList().add(p430906672);
    }

    /* access modifiers changed from: package-private */
    public void c449859470(Context c) {
        Puzzle p449859470 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load449859470(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save449859470(editor, this);
            }
        };
        p449859470.setID(449859470);
        p449859470.setName("449859470");
        p449859470.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p449859470);
        this.Desktop.RandomlyPlaced(p449859470);
        p449859470.setTopEdgeType(EdgeType.Convex);
        p449859470.setBottomEdgeType(EdgeType.Flat);
        p449859470.setLeftEdgeType(EdgeType.Concave);
        p449859470.setRightEdgeType(EdgeType.Concave);
        p449859470.setTopExactPuzzleID(430906672);
        p449859470.setBottomExactPuzzleID(-1);
        p449859470.setLeftExactPuzzleID(2050260507);
        p449859470.setRightExactPuzzleID(763230019);
        p449859470.getDisplayImage().loadImageFromResource(c, R.drawable.p449859470h);
        p449859470.setExactRow(4);
        p449859470.setExactColumn(4);
        p449859470.getSize().reset(100.0f, 126.6667f);
        p449859470.getPositionOffset().reset(-50.0f, -76.66666f);
        p449859470.setIsUseAbsolutePosition(true);
        p449859470.setIsUseAbsoluteSize(true);
        p449859470.getImage().setIsUseAbsolutePosition(true);
        p449859470.getImage().setIsUseAbsoluteSize(true);
        p449859470.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p449859470.resetPosition();
        getSpiritList().add(p449859470);
    }

    /* access modifiers changed from: package-private */
    public void c451329701(Context c) {
        Puzzle p451329701 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load451329701(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save451329701(editor, this);
            }
        };
        p451329701.setID(451329701);
        p451329701.setName("451329701");
        p451329701.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p451329701);
        this.Desktop.RandomlyPlaced(p451329701);
        p451329701.setTopEdgeType(EdgeType.Flat);
        p451329701.setBottomEdgeType(EdgeType.Concave);
        p451329701.setLeftEdgeType(EdgeType.Convex);
        p451329701.setRightEdgeType(EdgeType.Convex);
        p451329701.setTopExactPuzzleID(-1);
        p451329701.setBottomExactPuzzleID(305154650);
        p451329701.setLeftExactPuzzleID(2023439901);
        p451329701.setRightExactPuzzleID(1972244829);
        p451329701.getDisplayImage().loadImageFromResource(c, R.drawable.p451329701h);
        p451329701.setExactRow(0);
        p451329701.setExactColumn(5);
        p451329701.getSize().reset(153.3333f, 100.0f);
        p451329701.getPositionOffset().reset(-76.66666f, -50.0f);
        p451329701.setIsUseAbsolutePosition(true);
        p451329701.setIsUseAbsoluteSize(true);
        p451329701.getImage().setIsUseAbsolutePosition(true);
        p451329701.getImage().setIsUseAbsoluteSize(true);
        p451329701.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p451329701.resetPosition();
        getSpiritList().add(p451329701);
    }

    /* access modifiers changed from: package-private */
    public void c305154650(Context c) {
        Puzzle p305154650 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load305154650(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save305154650(editor, this);
            }
        };
        p305154650.setID(305154650);
        p305154650.setName("305154650");
        p305154650.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p305154650);
        this.Desktop.RandomlyPlaced(p305154650);
        p305154650.setTopEdgeType(EdgeType.Convex);
        p305154650.setBottomEdgeType(EdgeType.Concave);
        p305154650.setLeftEdgeType(EdgeType.Concave);
        p305154650.setRightEdgeType(EdgeType.Convex);
        p305154650.setTopExactPuzzleID(451329701);
        p305154650.setBottomExactPuzzleID(1729751336);
        p305154650.setLeftExactPuzzleID(1382041943);
        p305154650.setRightExactPuzzleID(1109760194);
        p305154650.getDisplayImage().loadImageFromResource(c, R.drawable.p305154650h);
        p305154650.setExactRow(1);
        p305154650.setExactColumn(5);
        p305154650.getSize().reset(126.6667f, 126.6667f);
        p305154650.getPositionOffset().reset(-50.0f, -76.66666f);
        p305154650.setIsUseAbsolutePosition(true);
        p305154650.setIsUseAbsoluteSize(true);
        p305154650.getImage().setIsUseAbsolutePosition(true);
        p305154650.getImage().setIsUseAbsoluteSize(true);
        p305154650.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p305154650.resetPosition();
        getSpiritList().add(p305154650);
    }

    /* access modifiers changed from: package-private */
    public void c1729751336(Context c) {
        Puzzle p1729751336 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1729751336(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1729751336(editor, this);
            }
        };
        p1729751336.setID(1729751336);
        p1729751336.setName("1729751336");
        p1729751336.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1729751336);
        this.Desktop.RandomlyPlaced(p1729751336);
        p1729751336.setTopEdgeType(EdgeType.Convex);
        p1729751336.setBottomEdgeType(EdgeType.Convex);
        p1729751336.setLeftEdgeType(EdgeType.Concave);
        p1729751336.setRightEdgeType(EdgeType.Concave);
        p1729751336.setTopExactPuzzleID(305154650);
        p1729751336.setBottomExactPuzzleID(892374211);
        p1729751336.setLeftExactPuzzleID(517003624);
        p1729751336.setRightExactPuzzleID(928289068);
        p1729751336.getDisplayImage().loadImageFromResource(c, R.drawable.p1729751336h);
        p1729751336.setExactRow(2);
        p1729751336.setExactColumn(5);
        p1729751336.getSize().reset(100.0f, 153.3333f);
        p1729751336.getPositionOffset().reset(-50.0f, -76.66666f);
        p1729751336.setIsUseAbsolutePosition(true);
        p1729751336.setIsUseAbsoluteSize(true);
        p1729751336.getImage().setIsUseAbsolutePosition(true);
        p1729751336.getImage().setIsUseAbsoluteSize(true);
        p1729751336.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1729751336.resetPosition();
        getSpiritList().add(p1729751336);
    }

    /* access modifiers changed from: package-private */
    public void c892374211(Context c) {
        Puzzle p892374211 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load892374211(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save892374211(editor, this);
            }
        };
        p892374211.setID(892374211);
        p892374211.setName("892374211");
        p892374211.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p892374211);
        this.Desktop.RandomlyPlaced(p892374211);
        p892374211.setTopEdgeType(EdgeType.Concave);
        p892374211.setBottomEdgeType(EdgeType.Concave);
        p892374211.setLeftEdgeType(EdgeType.Concave);
        p892374211.setRightEdgeType(EdgeType.Convex);
        p892374211.setTopExactPuzzleID(1729751336);
        p892374211.setBottomExactPuzzleID(763230019);
        p892374211.setLeftExactPuzzleID(430906672);
        p892374211.setRightExactPuzzleID(85982790);
        p892374211.getDisplayImage().loadImageFromResource(c, R.drawable.p892374211h);
        p892374211.setExactRow(3);
        p892374211.setExactColumn(5);
        p892374211.getSize().reset(126.6667f, 100.0f);
        p892374211.getPositionOffset().reset(-50.0f, -50.0f);
        p892374211.setIsUseAbsolutePosition(true);
        p892374211.setIsUseAbsoluteSize(true);
        p892374211.getImage().setIsUseAbsolutePosition(true);
        p892374211.getImage().setIsUseAbsoluteSize(true);
        p892374211.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p892374211.resetPosition();
        getSpiritList().add(p892374211);
    }

    /* access modifiers changed from: package-private */
    public void c763230019(Context c) {
        Puzzle p763230019 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load763230019(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save763230019(editor, this);
            }
        };
        p763230019.setID(763230019);
        p763230019.setName("763230019");
        p763230019.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p763230019);
        this.Desktop.RandomlyPlaced(p763230019);
        p763230019.setTopEdgeType(EdgeType.Convex);
        p763230019.setBottomEdgeType(EdgeType.Flat);
        p763230019.setLeftEdgeType(EdgeType.Convex);
        p763230019.setRightEdgeType(EdgeType.Convex);
        p763230019.setTopExactPuzzleID(892374211);
        p763230019.setBottomExactPuzzleID(-1);
        p763230019.setLeftExactPuzzleID(449859470);
        p763230019.setRightExactPuzzleID(1115224980);
        p763230019.getDisplayImage().loadImageFromResource(c, R.drawable.p763230019h);
        p763230019.setExactRow(4);
        p763230019.setExactColumn(5);
        p763230019.getSize().reset(153.3333f, 126.6667f);
        p763230019.getPositionOffset().reset(-76.66666f, -76.66666f);
        p763230019.setIsUseAbsolutePosition(true);
        p763230019.setIsUseAbsoluteSize(true);
        p763230019.getImage().setIsUseAbsolutePosition(true);
        p763230019.getImage().setIsUseAbsoluteSize(true);
        p763230019.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p763230019.resetPosition();
        getSpiritList().add(p763230019);
    }

    /* access modifiers changed from: package-private */
    public void c1972244829(Context c) {
        Puzzle p1972244829 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1972244829(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1972244829(editor, this);
            }
        };
        p1972244829.setID(1972244829);
        p1972244829.setName("1972244829");
        p1972244829.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1972244829);
        this.Desktop.RandomlyPlaced(p1972244829);
        p1972244829.setTopEdgeType(EdgeType.Flat);
        p1972244829.setBottomEdgeType(EdgeType.Convex);
        p1972244829.setLeftEdgeType(EdgeType.Concave);
        p1972244829.setRightEdgeType(EdgeType.Concave);
        p1972244829.setTopExactPuzzleID(-1);
        p1972244829.setBottomExactPuzzleID(1109760194);
        p1972244829.setLeftExactPuzzleID(451329701);
        p1972244829.setRightExactPuzzleID(1262102120);
        p1972244829.getDisplayImage().loadImageFromResource(c, R.drawable.p1972244829h);
        p1972244829.setExactRow(0);
        p1972244829.setExactColumn(6);
        p1972244829.getSize().reset(100.0f, 126.6667f);
        p1972244829.getPositionOffset().reset(-50.0f, -50.0f);
        p1972244829.setIsUseAbsolutePosition(true);
        p1972244829.setIsUseAbsoluteSize(true);
        p1972244829.getImage().setIsUseAbsolutePosition(true);
        p1972244829.getImage().setIsUseAbsoluteSize(true);
        p1972244829.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1972244829.resetPosition();
        getSpiritList().add(p1972244829);
    }

    /* access modifiers changed from: package-private */
    public void c1109760194(Context c) {
        Puzzle p1109760194 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1109760194(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1109760194(editor, this);
            }
        };
        p1109760194.setID(1109760194);
        p1109760194.setName("1109760194");
        p1109760194.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1109760194);
        this.Desktop.RandomlyPlaced(p1109760194);
        p1109760194.setTopEdgeType(EdgeType.Concave);
        p1109760194.setBottomEdgeType(EdgeType.Concave);
        p1109760194.setLeftEdgeType(EdgeType.Concave);
        p1109760194.setRightEdgeType(EdgeType.Convex);
        p1109760194.setTopExactPuzzleID(1972244829);
        p1109760194.setBottomExactPuzzleID(928289068);
        p1109760194.setLeftExactPuzzleID(305154650);
        p1109760194.setRightExactPuzzleID(1821292);
        p1109760194.getDisplayImage().loadImageFromResource(c, R.drawable.p1109760194h);
        p1109760194.setExactRow(1);
        p1109760194.setExactColumn(6);
        p1109760194.getSize().reset(126.6667f, 100.0f);
        p1109760194.getPositionOffset().reset(-50.0f, -50.0f);
        p1109760194.setIsUseAbsolutePosition(true);
        p1109760194.setIsUseAbsoluteSize(true);
        p1109760194.getImage().setIsUseAbsolutePosition(true);
        p1109760194.getImage().setIsUseAbsoluteSize(true);
        p1109760194.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1109760194.resetPosition();
        getSpiritList().add(p1109760194);
    }

    /* access modifiers changed from: package-private */
    public void c928289068(Context c) {
        Puzzle p928289068 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load928289068(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save928289068(editor, this);
            }
        };
        p928289068.setID(928289068);
        p928289068.setName("928289068");
        p928289068.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p928289068);
        this.Desktop.RandomlyPlaced(p928289068);
        p928289068.setTopEdgeType(EdgeType.Convex);
        p928289068.setBottomEdgeType(EdgeType.Concave);
        p928289068.setLeftEdgeType(EdgeType.Convex);
        p928289068.setRightEdgeType(EdgeType.Concave);
        p928289068.setTopExactPuzzleID(1109760194);
        p928289068.setBottomExactPuzzleID(85982790);
        p928289068.setLeftExactPuzzleID(1729751336);
        p928289068.setRightExactPuzzleID(676570845);
        p928289068.getDisplayImage().loadImageFromResource(c, R.drawable.p928289068h);
        p928289068.setExactRow(2);
        p928289068.setExactColumn(6);
        p928289068.getSize().reset(126.6667f, 126.6667f);
        p928289068.getPositionOffset().reset(-76.66666f, -76.66666f);
        p928289068.setIsUseAbsolutePosition(true);
        p928289068.setIsUseAbsoluteSize(true);
        p928289068.getImage().setIsUseAbsolutePosition(true);
        p928289068.getImage().setIsUseAbsoluteSize(true);
        p928289068.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p928289068.resetPosition();
        getSpiritList().add(p928289068);
    }

    /* access modifiers changed from: package-private */
    public void c85982790(Context c) {
        Puzzle p85982790 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load85982790(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save85982790(editor, this);
            }
        };
        p85982790.setID(85982790);
        p85982790.setName("85982790");
        p85982790.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p85982790);
        this.Desktop.RandomlyPlaced(p85982790);
        p85982790.setTopEdgeType(EdgeType.Convex);
        p85982790.setBottomEdgeType(EdgeType.Convex);
        p85982790.setLeftEdgeType(EdgeType.Concave);
        p85982790.setRightEdgeType(EdgeType.Concave);
        p85982790.setTopExactPuzzleID(928289068);
        p85982790.setBottomExactPuzzleID(1115224980);
        p85982790.setLeftExactPuzzleID(892374211);
        p85982790.setRightExactPuzzleID(2020327835);
        p85982790.getDisplayImage().loadImageFromResource(c, R.drawable.p85982790h);
        p85982790.setExactRow(3);
        p85982790.setExactColumn(6);
        p85982790.getSize().reset(100.0f, 153.3333f);
        p85982790.getPositionOffset().reset(-50.0f, -76.66666f);
        p85982790.setIsUseAbsolutePosition(true);
        p85982790.setIsUseAbsoluteSize(true);
        p85982790.getImage().setIsUseAbsolutePosition(true);
        p85982790.getImage().setIsUseAbsoluteSize(true);
        p85982790.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p85982790.resetPosition();
        getSpiritList().add(p85982790);
    }

    /* access modifiers changed from: package-private */
    public void c1115224980(Context c) {
        Puzzle p1115224980 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1115224980(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1115224980(editor, this);
            }
        };
        p1115224980.setID(1115224980);
        p1115224980.setName("1115224980");
        p1115224980.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1115224980);
        this.Desktop.RandomlyPlaced(p1115224980);
        p1115224980.setTopEdgeType(EdgeType.Concave);
        p1115224980.setBottomEdgeType(EdgeType.Flat);
        p1115224980.setLeftEdgeType(EdgeType.Concave);
        p1115224980.setRightEdgeType(EdgeType.Concave);
        p1115224980.setTopExactPuzzleID(85982790);
        p1115224980.setBottomExactPuzzleID(-1);
        p1115224980.setLeftExactPuzzleID(763230019);
        p1115224980.setRightExactPuzzleID(1349325407);
        p1115224980.getDisplayImage().loadImageFromResource(c, R.drawable.p1115224980h);
        p1115224980.setExactRow(4);
        p1115224980.setExactColumn(6);
        p1115224980.getSize().reset(100.0f, 100.0f);
        p1115224980.getPositionOffset().reset(-50.0f, -50.0f);
        p1115224980.setIsUseAbsolutePosition(true);
        p1115224980.setIsUseAbsoluteSize(true);
        p1115224980.getImage().setIsUseAbsolutePosition(true);
        p1115224980.getImage().setIsUseAbsoluteSize(true);
        p1115224980.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1115224980.resetPosition();
        getSpiritList().add(p1115224980);
    }

    /* access modifiers changed from: package-private */
    public void c1262102120(Context c) {
        Puzzle p1262102120 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1262102120(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1262102120(editor, this);
            }
        };
        p1262102120.setID(1262102120);
        p1262102120.setName("1262102120");
        p1262102120.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1262102120);
        this.Desktop.RandomlyPlaced(p1262102120);
        p1262102120.setTopEdgeType(EdgeType.Flat);
        p1262102120.setBottomEdgeType(EdgeType.Convex);
        p1262102120.setLeftEdgeType(EdgeType.Convex);
        p1262102120.setRightEdgeType(EdgeType.Flat);
        p1262102120.setTopExactPuzzleID(-1);
        p1262102120.setBottomExactPuzzleID(1821292);
        p1262102120.setLeftExactPuzzleID(1972244829);
        p1262102120.setRightExactPuzzleID(-1);
        p1262102120.getDisplayImage().loadImageFromResource(c, R.drawable.p1262102120h);
        p1262102120.setExactRow(0);
        p1262102120.setExactColumn(7);
        p1262102120.getSize().reset(126.6667f, 126.6667f);
        p1262102120.getPositionOffset().reset(-76.66666f, -50.0f);
        p1262102120.setIsUseAbsolutePosition(true);
        p1262102120.setIsUseAbsoluteSize(true);
        p1262102120.getImage().setIsUseAbsolutePosition(true);
        p1262102120.getImage().setIsUseAbsoluteSize(true);
        p1262102120.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1262102120.resetPosition();
        getSpiritList().add(p1262102120);
    }

    /* access modifiers changed from: package-private */
    public void c1821292(Context c) {
        Puzzle p1821292 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1821292(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1821292(editor, this);
            }
        };
        p1821292.setID(1821292);
        p1821292.setName("1821292");
        p1821292.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1821292);
        this.Desktop.RandomlyPlaced(p1821292);
        p1821292.setTopEdgeType(EdgeType.Concave);
        p1821292.setBottomEdgeType(EdgeType.Convex);
        p1821292.setLeftEdgeType(EdgeType.Concave);
        p1821292.setRightEdgeType(EdgeType.Flat);
        p1821292.setTopExactPuzzleID(1262102120);
        p1821292.setBottomExactPuzzleID(676570845);
        p1821292.setLeftExactPuzzleID(1109760194);
        p1821292.setRightExactPuzzleID(-1);
        p1821292.getDisplayImage().loadImageFromResource(c, R.drawable.p1821292h);
        p1821292.setExactRow(1);
        p1821292.setExactColumn(7);
        p1821292.getSize().reset(100.0f, 126.6667f);
        p1821292.getPositionOffset().reset(-50.0f, -50.0f);
        p1821292.setIsUseAbsolutePosition(true);
        p1821292.setIsUseAbsoluteSize(true);
        p1821292.getImage().setIsUseAbsolutePosition(true);
        p1821292.getImage().setIsUseAbsoluteSize(true);
        p1821292.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1821292.resetPosition();
        getSpiritList().add(p1821292);
    }

    /* access modifiers changed from: package-private */
    public void c676570845(Context c) {
        Puzzle p676570845 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load676570845(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save676570845(editor, this);
            }
        };
        p676570845.setID(676570845);
        p676570845.setName("676570845");
        p676570845.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p676570845);
        this.Desktop.RandomlyPlaced(p676570845);
        p676570845.setTopEdgeType(EdgeType.Concave);
        p676570845.setBottomEdgeType(EdgeType.Concave);
        p676570845.setLeftEdgeType(EdgeType.Convex);
        p676570845.setRightEdgeType(EdgeType.Flat);
        p676570845.setTopExactPuzzleID(1821292);
        p676570845.setBottomExactPuzzleID(2020327835);
        p676570845.setLeftExactPuzzleID(928289068);
        p676570845.setRightExactPuzzleID(-1);
        p676570845.getDisplayImage().loadImageFromResource(c, R.drawable.p676570845h);
        p676570845.setExactRow(2);
        p676570845.setExactColumn(7);
        p676570845.getSize().reset(126.6667f, 100.0f);
        p676570845.getPositionOffset().reset(-76.66666f, -50.0f);
        p676570845.setIsUseAbsolutePosition(true);
        p676570845.setIsUseAbsoluteSize(true);
        p676570845.getImage().setIsUseAbsolutePosition(true);
        p676570845.getImage().setIsUseAbsoluteSize(true);
        p676570845.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p676570845.resetPosition();
        getSpiritList().add(p676570845);
    }

    /* access modifiers changed from: package-private */
    public void c2020327835(Context c) {
        Puzzle p2020327835 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2020327835(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2020327835(editor, this);
            }
        };
        p2020327835.setID(2020327835);
        p2020327835.setName("2020327835");
        p2020327835.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2020327835);
        this.Desktop.RandomlyPlaced(p2020327835);
        p2020327835.setTopEdgeType(EdgeType.Convex);
        p2020327835.setBottomEdgeType(EdgeType.Convex);
        p2020327835.setLeftEdgeType(EdgeType.Convex);
        p2020327835.setRightEdgeType(EdgeType.Flat);
        p2020327835.setTopExactPuzzleID(676570845);
        p2020327835.setBottomExactPuzzleID(1349325407);
        p2020327835.setLeftExactPuzzleID(85982790);
        p2020327835.setRightExactPuzzleID(-1);
        p2020327835.getDisplayImage().loadImageFromResource(c, R.drawable.p2020327835h);
        p2020327835.setExactRow(3);
        p2020327835.setExactColumn(7);
        p2020327835.getSize().reset(126.6667f, 153.3333f);
        p2020327835.getPositionOffset().reset(-76.66666f, -76.66666f);
        p2020327835.setIsUseAbsolutePosition(true);
        p2020327835.setIsUseAbsoluteSize(true);
        p2020327835.getImage().setIsUseAbsolutePosition(true);
        p2020327835.getImage().setIsUseAbsoluteSize(true);
        p2020327835.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2020327835.resetPosition();
        getSpiritList().add(p2020327835);
    }

    /* access modifiers changed from: package-private */
    public void c1349325407(Context c) {
        Puzzle p1349325407 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1349325407(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1349325407(editor, this);
            }
        };
        p1349325407.setID(1349325407);
        p1349325407.setName("1349325407");
        p1349325407.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1349325407);
        this.Desktop.RandomlyPlaced(p1349325407);
        p1349325407.setTopEdgeType(EdgeType.Concave);
        p1349325407.setBottomEdgeType(EdgeType.Flat);
        p1349325407.setLeftEdgeType(EdgeType.Convex);
        p1349325407.setRightEdgeType(EdgeType.Flat);
        p1349325407.setTopExactPuzzleID(2020327835);
        p1349325407.setBottomExactPuzzleID(-1);
        p1349325407.setLeftExactPuzzleID(1115224980);
        p1349325407.setRightExactPuzzleID(-1);
        p1349325407.getDisplayImage().loadImageFromResource(c, R.drawable.p1349325407h);
        p1349325407.setExactRow(4);
        p1349325407.setExactColumn(7);
        p1349325407.getSize().reset(126.6667f, 100.0f);
        p1349325407.getPositionOffset().reset(-76.66666f, -50.0f);
        p1349325407.setIsUseAbsolutePosition(true);
        p1349325407.setIsUseAbsoluteSize(true);
        p1349325407.getImage().setIsUseAbsolutePosition(true);
        p1349325407.getImage().setIsUseAbsoluteSize(true);
        p1349325407.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1349325407.resetPosition();
        getSpiritList().add(p1349325407);
    }
}
