package anywheresoftware.b4a.audio;

import android.media.MediaPlayer;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.objects.MediaPlayerWrapper;
import java.io.IOException;

@BA.ShortName("MediaPlayerStream")
public class MediaPlayerStreamWrapper extends MediaPlayerWrapper {
    @BA.Hide
    public void Initialize2(BA ba, String EventName) {
    }

    public void Initialize(final BA ba, String EventName) throws IllegalArgumentException, IllegalStateException, IOException {
        super.Initialize2(ba, EventName);
        this.mp.setAudioStreamType(3);
        this.mp.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                String error;
                switch (what) {
                    case 100:
                        error = "MEDIA_ERROR_SERVER_DIED";
                        break;
                    case 200:
                        error = "MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK";
                        break;
                    default:
                        error = "MEDIA_ERROR_UNKNOWN";
                        break;
                }
                ba.raiseEvent(MediaPlayerStreamWrapper.this, String.valueOf(MediaPlayerStreamWrapper.this.eventName) + "_streamerror", error, Integer.valueOf(extra));
                return false;
            }
        });
        this.mp.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                ba.raiseEvent(MediaPlayerStreamWrapper.this, String.valueOf(MediaPlayerStreamWrapper.this.eventName) + "_streambuffer", Integer.valueOf(percent));
            }
        });
    }

    public void Load(final BA ba, String URL) throws IllegalArgumentException, IllegalStateException, IOException {
        this.mp.reset();
        this.mp.setDataSource(URL);
        this.mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                ba.raiseEvent(MediaPlayerStreamWrapper.this, String.valueOf(MediaPlayerStreamWrapper.this.eventName) + "_streamready", new Object[0]);
            }
        });
        this.mp.prepareAsync();
    }

    @BA.Hide
    public int getPosition() {
        return this.mp.getCurrentPosition();
    }

    @BA.Hide
    public void setPosition(int value) {
        this.mp.seekTo(value);
    }

    public void SetVolume(float Right, float Left) {
        this.mp.setVolume(Right, Left);
    }
}
