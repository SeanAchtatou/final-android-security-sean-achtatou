package anywheresoftware.b4a.objects.drawable;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import anywheresoftware.b4a.AbsObjectWrapper;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import java.util.HashMap;

@BA.ActivityObject
@BA.ShortName("ColorDrawable")
public class ColorDrawable extends AbsObjectWrapper<Drawable> {
    public void Initialize(int Color, int CornerRadius) {
        if (CornerRadius == 0) {
            setObject(new android.graphics.drawable.ColorDrawable(Color));
            return;
        }
        GradientDrawableWithCorners gd = new GradientDrawableWithCorners(GradientDrawable.Orientation.BL_TR, new int[]{Color, Color});
        gd.setCornerRadius((float) CornerRadius);
        setObject(gd);
    }

    @BA.Hide
    public static class GradientDrawableWithCorners extends GradientDrawable {
        public float cornerRadius;

        public GradientDrawableWithCorners(GradientDrawable.Orientation o, int[] colors) {
            super(o, colors);
        }

        public void setCornerRadius(float radius) {
            super.setCornerRadius(radius);
            this.cornerRadius = radius;
        }
    }

    @BA.Hide
    public static Drawable build(Object prev, HashMap<String, Object> d, boolean designer, Object tag) {
        int color = (((Integer) d.get("alpha")).intValue() << 24) | ((((Integer) d.get("color")).intValue() << 8) >>> 8);
        Integer corners = (Integer) d.get("cornerRadius");
        if (corners == null) {
            corners = 0;
        }
        ColorDrawable cd = new ColorDrawable();
        cd.Initialize(color, (int) (BALayout.getDeviceScale() * ((float) corners.intValue())));
        return (Drawable) cd.getObject();
    }
}
