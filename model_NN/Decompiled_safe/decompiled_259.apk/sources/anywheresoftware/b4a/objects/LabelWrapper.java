package anywheresoftware.b4a.objects;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.DynamicBuilder;
import java.util.HashMap;

@BA.ShortName("Label")
@BA.ActivityObject
public class LabelWrapper extends TextViewWrapper<TextView> {
    @BA.Hide
    public void innerInitialize(BA ba, String eventName, boolean keepOldObject) {
        if (!keepOldObject) {
            setObject(new TextView(ba.context));
        }
        super.innerInitialize(ba, eventName, true);
    }

    @BA.Hide
    public static View build(Object prev, HashMap<String, Object> props, boolean designer, Object tag) throws Exception {
        if (prev == null) {
            prev = new TextView((Context) tag);
        }
        TextView v = (TextView) TextViewWrapper.build(prev, props, designer);
        HashMap<String, Object> drawProps = (HashMap) props.get("drawable");
        if (drawProps != null) {
            v.setBackgroundDrawable((Drawable) DynamicBuilder.build(prev, drawProps, designer, null));
        }
        return v;
    }
}
