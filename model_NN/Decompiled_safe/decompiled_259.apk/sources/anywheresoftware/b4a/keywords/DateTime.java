package anywheresoftware.b4a.keywords;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateTime {
    public static final long TicksPerDay = 86400000;
    public static final long TicksPerHour = 3600000;
    public static final long TicksPerMinute = 60000;
    public static final long TicksPerSecond = 1000;
    private static DateTime _instance;
    private Calendar cal = Calendar.getInstance(Locale.US);
    private Date date;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    private SimpleDateFormat timeFormat;

    private static DateTime getInst() {
        if (_instance == null) {
            _instance = new DateTime();
        }
        return _instance;
    }

    private DateTime() {
        this.dateFormat.setLenient(false);
        this.timeFormat = new SimpleDateFormat("HH:mm:ss");
        this.timeFormat.setLenient(false);
        this.date = new Date(0);
    }

    public static long getNow() {
        return System.currentTimeMillis();
    }

    public static String Date(long Ticks) {
        DateTime d = getInst();
        d.date.setTime(Ticks);
        return d.dateFormat.format(d.date);
    }

    public static String Time(long Ticks) {
        DateTime d = getInst();
        d.date.setTime(Ticks);
        return d.timeFormat.format(d.date);
    }

    public static String getTimeFormat() {
        return getInst().timeFormat.toPattern();
    }

    public static void setTimeFormat(String Pattern) {
        getInst().timeFormat.applyPattern(Pattern);
    }

    public static String getDateFormat() {
        return getInst().dateFormat.toPattern();
    }

    public static void setDateFormat(String Pattern) {
        getInst().dateFormat.applyPattern(Pattern);
    }

    public static long DateParse(String Date) throws ParseException {
        return getInst().dateFormat.parse(Date).getTime();
    }

    public static long TimeParse(String Time) throws ParseException {
        long time = getInst().timeFormat.parse(Time).getTime();
        long today = System.currentTimeMillis();
        return (time % TicksPerDay) + (today - (today % TicksPerDay));
    }

    public static int GetYear(long Ticks) {
        getInst().cal.setTimeInMillis(Ticks);
        return getInst().cal.get(1);
    }

    public static int GetMonth(long Ticks) {
        getInst().cal.setTimeInMillis(Ticks);
        return getInst().cal.get(2) + 1;
    }

    public static int GetDayOfMonth(long Ticks) {
        getInst().cal.setTimeInMillis(Ticks);
        return getInst().cal.get(5);
    }

    public static int GetDayOfYear(long Ticks) {
        getInst().cal.setTimeInMillis(Ticks);
        return getInst().cal.get(6);
    }

    public static int GetDayOfWeek(long Ticks) {
        getInst().cal.setTimeInMillis(Ticks);
        return getInst().cal.get(7);
    }

    public static int GetHour(long Ticks) {
        getInst().cal.setTimeInMillis(Ticks);
        return getInst().cal.get(11);
    }

    public static int GetSecond(long Ticks) {
        getInst().cal.setTimeInMillis(Ticks);
        return getInst().cal.get(13);
    }

    public static int GetMinute(long Ticks) {
        getInst().cal.setTimeInMillis(Ticks);
        return getInst().cal.get(12);
    }

    public static long Add(long Ticks, int Years, int Months, int Days) {
        Calendar c = getInst().cal;
        c.setTimeInMillis(Ticks);
        c.add(1, Years);
        c.add(2, Months);
        c.add(6, Days);
        return c.getTimeInMillis();
    }
}
