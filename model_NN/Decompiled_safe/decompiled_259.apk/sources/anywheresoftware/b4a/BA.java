package anywheresoftware.b4a;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import anywheresoftware.b4a.keywords.Common;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Future;

public class BA {
    public static Application applicationContext;
    private static int checkStackTraceEvery50;
    public static final Locale cul = Locale.US;
    public static String debugLine;
    public static int debugLineNum;
    public static boolean debugMode = false;
    public static float density = 1.0f;
    public static final Handler handler = new Handler();
    public static NumberFormat numberFormat;
    public static String packageName;
    private static volatile B4AThreadPool threadPool;
    public final Activity activity;
    public WeakReference<BA> activityBA;
    public final String className;
    public final Context context;
    public final HashMap<String, Method> htSubs = new HashMap<>();
    /* access modifiers changed from: private */
    public boolean ignoreEventsFromOtherThreadsDuringMsgboxError = false;
    /* access modifiers changed from: private */
    public volatile boolean isActivityPaused = true;
    public final boolean isService;
    private Exception lastException = null;
    /* access modifiers changed from: private */
    public ArrayList<Runnable> messagesDuringPaused;
    private int numberOfStackedEvents = 0;
    private int onActivityResultCode = 1;
    private HashMap<Integer, WeakReference<IOnActivityResult>> onActivityResultMap;
    public final BA processBA;
    private Object sender;
    public Service service;
    public final BALayout vg;

    public @interface ActivityObject {
    }

    @Retention(RetentionPolicy.RUNTIME)
    public @interface Author {
        String value();
    }

    public interface B4ARunnable extends Runnable {
    }

    public @interface DesignerName {
        String value();
    }

    @Target({ElementType.TYPE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface DontInheritEvents {
    }

    @Target({ElementType.TYPE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Events {
        String[] values();
    }

    public @interface Hide {
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface Permissions {
        String[] values();
    }

    @Retention(RetentionPolicy.RUNTIME)
    public @interface ShortName {
        String value();
    }

    @Retention(RetentionPolicy.RUNTIME)
    public @interface Version {
        float value();
    }

    public BA(Context context2, BALayout vg2, BA processBA2, String packageName2, String className2) {
        this.context = context2;
        if (context2 != null) {
            density = context2.getResources().getDisplayMetrics().density;
        }
        if (context2 == null || !(context2 instanceof Activity)) {
            this.activity = null;
        } else {
            this.activity = (Activity) context2;
            applicationContext = this.activity.getApplication();
        }
        if (context2 == null || !(context2 instanceof Service)) {
            this.isService = false;
        } else {
            this.isService = true;
            applicationContext = ((Service) context2).getApplication();
        }
        this.processBA = processBA2;
        this.vg = vg2;
        packageName = packageName2;
        this.className = className2;
    }

    public boolean subExists(String sub) {
        if (this.processBA != null) {
            return this.processBA.subExists(sub);
        }
        return this.htSubs.containsKey(sub);
    }

    public Object raiseEvent(Object sender2, String event, Object... params) {
        return raiseEvent2(sender2, false, event, false, params);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0082, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0083, code lost:
        r15.numberOfStackedEvents--;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x008b, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x020b, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:?, code lost:
        r15.ignoreEventsFromOtherThreadsDuringMsgboxError = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x020f, code lost:
        throw r3;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:20:0x0081, B:66:0x018a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object raiseEvent2(java.lang.Object r16, boolean r17, java.lang.String r18, boolean r19, java.lang.Object... r20) {
        /*
            r15 = this;
            anywheresoftware.b4a.BA r3 = r15.processBA
            if (r3 == 0) goto L_0x0015
            anywheresoftware.b4a.BA r3 = r15.processBA
            r4 = r16
            r5 = r17
            r6 = r18
            r7 = r19
            r8 = r20
            java.lang.Object r3 = r3.raiseEvent2(r4, r5, r6, r7, r8)
        L_0x0014:
            return r3
        L_0x0015:
            boolean r3 = r15.isActivityPaused
            if (r3 == 0) goto L_0x0034
            if (r17 != 0) goto L_0x0034
            java.io.PrintStream r3 = java.lang.System.out
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "ignoring event: "
            r4.<init>(r5)
            r0 = r4
            r1 = r18
            java.lang.StringBuilder r4 = r0.append(r1)
            java.lang.String r4 = r4.toString()
            r3.println(r4)
            r3 = 0
            goto L_0x0014
        L_0x0034:
            int r3 = r15.numberOfStackedEvents     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            int r3 = r3 + 1
            r15.numberOfStackedEvents = r3     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            r0 = r16
            r1 = r15
            r1.sender = r0     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            java.util.HashMap<java.lang.String, java.lang.reflect.Method> r3 = r15.htSubs     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            r0 = r3
            r1 = r18
            java.lang.Object r12 = r0.get(r1)     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            java.lang.reflect.Method r12 = (java.lang.reflect.Method) r12     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            if (r12 == 0) goto L_0x008c
            android.content.Context r3 = r15.context     // Catch:{ IllegalArgumentException -> 0x005f }
            r0 = r12
            r1 = r3
            r2 = r20
            java.lang.Object r3 = r0.invoke(r1, r2)     // Catch:{ IllegalArgumentException -> 0x005f }
            int r4 = r15.numberOfStackedEvents
            r5 = 1
            int r4 = r4 - r5
            r15.numberOfStackedEvents = r4
            r16 = 0
            goto L_0x0014
        L_0x005f:
            r3 = move-exception
            r11 = r3
            java.lang.Exception r3 = new java.lang.Exception     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            java.lang.String r5 = "Sub "
            r4.<init>(r5)     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            r0 = r4
            r1 = r18
            java.lang.StringBuilder r4 = r0.append(r1)     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            java.lang.String r5 = " signature does not match expected signature."
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            java.lang.String r4 = r4.toString()     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            r3.<init>(r4)     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            throw r3     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
        L_0x007f:
            r3 = move-exception
            r11 = r3
            throw r11     // Catch:{ all -> 0x0082 }
        L_0x0082:
            r3 = move-exception
            int r4 = r15.numberOfStackedEvents
            r5 = 1
            int r4 = r4 - r5
            r15.numberOfStackedEvents = r4
            r16 = 0
            throw r3
        L_0x008c:
            if (r19 == 0) goto L_0x0210
            java.lang.Exception r3 = new java.lang.Exception     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            java.lang.String r5 = "Sub "
            r4.<init>(r5)     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            r0 = r4
            r1 = r18
            java.lang.StringBuilder r4 = r0.append(r1)     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            java.lang.String r5 = " was not found."
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            java.lang.String r4 = r4.toString()     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            r3.<init>(r4)     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
            throw r3     // Catch:{ B4AUncaughtException -> 0x007f, Throwable -> 0x00ac }
        L_0x00ac:
            r3 = move-exception
            r11 = r3
            boolean r3 = r11 instanceof java.lang.reflect.InvocationTargetException     // Catch:{ all -> 0x0082 }
            if (r3 == 0) goto L_0x00b6
            java.lang.Throwable r11 = r11.getCause()     // Catch:{ all -> 0x0082 }
        L_0x00b6:
            boolean r3 = r11 instanceof anywheresoftware.b4a.B4AUncaughtException     // Catch:{ all -> 0x0082 }
            if (r3 == 0) goto L_0x00d4
            int r3 = r15.numberOfStackedEvents     // Catch:{ all -> 0x0082 }
            r4 = 1
            if (r3 <= r4) goto L_0x00c2
            anywheresoftware.b4a.B4AUncaughtException r11 = (anywheresoftware.b4a.B4AUncaughtException) r11     // Catch:{ all -> 0x0082 }
            throw r11     // Catch:{ all -> 0x0082 }
        L_0x00c2:
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ all -> 0x0082 }
            java.lang.String r4 = "catching B4AUncaughtException"
            r3.println(r4)     // Catch:{ all -> 0x0082 }
            int r3 = r15.numberOfStackedEvents
            r4 = 1
            int r3 = r3 - r4
            r15.numberOfStackedEvents = r3
            r16 = 0
            r3 = 0
            goto L_0x0014
        L_0x00d4:
            boolean r3 = r11 instanceof java.lang.Error     // Catch:{ all -> 0x0082 }
            if (r3 == 0) goto L_0x00db
            java.lang.Error r11 = (java.lang.Error) r11     // Catch:{ all -> 0x0082 }
            throw r11     // Catch:{ all -> 0x0082 }
        L_0x00db:
            java.lang.String r14 = ""
            java.lang.StackTraceElement[] r3 = r11.getStackTrace()     // Catch:{ all -> 0x0082 }
            int r4 = r3.length     // Catch:{ all -> 0x0082 }
            r5 = 0
        L_0x00e3:
            if (r5 < r4) goto L_0x0101
        L_0x00e5:
            int r3 = r14.length()     // Catch:{ all -> 0x0082 }
            if (r3 <= 0) goto L_0x00f0
            java.lang.String r3 = "B4A"
            android.util.Log.e(r3, r14)     // Catch:{ all -> 0x0082 }
        L_0x00f0:
            java.lang.String r3 = "B4A"
            java.lang.String r4 = ""
            android.util.Log.e(r3, r4, r11)     // Catch:{ all -> 0x0082 }
            java.lang.ref.WeakReference<anywheresoftware.b4a.BA> r3 = r15.activityBA     // Catch:{ all -> 0x0082 }
            if (r3 != 0) goto L_0x0185
            java.lang.RuntimeException r3 = new java.lang.RuntimeException     // Catch:{ all -> 0x0082 }
            r3.<init>(r11)     // Catch:{ all -> 0x0082 }
            throw r3     // Catch:{ all -> 0x0082 }
        L_0x0101:
            r13 = r3[r5]     // Catch:{ all -> 0x0082 }
            java.lang.String r6 = r13.getClassName()     // Catch:{ all -> 0x0082 }
            java.lang.String r7 = anywheresoftware.b4a.BA.packageName     // Catch:{ all -> 0x0082 }
            boolean r6 = r6.startsWith(r7)     // Catch:{ all -> 0x0082 }
            if (r6 == 0) goto L_0x0181
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0082 }
            java.lang.String r4 = r13.getClassName()     // Catch:{ all -> 0x0082 }
            java.lang.String r5 = anywheresoftware.b4a.BA.packageName     // Catch:{ all -> 0x0082 }
            int r5 = r5.length()     // Catch:{ all -> 0x0082 }
            int r5 = r5 + 1
            java.lang.String r4 = r4.substring(r5)     // Catch:{ all -> 0x0082 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ all -> 0x0082 }
            r3.<init>(r4)     // Catch:{ all -> 0x0082 }
            java.lang.String r4 = r13.getMethodName()     // Catch:{ all -> 0x0082 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0082 }
            java.lang.String r14 = r3.toString()     // Catch:{ all -> 0x0082 }
            java.lang.String r3 = anywheresoftware.b4a.BA.debugLine     // Catch:{ all -> 0x0082 }
            if (r3 == 0) goto L_0x015e
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0082 }
            java.lang.String r4 = java.lang.String.valueOf(r14)     // Catch:{ all -> 0x0082 }
            r3.<init>(r4)     // Catch:{ all -> 0x0082 }
            java.lang.String r4 = " (B4A line: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0082 }
            int r4 = anywheresoftware.b4a.BA.debugLineNum     // Catch:{ all -> 0x0082 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0082 }
            java.lang.String r4 = ")\n"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0082 }
            java.lang.String r4 = anywheresoftware.b4a.BA.debugLine     // Catch:{ all -> 0x0082 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0082 }
            java.lang.String r14 = r3.toString()     // Catch:{ all -> 0x0082 }
            goto L_0x00e5
        L_0x015e:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0082 }
            java.lang.String r4 = java.lang.String.valueOf(r14)     // Catch:{ all -> 0x0082 }
            r3.<init>(r4)     // Catch:{ all -> 0x0082 }
            java.lang.String r4 = " (java line: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0082 }
            int r4 = r13.getLineNumber()     // Catch:{ all -> 0x0082 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0082 }
            java.lang.String r4 = ")"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0082 }
            java.lang.String r14 = r3.toString()     // Catch:{ all -> 0x0082 }
            goto L_0x00e5
        L_0x0181:
            int r5 = r5 + 1
            goto L_0x00e3
        L_0x0185:
            r3 = 1
            r15.ignoreEventsFromOtherThreadsDuringMsgboxError = r3     // Catch:{ all -> 0x0082 }
            java.lang.String r3 = "B4A"
            java.lang.String r4 = r11.toString()     // Catch:{ all -> 0x020b }
            android.util.Log.e(r3, r4)     // Catch:{ all -> 0x020b }
            android.app.AlertDialog$Builder r9 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x020b }
            java.lang.ref.WeakReference<anywheresoftware.b4a.BA> r3 = r15.activityBA     // Catch:{ all -> 0x020b }
            java.lang.Object r16 = r3.get()     // Catch:{ all -> 0x020b }
            anywheresoftware.b4a.BA r16 = (anywheresoftware.b4a.BA) r16     // Catch:{ all -> 0x020b }
            r0 = r16
            android.content.Context r0 = r0.context     // Catch:{ all -> 0x020b }
            r3 = r0
            r9.<init>(r3)     // Catch:{ all -> 0x020b }
            java.lang.String r3 = "Error occurred"
            r9.setTitle(r3)     // Catch:{ all -> 0x020b }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x020b }
            java.lang.String r4 = "An error has occurred in sub:"
            r3.<init>(r4)     // Catch:{ all -> 0x020b }
            java.lang.StringBuilder r3 = r3.append(r14)     // Catch:{ all -> 0x020b }
            java.lang.String r4 = "\n"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x020b }
            java.lang.String r4 = r11.toString()     // Catch:{ all -> 0x020b }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x020b }
            java.lang.String r4 = "\nContinue?"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x020b }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x020b }
            r9.setMessage(r3)     // Catch:{ all -> 0x020b }
            anywheresoftware.b4a.Msgbox$DialogResponse r10 = new anywheresoftware.b4a.Msgbox$DialogResponse     // Catch:{ all -> 0x020b }
            r3 = 0
            r10.<init>(r3)     // Catch:{ all -> 0x020b }
            java.lang.String r3 = "Yes"
            r9.setPositiveButton(r3, r10)     // Catch:{ all -> 0x020b }
            java.lang.String r3 = "No"
            r9.setNegativeButton(r3, r10)     // Catch:{ all -> 0x020b }
            android.app.AlertDialog r3 = r9.create()     // Catch:{ all -> 0x020b }
            int r4 = r15.numberOfStackedEvents     // Catch:{ all -> 0x020b }
            r5 = 1
            if (r4 != r5) goto L_0x0209
            r4 = 1
        L_0x01e8:
            anywheresoftware.b4a.Msgbox.msgbox(r3, r4)     // Catch:{ all -> 0x020b }
            int r3 = r10.res     // Catch:{ all -> 0x020b }
            r4 = -2
            if (r3 != r4) goto L_0x01fb
            int r3 = android.os.Process.myPid()     // Catch:{ all -> 0x020b }
            android.os.Process.killProcess(r3)     // Catch:{ all -> 0x020b }
            r3 = 0
            java.lang.System.exit(r3)     // Catch:{ all -> 0x020b }
        L_0x01fb:
            r3 = 0
            r15.ignoreEventsFromOtherThreadsDuringMsgboxError = r3     // Catch:{ all -> 0x0082 }
            int r3 = r15.numberOfStackedEvents
            r4 = 1
            int r3 = r3 - r4
            r15.numberOfStackedEvents = r3
            r16 = 0
        L_0x0206:
            r3 = 0
            goto L_0x0014
        L_0x0209:
            r4 = 0
            goto L_0x01e8
        L_0x020b:
            r3 = move-exception
            r4 = 0
            r15.ignoreEventsFromOtherThreadsDuringMsgboxError = r4     // Catch:{ all -> 0x0082 }
            throw r3     // Catch:{ all -> 0x0082 }
        L_0x0210:
            int r3 = r15.numberOfStackedEvents
            r4 = 1
            int r3 = r3 - r4
            r15.numberOfStackedEvents = r3
            r16 = 0
            goto L_0x0206
        */
        throw new UnsupportedOperationException("Method not decompiled: anywheresoftware.b4a.BA.raiseEvent2(java.lang.Object, boolean, java.lang.String, boolean, java.lang.Object[]):java.lang.Object");
    }

    public Object raiseEventFromDifferentThread(Object sender2, Object container, int TaskId, String event, boolean throwErrorIfMissingSub, Object[] params) {
        if (this.processBA != null) {
            return this.processBA.raiseEventFromDifferentThread(sender2, container, TaskId, event, throwErrorIfMissingSub, params);
        }
        final String str = event;
        final Object obj = container;
        final int i = TaskId;
        final Object obj2 = sender2;
        final boolean z = throwErrorIfMissingSub;
        final Object[] objArr = params;
        handler.post(new B4ARunnable() {
            public void run() {
                if (BA.this.ignoreEventsFromOtherThreadsDuringMsgboxError) {
                    Log.w("B4A", "Event: " + str + ", was ignored.");
                } else if (!BA.this.isService && BA.this.activityBA == null) {
                    Log.v("B4A", "Reposting event: " + str);
                    BA.handler.post(this);
                } else if (!BA.this.isActivityPaused) {
                    if (obj != null) {
                        BA.markTaskAsFinish(obj, i);
                    }
                    BA.this.raiseEvent2(obj2, false, str, z, objArr);
                } else if (BA.this.isService) {
                    Log.w("B4A", "Ignoring event as service was destroyed.");
                } else {
                    Log.w("B4A", "sending message to waiting queue.");
                    if (BA.this.messagesDuringPaused == null) {
                        BA.this.messagesDuringPaused = new ArrayList();
                    }
                    if (BA.this.messagesDuringPaused.size() > 10) {
                        Log.w("B4A", "Ignoring event (too many queued events: " + str + ")");
                    } else {
                        BA.this.messagesDuringPaused.add(this);
                    }
                }
            }
        });
        return null;
    }

    /* access modifiers changed from: private */
    public static void markTaskAsFinish(Object container, int TaskId) {
        if (threadPool != null) {
            threadPool.markTaskAsFinished(container, TaskId);
        }
    }

    public static Future<?> submitRunnable(Runnable runnable, Object container, int TaskId) {
        if (threadPool == null) {
            synchronized (BA.class) {
                if (threadPool == null) {
                    threadPool = new B4AThreadPool();
                }
            }
        }
        if (container instanceof ObjectWrapper) {
            container = ((ObjectWrapper) container).getObject();
        }
        return threadPool.submit(runnable, container, TaskId);
    }

    public static boolean isTaskRunning(Object container, int TaskId) {
        if (threadPool == null) {
            return false;
        }
        return threadPool.isRunning(container, TaskId);
    }

    public void loadHtSubs(Class<?> cls) {
        for (Method m : cls.getDeclaredMethods()) {
            if (m.getName().startsWith("_")) {
                this.htSubs.put(m.getName().substring(1).toLowerCase(cul), m);
            }
        }
    }

    public boolean isActivityPaused() {
        if (this.processBA != null) {
            return this.processBA.isActivityPaused();
        }
        return this.isActivityPaused;
    }

    public void setActivityPaused(boolean value) {
        if (this.processBA != null) {
            this.processBA.setActivityPaused(value);
            return;
        }
        this.isActivityPaused = value;
        if (!value && this.messagesDuringPaused != null && this.messagesDuringPaused.size() > 0) {
            try {
                Log.w("B4A", "running waiting messages (" + this.messagesDuringPaused.size() + ")");
                Iterator<Runnable> it = this.messagesDuringPaused.iterator();
                while (it.hasNext()) {
                    it.next().run();
                }
            } finally {
                this.messagesDuringPaused.clear();
            }
        }
    }

    public synchronized void startActivityForResult(IOnActivityResult iOnActivityResult, Intent intent) {
        BA aBa;
        if (this.processBA != null) {
            this.processBA.startActivityForResult(iOnActivityResult, intent);
        } else if (!(this.activityBA == null || (aBa = this.activityBA.get()) == null)) {
            if (this.onActivityResultMap == null) {
                this.onActivityResultMap = new HashMap<>();
            }
            this.onActivityResultMap.put(Integer.valueOf(this.onActivityResultCode), new WeakReference(iOnActivityResult));
            try {
                Activity activity2 = aBa.activity;
                int i = this.onActivityResultCode;
                this.onActivityResultCode = i + 1;
                activity2.startActivityForResult(intent, i);
            } catch (ActivityNotFoundException e) {
                this.onActivityResultMap.remove(Integer.valueOf(this.onActivityResultCode));
                iOnActivityResult.ResultArrived(0, null);
            }
        }
        return;
    }

    public void onActivityResult(int request, final int result, final Intent intent) {
        if (this.onActivityResultMap != null) {
            WeakReference<IOnActivityResult> wi = this.onActivityResultMap.get(Integer.valueOf(request));
            if (wi == null) {
                Log.w("B4A", "onActivityResult: wi is null");
                return;
            }
            this.onActivityResultMap.remove(Integer.valueOf(request));
            final IOnActivityResult i = (IOnActivityResult) wi.get();
            if (i == null) {
                Log.w("B4A", "onActivityResult: IOnActivityResult was released");
                return;
            }
            if (this.messagesDuringPaused == null) {
                this.messagesDuringPaused = new ArrayList<>();
            }
            this.messagesDuringPaused.add(new Runnable() {
                public void run() {
                    try {
                        i.ResultArrived(result, intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public static boolean parseBoolean(String b) {
        if (b.equals("true")) {
            return true;
        }
        if (b.equals("false")) {
            return false;
        }
        throw new RuntimeException("Cannot parse: " + b + " as boolean");
    }

    public static char CharFromString(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        return s.charAt(0);
    }

    public Object getSender() {
        if (this.processBA != null) {
            return this.processBA.getSender();
        }
        return this.sender;
    }

    public Exception getLastException() {
        if (this.processBA != null) {
            return this.processBA.getLastException();
        }
        return this.lastException;
    }

    public void setLastException(Exception e) {
        while (e != null && e.getCause() != null && (e instanceof Exception)) {
            e = (Exception) e.getCause();
        }
        this.lastException = e;
    }

    public static <T extends Enum<T>> T getEnumFromString(Class<T> enumType, String name) {
        return Enum.valueOf(enumType, name);
    }

    public static String NumberToString(double value) {
        String s = Double.toString(value);
        if (s.length() > 2 && s.charAt(s.length() - 2) == '.' && s.charAt(s.length() - 1) == '0') {
            return s.substring(0, s.length() - 2);
        }
        return s;
    }

    public static String NumberToString(int value) {
        return Integer.toString(value);
    }

    public static String NumberToString(long value) {
        return Long.toString(value);
    }

    public static double ObjectToNumber(Object o) {
        if (o instanceof Number) {
            return ((Number) o).doubleValue();
        }
        return Double.parseDouble(String.valueOf(o));
    }

    public static boolean ObjectToBoolean(Object o) {
        if (o instanceof Boolean) {
            return ((Boolean) o).booleanValue();
        }
        return parseBoolean(String.valueOf(o));
    }

    public static char ObjectToChar(Object o) {
        if (o instanceof Character) {
            return ((Character) o).charValue();
        }
        return CharFromString(o.toString());
    }

    public static String TypeToString(Object o) {
        try {
            int i = checkStackTraceEvery50 + 1;
            checkStackTraceEvery50 = i;
            if (i % 50 != 0 || Thread.currentThread().getStackTrace().length < 150) {
                StringBuilder sb = new StringBuilder();
                sb.append("[");
                int i2 = 0;
                for (Field f : o.getClass().getDeclaredFields()) {
                    f.setAccessible(true);
                    sb.append(f.getName()).append("=").append(String.valueOf(f.get(o)));
                    i2++;
                    if (i2 % 3 == 0) {
                        sb.append(Common.CRLF);
                    }
                    sb.append(", ");
                }
                if (sb.length() >= 2) {
                    sb.setLength(sb.length() - 2);
                }
                sb.append("]");
                return sb.toString();
            }
            checkStackTraceEvery50--;
            return "";
        } catch (Exception e) {
            return "N/A";
        }
    }

    public static <T> T gm(Map map, Object key, T defValue) {
        T o = map.get(key);
        if (o == null) {
            return defValue;
        }
        return o;
    }

    public static int switchObjectToInt(Object test, Object... values) {
        if (test instanceof Number) {
            double t = ((Number) test).doubleValue();
            for (int i = 0; i < values.length; i++) {
                if (t == ((Number) values[i]).doubleValue()) {
                    return i;
                }
            }
            return -1;
        }
        for (int i2 = 0; i2 < values.length; i2++) {
            if (test.equals(values[i2])) {
                return i2;
            }
        }
        return -1;
    }
}
