package com.madelephantstudios.tennistapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.B4AMenuItem;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.Msgbox;
import anywheresoftware.b4a.admobwrapper.AdViewWrapper;
import anywheresoftware.b4a.audio.SoundPoolWrapper;
import anywheresoftware.b4a.keywords.Common;
import anywheresoftware.b4a.keywords.constants.Colors;
import anywheresoftware.b4a.keywords.constants.Gravity;
import anywheresoftware.b4a.keywords.constants.TypefaceWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import anywheresoftware.b4a.objects.ImageViewWrapper;
import anywheresoftware.b4a.objects.LabelWrapper;
import anywheresoftware.b4a.objects.PanelWrapper;
import anywheresoftware.b4a.objects.Timer;
import anywheresoftware.b4a.objects.ViewWrapper;
import anywheresoftware.b4a.objects.drawable.CanvasWrapper;
import anywheresoftware.b4a.objects.drawable.GradientDrawable;
import anywheresoftware.b4a.objects.streams.File;
import anywheresoftware.b4a.phone.Phone;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public class activitygame extends Activity implements B4AActivity {
    public static boolean _bs = false;
    public static boolean _bv = false;
    public static int _deg1 = 0;
    public static int _deg2 = 0;
    public static int _deg3 = 0;
    public static int _hiscore = 0;
    public static int _score = 0;
    public static int _soundid = 0;
    public static Timer _tmr = null;
    public static double _velx = 0.0d;
    public static double _velx2 = 0.0d;
    public static double _velx3 = 0.0d;
    public static double _velx4 = 0.0d;
    public static double _velx5 = 0.0d;
    public static double _vely = 0.0d;
    public static double _vely2 = 0.0d;
    public static double _vely3 = 0.0d;
    public static double _vely4 = 0.0d;
    public static double _vely5 = 0.0d;
    static boolean afterFirstLayout = false;
    private static final boolean fullScreen = true;
    private static final boolean includeTitle = false;
    static boolean isFirst = true;
    static activitygame mostCurrent;
    public static WeakReference<Activity> previousOne;
    public static BA processBA;
    private static boolean processGlobalsRun = false;
    public Common __c = null;
    ActivityWrapper _activity;
    public activitysettings _activitysettings = null;
    public AdViewWrapper _ad = null;
    public CanvasWrapper.BitmapWrapper _bmp = null;
    public CanvasWrapper.BitmapWrapper _bmp2 = null;
    public CanvasWrapper.BitmapWrapper _bmp3 = null;
    public CanvasWrapper _cnv1 = null;
    public CanvasWrapper _cnv2 = null;
    public CanvasWrapper _cnv3 = null;
    public LabelWrapper _lblhiscore = null;
    public LabelWrapper _lblscore = null;
    public main _main = null;
    public PanelWrapper _pnl = null;
    public PanelWrapper _pnl2 = null;
    public PanelWrapper _pnl3 = null;
    public PanelWrapper _pnl4 = null;
    public PanelWrapper _pnl5 = null;
    public ImageViewWrapper _pnlbg = null;
    public PanelWrapper _pnlground = null;
    public Phone.PhoneVibrate _px = null;
    public CanvasWrapper.RectWrapper _rect1 = null;
    public SoundPoolWrapper _sounds = null;
    BA activityBA;
    BALayout layout;
    ArrayList<B4AMenuItem> menuItems;
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;

    public void onCreate(Bundle bundle) {
        Activity activity;
        super.onCreate(bundle);
        if (isFirst) {
            processBA = new BA(getApplicationContext(), null, null, "com.madelephantstudios.tennistapp", "activitygame");
            processBA.loadHtSubs(getClass());
            BALayout.setDeviceScale(getApplicationContext().getResources().getDisplayMetrics().density);
        } else if (!(previousOne == null || (activity = previousOne.get()) == null || activity == this)) {
            Common.Log("Killing previous instance (activitygame).");
            activity.finish();
        }
        getWindow().requestFeature(1);
        getWindow().setFlags(1024, 1024);
        mostCurrent = this;
        processBA.activityBA = null;
        this.layout = new BALayout(this);
        setContentView(this.layout);
        afterFirstLayout = false;
        BA.handler.postDelayed(new WaitForLayout(), 5);
    }

    private static class WaitForLayout implements Runnable {
        private WaitForLayout() {
        }

        public void run() {
            if (!activitygame.afterFirstLayout) {
                if (activitygame.mostCurrent.layout.getWidth() == 0) {
                    BA.handler.postDelayed(this, 5);
                    return;
                }
                activitygame.mostCurrent.layout.getLayoutParams().height = activitygame.mostCurrent.layout.getHeight();
                activitygame.mostCurrent.layout.getLayoutParams().width = activitygame.mostCurrent.layout.getWidth();
                activitygame.afterFirstLayout = true;
                activitygame.mostCurrent.afterFirstLayout();
            }
        }
    }

    /* access modifiers changed from: private */
    public void afterFirstLayout() {
        this.activityBA = new BA(this, this.layout, processBA, "com.madelephantstudios.tennistapp", "activitygame");
        processBA.activityBA = new WeakReference<>(this.activityBA);
        this._activity = new ActivityWrapper(this.activityBA, "activity");
        Msgbox.isDismissing = false;
        initializeProcessGlobals();
        initializeGlobals();
        ViewWrapper.lastId = 0;
        Common.Log("** Activity (activitygame) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, Boolean.valueOf(isFirst));
        isFirst = false;
        if (mostCurrent != null && mostCurrent == this) {
            processBA.setActivityPaused(false);
            Common.Log("** Activity (activitygame) Resume **");
            processBA.raiseEvent(null, "activity_resume", new Object[0]);
        }
    }

    public void addMenuItem(B4AMenuItem b4AMenuItem) {
        if (this.menuItems == null) {
            this.menuItems = new ArrayList<>();
        }
        this.menuItems.add(b4AMenuItem);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (this.menuItems == null) {
            return false;
        }
        Iterator<B4AMenuItem> it = this.menuItems.iterator();
        while (it.hasNext()) {
            B4AMenuItem next = it.next();
            MenuItem add = menu.add(next.title);
            if (next.drawable != null) {
                add.setIcon(next.drawable);
            }
            add.setOnMenuItemClickListener(new B4AMenuItemsClickListener(next.eventName.toLowerCase(BA.cul)));
        }
        return true;
    }

    private class B4AMenuItemsClickListener implements MenuItem.OnMenuItemClickListener {
        private final String eventName;

        public B4AMenuItemsClickListener(String str) {
            this.eventName = str;
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            activitygame.processBA.raiseEvent(menuItem.getTitle(), this.eventName + "_click", new Object[0]);
            return true;
        }
    }

    public static Class<?> getObject() {
        return activitygame.class;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.onKeySubExist == null) {
            this.onKeySubExist = Boolean.valueOf(processBA.subExists("activity_keypress"));
        }
        if (this.onKeySubExist.booleanValue()) {
            Boolean bool = (Boolean) processBA.raiseEvent2(this._activity, false, "activity_keypress", false, Integer.valueOf(i));
            if (bool == null || bool.booleanValue()) {
                return true;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.onKeyUpSubExist == null) {
            this.onKeyUpSubExist = Boolean.valueOf(processBA.subExists("activity_keyup"));
        }
        if (this.onKeyUpSubExist.booleanValue()) {
            Boolean bool = (Boolean) processBA.raiseEvent2(this._activity, false, "activity_keyup", false, Integer.valueOf(i));
            if (bool == null || bool.booleanValue()) {
                return true;
            }
        }
        return super.onKeyUp(i, keyEvent);
    }

    public void onNewIntent(Intent intent) {
        setIntent(intent);
    }

    public void onPause() {
        super.onPause();
        if (this._activity != null) {
            Msgbox.dismiss(true);
            Common.Log("** Activity (activitygame) Pause, UserClosed = " + this.activityBA.activity.isFinishing() + " **");
            processBA.raiseEvent2(this._activity, true, "activity_pause", false, Boolean.valueOf(this.activityBA.activity.isFinishing()));
            processBA.setActivityPaused(true);
            mostCurrent = null;
            if (!this.activityBA.activity.isFinishing()) {
                previousOne = new WeakReference<>(this);
            }
            Msgbox.isDismissing = false;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        previousOne = null;
    }

    public void onResume() {
        super.onResume();
        mostCurrent = this;
        Msgbox.isDismissing = false;
        if (this.activityBA != null) {
            BA.handler.post(new ResumeMessage(mostCurrent));
        }
    }

    private static class ResumeMessage implements Runnable {
        private final WeakReference<Activity> activity;

        public ResumeMessage(Activity activity2) {
            this.activity = new WeakReference<>(activity2);
        }

        public void run() {
            if (activitygame.mostCurrent != null && activitygame.mostCurrent == this.activity.get()) {
                activitygame.processBA.setActivityPaused(false);
                Common.Log("** Activity (activitygame) Resume **");
                activitygame.processBA.raiseEvent(activitygame.mostCurrent._activity, "activity_resume", null);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        processBA.onActivityResult(i, i2, intent);
    }

    private static void initializeGlobals() {
        processBA.raiseEvent2(null, true, "globals", false, null);
    }

    public static String _activity_create(boolean z) throws Exception {
        if (z) {
            _tmr.Initialize(processBA, "tmr", 30);
        }
        _bv = true;
        _bs = true;
        try {
            File file = Common.File;
            File file2 = Common.File;
            _bs = BA.ObjectToBoolean(File.ReadString(File.getDirInternal(), "balltappbs.dat"));
            File file3 = Common.File;
            File file4 = Common.File;
            _bv = BA.ObjectToBoolean(File.ReadString(File.getDirInternal(), "balltappbv.dat"));
        } catch (Exception e) {
            processBA.setLastException(e);
        }
        CanvasWrapper.BitmapWrapper bitmapWrapper = mostCurrent._bmp;
        File file5 = Common.File;
        bitmapWrapper.Initialize(File.getDirAssets(), "Tennis.png");
        CanvasWrapper.BitmapWrapper bitmapWrapper2 = mostCurrent._bmp2;
        File file6 = Common.File;
        bitmapWrapper2.Initialize(File.getDirAssets(), "Tennis.png");
        CanvasWrapper.BitmapWrapper bitmapWrapper3 = mostCurrent._bmp3;
        File file7 = Common.File;
        bitmapWrapper3.Initialize(File.getDirAssets(), "Tennis.png");
        mostCurrent._pnl.Initialize(mostCurrent.activityBA, "pnl");
        mostCurrent._activity.AddView((View) mostCurrent._pnl.getObject(), Common.PerXToCurrent(50.0f, mostCurrent.activityBA), 0, Common.PerXToCurrent(25.0f, mostCurrent.activityBA), Common.PerXToCurrent(25.0f, mostCurrent.activityBA));
        _score = 0;
        mostCurrent._lblscore.Initialize(mostCurrent.activityBA, "");
        LabelWrapper labelWrapper = mostCurrent._lblscore;
        Colors colors = Common.Colors;
        labelWrapper.setTextColor(-1);
        LabelWrapper labelWrapper2 = mostCurrent._lblscore;
        TypefaceWrapper typefaceWrapper = Common.Typeface;
        labelWrapper2.setTypeface(TypefaceWrapper.DEFAULT_BOLD);
        mostCurrent._activity.AddView((View) mostCurrent._lblscore.getObject(), 0, 0, Common.PerXToCurrent(100.0f, mostCurrent.activityBA), 30);
        mostCurrent._lblhiscore.Initialize(mostCurrent.activityBA, "");
        LabelWrapper labelWrapper3 = mostCurrent._lblhiscore;
        Colors colors2 = Common.Colors;
        labelWrapper3.setTextColor(-1);
        LabelWrapper labelWrapper4 = mostCurrent._lblhiscore;
        TypefaceWrapper typefaceWrapper2 = Common.Typeface;
        labelWrapper4.setTypeface(TypefaceWrapper.DEFAULT_BOLD);
        mostCurrent._activity.AddView((View) mostCurrent._lblhiscore.getObject(), 0, mostCurrent._lblscore.getHeight(), Common.PerXToCurrent(100.0f, mostCurrent.activityBA), 30);
        mostCurrent._lblscore.setText("Score: 0");
        _loadscore();
        mostCurrent._pnl.setTag("1");
        mostCurrent._pnlground.Initialize(mostCurrent.activityBA, "");
        mostCurrent._pnlbg.Initialize(mostCurrent.activityBA, "");
        mostCurrent._activity.AddView((View) mostCurrent._pnlground.getObject(), 0, Common.PerYToCurrent(100.0f, mostCurrent.activityBA) - Common.DipToCurrent(55), Common.PerXToCurrent(100.0f, mostCurrent.activityBA), Common.DipToCurrent(55));
        mostCurrent._activity.AddView((View) mostCurrent._pnlbg.getObject(), 0, 0, Common.PerXToCurrent(100.0f, mostCurrent.activityBA), Common.PerYToCurrent(105.0f, mostCurrent.activityBA));
        ImageViewWrapper imageViewWrapper = mostCurrent._pnlbg;
        File file8 = Common.File;
        imageViewWrapper.setBitmap((Bitmap) Common.LoadBitmap(File.getDirAssets(), "court.png").getObject());
        ImageViewWrapper imageViewWrapper2 = mostCurrent._pnlbg;
        Gravity gravity = Common.Gravity;
        imageViewWrapper2.setGravity(80);
        mostCurrent._pnlbg.SendToBack();
        GradientDrawable gradientDrawable = new GradientDrawable();
        Colors colors3 = Common.Colors;
        Colors colors4 = Common.Colors;
        gradientDrawable.Initialize((GradientDrawable.Orientation) BA.getEnumFromString(GradientDrawable.Orientation.class, "TOP_BOTTOM"), new int[]{-16711936, -16777216});
        PanelWrapper panelWrapper = mostCurrent._pnlground;
        Colors colors5 = Common.Colors;
        panelWrapper.setColor(Colors.Green);
        mostCurrent._pnlground.setVisible(false);
        ActivityWrapper activityWrapper = mostCurrent._activity;
        Colors colors6 = Common.Colors;
        activityWrapper.setColor(Colors.RGB(100, 149, 237));
        _loadads();
        mostCurrent._rect1.Initialize(Common.DipToCurrent(2), Common.DipToCurrent(2), mostCurrent._pnl.getWidth() - Common.DipToCurrent(4), mostCurrent._pnl.getHeight() - Common.DipToCurrent(4));
        _deg1 = 0;
        _deg2 = 0;
        _deg3 = 0;
        mostCurrent._cnv1.Initialize((View) mostCurrent._pnl.getObject());
        mostCurrent._cnv1.DrawBitmapRotated((Bitmap) mostCurrent._bmp.getObject(), (Rect) Common.Null, (Rect) mostCurrent._rect1.getObject(), (float) _deg1);
        mostCurrent._pnl.Invalidate();
        _tmr.setEnabled(true);
        mostCurrent._sounds.Initialize(3);
        SoundPoolWrapper soundPoolWrapper = mostCurrent._sounds;
        File file9 = Common.File;
        _soundid = soundPoolWrapper.Load(File.getDirAssets(), "thud.mp3");
        return "";
    }

    public static String _activity_pause(boolean z) throws Exception {
        mostCurrent._activity.Finish();
        return "";
    }

    public static String _activity_resume() throws Exception {
        mostCurrent._ad.LoadAd();
        return "";
    }

    public static String _checkscore() throws Exception {
        if (_score > _hiscore) {
            File file = Common.File;
            File file2 = Common.File;
            File.WriteString(File.getDirInternal(), "ballflipscore.dat", "" + BA.NumberToString(_score));
            Common.ToastMessageShow("CONGRATS! - New highscore registered!", false);
        }
        _loadscore();
        if (_score > 0) {
            Common.ToastMessageShow("Score achieved: " + BA.NumberToString(_score), false);
        }
        return "";
    }

    public static String _createball2() throws Exception {
        mostCurrent._pnl2.Initialize(mostCurrent.activityBA, "pnl");
        mostCurrent._activity.AddView((View) mostCurrent._pnl2.getObject(), Common.Rnd(Common.PerXToCurrent(10.0f, mostCurrent.activityBA), Common.PerXToCurrent(70.0f, mostCurrent.activityBA)), 0, mostCurrent._pnl.getWidth(), mostCurrent._pnl.getHeight());
        mostCurrent._pnl2.setTag("2");
        mostCurrent._pnl2.setVisible(true);
        mostCurrent._cnv2.Initialize((View) mostCurrent._pnl2.getObject());
        mostCurrent._cnv2.DrawBitmapRotated((Bitmap) mostCurrent._bmp.getObject(), (Rect) Common.Null, (Rect) mostCurrent._rect1.getObject(), (float) _deg2);
        mostCurrent._pnl2.Invalidate();
        return "";
    }

    public static String _createball3() throws Exception {
        mostCurrent._pnl3.Initialize(mostCurrent.activityBA, "pnl");
        mostCurrent._activity.AddView((View) mostCurrent._pnl3.getObject(), Common.Rnd(Common.PerXToCurrent(10.0f, mostCurrent.activityBA), Common.PerXToCurrent(70.0f, mostCurrent.activityBA)), 0, mostCurrent._pnl.getWidth(), mostCurrent._pnl.getHeight());
        mostCurrent._pnl3.setTag("3");
        mostCurrent._pnl3.setVisible(true);
        mostCurrent._cnv3.Initialize((View) mostCurrent._pnl3.getObject());
        mostCurrent._cnv3.DrawBitmapRotated((Bitmap) mostCurrent._bmp.getObject(), (Rect) Common.Null, (Rect) mostCurrent._rect1.getObject(), (float) _deg3);
        mostCurrent._pnl3.Invalidate();
        return "";
    }

    public static int _getdist(int i, int i2, int i3, int i4) throws Exception {
        return (int) Common.Sqrt(Common.Power((double) (i - i3), 2.0d) + Common.Power((double) (i2 - i4), 2.0d));
    }

    public static void initializeProcessGlobals() {
    }

    public static String _globals() throws Exception {
        mostCurrent._bmp = new CanvasWrapper.BitmapWrapper();
        mostCurrent._bmp2 = new CanvasWrapper.BitmapWrapper();
        mostCurrent._bmp3 = new CanvasWrapper.BitmapWrapper();
        mostCurrent._pnl = new PanelWrapper();
        _vely = 0.0d;
        _velx = 0.0d;
        _vely2 = 0.0d;
        _velx2 = 0.0d;
        _vely3 = 0.0d;
        _velx3 = 0.0d;
        _vely4 = 0.0d;
        _velx4 = 0.0d;
        _vely5 = 0.0d;
        _velx5 = 0.0d;
        _score = 0;
        _hiscore = 0;
        mostCurrent._lblscore = new LabelWrapper();
        mostCurrent._lblhiscore = new LabelWrapper();
        mostCurrent._pnlground = new PanelWrapper();
        mostCurrent._pnlbg = new ImageViewWrapper();
        mostCurrent._pnl2 = new PanelWrapper();
        mostCurrent._pnl3 = new PanelWrapper();
        mostCurrent._pnl4 = new PanelWrapper();
        mostCurrent._pnl5 = new PanelWrapper();
        mostCurrent._ad = new AdViewWrapper();
        mostCurrent._cnv1 = new CanvasWrapper();
        mostCurrent._rect1 = new CanvasWrapper.RectWrapper();
        _deg1 = 0;
        mostCurrent._cnv2 = new CanvasWrapper();
        _deg2 = 0;
        mostCurrent._cnv3 = new CanvasWrapper();
        _deg3 = 0;
        mostCurrent._px = new Phone.PhoneVibrate();
        _bv = false;
        _bs = false;
        mostCurrent._sounds = new SoundPoolWrapper();
        _soundid = 0;
        return "";
    }

    public static String _loadads() throws Exception {
        mostCurrent._ad.Initialize(mostCurrent.activityBA, "ad", "a14dff44d2197a1");
        mostCurrent._activity.AddView((View) mostCurrent._ad.getObject(), Common.DipToCurrent(0), mostCurrent._activity.getHeight() - Common.DipToCurrent(50), Common.DipToCurrent(320), Common.DipToCurrent(50));
        mostCurrent._ad.LoadAd();
        return "";
    }

    public static String _loadscore() throws Exception {
        _hiscore = (int) Double.parseDouble("0");
        try {
            File file = Common.File;
            File file2 = Common.File;
            _hiscore = (int) Double.parseDouble(File.ReadString(File.getDirInternal(), "ballflipscore.dat"));
        } catch (Exception e) {
            processBA.setLastException(e);
        }
        mostCurrent._lblhiscore.setText("Hi Score: " + BA.NumberToString(_hiscore));
        return "";
    }

    public static String _movepanel2() throws Exception {
        mostCurrent._pnl2.setTop((int) (((double) mostCurrent._pnl2.getTop()) + _vely2));
        mostCurrent._pnl2.setLeft((int) (((double) mostCurrent._pnl2.getLeft()) + _velx2));
        _deg2 = (int) (((double) _deg2) + (_velx2 * 2.0d));
        mostCurrent._cnv2.DrawBitmapRotated((Bitmap) mostCurrent._bmp.getObject(), (Rect) Common.Null, (Rect) mostCurrent._rect1.getObject(), (float) _deg2);
        mostCurrent._pnl2.Invalidate();
        if (mostCurrent._pnl2.getTop() + mostCurrent._pnl2.getHeight() > Common.PerYToCurrent(100.0f, mostCurrent.activityBA) - Common.DipToCurrent(55)) {
            if (_score > 0 && _bv) {
                Phone.PhoneVibrate phoneVibrate = mostCurrent._px;
                Phone.PhoneVibrate.Vibrate(processBA, 90);
            }
            _vely2 *= -1.0d;
            _checkscore();
            _score = 0;
            _tmr.setInterval(30);
            mostCurrent._lblscore.setText("Score: " + BA.NumberToString(_score));
            mostCurrent._pnl2.setVisible(false);
            if (mostCurrent._pnl3.IsInitialized()) {
                mostCurrent._pnl3.setVisible(false);
            }
        }
        if (mostCurrent._pnl2.getLeft() < 0) {
            mostCurrent._pnl2.setLeft(0);
            _velx2 *= -1.0d;
        }
        if (mostCurrent._pnl2.getLeft() + mostCurrent._pnl2.getWidth() > Common.PerXToCurrent(100.0f, mostCurrent.activityBA)) {
            mostCurrent._pnl2.setLeft(Common.PerXToCurrent(100.0f, mostCurrent.activityBA) - mostCurrent._pnl2.getWidth());
            _velx2 *= -1.0d;
        }
        _vely2 += 0.5d;
        return "";
    }

    public static String _movepanel3() throws Exception {
        mostCurrent._pnl3.setTop((int) (((double) mostCurrent._pnl3.getTop()) + _vely3));
        mostCurrent._pnl3.setLeft((int) (((double) mostCurrent._pnl3.getLeft()) + _velx3));
        _deg3 = (int) (((double) _deg3) + (_velx3 * 2.0d));
        mostCurrent._cnv3.DrawBitmapRotated((Bitmap) mostCurrent._bmp.getObject(), (Rect) Common.Null, (Rect) mostCurrent._rect1.getObject(), (float) _deg3);
        mostCurrent._pnl3.Invalidate();
        if (mostCurrent._pnl3.getTop() + mostCurrent._pnl3.getHeight() > Common.PerYToCurrent(100.0f, mostCurrent.activityBA) - Common.DipToCurrent(55)) {
            if (_score > 0 && _bv) {
                Phone.PhoneVibrate phoneVibrate = mostCurrent._px;
                Phone.PhoneVibrate.Vibrate(processBA, 90);
            }
            _vely3 *= -1.0d;
            _checkscore();
            _score = 0;
            _tmr.setInterval(30);
            mostCurrent._lblscore.setText("Score: " + BA.NumberToString(_score));
            mostCurrent._pnl3.setVisible(false);
            mostCurrent._pnl2.setVisible(false);
        }
        if (mostCurrent._pnl3.getLeft() < 0) {
            mostCurrent._pnl3.setLeft(0);
            _velx3 *= -1.0d;
        }
        if (mostCurrent._pnl3.getLeft() + mostCurrent._pnl3.getWidth() > Common.PerXToCurrent(100.0f, mostCurrent.activityBA)) {
            mostCurrent._pnl3.setLeft(Common.PerXToCurrent(100.0f, mostCurrent.activityBA) - mostCurrent._pnl3.getWidth());
            _velx3 *= -1.0d;
        }
        _vely3 += 0.5d;
        return "";
    }

    public static String _playtap() throws Exception {
        mostCurrent._sounds.Play(_soundid, 1.0f, 1.0f, 0, 0, 1.0f);
        return "";
    }

    public static String _pnl_touch(int i, float f, float f2) throws Exception {
        ActivityWrapper activityWrapper = mostCurrent._activity;
        if (i != 0) {
            return "";
        }
        PanelWrapper panelWrapper = new PanelWrapper();
        panelWrapper.setObject((ViewGroup) Common.Sender(mostCurrent.activityBA));
        if (panelWrapper.getTag().equals("1") && _vely > 0.0d) {
            _vely *= -1.0d;
            _vely += 0.5d;
            _velx = (double) Common.Rnd(-5, 6);
            _score++;
            mostCurrent._lblscore.setText("Score: " + BA.NumberToString(_score));
            if (_bs) {
                _playtap();
            }
        }
        if (panelWrapper.getTag().equals("2") && _vely2 > 0.0d) {
            _vely2 *= -1.0d;
            _vely2 += 0.5d;
            _velx2 = (double) Common.Rnd(-5, 6);
            _score++;
            mostCurrent._lblscore.setText("Score: " + BA.NumberToString(_score));
            if (_bs) {
                _playtap();
            }
        }
        if (panelWrapper.getTag().equals("3") && _vely3 > 0.0d) {
            _vely3 *= -1.0d;
            _vely3 += 0.5d;
            _velx3 = (double) Common.Rnd(-5, 6);
            _score++;
            mostCurrent._lblscore.setText("Score: " + BA.NumberToString(_score));
            if (_bs) {
                _playtap();
            }
        }
        if (!_bv) {
            return "";
        }
        Phone.PhoneVibrate phoneVibrate = mostCurrent._px;
        Phone.PhoneVibrate.Vibrate(processBA, 50);
        return "";
    }

    public static String _process_globals() throws Exception {
        _tmr = new Timer();
        return "";
    }

    public static String _tmr_tick() throws Exception {
        if (_score < 20) {
            _tmr.setInterval(30);
        }
        if (_score > 20) {
            _tmr.setInterval(28);
        }
        if (_score > 60) {
            _tmr.setInterval(25);
        }
        if (_score > 80) {
            _tmr.setInterval(20);
        }
        if (_score > 100) {
            _tmr.setInterval(18);
        }
        mostCurrent._pnl.setTop((int) (((double) mostCurrent._pnl.getTop()) + _vely));
        mostCurrent._pnl.setLeft((int) (((double) mostCurrent._pnl.getLeft()) + _velx));
        _deg1 = (int) (((double) _deg1) + (_velx * 2.0d));
        mostCurrent._cnv1.DrawBitmapRotated((Bitmap) mostCurrent._bmp.getObject(), (Rect) Common.Null, (Rect) mostCurrent._rect1.getObject(), (float) _deg1);
        if (mostCurrent._pnl.getTop() + mostCurrent._pnl.getHeight() > Common.PerYToCurrent(100.0f, mostCurrent.activityBA) - Common.DipToCurrent(55)) {
            if (_score > 0 && _bv) {
                Phone.PhoneVibrate phoneVibrate = mostCurrent._px;
                Phone.PhoneVibrate.Vibrate(processBA, 90);
            }
            _vely *= -1.0d;
            _checkscore();
            _score = 0;
            mostCurrent._lblscore.setText("Score: " + BA.NumberToString(_score));
            _tmr.setInterval(30);
            if (mostCurrent._pnl2.IsInitialized()) {
                mostCurrent._pnl2.setVisible(false);
            }
            if (mostCurrent._pnl3.IsInitialized()) {
                mostCurrent._pnl3.setVisible(false);
            }
        }
        if (mostCurrent._pnl.getLeft() < 0) {
            mostCurrent._pnl.setLeft(0);
            _velx *= -1.0d;
        }
        if (mostCurrent._pnl.getLeft() + mostCurrent._pnl.getWidth() > Common.PerXToCurrent(100.0f, mostCurrent.activityBA)) {
            mostCurrent._pnl.setLeft(Common.PerXToCurrent(100.0f, mostCurrent.activityBA) - mostCurrent._pnl.getWidth());
            _velx *= -1.0d;
        }
        _vely += 0.5d;
        if (_score > 3) {
            _movepanel2();
        }
        if (_score == 3) {
            if (Common.Not(mostCurrent._pnl2.IsInitialized())) {
                _createball2();
                _movepanel2();
            } else {
                mostCurrent._pnl2.setVisible(true);
                mostCurrent._pnl2.setTop(0);
                _velx2 = 0.0d;
                _vely2 = 0.5d;
                _movepanel2();
            }
        }
        if (_score > 15) {
            _movepanel3();
        }
        if (_score != 15) {
            return "";
        }
        if (Common.Not(mostCurrent._pnl3.IsInitialized())) {
            _createball3();
            _movepanel3();
            return "";
        }
        mostCurrent._pnl3.setVisible(true);
        mostCurrent._pnl3.setTop(0);
        _velx3 = 0.0d;
        _vely3 = 0.5d;
        _movepanel3();
        return "";
    }
}
