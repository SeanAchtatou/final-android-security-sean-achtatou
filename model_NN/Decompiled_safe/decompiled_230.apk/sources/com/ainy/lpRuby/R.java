package com.ainy.lpRuby;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int img1 = 2130837505;
        public static final int img10 = 2130837506;
        public static final int img11 = 2130837507;
        public static final int img12 = 2130837508;
        public static final int img13 = 2130837509;
        public static final int img14 = 2130837510;
        public static final int img15 = 2130837511;
        public static final int img16 = 2130837512;
        public static final int img17 = 2130837513;
        public static final int img18 = 2130837514;
        public static final int img19 = 2130837515;
        public static final int img2 = 2130837516;
        public static final int img20 = 2130837517;
        public static final int img21 = 2130837518;
        public static final int img22 = 2130837519;
        public static final int img23 = 2130837520;
        public static final int img24 = 2130837521;
        public static final int img25 = 2130837522;
        public static final int img26 = 2130837523;
        public static final int img27 = 2130837524;
        public static final int img28 = 2130837525;
        public static final int img29 = 2130837526;
        public static final int img3 = 2130837527;
        public static final int img30 = 2130837528;
        public static final int img31 = 2130837529;
        public static final int img32 = 2130837530;
        public static final int img33 = 2130837531;
        public static final int img34 = 2130837532;
        public static final int img35 = 2130837533;
        public static final int img36 = 2130837534;
        public static final int img37 = 2130837535;
        public static final int img38 = 2130837536;
        public static final int img39 = 2130837537;
        public static final int img4 = 2130837538;
        public static final int img40 = 2130837539;
        public static final int img41 = 2130837540;
        public static final int img42 = 2130837541;
        public static final int img43 = 2130837542;
        public static final int img44 = 2130837543;
        public static final int img45 = 2130837544;
        public static final int img46 = 2130837545;
        public static final int img47 = 2130837546;
        public static final int img48 = 2130837547;
        public static final int img49 = 2130837548;
        public static final int img5 = 2130837549;
        public static final int img50 = 2130837550;
        public static final int img51 = 2130837551;
        public static final int img52 = 2130837552;
        public static final int img53 = 2130837553;
        public static final int img54 = 2130837554;
        public static final int img55 = 2130837555;
        public static final int img56 = 2130837556;
        public static final int img57 = 2130837557;
        public static final int img58 = 2130837558;
        public static final int img59 = 2130837559;
        public static final int img6 = 2130837560;
        public static final int img60 = 2130837561;
        public static final int img7 = 2130837562;
        public static final int img8 = 2130837563;
        public static final int img9 = 2130837564;
        public static final int l = 2130837565;
        public static final int r = 2130837566;
        public static final int save = 2130837567;
        public static final int setting = 2130837568;
    }

    public static final class id {
        public static final int AbsoluteLayout01 = 2131034113;
        public static final int AdmainLayout = 2131034112;
        public static final int ImageButtonNext = 2131034117;
        public static final int ImageButtonPri = 2131034114;
        public static final int ImageButtonSave = 2131034116;
        public static final int ImageButtonWallpapers = 2131034115;
        public static final int ImageView01 = 2131034118;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
    }
}
