package com.menueph.entertainment.ucointoss;

public final class R {

    public static final class anim {
        public static final int cointoss_anim = 2130968576;
        public static final int slide_in_left = 2130968577;
        public static final int slide_in_right = 2130968578;
        public static final int slide_out_left = 2130968579;
        public static final int slide_out_right = 2130968580;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int ctbtn_toss = 2130837504;
        public static final int ctbtn_toss_dr = 2130837505;
        public static final int ctbtn_toss_hit = 2130837506;
        public static final int ctic_launcher = 2130837507;
        public static final int ctimg_chinacoin_anim = 2130837508;
        public static final int ctimg_chinacoin_half = 2130837509;
        public static final int ctimg_chinacoinheads = 2130837510;
        public static final int ctimg_chinacoinheads_turnleft = 2130837511;
        public static final int ctimg_chinacoinheads_turnright = 2130837512;
        public static final int ctimg_chinacointails = 2130837513;
        public static final int ctimg_chinacointails_turnleft = 2130837514;
        public static final int ctimg_chinacointails_turnright = 2130837515;
        public static final int ctimg_germancoin_anim = 2130837516;
        public static final int ctimg_germancoin_half = 2130837517;
        public static final int ctimg_germancoinheads = 2130837518;
        public static final int ctimg_germancoinheads_turnleft = 2130837519;
        public static final int ctimg_germancoinheads_turnright = 2130837520;
        public static final int ctimg_germancointails = 2130837521;
        public static final int ctimg_germancointails_turnleft = 2130837522;
        public static final int ctimg_germancointails_turnright = 2130837523;
        public static final int ctimg_japcoin_anim = 2130837524;
        public static final int ctimg_japcoin_half = 2130837525;
        public static final int ctimg_japcoinheads = 2130837526;
        public static final int ctimg_japcoinheads_turnleft = 2130837527;
        public static final int ctimg_japcoinheads_turnright = 2130837528;
        public static final int ctimg_japcointails = 2130837529;
        public static final int ctimg_japcointails_turnleft = 2130837530;
        public static final int ctimg_japcointails_turnright = 2130837531;
        public static final int ctimg_phcoin_anim = 2130837532;
        public static final int ctimg_phcoin_half = 2130837533;
        public static final int ctimg_phcoinheads = 2130837534;
        public static final int ctimg_phcoinheads_turnleft = 2130837535;
        public static final int ctimg_phcoinheads_turnright = 2130837536;
        public static final int ctimg_phcointails = 2130837537;
        public static final int ctimg_phcointails_turnleft = 2130837538;
        public static final int ctimg_phcointails_turnright = 2130837539;
        public static final int ctimg_ukcoin_anim = 2130837540;
        public static final int ctimg_ukcoin_half = 2130837541;
        public static final int ctimg_ukcoinheads = 2130837542;
        public static final int ctimg_ukcoinheads_turnleft = 2130837543;
        public static final int ctimg_ukcoinheads_turnright = 2130837544;
        public static final int ctimg_ukcointails = 2130837545;
        public static final int ctimg_ukcointails_turnleft = 2130837546;
        public static final int ctimg_ukcointails_turnright = 2130837547;
        public static final int ctimg_uscoin_anim = 2130837548;
        public static final int ctimg_uscoin_half = 2130837549;
        public static final int ctimg_uscoinheads = 2130837550;
        public static final int ctimg_uscoinheads_turnleft = 2130837551;
        public static final int ctimg_uscoinheads_turnright = 2130837552;
        public static final int ctimg_uscointails = 2130837553;
        public static final int ctimg_uscointails_turnleft = 2130837554;
        public static final int ctimg_uscointails_turnright = 2130837555;
        public static final int ctscrbg_concrete1 = 2130837556;
        public static final int ctscrbg_concrete2 = 2130837557;
        public static final int ctscrbg_tile = 2130837558;
        public static final int ctscrbg_wood = 2130837559;
        public static final int ic_menu_help = 2130837560;
        public static final int ic_menu_notifications = 2130837561;
    }

    public static final class id {
        public static final int coin_btn_flip = 2131165185;
        public static final int coin_scroller = 2131165184;
    }

    public static final class layout {
        public static final int cointoss_main = 2130903040;
    }

    public static final class raw {
        public static final int coin_toss = 2131034112;
    }

    public static final class string {
        public static final int app_name = 2131099648;
        public static final int btn_cancel = 2131099650;
        public static final int btn_ok = 2131099649;
        public static final int cointxt_de = 2131099652;
        public static final int cointxt_gb = 2131099655;
        public static final int cointxt_ja = 2131099653;
        public static final int cointxt_ph = 2131099654;
        public static final int cointxt_us = 2131099656;
        public static final int cointxt_zh = 2131099651;
        public static final int fdbk_close = 2131099661;
        public static final int fdbk_msg = 2131099659;
        public static final int fdbk_send = 2131099660;
        public static final int fdbk_title = 2131099658;
        public static final int help_msg = 2131099663;
        public static final int help_msg_title = 2131099662;
        public static final int uct_admob_key = 2131099657;
    }
}
