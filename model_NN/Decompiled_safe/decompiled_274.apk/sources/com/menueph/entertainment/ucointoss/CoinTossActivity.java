package com.menueph.entertainment.ucointoss;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewAnimator;
import com.google.ads.AdRequest;
import com.mobclick.android.MobclickAgent;
import com.mobclick.android.ReportPolicy;

public class CoinTossActivity extends Activity implements SensorEventListener {
    private static final int BG_CONCRETE2 = 3;
    private static final int BG_WOOD = 0;
    private static final int COIN_DE = 1;
    private static final int COIN_GB = 4;
    private static final int COIN_JA = 2;
    private static final int COIN_PH = 3;
    private static final int COIN_US = 5;
    private static final int COIN_ZH = 0;
    private static final float FLIP_SENS_THRESHOLD = 17.0f;
    private static final String TEST_MINI = "S5570f1675d18";
    private static final String TEST_TAB = "383408173AA300EC";
    private static final double UCT_HALFPOINT = 0.5d;
    private static final double UCT_LUCKTHRESH = 1.0E-4d;
    private static final long UCT_STOP_TIMER = 3133;
    private Animation m_Anim;
    private TransitionDrawable m_Backgrounds;
    private Drawable[] m_Beegees;
    /* access modifiers changed from: private */
    public ImageView m_Coin;
    private ViewAnimator m_CoinScroller;
    private View.OnTouchListener m_CoinTouchListener = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case ReportPolicy.REALTIME /*0*/:
                    CoinTossActivity.this.m_StartX = event.getX();
                    CoinTossActivity.this.m_StartY = event.getY();
                    return true;
                case 1:
                    float deltaX = CoinTossActivity.this.m_StartX - event.getX();
                    float deltaY = CoinTossActivity.this.m_StartY - event.getY();
                    if (deltaX < (-CoinTossActivity.this.m_MinDragDistX)) {
                        CoinTossActivity.this.showPrevCoin();
                    } else if (deltaX > CoinTossActivity.this.m_MinDragDistX) {
                        CoinTossActivity.this.showNextCoin();
                    }
                    if (deltaY < (-CoinTossActivity.this.m_MinDragDistY)) {
                        CoinTossActivity.this.showNextBackground();
                    } else if (deltaY > CoinTossActivity.this.m_MinDragDistX) {
                        CoinTossActivity.this.showPrevBackground();
                    }
                    if (deltaY <= (-CoinTossActivity.this.m_MinDragDistY) || deltaY >= CoinTossActivity.this.m_MinDragDistY || deltaX <= (-CoinTossActivity.this.m_MinDragDistX) || deltaX >= CoinTossActivity.this.m_MinDragDistX) {
                        return true;
                    }
                    CoinTossActivity.this.launchCoinFlip(v);
                    return true;
                default:
                    return true;
            }
        }
    };
    private int m_CurrentBg = 0;
    private int m_CurrentCoin = 0;
    private float m_DisplayHeight;
    private float m_DisplayWidth;
    /* access modifiers changed from: private */
    public AnimationDrawable m_FrameAnim;
    /* access modifiers changed from: private */
    public float m_MinDragDistX;
    /* access modifiers changed from: private */
    public float m_MinDragDistY;
    private View m_ParentView;
    private SensorManager m_SensorManager;
    private Animation m_SlideInLeft;
    private Animation m_SlideInRight;
    private Animation m_SlideOutLeft;
    private Animation m_SlideOutRight;
    private MediaPlayer m_Sound;
    private Runnable m_SpinTimerTask = new Runnable() {
        public void run() {
            CoinTossActivity.this.m_Coin.clearAnimation();
            CoinTossActivity.this.m_FrameAnim.stop();
            CoinTossActivity.this.determineOutcome(false);
        }
    };
    /* access modifiers changed from: private */
    public float m_StartX;
    /* access modifiers changed from: private */
    public float m_StartY;
    private Handler m_StopHandler;
    private int m_TossFlag = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.cointoss_main);
        this.m_Anim = AnimationUtils.loadAnimation(this, R.anim.cointoss_anim);
        this.m_ParentView = findViewById(16908290);
        this.m_CoinScroller = (ViewAnimator) findViewById(R.id.coin_scroller);
        loadBackgrounds();
        loadAnimations();
        refreshCoin();
        this.m_CoinScroller.setOnTouchListener(this.m_CoinTouchListener);
        this.m_SensorManager = (SensorManager) getSystemService("sensor");
        this.m_SensorManager.registerListener(this, this.m_SensorManager.getDefaultSensor(1), 2);
        AdRequest request = new AdRequest();
        request.addTestDevice(AdRequest.TEST_EMULATOR);
        request.addTestDevice(TEST_MINI);
        request.addTestDevice(TEST_TAB);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        this.m_StopHandler = new Handler();
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        this.m_DisplayWidth = (float) metrics.widthPixels;
        this.m_DisplayHeight = (float) metrics.heightPixels;
        this.m_MinDragDistX = this.m_DisplayWidth / 6.0f;
        this.m_MinDragDistY = this.m_DisplayHeight / 8.0f;
        this.m_SensorManager.registerListener(this, this.m_SensorManager.getDefaultSensor(1), 2);
        this.m_Sound = MediaPlayer.create(this, (int) R.raw.coin_toss);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.m_Sound.release();
        this.m_Sound = null;
        this.m_SensorManager.unregisterListener(this);
        super.onPause();
        MobclickAgent.onPause(this);
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() != 1) {
            return;
        }
        if (event.values[2] > FLIP_SENS_THRESHOLD) {
            this.m_TossFlag++;
        } else if (event.values[2] < FLIP_SENS_THRESHOLD && this.m_TossFlag > 0) {
            if (this.m_TossFlag < 2) {
                this.m_TossFlag++;
                return;
            }
            this.m_TossFlag = 0;
            launchCoinFlip(this.m_ParentView);
        }
    }

    public void launchCoinFlip(View target) {
        if (this.m_FrameAnim.isRunning()) {
            stopCoin();
            return;
        }
        this.m_StopHandler.removeCallbacks(this.m_SpinTimerTask);
        this.m_StopHandler.postDelayed(this.m_SpinTimerTask, UCT_STOP_TIMER);
        this.m_Anim.reset();
        this.m_Coin.startAnimation(this.m_Anim);
        this.m_FrameAnim.start();
        this.m_Sound.start();
    }

    public void launchHelpMsg(View target) {
        new AlertDialog.Builder(this).setTitle((int) R.string.help_msg_title).setMessage((int) R.string.help_msg).setPositiveButton((int) R.string.btn_ok, (DialogInterface.OnClickListener) null).show();
    }

    public void launchFeedbackMsg(View target) {
        new AlertDialog.Builder(this).setTitle((int) R.string.fdbk_title).setMessage((int) R.string.fdbk_msg).setPositiveButton((int) R.string.fdbk_send, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                CoinTossActivity.this.startActivity(new Intent("android.intent.action.SENDTO", Uri.fromParts("mailto", "coin@mymenue.com", null)));
            }
        }).setNegativeButton((int) R.string.fdbk_close, (DialogInterface.OnClickListener) null).show();
    }

    private void loadAnimations() {
        this.m_SlideInLeft = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
        this.m_SlideInRight = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
        this.m_SlideOutLeft = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);
        this.m_SlideOutRight = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);
    }

    private void loadBackgrounds() {
        this.m_Beegees = new Drawable[]{getResources().getDrawable(R.drawable.ctscrbg_wood), getResources().getDrawable(R.drawable.ctscrbg_tile), getResources().getDrawable(R.drawable.ctscrbg_concrete1), getResources().getDrawable(R.drawable.ctscrbg_concrete2)};
        this.m_Backgrounds = new TransitionDrawable(this.m_Beegees);
        this.m_ParentView.setBackgroundDrawable(this.m_Backgrounds);
    }

    /* access modifiers changed from: private */
    public void determineOutcome(boolean wasTapped) {
        double randomNum = Math.random();
        if (randomNum >= UCT_HALFPOINT) {
            this.m_FrameAnim.selectDrawable(0);
        } else if (randomNum > UCT_LUCKTHRESH || wasTapped) {
            this.m_FrameAnim.selectDrawable(4);
        } else {
            this.m_FrameAnim.selectDrawable(2);
        }
    }

    /* access modifiers changed from: private */
    public void showPrevCoin() {
        stopCoin();
        this.m_CoinScroller.setInAnimation(this.m_SlideInLeft);
        this.m_CoinScroller.setOutAnimation(this.m_SlideOutRight);
        this.m_CoinScroller.showPrevious();
        if (this.m_CurrentCoin == 0) {
            this.m_CurrentCoin = 5;
        } else {
            this.m_CurrentCoin--;
        }
        refreshCoin();
    }

    /* access modifiers changed from: private */
    public void showNextCoin() {
        stopCoin();
        this.m_CoinScroller.setInAnimation(this.m_SlideInRight);
        this.m_CoinScroller.setOutAnimation(this.m_SlideOutLeft);
        this.m_CoinScroller.showNext();
        if (this.m_CurrentCoin == 5) {
            this.m_CurrentCoin = 0;
        } else {
            this.m_CurrentCoin++;
        }
        refreshCoin();
    }

    private void stopCoin() {
        this.m_StopHandler.removeCallbacks(this.m_SpinTimerTask);
        this.m_Coin.clearAnimation();
        this.m_FrameAnim.stop();
        this.m_Sound.stop();
        this.m_Sound.reset();
        try {
            this.m_Sound.prepare();
        } catch (Exception e) {
            this.m_Sound.release();
            this.m_Sound = null;
            this.m_Sound = MediaPlayer.create(this, (int) R.raw.coin_toss);
        }
        determineOutcome(true);
    }

    private void refreshCoin() {
        this.m_Coin = (ImageView) this.m_CoinScroller.getCurrentView();
        this.m_FrameAnim = (AnimationDrawable) this.m_Coin.getDrawable();
        int coinTextRes = 0;
        switch (this.m_CurrentCoin) {
            case ReportPolicy.REALTIME /*0*/:
                coinTextRes = R.string.cointxt_zh;
                break;
            case 1:
                coinTextRes = R.string.cointxt_de;
                break;
            case 2:
                coinTextRes = R.string.cointxt_ja;
                break;
            case ReportPolicy.PUSH /*3*/:
                coinTextRes = R.string.cointxt_ph;
                break;
            case 4:
                coinTextRes = R.string.cointxt_gb;
                break;
            case 5:
                coinTextRes = R.string.cointxt_us;
                break;
        }
        Toast.makeText(this, coinTextRes, 0).show();
    }

    /* access modifiers changed from: private */
    public void showPrevBackground() {
        if (this.m_CurrentBg == 0) {
            this.m_Backgrounds = new TransitionDrawable(new Drawable[]{this.m_Beegees[this.m_CurrentBg], this.m_Beegees[3]});
            this.m_CurrentBg = 3;
        } else {
            this.m_Backgrounds = new TransitionDrawable(new Drawable[]{this.m_Beegees[this.m_CurrentBg], this.m_Beegees[this.m_CurrentBg - 1]});
            this.m_CurrentBg--;
        }
        this.m_ParentView.setBackgroundDrawable(this.m_Backgrounds);
        this.m_Backgrounds.reverseTransition(333);
    }

    /* access modifiers changed from: private */
    public void showNextBackground() {
        if (this.m_CurrentBg == 3) {
            this.m_Backgrounds = new TransitionDrawable(new Drawable[]{this.m_Beegees[this.m_CurrentBg], this.m_Beegees[0]});
            this.m_CurrentBg = 0;
        } else {
            this.m_Backgrounds = new TransitionDrawable(new Drawable[]{this.m_Beegees[this.m_CurrentBg], this.m_Beegees[this.m_CurrentBg + 1]});
            this.m_CurrentBg++;
        }
        this.m_ParentView.setBackgroundDrawable(this.m_Backgrounds);
        this.m_Backgrounds.reverseTransition(333);
    }
}
