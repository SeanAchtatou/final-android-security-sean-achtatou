package mominis.common.repositories;

import java.io.IOException;
import java.util.List;
import mominis.common.mvc.IObservable;
import mominis.common.mvc.ListChangedEventArgs;

public interface IReadableRepository<T> extends IObservable<ListChangedEventArgs> {
    T get(long j) throws IOException;

    List<T> getAll() throws IOException;

    int size() throws IOException;
}
