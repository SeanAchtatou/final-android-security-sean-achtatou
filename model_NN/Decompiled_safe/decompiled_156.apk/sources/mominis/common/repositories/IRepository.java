package mominis.common.repositories;

public interface IRepository<T> extends IReadableRepository<T>, IWriteableRepository<T> {
}
