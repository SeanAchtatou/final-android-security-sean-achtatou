package mominis.common.repositories;

import java.io.IOException;

public interface IWriteableRepository<T> {
    T create(T t) throws IOException;

    void delete(long j) throws IOException;

    void update(T t) throws IOException;
}
