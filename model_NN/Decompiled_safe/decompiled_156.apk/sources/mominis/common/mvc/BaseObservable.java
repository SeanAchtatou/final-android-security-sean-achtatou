package mominis.common.mvc;

import java.util.ArrayList;
import java.util.List;
import mominis.gameconsole.common.EventArgs;

public class BaseObservable<T extends EventArgs> implements IObservable<T> {
    private List<IObserver<T>> mObservable;
    private IObservable<T> m_observable;

    public BaseObservable() {
        this.mObservable = new ArrayList();
        this.m_observable = this;
    }

    public BaseObservable(IObservable<T> observable) {
        this.mObservable = new ArrayList();
        this.m_observable = observable;
    }

    public void registerObserver(IObserver<T> observer) {
        if (!this.mObservable.contains(observer)) {
            this.mObservable.add(observer);
        }
    }

    public void unregisterObserver(IObserver<T> observer) {
        this.mObservable.remove(observer);
    }

    public void notifyObservers(T eventArgs) {
        for (int i = 0; i < this.mObservable.size(); i++) {
            this.mObservable.get(i).onChanged(this.m_observable, eventArgs);
        }
    }
}
