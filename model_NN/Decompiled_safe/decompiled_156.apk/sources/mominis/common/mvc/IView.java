package mominis.common.mvc;

import android.content.DialogInterface;
import java.util.ArrayList;

public interface IView {
    void close();

    void closeWithResult(int i);

    void setStatusText(int i);

    void setStatusText(String str);

    void showCustomMessage(String str, String str2, ArrayList<String> arrayList, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2);

    void showMessage(String str, String str2);

    void showMessage(String str, String str2, DialogInterface.OnClickListener onClickListener);

    void showYesNoMessage(String str, String str2, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2);
}
