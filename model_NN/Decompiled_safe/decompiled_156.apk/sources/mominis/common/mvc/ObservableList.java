package mominis.common.mvc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import mominis.common.mvc.ListChangedEventArgs;

public class ObservableList<T> extends BaseObservable<ListChangedEventArgs> implements List<T> {
    List<T> mList = new ArrayList();

    public ObservableList() {
    }

    public ObservableList(IObservable<ListChangedEventArgs> sender) {
        super(sender);
    }

    /* access modifiers changed from: protected */
    public List<T> getList() {
        return this.mList;
    }

    public boolean add(T object) {
        boolean res = this.mList.add(object);
        if (res) {
            onAdd(this.mList.size() - 1, object);
        }
        return res;
    }

    public void add(int location, T object) {
        this.mList.add(location, object);
        onAdd(location, object);
    }

    public boolean addAll(Collection<? extends T> toAdd) {
        int location = this.mList.size();
        boolean res = this.mList.addAll(toAdd);
        if (res) {
            onAddRange(location, toAdd);
        }
        return res;
    }

    public boolean addAll(int location, Collection<? extends T> toAdd) {
        boolean res = this.mList.addAll(location, toAdd);
        if (res) {
            onAddRange(location, toAdd);
        }
        return res;
    }

    public void clear() {
        this.mList.clear();
        onClear();
    }

    public T remove(int location) {
        T prev = this.mList.remove(location);
        onRemove(location, prev);
        return prev;
    }

    public boolean remove(Object object) {
        int location = this.mList.indexOf(object);
        boolean res = this.mList.remove(object);
        if (res) {
            onRemove(location, object);
        }
        return res;
    }

    public T set(int location, T object) {
        T prev = this.mList.set(location, object);
        onSet(location, object);
        return prev;
    }

    public boolean removeAll(Collection<?> toRemove) {
        return removeOrRetainRange(toRemove, true);
    }

    public boolean retainAll(Collection<?> toRetain) {
        return removeOrRetainRange(toRetain, true);
    }

    private boolean removeOrRetainRange(Collection<?> collection, boolean isRetain) {
        Collection<T> removed = new ArrayList<>();
        for (T item : this.mList) {
            if ((collection.contains(item) ^ isRetain) && this.mList.remove(item)) {
                removed.add(item);
            }
        }
        onRemoveRange(removed);
        return removed.size() > 0;
    }

    public boolean contains(Object object) {
        return this.mList.contains(object);
    }

    public boolean containsAll(Collection<?> collection) {
        return this.mList.containsAll(collection);
    }

    public T get(int location) {
        return this.mList.get(location);
    }

    public int indexOf(Object object) {
        return this.mList.indexOf(object);
    }

    public boolean isEmpty() {
        return this.mList.isEmpty();
    }

    public Iterator<T> iterator() {
        return this.mList.iterator();
    }

    public int lastIndexOf(Object object) {
        return this.mList.lastIndexOf(object);
    }

    public ListIterator<T> listIterator() {
        return this.mList.listIterator();
    }

    public ListIterator<T> listIterator(int location) {
        return this.mList.listIterator(location);
    }

    public int size() {
        return this.mList.size();
    }

    public List<T> subList(int start, int end) {
        return this.mList.subList(start, end);
    }

    public Object[] toArray() {
        return this.mList.toArray();
    }

    public <K> K[] toArray(K[] array) {
        return this.mList.toArray(array);
    }

    public Object clone() {
        ObservableList<T> list = new ObservableList<>();
        for (T add : this.mList) {
            list.add(add);
        }
        return list;
    }

    private void onAdd(int i, T object) {
        notifyObservers(new ListChangedEventArgs(ListChangedEventArgs.Action.Add, i, object));
    }

    private void onClear() {
        notifyObservers(new ListChangedEventArgs(ListChangedEventArgs.Action.Clear, -1, null));
    }

    private void onRemoveRange(Collection<T> removed) {
        notifyObservers(new ListChangedEventArgs(ListChangedEventArgs.Action.RangeRemove, -1, removed));
    }

    private void onSet(int location, T object) {
        notifyObservers(new ListChangedEventArgs(ListChangedEventArgs.Action.Set, location, object));
    }

    private void onRemove(int location, T object) {
        notifyObservers(new ListChangedEventArgs(ListChangedEventArgs.Action.Remove, location, object));
    }

    private void onAddRange(int location, Collection<? extends T> added) {
        notifyObservers(new ListChangedEventArgs(ListChangedEventArgs.Action.RangeAdd, location, added));
    }
}
