package mominis.common.mvc;

import mominis.gameconsole.common.EventArgs;

public interface IObservable<T extends EventArgs> {
    void registerObserver(IObserver<T> iObserver);

    void unregisterObserver(IObserver<T> iObserver);
}
