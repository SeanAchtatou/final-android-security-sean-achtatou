package mominis.common.mvc;

import mominis.gameconsole.common.EventArgs;

public class BaseController implements IObservable<EventArgs> {
    private INavigationManager mNavMgr;
    private BaseObservable<EventArgs> mObservable = new BaseObservable<>();

    public BaseController(INavigationManager navMgr) {
        this.mNavMgr = navMgr;
        this.mObservable = new BaseObservable<>(this);
    }

    /* access modifiers changed from: protected */
    public INavigationManager getNavigation() {
        return this.mNavMgr;
    }

    public void registerObserver(IObserver<EventArgs> observer) {
        this.mObservable.registerObserver(observer);
    }

    public void unregisterObserver(IObserver<EventArgs> observer) {
        this.mObservable.unregisterObserver(observer);
    }

    /* access modifiers changed from: protected */
    public void notifyObservers(EventArgs args) {
        this.mObservable.notifyObservers(args);
    }
}
