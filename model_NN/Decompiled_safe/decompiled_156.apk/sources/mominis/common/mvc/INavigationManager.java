package mominis.common.mvc;

import android.net.Uri;
import android.os.Bundle;

public interface INavigationManager {
    void displayInstallationForm(IView iView, Uri uri, int i);

    Bundle getLaunchData(IView iView);

    void registerView(String str, Class<?> cls);

    void showView(IView iView, int i, String str);

    void showView(IView iView, int i, String str, Bundle bundle);
}
