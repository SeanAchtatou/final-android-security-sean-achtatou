package mominis.common.mvc;

import java.util.Arrays;
import java.util.Collection;
import mominis.gameconsole.common.EventArgs;

public class ListChangedEventArgs extends EventArgs {
    private Action mAction;
    private Collection<?> mAffectedObject;
    private int mCount;
    private int mIndex;

    public enum Action {
        Add,
        Remove,
        Set,
        Clear,
        RangeAdd,
        RangeRemove,
        ItemUpdated
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [java.util.Collection<T>, java.util.Collection<?>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> ListChangedEventArgs(mominis.common.mvc.ListChangedEventArgs.Action r1, int r2, int r3, java.util.Collection<T> r4) {
        /*
            r0 = this;
            r0.<init>()
            r0.mIndex = r2
            r0.mAction = r1
            r0.mCount = r3
            r0.mAffectedObject = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: mominis.common.mvc.ListChangedEventArgs.<init>(mominis.common.mvc.ListChangedEventArgs$Action, int, int, java.util.Collection):void");
    }

    public <T> ListChangedEventArgs(Action action, int index, T affectedObject) {
        this(action, index, 1, Arrays.asList(affectedObject));
    }

    public Action getAction() {
        return this.mAction;
    }

    public int getIndex() {
        return this.mIndex;
    }

    public int getCount() {
        return this.mCount;
    }

    public <T> T getFirstAffected() {
        return this.mAffectedObject.iterator().next();
    }

    public <T> Collection<T> getAffected() {
        return this.mAffectedObject;
    }
}
