package mominis.common.mvc;

import mominis.gameconsole.common.EventArgs;

public interface IObserver<T extends EventArgs> {
    void onChanged(IObservable<T> iObservable, T t);
}
