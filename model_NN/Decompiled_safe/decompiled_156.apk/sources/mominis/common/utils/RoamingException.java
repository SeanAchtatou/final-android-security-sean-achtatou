package mominis.common.utils;

import java.io.IOException;

public class RoamingException extends IOException {
    private static final long serialVersionUID = 1;

    public RoamingException(String message) {
        super(message);
    }
}
