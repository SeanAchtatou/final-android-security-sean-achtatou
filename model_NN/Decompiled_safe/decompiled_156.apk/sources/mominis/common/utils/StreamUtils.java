package mominis.common.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

public class StreamUtils {
    public static byte[] readAllBytes(InputStream is) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int bytesRead = is.read(buffer);
        while (bytesRead > 0) {
            output.write(buffer, 0, bytesRead);
            bytesRead = is.read(buffer);
        }
        return output.toByteArray();
    }

    public static String readToEnd(InputStream is) {
        return readToEnd(is, "utf-8");
    }

    public static String readToEnd(InputStream is, String charset) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName(charset)), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line != null) {
                    sb.append(line + "\n");
                } else {
                    try {
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                try {
                    is.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                throw th;
            }
        }
        is.close();
        return sb.toString();
    }

    public static InputStream createInputStream(String input, String charset) {
        try {
            return new ByteArrayInputStream(input.getBytes(charset));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static InputStream createInputStream(String input) {
        return createInputStream(input, "UTF-8");
    }
}
