package mominis.common.utils;

import android.content.Context;
import java.io.IOException;
import mominis.gameconsole.com.R;
import mominis.gameconsole.common.StringUtils;
import mominis.gameconsole.services.IConnectivityMonitor;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;

public class SafeHttpClient implements IHttpClient {
    private DefaultHttpClient mClient = new TrustAllSLLHttpClient();
    private Context mContext;
    private IConnectivityMonitor mMon;

    public SafeHttpClient(IConnectivityMonitor conMon, Context context) {
        this.mMon = conMon;
        this.mContext = context;
        String configuredUserAgent = this.mContext.getString(R.string.user_agent);
        if (StringUtils.isEmpty(configuredUserAgent)) {
            this.mClient.getParams().setParameter("http.useragent", configuredUserAgent);
        }
    }

    public HttpResponse execute(HttpUriRequest request) throws IOException {
        failIfRoaming();
        return this.mClient.execute(request);
    }

    public HttpResponse execute(HttpUriRequest request, HttpContext context) throws IOException, ClientProtocolException {
        failIfRoaming();
        return this.mClient.execute(request, context);
    }

    public HttpResponse execute(HttpHost target, HttpRequest request) throws IOException, ClientProtocolException {
        failIfRoaming();
        return this.mClient.execute(target, request);
    }

    public <T> T execute(HttpUriRequest arg0, ResponseHandler<? extends T> arg1) throws IOException, ClientProtocolException {
        failIfRoaming();
        return this.mClient.execute(arg0, arg1);
    }

    public HttpResponse execute(HttpHost target, HttpRequest request, HttpContext context) throws IOException, ClientProtocolException {
        failIfRoaming();
        return this.mClient.execute(target, request, context);
    }

    public <T> T execute(HttpUriRequest arg0, ResponseHandler<? extends T> arg1, HttpContext arg2) throws IOException, ClientProtocolException {
        failIfRoaming();
        return this.mClient.execute(arg0, arg1);
    }

    public <T> T execute(HttpHost arg0, HttpRequest arg1, ResponseHandler<? extends T> arg2) throws IOException, ClientProtocolException {
        failIfRoaming();
        return this.mClient.execute(arg0, arg1, arg2);
    }

    public <T> T execute(HttpHost arg0, HttpRequest arg1, ResponseHandler<? extends T> arg2, HttpContext arg3) throws IOException, ClientProtocolException {
        failIfRoaming();
        return this.mClient.execute(arg0, arg1, arg2, arg3);
    }

    public ClientConnectionManager getConnectionManager() {
        return this.mClient.getConnectionManager();
    }

    public HttpParams getParams() {
        return this.mClient.getParams();
    }

    private void failIfRoaming() throws IOException {
        if (this.mMon.isRoaming().booleanValue()) {
            throw new RoamingException("Cannot execute request while in roaming mode");
        }
    }

    public CookieStore getCookieStore() {
        return this.mClient.getCookieStore();
    }

    public void setCookieStore(CookieStore store) {
        this.mClient.setCookieStore(store);
    }
}
