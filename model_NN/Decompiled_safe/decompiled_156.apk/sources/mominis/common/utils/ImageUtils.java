package mominis.common.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

public class ImageUtils {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap decodeAndResizeIfLarger(byte[] data, int width, int height, BitmapFactory.Options options) {
        Bitmap bitmapOrg = BitmapFactory.decodeByteArray(data, 0, data.length, options);
        int orgWidth = bitmapOrg.getWidth();
        int orgHeight = bitmapOrg.getHeight();
        if (orgWidth <= width && orgHeight <= height) {
            return bitmapOrg;
        }
        Matrix matrix = new Matrix();
        matrix.postScale(((float) width) / ((float) orgWidth), ((float) height) / ((float) orgHeight));
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmapOrg, 0, 0, orgWidth, orgHeight, matrix, true);
        bitmapOrg.recycle();
        return resizedBitmap;
    }
}
