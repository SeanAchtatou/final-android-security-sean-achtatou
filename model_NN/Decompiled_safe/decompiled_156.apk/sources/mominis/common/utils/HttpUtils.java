package mominis.common.utils;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Hashtable;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

public class HttpUtils {
    public static HttpResponse executeHttpGet(HttpClient httpClient, String url, String host, Hashtable<String, Object> params, Hashtable<String, String> cookies) throws IOException, ClientProtocolException {
        String finalUrl;
        String urlParams = "";
        Enumeration<String> e = params.keys();
        while (e.hasMoreElements()) {
            String key = e.nextElement();
            urlParams = urlParams + key + "=" + URLEncoder.encode(params.get(key).toString(), "UTF-8");
            if (e.hasMoreElements()) {
                urlParams = urlParams + "&";
            }
        }
        if (urlParams != "") {
            finalUrl = url + "?" + urlParams;
        } else {
            finalUrl = url;
        }
        HttpGet httpGet = new HttpGet(host + finalUrl);
        if (cookies != null) {
            Enumeration<String> iter = cookies.keys();
            while (iter.hasMoreElements()) {
                String key2 = iter.nextElement();
                httpGet.setHeader("Cookie", key2 + "=" + cookies.get(key2));
            }
        }
        return httpClient.execute(httpGet);
    }
}
