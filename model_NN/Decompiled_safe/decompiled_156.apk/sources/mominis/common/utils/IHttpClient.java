package mominis.common.utils;

import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;

public interface IHttpClient extends HttpClient {
    CookieStore getCookieStore();

    void setCookieStore(CookieStore cookieStore);
}
