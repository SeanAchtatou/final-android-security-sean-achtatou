package mominis.common.utils;

import android.content.Context;
import com.google.inject.Inject;
import mominis.gameconsole.services.IConnectivityMonitor;

public class SafeHttpClientFactory implements IHttpClientFactory {
    IConnectivityMonitor mConMon;
    Context mContext;

    @Inject
    public SafeHttpClientFactory(IConnectivityMonitor conMon, Context context) {
        this.mConMon = conMon;
        this.mContext = context;
    }

    public IHttpClient create() {
        return new SafeHttpClient(this.mConMon, this.mContext);
    }
}
