package mominis.common.utils;

public interface IHttpClientFactory {
    IHttpClient create();
}
