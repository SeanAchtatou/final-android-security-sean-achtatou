package mominis.gameconsole.common;

public class ProgressEventArgs extends EventArgs {
    private int mProgress;

    public ProgressEventArgs(int progress) {
        setProgress(progress);
    }

    private void setProgress(int progress) {
        this.mProgress = progress;
    }

    public int getProgress() {
        return this.mProgress;
    }

    public boolean equals(Object o) {
        if (!(o instanceof ProgressEventArgs)) {
            return false;
        }
        return this.mProgress == ((ProgressEventArgs) o).mProgress;
    }

    public int hashCode() {
        return this.mProgress;
    }
}
