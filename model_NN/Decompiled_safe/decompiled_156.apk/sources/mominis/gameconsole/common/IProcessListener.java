package mominis.gameconsole.common;

import mominis.gameconsole.common.ProgressEventArgs;

public interface IProcessListener<T extends ProgressEventArgs> {
    void OnProgress(Object obj, T t);

    void onEnd(Object obj, T t);

    void onStart(Object obj, T t);
}
