package mominis.gameconsole.common;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import java.util.ArrayList;

public class MessageBox {

    public enum MessageBoxButtons {
        OK,
        YES_NO,
        CUSTOM
    }

    public static AlertDialog.Builder Show(Context ctx, String title, String message, MessageBoxButtons buttonsType) {
        return Show(ctx, title, message, buttonsType, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
    }

    public static AlertDialog.Builder Show(Context ctx, String title, String message, MessageBoxButtons buttonsType, DialogInterface.OnClickListener onButton1) {
        return Show(ctx, title, message, buttonsType, onButton1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
    }

    public static AlertDialog.Builder Show(Context ctx, String title, String message, MessageBoxButtons buttonsType, DialogInterface.OnClickListener onButton1, DialogInterface.OnClickListener onButton2) {
        return Show(ctx, title, message, buttonsType, new ArrayList(), onButton1, onButton2);
    }

    public static AlertDialog.Builder Show(Context ctx, String title, String message, MessageBoxButtons buttonsType, ArrayList<String> buttonTexts, DialogInterface.OnClickListener onButton1, DialogInterface.OnClickListener onButton2) {
        AlertDialog.Builder msgBox = new AlertDialog.Builder(ctx);
        msgBox.setTitle(title);
        msgBox.setMessage(message);
        switch (buttonsType) {
            case OK:
                msgBox.setPositiveButton(ctx.getResources().getString(17039370), onButton1);
                break;
            case YES_NO:
                msgBox.setPositiveButton(ctx.getResources().getString(17039379), onButton1);
                msgBox.setNegativeButton(ctx.getResources().getString(17039369), onButton2);
                break;
            case CUSTOM:
                if (buttonTexts.size() >= 1) {
                    msgBox.setPositiveButton(buttonTexts.get(0), onButton1);
                    if (buttonTexts.size() >= 2) {
                        msgBox.setNegativeButton(buttonTexts.get(1), onButton2);
                        break;
                    }
                }
                break;
        }
        msgBox.show();
        return msgBox;
    }
}
