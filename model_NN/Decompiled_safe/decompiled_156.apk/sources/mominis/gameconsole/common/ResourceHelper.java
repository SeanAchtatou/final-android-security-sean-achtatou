package mominis.gameconsole.common;

import android.content.Context;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;

public class ResourceHelper {
    public static CharSequence readAsset(Context context, String assetName) {
        BufferedReader in = null;
        try {
            BufferedReader in2 = new BufferedReader(new InputStreamReader(context.getAssets().open(assetName)));
            try {
                StringBuilder buffer = new StringBuilder();
                while (true) {
                    String line = in2.readLine();
                    if (line != null) {
                        buffer.append(line).append("\n");
                    } else {
                        closeStream(in2);
                        return buffer;
                    }
                }
            } catch (IOException e) {
                in = in2;
                closeStream(in);
                return "";
            } catch (Throwable th) {
                closeStream(in2);
                throw th;
            }
        } catch (IOException e2) {
            closeStream(in);
            return "";
        }
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
            }
        }
    }
}
