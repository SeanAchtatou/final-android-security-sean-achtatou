package mominis.gameconsole.common;

import java.util.Iterator;
import java.util.List;

public class StringUtils {
    public static String join(List<?> toJoin, String delimiter) {
        StringBuffer buffer = new StringBuffer();
        Iterator<?> iter = toJoin.iterator();
        while (iter.hasNext()) {
            buffer.append(iter.next());
            if (iter.hasNext()) {
                buffer.append(delimiter);
            }
        }
        return buffer.toString();
    }

    public static boolean isEmpty(String configuredUserAgent) {
        return (configuredUserAgent == null || configuredUserAgent == "") ? false : true;
    }
}
