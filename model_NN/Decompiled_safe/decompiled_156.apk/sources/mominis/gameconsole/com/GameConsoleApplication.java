package mominis.gameconsole.com;

import android.telephony.TelephonyManager;
import android.util.Log;
import com.google.inject.Binder;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.Scopes;
import dalvik.system.VMRuntime;
import java.util.List;
import java.util.concurrent.Executor;
import mominis.common.mvc.INavigationManager;
import mominis.common.utils.IHttpClientFactory;
import mominis.common.utils.SafeHttpClientFactory;
import mominis.common.utils.ThreadPerTaskExecutor;
import mominis.gameconsole.core.repositories.AppRepository;
import mominis.gameconsole.core.repositories.IAppRepository;
import mominis.gameconsole.core.repositories.IRemoteRepository;
import mominis.gameconsole.core.repositories.JsonRemoteRepository;
import mominis.gameconsole.services.IAnalyticsManager;
import mominis.gameconsole.services.IAppManager;
import mominis.gameconsole.services.IAutoUpdater;
import mominis.gameconsole.services.IConnectivityMonitor;
import mominis.gameconsole.services.IGameConsoleInfo;
import mominis.gameconsole.services.IGameConsoleServer;
import mominis.gameconsole.services.ISubscriptionManager;
import mominis.gameconsole.services.IUserMembership;
import mominis.gameconsole.services.IWelcomeNotificationManager;
import mominis.gameconsole.services.impl.AppManager;
import mominis.gameconsole.services.impl.AutoUpdater;
import mominis.gameconsole.services.impl.ConnectivityMonitor;
import mominis.gameconsole.services.impl.GameConsoleInfo;
import mominis.gameconsole.services.impl.GameConsoleServer;
import mominis.gameconsole.services.impl.GoogleAnalyticsImpl;
import mominis.gameconsole.services.impl.NavigationManager;
import mominis.gameconsole.services.impl.RemoteRepository;
import mominis.gameconsole.services.impl.ServerGoogleAnalytics;
import mominis.gameconsole.services.impl.SubscriptionManager;
import mominis.gameconsole.services.impl.UserMembership;
import mominis.gameconsole.services.impl.WelcomeNotificationManager;
import mominis.gameconsole.views.Views;
import mominis.gameconsole.views.impl.Catalog;
import mominis.gameconsole.views.impl.Download;
import mominis.gameconsole.views.impl.EulaView;
import mominis.gameconsole.views.impl.LauncherView;
import mominis.gameconsole.views.impl.Purchase;
import mominis.gameconsole.views.impl.Splash;
import roboguice.application.RoboApplication;
import roboguice.inject.SystemServiceProvider;

public class GameConsoleApplication extends RoboApplication {
    private static final String TAG = "GameConsoleApplication";
    private final long INITIAL_HEAP_SIZE = 8388608;
    private Module mModule;

    public GameConsoleApplication() {
        setModule(new MyModule());
    }

    public void onCreate() {
        super.onCreate();
        Injector injector = getInjector();
        Log.i(TAG, "initialize navigation manager");
        Log.d(TAG, String.format("initial heap size set. old size was %d", Long.valueOf(VMRuntime.getRuntime().setMinimumHeapSize(8388608))));
        INavigationManager navMgr = (INavigationManager) injector.getInstance(INavigationManager.class);
        navMgr.registerView(Views.CATALOG_VIEW, Catalog.class);
        navMgr.registerView(Views.DOWNLOAD_VIEW, Download.class);
        navMgr.registerView(Views.SPLASH_VIEW, Splash.class);
        navMgr.registerView(Views.PURCHASE_VIEW, Purchase.class);
        navMgr.registerView(Views.EULA_VIEW, EulaView.class);
        navMgr.registerView(Views.LAUNCHER_VIEW, LauncherView.class);
    }

    public Module getModule() {
        return this.mModule;
    }

    public void setModule(Module module) {
        this.mModule = module;
    }

    /* access modifiers changed from: protected */
    public void addApplicationModules(List<Module> modules) {
        modules.add(this.mModule);
    }

    private class MyModule implements Module {
        private MyModule() {
        }

        public void configure(Binder binder) {
            binder.bind(IAppRepository.class).to(AppRepository.class).in(Scopes.SINGLETON);
            binder.bind(IAppManager.class).to(AppManager.class).in(Scopes.SINGLETON);
            binder.bind(ISubscriptionManager.class).to(SubscriptionManager.class).in(Scopes.SINGLETON);
            binder.bind(INavigationManager.class).to(NavigationManager.class).in(Scopes.SINGLETON);
            if (GameConsoleApplication.this.getApplicationContext().getResources().getBoolean(R.Boolean.use_s2s_google_analytics)) {
                binder.bind(IAnalyticsManager.class).to(ServerGoogleAnalytics.class).in(Scopes.SINGLETON);
            } else {
                binder.bind(IAnalyticsManager.class).to(GoogleAnalyticsImpl.class).in(Scopes.SINGLETON);
            }
            binder.bind(IUserMembership.class).to(UserMembership.class).in(Scopes.SINGLETON);
            binder.bind(IConnectivityMonitor.class).to(ConnectivityMonitor.class).in(Scopes.SINGLETON);
            binder.bind(IGameConsoleServer.class).to(GameConsoleServer.class).in(Scopes.SINGLETON);
            binder.bind(Executor.class).to(ThreadPerTaskExecutor.class).in(Scopes.NO_SCOPE);
            binder.bind(IRemoteRepository.class).annotatedWith(JsonRemoteRepository.class).to(RemoteRepository.class);
            binder.bind(IAutoUpdater.class).to(AutoUpdater.class).in(Scopes.SINGLETON);
            binder.bind(IWelcomeNotificationManager.class).to(WelcomeNotificationManager.class).in(Scopes.SINGLETON);
            binder.bind(IHttpClientFactory.class).to(SafeHttpClientFactory.class).in(Scopes.SINGLETON);
            binder.bind(IGameConsoleInfo.class).to(GameConsoleInfo.class).in(Scopes.SINGLETON);
            binder.bind(TelephonyManager.class).toProvider(new SystemServiceProvider("phone"));
        }
    }
}
