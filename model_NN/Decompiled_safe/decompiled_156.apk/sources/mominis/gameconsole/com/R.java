package mominis.gameconsole.com;

public final class R {

    public static final class Boolean {
        public static final int use_s2s_google_analytics = 2131099648;
        public static final int use_uuid_for_tracking = 2131099649;
    }

    public static final class anim {
        public static final int fade_in_and_rotate_animation = 2130968576;
        public static final int fade_in_animation = 2130968577;
        public static final int fade_out_animation = 2130968578;
        public static final int loading_fade_out_animation = 2130968579;
        public static final int rotate_animation_infinite = 2130968580;
        public static final int scrolling_hint_animation = 2130968581;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int arrow_250_250 = 2130837504;
        public static final int cancel_btn = 2130837505;
        public static final int checkbox = 2130837506;
        public static final int checkbox_background = 2130837507;
        public static final int checkbox_label_background = 2130837508;
        public static final int checkbox_off_background = 2130837509;
        public static final int checkbox_off_background_focus_yellow = 2130837510;
        public static final int checkbox_on_background = 2130837511;
        public static final int checkbox_on_background_focus_yellow = 2130837512;
        public static final int download_progress_bar = 2130837513;
        public static final int free_button = 2130837514;
        public static final int header_top = 2130837515;
        public static final int ic_menu_refresh = 2130837516;
        public static final int ic_menu_terms = 2130837517;
        public static final int icon = 2130837518;
        public static final int loading_frame = 2130837519;
        public static final int loading_poly = 2130837520;
        public static final int loading_progress = 2130837521;
        public static final int logo_tri = 2130837522;
        public static final int new_icon = 2130837523;
        public static final int no_thanks_btn = 2130837524;
        public static final int no_thanks_btn_background = 2130837525;
        public static final int no_thanks_btn_hovered = 2130837526;
        public static final int poly_unlock = 2130837527;
        public static final int preinstalled = 2130837528;
        public static final int purchase_page_upper_background = 2130837529;
        public static final int separator = 2130837530;
        public static final int splash_intro = 2130837531;
        public static final int splash_logo_poly_white = 2130837532;
        public static final int splash_mominis_logo = 2130837533;
        public static final int subscribe_btn = 2130837534;
        public static final int subscribe_btn_background = 2130837535;
        public static final int subscribe_btn_dark = 2130837536;
        public static final int unlock_btn = 2130837537;
        public static final int upper_purchase_dialog = 2130837538;
    }

    public static final class id {
        public static final int BlurGameLayout = 2131230734;
        public static final int GamePolyView = 2131230725;
        public static final int MBLeft = 2131230741;
        public static final int MoMinisLogoView = 2131230785;
        public static final int PolyView = 2131230784;
        public static final int RelativeLayoutCatalogGame = 2131230723;
        public static final int RelativeLayoutSplash = 2131230782;
        public static final int RootLayout = 2131230745;
        public static final int SplashView = 2131230783;
        public static final int ThumbnailView = 2131230727;
        public static final int UnlockBtn = 2131230736;
        public static final int UpperLayout = 2131230746;
        public static final int btnCancel = 2131230744;
        public static final int btnFree = 2131230729;
        public static final int btnNew = 2131230733;
        public static final int btnNo = 2131230755;
        public static final int btnSubscribe = 2131230756;
        public static final int buttonsLayout = 2131230754;
        public static final int checkboxLayout = 2131230757;
        public static final int chkAgreeLicense = 2131230758;
        public static final int download_loading = 2131230740;
        public static final int ex_progress_bar = 2131230742;
        public static final int game_free_layout = 2131230728;
        public static final int game_locked_layout = 2131230735;
        public static final int gridview = 2131230720;
        public static final int imageView1 = 2131230750;
        public static final int layout_free_text = 2131230730;
        public static final int loading_game_layout = 2131230724;
        public static final int loading_msg = 2131230743;
        public static final int logo_header = 2131230721;
        public static final int mid = 2131230738;
        public static final int notesContainerLayout = 2131230759;
        public static final int notesLayout = 2131230761;
        public static final int priceLayout = 2131230751;
        public static final int progress_wrapper = 2131230739;
        public static final int refresh = 2131230786;
        public static final int scrolling_hint = 2131230722;
        public static final int textView10 = 2131230768;
        public static final int textView11 = 2131230769;
        public static final int textView12 = 2131230770;
        public static final int textView13 = 2131230771;
        public static final int textView14 = 2131230772;
        public static final int textView15 = 2131230773;
        public static final int textView16 = 2131230774;
        public static final int textView17 = 2131230775;
        public static final int textView18 = 2131230776;
        public static final int textView19 = 2131230777;
        public static final int textView20 = 2131230778;
        public static final int textView21 = 2131230779;
        public static final int textView22 = 2131230780;
        public static final int textView23 = 2131230781;
        public static final int textView4 = 2131230762;
        public static final int textView5 = 2131230763;
        public static final int textView6 = 2131230764;
        public static final int textView7 = 2131230765;
        public static final int textView8 = 2131230766;
        public static final int textView9 = 2131230767;
        public static final int topLayout = 2131230747;
        public static final int txtForEachMonth = 2131230753;
        public static final int txtFree = 2131230731;
        public static final int txtFree2 = 2131230732;
        public static final int txtLoading = 2131230726;
        public static final int txtNotesTitle = 2131230760;
        public static final int txtPurchasePrice = 2131230752;
        public static final int txtUnlock = 2131230737;
        public static final int txtUnlockAllGames = 2131230748;
        public static final int txtUnlockAllGamesSubtitle = 2131230749;
    }

    public static final class layout {
        public static final int catalog = 2130903040;
        public static final int catalog_game = 2130903041;
        public static final int download = 2130903042;
        public static final int purchase = 2130903043;
        public static final int splash = 2130903044;
    }

    public static final class menu {
        public static final int catalog_menu = 2131165184;
    }

    public static final class string {
        public static final int CC_InstallationCanceled = 2131034137;
        public static final int CC_Installing = 2131034135;
        public static final int CC_RegistrationCompleted = 2131034136;
        public static final int Cancel = 2131034133;
        public static final int Close = 2131034123;
        public static final int Google_TrackID = 2131034120;
        public static final int OK = 2131034132;
        public static final int RegistrationTitle = 2131034134;
        public static final int app_name = 2131034124;
        public static final int auto_update_url = 2131034115;
        public static final int catalog_refresh_already_executing = 2131034159;
        public static final int clicked_download_app = 2131034127;
        public static final int clicked_run_app = 2131034126;
        public static final int clicked_string = 2131034125;
        public static final int download_name = 2131034128;
        public static final int download_page_downloading = 2131034169;
        public static final int eula_accept = 2131034130;
        public static final int eula_refuse = 2131034131;
        public static final int eula_title = 2131034129;
        public static final int game_catalog_free1 = 2131034173;
        public static final int game_catalog_free2 = 2131034174;
        public static final int game_catalog_loading = 2131034168;
        public static final int game_catalog_unlock = 2131034170;
        public static final int game_catalog_url = 2131034116;
        public static final int game_console_address = 2131034113;
        public static final int game_console_base_url = 2131034114;
        public static final int game_console_hostname = 2131034112;
        public static final int game_download_canceled = 2131034176;
        public static final int game_download_failed_short = 2131034177;
        public static final int game_download_failed_text = 2131034172;
        public static final int game_download_failed_title = 2131034171;
        public static final int game_download_success = 2131034175;
        public static final int membership_dialog_title = 2131034143;
        public static final int membership_logged_in_non_purchased = 2131034156;
        public static final int membership_login_failed = 2131034140;
        public static final int membership_offline_mode = 2131034139;
        public static final int membership_one_day_before_suspension = 2131034141;
        public static final int membership_roaming_mode = 2131034138;
        public static final int membership_suspended = 2131034142;
        public static final int menu_refresh = 2131034158;
        public static final int preloaded_game_name = 2131034118;
        public static final int preloaded_game_package_name = 2131034119;
        public static final int purchase_complete_with_billing = 2131034154;
        public static final int purchase_complete_without_billing = 2131034153;
        public static final int purchase_message_already_paid = 2131034152;
        public static final int purchase_message_please_wait = 2131034155;
        public static final int purchase_message_title = 2131034151;
        public static final int purchase_name = 2131034160;
        public static final int purchase_page_checkbox = 2131034149;
        public static final int purchase_page_message = 2131034145;
        public static final int purchase_page_no = 2131034147;
        public static final int purchase_page_notes_bullet = 2131034157;
        public static final int purchase_page_notes_title = 2131034150;
        public static final int purchase_page_plans_not_loaded = 2131034162;
        public static final int purchase_page_price_sub_title = 2131034146;
        public static final int purchase_page_registration_failed = 2131034161;
        public static final int purchase_page_subscribe = 2131034148;
        public static final int purchase_page_title = 2131034144;
        public static final int refresh_dialog_text = 2131034163;
        public static final int update_message_button_exit = 2131034167;
        public static final int update_message_button_update = 2131034166;
        public static final int update_message_text = 2131034165;
        public static final int update_message_title = 2131034164;
        public static final int user_agent = 2131034121;
        public static final int welcome_caption = 2131034122;
        public static final int welcome_notification_url = 2131034117;
    }
}
