package mominis.gameconsole.core.repositories;

import mominis.gameconsole.core.models.Application;

public interface IRemoteRepository extends IReadableAppRepository {
    Application getByIndex(int i);

    boolean refresh();
}
