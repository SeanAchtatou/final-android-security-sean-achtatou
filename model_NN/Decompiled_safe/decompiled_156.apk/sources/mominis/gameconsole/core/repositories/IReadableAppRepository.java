package mominis.gameconsole.core.repositories;

import java.io.IOException;
import mominis.common.repositories.IReadableRepository;
import mominis.gameconsole.core.models.Application;

public interface IReadableAppRepository extends IReadableRepository<Application> {
    Application get(String str) throws IOException;
}
