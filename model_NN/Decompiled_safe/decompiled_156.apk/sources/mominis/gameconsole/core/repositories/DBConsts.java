package mominis.gameconsole.core.repositories;

public class DBConsts {
    public static final String APK_PATH_NAME = "apkpath";
    public static final String DATABASE_NAME = "datastorage";
    public static final int DATABASE_VERSION = 1;
    public static final String FREE_NAME = "free";
    public static final String KEY_ID = "_id";
    public static final String PAKCAGE_NAME = "package";
    public static final String STATE_NAME = "state";
    public static final String TABLE_NAME = "Apps";
    public static final String THUMBNAIL_NAME = "thumnail";
    public static final String TITLE_NAME = "appname";
}
