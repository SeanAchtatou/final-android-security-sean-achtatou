package mominis.gameconsole.core.repositories;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import com.google.inject.Inject;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import mominis.common.mvc.BaseObservable;
import mominis.common.mvc.IObserver;
import mominis.common.mvc.ListChangedEventArgs;
import mominis.gameconsole.core.models.Application;

public class AppRepository implements IAppRepository {
    private static final String TAG = "App Repository";
    private final Context context;
    private final AppRepositoryHelper dbhelper;
    private List<Application> mCache = new ArrayList();
    private BaseObservable<ListChangedEventArgs> mObservable;
    private Object mSync = new Object();
    boolean validCollection = false;

    @Inject
    public AppRepository(Context c) {
        this.context = c;
        this.dbhelper = new AppRepositoryHelper(this.context, DBConsts.DATABASE_NAME, null, 1);
        this.mObservable = new BaseObservable<>(this);
    }

    public Application create(Application toCreate) throws IOException {
        if (toCreate == null) {
            throw new InvalidParameterException("toCreate cannot be null");
        }
        SQLiteDatabase db = null;
        Application app = new Application();
        Log.d(TAG, "creating a new application");
        app.CopyFrom(toCreate);
        try {
            db = this.dbhelper.getWritableDatabase();
            long id = db.insert(DBConsts.TABLE_NAME, null, getContentValues(app));
            if (id == -1) {
                throw new IOException("entry creation failed (insert returned -1)");
            }
            app.setID(id);
            Log.d(TAG, "notifying observers of the change");
            this.mObservable.notifyObservers(new ListChangedEventArgs(ListChangedEventArgs.Action.Add, size(), app));
            invalidateCache();
            Log.d(TAG, "create ended succesfully - reutrning the created app");
            if (db != null && db.isOpen()) {
                db.close();
            }
            return app;
        } catch (SQLiteException e) {
            throw new IOException("cannot create an entry");
        } catch (Throwable th) {
            if (db != null && db.isOpen()) {
                db.close();
            }
            throw th;
        }
    }

    public void delete(long id) throws IOException {
        SQLiteDatabase db = null;
        Application toDelete = get(id);
        try {
            db = this.dbhelper.getWritableDatabase();
            if (db.delete(DBConsts.TABLE_NAME, "_id=?", new String[]{String.valueOf(id)}) != 1) {
                throw new IOException("entry deletion failed (rows effected is not 1)");
            }
            if (toDelete != null) {
                invalidateCache();
                this.mObservable.notifyObservers(new ListChangedEventArgs(ListChangedEventArgs.Action.Remove, -1, toDelete));
            }
            if (db != null && db.isOpen()) {
                db.close();
            }
        } catch (SQLiteException e) {
            throw new IOException("cannot delete the entry");
        } catch (Throwable th) {
            if (db != null && db.isOpen()) {
                db.close();
            }
            throw th;
        }
    }

    public Application get(long id) throws IOException {
        return getInternal(id);
    }

    private Application getInternal(long id) throws IOException {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = this.dbhelper.getReadableDatabase();
            c = db.query(DBConsts.TABLE_NAME, null, "_id=?", new String[]{String.valueOf(id)}, null, null, null, null);
            c.moveToFirst();
            Application appFromCursor = getAppFromCursor(c);
            if (db != null && db.isOpen()) {
                db.close();
            }
            if (c != null && !c.isClosed()) {
                c.close();
            }
            return appFromCursor;
        } catch (SQLiteException e) {
            throw new IOException("cannot read the entry");
        } catch (Throwable th) {
            if (db != null && db.isOpen()) {
                db.close();
            }
            if (c != null && !c.isClosed()) {
                c.close();
            }
            throw th;
        }
    }

    public Application get(String application) throws IOException {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = this.dbhelper.getReadableDatabase();
            c = db.query(DBConsts.TABLE_NAME, null, "package=?", new String[]{application}, null, null, null, null);
            c.moveToFirst();
            Application appFromCursor = getAppFromCursor(c);
            if (db != null && db.isOpen()) {
                db.close();
            }
            if (c != null && !c.isClosed()) {
                c.close();
            }
            return appFromCursor;
        } catch (SQLiteException e) {
            throw new IOException("cannot read the entry");
        } catch (Throwable th) {
            if (db != null && db.isOpen()) {
                db.close();
            }
            if (c != null && !c.isClosed()) {
                c.close();
            }
            throw th;
        }
    }

    public List<Application> getAll() throws IOException {
        if (!this.validCollection) {
            synchronized (this.mSync) {
                if (!this.validCollection) {
                    this.mCache = Collections.unmodifiableList(gettAllInternal());
                    this.validCollection = true;
                }
            }
        }
        return this.mCache;
    }

    private List<Application> gettAllInternal() throws IOException {
        SQLiteDatabase db = null;
        ArrayList<Application> list = new ArrayList<>();
        Cursor c = null;
        try {
            SQLiteDatabase db2 = this.dbhelper.getReadableDatabase();
            Cursor c2 = db2.query(DBConsts.TABLE_NAME, null, null, null, null, null, null);
            c2.moveToFirst();
            while (!c2.isAfterLast()) {
                list.add(getAppFromCursor(c2));
                c2.moveToNext();
            }
            if (db2 != null && db2.isOpen()) {
                db2.close();
            }
            if (c2 != null && !c2.isClosed()) {
                c2.close();
            }
            return list;
        } catch (SQLiteException e) {
            throw new IOException("cannot read the entries");
        } catch (Throwable th) {
            if (db != null && db.isOpen()) {
                db.close();
            }
            if (c != null && !c.isClosed()) {
                c.close();
            }
            throw th;
        }
    }

    public void update(Application toUpdate) throws IOException {
        SQLiteDatabase db = null;
        ContentValues values = getContentValues(toUpdate);
        try {
            db = this.dbhelper.getWritableDatabase();
            if (db.update(DBConsts.TABLE_NAME, values, "_id=?", new String[]{String.valueOf(toUpdate.getID())}) != 1) {
                throw new IOException("entry update failed (rows effected is not 1)");
            }
            invalidateCache();
            this.mObservable.notifyObservers(new ListChangedEventArgs(ListChangedEventArgs.Action.Set, -1, toUpdate));
            if (db != null && db.isOpen()) {
                db.close();
            }
        } catch (SQLiteException e) {
            throw new IOException("cannot read the entries");
        } catch (Throwable th) {
            if (db != null && db.isOpen()) {
                db.close();
            }
            throw th;
        }
    }

    public int size() throws IOException {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = this.dbhelper.getReadableDatabase();
            c = db.query(DBConsts.TABLE_NAME, null, null, null, null, null, null);
            int count = c.getCount();
            if (db != null && db.isOpen()) {
                db.close();
            }
            if (c != null && !c.isClosed()) {
                c.close();
            }
            return count;
        } catch (SQLiteException e) {
            throw new IOException("cannot read the entries");
        } catch (Throwable th) {
            if (db != null && db.isOpen()) {
                db.close();
            }
            if (c != null && !c.isClosed()) {
                c.close();
            }
            throw th;
        }
    }

    public void registerObserver(IObserver<ListChangedEventArgs> observer) {
        this.mObservable.registerObserver(observer);
    }

    public void unregisterObserver(IObserver<ListChangedEventArgs> observer) {
        this.mObservable.unregisterObserver(observer);
    }

    private static Application getAppFromCursor(Cursor c) {
        Application app = new Application();
        app.setAPKPath(c.getString(c.getColumnIndex(DBConsts.APK_PATH_NAME)));
        app.setID(c.getLong(c.getColumnIndex(DBConsts.KEY_ID)));
        app.setName(c.getString(c.getColumnIndex(DBConsts.TITLE_NAME)));
        app.setPackage(c.getString(c.getColumnIndex(DBConsts.PAKCAGE_NAME)));
        app.setThumbnail(c.getBlob(c.getColumnIndex(DBConsts.THUMBNAIL_NAME)));
        app.setState((Application.State) Enum.valueOf(Application.State.class, c.getString(c.getColumnIndex(DBConsts.STATE_NAME))));
        app.setFree(c.getInt(c.getColumnIndex(DBConsts.FREE_NAME)) != 0);
        return app;
    }

    private static ContentValues getContentValues(Application toUpdate) {
        ContentValues values = new ContentValues();
        values.put(DBConsts.APK_PATH_NAME, toUpdate.getAPKPath().toString());
        values.put(DBConsts.PAKCAGE_NAME, toUpdate.getPackage());
        values.put(DBConsts.TITLE_NAME, toUpdate.getName());
        values.put(DBConsts.STATE_NAME, toUpdate.getState().toString());
        byte[] imageData = toUpdate.getThumbnail();
        if (imageData != null) {
            values.put(DBConsts.THUMBNAIL_NAME, imageData);
        }
        values.put(DBConsts.FREE_NAME, String.valueOf(toUpdate.isFree() ? 1 : 0));
        return values;
    }

    private void invalidateCache() {
        Log.d(TAG, "invalidaing cache");
        synchronized (this.mSync) {
            this.validCollection = false;
        }
    }
}
