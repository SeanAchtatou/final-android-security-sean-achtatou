package mominis.gameconsole.core.repositories;

import java.util.List;
import mominis.common.mvc.IObserver;
import mominis.common.mvc.ListChangedEventArgs;
import mominis.common.mvc.ObservableList;
import mominis.gameconsole.core.models.Application;

public class AppRepositoryMock implements IAppRepository {
    private ObservableList<Application> mApps = new ObservableList<>(this);

    public Application create(Application toCreate) {
        Application app = new Application();
        app.CopyFrom(toCreate);
        app.setID((long) this.mApps.size());
        this.mApps.add(app);
        return app;
    }

    public void delete(long id) {
        int loc = -1;
        for (int i = 0; i < this.mApps.size(); i++) {
            if (this.mApps.get(i).getID() == id) {
                loc = i;
            }
        }
        if (loc != -1) {
            this.mApps.remove(loc);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public Application get(long id) {
        for (int i = 0; i < this.mApps.size(); i++) {
            if (this.mApps.get(i).getID() == id) {
                return this.mApps.get(i);
            }
        }
        return null;
    }

    public void update(Application toUpdate) {
        Application app = get(toUpdate.getID());
        if (app != null) {
            app.CopyFrom(toUpdate);
        }
    }

    public List<Application> getAll() {
        return this.mApps;
    }

    public int size() {
        return this.mApps.size();
    }

    public void registerObserver(IObserver<ListChangedEventArgs> observer) {
        this.mApps.registerObserver(observer);
    }

    public void unregisterObserver(IObserver<ListChangedEventArgs> observer) {
        this.mApps.unregisterObserver(observer);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public Application get(String packageName) {
        for (int i = 0; i < this.mApps.size(); i++) {
            if (this.mApps.get(i).getPackage() == packageName) {
                return this.mApps.get(i);
            }
        }
        return null;
    }
}
