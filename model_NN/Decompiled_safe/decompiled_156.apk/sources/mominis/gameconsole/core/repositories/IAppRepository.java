package mominis.gameconsole.core.repositories;

import mominis.common.repositories.IRepository;
import mominis.gameconsole.core.models.Application;

public interface IAppRepository extends IRepository<Application>, IReadableAppRepository {
}
