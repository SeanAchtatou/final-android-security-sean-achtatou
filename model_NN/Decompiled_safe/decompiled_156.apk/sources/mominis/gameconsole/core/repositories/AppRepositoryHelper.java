package mominis.gameconsole.core.repositories;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class AppRepositoryHelper extends SQLiteOpenHelper {
    private static final String CREATE_TABLE = "create table Apps (_id integer primary key autoincrement, appname text not null, apkpath text not null, package text not null unique, thumnail BLOB not null, state text not null, free int not null);";

    public AppRepositoryHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void onCreate(SQLiteDatabase db) {
        Log.v("MyDBhelper onCreate", "Creating all the tables");
        try {
            db.execSQL(CREATE_TABLE);
        } catch (SQLiteException e) {
            Log.v("Create table exception", e.getMessage());
        }
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w("TaskDBAdapter", "Upgrading from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
        db.execSQL("drop table if exists Apps");
        onCreate(db);
    }
}
