package mominis.gameconsole.core.models;

import java.util.Hashtable;

public class UserIdentifier {
    private static final String KEY__CHANNEL_ID = "ChannelID";
    private static final String KEY__PASSWORD = "Password";
    private static final String KEY__USERNAME = "Username";
    private static final String KEY__USER_ID = "UserID";
    public int ChannelID;
    public String Password;
    public int UserID;
    public String Username;

    public Hashtable<String, Object> toKeyValue() {
        Hashtable<String, Object> userIden = new Hashtable<>();
        userIden.put(KEY__CHANNEL_ID, Integer.toString(this.ChannelID));
        userIden.put(KEY__USERNAME, this.Username);
        userIden.put(KEY__USER_ID, Integer.valueOf(this.UserID));
        userIden.put(KEY__PASSWORD, this.Password);
        return userIden;
    }

    public static UserIdentifier fromKeyValue(Hashtable<String, Object> input) {
        UserIdentifier ret = new UserIdentifier();
        ret.Username = (String) input.get(KEY__USERNAME);
        ret.UserID = ((Integer) input.get(KEY__USER_ID)).intValue();
        ret.Password = (String) input.get(KEY__PASSWORD);
        if (input.containsKey(KEY__CHANNEL_ID)) {
            ret.ChannelID = Integer.parseInt((String) input.get(KEY__CHANNEL_ID));
        }
        return ret;
    }
}
