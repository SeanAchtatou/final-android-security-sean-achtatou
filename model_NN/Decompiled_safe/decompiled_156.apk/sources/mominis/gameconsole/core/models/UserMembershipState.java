package mominis.gameconsole.core.models;

public class UserMembershipState {
    public static final UserMembershipState DEFAULT = new UserMembershipState();
    private boolean mBillingMessageDisplayed;
    private BillingResult mBillingState;
    private UserMembershipStatus mMembershipStatus;
    private int planId;

    public enum BillingResult {
        BILLING_NOT_SUPPORTED,
        SUCCESS,
        ALREADY_REGISTERED,
        UA_PENDING,
        ERROR
    }

    public enum UserMembershipStatus {
        USER_LOGGED_IN_REGISTERED,
        USER_LOGGED_IN_NOT_REGISTERED,
        USER_INVALID_CREDENTIALS,
        USER_OFFLINE,
        USER_OFFLINE_REGISTERED,
        USER_OFFFLINE_IN_REGISTERED_BEFORE_SUSPENSION,
        USER_MEMBERSHIP_SUSPENDED
    }

    static {
        DEFAULT.setBillingMessageDisplayed(true);
        DEFAULT.setMembershipStatus(UserMembershipStatus.USER_OFFLINE);
        DEFAULT.setBillingState(BillingResult.ERROR);
        DEFAULT.setPlanId(-1);
    }

    public void setBillingMessageDisplayed(boolean billingMessageDisplayed) {
        this.mBillingMessageDisplayed = billingMessageDisplayed;
    }

    public boolean isBillingMessageDisplayed() {
        return this.mBillingMessageDisplayed;
    }

    public boolean hasAccess() {
        return this.mMembershipStatus == UserMembershipStatus.USER_LOGGED_IN_REGISTERED || this.mMembershipStatus == UserMembershipStatus.USER_OFFFLINE_IN_REGISTERED_BEFORE_SUSPENSION || this.mMembershipStatus == UserMembershipStatus.USER_OFFLINE_REGISTERED;
    }

    public void setBillingState(BillingResult billingState) {
        this.mBillingState = billingState;
    }

    public BillingResult getBillingState() {
        return this.mBillingState;
    }

    public void setMembershipStatus(UserMembershipStatus membershipStatus) {
        this.mMembershipStatus = membershipStatus;
    }

    public UserMembershipStatus getMembershipStatus() {
        return this.mMembershipStatus;
    }

    public void setPlanId(int planId2) {
        this.planId = planId2;
    }

    public int getPlanId() {
        return this.planId;
    }
}
