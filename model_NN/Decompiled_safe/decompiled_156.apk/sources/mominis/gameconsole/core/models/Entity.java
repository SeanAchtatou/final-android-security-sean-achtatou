package mominis.gameconsole.core.models;

public abstract class Entity {
    private long mID;

    public void setID(long id) {
        this.mID = id;
    }

    public long getID() {
        return this.mID;
    }
}
