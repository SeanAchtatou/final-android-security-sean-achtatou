package mominis.gameconsole.core.models;

import java.util.ArrayList;
import java.util.Hashtable;

public class BillingState {
    private static final String KEY__PARAMS = "Params";
    private static final String KEY__PARAMS_KEY = "Key";
    private static final String KEY__PARAMS_VALUE = "Value";
    private static final String KEY__STATE = "State";
    private static final String KEY__TRANSACTION_ID = "TransactionID";
    public Hashtable<String, Object> Params = new Hashtable<>();
    public BillingAuthenticationCode State;
    public int TransactionID;

    public enum BillingAuthenticationCode {
        UO_AUTH_REQUIRED,
        BILLING_NOT_SUPPORTED,
        COMPLETED,
        USER_ALREADY_PAID,
        INVALID_REQUEST
    }

    public static BillingState fromKeyValue(Hashtable<String, Object> input) {
        BillingState ret = new BillingState();
        ret.State = BillingAuthenticationCode.valueOf((String) input.get(KEY__STATE));
        ret.TransactionID = ((Integer) input.get(KEY__TRANSACTION_ID)).intValue();
        ArrayList<Hashtable<String, Object>> params = (ArrayList) input.get(KEY__PARAMS);
        for (int i = 0; i < params.size(); i++) {
            ret.Params.put((String) ((Hashtable) params.get(i)).get(KEY__PARAMS_KEY), (String) ((Hashtable) params.get(i)).get(KEY__PARAMS_VALUE));
        }
        return ret;
    }
}
