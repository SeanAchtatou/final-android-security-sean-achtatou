package mominis.gameconsole.core.models;

import java.util.ArrayList;

public class PurchasePlan {
    public int ChannelId;
    public String Description;
    public int Id;
    public Boolean IsDisabled;
    public String Name;
    public ArrayList<String> PlanNotes;
    public Price PlanPrice;
    public PlanType PlanType;
    public String TermsOfUse;

    public enum PlanType {
        ALL_YOU_CAN_EAT
    }
}
