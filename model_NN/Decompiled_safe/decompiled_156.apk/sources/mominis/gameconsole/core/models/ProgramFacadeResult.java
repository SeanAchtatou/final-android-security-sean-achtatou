package mominis.gameconsole.core.models;

import java.util.Hashtable;

public class ProgramFacadeResult {
    private static final String KEY__BILLING_STATE = "BillingState";
    private static final String KEY__DATA = "d";
    private static final String KEY__REGISTRATION_RESULT = "RegistrationResult";
    private BillingState mBillingState;
    private RegistrationResult mRegistrationResult;

    public void setRegistrationResult(RegistrationResult registrationResult) {
        this.mRegistrationResult = registrationResult;
    }

    public RegistrationResult getRegistrationResult() {
        return this.mRegistrationResult;
    }

    public void setBillingState(BillingState billingState) {
        this.mBillingState = billingState;
    }

    public BillingState getBillingState() {
        return this.mBillingState;
    }

    public static ProgramFacadeResult fromKeyValue(Hashtable<String, Object> input) {
        ProgramFacadeResult rv = new ProgramFacadeResult();
        if (input == null) {
            return null;
        }
        try {
            Hashtable<String, Object> results = (Hashtable) input.get(KEY__DATA);
            rv.mRegistrationResult = RegistrationResult.fromKeyValue((Hashtable) results.get(KEY__REGISTRATION_RESULT));
            rv.mBillingState = BillingState.fromKeyValue((Hashtable) results.get(KEY__BILLING_STATE));
            return rv;
        } catch (Exception e) {
            return null;
        }
    }
}
