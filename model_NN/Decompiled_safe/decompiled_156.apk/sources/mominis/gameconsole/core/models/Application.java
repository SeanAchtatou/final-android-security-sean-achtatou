package mominis.gameconsole.core.models;

import android.net.Uri;

public class Application extends Entity {
    private Uri mAPKPath = Uri.EMPTY;
    private boolean mFree = false;
    private String mName;
    private boolean mNew = false;
    private String mPackage;
    private State mState = State.Downloaded;
    byte[] mThumbnailData;
    private Uri mThumbnailUrl = Uri.EMPTY;
    private boolean mViewable = true;

    public enum State {
        Installed,
        Downloaded,
        Remote
    }

    public void setNew(Boolean state) {
        this.mNew = state.booleanValue();
    }

    public Boolean isNew() {
        return Boolean.valueOf(this.mNew);
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getName() {
        return this.mName;
    }

    public void setPackage(String _package) {
        this.mPackage = _package;
    }

    public String getPackage() {
        return this.mPackage;
    }

    public void setThumbnail(byte[] data) {
        this.mThumbnailData = data;
    }

    public byte[] getThumbnail() {
        return this.mThumbnailData;
    }

    public void setState(State state) {
        this.mState = state;
    }

    public State getState() {
        return this.mState;
    }

    public Uri getAPKPath() {
        return this.mAPKPath;
    }

    public void setAPKPath(Uri path) {
        this.mAPKPath = path;
    }

    public void setAPKPath(String path) {
        this.mAPKPath = Uri.parse(path);
    }

    public Uri getThumbnailUrl() {
        return this.mThumbnailUrl;
    }

    public void setThumbnailUrl(Uri path) {
        this.mThumbnailUrl = path;
    }

    public void setThumbnailUrl(String path) {
        this.mThumbnailUrl = Uri.parse(path);
    }

    public Boolean isLocal() {
        return Boolean.valueOf(this.mAPKPath != null);
    }

    public void setFree(boolean free) {
        this.mFree = free;
    }

    public boolean isFree() {
        return this.mFree;
    }

    public void setViewable(boolean viewable) {
        this.mViewable = viewable;
    }

    public boolean isViewable() {
        return this.mViewable;
    }

    public boolean equals(Object c) {
        if (!(c instanceof Application)) {
            return false;
        }
        return ((Application) c).getPackage().equals(getPackage());
    }

    public void CopyFrom(Application toUpdate) {
        this.mAPKPath = toUpdate.mAPKPath;
        this.mName = toUpdate.mName;
        this.mPackage = toUpdate.mPackage;
        this.mState = toUpdate.mState;
        this.mThumbnailData = toUpdate.mThumbnailData;
        this.mFree = toUpdate.mFree;
        this.mViewable = toUpdate.mViewable;
    }
}
