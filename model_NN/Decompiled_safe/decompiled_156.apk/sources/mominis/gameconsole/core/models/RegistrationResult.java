package mominis.gameconsole.core.models;

import java.util.Hashtable;

public class RegistrationResult {
    private static final String KEY__CHANNEL_ID = "ChannelID";
    private static final String KEY__CODE = "Code";
    private static final String KEY__ERROR_DESCRIPTION = "ErrorDescription";
    private static final String KEY__PASSWORD = "Password";
    private static final String KEY__USERNAME = "Username";
    private static final String KEY__USER_ID = "UserID";
    public RegistrationCode Code;
    public String ErrorDescription;
    private UserIdentifier mUserIdentifier = new UserIdentifier();

    public enum RegistrationCode {
        SUCCESS,
        USER_ALREDY_EXIST,
        INVALID_USERNAME,
        PASSWORD_TOO_WEAK
    }

    public void setUserIdentifier(UserIdentifier userIdentifier) {
        this.mUserIdentifier = userIdentifier;
    }

    public UserIdentifier getUserIdentifier() {
        return this.mUserIdentifier;
    }

    public static RegistrationResult fromKeyValue(Hashtable<String, Object> input) {
        RegistrationResult ret = new RegistrationResult();
        ret.Code = RegistrationCode.valueOf((String) input.get(KEY__CODE));
        Object errorDescription = input.get(KEY__ERROR_DESCRIPTION);
        if (errorDescription instanceof String) {
            ret.ErrorDescription = (String) errorDescription;
        } else {
            ret.ErrorDescription = null;
        }
        ret.getUserIdentifier().UserID = ((Integer) input.get(KEY__USER_ID)).intValue();
        ret.getUserIdentifier().Username = (String) input.get(KEY__USERNAME);
        ret.getUserIdentifier().Password = (String) input.get(KEY__PASSWORD);
        if (input.containsKey(KEY__CHANNEL_ID)) {
            ret.getUserIdentifier().ChannelID = Integer.parseInt((String) input.get(KEY__CHANNEL_ID));
        }
        return ret;
    }
}
