package mominis.gameconsole.controllers;

import android.net.Uri;
import mominis.gameconsole.core.models.Application;

public class AppPresentation {
    private Availability mAvailability;
    private String mCaption;
    private long mID;
    private Uri mImage;
    private Boolean mIsFree;
    private String mName;
    private Boolean mNew;
    private String mPackageName;
    private Application.State mState;
    private byte[] mThumbnail;
    private Boolean mThumbnailFirstLoaded;

    public enum Availability {
        Available,
        Locked
    }

    public static AppPresentation FromApplication(Application app, Availability state, Application.State repo) {
        AppPresentation appPres = new AppPresentation(app.getName(), app.getPackage(), app.getID(), state, repo, app.getThumbnail(), Boolean.valueOf(app.isFree()), app.isNew());
        if (app.getThumbnail() != null) {
            appPres.setThumbnailFirstLoaded(true);
        } else {
            appPres.setThumbnailFirstLoaded(false);
        }
        return appPres;
    }

    public AppPresentation(String name, String packageName, long id, Availability state, Application.State repo, byte[] bmpData, Boolean isFree, Boolean isNew) {
        this.mName = name;
        this.mPackageName = packageName;
        this.mID = id;
        this.mAvailability = state;
        this.mThumbnail = bmpData;
        this.mState = repo;
        this.mIsFree = isFree;
        this.mNew = isNew;
    }

    public void setNew(Boolean state) {
        this.mNew = state;
    }

    public Boolean isNew() {
        return this.mNew;
    }

    public String getName() {
        return this.mName;
    }

    public String getPackageName() {
        return this.mPackageName;
    }

    public long getID() {
        return this.mID;
    }

    public String getCaption() {
        return this.mCaption;
    }

    public Uri getImage() {
        return this.mImage;
    }

    public void setState(Availability availability) {
        this.mAvailability = availability;
    }

    public Availability getAvailability() {
        return this.mAvailability;
    }

    public byte[] getThumbnail() {
        return this.mThumbnail;
    }

    public void setThumbnail(byte[] bmp) {
        this.mThumbnail = bmp;
    }

    public void setRepository(Application.State repository) {
        this.mState = repository;
    }

    public void setFree(boolean free) {
        this.mIsFree = Boolean.valueOf(free);
    }

    public boolean isFree() {
        return this.mIsFree.booleanValue();
    }

    public void setThumbnailFirstLoaded(boolean loaded) {
        this.mThumbnailFirstLoaded = Boolean.valueOf(loaded);
    }

    public boolean isThumbnailFirstLoaded() {
        return this.mThumbnailFirstLoaded.booleanValue();
    }

    public Application.State getState() {
        return this.mState;
    }

    public boolean equals(Object o) {
        if (!(o instanceof AppPresentation)) {
            return false;
        }
        return ((AppPresentation) o).mID == this.mID;
    }

    public int hashCode() {
        return (int) this.mID;
    }
}
