package mominis.gameconsole.controllers;

import android.os.Bundle;
import android.util.Log;
import com.google.inject.Inject;
import java.io.IOException;
import mominis.common.mvc.BaseController;
import mominis.common.mvc.INavigationManager;
import mominis.common.mvc.IView;
import mominis.gameconsole.com.R;
import mominis.gameconsole.core.models.Application;
import mominis.gameconsole.services.IAppManager;
import mominis.gameconsole.views.Views;

public class LauncherController extends BaseController {
    public static final String APPID_KEY = "LAUNCHED_APP_ID";
    private static final String TAG = "Launcher Controller";
    private long mAppID = -1;
    private IAppManager mAppMgr;
    private IView mView;

    @Inject
    public LauncherController(IAppManager appMgr, INavigationManager navMgr) {
        super(navMgr);
        this.mAppMgr = appMgr;
    }

    public void setView(IView view) {
        this.mView = view;
    }

    public void onInit() {
        Bundle b = getNavigation().getLaunchData(this.mView);
        if (b != null) {
            this.mAppID = b.getLong(APPID_KEY);
        }
    }

    public void onAppLaunch() {
        this.mView.setStatusText((int) R.string.clicked_run_app);
        Log.d(TAG, String.format("launching app with id %d", Long.valueOf(this.mAppID)));
        try {
            Application app = (Application) this.mAppMgr.getLocalRepository().get(this.mAppID);
            Log.d(TAG, String.format("App %s retreived - launching", app.getName()));
            this.mAppMgr.launchApp(app);
        } catch (IOException e) {
            Log.e(TAG, "an unexpected exception occured while launching the application", e);
        } catch (Exception e2) {
            Log.e(TAG, "an unexpected exception occured while launching the application", e2);
        }
    }

    public void onAppExit() {
        Log.d(TAG, "returning to catalog and closing view");
        getNavigation().showView(this.mView, 0, Views.CATALOG_VIEW);
        this.mView.close();
    }
}
