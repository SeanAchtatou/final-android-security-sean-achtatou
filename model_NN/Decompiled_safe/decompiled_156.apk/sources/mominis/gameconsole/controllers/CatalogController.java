package mominis.gameconsole.controllers;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.google.inject.Inject;
import dalvik.system.VMRuntime;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;
import mominis.common.mvc.BaseController;
import mominis.common.mvc.INavigationManager;
import mominis.common.mvc.IObservable;
import mominis.common.mvc.IObserver;
import mominis.common.mvc.ListChangedEventArgs;
import mominis.gameconsole.com.R;
import mominis.gameconsole.common.EventArgs;
import mominis.gameconsole.common.IProcessListener;
import mominis.gameconsole.controllers.AppPresentation;
import mominis.gameconsole.core.models.Application;
import mominis.gameconsole.core.models.LocaleInfo;
import mominis.gameconsole.core.models.UserMembershipState;
import mominis.gameconsole.core.repositories.IReadableAppRepository;
import mominis.gameconsole.services.AppManagerProgressEventArgs;
import mominis.gameconsole.services.IAnalyticsManager;
import mominis.gameconsole.services.IAppManager;
import mominis.gameconsole.services.IAutoUpdater;
import mominis.gameconsole.services.IConnectivityMonitor;
import mominis.gameconsole.services.ISubscriptionManager;
import mominis.gameconsole.services.IUserMembership;
import mominis.gameconsole.services.IWelcomeNotificationManager;
import mominis.gameconsole.views.ICatalogView;
import mominis.gameconsole.views.Views;
import mominis.gameconsole.views.impl.Eula;

public class CatalogController extends BaseController implements IEulaListener, IProcessListener<AppManagerProgressEventArgs> {
    public static final int DOWNLOADED_SUCCESSFULLY = 1;
    private static final long PURCHASE_CHECK_TIMEOUT = 6000;
    private static final long SLEEP_TIME_BETWEEN_PURCHASE_CHECKS = 100;
    private static final String TAG = "Catalog Controller";
    private boolean doOnce = false;
    private IAnalyticsManager mAnalyticsMgr;
    /* access modifiers changed from: private */
    public IAppManager mAppMgr;
    /* access modifiers changed from: private */
    public List<AppPresentation> mApps = new ArrayList();
    /* access modifiers changed from: private */
    public IAutoUpdater mAutoUpdater;
    private IConnectivityMonitor mConnectivityMonitor;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public boolean mCurrentlyLoadingPurchasePlans = false;
    private Executor mExecutor;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public boolean mHintShown = false;
    private Application mLastGameClicked = null;
    /* access modifiers changed from: private */
    public boolean mLoadedPurchasePlans = false;
    /* access modifiers changed from: private */
    public IReadableAppRepository mLocalRepo;
    /* access modifiers changed from: private */
    public int mPurchasePlanID = -1;
    private RemoteSetObserver mRemoteObserver = new RemoteSetObserver();
    /* access modifiers changed from: private */
    public IReadableAppRepository mRemoteRepo;
    private Application mUpdate;
    /* access modifiers changed from: private */
    public IUserMembership mUserMembership;
    /* access modifiers changed from: private */
    public ICatalogView mView;
    /* access modifiers changed from: private */
    public IWelcomeNotificationManager mWelcomeNotificationManager;
    private Eula m_eula;

    @Inject
    public CatalogController(IAppManager appManager, ISubscriptionManager subManager, INavigationManager navMgr, IAnalyticsManager analyticsMgr, IUserMembership userMembership, IConnectivityMonitor connectivityMonitor, IAutoUpdater autoUpdater, Eula eula, Executor executer, IWelcomeNotificationManager welcomeNotificationManager, Context context) throws IOException {
        super(navMgr);
        this.mAppMgr = appManager;
        this.mAnalyticsMgr = analyticsMgr;
        this.mUserMembership = userMembership;
        this.mConnectivityMonitor = connectivityMonitor;
        this.mAutoUpdater = autoUpdater;
        this.mContext = context;
        this.mWelcomeNotificationManager = welcomeNotificationManager;
        this.mExecutor = executer;
        this.m_eula = eula;
        this.m_eula.setListener(this);
        this.mRemoteRepo = this.mAppMgr.getRemoteRepository();
        this.mLocalRepo = this.mAppMgr.getLocalRepository();
        this.mRemoteRepo.registerObserver(this.mRemoteObserver);
        this.mLocalRepo.registerObserver(this.mRemoteObserver);
    }

    public void setView(ICatalogView view) {
        this.mView = view;
    }

    public int Count() {
        return this.mApps.size();
    }

    public void AppActivated(int index) throws IOException {
        if (index >= 0) {
            try {
                if (index < this.mApps.size()) {
                    AppPresentation app = this.mApps.get(index);
                    this.mLastGameClicked = getRealApplication(app);
                    Log.d(TAG, String.format("app activated with id %d, name %s", Long.valueOf(app.getID()), app.getName()));
                    if (this.mLastGameClicked.getThumbnail() == null) {
                        return;
                    }
                    if (!isLocked(this.mLastGameClicked)) {
                        switch (this.mLastGameClicked.getState()) {
                            case Remote:
                                if (this.mAppMgr.isInstalled(this.mLastGameClicked)) {
                                    this.mLastGameClicked = this.mAppMgr.updateAppState(this.mLastGameClicked);
                                    launchApp(this.mLastGameClicked);
                                    return;
                                }
                                this.mAnalyticsMgr.GameDownloadStart(this.mLastGameClicked);
                                downloadApplicationAndStart(this.mLastGameClicked);
                                return;
                            case Downloaded:
                                if (this.mAppMgr.isInstalled(this.mLastGameClicked)) {
                                    this.mLastGameClicked = this.mAppMgr.updateAppState(this.mLastGameClicked);
                                    launchApp(this.mLastGameClicked);
                                    return;
                                }
                                this.mAnalyticsMgr.GameInstallStart(this.mLastGameClicked);
                                this.mView.setStatusText((int) R.string.CC_Installing);
                                try {
                                    if (this.mLastGameClicked != null) {
                                        getNavigation().displayInstallationForm(this.mView, this.mLastGameClicked.getAPKPath(), Codes.REQUEST_CODE_INSTALLATION);
                                        return;
                                    }
                                    return;
                                } catch (Exception e) {
                                    Log.e(TAG, "Application activation failed - " + e.toString());
                                    return;
                                }
                            case Installed:
                                if (this.mAppMgr.isInstalled(this.mLastGameClicked)) {
                                    launchApp(this.mLastGameClicked);
                                    return;
                                }
                                this.mLastGameClicked = this.mAppMgr.updateAppState(this.mLastGameClicked);
                                Application remoteAppCopy = this.mRemoteRepo.get(this.mLastGameClicked.getPackage());
                                if (remoteAppCopy != null) {
                                    this.mLastGameClicked.setState(Application.State.Remote);
                                    this.mLastGameClicked.setAPKPath(remoteAppCopy.getAPKPath());
                                    this.mLastGameClicked.setID(remoteAppCopy.getID());
                                    downloadApplicationAndStart(this.mLastGameClicked);
                                    return;
                                }
                                return;
                            default:
                                Log.e(TAG, "application with unfamiliar state encountered");
                                return;
                        }
                    } else if (this.mUserMembership.getMembershipState().getMembershipStatus() == UserMembershipState.UserMembershipStatus.USER_MEMBERSHIP_SUSPENDED) {
                        this.mView.showMessage(this.mContext.getResources().getString(R.string.membership_dialog_title), this.mContext.getResources().getString(R.string.membership_suspended));
                    } else {
                        this.mView.displayRegistrationBox();
                    }
                }
            } catch (Exception e2) {
                Log.e(TAG, "An error occurred while launching the app- ", e2);
            }
        }
    }

    private void launchApp(Application app) {
        this.mAnalyticsMgr.GameLaunched(app);
        if (app != null) {
            try {
                Log.d(TAG, String.format("Launching app %s with id %d", app.getName(), Long.valueOf(app.getID())));
                Bundle b = new Bundle();
                b.putLong(LauncherController.APPID_KEY, app.getID());
                getNavigation().showView(this.mView, Codes.REQUEST_LAUNCH_APP, Views.LAUNCHER_VIEW, b);
                this.mView.close();
            } catch (Exception e) {
                Log.e(TAG, "Application launch failed - " + e.toString());
            }
        }
    }

    public void onRegistrationSuccess() {
        resetAppsAvailabilityState();
        this.mAnalyticsMgr.SubscriptionActivated(this.mLastGameClicked, true);
        if (this.mAppMgr.isInstalled(this.mLastGameClicked)) {
            try {
                this.mLastGameClicked = this.mAppMgr.updateAppState(this.mLastGameClicked);
            } catch (IOException e) {
                Log.e(TAG, "Could not update application state after registration accepted");
            }
            launchApp(this.mLastGameClicked);
        } else if (this.mLastGameClicked.getState() == Application.State.Remote) {
            this.mAnalyticsMgr.GameDownloadStart(this.mLastGameClicked);
            downloadApplicationAndStart(this.mLastGameClicked);
        } else if (this.mLastGameClicked.getState() == Application.State.Installed) {
            launchApp(this.mLastGameClicked);
        } else if (this.mLastGameClicked.getState() == Application.State.Downloaded) {
            this.mAnalyticsMgr.GameInstallStart(this.mLastGameClicked);
            this.mView.setStatusText((int) R.string.CC_Installing);
            try {
                if (this.mLastGameClicked != null) {
                    getNavigation().displayInstallationForm(this.mView, this.mLastGameClicked.getAPKPath(), Codes.REQUEST_CODE_INSTALLATION);
                }
            } catch (Exception e2) {
                Log.e(TAG, "Application activation failed - " + e2.toString());
            }
        }
    }

    public void onUserDeclinedRegistration() {
        this.mAnalyticsMgr.SubscriptionActivated(this.mLastGameClicked, false);
        this.mLastGameClicked = null;
    }

    public void onInstallationResult(int resultCode) {
        if (this.mLastGameClicked != null) {
            try {
                this.mAppMgr.updateAppState(this.mLastGameClicked);
            } catch (Exception e) {
                Log.e(TAG, "Could not update app state - " + e.toString());
            }
        } else {
            this.mView.setStatusText((int) R.string.CC_InstallationCanceled);
        }
    }

    public void onDownloadResult(int resultCode) {
        if (resultCode == 1) {
            if (this.mLastGameClicked != null) {
                try {
                    this.mView.setStatusText(this.mContext.getString(R.string.game_download_success));
                    this.mLastGameClicked = this.mLocalRepo.get(this.mLastGameClicked.getPackage());
                    this.mAnalyticsMgr.GameInstallStart(this.mLastGameClicked);
                    getNavigation().displayInstallationForm(this.mView, this.mLastGameClicked.getAPKPath(), Codes.REQUEST_CODE_INSTALLATION);
                } catch (Exception e) {
                    e.printStackTrace();
                    this.mView.setStatusText(this.mContext.getString(R.string.game_download_failed_short));
                }
            }
        } else if (resultCode == 2) {
            this.mView.setStatusText(this.mContext.getString(R.string.game_download_canceled));
        } else {
            this.mView.setStatusText(this.mContext.getString(R.string.game_download_failed_short));
        }
    }

    public void init() {
        if (!this.doOnce) {
            this.doOnce = true;
            this.mAnalyticsMgr.ConsoleActivated();
            this.mExecutor.execute(new Runnable() {
                public void run() {
                    CatalogController.this.mWelcomeNotificationManager.checkForNewNotification();
                    if (CatalogController.this.mWelcomeNotificationManager.isNewNotificationAvailable()) {
                        CatalogController.this.mView.showMessage(CatalogController.this.mContext.getString(R.string.welcome_caption), CatalogController.this.mWelcomeNotificationManager.getMessage());
                        CatalogController.this.mWelcomeNotificationManager.deprecateMessage();
                    }
                }
            });
            this.mAppMgr.cleanup();
            onRefresh(true);
        }
    }

    /* access modifiers changed from: private */
    public void initLocalApps() {
        try {
            if (this.mApps.size() > 0) {
                this.mApps.clear();
            }
            List<Application> list = this.mLocalRepo.getAll();
            int size = list.size();
            Collection<AppPresentation> addedApps = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                AppPresentation app = getAppPresentation((Application) list.get(i), true);
                addedApps.add(app);
                this.mApps.add(app);
            }
            final ListChangedEventArgs args = new ListChangedEventArgs(ListChangedEventArgs.Action.RangeAdd, 0, addedApps.size(), addedApps);
            this.mHandler.post(new Runnable() {
                public void run() {
                    CatalogController.this.notifyObservers(args);
                }
            });
        } catch (IOException e) {
            IOException e2 = e;
            e2.printStackTrace();
            Log.e(TAG, "Could not initialize local repository", e2);
        }
    }

    public void onEulaAgreedTo() {
        Log.i(TAG, "eula accepted");
    }

    public void onEulaDeclinedTo() {
        Log.i(TAG, "eula refused");
        this.mView.close();
    }

    public void onShowPurchase() {
        long totalTime = 0;
        if (this.mCurrentlyLoadingPurchasePlans || this.mLoadedPurchasePlans) {
            while (this.mCurrentlyLoadingPurchasePlans && !this.mLoadedPurchasePlans && totalTime < PURCHASE_CHECK_TIMEOUT) {
                try {
                    Thread.sleep(SLEEP_TIME_BETWEEN_PURCHASE_CHECKS);
                    totalTime += SLEEP_TIME_BETWEEN_PURCHASE_CHECKS;
                } catch (InterruptedException e) {
                }
            }
            if (!this.mLoadedPurchasePlans) {
                this.mView.showMembershipMessage(this.mContext.getResources().getString(R.string.purchase_page_plans_not_loaded));
                return;
            }
            Bundle b = new Bundle();
            this.mPurchasePlanID = 0;
            b.putInt(DownloadController.DOWNLOAD_ID_KEY, this.mPurchasePlanID);
            getNavigation().showView(this.mView, Codes.REQUEST_CODE_PURCHASE_WINDOW, Views.PURCHASE_VIEW, b);
            return;
        }
        this.mView.showMembershipMessage(this.mContext.getResources().getString(R.string.purchase_page_plans_not_loaded));
    }

    public void onPurchasePlan(int code) {
        if (code == 1) {
            onUserAcceptRegistration();
        } else {
            onUserDeclinedRegistration();
        }
    }

    private void onUserAcceptRegistration() {
        this.mExecutor.execute(new Runnable() {
            public void run() {
                CatalogController.this.mView.onPurchasePlanStart();
                try {
                    UserMembershipState result = CatalogController.this.mUserMembership.purchasePlan(CatalogController.this.mUserMembership.getAvailablePlans().get(CatalogController.this.mPurchasePlanID).Id);
                    String message = CatalogController.this.billingStateToText(result.getBillingState());
                    String title = CatalogController.this.mContext.getResources().getString(R.string.membership_dialog_title);
                    final boolean unlock = result.hasAccess();
                    CatalogController.this.mView.showMessage(title, message, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (unlock) {
                                CatalogController.this.onRegistrationSuccess();
                            }
                        }
                    });
                } catch (Exception e) {
                    Log.e(CatalogController.TAG, "An error raised while completing purcahse", e);
                    CatalogController.this.mView.showMembershipMessage(CatalogController.this.mContext.getResources().getString(R.string.purchase_page_registration_failed));
                } finally {
                    CatalogController.this.mView.onPurchasePlanComplete();
                }
            }
        });
    }

    public void refreshPurchasePlans() {
        this.mExecutor.execute(new Runnable() {
            public void run() {
                LocaleInfo li = new LocaleInfo();
                li.Locale = Locale.getDefault().getLanguage();
                try {
                    boolean unused = CatalogController.this.mCurrentlyLoadingPurchasePlans = true;
                    boolean unused2 = CatalogController.this.mLoadedPurchasePlans = false;
                    CatalogController.this.mUserMembership.loadAvailablePlans(0, li);
                    if (CatalogController.this.mUserMembership.getAvailablePlans().size() > 0) {
                        boolean unused3 = CatalogController.this.mCurrentlyLoadingPurchasePlans = false;
                        boolean unused4 = CatalogController.this.mLoadedPurchasePlans = true;
                    }
                } catch (Exception e) {
                    boolean unused5 = CatalogController.this.mCurrentlyLoadingPurchasePlans = false;
                }
            }
        });
    }

    public void autoUpdate() {
        new Thread(new Runnable() {
            public void run() {
                CatalogController.this.mAutoUpdater.autoUpdate(this);
            }
        }).start();
    }

    public void loginUserAndRefreshCatalog(final boolean onInit) {
        this.mExecutor.execute(new Runnable() {
            public void run() {
                CatalogController.this.mView.onRefreshCatalogStart();
                UserMembershipState status = CatalogController.this.mUserMembership.loginUser();
                CatalogController.this.mView.onRefreshCatalogComplete();
                String loginStatus = CatalogController.this.loginStatusToText(status.getMembershipStatus());
                if (loginStatus != null) {
                    CatalogController.this.mView.showMembershipMessage(loginStatus);
                }
                if (onInit) {
                    CatalogController.this.initLocalApps();
                    CatalogController.this.mAppMgr.resyncRemote();
                } else if (!CatalogController.this.mAppMgr.resyncRemote()) {
                    CatalogController.this.mView.onRefreshAlreadyExecuting();
                }
                boolean unused = CatalogController.this.mHintShown = false;
            }
        });
    }

    private Application getRealApplication(AppPresentation app) throws IOException {
        if (app.getState() != Application.State.Remote) {
            Log.d(TAG, "getRealApplication - getting app from the local repository");
            return (Application) this.mLocalRepo.get(app.getID());
        }
        Log.d(TAG, "getRealApplication - getting app from the remote repository");
        return (Application) this.mRemoteRepo.get(app.getID());
    }

    /* access modifiers changed from: private */
    public String billingStateToText(UserMembershipState.BillingResult status) {
        switch (AnonymousClass7.$SwitchMap$mominis$gameconsole$core$models$UserMembershipState$BillingResult[status.ordinal()]) {
            case 1:
                return this.mContext.getResources().getString(R.string.purchase_message_already_paid);
            case 2:
                return this.mContext.getResources().getString(R.string.purchase_complete_without_billing);
            case 3:
                return this.mContext.getResources().getString(R.string.purchase_page_registration_failed);
            case 4:
                return this.mContext.getResources().getString(R.string.purchase_complete_with_billing);
            case CustomVariable.MAX_CUSTOM_VARIABLES:
                return this.mContext.getResources().getString(R.string.purchase_complete_with_billing);
            default:
                return null;
        }
    }

    /* access modifiers changed from: private */
    public String loginStatusToText(UserMembershipState.UserMembershipStatus status) {
        String message = null;
        switch (AnonymousClass7.$SwitchMap$mominis$gameconsole$core$models$UserMembershipState$UserMembershipStatus[status.ordinal()]) {
            case 1:
                return this.mContext.getResources().getString(R.string.membership_login_failed);
            case 2:
                return this.mConnectivityMonitor.isRoaming().booleanValue() ? this.mContext.getResources().getString(R.string.membership_roaming_mode) : this.mContext.getResources().getString(R.string.membership_offline_mode);
            case 3:
                return this.mContext.getResources().getString(R.string.membership_one_day_before_suspension);
            case 4:
                return this.mContext.getResources().getString(R.string.membership_suspended);
            case CustomVariable.MAX_CUSTOM_VARIABLES:
                return null;
            case 6:
                if (!this.mConnectivityMonitor.isConnected().booleanValue()) {
                    message = this.mContext.getResources().getString(R.string.membership_offline_mode);
                }
                if (this.mConnectivityMonitor.isRoaming().booleanValue()) {
                    return this.mContext.getResources().getString(R.string.membership_roaming_mode);
                }
                return message;
            case 7:
                return this.mContext.getResources().getString(R.string.membership_logged_in_non_purchased);
            default:
                return null;
        }
    }

    private void downloadApplicationAndStart(Application lastGameClicked) {
        if (lastGameClicked == null) {
            return;
        }
        if (!this.mConnectivityMonitor.isConnected().booleanValue()) {
            this.mView.showDownloadFailedNoConnectivity();
            return;
        }
        this.mLastGameClicked = lastGameClicked;
        Bundle b = new Bundle();
        b.putLong(DownloadController.DOWNLOAD_ID_KEY, this.mLastGameClicked.getID());
        getNavigation().showView(this.mView, 100, Views.DOWNLOAD_VIEW, b);
    }

    private class RemoteSetObserver implements IObserver<ListChangedEventArgs> {
        private RemoteSetObserver() {
        }

        public /* bridge */ /* synthetic */ void onChanged(IObservable x0, EventArgs x1) {
            onChanged((IObservable<ListChangedEventArgs>) x0, (ListChangedEventArgs) x1);
        }

        public void onChanged(IObservable<ListChangedEventArgs> sender, ListChangedEventArgs eventArgs) {
            UIUpdater updater = new UIUpdater(eventArgs, sender == CatalogController.this.mRemoteRepo);
            ListChangedEventArgs unused = updater.mArgs = eventArgs;
            CatalogController.this.mHandler.post(updater);
        }
    }

    private class UIUpdater implements Runnable {
        /* access modifiers changed from: private */
        public ListChangedEventArgs mArgs;
        private boolean mRemote;
        private boolean showHint = false;

        public UIUpdater(ListChangedEventArgs args, boolean remote) {
            this.mArgs = args;
            this.mRemote = remote;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0104, code lost:
            r20 = mominis.gameconsole.controllers.CatalogController.access$1800(r0.this$0).size();
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r25 = this;
                r9 = 0
                java.lang.String r20 = "Catalog Controller"
                java.lang.String r21 = "UIUpdater: %s - %b - %d"
                r22 = 3
                r0 = r22
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x014b }
                r22 = r0
                r23 = 0
                r0 = r25
                mominis.common.mvc.ListChangedEventArgs r0 = r0.mArgs     // Catch:{ Exception -> 0x014b }
                r24 = r0
                mominis.common.mvc.ListChangedEventArgs$Action r24 = r24.getAction()     // Catch:{ Exception -> 0x014b }
                java.lang.String r24 = r24.toString()     // Catch:{ Exception -> 0x014b }
                r22[r23] = r24     // Catch:{ Exception -> 0x014b }
                r23 = 1
                r0 = r25
                boolean r0 = r0.mRemote     // Catch:{ Exception -> 0x014b }
                r24 = r0
                java.lang.Boolean r24 = java.lang.Boolean.valueOf(r24)     // Catch:{ Exception -> 0x014b }
                r22[r23] = r24     // Catch:{ Exception -> 0x014b }
                r23 = 2
                r0 = r25
                mominis.common.mvc.ListChangedEventArgs r0 = r0.mArgs     // Catch:{ Exception -> 0x014b }
                r24 = r0
                int r24 = r24.getIndex()     // Catch:{ Exception -> 0x014b }
                java.lang.Integer r24 = java.lang.Integer.valueOf(r24)     // Catch:{ Exception -> 0x014b }
                r22[r23] = r24     // Catch:{ Exception -> 0x014b }
                java.lang.String r21 = java.lang.String.format(r21, r22)     // Catch:{ Exception -> 0x014b }
                android.util.Log.d(r20, r21)     // Catch:{ Exception -> 0x014b }
                int[] r20 = mominis.gameconsole.controllers.CatalogController.AnonymousClass7.$SwitchMap$mominis$common$mvc$ListChangedEventArgs$Action     // Catch:{ Exception -> 0x014b }
                r0 = r25
                mominis.common.mvc.ListChangedEventArgs r0 = r0.mArgs     // Catch:{ Exception -> 0x014b }
                r21 = r0
                mominis.common.mvc.ListChangedEventArgs$Action r21 = r21.getAction()     // Catch:{ Exception -> 0x014b }
                int r21 = r21.ordinal()     // Catch:{ Exception -> 0x014b }
                r20 = r20[r21]     // Catch:{ Exception -> 0x014b }
                switch(r20) {
                    case 1: goto L_0x00ae;
                    case 2: goto L_0x0162;
                    case 3: goto L_0x01e2;
                    case 4: goto L_0x028e;
                    default: goto L_0x005b;
                }     // Catch:{ Exception -> 0x014b }
            L_0x005b:
                java.lang.String r20 = "Catalog Controller"
                java.lang.String r21 = "An unexpected action detected for %s repository"
                r22 = 1
                r0 = r22
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x014b }
                r22 = r0
                r23 = 0
                r0 = r25
                boolean r0 = r0.mRemote     // Catch:{ Exception -> 0x014b }
                r24 = r0
                if (r24 == 0) goto L_0x0391
                java.lang.String r24 = "remote"
            L_0x0073:
                r22[r23] = r24     // Catch:{ Exception -> 0x014b }
                java.lang.String r21 = java.lang.String.format(r21, r22)     // Catch:{ Exception -> 0x014b }
                android.util.Log.e(r20, r21)     // Catch:{ Exception -> 0x014b }
            L_0x007c:
                r0 = r25
                boolean r0 = r0.showHint
                r20 = r0
                if (r20 == 0) goto L_0x009c
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this
                r20 = r0
                mominis.gameconsole.views.ICatalogView r20 = r20.mView
                r20.showScrollingHint()
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this
                r20 = r0
                r21 = 1
                boolean unused = r20.mHintShown = r21
            L_0x009c:
                if (r9 == 0) goto L_0x00ad
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this
                r20 = r0
                r0 = r25
                mominis.common.mvc.ListChangedEventArgs r0 = r0.mArgs
                r21 = r0
                r20.notifyObservers(r21)
            L_0x00ad:
                return
            L_0x00ae:
                r0 = r25
                mominis.common.mvc.ListChangedEventArgs r0 = r0.mArgs     // Catch:{ Exception -> 0x014b }
                r20 = r0
                java.lang.Object r8 = r20.getFirstAffected()     // Catch:{ Exception -> 0x014b }
                java.util.Collection r8 = (java.util.Collection) r8     // Catch:{ Exception -> 0x014b }
                java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ Exception -> 0x014b }
                r6.<init>()     // Catch:{ Exception -> 0x014b }
                r12 = 2147483647(0x7fffffff, float:NaN)
                java.util.Iterator r15 = r8.iterator()     // Catch:{ Exception -> 0x014b }
            L_0x00c6:
                boolean r20 = r15.hasNext()     // Catch:{ Exception -> 0x014b }
                if (r20 == 0) goto L_0x00f8
                java.lang.Object r7 = r15.next()     // Catch:{ Exception -> 0x014b }
                mominis.gameconsole.core.models.Application r7 = (mominis.gameconsole.core.models.Application) r7     // Catch:{ Exception -> 0x014b }
                r0 = r25
                r1 = r7
                mominis.gameconsole.controllers.AppPresentation r5 = r0.storeNewApp(r1)     // Catch:{ Exception -> 0x014b }
                if (r5 == 0) goto L_0x00c6
                r6.add(r5)     // Catch:{ Exception -> 0x014b }
                r9 = 1
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this     // Catch:{ Exception -> 0x014b }
                r20 = r0
                java.util.List r20 = r20.mApps     // Catch:{ Exception -> 0x014b }
                r0 = r20
                r1 = r5
                int r20 = r0.indexOf(r1)     // Catch:{ Exception -> 0x014b }
                r0 = r12
                r1 = r20
                int r12 = java.lang.Math.min(r0, r1)     // Catch:{ Exception -> 0x014b }
                goto L_0x00c6
            L_0x00f8:
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this     // Catch:{ Exception -> 0x014b }
                r20 = r0
                boolean r20 = r20.mHintShown     // Catch:{ Exception -> 0x014b }
                if (r20 != 0) goto L_0x015f
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this     // Catch:{ Exception -> 0x014b }
                r20 = r0
                java.util.List r20 = r20.mApps     // Catch:{ Exception -> 0x014b }
                int r20 = r20.size()     // Catch:{ Exception -> 0x014b }
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this     // Catch:{ Exception -> 0x014b }
                r21 = r0
                mominis.gameconsole.views.ICatalogView r21 = r21.mView     // Catch:{ Exception -> 0x014b }
                int r21 = r21.getNumAppsInScreen()     // Catch:{ Exception -> 0x014b }
                r0 = r20
                r1 = r21
                if (r0 <= r1) goto L_0x015f
                r20 = 1
            L_0x0128:
                r0 = r20
                r1 = r25
                r1.showHint = r0     // Catch:{ Exception -> 0x014b }
                if (r9 == 0) goto L_0x007c
                mominis.common.mvc.ListChangedEventArgs r20 = new mominis.common.mvc.ListChangedEventArgs     // Catch:{ Exception -> 0x014b }
                mominis.common.mvc.ListChangedEventArgs$Action r21 = mominis.common.mvc.ListChangedEventArgs.Action.RangeAdd     // Catch:{ Exception -> 0x014b }
                int r22 = r6.size()     // Catch:{ Exception -> 0x014b }
                r0 = r20
                r1 = r21
                r2 = r12
                r3 = r22
                r4 = r6
                r0.<init>(r1, r2, r3, r4)     // Catch:{ Exception -> 0x014b }
                r0 = r20
                r1 = r25
                r1.mArgs = r0     // Catch:{ Exception -> 0x014b }
                goto L_0x007c
            L_0x014b:
                r20 = move-exception
                r11 = r20
                java.lang.String r20 = "Catalog Controller"
                java.lang.String r21 = "Could not read from local repository"
                r0 = r20
                r1 = r21
                r2 = r11
                android.util.Log.e(r0, r1, r2)
                r11.printStackTrace()
                goto L_0x007c
            L_0x015f:
                r20 = 0
                goto L_0x0128
            L_0x0162:
                r0 = r25
                mominis.common.mvc.ListChangedEventArgs r0 = r0.mArgs     // Catch:{ Exception -> 0x014b }
                r20 = r0
                java.lang.Object r7 = r20.getFirstAffected()     // Catch:{ Exception -> 0x014b }
                mominis.gameconsole.core.models.Application r7 = (mominis.gameconsole.core.models.Application) r7     // Catch:{ Exception -> 0x014b }
                r0 = r25
                r1 = r7
                mominis.gameconsole.controllers.AppPresentation r5 = r0.storeNewApp(r1)     // Catch:{ Exception -> 0x014b }
                if (r5 == 0) goto L_0x01da
                r20 = 1
                r9 = r20
            L_0x017b:
                if (r9 == 0) goto L_0x01a2
                mominis.common.mvc.ListChangedEventArgs r20 = new mominis.common.mvc.ListChangedEventArgs     // Catch:{ Exception -> 0x014b }
                mominis.common.mvc.ListChangedEventArgs$Action r21 = mominis.common.mvc.ListChangedEventArgs.Action.Add     // Catch:{ Exception -> 0x014b }
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this     // Catch:{ Exception -> 0x014b }
                r22 = r0
                java.util.List r22 = r22.mApps     // Catch:{ Exception -> 0x014b }
                r0 = r22
                r1 = r5
                int r22 = r0.indexOf(r1)     // Catch:{ Exception -> 0x014b }
                r0 = r20
                r1 = r21
                r2 = r22
                r3 = r5
                r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x014b }
                r0 = r20
                r1 = r25
                r1.mArgs = r0     // Catch:{ Exception -> 0x014b }
            L_0x01a2:
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this     // Catch:{ Exception -> 0x014b }
                r20 = r0
                boolean r20 = r20.mHintShown     // Catch:{ Exception -> 0x014b }
                if (r20 != 0) goto L_0x01df
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this     // Catch:{ Exception -> 0x014b }
                r20 = r0
                java.util.List r20 = r20.mApps     // Catch:{ Exception -> 0x014b }
                int r20 = r20.size()     // Catch:{ Exception -> 0x014b }
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this     // Catch:{ Exception -> 0x014b }
                r21 = r0
                mominis.gameconsole.views.ICatalogView r21 = r21.mView     // Catch:{ Exception -> 0x014b }
                int r21 = r21.getNumAppsInScreen()     // Catch:{ Exception -> 0x014b }
                r0 = r20
                r1 = r21
                if (r0 <= r1) goto L_0x01df
                r20 = 1
            L_0x01d2:
                r0 = r20
                r1 = r25
                r1.showHint = r0     // Catch:{ Exception -> 0x014b }
                goto L_0x007c
            L_0x01da:
                r20 = 0
                r9 = r20
                goto L_0x017b
            L_0x01df:
                r20 = 0
                goto L_0x01d2
            L_0x01e2:
                r0 = r25
                boolean r0 = r0.mRemote     // Catch:{ Exception -> 0x014b }
                r20 = r0
                if (r20 == 0) goto L_0x026b
                r14 = 0
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this     // Catch:{ Exception -> 0x014b }
                r20 = r0
                java.util.List r20 = r20.mApps     // Catch:{ Exception -> 0x014b }
                int r20 = r20.size()     // Catch:{ Exception -> 0x014b }
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this     // Catch:{ Exception -> 0x014b }
                r21 = r0
                mominis.gameconsole.core.repositories.IReadableAppRepository r21 = r21.mLocalRepo     // Catch:{ Exception -> 0x014b }
                int r21 = r21.size()     // Catch:{ Exception -> 0x014b }
                int r17 = r20 - r21
                java.util.ArrayList r18 = new java.util.ArrayList     // Catch:{ Exception -> 0x014b }
                r18.<init>()     // Catch:{ Exception -> 0x014b }
                r12 = 2147483647(0x7fffffff, float:NaN)
                r13 = 0
            L_0x0212:
                r0 = r13
                r1 = r17
                if (r0 >= r1) goto L_0x0248
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this     // Catch:{ Exception -> 0x014b }
                r20 = r0
                java.util.List r20 = r20.mApps     // Catch:{ Exception -> 0x014b }
                int r20 = r20.size()     // Catch:{ Exception -> 0x014b }
                r21 = 1
                int r14 = r20 - r21
                int r12 = java.lang.Math.min(r12, r14)     // Catch:{ Exception -> 0x014b }
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this     // Catch:{ Exception -> 0x014b }
                r20 = r0
                java.util.List r20 = r20.mApps     // Catch:{ Exception -> 0x014b }
                r0 = r20
                r1 = r14
                java.lang.Object r20 = r0.remove(r1)     // Catch:{ Exception -> 0x014b }
                r0 = r18
                r1 = r20
                r0.add(r1)     // Catch:{ Exception -> 0x014b }
                int r13 = r13 + 1
                goto L_0x0212
            L_0x0248:
                int r20 = r18.size()     // Catch:{ Exception -> 0x014b }
                if (r20 <= 0) goto L_0x007c
                r9 = 1
                mominis.common.mvc.ListChangedEventArgs r20 = new mominis.common.mvc.ListChangedEventArgs     // Catch:{ Exception -> 0x014b }
                mominis.common.mvc.ListChangedEventArgs$Action r21 = mominis.common.mvc.ListChangedEventArgs.Action.RangeRemove     // Catch:{ Exception -> 0x014b }
                int r22 = r18.size()     // Catch:{ Exception -> 0x014b }
                r0 = r20
                r1 = r21
                r2 = r14
                r3 = r22
                r4 = r18
                r0.<init>(r1, r2, r3, r4)     // Catch:{ Exception -> 0x014b }
                r0 = r20
                r1 = r25
                r1.mArgs = r0     // Catch:{ Exception -> 0x014b }
                goto L_0x007c
            L_0x026b:
                java.lang.String r20 = "Catalog Controller"
                java.lang.String r21 = "Local repository was cleared - an unexpected and unhandeled behavior - %s"
                r22 = 1
                r0 = r22
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x014b }
                r22 = r0
                r23 = 0
                r0 = r25
                mominis.common.mvc.ListChangedEventArgs r0 = r0.mArgs     // Catch:{ Exception -> 0x014b }
                r24 = r0
                mominis.common.mvc.ListChangedEventArgs$Action r24 = r24.getAction()     // Catch:{ Exception -> 0x014b }
                r22[r23] = r24     // Catch:{ Exception -> 0x014b }
                java.lang.String r21 = java.lang.String.format(r21, r22)     // Catch:{ Exception -> 0x014b }
                android.util.Log.e(r20, r21)     // Catch:{ Exception -> 0x014b }
                goto L_0x007c
            L_0x028e:
                r14 = -1
                r0 = r25
                mominis.common.mvc.ListChangedEventArgs r0 = r0.mArgs     // Catch:{ Exception -> 0x014b }
                r20 = r0
                java.lang.Object r19 = r20.getFirstAffected()     // Catch:{ Exception -> 0x014b }
                mominis.gameconsole.core.models.Application r19 = (mominis.gameconsole.core.models.Application) r19     // Catch:{ Exception -> 0x014b }
                r0 = r25
                boolean r0 = r0.mRemote     // Catch:{ Exception -> 0x014b }
                r20 = r0
                if (r20 == 0) goto L_0x0323
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this     // Catch:{ Exception -> 0x014b }
                r20 = r0
                mominis.gameconsole.core.repositories.IReadableAppRepository r20 = r20.mLocalRepo     // Catch:{ Exception -> 0x014b }
                int r13 = r20.size()     // Catch:{ Exception -> 0x014b }
            L_0x02b1:
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this     // Catch:{ Exception -> 0x014b }
                r20 = r0
                java.util.List r20 = r20.mApps     // Catch:{ Exception -> 0x014b }
                int r20 = r20.size()     // Catch:{ Exception -> 0x014b }
                r0 = r13
                r1 = r20
                if (r0 >= r1) goto L_0x02e6
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this     // Catch:{ Exception -> 0x014b }
                r20 = r0
                java.util.List r20 = r20.mApps     // Catch:{ Exception -> 0x014b }
                r0 = r20
                r1 = r13
                java.lang.Object r10 = r0.get(r1)     // Catch:{ Exception -> 0x014b }
                mominis.gameconsole.controllers.AppPresentation r10 = (mominis.gameconsole.controllers.AppPresentation) r10     // Catch:{ Exception -> 0x014b }
                java.lang.String r20 = r10.getPackageName()     // Catch:{ Exception -> 0x014b }
                java.lang.String r21 = r19.getPackage()     // Catch:{ Exception -> 0x014b }
                boolean r20 = r20.equals(r21)     // Catch:{ Exception -> 0x014b }
                if (r20 == 0) goto L_0x0320
                r14 = r13
            L_0x02e6:
                if (r14 < 0) goto L_0x02fb
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this     // Catch:{ Exception -> 0x014b }
                r20 = r0
                java.util.List r20 = r20.mApps     // Catch:{ Exception -> 0x014b }
                int r20 = r20.size()     // Catch:{ Exception -> 0x014b }
                r0 = r14
                r1 = r20
                if (r0 < r1) goto L_0x034b
            L_0x02fb:
                r0 = r25
                boolean r0 = r0.mRemote     // Catch:{ Exception -> 0x014b }
                r20 = r0
                if (r20 != 0) goto L_0x032e
                java.lang.String r20 = "Catalog Controller"
                java.lang.String r21 = "Invalid index for set operation: %d"
                r22 = 1
                r0 = r22
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x014b }
                r22 = r0
                r23 = 0
                java.lang.Integer r24 = java.lang.Integer.valueOf(r14)     // Catch:{ Exception -> 0x014b }
                r22[r23] = r24     // Catch:{ Exception -> 0x014b }
                java.lang.String r21 = java.lang.String.format(r21, r22)     // Catch:{ Exception -> 0x014b }
                android.util.Log.e(r20, r21)     // Catch:{ Exception -> 0x014b }
                goto L_0x007c
            L_0x0320:
                int r13 = r13 + 1
                goto L_0x02b1
            L_0x0323:
                r0 = r25
                mominis.common.mvc.ListChangedEventArgs r0 = r0.mArgs     // Catch:{ Exception -> 0x014b }
                r20 = r0
                int r14 = r20.getIndex()     // Catch:{ Exception -> 0x014b }
                goto L_0x02e6
            L_0x032e:
                java.lang.String r20 = "Catalog Controller"
                java.lang.String r21 = "remote application is probably contained in the local repo - %s"
                r22 = 1
                r0 = r22
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x014b }
                r22 = r0
                r23 = 0
                java.lang.String r24 = r19.getName()     // Catch:{ Exception -> 0x014b }
                r22[r23] = r24     // Catch:{ Exception -> 0x014b }
                java.lang.String r21 = java.lang.String.format(r21, r22)     // Catch:{ Exception -> 0x014b }
                android.util.Log.d(r20, r21)     // Catch:{ Exception -> 0x014b }
                goto L_0x007c
            L_0x034b:
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this     // Catch:{ Exception -> 0x014b }
                r20 = r0
                r0 = r25
                boolean r0 = r0.mRemote     // Catch:{ Exception -> 0x014b }
                r21 = r0
                if (r21 != 0) goto L_0x038e
                r21 = 1
            L_0x035b:
                r0 = r20
                r1 = r19
                r2 = r21
                mominis.gameconsole.controllers.AppPresentation r16 = r0.getAppPresentation(r1, r2)     // Catch:{ Exception -> 0x014b }
                r0 = r25
                mominis.gameconsole.controllers.CatalogController r0 = mominis.gameconsole.controllers.CatalogController.this     // Catch:{ Exception -> 0x014b }
                r20 = r0
                java.util.List r20 = r20.mApps     // Catch:{ Exception -> 0x014b }
                r0 = r20
                r1 = r14
                r2 = r16
                r0.set(r1, r2)     // Catch:{ Exception -> 0x014b }
                mominis.common.mvc.ListChangedEventArgs r20 = new mominis.common.mvc.ListChangedEventArgs     // Catch:{ Exception -> 0x014b }
                mominis.common.mvc.ListChangedEventArgs$Action r21 = mominis.common.mvc.ListChangedEventArgs.Action.Set     // Catch:{ Exception -> 0x014b }
                r0 = r20
                r1 = r21
                r2 = r14
                r3 = r16
                r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x014b }
                r0 = r20
                r1 = r25
                r1.mArgs = r0     // Catch:{ Exception -> 0x014b }
                r9 = 1
                goto L_0x007c
            L_0x038e:
                r21 = 0
                goto L_0x035b
            L_0x0391:
                java.lang.String r24 = "local"
                goto L_0x0073
            */
            throw new UnsupportedOperationException("Method not decompiled: mominis.gameconsole.controllers.CatalogController.UIUpdater.run():void");
        }

        private AppPresentation storeNewApp(Application app) {
            boolean z;
            boolean z2;
            try {
                List<AppPresentation> catalogApps = CatalogController.this.mApps;
                if (this.mRemote) {
                    boolean contained = false;
                    int repoSize = CatalogController.this.mLocalRepo.size();
                    int i = 0;
                    while (true) {
                        if (i >= repoSize) {
                            break;
                        } else if (((AppPresentation) catalogApps.get(i)).getPackageName().equals(app.getPackage())) {
                            contained = true;
                            break;
                        } else {
                            i++;
                        }
                    }
                    if (contained) {
                        return null;
                    }
                    CatalogController catalogController = CatalogController.this;
                    if (!this.mRemote) {
                        z2 = true;
                    } else {
                        z2 = false;
                    }
                    AppPresentation rv = catalogController.getAppPresentation(app, z2);
                    CatalogController.this.mApps.add(rv);
                    return rv;
                }
                int index = -1;
                int i2 = CatalogController.this.mLocalRepo.size() - 1;
                while (true) {
                    if (i2 >= catalogApps.size()) {
                        break;
                    } else if (((AppPresentation) catalogApps.get(i2)).getPackageName().equals(app.getPackage())) {
                        index = i2;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (index != -1) {
                    catalogApps.remove(index);
                }
                CatalogController catalogController2 = CatalogController.this;
                if (!this.mRemote) {
                    z = true;
                } else {
                    z = false;
                }
                AppPresentation rv2 = catalogController2.getAppPresentation(app, z);
                catalogApps.add(CatalogController.this.mLocalRepo.size() - 1, rv2);
                return rv2;
            } catch (Exception e) {
                Exception e2 = e;
                Log.e(CatalogController.TAG, "Could not read from local repository", e2);
                e2.printStackTrace();
                return null;
            }
        }
    }

    /* renamed from: mominis.gameconsole.controllers.CatalogController$7  reason: invalid class name */
    static /* synthetic */ class AnonymousClass7 {
        static final /* synthetic */ int[] $SwitchMap$mominis$gameconsole$core$models$UserMembershipState$BillingResult = new int[UserMembershipState.BillingResult.values().length];
        static final /* synthetic */ int[] $SwitchMap$mominis$gameconsole$core$models$UserMembershipState$UserMembershipStatus = new int[UserMembershipState.UserMembershipStatus.values().length];

        static {
            $SwitchMap$mominis$common$mvc$ListChangedEventArgs$Action = new int[ListChangedEventArgs.Action.values().length];
            try {
                $SwitchMap$mominis$common$mvc$ListChangedEventArgs$Action[ListChangedEventArgs.Action.RangeAdd.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$mominis$common$mvc$ListChangedEventArgs$Action[ListChangedEventArgs.Action.Add.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$mominis$common$mvc$ListChangedEventArgs$Action[ListChangedEventArgs.Action.Clear.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$mominis$common$mvc$ListChangedEventArgs$Action[ListChangedEventArgs.Action.Set.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$mominis$gameconsole$core$models$UserMembershipState$UserMembershipStatus[UserMembershipState.UserMembershipStatus.USER_INVALID_CREDENTIALS.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$mominis$gameconsole$core$models$UserMembershipState$UserMembershipStatus[UserMembershipState.UserMembershipStatus.USER_OFFLINE_REGISTERED.ordinal()] = 2;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$mominis$gameconsole$core$models$UserMembershipState$UserMembershipStatus[UserMembershipState.UserMembershipStatus.USER_OFFFLINE_IN_REGISTERED_BEFORE_SUSPENSION.ordinal()] = 3;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$mominis$gameconsole$core$models$UserMembershipState$UserMembershipStatus[UserMembershipState.UserMembershipStatus.USER_MEMBERSHIP_SUSPENDED.ordinal()] = 4;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$mominis$gameconsole$core$models$UserMembershipState$UserMembershipStatus[UserMembershipState.UserMembershipStatus.USER_LOGGED_IN_REGISTERED.ordinal()] = 5;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$mominis$gameconsole$core$models$UserMembershipState$UserMembershipStatus[UserMembershipState.UserMembershipStatus.USER_OFFLINE.ordinal()] = 6;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$mominis$gameconsole$core$models$UserMembershipState$UserMembershipStatus[UserMembershipState.UserMembershipStatus.USER_LOGGED_IN_NOT_REGISTERED.ordinal()] = 7;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$mominis$gameconsole$core$models$UserMembershipState$BillingResult[UserMembershipState.BillingResult.ALREADY_REGISTERED.ordinal()] = 1;
            } catch (NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$mominis$gameconsole$core$models$UserMembershipState$BillingResult[UserMembershipState.BillingResult.BILLING_NOT_SUPPORTED.ordinal()] = 2;
            } catch (NoSuchFieldError e13) {
            }
            try {
                $SwitchMap$mominis$gameconsole$core$models$UserMembershipState$BillingResult[UserMembershipState.BillingResult.ERROR.ordinal()] = 3;
            } catch (NoSuchFieldError e14) {
            }
            try {
                $SwitchMap$mominis$gameconsole$core$models$UserMembershipState$BillingResult[UserMembershipState.BillingResult.SUCCESS.ordinal()] = 4;
            } catch (NoSuchFieldError e15) {
            }
            try {
                $SwitchMap$mominis$gameconsole$core$models$UserMembershipState$BillingResult[UserMembershipState.BillingResult.UA_PENDING.ordinal()] = 5;
            } catch (NoSuchFieldError e16) {
            }
            $SwitchMap$mominis$gameconsole$core$models$Application$State = new int[Application.State.values().length];
            try {
                $SwitchMap$mominis$gameconsole$core$models$Application$State[Application.State.Remote.ordinal()] = 1;
            } catch (NoSuchFieldError e17) {
            }
            try {
                $SwitchMap$mominis$gameconsole$core$models$Application$State[Application.State.Downloaded.ordinal()] = 2;
            } catch (NoSuchFieldError e18) {
            }
            try {
                $SwitchMap$mominis$gameconsole$core$models$Application$State[Application.State.Installed.ordinal()] = 3;
            } catch (NoSuchFieldError e19) {
            }
        }
    }

    private void resetAppsAvailabilityState() {
        int i = 0;
        while (i < this.mApps.size()) {
            try {
                AppPresentation currentPresentation = this.mApps.get(i);
                Application application = getRealApplication(currentPresentation);
                if (application == null) {
                    Log.e(TAG, "Application should not be null");
                } else {
                    currentPresentation.setState(isLocked(application) ? AppPresentation.Availability.Locked : AppPresentation.Availability.Available);
                }
                i++;
            } catch (IOException e) {
                Log.e(TAG, "an unexpected exception occurred while setting app lock state");
                return;
            }
        }
        notifyObservers(EventArgs.Empty);
    }

    /* access modifiers changed from: private */
    public AppPresentation getAppPresentation(Application app, boolean localApp) {
        AppPresentation rv = AppPresentation.FromApplication(app, isLocked(app) ? AppPresentation.Availability.Locked : AppPresentation.Availability.Available, app.getState());
        if (localApp) {
            rv.setThumbnailFirstLoaded(false);
        }
        return rv;
    }

    private boolean isLocked(Application app) {
        return !this.mUserMembership.getMembershipState().hasAccess() && !app.isFree();
    }

    public void onRefresh(boolean onInit) {
        loginUserAndRefreshCatalog(onInit);
        refreshPurchasePlans();
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public AppPresentation getApp(int position) {
        if (position < 0 || position >= this.mApps.size()) {
            return null;
        }
        return this.mApps.get(position);
    }

    public void OnProgress(Object sender, AppManagerProgressEventArgs args) {
    }

    public void onStart(Object sender, AppManagerProgressEventArgs args) {
    }

    public void onEnd(Object sender, AppManagerProgressEventArgs args) {
        if (args.getProgress() == 100) {
            this.mUpdate = args.getApp();
            Log.d(TAG, "Update download has ended - showing install message dialog");
            this.mView.showInstallUpdateMessage();
        }
    }

    public void onInstallUpdate() {
        getNavigation().displayInstallationForm(this.mView, this.mUpdate.getAPKPath(), Codes.REQUEST_CODE_UPDATE);
    }

    public void onExit() {
        Log.d(TAG, "unregistering remote and local observers");
        this.mRemoteRepo.unregisterObserver(this.mRemoteObserver);
        this.mLocalRepo.unregisterObserver(this.mRemoteObserver);
        Log.d(TAG, "onExit - freeing soft references");
        VMRuntime.getRuntime().gcSoftReferences();
    }

    public void onResume() {
        Log.d(TAG, "on resume - freeing soft references");
        VMRuntime.getRuntime().gcSoftReferences();
        autoUpdate();
    }

    public void onPause() {
        Log.d(TAG, "onPause - freeing soft references");
        VMRuntime.getRuntime().gcSoftReferences();
    }

    public void onEula() {
        getNavigation().showView(this.mView, 0, Views.EULA_VIEW);
    }
}
