package mominis.gameconsole.controllers;

public class Codes {
    public static final int REQUEST_CODE_INSTALLATION = 101;
    public static final int REQUEST_CODE_PURCHASE_WINDOW = 102;
    public static final int REQUEST_CODE_START_DOWNLOAD = 100;
    public static final int REQUEST_CODE_UPDATE = 103;
    public static final int REQUEST_LAUNCH_APP = 104;
    public static final int RESULT_CANCELED = 2;
    public static final int RESULT_ERROR = 3;
    public static final int RESULT_SUCCESS = 1;
}
