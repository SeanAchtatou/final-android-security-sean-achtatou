package mominis.gameconsole.controllers;

import android.util.Log;
import com.google.inject.Inject;
import mominis.common.mvc.BaseController;
import mominis.common.mvc.INavigationManager;
import mominis.gameconsole.core.models.Price;
import mominis.gameconsole.core.models.PurchasePlan;
import mominis.gameconsole.services.IUserMembership;
import mominis.gameconsole.views.IPurchaseView;

public class PurchaseController extends BaseController {
    public static final String PURCHASE_PLAN_ID_KEY = "PURCHASE_PLAN_ID";
    private static final String TAG = "Purchase Controller";
    private IUserMembership mUserMembership;
    private IPurchaseView mView;

    @Inject
    public PurchaseController(INavigationManager navMgr, IUserMembership userMembership) {
        super(navMgr);
        this.mUserMembership = userMembership;
    }

    public void setView(IPurchaseView view) {
        this.mView = view;
    }

    public void init() {
        PurchasePlan activePlan = this.mUserMembership.getAvailablePlans().get(getNavigation().getLaunchData(this.mView).getInt(PURCHASE_PLAN_ID_KEY));
        Price price = activePlan.PlanPrice;
        this.mView.setPrice(price.Symbol, price.Value, price.CurrencyCode);
        this.mView.setNotes(activePlan.PlanNotes);
        Log.i(TAG, "Purchase catalog initialized successfully");
    }
}
