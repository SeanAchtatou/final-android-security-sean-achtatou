package mominis.gameconsole.controllers;

public interface IEulaListener {
    void onEulaAgreedTo();

    void onEulaDeclinedTo();
}
