package mominis.gameconsole.controllers;

import android.content.Context;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import com.google.inject.Inject;
import java.io.IOException;
import mominis.common.mvc.BaseController;
import mominis.common.mvc.INavigationManager;
import mominis.gameconsole.common.IProcessListener;
import mominis.gameconsole.core.models.Application;
import mominis.gameconsole.services.AppManagerProgressEventArgs;
import mominis.gameconsole.services.IAppManager;
import mominis.gameconsole.services.IProcess;
import mominis.gameconsole.views.IDownloadView;

public class DownloadController extends BaseController implements IProcessListener<AppManagerProgressEventArgs> {
    public static final String DOWNLOAD_ID_KEY = "DOWNLOAD_KEY";
    private static final String TAG = "Download Controller";
    private Application mApp;
    private IAppManager mAppMgr;
    private boolean mCanceled = false;
    private boolean mCompleted = false;
    private IProcess mProcess;
    private IDownloadView mView;
    private Context m_context;
    private PowerManager.WakeLock m_wakeLock;

    @Inject
    public DownloadController(Context context, IAppManager remoteMgr, INavigationManager navMgr) {
        super(navMgr);
        this.mAppMgr = remoteMgr;
        this.m_context = context;
    }

    public void setView(IDownloadView view) {
        this.mView = view;
    }

    public void OnInit() {
        this.mCompleted = false;
        this.m_wakeLock = ((PowerManager) this.m_context.getSystemService("power")).newWakeLock(536870918, TAG);
        this.m_wakeLock.acquire();
        Bundle b = getNavigation().getLaunchData(this.mView);
        if (b != null) {
            try {
                this.mApp = (Application) this.mAppMgr.getRemoteRepository().get(b.getLong(DOWNLOAD_ID_KEY));
                this.mProcess = this.mAppMgr.DownloadApp(this.mApp, this);
            } catch (IOException e) {
                Log.e(TAG, "could not get application from remote repository - " + e.toString());
            }
        }
    }

    public void cancelClicked() {
        if (!this.mCompleted) {
            this.mCanceled = true;
            onBeforeClose();
            if (this.mProcess != null) {
                this.mProcess.stop();
            }
        }
    }

    public void OnProgress(Object sender, AppManagerProgressEventArgs args) {
        this.mView.showProgress(args.getProgress(), args.getTotalSize() / 1048576.0f);
    }

    public void onStart(Object sender, AppManagerProgressEventArgs args) {
        this.mView.showProgress(args.getProgress(), args.getTotalSize() / 1048576.0f);
    }

    public void onEnd(Object sender, AppManagerProgressEventArgs args) {
        if (!this.mCanceled) {
            this.mView.showProgress(args.getProgress(), args.getTotalSize() / 1048576.0f);
        }
        onBeforeClose();
        if (args.getProgress() == 100) {
            this.mCompleted = true;
            this.mView.closeWithResult(1);
        } else if (this.mCanceled) {
            this.mView.closeWithResult(2);
        } else {
            this.mView.closeWithResult(3);
        }
    }

    private void onBeforeClose() {
        if (this.m_wakeLock != null && this.m_wakeLock.isHeld()) {
            this.m_wakeLock.release();
            this.m_wakeLock = null;
        }
    }
}
