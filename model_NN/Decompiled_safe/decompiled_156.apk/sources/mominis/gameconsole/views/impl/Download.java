package mominis.gameconsole.views.impl;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.inject.Inject;
import mominis.gameconsole.com.R;
import mominis.gameconsole.controllers.DownloadController;
import mominis.gameconsole.views.IDownloadView;
import roboguice.inject.InjectView;

public class Download extends BaseActivity implements IDownloadView, View.OnClickListener {
    private static final String TAG = "Download";
    @InjectView(R.id.btnCancel)
    private ImageButton mCancelBtn;
    @Inject
    private DownloadController mController;
    @InjectView(R.id.download_loading)
    private ImageView mDownloadLoading;
    private final Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    @InjectView(R.id.MBLeft)
    public TextView mMBLef;
    /* access modifiers changed from: private */
    public int mPercentDone = 0;
    Animation mPolyAnimation;
    /* access modifiers changed from: private */
    @InjectView(R.id.ex_progress_bar)
    public ProgressBar mProgressBar;
    final Runnable mUpdateResults = new Runnable() {
        public void run() {
            Download.this.mProgressBar.setProgress(Download.this.mPercentDone);
            Download.this.mMBLef.setText(String.format("%d", Integer.valueOf(Download.this.mPercentDone)) + "%");
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.download);
        this.mCancelBtn.setOnClickListener(this);
        this.mController.setView(this);
        this.mController.OnInit();
        this.mPolyAnimation = AnimationUtils.loadAnimation(this, R.anim.rotate_animation_infinite);
        this.mDownloadLoading.startAnimation(this.mPolyAnimation);
        this.mProgressBar.setProgressDrawable(getResources().getDrawable(R.drawable.download_progress_bar));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Log.i(TAG, "Pause event received");
        this.mController.cancelClicked();
    }

    public void showProgress(int progress, float totalSize) {
        if (isDestroyed()) {
            Log.w(TAG, "showProgress called after catalog view has been destroyed - operation canceled");
            return;
        }
        this.mPercentDone = progress;
        this.mHandler.post(this.mUpdateResults);
    }

    public void onClick(View arg0) {
        this.mController.cancelClicked();
    }
}
