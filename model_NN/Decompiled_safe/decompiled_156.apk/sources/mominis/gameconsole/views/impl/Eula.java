package mominis.gameconsole.views.impl;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.google.inject.Inject;
import mominis.gameconsole.com.R;
import mominis.gameconsole.common.ResourceHelper;
import mominis.gameconsole.controllers.IEulaListener;

public class Eula {
    private static final String ASSET_EULA = "EULA";
    private static final String PREFERENCES_EULA = "eula";
    private static final String PREFERENCE_EULA_ACCEPTED = "eula.accepted";
    private Context mContext;
    private IEulaListener m_listener;

    @Inject
    public Eula(Context context) {
        this.mContext = context;
    }

    public void setListener(IEulaListener listener) {
        this.m_listener = listener;
    }

    public boolean show() {
        if (this.mContext.getSharedPreferences(PREFERENCES_EULA, 0).getBoolean(PREFERENCE_EULA_ACCEPTED, false)) {
            return true;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        builder.setTitle((int) R.string.eula_title);
        builder.setCancelable(true);
        builder.setPositiveButton((int) R.string.eula_accept, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Eula.this.accept();
            }
        });
        builder.setNegativeButton((int) R.string.eula_refuse, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Eula.this.refuse();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                Eula.this.refuse();
            }
        });
        builder.setMessage(readEula());
        builder.create().show();
        return false;
    }

    /* access modifiers changed from: private */
    public void accept() {
        this.mContext.getSharedPreferences(PREFERENCES_EULA, 0).edit().putBoolean(PREFERENCE_EULA_ACCEPTED, true).commit();
        if (this.m_listener != null) {
            this.m_listener.onEulaAgreedTo();
        }
    }

    /* access modifiers changed from: private */
    public void refuse() {
        if (this.m_listener != null) {
            this.m_listener.onEulaDeclinedTo();
        }
    }

    private CharSequence readEula() {
        return ResourceHelper.readAsset(this.mContext, ASSET_EULA);
    }
}
