package mominis.gameconsole.views.impl;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.google.inject.Inject;
import java.util.ArrayList;
import mominis.gameconsole.com.R;
import mominis.gameconsole.controllers.PurchaseController;
import mominis.gameconsole.views.IPurchaseView;
import roboguice.inject.InjectView;

public class Purchase extends BaseActivity implements IPurchaseView, View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    @InjectView(R.id.btnNo)
    private Button btnNo;
    @InjectView(R.id.btnSubscribe)
    private Button btnSubscribe;
    @InjectView(R.id.chkAgreeLicense)
    private CheckBox chkAgreeLicense;
    @Inject
    private PurchaseController mController;
    @InjectView(R.id.notesLayout)
    private TableLayout notesLayout;
    @InjectView(R.id.txtPurchasePrice)
    private TextView txtPurchasePrice;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.purchase);
        this.btnNo.setOnClickListener(this);
        this.btnSubscribe.setOnClickListener(this);
        this.chkAgreeLicense.setOnCheckedChangeListener(this);
        this.chkAgreeLicense.setText(Html.fromHtml(getResources().getString(R.string.purchase_page_checkbox).replace("http://", getPackageName() + "://")));
        this.chkAgreeLicense.setMovementMethod(LinkMovementMethod.getInstance());
        this.mController.setView(this);
        this.mController.init();
    }

    public void setNotes(ArrayList<String> notes) {
        for (int i = 0; i < notes.size(); i++) {
            TableRow currentRow = (TableRow) this.notesLayout.getChildAt(i);
            TextView secondColumn = (TextView) currentRow.getChildAt(1);
            ((TextView) currentRow.getChildAt(0)).setVisibility(0);
            secondColumn.setVisibility(0);
            String text = notes.get(i);
            if (text.toLowerCase().contains("href")) {
                text = text.replace("http://", getPackageName() + "://");
                secondColumn.setMovementMethod(LinkMovementMethod.getInstance());
            }
            secondColumn.setText(Html.fromHtml(text));
        }
    }

    public void setPrice(String symbol, double price, String currencyCode) {
        String priceText;
        if (Math.floor(price) == price) {
            priceText = String.format("%s%d %s", symbol, Integer.valueOf((int) price), currencyCode);
        } else {
            priceText = String.format("%s%.2f %s", symbol, Double.valueOf(price), currencyCode);
        }
        this.txtPurchasePrice.setText(priceText);
    }

    public void onClick(View view) {
        if (view == this.btnNo) {
            closeWithResult(2);
        } else if (view == this.btnSubscribe) {
            closeWithResult(1);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        this.btnSubscribe.setEnabled(isChecked);
    }
}
