package mominis.gameconsole.views.impl;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.google.inject.Inject;
import java.io.IOException;
import mominis.common.mvc.INavigationManager;
import mominis.gameconsole.com.R;
import mominis.gameconsole.services.impl.Bootstrapper;
import mominis.gameconsole.views.Views;
import roboguice.inject.InjectView;

public class Splash extends BaseActivity {
    private static final int ANIMATION_LENGTH = 3000;
    private static final String TAG = "Splash Screen";
    AnimationDrawable mAnimation;
    /* access modifiers changed from: private */
    @Inject
    public Bootstrapper mBootstrapper;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    @Inject
    public INavigationManager mNavigation;
    final Runnable mNext = new Runnable() {
        public void run() {
            Splash.this.mNavigation.showView(Splash.this, 0, Views.CATALOG_VIEW);
            Splash.this.close();
        }
    };
    @InjectView(R.id.PolyView)
    private ImageView mPoly;
    Animation mPolyAnimation;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView((int) R.layout.splash);
        } catch (Exception e) {
            Log.e(TAG, "could not set content", e);
        }
        this.mPolyAnimation = AnimationUtils.loadAnimation(this, R.anim.rotate_animation_infinite);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.mPoly.startAnimation(this.mPolyAnimation);
        new Thread(new Runnable() {
            public void run() {
                try {
                    long timeNano = System.nanoTime();
                    Splash.this.mBootstrapper.activate();
                    long timeElapsed = (System.nanoTime() - timeNano) / 1000000;
                    if (timeElapsed < 3000) {
                        Thread.sleep(3000 - timeElapsed);
                    }
                } catch (IOException e) {
                    IOException e2 = e;
                    e2.printStackTrace();
                    Log.e(Splash.TAG, "Bootstrap activate failed with exception", e2);
                } catch (InterruptedException e3) {
                    InterruptedException e4 = e3;
                    e4.printStackTrace();
                    Log.e(Splash.TAG, "Sleep failed with an unexpected exception", e4);
                }
                Splash.this.mHandler.post(Splash.this.mNext);
            }
        }).start();
    }
}
