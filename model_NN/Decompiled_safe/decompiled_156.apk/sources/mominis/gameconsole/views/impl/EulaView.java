package mominis.gameconsole.views.impl;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import mominis.gameconsole.com.R;
import mominis.gameconsole.common.ResourceHelper;

public class EulaView extends BaseActivity implements DialogInterface.OnCancelListener {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showDialog();
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.eula_title);
        builder.setPositiveButton((int) R.string.OK, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                EulaView.this.finish();
            }
        });
        builder.setOnCancelListener(this);
        builder.setMessage(ResourceHelper.readAsset(this, "EULA"));
        builder.create().show();
    }

    public void onCancel(DialogInterface sender) {
        finish();
    }
}
