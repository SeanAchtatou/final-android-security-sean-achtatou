package mominis.gameconsole.views.impl;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.google.inject.Inject;
import java.util.ArrayList;
import mominis.gameconsole.com.R;
import mominis.gameconsole.controllers.CatalogController;
import mominis.gameconsole.views.ICatalogView;
import roboguice.inject.InjectView;

public class Catalog extends BaseActivity implements ICatalogView {
    private static final int GESTIMATED_APPS_IN_SCREEN = 3;
    private static final String TAG = "CatalogView";
    /* access modifiers changed from: private */
    @Inject
    public Context mContext;
    /* access modifiers changed from: private */
    @Inject
    public CatalogController mController;
    @InjectView(R.id.gridview)
    private ConsoleGridView mGrid;
    /* access modifiers changed from: private */
    public ProgressDialog mPurchaseLoadingDialog;
    /* access modifiers changed from: private */
    public ProgressDialog mRefreshLoadingDialog;
    /* access modifiers changed from: private */
    @InjectView(R.id.scrolling_hint)
    public ImageView mScrollingHint;
    private Animation mScrollingHintAnimation;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate called - setting the content view");
        setContentView((int) R.layout.catalog);
        Display disp = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        Log.d(TAG, "initializing grid and controller");
        this.mController.setView(this);
        this.mGrid.setColumnWidth(disp.getWidth());
        this.mGrid.setController(this.mController);
        this.mController.init();
        this.mScrollingHintAnimation = AnimationUtils.loadAnimation(this, R.anim.scrolling_hint_animation);
        RelativeLayout.LayoutParams scrollingHintParams = new RelativeLayout.LayoutParams(disp.getWidth() / 3, disp.getHeight() / 3);
        scrollingHintParams.addRule(14, 1);
        this.mScrollingHint.setLayoutParams(scrollingHintParams);
        this.mScrollingHint.setVisibility(8);
        this.mScrollingHintAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation arg0) {
                Catalog.this.mScrollingHint.setVisibility(8);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
    }

    public void showScrollingHint() {
        if (isDestroyed()) {
            Log.w(TAG, "showScrollingHint called after catalog view has been destroyed - operation aborted");
            return;
        }
        this.mScrollingHint.setVisibility(0);
        this.mScrollingHint.startAnimation(this.mScrollingHintAnimation);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "destroying catalog view and cleaning resources");
        if (this.mGrid != null) {
            this.mGrid.release();
            this.mGrid = null;
        }
        if (this.mController != null) {
            this.mController.onExit();
            this.mController = null;
        }
    }

    public void onResume() {
        super.onResume();
        this.mController.onResume();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.catalog_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                this.mController.onRefresh(false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mController.onPause();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            this.mController.onDownloadResult(resultCode);
        } else if (requestCode == 101) {
            this.mController.onInstallationResult(resultCode);
        } else if (requestCode == 102) {
            this.mController.onPurchasePlan(resultCode);
        } else if (requestCode == 103 && resultCode == 0) {
            finish();
        }
    }

    public void onPurchasePlanStart() {
        if (isDestroyed()) {
            Log.w(TAG, "onPurchasePlanStart called after catalog view has been destroyed - operation aborted");
        } else {
            getHandler().post(new Runnable() {
                public void run() {
                    ProgressDialog unused = Catalog.this.mPurchaseLoadingDialog = ProgressDialog.show(Catalog.this.mContext, "", Catalog.this.getResources().getString(R.string.purchase_message_please_wait));
                }
            });
        }
    }

    public void onPurchasePlanComplete() {
        if (isDestroyed()) {
            Log.w(TAG, "onPurchasePlanComplete called after catalog view has been destroyed - operation aborted");
        } else {
            getHandler().post(new Runnable() {
                public void run() {
                    Catalog.this.mPurchaseLoadingDialog.dismiss();
                }
            });
        }
    }

    public void onRefreshAlreadyExecuting() {
        if (isDestroyed()) {
            Log.w(TAG, "onRefreshAlreadyExecuting called after catalog view has been destroyed - operation aborted");
        } else {
            getHandler().post(new Runnable() {
                public void run() {
                    Catalog.this.setStatusText((int) R.string.catalog_refresh_already_executing);
                }
            });
        }
    }

    public void onRefreshCatalogStart() {
        if (isDestroyed()) {
            Log.w(TAG, "onRefreshCatalogStart called after catalog view has been destroyed - operation aborted");
        } else {
            getHandler().post(new Runnable() {
                public void run() {
                    ProgressDialog unused = Catalog.this.mRefreshLoadingDialog = ProgressDialog.show(Catalog.this.mContext, "", Catalog.this.getResources().getString(R.string.refresh_dialog_text));
                }
            });
        }
    }

    public void onRefreshCatalogComplete() {
        if (isDestroyed()) {
            Log.w(TAG, "onRefreshCatalogComplete called after catalog view has been destroyed - operation aborted");
        } else {
            getHandler().post(new Runnable() {
                public void run() {
                    Catalog.this.mRefreshLoadingDialog.dismiss();
                }
            });
        }
    }

    public void showMembershipMessage(final String text) {
        if (isDestroyed()) {
            Log.w(TAG, "showMembershipMessage called after catalog view has been destroyed - operation aborted");
        } else {
            getHandler().post(new Runnable() {
                public void run() {
                    Catalog.this.showMessage(Catalog.this.getResources().getString(R.string.membership_dialog_title), text);
                }
            });
        }
    }

    public void showInstallUpdateMessage() {
        if (isDestroyed()) {
            Log.w(TAG, "shoWinstallUpdateMessage called after catalog view has been destroyed - operation canceled");
            return;
        }
        final DialogInterface.OnClickListener onUpdate = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                new Handler().post(new Runnable() {
                    public void run() {
                        Catalog.this.mController.onInstallUpdate();
                    }
                });
            }
        };
        final DialogInterface.OnClickListener onExit = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                new Handler().post(new Runnable() {
                    public void run() {
                        this.finish();
                    }
                });
            }
        };
        final ArrayList<String> buttonTexts = new ArrayList<>();
        buttonTexts.add(getResources().getString(R.string.update_message_button_update));
        buttonTexts.add(getResources().getString(R.string.update_message_button_exit));
        getHandler().post(new Runnable() {
            public void run() {
                Catalog.this.showCustomMessage(Catalog.this.getResources().getString(R.string.update_message_title), Catalog.this.getResources().getString(R.string.update_message_text), buttonTexts, onUpdate, onExit);
            }
        });
    }

    public void displayRegistrationBox() {
        if (isDestroyed()) {
            Log.w(TAG, "displayRegistrationBox called after catalog view has been destroyed - operation canceled");
        } else {
            getHandler().post(new Runnable() {
                public void run() {
                    Catalog.this.mController.onShowPurchase();
                }
            });
        }
    }

    public void showDownloadFailedNoConnectivity() {
        if (isDestroyed()) {
            Log.w(TAG, "showDownloadFailedNoConnectivity called after catalog view has been destroyed - operation canceled");
        } else {
            getHandler().post(new Runnable() {
                public void run() {
                    Catalog.this.showMessage(Catalog.this.getResources().getString(R.string.game_download_failed_title), Catalog.this.getResources().getString(R.string.game_download_failed_text));
                }
            });
        }
    }

    public int getNumAppsInScreen() {
        return 3;
    }
}
