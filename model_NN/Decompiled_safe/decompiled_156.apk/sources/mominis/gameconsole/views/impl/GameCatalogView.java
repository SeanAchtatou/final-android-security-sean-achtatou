package mominis.gameconsole.views.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import mominis.gameconsole.com.R;
import mominis.gameconsole.controllers.AppPresentation;
import mominis.gameconsole.views.impl.AppsAdapter;

public class GameCatalogView extends LinearLayout {
    private static final String TAG = "GameCatalogView";
    private AppPresentation mApp;
    private AppPresentation.Availability mAppAvailability = AppPresentation.Availability.Locked;
    private Context mContext;
    private int mFrameHeight;
    private int mFrameWidth;
    Animation mFreeButtonFadeInAnimation;
    Animation mFreeButtonRotateAnimation;
    ImageView mFreePlayIcon;
    RelativeLayout mFreePlayLayout;
    private ImageView mGameLoadingView;
    Animation mGameThumbnailFadeInAnimation;
    Animation mGameThumbnailFadeOutAnimation;
    ImageView mGameThumbnailView;
    private View mGameView;
    private AppsAdapter.IAppImageProvider mImageProvider;
    RelativeLayout mLoadingGameLayout;
    Animation mLoadingPolyAnimation;
    private TextView mLoadingTextView;
    /* access modifiers changed from: private */
    public RelativeLayout mLockedGameLayout;
    Animation mNewButtonFadeInAnimation;
    private ImageView mNewGameIcon;
    private ImageView mUnlockBtn;
    Animation mUnlockButtonFadeInAnimation;
    Animation mUnlockButtonRotateAnimation;
    private RelativeLayout mUnlockLayout;
    private GameCatalogState mViewState = GameCatalogState.Loading;

    private enum GameCatalogState {
        Loading,
        Showing
    }

    public GameCatalogView(Context context, int width, int height, AppsAdapter.IAppImageProvider imageProvider) {
        super(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        this.mImageProvider = imageProvider;
        this.mFrameWidth = width;
        this.mFrameHeight = height;
        this.mViewState = GameCatalogState.Loading;
        TableLayout layout = new TableLayout(context);
        this.mGameView = inflater.inflate((int) R.layout.catalog_game, layout);
        addView(layout, 0, new LinearLayout.LayoutParams(-1, -1));
        this.mContext = context;
        initGameView(this.mGameView);
        restoreDefaultState();
    }

    private void initGameView(View gameView) {
        this.mNewButtonFadeInAnimation = AnimationUtils.loadAnimation(this.mContext, R.anim.fade_in_animation);
        this.mFreeButtonFadeInAnimation = AnimationUtils.loadAnimation(this.mContext, R.anim.fade_in_animation);
        this.mFreeButtonRotateAnimation = AnimationUtils.loadAnimation(this.mContext, R.anim.rotate_animation_infinite);
        this.mUnlockButtonFadeInAnimation = AnimationUtils.loadAnimation(this.mContext, R.anim.fade_in_animation);
        this.mUnlockButtonRotateAnimation = AnimationUtils.loadAnimation(this.mContext, R.anim.rotate_animation_infinite);
        this.mLoadingPolyAnimation = AnimationUtils.loadAnimation(this.mContext, R.anim.rotate_animation_infinite);
        this.mGameThumbnailFadeInAnimation = AnimationUtils.loadAnimation(this.mContext, R.anim.fade_in_animation);
        this.mGameThumbnailFadeOutAnimation = AnimationUtils.loadAnimation(this.mContext, R.anim.fade_out_animation);
        this.mGameThumbnailView = (ImageView) gameView.findViewById(R.id.ThumbnailView);
        this.mLoadingGameLayout = (RelativeLayout) gameView.findViewById(R.id.loading_game_layout);
        this.mLockedGameLayout = (RelativeLayout) gameView.findViewById(R.id.BlurGameLayout);
        this.mUnlockBtn = (ImageView) gameView.findViewById(R.id.UnlockBtn);
        this.mNewGameIcon = (ImageView) gameView.findViewById(R.id.btnNew);
        this.mGameLoadingView = (ImageView) gameView.findViewById(R.id.GamePolyView);
        this.mLoadingTextView = (TextView) gameView.findViewById(R.id.txtLoading);
        this.mUnlockLayout = (RelativeLayout) gameView.findViewById(R.id.game_locked_layout);
        this.mFreePlayIcon = (ImageView) gameView.findViewById(R.id.btnFree);
        this.mFreePlayLayout = (RelativeLayout) gameView.findViewById(R.id.game_free_layout);
        initUnlockLayout(gameView);
        initLoadingAnim(gameView);
        initFreeLayout(gameView);
        this.mGameThumbnailView.bringToFront();
        this.mNewGameIcon.bringToFront();
        this.mFreePlayLayout.bringToFront();
        this.mLockedGameLayout.bringToFront();
    }

    private void restoreDefaultState() {
        Object[] objArr = new Object[1];
        objArr[0] = this.mApp != null ? this.mApp.getName() : "<empty>";
        Log.d(TAG, String.format("reset view of game %s", objArr));
        this.mFreeButtonFadeInAnimation.reset();
        this.mNewButtonFadeInAnimation.reset();
        this.mGameThumbnailFadeInAnimation.reset();
        this.mGameThumbnailFadeOutAnimation.reset();
        this.mLoadingGameLayout.clearAnimation();
        this.mGameThumbnailView.clearAnimation();
        this.mNewGameIcon.clearAnimation();
        this.mFreePlayLayout.clearAnimation();
        this.mLockedGameLayout.clearAnimation();
        this.mLoadingGameLayout.setVisibility(0);
        this.mFreePlayLayout.setVisibility(4);
        this.mGameThumbnailView.setVisibility(4);
        this.mGameLoadingView.setVisibility(0);
        this.mLoadingTextView.setVisibility(0);
        this.mLockedGameLayout.setVisibility(4);
        this.mNewGameIcon.setVisibility(4);
        this.mFreePlayIcon.startAnimation(this.mFreeButtonRotateAnimation);
        this.mGameLoadingView.startAnimation(this.mLoadingPolyAnimation);
    }

    public void resetState() {
        if (this.mApp != null) {
            refreshView();
        }
    }

    public AppPresentation getApp() {
        return this.mApp;
    }

    public void setApp(AppPresentation app, boolean forceRefresh) {
        if (forceRefresh || this.mApp != app || this.mAppAvailability != app.getAvailability() || this.mViewState == GameCatalogState.Loading || (this.mApp.getThumbnail() != null && this.mGameThumbnailView.getDrawable() == null)) {
            this.mApp = app;
            if (app != null) {
                this.mAppAvailability = app.getAvailability();
                if (this.mViewState == GameCatalogState.Loading && app.getThumbnail() == null) {
                    return;
                }
            }
            this.mViewState = GameCatalogState.Loading;
            refreshView();
        }
    }

    private void refreshView() {
        boolean z;
        boolean z2;
        boolean z3;
        Object[] objArr = new Object[3];
        objArr[0] = Integer.valueOf(hashCode());
        objArr[1] = true;
        objArr[2] = this.mApp != null ? this.mApp.getPackageName() : "<empty>";
        Log.d(TAG, String.format("refreshView: %X - %b - %s", objArr));
        restoreDefaultState();
        if (this.mApp != null) {
            try {
                Boolean fadeInIcons = false;
                Bitmap thumbnailImage = this.mImageProvider.getImage(this.mApp);
                this.mGameThumbnailView.setImageBitmap(thumbnailImage);
                if (thumbnailImage != null) {
                    this.mGameThumbnailView.setVisibility(0);
                    this.mViewState = GameCatalogState.Showing;
                    if (this.mApp.isThumbnailFirstLoaded()) {
                        showLoadingAnim(true);
                        this.mApp.setThumbnailFirstLoaded(false);
                        fadeInIcons = true;
                        Object[] objArr2 = new Object[1];
                        objArr2[0] = this.mApp != null ? this.mApp.getName() : "<empty>";
                        Log.d(TAG, String.format("starting incoming fade animation of app %s", objArr2));
                        this.mLoadingGameLayout.startAnimation(this.mGameThumbnailFadeOutAnimation);
                        this.mGameThumbnailView.startAnimation(this.mGameThumbnailFadeInAnimation);
                    } else {
                        showLoadingAnim(false);
                    }
                }
                View view = this.mGameView;
                if (this.mApp.getAvailability() != AppPresentation.Availability.Locked || thumbnailImage == null) {
                    z = false;
                } else {
                    z = true;
                }
                showUnlockLayout(view, z, fadeInIcons.booleanValue());
                View view2 = this.mGameView;
                if (!this.mApp.isFree() || thumbnailImage == null) {
                    z2 = false;
                } else {
                    z2 = true;
                }
                showFreeLayout(view2, z2, fadeInIcons.booleanValue());
                View view3 = this.mGameView;
                if (!this.mApp.isNew().booleanValue() || thumbnailImage == null) {
                    z3 = false;
                } else {
                    z3 = true;
                }
                showNewLayout(view3, z3, fadeInIcons.booleanValue());
            } catch (Exception e) {
                Exception e2 = e;
                e2.printStackTrace();
                Log.e(TAG, "Exception caught on getView", e2);
            }
        }
    }

    private void showLoadingAnim(boolean show) {
        int i;
        int i2;
        int i3;
        ImageView imageView = this.mGameLoadingView;
        if (show) {
            i = 0;
        } else {
            i = 4;
        }
        imageView.setVisibility(i);
        TextView textView = this.mLoadingTextView;
        if (show) {
            i2 = 0;
        } else {
            i2 = 4;
        }
        textView.setVisibility(i2);
        RelativeLayout relativeLayout = this.mLoadingGameLayout;
        if (show) {
            i3 = 0;
        } else {
            i3 = 4;
        }
        relativeLayout.setVisibility(i3);
    }

    private void showNewLayout(View gameView, boolean show, boolean fadeIn) {
        this.mNewGameIcon.setVisibility(show ? 0 : 4);
        if (fadeIn && show) {
            this.mNewGameIcon.startAnimation(this.mNewButtonFadeInAnimation);
        }
    }

    private void showFreeLayout(View gameView, boolean show, boolean fadeIn) {
        this.mFreePlayLayout.setVisibility(show ? 0 : 4);
        if (fadeIn && show) {
            this.mFreePlayLayout.startAnimation(this.mFreeButtonFadeInAnimation);
            this.mFreeButtonFadeInAnimation.setAnimationListener(new ShowFreePlayWhenEnd());
        }
    }

    private void showUnlockLayout(View gameView, boolean show, boolean fadeIn) {
        this.mLockedGameLayout.setVisibility(show ? 0 : 4);
        if (fadeIn && show) {
            this.mLockedGameLayout.startAnimation(this.mUnlockButtonFadeInAnimation);
            this.mUnlockButtonFadeInAnimation.setAnimationListener(new ShowLockItemsWhenEnd());
        }
    }

    private void initFreeLayout(View gameView) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (((double) (this.mFrameWidth - this.mFreePlayIcon.getWidth())) / 3.5d), (int) (((double) (this.mFrameHeight - this.mFreePlayIcon.getHeight())) / 3.5d));
        layoutParams.addRule(12, 1);
        layoutParams.addRule(11, 1);
        layoutParams.rightMargin = 5;
        layoutParams.bottomMargin = 10;
        this.mFreePlayLayout.setLayoutParams(layoutParams);
    }

    private void initLoadingAnim(View gameView) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) (((double) (this.mFrameWidth - this.mGameLoadingView.getWidth())) / 1.5d), (int) (((double) (this.mFrameHeight - this.mGameLoadingView.getHeight())) / 1.5d));
        params.addRule(13, 1);
        this.mGameLoadingView.setLayoutParams(params);
    }

    private void initUnlockLayout(View gameView) {
        this.mLockedGameLayout.getBackground().setAlpha(100);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (((double) (this.mFrameWidth - this.mUnlockLayout.getWidth())) / 1.8d), (int) (((double) (this.mFrameHeight - this.mUnlockLayout.getHeight())) / 1.8d));
        layoutParams.addRule(13, 1);
        this.mUnlockLayout.setLayoutParams(layoutParams);
    }

    private class ShowFreePlayWhenEnd implements Animation.AnimationListener {
        private ShowFreePlayWhenEnd() {
        }

        public void onAnimationEnd(Animation arg0) {
            GameCatalogView.this.mFreePlayLayout.setVisibility(0);
            GameCatalogView.this.mFreePlayIcon.startAnimation(GameCatalogView.this.mFreeButtonRotateAnimation);
        }

        public void onAnimationStart(Animation arg0) {
        }

        public void onAnimationRepeat(Animation arg0) {
        }
    }

    private class ShowLockItemsWhenEnd implements Animation.AnimationListener {
        private ShowLockItemsWhenEnd() {
        }

        public void onAnimationEnd(Animation arg0) {
            GameCatalogView.this.mLockedGameLayout.setVisibility(0);
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    public void stopHeavyAnimations() {
        this.mFreePlayIcon.clearAnimation();
        this.mUnlockBtn.clearAnimation();
    }

    public void startHeavyAnimations() {
        this.mFreePlayIcon.startAnimation(this.mFreeButtonRotateAnimation);
        this.mUnlockBtn.startAnimation(this.mUnlockButtonRotateAnimation);
    }
}
