package mominis.gameconsole.views.impl;

import android.content.DialogInterface;
import android.os.Handler;
import android.widget.Toast;
import java.util.ArrayList;
import mominis.common.mvc.IView;
import mominis.gameconsole.common.MessageBox;
import roboguice.activity.RoboActivity;

public class BaseActivity extends RoboActivity implements IView {
    boolean mDestroyed = false;
    public Handler mHandler = new Handler();

    /* access modifiers changed from: protected */
    public Handler getHandler() {
        return this.mHandler;
    }

    public void setStatusText(int textId) {
        Toast.makeText(this, textId, 0).show();
    }

    public void setStatusText(String text) {
        Toast.makeText(this, text, 0).show();
    }

    public void close() {
        finish();
    }

    public void closeWithResult(int result) {
        setResult(result);
        finish();
    }

    public void showMessage(final String title, final String text) {
        if (!this.mDestroyed) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    MessageBox.Show(BaseActivity.this, title, text, MessageBox.MessageBoxButtons.OK);
                }
            });
        }
    }

    public void showMessage(final String title, final String text, final DialogInterface.OnClickListener onButton1) {
        if (!this.mDestroyed) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    MessageBox.Show(BaseActivity.this, title, text, MessageBox.MessageBoxButtons.OK, onButton1);
                }
            });
        }
    }

    public void showYesNoMessage(String title, String text, DialogInterface.OnClickListener onYes, DialogInterface.OnClickListener onNo) {
        if (!this.mDestroyed) {
            final String str = title;
            final String str2 = text;
            final DialogInterface.OnClickListener onClickListener = onYes;
            final DialogInterface.OnClickListener onClickListener2 = onNo;
            this.mHandler.post(new Runnable() {
                public void run() {
                    MessageBox.Show(BaseActivity.this, str, str2, MessageBox.MessageBoxButtons.YES_NO, onClickListener, onClickListener2);
                }
            });
        }
    }

    public void showCustomMessage(String title, String text, ArrayList<String> buttons, DialogInterface.OnClickListener onButton1, DialogInterface.OnClickListener onButton2) {
        if (!this.mDestroyed) {
            final String str = title;
            final String str2 = text;
            final ArrayList<String> arrayList = buttons;
            final DialogInterface.OnClickListener onClickListener = onButton1;
            final DialogInterface.OnClickListener onClickListener2 = onButton2;
            this.mHandler.post(new Runnable() {
                public void run() {
                    MessageBox.Show(BaseActivity.this, str, str2, MessageBox.MessageBoxButtons.CUSTOM, arrayList, onClickListener, onClickListener2);
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mDestroyed = true;
    }

    /* access modifiers changed from: protected */
    public boolean isDestroyed() {
        return this.mDestroyed;
    }
}
