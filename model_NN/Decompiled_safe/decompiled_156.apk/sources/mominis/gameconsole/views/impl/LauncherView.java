package mominis.gameconsole.views.impl;

import android.os.Bundle;
import android.util.Log;
import com.google.inject.Inject;
import mominis.gameconsole.controllers.LauncherController;

public class LauncherView extends BaseActivity {
    private static final String TAG = "LauncherView";
    boolean mAppLaunched = false;
    @Inject
    private LauncherController mController;
    boolean mViewClosed = false;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("LauncherView", "View created");
        this.mController.setView(this);
        this.mController.onInit();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Log.d("LauncherView", "View resumed");
        if (!this.mAppLaunched) {
            this.mController.onAppLaunch();
            this.mAppLaunched = true;
        } else if (!this.mViewClosed) {
            this.mController.onAppExit();
            this.mViewClosed = true;
        }
    }
}
