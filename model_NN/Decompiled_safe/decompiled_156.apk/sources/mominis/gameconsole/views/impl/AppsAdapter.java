package mominis.gameconsole.views.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import mominis.common.mvc.IObservable;
import mominis.common.mvc.IObserver;
import mominis.common.mvc.ListChangedEventArgs;
import mominis.gameconsole.common.EventArgs;
import mominis.gameconsole.controllers.AppPresentation;
import mominis.gameconsole.controllers.CatalogController;

public class AppsAdapter extends BaseAdapter implements IObserver<EventArgs>, AbsListView.OnScrollListener {
    private static final int NUM_CUNCURRENT_VIEWS = 4;
    private static final String TAG = "AppsAdapter";
    private Context mContext;
    private CatalogController mController;
    private GridView mFather;
    private int mFirstVisibleItem;
    int mFrameHeight;
    int mFrameWidth;
    private AppsImageCache mImageCache = new AppsImageCache();
    private int mLastVisibleItem;
    private GameViewPool mPool;
    private float mWidthToHeightRatio = 1.4166666f;

    public interface IAppImageProvider {
        void addOrUpdate(AppPresentation appPresentation);

        void evictAll();

        Bitmap getImage(AppPresentation appPresentation);
    }

    public AppsAdapter(Context c, GridView father) {
        this.mContext = c;
        this.mFather = father;
        this.mFrameWidth = ((WindowManager) this.mContext.getSystemService("window")).getDefaultDisplay().getWidth();
        this.mFrameHeight = (int) (((float) this.mFrameWidth) / this.mWidthToHeightRatio);
        this.mFather.setOnScrollListener(this);
        this.mPool = new GameViewPool(this.mFrameWidth, this.mFrameHeight, NUM_CUNCURRENT_VIEWS);
    }

    public void setController(CatalogController controller) {
        this.mController = controller;
        this.mController.registerObserver(this);
    }

    public int getCount() {
        try {
            return this.mController.Count();
        } catch (Exception e) {
            Exception e2 = e;
            e2.printStackTrace();
            Log.e(TAG, "getCount threw an exception", e2);
            return 0;
        }
    }

    public Object getItem(int position) {
        try {
            return this.mController.getApp(position);
        } catch (Exception e) {
            Exception e2 = e;
            e2.printStackTrace();
            Log.e(TAG, "getItem threw an exception", e2);
            return null;
        }
    }

    public long getItemId(int position) {
        AppPresentation app = null;
        try {
            app = this.mController.getApp(position);
        } catch (Exception e) {
            Exception e2 = e;
            e2.printStackTrace();
            Log.e(TAG, "getItemId threw an exception", e2);
        }
        if (app != null) {
            return app.getID();
        }
        Log.e(TAG, "getApp returened null on getItemId");
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        boolean z;
        GameCatalogView gameView;
        Object[] objArr = new Object[NUM_CUNCURRENT_VIEWS];
        objArr[0] = Integer.valueOf(position);
        objArr[1] = Integer.valueOf(this.mFirstVisibleItem);
        objArr[2] = Integer.valueOf(this.mLastVisibleItem);
        if (convertView != null) {
            z = true;
        } else {
            z = false;
        }
        objArr[3] = Boolean.valueOf(z);
        Log.d(TAG, String.format("getView (%d): visible indices first (%d) last (%d) - %b", objArr));
        AppPresentation app = this.mController.getApp(position);
        if (convertView != null) {
            gameView = (GameCatalogView) convertView;
        } else {
            gameView = createView();
        }
        gameView.setApp(app, false);
        this.mPool.set(gameView, position);
        return gameView;
    }

    private GameCatalogView createView() {
        GameCatalogView gameView = new GameCatalogView(this.mContext, this.mFrameWidth, this.mFrameHeight, this.mImageCache);
        gameView.setLayoutParams(new AbsListView.LayoutParams(this.mFrameWidth, this.mFrameHeight));
        return gameView;
    }

    public void onChanged(IObservable<EventArgs> iObservable, EventArgs eventArgs) {
        if (eventArgs == EventArgs.Empty) {
            Log.d(TAG, "onChanged called with empty event args - invalidating all without cache refresh");
            invalidateCollection();
        }
        if (this.mPool.getCount() != getCount()) {
            this.mPool.onClear();
            this.mPool = new GameViewPool(this.mFrameWidth, this.mFrameHeight, getCount());
        }
        if (eventArgs instanceof ListChangedEventArgs) {
            ListChangedEventArgs args = (ListChangedEventArgs) eventArgs;
            applyChangesToCache(args);
            if (args.getAction() != ListChangedEventArgs.Action.Set) {
                Log.d(TAG, "onChanged called with ListEventArgs different from Set - invalidating all");
                invalidateCollection();
            } else if (isItemVisible(args.getIndex())) {
                Log.d(TAG, String.format("onChanged called set action while item is visible - invalidating item no. %s", Integer.valueOf(args.getIndex())));
                this.mPool.onAppSet(args.getIndex(), this.mController.getApp(args.getIndex()), true);
            }
        }
    }

    private void applyChangesToCache(ListChangedEventArgs args) {
        if (args.getAction() == ListChangedEventArgs.Action.RangeAdd || args.getAction() == ListChangedEventArgs.Action.Add || args.getAction() == ListChangedEventArgs.Action.Set) {
            int index = args.getIndex();
            for (AppPresentation app : args.getAffected()) {
                this.mPool.clearAppImage(index);
                this.mImageCache.evict(app);
                index++;
            }
        } else if (args.getAction() == ListChangedEventArgs.Action.Clear) {
            this.mImageCache.evictAll();
        } else if (args.getAction() == ListChangedEventArgs.Action.Remove || args.getAction() == ListChangedEventArgs.Action.RangeRemove) {
            for (AppPresentation app2 : args.getAffected()) {
                this.mImageCache.evict(app2);
            }
        }
    }

    private boolean isItemVisible(int index) {
        return index >= this.mFirstVisibleItem && index <= this.mLastVisibleItem;
    }

    private void invalidateCollection() {
        this.mFirstVisibleItem = 0;
        this.mLastVisibleItem = 3;
        notifyDataSetChanged();
    }

    public void release() {
        this.mController.unregisterObserver(this);
        this.mPool.onClear();
        this.mImageCache.evictAll();
    }

    private class GameViewPool {
        private GameCatalogView[] mPoolView;
        private int mSize;

        public GameViewPool(int width, int height, int size) {
            Log.d(AppsAdapter.TAG, "new game pool created");
            this.mPoolView = new GameCatalogView[size];
            this.mSize = size;
        }

        public int getCount() {
            return this.mSize;
        }

        public void set(GameCatalogView gameView, int position) {
            this.mPoolView[position] = gameView;
        }

        public void onAppSet(int index, AppPresentation app, boolean forceRefresh) {
            GameCatalogView view = this.mPoolView[index];
            if (view != null) {
                view.setApp(app, forceRefresh);
            }
        }

        public void clearAppImage(int index) {
            GameCatalogView view = this.mPoolView[index];
            if (view != null) {
                view.setApp(null, true);
            }
        }

        public void onAppRemoved(int index) {
            GameCatalogView view = this.mPoolView[index];
            if (view != null && view.getApp() != null) {
                view.setApp(null, false);
            }
        }

        public void resetAll() {
            for (GameCatalogView view : this.mPoolView) {
                view.resetState();
            }
        }

        public void onClear() {
            Log.d(AppsAdapter.TAG, "clearing apps adapter pool view");
            for (int i = 0; i < this.mPoolView.length; i++) {
                GameCatalogView view = this.mPoolView[i];
                if (view != null) {
                    view.setApp(null, true);
                }
                this.mPoolView[i] = null;
            }
        }

        public void stopHeavyAnimations() {
            for (int i = 0; i < this.mPoolView.length; i++) {
                if (this.mPoolView[i] != null) {
                    this.mPoolView[i].stopHeavyAnimations();
                }
            }
        }

        public void startHeavyAnimations() {
            for (int i = 0; i < this.mPoolView.length; i++) {
                if (this.mPoolView[i] != null) {
                    this.mPoolView[i].startHeavyAnimations();
                }
            }
        }
    }

    private class AppsImageCache implements IAppImageProvider {
        private HashMap<AppPresentation, SoftReference<Bitmap>> mCache;

        private AppsImageCache() {
            this.mCache = new HashMap<>();
        }

        /* Debug info: failed to restart local var, previous not found, register: 6 */
        public void addOrUpdate(AppPresentation app) {
            BitmapFactory.Options bmpOptions = new BitmapFactory.Options();
            bmpOptions.inPurgeable = true;
            bmpOptions.inInputShareable = false;
            bmpOptions.inPreferredConfig = Bitmap.Config.RGB_565;
            if (app.getThumbnail() != null) {
                SoftReference<Bitmap> oldValue = this.mCache.put(app, new SoftReference(BitmapFactory.decodeByteArray(app.getThumbnail(), 0, app.getThumbnail().length, bmpOptions)));
                if (oldValue != null && oldValue.get() != null) {
                    ((Bitmap) oldValue.get()).recycle();
                    return;
                }
                return;
            }
            this.mCache.put(app, null);
        }

        public void evict(AppPresentation app) {
            SoftReference<Bitmap> oldValue = this.mCache.put(app, null);
            if (oldValue != null && oldValue.get() != null) {
                ((Bitmap) oldValue.get()).recycle();
            }
        }

        public void evictAll() {
            Bitmap bmp;
            for (AppPresentation key : this.mCache.keySet()) {
                SoftReference<Bitmap> ref = this.mCache.get(key);
                if (!(ref == null || (bmp = (Bitmap) ref.get()) == null)) {
                    bmp.recycle();
                }
                this.mCache.put(key, null);
            }
        }

        /* Debug info: failed to restart local var, previous not found, register: 2 */
        public Bitmap getImage(AppPresentation app) {
            SoftReference<Bitmap> result = this.mCache.get(app);
            if (result == null || result.get() == null) {
                addOrUpdate(app);
                result = this.mCache.get(app);
            }
            if (result != null) {
                return (Bitmap) result.get();
            }
            return null;
        }
    }

    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.mFirstVisibleItem = firstVisibleItem > 0 ? firstVisibleItem - 1 : 0;
        this.mLastVisibleItem = this.mFirstVisibleItem + visibleItemCount + 1;
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (this.mPool != null) {
            if (scrollState == 0) {
                this.mPool.startHeavyAnimations();
            } else if (scrollState == 1) {
                this.mPool.stopHeavyAnimations();
            }
        }
    }
}
