package mominis.gameconsole.views.impl;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import java.io.IOException;
import mominis.gameconsole.controllers.CatalogController;

public class ConsoleGridView extends GridView implements AdapterView.OnItemClickListener {
    private static final String TAG = "ConsoleGridView";
    private AppsAdapter mAdapter;
    private Boolean mAdapterSet;
    private CatalogController mController;

    public ConsoleGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mAdapterSet = false;
        if (context != null) {
            this.mAdapter = new AppsAdapter(context, this);
            setOnItemClickListener(this);
        }
    }

    public ConsoleGridView(Context context) {
        this(context, null);
    }

    public void setController(CatalogController ctrl) {
        this.mController = ctrl;
        this.mAdapter.setController(this.mController);
        if (!this.mAdapterSet.booleanValue() && this.mController != null) {
            setAdapter((ListAdapter) this.mAdapter);
            this.mAdapterSet = true;
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
        try {
            this.mController.AppActivated(position);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void release() {
        if (this.mAdapter != null) {
            Log.d(TAG, "releasing adapter references and resources");
            setAdapter((ListAdapter) null);
            this.mAdapter.release();
            this.mAdapter = null;
        }
    }
}
