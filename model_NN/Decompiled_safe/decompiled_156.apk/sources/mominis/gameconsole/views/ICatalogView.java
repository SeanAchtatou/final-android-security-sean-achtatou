package mominis.gameconsole.views;

import mominis.common.mvc.IView;

public interface ICatalogView extends IView {
    void displayRegistrationBox();

    int getNumAppsInScreen();

    void onPurchasePlanComplete();

    void onPurchasePlanStart();

    void onRefreshAlreadyExecuting();

    void onRefreshCatalogComplete();

    void onRefreshCatalogStart();

    void setStatusText(int i);

    void setStatusText(String str);

    void showDownloadFailedNoConnectivity();

    void showInstallUpdateMessage();

    void showMembershipMessage(String str);

    void showScrollingHint();
}
