package mominis.gameconsole.views;

import java.util.ArrayList;
import mominis.common.mvc.IView;

public interface IPurchaseView extends IView {
    void setNotes(ArrayList<String> arrayList);

    void setPrice(String str, double d, String str2);

    void setStatusText(int i);

    void setStatusText(String str);
}
