package mominis.gameconsole.views;

import mominis.common.mvc.IView;

public interface IDownloadView extends IView {
    void showProgress(int i, float f);
}
