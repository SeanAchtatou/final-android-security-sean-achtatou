package mominis.gameconsole.views;

public class Views {
    public static final String CATALOG_VIEW = "CatalogScreen";
    public static final String DOWNLOAD_VIEW = "DownloadScreen";
    public static final String EULA_VIEW = "EulaView";
    public static final String LAUNCHER_VIEW = "LauncherView";
    public static final String PURCHASE_VIEW = "PurchaseScreen";
    public static final String SPLASH_VIEW = "SplashScreen";
}
