package mominis.gameconsole.services;

import mominis.gameconsole.core.models.Application;

public interface IAnalyticsManager {
    void ConsoleActivated();

    void GameDownloadStart(Application application);

    void GameDownloaded(Application application);

    void GameInstallStart(Application application);

    void GameLaunched(Application application);

    void SubscriptionActivated(Application application, Boolean bool);
}
