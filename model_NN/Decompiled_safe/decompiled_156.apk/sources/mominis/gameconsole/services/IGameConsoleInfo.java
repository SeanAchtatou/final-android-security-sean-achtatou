package mominis.gameconsole.services;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public interface IGameConsoleInfo {
    PackageInfo getInfo() throws PackageManager.NameNotFoundException;
}
