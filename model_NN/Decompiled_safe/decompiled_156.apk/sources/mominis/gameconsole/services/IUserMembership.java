package mominis.gameconsole.services;

import java.util.List;
import mominis.gameconsole.core.models.LocaleInfo;
import mominis.gameconsole.core.models.PurchasePlan;
import mominis.gameconsole.core.models.UserIdentifier;
import mominis.gameconsole.core.models.UserMembershipState;

public interface IUserMembership {
    List<PurchasePlan> getAvailablePlans();

    UserMembershipState getMembershipState();

    boolean isInPlan(int i);

    void loadAvailablePlans(int i, LocaleInfo localeInfo);

    UserMembershipState loginUser();

    UserMembershipState loginUser(UserIdentifier userIdentifier);

    UserMembershipState purchasePlan(int i);
}
