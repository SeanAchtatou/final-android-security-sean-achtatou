package mominis.gameconsole.services.stubs;

import mominis.gameconsole.core.models.Application;
import mominis.gameconsole.services.IAnalyticsManager;

public class AnalyticsManagerStub implements IAnalyticsManager {
    public void ConsoleActivated() {
    }

    public void GameDownloaded(Application app) {
    }

    public void GameLaunched(Application app) {
    }

    public void SubscriptionActivated(Application app, Boolean result) {
    }

    public void GameDownloadStart(Application app) {
    }

    public void GameInstallStart(Application app) {
    }
}
