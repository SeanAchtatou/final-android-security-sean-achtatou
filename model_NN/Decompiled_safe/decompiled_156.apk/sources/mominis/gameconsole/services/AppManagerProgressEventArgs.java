package mominis.gameconsole.services;

import mominis.gameconsole.common.ProgressEventArgs;
import mominis.gameconsole.core.models.Application;

public class AppManagerProgressEventArgs extends ProgressEventArgs {
    private Application mApp;
    private float mTotalSize;

    public AppManagerProgressEventArgs(Application app, int progress, float totalSize) {
        super(progress);
        this.mApp = app;
        this.mTotalSize = totalSize;
    }

    public Application getApp() {
        return this.mApp;
    }

    public float getTotalSize() {
        return this.mTotalSize;
    }
}
