package mominis.gameconsole.services;

import mominis.gameconsole.common.IProcessListener;

public interface IAutoUpdater {
    void autoUpdate(IProcessListener<AppManagerProgressEventArgs> iProcessListener);

    Boolean checkForNewVersion();

    void downloadNewVersion(IProcessListener<AppManagerProgressEventArgs> iProcessListener);

    int getLatestVersionCode();

    String getLatestVersionName();
}
