package mominis.gameconsole.services;

public interface ISubscriptionManager {
    Boolean IsUserSubscribed();

    void SubscribeUser();

    void UnsubscribeUser();
}
