package mominis.gameconsole.services;

import java.io.IOException;
import java.io.InputStream;
import mominis.gameconsole.common.IProcessListener;
import mominis.gameconsole.core.models.Application;
import mominis.gameconsole.core.repositories.IReadableAppRepository;

public interface IAppManager {
    IProcess DownloadApp(Application application, IProcessListener<AppManagerProgressEventArgs> iProcessListener);

    void cleanup();

    IReadableAppRepository getLocalRepository();

    IReadableAppRepository getRemoteRepository();

    boolean isInstalled(Application application);

    void launchApp(Application application) throws Exception;

    boolean resyncRemote();

    Application updateAppState(Application application) throws IOException;

    void writeApp(InputStream inputStream, Application application);
}
