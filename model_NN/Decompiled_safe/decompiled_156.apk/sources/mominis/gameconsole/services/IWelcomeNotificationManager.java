package mominis.gameconsole.services;

import mominis.gameconsole.common.IProcessListener;
import mominis.gameconsole.common.ProgressEventArgs;

public interface IWelcomeNotificationManager {
    void checkForNewNotification(IProcessListener<ProgressEventArgs> iProcessListener);

    boolean checkForNewNotification();

    void deprecateMessage();

    String getMessage();

    boolean isNewNotificationAvailable();
}
