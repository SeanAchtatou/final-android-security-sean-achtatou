package mominis.gameconsole.services;

public interface IConnectivityMonitor {
    Boolean isConnected();

    Boolean isRoaming();
}
