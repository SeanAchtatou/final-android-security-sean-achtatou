package mominis.gameconsole.services;

import java.util.List;
import mominis.gameconsole.core.models.LocaleInfo;
import mominis.gameconsole.core.models.ProgramFacadeResult;
import mominis.gameconsole.core.models.PurchasePlan;
import mominis.gameconsole.core.models.UserIdentifier;

public interface IGameConsoleServer {
    boolean Login(UserIdentifier userIdentifier);

    boolean completeWithoutPayment(int i);

    List<PurchasePlan> getPurchasePlans(int i, LocaleInfo localeInfo);

    boolean isInPlan(int i, int i2);

    ProgramFacadeResult registerAndPurchase(int i, String str, int i2);
}
