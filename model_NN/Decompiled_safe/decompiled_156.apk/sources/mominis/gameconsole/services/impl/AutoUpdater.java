package mominis.gameconsole.services.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.google.inject.Inject;
import java.net.URI;
import mominis.common.utils.IHttpClientFactory;
import mominis.common.utils.StreamUtils;
import mominis.gameconsole.com.R;
import mominis.gameconsole.common.IProcessListener;
import mominis.gameconsole.core.models.Application;
import mominis.gameconsole.services.AppManagerProgressEventArgs;
import mominis.gameconsole.services.IAppManager;
import mominis.gameconsole.services.IAutoUpdater;
import mominis.gameconsole.services.IGameConsoleInfo;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONObject;

public class AutoUpdater implements IAutoUpdater, IProcessListener<AppManagerProgressEventArgs> {
    private static final Boolean AUTO_UPDATE_DEFAULT_ENABLED = true;
    private static final String KEY__CURRENT_VERSION_CODE = "versionCode";
    private static final String KEY__CURRENT_VERSION_NAME = "versionName";
    private static final String KEY__UPDATE_URL = "url";
    public static final String PREFERENCES_AUTO_UPDATE = "auto_update";
    public static final String PREFERENCES_AUTO_UPDATE_ENABLED = "auto_update.enabled";
    public static final String PREFERENCES_AUTO_UPDATE_LOCAL_APK_PATH = "auto_update.local_apk_path";
    public static final String PREFERENCES_AUTO_UPDATE_LOCAL_APK_STATE = "auto_update.local_apk_state";
    public static final String PREFERENCES_AUTO_UPDATE_LOCAL_APK_VERSION_CODE = "auto_update.local_apk_version_code";
    private IAppManager mAppMgr;
    private IGameConsoleInfo mConsoleInfo;
    private Context mContext;
    private Boolean mDownloadInProgress = false;
    IProcessListener<AppManagerProgressEventArgs> mDownloadListener;
    private IHttpClientFactory mHttpClientFactory;
    private Boolean mIsEnabled = true;
    private String mLatestAPKUrl = null;
    private int mLatestVersionCode = 0;
    private String mLatestVersionName = null;
    private String mLocalAPKPath = null;
    private LocalAPKState mLocalAPKState = LocalAPKState.None;
    private String mUpdateFileUrl = null;

    public enum LocalAPKState {
        None,
        Downloaded
    }

    @Inject
    public AutoUpdater(Context context, IHttpClientFactory connectivityMonitor, IAppManager remoteMgr, IGameConsoleInfo consoleInfo) {
        this.mContext = context;
        this.mHttpClientFactory = connectivityMonitor;
        this.mAppMgr = remoteMgr;
        this.mConsoleInfo = consoleInfo;
        SharedPreferences preferences = this.mContext.getSharedPreferences(PREFERENCES_AUTO_UPDATE, 0);
        this.mIsEnabled = Boolean.valueOf(preferences.getBoolean(PREFERENCES_AUTO_UPDATE_ENABLED, AUTO_UPDATE_DEFAULT_ENABLED.booleanValue()));
        this.mUpdateFileUrl = this.mContext.getString(R.string.auto_update_url);
        this.mLocalAPKPath = preferences.getString(PREFERENCES_AUTO_UPDATE_LOCAL_APK_PATH, null);
        this.mLocalAPKState = LocalAPKState.valueOf(preferences.getString(PREFERENCES_AUTO_UPDATE_LOCAL_APK_STATE, LocalAPKState.None.name()));
        if (this.mLocalAPKState == LocalAPKState.Downloaded) {
            try {
                PackageInfo info = this.mConsoleInfo.getInfo();
                if (preferences.getInt(PREFERENCES_AUTO_UPDATE_LOCAL_APK_VERSION_CODE, info.versionCode) == info.versionCode) {
                    this.mLocalAPKState = LocalAPKState.None;
                    preferences.edit().putString(PREFERENCES_AUTO_UPDATE_LOCAL_APK_STATE, this.mLocalAPKState.name()).commit();
                    if (this.mLocalAPKPath != null) {
                        this.mContext.deleteFile(this.mLocalAPKPath.substring(this.mLocalAPKPath.lastIndexOf(47) + 1));
                        this.mLocalAPKPath = null;
                        preferences.edit().remove(PREFERENCES_AUTO_UPDATE_LOCAL_APK_PATH).commit();
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
            }
        }
    }

    public Boolean checkForNewVersion() {
        try {
            PackageInfo info = this.mConsoleInfo.getInfo();
            if (!this.mIsEnabled.booleanValue()) {
                return false;
            }
            if (this.mLocalAPKState == LocalAPKState.Downloaded) {
                return true;
            }
            JSONObject json = new JSONObject(StreamUtils.readToEnd(this.mHttpClientFactory.create().execute(new HttpGet(new URI(this.mUpdateFileUrl))).getEntity().getContent()).toString());
            if (!json.has(KEY__CURRENT_VERSION_CODE) || !json.has(KEY__CURRENT_VERSION_NAME) || !json.has(KEY__UPDATE_URL)) {
                return false;
            }
            int latestVersionCode = json.getInt(KEY__CURRENT_VERSION_CODE);
            String latestVersionName = json.getString(KEY__CURRENT_VERSION_NAME);
            this.mLatestAPKUrl = json.getString(KEY__UPDATE_URL);
            this.mLatestVersionName = latestVersionName;
            this.mLatestVersionCode = latestVersionCode;
            if (info.versionCode >= latestVersionCode) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        } catch (Exception e2) {
            return false;
        }
    }

    public void downloadNewVersion(IProcessListener<AppManagerProgressEventArgs> listener) {
        if (!this.mDownloadInProgress.booleanValue()) {
            Application newUpdate = new Application();
            if (this.mLocalAPKState == LocalAPKState.Downloaded) {
                newUpdate.setState(Application.State.Downloaded);
                newUpdate.setAPKPath(this.mLocalAPKPath);
                newUpdate.setViewable(false);
                listener.onEnd(this, new AppManagerProgressEventArgs(newUpdate, 100, 100.0f));
            } else if (this.mLatestAPKUrl != null) {
                newUpdate.setAPKPath(this.mLatestAPKUrl);
                newUpdate.setState(Application.State.Remote);
                newUpdate.setViewable(false);
                this.mDownloadListener = listener;
                this.mAppMgr.DownloadApp(newUpdate, this);
            }
        }
    }

    public void autoUpdate(IProcessListener<AppManagerProgressEventArgs> listener) {
        if (checkForNewVersion().booleanValue()) {
            downloadNewVersion(listener);
        }
    }

    public int getLatestVersionCode() {
        return this.mLatestVersionCode;
    }

    public String getLatestVersionName() {
        return this.mLatestVersionName;
    }

    public void OnProgress(Object sender, AppManagerProgressEventArgs args) {
        this.mDownloadListener.OnProgress(sender, args);
    }

    public void onStart(Object sender, AppManagerProgressEventArgs args) {
        this.mDownloadInProgress = true;
        this.mDownloadListener.onStart(sender, args);
    }

    public void onEnd(Object sender, AppManagerProgressEventArgs args) {
        this.mDownloadInProgress = false;
        if (args.getProgress() == 100) {
            String localFilename = args.getApp().getAPKPath().toString();
            SharedPreferences preferences = this.mContext.getSharedPreferences(PREFERENCES_AUTO_UPDATE, 0);
            this.mLocalAPKState = LocalAPKState.Downloaded;
            preferences.edit().putString(PREFERENCES_AUTO_UPDATE_LOCAL_APK_STATE, this.mLocalAPKState.name()).commit();
            this.mLocalAPKPath = localFilename;
            preferences.edit().putString(PREFERENCES_AUTO_UPDATE_LOCAL_APK_PATH, this.mLocalAPKPath).commit();
            preferences.edit().putInt(PREFERENCES_AUTO_UPDATE_LOCAL_APK_VERSION_CODE, this.mLatestVersionCode).commit();
        }
        this.mDownloadListener.onEnd(sender, args);
    }
}
