package mominis.gameconsole.services.impl;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.util.Log;
import com.google.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import mominis.common.utils.IHttpClientFactory;
import mominis.gameconsole.common.IProcessListener;
import mominis.gameconsole.core.models.Application;
import mominis.gameconsole.core.repositories.IAppRepository;
import mominis.gameconsole.core.repositories.IReadableAppRepository;
import mominis.gameconsole.core.repositories.IRemoteRepository;
import mominis.gameconsole.core.repositories.JsonRemoteRepository;
import mominis.gameconsole.services.AppManagerProgressEventArgs;
import mominis.gameconsole.services.IAppManager;
import mominis.gameconsole.services.IProcess;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;

public class AppManager implements IAppManager {
    protected static final String TAG = "Application Manager";
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public IHttpClientFactory mHttpClientFactory;
    /* access modifiers changed from: private */
    public IAppRepository mLocalRepo;
    private IRemoteRepository mRemoteRepo;
    private Thread mThread;

    @Inject
    public AppManager(IAppRepository localRepo, @JsonRemoteRepository IRemoteRepository remoteRepository, IHttpClientFactory httpClientFactory, Context context) throws IOException {
        this.mLocalRepo = localRepo;
        this.mRemoteRepo = remoteRepository;
        this.mContext = context;
        this.mHttpClientFactory = httpClientFactory;
    }

    public IProcess DownloadApp(final Application app, final IProcessListener<AppManagerProgressEventArgs> listener) {
        if (this.mThread == null || !this.mThread.isAlive()) {
            final AppStoringProcess proc = new AppStoringProcess();
            new Thread(new Runnable() {
                int totalSize;

                public void run() {
                    boolean exceptionOccurred;
                    InputStream is = null;
                    try {
                        String url = app.getAPKPath().toString();
                        Log.d(AppManager.TAG, String.format("getting application from url - %s", url));
                        HttpEntity entity = AppManager.this.mHttpClientFactory.create().execute(new HttpGet(url)).getEntity();
                        this.totalSize = (int) entity.getContentLength();
                        is = entity.getContent();
                        proc.writeApp(is, this.totalSize, app, listener);
                        if (!(0 == 0 || listener == null)) {
                            try {
                                listener.onEnd(this, new AppManagerProgressEventArgs(app, -1, 0.0f));
                            } catch (Exception e) {
                                Log.e(AppManager.TAG, "an error occurred in the listener", e);
                            }
                        }
                        if (is != null) {
                            try {
                                is.close();
                            } catch (IOException e2) {
                                Log.e(AppManager.TAG, "could not close output stream", e2);
                                e2.printStackTrace();
                            }
                        }
                    } catch (IOException e3) {
                        exceptionOccurred = true;
                        Log.e(AppManager.TAG, String.format("File download failed due to IOException %s - possible due to lack of connectiviry", e3.getMessage()));
                        if (!(1 == 0 || listener == null)) {
                            try {
                                listener.onEnd(this, new AppManagerProgressEventArgs(app, -1, 0.0f));
                            } catch (Exception e4) {
                                Log.e(AppManager.TAG, "an error occurred in the listener", e4);
                            }
                        }
                        if (is != null) {
                            try {
                                is.close();
                            } catch (IOException e5) {
                                Log.e(AppManager.TAG, "could not close output stream", e5);
                                e5.printStackTrace();
                            }
                        }
                    } catch (Exception e6) {
                        exceptionOccurred = true;
                        Log.e(AppManager.TAG, "File download failed due to an unexpected exception during the download initiation process", e6);
                        if (!(1 == 0 || listener == null)) {
                            try {
                                listener.onEnd(this, new AppManagerProgressEventArgs(app, -1, 0.0f));
                            } catch (Exception e7) {
                                Log.e(AppManager.TAG, "an error occurred in the listener", e7);
                            }
                        }
                        if (is != null) {
                            try {
                                is.close();
                            } catch (IOException e8) {
                                Log.e(AppManager.TAG, "could not close output stream", e8);
                                e8.printStackTrace();
                            }
                        }
                    } catch (Throwable th) {
                        if (exceptionOccurred && listener != null) {
                            try {
                                listener.onEnd(this, new AppManagerProgressEventArgs(app, -1, 0.0f));
                            } catch (Exception e9) {
                                Log.e(AppManager.TAG, "an error occurred in the listener", e9);
                            }
                        }
                        if (is != null) {
                            try {
                                is.close();
                            } catch (IOException e10) {
                                Log.e(AppManager.TAG, "could not close output stream", e10);
                                e10.printStackTrace();
                            }
                        }
                        throw th;
                    }
                }
            }).start();
            return proc;
        }
        throw new IllegalStateException("Cannot start download while one is ongoing");
    }

    public void writeApp(InputStream is, Application app) {
        new AppStoringProcess().writeApp(is, -1, app, null);
    }

    public void launchApp(Application app) throws Exception {
        if (app == null) {
            Log.e(TAG, "Application is null - returning");
            return;
        }
        Log.d(TAG, String.format("launching application %s", app.getName()));
        if (app.getState() == Application.State.Installed) {
            Log.d(TAG, String.format("Getting intent for package name %s", app.getPackage()));
            Intent intent = this.mContext.getPackageManager().getLaunchIntentForPackage(app.getPackage());
            if (intent != null) {
                Log.d(TAG, String.format("intent retreived - launching", new Object[0]));
                this.mContext.startActivity(intent);
                return;
            }
            Log.e(TAG, String.format("intent was null - returning", new Object[0]));
            return;
        }
        throw new Exception("Cannot launch a application - invalid state");
    }

    public IReadableAppRepository getLocalRepository() {
        return this.mLocalRepo;
    }

    public IReadableAppRepository getRemoteRepository() {
        return this.mRemoteRepo;
    }

    public void cleanup() {
        try {
            List<Application> apps = this.mLocalRepo.getAll();
            for (int i = 0; i < apps.size(); i++) {
                updateAppState((Application) apps.get(i));
            }
        } catch (IOException e) {
            IOException e2 = e;
            Log.e(TAG, "cleanup threw an IOException", e2);
            e2.printStackTrace();
        }
    }

    public Application updateAppState(Application app) throws IOException {
        Boolean appInstalled = Boolean.valueOf(isInstalled(app));
        Application retApp = app;
        if (app.getState() == Application.State.Downloaded && appInstalled.booleanValue()) {
            Log.d(TAG, String.format("App %s is downloaded and installed - updating state", app.getName()));
            if (app.getAPKPath() != Uri.EMPTY) {
                String s = app.getAPKPath().toString();
                this.mContext.deleteFile(s.substring(s.lastIndexOf(47) + 1));
            }
            Application app2 = (Application) this.mLocalRepo.get(app.getID());
            app2.setState(Application.State.Installed);
            app2.setAPKPath(Uri.EMPTY);
            this.mLocalRepo.update(app2);
            return app2;
        } else if (app.getState() == Application.State.Remote && appInstalled.booleanValue()) {
            try {
                Log.d(TAG, String.format("App %s is remote but already installed - saving new app", app.getName()));
                Application newApp = new Application();
                newApp.CopyFrom(app);
                newApp.setState(Application.State.Installed);
                newApp.setAPKPath(Uri.EMPTY);
                return (Application) this.mLocalRepo.create(newApp);
            } catch (Exception e) {
                Log.i(TAG, "create entry failed - " + e.toString());
                return retApp;
            }
        } else if (app.getState() != Application.State.Installed || appInstalled.booleanValue()) {
            return retApp;
        } else {
            Log.d(TAG, String.format("App %s installed locally but has been removed - deleting", app.getName()));
            this.mLocalRepo.delete(app.getID());
            return retApp;
        }
    }

    public boolean isInstalled(Application app) {
        return isInstalled(app, "android.intent.category.INFO") || isInstalled(app, "android.intent.category.LAUNCHER");
    }

    private boolean isInstalled(Application app, String category) {
        Intent mainIntent = new Intent("android.intent.action.MAIN", (Uri) null);
        mainIntent.addCategory(category);
        List<ResolveInfo> pkgAppsList = this.mContext.getPackageManager().queryIntentActivities(mainIntent, 0);
        Boolean appInstalled = false;
        int i = 0;
        while (true) {
            if (i >= pkgAppsList.size()) {
                break;
            } else if (pkgAppsList.get(i).activityInfo.applicationInfo.packageName.equals(app.getPackage())) {
                appInstalled = true;
                break;
            } else {
                i++;
            }
        }
        return appInstalled.booleanValue();
    }

    public boolean resyncRemote() {
        return this.mRemoteRepo.refresh();
    }

    private class AppStoringProcess implements IProcess {
        private boolean mShouldStop;

        private AppStoringProcess() {
            this.mShouldStop = false;
        }

        public void stop() {
            this.mShouldStop = true;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
            r10.flush();
            r13 = new mominis.gameconsole.core.models.Application();
            r13.CopyFrom(r25);
            r13.setState(mominis.gameconsole.core.models.Application.State.Downloaded);
            r13.setAPKPath("file://" + r7.getAbsolutePath());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x01ae, code lost:
            if (r25.isViewable() == false) goto L_0x01c0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:0x01b0, code lost:
            mominis.gameconsole.services.impl.AppManager.access$300(r0.this$0).create(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:0x01c0, code lost:
            if (r26 == null) goto L_0x01e0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:59:0x01c2, code lost:
            if (r24 <= 0) goto L_0x01e0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
            r26.onEnd(r1, new mominis.gameconsole.services.AppManagerProgressEventArgs(r13, 100, (float) r24));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:67:0x01f9, code lost:
            r18 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
            android.util.Log.e(mominis.gameconsole.services.impl.AppManager.TAG, "an error occurred in the listener", r18);
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void writeApp(java.io.InputStream r23, int r24, mominis.gameconsole.core.models.Application r25, mominis.gameconsole.common.IProcessListener<mominis.gameconsole.services.AppManagerProgressEventArgs> r26) {
            /*
                r22 = this;
                r9 = 0
                r10 = 0
                r7 = 0
                r18 = 0
                r0 = r18
                r1 = r22
                r1.mShouldStop = r0
                java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e0 }
                r18.<init>()     // Catch:{ Exception -> 0x00e0 }
                java.util.UUID r19 = java.util.UUID.randomUUID()     // Catch:{ Exception -> 0x00e0 }
                java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ Exception -> 0x00e0 }
                java.lang.String r19 = ".apk"
                java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ Exception -> 0x00e0 }
                java.lang.String r9 = r18.toString()     // Catch:{ Exception -> 0x00e0 }
                java.lang.String r16 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x00e0 }
                java.lang.String r18 = "mounted"
                r0 = r16
                r1 = r18
                boolean r18 = r0.equals(r1)     // Catch:{ Exception -> 0x00e0 }
                if (r18 == 0) goto L_0x0074
                java.io.File r15 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x00e0 }
                java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x00e0 }
                java.lang.String r18 = "Android/data/%s/files"
                r19 = 1
                r0 = r19
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x00e0 }
                r19 = r0
                r20 = 0
                r0 = r22
                mominis.gameconsole.services.impl.AppManager r0 = mominis.gameconsole.services.impl.AppManager.this     // Catch:{ Exception -> 0x00e0 }
                r21 = r0
                android.content.Context r21 = r21.mContext     // Catch:{ Exception -> 0x00e0 }
                java.lang.String r21 = r21.getPackageName()     // Catch:{ Exception -> 0x00e0 }
                r19[r20] = r21     // Catch:{ Exception -> 0x00e0 }
                java.lang.String r18 = java.lang.String.format(r18, r19)     // Catch:{ Exception -> 0x00e0 }
                r0 = r5
                r1 = r15
                r2 = r18
                r0.<init>(r1, r2)     // Catch:{ Exception -> 0x00e0 }
                java.io.File r8 = new java.io.File     // Catch:{ Exception -> 0x00e0 }
                r8.<init>(r5, r9)     // Catch:{ Exception -> 0x00e0 }
                r5.mkdirs()     // Catch:{ Exception -> 0x023a, all -> 0x0236 }
                boolean r14 = r8.createNewFile()     // Catch:{ Exception -> 0x023a, all -> 0x0236 }
                if (r14 == 0) goto L_0x0128
                java.io.FileOutputStream r11 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0119 }
                r11.<init>(r8)     // Catch:{ IOException -> 0x0119 }
                r7 = r8
                r10 = r11
            L_0x0074:
                if (r10 != 0) goto L_0x009c
                r0 = r22
                mominis.gameconsole.services.impl.AppManager r0 = mominis.gameconsole.services.impl.AppManager.this     // Catch:{ Exception -> 0x00e0 }
                r18 = r0
                android.content.Context r18 = r18.mContext     // Catch:{ Exception -> 0x00e0 }
                r19 = 1
                r0 = r18
                r1 = r9
                r2 = r19
                java.io.FileOutputStream r10 = r0.openFileOutput(r1, r2)     // Catch:{ Exception -> 0x00e0 }
                r0 = r22
                mominis.gameconsole.services.impl.AppManager r0 = mominis.gameconsole.services.impl.AppManager.this     // Catch:{ Exception -> 0x00e0 }
                r18 = r0
                android.content.Context r18 = r18.mContext     // Catch:{ Exception -> 0x00e0 }
                r0 = r18
                r1 = r9
                java.io.File r7 = r0.getFileStreamPath(r1)     // Catch:{ Exception -> 0x00e0 }
            L_0x009c:
                if (r26 == 0) goto L_0x00bd
                if (r24 <= 0) goto L_0x00bd
                mominis.gameconsole.services.AppManagerProgressEventArgs r18 = new mominis.gameconsole.services.AppManagerProgressEventArgs     // Catch:{ Exception -> 0x00e0 }
                r19 = 0
                r0 = r24
                float r0 = (float) r0     // Catch:{ Exception -> 0x00e0 }
                r20 = r0
                r0 = r18
                r1 = r25
                r2 = r19
                r3 = r20
                r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x00e0 }
                r0 = r26
                r1 = r22
                r2 = r18
                r0.onStart(r1, r2)     // Catch:{ Exception -> 0x00e0 }
            L_0x00bd:
                r18 = 16384(0x4000, float:2.2959E-41)
                r0 = r18
                byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x00e0 }
                r4 = r0
                r17 = 0
                r12 = 0
            L_0x00c7:
                r0 = r23
                r1 = r4
                int r12 = r0.read(r1)     // Catch:{ Exception -> 0x00e0 }
                if (r12 <= 0) goto L_0x0177
                r0 = r22
                boolean r0 = r0.mShouldStop     // Catch:{ Exception -> 0x00e0 }
                r18 = r0
                if (r18 == 0) goto L_0x012b
                java.lang.Exception r18 = new java.lang.Exception     // Catch:{ Exception -> 0x00e0 }
                java.lang.String r19 = "Operation Canceled"
                r18.<init>(r19)     // Catch:{ Exception -> 0x00e0 }
                throw r18     // Catch:{ Exception -> 0x00e0 }
            L_0x00e0:
                r18 = move-exception
                r6 = r18
            L_0x00e3:
                if (r26 == 0) goto L_0x0104
                if (r24 <= 0) goto L_0x0104
                mominis.gameconsole.services.AppManagerProgressEventArgs r18 = new mominis.gameconsole.services.AppManagerProgressEventArgs     // Catch:{ all -> 0x0170 }
                r19 = -1
                r0 = r24
                float r0 = (float) r0     // Catch:{ all -> 0x0170 }
                r20 = r0
                r0 = r18
                r1 = r25
                r2 = r19
                r3 = r20
                r0.<init>(r1, r2, r3)     // Catch:{ all -> 0x0170 }
                r0 = r26
                r1 = r22
                r2 = r18
                r0.onEnd(r1, r2)     // Catch:{ all -> 0x0170 }
            L_0x0104:
                if (r7 == 0) goto L_0x0113
                boolean r18 = r7.delete()     // Catch:{ all -> 0x0170 }
                if (r18 == 0) goto L_0x0209
                java.lang.String r18 = "Application Manager"
                java.lang.String r19 = "file write failed - file deleted"
                android.util.Log.e(r18, r19)     // Catch:{ all -> 0x0170 }
            L_0x0113:
                if (r10 == 0) goto L_0x0118
                r10.close()     // Catch:{ IOException -> 0x0212 }
            L_0x0118:
                return
            L_0x0119:
                r18 = move-exception
                r6 = r18
                java.lang.String r18 = "Application Manager"
                java.lang.String r19 = "File allegadly created, but could not open output stream"
                r0 = r18
                r1 = r19
                r2 = r6
                android.util.Log.e(r0, r1, r2)     // Catch:{ Exception -> 0x023a, all -> 0x0236 }
            L_0x0128:
                r7 = r8
                goto L_0x0074
            L_0x012b:
                int r17 = r17 + r12
                if (r26 == 0) goto L_0x0161
                if (r24 <= 0) goto L_0x0161
                mominis.gameconsole.services.AppManagerProgressEventArgs r18 = new mominis.gameconsole.services.AppManagerProgressEventArgs     // Catch:{ Exception -> 0x00e0 }
                r0 = r17
                float r0 = (float) r0     // Catch:{ Exception -> 0x00e0 }
                r19 = r0
                r0 = r24
                float r0 = (float) r0     // Catch:{ Exception -> 0x00e0 }
                r20 = r0
                float r19 = r19 / r20
                r20 = 1120403456(0x42c80000, float:100.0)
                float r19 = r19 * r20
                r0 = r19
                int r0 = (int) r0     // Catch:{ Exception -> 0x00e0 }
                r19 = r0
                r0 = r24
                float r0 = (float) r0     // Catch:{ Exception -> 0x00e0 }
                r20 = r0
                r0 = r18
                r1 = r25
                r2 = r19
                r3 = r20
                r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x00e0 }
                r0 = r26
                r1 = r22
                r2 = r18
                r0.OnProgress(r1, r2)     // Catch:{ Exception -> 0x00e0 }
            L_0x0161:
                r18 = 0
                r0 = r10
                r1 = r4
                r2 = r18
                r3 = r12
                r0.write(r1, r2, r3)     // Catch:{ Exception -> 0x00e0 }
                java.lang.Thread.yield()     // Catch:{ Exception -> 0x00e0 }
                goto L_0x00c7
            L_0x0170:
                r18 = move-exception
            L_0x0171:
                if (r10 == 0) goto L_0x0176
                r10.close()     // Catch:{ IOException -> 0x0224 }
            L_0x0176:
                throw r18
            L_0x0177:
                r10.flush()     // Catch:{ Exception -> 0x00e0 }
                mominis.gameconsole.core.models.Application r13 = new mominis.gameconsole.core.models.Application     // Catch:{ Exception -> 0x00e0 }
                r13.<init>()     // Catch:{ Exception -> 0x00e0 }
                r0 = r13
                r1 = r25
                r0.CopyFrom(r1)     // Catch:{ Exception -> 0x00e0 }
                mominis.gameconsole.core.models.Application$State r18 = mominis.gameconsole.core.models.Application.State.Downloaded     // Catch:{ Exception -> 0x00e0 }
                r0 = r13
                r1 = r18
                r0.setState(r1)     // Catch:{ Exception -> 0x00e0 }
                java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e0 }
                r18.<init>()     // Catch:{ Exception -> 0x00e0 }
                java.lang.String r19 = "file://"
                java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ Exception -> 0x00e0 }
                java.lang.String r19 = r7.getAbsolutePath()     // Catch:{ Exception -> 0x00e0 }
                java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ Exception -> 0x00e0 }
                java.lang.String r18 = r18.toString()     // Catch:{ Exception -> 0x00e0 }
                r0 = r13
                r1 = r18
                r0.setAPKPath(r1)     // Catch:{ Exception -> 0x00e0 }
                boolean r18 = r25.isViewable()     // Catch:{ Exception -> 0x00e0 }
                if (r18 == 0) goto L_0x01c0
                r0 = r22
                mominis.gameconsole.services.impl.AppManager r0 = mominis.gameconsole.services.impl.AppManager.this     // Catch:{ Exception -> 0x00e0 }
                r18 = r0
                mominis.gameconsole.core.repositories.IAppRepository r18 = r18.mLocalRepo     // Catch:{ Exception -> 0x00e0 }
                r0 = r18
                r1 = r13
                r0.create(r1)     // Catch:{ Exception -> 0x00e0 }
            L_0x01c0:
                if (r26 == 0) goto L_0x01e0
                if (r24 <= 0) goto L_0x01e0
                mominis.gameconsole.services.AppManagerProgressEventArgs r18 = new mominis.gameconsole.services.AppManagerProgressEventArgs     // Catch:{ Exception -> 0x01f9 }
                r19 = 100
                r0 = r24
                float r0 = (float) r0     // Catch:{ Exception -> 0x01f9 }
                r20 = r0
                r0 = r18
                r1 = r13
                r2 = r19
                r3 = r20
                r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x01f9 }
                r0 = r26
                r1 = r22
                r2 = r18
                r0.onEnd(r1, r2)     // Catch:{ Exception -> 0x01f9 }
            L_0x01e0:
                if (r10 == 0) goto L_0x0118
                r10.close()     // Catch:{ IOException -> 0x01e7 }
                goto L_0x0118
            L_0x01e7:
                r6 = move-exception
                java.lang.String r18 = "Application Manager"
                java.lang.String r19 = "could not close output stream"
                r0 = r18
                r1 = r19
                r2 = r6
                android.util.Log.e(r0, r1, r2)
                r6.printStackTrace()
                goto L_0x0118
            L_0x01f9:
                r18 = move-exception
                r6 = r18
                java.lang.String r18 = "Application Manager"
                java.lang.String r19 = "an error occurred in the listener"
                r0 = r18
                r1 = r19
                r2 = r6
                android.util.Log.e(r0, r1, r2)     // Catch:{ Exception -> 0x00e0 }
                goto L_0x01e0
            L_0x0209:
                java.lang.String r18 = "Application Manager"
                java.lang.String r19 = "file write failed - delete file failed"
                android.util.Log.e(r18, r19)     // Catch:{ all -> 0x0170 }
                goto L_0x0113
            L_0x0212:
                r6 = move-exception
                java.lang.String r18 = "Application Manager"
                java.lang.String r19 = "could not close output stream"
                r0 = r18
                r1 = r19
                r2 = r6
                android.util.Log.e(r0, r1, r2)
                r6.printStackTrace()
                goto L_0x0118
            L_0x0224:
                r6 = move-exception
                java.lang.String r19 = "Application Manager"
                java.lang.String r20 = "could not close output stream"
                r0 = r19
                r1 = r20
                r2 = r6
                android.util.Log.e(r0, r1, r2)
                r6.printStackTrace()
                goto L_0x0176
            L_0x0236:
                r18 = move-exception
                r7 = r8
                goto L_0x0171
            L_0x023a:
                r18 = move-exception
                r6 = r18
                r7 = r8
                goto L_0x00e3
            */
            throw new UnsupportedOperationException("Method not decompiled: mominis.gameconsole.services.impl.AppManager.AppStoringProcess.writeApp(java.io.InputStream, int, mominis.gameconsole.core.models.Application, mominis.gameconsole.common.IProcessListener):void");
        }
    }
}
