package mominis.gameconsole.services.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.util.Arrays;
import java.util.List;
import mominis.common.bi.Installation;
import mominis.gameconsole.com.R;
import mominis.gameconsole.common.StringUtils;
import mominis.gameconsole.core.models.Application;
import mominis.gameconsole.services.IAnalyticsManager;

public abstract class BaseGoogleAnalyticsMgr implements IAnalyticsManager {
    private static final String ACTION_DOWNLOAD_END = "download_end";
    private static final String ACTION_DOWNLOAD_START = "download_start";
    private static final String ACTION_INSTALL = "install";
    private static final String ACTION_LAUNCHED = "launched";
    private static final String ACTION_SUBSCRIBED = "subscribed";
    private static final String CATEGORY_CATALOG = "catalog";
    private static final String CATEGORY_DOWNLOAD = "download_screen";
    private static final String FIRST_LAUNCH = "console_first_launch";
    private static final String PAGE_CATALOG = "CATALOG PAGE";
    private static final String PREFERENCES_ANALYTICS = "analytics";
    private static final String PREFERENCE_INSTALLATION_REPORTED = "analytics.installationReported";
    protected static final String TAG = "Google Analytics Manage";
    protected Context mContext;
    protected String mPhoneNumber = getInstallationId();
    private TelephonyManager mTelephonyMgr = ((TelephonyManager) this.mContext.getSystemService("phone"));
    protected String mWebPropertyId = this.mContext.getResources().getString(R.string.Google_TrackID);

    /* access modifiers changed from: protected */
    public abstract void track(String str);

    public BaseGoogleAnalyticsMgr(Context context) {
        this.mContext = context;
    }

    /* access modifiers changed from: protected */
    public String getInstallationId() {
        if (this.mContext.getResources().getBoolean(R.Boolean.use_uuid_for_tracking)) {
            return Installation.id(this.mContext);
        }
        return this.mTelephonyMgr.getLine1Number();
    }

    public void ConsoleActivated() {
        SharedPreferences preferences = this.mContext.getSharedPreferences(PREFERENCES_ANALYTICS, 0);
        if (!preferences.getBoolean(PREFERENCE_INSTALLATION_REPORTED, false)) {
            constructAndTrack(Arrays.asList(FIRST_LAUNCH, this.mPhoneNumber));
            preferences.edit().putBoolean(PREFERENCE_INSTALLATION_REPORTED, true).commit();
        }
        constructAndTrack(Arrays.asList(PAGE_CATALOG, this.mPhoneNumber));
    }

    public void GameDownloaded(Application app) {
        constructAndTrack(Arrays.asList(CATEGORY_DOWNLOAD, ACTION_DOWNLOAD_END, app.getName(), this.mPhoneNumber));
    }

    public void GameLaunched(Application app) {
        constructAndTrack(Arrays.asList(CATEGORY_CATALOG, ACTION_LAUNCHED, app.getName(), this.mPhoneNumber));
    }

    public void SubscriptionActivated(Application app, Boolean result) {
        String[] strArr = new String[5];
        strArr[0] = CATEGORY_CATALOG;
        strArr[1] = ACTION_SUBSCRIBED;
        strArr[2] = app.getName();
        strArr[3] = result.booleanValue() ? "Yes" : "No";
        strArr[4] = this.mPhoneNumber;
        constructAndTrack(Arrays.asList(strArr));
    }

    public void GameDownloadStart(Application app) {
        constructAndTrack(Arrays.asList(CATEGORY_DOWNLOAD, ACTION_DOWNLOAD_START, app.getName(), this.mPhoneNumber));
    }

    public void GameInstallStart(Application app) {
        constructAndTrack(Arrays.asList(CATEGORY_CATALOG, ACTION_INSTALL, app.getName(), this.mPhoneNumber));
    }

    /* access modifiers changed from: package-private */
    public void constructAndTrack(List<String> data) {
        String page = StringUtils.join(data, "/");
        track(page);
        Log.i(TAG, String.format("Tracking page %s", page));
    }
}
