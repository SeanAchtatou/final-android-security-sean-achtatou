package mominis.gameconsole.services.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import mominis.gameconsole.core.models.BillingState;
import mominis.gameconsole.core.models.LocaleInfo;
import mominis.gameconsole.core.models.ProgramFacadeResult;
import mominis.gameconsole.core.models.PurchasePlan;
import mominis.gameconsole.core.models.RegistrationResult;
import mominis.gameconsole.core.models.UserIdentifier;
import mominis.gameconsole.core.models.UserMembershipState;
import mominis.gameconsole.services.IConnectivityMonitor;
import mominis.gameconsole.services.IGameConsoleServer;
import mominis.gameconsole.services.IUserMembership;

public class UserMembership implements IUserMembership {
    public static final int DEFAULT_CHANNEL_ID = 0;
    private static final long DEFAULT_OFFLINE_PERIOD_IN_MILLIS = 604800000;
    private static final String KEY__BILLING_STATE_SECRET = "Secret";
    private static final String KEY__BILLING_STATE_SHORT_CODE = "Shortcode";
    private static final long MILLIS_IN_DAY = 86400000;
    private static final String PREFERENCES_MEMBERSHIP = "membership";
    private static final String PREFERENCES_MEMBERSHIP_ALLOWED_OFFLINE_PERIOD_MILLIS = "membership.allowed_offline_period_millis";
    private static final String PREFERENCES_MEMBERSHIP_CHANNEL_ID = "membership.channel_id";
    private static final String PREFERENCES_MEMBERSHIP_LAST_LOGIN_TIME = "membership.last_login_time";
    private static final String PREFERENCES_MEMBERSHIP_PASSWORD = "membership.password";
    private static final String PREFERENCES_MEMBERSHIP_USERNAME = "membership.username";
    private static final String PREFERENCES_MEMBERSHIP_USER_ID = "membership.user_id";
    private static final String TAG = "User Membership";
    private IConnectivityMonitor mConnectivityMonitor;
    /* access modifiers changed from: private */
    public Context mContext;
    private UserIdentifier mCurrentUser = null;
    private IGameConsoleServer mGameConsoleServer;
    private MembershipRepo mMembershipRepo;
    private List<PurchasePlan> mPurchasePlans = new ArrayList();
    private boolean mRegisteredViaSms = false;

    @Inject
    public UserMembership(Context context, IGameConsoleServer gameConsoleServer, IConnectivityMonitor connectivityMonitor) {
        this.mContext = context;
        this.mConnectivityMonitor = connectivityMonitor;
        this.mGameConsoleServer = gameConsoleServer;
        this.mMembershipRepo = new MembershipRepo();
    }

    public UserMembershipState loginUser(UserIdentifier userIdentifier) {
        SharedPreferences preferences = this.mContext.getSharedPreferences(PREFERENCES_MEMBERSHIP, 0);
        if (!this.mConnectivityMonitor.isConnected().booleanValue() || this.mConnectivityMonitor.isRoaming().booleanValue()) {
            return loginOffline();
        }
        if (!this.mGameConsoleServer.Login(userIdentifier)) {
            return loginOffline();
        }
        saveUserIdentifier(userIdentifier, preferences);
        boolean isRegisteredToPlan = false;
        UserMembershipState rv = getMembershipState();
        if (rv.getPlanId() != -1) {
            isRegisteredToPlan = isInPlan(rv.getPlanId());
        }
        if (isRegisteredToPlan || this.mRegisteredViaSms) {
            rv.setMembershipStatus(UserMembershipState.UserMembershipStatus.USER_LOGGED_IN_REGISTERED);
        } else {
            rv.setMembershipStatus(UserMembershipState.UserMembershipStatus.USER_LOGGED_IN_NOT_REGISTERED);
        }
        this.mMembershipRepo.save(rv);
        return rv;
    }

    private void saveUserIdentifier(UserIdentifier userIdentifier, SharedPreferences preferences) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREFERENCES_MEMBERSHIP_USERNAME, userIdentifier.Username);
        editor.putInt(PREFERENCES_MEMBERSHIP_USER_ID, userIdentifier.UserID);
        editor.putString(PREFERENCES_MEMBERSHIP_PASSWORD, userIdentifier.Password);
        editor.putInt(PREFERENCES_MEMBERSHIP_CHANNEL_ID, userIdentifier.ChannelID);
        this.mCurrentUser = userIdentifier;
        editor.putLong(PREFERENCES_MEMBERSHIP_LAST_LOGIN_TIME, System.currentTimeMillis());
        editor.commit();
    }

    public UserMembershipState loginUser() {
        UserIdentifier iden = new UserIdentifier();
        SharedPreferences preferences = this.mContext.getSharedPreferences(PREFERENCES_MEMBERSHIP, 0);
        iden.Username = preferences.getString(PREFERENCES_MEMBERSHIP_USERNAME, null);
        iden.Password = preferences.getString(PREFERENCES_MEMBERSHIP_PASSWORD, null);
        iden.ChannelID = preferences.getInt(PREFERENCES_MEMBERSHIP_CHANNEL_ID, -1);
        if (iden.Username != null && iden.Password != null && iden.ChannelID == 0) {
            return loginUser(iden);
        }
        UserMembershipState rv = getMembershipState();
        rv.setMembershipStatus(UserMembershipState.UserMembershipStatus.USER_OFFLINE);
        this.mMembershipRepo.save(rv);
        return rv;
    }

    public UserMembershipState getMembershipState() {
        return this.mMembershipRepo.load();
    }

    public void loadAvailablePlans(int channelId, LocaleInfo localeInfo) {
        this.mPurchasePlans = this.mGameConsoleServer.getPurchasePlans(channelId, localeInfo);
    }

    public List<PurchasePlan> getAvailablePlans() {
        return this.mPurchasePlans;
    }

    public boolean isInPlan(int planID) {
        if (this.mCurrentUser == null) {
            return false;
        }
        return this.mGameConsoleServer.isInPlan(this.mCurrentUser.UserID, planID);
    }

    public UserMembershipState purchasePlan(int planId) {
        UserMembershipState rv = getMembershipState();
        String phoneNumber = getMyPhoneNumber(this.mContext);
        if (phoneNumber == null) {
            phoneNumber = "";
        }
        int channelId = 0;
        if (this.mCurrentUser != null) {
            channelId = this.mCurrentUser.ChannelID;
        }
        ProgramFacadeResult facadeRes = this.mGameConsoleServer.registerAndPurchase(planId, phoneNumber, channelId);
        if (facadeRes == null || facadeRes.getRegistrationResult().Code != RegistrationResult.RegistrationCode.SUCCESS || facadeRes.getBillingState().State == BillingState.BillingAuthenticationCode.INVALID_REQUEST) {
            this.mMembershipRepo.save(UserMembershipState.DEFAULT);
            return UserMembershipState.DEFAULT;
        }
        BillingState billingState = facadeRes.getBillingState();
        UserIdentifier userIden = facadeRes.getRegistrationResult().getUserIdentifier();
        rv.setPlanId(planId);
        this.mMembershipRepo.save(rv);
        loginUser(userIden);
        switch (billingState.State) {
            case BILLING_NOT_SUPPORTED:
                Log.i(TAG, "billing not supported - calling completeWithoutPayment");
                this.mGameConsoleServer.completeWithoutPayment(billingState.TransactionID);
                Log.i(TAG, "Complete without payment completed succesfully");
                if (!isInPlan(planId)) {
                    rv.setMembershipStatus(UserMembershipState.UserMembershipStatus.USER_LOGGED_IN_NOT_REGISTERED);
                } else {
                    rv.setMembershipStatus(UserMembershipState.UserMembershipStatus.USER_LOGGED_IN_REGISTERED);
                }
                rv.setBillingState(UserMembershipState.BillingResult.BILLING_NOT_SUPPORTED);
                break;
            case USER_ALREADY_PAID:
                rv.setMembershipStatus(UserMembershipState.UserMembershipStatus.USER_LOGGED_IN_REGISTERED);
                rv.setBillingState(UserMembershipState.BillingResult.ALREADY_REGISTERED);
                break;
            case UO_AUTH_REQUIRED:
                sendTextMessage(this.mContext, (String) billingState.Params.get(KEY__BILLING_STATE_SHORT_CODE), (String) billingState.Params.get(KEY__BILLING_STATE_SECRET));
                rv.setMembershipStatus(UserMembershipState.UserMembershipStatus.USER_LOGGED_IN_REGISTERED);
                this.mRegisteredViaSms = true;
                rv.setBillingState(UserMembershipState.BillingResult.UA_PENDING);
                break;
            case COMPLETED:
                rv.setMembershipStatus(UserMembershipState.UserMembershipStatus.USER_LOGGED_IN_REGISTERED);
                rv.setBillingState(UserMembershipState.BillingResult.SUCCESS);
                break;
            default:
                Log.e(TAG, "unexpected billing state received - " + billingState.State);
                return UserMembershipState.DEFAULT;
        }
        this.mMembershipRepo.save(rv);
        return rv;
    }

    private UserMembershipState loginOffline() {
        UserMembershipState rv = getMembershipState();
        SharedPreferences preferences = this.mContext.getSharedPreferences(PREFERENCES_MEMBERSHIP, 0);
        long lastLoginTime = preferences.getLong(PREFERENCES_MEMBERSHIP_LAST_LOGIN_TIME, 0);
        long allowedOfflinePeriod = preferences.getLong(PREFERENCES_MEMBERSHIP_ALLOWED_OFFLINE_PERIOD_MILLIS, DEFAULT_OFFLINE_PERIOD_IN_MILLIS);
        long currentTime = System.currentTimeMillis();
        if (lastLoginTime == 0 || rv.getPlanId() == -1) {
            rv.setMembershipStatus(UserMembershipState.UserMembershipStatus.USER_OFFLINE);
        } else if (currentTime - lastLoginTime > allowedOfflinePeriod) {
            rv.setMembershipStatus(UserMembershipState.UserMembershipStatus.USER_MEMBERSHIP_SUSPENDED);
        } else if (currentTime - lastLoginTime >= allowedOfflinePeriod - MILLIS_IN_DAY) {
            rv.setMembershipStatus(UserMembershipState.UserMembershipStatus.USER_OFFFLINE_IN_REGISTERED_BEFORE_SUSPENSION);
        } else {
            rv.setMembershipStatus(UserMembershipState.UserMembershipStatus.USER_OFFLINE_REGISTERED);
        }
        this.mMembershipRepo.save(rv);
        return rv;
    }

    private String getMyPhoneNumber(Context m_context) {
        return ((TelephonyManager) m_context.getSystemService("phone")).getLine1Number();
    }

    private void sendTextMessage(Context m_context, String phoneNumber, String text) {
        SmsManager.getDefault().sendTextMessage(phoneNumber, null, text, null, null);
    }

    private class MembershipRepo {
        private static final String BillingMessageDisplayedRecordName = "membership.billingMessageDisplayed";
        private static final String BillingStateRecordName = "membership.billingState";
        private static final String MembershipPlanIdRecordName = "membership.plan_id";
        private static final String UserMembershipStatusRecordName = "membership.membershupState";

        private MembershipRepo() {
        }

        public UserMembershipState load() {
            SharedPreferences preferences = UserMembership.this.mContext.getSharedPreferences(UserMembership.PREFERENCES_MEMBERSHIP, 0);
            UserMembershipState rv = new UserMembershipState();
            rv.setBillingState(UserMembershipState.BillingResult.valueOf(preferences.getString(BillingStateRecordName, UserMembershipState.DEFAULT.getBillingState().toString())));
            rv.setMembershipStatus(UserMembershipState.UserMembershipStatus.valueOf(preferences.getString(UserMembershipStatusRecordName, UserMembershipState.DEFAULT.getMembershipStatus().toString())));
            rv.setPlanId(preferences.getInt(MembershipPlanIdRecordName, UserMembershipState.DEFAULT.getPlanId()));
            rv.setBillingMessageDisplayed(preferences.getBoolean(BillingMessageDisplayedRecordName, UserMembershipState.DEFAULT.isBillingMessageDisplayed()));
            return rv;
        }

        public void save(UserMembershipState state) {
            SharedPreferences.Editor editor = UserMembership.this.mContext.getSharedPreferences(UserMembership.PREFERENCES_MEMBERSHIP, 0).edit();
            editor.putString(UserMembershipStatusRecordName, state.getMembershipStatus().toString());
            editor.putString(BillingStateRecordName, state.getBillingState().toString());
            editor.putBoolean(BillingMessageDisplayedRecordName, state.isBillingMessageDisplayed());
            editor.putInt(MembershipPlanIdRecordName, state.getPlanId());
            editor.commit();
        }
    }
}
