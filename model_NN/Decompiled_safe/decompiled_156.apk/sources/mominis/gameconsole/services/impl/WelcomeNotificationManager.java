package mominis.gameconsole.services.impl;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.inject.Inject;
import java.util.concurrent.Executor;
import mominis.common.utils.IHttpClientFactory;
import mominis.gameconsole.com.R;
import mominis.gameconsole.common.IProcessListener;
import mominis.gameconsole.common.ProgressEventArgs;
import mominis.gameconsole.common.ResourceHelper;
import mominis.gameconsole.services.IWelcomeNotificationManager;

public class WelcomeNotificationManager implements IWelcomeNotificationManager {
    public static final String ASSET_NAME_FIRST_NOTIFICATION = "WelcomeMessage";
    private static final String KEY__NOTIFICATION_MESSAGE = "messageText";
    private static final String KEY__VERSION_CODE = "version";
    private static final String PREFERENCES_WELCOME_NOTIFICATION = "welcome_notifications";
    private static final String RECORD_NAME_HAS_NEW_MESSAGE = "welcome_notifications.has_new_message";
    private static final String RECORD_NAME_LATEST_NOTIFICATION_VERSION = "welcome_notifications.last_version";
    private static final String RECORD_NAME_MESSAGE_TEXT = "welcome_notifications.message_text";
    private static final String TAG = "WELCOME_NOTIFICATION";
    private Context mContext;
    private long mCurrentNotificationVer;
    private Executor mExecutor;
    private boolean mHasNewMessage;
    private IHttpClientFactory mHttpFactory;
    private Object mLockObj = new Object();
    private String mMessage;
    private String mUrl;

    @Inject
    public WelcomeNotificationManager(Context context, IHttpClientFactory factory, Executor executor) {
        this.mHttpFactory = factory;
        this.mContext = context;
        this.mExecutor = executor;
        this.mUrl = this.mContext.getString(R.string.welcome_notification_url);
        loadState();
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean checkForNewNotification() {
        /*
            r15 = this;
            r14 = 1
            r13 = 0
            mominis.common.utils.IHttpClientFactory r9 = r15.mHttpFactory
            mominis.common.utils.IHttpClient r0 = r9.create()
            org.apache.http.client.methods.HttpGet r3 = new org.apache.http.client.methods.HttpGet
            java.lang.String r9 = r15.mUrl
            r3.<init>(r9)
            org.apache.http.HttpResponse r7 = r0.execute(r3)     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x0083, JSONException -> 0x0090, Exception -> 0x009d }
            org.apache.http.HttpEntity r2 = r7.getEntity()     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x0083, JSONException -> 0x0090, Exception -> 0x009d }
            java.io.InputStream r9 = r2.getContent()     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x0083, JSONException -> 0x0090, Exception -> 0x009d }
            java.lang.String r8 = mominis.common.utils.StreamUtils.readToEnd(r9)     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x0083, JSONException -> 0x0090, Exception -> 0x009d }
            java.lang.String r9 = "WELCOME_NOTIFICATION"
            java.lang.String r10 = "decoded json object %s"
            r11 = 1
            java.lang.Object[] r11 = new java.lang.Object[r11]     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x0083, JSONException -> 0x0090, Exception -> 0x009d }
            r12 = 0
            r11[r12] = r8     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x0083, JSONException -> 0x0090, Exception -> 0x009d }
            java.lang.String r10 = java.lang.String.format(r10, r11)     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x0083, JSONException -> 0x0090, Exception -> 0x009d }
            android.util.Log.i(r9, r10)     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x0083, JSONException -> 0x0090, Exception -> 0x009d }
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x0083, JSONException -> 0x0090, Exception -> 0x009d }
            r4.<init>(r8)     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x0083, JSONException -> 0x0090, Exception -> 0x009d }
            java.lang.String r9 = "version"
            boolean r9 = r4.has(r9)     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x0083, JSONException -> 0x0090, Exception -> 0x009d }
            if (r9 == 0) goto L_0x0045
            java.lang.String r9 = "messageText"
            boolean r9 = r4.has(r9)     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x0083, JSONException -> 0x0090, Exception -> 0x009d }
            if (r9 != 0) goto L_0x004e
        L_0x0045:
            java.lang.String r9 = "WELCOME_NOTIFICATION"
            java.lang.String r10 = "Invalid json format - a field is missing"
            android.util.Log.e(r9, r10)     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x0083, JSONException -> 0x0090, Exception -> 0x009d }
            r9 = r13
        L_0x004d:
            return r9
        L_0x004e:
            java.lang.Object r9 = r15.mLockObj     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x0083, JSONException -> 0x0090, Exception -> 0x009d }
            monitor-enter(r9)     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x0083, JSONException -> 0x0090, Exception -> 0x009d }
            java.lang.String r10 = "version"
            long r5 = r4.getLong(r10)     // Catch:{ all -> 0x0073 }
            java.lang.String r10 = "messageText"
            java.lang.String r10 = r4.getString(r10)     // Catch:{ all -> 0x0073 }
            r15.mMessage = r10     // Catch:{ all -> 0x0073 }
            long r10 = r15.mCurrentNotificationVer     // Catch:{ all -> 0x0073 }
            int r10 = (r10 > r5 ? 1 : (r10 == r5 ? 0 : -1))
            if (r10 >= 0) goto L_0x0071
            r10 = r14
        L_0x0066:
            r15.mHasNewMessage = r10     // Catch:{ all -> 0x0073 }
            r15.mCurrentNotificationVer = r5     // Catch:{ all -> 0x0073 }
            r15.saveState()     // Catch:{ all -> 0x0073 }
            monitor-exit(r9)     // Catch:{ all -> 0x0073 }
        L_0x006e:
            boolean r9 = r15.mHasNewMessage
            goto L_0x004d
        L_0x0071:
            r10 = r13
            goto L_0x0066
        L_0x0073:
            r10 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x0073 }
            throw r10     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x0083, JSONException -> 0x0090, Exception -> 0x009d }
        L_0x0076:
            r9 = move-exception
            r1 = r9
            java.lang.String r9 = "WELCOME_NOTIFICATION"
            java.lang.String r10 = "Client protocol exception raised while reading new notification data"
            android.util.Log.e(r9, r10, r1)
            r1.printStackTrace()
            goto L_0x006e
        L_0x0083:
            r9 = move-exception
            r1 = r9
            java.lang.String r9 = "WELCOME_NOTIFICATION"
            java.lang.String r10 = "IOException raised while reading new notification data"
            android.util.Log.w(r9, r10, r1)
            r1.printStackTrace()
            goto L_0x006e
        L_0x0090:
            r9 = move-exception
            r1 = r9
            java.lang.String r9 = "WELCOME_NOTIFICATION"
            java.lang.String r10 = "Invalid Json received while reading new notification data"
            android.util.Log.e(r9, r10, r1)
            r1.printStackTrace()
            goto L_0x006e
        L_0x009d:
            r9 = move-exception
            r1 = r9
            java.lang.String r9 = "WELCOME_NOTIFICATION"
            java.lang.String r10 = "An unknown exception was detected"
            android.util.Log.e(r9, r10, r1)
            r1.printStackTrace()
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: mominis.gameconsole.services.impl.WelcomeNotificationManager.checkForNewNotification():boolean");
    }

    public void checkForNewNotification(final IProcessListener<ProgressEventArgs> listener) {
        this.mExecutor.execute(new Runnable() {
            public void run() {
                if (listener != null) {
                    listener.onStart(WelcomeNotificationManager.this, new ProgressEventArgs(0));
                }
                WelcomeNotificationManager.this.checkForNewNotification();
                if (listener != null) {
                    listener.onEnd(WelcomeNotificationManager.this, new ProgressEventArgs(100));
                }
            }
        });
    }

    public void deprecateMessage() {
        synchronized (this.mLockObj) {
            this.mHasNewMessage = false;
            saveState();
        }
    }

    private void saveState() {
        SharedPreferences.Editor editor = this.mContext.getSharedPreferences(PREFERENCES_WELCOME_NOTIFICATION, 0).edit();
        editor.putLong(RECORD_NAME_LATEST_NOTIFICATION_VERSION, this.mCurrentNotificationVer);
        editor.putBoolean(RECORD_NAME_HAS_NEW_MESSAGE, this.mHasNewMessage);
        editor.putString(RECORD_NAME_MESSAGE_TEXT, this.mMessage);
        editor.commit();
    }

    private void loadState() {
        synchronized (this.mLockObj) {
            SharedPreferences preferences = this.mContext.getSharedPreferences(PREFERENCES_WELCOME_NOTIFICATION, 0);
            this.mCurrentNotificationVer = preferences.getLong(RECORD_NAME_LATEST_NOTIFICATION_VERSION, -1);
            this.mHasNewMessage = preferences.getBoolean(RECORD_NAME_HAS_NEW_MESSAGE, true);
            this.mMessage = preferences.getString(RECORD_NAME_MESSAGE_TEXT, "");
            this.mMessage = preferences.getString(RECORD_NAME_MESSAGE_TEXT, null);
            if (this.mMessage == null) {
                this.mMessage = ResourceHelper.readAsset(this.mContext, ASSET_NAME_FIRST_NOTIFICATION).toString();
            }
        }
    }

    public String getMessage() {
        return this.mMessage;
    }

    public boolean isNewNotificationAvailable() {
        return this.mHasNewMessage;
    }
}
