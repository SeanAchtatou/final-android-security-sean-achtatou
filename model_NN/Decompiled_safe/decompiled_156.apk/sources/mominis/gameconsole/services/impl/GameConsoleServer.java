package mominis.gameconsole.services.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.google.inject.Inject;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import mominis.common.utils.HttpUtils;
import mominis.common.utils.IHttpClientFactory;
import mominis.common.utils.StreamUtils;
import mominis.gameconsole.com.R;
import mominis.gameconsole.core.models.LocaleInfo;
import mominis.gameconsole.core.models.Price;
import mominis.gameconsole.core.models.ProgramFacadeResult;
import mominis.gameconsole.core.models.PurchasePlan;
import mominis.gameconsole.core.models.UserIdentifier;
import mominis.gameconsole.services.IGameConsoleServer;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class GameConsoleServer implements IGameConsoleServer {
    public static final int APP_ID__ALL_YOU_CAN_EAT = -1;
    private static final String COMMAND__COMPLETE_WITHOUT_BILLING = "ShoppingService/json/CompleteWithoutBilling";
    private static final String COMMAND__GET_OFFERING = "ShoppingService/json/GetOffering";
    private static final String COMMAND__IS_IN_PLAN = "ShoppingService/json/IsInPlan";
    private static final String COMMAND__LOGIN = "AccountService/json/Login";
    private static final String COMMAND__REGISTER_AND_PURCHASE_WITH_PHONE = "ProgramFacadeService/json/RegisterAndPurchaseWithPhone";
    private static final String KEY__APP_ID = "appID";
    private static final String KEY__AUTHENTICATION_TOKEN = "Token";
    private static final String KEY__CHANNEL_ID = "channelID";
    private static final String KEY__DATA = "d";
    private static final String KEY__IS_IN_PLAN_ID = "planID";
    private static final String KEY__LOCALE = "locale";
    private static final String KEY__LOGIN_USER_ID = "UserID";
    private static final String KEY__PASSWORD = "password";
    private static final String KEY__PHONE_NUMBER = "phoneNumber";
    private static final String KEY__PLAN = "Plan";
    private static final String KEY__PLAN_CHANNEL_ID = "ChannelID";
    private static final String KEY__PLAN_DESCRIPTION = "Description";
    private static final String KEY__PLAN_ID = "Id";
    private static final String KEY__PLAN_NAME = "Name";
    private static final String KEY__PLAN_NOTES = "PleaseNote";
    private static final String KEY__PLAN_PRICE_AMOUNT = "Amount";
    private static final String KEY__PLAN_PRICE_CURRENCY_CODE = "Currency";
    private static final String KEY__PLAN_PRICE_CURRENCY_SIGN = "Sign";
    private static final String KEY__PLAN_TOS = "TermsOfUse";
    private static final String KEY__PLAN_TYPE = "PlanType";
    private static final String KEY__PRICE = "Price";
    private static final String KEY__PRICE_CURRENCY = "Currency";
    private static final String KEY__TRANSACTION_ID = "transactionID";
    private static final String KEY__USERNAME = "username";
    private static final String KEY__USER_ID = "userID";
    private static final String LOGIN_COOKIE_NAME = "MoCookie";
    public static final String PLAN_TYPE__ALL_YOU_CAN_EAT = "ALL_YOU_CAN_EAT";
    private static final String PREFERENCES_GAMECONSOLESERVER = "GameConsoleServeer";
    private static final String PREFERENCES_GAMECONSOLESERVER_AUTHENTICATION_TOKEN = "GameConsoleServer.token";
    private static final String TAG = "GameConsoleServer";
    private static String mAuthenticationToken = null;
    private static String mServerAddress;
    private static String mServerUrl;
    private Context mContext;
    private IHttpClientFactory mHttpClientFactory;

    @Inject
    public GameConsoleServer(Context ctx, IHttpClientFactory httpClientFactory) throws Exception {
        this.mContext = ctx;
        this.mHttpClientFactory = httpClientFactory;
        mAuthenticationToken = this.mContext.getSharedPreferences(PREFERENCES_GAMECONSOLESERVER, 0).getString(PREFERENCES_GAMECONSOLESERVER_AUTHENTICATION_TOKEN, null);
        LoadServerAddress();
        if (mServerAddress == null || mServerUrl == null) {
            throw new Exception("Could not load server configuration - one of the paramerters are missing");
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 10 */
    public boolean Login(UserIdentifier userIdentifier) {
        Hashtable<String, Object> loginParams = new Hashtable<>();
        loginParams.put(KEY__USERNAME, userIdentifier.Username);
        loginParams.put(KEY__PASSWORD, userIdentifier.Password);
        Hashtable<String, Object> loginResults = SendCommand(COMMAND__LOGIN, loginParams, true);
        if (loginResults == null) {
            Log.i(TAG, "could not login to server - SendCommand returned null");
            return false;
        }
        Hashtable<String, Object> results = (Hashtable) loginResults.get(KEY__DATA);
        if (results == null) {
            Log.i(TAG, String.format("could not login to server - %s member is missing or empty", KEY__DATA));
            return false;
        } else if (!results.containsKey(KEY__AUTHENTICATION_TOKEN)) {
            Log.w(TAG, "could not login to server - authentication token does not exist");
            return false;
        } else {
            mAuthenticationToken = (String) results.get(KEY__AUTHENTICATION_TOKEN);
            SharedPreferences.Editor editor = this.mContext.getSharedPreferences(PREFERENCES_GAMECONSOLESERVER, 0).edit();
            editor.putString(PREFERENCES_GAMECONSOLESERVER_AUTHENTICATION_TOKEN, mAuthenticationToken);
            editor.commit();
            userIdentifier.UserID = ((Integer) results.get(KEY__LOGIN_USER_ID)).intValue();
            return true;
        }
    }

    public ProgramFacadeResult registerAndPurchase(int planId, String phoneNumber, int channelId) {
        Hashtable<String, Object> purchaseParams = new Hashtable<>();
        purchaseParams.put(KEY__CHANNEL_ID, Integer.toString(channelId));
        purchaseParams.put(KEY__PLAN_ID, Integer.toString(planId));
        purchaseParams.put(KEY__APP_ID, Integer.toString(-1));
        purchaseParams.put(KEY__PHONE_NUMBER, phoneNumber);
        Hashtable<String, Object> purchaseResults = SendCommand(COMMAND__REGISTER_AND_PURCHASE_WITH_PHONE, purchaseParams, true);
        if (purchaseResults == null) {
            return null;
        }
        return ProgramFacadeResult.fromKeyValue(purchaseResults);
    }

    public boolean completeWithoutPayment(int transactionID) {
        Hashtable<String, Object> completeParams = new Hashtable<>();
        completeParams.put(KEY__TRANSACTION_ID, Integer.valueOf(transactionID));
        Hashtable<String, String> loginDetails = new Hashtable<>();
        loginDetails.put(LOGIN_COOKIE_NAME, mAuthenticationToken);
        return ((Boolean) SendCommand(COMMAND__COMPLETE_WITHOUT_BILLING, completeParams, true, loginDetails).get(KEY__DATA)).booleanValue();
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public boolean isInPlan(int userId, int planID) {
        Hashtable<String, Object> isInPlanParams = new Hashtable<>();
        isInPlanParams.put(KEY__IS_IN_PLAN_ID, Integer.valueOf(planID));
        isInPlanParams.put(KEY__USER_ID, Integer.valueOf(userId));
        Hashtable<String, String> loginDetails = new Hashtable<>();
        loginDetails.put(LOGIN_COOKIE_NAME, mAuthenticationToken);
        Hashtable<String, Object> isInPlanResults = SendCommand(COMMAND__IS_IN_PLAN, isInPlanParams, false, loginDetails);
        if (isInPlanResults == null) {
            return false;
        }
        return ((Boolean) isInPlanResults.get(KEY__DATA)).booleanValue();
    }

    /* Debug info: failed to restart local var, previous not found, register: 15 */
    public List<PurchasePlan> getPurchasePlans(int channelId, LocaleInfo localeInfo) {
        Hashtable<String, Object> getAvailablePlansParams = new Hashtable<>();
        getAvailablePlansParams.put(KEY__CHANNEL_ID, Integer.toString(channelId));
        getAvailablePlansParams.put(KEY__PLAN_TYPE, PLAN_TYPE__ALL_YOU_CAN_EAT);
        getAvailablePlansParams.put(KEY__LOCALE, localeInfo.Locale);
        getAvailablePlansParams.put(KEY__APP_ID, Integer.toString(-1));
        Hashtable<String, Object> getAvailablePlansResults = SendCommand(COMMAND__GET_OFFERING, getAvailablePlansParams, false);
        if (getAvailablePlansResults == null) {
            return null;
        }
        List<Hashtable<String, Object>> plans = (ArrayList) getAvailablePlansResults.get(KEY__DATA);
        ArrayList<PurchasePlan> result = new ArrayList<>();
        for (int i = 0; i < plans.size(); i++) {
            Hashtable<String, Object> currentPlan = plans.get(i);
            Hashtable<String, Object> planDetails = (Hashtable) currentPlan.get(KEY__PLAN);
            PurchasePlan newPlan = new PurchasePlan();
            newPlan.Id = ((Integer) planDetails.get(KEY__PLAN_ID)).intValue();
            newPlan.ChannelId = ((Integer) planDetails.get(KEY__PLAN_CHANNEL_ID)).intValue();
            newPlan.Description = (String) planDetails.get(KEY__PLAN_DESCRIPTION);
            newPlan.Name = (String) planDetails.get(KEY__PLAN_NAME);
            newPlan.TermsOfUse = (String) planDetails.get(KEY__PLAN_TOS);
            newPlan.PlanType = PurchasePlan.PlanType.valueOf((String) planDetails.get(KEY__PLAN_TYPE));
            newPlan.PlanNotes = (ArrayList) planDetails.get(KEY__PLAN_NOTES);
            Hashtable<String, Object> planPrice = (Hashtable) currentPlan.get(KEY__PRICE);
            Hashtable<String, Object> planPriceCurrency = (Hashtable) planPrice.get("Currency");
            newPlan.PlanPrice = new Price();
            Object priceValue = planPrice.get(KEY__PLAN_PRICE_AMOUNT);
            if (priceValue instanceof Integer) {
                newPlan.PlanPrice.Value = ((Integer) priceValue).doubleValue();
            } else if (priceValue instanceof Float) {
                newPlan.PlanPrice.Value = ((Float) priceValue).doubleValue();
            } else if (priceValue instanceof Double) {
                newPlan.PlanPrice.Value = ((Double) priceValue).doubleValue();
            }
            newPlan.PlanPrice.CurrencyCode = (String) planPriceCurrency.get("Currency");
            newPlan.PlanPrice.Symbol = (String) planPriceCurrency.get(KEY__PLAN_PRICE_CURRENCY_SIGN);
            result.add(newPlan);
        }
        return result;
    }

    private Hashtable<String, Object> SendCommand(String command, Hashtable<String, Object> parameters, Boolean isPost) {
        return SendCommand(command, parameters, isPost, null);
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    private Hashtable<String, Object> SendCommand(String command, Hashtable<String, Object> parameters, Boolean isPost, Hashtable<String, String> cookies) {
        InputStream resultStream;
        if (isPost.booleanValue()) {
            resultStream = HttpPostData(mServerAddress, mServerUrl + command, parameters, cookies);
        } else {
            resultStream = HttpGetData(mServerAddress, mServerUrl + command, parameters, cookies);
        }
        if (resultStream == null) {
            return null;
        }
        return (Hashtable) JsonDecodeString(StreamUtils.readToEnd(resultStream));
    }

    private static Object JsonDecodeString(String input) {
        try {
            return JsonDecodeObject((JSONObject) new JSONTokener(input).nextValue());
        } catch (Exception e) {
            return null;
        }
    }

    private static Object JsonDecodeObject(Object input) {
        if (input instanceof JSONArray) {
            List<Object> arr = new ArrayList<>();
            try {
                JSONArray jarray = (JSONArray) input;
                for (int i = 0; i < jarray.length(); i++) {
                    arr.add(JsonDecodeObject(jarray.get(i)));
                }
                return arr;
            } catch (Exception e) {
                return null;
            }
        } else if (!(input instanceof JSONObject)) {
            return input;
        } else {
            Hashtable<String, Object> values = new Hashtable<>();
            JSONObject inputDict = (JSONObject) input;
            try {
                Iterator<String> iter = inputDict.keys();
                while (iter.hasNext()) {
                    String key = iter.next();
                    values.put(key, JsonDecodeObject(inputDict.get(key)));
                }
                return values;
            } catch (Exception e2) {
                return null;
            }
        }
    }

    private void LoadServerAddress() {
        mServerAddress = this.mContext.getString(R.string.game_console_address);
        mServerUrl = this.mContext.getString(R.string.game_console_base_url);
    }

    private InputStream HttpGetData(String host, String url, Hashtable<String, Object> params, Hashtable<String, String> cookies) {
        try {
            return HttpUtils.executeHttpGet(this.mHttpClientFactory.create(), url, host, params, cookies).getEntity().getContent();
        } catch (Exception e) {
            return null;
        }
    }

    private InputStream HttpPostData(String host, String url, Hashtable<String, Object> params, Hashtable<String, String> cookies) {
        HttpClient httpClient = this.mHttpClientFactory.create();
        HttpPost httpPost = new HttpPost(host + url);
        try {
            JSONObject json = new JSONObject();
            Enumeration<String> e = params.keys();
            while (e.hasMoreElements()) {
                String key = e.nextElement();
                json.put(key, params.get(key));
            }
            httpPost.setEntity(new StringEntity(json.toString(), "UTF-8"));
            httpPost.setHeader("Content-Type", "application/json");
            if (cookies != null) {
                Enumeration<String> iter = cookies.keys();
                while (iter.hasMoreElements()) {
                    String key2 = iter.nextElement();
                    httpPost.setHeader("Cookie", key2 + "=" + cookies.get(key2));
                }
            }
            return httpClient.execute(httpPost).getEntity().getContent();
        } catch (Exception e2) {
            Exception exc = e2;
            return null;
        }
    }
}
