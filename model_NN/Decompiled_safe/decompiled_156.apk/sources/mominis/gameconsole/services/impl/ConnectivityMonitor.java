package mominis.gameconsole.services.impl;

import android.content.Context;
import android.net.ConnectivityManager;
import android.telephony.TelephonyManager;
import com.google.inject.Inject;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import mominis.gameconsole.com.R;
import mominis.gameconsole.services.IConnectivityMonitor;

public class ConnectivityMonitor implements IConnectivityMonitor {
    private ConnectivityManager mConnectiviryMgr;
    private String mConsoleHost = this.mContext.getString(R.string.game_console_hostname);
    private Context mContext;
    private TelephonyManager mTeleManager;

    @Inject
    public ConnectivityMonitor(Context c, TelephonyManager teleManager, ConnectivityManager conMgr) {
        this.mConnectiviryMgr = conMgr;
        this.mTeleManager = teleManager;
        this.mContext = c;
    }

    public Boolean isConnected() {
        boolean res;
        boolean wifi;
        boolean internet;
        boolean google;
        boolean z;
        if (this.mConnectiviryMgr.getActiveNetworkInfo() == null || !this.mConnectiviryMgr.getActiveNetworkInfo().isConnectedOrConnecting()) {
            res = false;
        } else {
            res = true;
        }
        if (this.mConnectiviryMgr.getNetworkInfo(1) == null || !this.mConnectiviryMgr.getNetworkInfo(1).isConnectedOrConnecting()) {
            wifi = false;
        } else {
            wifi = true;
        }
        if (this.mConnectiviryMgr.getNetworkInfo(0) == null || !this.mConnectiviryMgr.getNetworkInfo(0).isConnectedOrConnecting()) {
            internet = false;
        } else {
            internet = true;
        }
        try {
            google = InetAddress.getByName(this.mConsoleHost).isReachable(500);
        } catch (UnknownHostException e) {
            google = false;
        } catch (IOException e2) {
            google = false;
        }
        if (res || internet || wifi || google) {
            z = true;
        } else {
            z = false;
        }
        return Boolean.valueOf(z);
    }

    public Boolean isRoaming() {
        boolean wifi;
        boolean z;
        boolean isRoaming = this.mTeleManager.isNetworkRoaming();
        if (this.mConnectiviryMgr.getNetworkInfo(1) == null || !this.mConnectiviryMgr.getNetworkInfo(1).isConnected()) {
            wifi = false;
        } else {
            wifi = true;
        }
        if (!isRoaming || wifi) {
            z = false;
        } else {
            z = true;
        }
        return Boolean.valueOf(z);
    }
}
