package mominis.gameconsole.services.impl;

import android.content.Context;
import com.google.inject.Inject;
import mominis.gameconsole.services.ISubscriptionManager;

public class SubscriptionManager implements ISubscriptionManager {
    private static final String PREFERENCES_SUBSCRIPTION = "subscription";
    private static final String PREFERENCES_SUBSCRIPTION_REGISTERED = "subscription.registered";
    private Context mContext;

    @Inject
    public SubscriptionManager(Context c) {
        this.mContext = c;
    }

    public Boolean IsUserSubscribed() {
        return Boolean.valueOf(this.mContext.getSharedPreferences(PREFERENCES_SUBSCRIPTION, 0).getBoolean(PREFERENCES_SUBSCRIPTION_REGISTERED, false));
    }

    public void SubscribeUser() {
        this.mContext.getSharedPreferences(PREFERENCES_SUBSCRIPTION, 0).edit().putBoolean(PREFERENCES_SUBSCRIPTION_REGISTERED, true).commit();
    }

    public void UnsubscribeUser() {
        this.mContext.getSharedPreferences(PREFERENCES_SUBSCRIPTION, 0).edit().putBoolean(PREFERENCES_SUBSCRIPTION_REGISTERED, false).commit();
    }
}
