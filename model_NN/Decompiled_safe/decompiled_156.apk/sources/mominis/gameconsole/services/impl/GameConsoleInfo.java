package mominis.gameconsole.services.impl;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.google.inject.Inject;
import mominis.gameconsole.services.IGameConsoleInfo;

public class GameConsoleInfo implements IGameConsoleInfo {
    private Context mContext;

    @Inject
    public GameConsoleInfo(Context context) {
        this.mContext = context;
    }

    public PackageInfo getInfo() throws PackageManager.NameNotFoundException {
        return this.mContext.getPackageManager().getPackageInfo(this.mContext.getPackageName(), 0);
    }
}
