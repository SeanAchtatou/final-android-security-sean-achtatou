package mominis.gameconsole.services.impl;

import android.content.Context;
import android.util.Log;
import com.google.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import mominis.common.mvc.IObserver;
import mominis.common.mvc.ListChangedEventArgs;
import mominis.common.mvc.ObservableList;
import mominis.common.utils.IHttpClientFactory;
import mominis.common.utils.StreamUtils;
import mominis.gameconsole.com.R;
import mominis.gameconsole.core.models.Application;
import mominis.gameconsole.core.repositories.DBConsts;
import mominis.gameconsole.core.repositories.IRemoteRepository;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RemoteRepository implements IRemoteRepository {
    /* access modifiers changed from: private */
    public String TAG = "Remote Repository";
    /* access modifiers changed from: private */
    public Object lockObj = new Object();
    /* access modifiers changed from: private */
    public ObservableList<Application> mApps = new ObservableList<>(this);
    private Executor mExecutor;
    /* access modifiers changed from: private */
    public IHttpClientFactory mHttpClientFactory;
    private int mNumAppsLoaded = 0;
    private int mNumAppsToLoad = 0;
    private boolean mUpdating = false;
    /* access modifiers changed from: private */
    public String mUrl;

    static /* synthetic */ int access$808(RemoteRepository x0) {
        int i = x0.mNumAppsLoaded;
        x0.mNumAppsLoaded = i + 1;
        return i;
    }

    @Inject
    public RemoteRepository(Context context, Executor executor, IHttpClientFactory httpClientFactory) {
        this.mExecutor = executor;
        this.mHttpClientFactory = httpClientFactory;
        this.mUrl = context.getString(R.string.game_catalog_url);
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public Application get(long id) {
        for (int i = 0; i < this.mApps.size(); i++) {
            if (this.mApps.get(i).getID() == id) {
                return this.mApps.get(i);
            }
        }
        return null;
    }

    public List<Application> getAll() {
        return Collections.unmodifiableList(this.mApps);
    }

    public int size() {
        return this.mApps.size();
    }

    public void registerObserver(IObserver<ListChangedEventArgs> observer) {
        this.mApps.registerObserver(observer);
    }

    public void unregisterObserver(IObserver<ListChangedEventArgs> observer) {
        this.mApps.unregisterObserver(observer);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0017, code lost:
        if (r3.mApps.size() <= 0) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0019, code lost:
        r3.mApps.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001e, code lost:
        r3.mExecutor.execute(new mominis.gameconsole.services.impl.RemoteRepository.AnonymousClass1(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean refresh() {
        /*
            r3 = this;
            r2 = 1
            java.lang.Object r0 = r3.lockObj
            monitor-enter(r0)
            boolean r1 = r3.mUpdating     // Catch:{ all -> 0x002a }
            if (r1 == 0) goto L_0x000c
            r1 = 0
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            r0 = r1
        L_0x000b:
            return r0
        L_0x000c:
            r1 = 1
            r3.initIsUpdatingState(r1)     // Catch:{ all -> 0x002a }
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            mominis.common.mvc.ObservableList<mominis.gameconsole.core.models.Application> r0 = r3.mApps
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x001e
            mominis.common.mvc.ObservableList<mominis.gameconsole.core.models.Application> r0 = r3.mApps
            r0.clear()
        L_0x001e:
            java.util.concurrent.Executor r0 = r3.mExecutor
            mominis.gameconsole.services.impl.RemoteRepository$1 r1 = new mominis.gameconsole.services.impl.RemoteRepository$1
            r1.<init>()
            r0.execute(r1)
            r0 = r2
            goto L_0x000b
        L_0x002a:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: mominis.gameconsole.services.impl.RemoteRepository.refresh():boolean");
    }

    /* access modifiers changed from: private */
    public void parse(JSONObject json) throws IOException {
        try {
            ArrayList<Application> newApps = new ArrayList<>();
            JSONArray array = json.getJSONArray("d");
            int count = array.length();
            for (int i = 0; i < count; i++) {
                if (!array.isNull(i)) {
                    JSONObject obj = array.getJSONObject(i);
                    long id = obj.getLong("ID");
                    String appUrl = obj.getString("AppUri");
                    String name = obj.getString("Name");
                    String pack = obj.getString("Package");
                    String thumUrl = obj.getString("ThumnailUrl");
                    boolean isFree = false;
                    if (obj.has(DBConsts.FREE_NAME)) {
                        isFree = obj.getBoolean(DBConsts.FREE_NAME);
                    }
                    Boolean isNew = Boolean.valueOf(obj.optBoolean("new", false));
                    Application current = new Application();
                    current.setID(id);
                    current.setAPKPath(appUrl);
                    current.setName(name);
                    current.setThumbnailUrl(thumUrl);
                    current.setPackage(pack);
                    current.setViewable(true);
                    current.setNew(isNew);
                    current.setState(Application.State.Remote);
                    current.setFree(isFree);
                    int index = i;
                    synchronized (this.lockObj) {
                        this.mNumAppsToLoad = this.mNumAppsToLoad + 1;
                    }
                    final String str = thumUrl;
                    final Application application = current;
                    final int i2 = index;
                    this.mExecutor.execute(new Runnable() {
                        public void run() {
                            try {
                                application.setThumbnail(RemoteRepository.this.getBitmap(str));
                                if (i2 >= RemoteRepository.this.mApps.size()) {
                                    Log.e(RemoteRepository.this.TAG, String.format("Trying to setThumbnail for non existing app - index: %d; size: %d", Integer.valueOf(i2), Integer.valueOf(RemoteRepository.this.mApps.size())));
                                } else {
                                    RemoteRepository.this.mApps.set(i2, application);
                                }
                                synchronized (RemoteRepository.this.lockObj) {
                                    RemoteRepository.access$808(RemoteRepository.this);
                                }
                                RemoteRepository.this.resetUpdatingStateIfNeeded();
                            } catch (IOException e) {
                                synchronized (RemoteRepository.this.lockObj) {
                                    RemoteRepository.access$808(RemoteRepository.this);
                                    RemoteRepository.this.resetUpdatingStateIfNeeded();
                                }
                            } catch (Throwable th) {
                                synchronized (RemoteRepository.this.lockObj) {
                                    RemoteRepository.access$808(RemoteRepository.this);
                                    RemoteRepository.this.resetUpdatingStateIfNeeded();
                                    throw th;
                                }
                            }
                        }
                    });
                    newApps.add(current);
                }
            }
            this.mApps.addAll(newApps);
            resetUpdatingStateIfNeeded();
        } catch (JSONException e) {
            Log.e(this.TAG, e.toString());
            initIsUpdatingState(false);
        }
    }

    /* access modifiers changed from: private */
    public void resetUpdatingStateIfNeeded() {
        synchronized (this.lockObj) {
            this.mNumAppsLoaded++;
            if (this.mNumAppsLoaded >= this.mNumAppsToLoad) {
                initIsUpdatingState(false);
            }
        }
    }

    /* access modifiers changed from: private */
    public void initIsUpdatingState(boolean isUpdating) {
        synchronized (this.lockObj) {
            this.mUpdating = isUpdating;
            this.mNumAppsLoaded = 0;
            this.mNumAppsToLoad = 0;
        }
    }

    /* access modifiers changed from: private */
    public byte[] getBitmap(String stringUri) throws IOException {
        Log.d(this.TAG, String.format("getting bitmap from url - %s", stringUri));
        return StreamUtils.readAllBytes(this.mHttpClientFactory.create().execute(new HttpGet(stringUri)).getEntity().getContent());
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public Application get(String packageName) throws IOException {
        for (int i = 0; i < this.mApps.size(); i++) {
            if (this.mApps.get(i).getPackage().equals(packageName)) {
                return this.mApps.get(i);
            }
        }
        return null;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Application getByIndex(int index) {
        if (index < 0 || index > this.mApps.size()) {
            return null;
        }
        return this.mApps.get(index);
    }
}
