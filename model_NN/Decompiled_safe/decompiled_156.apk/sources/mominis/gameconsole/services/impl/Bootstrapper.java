package mominis.gameconsole.services.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.google.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import mominis.common.utils.StreamUtils;
import mominis.gameconsole.com.R;
import mominis.gameconsole.core.models.Application;
import mominis.gameconsole.services.IAppManager;

public class Bootstrapper {
    private static final String PREFERENCES_BOOTSTRAP = "bootstrap";
    private static final String PREFERENCES_BOOTSTRAP_READY = "bootstrap.ready";
    private static final String PREINSTALLED_APK_PATH = "preinstalled.apk";
    private static final String TAG = "Bootstrapper";
    private static Object mSyncObject = new Object();
    private IAppManager mAppMgr;
    private Context mContext;
    private final String mPreloadedGameName = this.mContext.getString(R.string.preloaded_game_name);
    private final String mPreloadedGamePackageName = this.mContext.getString(R.string.preloaded_game_package_name);

    @Inject
    public Bootstrapper(Context context, IAppManager mgr) {
        this.mContext = context;
        this.mAppMgr = mgr;
    }

    public void activate() throws IOException {
        SharedPreferences preferences = this.mContext.getSharedPreferences(PREFERENCES_BOOTSTRAP, 0);
        synchronized (mSyncObject) {
            if (!preferences.getBoolean(PREFERENCES_BOOTSTRAP_READY, false)) {
                Log.d(TAG, "bootstrapper running for the first time");
                addToLocalRepo(PREINSTALLED_APK_PATH, this.mPreloadedGamePackageName, this.mPreloadedGameName, R.drawable.preinstalled);
                preferences.edit().putBoolean(PREFERENCES_BOOTSTRAP_READY, true).commit();
            }
        }
    }

    private void addToLocalRepo(String apkAssetName, String packageName, String gameName, int resourceId) {
        Application app = new Application();
        app.setName(gameName);
        app.setPackage(packageName);
        app.setState(Application.State.Downloaded);
        Log.d(TAG, String.format("adding app %s to local repository", app.getName()));
        try {
            app.setThumbnail(StreamUtils.readAllBytes(this.mContext.getResources().openRawResource(resourceId)));
        } catch (IOException e) {
            IOException e1 = e;
            Log.e(TAG, String.format("could not read app image from resources - %s", app.getName()), e1);
            e1.printStackTrace();
        }
        app.setFree(true);
        InputStream is = null;
        try {
            is = this.mContext.getAssets().open(PREINSTALLED_APK_PATH);
            this.mAppMgr.writeApp(is, app);
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e2) {
                    Log.e(TAG, "coule not close input stream", e2);
                }
            }
        } catch (IOException e3) {
            IOException e4 = e3;
            Log.e(TAG, String.format("could not write app %s to the local repository", app.getName()), e4);
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e5) {
                    Log.e(TAG, "coule not close input stream", e5);
                }
            }
        } catch (Throwable th) {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e6) {
                    Log.e(TAG, "coule not close input stream", e6);
                }
            }
            throw th;
        }
    }
}
