package mominis.gameconsole.services.impl;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import java.util.Hashtable;
import mominis.common.mvc.INavigationManager;
import mominis.common.mvc.IView;

public class NavigationManager implements INavigationManager {
    public final String ExtraDataKey = "KEY";
    private Hashtable<String, Class<?>> mViews = new Hashtable<>();

    public void registerView(String key, Class<?> target) {
        this.mViews.put(key, target);
    }

    public void showView(IView context, int code, String viewKey) {
        showView(context, code, viewKey, null);
    }

    public void showView(IView context, int code, String viewKey, Bundle bundle) {
        if (this.mViews.containsKey(viewKey)) {
            Activity act = (Activity) context;
            Intent intent = new Intent(act.getApplicationContext(), this.mViews.get(viewKey));
            if (bundle != null) {
                intent.putExtra("KEY", bundle);
            }
            act.startActivityForResult(intent, code);
        }
    }

    public void displayInstallationForm(IView view, Uri apkPath, int code) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(apkPath, "application/vnd.android.package-archive");
        ((Activity) view).startActivityForResult(intent, code);
    }

    public Bundle getLaunchData(IView view) {
        return ((Activity) view).getIntent().getBundleExtra("KEY");
    }
}
