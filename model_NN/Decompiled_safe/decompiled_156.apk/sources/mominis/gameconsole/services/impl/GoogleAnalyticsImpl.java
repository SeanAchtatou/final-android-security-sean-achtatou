package mominis.gameconsole.services.impl;

import android.content.Context;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.google.inject.Inject;
import mominis.gameconsole.services.IAnalyticsManager;

public class GoogleAnalyticsImpl extends BaseGoogleAnalyticsMgr implements IAnalyticsManager {
    private final int INTERVAL = 20;
    GoogleAnalyticsTracker mTracker = GoogleAnalyticsTracker.getInstance();

    @Inject
    public GoogleAnalyticsImpl(Context context) {
        super(context);
        start();
    }

    private void start() {
        this.mTracker.start(this.mWebPropertyId, 20, this.mContext);
    }

    /* access modifiers changed from: protected */
    public void track(String page) {
        this.mTracker.trackPageView(page);
    }
}
