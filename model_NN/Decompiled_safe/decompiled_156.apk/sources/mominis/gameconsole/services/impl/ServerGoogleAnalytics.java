package mominis.gameconsole.services.impl;

import android.content.Context;
import android.util.Log;
import com.google.inject.Inject;
import java.io.IOException;
import java.util.Hashtable;
import java.util.concurrent.Executor;
import mominis.common.utils.HttpUtils;
import mominis.common.utils.IHttpClient;
import mominis.common.utils.IHttpClientFactory;
import mominis.gameconsole.com.R;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.impl.client.BasicCookieStore;

public class ServerGoogleAnalytics extends BaseGoogleAnalyticsMgr {
    /* access modifiers changed from: private */
    public IHttpClientFactory mClientFactory;
    /* access modifiers changed from: private */
    public BasicCookieStore mCookieStore = new BasicCookieStore();
    private Executor mExecutor;
    /* access modifiers changed from: private */
    public String mServerAddress = this.mContext.getString(R.string.game_console_address);
    /* access modifiers changed from: private */
    public String mServerUrl = (this.mContext.getString(R.string.game_console_base_url) + "AnalyticsService/json/Track");

    @Inject
    public ServerGoogleAnalytics(Context context, IHttpClientFactory clientFactory, Executor executor) {
        super(context);
        Log.d("Google Analytics Manage", String.format("server base address is %s. relative url is %s", this.mServerAddress, this.mServerUrl));
        this.mClientFactory = clientFactory;
        this.mExecutor = executor;
    }

    /* access modifiers changed from: protected */
    public void track(final String page) {
        this.mExecutor.execute(new Runnable() {
            public void run() {
                IHttpClient httpClient = ServerGoogleAnalytics.this.mClientFactory.create();
                httpClient.setCookieStore(ServerGoogleAnalytics.this.mCookieStore);
                Hashtable<String, Object> params = new Hashtable<>();
                Hashtable<String, String> cookies = new Hashtable<>();
                params.put("content", page);
                params.put("account", ServerGoogleAnalytics.this.mWebPropertyId);
                try {
                    HttpResponse resp = HttpUtils.executeHttpGet(httpClient, ServerGoogleAnalytics.this.mServerUrl, ServerGoogleAnalytics.this.mServerAddress, params, cookies);
                    if (resp.getStatusLine().getStatusCode() != 200) {
                        Log.e("Google Analytics Manage", String.format("S2S google tracking failed - status code was %s", Integer.valueOf(resp.getStatusLine().getStatusCode())));
                        return;
                    }
                    Log.i("Google Analytics Manage", String.format("S2S google analytics returned %s", Integer.valueOf(resp.getStatusLine().getStatusCode())));
                } catch (ClientProtocolException e) {
                    ClientProtocolException e2 = e;
                    Log.e("Google Analytics Manage", "ClientProtocolException raised while executing get request", e2);
                    e2.printStackTrace();
                } catch (IOException e3) {
                    IOException e4 = e3;
                    Log.e("Google Analytics Manage", "IOException raised while executing get request", e4);
                    e4.printStackTrace();
                } catch (Exception e5) {
                    Log.e("Google Analytics Manage", "Unexpected exception raised while executing get request", e5);
                }
            }
        });
    }
}
