package roboguice.inject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.MembersInjector;
import com.google.inject.Provider;
import com.google.inject.TypeLiteral;
import com.google.inject.internal.Nullable;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;
import com.google.inject.util.Types;
import java.lang.reflect.Field;

public class ExtrasListener implements TypeListener {
    protected Provider<Context> contextProvider;

    public ExtrasListener(Provider<Context> contextProvider2) {
        this.contextProvider = contextProvider2;
    }

    public <I> void hear(TypeLiteral<I> typeLiteral, TypeEncounter<I> typeEncounter) {
        for (Class<?> c = typeLiteral.getRawType(); c != null; c = c.getSuperclass()) {
            for (Field field : c.getDeclaredFields()) {
                if (field.isAnnotationPresent(InjectExtra.class)) {
                    typeEncounter.register(new ExtrasMembersInjector(field, this.contextProvider, (InjectExtra) field.getAnnotation(InjectExtra.class)));
                }
            }
        }
    }

    protected static class ExtrasMembersInjector<T> implements MembersInjector<T> {
        protected InjectExtra annotation;
        protected Provider<Context> contextProvider;
        protected Field field;

        public ExtrasMembersInjector(Field field2, Provider<Context> contextProvider2, InjectExtra annotation2) {
            this.field = field2;
            this.contextProvider = contextProvider2;
            this.annotation = annotation2;
        }

        public void injectMembers(T instance) {
            Context context = this.contextProvider.get();
            if (context instanceof Activity) {
                String id = this.annotation.value();
                Bundle extras = ((Activity) context).getIntent().getExtras();
                if (extras != null && extras.containsKey(id)) {
                    Object value = extras.get(id);
                    if (context instanceof InjectorProvider) {
                        value = convert(this.field, value, ((InjectorProvider) context).getInjector());
                    }
                    if (value == null && this.field.getAnnotation(Nullable.class) == null) {
                        throw new NullPointerException(String.format("Can't inject null value into %s.%s when field is not @Nullable", this.field.getDeclaringClass(), this.field.getName()));
                    }
                    this.field.setAccessible(true);
                    try {
                        this.field.set(instance, value);
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    } catch (IllegalArgumentException e2) {
                        Object[] objArr = new Object[4];
                        objArr[0] = value != null ? value.getClass() : "(null)";
                        objArr[1] = value;
                        objArr[2] = this.field.getType();
                        objArr[3] = this.field.getName();
                        throw new IllegalArgumentException(String.format("Can't assign %s value %s to %s field %s", objArr));
                    }
                } else if (!this.annotation.optional()) {
                    throw new IllegalStateException(String.format("Can't find the mandatory extra identified by key [%s] on field %s.%s", id, this.field.getDeclaringClass(), this.field.getName()));
                }
            }
        }

        /* access modifiers changed from: protected */
        public Object convert(Field field2, Object value, Injector injector) {
            if (value == null || field2.getType().isPrimitive()) {
                return value;
            }
            Key<?> key = Key.get(Types.newParameterizedType(ExtraConverter.class, value.getClass(), field2.getType()));
            if (injector.getBindings().containsKey(key)) {
                value = ((ExtraConverter) injector.getInstance(key)).convert(value);
            }
            return value;
        }
    }
}
