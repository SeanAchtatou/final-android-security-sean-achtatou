package roboguice.inject;

import android.content.Context;
import android.preference.PreferenceActivity;
import com.google.inject.MembersInjector;
import com.google.inject.Provider;
import com.google.inject.internal.Nullable;
import com.google.inject.internal.Preconditions;
import java.lang.reflect.Field;

/* compiled from: PreferenceListener */
class PreferenceMembersInjector<T> implements MembersInjector<T> {
    protected InjectPreference annotation;
    protected Provider<Context> contextProvider;
    protected Field field;
    protected T instance;
    protected ContextScope scope;

    public PreferenceMembersInjector(Field field2, Provider<Context> contextProvider2, InjectPreference annotation2, ContextScope scope2) {
        this.field = field2;
        this.annotation = annotation2;
        this.contextProvider = contextProvider2;
        this.scope = scope2;
    }

    public void injectMembers(T instance2) {
        this.instance = instance2;
        this.scope.registerPreferenceForInjection(this);
    }

    /* JADX INFO: Multiple debug info for r2v2 'value'  android.preference.Preference: [D('value' android.preference.Preference), D('value' java.lang.Object)] */
    public void reallyInjectMembers() {
        Preconditions.checkNotNull(this.instance);
        Object value = null;
        try {
            value = ((PreferenceActivity) this.contextProvider.get()).findPreference(this.annotation.value());
            if (value == null && this.field.getAnnotation(Nullable.class) == null) {
                throw new NullPointerException(String.format("Can't inject null value into %s.%s when field is not @Nullable", this.field.getDeclaringClass(), this.field.getName()));
            }
            this.field.setAccessible(true);
            this.field.set(this.instance, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (IllegalArgumentException e2) {
            Object[] objArr = new Object[4];
            objArr[0] = value != null ? value.getClass() : "(null)";
            objArr[1] = value;
            objArr[2] = this.field.getType();
            objArr[3] = this.field.getName();
            throw new IllegalArgumentException(String.format("Can't assign %s value %s to %s field %s", objArr));
        }
    }
}
