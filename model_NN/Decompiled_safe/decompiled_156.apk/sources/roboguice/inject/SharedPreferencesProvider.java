package roboguice.inject;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.inject.Inject;
import com.google.inject.Provider;

public class SharedPreferencesProvider implements Provider<SharedPreferences> {
    protected static final String DEFAULT = "default";
    @Inject
    protected Provider<Context> contextProvider;
    protected String preferencesName;

    public static class PreferencesNameHolder {
        @SharedPreferencesName
        @Inject(optional = true)
        protected String value = SharedPreferencesProvider.DEFAULT;
    }

    public SharedPreferencesProvider() {
        this.preferencesName = DEFAULT;
    }

    @Inject
    public SharedPreferencesProvider(PreferencesNameHolder preferencesNameHolder) {
        this.preferencesName = preferencesNameHolder.value;
    }

    public SharedPreferencesProvider(String preferencesName2) {
        this.preferencesName = preferencesName2;
    }

    public SharedPreferences get() {
        return this.contextProvider.get().getSharedPreferences(this.preferencesName, 0);
    }
}
