package roboguice.inject;

import android.app.Application;
import android.content.Context;
import com.google.inject.Provider;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.TypeEncounter;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class PreferenceListener implements StaticTypeListener {
    protected Application application;
    protected Provider<Context> contextProvider;
    protected ContextScope scope;

    public PreferenceListener(Provider<Context> contextProvider2, Application application2, ContextScope scope2) {
        this.contextProvider = contextProvider2;
        this.application = application2;
        this.scope = scope2;
    }

    public <I> void hear(TypeLiteral<I> typeLiteral, TypeEncounter<I> typeEncounter) {
        for (Class<?> c = typeLiteral.getRawType(); c != null; c = c.getSuperclass()) {
            for (Field field : c.getDeclaredFields()) {
                if (!Modifier.isStatic(field.getModifiers()) && field.isAnnotationPresent(InjectPreference.class)) {
                    typeEncounter.register(new PreferenceMembersInjector(field, this.contextProvider, (InjectPreference) field.getAnnotation(InjectPreference.class), this.scope));
                }
            }
        }
    }

    public void requestStaticInjection(Class<?>... types) {
        for (Class<?> c : types) {
            while (c != null) {
                for (Field field : c.getDeclaredFields()) {
                    if (Modifier.isStatic(field.getModifiers()) && field.isAnnotationPresent(InjectPreference.class)) {
                        new PreferenceMembersInjector(field, this.contextProvider, (InjectPreference) field.getAnnotation(InjectPreference.class), this.scope).injectMembers(null);
                    }
                }
                c = c.getSuperclass();
            }
        }
    }
}
