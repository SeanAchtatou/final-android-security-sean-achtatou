package roboguice.inject;

import android.app.Application;
import com.google.inject.MembersInjector;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.TypeEncounter;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class ResourceListener implements StaticTypeListener {
    protected Application application;

    public ResourceListener(Application application2) {
        this.application = application2;
    }

    public <I> void hear(TypeLiteral<I> typeLiteral, TypeEncounter<I> typeEncounter) {
        for (Class<?> c = typeLiteral.getRawType(); c != Object.class; c = c.getSuperclass()) {
            for (Field field : c.getDeclaredFields()) {
                if (!Modifier.isStatic(field.getModifiers()) && field.isAnnotationPresent(InjectResource.class)) {
                    typeEncounter.register(new ResourceMembersInjector(field, this.application, (InjectResource) field.getAnnotation(InjectResource.class)));
                }
            }
        }
    }

    public void requestStaticInjection(Class<?>... types) {
        Class<?>[] arr$ = types;
        int len$ = arr$.length;
        for (int i$ = 0; i$ < len$; i$++) {
            for (Class<?> c = arr$[i$]; c != Object.class; c = c.getSuperclass()) {
                for (Field field : c.getDeclaredFields()) {
                    if (Modifier.isStatic(field.getModifiers()) && field.isAnnotationPresent(InjectResource.class)) {
                        new ResourceMembersInjector(field, this.application, (InjectResource) field.getAnnotation(InjectResource.class)).injectMembers(null);
                    }
                }
            }
        }
    }

    protected static class ResourceMembersInjector<T> implements MembersInjector<T> {
        protected InjectResource annotation;
        protected Application application;
        protected Field field;

        public ResourceMembersInjector(Field field2, Application application2, InjectResource annotation2) {
            this.field = field2;
            this.application = application2;
            this.annotation = annotation2;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x004f, code lost:
            r6 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0056, code lost:
            throw new java.lang.RuntimeException(r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:0x00f1, code lost:
            r6 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:0x00f2, code lost:
            r6 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:52:0x00fd, code lost:
            r10 = r6.getClass();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:0x011e, code lost:
            r10 = "(null)";
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:11:0x004f A[ExcHandler: IllegalAccessException (r6v7 'e' java.lang.IllegalAccessException A[CUSTOM_DECLARE]), Splitter:B:1:0x0004] */
        /* JADX WARNING: Removed duplicated region for block: B:52:0x00fd  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x011e  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void injectMembers(T r15) {
            /*
                r14 = this;
                r13 = 2
                r12 = 0
                r11 = 1
                r5 = 0
                roboguice.inject.InjectResource r6 = r14.annotation     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                int r2 = r6.value()     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                java.lang.reflect.Field r6 = r14.field     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                java.lang.Class r4 = r6.getType()     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                android.app.Application r6 = r14.application     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                android.content.res.Resources r3 = r6.getResources()     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                java.lang.Class<java.lang.String> r6 = java.lang.String.class
                boolean r6 = r6.isAssignableFrom(r4)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                if (r6 == 0) goto L_0x0057
                java.lang.String r5 = r3.getString(r2)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
            L_0x0022:
                if (r5 != 0) goto L_0x00e5
                java.lang.reflect.Field r6 = r14.field     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x0121 }
                java.lang.Class<com.google.inject.internal.Nullable> r7 = com.google.inject.internal.Nullable.class
                java.lang.annotation.Annotation r6 = r6.getAnnotation(r7)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x0121 }
                if (r6 != 0) goto L_0x00e5
                java.lang.NullPointerException r6 = new java.lang.NullPointerException     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x0121 }
                java.lang.String r7 = "Can't inject null value into %s.%s when field is not @Nullable"
                r8 = 2
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x0121 }
                r9 = 0
                java.lang.reflect.Field r10 = r14.field     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x0121 }
                java.lang.Class r10 = r10.getDeclaringClass()     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x0121 }
                r8[r9] = r10     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x0121 }
                r9 = 1
                java.lang.reflect.Field r10 = r14.field     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x0121 }
                java.lang.String r10 = r10.getName()     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x0121 }
                r8[r9] = r10     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x0121 }
                java.lang.String r7 = java.lang.String.format(r7, r8)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x0121 }
                r6.<init>(r7)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x0121 }
                throw r6     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x0121 }
            L_0x004f:
                r6 = move-exception
                r0 = r6
                java.lang.RuntimeException r6 = new java.lang.RuntimeException
                r6.<init>(r0)
                throw r6
            L_0x0057:
                java.lang.Class r6 = java.lang.Boolean.TYPE     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                boolean r6 = r6.isAssignableFrom(r4)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                if (r6 != 0) goto L_0x0067
                java.lang.Class<java.lang.Boolean> r6 = java.lang.Boolean.class
                boolean r6 = r6.isAssignableFrom(r4)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                if (r6 == 0) goto L_0x0070
            L_0x0067:
                boolean r6 = r3.getBoolean(r2)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                java.lang.Boolean r5 = java.lang.Boolean.valueOf(r6)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                goto L_0x0022
            L_0x0070:
                java.lang.Class<android.content.res.ColorStateList> r6 = android.content.res.ColorStateList.class
                boolean r6 = r6.isAssignableFrom(r4)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                if (r6 == 0) goto L_0x007d
                android.content.res.ColorStateList r5 = r3.getColorStateList(r2)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                goto L_0x0022
            L_0x007d:
                java.lang.Class r6 = java.lang.Integer.TYPE     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                boolean r6 = r6.isAssignableFrom(r4)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                if (r6 != 0) goto L_0x008d
                java.lang.Class<java.lang.Integer> r6 = java.lang.Integer.class
                boolean r6 = r6.isAssignableFrom(r4)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                if (r6 == 0) goto L_0x0096
            L_0x008d:
                int r6 = r3.getInteger(r2)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                java.lang.Integer r5 = java.lang.Integer.valueOf(r6)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                goto L_0x0022
            L_0x0096:
                java.lang.Class<android.graphics.drawable.Drawable> r6 = android.graphics.drawable.Drawable.class
                boolean r6 = r6.isAssignableFrom(r4)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                if (r6 == 0) goto L_0x00a3
                android.graphics.drawable.Drawable r5 = r3.getDrawable(r2)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                goto L_0x0022
            L_0x00a3:
                java.lang.Class<java.lang.String[]> r6 = java.lang.String[].class
                boolean r6 = r6.isAssignableFrom(r4)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                if (r6 == 0) goto L_0x00b1
                java.lang.String[] r5 = r3.getStringArray(r2)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                goto L_0x0022
            L_0x00b1:
                java.lang.Class<int[]> r6 = int[].class
                boolean r6 = r6.isAssignableFrom(r4)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                if (r6 != 0) goto L_0x00c1
                java.lang.Class<java.lang.Integer[]> r6 = java.lang.Integer[].class
                boolean r6 = r6.isAssignableFrom(r4)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                if (r6 == 0) goto L_0x00c7
            L_0x00c1:
                int[] r5 = r3.getIntArray(r2)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                goto L_0x0022
            L_0x00c7:
                java.lang.Class<android.view.animation.Animation> r6 = android.view.animation.Animation.class
                boolean r6 = r6.isAssignableFrom(r4)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                if (r6 == 0) goto L_0x00d7
                android.app.Application r6 = r14.application     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                android.view.animation.Animation r5 = android.view.animation.AnimationUtils.loadAnimation(r6, r2)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                goto L_0x0022
            L_0x00d7:
                java.lang.Class<android.graphics.Movie> r6 = android.graphics.Movie.class
                boolean r6 = r6.isAssignableFrom(r4)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                if (r6 == 0) goto L_0x0022
                android.graphics.Movie r5 = r3.getMovie(r2)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x00f1 }
                goto L_0x0022
            L_0x00e5:
                java.lang.reflect.Field r6 = r14.field     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x0121 }
                r7 = 1
                r6.setAccessible(r7)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x0121 }
                java.lang.reflect.Field r6 = r14.field     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x0121 }
                r6.set(r15, r5)     // Catch:{ IllegalAccessException -> 0x004f, IllegalArgumentException -> 0x0121 }
                return
            L_0x00f1:
                r6 = move-exception
                r1 = r6
                r6 = r5
            L_0x00f4:
                java.lang.IllegalArgumentException r7 = new java.lang.IllegalArgumentException
                java.lang.String r8 = "Can't assign %s value %s to %s field %s"
                r9 = 4
                java.lang.Object[] r9 = new java.lang.Object[r9]
                if (r6 == 0) goto L_0x011e
                java.lang.Class r10 = r6.getClass()
            L_0x0101:
                r9[r12] = r10
                r9[r11] = r6
                java.lang.reflect.Field r6 = r14.field
                java.lang.Class r6 = r6.getType()
                r9[r13] = r6
                r6 = 3
                java.lang.reflect.Field r10 = r14.field
                java.lang.String r10 = r10.getName()
                r9[r6] = r10
                java.lang.String r6 = java.lang.String.format(r8, r9)
                r7.<init>(r6)
                throw r7
            L_0x011e:
                java.lang.String r10 = "(null)"
                goto L_0x0101
            L_0x0121:
                r6 = move-exception
                r1 = r6
                r6 = r5
                goto L_0x00f4
            */
            throw new UnsupportedOperationException("Method not decompiled: roboguice.inject.ResourceListener.ResourceMembersInjector.injectMembers(java.lang.Object):void");
        }
    }
}
