package roboguice.inject;

import android.content.Context;
import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.Scope;
import com.google.inject.internal.Maps;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Map;
import java.util.WeakHashMap;
import roboguice.application.RoboApplication;
import roboguice.util.Ln;
import roboguice.util.Strings;

public class ContextScope implements Scope {
    protected ThreadLocal<WeakActiveStack<Context>> contextStack = new ThreadLocal<>();
    protected ArrayList<PreferenceMembersInjector<?>> preferencesForInjection = new ArrayList<>();
    protected WeakHashMap<Context, Map<Key<?>, Object>> values = new WeakHashMap<>();
    protected ArrayList<ViewMembersInjector<?>> viewsForInjection = new ArrayList<>();

    public ContextScope(RoboApplication app) {
        enter(app);
    }

    public void registerViewForInjection(ViewMembersInjector<?> injector) {
        this.viewsForInjection.add(injector);
    }

    public void registerPreferenceForInjection(PreferenceMembersInjector<?> injector) {
        this.preferencesForInjection.add(injector);
    }

    public void injectViews() {
        for (int i = this.viewsForInjection.size() - 1; i >= 0; i--) {
            this.viewsForInjection.remove(i).reallyInjectMembers();
        }
    }

    public void injectPreferenceViews() {
        for (int i = this.preferencesForInjection.size() - 1; i >= 0; i--) {
            this.preferencesForInjection.remove(i).reallyInjectMembers();
        }
    }

    public void enter(Context context) {
        WeakHashMap<Context, Map<Key<?>, Object>> map;
        ensureContextStack();
        this.contextStack.get().push(context);
        Key<Context> key = Key.get(Context.class);
        getScopedObjectMap(key).put(key, context);
        if (Ln.isVerboseEnabled() && (map = this.values) != null) {
            Ln.v("Contexts in the %s scope map after inserting %s: %s", Thread.currentThread().getName(), context, Strings.join(", ", map.keySet()));
        }
    }

    public void exit(Context context) {
        ensureContextStack();
        this.contextStack.get().remove(context);
    }

    public void dispose(Context context) {
        WeakHashMap<Context, Map<Key<?>, Object>> map = this.values;
        if (map != null) {
            Map<Key<?>, Object> scopedObjects = map.remove(context);
            if (scopedObjects != null) {
                scopedObjects.clear();
            }
            if (Ln.isVerboseEnabled()) {
                Ln.v("Contexts in the %s scope map after removing %s: %s", Thread.currentThread().getName(), context, Strings.join(", ", map.keySet()));
            }
        }
    }

    public <T> Provider<T> scope(final Key<T> key, final Provider<T> unscoped) {
        return new Provider<T>() {
            public T get() {
                Map<Key<?>, Object> scopedObjects = ContextScope.this.getScopedObjectMap(key);
                T current = scopedObjects.get(key);
                if (current != null || scopedObjects.containsKey(key)) {
                    return current;
                }
                T current2 = unscoped.get();
                scopedObjects.put(key, current2);
                return current2;
            }
        };
    }

    /* access modifiers changed from: protected */
    public void ensureContextStack() {
        if (this.contextStack.get() == null) {
            this.contextStack.set(new WeakActiveStack());
        }
    }

    /* access modifiers changed from: protected */
    public <T> Map<Key<?>, Object> getScopedObjectMap(Key<T> key) {
        Context context = (Context) this.contextStack.get().peek();
        Map<Key<?>, Object> scopedObjects = this.values.get(context);
        if (scopedObjects != null) {
            return scopedObjects;
        }
        Map<Key<?>, Object> scopedObjects2 = Maps.newHashMap();
        this.values.put(context, scopedObjects2);
        return scopedObjects2;
    }

    public static class WeakActiveStack<T> {
        private Node<T> head;
        private Node<T> tail;

        static class Node<T> {
            Node<T> next;
            Node<T> previous;
            WeakReference<T> value;

            public Node(T value2) {
                this.value = new WeakReference<>(value2);
            }
        }

        public void push(T value) {
            if (this.head == null) {
                this.head = new Node<>(value);
                this.tail = this.head;
                return;
            }
            Node<T> existingNode = findNode(value);
            if (existingNode == null) {
                Node<T> newNode = new Node<>(value);
                newNode.next = this.head;
                this.head.previous = newNode;
                this.head = newNode;
            } else if (existingNode != this.head) {
                if (existingNode == this.tail) {
                    this.tail = existingNode.previous;
                    this.tail.next = null;
                }
                if (existingNode.previous != null) {
                    existingNode.previous.next = existingNode.next;
                }
                if (existingNode.next != null) {
                    existingNode.next.previous = existingNode.previous;
                }
                existingNode.next = this.head;
                this.head.previous = existingNode;
                this.head = existingNode;
                this.head.previous = null;
            }
        }

        public T pop() {
            Node<T> node = this.head;
            while (node != null) {
                T value = node.value.get();
                if (value == null) {
                    node = disposeOfNode(node);
                } else {
                    if (node.next != null) {
                        this.head = node.next;
                        node.previous = this.tail;
                        this.tail.next = node;
                        node.next = null;
                        this.head.previous = null;
                        this.tail = node;
                    }
                    return value;
                }
            }
            return null;
        }

        public T peek() {
            Node<T> node = this.head;
            while (node != null) {
                T value = node.value.get();
                if (value != null) {
                    return value;
                }
                node = disposeOfNode(node);
            }
            return null;
        }

        public void remove(T value) {
            disposeOfNode(findNode(value));
        }

        /* access modifiers changed from: protected */
        public Node<T> disposeOfNode(Node<T> node) {
            if (node == this.head) {
                this.head = node.next;
                if (this.head == null) {
                    this.tail = null;
                } else {
                    this.head.previous = null;
                }
            }
            if (node.previous != null) {
                node.previous.next = node.next;
            }
            if (node.next != null) {
                node.next.previous = node.previous;
            }
            if (node == this.tail) {
                this.tail = node.previous;
                this.tail.next = null;
            }
            return node.next;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        protected roboguice.inject.ContextScope.WeakActiveStack.Node<T> findNode(T r4) {
            /*
                r3 = this;
                roboguice.inject.ContextScope$WeakActiveStack$Node<T> r0 = r3.head
            L_0x0002:
                if (r0 == 0) goto L_0x001c
                java.lang.ref.WeakReference<T> r2 = r0.value
                java.lang.Object r1 = r2.get()
                if (r1 != 0) goto L_0x0011
                roboguice.inject.ContextScope$WeakActiveStack$Node r0 = r3.disposeOfNode(r0)
                goto L_0x0002
            L_0x0011:
                boolean r2 = r1.equals(r4)
                if (r2 == 0) goto L_0x0019
                r2 = r0
            L_0x0018:
                return r2
            L_0x0019:
                roboguice.inject.ContextScope$WeakActiveStack$Node<T> r0 = r0.next
                goto L_0x0002
            L_0x001c:
                r2 = 0
                goto L_0x0018
            */
            throw new UnsupportedOperationException("Method not decompiled: roboguice.inject.ContextScope.WeakActiveStack.findNode(java.lang.Object):roboguice.inject.ContextScope$WeakActiveStack$Node");
        }
    }
}
