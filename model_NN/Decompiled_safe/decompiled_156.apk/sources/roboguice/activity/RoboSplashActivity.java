package roboguice.activity;

import android.app.Activity;
import android.os.Bundle;
import roboguice.application.RoboApplication;
import roboguice.inject.ContextScope;

public abstract class RoboSplashActivity extends Activity {
    protected int minDisplayMs = 2500;

    /* access modifiers changed from: protected */
    public abstract void startNextActivity();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final long start = System.currentTimeMillis();
        new Thread(new Runnable() {
            public void run() {
                RoboApplication app = (RoboApplication) RoboSplashActivity.this.getApplication();
                ContextScope scope = (ContextScope) app.getInjector().getInstance(ContextScope.class);
                scope.enter(app);
                try {
                    RoboSplashActivity.this.doStuffInBackground(app);
                    long duration = System.currentTimeMillis() - start;
                    if (duration < ((long) RoboSplashActivity.this.minDisplayMs)) {
                        Thread.sleep(((long) RoboSplashActivity.this.minDisplayMs) - duration);
                    }
                } catch (InterruptedException e) {
                    Thread.interrupted();
                } catch (Throwable th) {
                    scope.exit(app);
                    throw th;
                }
                RoboSplashActivity.this.startNextActivity();
                RoboSplashActivity.this.andFinishThisOne();
                scope.exit(app);
            }
        }).start();
    }

    /* access modifiers changed from: protected */
    public void doStuffInBackground(RoboApplication app) {
    }

    /* access modifiers changed from: protected */
    public void andFinishThisOne() {
        finish();
    }
}
