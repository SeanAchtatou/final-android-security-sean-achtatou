package roboguice.activity.event;

import android.content.res.Configuration;

public class OnConfigurationChangedEvent {
    protected Configuration newConfig;
    protected Configuration oldConfig;

    public OnConfigurationChangedEvent(Configuration oldConfig2, Configuration newConfig2) {
        this.oldConfig = oldConfig2;
        this.newConfig = newConfig2;
    }

    public Configuration getOldConfig() {
        return this.oldConfig;
    }

    public Configuration getNewConfig() {
        return this.newConfig;
    }
}
