package roboguice.activity.event;

import android.os.Bundle;

public class OnCreateEvent {
    protected Bundle savedInstanceState;

    public OnCreateEvent(Bundle savedInstanceState2) {
        this.savedInstanceState = savedInstanceState2;
    }

    public Bundle getSavedInstanceState() {
        return this.savedInstanceState;
    }
}
