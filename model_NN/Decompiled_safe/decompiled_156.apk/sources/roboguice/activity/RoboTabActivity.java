package roboguice.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import com.google.inject.Injector;
import roboguice.activity.event.OnActivityResultEvent;
import roboguice.activity.event.OnConfigurationChangedEvent;
import roboguice.activity.event.OnContentChangedEvent;
import roboguice.activity.event.OnContentViewAvailableEvent;
import roboguice.activity.event.OnCreateEvent;
import roboguice.activity.event.OnDestroyEvent;
import roboguice.activity.event.OnNewIntentEvent;
import roboguice.activity.event.OnPauseEvent;
import roboguice.activity.event.OnRestartEvent;
import roboguice.activity.event.OnResumeEvent;
import roboguice.activity.event.OnStartEvent;
import roboguice.activity.event.OnStopEvent;
import roboguice.application.RoboApplication;
import roboguice.event.EventManager;
import roboguice.inject.ContextScope;
import roboguice.inject.InjectorProvider;

public class RoboTabActivity extends TabActivity implements InjectorProvider {
    protected EventManager eventManager;
    protected ContextScope scope;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        Injector injector = getInjector();
        this.eventManager = (EventManager) injector.getInstance(EventManager.class);
        this.scope = (ContextScope) injector.getInstance(ContextScope.class);
        this.scope.enter(this);
        injector.injectMembers(this);
        super.onCreate(savedInstanceState);
        this.eventManager.fire(new OnCreateEvent(savedInstanceState));
    }

    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        this.scope.injectViews();
        this.eventManager.fire(new OnContentViewAvailableEvent());
    }

    public void setContentView(View view, ViewGroup.LayoutParams params) {
        super.setContentView(view, params);
        this.scope.injectViews();
        this.eventManager.fire(new OnContentViewAvailableEvent());
    }

    public void setContentView(View view) {
        super.setContentView(view);
        this.scope.injectViews();
        this.eventManager.fire(new OnContentViewAvailableEvent());
    }

    public Object onRetainNonConfigurationInstance() {
        return this;
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        this.scope.enter(this);
        super.onRestart();
        this.eventManager.fire(new OnRestartEvent());
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        this.scope.enter(this);
        super.onStart();
        this.eventManager.fire(new OnStartEvent());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.scope.enter(this);
        super.onResume();
        this.eventManager.fire(new OnResumeEvent());
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.eventManager.fire(new OnPauseEvent());
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.scope.enter(this);
        this.eventManager.fire(new OnNewIntentEvent());
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.scope.enter(this);
        try {
            this.eventManager.fire(new OnStopEvent());
        } finally {
            this.scope.exit(this);
            super.onStop();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.scope.enter(this);
        try {
            this.eventManager.fire(new OnDestroyEvent());
        } finally {
            this.eventManager.clear(this);
            this.scope.exit(this);
            this.scope.dispose(this);
            super.onDestroy();
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        Configuration currentConfig = getResources().getConfiguration();
        super.onConfigurationChanged(newConfig);
        this.eventManager.fire(new OnConfigurationChangedEvent(currentConfig, newConfig));
    }

    public void onContentChanged() {
        super.onContentChanged();
        this.eventManager.fire(new OnContentChangedEvent());
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.scope.enter(this);
        try {
            this.eventManager.fire(new OnActivityResultEvent(requestCode, resultCode, data));
        } finally {
            this.scope.exit(this);
        }
    }

    public Injector getInjector() {
        return ((RoboApplication) getApplication()).getInjector();
    }
}
