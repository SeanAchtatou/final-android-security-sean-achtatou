package roboguice.config;

import com.google.inject.AbstractModule;
import java.util.List;
import roboguice.inject.StaticTypeListener;

public abstract class AbstractAndroidModule extends AbstractModule {
    protected List<StaticTypeListener> listeners;

    /* access modifiers changed from: protected */
    public void requestStaticInjection(Class<?>... types) {
        super.requestStaticInjection(types);
        if (this.listeners != null) {
            for (StaticTypeListener l : this.listeners) {
                l.requestStaticInjection(types);
            }
        }
    }

    public void setStaticTypeListeners(List<StaticTypeListener> listeners2) {
        this.listeners = listeners2;
    }
}
