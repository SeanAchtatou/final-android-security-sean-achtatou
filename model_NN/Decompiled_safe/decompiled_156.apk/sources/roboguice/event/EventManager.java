package roboguice.event;

import android.app.Application;
import android.content.Context;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import roboguice.event.javaassist.RuntimeSupport;
import roboguice.util.Ln;

@Singleton
public class EventManager {
    @Inject
    protected Provider<Context> contextProvider;
    protected Map<Context, Map<Class<?>, Set<EventListener<?>>>> registrations = new WeakHashMap();

    public boolean isEnabled() {
        return true;
    }

    public <T> void registerObserver(Class<T> event, EventListener listener) {
        registerObserver(this.contextProvider.get(), event, listener);
    }

    public <T> void registerObserver(Object instance, Method method, Class<T> event) {
        registerObserver(this.contextProvider.get(), instance, method, event);
    }

    public <T> void unregisterObserver(Class<T> event, EventListener<T> listener) {
        unregisterObserver(this.contextProvider.get(), event, listener);
    }

    public <T> void unregisterObserver(Object instance, Class<T> event) {
        unregisterObserver(this.contextProvider.get(), instance, event);
    }

    public void fire(Object event) {
        fire(this.contextProvider.get(), event);
    }

    public <T> void registerObserver(Context context, Class<T> event, EventListener listener) {
        if (context instanceof Application) {
            throw new RuntimeException("You may not register event handlers on the Application context");
        }
        Map<Class<?>, Set<EventListener<?>>> methods = this.registrations.get(context);
        if (methods == null) {
            methods = new HashMap<>();
            this.registrations.put(context, methods);
        }
        Set<EventListener<?>> observers = methods.get(event);
        if (observers == null) {
            observers = new HashSet<>();
            methods.put(event, observers);
        }
        observers.add(listener);
    }

    public <T> void registerObserver(Context context, Object instance, Method method, Class<T> event) {
        registerObserver(context, event, new ObserverMethodListener(instance, method));
    }

    public <T> void unregisterObserver(Context context, Class<T> event, EventListener<T> listener) {
        Map<Class<?>, Set<EventListener<?>>> methods;
        Set<EventListener<?>> observers;
        if (isEnabled() && (methods = this.registrations.get(context)) != null && (observers = methods.get(event)) != null) {
            Iterator<EventListener<?>> iterator = observers.iterator();
            while (iterator.hasNext()) {
                if (iterator.next() == listener) {
                    iterator.remove();
                    return;
                }
            }
        }
    }

    public <T> void unregisterObserver(Context context, Object instance, Class<T> event) {
        Map<Class<?>, Set<EventListener<?>>> methods;
        Set<EventListener<?>> observers;
        if (isEnabled() && (methods = this.registrations.get(context)) != null && (observers = methods.get(event)) != null) {
            Iterator<EventListener<?>> iterator = observers.iterator();
            while (iterator.hasNext()) {
                EventListener listener = (EventListener) iterator.next();
                if ((listener instanceof ObserverMethodListener) && ((ObserverMethodListener) listener).instanceReference.get() == instance) {
                    iterator.remove();
                    return;
                }
            }
        }
    }

    public void clear(Context context) {
        Map<Class<?>, Set<EventListener<?>>> methods = this.registrations.get(context);
        if (methods != null) {
            this.registrations.remove(context);
            methods.clear();
        }
    }

    public void fire(Context context, Object event) {
        Map<Class<?>, Set<EventListener<?>>> methods;
        Set<EventListener<?>> observers;
        if (isEnabled() && (methods = this.registrations.get(context)) != null && (observers = methods.get(event.getClass())) != null) {
            for (EventListener observer : observers) {
                observer.onEvent(event);
            }
        }
    }

    public static class NullEventManager extends EventManager {
        public boolean isEnabled() {
            return false;
        }
    }

    public static class ObserverMethodListener<T> implements EventListener<T> {
        protected String descriptor;
        protected WeakReference<Object> instanceReference;
        protected Method method;

        public ObserverMethodListener(Object instance, Method method2) {
            this.instanceReference = new WeakReference<>(instance);
            this.method = method2;
            this.descriptor = method2.getName() + ':' + RuntimeSupport.makeDescriptor(method2);
            method2.setAccessible(true);
        }

        public void onEvent(T event) {
            try {
                Object instance = this.instanceReference.get();
                if (instance != null) {
                    this.method.invoke(instance, event);
                    return;
                }
                Ln.w("trying to observe event %1$s on disposed context, consider explicitly calling EventManager.unregisterObserver", this.method.getName());
            } catch (InvocationTargetException e) {
                Ln.e(e);
            } catch (IllegalAccessException e2) {
                throw new RuntimeException(e2);
            }
        }

        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            ObserverMethodListener that = (ObserverMethodListener) o;
            if (this.descriptor == null ? that.descriptor != null : !this.descriptor.equals(that.descriptor)) {
                return false;
            }
            Object thisInstance = this.instanceReference.get();
            Object thatInstance = that.instanceReference.get();
            return thisInstance == null ? thatInstance == null : thisInstance.equals(thatInstance);
        }

        public int hashCode() {
            int result;
            int i = 0;
            if (this.descriptor != null) {
                result = this.descriptor.hashCode();
            } else {
                result = 0;
            }
            Object thisInstance = this.instanceReference.get();
            int i2 = result * 31;
            if (thisInstance != null) {
                i = thisInstance.hashCode();
            }
            return i2 + i;
        }
    }
}
