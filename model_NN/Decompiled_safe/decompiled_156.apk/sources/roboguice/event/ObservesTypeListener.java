package roboguice.event;

import android.content.Context;
import com.google.inject.Provider;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class ObservesTypeListener implements TypeListener {
    protected Provider<Context> contextProvider;
    protected EventManager eventManager;

    public ObservesTypeListener(Provider<Context> contextProvider2, EventManager eventManager2) {
        this.eventManager = eventManager2;
        this.contextProvider = contextProvider2;
    }

    /* JADX INFO: Multiple debug info for r0v1 java.lang.Class<?>[]: [D('arr$' java.lang.reflect.Method[]), D('arr$' java.lang.Class[])] */
    public <I> void hear(TypeLiteral<I> iTypeLiteral, TypeEncounter<I> iTypeEncounter) {
        for (Class<?> c = iTypeLiteral.getRawType(); c != Object.class; c = c.getSuperclass()) {
            for (Method method : c.getDeclaredMethods()) {
                findContextObserver(method, iTypeEncounter);
            }
            for (Class<?> interfaceClass : c.getInterfaces()) {
                for (Method method2 : interfaceClass.getDeclaredMethods()) {
                    findContextObserver(method2, iTypeEncounter);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public <I> void findContextObserver(Method method, TypeEncounter<I> iTypeEncounter) {
        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        for (int i = 0; i < parameterAnnotations.length; i++) {
            Annotation[] annotationArray = parameterAnnotations[i];
            Class<?> parameterType = method.getParameterTypes()[i];
            for (Annotation annotation : annotationArray) {
                if (annotation.annotationType().equals(Observes.class)) {
                    registerContextObserver(iTypeEncounter, method, parameterType);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public <I> void registerContextObserver(TypeEncounter<I> iTypeEncounter, Method method, Class parameterType) {
        checkMethodParameters(method);
        iTypeEncounter.register(new ContextObserverMethodInjector(this.contextProvider, this.eventManager, method, parameterType));
    }

    /* access modifiers changed from: protected */
    public void checkMethodParameters(Method method) {
        if (method.getParameterTypes().length > 1) {
            throw new RuntimeException("Annotation @Observes must only annotate one parameter, which must be the only parameter in the listener method.");
        }
    }

    public static class ContextObserverMethodInjector<I> implements InjectionListener<I> {
        protected Provider<Context> contextProvider;
        protected Class<?> event;
        protected EventManager eventManager;
        protected Method method;

        public ContextObserverMethodInjector(Provider<Context> contextProvider2, EventManager eventManager2, Method method2, Class<?> event2) {
            this.contextProvider = contextProvider2;
            this.eventManager = eventManager2;
            this.method = method2;
            this.event = event2;
        }

        public void afterInjection(I i) {
            this.eventManager.registerObserver(this.contextProvider.get(), i, this.method, this.event);
        }
    }
}
