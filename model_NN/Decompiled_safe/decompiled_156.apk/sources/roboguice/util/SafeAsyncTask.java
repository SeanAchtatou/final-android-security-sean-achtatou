package roboguice.util;

import android.os.Handler;
import android.util.Log;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public abstract class SafeAsyncTask<ResultT> implements Callable<ResultT> {
    protected static final Executor DEFAULT_EXECUTOR = Executors.newFixedThreadPool(25);
    public static final int DEFAULT_POOL_SIZE = 25;
    protected Executor executor;
    protected FutureTask<Void> future;
    protected Handler handler;
    protected StackTraceElement[] launchLocation;

    public SafeAsyncTask() {
        this.executor = DEFAULT_EXECUTOR;
    }

    public SafeAsyncTask(Handler handler2) {
        this.handler = handler2;
        this.executor = DEFAULT_EXECUTOR;
    }

    public SafeAsyncTask(Executor executor2) {
        this.executor = executor2;
    }

    public SafeAsyncTask(Handler handler2, Executor executor2) {
        this.handler = handler2;
        this.executor = executor2;
    }

    public FutureTask<Void> future() {
        this.future = new FutureTask<>(newTask());
        return this.future;
    }

    public SafeAsyncTask<ResultT> executor(Executor executor2) {
        this.executor = executor2;
        return this;
    }

    public Executor executor() {
        return this.executor;
    }

    public SafeAsyncTask<ResultT> handler(Handler handler2) {
        this.handler = handler2;
        return this;
    }

    public Handler handler() {
        return this.handler;
    }

    public void execute() {
        execute(Thread.currentThread().getStackTrace());
    }

    /* access modifiers changed from: protected */
    public void execute(StackTraceElement[] launchLocation2) {
        this.launchLocation = launchLocation2;
        this.executor.execute(future());
    }

    public boolean cancel(boolean mayInterruptIfRunning) {
        if (this.future != null) {
            return this.future.cancel(mayInterruptIfRunning);
        }
        throw new UnsupportedOperationException("You cannot cancel this task before calling future()");
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() throws Exception {
    }

    /* access modifiers changed from: protected */
    public void onSuccess(ResultT resultt) throws Exception {
    }

    /* access modifiers changed from: protected */
    public void onInterrupted(Exception e) {
        onException(e);
    }

    /* access modifiers changed from: protected */
    public void onException(Exception e) throws RuntimeException {
        Log.e("roboguice", "Exception caught during background processing", e);
    }

    /* access modifiers changed from: protected */
    public void onFinally() throws RuntimeException {
    }

    /* access modifiers changed from: protected */
    public Task<ResultT> newTask() {
        return new Task<>(this);
    }

    public static class Task<ResultT> implements Callable<Void> {
        protected Handler handler;
        protected SafeAsyncTask<ResultT> parent;

        public Task(SafeAsyncTask parent2) {
            this.parent = parent2;
            this.handler = parent2.handler != null ? parent2.handler : new Handler();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x001c, code lost:
            throw r1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:3:0x000f, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:6:?, code lost:
            doException(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0018, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
            doFinally();
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:8:0x0018 A[ExcHandler:  FINALLY, Splitter:B:0:0x0000] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Void call() throws java.lang.Exception {
            /*
                r2 = this;
                r2.doPreExecute()     // Catch:{ Exception -> 0x000f, all -> 0x0018 }
                java.lang.Object r1 = r2.doCall()     // Catch:{ Exception -> 0x000f, all -> 0x0018 }
                r2.doSuccess(r1)     // Catch:{ Exception -> 0x000f, all -> 0x0018 }
                r2.doFinally()
            L_0x000d:
                r1 = 0
                return r1
            L_0x000f:
                r1 = move-exception
                r0 = r1
                r2.doException(r0)     // Catch:{ Exception -> 0x001d, all -> 0x0018 }
            L_0x0014:
                r2.doFinally()
                goto L_0x000d
            L_0x0018:
                r1 = move-exception
                r2.doFinally()
                throw r1
            L_0x001d:
                r1 = move-exception
                goto L_0x0014
            */
            throw new UnsupportedOperationException("Method not decompiled: roboguice.util.SafeAsyncTask.Task.call():java.lang.Void");
        }

        /* access modifiers changed from: protected */
        public void doPreExecute() throws Exception {
            postToUiThreadAndWait(new Callable<Object>() {
                public Object call() throws Exception {
                    Task.this.parent.onPreExecute();
                    return null;
                }
            });
        }

        /* access modifiers changed from: protected */
        public ResultT doCall() throws Exception {
            return this.parent.call();
        }

        /* access modifiers changed from: protected */
        public void doSuccess(final ResultT r) throws Exception {
            postToUiThreadAndWait(new Callable<Object>() {
                public Object call() throws Exception {
                    Task.this.parent.onSuccess(r);
                    return null;
                }
            });
        }

        /* access modifiers changed from: protected */
        public void doException(final Exception e) throws Exception {
            if (this.parent.launchLocation != null) {
                ArrayList<StackTraceElement> stack = new ArrayList<>(Arrays.asList(e.getStackTrace()));
                stack.addAll(Arrays.asList(this.parent.launchLocation));
                e.setStackTrace((StackTraceElement[]) stack.toArray(new StackTraceElement[stack.size()]));
            }
            postToUiThreadAndWait(new Callable<Object>() {
                public Object call() throws Exception {
                    if ((e instanceof InterruptedException) || (e instanceof InterruptedIOException)) {
                        Task.this.parent.onInterrupted(e);
                        return null;
                    }
                    Task.this.parent.onException(e);
                    return null;
                }
            });
        }

        /* access modifiers changed from: protected */
        public void doFinally() throws Exception {
            postToUiThreadAndWait(new Callable<Object>() {
                public Object call() throws Exception {
                    Task.this.parent.onFinally();
                    return null;
                }
            });
        }

        /* access modifiers changed from: protected */
        public void postToUiThreadAndWait(final Callable c) throws Exception {
            final CountDownLatch latch = new CountDownLatch(1);
            final Exception[] exceptions = new Exception[1];
            this.handler.post(new Runnable() {
                public void run() {
                    try {
                        c.call();
                    } catch (Exception e) {
                        exceptions[0] = e;
                    } finally {
                        latch.countDown();
                    }
                }
            });
            latch.await();
            if (exceptions[0] != null) {
                throw exceptions[0];
            }
        }
    }
}
