package roboguice.application;

import com.google.inject.Injector;
import roboguice.inject.ContextScope;

public class RoboInjectableApplication extends RoboApplication {
    public void onCreate() {
        super.onCreate();
        Injector i = getInjector();
        ((ContextScope) i.getInstance(ContextScope.class)).enter(this);
        i.injectMembers(this);
    }
}
