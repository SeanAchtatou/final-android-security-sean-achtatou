package com.google.inject;

import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.Lists;
import com.google.inject.internal.Maps;
import com.google.inject.internal.Preconditions;
import com.google.inject.spi.InjectionPoint;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

class Initializer {
    /* access modifiers changed from: private */
    public final Thread creatingThread = Thread.currentThread();
    /* access modifiers changed from: private */
    public final Map<Object, InjectableReference<?>> pendingInjection = Maps.newIdentityHashMap();
    /* access modifiers changed from: private */
    public final CountDownLatch ready = new CountDownLatch(1);

    Initializer() {
    }

    public <T> Initializable<T> requestInjection(InjectorImpl injector, T instance, Object source, Set<InjectionPoint> injectionPoints) {
        Preconditions.checkNotNull(source);
        if (instance == null || (injectionPoints.isEmpty() && !injector.membersInjectorStore.hasTypeListeners())) {
            return Initializables.of(instance);
        }
        InjectableReference<T> initializable = new InjectableReference<>(injector, instance, source);
        this.pendingInjection.put(instance, initializable);
        return initializable;
    }

    /* access modifiers changed from: package-private */
    public void validateOustandingInjections(Errors errors) {
        for (InjectableReference<?> reference : this.pendingInjection.values()) {
            try {
                reference.validate(errors);
            } catch (ErrorsException e) {
                errors.merge(e.getErrors());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void injectAll(Errors errors) {
        Iterator i$ = Lists.newArrayList(this.pendingInjection.values()).iterator();
        while (i$.hasNext()) {
            try {
                ((InjectableReference) i$.next()).get(errors);
            } catch (ErrorsException e) {
                errors.merge(e.getErrors());
            }
        }
        if (!this.pendingInjection.isEmpty()) {
            throw new AssertionError("Failed to satisfy " + this.pendingInjection);
        }
        this.ready.countDown();
    }

    private class InjectableReference<T> implements Initializable<T> {
        private final InjectorImpl injector;
        private final T instance;
        private MembersInjectorImpl<T> membersInjector;
        private final Object source;

        public InjectableReference(InjectorImpl injector2, T instance2, Object source2) {
            this.injector = injector2;
            this.instance = Preconditions.checkNotNull(instance2, "instance");
            this.source = Preconditions.checkNotNull(source2, "source");
        }

        public void validate(Errors errors) throws ErrorsException {
            this.membersInjector = this.injector.membersInjectorStore.get(TypeLiteral.get((Class) this.instance.getClass()), errors.withSource(this.source));
        }

        public T get(Errors errors) throws ErrorsException {
            if (Initializer.this.ready.getCount() == 0) {
                return this.instance;
            }
            if (Thread.currentThread() != Initializer.this.creatingThread) {
                try {
                    Initializer.this.ready.await();
                    return this.instance;
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            } else {
                if (Initializer.this.pendingInjection.remove(this.instance) != null) {
                    this.membersInjector.injectAndNotify(this.instance, errors.withSource(this.source));
                }
                return this.instance;
            }
        }

        public String toString() {
            return this.instance.toString();
        }
    }
}
