package com.google.inject;

import com.google.inject.BindingProcessor;
import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.InternalContext;
import com.google.inject.internal.InternalFactory;
import com.google.inject.spi.Dependency;

class BoundProviderFactory<T> implements InternalFactory<T>, BindingProcessor.CreationListener {
    private final InjectorImpl injector;
    private InternalFactory<? extends Provider<? extends T>> providerFactory;
    final Key<? extends Provider<? extends T>> providerKey;
    final Object source;

    BoundProviderFactory(InjectorImpl injector2, Key<? extends Provider<? extends T>> providerKey2, Object source2) {
        this.injector = injector2;
        this.providerKey = providerKey2;
        this.source = source2;
    }

    public void notify(Errors errors) {
        try {
            this.providerFactory = this.injector.getInternalFactory(this.providerKey, errors.withSource(this.source));
        } catch (ErrorsException e) {
            errors.merge(e.getErrors());
        }
    }

    public T get(Errors errors, InternalContext context, Dependency<?> dependency) throws ErrorsException {
        Errors errors2 = errors.withSource(this.providerKey);
        try {
            return errors2.checkForNull(((Provider) this.providerFactory.get(errors2, context, dependency)).get(), this.source, dependency);
        } catch (RuntimeException e) {
            throw errors2.errorInProvider(e).toException();
        }
    }

    public String toString() {
        return this.providerKey.toString();
    }
}
