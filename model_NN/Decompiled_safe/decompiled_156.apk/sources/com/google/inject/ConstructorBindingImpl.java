package com.google.inject;

import com.google.inject.internal.BindingImpl;
import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.ImmutableSet;
import com.google.inject.internal.InternalContext;
import com.google.inject.internal.InternalFactory;
import com.google.inject.internal.Preconditions;
import com.google.inject.internal.Scoping;
import com.google.inject.internal.ToStringBuilder;
import com.google.inject.spi.BindingTargetVisitor;
import com.google.inject.spi.ConstructorBinding;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.InjectionPoint;
import java.util.Set;

class ConstructorBindingImpl<T> extends BindingImpl<T> implements ConstructorBinding<T> {
    private final Factory<T> factory;

    private ConstructorBindingImpl(Injector injector, Key<T> key, Object source, InternalFactory<? extends T> scopedFactory, Scoping scoping, Factory<T> factory2) {
        super(injector, key, source, scopedFactory, scoping);
        this.factory = factory2;
    }

    static <T> ConstructorBindingImpl<T> create(InjectorImpl injector, Key<T> key, Object source, Scoping scoping) {
        Factory<T> factoryFactory = new Factory<>();
        return new ConstructorBindingImpl<>(injector, key, source, Scopes.scope(key, injector, factoryFactory, scoping), scoping, factoryFactory);
    }

    public void initialize(InjectorImpl injector, Errors errors) throws ErrorsException {
        ConstructorInjector unused = this.factory.constructorInjector = injector.constructors.get(getKey().getTypeLiteral(), errors);
    }

    public <V> V acceptTargetVisitor(BindingTargetVisitor<? super T, V> visitor) {
        Preconditions.checkState(this.factory.constructorInjector != null, "not initialized");
        return visitor.visit(this);
    }

    public InjectionPoint getConstructor() {
        Preconditions.checkState(this.factory.constructorInjector != null, "Binding is not ready");
        return this.factory.constructorInjector.getConstructionProxy().getInjectionPoint();
    }

    public Set<InjectionPoint> getInjectableMembers() {
        Preconditions.checkState(this.factory.constructorInjector != null, "Binding is not ready");
        return this.factory.constructorInjector.getInjectableMembers();
    }

    public Set<Dependency<?>> getDependencies() {
        return Dependency.forInjectionPoints(new ImmutableSet.Builder().add(getConstructor()).addAll(getInjectableMembers()).build());
    }

    public void applyTo(Binder binder) {
        throw new UnsupportedOperationException("This element represents a synthetic binding.");
    }

    public String toString() {
        return new ToStringBuilder(ConstructorBinding.class).add("key", getKey()).add("source", getSource()).add("scope", getScoping()).toString();
    }

    private static class Factory<T> implements InternalFactory<T> {
        /* access modifiers changed from: private */
        public ConstructorInjector<T> constructorInjector;

        private Factory() {
        }

        public T get(Errors errors, InternalContext context, Dependency<?> dependency) throws ErrorsException {
            Preconditions.checkState(this.constructorInjector != null, "Constructor not ready");
            return this.constructorInjector.construct(errors, context, dependency.getKey().getRawType());
        }
    }
}
