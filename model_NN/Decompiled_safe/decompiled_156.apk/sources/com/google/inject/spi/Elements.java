package com.google.inject.spi;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.Binding;
import com.google.inject.Key;
import com.google.inject.MembersInjector;
import com.google.inject.Module;
import com.google.inject.PrivateBinder;
import com.google.inject.PrivateModule;
import com.google.inject.Provider;
import com.google.inject.Scope;
import com.google.inject.Stage;
import com.google.inject.TypeLiteral;
import com.google.inject.binder.AnnotatedBindingBuilder;
import com.google.inject.binder.AnnotatedConstantBindingBuilder;
import com.google.inject.binder.AnnotatedElementBuilder;
import com.google.inject.internal.AbstractBindingBuilder;
import com.google.inject.internal.BindingBuilder;
import com.google.inject.internal.ConstantBindingBuilderImpl;
import com.google.inject.internal.Errors;
import com.google.inject.internal.ExposureBuilder;
import com.google.inject.internal.ImmutableList;
import com.google.inject.internal.Lists;
import com.google.inject.internal.Preconditions;
import com.google.inject.internal.PrivateElementsImpl;
import com.google.inject.internal.ProviderMethodsModule;
import com.google.inject.internal.Sets;
import com.google.inject.internal.SourceProvider;
import com.google.inject.matcher.Matcher;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public final class Elements {
    private static final BindingTargetVisitor<Object, Object> GET_INSTANCE_VISITOR = new DefaultBindingTargetVisitor<Object, Object>() {
        public Object visit(InstanceBinding<?> binding) {
            return binding.getInstance();
        }

        /* access modifiers changed from: protected */
        public Object visitOther(Binding<?> binding) {
            throw new IllegalArgumentException();
        }
    };

    public static List<Element> getElements(Module... modules) {
        return getElements(Stage.DEVELOPMENT, Arrays.asList(modules));
    }

    public static List<Element> getElements(Stage stage, Module... modules) {
        return getElements(stage, Arrays.asList(modules));
    }

    public static List<Element> getElements(Iterable<? extends Module> modules) {
        return getElements(Stage.DEVELOPMENT, modules);
    }

    public static List<Element> getElements(Stage stage, Iterable<? extends Module> modules) {
        RecordingBinder binder = new RecordingBinder(stage);
        for (Module module : modules) {
            binder.install(module);
        }
        return Collections.unmodifiableList(binder.elements);
    }

    public static Module getModule(final Iterable<? extends Element> elements) {
        return new Module() {
            public void configure(Binder binder) {
                for (Element element : elements) {
                    element.applyTo(binder);
                }
            }
        };
    }

    static <T> BindingTargetVisitor<T, T> getInstanceVisitor() {
        return GET_INSTANCE_VISITOR;
    }

    private static class RecordingBinder implements Binder, PrivateBinder {
        /* access modifiers changed from: private */
        public final List<Element> elements;
        private final Set<Module> modules;
        private final RecordingBinder parent;
        private final PrivateElementsImpl privateElements;
        private final Object source;
        private final SourceProvider sourceProvider;
        private final Stage stage;

        private RecordingBinder(Stage stage2) {
            this.stage = stage2;
            this.modules = Sets.newHashSet();
            this.elements = Lists.newArrayList();
            this.source = null;
            this.sourceProvider = new SourceProvider().plusSkippedClasses(Elements.class, RecordingBinder.class, AbstractModule.class, ConstantBindingBuilderImpl.class, AbstractBindingBuilder.class, BindingBuilder.class);
            this.parent = null;
            this.privateElements = null;
        }

        private RecordingBinder(RecordingBinder prototype, Object source2, SourceProvider sourceProvider2) {
            Preconditions.checkArgument((source2 == null) ^ (sourceProvider2 == null));
            this.stage = prototype.stage;
            this.modules = prototype.modules;
            this.elements = prototype.elements;
            this.source = source2;
            this.sourceProvider = sourceProvider2;
            this.parent = prototype.parent;
            this.privateElements = prototype.privateElements;
        }

        private RecordingBinder(RecordingBinder parent2, PrivateElementsImpl privateElements2) {
            this.stage = parent2.stage;
            this.modules = Sets.newHashSet();
            this.elements = privateElements2.getElementsMutable();
            this.source = parent2.source;
            this.sourceProvider = parent2.sourceProvider;
            this.parent = parent2;
            this.privateElements = privateElements2;
        }

        public void bindScope(Class<? extends Annotation> annotationType, Scope scope) {
            this.elements.add(new ScopeBinding(getSource(), annotationType, scope));
        }

        public void requestInjection(Object instance) {
            requestInjection(TypeLiteral.get((Class) instance.getClass()), instance);
        }

        public <T> void requestInjection(TypeLiteral<T> type, T instance) {
            this.elements.add(new InjectionRequest(getSource(), type, instance));
        }

        public <T> MembersInjector<T> getMembersInjector(TypeLiteral<T> typeLiteral) {
            MembersInjectorLookup<T> element = new MembersInjectorLookup<>(getSource(), typeLiteral);
            this.elements.add(element);
            return element.getMembersInjector();
        }

        public <T> MembersInjector<T> getMembersInjector(Class<T> type) {
            return getMembersInjector(TypeLiteral.get((Class) type));
        }

        public void bindListener(Matcher<? super TypeLiteral<?>> typeMatcher, TypeListener listener) {
            this.elements.add(new TypeListenerBinding(getSource(), listener, typeMatcher));
        }

        public void requestStaticInjection(Class<?>... types) {
            for (Class<?> type : types) {
                this.elements.add(new StaticInjectionRequest(getSource(), type));
            }
        }

        public void install(Module module) {
            if (this.modules.add(module)) {
                Binder binder = this;
                if (module instanceof PrivateModule) {
                    binder = binder.newPrivateBinder();
                }
                try {
                    module.configure(binder);
                } catch (RuntimeException e) {
                    Collection<Message> messages = Errors.getMessagesFromThrowable(e);
                    if (!messages.isEmpty()) {
                        this.elements.addAll(messages);
                    } else {
                        addError(e);
                    }
                }
                binder.install(ProviderMethodsModule.forModule(module));
            }
        }

        public Stage currentStage() {
            return this.stage;
        }

        public void addError(String message, Object... arguments) {
            this.elements.add(new Message(getSource(), Errors.format(message, arguments)));
        }

        public void addError(Throwable t) {
            this.elements.add(new Message(ImmutableList.of(getSource()), "An exception was caught and reported. Message: " + t.getMessage(), t));
        }

        public void addError(Message message) {
            this.elements.add(message);
        }

        public <T> AnnotatedBindingBuilder<T> bind(Key<T> key) {
            return new BindingBuilder(this, this.elements, getSource(), key);
        }

        public <T> AnnotatedBindingBuilder<T> bind(TypeLiteral<T> typeLiteral) {
            return bind((Key) Key.get(typeLiteral));
        }

        public <T> AnnotatedBindingBuilder<T> bind(Class<T> type) {
            return bind((Key) Key.get((Class) type));
        }

        public AnnotatedConstantBindingBuilder bindConstant() {
            return new ConstantBindingBuilderImpl(this, this.elements, getSource());
        }

        public <T> Provider<T> getProvider(Key<T> key) {
            ProviderLookup<T> element = new ProviderLookup<>(getSource(), key);
            this.elements.add(element);
            return element.getProvider();
        }

        public <T> Provider<T> getProvider(Class<T> type) {
            return getProvider(Key.get((Class) type));
        }

        public void convertToTypes(Matcher<? super TypeLiteral<?>> typeMatcher, TypeConverter converter) {
            this.elements.add(new TypeConverterBinding(getSource(), typeMatcher, converter));
        }

        public RecordingBinder withSource(Object source2) {
            return new RecordingBinder(this, source2, null);
        }

        public RecordingBinder skipSources(Class... classesToSkip) {
            return this.source != null ? this : new RecordingBinder(this, null, this.sourceProvider.plusSkippedClasses(classesToSkip));
        }

        public PrivateBinder newPrivateBinder() {
            PrivateElementsImpl privateElements2 = new PrivateElementsImpl(getSource());
            this.elements.add(privateElements2);
            return new RecordingBinder(this, privateElements2);
        }

        public void expose(Key<?> key) {
            exposeInternal(key);
        }

        public AnnotatedElementBuilder expose(Class<?> type) {
            return exposeInternal(Key.get((Class) type));
        }

        public AnnotatedElementBuilder expose(TypeLiteral<?> type) {
            return exposeInternal(Key.get(type));
        }

        private <T> AnnotatedElementBuilder exposeInternal(Key<T> key) {
            if (this.privateElements == null) {
                addError("Cannot expose %s on a standard binder. Exposed bindings are only applicable to private binders.", key);
                return new AnnotatedElementBuilder() {
                    public void annotatedWith(Class<? extends Annotation> cls) {
                    }

                    public void annotatedWith(Annotation annotation) {
                    }
                };
            }
            ExposureBuilder<T> builder = new ExposureBuilder<>(this, getSource(), key);
            this.privateElements.addExposureBuilder(builder);
            return builder;
        }

        /* access modifiers changed from: protected */
        public Object getSource() {
            return this.sourceProvider != null ? this.sourceProvider.get() : this.source;
        }

        public String toString() {
            return "Binder";
        }
    }
}
