package com.google.inject.spi;

import com.google.inject.Binder;
import com.google.inject.ConfigurationException;
import com.google.inject.internal.Preconditions;
import java.util.Set;

public final class StaticInjectionRequest implements Element {
    private final Object source;
    private final Class<?> type;

    StaticInjectionRequest(Object source2, Class<?> type2) {
        this.source = Preconditions.checkNotNull(source2, "source");
        this.type = (Class) Preconditions.checkNotNull(type2, "type");
    }

    public Object getSource() {
        return this.source;
    }

    public Class<?> getType() {
        return this.type;
    }

    public Set<InjectionPoint> getInjectionPoints() throws ConfigurationException {
        return InjectionPoint.forStaticMethodsAndFields(this.type);
    }

    public void applyTo(Binder binder) {
        binder.withSource(getSource()).requestStaticInjection(this.type);
    }

    public <T> T acceptVisitor(ElementVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
