package com.google.inject.spi;

import com.google.inject.ConfigurationException;
import com.google.inject.Inject;
import com.google.inject.Key;
import com.google.inject.TypeLiteral;
import com.google.inject.internal.Annotations;
import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.ImmutableList;
import com.google.inject.internal.ImmutableSet;
import com.google.inject.internal.Lists;
import com.google.inject.internal.MoreTypes;
import com.google.inject.internal.Nullability;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class InjectionPoint {
    private final ImmutableList<Dependency<?>> dependencies;
    private final Member member;
    private final boolean optional;

    private interface Factory<M extends Member & AnnotatedElement> {
        public static final Factory<Field> FIELDS = new Factory<Field>() {
            public /* bridge */ /* synthetic */ InjectionPoint create(TypeLiteral x0, Member x1, Errors x2) {
                return create((TypeLiteral<?>) x0, (Field) x1, x2);
            }

            public Field[] getMembers(Class<?> type) {
                return type.getDeclaredFields();
            }

            public InjectionPoint create(TypeLiteral<?> typeLiteral, Field member, Errors errors) {
                return new InjectionPoint(typeLiteral, member);
            }
        };
        public static final Factory<Method> METHODS = new Factory<Method>() {
            public /* bridge */ /* synthetic */ InjectionPoint create(TypeLiteral x0, Member x1, Errors x2) {
                return create((TypeLiteral<?>) x0, (Method) x1, x2);
            }

            public Method[] getMembers(Class<?> type) {
                return type.getDeclaredMethods();
            }

            public InjectionPoint create(TypeLiteral<?> typeLiteral, Method member, Errors errors) {
                InjectionPoint.checkForMisplacedBindingAnnotations(member, errors);
                return new InjectionPoint(typeLiteral, member);
            }
        };

        InjectionPoint create(TypeLiteral<?> typeLiteral, M m, Errors errors);

        M[] getMembers(Class<?> cls);
    }

    private InjectionPoint(Member member2, ImmutableList<Dependency<?>> dependencies2, boolean optional2) {
        this.member = member2;
        this.dependencies = dependencies2;
        this.optional = optional2;
    }

    InjectionPoint(TypeLiteral<?> type, Method method) {
        this.member = method;
        this.optional = ((Inject) method.getAnnotation(Inject.class)).optional();
        this.dependencies = forMember(method, type, method.getParameterAnnotations());
    }

    InjectionPoint(TypeLiteral<?> type, Constructor<?> constructor) {
        this.member = constructor;
        this.optional = false;
        this.dependencies = forMember(constructor, type, constructor.getParameterAnnotations());
    }

    InjectionPoint(TypeLiteral<?> type, Field field) {
        this.member = field;
        this.optional = ((Inject) field.getAnnotation(Inject.class)).optional();
        Annotation[] annotations = field.getAnnotations();
        Errors errors = new Errors(field);
        Key<?> key = null;
        try {
            key = Annotations.getKey(type.getFieldType(field), field, annotations, errors);
        } catch (ErrorsException e) {
            errors.merge(e.getErrors());
        }
        errors.throwConfigurationExceptionIfErrorsExist();
        this.dependencies = ImmutableList.of(newDependency(key, Nullability.allowsNull(annotations), -1));
    }

    private ImmutableList<Dependency<?>> forMember(Member member2, TypeLiteral<?> type, Annotation[][] paramterAnnotations) {
        Errors errors = new Errors(member2);
        Iterator<Annotation[]> annotationsIterator = Arrays.asList(paramterAnnotations).iterator();
        List<Dependency<?>> dependencies2 = Lists.newArrayList();
        int index = 0;
        for (TypeLiteral<?> parameterType : type.getParameterTypes(member2)) {
            try {
                Annotation[] parameterAnnotations = (Annotation[]) annotationsIterator.next();
                dependencies2.add(newDependency(Annotations.getKey(parameterType, member2, parameterAnnotations, errors), Nullability.allowsNull(parameterAnnotations), index));
                index++;
            } catch (ErrorsException e) {
                errors.merge(e.getErrors());
            }
        }
        errors.throwConfigurationExceptionIfErrorsExist();
        return ImmutableList.copyOf(dependencies2);
    }

    private <T> Dependency<T> newDependency(Key<T> key, boolean allowsNull, int parameterIndex) {
        return new Dependency<>(this, key, allowsNull, parameterIndex);
    }

    public Member getMember() {
        return this.member;
    }

    public List<Dependency<?>> getDependencies() {
        return this.dependencies;
    }

    public boolean isOptional() {
        return this.optional;
    }

    public boolean equals(Object o) {
        return (o instanceof InjectionPoint) && this.member.equals(((InjectionPoint) o).member);
    }

    public int hashCode() {
        return this.member.hashCode();
    }

    public String toString() {
        return MoreTypes.toString(this.member);
    }

    public static InjectionPoint forConstructorOf(TypeLiteral<?> type) {
        Class<?> rawType = MoreTypes.getRawType(type.getType());
        Errors errors = new Errors(rawType);
        Constructor<?> injectableConstructor = null;
        for (Constructor<?> constructor : rawType.getDeclaredConstructors()) {
            Inject inject = (Inject) constructor.getAnnotation(Inject.class);
            if (inject != null) {
                if (inject.optional()) {
                    errors.optionalConstructor(constructor);
                }
                if (injectableConstructor != null) {
                    errors.tooManyConstructors(rawType);
                }
                injectableConstructor = constructor;
                checkForMisplacedBindingAnnotations(injectableConstructor, errors);
            }
        }
        errors.throwConfigurationExceptionIfErrorsExist();
        if (injectableConstructor != null) {
            return new InjectionPoint(type, injectableConstructor);
        }
        try {
            Constructor<?> noArgConstructor = rawType.getDeclaredConstructor(new Class[0]);
            if (!Modifier.isPrivate(noArgConstructor.getModifiers()) || Modifier.isPrivate(rawType.getModifiers())) {
                checkForMisplacedBindingAnnotations(noArgConstructor, errors);
                return new InjectionPoint(type, noArgConstructor);
            }
            errors.missingConstructor(rawType);
            throw new ConfigurationException(errors.getMessages());
        } catch (NoSuchMethodException e) {
            errors.missingConstructor(rawType);
            throw new ConfigurationException(errors.getMessages());
        }
    }

    public static InjectionPoint forConstructorOf(Class<?> type) {
        return forConstructorOf(TypeLiteral.get((Class) type));
    }

    public static Set<InjectionPoint> forStaticMethodsAndFields(TypeLiteral type) {
        List<InjectionPoint> sink = Lists.newArrayList();
        Errors errors = new Errors();
        addInjectionPoints(type, Factory.FIELDS, true, sink, errors);
        addInjectionPoints(type, Factory.METHODS, true, sink, errors);
        ImmutableSet<InjectionPoint> result = ImmutableSet.copyOf(sink);
        if (!errors.hasErrors()) {
            return result;
        }
        throw new ConfigurationException(errors.getMessages()).withPartialValue(result);
    }

    public static Set<InjectionPoint> forStaticMethodsAndFields(Class<?> type) {
        return forStaticMethodsAndFields(TypeLiteral.get((Class) type));
    }

    public static Set<InjectionPoint> forInstanceMethodsAndFields(TypeLiteral<?> type) {
        List<InjectionPoint> sink = Lists.newArrayList();
        Errors errors = new Errors();
        addInjectionPoints(type, Factory.FIELDS, false, sink, errors);
        addInjectionPoints(type, Factory.METHODS, false, sink, errors);
        ImmutableSet<InjectionPoint> result = ImmutableSet.copyOf(sink);
        if (!errors.hasErrors()) {
            return result;
        }
        throw new ConfigurationException(errors.getMessages()).withPartialValue(result);
    }

    public static Set<InjectionPoint> forInstanceMethodsAndFields(Class<?> type) {
        return forInstanceMethodsAndFields(TypeLiteral.get((Class) type));
    }

    /* access modifiers changed from: private */
    public static void checkForMisplacedBindingAnnotations(Member member2, Errors errors) {
        Annotation misplacedBindingAnnotation = Annotations.findBindingAnnotation(errors, member2, ((AnnotatedElement) member2).getAnnotations());
        if (misplacedBindingAnnotation != null) {
            if (member2 instanceof Method) {
                try {
                    if (member2.getDeclaringClass().getDeclaredField(member2.getName()) != null) {
                        return;
                    }
                } catch (NoSuchFieldException e) {
                }
            }
            errors.misplacedBindingAnnotation(member2, misplacedBindingAnnotation);
        }
    }

    private static <M extends Member & AnnotatedElement> void addInjectionPoints(TypeLiteral<?> type, Factory<M> factory, boolean statics, Collection<InjectionPoint> injectionPoints, Errors errors) {
        if (type.getType() != Object.class) {
            addInjectionPoints(type.getSupertype(type.getRawType().getSuperclass()), factory, statics, injectionPoints, errors);
            addInjectorsForMembers(type, factory, statics, injectionPoints, errors);
        }
    }

    private static <M extends Member & AnnotatedElement> void addInjectorsForMembers(TypeLiteral<?> typeLiteral, Factory<M> factory, boolean statics, Collection<InjectionPoint> injectionPoints, Errors errors) {
        Inject inject;
        for (M member2 : factory.getMembers(MoreTypes.getRawType(typeLiteral.getType()))) {
            if (isStatic(member2) == statics && (inject = (Inject) ((AnnotatedElement) member2).getAnnotation(Inject.class)) != null) {
                try {
                    injectionPoints.add(factory.create(typeLiteral, member2, errors));
                } catch (ConfigurationException e) {
                    ConfigurationException ignorable = e;
                    if (!inject.optional()) {
                        errors.merge(ignorable.getErrorMessages());
                    }
                }
            }
        }
    }

    private static boolean isStatic(Member member2) {
        return Modifier.isStatic(member2.getModifiers());
    }
}
