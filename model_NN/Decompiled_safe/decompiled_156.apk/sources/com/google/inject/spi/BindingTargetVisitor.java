package com.google.inject.spi;

public interface BindingTargetVisitor<T, V> {
    V visit(ConstructorBinding constructorBinding);

    V visit(ConvertedConstantBinding convertedConstantBinding);

    V visit(ExposedBinding exposedBinding);

    V visit(InstanceBinding instanceBinding);

    V visit(LinkedKeyBinding linkedKeyBinding);

    V visit(ProviderBinding providerBinding);

    V visit(ProviderInstanceBinding providerInstanceBinding);

    V visit(ProviderKeyBinding providerKeyBinding);

    V visit(UntargettedBinding untargettedBinding);
}
