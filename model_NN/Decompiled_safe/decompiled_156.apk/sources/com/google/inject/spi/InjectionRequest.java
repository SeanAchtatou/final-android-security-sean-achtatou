package com.google.inject.spi;

import com.google.inject.Binder;
import com.google.inject.ConfigurationException;
import com.google.inject.TypeLiteral;
import com.google.inject.internal.Preconditions;
import java.util.Set;

public final class InjectionRequest<T> implements Element {
    private final T instance;
    private final Object source;
    private final TypeLiteral<T> type;

    public InjectionRequest(Object source2, TypeLiteral<T> type2, T instance2) {
        this.source = Preconditions.checkNotNull(source2, "source");
        this.type = (TypeLiteral) Preconditions.checkNotNull(type2, "type");
        this.instance = Preconditions.checkNotNull(instance2, "instance");
    }

    public Object getSource() {
        return this.source;
    }

    public T getInstance() {
        return this.instance;
    }

    public TypeLiteral<T> getType() {
        return this.type;
    }

    public Set<InjectionPoint> getInjectionPoints() throws ConfigurationException {
        return InjectionPoint.forInstanceMethodsAndFields(this.instance.getClass());
    }

    public <R> R acceptVisitor(ElementVisitor<R> visitor) {
        return visitor.visit(this);
    }

    public void applyTo(Binder binder) {
        binder.withSource(getSource()).requestInjection(this.type, this.instance);
    }
}
