package com.google.inject.spi;

import com.google.inject.Binding;

public interface ElementVisitor<V> {
    <T> V visit(Binding binding);

    V visit(InjectionRequest injectionRequest);

    <T> V visit(MembersInjectorLookup membersInjectorLookup);

    V visit(Message message);

    V visit(PrivateElements privateElements);

    <T> V visit(ProviderLookup providerLookup);

    V visit(ScopeBinding scopeBinding);

    V visit(StaticInjectionRequest staticInjectionRequest);

    V visit(TypeConverterBinding typeConverterBinding);

    V visit(TypeListenerBinding typeListenerBinding);
}
