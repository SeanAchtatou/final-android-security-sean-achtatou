package com.google.inject.spi;

import com.google.inject.Binder;
import com.google.inject.internal.Errors;
import com.google.inject.internal.ImmutableList;
import com.google.inject.internal.Objects;
import com.google.inject.internal.Preconditions;
import com.google.inject.internal.SourceProvider;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.List;

public final class Message implements Serializable, Element {
    private static final long serialVersionUID = 0;
    private final Throwable cause;
    private final String message;
    private final List<Object> sources;

    public Message(List<Object> sources2, String message2, Throwable cause2) {
        this.sources = ImmutableList.copyOf(sources2);
        this.message = (String) Preconditions.checkNotNull(message2, "message");
        this.cause = cause2;
    }

    public Message(Object source, String message2) {
        this(ImmutableList.of(source), message2, null);
    }

    public Message(String message2) {
        this(ImmutableList.of(), message2, null);
    }

    public String getSource() {
        return this.sources.isEmpty() ? SourceProvider.UNKNOWN_SOURCE.toString() : Errors.convert(this.sources.get(this.sources.size() - 1)).toString();
    }

    public List<Object> getSources() {
        return this.sources;
    }

    public String getMessage() {
        return this.message;
    }

    public <T> T acceptVisitor(ElementVisitor<T> visitor) {
        return visitor.visit(this);
    }

    public Throwable getCause() {
        return this.cause;
    }

    public String toString() {
        return this.message;
    }

    public int hashCode() {
        return this.message.hashCode();
    }

    public boolean equals(Object o) {
        if (!(o instanceof Message)) {
            return false;
        }
        Message e = (Message) o;
        return this.message.equals(e.message) && Objects.equal(this.cause, e.cause) && this.sources.equals(e.sources);
    }

    public void applyTo(Binder binder) {
        binder.withSource(getSource()).addError(this);
    }

    private Object writeReplace() throws ObjectStreamException {
        Object[] sourcesAsStrings = this.sources.toArray();
        for (int i = 0; i < sourcesAsStrings.length; i++) {
            sourcesAsStrings[i] = Errors.convert(sourcesAsStrings[i]).toString();
        }
        return new Message(ImmutableList.of(sourcesAsStrings), this.message, this.cause);
    }
}
