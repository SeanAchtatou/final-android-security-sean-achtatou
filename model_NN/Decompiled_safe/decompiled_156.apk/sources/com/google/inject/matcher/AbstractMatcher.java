package com.google.inject.matcher;

import java.io.Serializable;

public abstract class AbstractMatcher<T> implements Matcher<T> {
    public Matcher<T> and(Matcher<? super T> other) {
        return new AndMatcher(this, other);
    }

    public Matcher<T> or(Matcher<? super T> other) {
        return new OrMatcher(this, other);
    }

    private static class AndMatcher<T> extends AbstractMatcher<T> implements Serializable {
        private static final long serialVersionUID = 0;
        private final Matcher<? super T> a;
        private final Matcher<? super T> b;

        public AndMatcher(Matcher<? super T> a2, Matcher<? super T> b2) {
            this.a = a2;
            this.b = b2;
        }

        public boolean matches(T t) {
            return this.a.matches(t) && this.b.matches(t);
        }

        public boolean equals(Object other) {
            return (other instanceof AndMatcher) && ((AndMatcher) other).a.equals(this.a) && ((AndMatcher) other).b.equals(this.b);
        }

        public int hashCode() {
            return (this.a.hashCode() ^ this.b.hashCode()) * 41;
        }

        public String toString() {
            return "and(" + this.a + ", " + this.b + ")";
        }
    }

    private static class OrMatcher<T> extends AbstractMatcher<T> implements Serializable {
        private static final long serialVersionUID = 0;
        private final Matcher<? super T> a;
        private final Matcher<? super T> b;

        public OrMatcher(Matcher<? super T> a2, Matcher<? super T> b2) {
            this.a = a2;
            this.b = b2;
        }

        public boolean matches(T t) {
            return this.a.matches(t) || this.b.matches(t);
        }

        public boolean equals(Object other) {
            return (other instanceof OrMatcher) && ((OrMatcher) other).a.equals(this.a) && ((OrMatcher) other).b.equals(this.b);
        }

        public int hashCode() {
            return (this.a.hashCode() ^ this.b.hashCode()) * 37;
        }

        public String toString() {
            return "or(" + this.a + ", " + this.b + ")";
        }
    }
}
