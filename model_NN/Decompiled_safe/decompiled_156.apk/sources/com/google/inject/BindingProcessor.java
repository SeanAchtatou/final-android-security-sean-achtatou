package com.google.inject;

import com.google.inject.internal.Annotations;
import com.google.inject.internal.BindingImpl;
import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.ExposedBindingImpl;
import com.google.inject.internal.ImmutableSet;
import com.google.inject.internal.InstanceBindingImpl;
import com.google.inject.internal.LinkedBindingImpl;
import com.google.inject.internal.LinkedProviderBindingImpl;
import com.google.inject.internal.Lists;
import com.google.inject.internal.ProviderInstanceBindingImpl;
import com.google.inject.internal.ProviderMethod;
import com.google.inject.internal.Scoping;
import com.google.inject.internal.UntargettedBindingImpl;
import com.google.inject.spi.BindingTargetVisitor;
import com.google.inject.spi.ConstructorBinding;
import com.google.inject.spi.ConvertedConstantBinding;
import com.google.inject.spi.ExposedBinding;
import com.google.inject.spi.InjectionPoint;
import com.google.inject.spi.InstanceBinding;
import com.google.inject.spi.LinkedKeyBinding;
import com.google.inject.spi.PrivateElements;
import com.google.inject.spi.ProviderBinding;
import com.google.inject.spi.ProviderInstanceBinding;
import com.google.inject.spi.ProviderKeyBinding;
import com.google.inject.spi.UntargettedBinding;
import java.util.List;
import java.util.Set;

class BindingProcessor extends AbstractProcessor {
    private static final Set<Class<?>> FORBIDDEN_TYPES = ImmutableSet.of((Object[]) new Class[]{AbstractModule.class, Binder.class, Binding.class, Injector.class, Key.class, MembersInjector.class, Module.class, Provider.class, Scope.class, TypeLiteral.class});
    /* access modifiers changed from: private */
    public final List<CreationListener> creationListeners = Lists.newArrayList();
    /* access modifiers changed from: private */
    public final Initializer initializer;
    /* access modifiers changed from: private */
    public final List<Runnable> uninitializedBindings = Lists.newArrayList();

    interface CreationListener {
        void notify(Errors errors);
    }

    BindingProcessor(Errors errors, Initializer initializer2) {
        super(errors);
        this.initializer = initializer2;
    }

    public <T> Boolean visit(Binding<T> command) {
        final Object source = command.getSource();
        if (Void.class.equals(command.getKey().getRawType())) {
            if (!(command instanceof ProviderInstanceBinding) || !(((ProviderInstanceBinding) command).getProviderInstance() instanceof ProviderMethod)) {
                this.errors.missingConstantValues();
            } else {
                this.errors.voidProviderMethod();
            }
            return true;
        }
        final Key<T> key = command.getKey();
        if (key.getTypeLiteral().getRawType() == Provider.class) {
            this.errors.bindingToProvider();
            return true;
        }
        validateKey(command.getSource(), command.getKey());
        final Scoping scoping = Scopes.makeInjectable(((BindingImpl) command).getScoping(), this.injector, this.errors);
        command.acceptTargetVisitor(new BindingTargetVisitor<T, Void>() {
            public Void visit(InstanceBinding<? extends T> binding) {
                Set<InjectionPoint> injectionPoints = binding.getInjectionPoints();
                T instance = binding.getInstance();
                BindingProcessor.this.putBinding(new InstanceBindingImpl(BindingProcessor.this.injector, key, source, Scopes.scope(key, BindingProcessor.this.injector, new ConstantFactory<>(BindingProcessor.this.initializer.requestInjection(BindingProcessor.this.injector, instance, source, injectionPoints)), scoping), injectionPoints, instance));
                return null;
            }

            public Void visit(ProviderInstanceBinding<? extends T> binding) {
                Provider<? extends T> provider = binding.getProviderInstance();
                Set<InjectionPoint> injectionPoints = binding.getInjectionPoints();
                BindingProcessor.this.putBinding(new ProviderInstanceBindingImpl(BindingProcessor.this.injector, key, source, Scopes.scope(key, BindingProcessor.this.injector, new InternalFactoryToProviderAdapter<>(BindingProcessor.this.initializer.requestInjection(BindingProcessor.this.injector, provider, source, injectionPoints), source), scoping), scoping, provider, injectionPoints));
                return null;
            }

            public Void visit(ProviderKeyBinding<? extends T> binding) {
                Key<? extends Provider<? extends T>> providerKey = binding.getProviderKey();
                BoundProviderFactory<T> boundProviderFactory = new BoundProviderFactory<>(BindingProcessor.this.injector, providerKey, source);
                BindingProcessor.this.creationListeners.add(boundProviderFactory);
                BindingProcessor.this.putBinding(new LinkedProviderBindingImpl(BindingProcessor.this.injector, key, source, Scopes.scope(key, BindingProcessor.this.injector, boundProviderFactory, scoping), scoping, providerKey));
                return null;
            }

            public Void visit(LinkedKeyBinding<? extends T> binding) {
                Key<? extends T> linkedKey = binding.getLinkedKey();
                if (key.equals(linkedKey)) {
                    BindingProcessor.this.errors.recursiveBinding();
                }
                FactoryProxy<T> factory = new FactoryProxy<>(BindingProcessor.this.injector, key, linkedKey, source);
                BindingProcessor.this.creationListeners.add(factory);
                BindingProcessor.this.putBinding(new LinkedBindingImpl(BindingProcessor.this.injector, key, source, Scopes.scope(key, BindingProcessor.this.injector, factory, scoping), scoping, linkedKey));
                return null;
            }

            public Void visit(UntargettedBinding<? extends T> untargettedBinding) {
                if (key.hasAnnotationType()) {
                    BindingProcessor.this.errors.missingImplementation(key);
                    BindingProcessor.this.putBinding(BindingProcessor.this.invalidBinding(BindingProcessor.this.injector, key, source));
                    return null;
                }
                try {
                    final BindingImpl<T> binding = BindingProcessor.this.injector.createUnitializedBinding(key, scoping, source, BindingProcessor.this.errors);
                    BindingProcessor.this.putBinding(binding);
                    BindingProcessor.this.uninitializedBindings.add(new Runnable() {
                        public void run() {
                            try {
                                ((InjectorImpl) binding.getInjector()).initializeBinding(binding, BindingProcessor.this.errors.withSource(source));
                            } catch (ErrorsException e) {
                                BindingProcessor.this.errors.merge(e.getErrors());
                            }
                        }
                    });
                    return null;
                } catch (ErrorsException e) {
                    BindingProcessor.this.errors.merge(e.getErrors());
                    BindingProcessor.this.putBinding(BindingProcessor.this.invalidBinding(BindingProcessor.this.injector, key, source));
                    return null;
                }
            }

            public Void visit(ExposedBinding<? extends T> exposedBinding) {
                throw new IllegalArgumentException("Cannot apply a non-module element");
            }

            public Void visit(ConvertedConstantBinding<? extends T> convertedConstantBinding) {
                throw new IllegalArgumentException("Cannot apply a non-module element");
            }

            public Void visit(ConstructorBinding<? extends T> constructorBinding) {
                throw new IllegalArgumentException("Cannot apply a non-module element");
            }

            public Void visit(ProviderBinding<? extends T> providerBinding) {
                throw new IllegalArgumentException("Cannot apply a non-module element");
            }
        });
        return true;
    }

    public Boolean visit(PrivateElements privateElements) {
        for (Key<?> key : privateElements.getExposedKeys()) {
            bindExposed(privateElements, key);
        }
        return false;
    }

    private <T> void bindExposed(PrivateElements privateElements, Key<T> key) {
        ExposedKeyFactory<T> exposedKeyFactory = new ExposedKeyFactory<>(key, privateElements);
        this.creationListeners.add(exposedKeyFactory);
        putBinding(new ExposedBindingImpl(this.injector, privateElements.getExposedSource(key), key, exposedKeyFactory, privateElements));
    }

    private <T> void validateKey(Object source, Key<T> key) {
        Annotations.checkForMisplacedScopeAnnotations(key.getRawType(), source, this.errors);
    }

    /* access modifiers changed from: package-private */
    public <T> UntargettedBindingImpl<T> invalidBinding(InjectorImpl injector, Key<T> key, Object source) {
        return new UntargettedBindingImpl<>(injector, key, source);
    }

    public void initializeBindings() {
        for (Runnable initializer2 : this.uninitializedBindings) {
            initializer2.run();
        }
    }

    public void runCreationListeners() {
        for (CreationListener creationListener : this.creationListeners) {
            creationListener.notify(this.errors);
        }
    }

    /* access modifiers changed from: private */
    public void putBinding(BindingImpl<?> binding) {
        Key<?> key = binding.getKey();
        Class<?> rawType = key.getRawType();
        if (FORBIDDEN_TYPES.contains(rawType)) {
            this.errors.cannotBindToGuiceType(rawType.getSimpleName());
            return;
        }
        Binding<?> original = this.injector.state.getExplicitBinding(key);
        if (original == null || isOkayDuplicate(original, binding)) {
            this.injector.state.parent().blacklist(key);
            this.injector.state.putBinding(key, binding);
            return;
        }
        this.errors.bindingAlreadySet(key, original.getSource());
    }

    private boolean isOkayDuplicate(Binding<?> original, BindingImpl<?> binding) {
        if (!(original instanceof ExposedBindingImpl)) {
            return false;
        }
        if (((InjectorImpl) ((ExposedBindingImpl) original).getPrivateElements().getInjector()) == binding.getInjector()) {
            return true;
        }
        return false;
    }
}
