package com.google.inject;

import com.google.inject.internal.Annotations;
import com.google.inject.internal.MoreTypes;
import com.google.inject.internal.Preconditions;
import com.google.inject.internal.ToStringBuilder;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

public class Key<T> {
    private final AnnotationStrategy annotationStrategy;
    private final int hashCode;
    private final TypeLiteral<T> typeLiteral;

    interface AnnotationStrategy {
        Annotation getAnnotation();

        Class<? extends Annotation> getAnnotationType();

        boolean hasAttributes();

        AnnotationStrategy withoutAttributes();
    }

    protected Key(Class<? extends Annotation> annotationType) {
        this.annotationStrategy = strategyFor(annotationType);
        this.typeLiteral = TypeLiteral.fromSuperclassTypeParameter(getClass());
        this.hashCode = computeHashCode();
    }

    protected Key(Annotation annotation) {
        this.annotationStrategy = strategyFor(annotation);
        this.typeLiteral = TypeLiteral.fromSuperclassTypeParameter(getClass());
        this.hashCode = computeHashCode();
    }

    protected Key() {
        this.annotationStrategy = NullAnnotationStrategy.INSTANCE;
        this.typeLiteral = TypeLiteral.fromSuperclassTypeParameter(getClass());
        this.hashCode = computeHashCode();
    }

    private Key(Type type, AnnotationStrategy annotationStrategy2) {
        this.annotationStrategy = annotationStrategy2;
        this.typeLiteral = MoreTypes.makeKeySafe(TypeLiteral.get(type));
        this.hashCode = computeHashCode();
    }

    private Key(TypeLiteral<T> typeLiteral2, AnnotationStrategy annotationStrategy2) {
        this.annotationStrategy = annotationStrategy2;
        this.typeLiteral = MoreTypes.makeKeySafe(typeLiteral2);
        this.hashCode = computeHashCode();
    }

    private int computeHashCode() {
        return (this.typeLiteral.hashCode() * 31) + this.annotationStrategy.hashCode();
    }

    public final TypeLiteral<T> getTypeLiteral() {
        return this.typeLiteral;
    }

    public final Class<? extends Annotation> getAnnotationType() {
        return this.annotationStrategy.getAnnotationType();
    }

    public final Annotation getAnnotation() {
        return this.annotationStrategy.getAnnotation();
    }

    /* access modifiers changed from: package-private */
    public boolean hasAnnotationType() {
        return this.annotationStrategy.getAnnotationType() != null;
    }

    /* access modifiers changed from: package-private */
    public String getAnnotationName() {
        Annotation annotation = this.annotationStrategy.getAnnotation();
        if (annotation != null) {
            return annotation.toString();
        }
        return this.annotationStrategy.getAnnotationType().toString();
    }

    /* access modifiers changed from: package-private */
    public Class<? super T> getRawType() {
        return this.typeLiteral.getRawType();
    }

    /* access modifiers changed from: package-private */
    public Key<Provider<T>> providerKey() {
        return ofType(this.typeLiteral.providerType());
    }

    public final boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Key)) {
            return false;
        }
        Key<?> other = (Key) o;
        return this.annotationStrategy.equals(other.annotationStrategy) && this.typeLiteral.equals(other.typeLiteral);
    }

    public final int hashCode() {
        return this.hashCode;
    }

    public final String toString() {
        return new ToStringBuilder(Key.class).add("type", this.typeLiteral).add("annotation", this.annotationStrategy).toString();
    }

    static <T> Key<T> get(Class<T> type, AnnotationStrategy annotationStrategy2) {
        return new Key<>(type, annotationStrategy2);
    }

    public static <T> Key<T> get(Class<T> type) {
        return new Key<>(type, NullAnnotationStrategy.INSTANCE);
    }

    public static <T> Key<T> get(Class<T> type, Class<? extends Annotation> annotationType) {
        return new Key<>(type, strategyFor(annotationType));
    }

    public static <T> Key<T> get(Class<T> type, Annotation annotation) {
        return new Key<>(type, strategyFor(annotation));
    }

    public static Key<?> get(Type type) {
        return new Key<>(type, NullAnnotationStrategy.INSTANCE);
    }

    public static Key<?> get(Type type, Class<? extends Annotation> annotationType) {
        return new Key<>(type, strategyFor(annotationType));
    }

    public static Key<?> get(Type type, Annotation annotation) {
        return new Key<>(type, strategyFor(annotation));
    }

    public static <T> Key<T> get(TypeLiteral<T> typeLiteral2) {
        return new Key<>(typeLiteral2, NullAnnotationStrategy.INSTANCE);
    }

    public static <T> Key<T> get(TypeLiteral<T> typeLiteral2, Class<? extends Annotation> annotationType) {
        return new Key<>(typeLiteral2, strategyFor(annotationType));
    }

    public static <T> Key<T> get(TypeLiteral<T> typeLiteral2, Annotation annotation) {
        return new Key<>(typeLiteral2, strategyFor(annotation));
    }

    /* access modifiers changed from: package-private */
    public <T> Key<T> ofType(Class<T> type) {
        return new Key<>(type, this.annotationStrategy);
    }

    /* access modifiers changed from: package-private */
    public Key<?> ofType(Type type) {
        return new Key<>(type, this.annotationStrategy);
    }

    /* access modifiers changed from: package-private */
    public <T> Key<T> ofType(TypeLiteral<T> type) {
        return new Key<>(type, this.annotationStrategy);
    }

    /* access modifiers changed from: package-private */
    public boolean hasAttributes() {
        return this.annotationStrategy.hasAttributes();
    }

    /* access modifiers changed from: package-private */
    public Key<T> withoutAttributes() {
        return new Key<>(this.typeLiteral, this.annotationStrategy.withoutAttributes());
    }

    static boolean isMarker(Class<? extends Annotation> annotationType) {
        return annotationType.getDeclaredMethods().length == 0;
    }

    static AnnotationStrategy strategyFor(Annotation annotation) {
        Preconditions.checkNotNull(annotation, "annotation");
        Class<? extends Annotation> annotationType = annotation.annotationType();
        ensureRetainedAtRuntime(annotationType);
        ensureIsBindingAnnotation(annotationType);
        if (annotationType.getDeclaredMethods().length == 0) {
            return new AnnotationTypeStrategy(annotationType, annotation);
        }
        return new AnnotationInstanceStrategy(annotation);
    }

    static AnnotationStrategy strategyFor(Class<? extends Annotation> annotationType) {
        Preconditions.checkNotNull(annotationType, "annotation type");
        ensureRetainedAtRuntime(annotationType);
        ensureIsBindingAnnotation(annotationType);
        return new AnnotationTypeStrategy(annotationType, null);
    }

    private static void ensureRetainedAtRuntime(Class<? extends Annotation> annotationType) {
        Preconditions.checkArgument(Annotations.isRetainedAtRuntime(annotationType), "%s is not retained at runtime. Please annotate it with @Retention(RUNTIME).", annotationType.getName());
    }

    private static void ensureIsBindingAnnotation(Class<? extends Annotation> annotationType) {
        Preconditions.checkArgument(isBindingAnnotation(annotationType), "%s is not a binding annotation. Please annotate it with @BindingAnnotation.", annotationType.getName());
    }

    enum NullAnnotationStrategy implements AnnotationStrategy {
        INSTANCE;

        public boolean hasAttributes() {
            return false;
        }

        public AnnotationStrategy withoutAttributes() {
            throw new UnsupportedOperationException("Key already has no attributes.");
        }

        public Annotation getAnnotation() {
            return null;
        }

        public Class<? extends Annotation> getAnnotationType() {
            return null;
        }

        public String toString() {
            return "[none]";
        }
    }

    static class AnnotationInstanceStrategy implements AnnotationStrategy {
        final Annotation annotation;

        AnnotationInstanceStrategy(Annotation annotation2) {
            this.annotation = (Annotation) Preconditions.checkNotNull(annotation2, "annotation");
        }

        public boolean hasAttributes() {
            return true;
        }

        public AnnotationStrategy withoutAttributes() {
            return new AnnotationTypeStrategy(getAnnotationType(), this.annotation);
        }

        public Annotation getAnnotation() {
            return this.annotation;
        }

        public Class<? extends Annotation> getAnnotationType() {
            return this.annotation.annotationType();
        }

        public boolean equals(Object o) {
            if (!(o instanceof AnnotationInstanceStrategy)) {
                return false;
            }
            return this.annotation.equals(((AnnotationInstanceStrategy) o).annotation);
        }

        public int hashCode() {
            return this.annotation.hashCode();
        }

        public String toString() {
            return this.annotation.toString();
        }
    }

    static class AnnotationTypeStrategy implements AnnotationStrategy {
        final Annotation annotation;
        final Class<? extends Annotation> annotationType;

        AnnotationTypeStrategy(Class<? extends Annotation> annotationType2, Annotation annotation2) {
            this.annotationType = (Class) Preconditions.checkNotNull(annotationType2, "annotation type");
            this.annotation = annotation2;
        }

        public boolean hasAttributes() {
            return false;
        }

        public AnnotationStrategy withoutAttributes() {
            throw new UnsupportedOperationException("Key already has no attributes.");
        }

        public Annotation getAnnotation() {
            return this.annotation;
        }

        public Class<? extends Annotation> getAnnotationType() {
            return this.annotationType;
        }

        public boolean equals(Object o) {
            if (!(o instanceof AnnotationTypeStrategy)) {
                return false;
            }
            return this.annotationType.equals(((AnnotationTypeStrategy) o).annotationType);
        }

        public int hashCode() {
            return this.annotationType.hashCode();
        }

        public String toString() {
            return "@" + this.annotationType.getName();
        }
    }

    static boolean isBindingAnnotation(Annotation annotation) {
        return isBindingAnnotation(annotation.annotationType());
    }

    static boolean isBindingAnnotation(Class<? extends Annotation> annotationType) {
        return annotationType.isAnnotationPresent(BindingAnnotation.class);
    }
}
