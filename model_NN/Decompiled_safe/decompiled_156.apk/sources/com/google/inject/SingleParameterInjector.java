package com.google.inject;

import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.InternalContext;
import com.google.inject.internal.InternalFactory;
import com.google.inject.spi.Dependency;

class SingleParameterInjector<T> {
    private static final Object[] NO_ARGUMENTS = new Object[0];
    private final Dependency<T> dependency;
    private final InternalFactory<? extends T> factory;

    SingleParameterInjector(Dependency<T> dependency2, InternalFactory<? extends T> factory2) {
        this.dependency = dependency2;
        this.factory = factory2;
    }

    private T inject(Errors errors, InternalContext context) throws ErrorsException {
        context.setDependency(this.dependency);
        try {
            return this.factory.get(errors.withSource(this.dependency), context, this.dependency);
        } finally {
            context.setDependency(null);
        }
    }

    static Object[] getAll(Errors errors, InternalContext context, SingleParameterInjector<?>[] parameterInjectors) throws ErrorsException {
        if (parameterInjectors == null) {
            return NO_ARGUMENTS;
        }
        int numErrorsBefore = errors.size();
        int size = parameterInjectors.length;
        Object[] parameters = new Object[size];
        for (int i = 0; i < size; i++) {
            try {
                parameters[i] = parameterInjectors[i].inject(errors, context);
            } catch (ErrorsException e) {
                errors.merge(e.getErrors());
            }
        }
        errors.throwIfNewErrors(numErrorsBefore);
        return parameters;
    }
}
