package com.google.inject;

import com.google.inject.internal.Errors;
import com.google.inject.internal.ImmutableList;
import com.google.inject.internal.ImmutableSet;
import com.google.inject.internal.Preconditions;
import com.google.inject.spi.Message;
import java.util.Collection;

public final class ProvisionException extends RuntimeException {
    private static final long serialVersionUID = 0;
    private final ImmutableSet<Message> messages;

    public ProvisionException(Iterable<Message> messages2) {
        this.messages = ImmutableSet.copyOf(messages2);
        Preconditions.checkArgument(!this.messages.isEmpty());
        initCause(Errors.getOnlyCause(this.messages));
    }

    public ProvisionException(String message, Throwable cause) {
        super(cause);
        this.messages = ImmutableSet.of(new Message(ImmutableList.of(), message, cause));
    }

    public ProvisionException(String message) {
        this.messages = ImmutableSet.of(new Message(message));
    }

    public Collection<Message> getErrorMessages() {
        return this.messages;
    }

    public String getMessage() {
        return Errors.format("Guice provision errors", this.messages);
    }
}
