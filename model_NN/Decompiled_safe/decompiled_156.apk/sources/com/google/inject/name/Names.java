package com.google.inject.name;

import com.google.inject.Binder;
import com.google.inject.Key;
import java.lang.annotation.Annotation;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;

public class Names {
    private Names() {
    }

    public static Named named(String name) {
        return new NamedImpl(name);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.inject.Key.get(java.lang.Class, java.lang.annotation.Annotation):com.google.inject.Key<T>
     arg types: [java.lang.Class, com.google.inject.name.NamedImpl]
     candidates:
      com.google.inject.Key.get(com.google.inject.TypeLiteral, java.lang.Class<? extends java.lang.annotation.Annotation>):com.google.inject.Key<T>
      com.google.inject.Key.get(com.google.inject.TypeLiteral, java.lang.annotation.Annotation):com.google.inject.Key<T>
      com.google.inject.Key.get(java.lang.Class, com.google.inject.Key$AnnotationStrategy):com.google.inject.Key<T>
      com.google.inject.Key.get(java.lang.Class, java.lang.Class<? extends java.lang.annotation.Annotation>):com.google.inject.Key<T>
      com.google.inject.Key.get(java.lang.reflect.Type, java.lang.Class<? extends java.lang.annotation.Annotation>):com.google.inject.Key<?>
      com.google.inject.Key.get(java.lang.reflect.Type, java.lang.annotation.Annotation):com.google.inject.Key<?>
      com.google.inject.Key.get(java.lang.Class, java.lang.annotation.Annotation):com.google.inject.Key<T> */
    public static void bindProperties(Binder binder, Map<String, String> properties) {
        Binder binder2 = binder.skipSources(Names.class);
        for (Map.Entry<String, String> entry : properties.entrySet()) {
            binder2.bind(Key.get(String.class, (Annotation) new NamedImpl((String) entry.getKey()))).toInstance((String) entry.getValue());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.inject.Key.get(java.lang.Class, java.lang.annotation.Annotation):com.google.inject.Key<T>
     arg types: [java.lang.Class, com.google.inject.name.NamedImpl]
     candidates:
      com.google.inject.Key.get(com.google.inject.TypeLiteral, java.lang.Class<? extends java.lang.annotation.Annotation>):com.google.inject.Key<T>
      com.google.inject.Key.get(com.google.inject.TypeLiteral, java.lang.annotation.Annotation):com.google.inject.Key<T>
      com.google.inject.Key.get(java.lang.Class, com.google.inject.Key$AnnotationStrategy):com.google.inject.Key<T>
      com.google.inject.Key.get(java.lang.Class, java.lang.Class<? extends java.lang.annotation.Annotation>):com.google.inject.Key<T>
      com.google.inject.Key.get(java.lang.reflect.Type, java.lang.Class<? extends java.lang.annotation.Annotation>):com.google.inject.Key<?>
      com.google.inject.Key.get(java.lang.reflect.Type, java.lang.annotation.Annotation):com.google.inject.Key<?>
      com.google.inject.Key.get(java.lang.Class, java.lang.annotation.Annotation):com.google.inject.Key<T> */
    public static void bindProperties(Binder binder, Properties properties) {
        Binder binder2 = binder.skipSources(Names.class);
        Enumeration<?> e = properties.propertyNames();
        while (e.hasMoreElements()) {
            String propertyName = (String) e.nextElement();
            binder2.bind(Key.get(String.class, (Annotation) new NamedImpl(propertyName))).toInstance(properties.getProperty(propertyName));
        }
    }
}
