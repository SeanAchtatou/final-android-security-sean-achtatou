package com.google.inject;

import com.google.inject.internal.ImmutableList;
import com.google.inject.internal.MoreTypes;
import com.google.inject.internal.Preconditions;
import com.google.inject.util.Types;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.List;

public class TypeLiteral<T> {
    final int hashCode;
    final Class<? super T> rawType;
    final Type type;

    protected TypeLiteral() {
        this.type = getSuperclassTypeParameter(getClass());
        this.rawType = MoreTypes.getRawType(this.type);
        this.hashCode = MoreTypes.hashCode(this.type);
    }

    TypeLiteral(Type type2) {
        this.type = MoreTypes.canonicalize((Type) Preconditions.checkNotNull(type2, "type"));
        this.rawType = MoreTypes.getRawType(this.type);
        this.hashCode = MoreTypes.hashCode(this.type);
    }

    static Type getSuperclassTypeParameter(Class<?> subclass) {
        Type superclass = subclass.getGenericSuperclass();
        if (!(superclass instanceof Class)) {
            return MoreTypes.canonicalize(((ParameterizedType) superclass).getActualTypeArguments()[0]);
        }
        throw new RuntimeException("Missing type parameter.");
    }

    static TypeLiteral<?> fromSuperclassTypeParameter(Class<?> subclass) {
        return new TypeLiteral<>(getSuperclassTypeParameter(subclass));
    }

    public final Class<? super T> getRawType() {
        return this.rawType;
    }

    public final Type getType() {
        return this.type;
    }

    /* access modifiers changed from: package-private */
    public final TypeLiteral<Provider<T>> providerType() {
        return get(Types.providerOf(getType()));
    }

    public final int hashCode() {
        return this.hashCode;
    }

    public final boolean equals(Object o) {
        return (o instanceof TypeLiteral) && MoreTypes.equals(this.type, ((TypeLiteral) o).type);
    }

    public final String toString() {
        return MoreTypes.toString(this.type);
    }

    public static TypeLiteral<?> get(Type type2) {
        return new TypeLiteral<>(type2);
    }

    public static <T> TypeLiteral<T> get(Class cls) {
        return new TypeLiteral<>(cls);
    }

    private List<TypeLiteral<?>> resolveAll(Type[] types) {
        TypeLiteral<?>[] result = new TypeLiteral[types.length];
        for (int t = 0; t < types.length; t++) {
            result[t] = resolve(types[t]);
        }
        return ImmutableList.of((Object[]) result);
    }

    /* access modifiers changed from: package-private */
    public TypeLiteral<?> resolve(Type toResolve) {
        return get(resolveType(toResolve));
    }

    /* access modifiers changed from: package-private */
    public Type resolveType(Type toResolve) {
        boolean changed;
        while (toResolve instanceof TypeVariable) {
            TypeVariable original = (TypeVariable) toResolve;
            toResolve = MoreTypes.resolveTypeVariable(this.type, this.rawType, original);
            if (toResolve == original) {
                return toResolve;
            }
        }
        if (toResolve instanceof GenericArrayType) {
            GenericArrayType original2 = (GenericArrayType) toResolve;
            Type componentType = original2.getGenericComponentType();
            Type newComponentType = resolveType(componentType);
            return componentType == newComponentType ? original2 : Types.arrayOf(newComponentType);
        } else if (toResolve instanceof ParameterizedType) {
            ParameterizedType original3 = (ParameterizedType) toResolve;
            Type ownerType = original3.getOwnerType();
            Type newOwnerType = resolveType(ownerType);
            if (newOwnerType != ownerType) {
                changed = true;
            } else {
                changed = false;
            }
            Type[] args = original3.getActualTypeArguments();
            int length = args.length;
            for (int t = 0; t < length; t++) {
                Type resolvedTypeArgument = resolveType(args[t]);
                if (resolvedTypeArgument != args[t]) {
                    if (!changed) {
                        args = (Type[]) args.clone();
                        changed = true;
                    }
                    args[t] = resolvedTypeArgument;
                }
            }
            return changed ? Types.newParameterizedTypeWithOwner(newOwnerType, original3.getRawType(), args) : original3;
        } else if (!(toResolve instanceof WildcardType)) {
            return toResolve;
        } else {
            WildcardType original4 = (WildcardType) toResolve;
            Type[] originalLowerBound = original4.getLowerBounds();
            Type[] originalUpperBound = original4.getUpperBounds();
            if (originalLowerBound.length == 1) {
                Type lowerBound = resolveType(originalLowerBound[0]);
                if (lowerBound != originalLowerBound[0]) {
                    return Types.supertypeOf(lowerBound);
                }
            } else if (originalUpperBound.length == 1) {
                Type upperBound = resolveType(originalUpperBound[0]);
                if (upperBound != originalUpperBound[0]) {
                    return Types.subtypeOf(upperBound);
                }
            }
            return original4;
        }
    }

    public TypeLiteral<?> getSupertype(Class<?> supertype) {
        Preconditions.checkArgument(supertype.isAssignableFrom(this.rawType), "%s is not a supertype of %s", supertype, this.type);
        return resolve(MoreTypes.getGenericSupertype(this.type, this.rawType, supertype));
    }

    public TypeLiteral<?> getFieldType(Field field) {
        Preconditions.checkArgument(field.getDeclaringClass().isAssignableFrom(this.rawType), "%s is not defined by a supertype of %s", field, this.type);
        return resolve(field.getGenericType());
    }

    public List<TypeLiteral<?>> getParameterTypes(Member methodOrConstructor) {
        Type[] genericParameterTypes;
        if (methodOrConstructor instanceof Method) {
            Method method = (Method) methodOrConstructor;
            Preconditions.checkArgument(method.getDeclaringClass().isAssignableFrom(this.rawType), "%s is not defined by a supertype of %s", method, this.type);
            genericParameterTypes = method.getGenericParameterTypes();
        } else if (methodOrConstructor instanceof Constructor) {
            Constructor constructor = (Constructor) methodOrConstructor;
            Preconditions.checkArgument(constructor.getDeclaringClass().isAssignableFrom(this.rawType), "%s does not construct a supertype of %s", constructor, this.type);
            genericParameterTypes = constructor.getGenericParameterTypes();
        } else {
            throw new IllegalArgumentException("Not a method or a constructor: " + methodOrConstructor);
        }
        return resolveAll(genericParameterTypes);
    }

    public List<TypeLiteral<?>> getExceptionTypes(Member methodOrConstructor) {
        Type[] genericExceptionTypes;
        if (methodOrConstructor instanceof Method) {
            Method method = (Method) methodOrConstructor;
            Preconditions.checkArgument(method.getDeclaringClass().isAssignableFrom(this.rawType), "%s is not defined by a supertype of %s", method, this.type);
            genericExceptionTypes = method.getGenericExceptionTypes();
        } else if (methodOrConstructor instanceof Constructor) {
            Constructor constructor = (Constructor) methodOrConstructor;
            Preconditions.checkArgument(constructor.getDeclaringClass().isAssignableFrom(this.rawType), "%s does not construct a supertype of %s", constructor, this.type);
            genericExceptionTypes = constructor.getGenericExceptionTypes();
        } else {
            throw new IllegalArgumentException("Not a method or a constructor: " + methodOrConstructor);
        }
        return resolveAll(genericExceptionTypes);
    }

    public TypeLiteral<?> getReturnType(Method method) {
        Preconditions.checkArgument(method.getDeclaringClass().isAssignableFrom(this.rawType), "%s is not defined by a supertype of %s", method, this.type);
        return resolve(method.getGenericReturnType());
    }
}
