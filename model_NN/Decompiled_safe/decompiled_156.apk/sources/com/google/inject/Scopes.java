package com.google.inject;

import com.google.inject.internal.Errors;
import com.google.inject.internal.InternalFactory;
import com.google.inject.internal.Scoping;
import java.lang.annotation.Annotation;

public class Scopes {
    public static final Scope NO_SCOPE = new Scope() {
        public <T> Provider<T> scope(Key<T> key, Provider<T> unscoped) {
            return unscoped;
        }

        public String toString() {
            return "Scopes.NO_SCOPE";
        }
    };
    public static final Scope SINGLETON = new Scope() {
        public <T> Provider<T> scope(Key<T> key, final Provider<T> creator) {
            return new Provider<T>() {
                private volatile T instance;

                public T get() {
                    if (this.instance == null) {
                        synchronized (InjectorImpl.class) {
                            if (this.instance == null) {
                                this.instance = creator.get();
                            }
                        }
                    }
                    return this.instance;
                }

                public String toString() {
                    return String.format("%s[%s]", creator, Scopes.SINGLETON);
                }
            };
        }

        public String toString() {
            return "Scopes.SINGLETON";
        }
    };

    private Scopes() {
    }

    static <T> InternalFactory<? extends T> scope(Key<T> key, InjectorImpl injector, InternalFactory<? extends T> creator, Scoping scoping) {
        if (scoping.isNoScope()) {
            return creator;
        }
        return new InternalFactoryToProviderAdapter(Initializables.of(scoping.getScopeInstance().scope(key, new ProviderToInternalFactoryAdapter(injector, creator))));
    }

    static Scoping makeInjectable(Scoping scoping, InjectorImpl injector, Errors errors) {
        Class<? extends Annotation> scopeAnnotation = scoping.getScopeAnnotation();
        if (scopeAnnotation == null) {
            return scoping;
        }
        Scope scope = injector.state.getScope(scopeAnnotation);
        if (scope != null) {
            return Scoping.forInstance(scope);
        }
        errors.scopeNotFound(scopeAnnotation);
        return Scoping.UNSCOPED;
    }
}
