package com.google.inject;

import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.FailableCache;
import com.google.inject.internal.ImmutableList;
import com.google.inject.internal.Lists;
import com.google.inject.spi.InjectionPoint;
import com.google.inject.spi.TypeListenerBinding;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

class MembersInjectorStore {
    private final FailableCache<TypeLiteral<?>, MembersInjectorImpl<?>> cache = new FailableCache<TypeLiteral<?>, MembersInjectorImpl<?>>() {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object create(Object x0, Errors x1) throws ErrorsException {
            return create((TypeLiteral<?>) ((TypeLiteral) x0), x1);
        }

        /* access modifiers changed from: protected */
        public MembersInjectorImpl<?> create(TypeLiteral<?> type, Errors errors) throws ErrorsException {
            return MembersInjectorStore.this.createWithListeners(type, errors);
        }
    };
    private final InjectorImpl injector;
    private final ImmutableList<TypeListenerBinding> typeListenerBindings;

    MembersInjectorStore(InjectorImpl injector2, List<TypeListenerBinding> typeListenerBindings2) {
        this.injector = injector2;
        this.typeListenerBindings = ImmutableList.copyOf(typeListenerBindings2);
    }

    public boolean hasTypeListeners() {
        return !this.typeListenerBindings.isEmpty();
    }

    public <T> MembersInjectorImpl<T> get(TypeLiteral<T> key, Errors errors) throws ErrorsException {
        return this.cache.get(key, errors);
    }

    /* access modifiers changed from: private */
    public <T> MembersInjectorImpl<T> createWithListeners(TypeLiteral<T> type, Errors errors) throws ErrorsException {
        Set<InjectionPoint> injectionPoints;
        int numErrorsBefore = errors.size();
        try {
            injectionPoints = InjectionPoint.forInstanceMethodsAndFields((TypeLiteral<?>) type);
        } catch (ConfigurationException e) {
            errors.merge(e.getErrorMessages());
            injectionPoints = (Set) e.getPartialValue();
        }
        ImmutableList<SingleMemberInjector> injectors = getInjectors(injectionPoints, errors);
        errors.throwIfNewErrors(numErrorsBefore);
        EncounterImpl<T> encounter = new EncounterImpl<>(errors, this.injector.lookups);
        Iterator i$ = this.typeListenerBindings.iterator();
        while (i$.hasNext()) {
            TypeListenerBinding typeListener = (TypeListenerBinding) i$.next();
            if (typeListener.getTypeMatcher().matches(type)) {
                try {
                    typeListener.getListener().hear(type, encounter);
                } catch (RuntimeException e2) {
                    errors.errorNotifyingTypeListener(typeListener, type, e2);
                }
            }
        }
        encounter.invalidate();
        errors.throwIfNewErrors(numErrorsBefore);
        return new MembersInjectorImpl<>(this.injector, type, encounter, injectors);
    }

    /* access modifiers changed from: package-private */
    public ImmutableList<SingleMemberInjector> getInjectors(Set<InjectionPoint> injectionPoints, Errors errors) {
        List<SingleMemberInjector> injectors = Lists.newArrayList();
        for (InjectionPoint injectionPoint : injectionPoints) {
            try {
                Errors errorsForMember = injectionPoint.isOptional() ? new Errors(injectionPoint) : errors.withSource(injectionPoint);
                injectors.add(injectionPoint.getMember() instanceof Field ? new SingleFieldInjector(this.injector, injectionPoint, errorsForMember) : new SingleMethodInjector(this.injector, injectionPoint, errorsForMember));
            } catch (ErrorsException e) {
            }
        }
        return ImmutableList.copyOf(injectors);
    }
}
