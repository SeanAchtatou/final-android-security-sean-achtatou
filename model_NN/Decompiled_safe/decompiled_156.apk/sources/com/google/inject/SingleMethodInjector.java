package com.google.inject;

import com.google.inject.InjectorImpl;
import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.InternalContext;
import com.google.inject.spi.InjectionPoint;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

class SingleMethodInjector implements SingleMemberInjector {
    final InjectionPoint injectionPoint;
    final InjectorImpl.MethodInvoker methodInvoker;
    final SingleParameterInjector<?>[] parameterInjectors;

    public SingleMethodInjector(InjectorImpl injector, InjectionPoint injectionPoint2, Errors errors) throws ErrorsException {
        this.injectionPoint = injectionPoint2;
        this.methodInvoker = createMethodInvoker((Method) injectionPoint2.getMember());
        this.parameterInjectors = injector.getParametersInjectors(injectionPoint2.getDependencies(), errors);
    }

    private InjectorImpl.MethodInvoker createMethodInvoker(final Method method) {
        int modifiers = method.getModifiers();
        if (Modifier.isPrivate(modifiers) || !Modifier.isProtected(modifiers)) {
        }
        if (!Modifier.isPublic(modifiers)) {
            method.setAccessible(true);
        }
        return new InjectorImpl.MethodInvoker() {
            public Object invoke(Object target, Object... parameters) throws IllegalAccessException, InvocationTargetException {
                return method.invoke(target, parameters);
            }
        };
    }

    public InjectionPoint getInjectionPoint() {
        return this.injectionPoint;
    }

    public void inject(Errors errors, InternalContext context, Object o) {
        Throwable cause;
        try {
            try {
                this.methodInvoker.invoke(o, SingleParameterInjector.getAll(errors, context, this.parameterInjectors));
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            } catch (InvocationTargetException e2) {
                InvocationTargetException userException = e2;
                if (userException.getCause() != null) {
                    cause = userException.getCause();
                } else {
                    cause = userException;
                }
                errors.withSource(this.injectionPoint).errorInjectingMethod(cause);
            }
        } catch (ErrorsException e3) {
            errors.merge(e3.getErrors());
        }
    }
}
