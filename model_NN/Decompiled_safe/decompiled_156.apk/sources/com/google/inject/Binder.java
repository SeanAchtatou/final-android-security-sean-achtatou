package com.google.inject;

import com.google.inject.binder.AnnotatedBindingBuilder;
import com.google.inject.binder.AnnotatedConstantBindingBuilder;
import com.google.inject.binder.LinkedBindingBuilder;
import com.google.inject.matcher.Matcher;
import com.google.inject.spi.Message;
import com.google.inject.spi.TypeConverter;
import com.google.inject.spi.TypeListener;
import java.lang.annotation.Annotation;

public interface Binder {
    void addError(Message message);

    void addError(String str, Object... objArr);

    void addError(Throwable th);

    <T> AnnotatedBindingBuilder<T> bind(TypeLiteral typeLiteral);

    <T> AnnotatedBindingBuilder<T> bind(Class cls);

    <T> LinkedBindingBuilder<T> bind(Key key);

    AnnotatedConstantBindingBuilder bindConstant();

    void bindListener(Matcher<? super TypeLiteral<?>> matcher, TypeListener typeListener);

    void bindScope(Class<? extends Annotation> cls, Scope scope);

    void convertToTypes(Matcher<? super TypeLiteral<?>> matcher, TypeConverter typeConverter);

    Stage currentStage();

    <T> MembersInjector<T> getMembersInjector(TypeLiteral typeLiteral);

    <T> MembersInjector<T> getMembersInjector(Class cls);

    <T> Provider<T> getProvider(Key key);

    <T> Provider<T> getProvider(Class cls);

    void install(Module module);

    PrivateBinder newPrivateBinder();

    <T> void requestInjection(TypeLiteral<T> typeLiteral, T t);

    void requestInjection(Object obj);

    void requestStaticInjection(Class<?>... clsArr);

    Binder skipSources(Class... clsArr);

    Binder withSource(Object obj);
}
