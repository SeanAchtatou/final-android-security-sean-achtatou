package com.google.inject;

import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.FailableCache;
import com.google.inject.spi.InjectionPoint;

class ConstructorInjectorStore {
    private final FailableCache<TypeLiteral<?>, ConstructorInjector<?>> cache = new FailableCache<TypeLiteral<?>, ConstructorInjector<?>>() {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object create(Object x0, Errors x1) throws ErrorsException {
            return create((TypeLiteral<?>) ((TypeLiteral) x0), x1);
        }

        /* access modifiers changed from: protected */
        public ConstructorInjector<?> create(TypeLiteral<?> type, Errors errors) throws ErrorsException {
            return ConstructorInjectorStore.this.createConstructor(type, errors);
        }
    };
    private final InjectorImpl injector;

    ConstructorInjectorStore(InjectorImpl injector2) {
        this.injector = injector2;
    }

    public <T> ConstructorInjector<T> get(TypeLiteral<T> key, Errors errors) throws ErrorsException {
        return this.cache.get(key, errors);
    }

    /* access modifiers changed from: private */
    public <T> ConstructorInjector<T> createConstructor(TypeLiteral<T> type, Errors errors) throws ErrorsException {
        int numErrorsBefore = errors.size();
        try {
            InjectionPoint injectionPoint = InjectionPoint.forConstructorOf((TypeLiteral<?>) type);
            SingleParameterInjector<?>[] constructorParameterInjectors = this.injector.getParametersInjectors(injectionPoint.getDependencies(), errors);
            MembersInjectorImpl<T> membersInjector = this.injector.membersInjectorStore.get(type, errors);
            ConstructionProxyFactory<T> factory = new DefaultConstructionProxyFactory<>(injectionPoint);
            errors.throwIfNewErrors(numErrorsBefore);
            return new ConstructorInjector<>(membersInjector.getInjectionPoints(), factory.create(), constructorParameterInjectors, membersInjector);
        } catch (ConfigurationException e) {
            errors.merge(e.getErrorMessages());
            throw errors.toException();
        }
    }
}
