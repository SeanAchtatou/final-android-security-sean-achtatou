package com.google.inject;

import com.google.inject.internal.Annotations;
import com.google.inject.internal.Errors;
import com.google.inject.internal.Preconditions;
import com.google.inject.spi.ScopeBinding;
import java.lang.annotation.Annotation;

class ScopeBindingProcessor extends AbstractProcessor {
    ScopeBindingProcessor(Errors errors) {
        super(errors);
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public Boolean visit(ScopeBinding command) {
        Scope scope = command.getScope();
        Class<? extends Annotation> annotationType = command.getAnnotationType();
        if (!Annotations.isScopeAnnotation(annotationType)) {
            this.errors.withSource(annotationType).missingScopeAnnotation();
        }
        if (!Annotations.isRetainedAtRuntime(annotationType)) {
            this.errors.withSource(annotationType).missingRuntimeRetention(command.getSource());
        }
        Scope existing = this.injector.state.getScope((Class) Preconditions.checkNotNull(annotationType, "annotation type"));
        if (existing != null) {
            this.errors.duplicateScopes(existing, annotationType, scope);
        } else {
            this.injector.state.putAnnotation(annotationType, (Scope) Preconditions.checkNotNull(scope, "scope"));
        }
        return true;
    }
}
