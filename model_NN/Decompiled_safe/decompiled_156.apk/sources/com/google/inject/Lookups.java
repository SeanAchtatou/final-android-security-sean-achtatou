package com.google.inject;

interface Lookups {
    <T> MembersInjector<T> getMembersInjector(TypeLiteral typeLiteral);

    <T> Provider<T> getProvider(Key key);
}
