package com.google.inject;

import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.ImmutableSet;
import com.google.inject.internal.InternalContext;
import com.google.inject.internal.InternalFactory;
import com.google.inject.internal.Lists;
import com.google.inject.internal.Preconditions;
import com.google.inject.internal.PrivateElementsImpl;
import com.google.inject.internal.ProviderInstanceBindingImpl;
import com.google.inject.internal.Scoping;
import com.google.inject.internal.SourceProvider;
import com.google.inject.internal.Stopwatch;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.Element;
import com.google.inject.spi.Elements;
import com.google.inject.spi.InjectionPoint;
import com.google.inject.spi.PrivateElements;
import java.util.List;
import java.util.logging.Logger;

class InjectorShell {
    private final List<Element> elements;
    private final InjectorImpl injector;
    private final PrivateElements privateElements;

    private InjectorShell(Builder builder, List<Element> elements2, InjectorImpl injector2) {
        this.privateElements = builder.privateElements;
        this.elements = elements2;
        this.injector = injector2;
    }

    /* access modifiers changed from: package-private */
    public PrivateElements getPrivateElements() {
        return this.privateElements;
    }

    /* access modifiers changed from: package-private */
    public InjectorImpl getInjector() {
        return this.injector;
    }

    /* access modifiers changed from: package-private */
    public List<Element> getElements() {
        return this.elements;
    }

    static class Builder {
        private final List<Element> elements = Lists.newArrayList();
        private final List<Module> modules = Lists.newArrayList();
        private InjectorImpl parent;
        /* access modifiers changed from: private */
        public PrivateElementsImpl privateElements;
        private Stage stage;
        private State state;

        Builder() {
        }

        /* access modifiers changed from: package-private */
        public Builder parent(InjectorImpl parent2) {
            this.parent = parent2;
            this.state = new InheritingState(parent2.state);
            return this;
        }

        /* access modifiers changed from: package-private */
        public Builder stage(Stage stage2) {
            this.stage = stage2;
            return this;
        }

        /* access modifiers changed from: package-private */
        public Builder privateElements(PrivateElements privateElements2) {
            this.privateElements = (PrivateElementsImpl) privateElements2;
            this.elements.addAll(privateElements2.getElements());
            return this;
        }

        /* access modifiers changed from: package-private */
        public void addModules(Iterable<? extends Module> modules2) {
            for (Module module : modules2) {
                this.modules.add(module);
            }
        }

        /* access modifiers changed from: package-private */
        public Object lock() {
            return getState().lock();
        }

        /* access modifiers changed from: package-private */
        public List<InjectorShell> build(Initializer initializer, BindingProcessor bindingProcessor, Stopwatch stopwatch, Errors errors) {
            boolean z;
            boolean z2;
            boolean z3;
            if (this.stage != null) {
                z = true;
            } else {
                z = false;
            }
            Preconditions.checkState(z, "Stage not initialized");
            if (this.privateElements == null || this.parent != null) {
                z2 = true;
            } else {
                z2 = false;
            }
            Preconditions.checkState(z2, "PrivateElements with no parent");
            if (this.state != null) {
                z3 = true;
            } else {
                z3 = false;
            }
            Preconditions.checkState(z3, "no state. Did you remember to lock() ?");
            InjectorImpl injector = new InjectorImpl(this.parent, this.state, initializer);
            if (this.privateElements != null) {
                this.privateElements.initInjector(injector);
            }
            if (this.parent == null) {
                this.modules.add(0, new RootModule(this.stage));
                new TypeConverterBindingProcessor(errors).prepareBuiltInConverters(injector);
            }
            this.elements.addAll(Elements.getElements(this.stage, this.modules));
            stopwatch.resetAndLog("Module execution");
            new MessageProcessor(errors).process(injector, this.elements);
            new TypeListenerBindingProcessor(errors).process(injector, this.elements);
            injector.membersInjectorStore = new MembersInjectorStore(injector, injector.state.getTypeListenerBindings());
            stopwatch.resetAndLog("TypeListeners creation");
            new ScopeBindingProcessor(errors).process(injector, this.elements);
            stopwatch.resetAndLog("Scopes creation");
            new TypeConverterBindingProcessor(errors).process(injector, this.elements);
            stopwatch.resetAndLog("Converters creation");
            InjectorShell.bindInjector(injector);
            InjectorShell.bindLogger(injector);
            bindingProcessor.process(injector, this.elements);
            stopwatch.resetAndLog("Binding creation");
            List<InjectorShell> injectorShells = Lists.newArrayList();
            injectorShells.add(new InjectorShell(this, this.elements, injector));
            PrivateElementProcessor processor = new PrivateElementProcessor(errors, this.stage);
            processor.process(injector, this.elements);
            for (Builder builder : processor.getInjectorShellBuilders()) {
                injectorShells.addAll(builder.build(initializer, bindingProcessor, stopwatch, errors));
            }
            stopwatch.resetAndLog("Private environment creation");
            return injectorShells;
        }

        private State getState() {
            if (this.state == null) {
                this.state = new InheritingState(State.NONE);
            }
            return this.state;
        }
    }

    /* access modifiers changed from: private */
    public static void bindInjector(InjectorImpl injector2) {
        Key<Injector> key = Key.get(Injector.class);
        InjectorFactory injectorFactory = new InjectorFactory(injector2);
        injector2.state.putBinding(key, new ProviderInstanceBindingImpl(injector2, key, SourceProvider.UNKNOWN_SOURCE, injectorFactory, Scoping.UNSCOPED, injectorFactory, ImmutableSet.of()));
    }

    private static class InjectorFactory implements InternalFactory<Injector>, Provider<Injector> {
        private final Injector injector;

        private InjectorFactory(Injector injector2) {
            this.injector = injector2;
        }

        public Injector get(Errors errors, InternalContext context, Dependency<?> dependency) throws ErrorsException {
            return this.injector;
        }

        public Injector get() {
            return this.injector;
        }

        public String toString() {
            return "Provider<Injector>";
        }
    }

    /* access modifiers changed from: private */
    public static void bindLogger(InjectorImpl injector2) {
        Key<Logger> key = Key.get(Logger.class);
        LoggerFactory loggerFactory = new LoggerFactory();
        injector2.state.putBinding(key, new ProviderInstanceBindingImpl(injector2, key, SourceProvider.UNKNOWN_SOURCE, loggerFactory, Scoping.UNSCOPED, loggerFactory, ImmutableSet.of()));
    }

    private static class LoggerFactory implements InternalFactory<Logger>, Provider<Logger> {
        private LoggerFactory() {
        }

        public Logger get(Errors errors, InternalContext context, Dependency<?> dependency) {
            InjectionPoint injectionPoint = dependency.getInjectionPoint();
            return injectionPoint == null ? Logger.getAnonymousLogger() : Logger.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
        }

        public Logger get() {
            return Logger.getAnonymousLogger();
        }

        public String toString() {
            return "Provider<Logger>";
        }
    }

    private static class RootModule implements Module {
        final Stage stage;

        private RootModule(Stage stage2) {
            this.stage = (Stage) Preconditions.checkNotNull(stage2, "stage");
        }

        public void configure(Binder binder) {
            Binder binder2 = binder.withSource(SourceProvider.UNKNOWN_SOURCE);
            binder2.bind(Stage.class).toInstance(this.stage);
            binder2.bindScope(Singleton.class, Scopes.SINGLETON);
        }
    }
}
