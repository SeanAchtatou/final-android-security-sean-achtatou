package com.google.inject;

import com.google.inject.internal.BindingImpl;
import com.google.inject.internal.Errors;
import com.google.inject.internal.Lists;
import com.google.inject.internal.Maps;
import com.google.inject.internal.MatcherAndConverter;
import com.google.inject.internal.Preconditions;
import com.google.inject.spi.TypeListenerBinding;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

class InheritingState implements State {
    private final WeakKeySet blacklistedKeys = new WeakKeySet();
    private final List<MatcherAndConverter> converters = Lists.newArrayList();
    private final Map<Key<?>, Binding<?>> explicitBindings = Collections.unmodifiableMap(this.explicitBindingsMutable);
    private final Map<Key<?>, Binding<?>> explicitBindingsMutable = Maps.newLinkedHashMap();
    private final List<TypeListenerBinding> listenerBindings = Lists.newArrayList();
    private final Object lock;
    private final State parent;
    private final Map<Class<? extends Annotation>, Scope> scopes = Maps.newHashMap();

    InheritingState(State parent2) {
        this.parent = (State) Preconditions.checkNotNull(parent2, "parent");
        this.lock = parent2 == State.NONE ? this : parent2.lock();
    }

    public State parent() {
        return this.parent;
    }

    public <T> BindingImpl<T> getExplicitBinding(Key<T> key) {
        Binding<?> binding = this.explicitBindings.get(key);
        return binding != null ? (BindingImpl) binding : this.parent.getExplicitBinding(key);
    }

    public Map<Key<?>, Binding<?>> getExplicitBindingsThisLevel() {
        return this.explicitBindings;
    }

    public void putBinding(Key<?> key, BindingImpl<?> binding) {
        this.explicitBindingsMutable.put(key, binding);
    }

    public Scope getScope(Class<? extends Annotation> annotationType) {
        Scope scope = this.scopes.get(annotationType);
        return scope != null ? scope : this.parent.getScope(annotationType);
    }

    public void putAnnotation(Class<? extends Annotation> annotationType, Scope scope) {
        this.scopes.put(annotationType, scope);
    }

    public Iterable<MatcherAndConverter> getConvertersThisLevel() {
        return this.converters;
    }

    public void addConverter(MatcherAndConverter matcherAndConverter) {
        this.converters.add(matcherAndConverter);
    }

    public MatcherAndConverter getConverter(String stringValue, TypeLiteral<?> type, Errors errors, Object source) {
        MatcherAndConverter matchingConverter = null;
        for (State s = this; s != State.NONE; s = s.parent()) {
            for (MatcherAndConverter converter : s.getConvertersThisLevel()) {
                if (converter.getTypeMatcher().matches(type)) {
                    if (matchingConverter != null) {
                        errors.ambiguousTypeConversion(stringValue, source, type, matchingConverter, converter);
                    }
                    matchingConverter = converter;
                }
            }
        }
        return matchingConverter;
    }

    public void addTypeListener(TypeListenerBinding listenerBinding) {
        this.listenerBindings.add(listenerBinding);
    }

    public List<TypeListenerBinding> getTypeListenerBindings() {
        List<TypeListenerBinding> parentBindings = this.parent.getTypeListenerBindings();
        List<TypeListenerBinding> result = new ArrayList<>(parentBindings.size() + 1);
        result.addAll(parentBindings);
        result.addAll(this.listenerBindings);
        return result;
    }

    public void blacklist(Key<?> key) {
        this.parent.blacklist(key);
        this.blacklistedKeys.add(key);
    }

    public boolean isBlacklisted(Key<?> key) {
        return this.blacklistedKeys.contains(key);
    }

    public Object lock() {
        return this.lock;
    }
}
