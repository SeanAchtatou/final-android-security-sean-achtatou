package com.google.inject;

import com.google.inject.internal.Errors;
import com.google.inject.internal.ImmutableSet;
import com.google.inject.internal.Preconditions;
import com.google.inject.spi.Message;
import java.util.Collection;

public final class ConfigurationException extends RuntimeException {
    private static final long serialVersionUID = 0;
    private final ImmutableSet<Message> messages;
    private Object partialValue = null;

    public ConfigurationException(Iterable<Message> messages2) {
        this.messages = ImmutableSet.copyOf(messages2);
        initCause(Errors.getOnlyCause(this.messages));
    }

    public ConfigurationException withPartialValue(Object partialValue2) {
        boolean z;
        if (this.partialValue == null) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkState(z, "Can't clobber existing partial value %s with %s", this.partialValue, partialValue2);
        ConfigurationException result = new ConfigurationException(this.messages);
        result.partialValue = partialValue2;
        return result;
    }

    public Collection<Message> getErrorMessages() {
        return this.messages;
    }

    public <E> E getPartialValue() {
        return this.partialValue;
    }

    public String getMessage() {
        return Errors.format("Guice configuration errors", this.messages);
    }
}
