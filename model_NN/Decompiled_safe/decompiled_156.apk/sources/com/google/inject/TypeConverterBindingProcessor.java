package com.google.inject;

import com.google.inject.internal.Errors;
import com.google.inject.internal.MatcherAndConverter;
import com.google.inject.internal.SourceProvider;
import com.google.inject.internal.Strings;
import com.google.inject.matcher.AbstractMatcher;
import com.google.inject.matcher.Matcher;
import com.google.inject.matcher.Matchers;
import com.google.inject.spi.TypeConverter;
import com.google.inject.spi.TypeConverterBinding;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

class TypeConverterBindingProcessor extends AbstractProcessor {
    TypeConverterBindingProcessor(Errors errors) {
        super(errors);
    }

    public void prepareBuiltInConverters(InjectorImpl injector) {
        this.injector = injector;
        try {
            convertToPrimitiveType(Integer.TYPE, Integer.class);
            convertToPrimitiveType(Long.TYPE, Long.class);
            convertToPrimitiveType(Boolean.TYPE, Boolean.class);
            convertToPrimitiveType(Byte.TYPE, Byte.class);
            convertToPrimitiveType(Short.TYPE, Short.class);
            convertToPrimitiveType(Float.TYPE, Float.class);
            convertToPrimitiveType(Double.TYPE, Double.class);
            convertToClass(Character.class, new TypeConverter() {
                public Object convert(String value, TypeLiteral<?> typeLiteral) {
                    String value2 = value.trim();
                    if (value2.length() == 1) {
                        return Character.valueOf(value2.charAt(0));
                    }
                    throw new RuntimeException("Length != 1.");
                }

                public String toString() {
                    return "TypeConverter<Character>";
                }
            });
            convertToClasses(Matchers.subclassesOf(Enum.class), new TypeConverter() {
                public Object convert(String value, TypeLiteral<?> toType) {
                    return Enum.valueOf(toType.getRawType(), value);
                }

                public String toString() {
                    return "TypeConverter<E extends Enum<E>>";
                }
            });
            internalConvertToTypes(new AbstractMatcher<TypeLiteral<?>>() {
                public /* bridge */ /* synthetic */ boolean matches(Object x0) {
                    return matches((TypeLiteral<?>) ((TypeLiteral) x0));
                }

                public boolean matches(TypeLiteral<?> typeLiteral) {
                    return typeLiteral.getRawType() == Class.class;
                }

                public String toString() {
                    return "Class<?>";
                }
            }, new TypeConverter() {
                public Object convert(String value, TypeLiteral<?> typeLiteral) {
                    try {
                        return Class.forName(value);
                    } catch (ClassNotFoundException e) {
                        throw new RuntimeException(e.getMessage());
                    }
                }

                public String toString() {
                    return "TypeConverter<Class<?>>";
                }
            });
        } finally {
            this.injector = null;
        }
    }

    private <T> void convertToPrimitiveType(Class<T> primitiveType, final Class<T> wrapperType) {
        try {
            final Method parser = wrapperType.getMethod("parse" + Strings.capitalize(primitiveType.getName()), String.class);
            convertToClass(wrapperType, new TypeConverter() {
                public Object convert(String value, TypeLiteral<?> typeLiteral) {
                    try {
                        return parser.invoke(null, value);
                    } catch (IllegalAccessException e) {
                        throw new AssertionError(e);
                    } catch (InvocationTargetException e2) {
                        throw new RuntimeException(e2.getTargetException().getMessage());
                    }
                }

                public String toString() {
                    return "TypeConverter<" + wrapperType.getSimpleName() + ">";
                }
            });
        } catch (NoSuchMethodException e) {
            throw new AssertionError(e);
        }
    }

    private <T> void convertToClass(Class<T> type, TypeConverter converter) {
        convertToClasses(Matchers.identicalTo(type), converter);
    }

    private void convertToClasses(final Matcher<? super Class<?>> typeMatcher, TypeConverter converter) {
        internalConvertToTypes(new AbstractMatcher<TypeLiteral<?>>() {
            public /* bridge */ /* synthetic */ boolean matches(Object x0) {
                return matches((TypeLiteral<?>) ((TypeLiteral) x0));
            }

            public boolean matches(TypeLiteral<?> typeLiteral) {
                Type type = typeLiteral.getType();
                if (!(type instanceof Class)) {
                    return false;
                }
                return typeMatcher.matches((Class) type);
            }

            public String toString() {
                return typeMatcher.toString();
            }
        }, converter);
    }

    private void internalConvertToTypes(Matcher<? super TypeLiteral<?>> typeMatcher, TypeConverter converter) {
        this.injector.state.addConverter(new MatcherAndConverter(typeMatcher, converter, SourceProvider.UNKNOWN_SOURCE));
    }

    public Boolean visit(TypeConverterBinding command) {
        this.injector.state.addConverter(new MatcherAndConverter(command.getTypeMatcher(), command.getTypeConverter(), command.getSource()));
        return true;
    }
}
