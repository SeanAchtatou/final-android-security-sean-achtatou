package com.google.inject;

import com.google.inject.InjectorShell;
import com.google.inject.internal.BindingImpl;
import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.ImmutableSet;
import com.google.inject.internal.InternalContext;
import com.google.inject.internal.Iterables;
import com.google.inject.internal.Stopwatch;
import com.google.inject.spi.Dependency;
import java.util.List;
import java.util.Map;

class InjectorBuilder {
    private final BindingProcessor bindingProcesor = new BindingProcessor(this.errors, this.initializer);
    private final Errors errors = new Errors();
    private final Initializer initializer = new Initializer();
    private final InjectionRequestProcessor injectionRequestProcessor = new InjectionRequestProcessor(this.errors, this.initializer);
    private final InjectorShell.Builder shellBuilder = new InjectorShell.Builder();
    private List<InjectorShell> shells;
    private Stage stage;
    private final Stopwatch stopwatch = new Stopwatch();

    InjectorBuilder() {
    }

    /* access modifiers changed from: package-private */
    public InjectorBuilder stage(Stage stage2) {
        this.shellBuilder.stage(stage2);
        this.stage = stage2;
        return this;
    }

    /* access modifiers changed from: package-private */
    public InjectorBuilder parentInjector(InjectorImpl parent) {
        this.shellBuilder.parent(parent);
        return stage((Stage) parent.getInstance(Stage.class));
    }

    /* access modifiers changed from: package-private */
    public InjectorBuilder addModules(Iterable<? extends Module> modules) {
        this.shellBuilder.addModules(modules);
        return this;
    }

    /* access modifiers changed from: package-private */
    public Injector build() {
        if (this.shellBuilder == null) {
            throw new AssertionError("Already built, builders are not reusable.");
        }
        synchronized (this.shellBuilder.lock()) {
            this.shells = this.shellBuilder.build(this.initializer, this.bindingProcesor, this.stopwatch, this.errors);
            this.stopwatch.resetAndLog("Injector construction");
            initializeStatically();
        }
        if (this.stage == Stage.TOOL) {
            return new ToolStageInjector(primaryInjector());
        }
        injectDynamically();
        return primaryInjector();
    }

    private void initializeStatically() {
        this.bindingProcesor.initializeBindings();
        this.stopwatch.resetAndLog("Binding initialization");
        for (InjectorShell shell : this.shells) {
            shell.getInjector().index();
        }
        this.stopwatch.resetAndLog("Binding indexing");
        this.injectionRequestProcessor.process(this.shells);
        this.stopwatch.resetAndLog("Collecting injection requests");
        this.bindingProcesor.runCreationListeners();
        this.stopwatch.resetAndLog("Binding validation");
        this.injectionRequestProcessor.validate();
        this.stopwatch.resetAndLog("Static validation");
        this.initializer.validateOustandingInjections(this.errors);
        this.stopwatch.resetAndLog("Instance member validation");
        new LookupProcessor(this.errors).process(this.shells);
        for (InjectorShell shell2 : this.shells) {
            ((DeferredLookups) shell2.getInjector().lookups).initialize(this.errors);
        }
        this.stopwatch.resetAndLog("Provider verification");
        for (InjectorShell shell3 : this.shells) {
            if (!shell3.getElements().isEmpty()) {
                throw new AssertionError("Failed to execute " + shell3.getElements());
            }
        }
        this.errors.throwCreationExceptionIfErrorsExist();
    }

    private Injector primaryInjector() {
        return this.shells.get(0).getInjector();
    }

    private void injectDynamically() {
        this.injectionRequestProcessor.injectMembers();
        this.stopwatch.resetAndLog("Static member injection");
        this.initializer.injectAll(this.errors);
        this.stopwatch.resetAndLog("Instance injection");
        this.errors.throwCreationExceptionIfErrorsExist();
        for (InjectorShell shell : this.shells) {
            loadEagerSingletons(shell.getInjector(), this.stage, this.errors);
        }
        this.stopwatch.resetAndLog("Preloading singletons");
        this.errors.throwCreationExceptionIfErrorsExist();
    }

    public void loadEagerSingletons(InjectorImpl injector, Stage stage2, final Errors errors2) {
        for (final BindingImpl<?> binding : ImmutableSet.copyOf(Iterables.concat(injector.state.getExplicitBindingsThisLevel().values(), injector.jitBindings.values()))) {
            if (binding.getScoping().isEagerSingleton(stage2)) {
                try {
                    injector.callInContext(new ContextualCallable<Void>() {
                        Dependency<?> dependency = Dependency.get(binding.getKey());

                        public Void call(InternalContext context) {
                            context.setDependency(this.dependency);
                            Errors errorsForBinding = errors2.withSource(this.dependency);
                            try {
                                binding.getInternalFactory().get(errorsForBinding, context, this.dependency);
                            } catch (ErrorsException e) {
                                errorsForBinding.merge(e.getErrors());
                            } finally {
                                context.setDependency(null);
                            }
                            return null;
                        }
                    });
                } catch (ErrorsException e) {
                    throw new AssertionError();
                }
            }
        }
    }

    static class ToolStageInjector implements Injector {
        private final Injector delegateInjector;

        ToolStageInjector(Injector delegateInjector2) {
            this.delegateInjector = delegateInjector2;
        }

        public void injectMembers(Object o) {
            throw new UnsupportedOperationException("Injector.injectMembers(Object) is not supported in Stage.TOOL");
        }

        public Map<Key<?>, Binding<?>> getBindings() {
            return this.delegateInjector.getBindings();
        }

        public <T> Binding<T> getBinding(Key<T> key) {
            return this.delegateInjector.getBinding(key);
        }

        public <T> Binding<T> getBinding(Class<T> type) {
            return this.delegateInjector.getBinding(type);
        }

        public <T> List<Binding<T>> findBindingsByType(TypeLiteral<T> type) {
            return this.delegateInjector.findBindingsByType(type);
        }

        public Injector getParent() {
            return this.delegateInjector.getParent();
        }

        public Injector createChildInjector(Iterable<? extends Module> modules) {
            return this.delegateInjector.createChildInjector(modules);
        }

        public Injector createChildInjector(Module... modules) {
            return this.delegateInjector.createChildInjector(modules);
        }

        public <T> Provider<T> getProvider(Key<T> key) {
            throw new UnsupportedOperationException("Injector.getProvider(Key<T>) is not supported in Stage.TOOL");
        }

        public <T> Provider<T> getProvider(Class<T> cls) {
            throw new UnsupportedOperationException("Injector.getProvider(Class<T>) is not supported in Stage.TOOL");
        }

        public <T> MembersInjector<T> getMembersInjector(TypeLiteral<T> typeLiteral) {
            throw new UnsupportedOperationException("Injector.getMembersInjector(TypeLiteral<T>) is not supported in Stage.TOOL");
        }

        public <T> MembersInjector<T> getMembersInjector(Class<T> cls) {
            throw new UnsupportedOperationException("Injector.getMembersInjector(Class<T>) is not supported in Stage.TOOL");
        }

        public <T> T getInstance(Key<T> key) {
            throw new UnsupportedOperationException("Injector.getInstance(Key<T>) is not supported in Stage.TOOL");
        }

        public <T> T getInstance(Class<T> cls) {
            throw new UnsupportedOperationException("Injector.getInstance(Class<T>) is not supported in Stage.TOOL");
        }
    }
}
