package com.google.inject.binder;

import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.TypeLiteral;

public interface LinkedBindingBuilder<T> extends ScopedBindingBuilder {
    ScopedBindingBuilder to(Key<? extends T> key);

    ScopedBindingBuilder to(TypeLiteral<? extends T> typeLiteral);

    ScopedBindingBuilder to(Class<? extends T> cls);

    void toInstance(T t);

    ScopedBindingBuilder toProvider(Key<? extends Provider<? extends T>> key);

    ScopedBindingBuilder toProvider(Provider<? extends T> provider);

    ScopedBindingBuilder toProvider(Class<? extends Provider<? extends T>> cls);
}
