package com.google.inject;

import com.google.inject.binder.AnnotatedBindingBuilder;
import com.google.inject.binder.AnnotatedConstantBindingBuilder;
import com.google.inject.binder.AnnotatedElementBuilder;
import com.google.inject.binder.LinkedBindingBuilder;
import com.google.inject.internal.Preconditions;
import com.google.inject.matcher.Matcher;
import com.google.inject.spi.Message;
import com.google.inject.spi.TypeConverter;
import com.google.inject.spi.TypeListener;
import java.lang.annotation.Annotation;

public abstract class PrivateModule implements Module {
    private PrivateBinder binder;

    /* access modifiers changed from: protected */
    public abstract void configure();

    public final synchronized void configure(Binder binder2) {
        Preconditions.checkState(this.binder == null, "Re-entry is not allowed.");
        this.binder = (PrivateBinder) binder2.skipSources(PrivateModule.class);
        try {
            configure();
            this.binder = null;
        } catch (Throwable th) {
            this.binder = null;
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public final <T> void expose(Key<T> key) {
        this.binder.expose((Key<?>) key);
    }

    /* access modifiers changed from: protected */
    public final AnnotatedElementBuilder expose(Class<?> type) {
        return this.binder.expose(type);
    }

    /* access modifiers changed from: protected */
    public final AnnotatedElementBuilder expose(TypeLiteral<?> type) {
        return this.binder.expose(type);
    }

    /* access modifiers changed from: protected */
    public final PrivateBinder binder() {
        return this.binder;
    }

    /* access modifiers changed from: protected */
    public final void bindScope(Class<? extends Annotation> scopeAnnotation, Scope scope) {
        this.binder.bindScope(scopeAnnotation, scope);
    }

    /* access modifiers changed from: protected */
    public final <T> LinkedBindingBuilder<T> bind(Key<T> key) {
        return this.binder.bind(key);
    }

    /* access modifiers changed from: protected */
    public final <T> AnnotatedBindingBuilder<T> bind(TypeLiteral<T> typeLiteral) {
        return this.binder.bind(typeLiteral);
    }

    /* access modifiers changed from: protected */
    public final <T> AnnotatedBindingBuilder<T> bind(Class<T> clazz) {
        return this.binder.bind(clazz);
    }

    /* access modifiers changed from: protected */
    public final AnnotatedConstantBindingBuilder bindConstant() {
        return this.binder.bindConstant();
    }

    /* access modifiers changed from: protected */
    public final void install(Module module) {
        this.binder.install(module);
    }

    /* access modifiers changed from: protected */
    public final void addError(String message, Object... arguments) {
        this.binder.addError(message, arguments);
    }

    /* access modifiers changed from: protected */
    public final void addError(Throwable t) {
        this.binder.addError(t);
    }

    /* access modifiers changed from: protected */
    public final void addError(Message message) {
        this.binder.addError(message);
    }

    /* access modifiers changed from: protected */
    public final void requestInjection(Object instance) {
        this.binder.requestInjection(instance);
    }

    /* access modifiers changed from: protected */
    public final void requestStaticInjection(Class<?>... types) {
        this.binder.requestStaticInjection(types);
    }

    /* access modifiers changed from: protected */
    public final void requireBinding(Key<?> key) {
        this.binder.getProvider(key);
    }

    /* access modifiers changed from: protected */
    public final void requireBinding(Class<?> type) {
        this.binder.getProvider(type);
    }

    /* access modifiers changed from: protected */
    public final <T> Provider<T> getProvider(Key<T> key) {
        return this.binder.getProvider(key);
    }

    /* access modifiers changed from: protected */
    public final <T> Provider<T> getProvider(Class<T> type) {
        return this.binder.getProvider(type);
    }

    /* access modifiers changed from: protected */
    public final void convertToTypes(Matcher<? super TypeLiteral<?>> typeMatcher, TypeConverter converter) {
        this.binder.convertToTypes(typeMatcher, converter);
    }

    /* access modifiers changed from: protected */
    public final Stage currentStage() {
        return this.binder.currentStage();
    }

    /* access modifiers changed from: protected */
    public <T> MembersInjector<T> getMembersInjector(Class<T> type) {
        return this.binder.getMembersInjector(type);
    }

    /* access modifiers changed from: protected */
    public <T> MembersInjector<T> getMembersInjector(TypeLiteral<T> type) {
        return this.binder.getMembersInjector(type);
    }

    /* access modifiers changed from: protected */
    public void bindListener(Matcher<? super TypeLiteral<?>> typeMatcher, TypeListener listener) {
        this.binder.bindListener(typeMatcher, listener);
    }
}
