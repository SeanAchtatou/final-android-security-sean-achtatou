package com.google.inject;

import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;

class Initializables {
    Initializables() {
    }

    static <T> Initializable<T> of(final T instance) {
        return new Initializable<T>() {
            public T get(Errors errors) throws ErrorsException {
                return instance;
            }

            public String toString() {
                return String.valueOf(instance);
            }
        };
    }
}
