package com.google.inject;

import com.google.inject.internal.ConstructionContext;
import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.ImmutableSet;
import com.google.inject.internal.InternalContext;
import com.google.inject.spi.InjectionPoint;
import java.lang.reflect.InvocationTargetException;

class ConstructorInjector<T> {
    private final ConstructionProxy<T> constructionProxy;
    private final ImmutableSet<InjectionPoint> injectableMembers;
    private final MembersInjectorImpl<T> membersInjector;
    private final SingleParameterInjector<?>[] parameterInjectors;

    ConstructorInjector(ImmutableSet<InjectionPoint> injectableMembers2, ConstructionProxy<T> constructionProxy2, SingleParameterInjector<?>[] parameterInjectors2, MembersInjectorImpl<T> membersInjector2) throws ErrorsException {
        this.injectableMembers = injectableMembers2;
        this.constructionProxy = constructionProxy2;
        this.parameterInjectors = parameterInjectors2;
        this.membersInjector = membersInjector2;
    }

    public ImmutableSet<InjectionPoint> getInjectableMembers() {
        return this.injectableMembers;
    }

    /* access modifiers changed from: package-private */
    public ConstructionProxy<T> getConstructionProxy() {
        return this.constructionProxy;
    }

    /* access modifiers changed from: package-private */
    public Object construct(Errors errors, InternalContext context, Class<?> expectedType) throws ErrorsException {
        Throwable cause;
        ConstructionContext<T> constructionContext = context.getConstructionContext(this);
        if (constructionContext.isConstructing()) {
            return constructionContext.createProxy(errors, expectedType);
        }
        T t = constructionContext.getCurrentReference();
        if (t != null) {
            return t;
        }
        try {
            constructionContext.startConstruction();
            T t2 = this.constructionProxy.newInstance(SingleParameterInjector.getAll(errors, context, this.parameterInjectors));
            constructionContext.setProxyDelegates(t2);
            constructionContext.finishConstruction();
            constructionContext.setCurrentReference(t2);
            this.membersInjector.injectMembers(t2, errors, context);
            this.membersInjector.notifyListeners(t2, errors);
            constructionContext.removeCurrentReference();
            return t2;
        } catch (InvocationTargetException e) {
            InvocationTargetException userException = e;
            try {
                if (userException.getCause() != null) {
                    cause = userException.getCause();
                } else {
                    cause = userException;
                }
                throw errors.withSource(this.constructionProxy.getInjectionPoint()).errorInjectingConstructor(cause).toException();
            } catch (Throwable th) {
                constructionContext.removeCurrentReference();
                throw th;
            }
        } catch (Throwable th2) {
            constructionContext.finishConstruction();
            throw th2;
        }
    }
}
