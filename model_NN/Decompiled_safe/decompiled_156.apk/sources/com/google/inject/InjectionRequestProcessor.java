package com.google.inject;

import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.ImmutableList;
import com.google.inject.internal.InternalContext;
import com.google.inject.internal.Lists;
import com.google.inject.spi.InjectionPoint;
import com.google.inject.spi.InjectionRequest;
import com.google.inject.spi.StaticInjectionRequest;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

class InjectionRequestProcessor extends AbstractProcessor {
    private final Initializer initializer;
    private final List<StaticInjection> staticInjections = Lists.newArrayList();

    InjectionRequestProcessor(Errors errors, Initializer initializer2) {
        super(errors);
        this.initializer = initializer2;
    }

    public Boolean visit(StaticInjectionRequest request) {
        this.staticInjections.add(new StaticInjection(this.injector, request));
        return true;
    }

    public Boolean visit(InjectionRequest request) {
        Set<InjectionPoint> injectionPoints;
        try {
            injectionPoints = request.getInjectionPoints();
        } catch (ConfigurationException e) {
            this.errors.merge(e.getErrorMessages());
            injectionPoints = (Set) e.getPartialValue();
        }
        this.initializer.requestInjection(this.injector, request.getInstance(), request.getSource(), injectionPoints);
        return true;
    }

    public void validate() {
        for (StaticInjection staticInjection : this.staticInjections) {
            staticInjection.validate();
        }
    }

    public void injectMembers() {
        for (StaticInjection staticInjection : this.staticInjections) {
            staticInjection.injectMembers();
        }
    }

    private class StaticInjection {
        final InjectorImpl injector;
        ImmutableList<SingleMemberInjector> memberInjectors;
        final StaticInjectionRequest request;
        final Object source;

        public StaticInjection(InjectorImpl injector2, StaticInjectionRequest request2) {
            this.injector = injector2;
            this.source = request2.getSource();
            this.request = request2;
        }

        /* access modifiers changed from: package-private */
        public void validate() {
            Set<InjectionPoint> injectionPoints;
            Errors errorsForMember = InjectionRequestProcessor.this.errors.withSource(this.source);
            try {
                injectionPoints = this.request.getInjectionPoints();
            } catch (ConfigurationException e) {
                ConfigurationException e2 = e;
                InjectionRequestProcessor.this.errors.merge(e2.getErrorMessages());
                injectionPoints = (Set) e2.getPartialValue();
            }
            this.memberInjectors = this.injector.membersInjectorStore.getInjectors(injectionPoints, errorsForMember);
        }

        /* access modifiers changed from: package-private */
        public void injectMembers() {
            try {
                this.injector.callInContext(new ContextualCallable<Void>() {
                    public Void call(InternalContext context) {
                        Iterator i$ = StaticInjection.this.memberInjectors.iterator();
                        while (i$.hasNext()) {
                            ((SingleMemberInjector) i$.next()).inject(InjectionRequestProcessor.this.errors, context, null);
                        }
                        return null;
                    }
                });
            } catch (ErrorsException e) {
                throw new AssertionError();
            }
        }
    }
}
