package com.google.inject;

import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.InternalContext;
import com.google.inject.internal.InternalFactory;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.InjectionPoint;
import java.lang.reflect.Field;

class SingleFieldInjector implements SingleMemberInjector {
    final Dependency<?> dependency;
    final InternalFactory<?> factory;
    final Field field;
    final InjectionPoint injectionPoint;

    public SingleFieldInjector(InjectorImpl injector, InjectionPoint injectionPoint2, Errors errors) throws ErrorsException {
        this.injectionPoint = injectionPoint2;
        this.field = (Field) injectionPoint2.getMember();
        this.dependency = injectionPoint2.getDependencies().get(0);
        this.field.setAccessible(true);
        this.factory = injector.getInternalFactory(this.dependency.getKey(), errors);
    }

    public InjectionPoint getInjectionPoint() {
        return this.injectionPoint;
    }

    public void inject(Errors errors, InternalContext context, Object o) {
        Errors errors2 = errors.withSource(this.dependency);
        context.setDependency(this.dependency);
        try {
            this.field.set(o, this.factory.get(errors2, context, this.dependency));
            context.setDependency(null);
        } catch (ErrorsException e) {
            errors2.withSource(this.injectionPoint).merge(e.getErrors());
            context.setDependency(null);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (Throwable th) {
            context.setDependency(null);
            throw th;
        }
    }
}
