package com.google.inject;

import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.ImmutableList;
import com.google.inject.internal.ImmutableSet;
import com.google.inject.internal.InternalContext;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.InjectionPoint;
import java.util.Iterator;

class MembersInjectorImpl<T> implements MembersInjector<T> {
    private final ImmutableList<InjectionListener<? super T>> injectionListeners;
    private final InjectorImpl injector;
    private final ImmutableList<SingleMemberInjector> memberInjectors;
    private final TypeLiteral<T> typeLiteral;
    private final ImmutableList<MembersInjector<? super T>> userMembersInjectors;

    MembersInjectorImpl(InjectorImpl injector2, TypeLiteral<T> typeLiteral2, EncounterImpl<T> encounter, ImmutableList<SingleMemberInjector> memberInjectors2) {
        this.injector = injector2;
        this.typeLiteral = typeLiteral2;
        this.memberInjectors = memberInjectors2;
        this.userMembersInjectors = encounter.getMembersInjectors();
        this.injectionListeners = encounter.getInjectionListeners();
    }

    public ImmutableList<SingleMemberInjector> getMemberInjectors() {
        return this.memberInjectors;
    }

    public void injectMembers(T instance) {
        Errors errors = new Errors(this.typeLiteral);
        try {
            injectAndNotify(instance, errors);
        } catch (ErrorsException e) {
            errors.merge(e.getErrors());
        }
        errors.throwProvisionExceptionIfErrorsExist();
    }

    /* access modifiers changed from: package-private */
    public void injectAndNotify(final T instance, final Errors errors) throws ErrorsException {
        if (instance != null) {
            this.injector.callInContext(new ContextualCallable<Void>() {
                public Void call(InternalContext context) throws ErrorsException {
                    MembersInjectorImpl.this.injectMembers(instance, errors, context);
                    return null;
                }
            });
            notifyListeners(instance, errors);
        }
    }

    /* access modifiers changed from: package-private */
    public void notifyListeners(T instance, Errors errors) throws ErrorsException {
        int numErrorsBefore = errors.size();
        Iterator i$ = this.injectionListeners.iterator();
        while (i$.hasNext()) {
            InjectionListener<? super T> injectionListener = (InjectionListener) i$.next();
            try {
                injectionListener.afterInjection(instance);
            } catch (RuntimeException e) {
                errors.errorNotifyingInjectionListener(injectionListener, this.typeLiteral, e);
            }
        }
        errors.throwIfNewErrors(numErrorsBefore);
    }

    /* access modifiers changed from: package-private */
    public void injectMembers(T t, Errors errors, InternalContext context) {
        int size = this.memberInjectors.size();
        for (int i = 0; i < size; i++) {
            this.memberInjectors.get(i).inject(errors, context, t);
        }
        int size2 = this.userMembersInjectors.size();
        for (int i2 = 0; i2 < size2; i2++) {
            MembersInjector<? super T> userMembersInjector = this.userMembersInjectors.get(i2);
            try {
                userMembersInjector.injectMembers(t);
            } catch (RuntimeException e) {
                errors.errorInUserInjector(userMembersInjector, this.typeLiteral, e);
            }
        }
    }

    public String toString() {
        return "MembersInjector<" + this.typeLiteral + ">";
    }

    public ImmutableSet<InjectionPoint> getInjectionPoints() {
        ImmutableSet.Builder<InjectionPoint> builder = ImmutableSet.builder();
        Iterator i$ = this.memberInjectors.iterator();
        while (i$.hasNext()) {
            builder.add(((SingleMemberInjector) i$.next()).getInjectionPoint());
        }
        return builder.build();
    }
}
