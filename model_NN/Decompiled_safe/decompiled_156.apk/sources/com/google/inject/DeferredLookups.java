package com.google.inject;

import com.google.inject.internal.Errors;
import com.google.inject.internal.Lists;
import com.google.inject.spi.Element;
import com.google.inject.spi.MembersInjectorLookup;
import com.google.inject.spi.ProviderLookup;
import java.util.List;

class DeferredLookups implements Lookups {
    private final InjectorImpl injector;
    private final List<Element> lookups = Lists.newArrayList();

    public DeferredLookups(InjectorImpl injector2) {
        this.injector = injector2;
    }

    public void initialize(Errors errors) {
        this.injector.lookups = this.injector;
        new LookupProcessor(errors).process(this.injector, this.lookups);
    }

    public <T> Provider<T> getProvider(Key<T> key) {
        ProviderLookup<T> lookup = new ProviderLookup<>(key, key);
        this.lookups.add(lookup);
        return lookup.getProvider();
    }

    public <T> MembersInjector<T> getMembersInjector(TypeLiteral<T> type) {
        MembersInjectorLookup<T> lookup = new MembersInjectorLookup<>(type, type);
        this.lookups.add(lookup);
        return lookup.getMembersInjector();
    }
}
