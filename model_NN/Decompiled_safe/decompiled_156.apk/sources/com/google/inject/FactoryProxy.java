package com.google.inject;

import com.google.inject.BindingProcessor;
import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.InternalContext;
import com.google.inject.internal.InternalFactory;
import com.google.inject.internal.ToStringBuilder;
import com.google.inject.spi.Dependency;

class FactoryProxy<T> implements InternalFactory<T>, BindingProcessor.CreationListener {
    private final InjectorImpl injector;
    private final Key<T> key;
    private final Object source;
    private InternalFactory<? extends T> targetFactory;
    private final Key<? extends T> targetKey;

    FactoryProxy(InjectorImpl injector2, Key<T> key2, Key<? extends T> targetKey2, Object source2) {
        this.injector = injector2;
        this.key = key2;
        this.targetKey = targetKey2;
        this.source = source2;
    }

    public void notify(Errors errors) {
        try {
            this.targetFactory = this.injector.getInternalFactory(this.targetKey, errors.withSource(this.source));
        } catch (ErrorsException e) {
            errors.merge(e.getErrors());
        }
    }

    public T get(Errors errors, InternalContext context, Dependency<?> dependency) throws ErrorsException {
        return this.targetFactory.get(errors.withSource(this.targetKey), context, dependency);
    }

    public String toString() {
        return new ToStringBuilder(FactoryProxy.class).add("key", this.key).add("provider", this.targetFactory).toString();
    }
}
