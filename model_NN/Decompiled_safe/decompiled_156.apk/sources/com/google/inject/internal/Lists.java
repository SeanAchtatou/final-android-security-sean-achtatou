package com.google.inject.internal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

public final class Lists {
    private Lists() {
    }

    public static <E> ArrayList<E> newArrayList() {
        return new ArrayList<>();
    }

    public static <E> ArrayList<E> newArrayList(Object... objArr) {
        ArrayList<E> list = new ArrayList<>(computeArrayListCapacity(objArr.length));
        Collections.addAll(list, objArr);
        return list;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    static int computeArrayListCapacity(int arraySize) {
        Preconditions.checkArgument(arraySize >= 0);
        return (int) Math.min(5 + ((long) arraySize) + ((long) (arraySize / 10)), 2147483647L);
    }

    public static <E> ArrayList<E> newArrayList(Iterable iterable) {
        if (iterable instanceof Collection) {
            return new ArrayList<>((Collection) iterable);
        }
        return newArrayList(iterable.iterator());
    }

    public static <E> ArrayList<E> newArrayList(Iterator it) {
        ArrayList<E> list = newArrayList();
        while (it.hasNext()) {
            list.add(it.next());
        }
        return list;
    }

    public static <E> ArrayList<E> newArrayList(@Nullable E first, E[] rest) {
        ArrayList<E> result = new ArrayList<>(rest.length + 1);
        result.add(first);
        for (E element : rest) {
            result.add(element);
        }
        return result;
    }
}
