package com.google.inject.internal;

import java.lang.reflect.Constructor;
import java.lang.reflect.Member;

public class StackTraceElements {
    public static Object forMember(Member member) {
        if (member == null) {
            return SourceProvider.UNKNOWN_SOURCE;
        }
        return new StackTraceElement(member.getDeclaringClass().getName(), MoreTypes.memberType(member) == Constructor.class ? "<init>" : member.getName(), null, -1);
    }

    public static Object forType(Class<?> implementation) {
        return new StackTraceElement(implementation.getName(), "class", null, -1);
    }
}
