package com.google.inject.internal;

import com.google.inject.Scope;
import com.google.inject.Scopes;
import com.google.inject.Singleton;
import com.google.inject.Stage;
import com.google.inject.binder.ScopedBindingBuilder;
import com.google.inject.spi.BindingScopingVisitor;
import java.lang.annotation.Annotation;

public abstract class Scoping {
    public static final Scoping EAGER_SINGLETON = new Scoping() {
        public <V> V acceptVisitor(BindingScopingVisitor<V> visitor) {
            return visitor.visitEagerSingleton();
        }

        public Scope getScopeInstance() {
            return Scopes.SINGLETON;
        }

        public String toString() {
            return "eager singleton";
        }

        public void applyTo(ScopedBindingBuilder scopedBindingBuilder) {
            scopedBindingBuilder.asEagerSingleton();
        }
    };
    public static final Scoping SINGLETON_ANNOTATION = new Scoping() {
        public <V> V acceptVisitor(BindingScopingVisitor<V> visitor) {
            return visitor.visitScopeAnnotation(Singleton.class);
        }

        public Class<? extends Annotation> getScopeAnnotation() {
            return Singleton.class;
        }

        public String toString() {
            return Singleton.class.getName();
        }

        public void applyTo(ScopedBindingBuilder scopedBindingBuilder) {
            scopedBindingBuilder.in(Singleton.class);
        }
    };
    public static final Scoping SINGLETON_INSTANCE = new Scoping() {
        public <V> V acceptVisitor(BindingScopingVisitor<V> visitor) {
            return visitor.visitScope(Scopes.SINGLETON);
        }

        public Scope getScopeInstance() {
            return Scopes.SINGLETON;
        }

        public String toString() {
            return Scopes.SINGLETON.toString();
        }

        public void applyTo(ScopedBindingBuilder scopedBindingBuilder) {
            scopedBindingBuilder.in(Scopes.SINGLETON);
        }
    };
    public static final Scoping UNSCOPED = new Scoping() {
        public <V> V acceptVisitor(BindingScopingVisitor<V> visitor) {
            return visitor.visitNoScoping();
        }

        public Scope getScopeInstance() {
            return Scopes.NO_SCOPE;
        }

        public String toString() {
            return Scopes.NO_SCOPE.toString();
        }

        public void applyTo(ScopedBindingBuilder scopedBindingBuilder) {
        }
    };

    public abstract <V> V acceptVisitor(BindingScopingVisitor<V> bindingScopingVisitor);

    public abstract void applyTo(ScopedBindingBuilder scopedBindingBuilder);

    public static Scoping forAnnotation(final Class<? extends Annotation> scopingAnnotation) {
        if (scopingAnnotation == Singleton.class) {
            return SINGLETON_ANNOTATION;
        }
        return new Scoping() {
            public <V> V acceptVisitor(BindingScopingVisitor<V> visitor) {
                return visitor.visitScopeAnnotation(scopingAnnotation);
            }

            public Class<? extends Annotation> getScopeAnnotation() {
                return scopingAnnotation;
            }

            public String toString() {
                return scopingAnnotation.getName();
            }

            public void applyTo(ScopedBindingBuilder scopedBindingBuilder) {
                scopedBindingBuilder.in(scopingAnnotation);
            }
        };
    }

    public static Scoping forInstance(final Scope scope) {
        if (scope == Scopes.SINGLETON) {
            return SINGLETON_INSTANCE;
        }
        return new Scoping() {
            public <V> V acceptVisitor(BindingScopingVisitor<V> visitor) {
                return visitor.visitScope(scope);
            }

            public Scope getScopeInstance() {
                return scope;
            }

            public String toString() {
                return scope.toString();
            }

            public void applyTo(ScopedBindingBuilder scopedBindingBuilder) {
                scopedBindingBuilder.in(scope);
            }
        };
    }

    public boolean isExplicitlyScoped() {
        return this != UNSCOPED;
    }

    public boolean isNoScope() {
        return getScopeInstance() == Scopes.NO_SCOPE;
    }

    public boolean isEagerSingleton(Stage stage) {
        if (this == EAGER_SINGLETON) {
            return true;
        }
        if (stage == Stage.PRODUCTION) {
            return this == SINGLETON_ANNOTATION || this == SINGLETON_INSTANCE;
        }
        return false;
    }

    public Scope getScopeInstance() {
        return null;
    }

    public Class<? extends Annotation> getScopeAnnotation() {
        return null;
    }

    private Scoping() {
    }
}
