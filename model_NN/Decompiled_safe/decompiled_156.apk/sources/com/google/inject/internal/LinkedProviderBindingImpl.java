package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.spi.BindingTargetVisitor;
import com.google.inject.spi.ProviderKeyBinding;

public final class LinkedProviderBindingImpl<T> extends BindingImpl<T> implements ProviderKeyBinding<T> {
    final Key<? extends Provider<? extends T>> providerKey;

    public LinkedProviderBindingImpl(Injector injector, Key<T> key, Object source, InternalFactory<? extends T> internalFactory, Scoping scoping, Key<? extends Provider<? extends T>> providerKey2) {
        super(injector, key, source, internalFactory, scoping);
        this.providerKey = providerKey2;
    }

    LinkedProviderBindingImpl(Object source, Key<T> key, Scoping scoping, Key<? extends Provider<? extends T>> providerKey2) {
        super(source, key, scoping);
        this.providerKey = providerKey2;
    }

    public <V> V acceptTargetVisitor(BindingTargetVisitor<? super T, V> visitor) {
        return visitor.visit(this);
    }

    public Key<? extends Provider<? extends T>> getProviderKey() {
        return this.providerKey;
    }

    public BindingImpl<T> withScoping(Scoping scoping) {
        return new LinkedProviderBindingImpl(getSource(), getKey(), scoping, this.providerKey);
    }

    public BindingImpl<T> withKey(Key<T> key) {
        return new LinkedProviderBindingImpl(getSource(), key, getScoping(), this.providerKey);
    }

    public void applyTo(Binder binder) {
        getScoping().applyTo(binder.withSource(getSource()).bind(getKey()).toProvider(getProviderKey()));
    }

    public String toString() {
        return new ToStringBuilder(ProviderKeyBinding.class).add("key", getKey()).add("source", getSource()).add("scope", getScoping()).add("provider", this.providerKey).toString();
    }
}
