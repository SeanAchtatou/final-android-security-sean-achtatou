package com.google.inject.internal;

import java.lang.reflect.Array;

public final class ObjectArrays {
    private ObjectArrays() {
    }

    public static <T> T[] newArray(T[] reference, int length) {
        return (Object[]) ((Object[]) Array.newInstance(reference.getClass().getComponentType(), length));
    }
}
