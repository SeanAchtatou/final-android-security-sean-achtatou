package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.Exposed;
import com.google.inject.Key;
import com.google.inject.PrivateBinder;
import com.google.inject.Provider;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.ProviderWithDependencies;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;

public class ProviderMethod<T> implements ProviderWithDependencies<T> {
    private final ImmutableSet<Dependency<?>> dependencies;
    private final boolean exposed;
    private final Object instance;
    private final Key<T> key;
    private final Method method;
    private final List<Provider<?>> parameterProviders;
    private final Class<? extends Annotation> scopeAnnotation;

    ProviderMethod(Key<T> key2, Method method2, Object instance2, ImmutableSet<Dependency<?>> dependencies2, List<Provider<?>> parameterProviders2, Class<? extends Annotation> scopeAnnotation2) {
        this.key = key2;
        this.scopeAnnotation = scopeAnnotation2;
        this.instance = instance2;
        this.dependencies = dependencies2;
        this.method = method2;
        this.parameterProviders = parameterProviders2;
        this.exposed = method2.isAnnotationPresent(Exposed.class);
        method2.setAccessible(true);
    }

    public Key<T> getKey() {
        return this.key;
    }

    public Method getMethod() {
        return this.method;
    }

    public Object getInstance() {
        return this.instance;
    }

    public void configure(Binder binder) {
        Binder binder2 = binder.withSource(this.method);
        if (this.scopeAnnotation != null) {
            binder2.bind(this.key).toProvider(this).in(this.scopeAnnotation);
        } else {
            binder2.bind(this.key).toProvider(this);
        }
        if (this.exposed) {
            ((PrivateBinder) binder2).expose((Key<?>) this.key);
        }
    }

    public T get() {
        Object[] parameters = new Object[this.parameterProviders.size()];
        for (int i = 0; i < parameters.length; i++) {
            parameters[i] = this.parameterProviders.get(i).get();
        }
        try {
            return this.method.invoke(this.instance, parameters);
        } catch (IllegalAccessException e) {
            throw new AssertionError(e);
        } catch (InvocationTargetException e2) {
            throw new RuntimeException(e2);
        }
    }

    public Set<Dependency<?>> getDependencies() {
        return this.dependencies;
    }
}
