package com.google.inject.internal;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;

public class ConstructionContext<T> {
    boolean constructing;
    T currentReference;
    List<DelegatingInvocationHandler<T>> invocationHandlers;

    public T getCurrentReference() {
        return this.currentReference;
    }

    public void removeCurrentReference() {
        this.currentReference = null;
    }

    public void setCurrentReference(T currentReference2) {
        this.currentReference = currentReference2;
    }

    public boolean isConstructing() {
        return this.constructing;
    }

    public void startConstruction() {
        this.constructing = true;
    }

    public void finishConstruction() {
        this.constructing = false;
        this.invocationHandlers = null;
    }

    public Object createProxy(Errors errors, Class<?> expectedType) throws ErrorsException {
        if (!expectedType.isInterface()) {
            throw errors.cannotSatisfyCircularDependency(expectedType).toException();
        }
        if (this.invocationHandlers == null) {
            this.invocationHandlers = new ArrayList();
        }
        DelegatingInvocationHandler<T> invocationHandler = new DelegatingInvocationHandler<>();
        this.invocationHandlers.add(invocationHandler);
        return expectedType.cast(Proxy.newProxyInstance(BytecodeGen.getClassLoader(expectedType), new Class[]{expectedType}, invocationHandler));
    }

    public void setProxyDelegates(T delegate) {
        if (this.invocationHandlers != null) {
            for (DelegatingInvocationHandler<T> handler : this.invocationHandlers) {
                handler.setDelegate(delegate);
            }
        }
    }

    static class DelegatingInvocationHandler<T> implements InvocationHandler {
        T delegate;

        DelegatingInvocationHandler() {
        }

        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (this.delegate == null) {
                throw new IllegalStateException("This is a proxy used to support circular references involving constructors. The object we're proxying is not constructed yet. Please wait until after injection has completed to use this object.");
            }
            try {
                return method.invoke(this.delegate, args);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            } catch (IllegalArgumentException e2) {
                throw new RuntimeException(e2);
            } catch (InvocationTargetException e3) {
                throw e3.getTargetException();
            }
        }

        /* access modifiers changed from: package-private */
        public void setDelegate(T delegate2) {
            this.delegate = delegate2;
        }
    }
}
