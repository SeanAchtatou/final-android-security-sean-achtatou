package com.google.inject.internal;

import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Map;
import java.util.logging.Logger;

public final class BytecodeGen {
    private static final String CGLIB_PACKAGE = " ";
    private static final Map<ClassLoader, ClassLoader> CLASS_LOADER_CACHE = new MapMaker().weakKeys().weakValues().makeComputingMap(new Function<ClassLoader, ClassLoader>() {
        public ClassLoader apply(@Nullable final ClassLoader typeClassLoader) {
            BytecodeGen.logger.fine("Creating a bridge ClassLoader for " + typeClassLoader);
            return (ClassLoader) AccessController.doPrivileged(new PrivilegedAction<ClassLoader>() {
                public ClassLoader run() {
                    return new BridgeClassLoader(typeClassLoader);
                }
            });
        }
    });
    static final ClassLoader GUICE_CLASS_LOADER = BytecodeGen.class.getClassLoader();
    /* access modifiers changed from: private */
    public static final String GUICE_INTERNAL_PACKAGE = BytecodeGen.class.getName().replaceFirst("\\.internal\\..*$", ".internal");
    static final boolean HOOK_ENABLED = "true".equals(System.getProperty("guice.custom.loader", "true"));
    /* access modifiers changed from: private */
    public static final Logger logger = Logger.getLogger(BytecodeGen.class.getName());

    private static ClassLoader canonicalize(ClassLoader classLoader) {
        return classLoader != null ? classLoader : (ClassLoader) Preconditions.checkNotNull(getSystemClassLoaderOrNull(), "Couldn't get a ClassLoader");
    }

    private static ClassLoader getSystemClassLoaderOrNull() {
        try {
            return ClassLoader.getSystemClassLoader();
        } catch (SecurityException e) {
            return null;
        }
    }

    public static ClassLoader getClassLoader(Class<?> type) {
        return getClassLoader(type, type.getClassLoader());
    }

    private static ClassLoader getClassLoader(Class<?> type, ClassLoader delegate) {
        ClassLoader delegate2 = canonicalize(delegate);
        if (delegate2 == getSystemClassLoaderOrNull()) {
            return delegate2;
        }
        if (delegate2 instanceof BridgeClassLoader) {
            return delegate2;
        }
        return (!HOOK_ENABLED || Visibility.forType(type) != Visibility.PUBLIC) ? delegate2 : CLASS_LOADER_CACHE.get(delegate2);
    }

    public enum Visibility {
        PUBLIC {
            public Visibility and(Visibility that) {
                return that;
            }
        },
        SAME_PACKAGE {
            public Visibility and(Visibility that) {
                return this;
            }
        };

        public abstract Visibility and(Visibility visibility);

        public static Visibility forMember(Member member) {
            if ((member.getModifiers() & 5) == 0) {
                return SAME_PACKAGE;
            }
            for (Class type : member instanceof Constructor ? ((Constructor) member).getParameterTypes() : ((Method) member).getParameterTypes()) {
                if (forType(type) == SAME_PACKAGE) {
                    return SAME_PACKAGE;
                }
            }
            return PUBLIC;
        }

        public static Visibility forType(Class<?> type) {
            return (type.getModifiers() & 5) != 0 ? PUBLIC : SAME_PACKAGE;
        }
    }

    private static class BridgeClassLoader extends ClassLoader {
        public BridgeClassLoader(ClassLoader usersClassLoader) {
            super(usersClassLoader);
        }

        /* access modifiers changed from: protected */
        public Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
            if (name.startsWith(BytecodeGen.GUICE_INTERNAL_PACKAGE) || name.startsWith(BytecodeGen.CGLIB_PACKAGE)) {
                try {
                    Class<?> clazz = BytecodeGen.GUICE_CLASS_LOADER.loadClass(name);
                    if (resolve) {
                        resolveClass(clazz);
                    }
                    return clazz;
                } catch (Exception e) {
                }
            }
            return super.loadClass(name, resolve);
        }
    }
}
