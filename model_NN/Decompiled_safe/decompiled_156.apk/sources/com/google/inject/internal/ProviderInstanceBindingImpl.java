package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.spi.BindingTargetVisitor;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.HasDependencies;
import com.google.inject.spi.InjectionPoint;
import com.google.inject.spi.ProviderInstanceBinding;
import java.util.Set;

public final class ProviderInstanceBindingImpl<T> extends BindingImpl<T> implements ProviderInstanceBinding<T> {
    final ImmutableSet<InjectionPoint> injectionPoints;
    final Provider<? extends T> providerInstance;

    public ProviderInstanceBindingImpl(Injector injector, Key<T> key, Object source, InternalFactory<? extends T> internalFactory, Scoping scoping, Provider<? extends T> providerInstance2, Set<InjectionPoint> injectionPoints2) {
        super(injector, key, source, internalFactory, scoping);
        this.providerInstance = providerInstance2;
        this.injectionPoints = ImmutableSet.copyOf(injectionPoints2);
    }

    public ProviderInstanceBindingImpl(Object source, Key<T> key, Scoping scoping, Set<InjectionPoint> injectionPoints2, Provider<? extends T> providerInstance2) {
        super(source, key, scoping);
        this.injectionPoints = ImmutableSet.copyOf(injectionPoints2);
        this.providerInstance = providerInstance2;
    }

    public <V> V acceptTargetVisitor(BindingTargetVisitor<? super T, V> visitor) {
        return visitor.visit(this);
    }

    public Provider<? extends T> getProviderInstance() {
        return this.providerInstance;
    }

    public Set<InjectionPoint> getInjectionPoints() {
        return this.injectionPoints;
    }

    public Set<Dependency<?>> getDependencies() {
        return this.providerInstance instanceof HasDependencies ? ImmutableSet.copyOf(((HasDependencies) this.providerInstance).getDependencies()) : Dependency.forInjectionPoints(this.injectionPoints);
    }

    public BindingImpl<T> withScoping(Scoping scoping) {
        return new ProviderInstanceBindingImpl(getSource(), getKey(), scoping, this.injectionPoints, this.providerInstance);
    }

    public BindingImpl<T> withKey(Key<T> key) {
        return new ProviderInstanceBindingImpl(getSource(), key, getScoping(), this.injectionPoints, this.providerInstance);
    }

    public void applyTo(Binder binder) {
        getScoping().applyTo(binder.withSource(getSource()).bind(getKey()).toProvider(getProviderInstance()));
    }

    public String toString() {
        return new ToStringBuilder(ProviderInstanceBinding.class).add("key", getKey()).add("source", getSource()).add("scope", getScoping()).add("provider", this.providerInstance).toString();
    }
}
