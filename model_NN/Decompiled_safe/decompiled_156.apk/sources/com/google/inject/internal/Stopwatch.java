package com.google.inject.internal;

import java.util.logging.Logger;

public class Stopwatch {
    private static final Logger logger = Logger.getLogger(Stopwatch.class.getName());
    private long start = System.currentTimeMillis();

    public long reset() {
        long now = System.currentTimeMillis();
        try {
            return now - this.start;
        } finally {
            this.start = now;
        }
    }

    public void resetAndLog(String label) {
        logger.fine(label + ": " + reset() + "ms");
    }
}
