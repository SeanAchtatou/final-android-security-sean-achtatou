package com.google.inject.internal;

import com.google.inject.ConfigurationException;
import com.google.inject.TypeLiteral;
import com.google.inject.internal.ImmutableMap;
import com.google.inject.spi.Message;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Map;
import java.util.NoSuchElementException;

public class MoreTypes {
    public static final Type[] EMPTY_TYPE_ARRAY = new Type[0];
    private static final Map<TypeLiteral<?>, TypeLiteral<?>> PRIMITIVE_TO_WRAPPER = new ImmutableMap.Builder().put(TypeLiteral.get(Boolean.TYPE), TypeLiteral.get(Boolean.class)).put(TypeLiteral.get(Byte.TYPE), TypeLiteral.get(Byte.class)).put(TypeLiteral.get(Short.TYPE), TypeLiteral.get(Short.class)).put(TypeLiteral.get(Integer.TYPE), TypeLiteral.get(Integer.class)).put(TypeLiteral.get(Long.TYPE), TypeLiteral.get(Long.class)).put(TypeLiteral.get(Float.TYPE), TypeLiteral.get(Float.class)).put(TypeLiteral.get(Double.TYPE), TypeLiteral.get(Double.class)).put(TypeLiteral.get(Character.TYPE), TypeLiteral.get(Character.class)).put(TypeLiteral.get(Void.TYPE), TypeLiteral.get(Void.class)).build();

    private interface CompositeType {
        boolean isFullySpecified();
    }

    private MoreTypes() {
    }

    public static <T> TypeLiteral<T> makeKeySafe(TypeLiteral<T> type) {
        if (!isFullySpecified(type.getType())) {
            throw new ConfigurationException(ImmutableSet.of(new Message(type + " cannot be used as a key; It is not fully specified.")));
        }
        TypeLiteral<T> wrappedPrimitives = PRIMITIVE_TO_WRAPPER.get(type);
        return wrappedPrimitives != null ? wrappedPrimitives : type;
    }

    /* access modifiers changed from: private */
    public static boolean isFullySpecified(Type type) {
        if (type instanceof Class) {
            return true;
        }
        if (type instanceof CompositeType) {
            return ((CompositeType) type).isFullySpecified();
        }
        if (type instanceof TypeVariable) {
            return false;
        }
        return ((CompositeType) canonicalize(type)).isFullySpecified();
    }

    public static Type canonicalize(Type type) {
        if ((type instanceof ParameterizedTypeImpl) || (type instanceof GenericArrayTypeImpl) || (type instanceof WildcardTypeImpl)) {
            return type;
        }
        if (type instanceof ParameterizedType) {
            ParameterizedType p = (ParameterizedType) type;
            return new ParameterizedTypeImpl(p.getOwnerType(), p.getRawType(), p.getActualTypeArguments());
        } else if (type instanceof GenericArrayType) {
            return new GenericArrayTypeImpl(((GenericArrayType) type).getGenericComponentType());
        } else {
            if ((type instanceof Class) && ((Class) type).isArray()) {
                return new GenericArrayTypeImpl(((Class) type).getComponentType());
            }
            if (!(type instanceof WildcardType)) {
                return type;
            }
            WildcardType w = (WildcardType) type;
            return new WildcardTypeImpl(w.getUpperBounds(), w.getLowerBounds());
        }
    }

    public static Member serializableCopy(Member member) {
        return member instanceof MemberImpl ? member : new MemberImpl(member);
    }

    public static Class<?> getRawType(Type type) {
        if (type instanceof Class) {
            return (Class) type;
        }
        if (type instanceof ParameterizedType) {
            Type rawType = ((ParameterizedType) type).getRawType();
            Preconditions.checkArgument(rawType instanceof Class, "Expected a Class, but <%s> is of type %s", type, type.getClass().getName());
            return (Class) rawType;
        } else if (type instanceof GenericArrayType) {
            return Object[].class;
        } else {
            if (type instanceof TypeVariable) {
                return Object.class;
            }
            throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + type + "> is of type " + type.getClass().getName());
        }
    }

    public static boolean equals(Type a, Type b) {
        if (a == b) {
            return true;
        }
        if (a instanceof Class) {
            return a.equals(b);
        }
        if (a instanceof ParameterizedType) {
            if (!(b instanceof ParameterizedType)) {
                return false;
            }
            ParameterizedType pa = (ParameterizedType) a;
            ParameterizedType pb = (ParameterizedType) b;
            return Objects.equal(pa.getOwnerType(), pb.getOwnerType()) && pa.getRawType().equals(pb.getRawType()) && Arrays.equals(pa.getActualTypeArguments(), pb.getActualTypeArguments());
        } else if (a instanceof GenericArrayType) {
            if (!(b instanceof GenericArrayType)) {
                return false;
            }
            return equals(((GenericArrayType) a).getGenericComponentType(), ((GenericArrayType) b).getGenericComponentType());
        } else if (a instanceof WildcardType) {
            if (!(b instanceof WildcardType)) {
                return false;
            }
            WildcardType wa = (WildcardType) a;
            WildcardType wb = (WildcardType) b;
            return Arrays.equals(wa.getUpperBounds(), wb.getUpperBounds()) && Arrays.equals(wa.getLowerBounds(), wb.getLowerBounds());
        } else if (!(a instanceof TypeVariable)) {
            return false;
        } else {
            if (!(b instanceof TypeVariable)) {
                return false;
            }
            TypeVariable typeVariable = (TypeVariable) a;
            TypeVariable typeVariable2 = (TypeVariable) b;
            return typeVariable.getGenericDeclaration() == typeVariable2.getGenericDeclaration() && typeVariable.getName().equals(typeVariable2.getName());
        }
    }

    public static int hashCode(Type type) {
        if (type instanceof Class) {
            return type.hashCode();
        }
        if (type instanceof ParameterizedType) {
            ParameterizedType p = (ParameterizedType) type;
            return (Arrays.hashCode(p.getActualTypeArguments()) ^ p.getRawType().hashCode()) ^ hashCodeOrZero(p.getOwnerType());
        } else if (type instanceof GenericArrayType) {
            return hashCode(((GenericArrayType) type).getGenericComponentType());
        } else {
            if (!(type instanceof WildcardType)) {
                return hashCodeOrZero(type);
            }
            WildcardType w = (WildcardType) type;
            return Arrays.hashCode(w.getLowerBounds()) ^ Arrays.hashCode(w.getUpperBounds());
        }
    }

    private static int hashCodeOrZero(Object o) {
        if (o != null) {
            return o.hashCode();
        }
        return 0;
    }

    public static String toString(Type type) {
        if (type instanceof Class) {
            return ((Class) type).getName();
        }
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Type[] arguments = parameterizedType.getActualTypeArguments();
            Type ownerType = parameterizedType.getOwnerType();
            StringBuilder stringBuilder = new StringBuilder();
            if (ownerType != null) {
                stringBuilder.append(toString(ownerType)).append(".");
            }
            stringBuilder.append(toString(parameterizedType.getRawType()));
            if (arguments.length > 0) {
                stringBuilder.append("<").append(toString(arguments[0]));
                for (int i = 1; i < arguments.length; i++) {
                    stringBuilder.append(", ").append(toString(arguments[i]));
                }
            }
            return stringBuilder.append(">").toString();
        } else if (type instanceof GenericArrayType) {
            return toString(((GenericArrayType) type).getGenericComponentType()) + "[]";
        } else {
            if (!(type instanceof WildcardType)) {
                return type.toString();
            }
            WildcardType wildcardType = (WildcardType) type;
            Type[] lowerBounds = wildcardType.getLowerBounds();
            Type[] upperBounds = wildcardType.getUpperBounds();
            if (upperBounds.length != 1 || lowerBounds.length > 1) {
                throw new UnsupportedOperationException("Unsupported wildcard type " + type);
            } else if (lowerBounds.length == 1) {
                if (upperBounds[0] == Object.class) {
                    return "? super " + toString(lowerBounds[0]);
                }
                throw new UnsupportedOperationException("Unsupported wildcard type " + type);
            } else if (upperBounds[0] == Object.class) {
                return "?";
            } else {
                return "? extends " + toString(upperBounds[0]);
            }
        }
    }

    public static Class<? extends Member> memberType(Member member) {
        Preconditions.checkNotNull(member, "member");
        if (member instanceof MemberImpl) {
            return ((MemberImpl) member).memberType;
        }
        if (member instanceof Field) {
            return Field.class;
        }
        if (member instanceof Method) {
            return Method.class;
        }
        if (member instanceof Constructor) {
            return Constructor.class;
        }
        throw new IllegalArgumentException("Unsupported implementation class for Member, " + member.getClass());
    }

    public static String toString(Member member) {
        Class<? extends Member> memberType = memberType(member);
        if (memberType == Method.class) {
            return member.getDeclaringClass().getName() + "." + member.getName() + "()";
        }
        if (memberType == Field.class) {
            return member.getDeclaringClass().getName() + "." + member.getName();
        }
        if (memberType == Constructor.class) {
            return member.getDeclaringClass().getName() + ".<init>()";
        }
        throw new AssertionError();
    }

    public static String memberKey(Member member) {
        Preconditions.checkNotNull(member, "member");
        return "<NO_MEMBER_KEY>";
    }

    public static Type getGenericSupertype(Type type, Class<?> rawType, Class<?> toResolve) {
        if (toResolve == rawType) {
            return type;
        }
        if (toResolve.isInterface()) {
            Class[] interfaces = rawType.getInterfaces();
            int length = interfaces.length;
            for (int i = 0; i < length; i++) {
                if (interfaces[i] == toResolve) {
                    return rawType.getGenericInterfaces()[i];
                }
                if (toResolve.isAssignableFrom(interfaces[i])) {
                    return getGenericSupertype(rawType.getGenericInterfaces()[i], interfaces[i], toResolve);
                }
            }
        }
        if (!rawType.isInterface()) {
            while (rawType != Object.class) {
                Class<?> rawSupertype = rawType.getSuperclass();
                if (rawSupertype == toResolve) {
                    return rawType.getGenericSuperclass();
                }
                if (toResolve.isAssignableFrom(rawSupertype)) {
                    return getGenericSupertype(rawType.getGenericSuperclass(), rawSupertype, toResolve);
                }
                rawType = rawSupertype;
            }
        }
        return toResolve;
    }

    public static Type resolveTypeVariable(Type type, Class<?> rawType, TypeVariable unknown) {
        Class<?> declaredByRaw = declaringClassOf(unknown);
        if (declaredByRaw == null) {
            return unknown;
        }
        Type declaredBy = getGenericSupertype(type, rawType, declaredByRaw);
        if (!(declaredBy instanceof ParameterizedType)) {
            return unknown;
        }
        return ((ParameterizedType) declaredBy).getActualTypeArguments()[indexOf(declaredByRaw.getTypeParameters(), unknown)];
    }

    private static int indexOf(Object[] array, Object toFind) {
        for (int i = 0; i < array.length; i++) {
            if (toFind.equals(array[i])) {
                return i;
            }
        }
        throw new NoSuchElementException();
    }

    private static Class<?> declaringClassOf(TypeVariable typeVariable) {
        GenericDeclaration genericDeclaration = typeVariable.getGenericDeclaration();
        if (genericDeclaration instanceof Class) {
            return (Class) genericDeclaration;
        }
        return null;
    }

    public static class ParameterizedTypeImpl implements ParameterizedType, Serializable, CompositeType {
        private static final long serialVersionUID = 0;
        private final Type ownerType;
        private final Type rawType;
        private final Type[] typeArguments;

        public ParameterizedTypeImpl(Type ownerType2, Type rawType2, Type... typeArguments2) {
            boolean z;
            boolean z2;
            if (rawType2 instanceof Class) {
                Class rawTypeAsClass = (Class) rawType2;
                if (ownerType2 != null || rawTypeAsClass.getEnclosingClass() == null) {
                    z = true;
                } else {
                    z = false;
                }
                Preconditions.checkArgument(z, "No owner type for enclosed %s", rawType2);
                if (ownerType2 == null || rawTypeAsClass.getEnclosingClass() != null) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                Preconditions.checkArgument(z2, "Owner type for unenclosed %s", rawType2);
            }
            this.ownerType = ownerType2 == null ? null : MoreTypes.canonicalize(ownerType2);
            this.rawType = MoreTypes.canonicalize(rawType2);
            this.typeArguments = (Type[]) typeArguments2.clone();
            for (int t = 0; t < this.typeArguments.length; t++) {
                Preconditions.checkNotNull(this.typeArguments[t], "type parameter");
                MoreTypes.checkNotPrimitive(this.typeArguments[t], "type parameters");
                this.typeArguments[t] = MoreTypes.canonicalize(this.typeArguments[t]);
            }
        }

        public Type[] getActualTypeArguments() {
            return (Type[]) this.typeArguments.clone();
        }

        public Type getRawType() {
            return this.rawType;
        }

        public Type getOwnerType() {
            return this.ownerType;
        }

        public boolean isFullySpecified() {
            if (this.ownerType != null && !MoreTypes.isFullySpecified(this.ownerType)) {
                return false;
            }
            if (!MoreTypes.isFullySpecified(this.rawType)) {
                return false;
            }
            for (Type type : this.typeArguments) {
                if (!MoreTypes.isFullySpecified(type)) {
                    return false;
                }
            }
            return true;
        }

        public boolean equals(Object other) {
            return (other instanceof ParameterizedType) && MoreTypes.equals(this, (ParameterizedType) other);
        }

        public int hashCode() {
            return MoreTypes.hashCode(this);
        }

        public String toString() {
            return MoreTypes.toString(this);
        }
    }

    public static class GenericArrayTypeImpl implements GenericArrayType, Serializable, CompositeType {
        private static final long serialVersionUID = 0;
        private final Type componentType;

        public GenericArrayTypeImpl(Type componentType2) {
            this.componentType = MoreTypes.canonicalize(componentType2);
        }

        public Type getGenericComponentType() {
            return this.componentType;
        }

        public boolean isFullySpecified() {
            return MoreTypes.isFullySpecified(this.componentType);
        }

        public boolean equals(Object o) {
            return (o instanceof GenericArrayType) && MoreTypes.equals(this, (GenericArrayType) o);
        }

        public int hashCode() {
            return MoreTypes.hashCode(this);
        }

        public String toString() {
            return MoreTypes.toString(this);
        }
    }

    public static class WildcardTypeImpl implements WildcardType, Serializable, CompositeType {
        private static final long serialVersionUID = 0;
        private final Type lowerBound;
        private final Type upperBound;

        public WildcardTypeImpl(Type[] upperBounds, Type[] lowerBounds) {
            boolean z;
            boolean z2;
            Preconditions.checkArgument(lowerBounds.length <= 1, "Must have at most one lower bound.");
            if (upperBounds.length == 1) {
                z = true;
            } else {
                z = false;
            }
            Preconditions.checkArgument(z, "Must have exactly one upper bound.");
            if (lowerBounds.length == 1) {
                Preconditions.checkNotNull(lowerBounds[0], "lowerBound");
                MoreTypes.checkNotPrimitive(lowerBounds[0], "wildcard bounds");
                if (upperBounds[0] == Object.class) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                Preconditions.checkArgument(z2, "bounded both ways");
                this.lowerBound = MoreTypes.canonicalize(lowerBounds[0]);
                this.upperBound = Object.class;
                return;
            }
            Preconditions.checkNotNull(upperBounds[0], "upperBound");
            MoreTypes.checkNotPrimitive(upperBounds[0], "wildcard bounds");
            this.lowerBound = null;
            this.upperBound = MoreTypes.canonicalize(upperBounds[0]);
        }

        public Type[] getUpperBounds() {
            return new Type[]{this.upperBound};
        }

        public Type[] getLowerBounds() {
            if (this.lowerBound == null) {
                return MoreTypes.EMPTY_TYPE_ARRAY;
            }
            return new Type[]{this.lowerBound};
        }

        public boolean isFullySpecified() {
            return MoreTypes.isFullySpecified(this.upperBound) && (this.lowerBound == null || MoreTypes.isFullySpecified(this.lowerBound));
        }

        public boolean equals(Object other) {
            return (other instanceof WildcardType) && MoreTypes.equals(this, (WildcardType) other);
        }

        public int hashCode() {
            return MoreTypes.hashCode(this);
        }

        public String toString() {
            return MoreTypes.toString(this);
        }
    }

    /* access modifiers changed from: private */
    public static void checkNotPrimitive(Type type, String use) {
        boolean z;
        if (!(type instanceof Class) || !((Class) type).isPrimitive()) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkArgument(z, "Primitive types are not allowed in %s: %s", use, type);
    }

    public static class MemberImpl implements Member, Serializable {
        private final Class<?> declaringClass;
        private final String memberKey;
        /* access modifiers changed from: private */
        public final Class<? extends Member> memberType;
        private final int modifiers;
        private final String name;
        private final boolean synthetic;

        private MemberImpl(Member member) {
            this.declaringClass = member.getDeclaringClass();
            this.name = member.getName();
            this.modifiers = member.getModifiers();
            this.synthetic = member.isSynthetic();
            this.memberType = MoreTypes.memberType(member);
            this.memberKey = MoreTypes.memberKey(member);
        }

        public Class getDeclaringClass() {
            return this.declaringClass;
        }

        public String getName() {
            return this.name;
        }

        public int getModifiers() {
            return this.modifiers;
        }

        public boolean isSynthetic() {
            return this.synthetic;
        }

        public String toString() {
            return MoreTypes.toString(this);
        }
    }
}
