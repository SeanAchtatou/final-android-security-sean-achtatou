package com.google.inject.internal;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

public abstract class ImmutableCollection<E> implements Collection<E>, Serializable {
    /* access modifiers changed from: private */
    public static final Object[] EMPTY_ARRAY = new Object[0];
    static final ImmutableCollection<Object> EMPTY_IMMUTABLE_COLLECTION = new EmptyImmutableCollection();
    /* access modifiers changed from: private */
    public static final UnmodifiableIterator<Object> EMPTY_ITERATOR = new UnmodifiableIterator<Object>() {
        public boolean hasNext() {
            return false;
        }

        public Object next() {
            throw new NoSuchElementException();
        }
    };

    public abstract UnmodifiableIterator<E> iterator();

    ImmutableCollection() {
    }

    public Object[] toArray() {
        return toArray(new Object[size()]);
    }

    public <T> T[] toArray(T[] other) {
        int size = size();
        if (other.length < size) {
            other = ObjectArrays.newArray(other, size);
        } else if (other.length > size) {
            other[size] = null;
        }
        int index = 0;
        Iterator i$ = iterator();
        while (i$.hasNext()) {
            other[index] = i$.next();
            index++;
        }
        return other;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: E
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean contains(@com.google.inject.internal.Nullable java.lang.Object r5) {
        /*
            r4 = this;
            r3 = 0
            if (r5 != 0) goto L_0x0005
            r2 = r3
        L_0x0004:
            return r2
        L_0x0005:
            java.util.Iterator r1 = r4.iterator()
        L_0x0009:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x001b
            java.lang.Object r0 = r1.next()
            boolean r2 = r0.equals(r5)
            if (r2 == 0) goto L_0x0009
            r2 = 1
            goto L_0x0004
        L_0x001b:
            r2 = r3
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.ImmutableCollection.contains(java.lang.Object):boolean");
    }

    public boolean containsAll(Collection<?> targets) {
        for (Object target : targets) {
            if (!contains(target)) {
                return false;
            }
        }
        return true;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(size() * 16);
        sb.append('[');
        Iterator<E> i = iterator();
        if (i.hasNext()) {
            sb.append((Object) i.next());
        }
        while (i.hasNext()) {
            sb.append(", ");
            sb.append((Object) i.next());
        }
        return sb.append(']').toString();
    }

    public final boolean add(E e) {
        throw new UnsupportedOperationException();
    }

    public final boolean remove(Object object) {
        throw new UnsupportedOperationException();
    }

    public final boolean addAll(Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    public final boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    public final boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    public final void clear() {
        throw new UnsupportedOperationException();
    }

    private static class EmptyImmutableCollection extends ImmutableCollection<Object> {
        private EmptyImmutableCollection() {
        }

        public int size() {
            return 0;
        }

        public boolean isEmpty() {
            return true;
        }

        public boolean contains(@Nullable Object object) {
            return false;
        }

        public UnmodifiableIterator<Object> iterator() {
            return ImmutableCollection.EMPTY_ITERATOR;
        }

        public Object[] toArray() {
            return ImmutableCollection.EMPTY_ARRAY;
        }

        public <T> T[] toArray(T[] array) {
            if (array.length > 0) {
                array[0] = null;
            }
            return array;
        }
    }

    private static class ArrayImmutableCollection<E> extends ImmutableCollection<E> {
        /* access modifiers changed from: private */
        public final E[] elements;

        ArrayImmutableCollection(E[] elements2) {
            this.elements = elements2;
        }

        public int size() {
            return this.elements.length;
        }

        public boolean isEmpty() {
            return false;
        }

        public UnmodifiableIterator<E> iterator() {
            return new UnmodifiableIterator<E>() {
                int i = 0;

                public boolean hasNext() {
                    return this.i < ArrayImmutableCollection.this.elements.length;
                }

                public E next() {
                    if (!hasNext()) {
                        throw new NoSuchElementException();
                    }
                    E[] access$300 = ArrayImmutableCollection.this.elements;
                    int i2 = this.i;
                    this.i = i2 + 1;
                    return access$300[i2];
                }
            };
        }
    }

    private static class SerializedForm implements Serializable {
        private static final long serialVersionUID = 0;
        final Object[] elements;

        SerializedForm(Object[] elements2) {
            this.elements = elements2;
        }

        /* Debug info: failed to restart local var, previous not found, register: 2 */
        /* access modifiers changed from: package-private */
        public Object readResolve() {
            return this.elements.length == 0 ? ImmutableCollection.EMPTY_IMMUTABLE_COLLECTION : new ArrayImmutableCollection((Object[]) this.elements.clone());
        }
    }

    /* access modifiers changed from: package-private */
    public Object writeReplace() {
        return new SerializedForm(toArray());
    }
}
