package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.Key;
import com.google.inject.Module;
import com.google.inject.Provider;
import com.google.inject.Provides;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.Message;
import com.google.inject.util.Modules;
import java.lang.annotation.Annotation;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.List;

public final class ProviderMethodsModule implements Module {
    private final Object delegate;
    private final TypeLiteral<?> typeLiteral = TypeLiteral.get((Class) this.delegate.getClass());

    private ProviderMethodsModule(Object delegate2) {
        this.delegate = Preconditions.checkNotNull(delegate2, "delegate");
    }

    public static Module forModule(Module module) {
        return forObject(module);
    }

    public static Module forObject(Object object) {
        if (object instanceof ProviderMethodsModule) {
            return Modules.EMPTY_MODULE;
        }
        return new ProviderMethodsModule(object);
    }

    public synchronized void configure(Binder binder) {
        for (ProviderMethod<?> providerMethod : getProviderMethods(binder)) {
            providerMethod.configure(binder);
        }
    }

    public List<ProviderMethod<?>> getProviderMethods(Binder binder) {
        List<ProviderMethod<?>> result = Lists.newArrayList();
        for (Class<?> c = this.delegate.getClass(); c != Object.class; c = c.getSuperclass()) {
            for (Method method : c.getDeclaredMethods()) {
                if (method.isAnnotationPresent(Provides.class)) {
                    result.add(createProviderMethod(binder, method));
                }
            }
        }
        return result;
    }

    /* access modifiers changed from: package-private */
    public <T> ProviderMethod<T> createProviderMethod(Binder binder, Method method) {
        Binder binder2 = binder.withSource(method);
        Errors errors = new Errors(method);
        List<Dependency<?>> dependencies = Lists.newArrayList();
        List<Provider<?>> parameterProviders = Lists.newArrayList();
        List<TypeLiteral<?>> parameterTypes = this.typeLiteral.getParameterTypes(method);
        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        for (int i = 0; i < parameterTypes.size(); i++) {
            Key<?> key = getKey(errors, parameterTypes.get(i), method, parameterAnnotations[i]);
            dependencies.add(Dependency.get(key));
            parameterProviders.add(binder2.getProvider(key));
        }
        Key<T> key2 = getKey(errors, this.typeLiteral.getReturnType(method), method, method.getAnnotations());
        Class<? extends Annotation> scopeAnnotation = Annotations.findScopeAnnotation(errors, method.getAnnotations());
        for (Message message : errors.getMessages()) {
            binder2.addError(message);
        }
        return new ProviderMethod<>(key2, method, this.delegate, ImmutableSet.copyOf(dependencies), parameterProviders, scopeAnnotation);
    }

    /* access modifiers changed from: package-private */
    public <T> Key<T> getKey(Errors errors, TypeLiteral<T> type, Member member, Annotation[] annotations) {
        Annotation bindingAnnotation = Annotations.findBindingAnnotation(errors, member, annotations);
        return bindingAnnotation == null ? Key.get(type) : Key.get(type, bindingAnnotation);
    }

    public boolean equals(Object o) {
        return (o instanceof ProviderMethodsModule) && ((ProviderMethodsModule) o).delegate == this.delegate;
    }

    public int hashCode() {
        return this.delegate.hashCode();
    }
}
