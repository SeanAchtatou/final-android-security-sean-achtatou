package com.google.inject.internal;

import com.google.inject.internal.CustomConcurrentHashMap;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

public final class MapMaker {
    /* access modifiers changed from: private */
    public static final ValueReference<Object, Object> COMPUTING = new ValueReference<Object, Object>() {
        public Object get() {
            return null;
        }

        public ValueReference<Object, Object> copyFor(ReferenceEntry<Object, Object> referenceEntry) {
            throw new AssertionError();
        }

        public Object waitForValue() {
            throw new AssertionError();
        }
    };
    /* access modifiers changed from: private */
    public final CustomConcurrentHashMap.Builder builder = new CustomConcurrentHashMap.Builder();
    /* access modifiers changed from: private */
    public long expirationNanos = 0;
    /* access modifiers changed from: private */
    public Strength keyStrength = Strength.STRONG;
    private boolean useCustomMap;
    /* access modifiers changed from: private */
    public Strength valueStrength = Strength.STRONG;

    private interface ReferenceEntry<K, V> {
        int getHash();

        K getKey();

        ReferenceEntry<K, V> getNext();

        ValueReference<K, V> getValueReference();

        void setValueReference(ValueReference<K, V> valueReference);

        void valueReclaimed();
    }

    private enum Strength {
        WEAK {
            /* access modifiers changed from: package-private */
            public boolean equal(Object a, Object b) {
                return a == b;
            }

            /* access modifiers changed from: package-private */
            public int hash(Object o) {
                return System.identityHashCode(o);
            }

            /* access modifiers changed from: package-private */
            public <K, V> ValueReference<K, V> referenceValue(ReferenceEntry<K, V> entry, V value) {
                return new WeakValueReference(value, entry);
            }

            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> newEntry(CustomConcurrentHashMap.Internals<K, V, ReferenceEntry<K, V>> internals, K key, int hash, ReferenceEntry<K, V> next) {
                return next == null ? new WeakEntry(internals, key, hash) : new LinkedWeakEntry(internals, key, hash, next);
            }

            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> copyEntry(K key, ReferenceEntry<K, V> original, ReferenceEntry<K, V> newNext) {
                WeakEntry<K, V> from = (WeakEntry) original;
                return newNext == null ? new WeakEntry(from.internals, key, from.hash) : new LinkedWeakEntry(from.internals, key, from.hash, newNext);
            }
        },
        SOFT {
            /* access modifiers changed from: package-private */
            public boolean equal(Object a, Object b) {
                return a == b;
            }

            /* access modifiers changed from: package-private */
            public int hash(Object o) {
                return System.identityHashCode(o);
            }

            /* access modifiers changed from: package-private */
            public <K, V> ValueReference<K, V> referenceValue(ReferenceEntry<K, V> entry, V value) {
                return new SoftValueReference(value, entry);
            }

            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> newEntry(CustomConcurrentHashMap.Internals<K, V, ReferenceEntry<K, V>> internals, K key, int hash, ReferenceEntry<K, V> next) {
                return next == null ? new SoftEntry(internals, key, hash) : new LinkedSoftEntry(internals, key, hash, next);
            }

            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> copyEntry(K key, ReferenceEntry<K, V> original, ReferenceEntry<K, V> newNext) {
                SoftEntry<K, V> from = (SoftEntry) original;
                return newNext == null ? new SoftEntry(from.internals, key, from.hash) : new LinkedSoftEntry(from.internals, key, from.hash, newNext);
            }
        },
        STRONG {
            /* access modifiers changed from: package-private */
            public boolean equal(Object a, Object b) {
                return a.equals(b);
            }

            /* access modifiers changed from: package-private */
            public int hash(Object o) {
                return o.hashCode();
            }

            /* access modifiers changed from: package-private */
            public <K, V> ValueReference<K, V> referenceValue(ReferenceEntry<K, V> referenceEntry, V value) {
                return new StrongValueReference(value);
            }

            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> newEntry(CustomConcurrentHashMap.Internals<K, V, ReferenceEntry<K, V>> internals, K key, int hash, ReferenceEntry<K, V> next) {
                return next == null ? new StrongEntry(internals, key, hash) : new LinkedStrongEntry(internals, key, hash, next);
            }

            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> copyEntry(K key, ReferenceEntry<K, V> original, ReferenceEntry<K, V> newNext) {
                StrongEntry<K, V> from = (StrongEntry) original;
                return newNext == null ? new StrongEntry(from.internals, key, from.hash) : new LinkedStrongEntry(from.internals, key, from.hash, newNext);
            }
        };

        /* access modifiers changed from: package-private */
        public abstract <K, V> ReferenceEntry<K, V> copyEntry(K k, ReferenceEntry<K, V> referenceEntry, ReferenceEntry<K, V> referenceEntry2);

        /* access modifiers changed from: package-private */
        public abstract boolean equal(Object obj, Object obj2);

        /* access modifiers changed from: package-private */
        public abstract int hash(Object obj);

        /* access modifiers changed from: package-private */
        public abstract <K, V> ReferenceEntry<K, V> newEntry(CustomConcurrentHashMap.Internals<K, V, ReferenceEntry<K, V>> internals, K k, int i, ReferenceEntry<K, V> referenceEntry);

        /* access modifiers changed from: package-private */
        public abstract <K, V> ValueReference<K, V> referenceValue(ReferenceEntry<K, V> referenceEntry, V v);
    }

    private interface ValueReference<K, V> {
        ValueReference<K, V> copyFor(ReferenceEntry<K, V> referenceEntry);

        V get();

        V waitForValue() throws InterruptedException;
    }

    public MapMaker initialCapacity(int initialCapacity) {
        this.builder.initialCapacity(initialCapacity);
        return this;
    }

    public MapMaker loadFactor(float loadFactor) {
        this.builder.loadFactor(loadFactor);
        return this;
    }

    public MapMaker concurrencyLevel(int concurrencyLevel) {
        this.builder.concurrencyLevel(concurrencyLevel);
        return this;
    }

    public MapMaker weakKeys() {
        return setKeyStrength(Strength.WEAK);
    }

    public MapMaker softKeys() {
        return setKeyStrength(Strength.SOFT);
    }

    private MapMaker setKeyStrength(Strength strength) {
        if (this.keyStrength != Strength.STRONG) {
            throw new IllegalStateException("Key strength was already set to " + this.keyStrength + ".");
        }
        this.keyStrength = strength;
        this.useCustomMap = true;
        return this;
    }

    public MapMaker weakValues() {
        return setValueStrength(Strength.WEAK);
    }

    public MapMaker softValues() {
        return setValueStrength(Strength.SOFT);
    }

    private MapMaker setValueStrength(Strength strength) {
        if (this.valueStrength != Strength.STRONG) {
            throw new IllegalStateException("Value strength was already set to " + this.valueStrength + ".");
        }
        this.valueStrength = strength;
        this.useCustomMap = true;
        return this;
    }

    public MapMaker expiration(long duration, TimeUnit unit) {
        if (this.expirationNanos != 0) {
            throw new IllegalStateException("expiration time of " + this.expirationNanos + " ns was already set");
        } else if (duration <= 0) {
            throw new IllegalArgumentException("invalid duration: " + duration);
        } else {
            this.expirationNanos = unit.toNanos(duration);
            this.useCustomMap = true;
            return this;
        }
    }

    public <K, V> ConcurrentMap<K, V> makeMap() {
        return this.useCustomMap ? new StrategyImpl(this).map : new ConcurrentHashMap(this.builder.initialCapacity, this.builder.loadFactor, this.builder.concurrencyLevel);
    }

    public <K, V> ConcurrentMap<K, V> makeComputingMap(Function<? super K, ? extends V> computer) {
        return new StrategyImpl(this, computer).map;
    }

    private static class StrategyImpl<K, V> implements Serializable, CustomConcurrentHashMap.ComputingStrategy<K, V, ReferenceEntry<K, V>> {
        private static final long serialVersionUID = 0;
        final long expirationNanos;
        CustomConcurrentHashMap.Internals<K, V, ReferenceEntry<K, V>> internals;
        final Strength keyStrength;
        final ConcurrentMap<K, V> map;
        final Strength valueStrength;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.inject.internal.MapMaker.StrategyImpl.compute(java.lang.Object, com.google.inject.internal.MapMaker$ReferenceEntry, com.google.inject.internal.Function):V
         arg types: [java.lang.Object, java.lang.Object, com.google.inject.internal.Function]
         candidates:
          com.google.inject.internal.MapMaker.StrategyImpl.compute(java.lang.Object, java.lang.Object, com.google.inject.internal.Function):java.lang.Object
          com.google.inject.internal.CustomConcurrentHashMap.ComputingStrategy.compute(java.lang.Object, java.lang.Object, com.google.inject.internal.Function):V
          com.google.inject.internal.MapMaker.StrategyImpl.compute(java.lang.Object, com.google.inject.internal.MapMaker$ReferenceEntry, com.google.inject.internal.Function):V */
        public /* bridge */ /* synthetic */ Object compute(Object x0, Object x1, Function x2) {
            return compute(x0, (ReferenceEntry) ((ReferenceEntry) x1), x2);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.inject.internal.MapMaker.StrategyImpl.copyEntry(java.lang.Object, com.google.inject.internal.MapMaker$ReferenceEntry, com.google.inject.internal.MapMaker$ReferenceEntry):com.google.inject.internal.MapMaker$ReferenceEntry<K, V>
         arg types: [java.lang.Object, java.lang.Object, java.lang.Object]
         candidates:
          com.google.inject.internal.MapMaker.StrategyImpl.copyEntry(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
          com.google.inject.internal.CustomConcurrentHashMap.Strategy.copyEntry(java.lang.Object, java.lang.Object, java.lang.Object):E
          com.google.inject.internal.MapMaker.StrategyImpl.copyEntry(java.lang.Object, com.google.inject.internal.MapMaker$ReferenceEntry, com.google.inject.internal.MapMaker$ReferenceEntry):com.google.inject.internal.MapMaker$ReferenceEntry<K, V> */
        public /* bridge */ /* synthetic */ Object copyEntry(Object x0, Object x1, Object x2) {
            return copyEntry(x0, (ReferenceEntry) ((ReferenceEntry) x1), (ReferenceEntry) ((ReferenceEntry) x2));
        }

        public /* bridge */ /* synthetic */ Object getKey(Object x0) {
            return getKey((ReferenceEntry) ((ReferenceEntry) x0));
        }

        public /* bridge */ /* synthetic */ Object getNext(Object x0) {
            return getNext((ReferenceEntry) ((ReferenceEntry) x0));
        }

        public /* bridge */ /* synthetic */ Object getValue(Object x0) {
            return getValue((ReferenceEntry) ((ReferenceEntry) x0));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.inject.internal.MapMaker.StrategyImpl.newEntry(java.lang.Object, int, com.google.inject.internal.MapMaker$ReferenceEntry):com.google.inject.internal.MapMaker$ReferenceEntry<K, V>
         arg types: [java.lang.Object, int, java.lang.Object]
         candidates:
          com.google.inject.internal.MapMaker.StrategyImpl.newEntry(java.lang.Object, int, java.lang.Object):java.lang.Object
          com.google.inject.internal.CustomConcurrentHashMap.Strategy.newEntry(java.lang.Object, int, java.lang.Object):E
          com.google.inject.internal.MapMaker.StrategyImpl.newEntry(java.lang.Object, int, com.google.inject.internal.MapMaker$ReferenceEntry):com.google.inject.internal.MapMaker$ReferenceEntry<K, V> */
        public /* bridge */ /* synthetic */ Object newEntry(Object x0, int x1, Object x2) {
            return newEntry(x0, x1, (ReferenceEntry) ((ReferenceEntry) x2));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.inject.internal.MapMaker.StrategyImpl.setValue(com.google.inject.internal.MapMaker$ReferenceEntry, java.lang.Object):void
         arg types: [java.lang.Object, java.lang.Object]
         candidates:
          com.google.inject.internal.MapMaker.StrategyImpl.setValue(java.lang.Object, java.lang.Object):void
          com.google.inject.internal.CustomConcurrentHashMap.Strategy.setValue(java.lang.Object, java.lang.Object):void
          com.google.inject.internal.MapMaker.StrategyImpl.setValue(com.google.inject.internal.MapMaker$ReferenceEntry, java.lang.Object):void */
        public /* bridge */ /* synthetic */ void setValue(Object x0, Object x1) {
            setValue((ReferenceEntry) ((ReferenceEntry) x0), x1);
        }

        public /* bridge */ /* synthetic */ Object waitForValue(Object x0) throws InterruptedException {
            return waitForValue((ReferenceEntry) ((ReferenceEntry) x0));
        }

        StrategyImpl(MapMaker maker) {
            this.keyStrength = maker.keyStrength;
            this.valueStrength = maker.valueStrength;
            this.expirationNanos = maker.expirationNanos;
            this.map = maker.builder.buildMap(this);
        }

        StrategyImpl(MapMaker maker, Function<? super K, ? extends V> computer) {
            this.keyStrength = maker.keyStrength;
            this.valueStrength = maker.valueStrength;
            this.expirationNanos = maker.expirationNanos;
            this.map = maker.builder.buildComputingMap(this, computer);
        }

        public void setValue(ReferenceEntry<K, V> entry, V value) {
            setValueReference(entry, this.valueStrength.referenceValue(entry, value));
            if (this.expirationNanos > 0) {
                scheduleRemoval(entry.getKey(), value);
            }
        }

        /* access modifiers changed from: package-private */
        public void scheduleRemoval(K key, V value) {
            final WeakReference<K> keyReference = new WeakReference<>(key);
            final WeakReference<V> valueReference = new WeakReference<>(value);
            ExpirationTimer.instance.schedule(new TimerTask() {
                public void run() {
                    K key = keyReference.get();
                    if (key != null) {
                        StrategyImpl.this.map.remove(key, valueReference.get());
                    }
                }
            }, TimeUnit.NANOSECONDS.toMillis(this.expirationNanos));
        }

        public boolean equalKeys(K a, Object b) {
            return this.keyStrength.equal(a, b);
        }

        public boolean equalValues(V a, Object b) {
            return this.valueStrength.equal(a, b);
        }

        public int hashKey(Object key) {
            return this.keyStrength.hash(key);
        }

        public K getKey(ReferenceEntry<K, V> entry) {
            return entry.getKey();
        }

        public int getHash(ReferenceEntry entry) {
            return entry.getHash();
        }

        public ReferenceEntry<K, V> newEntry(K key, int hash, ReferenceEntry<K, V> next) {
            return this.keyStrength.newEntry(this.internals, key, hash, next);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.inject.internal.MapMaker.StrategyImpl.newEntry(java.lang.Object, int, com.google.inject.internal.MapMaker$ReferenceEntry):com.google.inject.internal.MapMaker$ReferenceEntry<K, V>
         arg types: [K, int, com.google.inject.internal.MapMaker$ReferenceEntry<K, V>]
         candidates:
          com.google.inject.internal.MapMaker.StrategyImpl.newEntry(java.lang.Object, int, java.lang.Object):java.lang.Object
          com.google.inject.internal.CustomConcurrentHashMap.Strategy.newEntry(java.lang.Object, int, java.lang.Object):E
          com.google.inject.internal.MapMaker.StrategyImpl.newEntry(java.lang.Object, int, com.google.inject.internal.MapMaker$ReferenceEntry):com.google.inject.internal.MapMaker$ReferenceEntry<K, V> */
        public ReferenceEntry<K, V> copyEntry(K key, ReferenceEntry<K, V> original, ReferenceEntry<K, V> newNext) {
            ValueReference<K, V> valueReference = original.getValueReference();
            if (valueReference == MapMaker.COMPUTING) {
                ReferenceEntry<K, V> newEntry = newEntry((Object) key, original.getHash(), (ReferenceEntry) newNext);
                newEntry.setValueReference(new FutureValueReference(original, newEntry));
                return newEntry;
            }
            ReferenceEntry<K, V> newEntry2 = newEntry((Object) key, original.getHash(), (ReferenceEntry) newNext);
            newEntry2.setValueReference(valueReference.copyFor(newEntry2));
            return newEntry2;
        }

        public V waitForValue(ReferenceEntry<K, V> entry) throws InterruptedException {
            ValueReference<K, V> valueReference = entry.getValueReference();
            if (valueReference == MapMaker.COMPUTING) {
                synchronized (entry) {
                    while (true) {
                        valueReference = entry.getValueReference();
                        if (valueReference != MapMaker.COMPUTING) {
                            break;
                        }
                        entry.wait();
                    }
                }
            }
            return valueReference.waitForValue();
        }

        public V getValue(ReferenceEntry<K, V> entry) {
            return entry.getValueReference().get();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.inject.internal.MapMaker.StrategyImpl.setValue(com.google.inject.internal.MapMaker$ReferenceEntry, java.lang.Object):void
         arg types: [com.google.inject.internal.MapMaker$ReferenceEntry<K, V>, V]
         candidates:
          com.google.inject.internal.MapMaker.StrategyImpl.setValue(java.lang.Object, java.lang.Object):void
          com.google.inject.internal.CustomConcurrentHashMap.Strategy.setValue(java.lang.Object, java.lang.Object):void
          com.google.inject.internal.MapMaker.StrategyImpl.setValue(com.google.inject.internal.MapMaker$ReferenceEntry, java.lang.Object):void */
        public V compute(K key, ReferenceEntry<K, V> entry, Function<? super K, ? extends V> computer) {
            try {
                V value = computer.apply(key);
                if (value == null) {
                    String message = computer + " returned null for key " + ((Object) key) + ".";
                    setValueReference(entry, new NullOutputExceptionReference(message));
                    throw new NullOutputException(message);
                }
                setValue((ReferenceEntry) entry, (Object) value);
                return value;
            } catch (Throwable t) {
                setValueReference(entry, new ComputationExceptionReference(t));
                throw new ComputationException(t);
            }
        }

        /* access modifiers changed from: package-private */
        public void setValueReference(ReferenceEntry<K, V> entry, ValueReference<K, V> valueReference) {
            boolean notifyOthers = entry.getValueReference() == MapMaker.COMPUTING;
            entry.setValueReference(valueReference);
            if (notifyOthers) {
                synchronized (entry) {
                    entry.notifyAll();
                }
            }
        }

        private class FutureValueReference implements ValueReference<K, V> {
            final ReferenceEntry<K, V> newEntry;
            final ReferenceEntry<K, V> original;

            FutureValueReference(ReferenceEntry<K, V> original2, ReferenceEntry<K, V> newEntry2) {
                this.original = original2;
                this.newEntry = newEntry2;
            }

            public V get() {
                boolean success = false;
                try {
                    success = true;
                    return this.original.getValueReference().get();
                } finally {
                    if (!success) {
                        removeEntry();
                    }
                }
            }

            public ValueReference<K, V> copyFor(ReferenceEntry<K, V> entry) {
                return new FutureValueReference(this.original, entry);
            }

            public V waitForValue() throws InterruptedException {
                boolean success = false;
                try {
                    success = true;
                    return StrategyImpl.this.waitForValue((ReferenceEntry) this.original);
                } finally {
                    if (!success) {
                        removeEntry();
                    }
                }
            }

            /* access modifiers changed from: package-private */
            public void removeEntry() {
                StrategyImpl.this.internals.removeEntry(this.newEntry);
            }
        }

        public ReferenceEntry<K, V> getNext(ReferenceEntry<K, V> entry) {
            return entry.getNext();
        }

        public void setInternals(CustomConcurrentHashMap.Internals<K, V, ReferenceEntry<K, V>> internals2) {
            this.internals = internals2;
        }

        private void writeObject(ObjectOutputStream out) throws IOException {
            out.writeObject(this.keyStrength);
            out.writeObject(this.valueStrength);
            out.writeLong(this.expirationNanos);
            out.writeObject(this.internals);
            out.writeObject(this.map);
        }

        private static class Fields {
            static final Field expirationNanos = findField("expirationNanos");
            static final Field internals = findField("internals");
            static final Field keyStrength = findField("keyStrength");
            static final Field map = findField("map");
            static final Field valueStrength = findField("valueStrength");

            private Fields() {
            }

            static Field findField(String name) {
                try {
                    Field f = StrategyImpl.class.getDeclaredField(name);
                    f.setAccessible(true);
                    return f;
                } catch (NoSuchFieldException e) {
                    throw new AssertionError(e);
                }
            }
        }

        private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
            try {
                Fields.keyStrength.set(this, in.readObject());
                Fields.valueStrength.set(this, in.readObject());
                Fields.expirationNanos.set(this, Long.valueOf(in.readLong()));
                Fields.internals.set(this, in.readObject());
                Fields.map.set(this, in.readObject());
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            }
        }
    }

    /* access modifiers changed from: private */
    public static <K, V> ValueReference<K, V> computing() {
        return COMPUTING;
    }

    private static class NullOutputExceptionReference<K, V> implements ValueReference<K, V> {
        final String message;

        NullOutputExceptionReference(String message2) {
            this.message = message2;
        }

        public V get() {
            return null;
        }

        public ValueReference<K, V> copyFor(ReferenceEntry<K, V> referenceEntry) {
            return this;
        }

        public V waitForValue() {
            throw new NullOutputException(this.message);
        }
    }

    private static class ComputationExceptionReference<K, V> implements ValueReference<K, V> {
        final Throwable t;

        ComputationExceptionReference(Throwable t2) {
            this.t = t2;
        }

        public V get() {
            return null;
        }

        public ValueReference<K, V> copyFor(ReferenceEntry<K, V> referenceEntry) {
            return this;
        }

        public V waitForValue() {
            throw new AsynchronousComputationException(this.t);
        }
    }

    private static class QueueHolder {
        static final FinalizableReferenceQueue queue = new FinalizableReferenceQueue();

        private QueueHolder() {
        }
    }

    private static class StrongEntry<K, V> implements ReferenceEntry<K, V> {
        final int hash;
        final CustomConcurrentHashMap.Internals<K, V, ReferenceEntry<K, V>> internals;
        final K key;
        volatile ValueReference<K, V> valueReference = MapMaker.computing();

        StrongEntry(CustomConcurrentHashMap.Internals<K, V, ReferenceEntry<K, V>> internals2, K key2, int hash2) {
            this.internals = internals2;
            this.key = key2;
            this.hash = hash2;
        }

        public K getKey() {
            return this.key;
        }

        public ValueReference<K, V> getValueReference() {
            return this.valueReference;
        }

        public void setValueReference(ValueReference<K, V> valueReference2) {
            this.valueReference = valueReference2;
        }

        public void valueReclaimed() {
            this.internals.removeEntry(this, null);
        }

        public ReferenceEntry<K, V> getNext() {
            return null;
        }

        public int getHash() {
            return this.hash;
        }
    }

    private static class LinkedStrongEntry<K, V> extends StrongEntry<K, V> {
        final ReferenceEntry<K, V> next;

        LinkedStrongEntry(CustomConcurrentHashMap.Internals<K, V, ReferenceEntry<K, V>> internals, K key, int hash, ReferenceEntry<K, V> next2) {
            super(internals, key, hash);
            this.next = next2;
        }

        public ReferenceEntry<K, V> getNext() {
            return this.next;
        }
    }

    private static class SoftEntry<K, V> extends FinalizableSoftReference<K> implements ReferenceEntry<K, V> {
        final int hash;
        final CustomConcurrentHashMap.Internals<K, V, ReferenceEntry<K, V>> internals;
        volatile ValueReference<K, V> valueReference = MapMaker.computing();

        SoftEntry(CustomConcurrentHashMap.Internals<K, V, ReferenceEntry<K, V>> internals2, K key, int hash2) {
            super(key, QueueHolder.queue);
            this.internals = internals2;
            this.hash = hash2;
        }

        public K getKey() {
            return get();
        }

        public void finalizeReferent() {
            this.internals.removeEntry(this);
        }

        public ValueReference<K, V> getValueReference() {
            return this.valueReference;
        }

        public void setValueReference(ValueReference<K, V> valueReference2) {
            this.valueReference = valueReference2;
        }

        public void valueReclaimed() {
            this.internals.removeEntry(this, null);
        }

        public ReferenceEntry<K, V> getNext() {
            return null;
        }

        public int getHash() {
            return this.hash;
        }
    }

    private static class LinkedSoftEntry<K, V> extends SoftEntry<K, V> {
        final ReferenceEntry<K, V> next;

        LinkedSoftEntry(CustomConcurrentHashMap.Internals<K, V, ReferenceEntry<K, V>> internals, K key, int hash, ReferenceEntry<K, V> next2) {
            super(internals, key, hash);
            this.next = next2;
        }

        public ReferenceEntry<K, V> getNext() {
            return this.next;
        }
    }

    private static class WeakEntry<K, V> extends FinalizableWeakReference<K> implements ReferenceEntry<K, V> {
        final int hash;
        final CustomConcurrentHashMap.Internals<K, V, ReferenceEntry<K, V>> internals;
        volatile ValueReference<K, V> valueReference = MapMaker.computing();

        WeakEntry(CustomConcurrentHashMap.Internals<K, V, ReferenceEntry<K, V>> internals2, K key, int hash2) {
            super(key, QueueHolder.queue);
            this.internals = internals2;
            this.hash = hash2;
        }

        public K getKey() {
            return get();
        }

        public void finalizeReferent() {
            this.internals.removeEntry(this);
        }

        public ValueReference<K, V> getValueReference() {
            return this.valueReference;
        }

        public void setValueReference(ValueReference<K, V> valueReference2) {
            this.valueReference = valueReference2;
        }

        public void valueReclaimed() {
            this.internals.removeEntry(this, null);
        }

        public ReferenceEntry<K, V> getNext() {
            return null;
        }

        public int getHash() {
            return this.hash;
        }
    }

    private static class LinkedWeakEntry<K, V> extends WeakEntry<K, V> {
        final ReferenceEntry<K, V> next;

        LinkedWeakEntry(CustomConcurrentHashMap.Internals<K, V, ReferenceEntry<K, V>> internals, K key, int hash, ReferenceEntry<K, V> next2) {
            super(internals, key, hash);
            this.next = next2;
        }

        public ReferenceEntry<K, V> getNext() {
            return this.next;
        }
    }

    private static class WeakValueReference<K, V> extends FinalizableWeakReference<V> implements ValueReference<K, V> {
        final ReferenceEntry<K, V> entry;

        WeakValueReference(V referent, ReferenceEntry<K, V> entry2) {
            super(referent, QueueHolder.queue);
            this.entry = entry2;
        }

        public void finalizeReferent() {
            this.entry.valueReclaimed();
        }

        public ValueReference<K, V> copyFor(ReferenceEntry<K, V> entry2) {
            return new WeakValueReference(get(), entry2);
        }

        public V waitForValue() throws InterruptedException {
            return get();
        }
    }

    private static class SoftValueReference<K, V> extends FinalizableSoftReference<V> implements ValueReference<K, V> {
        final ReferenceEntry<K, V> entry;

        SoftValueReference(V referent, ReferenceEntry<K, V> entry2) {
            super(referent, QueueHolder.queue);
            this.entry = entry2;
        }

        public void finalizeReferent() {
            this.entry.valueReclaimed();
        }

        public ValueReference<K, V> copyFor(ReferenceEntry<K, V> entry2) {
            return new SoftValueReference(get(), entry2);
        }

        public V waitForValue() {
            return get();
        }
    }

    private static class StrongValueReference<K, V> implements ValueReference<K, V> {
        final V referent;

        StrongValueReference(V referent2) {
            this.referent = referent2;
        }

        public V get() {
            return this.referent;
        }

        public ValueReference<K, V> copyFor(ReferenceEntry<K, V> referenceEntry) {
            return this;
        }

        public V waitForValue() {
            return get();
        }
    }
}
