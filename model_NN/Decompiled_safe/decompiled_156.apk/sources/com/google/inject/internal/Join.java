package com.google.inject.internal;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

public final class Join {
    private Join() {
    }

    public static String join(String delimiter, Iterable<?> tokens) {
        return join(delimiter, tokens.iterator());
    }

    public static String join(String delimiter, Object[] tokens) {
        return join(delimiter, Arrays.asList(tokens));
    }

    public static String join(String delimiter, @Nullable Object firstToken, Object... otherTokens) {
        Preconditions.checkNotNull(otherTokens);
        return join(delimiter, Lists.newArrayList(firstToken, otherTokens));
    }

    public static String join(String delimiter, Iterator<?> tokens) {
        StringBuilder sb = new StringBuilder();
        join(sb, delimiter, tokens);
        return sb.toString();
    }

    public static String join(String keyValueSeparator, String entryDelimiter, Map<?, ?> map) {
        return ((StringBuilder) join(new StringBuilder(), keyValueSeparator, entryDelimiter, map)).toString();
    }

    public static <T extends Appendable> T join(T appendable, String delimiter, Iterable<?> tokens) {
        return join(appendable, delimiter, tokens.iterator());
    }

    public static <T extends Appendable> T join(T appendable, String delimiter, Object[] tokens) {
        return join(appendable, delimiter, Arrays.asList(tokens));
    }

    public static <T extends Appendable> T join(T appendable, String delimiter, @Nullable Object firstToken, Object... otherTokens) {
        Preconditions.checkNotNull(otherTokens);
        return join(appendable, delimiter, Lists.newArrayList(firstToken, otherTokens));
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static <T extends java.lang.Appendable> T join(T r3, java.lang.String r4, java.util.Iterator<?> r5) {
        /*
            com.google.inject.internal.Preconditions.checkNotNull(r3)
            com.google.inject.internal.Preconditions.checkNotNull(r4)
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L_0x002d
            java.lang.Object r1 = r5.next()     // Catch:{ IOException -> 0x0024 }
            appendOneToken(r3, r1)     // Catch:{ IOException -> 0x0024 }
        L_0x0013:
            boolean r1 = r5.hasNext()     // Catch:{ IOException -> 0x0024 }
            if (r1 == 0) goto L_0x002d
            r3.append(r4)     // Catch:{ IOException -> 0x0024 }
            java.lang.Object r1 = r5.next()     // Catch:{ IOException -> 0x0024 }
            appendOneToken(r3, r1)     // Catch:{ IOException -> 0x0024 }
            goto L_0x0013
        L_0x0024:
            r1 = move-exception
            r0 = r1
            com.google.inject.internal.Join$JoinException r1 = new com.google.inject.internal.Join$JoinException
            r2 = 0
            r1.<init>(r0)
            throw r1
        L_0x002d:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.Join.join(java.lang.Appendable, java.lang.String, java.util.Iterator):java.lang.Appendable");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static <T extends java.lang.Appendable> T join(T r4, java.lang.String r5, java.lang.String r6, java.util.Map<?, ?> r7) {
        /*
            com.google.inject.internal.Preconditions.checkNotNull(r4)
            com.google.inject.internal.Preconditions.checkNotNull(r5)
            com.google.inject.internal.Preconditions.checkNotNull(r6)
            java.util.Set r2 = r7.entrySet()
            java.util.Iterator r1 = r2.iterator()
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x003c
            java.lang.Object r2 = r1.next()     // Catch:{ IOException -> 0x0033 }
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2     // Catch:{ IOException -> 0x0033 }
            appendOneEntry(r4, r5, r2)     // Catch:{ IOException -> 0x0033 }
        L_0x0020:
            boolean r2 = r1.hasNext()     // Catch:{ IOException -> 0x0033 }
            if (r2 == 0) goto L_0x003c
            r4.append(r6)     // Catch:{ IOException -> 0x0033 }
            java.lang.Object r2 = r1.next()     // Catch:{ IOException -> 0x0033 }
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2     // Catch:{ IOException -> 0x0033 }
            appendOneEntry(r4, r5, r2)     // Catch:{ IOException -> 0x0033 }
            goto L_0x0020
        L_0x0033:
            r2 = move-exception
            r0 = r2
            com.google.inject.internal.Join$JoinException r2 = new com.google.inject.internal.Join$JoinException
            r3 = 0
            r2.<init>(r0)
            throw r2
        L_0x003c:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.Join.join(java.lang.Appendable, java.lang.String, java.lang.String, java.util.Map):java.lang.Appendable");
    }

    private static void appendOneEntry(Appendable appendable, String keyValueSeparator, Map.Entry<?, ?> entry) throws IOException {
        appendOneToken(appendable, entry.getKey());
        appendable.append(keyValueSeparator);
        appendOneToken(appendable, entry.getValue());
    }

    private static void appendOneToken(Appendable appendable, Object token) throws IOException {
        appendable.append(toCharSequence(token));
    }

    private static CharSequence toCharSequence(Object token) {
        return token instanceof CharSequence ? (CharSequence) token : String.valueOf(token);
    }

    public static class JoinException extends RuntimeException {
        private static final long serialVersionUID = 1;

        private JoinException(IOException cause) {
            super(cause);
        }
    }
}
