package com.google.inject.internal;

import com.google.inject.ConfigurationException;
import com.google.inject.CreationException;
import com.google.inject.Key;
import com.google.inject.MembersInjector;
import com.google.inject.Provider;
import com.google.inject.ProvisionException;
import com.google.inject.Scope;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.InjectionPoint;
import com.google.inject.spi.Message;
import com.google.inject.spi.TypeListenerBinding;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Formatter;
import java.util.List;

public final class Errors implements Serializable {
    private static final String CONSTRUCTOR_RULES = "Classes must have either one (and only one) constructor annotated with @Inject or a zero-argument constructor that is not private.";
    private static final Collection<Converter<?>> converters = ImmutableList.of(new Converter<Class>(Class.class) {
        public String toString(Class c) {
            return c.getName();
        }
    }, new Converter<Member>(Member.class) {
        public String toString(Member member) {
            return MoreTypes.toString(member);
        }
    }, new Converter<Key>(Key.class) {
        public String toString(Key key) {
            if (key.getAnnotationType() == null) {
                return key.getTypeLiteral().toString();
            }
            return key.getTypeLiteral() + " annotated with " + (key.getAnnotation() != null ? key.getAnnotation() : key.getAnnotationType());
        }
    });
    private List<Message> errors;
    private final Errors parent;
    private final Errors root;
    private final Object source;

    public Errors() {
        this.root = this;
        this.parent = null;
        this.source = SourceProvider.UNKNOWN_SOURCE;
    }

    public Errors(Object source2) {
        this.root = this;
        this.parent = null;
        this.source = source2;
    }

    private Errors(Errors parent2, Object source2) {
        this.root = parent2.root;
        this.parent = parent2;
        this.source = source2;
    }

    public Errors withSource(Object source2) {
        return source2 == SourceProvider.UNKNOWN_SOURCE ? this : new Errors(this, source2);
    }

    public Errors missingImplementation(Key key) {
        return addMessage("No implementation for %s was bound.", key);
    }

    public Errors converterReturnedNull(String stringValue, Object source2, TypeLiteral<?> type, MatcherAndConverter matchingConverter) {
        return addMessage("Received null converting '%s' (bound at %s) to %s%n using %s.", stringValue, convert(source2), type, matchingConverter);
    }

    public Errors conversionTypeError(String stringValue, Object source2, TypeLiteral<?> type, MatcherAndConverter matchingConverter, Object converted) {
        return addMessage("Type mismatch converting '%s' (bound at %s) to %s%n using %s.%n Converter returned %s.", stringValue, convert(source2), type, matchingConverter, converted);
    }

    public Errors conversionError(String stringValue, Object source2, TypeLiteral<?> type, MatcherAndConverter matchingConverter, RuntimeException cause) {
        return errorInUserCode(cause, "Error converting '%s' (bound at %s) to %s%n using %s.%n Reason: %s", stringValue, convert(source2), type, matchingConverter, cause);
    }

    public Errors ambiguousTypeConversion(String stringValue, Object source2, TypeLiteral<?> type, MatcherAndConverter a, MatcherAndConverter b) {
        return addMessage("Multiple converters can convert '%s' (bound at %s) to %s:%n %s and%n %s.%n Please adjust your type converter configuration to avoid overlapping matches.", stringValue, convert(source2), type, a, b);
    }

    public Errors bindingToProvider() {
        return addMessage("Binding to Provider is not allowed.", new Object[0]);
    }

    public Errors subtypeNotProvided(Class<? extends Provider<?>> providerType, Class<?> type) {
        return addMessage("%s doesn't provide instances of %s.", providerType, type);
    }

    public Errors notASubtype(Class<?> implementationType, Class<?> type) {
        return addMessage("%s doesn't extend %s.", implementationType, type);
    }

    public Errors recursiveImplementationType() {
        return addMessage("@ImplementedBy points to the same class it annotates.", new Object[0]);
    }

    public Errors recursiveProviderType() {
        return addMessage("@ProvidedBy points to the same class it annotates.", new Object[0]);
    }

    public Errors missingRuntimeRetention(Object source2) {
        return addMessage("Please annotate with @Retention(RUNTIME).%n Bound at %s.", convert(source2));
    }

    public Errors missingScopeAnnotation() {
        return addMessage("Please annotate with @ScopeAnnotation.", new Object[0]);
    }

    public Errors optionalConstructor(Constructor constructor) {
        return addMessage("%s is annotated @Inject(optional=true), but constructors cannot be optional.", constructor);
    }

    public Errors cannotBindToGuiceType(String simpleName) {
        return addMessage("Binding to core guice framework type is not allowed: %s.", simpleName);
    }

    public Errors scopeNotFound(Class<? extends Annotation> scopeAnnotation) {
        return addMessage("No scope is bound to %s.", scopeAnnotation);
    }

    public Errors scopeAnnotationOnAbstractType(Class<? extends Annotation> scopeAnnotation, Class<?> type, Object source2) {
        return addMessage("%s is annotated with %s, but scope annotations are not supported for abstract types.%n Bound at %s.", type, scopeAnnotation, convert(source2));
    }

    public Errors misplacedBindingAnnotation(Member member, Annotation bindingAnnotation) {
        return addMessage("%s is annotated with %s, but binding annotations should be applied to its parameters instead.", member, bindingAnnotation);
    }

    public Errors missingConstructor(Class<?> implementation) {
        return addMessage("Could not find a suitable constructor in %s. Classes must have either one (and only one) constructor annotated with @Inject or a zero-argument constructor that is not private.", implementation);
    }

    public Errors tooManyConstructors(Class<?> implementation) {
        return addMessage("%s has more than one constructor annotated with @Inject. Classes must have either one (and only one) constructor annotated with @Inject or a zero-argument constructor that is not private.", implementation);
    }

    public Errors duplicateScopes(Scope existing, Class<? extends Annotation> annotationType, Scope scope) {
        return addMessage("Scope %s is already bound to %s. Cannot bind %s.", existing, annotationType, scope);
    }

    public Errors voidProviderMethod() {
        return addMessage("Provider methods must return a value. Do not return void.", new Object[0]);
    }

    public Errors missingConstantValues() {
        return addMessage("Missing constant value. Please call to(...).", new Object[0]);
    }

    public Errors cannotInjectInnerClass(Class<?> type) {
        return addMessage("Injecting into inner classes is not supported.  Please use a 'static' class (top-level or nested) instead of %s.", type);
    }

    public Errors duplicateBindingAnnotations(Member member, Class<? extends Annotation> a, Class<? extends Annotation> b) {
        return addMessage("%s has more than one annotation annotated with @BindingAnnotation: %s and %s", member, a, b);
    }

    public Errors duplicateScopeAnnotations(Class<? extends Annotation> a, Class<? extends Annotation> b) {
        return addMessage("More than one scope annotation was found: %s and %s.", a, b);
    }

    public Errors recursiveBinding() {
        return addMessage("Binding points to itself.", new Object[0]);
    }

    public Errors bindingAlreadySet(Key<?> key, Object source2) {
        return addMessage("A binding to %s was already configured at %s.", key, convert(source2));
    }

    public Errors childBindingAlreadySet(Key<?> key) {
        return addMessage("A binding to %s already exists on a child injector.", key);
    }

    public Errors errorInjectingMethod(Throwable cause) {
        return errorInUserCode(cause, "Error injecting method, %s", cause);
    }

    public Errors errorNotifyingTypeListener(TypeListenerBinding listener, TypeLiteral<?> type, Throwable cause) {
        return errorInUserCode(cause, "Error notifying TypeListener %s (bound at %s) of %s.%n Reason: %s", listener.getListener(), convert(listener.getSource()), type, cause);
    }

    public Errors errorInjectingConstructor(Throwable cause) {
        return errorInUserCode(cause, "Error injecting constructor, %s", cause);
    }

    public Errors errorInProvider(RuntimeException runtimeException) {
        return errorInUserCode(runtimeException, "Error in custom provider, %s", runtimeException);
    }

    public Errors errorInUserInjector(MembersInjector<?> listener, TypeLiteral<?> type, RuntimeException cause) {
        return errorInUserCode(cause, "Error injecting %s using %s.%n Reason: %s", type, listener, cause);
    }

    public Errors errorNotifyingInjectionListener(InjectionListener<?> listener, TypeLiteral<?> type, RuntimeException cause) {
        return errorInUserCode(cause, "Error notifying InjectionListener %s of %s.%n Reason: %s", listener, type, cause);
    }

    public void exposedButNotBound(Key<?> key) {
        addMessage("Could not expose() %s, it must be explicitly bound.", key);
    }

    public static Collection<Message> getMessagesFromThrowable(Throwable throwable) {
        if (throwable instanceof ProvisionException) {
            return ((ProvisionException) throwable).getErrorMessages();
        }
        if (throwable instanceof ConfigurationException) {
            return ((ConfigurationException) throwable).getErrorMessages();
        }
        if (throwable instanceof CreationException) {
            return ((CreationException) throwable).getErrorMessages();
        }
        return ImmutableSet.of();
    }

    public Errors errorInUserCode(Throwable cause, String messageFormat, Object... arguments) {
        Collection<Message> messages = getMessagesFromThrowable(cause);
        if (!messages.isEmpty()) {
            return merge(messages);
        }
        return addMessage(cause, messageFormat, arguments);
    }

    public Errors cannotInjectRawProvider() {
        return addMessage("Cannot inject a Provider that has no type parameter", new Object[0]);
    }

    public Errors cannotInjectRawMembersInjector() {
        return addMessage("Cannot inject a MembersInjector that has no type parameter", new Object[0]);
    }

    public Errors cannotInjectTypeLiteralOf(Type unsupportedType) {
        return addMessage("Cannot inject a TypeLiteral of %s", unsupportedType);
    }

    public Errors cannotInjectRawTypeLiteral() {
        return addMessage("Cannot inject a TypeLiteral that has no type parameter", new Object[0]);
    }

    public Errors cannotSatisfyCircularDependency(Class<?> expectedType) {
        return addMessage("Tried proxying %s to support a circular dependency, but it is not an interface.", expectedType);
    }

    public void throwCreationExceptionIfErrorsExist() {
        if (hasErrors()) {
            throw new CreationException(getMessages());
        }
    }

    public void throwConfigurationExceptionIfErrorsExist() {
        if (hasErrors()) {
            throw new ConfigurationException(getMessages());
        }
    }

    public void throwProvisionExceptionIfErrorsExist() {
        if (hasErrors()) {
            throw new ProvisionException(getMessages());
        }
    }

    private Message merge(Message message) {
        List<Object> sources = Lists.newArrayList();
        sources.addAll(getSources());
        sources.addAll(message.getSources());
        return new Message(sources, message.getMessage(), message.getCause());
    }

    public Errors merge(Collection<Message> messages) {
        for (Message message : messages) {
            addMessage(merge(message));
        }
        return this;
    }

    public Errors merge(Errors moreErrors) {
        if (!(moreErrors.root == this.root || moreErrors.root.errors == null)) {
            merge(moreErrors.root.errors);
        }
        return this;
    }

    public List<Object> getSources() {
        List<Object> sources = Lists.newArrayList();
        for (Errors e = this; e != null; e = e.parent) {
            if (e.source != SourceProvider.UNKNOWN_SOURCE) {
                sources.add(0, e.source);
            }
        }
        return sources;
    }

    public void throwIfNewErrors(int expectedSize) throws ErrorsException {
        if (size() != expectedSize) {
            throw toException();
        }
    }

    public ErrorsException toException() {
        return new ErrorsException(this);
    }

    public boolean hasErrors() {
        return this.root.errors != null;
    }

    public Errors addMessage(String messageFormat, Object... arguments) {
        return addMessage(null, messageFormat, arguments);
    }

    private Errors addMessage(Throwable cause, String messageFormat, Object... arguments) {
        addMessage(new Message(getSources(), format(messageFormat, arguments), cause));
        return this;
    }

    public Errors addMessage(Message message) {
        if (this.root.errors == null) {
            this.root.errors = Lists.newArrayList();
        }
        this.root.errors.add(message);
        return this;
    }

    public static String format(String messageFormat, Object... arguments) {
        for (int i = 0; i < arguments.length; i++) {
            arguments[i] = convert(arguments[i]);
        }
        return String.format(messageFormat, arguments);
    }

    public List<Message> getMessages() {
        if (this.root.errors == null) {
            return ImmutableList.of();
        }
        List<Message> result = Lists.newArrayList(this.root.errors);
        Collections.sort(result, new Comparator<Message>() {
            public int compare(Message a, Message b) {
                return a.getSource().compareTo(b.getSource());
            }
        });
        return result;
    }

    /* JADX INFO: Multiple debug info for r8v11 java.lang.Throwable: [D('cause' java.lang.Throwable), D('dependencies' java.util.List<java.lang.Object>)] */
    /* JADX INFO: Multiple debug info for r1v10 java.io.StringWriter: [D('errorMessage' com.google.inject.spi.Message), D('writer' java.io.StringWriter)] */
    public static String format(String heading, Collection<Message> errorMessages) {
        Formatter fmt = new Formatter().format(heading, new Object[0]).format(":%n%n", new Object[0]);
        int index = 1;
        boolean displayCauses = getOnlyCause(errorMessages) == null;
        for (Message errorMessage : errorMessages) {
            int index2 = index + 1;
            fmt.format("%s) %s%n", Integer.valueOf(index), errorMessage.getMessage());
            List<Object> dependencies = errorMessage.getSources();
            for (int i = dependencies.size() - 1; i >= 0; i--) {
                formatSource(fmt, dependencies.get(i));
            }
            Throwable cause = errorMessage.getCause();
            if (displayCauses && cause != null) {
                StringWriter writer = new StringWriter();
                cause.printStackTrace(new PrintWriter(writer));
                fmt.format("Caused by: %s", writer.getBuffer());
            }
            fmt.format("%n", new Object[0]);
            index = index2;
        }
        if (errorMessages.size() == 1) {
            fmt.format("1 error", new Object[0]);
        } else {
            fmt.format("%s errors", Integer.valueOf(errorMessages.size()));
        }
        return fmt.toString();
    }

    public <T> T checkForNull(T value, Object source2, Dependency<?> dependency) throws ErrorsException {
        if (value != null || dependency.isNullable()) {
            return value;
        }
        int parameterIndex = dependency.getParameterIndex();
        addMessage("null returned by binding at %s%n but %s%s is not @Nullable", source2, parameterIndex != -1 ? "parameter " + parameterIndex + " of " : "", dependency.getInjectionPoint().getMember());
        throw toException();
    }

    public static Throwable getOnlyCause(Collection<Message> messages) {
        Throwable onlyCause = null;
        for (Message message : messages) {
            Throwable messageCause = message.getCause();
            if (messageCause != null) {
                if (onlyCause != null) {
                    return null;
                }
                onlyCause = messageCause;
            }
        }
        return onlyCause;
    }

    public int size() {
        if (this.root.errors == null) {
            return 0;
        }
        return this.root.errors.size();
    }

    private static abstract class Converter<T> {
        final Class<T> type;

        /* access modifiers changed from: package-private */
        public abstract String toString(T t);

        Converter(Class<T> type2) {
            this.type = type2;
        }

        /* access modifiers changed from: package-private */
        public boolean appliesTo(Object o) {
            return this.type.isAssignableFrom(o.getClass());
        }

        /* access modifiers changed from: package-private */
        public String convert(Object o) {
            return toString(this.type.cast(o));
        }
    }

    public static Object convert(Object o) {
        for (Converter<?> converter : converters) {
            if (converter.appliesTo(o)) {
                return converter.convert(o);
            }
        }
        return o;
    }

    public static void formatSource(Formatter formatter, Object source2) {
        if (source2 instanceof Dependency) {
            Dependency dependency = (Dependency) source2;
            InjectionPoint injectionPoint = dependency.getInjectionPoint();
            if (injectionPoint != null) {
                formatInjectionPoint(formatter, dependency, injectionPoint);
            } else {
                formatSource(formatter, dependency.getKey());
            }
        } else if (source2 instanceof InjectionPoint) {
            formatInjectionPoint(formatter, null, (InjectionPoint) source2);
        } else if (source2 instanceof Class) {
            formatter.format("  at %s%n", StackTraceElements.forType((Class) source2));
        } else if (source2 instanceof Member) {
            formatter.format("  at %s%n", StackTraceElements.forMember((Member) source2));
        } else if (source2 instanceof TypeLiteral) {
            formatter.format("  while locating %s%n", source2);
        } else if (source2 instanceof Key) {
            formatter.format("  while locating %s%n", convert((Key) source2));
        } else {
            formatter.format("  at %s%n", source2);
        }
    }

    public static void formatInjectionPoint(Formatter formatter, Dependency<?> dependency, InjectionPoint injectionPoint) {
        Member member = injectionPoint.getMember();
        if (MoreTypes.memberType(member) == Field.class) {
            formatter.format("  while locating %s%n", convert(injectionPoint.getDependencies().get(0).getKey()));
            formatter.format("    for field at %s%n", StackTraceElements.forMember(member));
        } else if (dependency != null) {
            formatter.format("  while locating %s%n", convert(dependency.getKey()));
            formatter.format("    for parameter %s at %s%n", Integer.valueOf(dependency.getParameterIndex()), StackTraceElements.forMember(member));
        } else {
            formatSource(formatter, injectionPoint.getMember());
        }
    }
}
