package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.ConfigurationException;
import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.TypeLiteral;
import com.google.inject.binder.AnnotatedBindingBuilder;
import com.google.inject.spi.Element;
import com.google.inject.spi.InjectionPoint;
import com.google.inject.spi.Message;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Set;

public class BindingBuilder<T> extends AbstractBindingBuilder<T> implements AnnotatedBindingBuilder<T> {
    public BindingBuilder(Binder binder, List<Element> elements, Object source, Key<T> key) {
        super(binder, elements, source, key);
    }

    public BindingBuilder<T> annotatedWith(Class<? extends Annotation> annotationType) {
        annotatedWithInternal(annotationType);
        return this;
    }

    public BindingBuilder<T> annotatedWith(Annotation annotation) {
        annotatedWithInternal(annotation);
        return this;
    }

    public BindingBuilder<T> to(Class<? extends T> implementation) {
        return to((Key) Key.get((Class) implementation));
    }

    public BindingBuilder<T> to(TypeLiteral<? extends T> implementation) {
        return to((Key) Key.get(implementation));
    }

    public BindingBuilder<T> to(Key<? extends T> linkedKey) {
        Preconditions.checkNotNull(linkedKey, "linkedKey");
        checkNotTargetted();
        BindingImpl<T> base = getBinding();
        setBinding(new LinkedBindingImpl(base.getSource(), base.getKey(), base.getScoping(), linkedKey));
        return this;
    }

    public void toInstance(T instance) {
        Set<InjectionPoint> injectionPoints;
        checkNotTargetted();
        if (instance != null) {
            try {
                injectionPoints = InjectionPoint.forInstanceMethodsAndFields(instance.getClass());
            } catch (ConfigurationException e) {
                ConfigurationException e2 = e;
                for (Message message : e2.getErrorMessages()) {
                    this.binder.addError(message);
                }
                injectionPoints = (Set) e2.getPartialValue();
            }
        } else {
            this.binder.addError(AbstractBindingBuilder.BINDING_TO_NULL, new Object[0]);
            injectionPoints = ImmutableSet.of();
        }
        BindingImpl<T> base = getBinding();
        setBinding(new InstanceBindingImpl(base.getSource(), base.getKey(), base.getScoping(), injectionPoints, instance));
    }

    public BindingBuilder<T> toProvider(Provider<? extends T> provider) {
        Set<InjectionPoint> injectionPoints;
        Preconditions.checkNotNull(provider, "provider");
        checkNotTargetted();
        try {
            injectionPoints = InjectionPoint.forInstanceMethodsAndFields(provider.getClass());
        } catch (ConfigurationException e) {
            ConfigurationException e2 = e;
            for (Message message : e2.getErrorMessages()) {
                this.binder.addError(message);
            }
            injectionPoints = (Set) e2.getPartialValue();
        }
        BindingImpl<T> base = getBinding();
        setBinding(new ProviderInstanceBindingImpl(base.getSource(), base.getKey(), base.getScoping(), injectionPoints, provider));
        return this;
    }

    public BindingBuilder<T> toProvider(Class<? extends Provider<? extends T>> providerType) {
        return toProvider((Key) Key.get((Class) providerType));
    }

    public BindingBuilder<T> toProvider(Key<? extends Provider<? extends T>> providerKey) {
        Preconditions.checkNotNull(providerKey, "providerKey");
        checkNotTargetted();
        BindingImpl<T> base = getBinding();
        setBinding(new LinkedProviderBindingImpl(base.getSource(), base.getKey(), base.getScoping(), providerKey));
        return this;
    }

    public String toString() {
        return "BindingBuilder<" + getBinding().getKey().getTypeLiteral() + ">";
    }
}
