package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.spi.BindingTargetVisitor;
import com.google.inject.spi.LinkedKeyBinding;

public final class LinkedBindingImpl<T> extends BindingImpl<T> implements LinkedKeyBinding<T> {
    final Key<? extends T> targetKey;

    public LinkedBindingImpl(Injector injector, Key<T> key, Object source, InternalFactory<? extends T> internalFactory, Scoping scoping, Key<? extends T> targetKey2) {
        super(injector, key, source, internalFactory, scoping);
        this.targetKey = targetKey2;
    }

    public LinkedBindingImpl(Object source, Key<T> key, Scoping scoping, Key<? extends T> targetKey2) {
        super(source, key, scoping);
        this.targetKey = targetKey2;
    }

    public <V> V acceptTargetVisitor(BindingTargetVisitor<? super T, V> visitor) {
        return visitor.visit(this);
    }

    public Key<? extends T> getLinkedKey() {
        return this.targetKey;
    }

    public BindingImpl<T> withScoping(Scoping scoping) {
        return new LinkedBindingImpl(getSource(), getKey(), scoping, this.targetKey);
    }

    public BindingImpl<T> withKey(Key<T> key) {
        return new LinkedBindingImpl(getSource(), key, getScoping(), this.targetKey);
    }

    public void applyTo(Binder binder) {
        getScoping().applyTo(binder.withSource(getSource()).bind(getKey()).to(getLinkedKey()));
    }

    public String toString() {
        return new ToStringBuilder(LinkedKeyBinding.class).add("key", getKey()).add("source", getSource()).add("scope", getScoping()).add("target", this.targetKey).toString();
    }
}
