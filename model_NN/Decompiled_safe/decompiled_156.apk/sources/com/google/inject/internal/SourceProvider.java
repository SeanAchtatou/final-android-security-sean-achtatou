package com.google.inject.internal;

import java.util.List;

public class SourceProvider {
    public static final SourceProvider DEFAULT_INSTANCE = new SourceProvider(ImmutableSet.of(SourceProvider.class.getName()));
    public static final Object UNKNOWN_SOURCE = "[unknown source]";
    private final ImmutableSet<String> classNamesToSkip;

    public SourceProvider() {
        this.classNamesToSkip = ImmutableSet.of(SourceProvider.class.getName());
    }

    private SourceProvider(Iterable<String> classesToSkip) {
        this.classNamesToSkip = ImmutableSet.copyOf(classesToSkip);
    }

    public SourceProvider plusSkippedClasses(Class... moreClassesToSkip) {
        return new SourceProvider(Iterables.concat(this.classNamesToSkip, asStrings(moreClassesToSkip)));
    }

    private static List<String> asStrings(Class... classes) {
        List<String> strings = Lists.newArrayList();
        for (Class c : classes) {
            strings.add(c.getName());
        }
        return strings;
    }

    public StackTraceElement get() {
        for (StackTraceElement element : new Throwable().getStackTrace()) {
            if (!this.classNamesToSkip.contains(element.getClassName())) {
                return element;
            }
        }
        throw new AssertionError();
    }
}
