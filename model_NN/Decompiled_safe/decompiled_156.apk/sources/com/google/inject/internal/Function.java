package com.google.inject.internal;

public interface Function<F, T> {
    T apply(@Nullable Object obj);

    boolean equals(@Nullable Object obj);
}
