package com.google.inject.internal;

import java.util.Collection;
import java.util.Set;

public final class Collections2 {
    private Collections2() {
    }

    static <E> Collection<E> toCollection(Iterable<E> iterable) {
        return iterable instanceof Collection ? (Collection) iterable : Lists.newArrayList(iterable);
    }

    static boolean setEquals(Set<?> thisSet, @Nullable Object object) {
        if (object == thisSet) {
            return true;
        }
        if (!(object instanceof Set)) {
            return false;
        }
        Set set = (Set) object;
        return thisSet.size() == set.size() && thisSet.containsAll(set);
    }
}
