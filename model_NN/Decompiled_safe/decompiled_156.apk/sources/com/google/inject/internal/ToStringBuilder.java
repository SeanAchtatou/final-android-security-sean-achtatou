package com.google.inject.internal;

import java.util.LinkedHashMap;
import java.util.Map;

public class ToStringBuilder {
    final Map<String, Object> map = new LinkedHashMap();
    final String name;

    public ToStringBuilder(String name2) {
        this.name = name2;
    }

    public ToStringBuilder(Class type) {
        this.name = type.getSimpleName();
    }

    public ToStringBuilder add(String name2, Object value) {
        if (this.map.put(name2, value) == null) {
            return this;
        }
        throw new RuntimeException("Duplicate names: " + name2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public String toString() {
        return this.name + this.map.toString().replace('{', '[').replace('}', ']');
    }
}
