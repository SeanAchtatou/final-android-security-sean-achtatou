package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.Key;
import com.google.inject.binder.AnnotatedConstantBindingBuilder;
import com.google.inject.binder.ConstantBindingBuilder;
import com.google.inject.spi.Element;
import java.lang.annotation.Annotation;
import java.util.List;

public final class ConstantBindingBuilderImpl<T> extends AbstractBindingBuilder<T> implements AnnotatedConstantBindingBuilder, ConstantBindingBuilder {
    public ConstantBindingBuilderImpl(Binder binder, List<Element> elements, Object source) {
        super(binder, elements, source, NULL_KEY);
    }

    public ConstantBindingBuilder annotatedWith(Class<? extends Annotation> annotationType) {
        annotatedWithInternal(annotationType);
        return this;
    }

    public ConstantBindingBuilder annotatedWith(Annotation annotation) {
        annotatedWithInternal(annotation);
        return this;
    }

    public void to(String value) {
        toConstant(String.class, value);
    }

    public void to(int value) {
        toConstant(Integer.class, Integer.valueOf(value));
    }

    public void to(long value) {
        toConstant(Long.class, Long.valueOf(value));
    }

    public void to(boolean value) {
        toConstant(Boolean.class, Boolean.valueOf(value));
    }

    public void to(double value) {
        toConstant(Double.class, Double.valueOf(value));
    }

    public void to(float value) {
        toConstant(Float.class, Float.valueOf(value));
    }

    public void to(short value) {
        toConstant(Short.class, Short.valueOf(value));
    }

    public void to(char value) {
        toConstant(Character.class, Character.valueOf(value));
    }

    public void to(Class<?> value) {
        toConstant(Class.class, value);
    }

    public <E extends Enum<E>> void to(E value) {
        toConstant(value.getDeclaringClass(), value);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.inject.Key.get(java.lang.Class, java.lang.annotation.Annotation):com.google.inject.Key<T>
     arg types: [java.lang.Class<?>, java.lang.annotation.Annotation]
     candidates:
      com.google.inject.Key.get(com.google.inject.TypeLiteral, java.lang.Class<? extends java.lang.annotation.Annotation>):com.google.inject.Key<T>
      com.google.inject.Key.get(com.google.inject.TypeLiteral, java.lang.annotation.Annotation):com.google.inject.Key<T>
      com.google.inject.Key.get(java.lang.Class, com.google.inject.Key$AnnotationStrategy):com.google.inject.Key<T>
      com.google.inject.Key.get(java.lang.Class, java.lang.Class<? extends java.lang.annotation.Annotation>):com.google.inject.Key<T>
      com.google.inject.Key.get(java.lang.reflect.Type, java.lang.Class<? extends java.lang.annotation.Annotation>):com.google.inject.Key<?>
      com.google.inject.Key.get(java.lang.reflect.Type, java.lang.annotation.Annotation):com.google.inject.Key<?>
      com.google.inject.Key.get(java.lang.Class, java.lang.annotation.Annotation):com.google.inject.Key<T> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.inject.Key.get(java.lang.Class, java.lang.Class<? extends java.lang.annotation.Annotation>):com.google.inject.Key<T>
     arg types: [java.lang.Class<?>, java.lang.Class<? extends java.lang.annotation.Annotation>]
     candidates:
      com.google.inject.Key.get(com.google.inject.TypeLiteral, java.lang.Class<? extends java.lang.annotation.Annotation>):com.google.inject.Key<T>
      com.google.inject.Key.get(com.google.inject.TypeLiteral, java.lang.annotation.Annotation):com.google.inject.Key<T>
      com.google.inject.Key.get(java.lang.Class, com.google.inject.Key$AnnotationStrategy):com.google.inject.Key<T>
      com.google.inject.Key.get(java.lang.Class, java.lang.annotation.Annotation):com.google.inject.Key<T>
      com.google.inject.Key.get(java.lang.reflect.Type, java.lang.Class<? extends java.lang.annotation.Annotation>):com.google.inject.Key<?>
      com.google.inject.Key.get(java.lang.reflect.Type, java.lang.annotation.Annotation):com.google.inject.Key<?>
      com.google.inject.Key.get(java.lang.Class, java.lang.Class<? extends java.lang.annotation.Annotation>):com.google.inject.Key<T> */
    private void toConstant(Class<?> type, Object instance) {
        Key<T> key;
        Class<?> cls = type;
        Object obj = instance;
        if (keyTypeIsSet()) {
            this.binder.addError(AbstractBindingBuilder.CONSTANT_VALUE_ALREADY_SET, new Object[0]);
            return;
        }
        BindingImpl<T> base = getBinding();
        if (base.getKey().getAnnotation() != null) {
            key = Key.get((Class) cls, base.getKey().getAnnotation());
        } else if (base.getKey().getAnnotationType() != null) {
            key = Key.get((Class) cls, base.getKey().getAnnotationType());
        } else {
            key = Key.get((Class) cls);
        }
        if (obj == null) {
            this.binder.addError(AbstractBindingBuilder.BINDING_TO_NULL, new Object[0]);
        }
        setBinding(new InstanceBindingImpl(base.getSource(), key, base.getScoping(), ImmutableSet.of(), obj));
    }

    public String toString() {
        return "ConstantBindingBuilder";
    }
}
