package com.google.inject.internal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import mominis.gameconsole.services.impl.UserMembership;

public abstract class ImmutableSet<E> extends ImmutableCollection<E> implements Set<E> {
    private static final ImmutableSet<?> EMPTY_IMMUTABLE_SET = new EmptyImmutableSet();

    public abstract UnmodifiableIterator<E> iterator();

    public static <E> ImmutableSet<E> of() {
        return EMPTY_IMMUTABLE_SET;
    }

    public static <E> ImmutableSet<E> of(Object obj) {
        return new SingletonImmutableSet(obj, obj.hashCode());
    }

    public static <E> ImmutableSet<E> of(Object... objArr) {
        switch (objArr.length) {
            case UserMembership.DEFAULT_CHANNEL_ID /*0*/:
                return of();
            case 1:
                return of(objArr[0]);
            default:
                return create(Arrays.asList(objArr), objArr.length);
        }
    }

    public static <E> ImmutableSet<E> copyOf(Iterable<? extends E> elements) {
        if (elements instanceof ImmutableSet) {
            return (ImmutableSet) elements;
        }
        return copyOfInternal(Collections2.toCollection(elements));
    }

    public static <E> ImmutableSet<E> copyOf(Iterator<? extends E> elements) {
        return copyOfInternal(Lists.newArrayList(elements));
    }

    private static <E> ImmutableSet<E> copyOfInternal(Collection<? extends E> collection) {
        switch (collection.size()) {
            case UserMembership.DEFAULT_CHANNEL_ID /*0*/:
                return of();
            case 1:
                return of(collection.iterator().next());
            default:
                return create(collection, collection.size());
        }
    }

    ImmutableSet() {
    }

    /* access modifiers changed from: package-private */
    public boolean isHashCodeFast() {
        return false;
    }

    public boolean equals(@Nullable Object object) {
        if (object == this) {
            return true;
        }
        if (!(object instanceof ImmutableSet) || !isHashCodeFast() || !((ImmutableSet) object).isHashCodeFast() || hashCode() == object.hashCode()) {
            return Collections2.setEquals(this, object);
        }
        return false;
    }

    public int hashCode() {
        int hashCode = 0;
        Iterator i$ = iterator();
        while (i$.hasNext()) {
            hashCode += i$.next().hashCode();
        }
        return hashCode;
    }

    public String toString() {
        if (isEmpty()) {
            return "[]";
        }
        Iterator<E> iterator = iterator();
        StringBuilder result = new StringBuilder(size() * 16);
        result.append('[').append(iterator.next().toString());
        for (int i = 1; i < size(); i++) {
            result.append(", ").append(iterator.next().toString());
        }
        return result.append(']').toString();
    }

    private static final class EmptyImmutableSet extends ImmutableSet<Object> {
        private static final Object[] EMPTY_ARRAY = new Object[0];

        private EmptyImmutableSet() {
        }

        public int size() {
            return 0;
        }

        public boolean isEmpty() {
            return true;
        }

        public boolean contains(Object target) {
            return false;
        }

        public UnmodifiableIterator<Object> iterator() {
            return Iterators.emptyIterator();
        }

        public Object[] toArray() {
            return EMPTY_ARRAY;
        }

        public <T> T[] toArray(T[] a) {
            if (a.length > 0) {
                a[0] = null;
            }
            return a;
        }

        public boolean containsAll(Collection<?> targets) {
            return targets.isEmpty();
        }

        public boolean equals(@Nullable Object object) {
            if (object instanceof Set) {
                return ((Set) object).isEmpty();
            }
            return false;
        }

        public final int hashCode() {
            return 0;
        }

        /* access modifiers changed from: package-private */
        public boolean isHashCodeFast() {
            return true;
        }

        public String toString() {
            return "[]";
        }
    }

    private static final class SingletonImmutableSet<E> extends ImmutableSet<E> {
        final E element;
        final int hashCode;

        SingletonImmutableSet(E element2, int hashCode2) {
            this.element = element2;
            this.hashCode = hashCode2;
        }

        public int size() {
            return 1;
        }

        public boolean isEmpty() {
            return false;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: E
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean contains(java.lang.Object r2) {
            /*
                r1 = this;
                E r0 = r1.element
                boolean r0 = r0.equals(r2)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.ImmutableSet.SingletonImmutableSet.contains(java.lang.Object):boolean");
        }

        public UnmodifiableIterator<E> iterator() {
            return Iterators.singletonIterator(this.element);
        }

        public Object[] toArray() {
            return new Object[]{this.element};
        }

        /* JADX INFO: additional move instructions added (2) to help type inference */
        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public <T> T[] toArray(T[] r3) {
            /*
                r2 = this;
                r1 = 1
                int r0 = r3.length
                if (r0 != 0) goto L_0x000e
                java.lang.Object[] r3 = com.google.inject.internal.ObjectArrays.newArray(r3, r1)
            L_0x0008:
                r0 = 0
                E r1 = r2.element
                r3[r0] = r1
                return r3
            L_0x000e:
                int r0 = r3.length
                if (r0 <= r1) goto L_0x0008
                r0 = 0
                r3[r1] = r0
                goto L_0x0008
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.ImmutableSet.SingletonImmutableSet.toArray(java.lang.Object[]):java.lang.Object[]");
        }

        public boolean equals(@Nullable Object object) {
            if (object == this) {
                return true;
            }
            if (!(object instanceof Set)) {
                return false;
            }
            Set set = (Set) object;
            return set.size() == 1 && this.element.equals(set.iterator().next());
        }

        public final int hashCode() {
            return this.hashCode;
        }

        /* access modifiers changed from: package-private */
        public boolean isHashCodeFast() {
            return true;
        }

        public String toString() {
            String elementToString = this.element.toString();
            return new StringBuilder(elementToString.length() + 2).append('[').append(elementToString).append(']').toString();
        }
    }

    private static <E> ImmutableSet<E> create(Iterable<? extends E> iterable, int count) {
        int tableSize = Hashing.chooseTableSize(count);
        Object[] table = new Object[tableSize];
        int mask = tableSize - 1;
        List<E> elements = new ArrayList<>(count);
        int hashCode = 0;
        for (E element : iterable) {
            int hash = element.hashCode();
            int i = Hashing.smear(hash);
            while (true) {
                int index = i & mask;
                Object value = table[index];
                if (value != null) {
                    if (value.equals(element)) {
                        break;
                    }
                    i++;
                } else {
                    table[index] = element;
                    elements.add(element);
                    hashCode += hash;
                    break;
                }
            }
        }
        return elements.size() == 1 ? new SingletonImmutableSet(elements.get(0), hashCode) : new RegularImmutableSet(elements.toArray(), hashCode, table, mask);
    }

    static abstract class ArrayImmutableSet<E> extends ImmutableSet<E> {
        final Object[] elements;

        ArrayImmutableSet(Object[] elements2) {
            this.elements = elements2;
        }

        public int size() {
            return this.elements.length;
        }

        public boolean isEmpty() {
            return false;
        }

        public UnmodifiableIterator<E> iterator() {
            return Iterators.forArray(this.elements);
        }

        public Object[] toArray() {
            Object[] array = new Object[size()];
            System.arraycopy(this.elements, 0, array, 0, size());
            return array;
        }

        public <T> T[] toArray(T[] array) {
            int size = size();
            if (array.length < size) {
                array = ObjectArrays.newArray(array, size);
            } else if (array.length > size) {
                array[size] = null;
            }
            System.arraycopy(this.elements, 0, array, 0, size);
            return array;
        }

        public boolean containsAll(Collection<?> targets) {
            if (targets == this) {
                return true;
            }
            if (!(targets instanceof ArrayImmutableSet)) {
                return ImmutableSet.super.containsAll(targets);
            }
            if (targets.size() > size()) {
                return false;
            }
            for (Object target : ((ArrayImmutableSet) targets).elements) {
                if (!contains(target)) {
                    return false;
                }
            }
            return true;
        }
    }

    private static final class RegularImmutableSet<E> extends ArrayImmutableSet<E> {
        final int hashCode;
        final int mask;
        final Object[] table;

        RegularImmutableSet(Object[] elements, int hashCode2, Object[] table2, int mask2) {
            super(elements);
            this.table = table2;
            this.mask = mask2;
            this.hashCode = hashCode2;
        }

        public boolean contains(Object target) {
            if (target == null) {
                return false;
            }
            int i = Hashing.smear(target.hashCode());
            while (true) {
                Object candidate = this.table[this.mask & i];
                if (candidate == null) {
                    return false;
                }
                if (candidate.equals(target)) {
                    return true;
                }
                i++;
            }
        }

        public int hashCode() {
            return this.hashCode;
        }

        /* access modifiers changed from: package-private */
        public boolean isHashCodeFast() {
            return true;
        }
    }

    static abstract class TransformedImmutableSet<D, E> extends ImmutableSet<E> {
        final int hashCode;
        final D[] source;

        /* access modifiers changed from: package-private */
        public abstract E transform(D d);

        TransformedImmutableSet(D[] source2, int hashCode2) {
            this.source = source2;
            this.hashCode = hashCode2;
        }

        public int size() {
            return this.source.length;
        }

        public boolean isEmpty() {
            return false;
        }

        public UnmodifiableIterator<E> iterator() {
            return Iterators.unmodifiableIterator(new AbstractIterator<E>() {
                int index = 0;

                /* access modifiers changed from: protected */
                public E computeNext() {
                    if (this.index >= TransformedImmutableSet.this.source.length) {
                        return endOfData();
                    }
                    TransformedImmutableSet transformedImmutableSet = TransformedImmutableSet.this;
                    D[] dArr = TransformedImmutableSet.this.source;
                    int i = this.index;
                    this.index = i + 1;
                    return transformedImmutableSet.transform(dArr[i]);
                }
            });
        }

        public Object[] toArray() {
            return toArray(new Object[size()]);
        }

        public <T> T[] toArray(T[] array) {
            int size = size();
            if (array.length < size) {
                array = ObjectArrays.newArray(array, size);
            } else if (array.length > size) {
                array[size] = null;
            }
            for (int i = 0; i < this.source.length; i++) {
                array[i] = transform(this.source[i]);
            }
            return array;
        }

        public final int hashCode() {
            return this.hashCode;
        }

        /* access modifiers changed from: package-private */
        public boolean isHashCodeFast() {
            return true;
        }
    }

    private static class SerializedForm implements Serializable {
        private static final long serialVersionUID = 0;
        final Object[] elements;

        SerializedForm(Object[] elements2) {
            this.elements = elements2;
        }

        /* access modifiers changed from: package-private */
        public Object readResolve() {
            return ImmutableSet.of(this.elements);
        }
    }

    /* access modifiers changed from: package-private */
    public Object writeReplace() {
        return new SerializedForm(toArray());
    }

    public static <E> Builder<E> builder() {
        return new Builder<>();
    }

    public static class Builder<E> {
        final ArrayList<E> contents = Lists.newArrayList();

        public Builder<E> add(E element) {
            Preconditions.checkNotNull(element, "element cannot be null");
            this.contents.add(element);
            return this;
        }

        public Builder<E> add(E... elements) {
            Preconditions.checkNotNull(elements, "elements cannot be null");
            List<E> elemsAsList = Arrays.asList(elements);
            Preconditions.checkContentsNotNull(elemsAsList, "elements cannot contain null");
            this.contents.addAll(elemsAsList);
            return this;
        }

        public Builder<E> addAll(Iterable<? extends E> elements) {
            if (elements instanceof Collection) {
                this.contents.ensureCapacity(this.contents.size() + ((Collection) elements).size());
            }
            for (E elem : elements) {
                Preconditions.checkNotNull(elem, "elements contains a null");
                this.contents.add(elem);
            }
            return this;
        }

        public Builder<E> addAll(Iterator<? extends E> elements) {
            while (elements.hasNext()) {
                E element = elements.next();
                Preconditions.checkNotNull(element, "element cannot be null");
                this.contents.add(element);
            }
            return this;
        }

        public ImmutableSet<E> build() {
            return ImmutableSet.copyOf(this.contents);
        }
    }
}
