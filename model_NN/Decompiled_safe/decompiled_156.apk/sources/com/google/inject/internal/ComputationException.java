package com.google.inject.internal;

public class ComputationException extends RuntimeException {
    public ComputationException(Throwable cause) {
        super(cause);
    }
}
