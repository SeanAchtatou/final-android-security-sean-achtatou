package com.google.inject.internal;

import java.util.Map;

public abstract class FailableCache<K, V> {
    private final Map<K, Object> delegate = new MapMaker().makeComputingMap(new Function<K, Object>() {
        public Object apply(@Nullable K key) {
            Errors errors = new Errors();
            V result = null;
            try {
                result = FailableCache.this.create(key, errors);
            } catch (ErrorsException e) {
                errors.merge(e.getErrors());
            }
            if (errors.hasErrors()) {
                return errors;
            }
            return result;
        }
    });

    /* access modifiers changed from: protected */
    public abstract V create(Object obj, Errors errors) throws ErrorsException;

    public V get(K key, Errors errors) throws ErrorsException {
        V resultOrError = this.delegate.get(key);
        if (!(resultOrError instanceof Errors)) {
            return resultOrError;
        }
        errors.merge((Errors) resultOrError);
        throw errors.toException();
    }
}
