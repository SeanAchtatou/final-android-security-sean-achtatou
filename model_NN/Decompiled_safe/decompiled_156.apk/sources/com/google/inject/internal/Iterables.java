package com.google.inject.internal;

import java.util.Arrays;
import java.util.Iterator;

public final class Iterables {
    private Iterables() {
    }

    public static String toString(Iterable<?> iterable) {
        return Iterators.toString(iterable.iterator());
    }

    public static <T> T getOnlyElement(Iterable<T> iterable) {
        return Iterators.getOnlyElement(iterable.iterator());
    }

    public static <T> Iterable<T> concat(Iterable<? extends T> a, Iterable<? extends T> b) {
        Preconditions.checkNotNull(a);
        Preconditions.checkNotNull(b);
        return concat(Arrays.asList(a, b));
    }

    public static <T> Iterable<T> concat(Iterable<? extends Iterable<? extends T>> inputs) {
        final Iterable<Iterator<? extends T>> iterators = transform(inputs, new Function<Iterable<? extends T>, Iterator<? extends T>>() {
            public /* bridge */ /* synthetic */ Object apply(Object x0) {
                return apply((Iterable) ((Iterable) x0));
            }

            public Iterator<? extends T> apply(Iterable<? extends T> from) {
                return from.iterator();
            }
        });
        return new IterableWithToString<T>() {
            public Iterator<T> iterator() {
                return Iterators.concat(iterators.iterator());
            }
        };
    }

    public static <F, T> Iterable<T> transform(final Iterable<F> fromIterable, final Function<? super F, ? extends T> function) {
        Preconditions.checkNotNull(fromIterable);
        Preconditions.checkNotNull(function);
        return new IterableWithToString<T>() {
            public Iterator<T> iterator() {
                return Iterators.transform(fromIterable.iterator(), function);
            }
        };
    }

    static abstract class IterableWithToString<E> implements Iterable<E> {
        IterableWithToString() {
        }

        public String toString() {
            return Iterables.toString(this);
        }
    }
}
