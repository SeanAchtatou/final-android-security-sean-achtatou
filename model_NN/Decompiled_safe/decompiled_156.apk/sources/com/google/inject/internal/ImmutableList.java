package com.google.inject.internal;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;

public abstract class ImmutableList<E> extends ImmutableCollection<E> implements List<E>, RandomAccess {
    private static final ImmutableList<?> EMPTY_IMMUTABLE_LIST = new EmptyImmutableList();

    public abstract int indexOf(@Nullable Object obj);

    public abstract UnmodifiableIterator<E> iterator();

    public abstract int lastIndexOf(@Nullable Object obj);

    public abstract ImmutableList<E> subList(int i, int i2);

    public static <E> ImmutableList<E> of() {
        return EMPTY_IMMUTABLE_LIST;
    }

    public static <E> ImmutableList<E> of(Object obj) {
        return new RegularImmutableList(copyIntoArray(obj));
    }

    public static <E> ImmutableList<E> of(E e1, E e2) {
        return new RegularImmutableList(copyIntoArray(e1, e2));
    }

    public static <E> ImmutableList<E> of(E e1, E e2, E e3) {
        return new RegularImmutableList(copyIntoArray(e1, e2, e3));
    }

    public static <E> ImmutableList<E> of(E e1, E e2, E e3, E e4) {
        return new RegularImmutableList(copyIntoArray(e1, e2, e3, e4));
    }

    public static <E> ImmutableList<E> of(E e1, E e2, E e3, E e4, E e5) {
        return new RegularImmutableList(copyIntoArray(e1, e2, e3, e4, e5));
    }

    public static <E> ImmutableList<E> of(Object... objArr) {
        return objArr.length == 0 ? of() : new RegularImmutableList(copyIntoArray(objArr));
    }

    public static <E> ImmutableList<E> copyOf(Iterable iterable) {
        if (iterable instanceof ImmutableList) {
            return (ImmutableList) iterable;
        }
        if (iterable instanceof Collection) {
            return copyOfInternal((Collection) iterable);
        }
        return copyOfInternal(Lists.newArrayList(iterable));
    }

    public static <E> ImmutableList<E> copyOf(Iterator it) {
        return copyOfInternal(Lists.newArrayList(it));
    }

    private static <E> ImmutableList<E> copyOfInternal(ArrayList<? extends E> list) {
        return list.isEmpty() ? of() : new RegularImmutableList(nullChecked(list.toArray()));
    }

    private static Object[] nullChecked(Object[] array) {
        int len = array.length;
        for (int i = 0; i < len; i++) {
            if (array[i] == null) {
                throw new NullPointerException("at index " + i);
            }
        }
        return array;
    }

    private static <E> ImmutableList<E> copyOfInternal(Collection<? extends E> collection) {
        int size = collection.size();
        return size == 0 ? of() : createFromIterable(collection, size);
    }

    private ImmutableList() {
    }

    public final boolean addAll(int index, Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    public final E set(int index, E e) {
        throw new UnsupportedOperationException();
    }

    public final void add(int index, E e) {
        throw new UnsupportedOperationException();
    }

    public final E remove(int index) {
        throw new UnsupportedOperationException();
    }

    private static final class EmptyImmutableList extends ImmutableList<Object> {
        private static final Object[] EMPTY_ARRAY = new Object[0];

        private EmptyImmutableList() {
            super();
        }

        public int size() {
            return 0;
        }

        public boolean isEmpty() {
            return true;
        }

        public boolean contains(Object target) {
            return false;
        }

        public UnmodifiableIterator<Object> iterator() {
            return Iterators.emptyIterator();
        }

        public Object[] toArray() {
            return EMPTY_ARRAY;
        }

        public <T> T[] toArray(T[] a) {
            if (a.length > 0) {
                a[0] = null;
            }
            return a;
        }

        public Object get(int index) {
            Preconditions.checkElementIndex(index, 0);
            throw new AssertionError("unreachable");
        }

        public int indexOf(Object target) {
            return -1;
        }

        public int lastIndexOf(Object target) {
            return -1;
        }

        public ImmutableList<Object> subList(int fromIndex, int toIndex) {
            Preconditions.checkPositionIndexes(fromIndex, toIndex, 0);
            return this;
        }

        public ListIterator<Object> listIterator() {
            return Iterators.emptyListIterator();
        }

        public ListIterator<Object> listIterator(int start) {
            Preconditions.checkPositionIndex(start, 0);
            return Iterators.emptyListIterator();
        }

        public boolean containsAll(Collection<?> targets) {
            return targets.isEmpty();
        }

        public boolean equals(@Nullable Object object) {
            if (object instanceof List) {
                return ((List) object).isEmpty();
            }
            return false;
        }

        public int hashCode() {
            return 1;
        }

        public String toString() {
            return "[]";
        }
    }

    private static final class RegularImmutableList<E> extends ImmutableList<E> {
        private final Object[] array;
        private final int offset;
        /* access modifiers changed from: private */
        public final int size;

        private RegularImmutableList(Object[] array2, int offset2, int size2) {
            super();
            this.offset = offset2;
            this.size = size2;
            this.array = array2;
        }

        private RegularImmutableList(Object[] array2) {
            this(array2, 0, array2.length);
        }

        public int size() {
            return this.size;
        }

        public boolean isEmpty() {
            return false;
        }

        public boolean contains(Object target) {
            return indexOf(target) != -1;
        }

        public UnmodifiableIterator<E> iterator() {
            return Iterators.forArray(this.array, this.offset, this.size);
        }

        public Object[] toArray() {
            Object[] newArray = new Object[size()];
            System.arraycopy(this.array, this.offset, newArray, 0, this.size);
            return newArray;
        }

        public <T> T[] toArray(T[] other) {
            if (other.length < this.size) {
                other = ObjectArrays.newArray(other, this.size);
            } else if (other.length > this.size) {
                other[this.size] = null;
            }
            System.arraycopy(this.array, this.offset, other, 0, this.size);
            return other;
        }

        public E get(int index) {
            Preconditions.checkElementIndex(index, this.size);
            return this.array[this.offset + index];
        }

        public int indexOf(Object target) {
            if (target != null) {
                for (int i = this.offset; i < this.offset + this.size; i++) {
                    if (this.array[i].equals(target)) {
                        return i - this.offset;
                    }
                }
            }
            return -1;
        }

        public int lastIndexOf(Object target) {
            if (target != null) {
                for (int i = (this.offset + this.size) - 1; i >= this.offset; i--) {
                    if (this.array[i].equals(target)) {
                        return i - this.offset;
                    }
                }
            }
            return -1;
        }

        public ImmutableList<E> subList(int fromIndex, int toIndex) {
            Preconditions.checkPositionIndexes(fromIndex, toIndex, this.size);
            return fromIndex == toIndex ? ImmutableList.of() : new RegularImmutableList(this.array, this.offset + fromIndex, toIndex - fromIndex);
        }

        public ListIterator<E> listIterator() {
            return listIterator(0);
        }

        public ListIterator<E> listIterator(final int start) {
            Preconditions.checkPositionIndex(start, this.size);
            return new ListIterator<E>() {
                int index = start;

                public boolean hasNext() {
                    return this.index < RegularImmutableList.this.size;
                }

                public boolean hasPrevious() {
                    return this.index > 0;
                }

                public int nextIndex() {
                    return this.index;
                }

                public int previousIndex() {
                    return this.index - 1;
                }

                public E next() {
                    try {
                        E result = RegularImmutableList.this.get(this.index);
                        this.index++;
                        return result;
                    } catch (IndexOutOfBoundsException e) {
                        throw new NoSuchElementException();
                    }
                }

                public E previous() {
                    try {
                        E result = RegularImmutableList.this.get(this.index - 1);
                        this.index--;
                        return result;
                    } catch (IndexOutOfBoundsException e) {
                        throw new NoSuchElementException();
                    }
                }

                public void set(E e) {
                    throw new UnsupportedOperationException();
                }

                public void add(E e) {
                    throw new UnsupportedOperationException();
                }

                public void remove() {
                    throw new UnsupportedOperationException();
                }
            };
        }

        public boolean equals(@Nullable Object object) {
            if (object == this) {
                return true;
            }
            if (!(object instanceof List)) {
                return false;
            }
            List<Object> list = (List) object;
            if (size() != list.size()) {
                return false;
            }
            int index = this.offset;
            if (object instanceof RegularImmutableList) {
                RegularImmutableList<?> other = (RegularImmutableList) object;
                int i = other.offset;
                while (i < other.offset + other.size) {
                    int index2 = index + 1;
                    if (!this.array[index].equals(other.array[i])) {
                        return false;
                    }
                    i++;
                    index = index2;
                }
            } else {
                for (Object element : list) {
                    int index3 = index + 1;
                    if (!this.array[index].equals(element)) {
                        return false;
                    }
                    index = index3;
                }
            }
            return true;
        }

        public int hashCode() {
            int hashCode = 1;
            for (int i = this.offset; i < this.offset + this.size; i++) {
                hashCode = (hashCode * 31) + this.array[i].hashCode();
            }
            return hashCode;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(size() * 16);
            sb.append('[').append(this.array[this.offset]);
            for (int i = this.offset + 1; i < this.offset + this.size; i++) {
                sb.append(", ").append(this.array[i]);
            }
            return sb.append(']').toString();
        }
    }

    private static Object[] copyIntoArray(Object... source) {
        Object[] array = new Object[source.length];
        Object[] arr$ = source;
        int len$ = arr$.length;
        int i$ = 0;
        int index = 0;
        while (i$ < len$) {
            Object element = arr$[i$];
            if (element == null) {
                throw new NullPointerException("at index " + index);
            }
            array[index] = element;
            i$++;
            index++;
        }
        return array;
    }

    private static <E> ImmutableList<E> createFromIterable(Iterable<?> source, int estimatedSize) {
        Object[] array = new Object[estimatedSize];
        int index = 0;
        for (Object element : source) {
            if (index == estimatedSize) {
                estimatedSize = ((estimatedSize / 2) + 1) * 3;
                array = copyOf(array, estimatedSize);
            }
            if (element == null) {
                throw new NullPointerException("at index " + index);
            }
            array[index] = element;
            index++;
        }
        if (index == 0) {
            return of();
        }
        if (index != estimatedSize) {
            array = copyOf(array, index);
        }
        return new RegularImmutableList(array, 0, index);
    }

    private static Object[] copyOf(Object[] oldArray, int newSize) {
        Object[] newArray = new Object[newSize];
        System.arraycopy(oldArray, 0, newArray, 0, Math.min(oldArray.length, newSize));
        return newArray;
    }

    private static class SerializedForm implements Serializable {
        private static final long serialVersionUID = 0;
        final Object[] elements;

        SerializedForm(Object[] elements2) {
            this.elements = elements2;
        }

        /* access modifiers changed from: package-private */
        public Object readResolve() {
            return ImmutableList.of(this.elements);
        }
    }

    private void readObject(ObjectInputStream stream) throws InvalidObjectException {
        throw new InvalidObjectException("Use SerializedForm");
    }

    /* access modifiers changed from: package-private */
    public Object writeReplace() {
        return new SerializedForm(toArray());
    }

    public static <E> Builder<E> builder() {
        return new Builder<>();
    }

    public static class Builder<E> {
        private final ArrayList<E> contents = Lists.newArrayList();

        public Builder<E> add(E element) {
            Preconditions.checkNotNull(element, "element cannot be null");
            this.contents.add(element);
            return this;
        }

        public Builder<E> addAll(Iterable<? extends E> elements) {
            if (elements instanceof Collection) {
                this.contents.ensureCapacity(this.contents.size() + ((Collection) elements).size());
            }
            for (E elem : elements) {
                Preconditions.checkNotNull(elem, "elements contains a null");
                this.contents.add(elem);
            }
            return this;
        }

        public ImmutableList<E> build() {
            return ImmutableList.copyOf(this.contents);
        }
    }
}
