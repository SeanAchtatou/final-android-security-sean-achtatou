package com.google.inject.internal;

import com.google.inject.internal.ImmutableSet;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import mominis.gameconsole.services.impl.UserMembership;

public abstract class ImmutableMap<K, V> implements ConcurrentMap<K, V>, Serializable {
    private static final ImmutableMap<?, ?> EMPTY_IMMUTABLE_MAP = new EmptyImmutableMap();

    public abstract boolean containsKey(@Nullable Object obj);

    public abstract boolean containsValue(@Nullable Object obj);

    public abstract ImmutableSet<Map.Entry<K, V>> entrySet();

    public abstract V get(@Nullable Object obj);

    public abstract ImmutableSet<K> keySet();

    public abstract ImmutableCollection<V> values();

    public static <K, V> ImmutableMap<K, V> of() {
        return EMPTY_IMMUTABLE_MAP;
    }

    public static <K, V> ImmutableMap<K, V> of(K k1, V v1) {
        return new SingletonImmutableMap(Preconditions.checkNotNull(k1), Preconditions.checkNotNull(v1));
    }

    public static <K, V> ImmutableMap<K, V> of(K k1, V v1, K k2, V v2) {
        return new RegularImmutableMap(new Map.Entry[]{entryOf(k1, v1), entryOf(k2, v2)});
    }

    public static <K, V> ImmutableMap<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3) {
        return new RegularImmutableMap(new Map.Entry[]{entryOf(k1, v1), entryOf(k2, v2), entryOf(k3, v3)});
    }

    public static <K, V> ImmutableMap<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4) {
        return new RegularImmutableMap(new Map.Entry[]{entryOf(k1, v1), entryOf(k2, v2), entryOf(k3, v3), entryOf(k4, v4)});
    }

    public static <K, V> ImmutableMap<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return new RegularImmutableMap(new Map.Entry[]{entryOf(k1, v1), entryOf(k2, v2), entryOf(k3, v3), entryOf(k4, v4), entryOf(k5, v5)});
    }

    public static <K, V> Builder<K, V> builder() {
        return new Builder<>();
    }

    /* access modifiers changed from: private */
    public static <K, V> Map.Entry<K, V> entryOf(K key, V value) {
        return Maps.immutableEntry(Preconditions.checkNotNull(key), Preconditions.checkNotNull(value));
    }

    public static class Builder<K, V> {
        final List<Map.Entry<K, V>> entries = Lists.newArrayList();

        public Builder<K, V> put(K key, V value) {
            this.entries.add(ImmutableMap.entryOf(key, value));
            return this;
        }

        public Builder<K, V> putAll(Map<? extends K, ? extends V> map) {
            for (Map.Entry<? extends K, ? extends V> entry : map.entrySet()) {
                put(entry.getKey(), entry.getValue());
            }
            return this;
        }

        public ImmutableMap<K, V> build() {
            return fromEntryList(this.entries);
        }

        private static <K, V> ImmutableMap<K, V> fromEntryList(List<Map.Entry<K, V>> entries2) {
            switch (entries2.size()) {
                case UserMembership.DEFAULT_CHANNEL_ID /*0*/:
                    return ImmutableMap.of();
                case 1:
                    return new SingletonImmutableMap((Map.Entry) Iterables.getOnlyElement(entries2));
                default:
                    return new RegularImmutableMap((Map.Entry[]) entries2.toArray(new Map.Entry[entries2.size()]));
            }
        }
    }

    public static <K, V> ImmutableMap<K, V> copyOf(Map<? extends K, ? extends V> map) {
        if (map instanceof ImmutableMap) {
            return (ImmutableMap) map;
        }
        int size = map.size();
        switch (size) {
            case UserMembership.DEFAULT_CHANNEL_ID /*0*/:
                return of();
            case 1:
                Map.Entry<? extends K, ? extends V> loneEntry = (Map.Entry) Iterables.getOnlyElement(map.entrySet());
                return of(loneEntry.getKey(), loneEntry.getValue());
            default:
                Map.Entry<?, ?>[] array = new Map.Entry[size];
                int i = 0;
                for (Map.Entry<? extends K, ? extends V> entry : map.entrySet()) {
                    array[i] = entryOf(entry.getKey(), entry.getValue());
                    i++;
                }
                return new RegularImmutableMap(array);
        }
    }

    ImmutableMap() {
    }

    public final V put(K k, V v) {
        throw new UnsupportedOperationException();
    }

    public final V remove(Object o) {
        throw new UnsupportedOperationException();
    }

    public final V putIfAbsent(K k, V v) {
        throw new UnsupportedOperationException();
    }

    public final boolean remove(Object key, Object value) {
        throw new UnsupportedOperationException();
    }

    public final boolean replace(K k, V v, V v2) {
        throw new UnsupportedOperationException();
    }

    public final V replace(K k, V v) {
        throw new UnsupportedOperationException();
    }

    public final void putAll(Map<? extends K, ? extends V> map) {
        throw new UnsupportedOperationException();
    }

    public final void clear() {
        throw new UnsupportedOperationException();
    }

    public boolean equals(@Nullable Object object) {
        if (object == this) {
            return true;
        }
        if (object instanceof Map) {
            return entrySet().equals(((Map) object).entrySet());
        }
        return false;
    }

    public int hashCode() {
        return entrySet().hashCode();
    }

    public String toString() {
        StringBuilder result = new StringBuilder(size() * 16).append('{');
        Iterator<Map.Entry<K, V>> entries = entrySet().iterator();
        result.append(entries.next());
        while (entries.hasNext()) {
            result.append(", ").append(entries.next());
        }
        return result.append('}').toString();
    }

    private static final class EmptyImmutableMap extends ImmutableMap<Object, Object> {
        private EmptyImmutableMap() {
        }

        public Object get(Object key) {
            return null;
        }

        public int size() {
            return 0;
        }

        public boolean isEmpty() {
            return true;
        }

        public boolean containsKey(Object key) {
            return false;
        }

        public boolean containsValue(Object value) {
            return false;
        }

        public ImmutableSet<Map.Entry<Object, Object>> entrySet() {
            return ImmutableSet.of();
        }

        public ImmutableSet<Object> keySet() {
            return ImmutableSet.of();
        }

        public ImmutableCollection<Object> values() {
            return ImmutableCollection.EMPTY_IMMUTABLE_COLLECTION;
        }

        public boolean equals(@Nullable Object object) {
            if (object instanceof Map) {
                return ((Map) object).isEmpty();
            }
            return false;
        }

        public int hashCode() {
            return 0;
        }

        public String toString() {
            return "{}";
        }
    }

    private static final class SingletonImmutableMap<K, V> extends ImmutableMap<K, V> {
        private transient Map.Entry<K, V> entry;
        private transient ImmutableSet<Map.Entry<K, V>> entrySet;
        private transient ImmutableSet<K> keySet;
        private final transient K singleKey;
        private final transient V singleValue;
        private transient ImmutableCollection<V> values;

        private SingletonImmutableMap(K singleKey2, V singleValue2) {
            this.singleKey = singleKey2;
            this.singleValue = singleValue2;
        }

        private SingletonImmutableMap(Map.Entry<K, V> entry2) {
            this.entry = entry2;
            this.singleKey = entry2.getKey();
            this.singleValue = entry2.getValue();
        }

        private Map.Entry<K, V> entry() {
            Map.Entry<K, V> e = this.entry;
            if (e != null) {
                return e;
            }
            Map.Entry<K, V> immutableEntry = Maps.immutableEntry(this.singleKey, this.singleValue);
            this.entry = immutableEntry;
            return immutableEntry;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public V get(java.lang.Object r2) {
            /*
                r1 = this;
                K r0 = r1.singleKey
                boolean r0 = r0.equals(r2)
                if (r0 == 0) goto L_0x000b
                V r0 = r1.singleValue
            L_0x000a:
                return r0
            L_0x000b:
                r0 = 0
                goto L_0x000a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.ImmutableMap.SingletonImmutableMap.get(java.lang.Object):java.lang.Object");
        }

        public int size() {
            return 1;
        }

        public boolean isEmpty() {
            return false;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean containsKey(java.lang.Object r2) {
            /*
                r1 = this;
                K r0 = r1.singleKey
                boolean r0 = r0.equals(r2)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.ImmutableMap.SingletonImmutableMap.containsKey(java.lang.Object):boolean");
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean containsValue(java.lang.Object r2) {
            /*
                r1 = this;
                V r0 = r1.singleValue
                boolean r0 = r0.equals(r2)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.ImmutableMap.SingletonImmutableMap.containsValue(java.lang.Object):boolean");
        }

        public ImmutableSet<Map.Entry<K, V>> entrySet() {
            ImmutableSet<Map.Entry<K, V>> es = this.entrySet;
            if (es != null) {
                return es;
            }
            ImmutableSet<Map.Entry<K, V>> of = ImmutableSet.of(entry());
            this.entrySet = of;
            return of;
        }

        public ImmutableSet<K> keySet() {
            ImmutableSet<K> ks = this.keySet;
            if (ks != null) {
                return ks;
            }
            ImmutableSet<K> of = ImmutableSet.of(this.singleKey);
            this.keySet = of;
            return of;
        }

        public ImmutableCollection<V> values() {
            ImmutableCollection<V> v = this.values;
            if (v != null) {
                return v;
            }
            Values values2 = new Values(this.singleValue);
            this.values = values2;
            return values2;
        }

        private static class Values<V> extends ImmutableCollection<V> {
            final V singleValue;

            Values(V singleValue2) {
                this.singleValue = singleValue2;
            }

            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public boolean contains(java.lang.Object r2) {
                /*
                    r1 = this;
                    V r0 = r1.singleValue
                    boolean r0 = r0.equals(r2)
                    return r0
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.ImmutableMap.SingletonImmutableMap.Values.contains(java.lang.Object):boolean");
            }

            public boolean isEmpty() {
                return false;
            }

            public int size() {
                return 1;
            }

            public UnmodifiableIterator<V> iterator() {
                return Iterators.singletonIterator(this.singleValue);
            }
        }

        public boolean equals(@Nullable Object object) {
            if (object == this) {
                return true;
            }
            if (!(object instanceof Map)) {
                return false;
            }
            Map map = (Map) object;
            if (map.size() != 1) {
                return false;
            }
            Map.Entry<?, ?> entry2 = (Map.Entry) map.entrySet().iterator().next();
            return this.singleKey.equals(entry2.getKey()) && this.singleValue.equals(entry2.getValue());
        }

        public int hashCode() {
            return this.singleKey.hashCode() ^ this.singleValue.hashCode();
        }

        public String toString() {
            return '{' + this.singleKey.toString() + '=' + this.singleValue.toString() + '}';
        }
    }

    private static final class RegularImmutableMap<K, V> extends ImmutableMap<K, V> {
        /* access modifiers changed from: private */
        public final transient Map.Entry<K, V>[] entries;
        private transient ImmutableSet<Map.Entry<K, V>> entrySet;
        private transient ImmutableSet<K> keySet;
        /* access modifiers changed from: private */
        public final transient int keySetHashCode;
        private final transient int mask;
        private final transient Object[] table;
        private transient ImmutableCollection<V> values;

        /* JADX WARNING: Code restructure failed: missing block: B:5:0x004f, code lost:
            r14 = r3.getValue();
            r0.table[r7] = r8;
            r0.table[r7 + 1] = r14;
            r10 = r10 + r9;
            r6 = r6 + 1;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private RegularImmutableMap(java.util.Map.Entry<?, ?>... r19) {
            /*
                r18 = this;
                r18.<init>()
                r0 = r19
                java.util.Map$Entry[] r0 = (java.util.Map.Entry[]) r0
                r13 = r0
                r0 = r13
                r1 = r18
                r1.entries = r0
                r0 = r19
                int r0 = r0.length
                r15 = r0
                int r12 = com.google.inject.internal.Hashing.chooseTableSize(r15)
                int r15 = r12 * 2
                java.lang.Object[] r15 = new java.lang.Object[r15]
                r0 = r15
                r1 = r18
                r1.table = r0
                r15 = 1
                int r15 = r12 - r15
                r0 = r15
                r1 = r18
                r1.mask = r0
                r10 = 0
                r0 = r18
                java.util.Map$Entry<K, V>[] r0 = r0.entries
                r2 = r0
                int r11 = r2.length
                r6 = 0
            L_0x002e:
                if (r6 >= r11) goto L_0x008c
                r3 = r2[r6]
                java.lang.Object r8 = r3.getKey()
                int r9 = r8.hashCode()
                int r5 = com.google.inject.internal.Hashing.smear(r9)
            L_0x003e:
                r0 = r18
                int r0 = r0.mask
                r15 = r0
                r15 = r15 & r5
                int r7 = r15 * 2
                r0 = r18
                java.lang.Object[] r0 = r0.table
                r15 = r0
                r4 = r15[r7]
                if (r4 != 0) goto L_0x0067
                java.lang.Object r14 = r3.getValue()
                r0 = r18
                java.lang.Object[] r0 = r0.table
                r15 = r0
                r15[r7] = r8
                r0 = r18
                java.lang.Object[] r0 = r0.table
                r15 = r0
                int r16 = r7 + 1
                r15[r16] = r14
                int r10 = r10 + r9
                int r6 = r6 + 1
                goto L_0x002e
            L_0x0067:
                boolean r15 = r4.equals(r8)
                if (r15 == 0) goto L_0x0089
                java.lang.IllegalArgumentException r15 = new java.lang.IllegalArgumentException
                java.lang.StringBuilder r16 = new java.lang.StringBuilder
                r16.<init>()
                java.lang.String r17 = "duplicate key: "
                java.lang.StringBuilder r16 = r16.append(r17)
                r0 = r16
                r1 = r8
                java.lang.StringBuilder r16 = r0.append(r1)
                java.lang.String r16 = r16.toString()
                r15.<init>(r16)
                throw r15
            L_0x0089:
                int r5 = r5 + 1
                goto L_0x003e
            L_0x008c:
                r0 = r10
                r1 = r18
                r1.keySetHashCode = r0
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.ImmutableMap.RegularImmutableMap.<init>(java.util.Map$Entry[]):void");
        }

        public V get(Object key) {
            if (key == null) {
                return null;
            }
            int i = Hashing.smear(key.hashCode());
            while (true) {
                int index = (this.mask & i) * 2;
                Object candidate = this.table[index];
                if (candidate == null) {
                    return null;
                }
                if (candidate.equals(key)) {
                    return this.table[index + 1];
                }
                i++;
            }
        }

        public int size() {
            return this.entries.length;
        }

        public boolean isEmpty() {
            return false;
        }

        public boolean containsKey(Object key) {
            return get(key) != null;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean containsValue(java.lang.Object r7) {
            /*
                r6 = this;
                r5 = 0
                if (r7 != 0) goto L_0x0005
                r4 = r5
            L_0x0004:
                return r4
            L_0x0005:
                java.util.Map$Entry<K, V>[] r0 = r6.entries
                int r3 = r0.length
                r2 = 0
            L_0x0009:
                if (r2 >= r3) goto L_0x001c
                r1 = r0[r2]
                java.lang.Object r4 = r1.getValue()
                boolean r4 = r4.equals(r7)
                if (r4 == 0) goto L_0x0019
                r4 = 1
                goto L_0x0004
            L_0x0019:
                int r2 = r2 + 1
                goto L_0x0009
            L_0x001c:
                r4 = r5
                goto L_0x0004
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.ImmutableMap.RegularImmutableMap.containsValue(java.lang.Object):boolean");
        }

        public ImmutableSet<Map.Entry<K, V>> entrySet() {
            ImmutableSet<Map.Entry<K, V>> es = this.entrySet;
            if (es != null) {
                return es;
            }
            EntrySet entrySet2 = new EntrySet(this);
            this.entrySet = entrySet2;
            return entrySet2;
        }

        private static class EntrySet<K, V> extends ImmutableSet.ArrayImmutableSet<Map.Entry<K, V>> {
            final RegularImmutableMap<K, V> map;

            EntrySet(RegularImmutableMap<K, V> map2) {
                super(map2.entries);
                this.map = map2;
            }

            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public boolean contains(java.lang.Object r7) {
                /*
                    r6 = this;
                    r5 = 0
                    boolean r3 = r7 instanceof java.util.Map.Entry
                    if (r3 == 0) goto L_0x0023
                    r0 = r7
                    java.util.Map$Entry r0 = (java.util.Map.Entry) r0
                    r1 = r0
                    com.google.inject.internal.ImmutableMap$RegularImmutableMap<K, V> r3 = r6.map
                    java.lang.Object r4 = r1.getKey()
                    java.lang.Object r2 = r3.get(r4)
                    if (r2 == 0) goto L_0x0021
                    java.lang.Object r3 = r1.getValue()
                    boolean r3 = r2.equals(r3)
                    if (r3 == 0) goto L_0x0021
                    r3 = 1
                L_0x0020:
                    return r3
                L_0x0021:
                    r3 = r5
                    goto L_0x0020
                L_0x0023:
                    r3 = r5
                    goto L_0x0020
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.ImmutableMap.RegularImmutableMap.EntrySet.contains(java.lang.Object):boolean");
            }
        }

        public ImmutableSet<K> keySet() {
            ImmutableSet<K> ks = this.keySet;
            if (ks != null) {
                return ks;
            }
            KeySet keySet2 = new KeySet(this);
            this.keySet = keySet2;
            return keySet2;
        }

        private static class KeySet<K, V> extends ImmutableSet.TransformedImmutableSet<Map.Entry<K, V>, K> {
            final RegularImmutableMap<K, V> map;

            /* access modifiers changed from: package-private */
            public /* bridge */ /* synthetic */ Object transform(Object x0) {
                return transform((Map.Entry) ((Map.Entry) x0));
            }

            KeySet(RegularImmutableMap<K, V> map2) {
                super(map2.entries, map2.keySetHashCode);
                this.map = map2;
            }

            /* access modifiers changed from: package-private */
            public K transform(Map.Entry<K, V> element) {
                return element.getKey();
            }

            public boolean contains(Object target) {
                return this.map.containsKey(target);
            }
        }

        public ImmutableCollection<V> values() {
            ImmutableCollection<V> v = this.values;
            if (v != null) {
                return v;
            }
            Values values2 = new Values(this);
            this.values = values2;
            return values2;
        }

        private static class Values<V> extends ImmutableCollection<V> {
            final RegularImmutableMap<?, V> map;

            Values(RegularImmutableMap<?, V> map2) {
                this.map = map2;
            }

            public int size() {
                return this.map.entries.length;
            }

            public boolean isEmpty() {
                return false;
            }

            public UnmodifiableIterator<V> iterator() {
                return Iterators.unmodifiableIterator(new AbstractIterator<V>() {
                    int index = 0;

                    /* access modifiers changed from: protected */
                    public V computeNext() {
                        if (this.index >= Values.this.map.entries.length) {
                            return endOfData();
                        }
                        Map.Entry[] access$500 = Values.this.map.entries;
                        int i = this.index;
                        this.index = i + 1;
                        return access$500[i].getValue();
                    }
                });
            }

            public boolean contains(Object target) {
                return this.map.containsValue(target);
            }
        }

        public String toString() {
            StringBuilder result = new StringBuilder(size() * 16).append('{').append(this.entries[0]);
            for (int e = 1; e < this.entries.length; e++) {
                result.append(", ").append(this.entries[e].toString());
            }
            return result.append('}').toString();
        }
    }

    private static class SerializedForm implements Serializable {
        private static final long serialVersionUID = 0;
        final Object[] keys;
        final Object[] values;

        SerializedForm(ImmutableMap<?, ?> map) {
            this.keys = new Object[map.size()];
            this.values = new Object[map.size()];
            int i = 0;
            Iterator i$ = map.entrySet().iterator();
            while (i$.hasNext()) {
                Map.Entry<?, ?> entry = (Map.Entry) i$.next();
                this.keys[i] = entry.getKey();
                this.values[i] = entry.getValue();
                i++;
            }
        }

        /* access modifiers changed from: package-private */
        public Object readResolve() {
            Builder<Object, Object> builder = new Builder<>();
            for (int i = 0; i < this.keys.length; i++) {
                builder.put(this.keys[i], this.values[i]);
            }
            return builder.build();
        }
    }

    /* access modifiers changed from: package-private */
    public Object writeReplace() {
        return new SerializedForm(this);
    }
}
