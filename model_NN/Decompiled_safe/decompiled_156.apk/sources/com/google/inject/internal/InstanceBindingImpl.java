package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.spi.BindingTargetVisitor;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.HasDependencies;
import com.google.inject.spi.InjectionPoint;
import com.google.inject.spi.InstanceBinding;
import com.google.inject.util.Providers;
import java.util.Set;

public class InstanceBindingImpl<T> extends BindingImpl<T> implements InstanceBinding<T> {
    final ImmutableSet<InjectionPoint> injectionPoints;
    final T instance;
    final Provider<T> provider;

    public InstanceBindingImpl(Injector injector, Key<T> key, Object source, InternalFactory<? extends T> internalFactory, Set<InjectionPoint> injectionPoints2, T instance2) {
        super(injector, key, source, internalFactory, Scoping.UNSCOPED);
        this.injectionPoints = ImmutableSet.copyOf(injectionPoints2);
        this.instance = instance2;
        this.provider = Providers.of(instance2);
    }

    public InstanceBindingImpl(Object source, Key<T> key, Scoping scoping, Set<InjectionPoint> injectionPoints2, T instance2) {
        super(source, key, scoping);
        this.injectionPoints = ImmutableSet.copyOf(injectionPoints2);
        this.instance = instance2;
        this.provider = Providers.of(instance2);
    }

    public Provider<T> getProvider() {
        return this.provider;
    }

    public <V> V acceptTargetVisitor(BindingTargetVisitor<? super T, V> visitor) {
        return visitor.visit(this);
    }

    public T getInstance() {
        return this.instance;
    }

    public Set<InjectionPoint> getInjectionPoints() {
        return this.injectionPoints;
    }

    public Set<Dependency<?>> getDependencies() {
        return this.instance instanceof HasDependencies ? ImmutableSet.copyOf(this.instance.getDependencies()) : Dependency.forInjectionPoints(this.injectionPoints);
    }

    public BindingImpl<T> withScoping(Scoping scoping) {
        return new InstanceBindingImpl(getSource(), getKey(), scoping, this.injectionPoints, this.instance);
    }

    public BindingImpl<T> withKey(Key<T> key) {
        return new InstanceBindingImpl(getSource(), key, getScoping(), this.injectionPoints, this.instance);
    }

    public void applyTo(Binder binder) {
        binder.withSource(getSource()).bind(getKey()).toInstance(this.instance);
    }

    public String toString() {
        return new ToStringBuilder(InstanceBinding.class).add("key", getKey()).add("source", getSource()).add("instance", this.instance).toString();
    }
}
