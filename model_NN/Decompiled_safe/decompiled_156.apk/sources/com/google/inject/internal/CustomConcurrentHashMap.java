package com.google.inject.internal;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.locks.ReentrantLock;

final class CustomConcurrentHashMap {

    public interface ComputingStrategy<K, V, E> extends Strategy<K, V, E> {
        V compute(K k, E e, Function<? super K, ? extends V> function);

        V waitForValue(E e) throws InterruptedException;
    }

    public interface Internals<K, V, E> {
        E getEntry(K k);

        boolean removeEntry(E e);

        boolean removeEntry(E e, @Nullable V v);
    }

    public interface Strategy<K, V, E> {
        E copyEntry(K k, E e, E e2);

        boolean equalKeys(K k, Object obj);

        boolean equalValues(V v, Object obj);

        int getHash(E e);

        K getKey(E e);

        E getNext(E e);

        V getValue(E e);

        int hashKey(Object obj);

        E newEntry(K k, int i, E e);

        void setInternals(Internals<K, V, E> internals);

        void setValue(E e, V v);
    }

    private CustomConcurrentHashMap() {
    }

    static final class Builder {
        int concurrencyLevel = 16;
        int initialCapacity = 16;
        float loadFactor = 0.75f;

        Builder() {
        }

        public Builder loadFactor(float loadFactor2) {
            if (loadFactor2 <= 0.0f) {
                throw new IllegalArgumentException();
            }
            this.loadFactor = loadFactor2;
            return this;
        }

        public Builder initialCapacity(int initialCapacity2) {
            if (initialCapacity2 < 0) {
                throw new IllegalArgumentException();
            }
            this.initialCapacity = initialCapacity2;
            return this;
        }

        public Builder concurrencyLevel(int concurrencyLevel2) {
            if (concurrencyLevel2 <= 0) {
                throw new IllegalArgumentException();
            }
            this.concurrencyLevel = concurrencyLevel2;
            return this;
        }

        public <K, V, E> ConcurrentMap<K, V> buildMap(Strategy<K, V, E> strategy) {
            if (strategy != null) {
                return new Impl(strategy, this);
            }
            throw new NullPointerException("strategy");
        }

        public <K, V, E> ConcurrentMap<K, V> buildComputingMap(ComputingStrategy<K, V, E> strategy, Function<? super K, ? extends V> computer) {
            if (strategy == null) {
                throw new NullPointerException("strategy");
            } else if (computer != null) {
                return new ComputingImpl(strategy, this, computer);
            } else {
                throw new NullPointerException("computer");
            }
        }
    }

    /* access modifiers changed from: private */
    public static int rehash(int h) {
        int h2 = h + ((h << 15) ^ -12931);
        int h3 = h2 ^ (h2 >>> 10);
        int h4 = h3 + (h3 << 3);
        int h5 = h4 ^ (h4 >>> 6);
        int h6 = h5 + (h5 << 2) + (h5 << 14);
        return (h6 >>> 16) ^ h6;
    }

    static class Impl<K, V, E> extends AbstractMap<K, V> implements ConcurrentMap<K, V>, Serializable {
        static final int MAXIMUM_CAPACITY = 1073741824;
        static final int MAX_SEGMENTS = 65536;
        static final int RETRIES_BEFORE_LOCK = 2;
        private static final long serialVersionUID = 0;
        Set<Map.Entry<K, V>> entrySet;
        Set<K> keySet;
        final float loadFactor;
        final int segmentMask;
        final int segmentShift;
        final Impl<K, V, E>.Segment[] segments;
        final Strategy<K, V, E> strategy;
        Collection<V> values;

        Impl(Strategy<K, V, E> strategy2, Builder builder) {
            this.loadFactor = builder.loadFactor;
            int concurrencyLevel = builder.concurrencyLevel;
            int initialCapacity = builder.initialCapacity;
            int segmentShift2 = 0;
            int segmentCount = 1;
            while (segmentCount < (concurrencyLevel > MAX_SEGMENTS ? MAX_SEGMENTS : concurrencyLevel)) {
                segmentShift2++;
                segmentCount <<= 1;
            }
            this.segmentShift = 32 - segmentShift2;
            this.segmentMask = segmentCount - 1;
            this.segments = newSegmentArray(segmentCount);
            initialCapacity = initialCapacity > MAXIMUM_CAPACITY ? MAXIMUM_CAPACITY : initialCapacity;
            int segmentCapacity = initialCapacity / segmentCount;
            int segmentSize = 1;
            while (segmentSize < (segmentCapacity * segmentCount < initialCapacity ? segmentCapacity + 1 : segmentCapacity)) {
                segmentSize <<= 1;
            }
            for (int i = 0; i < this.segments.length; i++) {
                this.segments[i] = new Segment(segmentSize);
            }
            this.strategy = strategy2;
            strategy2.setInternals(new InternalsImpl());
        }

        /* access modifiers changed from: package-private */
        public int hash(Object key) {
            return CustomConcurrentHashMap.rehash(this.strategy.hashKey(key));
        }

        class InternalsImpl implements Internals<K, V, E>, Serializable {
            static final long serialVersionUID = 0;

            InternalsImpl() {
            }

            public E getEntry(K key) {
                if (key == null) {
                    throw new NullPointerException("key");
                }
                int hash = Impl.this.hash(key);
                return Impl.this.segmentFor(hash).getEntry(key, hash);
            }

            public boolean removeEntry(E entry, V value) {
                if (entry == null) {
                    throw new NullPointerException("entry");
                }
                int hash = Impl.this.strategy.getHash(entry);
                return Impl.this.segmentFor(hash).removeEntry(entry, hash, value);
            }

            public boolean removeEntry(E entry) {
                if (entry == null) {
                    throw new NullPointerException("entry");
                }
                int hash = Impl.this.strategy.getHash(entry);
                return Impl.this.segmentFor(hash).removeEntry(entry, hash);
            }
        }

        /* access modifiers changed from: package-private */
        public Impl<K, V, E>.Segment[] newSegmentArray(int ssize) {
            return (Segment[]) Array.newInstance(Segment.class, ssize);
        }

        /* access modifiers changed from: package-private */
        public Impl<K, V, E>.Segment segmentFor(int hash) {
            return this.segments[(hash >>> this.segmentShift) & this.segmentMask];
        }

        final class Segment extends ReentrantLock {
            volatile int count;
            int modCount;
            volatile AtomicReferenceArray<E> table;
            int threshold;

            Segment(int initialCapacity) {
                setTable(newEntryArray(initialCapacity));
            }

            /* access modifiers changed from: package-private */
            public AtomicReferenceArray<E> newEntryArray(int size) {
                return new AtomicReferenceArray<>(size);
            }

            /* access modifiers changed from: package-private */
            public void setTable(AtomicReferenceArray<E> newTable) {
                this.threshold = (int) (((float) newTable.length()) * Impl.this.loadFactor);
                this.table = newTable;
            }

            /* access modifiers changed from: package-private */
            public E getFirst(int hash) {
                AtomicReferenceArray<E> table2 = this.table;
                return table2.get((table2.length() - 1) & hash);
            }

            public E getEntry(Object key, int hash) {
                K entryKey;
                Strategy<K, V, E> s = Impl.this.strategy;
                if (this.count != 0) {
                    for (E e = getFirst(hash); e != null; e = s.getNext(e)) {
                        if (s.getHash(e) == hash && (entryKey = s.getKey(e)) != null && s.equalKeys(entryKey, key)) {
                            return e;
                        }
                    }
                }
                return null;
            }

            /* access modifiers changed from: package-private */
            public V get(Object key, int hash) {
                E entry = getEntry(key, hash);
                if (entry == null) {
                    return null;
                }
                return Impl.this.strategy.getValue(entry);
            }

            /* access modifiers changed from: package-private */
            public boolean containsKey(Object key, int hash) {
                K entryKey;
                Strategy<K, V, E> s = Impl.this.strategy;
                if (this.count != 0) {
                    E e = getFirst(hash);
                    while (e != null) {
                        if (s.getHash(e) != hash || (entryKey = s.getKey(e)) == null || !s.equalKeys(entryKey, key)) {
                            e = s.getNext(e);
                        } else if (s.getValue(e) != null) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
                return false;
            }

            /* access modifiers changed from: package-private */
            public boolean containsValue(Object value) {
                Strategy<K, V, E> s = Impl.this.strategy;
                if (this.count != 0) {
                    AtomicReferenceArray<E> table2 = this.table;
                    int length = table2.length();
                    for (int i = 0; i < length; i++) {
                        for (E e = table2.get(i); e != null; e = s.getNext(e)) {
                            V entryValue = s.getValue(e);
                            if (entryValue != null && s.equalValues(entryValue, value)) {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }

            /* access modifiers changed from: package-private */
            public boolean replace(K key, int hash, V oldValue, V newValue) {
                Strategy<K, V, E> s = Impl.this.strategy;
                lock();
                try {
                    for (E e = getFirst(hash); e != null; e = s.getNext(e)) {
                        K entryKey = s.getKey(e);
                        if (s.getHash(e) == hash && entryKey != null && s.equalKeys(key, entryKey)) {
                            V entryValue = s.getValue(e);
                            if (entryValue == null) {
                                unlock();
                                return false;
                            } else if (s.equalValues(entryValue, oldValue)) {
                                s.setValue(e, newValue);
                                return true;
                            }
                        }
                    }
                    unlock();
                    return false;
                } finally {
                    unlock();
                }
            }

            /* JADX INFO: finally extract failed */
            /* access modifiers changed from: package-private */
            public V replace(K key, int hash, V newValue) {
                Strategy<K, V, E> s = Impl.this.strategy;
                lock();
                try {
                    E e = getFirst(hash);
                    while (e != null) {
                        K entryKey = s.getKey(e);
                        if (s.getHash(e) != hash || entryKey == null || !s.equalKeys(key, entryKey)) {
                            e = s.getNext(e);
                        } else {
                            V entryValue = s.getValue(e);
                            if (entryValue == null) {
                                unlock();
                                return null;
                            }
                            s.setValue(e, newValue);
                            unlock();
                            return entryValue;
                        }
                    }
                    unlock();
                    return null;
                } catch (Throwable th) {
                    unlock();
                    throw th;
                }
            }

            /* access modifiers changed from: package-private */
            public V put(K key, int hash, V value, boolean onlyIfAbsent) {
                Strategy<K, V, E> s = Impl.this.strategy;
                lock();
                int count2 = this.count;
                int count3 = count2 + 1;
                if (count2 > this.threshold) {
                    expand();
                }
                AtomicReferenceArray<E> table2 = this.table;
                int index = hash & (table2.length() - 1);
                E first = table2.get(index);
                E e = first;
                while (e != null) {
                    K entryKey = s.getKey(e);
                    if (s.getHash(e) != hash || entryKey == null || !s.equalKeys(key, entryKey)) {
                        try {
                            e = s.getNext(e);
                        } catch (Throwable th) {
                            unlock();
                            throw th;
                        }
                    } else {
                        V entryValue = s.getValue(e);
                        if (!onlyIfAbsent || entryValue == null) {
                            s.setValue(e, value);
                            unlock();
                            return entryValue;
                        }
                        unlock();
                        return entryValue;
                    }
                }
                this.modCount++;
                E newEntry = s.newEntry(key, hash, first);
                s.setValue(newEntry, value);
                table2.set(index, newEntry);
                this.count = count3;
                unlock();
                return null;
            }

            /* access modifiers changed from: package-private */
            public void expand() {
                AtomicReferenceArray<E> oldTable = this.table;
                int oldCapacity = oldTable.length();
                if (oldCapacity < Impl.MAXIMUM_CAPACITY) {
                    Strategy<K, V, E> s = Impl.this.strategy;
                    AtomicReferenceArray<E> newTable = newEntryArray(oldCapacity << 1);
                    this.threshold = (int) (((float) newTable.length()) * Impl.this.loadFactor);
                    int newMask = newTable.length() - 1;
                    for (int oldIndex = 0; oldIndex < oldCapacity; oldIndex++) {
                        E head = oldTable.get(oldIndex);
                        if (head != null) {
                            E next = s.getNext(head);
                            int headIndex = s.getHash(head) & newMask;
                            if (next == null) {
                                newTable.set(headIndex, head);
                            } else {
                                E tail = head;
                                int tailIndex = headIndex;
                                for (E last = next; last != null; last = s.getNext(last)) {
                                    int newIndex = s.getHash(last) & newMask;
                                    if (newIndex != tailIndex) {
                                        tailIndex = newIndex;
                                        tail = last;
                                    }
                                }
                                newTable.set(tailIndex, tail);
                                for (E e = head; e != tail; e = s.getNext(e)) {
                                    K key = s.getKey(e);
                                    if (key != null) {
                                        int newIndex2 = s.getHash(e) & newMask;
                                        newTable.set(newIndex2, s.copyEntry(key, e, newTable.get(newIndex2)));
                                    }
                                }
                            }
                        }
                    }
                    this.table = newTable;
                }
            }

            /* JADX INFO: finally extract failed */
            /* access modifiers changed from: package-private */
            public V remove(Object key, int hash) {
                Strategy<K, V, E> s = Impl.this.strategy;
                lock();
                try {
                    int count2 = this.count - 1;
                    AtomicReferenceArray<E> table2 = this.table;
                    int index = hash & (table2.length() - 1);
                    E first = table2.get(index);
                    E e = first;
                    while (e != null) {
                        K entryKey = s.getKey(e);
                        if (s.getHash(e) != hash || entryKey == null || !s.equalKeys(entryKey, key)) {
                            e = s.getNext(e);
                        } else {
                            V entryValue = Impl.this.strategy.getValue(e);
                            this.modCount++;
                            E newFirst = s.getNext(e);
                            for (E p = first; p != e; p = s.getNext(p)) {
                                K pKey = s.getKey(p);
                                if (pKey != null) {
                                    newFirst = s.copyEntry(pKey, p, newFirst);
                                }
                            }
                            table2.set(index, newFirst);
                            this.count = count2;
                            unlock();
                            return entryValue;
                        }
                    }
                    unlock();
                    return null;
                } catch (Throwable th) {
                    unlock();
                    throw th;
                }
            }

            /* access modifiers changed from: package-private */
            public boolean remove(Object key, int hash, Object value) {
                Strategy<K, V, E> s = Impl.this.strategy;
                lock();
                try {
                    int count2 = this.count - 1;
                    AtomicReferenceArray<E> table2 = this.table;
                    int index = hash & (table2.length() - 1);
                    E first = table2.get(index);
                    E e = first;
                    while (e != null) {
                        K entryKey = s.getKey(e);
                        if (s.getHash(e) != hash || entryKey == null || !s.equalKeys(entryKey, key)) {
                            e = s.getNext(e);
                        } else {
                            V entryValue = Impl.this.strategy.getValue(e);
                            if (value == entryValue || !(value == null || entryValue == null || !s.equalValues(entryValue, value))) {
                                this.modCount = this.modCount + 1;
                                E newFirst = s.getNext(e);
                                for (E p = first; p != e; p = s.getNext(p)) {
                                    K pKey = s.getKey(p);
                                    if (pKey != null) {
                                        newFirst = s.copyEntry(pKey, p, newFirst);
                                    }
                                }
                                table2.set(index, newFirst);
                                this.count = count2;
                                return true;
                            }
                            unlock();
                            return false;
                        }
                    }
                    unlock();
                    return false;
                } finally {
                    unlock();
                }
            }

            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: E
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public boolean removeEntry(E r13, int r14, V r15) {
                /*
                    r12 = this;
                    com.google.inject.internal.CustomConcurrentHashMap$Impl r10 = com.google.inject.internal.CustomConcurrentHashMap.Impl.this
                    com.google.inject.internal.CustomConcurrentHashMap$Strategy<K, V, E> r8 = r10.strategy
                    r12.lock()
                    int r10 = r12.count     // Catch:{ all -> 0x006c }
                    r11 = 1
                    int r0 = r10 - r11
                    java.util.concurrent.atomic.AtomicReferenceArray<E> r9 = r12.table     // Catch:{ all -> 0x006c }
                    int r10 = r9.length()     // Catch:{ all -> 0x006c }
                    r11 = 1
                    int r10 = r10 - r11
                    r4 = r14 & r10
                    java.lang.Object r3 = r9.get(r4)     // Catch:{ all -> 0x006c }
                    r1 = r3
                L_0x001b:
                    if (r1 == 0) goto L_0x0067
                    int r10 = r8.getHash(r1)     // Catch:{ all -> 0x006c }
                    if (r10 != r14) goto L_0x0062
                    boolean r10 = r13.equals(r1)     // Catch:{ all -> 0x006c }
                    if (r10 == 0) goto L_0x0062
                    java.lang.Object r2 = r8.getValue(r1)     // Catch:{ all -> 0x006c }
                    if (r2 == r15) goto L_0x0037
                    if (r15 == 0) goto L_0x005d
                    boolean r10 = r8.equalValues(r2, r15)     // Catch:{ all -> 0x006c }
                    if (r10 == 0) goto L_0x005d
                L_0x0037:
                    int r10 = r12.modCount     // Catch:{ all -> 0x006c }
                    int r10 = r10 + 1
                    r12.modCount = r10     // Catch:{ all -> 0x006c }
                    java.lang.Object r5 = r8.getNext(r1)     // Catch:{ all -> 0x006c }
                    r6 = r3
                L_0x0042:
                    if (r6 == r1) goto L_0x0053
                    java.lang.Object r7 = r8.getKey(r6)     // Catch:{ all -> 0x006c }
                    if (r7 == 0) goto L_0x004e
                    java.lang.Object r5 = r8.copyEntry(r7, r6, r5)     // Catch:{ all -> 0x006c }
                L_0x004e:
                    java.lang.Object r6 = r8.getNext(r6)     // Catch:{ all -> 0x006c }
                    goto L_0x0042
                L_0x0053:
                    r9.set(r4, r5)     // Catch:{ all -> 0x006c }
                    r12.count = r0     // Catch:{ all -> 0x006c }
                    r10 = 1
                    r12.unlock()
                L_0x005c:
                    return r10
                L_0x005d:
                    r10 = 0
                    r12.unlock()
                    goto L_0x005c
                L_0x0062:
                    java.lang.Object r1 = r8.getNext(r1)     // Catch:{ all -> 0x006c }
                    goto L_0x001b
                L_0x0067:
                    r10 = 0
                    r12.unlock()
                    goto L_0x005c
                L_0x006c:
                    r10 = move-exception
                    r12.unlock()
                    throw r10
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.CustomConcurrentHashMap.Impl.Segment.removeEntry(java.lang.Object, int, java.lang.Object):boolean");
            }

            /* JADX INFO: finally extract failed */
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: E
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public boolean removeEntry(E r12, int r13) {
                /*
                    r11 = this;
                    r10 = 1
                    com.google.inject.internal.CustomConcurrentHashMap$Impl r9 = com.google.inject.internal.CustomConcurrentHashMap.Impl.this
                    com.google.inject.internal.CustomConcurrentHashMap$Strategy<K, V, E> r7 = r9.strategy
                    r11.lock()
                    int r9 = r11.count     // Catch:{ all -> 0x0058 }
                    int r0 = r9 - r10
                    java.util.concurrent.atomic.AtomicReferenceArray<E> r8 = r11.table     // Catch:{ all -> 0x0058 }
                    int r9 = r8.length()     // Catch:{ all -> 0x0058 }
                    int r9 = r9 - r10
                    r3 = r13 & r9
                    java.lang.Object r2 = r8.get(r3)     // Catch:{ all -> 0x0058 }
                    r1 = r2
                L_0x001a:
                    if (r1 == 0) goto L_0x0053
                    int r9 = r7.getHash(r1)     // Catch:{ all -> 0x0058 }
                    if (r9 != r13) goto L_0x004e
                    boolean r9 = r12.equals(r1)     // Catch:{ all -> 0x0058 }
                    if (r9 == 0) goto L_0x004e
                    int r9 = r11.modCount     // Catch:{ all -> 0x0058 }
                    int r9 = r9 + 1
                    r11.modCount = r9     // Catch:{ all -> 0x0058 }
                    java.lang.Object r4 = r7.getNext(r1)     // Catch:{ all -> 0x0058 }
                    r5 = r2
                L_0x0033:
                    if (r5 == r1) goto L_0x0044
                    java.lang.Object r6 = r7.getKey(r5)     // Catch:{ all -> 0x0058 }
                    if (r6 == 0) goto L_0x003f
                    java.lang.Object r4 = r7.copyEntry(r6, r5, r4)     // Catch:{ all -> 0x0058 }
                L_0x003f:
                    java.lang.Object r5 = r7.getNext(r5)     // Catch:{ all -> 0x0058 }
                    goto L_0x0033
                L_0x0044:
                    r8.set(r3, r4)     // Catch:{ all -> 0x0058 }
                    r11.count = r0     // Catch:{ all -> 0x0058 }
                    r11.unlock()
                    r9 = r10
                L_0x004d:
                    return r9
                L_0x004e:
                    java.lang.Object r1 = r7.getNext(r1)     // Catch:{ all -> 0x0058 }
                    goto L_0x001a
                L_0x0053:
                    r9 = 0
                    r11.unlock()
                    goto L_0x004d
                L_0x0058:
                    r9 = move-exception
                    r11.unlock()
                    throw r9
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.CustomConcurrentHashMap.Impl.Segment.removeEntry(java.lang.Object, int):boolean");
            }

            /* access modifiers changed from: package-private */
            public void clear() {
                if (this.count != 0) {
                    lock();
                    try {
                        AtomicReferenceArray<E> table2 = this.table;
                        for (int i = 0; i < table2.length(); i++) {
                            table2.set(i, null);
                        }
                        this.modCount++;
                        this.count = 0;
                    } finally {
                        unlock();
                    }
                }
            }
        }

        public boolean isEmpty() {
            Impl<K, V, E>.Segment[] segments2 = this.segments;
            int[] mc = new int[segments2.length];
            int mcsum = 0;
            for (int i = 0; i < segments2.length; i++) {
                if (segments2[i].count != 0) {
                    return false;
                }
                int i2 = segments2[i].modCount;
                mc[i] = i2;
                mcsum += i2;
            }
            if (mcsum != 0) {
                for (int i3 = 0; i3 < segments2.length; i3++) {
                    if (segments2[i3].count != 0 || mc[i3] != segments2[i3].modCount) {
                        return false;
                    }
                }
            }
            return true;
        }

        public int size() {
            Impl<K, V, E>.Segment[] segments2 = this.segments;
            long sum = 0;
            long check = 0;
            int[] mc = new int[segments2.length];
            for (int k = 0; k < 2; k++) {
                check = 0;
                sum = 0;
                int mcsum = 0;
                for (int i = 0; i < segments2.length; i++) {
                    sum += (long) segments2[i].count;
                    int i2 = segments2[i].modCount;
                    mc[i] = i2;
                    mcsum += i2;
                }
                if (mcsum != 0) {
                    int i3 = 0;
                    while (true) {
                        if (i3 >= segments2.length) {
                            break;
                        }
                        check += (long) segments2[i3].count;
                        if (mc[i3] != segments2[i3].modCount) {
                            check = -1;
                            break;
                        }
                        i3++;
                    }
                }
                if (check == sum) {
                    break;
                }
            }
            if (check != sum) {
                long sum2 = 0;
                for (Impl<K, V, E>.Segment segment : segments2) {
                    segment.lock();
                }
                for (Impl<K, V, E>.Segment segment2 : segments2) {
                    sum2 = sum + ((long) segment2.count);
                }
                for (Impl<K, V, E>.Segment segment3 : segments2) {
                    segment3.unlock();
                }
            }
            if (sum > 2147483647L) {
                return Integer.MAX_VALUE;
            }
            return (int) sum;
        }

        public V get(Object key) {
            if (key == null) {
                throw new NullPointerException("key");
            }
            int hash = hash(key);
            return segmentFor(hash).get(key, hash);
        }

        public boolean containsKey(Object key) {
            if (key == null) {
                throw new NullPointerException("key");
            }
            int hash = hash(key);
            return segmentFor(hash).containsKey(key, hash);
        }

        /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
            jadx.core.utils.exceptions.JadxOverflowException: 
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
            */
        public boolean containsValue(java.lang.Object r15) {
            /*
                r14 = this;
                if (r15 != 0) goto L_0x000a
                java.lang.NullPointerException r12 = new java.lang.NullPointerException
                java.lang.String r13 = "value"
                r12.<init>(r13)
                throw r12
            L_0x000a:
                com.google.inject.internal.CustomConcurrentHashMap$Impl<K, V, E>$Segment[] r11 = r14.segments
                int r12 = r11.length
                int[] r8 = new int[r12]
                r6 = 0
            L_0x0010:
                r12 = 2
                if (r6 >= r12) goto L_0x004e
                r9 = 0
                r4 = 0
            L_0x0015:
                int r12 = r11.length
                if (r4 >= r12) goto L_0x0030
                r12 = r11[r4]
                int r1 = r12.count
                r12 = r11[r4]
                int r12 = r12.modCount
                r8[r4] = r12
                int r9 = r9 + r12
                r12 = r11[r4]
                boolean r12 = r12.containsValue(r15)
                if (r12 == 0) goto L_0x002d
                r12 = 1
            L_0x002c:
                return r12
            L_0x002d:
                int r4 = r4 + 1
                goto L_0x0015
            L_0x0030:
                r2 = 1
                if (r9 == 0) goto L_0x0044
                r4 = 0
            L_0x0034:
                int r12 = r11.length
                if (r4 >= r12) goto L_0x0044
                r12 = r11[r4]
                int r1 = r12.count
                r12 = r8[r4]
                r13 = r11[r4]
                int r13 = r13.modCount
                if (r12 == r13) goto L_0x0048
                r2 = 0
            L_0x0044:
                if (r2 == 0) goto L_0x004b
                r12 = 0
                goto L_0x002c
            L_0x0048:
                int r4 = r4 + 1
                goto L_0x0034
            L_0x004b:
                int r6 = r6 + 1
                goto L_0x0010
            L_0x004e:
                r0 = r11
                int r7 = r0.length
                r5 = 0
            L_0x0051:
                if (r5 >= r7) goto L_0x005b
                r10 = r0[r5]
                r10.lock()
                int r5 = r5 + 1
                goto L_0x0051
            L_0x005b:
                r3 = 0
                r0 = r11
                int r7 = r0.length     // Catch:{ all -> 0x007a }
                r5 = 0
            L_0x005f:
                if (r5 >= r7) goto L_0x006a
                r10 = r0[r5]     // Catch:{ all -> 0x007a }
                boolean r12 = r10.containsValue(r15)     // Catch:{ all -> 0x007a }
                if (r12 == 0) goto L_0x0077
                r3 = 1
            L_0x006a:
                r0 = r11
                int r7 = r0.length
                r5 = 0
            L_0x006d:
                if (r5 >= r7) goto L_0x0089
                r10 = r0[r5]
                r10.unlock()
                int r5 = r5 + 1
                goto L_0x006d
            L_0x0077:
                int r5 = r5 + 1
                goto L_0x005f
            L_0x007a:
                r12 = move-exception
                r0 = r11
                int r7 = r0.length
                r5 = 0
            L_0x007e:
                if (r5 >= r7) goto L_0x0088
                r10 = r0[r5]
                r10.unlock()
                int r5 = r5 + 1
                goto L_0x007e
            L_0x0088:
                throw r12
            L_0x0089:
                r12 = r3
                goto L_0x002c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.CustomConcurrentHashMap.Impl.containsValue(java.lang.Object):boolean");
        }

        public V put(K key, V value) {
            if (key == null) {
                throw new NullPointerException("key");
            } else if (value == null) {
                throw new NullPointerException("value");
            } else {
                int hash = hash(key);
                return segmentFor(hash).put(key, hash, value, false);
            }
        }

        public V putIfAbsent(K key, V value) {
            if (key == null) {
                throw new NullPointerException("key");
            } else if (value == null) {
                throw new NullPointerException("value");
            } else {
                int hash = hash(key);
                return segmentFor(hash).put(key, hash, value, true);
            }
        }

        public void putAll(Map<? extends K, ? extends V> m) {
            for (Map.Entry<? extends K, ? extends V> e : m.entrySet()) {
                put(e.getKey(), e.getValue());
            }
        }

        public V remove(Object key) {
            if (key == null) {
                throw new NullPointerException("key");
            }
            int hash = hash(key);
            return segmentFor(hash).remove(key, hash);
        }

        public boolean remove(Object key, Object value) {
            if (key == null) {
                throw new NullPointerException("key");
            }
            int hash = hash(key);
            return segmentFor(hash).remove(key, hash, value);
        }

        public boolean replace(K key, V oldValue, V newValue) {
            if (key == null) {
                throw new NullPointerException("key");
            } else if (oldValue == null) {
                throw new NullPointerException("oldValue");
            } else if (newValue == null) {
                throw new NullPointerException("newValue");
            } else {
                int hash = hash(key);
                return segmentFor(hash).replace(key, hash, oldValue, newValue);
            }
        }

        public V replace(K key, V value) {
            if (key == null) {
                throw new NullPointerException("key");
            } else if (value == null) {
                throw new NullPointerException("value");
            } else {
                int hash = hash(key);
                return segmentFor(hash).replace(key, hash, value);
            }
        }

        public void clear() {
            for (Impl<K, V, E>.Segment segment : this.segments) {
                segment.clear();
            }
        }

        public Set<K> keySet() {
            Set<K> ks = this.keySet;
            if (ks != null) {
                return ks;
            }
            KeySet keySet2 = new KeySet();
            this.keySet = keySet2;
            return keySet2;
        }

        public Collection<V> values() {
            Collection<V> vs = this.values;
            if (vs != null) {
                return vs;
            }
            Values values2 = new Values();
            this.values = values2;
            return values2;
        }

        public Set<Map.Entry<K, V>> entrySet() {
            Set<Map.Entry<K, V>> es = this.entrySet;
            if (es != null) {
                return es;
            }
            EntrySet entrySet2 = new EntrySet();
            this.entrySet = entrySet2;
            return entrySet2;
        }

        abstract class HashIterator {
            AtomicReferenceArray<E> currentTable;
            Impl<K, V, E>.WriteThroughEntry lastReturned;
            E nextEntry;
            Impl<K, V, E>.WriteThroughEntry nextExternal;
            int nextSegmentIndex;
            int nextTableIndex = -1;

            HashIterator() {
                this.nextSegmentIndex = Impl.this.segments.length - 1;
                advance();
            }

            public boolean hasMoreElements() {
                return hasNext();
            }

            /* access modifiers changed from: package-private */
            public final void advance() {
                this.nextExternal = null;
                if (!nextInChain() && !nextInTable()) {
                    while (this.nextSegmentIndex >= 0) {
                        Impl<K, V, E>.Segment[] segmentArr = Impl.this.segments;
                        int i = this.nextSegmentIndex;
                        this.nextSegmentIndex = i - 1;
                        Impl<K, V, E>.Segment seg = segmentArr[i];
                        if (seg.count != 0) {
                            this.currentTable = seg.table;
                            this.nextTableIndex = this.currentTable.length() - 1;
                            if (nextInTable()) {
                                return;
                            }
                        }
                    }
                }
            }

            /* access modifiers changed from: package-private */
            public boolean nextInChain() {
                Strategy<K, V, E> s = Impl.this.strategy;
                if (this.nextEntry != null) {
                    this.nextEntry = s.getNext(this.nextEntry);
                    while (this.nextEntry != null) {
                        if (advanceTo(this.nextEntry)) {
                            return true;
                        }
                        this.nextEntry = s.getNext(this.nextEntry);
                    }
                }
                return false;
            }

            /* access modifiers changed from: package-private */
            public boolean nextInTable() {
                while (this.nextTableIndex >= 0) {
                    AtomicReferenceArray<E> atomicReferenceArray = this.currentTable;
                    int i = this.nextTableIndex;
                    this.nextTableIndex = i - 1;
                    E e = atomicReferenceArray.get(i);
                    this.nextEntry = e;
                    if (e != null && (advanceTo(this.nextEntry) || nextInChain())) {
                        return true;
                    }
                }
                return false;
            }

            /* access modifiers changed from: package-private */
            public boolean advanceTo(E entry) {
                Strategy<K, V, E> s = Impl.this.strategy;
                K key = s.getKey(entry);
                V value = s.getValue(entry);
                if (key == null || value == null) {
                    return false;
                }
                this.nextExternal = new WriteThroughEntry(key, value);
                return true;
            }

            public boolean hasNext() {
                return this.nextExternal != null;
            }

            /* access modifiers changed from: package-private */
            public Impl<K, V, E>.WriteThroughEntry nextEntry() {
                if (this.nextExternal == null) {
                    throw new NoSuchElementException();
                }
                this.lastReturned = this.nextExternal;
                advance();
                return this.lastReturned;
            }

            public void remove() {
                if (this.lastReturned == null) {
                    throw new IllegalStateException();
                }
                Impl.this.remove(this.lastReturned.getKey());
                this.lastReturned = null;
            }
        }

        final class KeyIterator extends Impl<K, V, E>.HashIterator implements Iterator<K> {
            KeyIterator() {
                super();
            }

            public K next() {
                return super.nextEntry().getKey();
            }
        }

        final class ValueIterator extends Impl<K, V, E>.HashIterator implements Iterator<V> {
            ValueIterator() {
                super();
            }

            public V next() {
                return super.nextEntry().getValue();
            }
        }

        final class WriteThroughEntry extends AbstractMapEntry<K, V> {
            final K key;
            V value;

            WriteThroughEntry(K key2, V value2) {
                this.key = key2;
                this.value = value2;
            }

            public K getKey() {
                return this.key;
            }

            public V getValue() {
                return this.value;
            }

            public V setValue(V value2) {
                if (value2 == null) {
                    throw new NullPointerException();
                }
                V oldValue = Impl.this.put(getKey(), value2);
                this.value = value2;
                return oldValue;
            }
        }

        final class EntryIterator extends Impl<K, V, E>.HashIterator implements Iterator<Map.Entry<K, V>> {
            EntryIterator() {
                super();
            }

            public Map.Entry<K, V> next() {
                return nextEntry();
            }
        }

        final class KeySet extends AbstractSet<K> {
            KeySet() {
            }

            public Iterator<K> iterator() {
                return new KeyIterator();
            }

            public int size() {
                return Impl.this.size();
            }

            public boolean isEmpty() {
                return Impl.this.isEmpty();
            }

            public boolean contains(Object o) {
                return Impl.this.containsKey(o);
            }

            public boolean remove(Object o) {
                return Impl.this.remove(o) != null;
            }

            public void clear() {
                Impl.this.clear();
            }
        }

        final class Values extends AbstractCollection<V> {
            Values() {
            }

            public Iterator<V> iterator() {
                return new ValueIterator();
            }

            public int size() {
                return Impl.this.size();
            }

            public boolean isEmpty() {
                return Impl.this.isEmpty();
            }

            public boolean contains(Object o) {
                return Impl.this.containsValue(o);
            }

            public void clear() {
                Impl.this.clear();
            }
        }

        final class EntrySet extends AbstractSet<Map.Entry<K, V>> {
            EntrySet() {
            }

            public Iterator<Map.Entry<K, V>> iterator() {
                return new EntryIterator();
            }

            public boolean contains(Object o) {
                if (!(o instanceof Map.Entry)) {
                    return false;
                }
                Map.Entry entry = (Map.Entry) o;
                Object key = entry.getKey();
                if (key == null) {
                    return false;
                }
                V v = Impl.this.get(key);
                return v != null && Impl.this.strategy.equalValues(v, entry.getValue());
            }

            public boolean remove(Object o) {
                if (!(o instanceof Map.Entry)) {
                    return false;
                }
                Map.Entry entry = (Map.Entry) o;
                Object key = entry.getKey();
                return key != null && Impl.this.remove(key, entry.getValue());
            }

            public int size() {
                return Impl.this.size();
            }

            public boolean isEmpty() {
                return Impl.this.isEmpty();
            }

            public void clear() {
                Impl.this.clear();
            }
        }

        private void writeObject(ObjectOutputStream out) throws IOException {
            out.writeInt(size());
            out.writeFloat(this.loadFactor);
            out.writeInt(this.segments.length);
            out.writeObject(this.strategy);
            for (Map.Entry<K, V> entry : entrySet()) {
                out.writeObject(entry.getKey());
                out.writeObject(entry.getValue());
            }
            out.writeObject(null);
        }

        static class Fields {
            static final Field loadFactor = findField("loadFactor");
            static final Field segmentMask = findField("segmentMask");
            static final Field segmentShift = findField("segmentShift");
            static final Field segments = findField("segments");
            static final Field strategy = findField("strategy");

            Fields() {
            }

            static Field findField(String name) {
                try {
                    Field f = Impl.class.getDeclaredField(name);
                    f.setAccessible(true);
                    return f;
                } catch (NoSuchFieldException e) {
                    throw new AssertionError(e);
                }
            }
        }

        private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
            try {
                int initialCapacity = in.readInt();
                float loadFactor2 = in.readFloat();
                int concurrencyLevel = in.readInt();
                Strategy<K, V, E> strategy2 = (Strategy) in.readObject();
                Fields.loadFactor.set(this, Float.valueOf(loadFactor2));
                if (concurrencyLevel > MAX_SEGMENTS) {
                    concurrencyLevel = MAX_SEGMENTS;
                }
                int segmentShift2 = 0;
                int segmentCount = 1;
                while (segmentCount < concurrencyLevel) {
                    segmentShift2++;
                    segmentCount <<= 1;
                }
                Fields.segmentShift.set(this, Integer.valueOf(32 - segmentShift2));
                Fields.segmentMask.set(this, Integer.valueOf(segmentCount - 1));
                Fields.segments.set(this, newSegmentArray(segmentCount));
                if (initialCapacity > MAXIMUM_CAPACITY) {
                    initialCapacity = MAXIMUM_CAPACITY;
                }
                int segmentCapacity = initialCapacity / segmentCount;
                if (segmentCapacity * segmentCount < initialCapacity) {
                    segmentCapacity++;
                }
                int segmentSize = 1;
                while (segmentSize < segmentCapacity) {
                    segmentSize <<= 1;
                }
                for (int i = 0; i < this.segments.length; i++) {
                    this.segments[i] = new Segment(segmentSize);
                }
                Fields.strategy.set(this, strategy2);
                while (true) {
                    K key = in.readObject();
                    if (key != null) {
                        put(key, in.readObject());
                    } else {
                        return;
                    }
                }
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            }
        }
    }

    static class ComputingImpl<K, V, E> extends Impl<K, V, E> {
        static final long serialVersionUID = 0;
        final Function<? super K, ? extends V> computer;
        final ComputingStrategy<K, V, E> computingStrategy;

        ComputingImpl(ComputingStrategy<K, V, E> strategy, Builder builder, Function<? super K, ? extends V> computer2) {
            super(strategy, builder);
            this.computingStrategy = strategy;
            this.computer = computer2;
        }

        public V get(Object k) {
            V value;
            Object obj = k;
            if (obj == null) {
                throw new NullPointerException("key");
            }
            int hash = hash(obj);
            Impl<K, V, E>.Segment segment = segmentFor(hash);
            while (true) {
                E entry = segment.getEntry(obj, hash);
                if (entry == null) {
                    boolean created = false;
                    segment.lock();
                    try {
                        entry = segment.getEntry(obj, hash);
                        if (entry == null) {
                            created = true;
                            int count = segment.count;
                            int count2 = count + 1;
                            if (count > segment.threshold) {
                                segment.expand();
                            }
                            AtomicReferenceArray<E> table = segment.table;
                            int index = hash & (table.length() - 1);
                            E first = table.get(index);
                            segment.modCount++;
                            entry = this.computingStrategy.newEntry(obj, hash, first);
                            table.set(index, entry);
                            segment.count = count2;
                        }
                        if (created) {
                            boolean success = false;
                            try {
                                value = this.computingStrategy.compute(obj, entry, this.computer);
                                if (value == null) {
                                    throw new NullPointerException("compute() returned null unexpectedly");
                                }
                                success = true;
                            } finally {
                                if (!success) {
                                    segment.removeEntry(entry, hash);
                                }
                            }
                        }
                    } finally {
                        segment.unlock();
                    }
                }
                boolean interrupted = false;
                while (true) {
                    try {
                        value = this.computingStrategy.waitForValue(entry);
                        break;
                    } catch (InterruptedException e) {
                        interrupted = true;
                    } catch (Throwable th) {
                        if (interrupted) {
                            Thread.currentThread().interrupt();
                        }
                        throw th;
                    }
                }
                if (value == null) {
                    segment.removeEntry(entry, hash);
                    if (interrupted) {
                        Thread.currentThread().interrupt();
                    }
                } else if (interrupted) {
                    Thread.currentThread().interrupt();
                }
            }
            return value;
        }
    }

    static class SimpleStrategy<K, V> implements Strategy<K, V, SimpleInternalEntry<K, V>> {
        SimpleStrategy() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.inject.internal.CustomConcurrentHashMap.SimpleStrategy.copyEntry(java.lang.Object, com.google.inject.internal.CustomConcurrentHashMap$SimpleInternalEntry, com.google.inject.internal.CustomConcurrentHashMap$SimpleInternalEntry):com.google.inject.internal.CustomConcurrentHashMap$SimpleInternalEntry<K, V>
         arg types: [java.lang.Object, java.lang.Object, java.lang.Object]
         candidates:
          com.google.inject.internal.CustomConcurrentHashMap.SimpleStrategy.copyEntry(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
          com.google.inject.internal.CustomConcurrentHashMap.Strategy.copyEntry(java.lang.Object, java.lang.Object, java.lang.Object):E
          com.google.inject.internal.CustomConcurrentHashMap.SimpleStrategy.copyEntry(java.lang.Object, com.google.inject.internal.CustomConcurrentHashMap$SimpleInternalEntry, com.google.inject.internal.CustomConcurrentHashMap$SimpleInternalEntry):com.google.inject.internal.CustomConcurrentHashMap$SimpleInternalEntry<K, V> */
        public /* bridge */ /* synthetic */ Object copyEntry(Object x0, Object x1, Object x2) {
            return copyEntry(x0, (SimpleInternalEntry) ((SimpleInternalEntry) x1), (SimpleInternalEntry) ((SimpleInternalEntry) x2));
        }

        public /* bridge */ /* synthetic */ int getHash(Object x0) {
            return getHash((SimpleInternalEntry) ((SimpleInternalEntry) x0));
        }

        public /* bridge */ /* synthetic */ Object getKey(Object x0) {
            return getKey((SimpleInternalEntry) ((SimpleInternalEntry) x0));
        }

        public /* bridge */ /* synthetic */ Object getNext(Object x0) {
            return getNext((SimpleInternalEntry) ((SimpleInternalEntry) x0));
        }

        public /* bridge */ /* synthetic */ Object getValue(Object x0) {
            return getValue((SimpleInternalEntry) ((SimpleInternalEntry) x0));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.inject.internal.CustomConcurrentHashMap.SimpleStrategy.newEntry(java.lang.Object, int, com.google.inject.internal.CustomConcurrentHashMap$SimpleInternalEntry):com.google.inject.internal.CustomConcurrentHashMap$SimpleInternalEntry<K, V>
         arg types: [java.lang.Object, int, java.lang.Object]
         candidates:
          com.google.inject.internal.CustomConcurrentHashMap.SimpleStrategy.newEntry(java.lang.Object, int, java.lang.Object):java.lang.Object
          com.google.inject.internal.CustomConcurrentHashMap.Strategy.newEntry(java.lang.Object, int, java.lang.Object):E
          com.google.inject.internal.CustomConcurrentHashMap.SimpleStrategy.newEntry(java.lang.Object, int, com.google.inject.internal.CustomConcurrentHashMap$SimpleInternalEntry):com.google.inject.internal.CustomConcurrentHashMap$SimpleInternalEntry<K, V> */
        public /* bridge */ /* synthetic */ Object newEntry(Object x0, int x1, Object x2) {
            return newEntry(x0, x1, (SimpleInternalEntry) ((SimpleInternalEntry) x2));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.inject.internal.CustomConcurrentHashMap.SimpleStrategy.setValue(com.google.inject.internal.CustomConcurrentHashMap$SimpleInternalEntry, java.lang.Object):void
         arg types: [java.lang.Object, java.lang.Object]
         candidates:
          com.google.inject.internal.CustomConcurrentHashMap.SimpleStrategy.setValue(java.lang.Object, java.lang.Object):void
          com.google.inject.internal.CustomConcurrentHashMap.Strategy.setValue(java.lang.Object, java.lang.Object):void
          com.google.inject.internal.CustomConcurrentHashMap.SimpleStrategy.setValue(com.google.inject.internal.CustomConcurrentHashMap$SimpleInternalEntry, java.lang.Object):void */
        public /* bridge */ /* synthetic */ void setValue(Object x0, Object x1) {
            setValue((SimpleInternalEntry) ((SimpleInternalEntry) x0), x1);
        }

        public SimpleInternalEntry<K, V> newEntry(K key, int hash, SimpleInternalEntry<K, V> next) {
            return new SimpleInternalEntry<>(key, hash, null, next);
        }

        public SimpleInternalEntry<K, V> copyEntry(K key, SimpleInternalEntry<K, V> original, SimpleInternalEntry<K, V> next) {
            return new SimpleInternalEntry<>(key, original.hash, original.value, next);
        }

        public void setValue(SimpleInternalEntry<K, V> entry, V value) {
            entry.value = value;
        }

        public V getValue(SimpleInternalEntry<K, V> entry) {
            return entry.value;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean equalKeys(K r2, java.lang.Object r3) {
            /*
                r1 = this;
                boolean r0 = r2.equals(r3)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.CustomConcurrentHashMap.SimpleStrategy.equalKeys(java.lang.Object, java.lang.Object):boolean");
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean equalValues(V r2, java.lang.Object r3) {
            /*
                r1 = this;
                boolean r0 = r2.equals(r3)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.CustomConcurrentHashMap.SimpleStrategy.equalValues(java.lang.Object, java.lang.Object):boolean");
        }

        public int hashKey(Object key) {
            return key.hashCode();
        }

        public K getKey(SimpleInternalEntry<K, V> entry) {
            return entry.key;
        }

        public SimpleInternalEntry<K, V> getNext(SimpleInternalEntry<K, V> entry) {
            return entry.next;
        }

        public int getHash(SimpleInternalEntry<K, V> entry) {
            return entry.hash;
        }

        public void setInternals(Internals<K, V, SimpleInternalEntry<K, V>> internals) {
        }
    }

    static class SimpleInternalEntry<K, V> {
        final int hash;
        final K key;
        final SimpleInternalEntry<K, V> next;
        volatile V value;

        SimpleInternalEntry(K key2, int hash2, @Nullable V value2, SimpleInternalEntry<K, V> next2) {
            this.key = key2;
            this.hash = hash2;
            this.value = value2;
            this.next = next2;
        }
    }
}
