package com.google.inject.internal;

class NullOutputException extends NullPointerException {
    public NullOutputException(String s) {
        super(s);
    }
}
