package com.google.inject;

import com.google.inject.internal.Annotations;
import com.google.inject.internal.BindingImpl;
import com.google.inject.internal.Classes;
import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.ImmutableList;
import com.google.inject.internal.ImmutableSet;
import com.google.inject.internal.InstanceBindingImpl;
import com.google.inject.internal.InternalContext;
import com.google.inject.internal.InternalFactory;
import com.google.inject.internal.LinkedBindingImpl;
import com.google.inject.internal.LinkedProviderBindingImpl;
import com.google.inject.internal.Lists;
import com.google.inject.internal.Maps;
import com.google.inject.internal.MatcherAndConverter;
import com.google.inject.internal.Nullable;
import com.google.inject.internal.Scoping;
import com.google.inject.internal.SourceProvider;
import com.google.inject.internal.ToStringBuilder;
import com.google.inject.spi.BindingTargetVisitor;
import com.google.inject.spi.ConvertedConstantBinding;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.ProviderBinding;
import com.google.inject.spi.ProviderKeyBinding;
import com.google.inject.util.Providers;
import java.lang.annotation.Annotation;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

class InjectorImpl implements Injector, Lookups {
    final BindingsMultimap bindingsMultimap = new BindingsMultimap();
    final ConstructorInjectorStore constructors = new ConstructorInjectorStore(this);
    final Initializer initializer;
    final Map<Key<?>, BindingImpl<?>> jitBindings = Maps.newHashMap();
    final ThreadLocal<Object[]> localContext;
    Lookups lookups = new DeferredLookups(this);
    MembersInjectorStore membersInjectorStore;
    final InjectorImpl parent;
    final State state;

    interface MethodInvoker {
        Object invoke(Object obj, Object... objArr) throws IllegalAccessException, InvocationTargetException;
    }

    InjectorImpl(@Nullable InjectorImpl parent2, State state2, Initializer initializer2) {
        this.parent = parent2;
        this.state = state2;
        this.initializer = initializer2;
        if (parent2 != null) {
            this.localContext = parent2.localContext;
        } else {
            this.localContext = new ThreadLocal<Object[]>() {
                /* access modifiers changed from: protected */
                public Object[] initialValue() {
                    return new Object[1];
                }
            };
        }
    }

    /* access modifiers changed from: package-private */
    public void index() {
        for (Binding<?> binding : this.state.getExplicitBindingsThisLevel().values()) {
            index(binding);
        }
    }

    /* access modifiers changed from: package-private */
    public <T> void index(Binding<T> binding) {
        this.bindingsMultimap.put(binding.getKey().getTypeLiteral(), binding);
    }

    public <T> List<Binding<T>> findBindingsByType(TypeLiteral<T> type) {
        return this.bindingsMultimap.getAll(type);
    }

    public <T> BindingImpl<T> getBinding(Key<T> key) {
        Errors errors = new Errors(key);
        try {
            BindingImpl<T> result = getBindingOrThrow(key, errors);
            errors.throwConfigurationExceptionIfErrorsExist();
            return result;
        } catch (ErrorsException e) {
            throw new ConfigurationException(errors.merge(e.getErrors()).getMessages());
        }
    }

    public <T> BindingImpl<T> getBindingOrThrow(Key<T> key, Errors errors) throws ErrorsException {
        BindingImpl<T> binding = this.state.getExplicitBinding(key);
        if (binding != null) {
            return binding;
        }
        return getJustInTimeBinding(key, errors);
    }

    public <T> Binding<T> getBinding(Class<T> type) {
        return getBinding((Key) Key.get((Class) type));
    }

    public Injector getParent() {
        return this.parent;
    }

    public Injector createChildInjector(Iterable<? extends Module> modules) {
        return new InjectorBuilder().parentInjector(this).addModules(modules).build();
    }

    public Injector createChildInjector(Module... modules) {
        return createChildInjector(ImmutableList.of((Object[]) modules));
    }

    private <T> BindingImpl<T> getJustInTimeBinding(Key<T> key, Errors errors) throws ErrorsException {
        synchronized (this.state.lock()) {
            for (InjectorImpl injector = this; injector != null; injector = injector.parent) {
                BindingImpl<T> binding = injector.jitBindings.get(key);
                if (binding != null) {
                    return binding;
                }
            }
            BindingImpl<T> createJustInTimeBindingRecursive = createJustInTimeBindingRecursive(key, errors);
            return createJustInTimeBindingRecursive;
        }
    }

    static boolean isProvider(Key<?> key) {
        return key.getTypeLiteral().getRawType().equals(Provider.class);
    }

    static boolean isMembersInjector(Key<?> key) {
        return key.getTypeLiteral().getRawType().equals(MembersInjector.class) && !key.hasAnnotationType();
    }

    private <T> BindingImpl<MembersInjector<T>> createMembersInjectorBinding(Key<MembersInjector<T>> key, Errors errors) throws ErrorsException {
        Type membersInjectorType = key.getTypeLiteral().getType();
        if (!(membersInjectorType instanceof ParameterizedType)) {
            throw errors.cannotInjectRawMembersInjector().toException();
        }
        MembersInjector<T> membersInjector = this.membersInjectorStore.get(TypeLiteral.get(((ParameterizedType) membersInjectorType).getActualTypeArguments()[0]), errors);
        return new InstanceBindingImpl(this, key, SourceProvider.UNKNOWN_SOURCE, new ConstantFactory<>(Initializables.of(membersInjector)), ImmutableSet.of(), membersInjector);
    }

    private <T> BindingImpl<Provider<T>> createProviderBinding(Key<Provider<T>> key, Errors errors) throws ErrorsException {
        Type providerType = key.getTypeLiteral().getType();
        if (providerType instanceof ParameterizedType) {
            return new ProviderBindingImpl(this, key, getBindingOrThrow(key.ofType(((ParameterizedType) providerType).getActualTypeArguments()[0]), errors));
        }
        throw errors.cannotInjectRawProvider().toException();
    }

    static class ProviderBindingImpl<T> extends BindingImpl<Provider<T>> implements ProviderBinding<Provider<T>> {
        final BindingImpl<T> providedBinding;

        ProviderBindingImpl(InjectorImpl injector, Key<Provider<T>> key, Binding<T> providedBinding2) {
            super(injector, key, providedBinding2.getSource(), createInternalFactory(providedBinding2), Scoping.UNSCOPED);
            this.providedBinding = (BindingImpl) providedBinding2;
        }

        static <T> InternalFactory<Provider<T>> createInternalFactory(Binding<T> providedBinding2) {
            final Provider<T> provider = providedBinding2.getProvider();
            return new InternalFactory<Provider<T>>() {
                public Provider<T> get(Errors errors, InternalContext context, Dependency dependency) {
                    return provider;
                }
            };
        }

        public Key<? extends T> getProvidedKey() {
            return this.providedBinding.getKey();
        }

        public <V> V acceptTargetVisitor(BindingTargetVisitor<? super Provider<T>, V> visitor) {
            return visitor.visit(this);
        }

        public void applyTo(Binder binder) {
            throw new UnsupportedOperationException("This element represents a synthetic binding.");
        }

        public String toString() {
            return new ToStringBuilder(ProviderKeyBinding.class).add("key", getKey()).add("providedKey", getProvidedKey()).toString();
        }
    }

    private <T> BindingImpl<T> convertConstantStringBinding(Key<T> key, Errors errors) throws ErrorsException {
        BindingImpl<String> stringBinding = this.state.getExplicitBinding(key.ofType(String.class));
        if (stringBinding == null || !stringBinding.isConstant()) {
            return null;
        }
        String stringValue = (String) stringBinding.getProvider().get();
        Object source = stringBinding.getSource();
        TypeLiteral<T> type = key.getTypeLiteral();
        MatcherAndConverter matchingConverter = this.state.getConverter(stringValue, type, errors, source);
        if (matchingConverter == null) {
            return null;
        }
        try {
            T converted = matchingConverter.getTypeConverter().convert(stringValue, type);
            if (converted == null) {
                throw errors.converterReturnedNull(stringValue, source, type, matchingConverter).toException();
            } else if (type.getRawType().isInstance(converted)) {
                return new ConvertedConstantBindingImpl(this, key, converted, stringBinding);
            } else {
                throw errors.conversionTypeError(stringValue, source, type, matchingConverter, converted).toException();
            }
        } catch (ErrorsException e) {
            throw e;
        } catch (RuntimeException e2) {
            throw errors.conversionError(stringValue, source, type, matchingConverter, e2).toException();
        }
    }

    private static class ConvertedConstantBindingImpl<T> extends BindingImpl<T> implements ConvertedConstantBinding<T> {
        final Binding<String> originalBinding;
        final Provider<T> provider;
        final T value;

        ConvertedConstantBindingImpl(Injector injector, Key<T> key, T value2, Binding<String> originalBinding2) {
            super(injector, key, originalBinding2.getSource(), new ConstantFactory(Initializables.of(value2)), Scoping.UNSCOPED);
            this.value = value2;
            this.provider = Providers.of(value2);
            this.originalBinding = originalBinding2;
        }

        public Provider<T> getProvider() {
            return this.provider;
        }

        public <V> V acceptTargetVisitor(BindingTargetVisitor<? super T, V> visitor) {
            return visitor.visit(this);
        }

        public T getValue() {
            return this.value;
        }

        public Key<String> getSourceKey() {
            return this.originalBinding.getKey();
        }

        public Set<Dependency<?>> getDependencies() {
            return ImmutableSet.of(Dependency.get(getSourceKey()));
        }

        public void applyTo(Binder binder) {
            throw new UnsupportedOperationException("This element represents a synthetic binding.");
        }

        public String toString() {
            return new ToStringBuilder(ConvertedConstantBinding.class).add("key", getKey()).add("sourceKey", getSourceKey()).add("value", this.value).toString();
        }
    }

    /* access modifiers changed from: package-private */
    public <T> void initializeBinding(BindingImpl<T> binding, Errors errors) throws ErrorsException {
        if (binding instanceof ConstructorBindingImpl) {
            Key<T> key = binding.getKey();
            this.jitBindings.put(key, binding);
            boolean successful = false;
            try {
                ((ConstructorBindingImpl) binding).initialize(this, errors);
                successful = true;
            } finally {
                if (!successful) {
                    this.jitBindings.remove(key);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public <T> BindingImpl<T> createUnitializedBinding(Key<T> key, Scoping scoping, Object source, Errors errors) throws ErrorsException {
        Class<? extends Annotation> scopeAnnotation;
        Class<?> rawType = key.getTypeLiteral().getRawType();
        if (rawType.isArray() || rawType.isEnum()) {
            throw errors.missingImplementation(key).toException();
        } else if (rawType == TypeLiteral.class) {
            return createTypeLiteralBinding(key, errors);
        } else {
            ImplementedBy implementedBy = (ImplementedBy) rawType.getAnnotation(ImplementedBy.class);
            if (implementedBy != null) {
                Annotations.checkForMisplacedScopeAnnotations(rawType, source, errors);
                return createImplementedByBinding(key, scoping, implementedBy, errors);
            }
            ProvidedBy providedBy = (ProvidedBy) rawType.getAnnotation(ProvidedBy.class);
            if (providedBy != null) {
                Annotations.checkForMisplacedScopeAnnotations(rawType, source, errors);
                return createProvidedByBinding(key, scoping, providedBy, errors);
            } else if (Modifier.isAbstract(rawType.getModifiers())) {
                throw errors.missingImplementation(key).toException();
            } else if (Classes.isInnerClass(rawType)) {
                throw errors.cannotInjectInnerClass(rawType).toException();
            } else {
                if (!scoping.isExplicitlyScoped() && (scopeAnnotation = Annotations.findScopeAnnotation(errors, rawType)) != null) {
                    scoping = Scopes.makeInjectable(Scoping.forAnnotation(scopeAnnotation), this, errors.withSource(rawType));
                }
                return ConstructorBindingImpl.create(this, key, source, scoping);
            }
        }
    }

    private <T> BindingImpl<TypeLiteral<T>> createTypeLiteralBinding(Key<TypeLiteral<T>> key, Errors errors) throws ErrorsException {
        Type typeLiteralType = key.getTypeLiteral().getType();
        if (!(typeLiteralType instanceof ParameterizedType)) {
            throw errors.cannotInjectRawTypeLiteral().toException();
        }
        Type innerType = ((ParameterizedType) typeLiteralType).getActualTypeArguments()[0];
        if ((innerType instanceof Class) || (innerType instanceof GenericArrayType) || (innerType instanceof ParameterizedType)) {
            TypeLiteral<?> typeLiteral = TypeLiteral.get(innerType);
            return new InstanceBindingImpl(this, key, SourceProvider.UNKNOWN_SOURCE, new ConstantFactory<>(Initializables.of(typeLiteral)), ImmutableSet.of(), typeLiteral);
        }
        throw errors.cannotInjectTypeLiteralOf(innerType).toException();
    }

    /* access modifiers changed from: package-private */
    public <T> BindingImpl<T> createProvidedByBinding(Key<T> key, Scoping scoping, ProvidedBy providedBy, Errors errors) throws ErrorsException {
        final Class<?> rawType = key.getTypeLiteral().getRawType();
        final Class<? extends Provider<?>> providerType = providedBy.value();
        if (providerType == rawType) {
            throw errors.recursiveProviderType().toException();
        }
        final Key<? extends Provider<T>> providerKey = Key.get((Class) providerType);
        final BindingImpl<? extends Provider<?>> providerBinding = getBindingOrThrow(providerKey, errors);
        return new LinkedProviderBindingImpl(this, key, rawType, Scopes.scope(key, this, new InternalFactory<T>() {
            public T get(Errors errors, InternalContext context, Dependency dependency) throws ErrorsException {
                Errors errors2 = errors.withSource(providerKey);
                try {
                    Object o = ((Provider) providerBinding.getInternalFactory().get(errors2, context, dependency)).get();
                    if (o == null || rawType.isInstance(o)) {
                        return o;
                    }
                    throw errors2.subtypeNotProvided(providerType, rawType).toException();
                } catch (RuntimeException e) {
                    throw errors2.errorInProvider(e).toException();
                }
            }
        }, scoping), scoping, providerKey);
    }

    /* access modifiers changed from: package-private */
    public <T> BindingImpl<T> createImplementedByBinding(Key<T> key, Scoping scoping, ImplementedBy implementedBy, Errors errors) throws ErrorsException {
        Class<?> rawType = key.getTypeLiteral().getRawType();
        Class<?> implementationType = implementedBy.value();
        if (implementationType == rawType) {
            throw errors.recursiveImplementationType().toException();
        } else if (!rawType.isAssignableFrom(implementationType)) {
            throw errors.notASubtype(implementationType, rawType).toException();
        } else {
            final Key<? extends T> targetKey = Key.get((Class) implementationType);
            final BindingImpl<? extends T> targetBinding = getBindingOrThrow(targetKey, errors);
            return new LinkedBindingImpl(this, key, rawType, Scopes.scope(key, this, new InternalFactory<T>() {
                public T get(Errors errors, InternalContext context, Dependency<?> dependency) throws ErrorsException {
                    return targetBinding.getInternalFactory().get(errors.withSource(targetKey), context, dependency);
                }
            }, scoping), scoping, targetKey);
        }
    }

    private <T> BindingImpl<T> createJustInTimeBindingRecursive(Key<T> key, Errors errors) throws ErrorsException {
        if (this.parent != null) {
            try {
                return this.parent.createJustInTimeBindingRecursive(key, new Errors());
            } catch (ErrorsException e) {
            }
        }
        if (this.state.isBlacklisted(key)) {
            throw errors.childBindingAlreadySet(key).toException();
        }
        BindingImpl<T> binding = createJustInTimeBinding(key, errors);
        this.state.parent().blacklist(key);
        this.jitBindings.put(key, binding);
        return binding;
    }

    /* access modifiers changed from: package-private */
    public <T> BindingImpl<T> createJustInTimeBinding(Key<T> key, Errors errors) throws ErrorsException {
        if (this.state.isBlacklisted(key)) {
            throw errors.childBindingAlreadySet(key).toException();
        } else if (isProvider(key)) {
            return createProviderBinding(key, errors);
        } else {
            if (isMembersInjector(key)) {
                return createMembersInjectorBinding(key, errors);
            }
            BindingImpl<T> convertedBinding = convertConstantStringBinding(key, errors);
            if (convertedBinding != null) {
                return convertedBinding;
            }
            if (key.hasAnnotationType()) {
                if (key.hasAttributes()) {
                    try {
                        return getBindingOrThrow(key.withoutAttributes(), new Errors());
                    } catch (ErrorsException e) {
                    }
                }
                throw errors.missingImplementation(key).toException();
            }
            BindingImpl<T> binding = createUnitializedBinding(key, Scoping.UNSCOPED, key.getTypeLiteral().getRawType(), errors);
            initializeBinding(binding, errors);
            return binding;
        }
    }

    /* access modifiers changed from: package-private */
    public <T> InternalFactory<? extends T> getInternalFactory(Key<T> key, Errors errors) throws ErrorsException {
        return getBindingOrThrow(key, errors).getInternalFactory();
    }

    public Map<Key<?>, Binding<?>> getBindings() {
        return this.state.getExplicitBindingsThisLevel();
    }

    private static class BindingsMultimap {
        final Map<TypeLiteral<?>, List<Binding<?>>> multimap;

        private BindingsMultimap() {
            this.multimap = Maps.newHashMap();
        }

        /* access modifiers changed from: package-private */
        public <T> void put(TypeLiteral<T> type, Binding<T> binding) {
            List<Binding<?>> bindingsForType = this.multimap.get(type);
            if (bindingsForType == null) {
                bindingsForType = Lists.newArrayList();
                this.multimap.put(type, bindingsForType);
            }
            bindingsForType.add(binding);
        }

        /* Debug info: failed to restart local var, previous not found, register: 2 */
        /* access modifiers changed from: package-private */
        public <T> List<Binding<T>> getAll(TypeLiteral<T> type) {
            return this.multimap.get(type) != null ? Collections.unmodifiableList(this.multimap.get(type)) : ImmutableList.of();
        }
    }

    /* access modifiers changed from: package-private */
    public SingleParameterInjector<?>[] getParametersInjectors(List<Dependency<?>> parameters, Errors errors) throws ErrorsException {
        if (parameters.isEmpty()) {
            return null;
        }
        int numErrorsBefore = errors.size();
        SingleParameterInjector<?>[] result = new SingleParameterInjector[parameters.size()];
        int i = 0;
        for (Dependency<?> parameter : parameters) {
            int i2 = i + 1;
            try {
                result[i] = createParameterInjector(parameter, errors.withSource(parameter));
            } catch (ErrorsException e) {
            }
            i = i2;
        }
        errors.throwIfNewErrors(numErrorsBefore);
        return result;
    }

    /* access modifiers changed from: package-private */
    public <T> SingleParameterInjector<T> createParameterInjector(Dependency<T> dependency, Errors errors) throws ErrorsException {
        return new SingleParameterInjector<>(dependency, getInternalFactory(dependency.getKey(), errors));
    }

    public void injectMembers(Object instance) {
        getMembersInjector(instance.getClass()).injectMembers(instance);
    }

    public <T> MembersInjector<T> getMembersInjector(TypeLiteral<T> typeLiteral) {
        Errors errors = new Errors(typeLiteral);
        try {
            return this.membersInjectorStore.get(typeLiteral, errors);
        } catch (ErrorsException e) {
            throw new ConfigurationException(errors.merge(e.getErrors()).getMessages());
        }
    }

    public <T> MembersInjector<T> getMembersInjector(Class<T> type) {
        return getMembersInjector(TypeLiteral.get((Class) type));
    }

    public <T> Provider<T> getProvider(Class<T> type) {
        return getProvider(Key.get((Class) type));
    }

    /* access modifiers changed from: package-private */
    public <T> Provider<T> getProviderOrThrow(Key<T> key, Errors errors) throws ErrorsException {
        final InternalFactory<? extends T> factory = getInternalFactory(key, errors);
        final Dependency<T> dependency = Dependency.get(key);
        return new Provider<T>() {
            public T get() {
                final Errors errors = new Errors(dependency);
                try {
                    T t = InjectorImpl.this.callInContext(new ContextualCallable<T>() {
                        public T call(InternalContext context) throws ErrorsException {
                            context.setDependency(dependency);
                            try {
                                return factory.get(errors, context, dependency);
                            } finally {
                                context.setDependency(null);
                            }
                        }
                    });
                    errors.throwIfNewErrors(0);
                    return t;
                } catch (ErrorsException e) {
                    throw new ProvisionException(errors.merge(e.getErrors()).getMessages());
                }
            }

            public String toString() {
                return factory.toString();
            }
        };
    }

    public <T> Provider<T> getProvider(Key<T> key) {
        Errors errors = new Errors(key);
        try {
            Provider<T> result = getProviderOrThrow(key, errors);
            errors.throwIfNewErrors(0);
            return result;
        } catch (ErrorsException e) {
            throw new ConfigurationException(errors.merge(e.getErrors()).getMessages());
        }
    }

    public <T> T getInstance(Key<T> key) {
        return getProvider(key).get();
    }

    public <T> T getInstance(Class<T> type) {
        return getProvider(type).get();
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    /* access modifiers changed from: package-private */
    public <T> T callInContext(ContextualCallable<T> callable) throws ErrorsException {
        Object[] reference = this.localContext.get();
        if (reference[0] != null) {
            return callable.call((InternalContext) reference[0]);
        }
        reference[0] = new InternalContext();
        try {
            return callable.call((InternalContext) reference[0]);
        } finally {
            reference[0] = null;
        }
    }

    public String toString() {
        return new ToStringBuilder(Injector.class).add("bindings", this.state.getExplicitBindingsThisLevel().values()).toString();
    }
}
