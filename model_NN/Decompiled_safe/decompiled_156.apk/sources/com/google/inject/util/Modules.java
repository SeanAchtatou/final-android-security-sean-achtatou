package com.google.inject.util;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.Binding;
import com.google.inject.Key;
import com.google.inject.Module;
import com.google.inject.PrivateBinder;
import com.google.inject.Scope;
import com.google.inject.internal.ImmutableSet;
import com.google.inject.internal.Lists;
import com.google.inject.internal.Maps;
import com.google.inject.internal.Sets;
import com.google.inject.spi.DefaultBindingScopingVisitor;
import com.google.inject.spi.DefaultElementVisitor;
import com.google.inject.spi.Element;
import com.google.inject.spi.Elements;
import com.google.inject.spi.PrivateElements;
import com.google.inject.spi.ScopeBinding;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class Modules {
    public static final Module EMPTY_MODULE = new Module() {
        public void configure(Binder binder) {
        }
    };

    public interface OverriddenModuleBuilder {
        Module with(Iterable<? extends Module> iterable);

        Module with(Module... moduleArr);
    }

    private Modules() {
    }

    public static OverriddenModuleBuilder override(Module... modules) {
        return new RealOverriddenModuleBuilder(Arrays.asList(modules));
    }

    public static OverriddenModuleBuilder override(Iterable<? extends Module> modules) {
        return new RealOverriddenModuleBuilder(modules);
    }

    public static Module combine(Module... modules) {
        return combine(ImmutableSet.of((Object[]) modules));
    }

    public static Module combine(Iterable<? extends Module> modules) {
        final Set<Module> modulesSet = ImmutableSet.copyOf(modules);
        return new Module() {
            public void configure(Binder binder) {
                Binder binder2 = binder.skipSources(getClass());
                for (Module module : modulesSet) {
                    binder2.install(module);
                }
            }
        };
    }

    private static final class RealOverriddenModuleBuilder implements OverriddenModuleBuilder {
        /* access modifiers changed from: private */
        public final ImmutableSet<Module> baseModules;

        private RealOverriddenModuleBuilder(Iterable<? extends Module> baseModules2) {
            this.baseModules = ImmutableSet.copyOf(baseModules2);
        }

        public Module with(Module... overrides) {
            return with(Arrays.asList(overrides));
        }

        public Module with(final Iterable<? extends Module> overrides) {
            return new AbstractModule() {
                public void configure() {
                    List<Element> elements = Elements.getElements(RealOverriddenModuleBuilder.this.baseModules);
                    List<Element> overrideElements = Elements.getElements(overrides);
                    final Set<Key> overriddenKeys = Sets.newHashSet();
                    final Set<Class<? extends Annotation>> overridesScopeAnnotations = Sets.newHashSet();
                    new ModuleWriter(binder()) {
                        public <T> Void visit(Binding<T> binding) {
                            overriddenKeys.add(binding.getKey());
                            return (Void) super.visit((Binding) binding);
                        }

                        public Void visit(ScopeBinding scopeBinding) {
                            overridesScopeAnnotations.add(scopeBinding.getAnnotationType());
                            return (Void) super.visit(scopeBinding);
                        }

                        public Void visit(PrivateElements privateElements) {
                            overriddenKeys.addAll(privateElements.getExposedKeys());
                            return (Void) super.visit(privateElements);
                        }
                    }.writeAll(overrideElements);
                    final Map<Scope, Object> scopeInstancesInUse = Maps.newHashMap();
                    final List<ScopeBinding> scopeBindings = Lists.newArrayList();
                    new ModuleWriter(binder()) {
                        public <T> Void visit(Binding<T> binding) {
                            if (overriddenKeys.remove(binding.getKey())) {
                                return null;
                            }
                            super.visit((Binding) binding);
                            Scope scope = AnonymousClass1.this.getScopeInstanceOrNull(binding);
                            if (scope == null) {
                                return null;
                            }
                            scopeInstancesInUse.put(scope, binding.getSource());
                            return null;
                        }

                        public Void visit(PrivateElements privateElements) {
                            PrivateBinder privateBinder = this.binder.withSource(privateElements.getSource()).newPrivateBinder();
                            Set<Key<?>> skippedExposes = Sets.newHashSet();
                            for (Key<?> key : privateElements.getExposedKeys()) {
                                if (overriddenKeys.remove(key)) {
                                    skippedExposes.add(key);
                                } else {
                                    privateBinder.withSource(privateElements.getExposedSource(key)).expose(key);
                                }
                            }
                            for (Element element : privateElements.getElements()) {
                                if (!(element instanceof Binding) || !skippedExposes.contains(((Binding) element).getKey())) {
                                    element.applyTo(privateBinder);
                                }
                            }
                            return null;
                        }

                        public Void visit(ScopeBinding scopeBinding) {
                            scopeBindings.add(scopeBinding);
                            return null;
                        }
                    }.writeAll(elements);
                    new ModuleWriter(binder()) {
                        public Void visit(ScopeBinding scopeBinding) {
                            if (!overridesScopeAnnotations.remove(scopeBinding.getAnnotationType())) {
                                super.visit(scopeBinding);
                                return null;
                            }
                            Object source = scopeInstancesInUse.get(scopeBinding.getScope());
                            if (source == null) {
                                return null;
                            }
                            AnonymousClass1.this.binder().withSource(source).addError("The scope for @%s is bound directly and cannot be overridden.", scopeBinding.getAnnotationType().getSimpleName());
                            return null;
                        }
                    }.writeAll(scopeBindings);
                }

                /* access modifiers changed from: private */
                public Scope getScopeInstanceOrNull(Binding<?> binding) {
                    return (Scope) binding.acceptScopingVisitor(new DefaultBindingScopingVisitor<Scope>() {
                        public Scope visitScope(Scope scope) {
                            return scope;
                        }
                    });
                }
            };
        }
    }

    private static class ModuleWriter extends DefaultElementVisitor<Void> {
        protected final Binder binder;

        ModuleWriter(Binder binder2) {
            this.binder = binder2;
        }

        /* access modifiers changed from: protected */
        public Void visitOther(Element element) {
            element.applyTo(this.binder);
            return null;
        }

        /* access modifiers changed from: package-private */
        public void writeAll(Iterable<? extends Element> elements) {
            for (Element element : elements) {
                element.acceptVisitor(this);
            }
        }
    }
}
