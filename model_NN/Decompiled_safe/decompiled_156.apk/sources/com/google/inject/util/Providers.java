package com.google.inject.util;

import com.google.inject.Provider;

public final class Providers {
    private Providers() {
    }

    public static <T> Provider<T> of(final T instance) {
        return new Provider<T>() {
            public T get() {
                return instance;
            }

            public String toString() {
                return "of(" + instance + ")";
            }
        };
    }
}
