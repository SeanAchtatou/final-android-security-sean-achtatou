package com.google.inject;

import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.InternalContext;
import com.google.inject.internal.InternalFactory;

class ProviderToInternalFactoryAdapter<T> implements Provider<T> {
    private final InjectorImpl injector;
    /* access modifiers changed from: private */
    public final InternalFactory<? extends T> internalFactory;

    public ProviderToInternalFactoryAdapter(InjectorImpl injector2, InternalFactory<? extends T> internalFactory2) {
        this.injector = injector2;
        this.internalFactory = internalFactory2;
    }

    public T get() {
        final Errors errors = new Errors();
        try {
            T t = this.injector.callInContext(new ContextualCallable<T>() {
                public T call(InternalContext context) throws ErrorsException {
                    return ProviderToInternalFactoryAdapter.this.internalFactory.get(errors, context, context.getDependency());
                }
            });
            errors.throwIfNewErrors(0);
            return t;
        } catch (ErrorsException e) {
            throw new ProvisionException(errors.merge(e.getErrors()).getMessages());
        }
    }

    public String toString() {
        return this.internalFactory.toString();
    }
}
