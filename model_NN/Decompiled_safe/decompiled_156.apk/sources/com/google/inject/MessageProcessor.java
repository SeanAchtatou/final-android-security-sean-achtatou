package com.google.inject;

import com.google.inject.internal.Errors;
import com.google.inject.spi.Message;
import java.util.logging.Level;
import java.util.logging.Logger;

class MessageProcessor extends AbstractProcessor {
    private static final Logger logger = Logger.getLogger(Guice.class.getName());

    MessageProcessor(Errors errors) {
        super(errors);
    }

    public Boolean visit(Message message) {
        if (message.getCause() != null) {
            logger.log(Level.INFO, "An exception was caught and reported. Message: " + getRootMessage(message.getCause()), message.getCause());
        }
        this.errors.addMessage(message);
        return true;
    }

    public static String getRootMessage(Throwable t) {
        Throwable cause = t.getCause();
        return cause == null ? t.toString() : getRootMessage(cause);
    }
}
