package com.google.android.apps.analytics;

import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import com.google.android.apps.analytics.Dispatcher;
import com.google.android.apps.analytics.PipelinedRequester;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Locale;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.ParseException;
import org.apache.http.message.BasicHttpRequest;

class NetworkDispatcher implements Dispatcher {
    /* access modifiers changed from: private */
    public static final HttpHost GOOGLE_ANALYTICS_HOST = new HttpHost("www.google-analytics.com", 80);
    private static final int MAX_EVENTS_PER_PIPELINE = 30;
    private static final int MAX_SEQUENTIAL_REQUESTS = 5;
    private static final long MIN_RETRY_INTERVAL = 2;
    private static final String USER_AGENT_TEMPLATE = "%s/%s (Linux; U; Android %s; %s-%s; %s; Build/%s)";
    private DispatcherThread dispatcherThread;
    private final String userAgent;

    private static class DispatcherThread extends HandlerThread {
        /* access modifiers changed from: private */
        public final Dispatcher.Callbacks callbacks;
        /* access modifiers changed from: private */
        public AsyncDispatchTask currentTask;
        private Handler handlerExecuteOnDispatcherThread;
        /* access modifiers changed from: private */
        public int lastStatusCode;
        /* access modifiers changed from: private */
        public int maxEventsPerRequest;
        /* access modifiers changed from: private */
        public final PipelinedRequester pipelinedRequester;
        /* access modifiers changed from: private */
        public final String referrer;
        /* access modifiers changed from: private */
        public long retryInterval;
        /* access modifiers changed from: private */
        public final String userAgent;

        private class AsyncDispatchTask implements Runnable {
            private final LinkedList<Event> events = new LinkedList<>();

            public AsyncDispatchTask(Event[] eventArr) {
                Collections.addAll(this.events, eventArr);
            }

            private void dispatchSomePendingEvents() throws IOException, ParseException, HttpException {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= this.events.size() || i2 >= DispatcherThread.this.maxEventsPerRequest) {
                        DispatcherThread.this.pipelinedRequester.sendRequests();
                    } else {
                        Event event = this.events.get(i2);
                        BasicHttpRequest basicHttpRequest = new BasicHttpRequest("GET", "__##GOOGLEPAGEVIEW##__".equals(event.category) ? NetworkRequestUtil.constructPageviewRequestPath(event, DispatcherThread.this.referrer) : NetworkRequestUtil.constructEventRequestPath(event, DispatcherThread.this.referrer));
                        basicHttpRequest.addHeader("Host", NetworkDispatcher.GOOGLE_ANALYTICS_HOST.getHostName());
                        basicHttpRequest.addHeader("User-Agent", DispatcherThread.this.userAgent);
                        DispatcherThread.this.pipelinedRequester.addRequest(basicHttpRequest);
                        i = i2 + 1;
                    }
                }
                DispatcherThread.this.pipelinedRequester.sendRequests();
            }

            public Event removeNextEvent() {
                return this.events.poll();
            }

            public void run() {
                AsyncDispatchTask unused = DispatcherThread.this.currentTask = this;
                int i = 0;
                while (i < 5 && this.events.size() > 0) {
                    long j = 0;
                    try {
                        if (DispatcherThread.this.lastStatusCode == 500 || DispatcherThread.this.lastStatusCode == 503) {
                            j = (long) (Math.random() * ((double) DispatcherThread.this.retryInterval));
                            if (DispatcherThread.this.retryInterval < 256) {
                                DispatcherThread.access$630(DispatcherThread.this, NetworkDispatcher.MIN_RETRY_INTERVAL);
                            }
                        } else {
                            long unused2 = DispatcherThread.this.retryInterval = NetworkDispatcher.MIN_RETRY_INTERVAL;
                        }
                        Thread.sleep(j * 1000);
                        dispatchSomePendingEvents();
                        i++;
                    } catch (InterruptedException e) {
                        Log.w(GoogleAnalyticsTracker.TRACKER_TAG, "Couldn't sleep.", e);
                    } catch (IOException e2) {
                        Log.w(GoogleAnalyticsTracker.TRACKER_TAG, "Problem with socket or streams.", e2);
                    } catch (HttpException e3) {
                        Log.w(GoogleAnalyticsTracker.TRACKER_TAG, "Problem with http streams.", e3);
                    }
                }
                DispatcherThread.this.pipelinedRequester.finishedCurrentRequests();
                DispatcherThread.this.callbacks.dispatchFinished();
                AsyncDispatchTask unused3 = DispatcherThread.this.currentTask = null;
            }
        }

        private class RequesterCallbacks implements PipelinedRequester.Callbacks {
            private RequesterCallbacks() {
            }

            public void pipelineModeChanged(boolean z) {
                if (z) {
                    int unused = DispatcherThread.this.maxEventsPerRequest = NetworkDispatcher.MAX_EVENTS_PER_PIPELINE;
                } else {
                    int unused2 = DispatcherThread.this.maxEventsPerRequest = 1;
                }
            }

            public void requestSent() {
                Event removeNextEvent;
                if (DispatcherThread.this.currentTask != null && (removeNextEvent = DispatcherThread.this.currentTask.removeNextEvent()) != null) {
                    DispatcherThread.this.callbacks.eventDispatched(removeNextEvent.eventId);
                }
            }

            public void serverError(int i) {
                int unused = DispatcherThread.this.lastStatusCode = i;
            }
        }

        private DispatcherThread(Dispatcher.Callbacks callbacks2, PipelinedRequester pipelinedRequester2, String str, String str2) {
            super("DispatcherThread");
            this.maxEventsPerRequest = NetworkDispatcher.MAX_EVENTS_PER_PIPELINE;
            this.currentTask = null;
            this.callbacks = callbacks2;
            this.referrer = str;
            this.userAgent = str2;
            this.pipelinedRequester = pipelinedRequester2;
            this.pipelinedRequester.installCallbacks(new RequesterCallbacks());
        }

        private DispatcherThread(Dispatcher.Callbacks callbacks2, String str, String str2) {
            this(callbacks2, new PipelinedRequester(NetworkDispatcher.GOOGLE_ANALYTICS_HOST), str, str2);
        }

        static /* synthetic */ long access$630(DispatcherThread dispatcherThread, long j) {
            long j2 = dispatcherThread.retryInterval * j;
            dispatcherThread.retryInterval = j2;
            return j2;
        }

        public void dispatchEvents(Event[] eventArr) {
            if (this.handlerExecuteOnDispatcherThread != null) {
                this.handlerExecuteOnDispatcherThread.post(new AsyncDispatchTask(eventArr));
            }
        }

        /* access modifiers changed from: protected */
        public void onLooperPrepared() {
            this.handlerExecuteOnDispatcherThread = new Handler();
        }
    }

    public NetworkDispatcher() {
        this(GoogleAnalyticsTracker.PRODUCT, GoogleAnalyticsTracker.VERSION);
    }

    public NetworkDispatcher(String str, String str2) {
        Locale locale = Locale.getDefault();
        Object[] objArr = new Object[7];
        objArr[0] = str;
        objArr[1] = str2;
        objArr[2] = Build.VERSION.RELEASE;
        objArr[3] = locale.getLanguage() != null ? locale.getLanguage().toLowerCase() : "en";
        objArr[4] = locale.getCountry() != null ? locale.getCountry().toLowerCase() : "";
        objArr[5] = Build.MODEL;
        objArr[6] = Build.ID;
        this.userAgent = String.format(USER_AGENT_TEMPLATE, objArr);
    }

    public void dispatchEvents(Event[] eventArr) {
        if (this.dispatcherThread != null) {
            this.dispatcherThread.dispatchEvents(eventArr);
        }
    }

    public void init(Dispatcher.Callbacks callbacks, PipelinedRequester pipelinedRequester, String str) {
        stop();
        this.dispatcherThread = new DispatcherThread(callbacks, pipelinedRequester, str, this.userAgent);
        this.dispatcherThread.start();
    }

    public void init(Dispatcher.Callbacks callbacks, String str) {
        stop();
        this.dispatcherThread = new DispatcherThread(callbacks, str, this.userAgent);
        this.dispatcherThread.start();
    }

    public void stop() {
        if (this.dispatcherThread != null && this.dispatcherThread.getLooper() != null) {
            this.dispatcherThread.getLooper().quit();
            this.dispatcherThread = null;
        }
    }

    public void waitForThreadLooper() {
        this.dispatcherThread.getLooper();
    }
}
