package android.opengl.derived;

import android.content.Context;
import android.opengl.GLDebugHelper;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import com.ansca.corona.version.ISurfaceView;
import java.io.Writer;
import java.util.ArrayList;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;

public class SwapGLSurfaceView extends SurfaceView implements SurfaceHolder.Callback, ISurfaceView {
    private static final boolean LOG_THREADS = false;
    /* access modifiers changed from: private */
    public static final GLThreadManager sGLThreadManager = new GLThreadManager();
    /* access modifiers changed from: private */
    public int mDebugFlags;
    /* access modifiers changed from: private */
    public GLSurfaceView.EGLConfigChooser mEGLConfigChooser;
    /* access modifiers changed from: private */
    public GLSurfaceView.EGLContextFactory mEGLContextFactory;
    /* access modifiers changed from: private */
    public GLSurfaceView.EGLWindowSurfaceFactory mEGLWindowSurfaceFactory;
    private GLThread mGLThread;
    /* access modifiers changed from: private */
    public GLSurfaceView.GLWrapper mGLWrapper;
    /* access modifiers changed from: private */
    public boolean mNeedsSwap = true;
    /* access modifiers changed from: private */
    public boolean mSizeChanged = true;

    public SwapGLSurfaceView(Context context) {
        super(context);
        init();
    }

    public SwapGLSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        getHolder().addCallback(this);
    }

    public void setGLWrapper(GLSurfaceView.GLWrapper glWrapper) {
        this.mGLWrapper = glWrapper;
    }

    public void setDebugFlags(int debugFlags) {
        this.mDebugFlags = debugFlags;
    }

    public int getDebugFlags() {
        return this.mDebugFlags;
    }

    public void setRenderer(GLSurfaceView.Renderer renderer) {
        checkRenderThreadState();
        if (this.mEGLConfigChooser == null) {
            this.mEGLConfigChooser = new SimpleEGLConfigChooser(true);
        }
        if (this.mEGLContextFactory == null) {
            this.mEGLContextFactory = new DefaultContextFactory();
        }
        if (this.mEGLWindowSurfaceFactory == null) {
            this.mEGLWindowSurfaceFactory = new DefaultWindowSurfaceFactory();
        }
        this.mGLThread = new GLThread(renderer);
        this.mGLThread.start();
    }

    public void setEGLContextFactory(GLSurfaceView.EGLContextFactory factory) {
        checkRenderThreadState();
        this.mEGLContextFactory = factory;
    }

    public void setEGLWindowSurfaceFactory(GLSurfaceView.EGLWindowSurfaceFactory factory) {
        checkRenderThreadState();
        this.mEGLWindowSurfaceFactory = factory;
    }

    public void setEGLConfigChooser(GLSurfaceView.EGLConfigChooser configChooser) {
        checkRenderThreadState();
        this.mEGLConfigChooser = configChooser;
    }

    public void setEGLConfigChooser(boolean needDepth) {
        setEGLConfigChooser(new SimpleEGLConfigChooser(needDepth));
    }

    public void setEGLConfigChooser(int redSize, int greenSize, int blueSize, int alphaSize, int depthSize, int stencilSize) {
        setEGLConfigChooser(new ComponentSizeChooser(redSize, greenSize, blueSize, alphaSize, depthSize, stencilSize));
    }

    public void setRenderMode(int renderMode) {
        this.mGLThread.setRenderMode(renderMode);
    }

    public int getRenderMode() {
        return this.mGLThread.getRenderMode();
    }

    public void requestRender() {
        this.mGLThread.requestRender();
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.mGLThread.surfaceCreated();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        this.mGLThread.surfaceDestroyed();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        this.mGLThread.onWindowResize(w, h);
    }

    public void onPause() {
        this.mGLThread.onPause();
    }

    public void onResume() {
        this.mGLThread.onResume();
    }

    public void queueEvent(Runnable r) {
        this.mGLThread.queueEvent(r);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mGLThread.requestExitAndWait();
    }

    private static class DefaultContextFactory implements GLSurfaceView.EGLContextFactory {
        private DefaultContextFactory() {
        }

        public EGLContext createContext(EGL10 egl, EGLDisplay display, EGLConfig config) {
            return egl.eglCreateContext(display, config, EGL10.EGL_NO_CONTEXT, null);
        }

        public void destroyContext(EGL10 egl, EGLDisplay display, EGLContext context) {
            egl.eglDestroyContext(display, context);
        }
    }

    private static class DefaultWindowSurfaceFactory implements GLSurfaceView.EGLWindowSurfaceFactory {
        private DefaultWindowSurfaceFactory() {
        }

        public EGLSurface createWindowSurface(EGL10 egl, EGLDisplay display, EGLConfig config, Object nativeWindow) {
            return egl.eglCreateWindowSurface(display, config, nativeWindow, null);
        }

        public void destroySurface(EGL10 egl, EGLDisplay display, EGLSurface surface) {
            egl.eglDestroySurface(display, surface);
        }
    }

    private static abstract class BaseConfigChooser implements GLSurfaceView.EGLConfigChooser {
        protected int[] mConfigSpec;

        /* access modifiers changed from: package-private */
        public abstract EGLConfig chooseConfig(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig[] eGLConfigArr);

        public BaseConfigChooser(int[] configSpec) {
            this.mConfigSpec = configSpec;
        }

        public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display) {
            int[] num_config = new int[1];
            egl.eglChooseConfig(display, this.mConfigSpec, null, 0, num_config);
            int numConfigs = num_config[0];
            if (numConfigs <= 0) {
                throw new IllegalArgumentException("No configs match configSpec");
            }
            EGLConfig[] configs = new EGLConfig[numConfigs];
            egl.eglChooseConfig(display, this.mConfigSpec, configs, numConfigs, num_config);
            EGLConfig config = chooseConfig(egl, display, configs);
            if (config != null) {
                return config;
            }
            throw new IllegalArgumentException("No config chosen");
        }
    }

    private static class ComponentSizeChooser extends BaseConfigChooser {
        protected int mAlphaSize;
        protected int mBlueSize;
        protected int mDepthSize;
        protected int mGreenSize;
        protected int mRedSize;
        protected int mStencilSize;
        private int[] mValue = new int[1];

        public ComponentSizeChooser(int redSize, int greenSize, int blueSize, int alphaSize, int depthSize, int stencilSize) {
            super(new int[]{12324, redSize, 12323, greenSize, 12322, blueSize, 12321, alphaSize, 12325, depthSize, 12326, stencilSize, 12344});
            this.mRedSize = redSize;
            this.mGreenSize = greenSize;
            this.mBlueSize = blueSize;
            this.mAlphaSize = alphaSize;
            this.mDepthSize = depthSize;
            this.mStencilSize = stencilSize;
        }

        public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display, EGLConfig[] configs) {
            EGLConfig closestConfig = null;
            int closestDistance = 1000;
            EGLConfig[] arr$ = configs;
            int len$ = arr$.length;
            for (int i$ = 0; i$ < len$; i$++) {
                EGLConfig config = arr$[i$];
                int d = findConfigAttrib(egl, display, config, 12325, 0);
                int s = findConfigAttrib(egl, display, config, 12326, 0);
                if (d >= this.mDepthSize && s >= this.mStencilSize) {
                    int distance = Math.abs(findConfigAttrib(egl, display, config, 12324, 0) - this.mRedSize) + Math.abs(findConfigAttrib(egl, display, config, 12323, 0) - this.mGreenSize) + Math.abs(findConfigAttrib(egl, display, config, 12322, 0) - this.mBlueSize) + Math.abs(findConfigAttrib(egl, display, config, 12321, 0) - this.mAlphaSize);
                    if (distance < closestDistance) {
                        closestDistance = distance;
                        closestConfig = config;
                    }
                }
            }
            return closestConfig;
        }

        private int findConfigAttrib(EGL10 egl, EGLDisplay display, EGLConfig config, int attribute, int defaultValue) {
            if (egl.eglGetConfigAttrib(display, config, attribute, this.mValue)) {
                return this.mValue[0];
            }
            return defaultValue;
        }
    }

    private static class SimpleEGLConfigChooser extends ComponentSizeChooser {
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public SimpleEGLConfigChooser(boolean withDepthBuffer) {
            super(4, 4, 4, 0, withDepthBuffer ? 16 : 0, 0);
            this.mRedSize = 5;
            this.mGreenSize = 6;
            this.mBlueSize = 5;
        }
    }

    private class EglHelper {
        EGL10 mEgl;
        EGLConfig mEglConfig;
        EGLContext mEglContext;
        EGLDisplay mEglDisplay;
        EGLSurface mEglSurface;

        public EglHelper() {
        }

        public void start() {
            this.mEgl = (EGL10) EGLContext.getEGL();
            this.mEglDisplay = this.mEgl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            this.mEgl.eglInitialize(this.mEglDisplay, new int[2]);
            this.mEglConfig = SwapGLSurfaceView.this.mEGLConfigChooser.chooseConfig(this.mEgl, this.mEglDisplay);
            this.mEglContext = SwapGLSurfaceView.this.mEGLContextFactory.createContext(this.mEgl, this.mEglDisplay, this.mEglConfig);
            if (this.mEglContext == null || this.mEglContext == EGL10.EGL_NO_CONTEXT) {
                throw new RuntimeException("createContext failed");
            }
            this.mEglSurface = null;
        }

        public GL createSurface(SurfaceHolder holder) {
            if (!(this.mEglSurface == null || this.mEglSurface == EGL10.EGL_NO_SURFACE)) {
                this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                SwapGLSurfaceView.this.mEGLWindowSurfaceFactory.destroySurface(this.mEgl, this.mEglDisplay, this.mEglSurface);
            }
            this.mEglSurface = SwapGLSurfaceView.this.mEGLWindowSurfaceFactory.createWindowSurface(this.mEgl, this.mEglDisplay, this.mEglConfig, holder);
            if (this.mEglSurface == null || this.mEglSurface == EGL10.EGL_NO_SURFACE) {
                throw new RuntimeException("createWindowSurface failed");
            } else if (!this.mEgl.eglMakeCurrent(this.mEglDisplay, this.mEglSurface, this.mEglSurface, this.mEglContext)) {
                throw new RuntimeException("eglMakeCurrent failed.");
            } else {
                GL gl = this.mEglContext.getGL();
                if (SwapGLSurfaceView.this.mGLWrapper != null) {
                    gl = SwapGLSurfaceView.this.mGLWrapper.wrap(gl);
                }
                if ((SwapGLSurfaceView.this.mDebugFlags & 3) == 0) {
                    return gl;
                }
                int configFlags = 0;
                Writer log = null;
                if ((SwapGLSurfaceView.this.mDebugFlags & 1) != 0) {
                    configFlags = 0 | 1;
                }
                if ((SwapGLSurfaceView.this.mDebugFlags & 2) != 0) {
                    log = new LogWriter();
                }
                return GLDebugHelper.wrap(gl, configFlags, log);
            }
        }

        public boolean swap() {
            this.mEgl.eglSwapBuffers(this.mEglDisplay, this.mEglSurface);
            return this.mEgl.eglGetError() != 12302;
        }

        public void destroySurface() {
            if (this.mEglSurface != null && this.mEglSurface != EGL10.EGL_NO_SURFACE) {
                this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                SwapGLSurfaceView.this.mEGLWindowSurfaceFactory.destroySurface(this.mEgl, this.mEglDisplay, this.mEglSurface);
                this.mEglSurface = null;
            }
        }

        public void finish() {
            if (this.mEglContext != null) {
                SwapGLSurfaceView.this.mEGLContextFactory.destroyContext(this.mEgl, this.mEglDisplay, this.mEglContext);
                this.mEglContext = null;
            }
            if (this.mEglDisplay != null) {
                this.mEgl.eglTerminate(this.mEglDisplay);
                this.mEglDisplay = null;
            }
        }
    }

    class GLThread extends Thread {
        /* access modifiers changed from: private */
        public boolean mDone = false;
        private EglHelper mEglHelper;
        private ArrayList<Runnable> mEventQueue = new ArrayList<>();
        private boolean mEventsWaiting;
        private boolean mHasSurface;
        private boolean mHaveEgl;
        private int mHeight = 0;
        private boolean mPaused;
        private int mRenderMode = 1;
        private GLSurfaceView.Renderer mRenderer;
        private boolean mRequestRender = true;
        private boolean mWaitingForSurface;
        private int mWidth = 0;

        GLThread(GLSurfaceView.Renderer renderer) {
            this.mRenderer = renderer;
        }

        public void run() {
            setName("GLThread " + getId());
            try {
                guardedRun();
            } catch (InterruptedException e) {
            } finally {
                SwapGLSurfaceView.sGLThreadManager.threadExiting(this);
            }
        }

        private void stopEglLocked() {
            if (this.mHaveEgl) {
                this.mHaveEgl = false;
                this.mEglHelper.destroySurface();
                this.mEglHelper.finish();
                SwapGLSurfaceView.sGLThreadManager.releaseEglSurface(this);
            }
        }

        /* JADX WARN: Type inference failed for: r10v15, types: [javax.microedition.khronos.opengles.GL] */
        /* JADX WARNING: Code restructure failed: missing block: B:100:0x0123, code lost:
            r14.mEglHelper.swap();
            android.opengl.derived.SwapGLSurfaceView.access$902(r14.this$0, false);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:118:0x000d, code lost:
            continue;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:133:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x006f, code lost:
            if (r2 == false) goto L_0x00e7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
            r6 = getEvent();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x0075, code lost:
            if (r6 == null) goto L_0x000d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x0077, code lost:
            r6.run();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:0x007e, code lost:
            if (isDone() == false) goto L_0x0071;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x0080, code lost:
            r10 = android.opengl.derived.SwapGLSurfaceView.access$700();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x0084, code lost:
            monitor-enter(r10);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
            stopEglLocked();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:0x0088, code lost:
            monitor-exit(r10);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:69:0x00a9, code lost:
            r1 = android.opengl.derived.SwapGLSurfaceView.access$800(r14.this$0);
            r9 = r14.mWidth;
            r4 = r14.mHeight;
            android.opengl.derived.SwapGLSurfaceView.access$802(r14.this$0, false);
            r14.mRequestRender = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:70:0x00be, code lost:
            if (r14.mHasSurface == false) goto L_0x006e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:72:0x00c2, code lost:
            if (r14.mWaitingForSurface == false) goto L_0x006e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:73:0x00c4, code lost:
            r1 = true;
            r14.mWaitingForSurface = false;
            android.opengl.derived.SwapGLSurfaceView.access$700().notifyAll();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:78:0x00d3, code lost:
            r10 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:80:0x00d8, code lost:
            monitor-enter(android.opengl.derived.SwapGLSurfaceView.access$700());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:82:?, code lost:
            stopEglLocked();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:84:0x00dd, code lost:
            throw r10;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:87:0x00e7, code lost:
            if (r5 == false) goto L_0x00eb;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:88:0x00e9, code lost:
            r8 = true;
            r1 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:89:0x00eb, code lost:
            if (r1 == false) goto L_0x00fe;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:91:?, code lost:
            r3 = r14.mEglHelper.createSurface(r14.this$0.getHolder());
            r7 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:92:0x00fe, code lost:
            if (r8 == false) goto L_0x010a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:93:0x0100, code lost:
            r14.mRenderer.onSurfaceCreated(r3, r14.mEglHelper.mEglConfig);
            r8 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:94:0x010a, code lost:
            if (r7 == false) goto L_0x0112;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:95:0x010c, code lost:
            r14.mRenderer.onSurfaceChanged(r3, r9, r4);
            r7 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:96:0x0112, code lost:
            if (r9 <= 0) goto L_0x000d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:97:0x0114, code lost:
            if (r4 <= 0) goto L_0x000d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:98:0x0116, code lost:
            r14.mRenderer.onDrawFrame(r3);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:99:0x0121, code lost:
            if (android.opengl.derived.SwapGLSurfaceView.access$900(r14.this$0) == false) goto L_0x000d;
         */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void guardedRun() throws java.lang.InterruptedException {
            /*
                r14 = this;
                r13 = 1
                android.opengl.derived.SwapGLSurfaceView$EglHelper r10 = new android.opengl.derived.SwapGLSurfaceView$EglHelper
                android.opengl.derived.SwapGLSurfaceView r11 = android.opengl.derived.SwapGLSurfaceView.this
                r10.<init>()
                r14.mEglHelper = r10
                r3 = 0
                r8 = 1
                r7 = 1
            L_0x000d:
                boolean r10 = r14.isDone()     // Catch:{ all -> 0x00d3 }
                if (r10 != 0) goto L_0x0130
                r9 = 0
                r4 = 0
                r1 = 0
                r5 = 0
                r2 = 0
                android.opengl.derived.SwapGLSurfaceView$GLThreadManager r10 = android.opengl.derived.SwapGLSurfaceView.sGLThreadManager     // Catch:{ all -> 0x00d3 }
                monitor-enter(r10)     // Catch:{ all -> 0x00d3 }
            L_0x001d:
                boolean r11 = r14.mPaused     // Catch:{ all -> 0x00d0 }
                if (r11 == 0) goto L_0x0024
                r14.stopEglLocked()     // Catch:{ all -> 0x00d0 }
            L_0x0024:
                boolean r11 = r14.mHasSurface     // Catch:{ all -> 0x00d0 }
                if (r11 != 0) goto L_0x0048
                boolean r11 = r14.mWaitingForSurface     // Catch:{ all -> 0x00d0 }
                if (r11 != 0) goto L_0x0039
                r14.stopEglLocked()     // Catch:{ all -> 0x00d0 }
                r11 = 1
                r14.mWaitingForSurface = r11     // Catch:{ all -> 0x00d0 }
                android.opengl.derived.SwapGLSurfaceView$GLThreadManager r11 = android.opengl.derived.SwapGLSurfaceView.sGLThreadManager     // Catch:{ all -> 0x00d0 }
                r11.notifyAll()     // Catch:{ all -> 0x00d0 }
            L_0x0039:
                boolean r11 = r14.mDone     // Catch:{ all -> 0x00d0 }
                if (r11 == 0) goto L_0x0066
                monitor-exit(r10)     // Catch:{ all -> 0x00d0 }
                android.opengl.derived.SwapGLSurfaceView$GLThreadManager r10 = android.opengl.derived.SwapGLSurfaceView.sGLThreadManager
                monitor-enter(r10)
                r14.stopEglLocked()     // Catch:{ all -> 0x0063 }
                monitor-exit(r10)     // Catch:{ all -> 0x0063 }
            L_0x0047:
                return
            L_0x0048:
                boolean r11 = r14.mHaveEgl     // Catch:{ all -> 0x00d0 }
                if (r11 != 0) goto L_0x0039
                android.opengl.derived.SwapGLSurfaceView$GLThreadManager r11 = android.opengl.derived.SwapGLSurfaceView.sGLThreadManager     // Catch:{ all -> 0x00d0 }
                boolean r11 = r11.tryAcquireEglSurface(r14)     // Catch:{ all -> 0x00d0 }
                if (r11 == 0) goto L_0x0039
                r11 = 1
                r14.mHaveEgl = r11     // Catch:{ all -> 0x00d0 }
                android.opengl.derived.SwapGLSurfaceView$EglHelper r11 = r14.mEglHelper     // Catch:{ all -> 0x00d0 }
                r11.start()     // Catch:{ all -> 0x00d0 }
                r11 = 1
                r14.mRequestRender = r11     // Catch:{ all -> 0x00d0 }
                r5 = 1
                goto L_0x0039
            L_0x0063:
                r11 = move-exception
                monitor-exit(r10)     // Catch:{ all -> 0x0063 }
                throw r11
            L_0x0066:
                boolean r11 = r14.mEventsWaiting     // Catch:{ all -> 0x00d0 }
                if (r11 == 0) goto L_0x008d
                r2 = 1
                r11 = 0
                r14.mEventsWaiting = r11     // Catch:{ all -> 0x00d0 }
            L_0x006e:
                monitor-exit(r10)     // Catch:{ all -> 0x00d0 }
                if (r2 == 0) goto L_0x00e7
            L_0x0071:
                java.lang.Runnable r6 = r14.getEvent()     // Catch:{ all -> 0x00d3 }
                if (r6 == 0) goto L_0x000d
                r6.run()     // Catch:{ all -> 0x00d3 }
                boolean r10 = r14.isDone()     // Catch:{ all -> 0x00d3 }
                if (r10 == 0) goto L_0x0071
                android.opengl.derived.SwapGLSurfaceView$GLThreadManager r10 = android.opengl.derived.SwapGLSurfaceView.sGLThreadManager
                monitor-enter(r10)
                r14.stopEglLocked()     // Catch:{ all -> 0x008a }
                monitor-exit(r10)     // Catch:{ all -> 0x008a }
                goto L_0x0047
            L_0x008a:
                r11 = move-exception
                monitor-exit(r10)     // Catch:{ all -> 0x008a }
                throw r11
            L_0x008d:
                boolean r11 = r14.mPaused     // Catch:{ all -> 0x00d0 }
                if (r11 != 0) goto L_0x00de
                boolean r11 = r14.mHasSurface     // Catch:{ all -> 0x00d0 }
                if (r11 == 0) goto L_0x00de
                boolean r11 = r14.mHaveEgl     // Catch:{ all -> 0x00d0 }
                if (r11 == 0) goto L_0x00de
                int r11 = r14.mWidth     // Catch:{ all -> 0x00d0 }
                if (r11 <= 0) goto L_0x00de
                int r11 = r14.mHeight     // Catch:{ all -> 0x00d0 }
                if (r11 <= 0) goto L_0x00de
                boolean r11 = r14.mRequestRender     // Catch:{ all -> 0x00d0 }
                if (r11 != 0) goto L_0x00a9
                int r11 = r14.mRenderMode     // Catch:{ all -> 0x00d0 }
                if (r11 != r13) goto L_0x00de
            L_0x00a9:
                android.opengl.derived.SwapGLSurfaceView r11 = android.opengl.derived.SwapGLSurfaceView.this     // Catch:{ all -> 0x00d0 }
                boolean r1 = r11.mSizeChanged     // Catch:{ all -> 0x00d0 }
                int r9 = r14.mWidth     // Catch:{ all -> 0x00d0 }
                int r4 = r14.mHeight     // Catch:{ all -> 0x00d0 }
                android.opengl.derived.SwapGLSurfaceView r11 = android.opengl.derived.SwapGLSurfaceView.this     // Catch:{ all -> 0x00d0 }
                r12 = 0
                boolean unused = r11.mSizeChanged = r12     // Catch:{ all -> 0x00d0 }
                r11 = 0
                r14.mRequestRender = r11     // Catch:{ all -> 0x00d0 }
                boolean r11 = r14.mHasSurface     // Catch:{ all -> 0x00d0 }
                if (r11 == 0) goto L_0x006e
                boolean r11 = r14.mWaitingForSurface     // Catch:{ all -> 0x00d0 }
                if (r11 == 0) goto L_0x006e
                r1 = 1
                r11 = 0
                r14.mWaitingForSurface = r11     // Catch:{ all -> 0x00d0 }
                android.opengl.derived.SwapGLSurfaceView$GLThreadManager r11 = android.opengl.derived.SwapGLSurfaceView.sGLThreadManager     // Catch:{ all -> 0x00d0 }
                r11.notifyAll()     // Catch:{ all -> 0x00d0 }
                goto L_0x006e
            L_0x00d0:
                r11 = move-exception
                monitor-exit(r10)     // Catch:{ all -> 0x00d0 }
                throw r11     // Catch:{ all -> 0x00d3 }
            L_0x00d3:
                r10 = move-exception
                android.opengl.derived.SwapGLSurfaceView$GLThreadManager r11 = android.opengl.derived.SwapGLSurfaceView.sGLThreadManager
                monitor-enter(r11)
                r14.stopEglLocked()     // Catch:{ all -> 0x013e }
                monitor-exit(r11)     // Catch:{ all -> 0x013e }
                throw r10
            L_0x00de:
                android.opengl.derived.SwapGLSurfaceView$GLThreadManager r11 = android.opengl.derived.SwapGLSurfaceView.sGLThreadManager     // Catch:{ all -> 0x00d0 }
                r11.wait()     // Catch:{ all -> 0x00d0 }
                goto L_0x001d
            L_0x00e7:
                if (r5 == 0) goto L_0x00eb
                r8 = 1
                r1 = 1
            L_0x00eb:
                if (r1 == 0) goto L_0x00fe
                android.opengl.derived.SwapGLSurfaceView$EglHelper r10 = r14.mEglHelper     // Catch:{ all -> 0x00d3 }
                android.opengl.derived.SwapGLSurfaceView r11 = android.opengl.derived.SwapGLSurfaceView.this     // Catch:{ all -> 0x00d3 }
                android.view.SurfaceHolder r11 = r11.getHolder()     // Catch:{ all -> 0x00d3 }
                javax.microedition.khronos.opengles.GL r10 = r10.createSurface(r11)     // Catch:{ all -> 0x00d3 }
                r0 = r10
                javax.microedition.khronos.opengles.GL10 r0 = (javax.microedition.khronos.opengles.GL10) r0     // Catch:{ all -> 0x00d3 }
                r3 = r0
                r7 = 1
            L_0x00fe:
                if (r8 == 0) goto L_0x010a
                android.opengl.GLSurfaceView$Renderer r10 = r14.mRenderer     // Catch:{ all -> 0x00d3 }
                android.opengl.derived.SwapGLSurfaceView$EglHelper r11 = r14.mEglHelper     // Catch:{ all -> 0x00d3 }
                javax.microedition.khronos.egl.EGLConfig r11 = r11.mEglConfig     // Catch:{ all -> 0x00d3 }
                r10.onSurfaceCreated(r3, r11)     // Catch:{ all -> 0x00d3 }
                r8 = 0
            L_0x010a:
                if (r7 == 0) goto L_0x0112
                android.opengl.GLSurfaceView$Renderer r10 = r14.mRenderer     // Catch:{ all -> 0x00d3 }
                r10.onSurfaceChanged(r3, r9, r4)     // Catch:{ all -> 0x00d3 }
                r7 = 0
            L_0x0112:
                if (r9 <= 0) goto L_0x000d
                if (r4 <= 0) goto L_0x000d
                android.opengl.GLSurfaceView$Renderer r10 = r14.mRenderer     // Catch:{ all -> 0x00d3 }
                r10.onDrawFrame(r3)     // Catch:{ all -> 0x00d3 }
                android.opengl.derived.SwapGLSurfaceView r10 = android.opengl.derived.SwapGLSurfaceView.this     // Catch:{ all -> 0x00d3 }
                boolean r10 = r10.mNeedsSwap     // Catch:{ all -> 0x00d3 }
                if (r10 == 0) goto L_0x000d
                android.opengl.derived.SwapGLSurfaceView$EglHelper r10 = r14.mEglHelper     // Catch:{ all -> 0x00d3 }
                r10.swap()     // Catch:{ all -> 0x00d3 }
                android.opengl.derived.SwapGLSurfaceView r10 = android.opengl.derived.SwapGLSurfaceView.this     // Catch:{ all -> 0x00d3 }
                r11 = 0
                boolean unused = r10.mNeedsSwap = r11     // Catch:{ all -> 0x00d3 }
                goto L_0x000d
            L_0x0130:
                android.opengl.derived.SwapGLSurfaceView$GLThreadManager r10 = android.opengl.derived.SwapGLSurfaceView.sGLThreadManager
                monitor-enter(r10)
                r14.stopEglLocked()     // Catch:{ all -> 0x013b }
                monitor-exit(r10)     // Catch:{ all -> 0x013b }
                goto L_0x0047
            L_0x013b:
                r11 = move-exception
                monitor-exit(r10)     // Catch:{ all -> 0x013b }
                throw r11
            L_0x013e:
                r10 = move-exception
                monitor-exit(r11)     // Catch:{ all -> 0x013e }
                throw r10
            */
            throw new UnsupportedOperationException("Method not decompiled: android.opengl.derived.SwapGLSurfaceView.GLThread.guardedRun():void");
        }

        private boolean isDone() {
            boolean z;
            synchronized (SwapGLSurfaceView.sGLThreadManager) {
                z = this.mDone;
            }
            return z;
        }

        public void setRenderMode(int renderMode) {
            if (renderMode < 0 || renderMode > 1) {
                throw new IllegalArgumentException("renderMode");
            }
            synchronized (SwapGLSurfaceView.sGLThreadManager) {
                this.mRenderMode = renderMode;
                if (renderMode == 1) {
                    SwapGLSurfaceView.sGLThreadManager.notifyAll();
                }
            }
        }

        public int getRenderMode() {
            int i;
            synchronized (SwapGLSurfaceView.sGLThreadManager) {
                i = this.mRenderMode;
            }
            return i;
        }

        public void requestRender() {
            synchronized (SwapGLSurfaceView.sGLThreadManager) {
                this.mRequestRender = true;
                SwapGLSurfaceView.sGLThreadManager.notifyAll();
            }
        }

        public void surfaceCreated() {
            synchronized (SwapGLSurfaceView.sGLThreadManager) {
                this.mHasSurface = true;
                SwapGLSurfaceView.sGLThreadManager.notifyAll();
            }
        }

        public void surfaceDestroyed() {
            synchronized (SwapGLSurfaceView.sGLThreadManager) {
                this.mHasSurface = false;
                SwapGLSurfaceView.sGLThreadManager.notifyAll();
                while (!this.mWaitingForSurface && isAlive() && !this.mDone) {
                    try {
                        SwapGLSurfaceView.sGLThreadManager.wait();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }

        public void onPause() {
            synchronized (SwapGLSurfaceView.sGLThreadManager) {
                this.mPaused = true;
                SwapGLSurfaceView.sGLThreadManager.notifyAll();
            }
        }

        public void onResume() {
            synchronized (SwapGLSurfaceView.sGLThreadManager) {
                this.mPaused = false;
                this.mRequestRender = true;
                SwapGLSurfaceView.sGLThreadManager.notifyAll();
            }
        }

        public void onWindowResize(int w, int h) {
            synchronized (SwapGLSurfaceView.sGLThreadManager) {
                this.mWidth = w;
                this.mHeight = h;
                boolean unused = SwapGLSurfaceView.this.mSizeChanged = true;
                SwapGLSurfaceView.sGLThreadManager.notifyAll();
            }
        }

        public void requestExitAndWait() {
            synchronized (SwapGLSurfaceView.sGLThreadManager) {
                this.mDone = true;
                SwapGLSurfaceView.sGLThreadManager.notifyAll();
            }
            try {
                join();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        public void queueEvent(Runnable r) {
            synchronized (this) {
                this.mEventQueue.add(r);
                synchronized (SwapGLSurfaceView.sGLThreadManager) {
                    this.mEventsWaiting = true;
                    SwapGLSurfaceView.sGLThreadManager.notifyAll();
                }
            }
        }

        private Runnable getEvent() {
            synchronized (this) {
                if (this.mEventQueue.size() <= 0) {
                    return null;
                }
                Runnable remove = this.mEventQueue.remove(0);
                return remove;
            }
        }
    }

    static class LogWriter extends Writer {
        private StringBuilder mBuilder = new StringBuilder();

        LogWriter() {
        }

        public void close() {
            flushBuilder();
        }

        public void flush() {
            flushBuilder();
        }

        public void write(char[] buf, int offset, int count) {
            for (int i = 0; i < count; i++) {
                char c = buf[offset + i];
                if (c == 10) {
                    flushBuilder();
                } else {
                    this.mBuilder.append(c);
                }
            }
        }

        private void flushBuilder() {
            if (this.mBuilder.length() > 0) {
                Log.v("GLSurfaceView", this.mBuilder.toString());
                this.mBuilder.delete(0, this.mBuilder.length());
            }
        }
    }

    private void checkRenderThreadState() {
        if (this.mGLThread != null) {
            throw new IllegalStateException("setRenderer has already been called for this instance.");
        }
    }

    private static class GLThreadManager {
        private GLThread mEglOwner;

        private GLThreadManager() {
        }

        public synchronized void threadExiting(GLThread thread) {
            boolean unused = thread.mDone = true;
            if (this.mEglOwner == thread) {
                this.mEglOwner = null;
            }
            notifyAll();
        }

        public synchronized boolean tryAcquireEglSurface(GLThread thread) {
            boolean z;
            if (this.mEglOwner == thread || this.mEglOwner == null) {
                this.mEglOwner = thread;
                notifyAll();
                z = true;
            } else {
                z = false;
            }
            return z;
        }

        public synchronized void releaseEglSurface(GLThread thread) {
            if (this.mEglOwner == thread) {
                this.mEglOwner = null;
            }
            notifyAll();
        }
    }

    public void setNeedsSwap() {
        this.mNeedsSwap = true;
    }

    public void clearNeedsSwap() {
        this.mNeedsSwap = false;
    }

    public View getView() {
        return this;
    }
}
