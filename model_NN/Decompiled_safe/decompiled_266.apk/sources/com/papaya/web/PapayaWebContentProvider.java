package com.papaya.web;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.papaya.si.C0001a;
import com.papaya.si.X;
import com.papaya.si.bz;
import java.io.File;
import java.io.FileNotFoundException;

public class PapayaWebContentProvider extends ContentProvider {
    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    public boolean onCreate() {
        return false;
    }

    public ParcelFileDescriptor openFile(Uri uri, String str) throws FileNotFoundException {
        boolean z;
        String str2;
        try {
            String uri2 = uri.toString();
            if (uri2.startsWith("file:///android_asset/")) {
                str2 = uri2.substring("file:///android_asset/".length());
                z = true;
            } else if (uri2.startsWith(bz.mW)) {
                str2 = uri2.substring(bz.mW.length());
                z = false;
            } else {
                z = false;
                str2 = null;
            }
            if (str2 != null && !z) {
                File file = new File(C0001a.getWebCache().getCacheDir(), str2);
                if (!file.exists()) {
                    X.w("cache file doesn't exist %s", file);
                }
                return ParcelFileDescriptor.open(file, 268435456);
            }
        } catch (Exception e) {
            X.w(e, "openFile failed %s", uri);
        }
        return null;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        return null;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return 0;
    }
}
