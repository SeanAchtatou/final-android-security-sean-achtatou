package com.papaya.web;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import com.adknowledge.superrewards.model.SROffer;
import com.inmobi.androidsdk.InMobiAdDelegate;
import com.papaya.base.TitleActivity;
import com.papaya.si.C0028b;
import com.papaya.si.C0030bb;
import com.papaya.si.C0034bf;
import com.papaya.si.C0037c;
import com.papaya.si.C0060z;
import com.papaya.si.G;
import com.papaya.si.X;
import com.papaya.si.aV;
import com.papaya.si.bE;
import com.papaya.si.bG;
import com.papaya.si.bp;
import com.papaya.si.bt;
import com.papaya.si.bv;
import com.papaya.utils.CountryCodeActivity;
import com.papaya.view.TakePhotoBridge;
import java.io.ByteArrayOutputStream;

public class WebActivity extends TitleActivity implements bt.a {
    public static final String EXTRA_BACKABLE = "extra_backable";
    public static final String EXTRA_INIT_URL = "init_url";
    public static final String EXTRA_REQUIRE_SID = "extra_require_sid";
    public static final String EXTRA_SUPPORT_MENU = "extra_support_menu";
    private bG lP;
    private boolean mO = true;
    private boolean mP = true;

    public void connectionFailed(bt btVar, int i) {
        if (this.lP != null) {
            this.lP.hideLoading();
        }
    }

    public void connectionFinished(bt btVar) {
        if (this.lP != null) {
            this.lP.hideLoading();
        }
    }

    /* access modifiers changed from: protected */
    public View createContentView(Bundle bundle) {
        this.lP = prepareWebViewController();
        this.lP.setOwnerActivity(this);
        this.mO = loadBackable();
        this.mP = loadSupportMenu();
        this.lP.setRequireSid(loadRequireSid());
        return this.lP.getContentLayout();
    }

    public void finish() {
        super.finish();
    }

    /* access modifiers changed from: protected */
    public String getDefaultInitUrl() {
        return null;
    }

    /* access modifiers changed from: protected */
    public String getInitUrl() {
        String stringExtra = getIntent().getStringExtra(EXTRA_INIT_URL);
        return aV.isNotEmpty(stringExtra) ? stringExtra : getDefaultInitUrl();
    }

    public String getTabName() {
        return "";
    }

    public bG getWebViewController() {
        return this.lP;
    }

    /* access modifiers changed from: protected */
    public boolean loadBackable() {
        return getIntent().getBooleanExtra(EXTRA_BACKABLE, true);
    }

    /* access modifiers changed from: protected */
    public boolean loadRequireSid() {
        return getIntent().getBooleanExtra(EXTRA_REQUIRE_SID, true);
    }

    /* access modifiers changed from: protected */
    public boolean loadSupportMenu() {
        return getIntent().getBooleanExtra(EXTRA_SUPPORT_MENU, true);
    }

    /* access modifiers changed from: protected */
    public int myLayout() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        switch (i) {
            case 1:
            case 2:
            case 3:
            case 4:
                if (i2 == -1) {
                    Bitmap bitmap = null;
                    if (i == 1) {
                        try {
                            bitmap = C0030bb.getCameraBitmap(this, intent, 128, 128, true);
                        } catch (Exception e) {
                            X.w("Failed to handle activity result: %d, %s", Integer.valueOf(i), e);
                            return;
                        }
                    } else if (i == 3) {
                        bitmap = C0030bb.getCameraBitmap(this, intent, 320, 320, true);
                    } else if (i == 2) {
                        bitmap = C0030bb.createScaledBitmap(getContentResolver(), intent.getData(), 128, 128, true);
                    } else if (i == 4) {
                        bitmap = C0030bb.createScaledBitmap(getContentResolver(), intent.getData(), 320, 320, true);
                    }
                    if (bitmap != null) {
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 35, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        if (byteArray.length > 0) {
                            if (i == 2 || i == 1) {
                                bv bvVar = new bv(C0034bf.createURL("json_changehp"), false);
                                bvVar.addPostParam(SROffer.ID, aV.format("%d-%d", Integer.valueOf(hashCode()), 74565));
                                bvVar.addPostParam(SROffer.TYPE, "jpg");
                                bvVar.addPostParam("photo", byteArray, 2);
                                bvVar.setDelegate(this);
                                bvVar.setDispatchable(true);
                                bvVar.start(false);
                            } else {
                                bv bvVar2 = new bv(C0034bf.createURL("json_uploadpic"), false);
                                bvVar2.addPostParam(SROffer.ID, aV.format("%d-%d", Integer.valueOf(hashCode()), 1193046));
                                bvVar2.addPostParam(SROffer.TYPE, "jpg");
                                bvVar2.addPostParam("photo", byteArray, 2);
                                bvVar2.setDelegate(this);
                                bvVar2.setDispatchable(true);
                                bvVar2.start(false);
                            }
                            bp.startUploading(this);
                            this.lP.showLoading();
                        }
                        bitmap.recycle();
                        return;
                    }
                    return;
                }
                return;
            case 5:
                return;
            case 6:
                if (i2 == -1) {
                    int intExtra = intent.getIntExtra("CountryIndex", 0);
                    String stringExtra = intent.getStringExtra("action");
                    String str = CountryCodeActivity.gl[intExtra];
                    String str2 = CountryCodeActivity.gk[intExtra];
                    if (stringExtra != null) {
                        this.lP.callJS(aV.format("%s('%s', '%s')", stringExtra, C0034bf.escapeJS(str), str2));
                        return;
                    }
                    return;
                }
                return;
            case 7:
            case 9:
            default:
                X.w("unknown request %d", Integer.valueOf(i));
                return;
            case 8:
                G.onActivityFinished(i2, intent);
                return;
            case 10:
                if (i2 == -1 && intent != null) {
                    String stringExtra2 = intent.getStringExtra("url");
                    if (!aV.isEmpty(stringExtra2)) {
                        this.lP.openUrl(stringExtra2);
                        return;
                    }
                    String stringExtra3 = intent.getStringExtra("script");
                    if (!aV.isEmpty(stringExtra2)) {
                        this.lP.callJS(stringExtra3);
                        return;
                    }
                    return;
                }
                return;
            case 11:
            case 12:
                bE.onPhotoTaken(this, i, i2, intent);
                return;
            case InMobiAdDelegate.INMOBI_AD_UNIT_120X600:
            case 14:
                TakePhotoBridge.onPhtotoTaken(this, i, i2, intent);
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.lP != null) {
            this.lP.onOrientationChanged(configuration.orientation);
        }
    }

    public void onDestroy() {
        if (this.lP != null) {
            this.lP.close();
            C0030bb.removeFromSuperView(this.lP.getContentLayout());
        }
        this.lP = null;
        super.onDestroy();
    }

    public void onImageUploadFailed(int i) {
        if (this.lP != null) {
            this.lP.hideLoading();
        }
        C0028b.showInfo(C0037c.getApplicationContext().getString(C0060z.stringID("fail_to_upload_photo")));
    }

    public void onImageUploaded(int i, int i2) {
        if (this.lP == null) {
            return;
        }
        if (i == 74565) {
            this.lP.hideLoading();
            this.lP.callJS("changehp()");
        } else if (i == 1193046) {
            this.lP.hideLoading();
            this.lP.callJS(aV.format("photouploaded(%d)", Integer.valueOf(i2)));
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            if (this.lP != null && this.lP.onBackClicked()) {
                return true;
            }
            if (!this.mO) {
                return true;
            }
        } else if (i == 82 && !this.mP) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.lP != null) {
            this.lP.onPause();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.lP != null) {
            this.lP.onResume();
        }
    }

    public void openUrl(String str) {
        if (this.lP != null) {
            this.lP.openUrl(str);
        }
    }

    /* access modifiers changed from: protected */
    public bG prepareWebViewController() {
        return new bG(C0037c.getApplicationContext(), getInitUrl());
    }

    public void priaBack(int i) {
        if (i == 0) {
            super.onKeyDown(4, null);
        } else {
            onKeyDown(4, null);
        }
    }
}
