package com.papaya.achievement;

import android.graphics.drawable.Drawable;
import com.papaya.si.C0001a;
import com.papaya.si.C0027az;
import com.papaya.si.C0030bb;

public class PPYAchievement {
    private Drawable G;
    private CharSequence H;
    private boolean I;
    private boolean J;
    private CharSequence description;
    private int id;

    public PPYAchievement(int i, CharSequence charSequence, CharSequence charSequence2, boolean z, boolean z2) {
        this.H = charSequence;
        this.description = charSequence2;
        this.I = z;
        this.J = z2;
        this.id = i;
    }

    private String cacheName() {
        return "achievementicon?id=" + this.id;
    }

    public void downloadIcon(Boolean bool, PPYAchievementDelegate pPYAchievementDelegate) {
        if (!bool.booleanValue() || C0001a.getWebCache().cachedFile(cacheName(), true) == null) {
            new C0027az(this, pPYAchievementDelegate).downloadicon();
        } else if (pPYAchievementDelegate != null) {
            pPYAchievementDelegate.onDownloadIconSuccess(C0030bb.bitmapFromFD(C0001a.getWebCache().cachedFD(cacheName())));
        }
    }

    public CharSequence getDescription() {
        return this.description;
    }

    public Drawable getIconDrawable() {
        return this.G;
    }

    public int getId() {
        return this.id;
    }

    public CharSequence getTitle() {
        return this.H;
    }

    public CharSequence iconUrl() {
        return "";
    }

    public boolean isSecret() {
        return this.I;
    }

    public boolean isUnlocked() {
        return this.J;
    }

    public void setIconDrawable(Drawable drawable) {
        this.G = drawable;
    }

    public void unlock(PPYAchievementDelegate pPYAchievementDelegate) {
        if (!this.J) {
            new C0027az(this, pPYAchievementDelegate).unlock();
        }
    }

    public CharSequence unlockDate() {
        return "";
    }
}
