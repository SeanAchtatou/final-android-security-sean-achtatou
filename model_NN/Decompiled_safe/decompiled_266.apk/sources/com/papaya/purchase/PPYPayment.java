package com.papaya.purchase;

import android.graphics.drawable.Drawable;
import org.json.JSONObject;

public class PPYPayment {
    private Drawable G;
    private CharSequence description;
    private CharSequence eA;
    private int eB;
    private JSONObject eC;
    private PPYPaymentDelegate eD;
    private String eE;
    private String eF;
    private int ey;
    private String ez;

    public PPYPayment(CharSequence charSequence, CharSequence charSequence2, int i, JSONObject jSONObject, PPYPaymentDelegate pPYPaymentDelegate) {
        this.eA = charSequence;
        this.description = charSequence2;
        this.eB = i;
        this.eC = jSONObject;
        this.eD = pPYPaymentDelegate;
    }

    public PPYPaymentDelegate getDelegate() {
        return this.eD;
    }

    public CharSequence getDescription() {
        return this.description;
    }

    public Drawable getIconDrawable() {
        return this.G;
    }

    public int getIconID() {
        return this.ey;
    }

    public String getIconURL() {
        return this.ez;
    }

    public CharSequence getName() {
        return this.eA;
    }

    public int getPapayas() {
        return this.eB;
    }

    public JSONObject getPayload() {
        return this.eC;
    }

    public String getReceipt() {
        return this.eF;
    }

    public String getTransactionID() {
        return this.eE;
    }

    public void setDelegate(PPYPaymentDelegate pPYPaymentDelegate) {
        this.eD = pPYPaymentDelegate;
    }

    public void setIconDrawable(Drawable drawable) {
        this.G = drawable;
    }

    public void setIconID(int i) {
        this.ey = i;
    }

    public void setIconURL(String str) {
        this.ez = str;
    }

    public void setReceipt(String str) {
        this.eF = str;
    }

    public void setTransactionID(String str) {
        this.eE = str;
    }
}
