package com.papaya.analytics;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.papaya.si.X;
import com.papaya.si.aV;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class PPYReferralReceiver extends BroadcastReceiver {
    private static String ac = null;
    private static HashMap<String, String> ad = new HashMap<>();

    public static JSONObject getReferralJSON(Context context) {
        loadReferrer(context);
        JSONObject jSONObject = new JSONObject();
        try {
            for (String next : ad.keySet()) {
                jSONObject.put(next, ad.get(next));
            }
        } catch (JSONException e) {
            X.w(e, "Failed to convert to json", new Object[0]);
        }
        return jSONObject;
    }

    public static Map<String, String> getReferrals(Context context) {
        loadReferrer(context);
        return ad;
    }

    public static String getReferrer(Context context) {
        loadReferrer(context);
        return ac;
    }

    public static String getReferrerValue(Context context, String str, String str2) {
        loadReferrer(context);
        return ad.get(str) == null ? str2 : ad.get(str);
    }

    private static void loadReferrer(Context context) {
        if (ac == null) {
            ac = "";
            ad = parseReferrer("");
        }
    }

    private static HashMap<String, String> parseReferrer(String str) {
        HashMap<String, String> hashMap = new HashMap<>();
        if (!aV.isEmpty(str)) {
            try {
                for (String str2 : str.split("&")) {
                    int indexOf = str2.indexOf(61);
                    if (indexOf != -1) {
                        hashMap.put(str2.substring(0, indexOf), str2.substring(indexOf + 1));
                    }
                }
            } catch (Exception e) {
                X.w(e, "Failed to parse referrer", new Object[0]);
            }
        }
        return hashMap;
    }

    public static void sendReferrerReport(Context context) {
    }

    public void onReceive(Context context, Intent intent) {
    }
}
