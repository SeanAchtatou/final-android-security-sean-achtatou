package com.papaya.social;

public class BillingChannel {
    public static final int ALL = Integer.MAX_VALUE;
    public static final int BILLING_REVOLUTION = 4;
    public static final int GOOGLE_CHECKOUT = 1;
    public static final int NONE = 0;
    public static final int PAYPAL = 2;
    public static final int REDEEM = 16;
    public static final int ZONG = 8;

    private BillingChannel() {
    }
}
