package com.papaya.social;

import android.content.Context;
import com.papaya.achievement.PPYAchievementDelegate;
import com.papaya.purchase.PPYPayment;
import com.papaya.si.C0023av;
import com.papaya.si.C0025ax;
import com.papaya.si.X;
import com.papaya.social.PPYSocialQuery;
import java.util.Collections;
import java.util.List;

public class PPYSocial {
    public static final String LANG_AUTO = "auto";
    public static final String LANG_EN = "en";
    public static final String LANG_ZH_CN = "zh_CN";
    public static final int UI_ACHIEVEMENT = 8;
    public static final int UI_APPS = 12;
    public static final int UI_AVATAR = 4;
    public static final int UI_CHALLENGE = 11;
    public static final int UI_CIRCLE = 9;
    public static final int UI_FRIENDS = 1;
    public static final int UI_HOME = 0;
    public static final int UI_INVITE = 7;
    public static final int UI_LEADERBOARD = 5;
    public static final int UI_LOCATION = 10;
    public static final int UI_OFFLINE_LEADERBOARD = 6;
    public static final int UI_PHOTO = 2;
    public static final int UI_PMAIL = 3;

    public static abstract class Config {
        public String getAndroidMapsAPIKey() {
            return null;
        }

        public abstract String getApiKey();

        public int getBillingChannels() {
            return BillingChannel.ALL;
        }

        public abstract String getPreferredLanguage();

        public Class getRClass() {
            return null;
        }

        public boolean isLeaderboardVisible() {
            return true;
        }

        public int timeToShowRegistration() {
            return 0;
        }
    }

    public interface Delegate {
        void onAccountChanged(int i, int i2);

        void onScoreUpdated();

        void onSessionUpdated();
    }

    public static void addDelegate(Delegate delegate) {
        C0023av.getInstance().addDelegate(delegate);
    }

    public static void addPayment(Context context, PPYPayment pPYPayment) {
        try {
            if (checkInitialized()) {
                C0023av.getInstance().addPayment(context, pPYPayment);
            }
        } catch (Exception e) {
            X.w(e, "Failed to addPayment", new Object[0]);
        }
    }

    private static boolean checkInitialized() {
        boolean isInitialized = C0023av.getInstance().isInitialized();
        if (!isInitialized) {
            X.e("Social SDK is not initialized", new Object[0]);
        }
        return isInitialized;
    }

    public static void dispose() {
    }

    public static String getAvatarUrlString(int i) {
        return C0023av.getInstance().getAvatarUrlString(i);
    }

    public static void initWithConfig(Context context, Config config) {
        C0023av.getInstance().initialize(context, config);
    }

    public static void listAchievements(PPYAchievementDelegate pPYAchievementDelegate) {
        try {
            if (checkInitialized()) {
                C0023av.getInstance().getAchievementList(pPYAchievementDelegate);
            }
        } catch (Exception e) {
            X.w(e, "Failed to getAchievementList( %s)", pPYAchievementDelegate.toString());
        }
    }

    public static List<PPYLocalScore> listLocalScores(int i, String str) {
        try {
            if (checkInitialized()) {
                return C0023av.getInstance().getScoreDatabase(str).listScores(i);
            }
        } catch (Exception e) {
            X.w(e, "Failed to listLocalScores(%d, %s)", Integer.valueOf(i), str);
        }
        return Collections.emptyList();
    }

    public static void loadAchievement(int i, PPYAchievementDelegate pPYAchievementDelegate) {
        try {
            if (checkInitialized()) {
                C0023av.getInstance().loadAchievement(i, pPYAchievementDelegate);
            }
        } catch (Exception e) {
            X.w(e, "Failed to loadAchievement(%s,%s)", Integer.valueOf(i), pPYAchievementDelegate.toString());
        }
    }

    public static void query(PPYSocialQuery pPYSocialQuery) {
        try {
            if (checkInitialized()) {
                C0023av.getInstance().submitQuery(pPYSocialQuery);
            }
        } catch (Exception e) {
            X.w(e, "Failed to query(%s)", pPYSocialQuery);
        }
    }

    public static void recommendMyApp(String str) {
        C0023av.getInstance().recommendMyApp(str);
    }

    public static void removeDelegate(Delegate delegate) {
        C0023av.getInstance().removeDelegate(delegate);
    }

    public static void setScore(int i) {
        try {
            if (checkInitialized()) {
                setScore(i, null);
            }
        } catch (Exception e) {
            X.w(e, "Failed to setScore(%d)", Integer.valueOf(i));
        }
    }

    public static void setScore(int i, String str) {
        try {
            if (checkInitialized()) {
                C0023av.getInstance().setScore(i, str);
            }
        } catch (Exception e) {
            X.w(e, "Failed to setScore(%d, %s)", Integer.valueOf(i), str);
        }
    }

    public static void showChat(Context context) {
        try {
            if (checkInitialized()) {
                C0023av.getInstance().showChat(context);
            }
        } catch (Exception e) {
            X.w(e, "Failed to showChat", new Object[0]);
        }
    }

    public static void showLBS(Context context) {
        try {
            if (checkInitialized()) {
                C0023av.getInstance().showLBS(context);
            }
        } catch (Exception e) {
            X.w(e, "Failed to showLBS", new Object[0]);
        }
    }

    public static void showLeaderboard(Context context, String str, boolean z) {
        try {
            if (checkInitialized()) {
                C0023av.getInstance().showLeaderboard(context, str, z);
            }
        } catch (Exception e) {
            X.w(e, "Failed to showLeaderboard", new Object[0]);
        }
    }

    public static void showSocial(Context context, int i) {
        try {
            if (checkInitialized()) {
                C0023av.getInstance().show(context, i);
            }
        } catch (Exception e) {
            X.w(e, "Failed to showSocial", new Object[0]);
        }
    }

    public static PPYSocialQuery updateNickname(String str, PPYSocialQuery.QueryDelegate queryDelegate) {
        try {
            if (checkInitialized()) {
                return C0025ax.getInstance().updateNickname(str, queryDelegate);
            }
        } catch (Exception e) {
            X.w(e, "Failed to updateNickname(%s, %s)", str, queryDelegate);
        }
        return null;
    }

    public static void updateScore(int i) {
        try {
            if (checkInitialized()) {
                updateScore(i, null);
            }
        } catch (Exception e) {
            X.w(e, "Failed to upteScore(%d)", Integer.valueOf(i));
        }
    }

    public static void updateScore(int i, String str) {
        try {
            if (checkInitialized()) {
                C0023av.getInstance().updateScore(i, str);
            }
        } catch (Exception e) {
            X.w(e, "Failed to updateScore(%d, %s)", Integer.valueOf(i), str);
        }
    }
}
