package com.papaya.social;

public class PPYSocialChallengeRecord {
    public static final int PAYLOAD_BINARY = 1;
    public static final int PAYLOAD_STRING = 0;
    private String eJ;
    private int eO;
    private int eP;
    private int eQ;
    private int eR;
    private String eS;
    private byte[] eT;
    private int eU = 0;

    public int getChallengeDefinitionID() {
        return this.eP;
    }

    public String getMessage() {
        return this.eJ;
    }

    public String getPayload() {
        return this.eS;
    }

    public byte[] getPayloadBinary() {
        return this.eT;
    }

    public int getPayloadType() {
        return this.eU;
    }

    public int getReceiverUserID() {
        return this.eR;
    }

    public int getRecordID() {
        return this.eO;
    }

    public int getSenderUserID() {
        return this.eQ;
    }

    public void setChallengeDefinitionID(int i) {
        this.eP = i;
    }

    public void setMessage(String str) {
        this.eJ = str;
    }

    public void setPayload(String str) {
        this.eS = str;
        this.eT = null;
        this.eU = 0;
    }

    public void setPayloadBinary(byte[] bArr) {
        this.eT = bArr;
        this.eS = null;
        this.eU = 1;
    }

    public void setReceiverUserID(int i) {
        this.eR = i;
    }

    public void setRecordID(int i) {
        this.eO = i;
    }

    public void setSenderUserID(int i) {
        this.eQ = i;
    }
}
