package com.papaya.social;

import com.papaya.si.aU;
import org.json.JSONException;
import org.json.JSONObject;

public class PPYSocialQuery {
    private JSONObject eC;
    private QueryDelegate eW;
    private boolean eX;

    public interface QueryDelegate {
        void onQueryFailed(PPYSocialQuery pPYSocialQuery, String str);

        void onQueryResponse(PPYSocialQuery pPYSocialQuery, JSONObject jSONObject);
    }

    public PPYSocialQuery(String str) {
        this(str, null);
    }

    public PPYSocialQuery(String str, QueryDelegate queryDelegate) {
        this.eX = false;
        this.eC = new JSONObject();
        this.eW = queryDelegate;
        put("api", str);
    }

    public void cancel() {
        this.eX = true;
        this.eW = null;
    }

    public String getAPI() {
        if (this.eC == null) {
            return null;
        }
        return this.eC.optString("api");
    }

    public String getPayloadString() {
        return this.eC.toString();
    }

    public QueryDelegate getQueryDelegate() {
        return this.eW;
    }

    public boolean isCanceled() {
        return this.eX;
    }

    public PPYSocialQuery put(String str, int i) {
        try {
            this.eC.put(str, i);
        } catch (JSONException e) {
        }
        return this;
    }

    public PPYSocialQuery put(String str, String str2) {
        if (str2 == null) {
            return this;
        }
        try {
            this.eC.put(str, str2);
        } catch (JSONException e) {
        }
        return this;
    }

    public PPYSocialQuery put(String str, byte[] bArr) {
        if (bArr == null) {
            return this;
        }
        try {
            this.eC.put(str, aU.a.encode(bArr));
        } catch (JSONException e) {
        }
        return this;
    }

    public PPYSocialQuery setQueryDelegate(QueryDelegate queryDelegate) {
        this.eW = queryDelegate;
        return this;
    }
}
