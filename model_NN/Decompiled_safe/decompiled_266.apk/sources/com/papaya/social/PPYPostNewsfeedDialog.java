package com.papaya.social;

import android.content.Context;
import android.content.DialogInterface;
import android.view.ViewGroup;
import android.widget.TextView;
import com.papaya.si.C0023av;
import com.papaya.si.C0037c;
import com.papaya.si.C0060z;
import com.papaya.view.AppIconView;
import com.papaya.view.CustomDialog;
import com.papaya.view.LazyImageView;

public final class PPYPostNewsfeedDialog extends CustomDialog implements DialogInterface.OnClickListener {
    private String eJ;
    private String eK;
    private TextView eL;
    private LazyImageView eM;

    public PPYPostNewsfeedDialog(Context context, String str) {
        this(context, str, null);
    }

    public PPYPostNewsfeedDialog(Context context, String str, String str2) {
        super(context);
        this.eJ = str;
        this.eK = str2;
        setTitle(C0037c.getString("dlg_post_newsfeed_title"));
        setView(getLayoutInflater().inflate(C0060z.layoutID("newsfeed_dialog_content"), (ViewGroup) null));
        this.eL = (TextView) findViewById(C0060z.id("newsfeed_content"));
        this.eM = (LazyImageView) findViewById(C0060z.id("app_icon"));
        this.eL.setText(str);
        this.eM.setImageUrl(AppIconView.composeAppIconUrl(PPYSession.getInstance().getAppID()));
        setButton(-1, C0037c.getString("button_share"), this);
        setButton(-2, C0037c.getString("button_cancel"), this);
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i == -1) {
            C0023av.getInstance().postNewsfeed(this.eJ, this.eK);
        }
    }

    public final void setMessage(String str) {
        this.eJ = str;
        this.eL.setText(str);
    }

    public final void setUri(String str) {
        this.eK = str;
    }
}
