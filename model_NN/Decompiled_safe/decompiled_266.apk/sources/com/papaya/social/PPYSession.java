package com.papaya.social;

import com.papaya.si.C0025ax;
import java.util.HashMap;
import java.util.List;

public final class PPYSession {
    private static PPYSession eN = new PPYSession();

    private PPYSession() {
    }

    public static PPYSession getInstance() {
        return eN;
    }

    public final int getAppID() {
        return C0025ax.getInstance().getAppID();
    }

    public final String getDefaultBoardID() {
        return C0025ax.getInstance().fJ;
    }

    public final String getNickname() {
        return C0025ax.getInstance().getNickname();
    }

    public final int getScore() {
        return C0025ax.getInstance().getScore();
    }

    public final int getScore(String str) {
        return C0025ax.getInstance().getScore(str);
    }

    public final HashMap<String, Integer> getScores() {
        return C0025ax.getInstance().getScores();
    }

    public final String getSessionReceipt() {
        return C0025ax.getInstance().getSessionReceipt();
    }

    public final String getSessionSecret() {
        return C0025ax.getInstance().getSessionSecret();
    }

    public final int getUID() {
        return C0025ax.getInstance().getUID();
    }

    public final boolean isConnected() {
        return C0025ax.getInstance().isConnected();
    }

    public final boolean isDev() {
        return C0025ax.getInstance().isDev();
    }

    public final List<PPYUser> listFriends() {
        return C0025ax.getInstance().listFriends();
    }

    public final String toString() {
        return "PPYSession, " + "UID:" + getUID() + ", nickname:" + getNickname() + ", scores:" + getScores();
    }
}
