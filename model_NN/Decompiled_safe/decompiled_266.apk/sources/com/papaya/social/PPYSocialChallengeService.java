package com.papaya.social;

import com.papaya.si.C0021at;
import com.papaya.si.C0023av;
import com.papaya.social.PPYSocialQuery;

public class PPYSocialChallengeService {
    public static final int STATUS_FAILED = 3;
    public static final int STATUS_SUCCESS = 2;
    private static PPYSocialChallengeService eV = new PPYSocialChallengeService();

    public interface Delegate {
        void onChallengeAccepted(PPYSocialChallengeRecord pPYSocialChallengeRecord);
    }

    private PPYSocialChallengeService() {
    }

    public static PPYSocialChallengeService getInstance() {
        return eV;
    }

    public void addDelegate(Delegate delegate) {
        C0021at.instance().addDelegate(delegate);
    }

    public void removedelegate(Delegate delegate) {
        C0021at.instance().removedelegate(delegate);
    }

    public PPYSocialQuery sendChallenge(PPYSocialChallengeRecord pPYSocialChallengeRecord, PPYSocialQuery.QueryDelegate queryDelegate) {
        return C0023av.getInstance().sendChallenge(pPYSocialChallengeRecord, queryDelegate);
    }

    public PPYSocialQuery updateChallengeStatus(int i, int i2, PPYSocialQuery.QueryDelegate queryDelegate) {
        return C0023av.getInstance().updateChallengeStatus(i, i2, queryDelegate);
    }
}
