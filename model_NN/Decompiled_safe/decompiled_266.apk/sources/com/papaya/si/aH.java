package com.papaya.si;

import java.util.HashMap;
import java.util.HashSet;

public final class aH implements aP {
    private aO gc = new aO(this);
    private HashMap<Integer, String> gd = new HashMap<>(20);
    private HashSet<Integer> ge = new HashSet<>(10);

    public final boolean contains(int i) {
        if (this.gd.containsKey(Integer.valueOf(i))) {
            return true;
        }
        get(i);
        return false;
    }

    public final void fireDataStateChanged() {
        this.gc.fireDataStateChanged();
    }

    public final String get(int i) {
        if (this.gd.containsKey(Integer.valueOf(i)) || this.ge.contains(Integer.valueOf(i))) {
            return this.gd.get(Integer.valueOf(i));
        }
        C0037c.A.send(602, Integer.valueOf(i));
        this.ge.add(Integer.valueOf(i));
        return null;
    }

    public final void put(int i, String str) {
        this.gd.put(Integer.valueOf(i), str);
        this.ge.remove(Integer.valueOf(i));
    }

    public final void registerMonitor(aN aNVar) {
        this.gc.registerMonitor(aNVar);
    }

    public final void unregisterMonitor(aN aNVar) {
        this.gc.unregisterMonitor(aNVar);
    }
}
