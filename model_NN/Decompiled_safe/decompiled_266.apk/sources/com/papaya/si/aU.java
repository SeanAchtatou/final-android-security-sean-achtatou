package com.papaya.si;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import com.ansca.corona.Crypto;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

public final class aU {
    private static byte[] gH = new byte[0];

    public static class a {
        private static char[] gI = new char[64];
        private static byte[] gJ = new byte[128];

        static {
            char c = 'A';
            int i = 0;
            while (c <= 'Z') {
                gI[i] = c;
                c = (char) (c + 1);
                i++;
            }
            char c2 = 'a';
            while (c2 <= 'z') {
                gI[i] = c2;
                c2 = (char) (c2 + 1);
                i++;
            }
            char c3 = '0';
            while (c3 <= '9') {
                gI[i] = c3;
                c3 = (char) (c3 + 1);
                i++;
            }
            gI[i] = '+';
            gI[i + 1] = '/';
            for (int i2 = 0; i2 < gJ.length; i2++) {
                gJ[i2] = -1;
            }
            for (int i3 = 0; i3 < 64; i3++) {
                gJ[gI[i3]] = (byte) i3;
            }
        }

        private a() {
        }

        public static byte[] decode(String str) {
            return decode(str.toCharArray(), 0, str.length());
        }

        public static byte[] decode(char[] cArr, int i, int i2) {
            int i3;
            char c;
            int i4;
            char c2;
            if (i2 % 4 != 0) {
                throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
            }
            int i5 = i2;
            while (i5 > 0 && cArr[(i + i5) - 1] == '=') {
                i5--;
            }
            int i6 = (i5 * 3) / 4;
            byte[] bArr = new byte[i6];
            int i7 = i5 + i;
            int i8 = 0;
            int i9 = i;
            while (i9 < i7) {
                int i10 = i9 + 1;
                char c3 = cArr[i9];
                int i11 = i10 + 1;
                char c4 = cArr[i10];
                if (i11 < i7) {
                    i3 = i11 + 1;
                    c = cArr[i11];
                } else {
                    i3 = i11;
                    c = 'A';
                }
                if (i3 < i7) {
                    i4 = i3 + 1;
                    c2 = cArr[i3];
                } else {
                    i4 = i3;
                    c2 = 'A';
                }
                if (c3 > 127 || c4 > 127 || c > 127 || c2 > 127) {
                    throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
                }
                byte b = gJ[c3];
                byte b2 = gJ[c4];
                byte b3 = gJ[c];
                byte b4 = gJ[c2];
                if (b < 0 || b2 < 0 || b3 < 0 || b4 < 0) {
                    throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
                }
                int i12 = (b << 2) | (b2 >>> 4);
                int i13 = ((b2 & 15) << 4) | (b3 >>> 2);
                byte b5 = ((b3 & 3) << 6) | b4;
                int i14 = i8 + 1;
                bArr[i8] = (byte) i12;
                if (i14 < i6) {
                    i8 = i14 + 1;
                    bArr[i14] = (byte) i13;
                } else {
                    i8 = i14;
                }
                if (i8 < i6) {
                    bArr[i8] = (byte) b5;
                    i8++;
                }
                i9 = i4;
            }
            return bArr;
        }

        public static String decodeString(String str) {
            return new String(decode(str));
        }

        public static String encode(byte[] bArr) {
            return new String(encode(bArr, 0, bArr.length));
        }

        public static char[] encode(byte[] bArr, int i, int i2) {
            int i3;
            byte b;
            int i4;
            byte b2;
            int i5 = ((i2 * 4) + 2) / 3;
            char[] cArr = new char[(((i2 + 2) / 3) * 4)];
            int i6 = i + i2;
            int i7 = 0;
            int i8 = i;
            while (i8 < i6) {
                int i9 = i8 + 1;
                byte b3 = bArr[i8] & 255;
                if (i9 < i6) {
                    i3 = i9 + 1;
                    b = bArr[i9] & 255;
                } else {
                    i3 = i9;
                    b = 0;
                }
                if (i3 < i6) {
                    i4 = i3 + 1;
                    b2 = bArr[i3] & 255;
                } else {
                    i4 = i3;
                    b2 = 0;
                }
                int i10 = b3 >>> 2;
                int i11 = ((b3 & 3) << 4) | (b >>> 4);
                int i12 = ((b & 15) << 2) | (b2 >>> 6);
                byte b4 = b2 & 63;
                int i13 = i7 + 1;
                cArr[i7] = gI[i10];
                int i14 = i13 + 1;
                cArr[i13] = gI[i11];
                cArr[i14] = i14 < i5 ? gI[i12] : '=';
                int i15 = i14 + 1;
                cArr[i15] = i15 < i5 ? gI[b4] : '=';
                i7 = i15 + 1;
                i8 = i4;
            }
            return cArr;
        }
    }

    public static class b {
        private Key gK;
        private PBEParameterSpec gL;
        private Cipher gM;

        public b(String str, byte[] bArr, int i) {
            try {
                this.gK = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(new PBEKeySpec(str.toCharArray()));
                this.gL = new PBEParameterSpec(bArr, i);
                this.gM = Cipher.getInstance("PBEWithMD5AndDES/CBC/PKCS5Padding");
                this.gM = Cipher.getInstance("PBEWithMD5AndDES");
            } catch (Exception e) {
            }
        }

        public final String decrypt(String str) {
            try {
                this.gM.init(2, this.gK, this.gL);
                return new String(this.gM.doFinal(a.decode(str)), "UTF-8");
            } catch (Exception e) {
                return str;
            }
        }

        public final String encrypt(String str) {
            try {
                return encrypt(str.getBytes("UTF-8"));
            } catch (Exception e) {
                X.e(e, "Failed to encrypt", new Object[0]);
                return str;
            }
        }

        public final String encrypt(byte[] bArr) {
            try {
                this.gM.init(1, this.gK, this.gL);
                return a.encode(this.gM.doFinal(bArr));
            } catch (Exception e) {
                return new String(bArr);
            }
        }
    }

    private aU() {
    }

    public static void clearDir(File file) {
        if (file.exists() && file.isDirectory()) {
            for (File deleteFileOrDir : file.listFiles()) {
                deleteFileOrDir(deleteFileOrDir);
            }
        }
    }

    public static boolean close(ParcelFileDescriptor parcelFileDescriptor) {
        if (parcelFileDescriptor == null) {
            return true;
        }
        try {
            parcelFileDescriptor.close();
            return true;
        } catch (IOException e) {
            X.w(e, "Failed to close descriptor", new Object[0]);
            return false;
        }
    }

    public static boolean close(Closeable closeable) {
        if (closeable == null) {
            return true;
        }
        try {
            closeable.close();
            return true;
        } catch (IOException e) {
            X.e(e, "Failed to close IO %s", closeable);
            return false;
        }
    }

    public static boolean compressZlib(InputStream inputStream, OutputStream outputStream) {
        if (inputStream == null || outputStream == null) {
            return false;
        }
        byte[] acquireBytes = aV.acquireBytes(1024);
        DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(outputStream);
        while (true) {
            try {
                int read = inputStream.read(acquireBytes);
                if (read != -1) {
                    deflaterOutputStream.write(acquireBytes, 0, read);
                } else {
                    close(inputStream);
                    close(deflaterOutputStream);
                    aV.releaseBytes(acquireBytes);
                    return true;
                }
            } catch (Exception e) {
                X.e(e, "failed to compress", new Object[0]);
                close(inputStream);
                close(deflaterOutputStream);
                aV.releaseBytes(acquireBytes);
                return false;
            } catch (Throwable th) {
                close(inputStream);
                close(deflaterOutputStream);
                aV.releaseBytes(acquireBytes);
                throw th;
            }
        }
    }

    public static byte[] compressZlib(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (compressZlib(new ByteArrayInputStream(bArr), byteArrayOutputStream)) {
            return byteArrayOutputStream.toByteArray();
        }
        return null;
    }

    public static byte[] dataFromFile(File file) {
        if (file == null || !file.exists()) {
            return null;
        }
        try {
            return dataFromStream(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            X.w(e, "Failed to load dataFromFile %s", file);
            return null;
        }
    }

    /* JADX INFO: finally extract failed */
    public static byte[] dataFromStream(InputStream inputStream) {
        if (inputStream == null) {
            return gH;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] acquireBytes = aV.acquireBytes(1024);
        while (true) {
            try {
                int read = inputStream.read(acquireBytes);
                if (read != -1) {
                    byteArrayOutputStream.write(acquireBytes, 0, read);
                } else {
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    close(inputStream);
                    aV.releaseBytes(acquireBytes);
                    return byteArray;
                }
            } catch (IOException e) {
                X.w(e, "Failed to load dataFromStream", new Object[0]);
                close(inputStream);
                aV.releaseBytes(acquireBytes);
                return gH;
            } catch (Throwable th) {
                close(inputStream);
                aV.releaseBytes(acquireBytes);
                throw th;
            }
        }
    }

    public static String decodeData(byte[] bArr, String str) {
        if (bArr == null) {
            return "";
        }
        try {
            return new String(bArr, str);
        } catch (UnsupportedEncodingException e) {
            X.w(e, "failed to decode data %s", str);
            return new String(bArr);
        }
    }

    public static boolean decompressGZip(InputStream inputStream, OutputStream outputStream) {
        GZIPInputStream gZIPInputStream;
        Exception e;
        Throwable th;
        byte[] acquireBytes = aV.acquireBytes(1024);
        try {
            gZIPInputStream = new GZIPInputStream(inputStream);
            while (true) {
                try {
                    int read = gZIPInputStream.read(acquireBytes);
                    if (read != -1) {
                        outputStream.write(acquireBytes, 0, read);
                    } else {
                        close(gZIPInputStream);
                        close(outputStream);
                        aV.releaseBytes(acquireBytes);
                        return true;
                    }
                } catch (Exception e2) {
                    e = e2;
                    try {
                        X.w(e, "Failed to decompressGZip", new Object[0]);
                        close(gZIPInputStream);
                        close(outputStream);
                        aV.releaseBytes(acquireBytes);
                        return false;
                    } catch (Throwable th2) {
                        th = th2;
                        close(gZIPInputStream);
                        close(outputStream);
                        aV.releaseBytes(acquireBytes);
                        throw th;
                    }
                }
            }
        } catch (Exception e3) {
            Exception exc = e3;
            gZIPInputStream = null;
            e = exc;
            X.w(e, "Failed to decompressGZip", new Object[0]);
            close(gZIPInputStream);
            close(outputStream);
            aV.releaseBytes(acquireBytes);
            return false;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            gZIPInputStream = null;
            th = th4;
            close(gZIPInputStream);
            close(outputStream);
            aV.releaseBytes(acquireBytes);
            throw th;
        }
    }

    public static boolean decompressGZipToFile(InputStream inputStream, File file) {
        if (inputStream == null || file == null) {
            return false;
        }
        File parentFile = file.getParentFile();
        if (!parentFile.exists()) {
            parentFile.mkdirs();
        }
        try {
            return decompressGZip(inputStream, new FileOutputStream(file));
        } catch (FileNotFoundException e) {
            X.e("Failed to decompressGZipToFile, %s", file);
            return false;
        }
    }

    public static byte[] decompressZlib(InputStream inputStream) {
        boolean z;
        InflaterInputStream inflaterInputStream = new InflaterInputStream(inputStream);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] acquireBytes = aV.acquireBytes(1024);
        while (true) {
            try {
                int read = inflaterInputStream.read(acquireBytes);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(acquireBytes, 0, read);
            } catch (IOException e) {
                X.e(e, "failed to decompress", new Object[0]);
                close(inflaterInputStream);
                close(byteArrayOutputStream);
                aV.releaseBytes(acquireBytes);
                z = true;
            } catch (Throwable th) {
                close(inflaterInputStream);
                close(byteArrayOutputStream);
                aV.releaseBytes(acquireBytes);
                throw th;
            }
        }
        close(inflaterInputStream);
        close(byteArrayOutputStream);
        aV.releaseBytes(acquireBytes);
        z = false;
        if (z) {
            return null;
        }
        return byteArrayOutputStream.toByteArray();
    }

    public static boolean deleteFile(File file) {
        if (file == null) {
            return true;
        }
        return (!file.isFile() || !file.exists()) ? !file.exists() : file.delete();
    }

    public static boolean deleteFileOrDir(File file) {
        if (!exist(file)) {
            return true;
        }
        if (file.isFile()) {
            return file.delete();
        }
        if (file.isDirectory()) {
            File[] listFiles = file.listFiles();
            if (listFiles.length == 0) {
                return file.delete();
            }
            boolean z = true;
            for (File deleteFileOrDir : listFiles) {
                z = z && deleteFileOrDir(deleteFileOrDir);
            }
            return z && file.delete();
        }
        X.w("unknown file type: %s", file);
        return false;
    }

    public static boolean exist(File file) {
        return file != null && file.exists();
    }

    public static AssetFileDescriptor getAssetFileDescriptor(AssetManager assetManager, String str) {
        if (str == null) {
            return null;
        }
        try {
            AssetFileDescriptor openFd = assetManager.openFd(str);
            if (openFd != null) {
                return openFd;
            }
            X.w("%s is null??", str);
            return openFd;
        } catch (Exception e) {
            X.w(e, "%s", str);
            return null;
        }
    }

    public static ParcelFileDescriptor getParcelFileDescriptor(File file, int i) {
        if (file == null || !file.exists()) {
            return null;
        }
        try {
            return ParcelFileDescriptor.open(file, i);
        } catch (Exception e) {
            X.e(e, "Failed to getParcelFileDescriptor", new Object[0]);
            return null;
        }
    }

    public static Collection<String> linesFromStream(InputStream inputStream, Collection<String> collection) {
        BufferedReader bufferedReader;
        Exception e;
        Collection<String> arrayList = collection == null ? new ArrayList<>() : collection;
        if (inputStream != null) {
            BufferedReader bufferedReader2 = null;
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                while (true) {
                    try {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        arrayList.add(readLine.trim());
                    } catch (Exception e2) {
                        e = e2;
                        try {
                            X.w(e, "Failed to load linesFromStream", new Object[0]);
                            close(bufferedReader);
                            return arrayList;
                        } catch (Throwable th) {
                            th = th;
                            bufferedReader2 = bufferedReader;
                            close(bufferedReader2);
                            throw th;
                        }
                    }
                }
                close(bufferedReader);
            } catch (Exception e3) {
                Exception exc = e3;
                bufferedReader = null;
                e = exc;
                X.w(e, "Failed to load linesFromStream", new Object[0]);
                close(bufferedReader);
                return arrayList;
            } catch (Throwable th2) {
                th = th2;
                close(bufferedReader2);
                throw th;
            }
        }
        return arrayList;
    }

    public static <T> T loadPotpStorage(String str) {
        byte[] dataFromFile;
        try {
            File fileStreamPath = C0037c.getApplicationContext().getFileStreamPath(str);
            if (fileStreamPath.exists() && (dataFromFile = dataFromFile(fileStreamPath)) != null) {
                return C0035bg.loads(dataFromFile);
            }
        } catch (Exception e) {
            X.e(e, "Failed to load potp storage: " + str, new Object[0]);
        }
        return null;
    }

    public static Map<String, String> mapFromStream(InputStream inputStream, Map<String, String> map) {
        BufferedReader bufferedReader;
        Exception e;
        int indexOf;
        Map<String, String> hashMap = map == null ? new HashMap<>() : map;
        if (inputStream != null) {
            BufferedReader bufferedReader2 = null;
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                while (true) {
                    try {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        String trim = readLine.trim();
                        if (!trim.startsWith("#") && (indexOf = trim.indexOf(61)) != -1) {
                            hashMap.put(trim.substring(0, indexOf), trim.substring(indexOf + 1));
                        }
                    } catch (Exception e2) {
                        e = e2;
                        try {
                            X.w("Faied to load string resources: " + e, new Object[0]);
                            close(bufferedReader);
                            return hashMap;
                        } catch (Throwable th) {
                            th = th;
                            bufferedReader2 = bufferedReader;
                            close(bufferedReader2);
                            throw th;
                        }
                    }
                }
                close(bufferedReader);
            } catch (Exception e3) {
                Exception exc = e3;
                bufferedReader = null;
                e = exc;
                X.w("Faied to load string resources: " + e, new Object[0]);
                close(bufferedReader);
                return hashMap;
            } catch (Throwable th2) {
                th = th2;
                close(bufferedReader2);
                throw th;
            }
        }
        return hashMap;
    }

    public static String md5(String str) {
        String str2 = str == null ? "" : str;
        try {
            byte[] bytes = str2.getBytes();
            MessageDigest instance = MessageDigest.getInstance(Crypto.ALGORITHM_MD5);
            instance.reset();
            instance.update(bytes);
            byte[] digest = instance.digest();
            StringBuilder acquireStringBuilder = aV.acquireStringBuilder(str2.length());
            for (byte b2 : digest) {
                String hexString = Integer.toHexString(b2 & 255);
                if (hexString.length() > 1) {
                    acquireStringBuilder.append(hexString);
                } else {
                    acquireStringBuilder.append('0').append(hexString);
                }
            }
            return aV.releaseStringBuilder(acquireStringBuilder);
        } catch (NoSuchAlgorithmException e) {
            X.w(e, "Failed to compute md5", new Object[0]);
            return str2;
        }
    }

    public static void saveBitmap(Bitmap bitmap, File file, Bitmap.CompressFormat compressFormat, int i) {
        FileOutputStream fileOutputStream;
        Throwable th;
        Exception e;
        try {
            fileOutputStream = new FileOutputStream(file);
            try {
                bitmap.compress(compressFormat, i, fileOutputStream);
                close(fileOutputStream);
            } catch (Exception e2) {
                e = e2;
                try {
                    X.w(e, "Failed to save bitmap", new Object[0]);
                    close(fileOutputStream);
                } catch (Throwable th2) {
                    th = th2;
                    close(fileOutputStream);
                    throw th;
                }
            }
        } catch (Exception e3) {
            Exception exc = e3;
            fileOutputStream = null;
            e = exc;
            X.w(e, "Failed to save bitmap", new Object[0]);
            close(fileOutputStream);
        } catch (Throwable th3) {
            Throwable th4 = th3;
            fileOutputStream = null;
            th = th4;
            close(fileOutputStream);
            throw th;
        }
    }

    public static String stringFromStream(InputStream inputStream) {
        BufferedReader bufferedReader;
        IOException e;
        StringBuilder acquireStringBuilder = aV.acquireStringBuilder(0);
        BufferedReader bufferedReader2 = null;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            try {
                char[] cArr = new char[1024];
                while (true) {
                    int read = bufferedReader.read(cArr);
                    if (read == -1) {
                        break;
                    }
                    acquireStringBuilder.append(cArr, 0, read);
                }
                close(bufferedReader);
            } catch (IOException e2) {
                e = e2;
                try {
                    X.w(e, "Failed to load stringFromStream", new Object[0]);
                    close(bufferedReader);
                    return aV.releaseStringBuilder(acquireStringBuilder);
                } catch (Throwable th) {
                    th = th;
                    bufferedReader2 = bufferedReader;
                    close(bufferedReader2);
                    throw th;
                }
            }
        } catch (IOException e3) {
            IOException iOException = e3;
            bufferedReader = null;
            e = iOException;
            X.w(e, "Failed to load stringFromStream", new Object[0]);
            close(bufferedReader);
            return aV.releaseStringBuilder(acquireStringBuilder);
        } catch (Throwable th2) {
            th = th2;
            close(bufferedReader2);
            throw th;
        }
        return aV.releaseStringBuilder(acquireStringBuilder);
    }

    public static boolean writeBufferToFile(File file, ByteBuffer byteBuffer) {
        FileChannel fileChannel;
        Throwable th;
        IOException iOException;
        try {
            FileChannel channel = new FileOutputStream(file).getChannel();
            try {
                channel.write(byteBuffer);
                close(channel);
                return true;
            } catch (IOException e) {
                IOException iOException2 = e;
                fileChannel = channel;
                iOException = iOException2;
                try {
                    X.e(iOException, "Failed to write buffer %s", file);
                    close(fileChannel);
                    return false;
                } catch (Throwable th2) {
                    th = th2;
                    close(fileChannel);
                    throw th;
                }
            } catch (Throwable th3) {
                Throwable th4 = th3;
                fileChannel = channel;
                th = th4;
                close(fileChannel);
                throw th;
            }
        } catch (IOException e2) {
            IOException iOException3 = e2;
            fileChannel = null;
            iOException = iOException3;
        } catch (Throwable th5) {
            Throwable th6 = th5;
            fileChannel = null;
            th = th6;
            close(fileChannel);
            throw th;
        }
    }

    public static boolean writeBytesToFile(File file, byte[] bArr) {
        FileOutputStream fileOutputStream;
        Throwable th;
        IOException e;
        if (file == null || bArr == null) {
            return false;
        }
        try {
            fileOutputStream = new FileOutputStream(file);
            try {
                fileOutputStream.write(bArr);
                close(fileOutputStream);
                return true;
            } catch (IOException e2) {
                e = e2;
                try {
                    X.e(e, "Failed to write bytes %s", file);
                    close(fileOutputStream);
                    return false;
                } catch (Throwable th2) {
                    th = th2;
                    close(fileOutputStream);
                    throw th;
                }
            }
        } catch (IOException e3) {
            IOException iOException = e3;
            fileOutputStream = null;
            e = iOException;
            X.e(e, "Failed to write bytes %s", file);
            close(fileOutputStream);
            return false;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            fileOutputStream = null;
            th = th4;
            close(fileOutputStream);
            throw th;
        }
    }

    public static boolean writeStreamToFile(File file, InputStream inputStream) {
        FileOutputStream fileOutputStream;
        IOException e;
        Throwable th;
        byte[] acquireBytes = aV.acquireBytes(1024);
        try {
            fileOutputStream = new FileOutputStream(file);
            while (true) {
                try {
                    int read = inputStream.read(acquireBytes);
                    if (read != -1) {
                        fileOutputStream.write(acquireBytes, 0, read);
                    } else {
                        close(fileOutputStream);
                        close(inputStream);
                        aV.releaseBytes(acquireBytes);
                        return true;
                    }
                } catch (IOException e2) {
                    e = e2;
                    try {
                        X.e(e, "Failed to write stream %s", file);
                        close(fileOutputStream);
                        close(inputStream);
                        aV.releaseBytes(acquireBytes);
                        return false;
                    } catch (Throwable th2) {
                        th = th2;
                        close(fileOutputStream);
                        close(inputStream);
                        aV.releaseBytes(acquireBytes);
                        throw th;
                    }
                }
            }
        } catch (IOException e3) {
            IOException iOException = e3;
            fileOutputStream = null;
            e = iOException;
            X.e(e, "Failed to write stream %s", file);
            close(fileOutputStream);
            close(inputStream);
            aV.releaseBytes(acquireBytes);
            return false;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            fileOutputStream = null;
            th = th4;
            close(fileOutputStream);
            close(inputStream);
            aV.releaseBytes(acquireBytes);
            throw th;
        }
    }
}
