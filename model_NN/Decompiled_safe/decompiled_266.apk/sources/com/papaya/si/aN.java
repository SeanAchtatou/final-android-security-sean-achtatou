package com.papaya.si;

import com.papaya.si.aP;

public interface aN<PT extends aP> {
    boolean onDataStateChanged(PT pt);
}
