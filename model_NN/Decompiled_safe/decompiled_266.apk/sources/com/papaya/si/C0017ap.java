package com.papaya.si;

import java.lang.ref.WeakReference;

/* renamed from: com.papaya.si.ap  reason: case insensitive filesystem */
public abstract class C0017ap {
    public long er;
    private WeakReference<a> es;
    protected int state = 0;

    /* renamed from: com.papaya.si.ap$a */
    public interface a {
        void onConnectionStateUpdated(int i, int i2);
    }

    public C0017ap(a aVar) {
        this.es = new WeakReference<>(aVar);
    }

    public static C0017ap getConnecter(int i, a aVar) {
        return new C0018aq(aVar);
    }

    public abstract void close();

    public abstract void connect(String str, int i);

    public abstract byte[] recv();

    public abstract boolean send(Object obj);

    public void setState(int i) {
        int i2 = this.state;
        this.state = i;
        a aVar = this.es == null ? null : this.es.get();
        if (aVar != null) {
            aVar.onConnectionStateUpdated(i2, i);
        }
    }

    public abstract int state();
}
