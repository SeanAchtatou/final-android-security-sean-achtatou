package com.papaya.si;

import com.papaya.si.C0017ap;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Vector;

/* renamed from: com.papaya.si.aq  reason: case insensitive filesystem */
public final class C0018aq extends C0017ap implements Runnable {
    private String bt;
    private Socket et;
    private InputStream eu;
    private OutputStream ev;
    private Vector<byte[]> ew = new Vector<>();
    private Vector<byte[]> ex = new Vector<>();
    private int port;

    public C0018aq(C0017ap.a aVar) {
        super(aVar);
    }

    private synchronized void checksend() {
        if (this.ev != null) {
            while (this.ex.size() > 0) {
                byte[] remove = this.ex.remove(0);
                try {
                    this.ev.write(remove);
                } catch (Exception e) {
                    this.ex.add(0, remove);
                    X.e(e, "error in send", new Object[0]);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void _close() {
        try {
            this.eu.close();
            this.eu = null;
        } catch (Exception e) {
        }
        try {
            this.ev.close();
            this.ev = null;
        } catch (Exception e2) {
        }
        try {
            this.et.close();
            this.et = null;
        } catch (Exception e3) {
        }
    }

    /* access modifiers changed from: protected */
    public final boolean _connect() {
        try {
            this.et = new Socket(this.bt, this.port);
            this.et.setSoTimeout(100);
            this.eu = this.et.getInputStream();
            this.ev = this.et.getOutputStream();
            return true;
        } catch (SecurityException e) {
            return false;
        } catch (Exception e2) {
            return false;
        }
    }

    public final void close() {
        if (this.state != 0) {
            try {
                setState(3);
                _close();
                this.ew.removeAllElements();
                this.ex.removeAllElements();
                this.bt = null;
            } catch (Exception e) {
            }
        }
    }

    public final void connect(String str, int i) {
        close();
        this.bt = str;
        this.port = i;
        setState(1);
        if (_connect()) {
            setState(2);
            new Thread(this).start();
            return;
        }
        setState(0);
    }

    public final byte[] recv() {
        if (this.state != 2) {
            return null;
        }
        try {
            if (this.ew.size() == 0) {
                return null;
            }
            return this.ew.remove(0);
        } catch (Exception e) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        com.papaya.si.X.i("read failed ret=%d, closing the channel", java.lang.Integer.valueOf(r4));
        setState(3);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r9 = this;
            r8 = 2
            r7 = 4
            r6 = 0
            long r0 = java.lang.System.currentTimeMillis()
            r9.er = r0
            byte[] r0 = new byte[r7]
            r1 = 0
            r2 = r1
            r3 = r6
            r1 = r6
        L_0x000f:
            int r4 = r9.state     // Catch:{ Exception -> 0x005a }
            if (r4 != r8) goto L_0x001f
            java.net.Socket r4 = r9.et     // Catch:{ Exception -> 0x004c }
            boolean r4 = r4.isConnected()     // Catch:{ Exception -> 0x004c }
            if (r4 != 0) goto L_0x002a
            r0 = 3
            r9.setState(r0)     // Catch:{ Exception -> 0x004c }
        L_0x001f:
            java.net.Socket r0 = r9.et
            if (r0 == 0) goto L_0x0026
            r9._close()
        L_0x0026:
            r9.setState(r6)
            return
        L_0x002a:
            int r3 = r3 + 1
            if (r2 != 0) goto L_0x0063
            java.io.InputStream r4 = r9.eu     // Catch:{ SocketTimeoutException -> 0x006c }
            int r5 = r7 - r1
            int r4 = r4.read(r0, r1, r5)     // Catch:{ SocketTimeoutException -> 0x006c }
        L_0x0036:
            if (r4 >= 0) goto L_0x006f
            java.lang.String r0 = "read failed ret=%d, closing the channel"
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x004c }
            r2 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x004c }
            r1[r2] = r3     // Catch:{ Exception -> 0x004c }
            com.papaya.si.X.i(r0, r1)     // Catch:{ Exception -> 0x004c }
            r0 = 3
            r9.setState(r0)     // Catch:{ Exception -> 0x004c }
            goto L_0x001f
        L_0x004c:
            r0 = move-exception
            java.lang.String r1 = "other error in run of vSocketConnector"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x005a }
            com.papaya.si.X.e(r0, r1, r2)     // Catch:{ Exception -> 0x005a }
            r0 = 3
            r9.setState(r0)     // Catch:{ Exception -> 0x005a }
            goto L_0x001f
        L_0x005a:
            r0 = move-exception
            java.lang.String r1 = "error in run of vSocketConnector"
            java.lang.Object[] r2 = new java.lang.Object[r6]
            com.papaya.si.X.e(r0, r1, r2)
            goto L_0x001f
        L_0x0063:
            java.io.InputStream r4 = r9.eu     // Catch:{ SocketTimeoutException -> 0x006c }
            int r5 = r2.length     // Catch:{ SocketTimeoutException -> 0x006c }
            int r5 = r5 - r1
            int r4 = r4.read(r2, r1, r5)     // Catch:{ SocketTimeoutException -> 0x006c }
            goto L_0x0036
        L_0x006c:
            r4 = move-exception
            r4 = r6
            goto L_0x0036
        L_0x006f:
            if (r4 <= 0) goto L_0x00bc
            int r1 = r1 + r4
            if (r2 != 0) goto L_0x00da
            if (r1 < r7) goto L_0x00e7
            r1 = 3
            byte r1 = r0[r1]     // Catch:{ Exception -> 0x004c }
            r1 = r1 & 255(0xff, float:3.57E-43)
            r3 = 2
            byte r3 = r0[r3]     // Catch:{ Exception -> 0x004c }
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r3 = r3 << 8
            r1 = r1 | r3
            r3 = 1
            byte r3 = r0[r3]     // Catch:{ Exception -> 0x004c }
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r3 = r3 << 16
            r1 = r1 | r3
            r3 = 0
            byte r3 = r0[r3]     // Catch:{ Exception -> 0x004c }
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r3 = r3 << 24
            r1 = r1 | r3
            r3 = 1048576(0x100000, float:1.469368E-39)
            if (r1 > r3) goto L_0x0099
            if (r1 >= 0) goto L_0x00ae
        L_0x0099:
            java.lang.String r0 = "read package length=%d, closing the channel"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x004c }
            r3 = 0
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x004c }
            r2[r3] = r1     // Catch:{ Exception -> 0x004c }
            com.papaya.si.X.i(r0, r2)     // Catch:{ Exception -> 0x004c }
            r0 = 3
            r9.setState(r0)     // Catch:{ Exception -> 0x004c }
            goto L_0x001f
        L_0x00ae:
            if (r1 != 0) goto L_0x00d7
            java.util.Vector<byte[]> r1 = r9.ew     // Catch:{ Exception -> 0x004c }
            r3 = 0
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x004c }
            r1.add(r3)     // Catch:{ Exception -> 0x004c }
            r1 = r2
        L_0x00b9:
            r2 = r1
            r3 = r6
            r1 = r6
        L_0x00bc:
            r4 = 600(0x258, float:8.41E-43)
            if (r3 < r4) goto L_0x00d2
            java.util.Vector r3 = new java.util.Vector     // Catch:{ Exception -> 0x004c }
            r3.<init>()     // Catch:{ Exception -> 0x004c }
            r4 = 9999(0x270f, float:1.4012E-41)
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x004c }
            r3.add(r4)     // Catch:{ Exception -> 0x004c }
            r9.send(r3)     // Catch:{ Exception -> 0x004c }
            r3 = r6
        L_0x00d2:
            r9.checksend()     // Catch:{ Exception -> 0x004c }
            goto L_0x000f
        L_0x00d7:
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x004c }
            goto L_0x00b9
        L_0x00da:
            int r3 = r2.length     // Catch:{ Exception -> 0x004c }
            if (r1 < r3) goto L_0x00e7
            java.util.Vector<byte[]> r1 = r9.ew     // Catch:{ Exception -> 0x004c }
            r1.add(r2)     // Catch:{ Exception -> 0x004c }
            r1 = 0
            r2 = r1
            r3 = r6
            r1 = r6
            goto L_0x00bc
        L_0x00e7:
            r3 = r6
            goto L_0x00bc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.papaya.si.C0018aq.run():void");
    }

    public final boolean send(Object obj) {
        if (obj == null) {
            return this.ex.size() > 0;
        }
        byte[] dumps = C0035bg.dumps(obj);
        this.ex.addElement(new byte[]{(byte) (dumps.length >> 24), (byte) (dumps.length >> 16), (byte) (dumps.length >> 8), (byte) dumps.length});
        this.ex.addElement(dumps);
        checksend();
        return true;
    }

    public final int state() {
        return this.state;
    }
}
