package com.papaya.si;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import com.papaya.analytics.PPYReferralReceiver;
import com.papaya.service.AppManager;
import com.papaya.si.C0060z;
import com.papaya.view.TakePhotoBridge;
import java.util.List;

/* renamed from: com.papaya.si.c  reason: case insensitive filesystem */
public final class C0037c {
    public static C0019ar A = new C0019ar();
    public static final aH B = new aH();
    private static final ColorDrawable C = new ColorDrawable(-65536);
    private static final Bitmap D = Bitmap.createBitmap(1, 1, Bitmap.Config.RGB_565);
    private static final aI E = new aI();
    private static C F;
    private static boolean u = false;
    private static Application v;
    /* access modifiers changed from: private */
    public static A w;
    /* access modifiers changed from: private */
    public static bq z;

    public static Application getApplicationContext() {
        return v;
    }

    private static Bitmap getBitmap(int i) {
        try {
            if (v != null) {
                return BitmapFactory.decodeResource(v.getResources(), i);
            }
        } catch (Exception e) {
            X.e(e, "Failde to getBitmap: " + i, new Object[0]);
        }
        return D;
    }

    public static BitmapDrawable getBitmapDrawable(String str) {
        Bitmap bitmap = E.get(str);
        if (bitmap == null) {
            bitmap = getBitmap(C0060z.drawableID(str));
            E.put(str, bitmap);
        }
        if (bitmap == D) {
            X.e("Can't decode bitmap from resource: " + str, new Object[0]);
        }
        return new BitmapDrawable(bitmap);
    }

    public static Drawable getDrawable(int i) {
        try {
            if (v != null) {
                return v.getResources().getDrawable(i);
            }
        } catch (Exception e) {
            X.e(e, "Failed to get drawable " + i, new Object[0]);
        }
        return C;
    }

    public static Drawable getDrawable(String str) {
        Drawable drawable = getDrawable(C0060z.drawableID(str));
        if (drawable == C) {
            X.e("Failed to get drawable: " + str, new Object[0]);
        }
        return drawable;
    }

    public static A getSession() {
        return w;
    }

    public static String getString(int i) {
        try {
            return v.getResources().getString(i);
        } catch (Exception e) {
            X.e(e, "Failed to find string " + i, new Object[0]);
            return "";
        }
    }

    public static String getString(String str) {
        String string = getString(C0060z.stringID(str));
        if (aV.isEmpty(string)) {
            X.e("Failed to get string: " + str, new Object[0]);
        }
        return string;
    }

    public static C getTabBadgeValues() {
        return F;
    }

    public static bq getWebGameBridge() {
        return z;
    }

    public static void initialize(Context context) {
        Context applicationContext = context.getApplicationContext();
        if (!u) {
            long currentTimeMillis = System.currentTimeMillis();
            v = (Application) applicationContext;
            C0056v.setup(applicationContext);
            System.currentTimeMillis();
            initializeUtils();
            initializePOTP();
            initializeCaches();
            initializeServices();
            initialize3rd();
            postInitialize();
            X.i("initialization time: %dms", Long.valueOf(System.currentTimeMillis() - currentTimeMillis));
            X.i("version: %d, source %s, lang %s", Integer.valueOf(C0056v.bc), C0056v.be, C0056v.bd);
            u = true;
        }
    }

    private static void initialize3rd() {
        C0042h.initialize(v);
        C0043i.initialize(v);
        G.initialize(v);
        C0039e.initialize(v);
        C0040f.initialize(v);
        C0060z.a.initialize(v);
        C0041g.register();
    }

    private static void initializeCaches() {
        C0001a.initialize(v);
        C0016ao.getInstance().initialize(v);
        C0014am.getInstance().initialize(v);
    }

    private static void initializePOTP() {
        try {
            A = new C0019ar();
            A a = new A();
            w = a;
            a.initialize();
            z = new bq();
            F = new C();
        } catch (Exception e) {
            X.e("Failed to start POTP thread: " + e, new Object[0]);
            throw new RuntimeException(e);
        }
    }

    private static void initializeServices() {
        C0038d.initialize(v);
        PPYReferralReceiver.sendReferrerReport(v);
        AppManager.initialize(v);
    }

    private static void initializeUtils() {
        C0029ba.initialize(v);
        C0030bb.initialize();
    }

    public static boolean isInitialized() {
        return u;
    }

    private static void postInitialize() {
        C0016ao.getInstance().sendUpdateRequest();
        C0029ba.checkFreeSpace(v);
    }

    public static void quit() {
        u = false;
        C0028b.hideAllOverlayDialogs();
        C0030bb.post(new Runnable() {
            public final void run() {
                C0028b.finishAllActivities();
                try {
                    C0037c.w.quit();
                    A unused = C0037c.w = new A();
                } catch (Exception e) {
                    X.e(e, "Failed to saverms", new Object[0]);
                }
                C0037c.A.close();
                C0001a.destroy();
                bH.getInstance().clear();
                C0014am.getInstance().clear();
                C0016ao.getInstance().clear();
                C0034bf.clear();
                aV.clear();
                bp.clear();
                TakePhotoBridge.clear();
                C0037c.z.clear();
                C0028b.clear();
                C0040f.destroy();
                C0030bb.destroy();
                C0030bb.forceClearBitmap();
            }
        });
    }

    public static void send(int i, Object... objArr) {
        if (A != null) {
            A.send(i, objArr);
        }
    }

    public static void send(List list) {
        if (A != null) {
            A.send(list);
        }
    }
}
