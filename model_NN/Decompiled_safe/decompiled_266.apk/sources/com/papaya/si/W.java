package com.papaya.si;

import android.view.View;
import android.widget.TextView;
import com.adknowledge.superrewards.model.SROffer;
import com.papaya.view.AppIconView;
import com.papaya.view.CardImageView;

public final class W {
    public AppIconView dp;
    public CardImageView dq;
    public TextView dr;

    public W(View view) {
        this.dp = (AppIconView) C0030bb.find(view, "app_icon");
        this.dp.setFocusable(false);
        this.dq = (CardImageView) C0030bb.find(view, SROffer.IMAGE);
        this.dr = (TextView) C0030bb.find(view, "message");
    }
}
