package com.papaya.si;

import java.net.URL;
import java.util.Vector;

public final class bq implements C0054t {
    private bG lP;

    private void hideLoading() {
    }

    public final void callJS(String str) {
        if (this.lP != null) {
            this.lP.callJS(str);
        }
    }

    public final void clear() {
        this.lP = null;
    }

    public final bG getController() {
        return this.lP;
    }

    public final boolean handlePapayaUrl(bG bGVar, bk bkVar, String str, String str2, String str3, URL url) {
        return false;
    }

    public final void handleServerResponse(final Vector<Object> vector) {
        C0030bb.runInHandlerThread(new Runnable() {
            public final void run() {
                bq.this.handlerServerResponseInUIThread(vector);
            }
        });
    }

    public final void handlerServerResponseInUIThread(Vector<Object> vector) {
    }

    public final void onGameClosed() {
    }

    public final void openUrl(String str) {
    }

    public final void setController(bG bGVar) {
        this.lP = bGVar;
    }

    public final void setWebView(bk bkVar) {
    }

    public final void startGame(int i, int i2, String[] strArr, String[] strArr2, byte[] bArr, int i3, int i4, int i5) {
    }

    public final void startScript(byte[] bArr) {
    }
}
