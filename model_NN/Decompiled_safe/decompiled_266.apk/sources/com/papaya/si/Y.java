package com.papaya.si;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.papaya.chat.ChatActivity;
import com.papaya.view.Action;
import com.papaya.view.CustomDialog;
import com.papaya.view.PausableAdapter;
import java.util.ArrayList;
import java.util.List;

public final class Y extends BaseExpandableListAdapter implements ExpandableListView.OnChildClickListener, aN, PausableAdapter {
    private boolean bq;
    private LayoutInflater cQ;
    /* access modifiers changed from: private */
    public Activity dA;
    /* access modifiers changed from: private */
    public List<E> dB = new ArrayList();
    private View.OnClickListener dC = new Z(this);

    public Y(Activity activity) {
        this.dA = activity;
        this.cQ = LayoutInflater.from(activity);
        refreshVisibleCardLists();
        C0037c.getSession().registerMonitor(this);
    }

    private void refreshVisibleCardLists() {
        List<E> cardLists = C0037c.getSession().getCardLists();
        this.dB.clear();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < cardLists.size()) {
                E e = cardLists.get(i2);
                if (e.sectionHeaderVisible()) {
                    this.dB.add(e);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void startChat(int i, int i2) {
        A session = C0037c.getSession();
        E e = this.dB.get(i);
        if (e != session.getContacts()) {
            D d = e.get(i2);
            session.getChattings().add(d);
            Intent intent = new Intent(this.dA, ChatActivity.class);
            intent.putExtra("active", session.getChattings().indexOf(d));
            session.getChattings().fireDataStateChanged();
            session.fireDataStateChanged();
            C0028b.startActivity(this.dA, intent);
        }
    }

    public final Object getChild(int i, int i2) {
        return this.dB.get(i).get(i2);
    }

    public final long getChildId(int i, int i2) {
        return (long) i2;
    }

    public final View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        View view2;
        if (view == null) {
            View inflate = this.cQ.inflate(C0060z.layoutID("friends_child"), (ViewGroup) null);
            inflate.setTag(new C0002aa(inflate));
            view2 = inflate;
        } else {
            view2 = view;
        }
        C0002aa aaVar = (C0002aa) view2.getTag();
        D d = this.dB.get(i).get(i2);
        aaVar.dq.refreshWithCard(d);
        aaVar.cn.setText(d.getTitle());
        aaVar.dH.setText(d.getSubtitle());
        aaVar.dI.setText(d.getTimeLabel());
        if (d.co == null || d.co.getUnread() <= 0) {
            aaVar.dJ.setBadgeValue(null);
        } else {
            aaVar.dJ.setBadgeValue(String.valueOf(d.co.getUnread()));
        }
        return view2;
    }

    public final int getChildrenCount(int i) {
        return this.dB.get(i).size();
    }

    public final Object getGroup(int i) {
        return this.dB.get(i);
    }

    public final int getGroupCount() {
        return this.dB.size();
    }

    public final long getGroupId(int i) {
        return (long) i;
    }

    public final View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        View view2;
        if (view == null) {
            view2 = this.cQ.inflate(C0060z.layoutID("friends_group"), (ViewGroup) null);
            view2.setTag(new C0003ab(view2));
        } else {
            view2 = view;
        }
        C0003ab abVar = (C0003ab) view2.getTag();
        E e = this.dB.get(i);
        abVar.dK.setImageDrawable(e.getIcon());
        abVar.dL.setText(e.getLabel());
        abVar.dM.setImageState(z ? new int[]{16842920} : new int[0], false);
        if (z) {
            abVar.dN.setVisibility(0);
            abVar.dN.removeAllViews();
            List<Action> actions = e.getActions();
            if (actions != null) {
                for (int i2 = 0; i2 < actions.size(); i2++) {
                    Action action = actions.get(i2);
                    TextView textView = new TextView(this.dA);
                    textView.setTag(action);
                    textView.setText(action.label);
                    textView.setOnClickListener(this.dC);
                    textView.setEnabled(action.enabled);
                    textView.setFocusable(false);
                    textView.setBackgroundDrawable(C0037c.getDrawable("gray_button_bgs"));
                    textView.setTextColor(-16777216);
                    textView.setGravity(16);
                    textView.setPadding(C0030bb.rp(3), C0030bb.rp(0), C0030bb.rp(3), C0030bb.rp(0));
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                    layoutParams.rightMargin = C0030bb.rp(4);
                    abVar.dN.addView(textView, layoutParams);
                }
            }
        } else {
            abVar.dN.setVisibility(8);
        }
        return view2;
    }

    public final boolean hasStableIds() {
        return false;
    }

    public final boolean isChildSelectable(int i, int i2) {
        return true;
    }

    public final boolean isPaused() {
        return this.bq;
    }

    public final void notifyDataSetChanged() {
        if (!this.bq) {
            refreshVisibleCardLists();
            super.notifyDataSetChanged();
        }
    }

    public final boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        D d = this.dB.get(i).get(i2);
        final A session = C0037c.getSession();
        if (d instanceof Q) {
            final Q q = (Q) d;
            if (q.dg != null && q.state == 0 && !session.getAcceptedChatRoomRules().contains(Integer.valueOf(q.df))) {
                new CustomDialog.Builder(this.dA).setTitle(C0037c.getString("accept_roomrule")).setMessage(q.dg).setPositiveButton(C0037c.getString("accept"), new DialogInterface.OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        int indexOf = Y.this.dB.indexOf(session.getChatRooms());
                        int indexOf2 = session.getChatRooms().indexOf(q);
                        session.getAcceptedChatRoomRules().add(Integer.valueOf(q.df));
                        Y.this.startChat(indexOf, indexOf2);
                    }
                }).setNegativeButton(C0037c.getString("btn_cancel"), (DialogInterface.OnClickListener) null).show();
                return true;
            }
        }
        startChat(i, i2);
        return true;
    }

    public final boolean onDataStateChanged(aP aPVar) {
        notifyDataSetChanged();
        return false;
    }

    public final void setPaused(boolean z) {
        this.bq = z;
        if (!z) {
            notifyDataSetChanged();
        }
    }
}
