package com.papaya.si;

import android.content.Context;
import android.content.pm.PackageInfo;
import com.papaya.social.PPYSocial;
import java.net.URL;

/* renamed from: com.papaya.si.v  reason: case insensitive filesystem */
public final class C0056v {
    public static final String DEFAULT_HOST = "corona.papayamobile.com";
    public static int bc = 144;
    public static String bd = PPYSocial.LANG_EN;
    public static String be = "corona";
    public static final String bf = "http://corona.papayamobile.com:8080/";
    public static int bg = 0;
    public static String bh = null;
    public static final String bi = (bf + "social/");
    public static CharSequence bj;
    public static String bk = "Apps";
    public static String bl;
    public static String bm = "static_moreapps";
    public static String bn = "android";
    public static URL bo;

    static {
        try {
            bo = new URL(bi);
        } catch (Exception e) {
            X.e("invalid url string for default_web_url: %s", bi);
        }
    }

    private C0056v() {
    }

    public static void setup(Context context) {
        bl = context.getPackageName();
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(bl, 0);
            bg = packageInfo.versionCode;
            bh = packageInfo.versionName;
            bj = context.getPackageManager().getApplicationLabel(context.getPackageManager().getApplicationInfo(bl, 128));
        } catch (Exception e) {
            X.e(e, "Failed to getApplicationInfo", new Object[0]);
        }
        C0060z.setup();
    }
}
