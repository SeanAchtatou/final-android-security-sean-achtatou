package com.papaya.si;

import com.papaya.social.PPYSocialQuery;
import com.papaya.social.PPYUser;
import java.util.ArrayList;
import java.util.HashMap;

/* renamed from: com.papaya.si.ax  reason: case insensitive filesystem */
public final class C0025ax {
    private static C0025ax fC = new C0025ax();
    private int dd;
    private String dn;
    private String fD;
    private String fE;
    private String fF;
    private long fG = 0;
    private int fH = 0;
    private HashMap<String, Integer> fI = new HashMap<>(4);
    public String fJ;
    private boolean fK = false;
    private String fl;

    private C0025ax() {
    }

    public static C0025ax getInstance() {
        return fC;
    }

    public final void clear() {
        this.fD = null;
        this.fG = 0;
        this.fH = 0;
        this.dn = null;
        this.fI.clear();
        this.dd = 0;
        this.fJ = null;
    }

    public final String getApiKey() {
        return this.fl;
    }

    public final int getAppID() {
        return this.dd;
    }

    public final long getExpirationDate() {
        return this.fG;
    }

    public final String getNickname() {
        return this.dn;
    }

    public final int getScore() {
        return getScore(this.fJ);
    }

    public final int getScore(String str) {
        Integer num = this.fI.get(str);
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    public final HashMap<String, Integer> getScores() {
        return new HashMap<>(this.fI);
    }

    public final String getSessionKey() {
        return this.fD;
    }

    public final String getSessionReceipt() {
        return this.fF;
    }

    public final String getSessionSecret() {
        return this.fE;
    }

    public final int getUID() {
        return this.fH;
    }

    public final boolean isAppFriend(int i) {
        return C0037c.getSession().getAppFriends().isFriend(i);
    }

    public final boolean isConnected() {
        return this.fD != null;
    }

    public final boolean isDev() {
        return this.fK;
    }

    public final boolean isFriend(int i) {
        return C0037c.getSession().getFriends().isFriend(i);
    }

    public final boolean isNonappFriend(int i) {
        return C0037c.getSession().getNonappFriends().isFriend(i);
    }

    public final ArrayList<PPYUser> listFriends() {
        return C0037c.getSession().getFriends().listUsers();
    }

    public final void save() {
    }

    public final void setApiKey(String str) {
        this.fl = str;
    }

    public final void setAppID(int i) {
        this.dd = i;
    }

    public final void setDev(boolean z) {
        this.fK = z;
    }

    public final void setExpirationDate(long j) {
        this.fG = j;
    }

    public final void setNickname(String str) {
        this.dn = str;
    }

    public final void setScore(int i) {
        setScore(this.fJ, i);
    }

    public final void setScore(String str, int i) {
        this.fI.put(str == null ? this.fJ : str, Integer.valueOf(i));
    }

    public final void setSessionKey(String str) {
        this.fD = str;
    }

    public final void setSessionReceipt(String str) {
        this.fF = str;
    }

    public final void setSessionSecret(String str) {
        this.fE = str;
    }

    public final void setUID(int i) {
        this.fH = i;
        A.bL.setUserID(i);
    }

    public final String toString() {
        return "PPYSession: [UID=" + this.fH + ", nickname=" + this.dn + ", scores=" + this.fI + ']';
    }

    public final PPYSocialQuery updateNickname(String str, PPYSocialQuery.QueryDelegate queryDelegate) {
        if (aV.isEmpty(str)) {
            return null;
        }
        PPYSocialQuery pPYSocialQuery = new PPYSocialQuery("w_update_nickname", queryDelegate);
        pPYSocialQuery.put("name", str);
        C0023av.getInstance().submitQuery(pPYSocialQuery);
        return pPYSocialQuery;
    }
}
