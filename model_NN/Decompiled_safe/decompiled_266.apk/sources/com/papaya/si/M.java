package com.papaya.si;

import com.papaya.view.Action;
import java.util.ArrayList;

public final class M extends E<L> {
    public M() {
        setName(C0037c.getString("friend_chatgroup"));
        setReserveGroupHeader(true);
        this.cs = new ArrayList();
        this.cs.add(new Action(0, null, C0037c.getString("create")));
        this.cs.add(new Action(1, null, C0037c.getString("search")));
    }

    public final L findByGroupID(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.cq.size()) {
                return null;
            }
            L l = (L) this.cq.get(i3);
            if (l.cS == i) {
                return l;
            }
            i2 = i3 + 1;
        }
    }
}
