package com.papaya.si;

import android.app.Activity;
import java.lang.ref.WeakReference;

public class bw<T extends Activity> {
    private WeakReference<T> mN;

    public T getOwnerActivity() {
        if (this.mN == null) {
            return null;
        }
        return (Activity) this.mN.get();
    }

    public void setOwnerActivity(T t) {
        if (t == null) {
            this.mN = null;
        } else {
            this.mN = new WeakReference<>(t);
        }
    }
}
