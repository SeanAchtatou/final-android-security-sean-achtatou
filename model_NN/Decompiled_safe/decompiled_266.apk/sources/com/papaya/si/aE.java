package com.papaya.si;

import java.io.File;
import java.util.HashMap;

public final class aE extends bv {
    public aE() {
        this.lX = false;
        this.mI = true;
        HashMap hashMap = new HashMap();
        hashMap.put("api_version", 171);
        hashMap.put("model", 6);
        hashMap.put("device", C0029ba.ANDROID_ID);
        hashMap.put("app_id", C0056v.bl);
        hashMap.put("api_key", C0023av.getInstance().getApiKey());
        hashMap.put("db_version", Integer.valueOf(C0016ao.getInstance().getVersion()));
        hashMap.put("lang", C0056v.bd);
        File file = new File(C0037c.getApplicationContext().getFilesDir(), "__ppy_secret");
        if (file.exists() && file.length() > 0) {
            String utf8String = aV.utf8String(aU.dataFromFile(file), null);
            if (aV.isNotEmpty(utf8String)) {
                hashMap.put("__credential", utf8String);
                hashMap.put("__sig", aU.md5(utf8String + C0023av.getInstance().getApiKey()));
            }
        }
        this.url = C0034bf.createURL(C0034bf.compositeUrl("json_login", hashMap));
    }

    public static boolean hasCredentialFile() {
        File file = new File(C0037c.getApplicationContext().getFilesDir(), "__ppy_secret");
        return file.exists() && file.length() > 0;
    }
}
