package com.papaya.si;

import com.papaya.social.BillingChannel;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

public final class bH implements aM {
    private static bH ol = new bH();
    private static final ArrayList<bG> om = new ArrayList<>(8);
    private ArrayList<bk> on = new ArrayList<>(8);
    private ArrayList<bk> oo = new ArrayList<>(8);

    private bH() {
    }

    public static int freeWebViews(int i, bG bGVar) {
        int i2;
        if (i == 0) {
            return 0;
        }
        int i3 = i < 0 ? BillingChannel.ALL : i;
        synchronized (bG.class) {
            int size = om.size();
            if (size > 0) {
                ArrayList arrayList = new ArrayList();
                for (int i4 = 0; i4 < size; i4++) {
                    arrayList.add(Integer.valueOf(i4));
                }
                Collections.shuffle(arrayList);
                int i5 = 0;
                for (int i6 = 0; i6 < size && i5 < i3; i6++) {
                    bG bGVar2 = om.get(((Integer) arrayList.get(i6)).intValue());
                    if (bGVar2 != bGVar) {
                        i5 += bGVar2.forceFreeWebViews(i3 - i5, false);
                    }
                }
                if (i5 < i3 && bGVar != null) {
                    bGVar.forceFreeWebViews(i3 - i5, true);
                }
                i2 = i5;
            } else {
                i2 = 0;
            }
        }
        return i2;
    }

    public static bH getInstance() {
        return ol;
    }

    public final void clear() {
        try {
            freeWebViews();
            this.oo.clear();
        } catch (Exception e) {
            X.w(e, "Failed to clear webviews", new Object[0]);
        }
    }

    public final int findReuseWebViewIndex(bG bGVar, URL url) {
        if (url != null) {
            String url2 = url.toString();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.on.size()) {
                    break;
                }
                bk bkVar = this.on.get(i2);
                if (bkVar.getLastController() == null || bkVar.getLastController() == bGVar) {
                    if (bkVar.isReusable() && !bkVar.isLoadFromString() && bkVar.getPapayaURL() != null && url2.equals(bkVar.getPapayaURL().toString())) {
                        return i2;
                    }
                } else if (bkVar.isGlobalReusable() && !bkVar.isLoadFromString() && bkVar.getPapayaURL() != null && url2.equals(bkVar.getPapayaURL().toString())) {
                    return i2;
                }
                i = i2 + 1;
            }
        }
        return -1;
    }

    public final void freeWebView(bk bkVar) {
        if (bkVar != null) {
            bkVar.setVisibility(4);
            bkVar.setController(null);
            bkVar.setDelegate(null);
            this.oo.remove(bkVar);
            if (bkVar.getParent() != null) {
                this.on.add(bkVar);
                if (!bkVar.isReusable()) {
                    bkVar.getPapayaScript().clearSessionState();
                    return;
                }
                return;
            }
            bkVar.close();
        }
    }

    public final void freeWebViews() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.on.size()) {
                bk bkVar = this.on.get(i2);
                C0030bb.removeFromSuperView(bkVar);
                bkVar.noWarnCallJS("webdestroyed", "webdestroyed();");
                bkVar.close();
                i = i2 + 1;
            } else {
                this.on.clear();
                return;
            }
        }
    }

    public final bk getWebView(bG bGVar) {
        return getWebView(bGVar, null, null);
    }

    public final bk getWebView(bG bGVar, URL url, aT<Boolean> aTVar) {
        C0030bb.assertMainThread();
        if (aTVar != null) {
            aTVar.gG = Boolean.FALSE;
        }
        if (this.oo.size() > 8) {
            freeWebViews(1, bGVar);
        }
        bk bkVar = null;
        int findReuseWebViewIndex = findReuseWebViewIndex(bGVar, url);
        if (findReuseWebViewIndex >= 0) {
            bkVar = this.on.remove(findReuseWebViewIndex);
            if (aTVar != null) {
                aTVar.gG = Boolean.TRUE;
            }
        } else if (!this.on.isEmpty() && this.on.size() + this.oo.size() >= 8) {
            bkVar = this.on.remove(0);
        }
        if (bkVar == null) {
            bkVar = new bk(C0037c.getApplicationContext());
        }
        this.oo.add(bkVar);
        bGVar.configWebView(bkVar);
        return bkVar;
    }

    public final synchronized void onControllerClosed(bG bGVar) {
        om.remove(bGVar);
    }

    public final synchronized void onControllerCreated(bG bGVar) {
        om.add(bGVar);
    }

    public final synchronized void startAllControllers() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < om.size()) {
                om.get(i2).openInitUrlIfPossible();
                i = i2 + 1;
            }
        }
    }
}
