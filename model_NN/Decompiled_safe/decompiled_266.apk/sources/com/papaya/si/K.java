package com.papaya.si;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import com.papaya.view.AppIconView;
import com.papaya.view.BasePausableAdapter;
import com.papaya.view.CustomDialog;
import com.papaya.view.LazyImageView;

public final class K extends BasePausableAdapter implements View.OnClickListener, AdapterView.OnItemClickListener {
    private Context cP;
    private LayoutInflater cQ;
    private D cR;

    public K(Context context) {
        this.cP = context;
        this.cQ = LayoutInflater.from(context);
    }

    public final int getCount() {
        if (this.cR == null || this.cR.co == null) {
            return 0;
        }
        return this.cR.co.size();
    }

    public final Object getItem(int i) {
        if (this.cR == null || this.cR.co == null) {
            return null;
        }
        return (O) this.cR.co.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final int getItemViewType(int i) {
        if (this.cR == null) {
            X.e("invalid card states in getItemViewType: card is null", new Object[0]);
        } else if (i > 0 && (this.cR.co == null || this.cR.co.size() <= i)) {
            X.e("invalid card message states: %d, %s", Integer.valueOf(i), this.cR.co);
        }
        if (this.cR == null || this.cR.co == null) {
            return 0;
        }
        return ((O) this.cR.co.get(i)).type;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00dd A[Catch:{ Exception -> 0x0103 }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00f3  */
    /* JADX WARNING: Removed duplicated region for block: B:59:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:19:0x0060=Splitter:B:19:0x0060, B:41:0x00ca=Splitter:B:41:0x00ca, B:30:0x008f=Splitter:B:30:0x008f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.view.View getView(int r7, android.view.View r8, android.view.ViewGroup r9) {
        /*
            r6 = this;
            r5 = 0
            com.papaya.si.D r0 = r6.cR
            if (r0 == 0) goto L_0x010d
            com.papaya.si.D r0 = r6.cR
            com.papaya.si.P r0 = r0.co
            if (r0 == 0) goto L_0x010d
            com.papaya.si.D r0 = r6.cR     // Catch:{ Exception -> 0x00f5 }
            com.papaya.si.P r0 = r0.co     // Catch:{ Exception -> 0x00f5 }
            java.lang.Object r0 = r0.get(r7)     // Catch:{ Exception -> 0x00f5 }
            com.papaya.si.O r0 = (com.papaya.si.O) r0     // Catch:{ Exception -> 0x00f5 }
            int r1 = r0.type     // Catch:{ Exception -> 0x00f5 }
            switch(r1) {
                case 1: goto L_0x0040;
                case 2: goto L_0x006f;
                case 3: goto L_0x00a5;
                default: goto L_0x001a;
            }     // Catch:{ Exception -> 0x00f5 }
        L_0x001a:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f5 }
            r1.<init>()     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r2 = "unknown message type "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00f5 }
            int r0 = r0.type     // Catch:{ Exception -> 0x00f5 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00f5 }
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x00f5 }
            com.papaya.si.X.e(r0, r1)     // Catch:{ Exception -> 0x00f5 }
            r0 = r8
        L_0x0036:
            if (r0 != 0) goto L_0x003f
            android.view.View r0 = new android.view.View
            android.content.Context r1 = r6.cP
            r0.<init>(r1)
        L_0x003f:
            return r0
        L_0x0040:
            if (r8 == 0) goto L_0x004a
            java.lang.Object r1 = r8.getTag()     // Catch:{ Exception -> 0x00f5 }
            boolean r1 = r1 instanceof com.papaya.si.C0010ai     // Catch:{ Exception -> 0x00f5 }
            if (r1 != 0) goto L_0x010a
        L_0x004a:
            android.view.LayoutInflater r1 = r6.cQ     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r2 = "message_system"
            int r2 = com.papaya.si.C0060z.layoutID(r2)     // Catch:{ Exception -> 0x00f5 }
            r3 = 0
            android.view.View r1 = r1.inflate(r2, r3)     // Catch:{ Exception -> 0x00f5 }
            com.papaya.si.ai r2 = new com.papaya.si.ai     // Catch:{ Exception -> 0x0101 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0101 }
            r1.setTag(r2)     // Catch:{ Exception -> 0x0101 }
            r2 = r1
        L_0x0060:
            java.lang.Object r1 = r2.getTag()     // Catch:{ Exception -> 0x0103 }
            com.papaya.si.ai r1 = (com.papaya.si.C0010ai) r1     // Catch:{ Exception -> 0x0103 }
            android.widget.TextView r1 = r1.dr     // Catch:{ Exception -> 0x0103 }
            java.lang.CharSequence r0 = r0.db     // Catch:{ Exception -> 0x0103 }
            r1.setText(r0)     // Catch:{ Exception -> 0x0103 }
            r0 = r2
            goto L_0x0036
        L_0x006f:
            if (r8 == 0) goto L_0x0079
            java.lang.Object r1 = r8.getTag()     // Catch:{ Exception -> 0x00f5 }
            boolean r1 = r1 instanceof com.papaya.si.C0009ah     // Catch:{ Exception -> 0x00f5 }
            if (r1 != 0) goto L_0x0108
        L_0x0079:
            android.view.LayoutInflater r1 = r6.cQ     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r2 = "message_self"
            int r2 = com.papaya.si.C0060z.layoutID(r2)     // Catch:{ Exception -> 0x00f5 }
            r3 = 0
            android.view.View r1 = r1.inflate(r2, r3)     // Catch:{ Exception -> 0x00f5 }
            com.papaya.si.ah r2 = new com.papaya.si.ah     // Catch:{ Exception -> 0x0101 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0101 }
            r1.setTag(r2)     // Catch:{ Exception -> 0x0101 }
            r2 = r1
        L_0x008f:
            java.lang.Object r1 = r2.getTag()     // Catch:{ Exception -> 0x0103 }
            com.papaya.si.ah r1 = (com.papaya.si.C0009ah) r1     // Catch:{ Exception -> 0x0103 }
            com.papaya.view.CardImageView r3 = r1.dq     // Catch:{ Exception -> 0x0103 }
            com.papaya.si.D r4 = r0.cR     // Catch:{ Exception -> 0x0103 }
            r3.refreshWithCard(r4)     // Catch:{ Exception -> 0x0103 }
            android.widget.TextView r1 = r1.dr     // Catch:{ Exception -> 0x0103 }
            java.lang.CharSequence r0 = r0.db     // Catch:{ Exception -> 0x0103 }
            r1.setText(r0)     // Catch:{ Exception -> 0x0103 }
            r0 = r2
            goto L_0x0036
        L_0x00a5:
            if (r8 == 0) goto L_0x00af
            java.lang.Object r1 = r8.getTag()     // Catch:{ Exception -> 0x00f5 }
            boolean r1 = r1 instanceof com.papaya.si.W     // Catch:{ Exception -> 0x00f5 }
            if (r1 != 0) goto L_0x0106
        L_0x00af:
            android.view.LayoutInflater r1 = r6.cQ     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r2 = "message_friend"
            int r2 = com.papaya.si.C0060z.layoutID(r2)     // Catch:{ Exception -> 0x00f5 }
            r3 = 0
            android.view.View r1 = r1.inflate(r2, r3)     // Catch:{ Exception -> 0x00f5 }
            com.papaya.si.W r2 = new com.papaya.si.W     // Catch:{ Exception -> 0x0101 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0101 }
            com.papaya.view.AppIconView r3 = r2.dp     // Catch:{ Exception -> 0x0101 }
            r3.setOnClickListener(r6)     // Catch:{ Exception -> 0x0101 }
            r1.setTag(r2)     // Catch:{ Exception -> 0x0101 }
            r2 = r1
        L_0x00ca:
            java.lang.Object r1 = r2.getTag()     // Catch:{ Exception -> 0x0103 }
            com.papaya.si.W r1 = (com.papaya.si.W) r1     // Catch:{ Exception -> 0x0103 }
            com.papaya.view.AppIconView r3 = r1.dp     // Catch:{ Exception -> 0x0103 }
            int r4 = r0.dd     // Catch:{ Exception -> 0x0103 }
            r3.setApp(r4)     // Catch:{ Exception -> 0x0103 }
            com.papaya.view.AppIconView r3 = r1.dp     // Catch:{ Exception -> 0x0103 }
            int r4 = r0.dd     // Catch:{ Exception -> 0x0103 }
            if (r4 > 0) goto L_0x00f3
            r4 = 8
        L_0x00df:
            r3.setVisibility(r4)     // Catch:{ Exception -> 0x0103 }
            com.papaya.view.CardImageView r3 = r1.dq     // Catch:{ Exception -> 0x0103 }
            com.papaya.si.D r4 = r0.cR     // Catch:{ Exception -> 0x0103 }
            r3.refreshWithCard(r4)     // Catch:{ Exception -> 0x0103 }
            android.widget.TextView r1 = r1.dr     // Catch:{ Exception -> 0x0103 }
            java.lang.CharSequence r0 = r0.db     // Catch:{ Exception -> 0x0103 }
            r1.setText(r0)     // Catch:{ Exception -> 0x0103 }
            r0 = r2
            goto L_0x0036
        L_0x00f3:
            r4 = r5
            goto L_0x00df
        L_0x00f5:
            r0 = move-exception
            r1 = r8
        L_0x00f7:
            java.lang.String r2 = "Failed in ChatAdapter.getView"
            java.lang.Object[] r3 = new java.lang.Object[r5]
            com.papaya.si.X.e(r0, r2, r3)
            r0 = r1
            goto L_0x0036
        L_0x0101:
            r0 = move-exception
            goto L_0x00f7
        L_0x0103:
            r0 = move-exception
            r1 = r2
            goto L_0x00f7
        L_0x0106:
            r2 = r8
            goto L_0x00ca
        L_0x0108:
            r2 = r8
            goto L_0x008f
        L_0x010a:
            r2 = r8
            goto L_0x0060
        L_0x010d:
            r0 = r8
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.papaya.si.K.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }

    public final int getViewTypeCount() {
        return 4;
    }

    public final boolean hasStableIds() {
        return false;
    }

    public final void onClick(View view) {
        if (view instanceof AppIconView) {
            AppIconView appIconView = (AppIconView) view;
            if (appIconView.getAppID() > 0) {
                String str = C0037c.B.get(appIconView.getAppID());
                if (!aV.isEmpty(str)) {
                    C0030bb.openExternalUri(this.cP, str);
                }
            }
        }
    }

    public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        try {
            if (this.cR.co != null) {
                O o = (O) this.cR.co.get(i);
                if (!aV.isEmpty(o.dc)) {
                    LazyImageView lazyImageView = new LazyImageView(this.cP);
                    lazyImageView.setImageUrl(o.dc);
                    new CustomDialog.Builder(this.cP).setView(lazyImageView).create().show();
                }
            }
        } catch (Exception e) {
            X.e(e, "Failed in onItemClick", new Object[0]);
        }
    }

    public final void refreshWithCard(D d) {
        this.cR = d;
        notifyDataSetChanged();
    }
}
