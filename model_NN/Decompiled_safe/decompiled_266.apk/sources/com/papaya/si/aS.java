package com.papaya.si;

import java.util.Formatter;

public final class aS {
    private Formatter gE = new Formatter(this.gF);
    private StringBuilder gF = new StringBuilder(256);

    aS() {
    }

    public final String format(String str, Object... objArr) {
        try {
            this.gF.setLength(0);
            this.gE.format(str, objArr);
            return this.gF.toString();
        } catch (Exception e) {
            X.e(e, "Failed to format " + str, new Object[0]);
            return str;
        }
    }
}
