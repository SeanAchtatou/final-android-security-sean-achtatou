package com.papaya.si;

import android.app.Dialog;
import android.content.Context;

public final class bA extends bG {
    private bG mX;
    private Dialog mY;

    public bA(Context context, String str) {
        super(context, str);
    }

    public final Dialog getDialog() {
        return this.mY;
    }

    public final bG getParentController() {
        return this.mX;
    }

    public final void setDialog(Dialog dialog) {
        this.mY = dialog;
    }

    public final void setParentController(bG bGVar) {
        this.mX = bGVar;
    }
}
