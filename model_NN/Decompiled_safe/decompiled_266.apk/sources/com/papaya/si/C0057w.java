package com.papaya.si;

import android.graphics.Paint;
import com.papaya.si.C0017ap;
import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Vector;

/* renamed from: com.papaya.si.w  reason: case insensitive filesystem */
public final class C0057w extends Thread implements C0017ap.a {
    private static int bp = 0;
    public boolean bq = false;
    public String br = C0037c.getString(C0060z.stringID("base_fpapaya"));
    public C0017ap bs;
    private String bt = C0056v.DEFAULT_HOST;
    public boolean bu = false;
    private long bv = 0;
    private Paint bw = new Paint();
    private C0019ar bx;

    public C0057w(C0019ar arVar) {
        byte[] read;
        this.bx = arVar;
        if (C0035bg.exist("papayafact") && (read = C0035bg.read("papayafact")) != null) {
            try {
                this.br = (String) ((Vector) C0035bg.loads(read)).elementAt(0);
            } catch (Exception e) {
                this.br = "";
            }
        }
    }

    private void handleCommand(final Vector vector) {
        int intValue = ((Integer) vector.elementAt(0)).intValue();
        if (!this.bx.dispatchCmd(intValue, vector)) {
            switch (intValue) {
                case 19:
                    C0028b.showInfo((String) vector.elementAt(1));
                    return;
                case 36:
                    this.br = (String) vector.elementAt(1);
                    return;
                case 62:
                case 87:
                case 106:
                case 111:
                case 10005:
                default:
                    return;
                case 63:
                    vector.elementAt(1);
                    return;
                case 71:
                    try {
                        int sgetInt = aV.sgetInt(vector, 1);
                        final Object sget = aV.sget(vector, 2);
                        if (sget instanceof Number) {
                            final int intValue2 = vector.size() > 4 ? aV.intValue(vector.get(3)) : 0;
                            if (sgetInt > 0) {
                                C0030bb.runInHandlerThread(new Runnable() {
                                    public final void run() {
                                        bp.onImageUploaded(((Number) sget).intValue(), intValue2);
                                    }
                                });
                                return;
                            } else {
                                C0030bb.runInHandlerThread(new Runnable() {
                                    public final void run() {
                                        bp.onImageUploadFailed(((Number) sget).intValue());
                                    }
                                });
                                return;
                            }
                        } else if (sget instanceof String) {
                            String[] split = ((String) sget).split("-");
                            final int intValue3 = aV.intValue(split[0]);
                            final int intValue4 = aV.intValue(split[1]);
                            final int intValue5 = vector.size() > 4 ? aV.intValue(vector.get(3)) : 0;
                            if (split.length != 2) {
                                return;
                            }
                            if (sgetInt > 0) {
                                C0030bb.runInHandlerThread(new Runnable() {
                                    public final void run() {
                                        bp.fireImageUploaded(intValue3, intValue4, intValue5);
                                    }
                                });
                                return;
                            } else {
                                C0030bb.runInHandlerThread(new Runnable() {
                                    public final void run() {
                                        bp.fireImageUploadFailed(intValue3, intValue4);
                                    }
                                });
                                return;
                            }
                        } else {
                            return;
                        }
                    } catch (Throwable th) {
                        X.e(th, "process_commands", new Object[0]);
                    }
                case 99:
                    C0028b.showInfo((String) vector.get(2));
                    return;
                case 107:
                    C0030bb.runInHandlerThread(new Runnable() {
                        public final void run() {
                            C0016ao.getInstance().checkUpdateRequest(vector);
                        }
                    });
                    return;
                case 108:
                    C0030bb.runInHandlerThread(new Runnable() {
                        public final void run() {
                            C0014am.getInstance().serverPushedUpdate(vector);
                        }
                    });
                    return;
                case 109:
                    C0030bb.runInHandlerThread(new Runnable() {
                        public final void run() {
                            C0014am.getInstance().updateFeatureVisible(vector);
                        }
                    });
                    return;
                case 201:
                    this.bt = (String) vector.elementAt(1);
                    try {
                        this.bs.close();
                    } catch (Exception e) {
                    }
                    if (connect()) {
                        C0037c.getSession().login();
                        return;
                    }
                    return;
                case 602:
                    C0037c.B.put(aV.sgetInt(vector, 1), (String) aV.sget(vector, 2));
                    C0037c.B.fireDataStateChanged();
                    return;
                case 1000:
                    int intValue6 = aV.intValue(vector.get(1));
                    if (intValue6 == 1) {
                        byte[] decompressZlib = aU.decompressZlib(new ByteArrayInputStream((byte[]) vector.get(2)));
                        if (decompressZlib != null) {
                            handleCommand((Vector) C0035bg.loads(decompressZlib));
                            return;
                        } else {
                            X.w("failed to decompress %s", vector);
                            return;
                        }
                    } else {
                        X.w("unknown compress type %d", Integer.valueOf(intValue6));
                        return;
                    }
            }
            X.e(th, "process_commands", new Object[0]);
        }
    }

    private void process_commands() throws Exception {
        byte[] recv;
        Vector vector;
        try {
            if (!this.bu && (recv = this.bs.recv()) != null && (vector = (Vector) C0035bg.loads(recv)) != null) {
                handleCommand(vector);
            }
        } catch (Exception e) {
            X.e(e, "Error in process_commands", new Object[0]);
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean connect() {
        C0028b.showOverlayDialog(4);
        int i = 0;
        while (i <= 0) {
            this.bs = C0017ap.getConnecter(0, this);
            this.bs.connect(this.bt, 1237);
            i++;
            if (this.bs.state() == 2) {
                break;
            }
        }
        C0028b.removeOverlayDialog(4);
        return this.bs.state() == 2;
    }

    public final int fontgetHeight() {
        return (int) (this.bw.descent() - this.bw.ascent());
    }

    public final int fontstringWidth(String str) {
        return (int) this.bw.measureText(str);
    }

    public final int fontsubstringWidth(String str, int i, int i2) {
        return (int) this.bw.measureText(str, i, i2);
    }

    public final void onConnectionStateUpdated(int i, int i2) {
        if (i != i2) {
            if (i2 == 0) {
                this.bx.fireConnectionLost();
            } else if (i2 == 2) {
                this.bx.fireConnectionEstablished();
            }
        }
    }

    public final void requestImage(int i, int i2, int i3) {
        send(71, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public final void run() {
        C0037c.getSession().loadCoreData();
        try {
            if (connect()) {
                C0037c.getSession().login();
            } else {
                this.bq = true;
                C0028b.showOverlayDialog(6);
            }
            while (!this.bu) {
                if (this.bq) {
                    aV.safeSleep(100);
                } else {
                    if (this.bs.state() != 2) {
                        if (System.currentTimeMillis() - this.bs.er < 5000 || !connect()) {
                            C0028b.showOverlayDialog(6);
                            this.bq = true;
                        } else {
                            C0037c.getSession().login();
                        }
                    }
                    bp++;
                    process_commands();
                    C0001a.getWebCache().poll();
                    C0055u imageVersion = C0001a.getImageVersion();
                    if (imageVersion != null) {
                        imageVersion.poll();
                    }
                    Thread.yield();
                    aV.safeSleep(Math.max(1L, (this.bv + 50) - System.currentTimeMillis()));
                    this.bv = System.currentTimeMillis();
                }
            }
        } catch (Exception e) {
            X.e(e, "exception in PapayaThread", new Object[0]);
        }
        X.i("PapayaThread exits", new Object[0]);
    }

    public final void send(int i, Object... objArr) {
        if (this.bs != null) {
            Vector vector = new Vector();
            vector.add(Integer.valueOf(i));
            for (Object add : objArr) {
                vector.add(add);
            }
            this.bs.send(vector);
        }
    }

    public final void send(List list) {
        if (this.bs != null) {
            this.bs.send(list);
        }
    }

    /* access modifiers changed from: package-private */
    public final byte[] sendlarge(byte[] bArr) {
        int i = 0;
        while (bArr.length - i > 512) {
            byte[] bArr2 = new byte[512];
            System.arraycopy(bArr, i, bArr2, 0, 512);
            i += 512;
            send(20, bArr2);
        }
        if (i == 0) {
            return bArr;
        }
        byte[] bArr3 = new byte[(bArr.length - i)];
        System.arraycopy(bArr, i, bArr3, 0, bArr.length - i);
        return bArr3;
    }

    public final void showPmailnum(Vector vector) {
    }

    public final void updateProgress(int i) {
    }
}
