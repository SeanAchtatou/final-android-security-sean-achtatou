package com.papaya.si;

import android.graphics.drawable.Drawable;
import com.papaya.social.PPYUser;

/* renamed from: com.papaya.si.aj  reason: case insensitive filesystem */
public final class C0011aj extends D implements PPYUser {
    private int cW;
    private int dY;
    public String dZ;
    private String dn = "";
    public long ea = 0;

    public C0011aj(int i, String str) {
        this.cW = i;
        this.dn = str;
    }

    public final int compareTo(D d) {
        if (getUserID() == A.bL.getUserID()) {
            return -1;
        }
        if (!(d instanceof C0011aj) || ((C0011aj) d).getUserID() != A.bL.getUserID()) {
            return super.compareTo(d);
        }
        return 1;
    }

    public final Drawable getDefaultDrawable() {
        return getUserID() == 0 ? C0037c.getDrawable("contacts") : C0037c.getBitmapDrawable("avatar_unknown");
    }

    public final String getMiniblog() {
        return this.dZ;
    }

    public final long getMiniblogTime() {
        return this.ea;
    }

    public final String getName() {
        return this.dn;
    }

    public final String getNickname() {
        return this.dn;
    }

    public final CharSequence getSubtitle() {
        return this.dZ;
    }

    public final String getTimeLabel() {
        if (this.ea <= 0) {
            return null;
        }
        long currentTimeMillis = (System.currentTimeMillis() / 1000) - this.ea;
        if (currentTimeMillis < 1) {
            return C0037c.getString("currently");
        }
        if (currentTimeMillis < 60) {
            return C0037c.getString("moment");
        }
        if (currentTimeMillis < 3600) {
            int i = ((int) currentTimeMillis) / 60;
            return String.valueOf(i) + ' ' + C0037c.getString("minute") + (i == 1 ? "" : C0037c.getString("plural")) + ' ' + C0037c.getString("ago");
        } else if (currentTimeMillis < 86400) {
            int i2 = ((int) currentTimeMillis) / 3600;
            return String.valueOf(i2) + ' ' + C0037c.getString("hour") + (i2 == 1 ? "" : C0037c.getString("plural")) + ' ' + C0037c.getString("ago");
        } else if (currentTimeMillis < 2592000) {
            int i3 = (((int) currentTimeMillis) / 3600) / 24;
            return String.valueOf(i3) + ' ' + C0037c.getString("day") + (i3 == 1 ? "" : C0037c.getString("plural")) + ' ' + C0037c.getString("ago");
        } else {
            int i4 = ((((int) currentTimeMillis) / 3600) / 24) / 30;
            return String.valueOf(i4) + ' ' + C0037c.getString("month") + (i4 == 1 ? "" : C0037c.getString("plural")) + ' ' + C0037c.getString("ago");
        }
    }

    public final String getTitle() {
        return getName();
    }

    public final int getUserID() {
        return this.cW;
    }

    public final boolean hasuserid() {
        return getUserID() > 0;
    }

    public final boolean isGrayScaled() {
        return getUserID() != 0 && this.state == 0;
    }

    public final void setMiniblog(String str) {
        this.dZ = str;
    }

    public final void setMiniblogTime(long j) {
        this.ea = j;
    }

    public final void setUserID(int i) {
        this.cW = i;
    }

    public final void updateOnline(boolean z) {
        if (z) {
            this.dY++;
        } else if (this.dY > 0) {
            this.dY--;
        }
        if (this.dY > 0 && this.state != 1) {
            this.state = 1;
            if (this.cp) {
                addSystemMessage(getName() + " " + C0037c.getString("chat_msg_sys_online"));
            }
        }
        if (this.dY == 0) {
            this.state = 0;
            if (this.cp) {
                addSystemMessage(getName() + " " + C0037c.getString("chat_msg_sys_offline"));
            }
        }
    }
}
