package com.papaya.si;

import android.content.Context;
import android.content.res.AssetManager;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/* renamed from: com.papaya.si.a  reason: case insensitive filesystem */
public final class C0001a {
    /* access modifiers changed from: private */
    public static C0026ay a;
    private static C0055u b;
    private String c;

    private C0001a() {
    }

    public C0001a(String str) {
        this.c = str;
    }

    public static void clearCaches() {
        C0028b.showOverlayDialog(7);
        new Thread(new Runnable() {
            public final void run() {
                C0001a.a.clearCache();
                C0028b.removeOverlayDialog(7);
            }
        }).start();
    }

    public static void destroy() {
        if (a != null) {
            a.close();
        }
        if (b != null) {
            b.close();
        }
    }

    public static C0055u getImageVersion() {
        return b;
    }

    public static C0026ay getWebCache() {
        return a;
    }

    public static void initialize(Context context) {
        C0026ay ayVar = new C0026ay("social_cache", context);
        a = ayVar;
        ayVar.initCache();
        b = new C0055u();
    }

    public static void insertRequests(List<bv> list) {
        if (a != null) {
            a.insertRequests(list);
        } else {
            X.e("skip insertRequests", new Object[0]);
        }
    }

    public final InputStream openAssetInput(AssetManager assetManager) {
        if (this.c != null) {
            try {
                return assetManager.open(this.c);
            } catch (IOException e) {
                X.w(e, "Failed to openAssetInput %s", this.c);
            }
        } else {
            X.w("not an asset fd", new Object[0]);
            return null;
        }
    }

    public final InputStream openInput(AssetManager assetManager) {
        return openAssetInput(assetManager);
    }
}
