package com.papaya.si;

import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public final class aZ {
    private List<StringBuilder> gD = new ArrayList();

    private void debug() {
        int i = 0;
        for (StringBuilder capacity : this.gD) {
            i += capacity.capacity();
        }
        Log.d("PPYSocial", "pool size: " + i);
    }

    public final synchronized StringBuilder acquire(int i) {
        int i2;
        StringBuilder remove;
        int i3 = 0;
        synchronized (this) {
            int size = this.gD.size();
            while (true) {
                if (i3 >= size) {
                    i2 = -1;
                    break;
                } else if (this.gD.get(i3).capacity() >= i) {
                    i2 = i3;
                    break;
                } else {
                    i3++;
                }
            }
            remove = i2 != -1 ? this.gD.remove(i2) : this.gD.isEmpty() ? new StringBuilder(i) : this.gD.remove(0);
            remove.setLength(0);
        }
        return remove;
    }

    public final synchronized void clear() {
        this.gD.clear();
    }

    public final synchronized String release(StringBuilder sb) {
        if (!this.gD.contains(sb)) {
            this.gD.add(sb);
        }
        return sb.toString();
    }

    public final synchronized void releaseOnly(StringBuilder sb) {
        if (!this.gD.contains(sb)) {
            this.gD.add(sb);
        }
    }
}
