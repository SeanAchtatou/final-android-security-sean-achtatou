package com.papaya.si;

import com.adknowledge.superrewards.model.SROffer;
import java.net.URL;
import org.json.JSONObject;

public final class bB {
    private bk ki;
    private String mZ;
    private JSONObject na;
    private URL url;

    public bB(URL url2, String str) {
        this.url = url2;
        this.mZ = str;
    }

    public final void assignWebView(bk bkVar) {
        if (this.ki != null) {
            X.e("duplicated assign?", new Object[0]);
        }
        this.ki = bkVar;
        bkVar.setHistory(this);
    }

    public final void freeWebView() {
        if (this.ki != null) {
            this.ki.noWarnCallJS("webdisappeared", "webdisappeared()");
            this.ki.setHistory(null);
            bH.getInstance().freeWebView(this.ki);
            this.ki = null;
        }
    }

    public final String getTitle() {
        return this.mZ;
    }

    public final JSONObject getTitleCtx() {
        return this.na;
    }

    public final URL getURL() {
        return this.url;
    }

    public final bk getWebView() {
        return this.ki;
    }

    public final void hideWebView() {
        if (this.ki != null) {
            this.ki.setVisibility(4);
            this.ki.noWarnCallJS("webdisappeared", "webdisappeared()");
        }
    }

    public final boolean openWebView(bG bGVar, URL url2, boolean z) {
        if (bGVar == null) {
            X.e("nil controller ?!", new Object[0]);
            return false;
        } else if (url2 == null && this.url == null) {
            X.e("both uris are null!!!", new Object[0]);
            return false;
        } else {
            if (this.ki == null) {
                aT aTVar = new aT(Boolean.FALSE);
                assignWebView(bH.getInstance().getWebView(bGVar, z ? url2 : null, aTVar));
                if (((Boolean) aTVar.gG).booleanValue()) {
                    this.ki.setVisibility(0);
                    this.ki.noWarnCallJS("webappeared", "webappeared(false)");
                    return true;
                }
                this.ki.setVisibility(4);
                this.ki.noWarnCallJS("webdisappeared", "webdisappeared()");
                this.ki.loadPapayaURL(url2 == null ? this.url : url2);
            } else {
                if (this.ki.getController() != bGVar) {
                    X.w("Inconsistent controller of webview !!!", new Object[0]);
                    bGVar.configWebView(this.ki);
                }
                if (url2 != null || !z) {
                    this.ki.setVisibility(4);
                    this.ki.noWarnCallJS("webdisappeared", "webdisappeared()");
                    this.ki.loadPapayaURL(url2 == null ? this.url : url2);
                } else if (!this.ki.isReusable() || this.ki.isLoadFromString()) {
                    this.ki.setVisibility(4);
                    this.ki.noWarnCallJS("webdisappeared", "webdisappeared()");
                    this.ki.loadPapayaURL(this.url);
                } else {
                    this.ki.setVisibility(0);
                    this.ki.noWarnCallJS("webappeared", "webappeared(false)");
                    return true;
                }
            }
            return false;
        }
    }

    public final void setTitle(String str) {
        this.mZ = str;
    }

    public final void setTitleCtx(JSONObject jSONObject) {
        this.na = jSONObject;
        this.mZ = aV.nonNullString(C0034bf.getJsonString(this.na, SROffer.TITLE), this.mZ);
    }

    public final void setURL(URL url2) {
        this.url = url2;
    }
}
