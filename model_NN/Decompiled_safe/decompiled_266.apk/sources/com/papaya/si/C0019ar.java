package com.papaya.si;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/* renamed from: com.papaya.si.ar  reason: case insensitive filesystem */
public final class C0019ar {
    private C0057w eY;
    private a eZ;
    private final HashMap<Integer, C0054t> fa = new HashMap<>(100);
    private final C0031bc<C0058x> fb = new C0031bc<>(8);

    /* renamed from: com.papaya.si.ar$a */
    class a extends aL {
        /* synthetic */ a(C0019ar arVar) {
            this((byte) 0);
        }

        private a(byte b) {
        }

        /* access modifiers changed from: protected */
        public final void runTask() {
            C0019ar.this.close();
        }
    }

    public final void addConnectionDelegate(C0058x xVar) {
        if (xVar != null) {
            synchronized (this.fb) {
                if (!this.fb.contains(xVar)) {
                    this.fb.add(xVar);
                }
            }
        }
    }

    public final void cancelDisconnectTask() {
        aL.cancelTask(this.eZ);
        this.eZ = null;
    }

    public final void close() {
        if (this.eY != null) {
            this.eY.bu = true;
        }
        if (!(this.eY == null || this.eY.bs == null)) {
            this.eY.bs.close();
            this.eY.bs = null;
        }
        this.eY = null;
    }

    public final boolean dispatchCmd(int i, final Vector vector) {
        final C0054t tVar;
        synchronized (this.fa) {
            tVar = this.fa.get(Integer.valueOf(i));
        }
        if (tVar == null) {
            return false;
        }
        if (tVar instanceof aW) {
            C0030bb.runInHandlerThread(new Runnable() {
                public final void run() {
                    C0054t.this.handleServerResponse(vector);
                }
            });
        } else {
            tVar.handleServerResponse(vector);
        }
        return true;
    }

    public final void fireConnectionEstablished() {
        synchronized (this.fb) {
            int size = this.fb.size();
            for (int i = 0; i < size; i++) {
                final C0058x xVar = this.fb.get(i);
                if (xVar != null) {
                    if (xVar instanceof aW) {
                        C0030bb.runInHandlerThread(new Runnable() {
                            public final void run() {
                                C0058x.this.onConnectionEstablished();
                            }
                        });
                    } else {
                        xVar.onConnectionEstablished();
                    }
                }
            }
        }
    }

    public final void fireConnectionLost() {
        synchronized (this.fb) {
            int size = this.fb.size();
            for (int i = 0; i < size; i++) {
                final C0058x xVar = this.fb.get(i);
                if (xVar != null) {
                    if (xVar instanceof aW) {
                        C0030bb.runInHandlerThread(new Runnable() {
                            public final void run() {
                                C0058x.this.onConnectionLost();
                            }
                        });
                    } else {
                        xVar.onConnectionLost();
                    }
                }
            }
        }
    }

    public final String getFunFact() {
        return this.eY != null ? this.eY.br : C0037c.getString("base_entry_wait");
    }

    public final boolean isRunning() {
        return this.eY != null && !this.eY.bu;
    }

    public final void registerCmds(C0054t tVar, Integer... numArr) {
        synchronized (this.fa) {
            for (Integer put : numArr) {
                this.fa.put(put, tVar);
            }
        }
    }

    public final void removeConnectionDelegate(C0058x xVar) {
        if (xVar != null) {
            synchronized (this.fb) {
                this.fb.remove(xVar);
            }
        }
    }

    public final void send(int i, Object... objArr) {
        if (this.eY != null) {
            this.eY.send(i, objArr);
        }
    }

    public final void send(List list) {
        if (this.eY != null) {
            this.eY.send(list);
        }
    }

    public final void setPaused(boolean z) {
        if (this.eY != null) {
            this.eY.bq = z;
        }
    }

    public final void start() {
        if (this.eY == null) {
            this.eY = new C0057w(this);
            this.eY.start();
        }
        cancelDisconnectTask();
    }

    public final void startDisconnectTask() {
        aL.cancelTask(this.eZ);
        if (this.eY != null) {
            this.eZ = new a(this);
            C0030bb.postDelayed(this.eZ, 180000);
        }
    }

    public final void unregisterCmd(C0054t tVar, Integer... numArr) {
        synchronized (this.fa) {
            if (numArr.length == 0) {
                Iterator it = new ArrayList(this.fa.keySet()).iterator();
                while (it.hasNext()) {
                    int intValue = ((Integer) it.next()).intValue();
                    if (this.fa.get(Integer.valueOf(intValue)) == tVar) {
                        this.fa.remove(Integer.valueOf(intValue));
                    }
                }
            } else {
                for (Integer num : numArr) {
                    if (this.fa.get(num) == tVar) {
                        this.fa.remove(num);
                    }
                }
            }
        }
    }
}
