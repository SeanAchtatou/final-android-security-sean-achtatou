package com.papaya.si;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/* renamed from: com.papaya.si.ac  reason: case insensitive filesystem */
public final class C0004ac extends ArrayList<C0005ad> {
    public final void loadFromFile(String str) {
        clear();
        List list = (List) aU.loadPotpStorage(str);
        if (list != null) {
            int i = 0;
            while (i < list.size()) {
                try {
                    C0005ad adVar = new C0005ad();
                    adVar.dP = aV.sgetInt(list, i);
                    adVar.dO = aV.sgetInt(list, i + 1);
                    adVar.dQ = (String) aV.sget(list, i + 2);
                    adVar.dR = (String) aV.sget(list, i + 3);
                    add(adVar);
                    i += 4;
                } catch (Exception e) {
                    X.e(e, "Failed to load from " + str, new Object[0]);
                    return;
                }
            }
        }
    }

    public final void saveToFile(String str) {
        try {
            Vector vector = new Vector();
            for (int i = 0; i < size(); i++) {
                C0005ad adVar = (C0005ad) get(i);
                vector.add(Integer.valueOf(adVar.dP));
                vector.add(Integer.valueOf(adVar.dO));
                vector.add(adVar.dQ);
                vector.add(adVar.dR);
            }
            C0035bg.write(str, C0035bg.dumps(vector));
        } catch (Exception e) {
            X.e(e, "Failed to save to " + str, new Object[0]);
        }
    }
}
