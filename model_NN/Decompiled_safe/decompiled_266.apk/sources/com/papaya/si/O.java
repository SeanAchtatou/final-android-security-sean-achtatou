package com.papaya.si;

import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class O {
    private static Pattern cX = null;
    private static String[] cY = {":)", ":D", ":p", ":(", ":'(", "|-)", ":@", ":s", ":o", ":$", "(y)", "(n)", "(h)", "8o|", "(l)"};
    private static ArrayList<String> cZ = new ArrayList<>(42);
    private static HashMap<String, String> da = new HashMap<>(42);
    public D cR;
    public CharSequence db;
    public String dc;
    public int dd;
    public int type;

    static {
        for (int i = 0; i < cY.length; i++) {
            cZ.add(cY[i]);
            da.put(cY[i], "e" + i);
        }
        for (int length = cY.length; length < 42; length++) {
            String str = "[~" + (length - cY.length) + "~]";
            cZ.add(str);
            da.put(str, "e" + length);
        }
        cZ.add("[~pic~]");
        da.put("[~pic~]", "photo");
    }

    public O() {
    }

    public O(int i, D d, CharSequence charSequence, int i2) {
        this.type = i;
        this.cR = d;
        this.dd = i2;
        if (charSequence instanceof String) {
            String string = i == 2 ? C0037c.getString("chat_msg_prefix") : i == 3 ? d.getTitle() + ": " : null;
            this.dc = parseUri((String) charSequence);
            this.db = parseEmoticon((String) (this.dc != null ? C0037c.getString("chat_msg_just_sent_picture") + cZ.get(cZ.size() - 1) + C0037c.getString("chat_msg_tap_view") : charSequence), string);
            return;
        }
        this.db = charSequence;
    }

    private static void buildPattern() {
        StringBuilder sb = new StringBuilder(168);
        sb.append('(');
        for (int i = 0; i < 43; i++) {
            sb.append(Pattern.quote(getEmoticonString(i)));
            if (i < 42) {
                sb.append('|');
            }
        }
        sb.append(')');
        cX = Pattern.compile(sb.toString());
    }

    public static String getEmoticonString(int i) {
        return cZ.get(i);
    }

    public static CharSequence parseEmoticon(String str) {
        return parseEmoticon(str, null);
    }

    public static CharSequence parseEmoticon(String str, String str2) {
        if (str == null) {
            return null;
        }
        if (cX == null) {
            buildPattern();
        }
        String str3 = str2 != null ? str2 + str : str;
        SpannableString spannableString = new SpannableString(str3);
        Matcher matcher = cX.matcher(str3);
        int i = 0;
        while (matcher.find() && (i = i + 1) <= 500) {
            Drawable drawable = C0037c.getApplicationContext().getResources().getDrawable(C0060z.drawableID(da.get(matcher.group())));
            drawable.setBounds(0, 0, C0030bb.rp(16), C0030bb.rp(16));
            spannableString.setSpan(new ImageSpan(drawable, 1), matcher.start(), matcher.end(), 33);
        }
        return spannableString;
    }

    public static String parseUri(String str) {
        int indexOf;
        if (str == null) {
            return null;
        }
        if (!str.startsWith("[Picture ") || (indexOf = str.indexOf(93)) == -1) {
            return null;
        }
        return str.substring(9, indexOf).trim();
    }
}
