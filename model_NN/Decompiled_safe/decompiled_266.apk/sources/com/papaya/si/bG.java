package com.papaya.si;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.provider.MediaStore;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.papaya.si.C0055u;
import com.papaya.si.bk;
import com.papaya.si.bn;
import com.papaya.si.bo;
import com.papaya.utils.CountryCodeActivity;
import com.papaya.view.AdWrapperView;
import com.papaya.view.CustomDialog;
import com.papaya.view.MaskLoadingView;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;

public class bG extends bw<Activity> implements bk.a, bn.a, bo.a, C0058x, aN, aW {
    private String aV;
    private Context cP;
    private boolean lX = true;
    private MaskLoadingView ly;
    private bF nc;
    private HashMap<String, C0013al> ne = new HashMap<>();
    private RelativeLayout oa;
    private FrameLayout ob;
    private URL oc;
    private LinkedList<bB> od = new LinkedList<>();
    private int oe = 0;
    private bn of;
    private bo og;
    private boolean oh = false;
    protected String oi;
    AdWrapperView oj = null;

    public bG(Context context, String str) {
        this.cP = context;
        if (this.cP instanceof Activity) {
            setOwnerActivity((Activity) this.cP);
        }
        this.oa = new RelativeLayout(context);
        this.oa.setBackgroundColor(-1);
        this.aV = str;
        this.oc = C0034bf.createURL(this.aV);
        setupViews();
        this.nc = new bF();
        bH.getInstance().onControllerCreated(this);
        C0037c.A.addConnectionDelegate(this);
        A.bL.registerMonitor(this);
    }

    public void callJS(String str) {
        bk topWebView = getTopWebView();
        if (topWebView != null) {
            topWebView.callJS(str);
        }
    }

    public boolean canStartGPS() {
        return ((LocationManager) C0037c.getApplicationContext().getSystemService("location")).isProviderEnabled("network");
    }

    public void clearHistory(int i, bk bkVar) {
        if (i == 1) {
            while (this.od.size() > 1) {
                bB first = this.od.getFirst();
                if (bkVar != null && bkVar == first.getWebView()) {
                    break;
                }
                first.freeWebView();
                this.od.removeFirst();
            }
        } else if (i == 2) {
            int historyIndex = historyIndex(bkVar);
            if (historyIndex > 1) {
                for (int i2 = 1; i2 < historyIndex; i2++) {
                    this.od.get(1).freeWebView();
                    this.od.remove(1);
                }
            }
        } else {
            X.e("Unknown mode for clear history %d", Integer.valueOf(i));
        }
        updateActivityTitle(bkVar);
    }

    public void close() {
        this.nc.clear();
        for (C0013al close : this.ne.values()) {
            close.close();
        }
        this.ne.clear();
        if (C0037c.A != null) {
            C0037c.A.removeConnectionDelegate(this);
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.od.size()) {
                bB bBVar = this.od.get(i2);
                C0030bb.removeFromSuperView(bBVar.getWebView());
                bBVar.freeWebView();
                i = i2 + 1;
            } else {
                this.od.clear();
                bH.getInstance().onControllerClosed(this);
                A.bL.unregisterMonitor(this);
                return;
            }
        }
    }

    public void configWebView(bk bkVar) {
        C0030bb.addView(this.ob, bkVar, true);
        bkVar.setController(this);
        bkVar.setDelegate(this);
        bkVar.setRequireSid(this.lX);
    }

    public int forceFreeWebViews(int i, boolean z) {
        int i2;
        int i3 = 0;
        int size = this.od.size();
        int i4 = i < 0 ? size : i;
        int i5 = 0;
        while (true) {
            if (i3 >= (z ? size - 1 : size)) {
                return i5;
            }
            bB bBVar = this.od.get(i3);
            bk webView = bBVar.getWebView();
            if (webView == null || !webView.isRecylable()) {
                i2 = i5;
            } else {
                bBVar.freeWebView();
                i2 = i5 + 1;
            }
            if (i2 >= i4) {
                return i2;
            }
            i3++;
            i5 = i2;
        }
    }

    public RelativeLayout getContentLayout() {
        return this.oa;
    }

    public Context getContext() {
        return this.cP;
    }

    public LinkedList<bB> getHistories() {
        return this.od;
    }

    public bk getTopWebView() {
        if (!this.od.isEmpty()) {
            return this.od.getLast().getWebView();
        }
        return null;
    }

    public bF getUIHelper() {
        return this.nc;
    }

    public ViewGroup getWebContentView() {
        return this.ob;
    }

    /* access modifiers changed from: protected */
    public void handlePapayaUrl(bk bkVar, String str, String str2, String str3, URL url) {
        String str4 = str == null ? "" : str;
        String str5 = str3 == null ? "" : str3;
        if (str4.startsWith("slide")) {
            if ("slideback".equals(str4) || !aV.isEmpty(str5)) {
                this.oe = 0;
                if (str4.equals("slidetoright")) {
                    this.oe = 1;
                } else if (str4.equals("slidetoleft")) {
                    this.oe = 2;
                } else if (str4.equals("slidetotop")) {
                    this.oe = 4;
                } else if (str4.equals("slidetobottom")) {
                    this.oe = 3;
                } else if (str4.equals("slidenewpage")) {
                    this.oe = 5;
                } else if (str4.equals("slideback")) {
                    this.oe = 6;
                } else if (str4.startsWith("sliderefresh")) {
                    this.oe = 7;
                } else if (str4.startsWith("slideno")) {
                    this.oe = 0;
                }
            }
            if (!(bkVar == null || getTopWebView() == null || bkVar == getTopWebView())) {
                if (this.oe == 7 || this.oe == 0) {
                    this.oe = 5;
                } else if (this.oe == 6) {
                    X.w("Skip slide back request: %s, %s", bkVar, getTopWebView());
                    return;
                }
            }
            if (this.oe != 6) {
                openURL(C0034bf.createURL(str5, url), this.oe != 7);
            } else if (this.od.size() > 1) {
                this.od.getLast().freeWebView();
                this.od.removeLast();
                bB last = this.od.getLast();
                boolean openWebView = last.openWebView(this, null, true);
                C0030bb.addView(this.ob, last.getWebView(), true);
                updateActivityTitle(last.getWebView());
                if (openWebView) {
                    webViewPostProcess(getTopWebView(), false);
                }
            } else {
                X.w("incorrect history size %d", Integer.valueOf(this.od.size()));
            }
        } else if ("ajax".equals(str4)) {
            X.e("ajax is deprecated", new Object[0]);
            C0028b.showInfo("ajax is deprecated");
        } else if ("changehp".equals(str4)) {
            Activity ownerActivity = getOwnerActivity();
            if (ownerActivity != null) {
                new CustomDialog.Builder(ownerActivity).setTitle(C0060z.stringID("web_hp_title")).setItems(new CharSequence[]{C0037c.getString(C0060z.stringID("web_hp_camera")), C0037c.getString(C0060z.stringID("web_hp_pictures"))}, new DialogInterface.OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        Activity ownerActivity = bG.this.getOwnerActivity();
                        if (i == 0) {
                            C0030bb.startCameraActivity(ownerActivity, 1);
                        } else if (i == 1) {
                            C0030bb.startGalleryActivity(ownerActivity, 2);
                        }
                    }
                }).show();
            }
        } else if ("uploadphoto".equals(str4)) {
            Activity ownerActivity2 = getOwnerActivity();
            if (ownerActivity2 != null) {
                new CustomDialog.Builder(ownerActivity2).setTitle(C0060z.stringID("web_up_photo_title")).setItems(new CharSequence[]{C0037c.getString(C0060z.stringID("web_up_photo_camera")), C0037c.getString(C0060z.stringID("web_up_photo_pictures"))}, new DialogInterface.OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        Activity ownerActivity = bG.this.getOwnerActivity();
                        if (ownerActivity == null) {
                            return;
                        }
                        if (i == 0) {
                            C0030bb.startCameraActivity(ownerActivity, 3);
                        } else if (i == 1) {
                            ownerActivity.startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 4);
                        }
                    }
                }).show();
            }
        } else if ("savetoalbum".equals(str4)) {
            int saveToPictures = this.of.saveToPictures(str5, bkVar.getPapayaURL(), str2);
            if (saveToPictures == 1) {
                bkVar.callJS(aV.format("photosaved(%d, '%s')", 1, str2));
            } else if (saveToPictures == -1) {
                bkVar.callJS(aV.format("photosaved(%d, '%s')", 0, str2));
            }
        } else if ("uploadtopicasa".equals(str4)) {
            int uploadToPicasa = this.og.uploadToPicasa(str5, bkVar.getPapayaURL(), str2);
            if (uploadToPicasa == 1) {
                bkVar.noWarnCallJS("picasaupload", aV.format("picasaupload(%d, '%s')", 1, str2));
            } else if (uploadToPicasa == -1) {
                bkVar.noWarnCallJS("picasaupload", aV.format("picasaupload(%d, '%s')", 0, str2));
            }
        } else if ("createselector".equals(str4)) {
            X.w("createselector is deprecated", new Object[0]);
            if (bkVar != null) {
                bkVar.callJS(aV.format("window.Papaya.selectorShow_(%s)", str2));
            }
        } else if ("createdatepicker".equals(str4)) {
            X.w("createdatepicker is deprecated", new Object[0]);
            if (bkVar != null) {
                bkVar.callJS(aV.format("window.Papaya.datePickerShow_(%s)", str2));
            }
        } else if ("showpictures".equals(str4)) {
            if (bkVar != null) {
                bkVar.callJS(aV.format("window.Papaya.picturesShow_(%s)", str2));
            }
        } else if ("showalert".equals(str4)) {
            if (bkVar != null) {
                bkVar.callJS(aV.format("window.Papaya.alertShow_(%s)", str2));
            }
        } else if ("synccontactsimport".equals(str4)) {
            C0055u.b.importContacts();
        } else if ("synccontactsexport".equals(str4)) {
            C0055u.b.exportContacts();
        } else if ("synccontactsmerge".equals(str4)) {
            C0055u.b.mergeContacts();
        } else if ("startchat".equals(str4)) {
            C0037c.getSession().startChat(aV.intValue(str5));
        } else if ("showmultiplelineinput".equals(str4)) {
            if (bkVar != null) {
                bkVar.callJS(aV.format("window.Papaya.multipleLineInputShow_(%s)", str2));
            }
        } else if ("showaddressbook".equals(str4)) {
            X.e("social sdk doesn't support addressbook", new Object[0]);
        } else if ("switchtab".equals(str4)) {
            C0028b.openPRIALink(getOwnerActivity(), str5, str2);
        } else if ("showcountry".equals(str4)) {
            Activity ownerActivity3 = getOwnerActivity();
            if (ownerActivity3 != null) {
                ownerActivity3.startActivityForResult(new Intent(ownerActivity3, CountryCodeActivity.class).putExtra("action", str2), 6);
            }
        } else if ("clearcache".equals(str4)) {
            C0001a.clearCaches();
        } else if ("papayalogout".equals(str4)) {
            C0037c.getSession().logout();
            C0030bb.runInHandlerThreadDelay(new Runnable() {
                public final void run() {
                    C0037c.quit();
                }
            });
        } else if ("showchatgroupinvite".equals(str4)) {
            X.e("showchatgroupinvite is deprecated", new Object[0]);
        } else if ("openMarket".equals(str4)) {
            C0030bb.openExternalUri(getOwnerActivity(), str5);
        } else if ("go".equals(str4)) {
            C0028b.openPRIALink(getOwnerActivity(), str5, str2);
        } else if (!C0037c.getWebGameBridge().handlePapayaUrl(this, bkVar, str4, str2, str5, url)) {
            X.w("unknown papaya uri %s %s, %s, %s", str4, str2, str5, url);
        }
    }

    public void hideLoading() {
        if (C0030bb.isMainThread()) {
            hideLoadingInUIThread();
        } else {
            C0030bb.runInHandlerThread(new Runnable() {
                public final void run() {
                    bG.this.hideLoadingInUIThread();
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void hideLoadingInUIThread() {
        try {
            this.ly.setVisibility(8);
        } catch (Exception e) {
            X.e(e, "Failed to hide loadingView", new Object[0]);
        }
    }

    public void hideMap() {
    }

    public int historyIndex(bk bkVar) {
        if (bkVar != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.od.size()) {
                    break;
                } else if (bkVar == this.od.get(i2).getWebView()) {
                    return i2;
                } else {
                    i = i2 + 1;
                }
            }
        }
        return -1;
    }

    public boolean isRequireSid() {
        return this.lX;
    }

    public boolean isSupportReload() {
        return this.oh;
    }

    public boolean onBackClicked() {
        bk topWebView = getTopWebView();
        if (topWebView == null) {
            return false;
        }
        String jsonString = C0034bf.getJsonString(C0034bf.getJsonObject(this.od.getLast().getTitleCtx(), "leftbtn"), "action");
        if (!aV.isEmpty(jsonString)) {
            topWebView.callJS(jsonString);
            return true;
        } else if (this.od.size() <= 1) {
            return false;
        } else {
            shouldOverrideUrlLoading(topWebView, "papaya://slideback");
            return true;
        }
    }

    public void onConnectionEstablished() {
        openInitUrlIfPossible();
    }

    public void onConnectionLost() {
    }

    public boolean onDataStateChanged(aP aPVar) {
        openInitUrlIfPossible();
        return false;
    }

    public void onOrientationChanged(int i) {
        bk topWebView = getTopWebView();
        if (topWebView != null) {
            topWebView.changeOrientation(i);
        }
    }

    public void onPageFinished(bk bkVar, String str) {
        if (!bkVar.isClosed()) {
            if (bkVar == getTopWebView()) {
                bkVar.setVisibility(0);
            } else {
                X.e("not the top webview!!!, %s", str);
            }
            bkVar.updateTitleFromHTML();
            bkVar.noWarnCallJS("webloading", "webloading()");
            webViewPostProcess(bkVar, true);
            bkVar.setLoadFromString(false);
            updateActivityTitle(bkVar);
        }
    }

    public void onPageStarted(bk bkVar, String str, Bitmap bitmap) {
        showLoading();
        bkVar.getPapayaScript().clearSessionState();
    }

    public void onPause() {
        bk webView;
        try {
            if (!this.od.isEmpty() && (webView = this.od.getLast().getWebView()) != null) {
                webView.noWarnCallJS("webdisappeared", "webdisappeared();");
            }
        } catch (Exception e) {
            X.e(e, "Failed in onPause", new Object[0]);
        }
    }

    public void onPhotoPicasa(URL url, URL url2, String str, boolean z) {
        bk topWebView = getTopWebView();
        if (topWebView != null && url2 == topWebView.getPapayaURL()) {
            Object[] objArr = new Object[2];
            objArr[0] = Integer.valueOf(z ? 1 : 0);
            objArr[1] = str;
            topWebView.callJS(aV.format("picasaupload(%d, '%s')", objArr));
        }
    }

    public void onPhotoSaved(URL url, URL url2, String str, boolean z) {
        bk topWebView = getTopWebView();
        if (topWebView != null && url2 == topWebView.getPapayaURL()) {
            Object[] objArr = new Object[2];
            objArr[0] = Integer.valueOf(z ? 1 : 0);
            objArr[1] = str;
            topWebView.callJS(aV.format("photosaved(%d, '%s')", objArr));
        }
    }

    public void onReceivedError(bk bkVar, int i, String str, String str2) {
        hideLoading();
        this.oi = str2;
        showLoadError();
    }

    public void onResume() {
        try {
            if (!this.od.isEmpty()) {
                bB last = this.od.getLast();
                bk webView = last.getWebView();
                if (webView != null) {
                    webView.noWarnCallJS("webappeared", "webappeared(false);");
                } else {
                    last.openWebView(this, last.getURL(), true);
                }
            } else {
                openInitUrlIfPossible();
            }
        } catch (Exception e) {
            X.e(e, "Failed in onResume", new Object[0]);
        }
    }

    public void onWebLoaded(bk bkVar) {
        updateActivityTitle(bkVar);
    }

    public C0013al openDatabase(String str) {
        C0013al alVar = this.ne.get(str);
        if (alVar != null) {
            return alVar;
        }
        C0013al openMemoryDatabase = C0013al.openMemoryDatabase();
        openMemoryDatabase.setDbId(str);
        openMemoryDatabase.setScope(2);
        this.ne.put(str, openMemoryDatabase);
        return openMemoryDatabase;
    }

    public void openInitUrlIfPossible() {
        if ((!this.lX || C0025ax.getInstance().isConnected() || C0016ao.isSessionLess(this.aV)) && getTopWebView() == null && C0016ao.getInstance().isReady()) {
            openUrl(this.aV);
        } else if (getTopWebView() == null) {
            showLoading();
        }
    }

    /* access modifiers changed from: protected */
    public void openURL(URL url, boolean z) {
        if (url != null) {
            if (!this.od.isEmpty()) {
                if (this.oe == 5 || this.oe == 6) {
                    this.od.getLast().hideWebView();
                } else {
                    this.od.getLast().freeWebView();
                    this.od.removeLast();
                }
            } else if (this.oc != null && !C0034bf.urlEquals(this.oc, url)) {
                this.od.addLast(new bB(this.oc, null));
            }
            bB bBVar = new bB(url, null);
            boolean openWebView = bBVar.openWebView(this, url, z);
            this.od.addLast(bBVar);
            C0030bb.addView(this.ob, bBVar.getWebView(), true);
            updateActivityTitle(bBVar.getWebView());
            if (openWebView) {
                webViewPostProcess(getTopWebView(), false);
            }
        }
    }

    public void openUrl(String str) {
        if (aV.isEmpty(str)) {
            X.i("skip null url in openUrl", new Object[0]);
        } else {
            shouldOverrideUrlLoading(null, str);
        }
    }

    public void setRequireSid(boolean z) {
        this.lX = z;
    }

    public void setSupportReload(boolean z) {
        this.oh = z;
    }

    /* access modifiers changed from: protected */
    public void setupViews() {
        this.of = new bn(this.cP);
        this.of.setDelegate(this);
        this.og = new bo(this.cP);
        this.og.setDelegate(this);
        this.ob = new FrameLayout(this.cP);
        this.oa.addView(this.ob, new RelativeLayout.LayoutParams(-1, -1));
        this.ly = new MaskLoadingView(this.cP, 1, 0);
        this.ly.setVisibility(8);
        this.oa.addView(this.ly, new RelativeLayout.LayoutParams(-1, -1));
    }

    public boolean shouldOverrideUrlLoading(bk bkVar, String str) {
        String str2;
        String str3;
        String str4 = null;
        URL papayaURL = bkVar != null ? bkVar.getPapayaURL() : null;
        URL url = papayaURL == null ? C0056v.bo : papayaURL;
        try {
            int indexOf = str.indexOf("://");
            if (indexOf == -1 || indexOf + 3 >= str.length()) {
                str2 = null;
                str3 = str;
            } else {
                str3 = str.substring(indexOf + 3);
                str2 = str.substring(0, indexOf);
            }
            if ("papaya".equals(str2)) {
                int indexOf2 = str3.indexOf(63);
                String substring = (indexOf2 == -1 || indexOf2 == str3.length() - 1) ? null : str3.substring(indexOf2 + 1);
                String substring2 = indexOf2 != -1 ? str3.substring(0, indexOf2) : str3;
                int indexOf3 = substring2.indexOf(126);
                if (!(indexOf3 == -1 || indexOf3 == substring2.length() - 1)) {
                    str4 = substring2.substring(indexOf3 + 1);
                }
                try {
                    handlePapayaUrl(bkVar, indexOf3 != -1 ? substring2.substring(0, indexOf3) : substring2, str4, substring, url);
                } catch (Exception e) {
                    X.e(e, "failed to handle papaya url %s", substring);
                }
                return true;
            }
            if (bkVar != null) {
                if (bkVar.isLoadFromString()) {
                    return bkVar == null || !bkVar.isLoadFromString();
                }
            }
            handlePapayaUrl(bkVar, "slideno", null, str, url);
            return true;
        } catch (Exception e2) {
            X.e(e2, "Failed in shouldOverrideUrlLoading", new Object[0]);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void showLoadError() {
        C0030bb.runInHandlerThread(new Runnable() {
            public final void run() {
                bG.this.showLoadErrorInUIThread();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void showLoadErrorInUIThread() {
        Activity ownerActivity = getOwnerActivity();
        if (ownerActivity == null) {
            return;
        }
        if (this.oh) {
            new CustomDialog.Builder(ownerActivity).setTitle(C0060z.stringID("error")).setMessage(C0060z.stringID("fail_load_page")).setCancelable(false).setNegativeButton(C0060z.stringID("close"), new DialogInterface.OnClickListener() {
                public final void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).setPositiveButton(C0060z.stringID("retry"), new DialogInterface.OnClickListener() {
                public final void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    if (bG.this.oi != null) {
                        bk topWebView = bG.this.getTopWebView();
                        if (topWebView != null) {
                            topWebView.loadPapayaURL(C0034bf.createURL(bG.this.oi, topWebView.getPapayaURL()));
                        }
                        bG.this.oi = null;
                    }
                }
            }).show();
        } else {
            new CustomDialog.Builder(ownerActivity).setMessage(C0060z.stringID("fail_load_page")).setNegativeButton(C0060z.stringID("close"), new DialogInterface.OnClickListener() {
                public final void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).show();
        }
    }

    public void showLoading() {
        showLoading(null);
    }

    public void showLoading(final String str) {
        if (C0030bb.isMainThread()) {
            showLoadingInUIThread(str);
        } else {
            C0030bb.runInHandlerThread(new Runnable() {
                public final void run() {
                    bG.this.showLoadingInUIThread(str);
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void showLoadingInUIThread(String str) {
        if (str != null) {
            try {
                this.ly.getLoadingView().getTextView().setText(str);
            } catch (Exception e) {
                X.w("show loading e:%s", e.toString());
                return;
            }
        }
        this.ly.setVisibility(0);
    }

    public void showMap(int i, int i2, int i3, int i4) {
    }

    public void updateActivityTitle(bk bkVar) {
        Activity ownerActivity = getOwnerActivity();
        if (ownerActivity != null && !this.od.isEmpty()) {
            CharSequence title = this.od.getLast().getTitle();
            if (aV.isEmpty(title)) {
                title = C0056v.bj;
            }
            ownerActivity.setTitle(title);
        }
    }

    /* access modifiers changed from: protected */
    public void webViewPostProcess(bk bkVar, boolean z) {
        hideLoading();
        if (bkVar != null) {
            bG controller = bkVar.getController();
            if (controller != null) {
                C0030bb.removeFromSuperView(controller.oj);
            }
            if (z) {
                bkVar.noWarnCallJS("webloaded", "webloaded()");
                bkVar.noWarnCallJS("webappeared", "webappeared(true)");
            } else {
                bkVar.noWarnCallJS("webappeared", "webappeared(false)");
            }
            if (bkVar.getPageName() != null) {
                C0043i.trackPageView(bkVar.getPageName());
            }
        }
        C0042h.pageView();
    }
}
