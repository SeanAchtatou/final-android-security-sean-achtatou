package com.papaya.si;

import android.graphics.Bitmap;
import java.lang.ref.SoftReference;
import java.util.HashMap;

public final class aI {
    private HashMap<String, SoftReference<Bitmap>> gf;

    public aI() {
        this(10);
    }

    public aI(int i) {
        this.gf = new HashMap<>(i);
    }

    public final Bitmap get(String str) {
        SoftReference softReference = this.gf.get(str);
        if (softReference == null) {
            return null;
        }
        Bitmap bitmap = (Bitmap) softReference.get();
        if (bitmap != null && !bitmap.isRecycled()) {
            return bitmap;
        }
        this.gf.remove(str);
        return null;
    }

    public final void put(String str, Bitmap bitmap) {
        if (str != null && bitmap != null) {
            this.gf.put(str, new SoftReference(bitmap));
        }
    }

    public final void refresh() {
        aV.clearReferences(this.gf);
    }

    public final int size() {
        aV.clearReferences(this.gf);
        return this.gf.size();
    }
}
