package com.papaya.si;

public abstract class aG implements aP {
    private aO gb;

    public void fireDataStateChanged() {
        if (this.gb != null) {
            this.gb.fireDataStateChanged();
        }
    }

    public void registerMonitor(aN aNVar) {
        if (this.gb == null) {
            this.gb = new aO(this);
        }
        this.gb.registerMonitor(aNVar);
    }

    public void unregisterMonitor(aN aNVar) {
        if (this.gb != null) {
            this.gb.unregisterMonitor(aNVar);
        }
    }
}
