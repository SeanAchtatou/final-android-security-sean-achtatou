package com.papaya.si;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import java.io.Closeable;
import java.lang.reflect.Array;

/* renamed from: com.papaya.si.al  reason: case insensitive filesystem */
public class C0013al implements Closeable {
    private boolean eb;
    private String ec;
    private String ed;
    SQLiteDatabase ee;
    private int ef;

    public C0013al(String str) {
        this(str, true);
    }

    protected C0013al(String str, boolean z) {
        this.ec = str;
        if (str == null) {
            this.ee = SQLiteDatabase.create(null);
            this.eb = true;
        } else {
            this.eb = false;
            try {
                this.ee = C0037c.getApplicationContext().openOrCreateDatabase(str, 0, null);
            } catch (Exception e) {
                X.e(e, "Failed to open " + str, new Object[0]);
                aU.deleteFile(C0037c.getApplicationContext().getDatabasePath(str));
                X.i("deleted corrupted db " + str, new Object[0]);
                if (z) {
                    this.ee = C0037c.getApplicationContext().openOrCreateDatabase(str, 0, null);
                } else {
                    throw new RuntimeException(e);
                }
            }
        }
        createInitialTables();
    }

    public static String[] convertBindArgs(Object[] objArr) {
        if (objArr == null) {
            return null;
        }
        String[] strArr = new String[objArr.length];
        for (int i = 0; i < objArr.length; i++) {
            Object obj = objArr[i];
            if (obj == null) {
                X.w("null binding is not supported on Android, %s", aV.toString(objArr));
                strArr[i] = "";
            } else {
                strArr[i] = obj.toString();
            }
        }
        return strArr;
    }

    public static C0013al openFileDatabase(String str) {
        return new C0013al(str);
    }

    public static C0013al openMemoryDatabase() {
        return new C0013al(null);
    }

    public void close() {
        if (this.ee != null) {
            this.ee.close();
        }
    }

    public synchronized int countForTable(String str) {
        int i;
        i = -1;
        if (this.ee != null) {
            try {
                i = (int) this.ee.compileStatement("select count(1) from " + str).simpleQueryForLong();
            } catch (SQLException e) {
                X.e(e, "failed to execute countForTable %s", str);
            }
        }
        return i;
    }

    public void createInitialTables() {
        createTable("create table if not exists kv (key text, value text, utime integer, life integer, primary key(key))");
    }

    public synchronized boolean createTable(String str) {
        boolean z;
        if (this.ee != null) {
            try {
                this.ee.execSQL(str);
                z = true;
            } catch (SQLException e) {
                X.e(e, "Failed to create table with sql %s", str);
            }
        }
        z = false;
        return z;
    }

    public boolean deleteKv(String str) {
        return update("delete from kv where key=?", str);
    }

    public boolean dropDatabase() {
        return C0037c.getApplicationContext().deleteDatabase(this.ec);
    }

    public String getDbFilename() {
        return this.ec;
    }

    public String getDbId() {
        return this.ed;
    }

    public int getScope() {
        return this.ef;
    }

    public boolean isMemory() {
        return this.eb;
    }

    public int kvInt(String str, int i) {
        return aV.intValue(newKV(str), i);
    }

    public void kvSave(String str, String str2) {
        kvSave(str, str2, System.currentTimeMillis() / 1000, -1);
    }

    public void kvSave(String str, String str2, int i) {
        kvSave(str, str2, System.currentTimeMillis() / 1000, i);
    }

    public synchronized void kvSave(String str, String str2, long j, int i) {
        if (this.ee != null) {
            try {
                this.ee.execSQL("REPLACE INTO kv (key, value, utime, life) VALUES (?, ?, ?, ?)", new Object[]{str, str2, Long.valueOf(j), Integer.valueOf(i)});
            } catch (Exception e) {
                X.e("Failed to kvSave (%s, %s), %d, %d", str, str2, Long.valueOf(j), Integer.valueOf(i));
            }
        }
        return;
    }

    public void kvSaveInt(String str, int i, int i2) {
        kvSave(str, String.valueOf(i), System.currentTimeMillis() / 1000, i2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x003d A[SYNTHETIC, Splitter:B:18:0x003d] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x005a A[Catch:{ Exception -> 0x0042, all -> 0x0056 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x006f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.String newKV(java.lang.String r10) {
        /*
            r9 = this;
            r7 = 0
            monitor-enter(r9)
            if (r10 != 0) goto L_0x0007
            r0 = r7
        L_0x0005:
            monitor-exit(r9)
            return r0
        L_0x0007:
            android.database.sqlite.SQLiteDatabase r0 = r9.ee     // Catch:{ all -> 0x005e }
            if (r0 == 0) goto L_0x006d
            java.lang.String r0 = "SELECT value FROM kv WHERE key=? AND ( life = -1 OR (utime +life) > %d)"
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0042, all -> 0x0056 }
            r2 = 0
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0042, all -> 0x0056 }
            r5 = 1000(0x3e8, double:4.94E-321)
            long r3 = r3 / r5
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x0042, all -> 0x0056 }
            r1[r2] = r3     // Catch:{ Exception -> 0x0042, all -> 0x0056 }
            java.lang.String r0 = com.papaya.si.aV.format(r0, r1)     // Catch:{ Exception -> 0x0042, all -> 0x0056 }
            android.database.sqlite.SQLiteDatabase r1 = r9.ee     // Catch:{ Exception -> 0x0042, all -> 0x0056 }
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0042, all -> 0x0056 }
            r3 = 0
            r2[r3] = r10     // Catch:{ Exception -> 0x0042, all -> 0x0056 }
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x0042, all -> 0x0056 }
            if (r0 == 0) goto L_0x0071
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x0068, all -> 0x0061 }
            if (r1 == 0) goto L_0x0071
            r1 = 0
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x0068, all -> 0x0061 }
        L_0x003b:
            if (r0 == 0) goto L_0x006f
            r0.close()     // Catch:{ all -> 0x005e }
            r0 = r1
            goto L_0x0005
        L_0x0042:
            r0 = move-exception
            r1 = r7
        L_0x0044:
            java.lang.String r2 = "Failed to execute newKV %s"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0066 }
            r4 = 0
            r3[r4] = r10     // Catch:{ all -> 0x0066 }
            com.papaya.si.X.e(r0, r2, r3)     // Catch:{ all -> 0x0066 }
            if (r1 == 0) goto L_0x006d
            r1.close()     // Catch:{ all -> 0x005e }
            r0 = r7
            goto L_0x0005
        L_0x0056:
            r0 = move-exception
            r1 = r7
        L_0x0058:
            if (r1 == 0) goto L_0x005d
            r1.close()     // Catch:{ all -> 0x005e }
        L_0x005d:
            throw r0     // Catch:{ all -> 0x005e }
        L_0x005e:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x0061:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0058
        L_0x0066:
            r0 = move-exception
            goto L_0x0058
        L_0x0068:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0044
        L_0x006d:
            r0 = r7
            goto L_0x0005
        L_0x006f:
            r0 = r1
            goto L_0x0005
        L_0x0071:
            r1 = r7
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.papaya.si.C0013al.newKV(java.lang.String):java.lang.String");
    }

    public synchronized Object[][] newQueryResult(String str, Object[] objArr) {
        Object[][] objArr2;
        Cursor cursor;
        Object[][] objArr3;
        if (this.ee != null) {
            try {
                cursor = this.ee.rawQuery(str, convertBindArgs(objArr));
            } catch (Exception e) {
                X.e(e, "Failed to rawQuery %s: %s", str, aV.toString(objArr));
                cursor = null;
            } catch (Throwable th) {
                cursor.close();
                throw th;
            }
            if (cursor != null) {
                try {
                    int columnCount = cursor.getColumnCount();
                    objArr2 = (Object[][]) Array.newInstance(Object.class, cursor.getCount(), columnCount);
                    int i = 0;
                    while (cursor.moveToNext()) {
                        try {
                            for (int i2 = 0; i2 < columnCount; i2++) {
                                objArr2[i][i2] = cursor.getString(i2);
                            }
                            i++;
                        } catch (Exception e2) {
                            Exception exc = e2;
                            objArr3 = objArr2;
                            e = exc;
                            X.e(e, "Failed to execute newQueryResult", new Object[0]);
                            cursor.close();
                            objArr2 = objArr3;
                            return objArr2;
                        }
                    }
                    cursor.close();
                } catch (Exception e3) {
                    e = e3;
                    objArr3 = null;
                    X.e(e, "Failed to execute newQueryResult", new Object[0]);
                    cursor.close();
                    objArr2 = objArr3;
                    return objArr2;
                }
            }
        }
        objArr2 = null;
        return objArr2;
    }

    public void setDbId(String str) {
        this.ed = str;
    }

    public void setScope(int i) {
        this.ef = i;
    }

    public void setSynchronousLevel(int i) {
        update("PRAGMA synchronous=" + i, new Object[0]);
    }

    public synchronized boolean update(String str, Object... objArr) {
        boolean z;
        Object[] objArr2;
        Object[] objArr3;
        SQLException e;
        if (this.ee != null) {
            if (objArr == null) {
                try {
                    objArr2 = new Object[0];
                } catch (SQLException e2) {
                    e = e2;
                    objArr3 = objArr;
                    X.e(e, "failed to update %s, args %s", str, aV.toString(objArr3));
                    z = false;
                    return z;
                }
            } else {
                objArr2 = objArr;
            }
            try {
                this.ee.execSQL(str, objArr2);
                z = true;
            } catch (SQLException e3) {
                SQLException sQLException = e3;
                objArr3 = objArr2;
                e = sQLException;
                X.e(e, "failed to update %s, args %s", str, aV.toString(objArr3));
                z = false;
                return z;
            }
        }
        z = false;
        return z;
    }
}
