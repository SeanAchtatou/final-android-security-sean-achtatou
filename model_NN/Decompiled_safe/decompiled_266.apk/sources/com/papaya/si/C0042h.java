package com.papaya.si;

import android.app.Application;
import android.content.Context;
import java.util.HashMap;

/* renamed from: com.papaya.si.h  reason: case insensitive filesystem */
public final class C0042h {
    private C0049o aa;

    private C0042h() {
    }

    public C0042h(C0049o oVar) {
        this.aa = oVar;
    }

    public static void endSession(Context context) {
    }

    public static void initialize(Application application) {
    }

    public static void onEvent(String str, HashMap<String, String> hashMap) {
    }

    public static void pageView() {
    }

    public static void startSession(Context context) {
    }

    public final void pipelineModeChanged(boolean z) {
        if (z) {
            this.aa.aj = 30;
        } else {
            this.aa.aj = 1;
        }
    }

    public final void requestSent() {
        C0050p removeNextEvent;
        if (this.aa.al != null && (removeNextEvent = this.aa.al.removeNextEvent()) != null) {
            this.aa.am.eventDispatched(removeNextEvent.an);
        }
    }

    public final void serverError(int i) {
        this.aa.W = i;
    }
}
