package com.papaya.si;

import com.adknowledge.superrewards.model.SROffer;
import com.papaya.achievement.PPYAchievement;
import com.papaya.achievement.PPYAchievementDelegate;
import com.papaya.si.bt;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: com.papaya.si.az  reason: case insensitive filesystem */
public final class C0027az extends bv implements aW, bt.a {
    private PPYAchievementDelegate fO;
    private PPYAchievement fP;

    public C0027az(PPYAchievement pPYAchievement, PPYAchievementDelegate pPYAchievementDelegate) {
        this.fP = pPYAchievement;
        this.fO = pPYAchievementDelegate;
        setCacheable(false);
        setDispatchable(true);
        setRequireSid(true);
        setDelegate(this);
    }

    public C0027az(PPYAchievementDelegate pPYAchievementDelegate) {
        this.fO = pPYAchievementDelegate;
        setCacheable(false);
        setDispatchable(true);
        setRequireSid(true);
        setDelegate(this);
    }

    public final void connectionFailed(bt btVar, int i) {
        if (this.fO != null) {
            this.fO.onListFailed();
        }
    }

    public final void connectionFinished(bt btVar) {
        try {
            if (this.fO != null) {
                String url = btVar.getRequest().getUrl().toString();
                if (url.contains("json_achievementlist")) {
                    C0023av.getInstance().getAchievementDatabase().clearAchievements();
                    JSONArray jSONArray = C0034bf.parseJsonObject(aV.utf8String(btVar.getData(), null)).getJSONArray("items");
                    ArrayList arrayList = new ArrayList();
                    for (int i = 0; i < jSONArray.length(); i++) {
                        JSONObject jSONObject = jSONArray.getJSONObject(i);
                        arrayList.add(new PPYAchievement(jSONObject.getInt("aid"), jSONObject.getString(SROffer.TITLE), jSONObject.getString("desc"), jSONObject.getInt("secret") != 0, jSONObject.getInt("unlock") != 0));
                    }
                    this.fO.onListSuccess(arrayList);
                } else if (url.contains("json_loadachievement")) {
                    JSONObject parseJsonObject = C0034bf.parseJsonObject(aV.utf8String(btVar.getData(), null));
                    this.fO.onLoadSuccess(new PPYAchievement(parseJsonObject.getInt("aid"), parseJsonObject.getString(SROffer.TITLE), parseJsonObject.getString("desc"), parseJsonObject.getInt("secret") != 0, parseJsonObject.getInt("unlock") != 0));
                } else if (this.fP == null) {
                } else {
                    if (url.contains("achievementicon")) {
                        this.fO.onDownloadIconSuccess(btVar.getBitmap());
                        C0001a.getWebCache().saveCacheWebFile("achievementicon?id=" + this.fP.getId(), btVar.getData());
                    } else if (url.contains("json_unlock")) {
                        C0023av.getInstance().getAchievementDatabase().deleteAchievement(this.fP.getId());
                        this.fO.onUnlockSuccess(Boolean.valueOf(C0034bf.parseJsonObject(aV.utf8String(btVar.getData(), null)).getInt("ret") != 0));
                    }
                }
            }
        } catch (Exception e) {
            X.e(e, "Failed in PPYUrlAchievementRequest", new Object[0]);
        }
    }

    public final void downloadicon() {
        if (this.fP != null) {
            this.url = C0034bf.createURL("achievementicon?id=" + this.fP.getId());
            start(true);
        }
    }

    public final void getAchievementList() {
        this.url = C0034bf.createURL("json_achievementlist?all=1&unlock=" + C0023av.getInstance().getAchievementDatabase().stringList());
        start(true);
    }

    public final void loadAchievement(int i) {
        this.url = C0034bf.createURL("json_loadachievement?aid=" + i);
        start(true);
    }

    public final void unlock() {
        if (this.fP != null) {
            C0023av.getInstance().getAchievementDatabase().addAchievement(this.fP.getId());
            this.url = C0034bf.createURL("json_unlock?aid=" + this.fP.getId());
            start(true);
        }
    }
}
