package com.papaya.si;

import java.lang.ref.WeakReference;

/* renamed from: com.papaya.si.be  reason: case insensitive filesystem */
public class C0033be<T> {
    private WeakReference<T> hp;

    public T getDelegate() {
        if (this.hp == null) {
            return null;
        }
        return this.hp.get();
    }

    public void setDelegate(T t) {
        if (t == null) {
            this.hp = null;
        } else {
            this.hp = new WeakReference<>(t);
        }
    }
}
