package com.papaya.si;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.papaya.purchase.PPYPayment;
import com.papaya.purchase.PPYPaymentDelegate;
import com.papaya.social.PPYSession;
import com.papaya.social.PPYSocialQuery;
import com.papaya.view.LazyImageView;
import com.papaya.view.OverlayCustomDialog;
import com.papaya.view.UserImageView;
import org.json.JSONObject;

public final class aB extends OverlayCustomDialog implements PPYPaymentDelegate, PPYSocialQuery.QueryDelegate, OverlayCustomDialog.OnClickListener {
    private PPYPayment fQ;
    private UserImageView fS;
    private TextView fT;
    private TextView fU;
    private int fV = -1;
    private ImageView fW;
    private LinearLayout fX;

    public aB(Context context, PPYPayment pPYPayment) {
        super(context);
        this.fQ = pPYPayment;
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setOrientation(0);
        linearLayout.addView(linearLayout2, new LinearLayout.LayoutParams(-1, -2));
        LazyImageView lazyImageView = new LazyImageView(context);
        lazyImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        if (pPYPayment.getIconID() > 0) {
            lazyImageView.setImageResource(pPYPayment.getIconID());
        } else if (pPYPayment.getIconDrawable() != null) {
            lazyImageView.setImageDrawable(pPYPayment.getIconDrawable());
        } else if (pPYPayment.getIconURL() != null) {
            lazyImageView.setImageUrl(pPYPayment.getIconURL());
        } else {
            lazyImageView.setVisibility(8);
        }
        linearLayout2.addView(lazyImageView, new LinearLayout.LayoutParams(C0030bb.rp(45), C0030bb.rp(45)));
        LinearLayout linearLayout3 = new LinearLayout(context);
        linearLayout3.setOrientation(1);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.leftMargin = C0030bb.rp(5);
        linearLayout2.addView(linearLayout3, layoutParams);
        TextView textView = new TextView(context);
        textView.setText(pPYPayment.getName());
        textView.setTextColor(Color.parseColor("#eca337"));
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setTextSize(16.0f);
        linearLayout3.addView(textView, new LinearLayout.LayoutParams(-2, -2));
        ScrollView scrollView = new ScrollView(context);
        linearLayout3.addView(scrollView, new LinearLayout.LayoutParams(-2, -2));
        TextView textView2 = new TextView(context);
        textView2.setText(pPYPayment.getDescription());
        textView2.setTextColor(-12303292);
        scrollView.addView(textView2);
        this.fW = new ImageView(context);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, C0030bb.rp(1));
        layoutParams2.topMargin = C0030bb.rp(2);
        layoutParams2.bottomMargin = C0030bb.rp(5);
        this.fW.setBackgroundResource(C0060z.drawableID("line"));
        linearLayout.addView(this.fW, layoutParams2);
        this.fX = new LinearLayout(context);
        this.fX.setOrientation(0);
        linearLayout.addView(this.fX, new LinearLayout.LayoutParams(-1, -2));
        this.fS = new UserImageView(context);
        this.fX.addView(this.fS, new LinearLayout.LayoutParams(C0030bb.rp(45), C0030bb.rp(45)));
        LinearLayout linearLayout4 = new LinearLayout(context);
        linearLayout4.setOrientation(1);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -1);
        layoutParams3.leftMargin = C0030bb.rp(5);
        this.fX.addView(linearLayout4, layoutParams3);
        this.fT = new TextView(context);
        this.fT.setTextColor(-16777216);
        this.fT.setTypeface(Typeface.DEFAULT_BOLD);
        this.fT.setEllipsize(TextUtils.TruncateAt.MIDDLE);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams4.weight = 1.0f;
        linearLayout4.addView(this.fT, layoutParams4);
        this.fU = new TextView(context);
        this.fU.setTextColor(-16777216);
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams5.weight = 1.0f;
        linearLayout4.addView(this.fU, layoutParams5);
        setView(linearLayout);
        setIcon(C0060z.drawableID("papaya"));
        setTitle(pPYPayment.getPapayas() + " " + C0037c.getString("papayas"));
        setButton(-2, C0037c.getString("close"), this);
        setButton(-3, C0030bb.prefixWithDrawable(C0037c.getDrawable("morepapayas"), " " + C0037c.getString("button_more"), C0030bb.rp(16), C0030bb.rp(16)), this);
        setButton(-1, C0030bb.prefixWithDrawable(C0037c.getDrawable("purchase"), " " + C0037c.getString("button_purchase"), C0030bb.rp(16), C0030bb.rp(16)), this);
        this.fW.setVisibility(8);
        this.fX.setVisibility(8);
        C0023av.getInstance().submitQuery(new PPYSocialQuery("papayas", this));
    }

    private void hideCallDelegate() {
        hide();
        PPYPaymentDelegate delegate = this.fQ.getDelegate();
        if (delegate != null) {
            delegate.onPaymentClosed(this.fQ);
        }
    }

    private void showMorePapayasTips() {
        C0030bb.showToast(C0037c.getString("tip_get_more_papayas"), 0);
    }

    /* access modifiers changed from: protected */
    public final int getDefaultVisibility() {
        return 0;
    }

    public final void onClick(OverlayCustomDialog overlayCustomDialog, int i) {
        if (i == -1) {
            if (this.fV < 0 || this.fV >= this.fQ.getPapayas()) {
                showLoading();
                new aA(this.fQ, this).start(true);
                return;
            }
            showMorePapayasTips();
        } else if (i == -3) {
            hideCallDelegate();
            getContext();
            C0030bb.post(new Runnable() {
                public final void run() {
                    C0028b.openPRIALink(null, "static_more");
                }
            });
        } else if (i == -2) {
            hideCallDelegate();
        }
    }

    public final void onPaymentClosed(PPYPayment pPYPayment) {
    }

    public final void onPaymentFailed(PPYPayment pPYPayment, int i, String str) {
        hideLoading();
        hide();
        PPYPaymentDelegate delegate = pPYPayment.getDelegate();
        if (delegate != null) {
            delegate.onPaymentFailed(pPYPayment, i, str);
        }
        X.w("onPaymentFailed: %d, %s", Integer.valueOf(i), str);
    }

    public final void onPaymentFinished(PPYPayment pPYPayment) {
        hideLoading();
        hide();
        PPYPaymentDelegate delegate = pPYPayment.getDelegate();
        if (delegate != null) {
            delegate.onPaymentFinished(pPYPayment);
        }
    }

    public final void onQueryFailed(PPYSocialQuery pPYSocialQuery, String str) {
    }

    public final void onQueryResponse(PPYSocialQuery pPYSocialQuery, JSONObject jSONObject) {
        this.fW.setVisibility(0);
        this.fX.setVisibility(0);
        this.fS.setImageUrl(aV.format("getavatarhead?uid=%d", Integer.valueOf(PPYSession.getInstance().getUID())));
        this.fT.setText(C0025ax.getInstance().getNickname());
        this.fV = jSONObject.optInt("papayas", 0);
        this.fU.setText(this.fV + " " + C0037c.getString("text_papayas_left"));
        if (this.fV < this.fQ.getPapayas()) {
            showMorePapayasTips();
        }
    }
}
