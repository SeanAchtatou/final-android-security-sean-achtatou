package com.papaya.si;

import java.lang.reflect.Method;
import org.json.JSONException;
import org.json.JSONObject;

public final class bx {
    private static final Object[] mQ = new Object[0];

    public static String capitalize(String str) {
        return !aV.isEmpty(str) ? Character.isUpperCase(str.charAt(0)) ? str : str.substring(0, 1).toUpperCase() + str.substring(1) : str;
    }

    public static Object getProperty(Object obj, String str) {
        if (obj == null || str == null) {
            return null;
        }
        try {
            String[] split = str.split("\\.");
            Object obj2 = null;
            for (int i = 0; i < split.length; i++) {
                String str2 = split[i];
                if (i == 0) {
                    obj2 = obj;
                }
                obj2 = getSimpleProperty(obj2, str2);
                if (obj2 == null) {
                    return null;
                }
            }
            return obj2;
        } catch (Exception e) {
            X.w(e, "Failed to get property %s from %s", str, obj);
            return null;
        }
    }

    public static Object getSimpleProperty(Object obj, String str) {
        if (obj == null || str == null) {
            return null;
        }
        try {
            String str2 = "get" + capitalize(str);
            Method[] methods = obj.getClass().getMethods();
            for (Method method : methods) {
                if (method.getName().equals(str2)) {
                    return method.invoke(obj, mQ);
                }
            }
        } catch (Exception e) {
            X.w(e, "Fauled to get simple property %s from %s", str, obj);
        }
        return null;
    }

    public static void setProperty(Object obj, String str, int i) {
        try {
            setProperty(obj, str, JSONObject.numberToString(Integer.valueOf(i)));
        } catch (JSONException e) {
            X.w(e, "Failed to set int property %s, %s, %d", obj, str, Integer.valueOf(i));
        }
    }

    public static void setProperty(Object obj, String str, String str2) {
        if (obj != null && str != null) {
            try {
                String[] split = str.split("\\.");
                Object obj2 = obj;
                for (int i = 0; i < split.length; i++) {
                    String str3 = split[i];
                    if (i < split.length - 1) {
                        obj2 = getSimpleProperty(obj2, str3);
                        if (obj2 == null) {
                            return;
                        }
                    } else {
                        setSimpleProperty(obj2, str3, str2);
                    }
                }
            } catch (Exception e) {
                X.w(e, "Failed to setProperty %s, %s, %s", obj, str, str2);
            }
        }
    }

    public static void setSimpleProperty(Object obj, String str, String str2) {
        if (obj != null && str != null) {
            try {
                String str3 = "set" + capitalize(str);
                Method[] methods = obj.getClass().getMethods();
                int i = 0;
                while (i < methods.length) {
                    Method method = methods[i];
                    if (method.getName().equals(str3)) {
                        try {
                            method.invoke(obj, C0034bf.parseJsonAsArray(str2));
                            return;
                        } catch (Exception e) {
                        }
                    } else {
                        i++;
                    }
                }
            } catch (Exception e2) {
                X.w(e2, "Faile to setSimpleProperty %s, %s, %s", obj, str, str2);
            }
        }
    }
}
