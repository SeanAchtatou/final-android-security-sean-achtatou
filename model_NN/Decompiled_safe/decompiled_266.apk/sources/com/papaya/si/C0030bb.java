package com.papaya.si;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsoluteLayout;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.papaya.view.PPYAbsoluteLayout;
import com.papaya.view.ViewControl;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/* renamed from: com.papaya.si.bb  reason: case insensitive filesystem */
public final class C0030bb {
    private static int gX = 0;
    private static aQ gY;
    private static WeakReference<Thread> gZ;
    private static File ha = null;
    private static Uri hb = null;
    private static BitmapFactory.Options hc;
    private static boolean u = false;

    private C0030bb() {
    }

    public static void addView(ViewGroup viewGroup, View view) {
        addView(viewGroup, view, false);
    }

    public static void addView(ViewGroup viewGroup, View view, boolean z) {
        if (viewGroup != null && view != null) {
            try {
                if (view.getParent() == null) {
                    viewGroup.addView(view);
                } else if (!z) {
                } else {
                    if (view.getParent() != viewGroup) {
                        removeFromSuperView(view);
                        viewGroup.addView(view);
                    }
                }
            } catch (Exception e) {
                X.e(e, "Failed to addView, parent %s, view %s, force %b, layoutParams %s", viewGroup, view, Boolean.valueOf(z), view.getLayoutParams());
            }
        }
    }

    public static void assertMainThread() {
        if (!isMainThread()) {
            X.e(new Exception(), "UI thread assertion failed", new Object[0]);
        }
    }

    public static synchronized Bitmap bitmapFromFD(aK aKVar) {
        InputStream inputStream;
        Bitmap bitmap;
        synchronized (C0030bb.class) {
            if (aKVar == null) {
                bitmap = null;
            } else {
                try {
                    if (hc == null) {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        hc = options;
                        options.inTempStorage = new byte[16384];
                    }
                    InputStream openInput = aKVar.openInput();
                    try {
                        Bitmap decodeStream = BitmapFactory.decodeStream(openInput, null, hc);
                        aU.close(openInput);
                        bitmap = decodeStream;
                    } catch (Exception e) {
                        Exception exc = e;
                        inputStream = openInput;
                        e = exc;
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        inputStream = openInput;
                        th = th2;
                        aU.close(inputStream);
                        throw th;
                    }
                } catch (Exception e2) {
                    e = e2;
                    inputStream = null;
                    try {
                        X.w(e, "Failed to get bitmapFromFD %s", aKVar);
                        aU.close(inputStream);
                        bitmap = null;
                        return bitmap;
                    } catch (Throwable th3) {
                        th = th3;
                        aU.close(inputStream);
                        throw th;
                    }
                } catch (Throwable th4) {
                    th = th4;
                    inputStream = null;
                    aU.close(inputStream);
                    throw th;
                }
            }
        }
        return bitmap;
    }

    public static synchronized Bitmap bitmapFromFD(aK aKVar, Bitmap.Config config) {
        InputStream inputStream;
        Bitmap bitmap;
        synchronized (C0030bb.class) {
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = config;
                inputStream = aKVar.openInput();
                try {
                    bitmap = BitmapFactory.decodeStream(inputStream, null, options);
                    if (!(bitmap.getConfig() == config || config == Bitmap.Config.ARGB_8888)) {
                        Bitmap copy = bitmap.copy(config, false);
                        bitmap.recycle();
                        bitmap = copy;
                    }
                    aU.close(inputStream);
                } catch (Exception e) {
                    e = e;
                    try {
                        X.w(e, "Failed to get bitmapFromFD %s", aKVar);
                        aU.close(inputStream);
                        bitmap = null;
                        return bitmap;
                    } catch (Throwable th) {
                        th = th;
                        aU.close(inputStream);
                        throw th;
                    }
                }
            } catch (Exception e2) {
                e = e2;
                inputStream = null;
                X.w(e, "Failed to get bitmapFromFD %s", aKVar);
                aU.close(inputStream);
                bitmap = null;
                return bitmap;
            } catch (Throwable th2) {
                th = th2;
                inputStream = null;
                aU.close(inputStream);
                throw th;
            }
        }
        return bitmap;
    }

    public static synchronized Bitmap bitmapFromFD$6caae880(C0001a aVar, AssetManager assetManager) {
        Bitmap bitmap;
        InputStream inputStream;
        synchronized (C0030bb.class) {
            if (aVar == null || assetManager == null) {
                bitmap = null;
            } else {
                try {
                    if (hc == null) {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        hc = options;
                        options.inTempStorage = new byte[16384];
                    }
                    InputStream openInput = aVar.openInput(assetManager);
                    try {
                        Bitmap decodeStream = BitmapFactory.decodeStream(openInput, null, hc);
                        aU.close(openInput);
                        bitmap = decodeStream;
                    } catch (Exception e) {
                        Exception exc = e;
                        inputStream = openInput;
                        e = exc;
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        inputStream = openInput;
                        th = th2;
                        aU.close(inputStream);
                        throw th;
                    }
                } catch (Exception e2) {
                    e = e2;
                    inputStream = null;
                    try {
                        X.w(e, "Failed to get bitmapFromFD %s", aVar);
                        aU.close(inputStream);
                        bitmap = null;
                        return bitmap;
                    } catch (Throwable th3) {
                        th = th3;
                        aU.close(inputStream);
                        throw th;
                    }
                } catch (Throwable th4) {
                    th = th4;
                    inputStream = null;
                    aU.close(inputStream);
                    throw th;
                }
            }
        }
        return bitmap;
    }

    public static <T> T callInHandlerThread(Callable<T> callable, T t) {
        return gY != null ? gY.callInHandlerThread(callable, t) : t;
    }

    public static int color(int i) {
        return Color.rgb(i >> 16, i >> 8, i & 255);
    }

    public static byte[] compressBitmap(Bitmap bitmap, Bitmap.CompressFormat compressFormat, int i) {
        ByteArrayOutputStream byteArrayOutputStream;
        if (bitmap == null) {
            return null;
        }
        try {
            ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
            try {
                bitmap.compress(compressFormat, i, byteArrayOutputStream2);
                byte[] byteArray = byteArrayOutputStream2.toByteArray();
                aU.close(byteArrayOutputStream2);
                return byteArray;
            } catch (Exception e) {
                Exception exc = e;
                byteArrayOutputStream = byteArrayOutputStream2;
                e = exc;
                try {
                    X.e(e, "Failed to compressBitmap, %s, %d", compressFormat, Integer.valueOf(i));
                    aU.close(byteArrayOutputStream);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    aU.close(byteArrayOutputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                Throwable th3 = th2;
                byteArrayOutputStream = byteArrayOutputStream2;
                th = th3;
                aU.close(byteArrayOutputStream);
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            byteArrayOutputStream = null;
            X.e(e, "Failed to compressBitmap, %s, %d", compressFormat, Integer.valueOf(i));
            aU.close(byteArrayOutputStream);
            return null;
        } catch (Throwable th4) {
            th = th4;
            byteArrayOutputStream = null;
            aU.close(byteArrayOutputStream);
            throw th;
        }
    }

    public static Activity contextAsActivity(Context context) {
        if (context instanceof Activity) {
            return (Activity) context;
        }
        return null;
    }

    private static WindowManager crackWindowManager() {
        try {
            return (WindowManager) Class.forName("android.view.WindowManagerImpl").getDeclaredMethod("getDefault", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception e) {
            X.i(e, "Failed to find WindowManagerImpl", new Object[0]);
            return null;
        }
    }

    public static Bitmap createScaledBitmap(ContentResolver contentResolver, Uri uri, int i, int i2, boolean z) {
        InputStream inputStream;
        InputStream inputStream2;
        Exception exc;
        InputStream inputStream3;
        double d;
        double d2;
        int i3;
        InputStream inputStream4;
        InputStream inputStream5;
        double d3 = 0.0d;
        try {
            inputStream3 = contentResolver.openInputStream(uri);
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(inputStream3, null, options);
                d3 = (double) options.outWidth;
                double d4 = (double) options.outHeight;
                aU.close(inputStream3);
                double d5 = d4;
                d2 = d3;
                d = d5;
            } catch (Exception e) {
                Exception exc2 = e;
                inputStream2 = inputStream3;
                exc = exc2;
                try {
                    X.e(exc, "Failed to createScaledBitmap, get outWidth/Height %s, %d, %d", uri, Integer.valueOf(i), Integer.valueOf(i2));
                    aU.close(inputStream2);
                    inputStream3 = inputStream2;
                    double d6 = d3;
                    d = 0.0d;
                    d2 = d6;
                    if (d2 != 0.0d) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    inputStream = inputStream2;
                    aU.close(inputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                inputStream = inputStream3;
                aU.close(inputStream);
                throw th;
            }
        } catch (Exception e2) {
            Exception exc3 = e2;
            inputStream2 = null;
            exc = exc3;
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
            aU.close(inputStream);
            throw th;
        }
        if (d2 != 0.0d || d == 0.0d) {
            return null;
        }
        if (z && (d2 - d) * ((double) (i - i2)) < 0.0d) {
            double d7 = d2;
            d2 = d;
            d = d7;
        }
        if (d2 > ((double) i)) {
            i3 = (int) Math.ceil(d2 / ((double) i));
            d /= (double) i3;
        } else {
            i3 = 1;
        }
        if (d > ((double) i2) * 1.1d) {
            i3 += (int) Math.ceil(d / ((double) i2));
        }
        try {
            InputStream openInputStream = contentResolver.openInputStream(uri);
            try {
                BitmapFactory.Options options2 = new BitmapFactory.Options();
                options2.inSampleSize = i3;
                Bitmap decodeStream = BitmapFactory.decodeStream(openInputStream, null, options2);
                aU.close(openInputStream);
                return decodeStream;
            } catch (Exception e3) {
                Exception exc4 = e3;
                inputStream5 = openInputStream;
                e = exc4;
                try {
                    X.e(e, "Failed to createScaledBitmap, %s, %d, %d", uri, Integer.valueOf(i), Integer.valueOf(i2));
                    aU.close(inputStream5);
                    return null;
                } catch (Throwable th4) {
                    th = th4;
                    inputStream4 = inputStream5;
                    aU.close(inputStream4);
                    throw th;
                }
            } catch (Throwable th5) {
                Throwable th6 = th5;
                inputStream4 = openInputStream;
                th = th6;
                aU.close(inputStream4);
                throw th;
            }
        } catch (Exception e4) {
            e = e4;
            inputStream5 = inputStream3;
        } catch (Throwable th7) {
            th = th7;
            inputStream4 = inputStream3;
            aU.close(inputStream4);
            throw th;
        }
    }

    public static Bitmap createScaledBitmap(Bitmap bitmap, int i, int i2) {
        int i3;
        int i4;
        if (bitmap.getWidth() <= i && bitmap.getHeight() <= i2) {
            return bitmap;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (bitmap.getWidth() > i) {
            i4 = (int) ((((double) bitmap.getHeight()) * (((double) i) + 0.0d)) / ((double) bitmap.getWidth()));
            i3 = i;
        } else {
            int i5 = height;
            i3 = width;
            i4 = i5;
        }
        if (i4 > i2) {
            i3 = (int) ((((double) i3) * (((double) i2) + 0.0d)) / ((double) i4));
            i4 = i2;
        }
        return Bitmap.createScaledBitmap(bitmap, i3, i4, true);
    }

    public static void debugFocus(String str, View view) {
        if (view != null) {
            X.i("%s %s, visibility %d, focusableInTouchMode %b, focused %b", str, view, Integer.valueOf(view.getVisibility()), Boolean.valueOf(view.isFocusableInTouchMode()), Boolean.valueOf(view.isFocused()));
        }
    }

    public static void destroy() {
        gY = null;
        u = false;
    }

    public static Drawable drawableFromFD(aK aKVar) {
        InputStream inputStream;
        if (aKVar == null) {
            return null;
        }
        try {
            InputStream openInput = aKVar.openInput();
            try {
                Drawable createFromStream = Drawable.createFromStream(openInput, "");
                if (createFromStream == null) {
                    X.w("drawable from %s is null???", aKVar);
                }
                aU.close(openInput);
                return createFromStream;
            } catch (Exception e) {
                Exception exc = e;
                inputStream = openInput;
                e = exc;
                try {
                    X.w(e, "Failed to get drawableFromFD %s", aKVar);
                    aU.close(inputStream);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    aU.close(inputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                Throwable th3 = th2;
                inputStream = openInput;
                th = th3;
                aU.close(inputStream);
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            inputStream = null;
            X.w(e, "Failed to get drawableFromFD %s", aKVar);
            aU.close(inputStream);
            return null;
        } catch (Throwable th4) {
            th = th4;
            inputStream = null;
            aU.close(inputStream);
            throw th;
        }
    }

    public static <T> T find(Activity activity, String str) {
        return activity.findViewById(C0060z.id(str));
    }

    public static <T> T find(View view, String str) {
        return view.findViewById(C0060z.id(str));
    }

    public static void forceClearBitmap() {
        System.gc();
        System.runFinalization();
        System.gc();
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0022 A[SYNTHETIC, Splitter:B:11:0x0022] */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap getCameraBitmap(android.app.Activity r6, android.content.Intent r7, int r8, int r9, boolean r10) {
        /*
            r4 = 0
            r3 = 0
            java.io.File r0 = com.papaya.si.C0030bb.ha     // Catch:{ Exception -> 0x0038 }
            if (r0 == 0) goto L_0x0076
            boolean r1 = r0.exists()     // Catch:{ Exception -> 0x0038 }
            if (r1 == 0) goto L_0x0076
            android.content.ContentResolver r1 = r6.getContentResolver()     // Catch:{ Exception -> 0x0038 }
            android.net.Uri r2 = com.papaya.si.C0030bb.hb     // Catch:{ Exception -> 0x0038 }
            android.graphics.Bitmap r1 = createScaledBitmap(r1, r2, r8, r9, r10)     // Catch:{ Exception -> 0x0038 }
            r0.delete()     // Catch:{ Exception -> 0x0074 }
            r0 = 0
            com.papaya.si.C0030bb.ha = r0     // Catch:{ Exception -> 0x0074 }
            r0 = 0
            com.papaya.si.C0030bb.hb = r0     // Catch:{ Exception -> 0x0074 }
        L_0x001f:
            r0 = r1
        L_0x0020:
            if (r0 != 0) goto L_0x0037
            android.os.Bundle r1 = r7.getExtras()     // Catch:{ Exception -> 0x0053 }
            java.lang.String r2 = "data"
            java.lang.Object r6 = r1.get(r2)     // Catch:{ Exception -> 0x0053 }
            android.graphics.Bitmap r6 = (android.graphics.Bitmap) r6     // Catch:{ Exception -> 0x0053 }
            android.graphics.Bitmap r0 = createScaledBitmap(r6, r8, r9)     // Catch:{ Exception -> 0x0071 }
            if (r0 == r6) goto L_0x0037
            r6.recycle()     // Catch:{ Exception -> 0x0071 }
        L_0x0037:
            return r0
        L_0x0038:
            r0 = move-exception
            r1 = r3
        L_0x003a:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Failed to get camera picture from TMP_PHOTO: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            java.lang.Object[] r2 = new java.lang.Object[r4]
            com.papaya.si.X.w(r0, r2)
            goto L_0x001f
        L_0x0053:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0057:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Failed to get camera picture from extras: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            java.lang.Object[] r2 = new java.lang.Object[r4]
            com.papaya.si.X.w(r0, r2)
            r0 = r1
            goto L_0x0037
        L_0x0071:
            r0 = move-exception
            r1 = r6
            goto L_0x0057
        L_0x0074:
            r0 = move-exception
            goto L_0x003a
        L_0x0076:
            r0 = r3
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.papaya.si.C0030bb.getCameraBitmap(android.app.Activity, android.content.Intent, int, int, boolean):android.graphics.Bitmap");
    }

    public static String getDeviceID(Context context) {
        String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
        return (string == null || string.length() == 0) ? "emulator" : string;
    }

    public static int getOrientation(Context context) {
        return (context == null ? C0037c.getApplicationContext() : context).getResources().getConfiguration().orientation;
    }

    public static WindowManager getWindowManager(Context context) {
        WindowManager crackWindowManager = crackWindowManager();
        if (crackWindowManager != null || context == null) {
            return crackWindowManager;
        }
        try {
            return (WindowManager) context.getSystemService("window");
        } catch (Exception e) {
            X.w("Failed to get WindowManager", new Object[0]);
            return crackWindowManager;
        }
    }

    public static void hide(Object obj, boolean z) {
        if (obj != null) {
            if (obj instanceof ViewControl) {
                ((ViewControl) obj).hide(z);
            } else if (obj instanceof Dialog) {
                ((Dialog) obj).hide();
            } else {
                X.w("Can't hide for unknown view %s", obj);
            }
        }
    }

    public static void initialize() {
        if (!u) {
            gY = new aQ();
            gZ = new WeakReference<>(Thread.currentThread());
            u = true;
        }
    }

    public static boolean isEmulator(Context context) {
        return getDeviceID(context).equals("emulator") || "sdk".equals(Build.MODEL) || "google_sdk".equals(Build.MODEL);
    }

    public static boolean isMainThread() {
        if (!u) {
            return true;
        }
        if (gZ != null) {
            return gZ.get() == Thread.currentThread();
        }
        return false;
    }

    public static AnimationSet makeSlideAnimation(int i) {
        TranslateAnimation translateAnimation;
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(new AlphaAnimation(0.0f, 1.0f));
        switch (i) {
            case 3:
                translateAnimation = new TranslateAnimation(1, -1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                break;
            case 5:
                translateAnimation = new TranslateAnimation(1, 1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                break;
            case 48:
                translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
                break;
            case 80:
                translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
                break;
            default:
                translateAnimation = null;
                break;
        }
        if (translateAnimation != null) {
            animationSet.addAnimation(translateAnimation);
        }
        animationSet.setInterpolator(new AccelerateDecelerateInterpolator());
        return animationSet;
    }

    public static boolean openExternalUri(Context context, String str) {
        String str2;
        String str3;
        try {
            if (aV.isEmpty(str)) {
                return false;
            }
            if ("amazon".equals(C0056v.bn)) {
                if (str.contains("/mr?")) {
                    str3 = str + "&_l=amazon";
                } else if (str.startsWith("market://details?id=")) {
                    str3 = "http://papayamobile.com/a/mr?_l=amazon&p=" + str.substring("market://details?id=".length());
                }
                X.d("external uri: [%s]", str3);
                C0028b.startActivity(context, new Intent("android.intent.action.VIEW", Uri.parse(str3)));
                return true;
            }
            str3 = str;
            try {
                X.d("external uri: [%s]", str3);
                C0028b.startActivity(context, new Intent("android.intent.action.VIEW", Uri.parse(str3)));
                return true;
            } catch (Exception e) {
                Exception exc = e;
                str2 = str3;
                e = exc;
                X.e(e, "Failed to open external uri: " + str2, new Object[0]);
                return false;
            }
        } catch (Exception e2) {
            e = e2;
            str2 = str;
            X.e(e, "Failed to open external uri: " + str2, new Object[0]);
            return false;
        }
    }

    public static void post(Runnable runnable) {
        if (gY != null) {
            gY.post(runnable);
        }
    }

    public static <T> Future<T> postCallable(Callable<T> callable) {
        if (gY != null) {
            return gY.postCallable(callable);
        }
        return null;
    }

    public static void postDelayed(Runnable runnable, long j) {
        if (gY != null) {
            gY.postDelayed(runnable, j);
        }
    }

    public static CharSequence prefixWithDrawable(Drawable drawable, String str, int i, int i2) {
        if (drawable == null) {
            X.w("drawable is null?", new Object[0]);
            return str;
        }
        SpannableString spannableString = new SpannableString(" " + (str == null ? "" : str));
        drawable.setBounds(0, 0, i, i2);
        spannableString.setSpan(new ImageSpan(drawable, 1), 0, 1, 33);
        return spannableString;
    }

    public static AbsoluteLayout.LayoutParams rawAbsoluteLayoutParams(Context context, int i, int i2, int i3, int i4) {
        return new AbsoluteLayout.LayoutParams(rp(context, i), rp(context, i2), rp(context, i3), rp(context, i4));
    }

    public static PPYAbsoluteLayout.LayoutParams rawPPYAbsoluteLayoutParams(Context context, int i, int i2, int i3, int i4) {
        return new PPYAbsoluteLayout.LayoutParams(rp(context, i), rp(context, i2), rp(context, i3), rp(context, i4));
    }

    public static void recycle(Bitmap bitmap) {
        if (bitmap != null) {
            try {
                bitmap.recycle();
            } catch (Exception e) {
                X.e("Failed to recycle bitmap: " + e, new Object[0]);
            }
        }
    }

    public static void removeFromSuperView(Object obj) {
        if (obj != null) {
            if (obj instanceof View) {
                View view = (View) obj;
                ViewParent parent = view.getParent();
                if (parent == null) {
                    return;
                }
                if (parent instanceof ViewGroup) {
                    ((ViewGroup) parent).removeView(view);
                    return;
                }
                X.w("the parent of view %s is not a viewgroup: %s", view, parent);
            } else if (obj instanceof Dialog) {
                ((Dialog) obj).hide();
            }
        }
    }

    public static int rp(int i) {
        return rp(C0037c.getApplicationContext(), i);
    }

    public static int rp(Context context, int i) {
        Context applicationContext = context == null ? C0037c.getApplicationContext() : context;
        if (applicationContext != null) {
            return i > 0 ? (int) ((applicationContext.getResources().getDisplayMetrics().density * ((float) i)) + 0.5f) : i;
        }
        X.e("context is null for rp", new Object[0]);
        return i;
    }

    public static void runInHandlerThread(Runnable runnable) {
        if (gY != null) {
            gY.runInHandlerThread(runnable);
        }
    }

    public static void runInHandlerThreadDelay(Runnable runnable) {
        if (gY != null) {
            gY.runInHandlerThreadDelay(runnable);
        }
    }

    public static void setViewFrame(Object obj, int i, int i2, int i3, int i4) {
        if (obj == null || !(obj instanceof View)) {
            X.w("%s is not a view object, can't set frame", obj);
            return;
        }
        View view = (View) obj;
        if (view.getParent() == null || (view.getParent() instanceof AbsoluteLayout)) {
            view.setLayoutParams(rawAbsoluteLayoutParams(view.getContext(), i3, i4, i, i2));
        } else if (view.getParent() instanceof PPYAbsoluteLayout) {
            view.setLayoutParams(rawAbsoluteLayoutParams(view.getContext(), i3, i4, i, i2));
        } else {
            X.w("Parent %s is not absolutelayout, can't set frame", view.getParent());
        }
    }

    public static void setVisibility(Object obj, int i) {
        if (obj != null && (obj instanceof View)) {
            ((View) obj).setVisibility(i);
        }
    }

    public static void showInView(Object obj, ViewGroup viewGroup, boolean z) {
        if (obj != null) {
            if (obj instanceof ViewControl) {
                ((ViewControl) obj).showInView(viewGroup, z);
            } else if (obj instanceof Dialog) {
                ((Dialog) obj).show();
            } else if (obj instanceof View) {
                addView(viewGroup, (View) obj, false);
            } else {
                X.w("Can't showInView for unknown view %s", obj);
            }
        }
    }

    public static void showMoreTipToast() {
        if (C0025ax.getInstance().isConnected()) {
            try {
                if (gX <= 0 && C0028b.getActiveActivity() != null) {
                    gX++;
                    View inflate = LayoutInflater.from(C0037c.getApplicationContext()).inflate(C0060z.layoutID("toast_layout"), (ViewGroup) null);
                    ((ImageView) inflate.findViewById(C0060z.id("toastimage"))).setImageResource(C0060z.drawableID("toast_image"));
                    ((TextView) inflate.findViewById(C0060z.id("toasttext"))).setText(C0037c.getString("view_menu"));
                    Toast toast = new Toast(C0037c.getApplicationContext());
                    toast.setDuration(0);
                    toast.setView(inflate);
                    toast.show();
                }
            } catch (Exception e) {
                X.e(e, "Failed to show more tip toast", new Object[0]);
            }
        }
    }

    public static void showSoftKeyboard(EditText editText) {
        try {
            editText.requestFocusFromTouch();
            ((InputMethodManager) editText.getContext().getSystemService("input_method")).showSoftInput(editText, 0);
        } catch (Exception e) {
            X.e(e, "Failed to show soft keyboard", new Object[0]);
        }
    }

    public static void showToast(final View view, final int i) {
        if (isMainThread()) {
            showToastIMT(view, i);
        } else {
            runInHandlerThread(new Runnable() {
                public final void run() {
                    C0030bb.showToastIMT(view, i);
                }
            });
        }
    }

    public static void showToast(String str, int i) {
        showToast(str, i, false);
    }

    public static void showToast(final String str, final int i, final boolean z) {
        if (!aV.isEmpty(str)) {
            if (isMainThread()) {
                showToastIMT(str, i, z);
            } else {
                runInHandlerThread(new Runnable() {
                    public final void run() {
                        C0030bb.showToastIMT(str, i, z);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static void showToastIMT(View view, int i) {
        try {
            Toast toast = new Toast(view.getContext());
            toast.setView(view);
            toast.setDuration(i);
            toast.show();
        } catch (Exception e) {
            X.w("Failed to show toast: " + e, new Object[0]);
        }
    }

    /* access modifiers changed from: private */
    public static void showToastIMT(String str, int i, boolean z) {
        if (!z) {
            try {
                Toast.makeText(C0037c.getApplicationContext(), str, i).show();
            } catch (Exception e) {
                X.w("Failed to show toast: " + e, new Object[0]);
            }
        } else {
            TextView textView = new TextView(C0037c.getApplicationContext());
            textView.setBackgroundColor(-3355444);
            textView.setTextColor(-65536);
            textView.setText(str);
            Toast toast = new Toast(textView.getContext());
            toast.setView(textView);
            toast.setDuration(0);
            toast.show();
        }
    }

    public static void startCameraActivity(Activity activity, int i) {
        if (activity != null) {
            try {
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                if (ha == null) {
                    File file = new File(Environment.getExternalStorageDirectory(), "papaya_tmp_photo");
                    ha = file;
                    hb = Uri.fromFile(file);
                }
                intent.putExtra("output", hb);
                activity.startActivityForResult(intent, i);
            } catch (Exception e) {
                X.w("Failed to start camera: " + e, new Object[0]);
                Toast.makeText(activity, "Failed to open camera", 0).show();
            }
        }
    }

    public static void startGalleryActivity(Activity activity, int i) {
        if (activity != null) {
            C0028b.startActivityForResult(activity, new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI), i);
        }
    }

    public static int toGravity(String str, int i) {
        if ("bottom".equals(str)) {
            return 80;
        }
        if ("left".equals(str)) {
            return 3;
        }
        if ("right".equals(str)) {
            return 5;
        }
        if ("top".equals(str)) {
            return 48;
        }
        return i;
    }
}
