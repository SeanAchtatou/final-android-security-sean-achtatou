package com.papaya.si;

import com.papaya.base.EntryActivity;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

/* renamed from: com.papaya.si.r  reason: case insensitive filesystem */
public final class C0052r implements aN<C> {
    private /* synthetic */ EntryActivity aA;

    C0052r() {
    }

    public C0052r(EntryActivity entryActivity) {
        this.aA = entryActivity;
    }

    public static String constructEventRequestPath(C0050p pVar, String str) {
        Locale locale = Locale.getDefault();
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(String.format("5(%s*%s", encode(pVar.au), encode(pVar.av)));
        if (pVar.label != null) {
            sb2.append('*').append(encode(pVar.label));
        }
        sb2.append(")");
        if (pVar.value >= 0) {
            sb2.append(String.format("(%d)", Integer.valueOf(pVar.value)));
        }
        sb.append("/__utm.gif");
        sb.append("?utmwv=4.4ma");
        sb.append("&utmn=").append(pVar.aq);
        sb.append("&utmt=event");
        sb.append("&utme=").append(sb2.toString());
        sb.append("&utmcs=UTF-8");
        sb.append(String.format("&utmsr=%dx%d", Integer.valueOf(pVar.aw), Integer.valueOf(pVar.ax)));
        sb.append(String.format("&utmul=%s-%s", locale.getLanguage(), locale.getCountry()));
        sb.append("&utmac=").append(pVar.ap);
        sb.append("&utmcc=").append(getEscapedCookieString(pVar, str));
        return sb.toString();
    }

    public static String constructPageviewRequestPath(C0050p pVar, String str) {
        String str2 = "";
        if (pVar.av != null) {
            str2 = pVar.av;
        }
        if (!str2.startsWith("/")) {
            str2 = "/" + str2;
        }
        String encode = encode(str2);
        Locale locale = Locale.getDefault();
        StringBuilder sb = new StringBuilder();
        sb.append("/__utm.gif");
        sb.append("?utmwv=4.4ma");
        sb.append("&utmn=").append(pVar.aq);
        sb.append("&utmcs=UTF-8");
        sb.append(String.format("&utmsr=%dx%d", Integer.valueOf(pVar.aw), Integer.valueOf(pVar.ax)));
        sb.append(String.format("&utmul=%s-%s", locale.getLanguage(), locale.getCountry()));
        sb.append("&utmp=").append(encode);
        sb.append("&utmac=").append(pVar.ap);
        sb.append("&utmcc=").append(getEscapedCookieString(pVar, str));
        return sb.toString();
    }

    private static String encode(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getEscapedCookieString(C0050p pVar, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("__utma=");
        sb.append("999").append(".");
        sb.append(pVar.ao).append(".");
        sb.append(pVar.ar).append(".");
        sb.append(pVar.as).append(".");
        sb.append(pVar.at).append(".");
        sb.append(pVar.N);
        if (str != null) {
            sb.append("+__utmz=");
            sb.append("999").append(".");
            sb.append(pVar.ar).append(".");
            sb.append("1.1.");
            sb.append(str);
        }
        return encode(sb.toString());
    }

    public final /* bridge */ /* synthetic */ boolean onDataStateChanged(aP aPVar) {
        return onDataStateChanged$481ea20f();
    }

    public final boolean onDataStateChanged$481ea20f() {
        this.aA.refreshBadgeValues();
        return false;
    }
}
