package com.papaya.si;

import android.graphics.drawable.Drawable;

public final class S extends D {
    public int cW;
    public int dm;
    public String dn;

    /* renamed from: do  reason: not valid java name */
    public boolean f0do;

    public final Drawable getDefaultDrawable() {
        return C0037c.getBitmapDrawable("avatar_unknown");
    }

    public final CharSequence getSubtitle() {
        return C0037c.getSession().getPrivateChatWhiteList().contains(Integer.valueOf(this.cW)) ? C0037c.getString("allow") : C0037c.getString("disallow");
    }

    public final String getTimeLabel() {
        return null;
    }

    public final String getTitle() {
        return this.dn;
    }

    public final boolean isGrayScaled() {
        return false;
    }
}
