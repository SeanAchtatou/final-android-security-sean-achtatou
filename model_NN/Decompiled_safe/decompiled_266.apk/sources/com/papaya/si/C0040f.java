package com.papaya.si;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import com.papaya.social.BillingChannel;
import java.security.SecureRandom;
import java.util.ArrayList;

/* renamed from: com.papaya.si.f  reason: case insensitive filesystem */
public final class C0040f {
    private C0045k L;
    private int M;
    private int N;
    private int O;
    private long P;
    private long Q;
    private long R;
    private boolean S;
    private SQLiteStatement T;

    private C0040f() {
    }

    public C0040f(Context context) {
        this(context, null);
    }

    public C0040f(Context context, String str) {
        this.T = null;
        if (str != null) {
            this.L = new C0045k(context, str);
        } else {
            this.L = new C0045k(context);
        }
    }

    public static void destroy() {
    }

    public static void initialize(Application application) {
    }

    public final void deleteEvent(long j) {
        if (this.L.getWritableDatabase().delete("events", "event_id=" + j, null) != 0) {
            this.O--;
        }
    }

    public final int getNumStoredEvents() {
        if (this.T == null) {
            this.T = this.L.getReadableDatabase().compileStatement("SELECT COUNT(*) from events");
        }
        return (int) this.T.simpleQueryForLong();
    }

    public final String getReferrer() {
        Cursor query = this.L.getReadableDatabase().query("install_referrer", new String[]{"referrer"}, null, null, null, null, null);
        String string = query.moveToFirst() ? query.getString(0) : null;
        query.close();
        return string;
    }

    public final int getStoreId() {
        return this.M;
    }

    public final C0050p[] peekEvents() {
        return peekEvents(1000);
    }

    public final C0050p[] peekEvents(int i) {
        Cursor query = this.L.getReadableDatabase().query("events", null, null, null, null, null, "event_id", Integer.toString(i));
        ArrayList arrayList = new ArrayList();
        while (query.moveToNext()) {
            arrayList.add(new C0050p(query.getLong(0), query.getInt(1), query.getString(2), query.getInt(3), query.getInt(4), query.getInt(5), query.getInt(6), query.getInt(7), query.getString(8), query.getString(9), query.getString(10), query.getInt(11), query.getInt(12), query.getInt(13)));
        }
        query.close();
        return (C0050p[]) arrayList.toArray(new C0050p[arrayList.size()]);
    }

    public final void putEvent(C0050p pVar) {
        if (this.O < 1000) {
            if (!this.S) {
                storeUpdatedSession();
            }
            SQLiteDatabase writableDatabase = this.L.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("user_id", Integer.valueOf(pVar.ao));
            contentValues.put("account_id", pVar.ap);
            contentValues.put("random_val", Integer.valueOf((int) (Math.random() * 2.147483647E9d)));
            contentValues.put("timestamp_first", Long.valueOf(this.P));
            contentValues.put("timestamp_previous", Long.valueOf(this.Q));
            contentValues.put("timestamp_current", Long.valueOf(this.R));
            contentValues.put("visits", Integer.valueOf(this.N));
            contentValues.put("category", pVar.au);
            contentValues.put("action", pVar.av);
            contentValues.put("label", pVar.label);
            contentValues.put("value", Integer.valueOf(pVar.value));
            contentValues.put("screen_width", Integer.valueOf(pVar.aw));
            contentValues.put("screen_height", Integer.valueOf(pVar.ax));
            if (writableDatabase.insert("events", "event_id", contentValues) != -1) {
                this.O++;
            }
        }
    }

    public final void setReferrer(String str) {
        SQLiteDatabase writableDatabase = this.L.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("referrer", str);
        writableDatabase.insert("install_referrer", null, contentValues);
    }

    public final void startNewVisit() {
        this.S = false;
        this.O = getNumStoredEvents();
        SQLiteDatabase writableDatabase = this.L.getWritableDatabase();
        Cursor query = writableDatabase.query("session", null, null, null, null, null, null);
        if (!query.moveToFirst()) {
            long currentTimeMillis = System.currentTimeMillis() / 1000;
            this.P = currentTimeMillis;
            this.Q = currentTimeMillis;
            this.R = currentTimeMillis;
            this.N = 1;
            this.M = new SecureRandom().nextInt() & BillingChannel.ALL;
            ContentValues contentValues = new ContentValues();
            contentValues.put("timestamp_first", Long.valueOf(this.P));
            contentValues.put("timestamp_previous", Long.valueOf(this.Q));
            contentValues.put("timestamp_current", Long.valueOf(this.R));
            contentValues.put("visits", Integer.valueOf(this.N));
            contentValues.put("store_id", Integer.valueOf(this.M));
            writableDatabase.insert("session", "timestamp_first", contentValues);
        } else {
            this.P = query.getLong(0);
            this.Q = query.getLong(2);
            this.R = System.currentTimeMillis() / 1000;
            this.N = query.getInt(3) + 1;
            this.M = query.getInt(4);
        }
        query.close();
    }

    public final void storeUpdatedSession() {
        SQLiteDatabase writableDatabase = this.L.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("timestamp_previous", Long.valueOf(this.Q));
        contentValues.put("timestamp_current", Long.valueOf(this.R));
        contentValues.put("visits", Integer.valueOf(this.N));
        writableDatabase.update("session", contentValues, "timestamp_first=?", new String[]{Long.toString(this.P)});
        this.S = true;
    }
}
