package com.papaya.si;

import android.view.View;
import android.widget.TextView;
import com.adknowledge.superrewards.model.SROffer;
import com.papaya.view.BadgeView;
import com.papaya.view.CardImageView;

/* renamed from: com.papaya.si.aa  reason: case insensitive filesystem */
public final class C0002aa {
    public TextView cn;
    public TextView dH;
    public TextView dI;
    public BadgeView dJ;
    public CardImageView dq;

    public C0002aa(View view) {
        this.dq = (CardImageView) C0030bb.find(view, SROffer.IMAGE);
        this.cn = (TextView) C0030bb.find(view, SROffer.TITLE);
        this.dH = (TextView) C0030bb.find(view, "subtitle");
        this.dI = (TextView) C0030bb.find(view, "time");
        this.dJ = (BadgeView) C0030bb.find(view, "unread");
    }
}
