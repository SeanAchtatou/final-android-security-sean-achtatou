package com.papaya.si;

import android.content.Context;

/* renamed from: com.papaya.si.e  reason: case insensitive filesystem */
public final class C0039e {
    private C0053s K;

    private C0039e() {
    }

    public C0039e(C0053s sVar) {
        this.K = sVar;
    }

    public static void initialize(Context context) {
    }

    public static void show(Context context) {
    }

    public final void dispatchFinished() {
        this.K.handler.post(new C0046l());
    }

    public final void eventDispatched(long j) {
        this.K.aH.deleteEvent(j);
    }
}
