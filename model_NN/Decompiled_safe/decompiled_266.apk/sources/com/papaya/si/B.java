package com.papaya.si;

import android.content.Context;
import android.os.Environment;
import java.io.File;

public abstract class B {
    private String ci;
    protected File cj;
    protected Context ck;
    protected boolean cl = true;

    public B(String str, Context context) {
        this.ci = str;
        this.ck = context;
    }

    public boolean clearCache() {
        aU.clearDir(this.cj);
        return true;
    }

    public void close() {
        doClose();
        this.cl = true;
    }

    /* access modifiers changed from: protected */
    public abstract void doClose();

    /* access modifiers changed from: protected */
    public abstract boolean doInitCache();

    public File getCacheDir() {
        return this.cj;
    }

    public String getCacheDirName() {
        return this.ci;
    }

    public File getCacheFile(String str) {
        return new File(this.cj, keyToStoreName(str));
    }

    public Context getContext() {
        return this.ck;
    }

    public boolean initCache() {
        prepareCacheDir();
        this.cl = false;
        return aU.exist(this.cj) && doInitCache();
    }

    /* access modifiers changed from: protected */
    public abstract String keyToStoreName(String str);

    public byte[] loadBytesWithKey(String str) {
        return aU.dataFromFile(getCacheFile(str));
    }

    /* access modifiers changed from: protected */
    public void prepareCacheDir() {
        if (C0029ba.gW) {
            this.cj = new File(Environment.getExternalStorageDirectory(), this.ci);
            if (!this.cj.exists()) {
                this.cj.mkdirs();
            }
            X.i("cache dir in external storage", new Object[0]);
        } else {
            this.cj = this.ck.getDir(this.ci, 1);
            X.i("cache dir in phone storage", new Object[0]);
        }
        if (!this.cj.exists()) {
            X.w("cache dir %s, doesn't exist", this.cj);
        }
    }

    public boolean saveBytesWithKey(String str, byte[] bArr) {
        return aU.writeBytesToFile(getCacheFile(str), bArr);
    }
}
