package com.papaya.si;

import com.papaya.base.EntryActivity;
import java.util.ArrayList;

public final class P extends ArrayList<O> {
    private int de;

    public final boolean add(O o) {
        boolean add = super.add((Object) o);
        if (size() > 200) {
            remove(0);
        }
        return add;
    }

    public final int getUnread() {
        return this.de;
    }

    public final void increaseUnread() {
        setUnread(this.de + 1);
    }

    public final void removeAllMessages() {
        clear();
        this.de = 0;
    }

    public final void setUnread(int i) {
        this.de = i;
        EntryActivity.a.refreshUnread();
    }
}
