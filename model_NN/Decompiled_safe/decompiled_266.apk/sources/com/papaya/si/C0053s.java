package com.papaya.si;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Handler;

/* renamed from: com.papaya.si.s  reason: case insensitive filesystem */
public final class C0053s {
    private static final C0053s aB = new C0053s();
    private String aC = "GoogleAnalytics";
    private String aD = "1.0";
    private Context aE;
    private ConnectivityManager aF;
    private int aG;
    public C0040f aH;
    private C0048n aI;
    private boolean aJ;
    private boolean aK;
    private Runnable aL = new C0047m(this);
    private String ap;
    public Handler handler;

    private C0053s() {
    }

    private void cancelPendingDispathes() {
        this.handler.removeCallbacks(this.aL);
    }

    private void createEvent(String str, String str2, String str3, String str4, int i) {
        this.aH.putEvent(new C0050p(this.aH.getStoreId(), str, str2, str3, str4, i, this.aE.getResources().getDisplayMetrics().widthPixels, this.aE.getResources().getDisplayMetrics().heightPixels));
        resetPowerSaveMode();
    }

    public static C0053s getInstance() {
        return aB;
    }

    private void maybeScheduleNextDispatch() {
        if (this.aG >= 0) {
            this.handler.postDelayed(this.aL, (long) (this.aG * 1000));
        }
    }

    private void resetPowerSaveMode() {
        if (this.aJ) {
            this.aJ = false;
            maybeScheduleNextDispatch();
        }
    }

    public final boolean dispatch() {
        if (this.aK) {
            maybeScheduleNextDispatch();
            return false;
        } else if (!C0029ba.isNetworkAvailable()) {
            maybeScheduleNextDispatch();
            return false;
        } else if (this.aH.getNumStoredEvents() != 0) {
            this.aI.dispatchEvents(this.aH.peekEvents());
            this.aK = true;
            maybeScheduleNextDispatch();
            return true;
        } else {
            this.aJ = true;
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final void dispatchFinished() {
        this.aK = false;
    }

    /* access modifiers changed from: package-private */
    public final C0048n getDispatcher() {
        return this.aI;
    }

    /* access modifiers changed from: package-private */
    public final C0040f getEventStore$109a4497() {
        return this.aH;
    }

    public final void setDispatchPeriod(int i) {
        int i2 = this.aG;
        this.aG = i;
        if (i2 <= 0) {
            maybeScheduleNextDispatch();
        } else if (i2 > 0) {
            cancelPendingDispathes();
            maybeScheduleNextDispatch();
        }
    }

    public final void setProductVersion(String str, String str2) {
        this.aC = str;
        this.aD = str2;
    }

    public final void start(String str, int i, Context context) {
        start$3121730(str, i, context, this.aH == null ? new C0040f(context) : this.aH, this.aI == null ? new C0051q(this.aC, this.aD) : this.aI);
    }

    public final void start(String str, Context context) {
        start(str, -1, context);
    }

    /* access modifiers changed from: package-private */
    public final void start$3121730(String str, int i, Context context, C0040f fVar, C0048n nVar) {
        start$6cdadacb(str, i, context, fVar, nVar, new C0039e(this));
    }

    /* access modifiers changed from: package-private */
    public final void start$6cdadacb(String str, int i, Context context, C0040f fVar, C0048n nVar, C0039e eVar) {
        this.ap = str;
        this.aE = context;
        this.aH = fVar;
        this.aH.startNewVisit();
        this.aI = nVar;
        this.aI.init$67669206(eVar, this.aH.getReferrer());
        this.aK = false;
        if (this.aF == null) {
            this.aF = (ConnectivityManager) this.aE.getSystemService("connectivity");
        }
        if (this.handler == null) {
            this.handler = new Handler(context.getMainLooper());
        } else {
            cancelPendingDispathes();
        }
        setDispatchPeriod(i);
    }

    public final void stop() {
        this.aI.stop();
        cancelPendingDispathes();
    }

    public final void trackEvent(String str, String str2, String str3, int i) {
        createEvent(this.ap, str, str2, str3, i);
    }

    public final void trackPageView(String str) {
        createEvent(this.ap, "__##GOOGLEPAGEVIEW##__", str, null, -1);
    }
}
