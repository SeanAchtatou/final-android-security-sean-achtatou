package com.papaya.si;

import android.graphics.drawable.Drawable;
import com.papaya.si.D;
import com.papaya.view.Action;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class E<T extends D> extends aG implements Serializable {
    protected ArrayList<T> cq = new ArrayList<>();
    private boolean cr = true;
    protected List<Action> cs;
    private transient Drawable icon;
    private String name = "";

    public synchronized boolean add(T t) {
        boolean z;
        if (this.cq.contains(t)) {
            z = false;
        } else {
            this.cq.add(t);
            z = true;
        }
        return z;
    }

    public synchronized void clear() {
        this.cq.clear();
    }

    public synchronized boolean contains(T t) {
        return this.cq.contains(t);
    }

    public synchronized void fireAllDataStateChanged() {
        int i = 0;
        synchronized (this) {
            while (true) {
                int i2 = i;
                if (i2 < this.cq.size()) {
                    try {
                        ((D) this.cq.get(i2)).fireDataStateChanged();
                    } catch (Exception e) {
                        X.e(e, "Failed to relay state changed", new Object[0]);
                    }
                    i = i2 + 1;
                } else {
                    fireDataStateChanged();
                }
            }
        }
    }

    public synchronized T get(int i) {
        return (D) this.cq.get(i);
    }

    public List<Action> getActions() {
        return this.cs;
    }

    public Drawable getIcon() {
        return this.icon;
    }

    public String getLabel() {
        return aV.format("%s (%d)", getName(), Integer.valueOf(size()));
    }

    public String getName() {
        return this.name;
    }

    public synchronized int indexOf(T t) {
        return this.cq.indexOf(t);
    }

    public synchronized void insertSort(T t) {
        int binarySearch = Collections.binarySearch(this.cq, t);
        if (binarySearch < 0) {
            this.cq.add((-binarySearch) - 1, t);
        }
    }

    public boolean isReserveGroupHeader() {
        return this.cr;
    }

    public synchronized T remove(int i) {
        return (D) this.cq.remove(i);
    }

    public synchronized boolean remove(D d) {
        return this.cq.remove(d);
    }

    public synchronized boolean sectionHeaderVisible() {
        return this.cq.size() > 0 || this.cr;
    }

    public void setCardStates(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.cq.size()) {
                ((D) this.cq.get(i3)).setState(i);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public void setIcon(Drawable drawable) {
        this.icon = drawable;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setReserveGroupHeader(boolean z) {
        this.cr = z;
    }

    public synchronized int size() {
        return this.cq.size();
    }

    public synchronized void sort() {
        Collections.sort(this.cq);
    }

    public synchronized ArrayList<T> toList() {
        return new ArrayList<>(this.cq);
    }
}
