package com.papaya.si;

import android.content.DialogInterface;
import android.view.View;
import com.papaya.view.Action;
import com.papaya.view.CustomDialog;

class Z implements View.OnClickListener {
    private /* synthetic */ Y dD;

    Z(Y y) {
        this.dD = y;
    }

    public final void onClick(View view) {
        Action action = (Action) view.getTag();
        switch (action.id) {
            case 0:
                C0028b.openPRIALink(this.dD.dA, "static_newchatgroup");
                return;
            case 1:
                C0028b.openPRIALink(this.dD.dA, "static_searchchatgroup");
                return;
            case 2:
                C0005ad adVar = (C0005ad) action.data;
                adVar.setImState(2);
                C0037c.send(4, Integer.valueOf(adVar.dO), adVar.dQ, adVar.dR);
                C0037c.getSession().fireDataStateChanged();
                return;
            case 3:
                C0005ad adVar2 = (C0005ad) action.data;
                C0037c.send(5, Integer.valueOf(adVar2.dP));
                C0037c.getSession().closeIM(adVar2);
                return;
            case 4:
                return;
            case 5:
                final C0005ad adVar3 = (C0005ad) action.data;
                if (adVar3 != null) {
                    new CustomDialog.Builder(this.dD.dA).setMessage(C0037c.getString("im_delete_confirm")).setPositiveButton(C0037c.getString("delete"), new DialogInterface.OnClickListener() {
                        public final void onClick(DialogInterface dialogInterface, int i) {
                            C0037c.getSession().closeIM(C0005ad.this);
                            C0037c.getSession().getIms().remove(C0005ad.this);
                            C0037c.getSession().getIms().saveToFile("papayaim1");
                            C0037c.getSession().refreshCardLists();
                            C0037c.getSession().fireDataStateChanged();
                        }
                    }).setNegativeButton(C0037c.getString("btn_cancel"), (DialogInterface.OnClickListener) null).show();
                    return;
                }
                return;
            default:
                X.e("unknown action id: " + action.id, new Object[0]);
                return;
        }
    }
}
