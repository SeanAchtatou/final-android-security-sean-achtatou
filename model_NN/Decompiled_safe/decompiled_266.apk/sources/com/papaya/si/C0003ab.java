package com.papaya.si;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.adknowledge.superrewards.model.SROffer;

/* renamed from: com.papaya.si.ab  reason: case insensitive filesystem */
public final class C0003ab {
    public ImageView dK;
    public TextView dL;
    public ImageView dM;
    public LinearLayout dN;

    public C0003ab(View view) {
        this.dK = (ImageView) C0030bb.find(view, SROffer.ICON);
        this.dL = (TextView) C0030bb.find(view, "label");
        this.dM = (ImageView) C0030bb.find(view, "group_indicator");
        this.dN = (LinearLayout) C0030bb.find(view, "actions_content");
    }
}
