package com.papaya.si;

import java.util.Iterator;

public final class aO implements aP {
    private boolean gA = false;
    private aP gy;
    private C0031bc<aN> gz;

    public aO(aP aPVar) {
        this.gy = aPVar;
    }

    public final void fireDataStateChanged() {
        if (C0030bb.isMainThread()) {
            fireDataStateChangedInUIThread();
        } else {
            C0030bb.runInHandlerThread(new Runnable() {
                public final void run() {
                    aO.this.fireDataStateChangedInUIThread();
                }
            });
        }
    }

    public final void fireDataStateChangedInUIThread() {
        if (!this.gA) {
            this.gA = true;
            if (this.gz != null) {
                this.gz.trimGarbage();
                Iterator<aN> it = this.gz.iterator();
                while (it.hasNext()) {
                    aN next = it.next();
                    if (next != null) {
                        try {
                            if (next.onDataStateChanged(this.gy)) {
                                it.remove();
                            }
                        } catch (Exception e) {
                            X.e(e, "Failed to callback onDataStateChanged on " + next, new Object[0]);
                        }
                    }
                }
            }
            this.gA = false;
        }
    }

    public final int indexOf(aN aNVar) {
        if (this.gz != null) {
            for (int i = 0; i < this.gz.size(); i++) {
                if (this.gz.get(i) == aNVar) {
                    return i;
                }
            }
        }
        return -1;
    }

    public final void registerMonitor(aN aNVar) {
        if (this.gz == null) {
            this.gz = new C0031bc<>(4);
        }
        if (indexOf(aNVar) == -1) {
            this.gz.add(aNVar);
        }
    }

    public final void unregisterMonitor(aN aNVar) {
        int indexOf = indexOf(aNVar);
        if (indexOf != -1) {
            this.gz.remove(indexOf);
        }
    }
}
