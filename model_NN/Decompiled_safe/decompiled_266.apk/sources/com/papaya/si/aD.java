package com.papaya.si;

import android.app.Activity;
import android.app.ProgressDialog;
import com.adknowledge.superrewards.model.SROffer;
import com.papaya.si.aU;
import com.papaya.social.PPYSocialChallengeRecord;
import com.papaya.social.PPYSocialQuery;
import org.json.JSONObject;

public final class aD extends ProgressDialog implements PPYSocialQuery.QueryDelegate {
    private Activity fZ;

    public aD(Activity activity) {
        super(activity);
        this.fZ = activity;
        setMessage(C0037c.getString("accepting_challenge"));
        setCancelable(false);
    }

    public final void acceptChallenge(int i) {
        show();
        setIndeterminate(true);
        PPYSocialQuery pPYSocialQuery = new PPYSocialQuery("w_challenge_accept", this);
        pPYSocialQuery.put("rid", i);
        C0023av.getInstance().submitQuery(pPYSocialQuery);
    }

    public final void onQueryFailed(PPYSocialQuery pPYSocialQuery, String str) {
        dismiss();
        C0030bb.showToast(C0037c.getString("fail_accept_challenge"), 0);
    }

    public final void onQueryResponse(PPYSocialQuery pPYSocialQuery, JSONObject jSONObject) {
        dismiss();
        if (jSONObject != null) {
            PPYSocialChallengeRecord pPYSocialChallengeRecord = new PPYSocialChallengeRecord();
            pPYSocialChallengeRecord.setRecordID(jSONObject.optInt(SROffer.ID, 0));
            pPYSocialChallengeRecord.setSenderUserID(jSONObject.optInt("sender_id", 0));
            pPYSocialChallengeRecord.setReceiverUserID(jSONObject.optInt("receiver_id", 0));
            pPYSocialChallengeRecord.setMessage(jSONObject.optString("message", null));
            int optInt = jSONObject.optInt("payload_type", 0);
            String optString = jSONObject.optString("payload", null);
            if (optInt == 0) {
                pPYSocialChallengeRecord.setPayload(optString);
            } else {
                pPYSocialChallengeRecord.setPayloadBinary(optString != null ? aU.a.decode(optString) : null);
            }
            try {
                this.fZ.finish();
            } catch (Exception e) {
            }
            C0021at.instance().fireChallengeAccepted(pPYSocialChallengeRecord);
        }
    }
}
