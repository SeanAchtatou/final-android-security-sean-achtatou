package com.papaya.si;

import android.content.Context;
import android.text.Html;
import com.papaya.view.OverlayCustomDialog;

public final class aC extends OverlayCustomDialog implements OverlayCustomDialog.OnClickListener {
    private CharSequence db;
    private boolean fY = false;

    public aC(Context context, CharSequence charSequence) {
        super(context);
        CharSequence charSequence2;
        if (aV.isEmpty(charSequence)) {
            charSequence2 = Html.fromHtml(aV.format(C0037c.getString("dlg_recommend_message"), C0056v.bj));
            this.fY = true;
        } else {
            charSequence2 = charSequence;
        }
        this.db = charSequence2;
        setMessage(charSequence2);
        setTitle(aV.format(C0037c.getString("dlg_recommend_title"), C0056v.bj));
        setPositiveButton(C0037c.getString("button_recommend"), this);
        setNegativeButton(C0037c.getString("button_cancel"), this);
    }

    /* access modifiers changed from: protected */
    public final int getDefaultVisibility() {
        return 0;
    }

    public final void onClick(OverlayCustomDialog overlayCustomDialog, int i) {
        if (i == -1) {
            C0023av.getInstance().postNewsfeed(aV.nullAsEmpty(this.db), null, this.fY ? 1 : 2);
        }
    }
}
