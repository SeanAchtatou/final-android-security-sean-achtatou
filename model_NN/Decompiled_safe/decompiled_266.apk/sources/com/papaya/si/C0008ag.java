package com.papaya.si;

import android.graphics.drawable.Drawable;

/* renamed from: com.papaya.si.ag  reason: case insensitive filesystem */
public final class C0008ag extends D {
    public int cW;
    public int dm;
    public String dn;

    public C0008ag() {
        this.state = 1;
    }

    public final Drawable getDefaultDrawable() {
        return C0037c.getBitmapDrawable("avatar_unknown");
    }

    public final String getTimeLabel() {
        return null;
    }

    public final String getTitle() {
        return this.dn;
    }

    public final boolean isGrayScaled() {
        return false;
    }
}
