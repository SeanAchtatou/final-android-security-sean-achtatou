package com.papaya.si;

import android.graphics.drawable.Drawable;

/* renamed from: com.papaya.si.ae  reason: case insensitive filesystem */
public final class C0006ae extends D {
    public String dQ;
    public String dV;
    public String dW;
    public C0005ad dX;

    public final Drawable getDefaultDrawable() {
        return C0037c.getBitmapDrawable("im_face_" + this.dX.dO);
    }

    public final CharSequence getSubtitle() {
        return this.dW;
    }

    public final String getTimeLabel() {
        return null;
    }

    public final String getTitle() {
        return this.dV;
    }

    public final boolean isGrayScaled() {
        return this.state == 0;
    }
}
