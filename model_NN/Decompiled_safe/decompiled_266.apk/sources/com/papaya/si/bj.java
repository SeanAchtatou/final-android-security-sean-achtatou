package com.papaya.si;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public final class bj {
    private DataOutputStream lL = null;
    private String lM = null;

    public bj(OutputStream outputStream, String str) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream is required.");
        } else if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("Boundary stream is required.");
        } else {
            this.lL = new DataOutputStream(outputStream);
            this.lM = str;
        }
    }

    public static String createBoundary() {
        return "--------------------" + Long.toString(System.currentTimeMillis(), 16);
    }

    public static URLConnection createConnection(URL url) throws IOException {
        URLConnection openConnection = url.openConnection();
        if (openConnection instanceof HttpURLConnection) {
            ((HttpURLConnection) openConnection).setRequestMethod("POST");
        }
        openConnection.setDoInput(true);
        openConnection.setDoOutput(true);
        openConnection.setUseCaches(false);
        return openConnection;
    }

    public static String getContentType(String str) {
        return "multipart/form-data; boundary=" + str;
    }

    public final void close() throws IOException {
        this.lL.writeBytes("--");
        this.lL.writeBytes(this.lM);
        this.lL.writeBytes("--");
        this.lL.writeBytes("\r\n");
        this.lL.flush();
        this.lL.close();
    }

    public final void flush() throws IOException {
    }

    public final String getBoundary() {
        return this.lM;
    }

    public final void writeField(String str, String str2) throws IOException {
        String str3 = str2 == null ? "" : str2;
        this.lL.writeBytes("--");
        this.lL.writeBytes(this.lM);
        this.lL.writeBytes("\r\n");
        this.lL.writeBytes("Content-Disposition: form-data; name=\"" + str + '\"');
        this.lL.writeBytes("\r\n");
        this.lL.writeBytes("\r\n");
        byte[] bytes = str3.getBytes("UTF-8");
        this.lL.write(bytes, 0, bytes.length);
        this.lL.writeBytes("\r\n");
        this.lL.flush();
    }

    public final void writeFile(String str, String str2, String str3, InputStream inputStream) throws IOException {
        if (inputStream == null) {
            X.w("Input stream is null", new Object[0]);
        } else if (str3 == null || str3.length() == 0) {
            X.w("fileName is empty", new Object[0]);
        } else {
            byte[] acquireBytes = aV.acquireBytes(1024);
            try {
                this.lL.writeBytes("--");
                this.lL.writeBytes(this.lM);
                this.lL.writeBytes("\r\n");
                this.lL.writeBytes("Content-Disposition: form-data; name=\"" + str + "\"; filename=\"" + str3 + '\"');
                this.lL.writeBytes("\r\n");
                if (str2 != null) {
                    this.lL.writeBytes("Content-Type: " + str2);
                    this.lL.writeBytes("\r\n");
                }
                this.lL.writeBytes("\r\n");
                while (true) {
                    int read = inputStream.read(acquireBytes, 0, acquireBytes.length);
                    if (read != -1) {
                        this.lL.write(acquireBytes, 0, read);
                    } else {
                        this.lL.writeBytes("\r\n");
                        this.lL.flush();
                        aU.close(inputStream);
                        aV.releaseBytes(acquireBytes);
                        return;
                    }
                }
            } catch (IOException e) {
                throw e;
            } catch (Throwable th) {
                aU.close(inputStream);
                aV.releaseBytes(acquireBytes);
                throw th;
            }
        }
    }

    public final void writeFile(String str, String str2, String str3, byte[] bArr) throws IOException {
        if (bArr == null) {
            throw new IllegalArgumentException("Data cannot be null.");
        } else if (str3 == null || str3.length() == 0) {
            throw new IllegalArgumentException("File name cannot be null or empty.");
        } else {
            this.lL.writeBytes("--");
            this.lL.writeBytes(this.lM);
            this.lL.writeBytes("\r\n");
            this.lL.writeBytes("Content-Disposition: form-data; name=\"" + str + "\"; filename=\"" + str3 + '\"');
            this.lL.writeBytes("\r\n");
            if (str2 != null) {
                this.lL.writeBytes("Content-Type: " + str2);
                this.lL.writeBytes("\r\n");
            }
            this.lL.writeBytes("\r\n");
            this.lL.write(bArr, 0, bArr.length);
            this.lL.writeBytes("\r\n");
            this.lL.flush();
        }
    }
}
