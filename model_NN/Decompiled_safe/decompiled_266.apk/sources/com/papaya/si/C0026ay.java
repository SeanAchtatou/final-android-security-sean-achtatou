package com.papaya.si;

import android.app.Application;
import android.content.Context;
import java.util.List;

/* renamed from: com.papaya.si.ay  reason: case insensitive filesystem */
public final class C0026ay extends by {
    private aE fL;
    private int fM = 1500;

    public C0026ay(String str, Context context) {
        super(str, context);
    }

    private void increateLoginInterval() {
        if (this.fM * 2 < 45000) {
            this.fM *= 2;
        }
    }

    public final void appendRequest(bv bvVar) {
        super.appendRequest(bvVar);
        poll();
    }

    public final void appendRequests(List<bv> list) {
        super.appendRequests(list);
        poll();
    }

    public final void connectionFailed(bt btVar, int i) {
        if (btVar.getRequest() == this.fL) {
            this.fL = null;
            increateLoginInterval();
            C0030bb.postDelayed(new Runnable() {
                public final void run() {
                    C0026ay.this.tryLogin();
                }
            }, (long) this.fM);
        }
        super.connectionFailed(btVar, i);
        poll();
    }

    public final void connectionFinished(bt btVar) {
        if (btVar.getRequest() == this.fL) {
            this.fL = null;
            this.fM = 1500;
        }
        super.connectionFinished(btVar);
        poll();
    }

    public final boolean encapsuleHttpInTcp() {
        return false;
    }

    public final void insertRequest(bv bvVar) {
        super.insertRequest(bvVar);
        poll();
    }

    public final void insertRequests(List<bv> list) {
        super.insertRequests(list);
        poll();
    }

    public final boolean isLoggingin() {
        return this.fL != null;
    }

    public final boolean removeRequest(bv bvVar) {
        boolean removeRequest = super.removeRequest(bvVar);
        poll();
        return removeRequest;
    }

    public final synchronized void tryLogin() {
        if (this.fL == null) {
            Application applicationContext = C0037c.getApplicationContext();
            if (applicationContext == null || C0029ba.isNetworkAvailable(applicationContext)) {
                this.fL = new aE();
                this.fL.start(true);
            } else {
                C0030bb.postDelayed(new Runnable() {
                    public final void run() {
                        C0026ay.this.tryLogin();
                    }
                }, (long) this.fM);
                increateLoginInterval();
            }
        }
        poll();
    }
}
