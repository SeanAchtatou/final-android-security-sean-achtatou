package com.papaya.si;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import org.apache.http.HttpException;
import org.apache.http.ParseException;
import org.apache.http.message.BasicHttpRequest;

/* renamed from: com.papaya.si.j  reason: case insensitive filesystem */
public final class C0044j implements Runnable {
    private C0049o aa;
    private final LinkedList ae = new LinkedList();

    public C0044j(C0049o oVar, C0050p[] pVarArr) {
        this.aa = oVar;
        Collections.addAll(this.ae, pVarArr);
    }

    private void dispatchSomePendingEvents() throws IOException, ParseException, HttpException {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.ae.size() || i2 >= this.aa.aj) {
                this.aa.ag.sendRequests();
            } else {
                C0050p pVar = (C0050p) this.ae.get(i2);
                BasicHttpRequest basicHttpRequest = new BasicHttpRequest("GET", "__##GOOGLEPAGEVIEW##__".equals(pVar.au) ? C0052r.constructPageviewRequestPath(pVar, this.aa.ah) : C0052r.constructEventRequestPath(pVar, this.aa.ah));
                basicHttpRequest.addHeader("Host", C0051q.ay.getHostName());
                basicHttpRequest.addHeader("User-Agent", this.aa.ai);
                this.aa.ag.addRequest(basicHttpRequest);
                i = i2 + 1;
            }
        }
        this.aa.ag.sendRequests();
    }

    public final C0050p removeNextEvent() {
        return (C0050p) this.ae.poll();
    }

    public final void run() {
        this.aa.al = this;
        int i = 0;
        while (i < 5 && this.ae.size() > 0) {
            long j = 0;
            try {
                if (this.aa.W == 500 || this.aa.W == 503) {
                    j = (long) (Math.random() * ((double) this.aa.ak));
                    if (this.aa.ak < 256) {
                        this.aa.ak *= 2;
                    }
                } else {
                    this.aa.ak = 2;
                }
                Thread.sleep(j * 1000);
                dispatchSomePendingEvents();
                i++;
            } catch (IOException | InterruptedException | HttpException e) {
            }
        }
        this.aa.ag.finishedCurrentRequests();
        this.aa.am.dispatchFinished();
        this.aa.al = null;
    }
}
