package com.papaya.si;

import java.util.HashMap;

/* renamed from: com.papaya.si.bd  reason: case insensitive filesystem */
public final class C0032bd<K, V> {
    private HashMap<K, C0031bc<V>> ho = new HashMap<>();

    public final C0031bc<V> getList(K k) {
        return this.ho.get(k);
    }

    public final boolean put(K k, V v) {
        C0031bc bcVar = this.ho.get(k);
        if (bcVar == null) {
            bcVar = new C0031bc();
            this.ho.put(k, bcVar);
        }
        if (bcVar.contains(v)) {
            return false;
        }
        bcVar.add(v);
        return true;
    }

    public final void remove(K k, V v) {
        C0031bc bcVar = this.ho.get(k);
        if (bcVar != null) {
            bcVar.remove(v);
        }
    }
}
