package com.papaya.si;

public final class R extends E<Q> {
    public R() {
        setName(C0037c.getString("chatroom"));
        setReserveGroupHeader(true);
    }

    public final Q findByRoomID(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.cq.size()) {
                return null;
            }
            Q q = (Q) this.cq.get(i3);
            if (q.df == i) {
                return q;
            }
            i2 = i3 + 1;
        }
    }
}
