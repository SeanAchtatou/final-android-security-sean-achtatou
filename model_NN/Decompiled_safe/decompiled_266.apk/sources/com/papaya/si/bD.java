package com.papaya.si;

import java.util.Vector;

class bD implements C0054t {
    private /* synthetic */ bC nn;

    bD(bC bCVar) {
        this.nn = bCVar;
    }

    public final void handleServerResponse(Vector<Object> vector) {
        bk access$000 = this.nn.nf;
        if (access$000 != null) {
            try {
                Object java2jsonValue = C0034bf.java2jsonValue(vector);
                if (java2jsonValue != null) {
                    access$000.noWarnCallJS("onPotpData", aV.format("onPotpData('%s')", C0034bf.escapeJS(java2jsonValue.toString())));
                    return;
                }
                X.w("java2jsonValue is null", new Object[0]);
            } catch (Exception e) {
                X.e(e, "Failed in webscript potp callback", new Object[0]);
            }
        }
    }
}
