package com.papaya.si;

import com.papaya.web.WebActivity;

public final class bp {
    private static WebActivity ms;

    private bp() {
    }

    public static void clear() {
        ms = null;
    }

    public static synchronized void fireImageUploadFailed(int i, int i2) {
        synchronized (bp.class) {
            try {
                WebActivity webActivity = (WebActivity) C0028b.findActivity(WebActivity.class, i);
                if (webActivity != null) {
                    webActivity.onImageUploadFailed(i2);
                }
            } catch (Exception e) {
                X.e(e, "Failed to fireImageUploaded", new Object[0]);
            }
        }
        return;
    }

    public static synchronized void fireImageUploaded(int i, int i2, int i3) {
        synchronized (bp.class) {
            try {
                WebActivity webActivity = (WebActivity) C0028b.findActivity(WebActivity.class, i);
                if (webActivity != null) {
                    webActivity.onImageUploaded(i2, i3);
                }
            } catch (Exception e) {
                X.e(e, "Failed to fireImageUploaded", new Object[0]);
            }
        }
        return;
    }

    public static void onImageUploadFailed(int i) {
        if (ms != null) {
            try {
                ms.onImageUploadFailed(i);
            } catch (Exception e) {
                X.e(e, "Failed to invoke onImageUploadFailed", new Object[0]);
            }
        }
        ms = null;
    }

    public static void onImageUploaded(int i, int i2) {
        if (ms != null) {
            try {
                ms.onImageUploaded(i, i2);
            } catch (Exception e) {
                X.e(e, "Failed to invoke onImageUploaded", new Object[0]);
            }
        }
        ms = null;
    }

    public static void startUploading(WebActivity webActivity) {
        ms = webActivity;
    }
}
