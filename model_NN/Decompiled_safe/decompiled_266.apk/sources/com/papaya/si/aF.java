package com.papaya.si;

import com.papaya.si.bt;
import com.papaya.social.PPYSocialQuery;
import java.util.HashMap;
import org.json.JSONObject;

public final class aF extends bv implements aW, bt.a {
    private long fR = System.currentTimeMillis();
    private PPYSocialQuery ga;

    public aF(PPYSocialQuery pPYSocialQuery) {
        this.ga = pPYSocialQuery;
        HashMap hashMap = new HashMap(1);
        hashMap.put("payload", pPYSocialQuery.getPayloadString());
        this.url = C0034bf.createURL(C0034bf.compositeUrl("query", hashMap), C0056v.bo);
        this.mI = true;
        setDelegate(this);
    }

    public final void connectionFailed(bt btVar, int i) {
        PPYSocialQuery.QueryDelegate queryDelegate = this.ga.getQueryDelegate();
        if (queryDelegate != null) {
            queryDelegate.onQueryFailed(this.ga, null);
        }
    }

    public final void connectionFinished(bt btVar) {
        PPYSocialQuery.QueryDelegate queryDelegate = this.ga.getQueryDelegate();
        if (queryDelegate != null) {
            JSONObject parseJsonObject = C0034bf.parseJsonObject(aV.utf8String(btVar.getData(), "{}"));
            if (C0034bf.getJsonInt(parseJsonObject, "status", 0) == 1) {
                queryDelegate.onQueryResponse(this.ga, parseJsonObject);
            } else {
                queryDelegate.onQueryFailed(this.ga, C0034bf.getJsonString(parseJsonObject, "error"));
            }
        }
    }

    public final boolean isExpired() {
        return this.ga.isCanceled() || System.currentTimeMillis() - this.fR > 45000;
    }
}
