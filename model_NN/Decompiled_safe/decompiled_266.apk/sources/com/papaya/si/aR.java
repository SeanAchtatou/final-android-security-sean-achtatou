package com.papaya.si;

import java.util.ArrayList;
import java.util.List;

public final class aR {
    private List<aS> gD = new ArrayList();

    public final synchronized aS acquire() {
        return this.gD.isEmpty() ? new aS() : this.gD.remove(0);
    }

    public final synchronized void clear() {
        this.gD.clear();
    }

    public final synchronized void release(aS aSVar) {
        if (!this.gD.contains(aSVar)) {
            this.gD.add(aSVar);
        }
    }
}
