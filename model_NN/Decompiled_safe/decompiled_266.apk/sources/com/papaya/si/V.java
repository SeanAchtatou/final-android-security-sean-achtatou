package com.papaya.si;

import com.papaya.social.PPYUser;
import java.util.ArrayList;
import org.json.JSONArray;

public final class V extends C0012ak {
    public V() {
        setName(C0037c.getString("Papayafriends"));
        setReserveGroupHeader(true);
    }

    public final void addUserFromJSON(JSONArray jSONArray, int i) {
        if (jSONArray != null) {
            for (int i2 = i; i2 < jSONArray.length(); i2 += 2) {
                add(new C0011aj(jSONArray.optInt(i2), jSONArray.optString(i2 + 1)));
            }
        }
    }

    public final C0011aj findByUserID(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.cq.size()) {
                return null;
            }
            C0011aj ajVar = (C0011aj) this.cq.get(i3);
            if (ajVar.getUserID() == i) {
                return ajVar;
            }
            i2 = i3 + 1;
        }
    }

    public final boolean isFriend(int i) {
        return findByUserID(i) != null;
    }

    public final ArrayList<PPYUser> listUsers() {
        return new ArrayList<>(this.cq);
    }

    public final void updateMiniblog(int i, String str, int i2) {
        C0011aj findByUserID = findByUserID(i);
        if (findByUserID != null) {
            findByUserID.dZ = str;
            findByUserID.ea = (System.currentTimeMillis() / 1000) - ((long) i2);
        }
    }
}
