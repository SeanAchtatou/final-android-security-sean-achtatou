package com.papaya.si;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import java.lang.ref.WeakReference;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONObject;

public class bt {
    private static aI mA = new aI(10);
    private long fR;
    private WeakReference<a> hp;
    protected bv jn;
    private URL mx;
    private byte[] my;
    protected long mz;

    public interface a {
        void connectionFailed(bt btVar, int i);

        void connectionFinished(bt btVar);
    }

    public bt(bv bvVar) {
        this(bvVar, null);
    }

    public bt(bv bvVar, a aVar) {
        this.jn = bvVar;
        setDelegate(aVar);
        this.fR = System.currentTimeMillis();
    }

    public static void delegateConnectionFailed(final bt btVar, final a aVar, final int i) {
        if (aVar == null) {
            return;
        }
        if (C0030bb.isMainThread() || !(aVar instanceof aW)) {
            aVar.connectionFailed(btVar, i);
        } else {
            C0030bb.runInHandlerThread(new Runnable() {
                public final void run() {
                    aVar.connectionFailed(btVar, i);
                }
            });
        }
    }

    public static void delegateConnectionFinished(final bt btVar, final a aVar) {
        if (aVar == null) {
            return;
        }
        if (C0030bb.isMainThread() || !(aVar instanceof aW)) {
            aVar.connectionFinished(btVar);
        } else {
            C0030bb.runInHandlerThread(new Runnable() {
                public final void run() {
                    aVar.connectionFinished(btVar);
                }
            });
        }
    }

    public static Bitmap getCachedBitmap(String str) {
        return mA.get(str);
    }

    public static Bitmap getCachedBitmap(URL url) {
        return mA.get(url.toString());
    }

    public void cancel() {
    }

    public void fireConnectionFailed(int i) {
        delegateConnectionFailed(this, getDelegate(), i);
    }

    public void fireConnectionFailedToRequest(int i) {
        if (this.jn != null) {
            delegateConnectionFailed(this, this.jn.getDelegate(), i);
        }
    }

    public void fireConnectionFinished() {
        delegateConnectionFinished(this, getDelegate());
    }

    public void fireConnectionFinishedToRequest() {
        if (this.jn != null) {
            delegateConnectionFinished(this, this.jn.getDelegate());
        } else {
            X.i("request is null??", new Object[0]);
        }
    }

    public Bitmap getBitmap() {
        byte[] data;
        try {
            String url = this.jn.getUrl().toString();
            Bitmap bitmap = mA.get(url);
            if (bitmap == null && (data = getData()) != null && data.length > 0 && (bitmap = BitmapFactory.decodeByteArray(data, 0, data.length)) != null) {
                mA.put(url, bitmap);
            }
            return bitmap;
        } catch (Exception e) {
            return null;
        }
    }

    public byte[] getData() {
        if (this.my == null && this.jn.getSaveFile() != null) {
            this.my = aU.dataFromFile(this.jn.getSaveFile());
        }
        return this.my;
    }

    public long getDataLength() {
        return this.mz;
    }

    public a getDelegate() {
        if (this.hp != null) {
            return this.hp.get();
        }
        return null;
    }

    public URL getRedirectUrl() {
        return this.mx;
    }

    public bv getRequest() {
        return this.jn;
    }

    public long getStartTime() {
        return this.fR;
    }

    public void setData(byte[] bArr) {
        JSONObject parseJsonObject;
        this.my = bArr;
        setDataLength(this.my == null ? 0 : (long) this.my.length);
        if (this.jn.isDispatchable() && this.my != null) {
            String utf8String = aV.utf8String(this.my, "");
            if (utf8String.contains("__COMPOSITE__") && (parseJsonObject = C0034bf.parseJsonObject(utf8String)) != null) {
                String aVVar = aV.toString(C0034bf.getJsonObject(parseJsonObject, "__ORIGINAL__"), "");
                if (aV.intValue(Build.VERSION.SDK, 0) >= 8) {
                    aVVar = aVVar.replaceAll("\\\\/", "/");
                }
                this.my = aV.getBytes(aVVar);
                setDataLength((long) this.my.length);
                final int jsonInt = C0034bf.getJsonInt(parseJsonObject, "__COMPOSITE__");
                final JSONArray jsonArray = C0034bf.getJsonArray(parseJsonObject, "__PAYLOAD__");
                C0030bb.runInHandlerThread(new Runnable() {
                    public final void run() {
                        if (jsonInt == 1) {
                            C0023av.getInstance().handleResponse(jsonArray);
                            return;
                        }
                        for (int i = 0; i < jsonArray.length(); i++) {
                            C0023av.getInstance().handleResponse(jsonArray.optJSONArray(i));
                        }
                    }
                });
            }
        }
        if (this.my != null && this.jn.getSaveFile() != null) {
            aU.writeBytesToFile(this.jn.getSaveFile(), this.my);
        }
    }

    public void setDataLength(long j) {
        this.mz = j;
    }

    public void setDelegate(a aVar) {
        if (aVar != null) {
            this.hp = new WeakReference<>(aVar);
        } else {
            this.hp = null;
        }
    }

    public void setRedirectUrl(URL url) {
        this.mx = url;
    }

    public void setRequest(bv bvVar) {
        this.jn = bvVar;
    }

    public void setStartTime(long j) {
        this.fR = j;
    }

    public String toString() {
        return "UrlConnection{_request=" + this.jn + ", _redirectUri=" + this.mx + ", _startTime=" + this.fR + '}';
    }
}
