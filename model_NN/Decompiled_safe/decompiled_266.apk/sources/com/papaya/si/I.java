package com.papaya.si;

import com.papaya.chat.ChatActivity;
import com.papaya.view.Action;
import com.papaya.view.ActionsImageButton;

public class I implements ActionsImageButton.Delegate {
    private /* synthetic */ ChatActivity cO;

    public I(ChatActivity chatActivity) {
        this.cO = chatActivity;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void onActionSelected(ActionsImageButton actionsImageButton, Action action) {
        try {
            A session = C0037c.getSession();
            switch (action.id) {
                case 0:
                    if (this.cO.cI != null) {
                        this.cO.closeActiveChat();
                        return;
                    }
                    return;
                case 1:
                    if (this.cO.cI instanceof C0011aj) {
                        C0028b.openHome(this.cO, ((C0011aj) this.cO.cI).getUserID());
                        return;
                    }
                    return;
                case 2:
                case 4:
                default:
                    X.e("unknown action: " + action.id, new Object[0]);
                    return;
                case 3:
                    if (this.cO.cI instanceof C0011aj) {
                        this.cO.showDialog(5);
                        return;
                    }
                    return;
                case 5:
                    if (this.cO.cI instanceof Q) {
                        this.cO.showDialog(2);
                        return;
                    }
                    return;
                case 6:
                    if (this.cO.cI instanceof Q) {
                        Q q = (Q) this.cO.cI;
                        q.addSystemMessage(C0037c.getString("privateoff_info"));
                        q.dk = false;
                        for (int i = 0; i < q.dl.size(); i++) {
                            session.getPrivateChatWhiteList().remove(Integer.valueOf(q.dl.get(i).cW));
                        }
                        q.fireDataStateChanged();
                        return;
                    } else if (this.cO.cI instanceof C0008ag) {
                        this.cO.cI.addSystemMessage(C0037c.getString("chat_msg_sys_private_chat_off"));
                        session.getPrivateChatWhiteList().remove(Integer.valueOf(((C0008ag) this.cO.cI).cW));
                        this.cO.cI.fireDataStateChanged();
                        return;
                    } else {
                        return;
                    }
                case 7:
                    if (this.cO.cI instanceof Q) {
                        Q q2 = (Q) this.cO.cI;
                        q2.addSystemMessage(C0037c.getString("privateon_info"));
                        q2.dk = true;
                        for (int i2 = 0; i2 < q2.dl.size(); i2++) {
                            session.getPrivateChatWhiteList().add(Integer.valueOf(q2.dl.get(i2).cW));
                        }
                        q2.fireDataStateChanged();
                        return;
                    } else if (this.cO.cI instanceof C0008ag) {
                        this.cO.cI.addSystemMessage(C0037c.getString("chat_msg_sys_private_chat_on"));
                        session.getPrivateChatWhiteList().add(Integer.valueOf(((C0008ag) this.cO.cI).cW));
                        this.cO.cI.fireDataStateChanged();
                        return;
                    } else {
                        return;
                    }
                case 8:
                    this.cO.showDialog(4);
                    return;
                case 9:
                    if (this.cO.cI instanceof L) {
                        C0028b.openPRIALink(this.cO, "static_manage_chatgroup?gid=" + ((L) this.cO.cI).cS);
                        return;
                    }
                    return;
                case 10:
                    this.cO.showDialog(3);
                    return;
                case 11:
                    if (this.cO.cI instanceof L) {
                        L l = (L) this.cO.cI;
                        C0037c.send(315, Integer.valueOf(l.cS), 1);
                        l.cU = aV.bitSet(l.cU, 0, 1);
                        l.fireDataStateChanged();
                        return;
                    }
                    return;
                case 12:
                    if (this.cO.cI instanceof L) {
                        L l2 = (L) this.cO.cI;
                        C0037c.send(315, Integer.valueOf(l2.cS), 0);
                        l2.cU = aV.bitSet(l2.cU, 0, 0);
                        l2.fireDataStateChanged();
                        return;
                    }
                    return;
            }
        } catch (Exception e) {
            X.e(e, "Error occurred in onActionSelected", new Object[0]);
        }
        X.e(e, "Error occurred in onActionSelected", new Object[0]);
    }
}
