package com.papaya.si;

import android.graphics.drawable.Drawable;
import com.papaya.base.EntryActivity;
import java.io.Serializable;

public abstract class D extends aG implements Serializable, Comparable<D> {
    public P co;
    protected boolean cp = false;
    public int state;

    public static String getStateString(int i) {
        switch (i) {
            case 0:
                return C0037c.getString("state_offline");
            case 1:
                return C0037c.getString("state_online");
            case 2:
                return C0037c.getString("state_idle");
            case 3:
                return C0037c.getString("state_busy");
            default:
                return "";
        }
    }

    public O addChatMessage(CharSequence charSequence, D d, int i) {
        O o = new O(3, d, charSequence, i);
        addMessage(o);
        if (!this.cp) {
            if (this.co != null) {
                this.co.increaseUnread();
            }
            EntryActivity.a.indicateUnread(d, charSequence);
        }
        return o;
    }

    public void addMessage(O o) {
        if (this.co == null) {
            this.co = new P();
        }
        this.co.add(o);
        C0037c.getSession().getChattings().add(this);
    }

    public O addSelfMessage(CharSequence charSequence) {
        O o = new O(2, A.bL, charSequence, 0);
        addMessage(o);
        return o;
    }

    public O addSystemMessage(CharSequence charSequence) {
        O o = new O(1, null, charSequence, 0);
        addMessage(o);
        return o;
    }

    public int compareTo(D d) {
        if (this == d) {
            return 0;
        }
        if (this.state == 0 && d.state != 0) {
            return 1;
        }
        if (this.state != 0 && d.state == 0) {
            return -1;
        }
        return aV.nullAsEmpty(getTitle()).compareTo(aV.nullAsEmpty(d.getTitle()));
    }

    public Drawable getDefaultDrawable() {
        return null;
    }

    public String getImageUrl() {
        return null;
    }

    public int getState() {
        return this.state;
    }

    public CharSequence getSubtitle() {
        return null;
    }

    public abstract String getTimeLabel();

    public abstract String getTitle();

    public boolean isChatActive() {
        return this.cp;
    }

    public abstract boolean isGrayScaled();

    public void setChatActive(boolean z) {
        this.cp = z;
        if (z && this.co != null) {
            this.co.setUnread(0);
        }
    }

    public void setState(int i) {
        this.state = i;
    }
}
