package com.papaya.si;

import com.adknowledge.superrewards.model.SROffer;
import org.json.JSONArray;
import org.json.JSONObject;

public final class br extends bv {
    public JSONObject mv;

    public br(JSONObject jSONObject) {
        this.mv = jSONObject;
        this.url = C0034bf.createURL(jSONObject.optString("url"));
        this.mH = false;
        JSONArray optJSONArray = jSONObject.optJSONArray("data");
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                JSONArray optJSONArray2 = optJSONArray.optJSONArray(i);
                if (optJSONArray2 != null) {
                    addPostParam(optJSONArray2.optString(1), optJSONArray2.optString(2), optJSONArray2.optInt(0, 0));
                }
            }
        }
    }

    public final String getID() {
        return this.mv.optString(SROffer.ID, "");
    }
}
