package com.papaya.si;

import android.view.View;
import android.widget.TextView;
import com.adknowledge.superrewards.model.SROffer;
import com.papaya.view.CardImageView;

/* renamed from: com.papaya.si.ah  reason: case insensitive filesystem */
public final class C0009ah {
    public CardImageView dq;
    public TextView dr;

    public C0009ah(View view) {
        this.dq = (CardImageView) C0030bb.find(view, SROffer.IMAGE);
        this.dr = (TextView) C0030bb.find(view, "message");
    }
}
