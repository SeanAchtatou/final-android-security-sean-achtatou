package com.papaya.si;

import android.app.Activity;
import android.content.Context;
import com.adknowledge.superrewards.model.SROffer;
import java.util.HashMap;

/* renamed from: com.papaya.si.z  reason: case insensitive filesystem */
public final class C0060z {
    /* access modifiers changed from: private */
    public static Class bD;
    private static final a bE = new a("layout");
    private static final a bF = new a("drawable");
    private static final a bG = new a(SROffer.ID);
    private static final a bH = new a("string");
    private static final a bI = new a("style");
    /* access modifiers changed from: private */
    public static String bl;

    /* renamed from: com.papaya.si.z$a */
    public static final class a {
        private HashMap<String, Integer> bJ;
        private Class bK;
        private String type;

        private a() {
        }

        public a(String str) {
            this.bJ = new HashMap<>();
            this.type = str;
        }

        public static String getAccessToken() {
            return null;
        }

        public static String getAppId() {
            return null;
        }

        public static void initialize(Context context) {
        }

        public static boolean isSessionValid() {
            return false;
        }

        public static void login(Activity activity) {
        }

        public static void logout() {
        }

        public final int get(String str) {
            return get(str, true);
        }

        public final int get(String str, boolean z) {
            String str2 = z ? "ppy_" + str : str;
            Integer num = this.bJ.get(str2);
            if (num == null) {
                Integer.valueOf(0);
                if (this.bK == null && C0060z.bD != null) {
                    try {
                        this.bK = Class.forName(C0060z.bD.getCanonicalName() + "$" + this.type);
                    } catch (ClassNotFoundException e) {
                        X.i(e, "Failed to lookup inner class of " + this.type, new Object[0]);
                    }
                }
                if (this.bK == null) {
                    num = Integer.valueOf(C0037c.getApplicationContext().getResources().getIdentifier(str2, this.type, C0060z.bl));
                } else {
                    try {
                        num = Integer.valueOf(this.bK.getField(str2).getInt(null));
                    } catch (Exception e2) {
                        X.i(e2, "Failed to lookup identifier through reflection: " + str2, new Object[0]);
                        num = 0;
                    }
                }
                if (num.intValue() == 0) {
                    X.e("id of %s for %s is 0", this.type, str2);
                }
                this.bJ.put(str2, num);
            }
            return num.intValue();
        }
    }

    public static int drawableID(String str) {
        try {
            return bF.get(str, true);
        } catch (Exception e) {
            X.e(e, "Failed to get drawable id: " + str, new Object[0]);
            return 0;
        }
    }

    public static int drawableID(String str, boolean z) {
        try {
            return bF.get(str, z);
        } catch (Exception e) {
            X.e(e, "Failed to get drawable id: " + str, new Object[0]);
            return 0;
        }
    }

    public static int id(String str) {
        try {
            return bG.get(str, false);
        } catch (Exception e) {
            X.e(e, "Failed to get id: " + str, new Object[0]);
            return 0;
        }
    }

    public static int layoutID(String str) {
        try {
            return bE.get(str, true);
        } catch (Exception e) {
            X.e(e, "Failed to get layout id: " + str, new Object[0]);
            return 0;
        }
    }

    public static void setup() {
        Class rClass = C0023av.getInstance().getSocialConfig().getRClass();
        bD = rClass;
        if (rClass != null) {
            bl = bD.getPackage().getName();
        }
        if (bl == null) {
            bl = C0056v.bl;
        }
        if (bl == null) {
            bl = C0037c.getApplicationContext().getPackageName();
        }
        X.i("package name of resources is %s", bl);
    }

    public static int stringID(String str) {
        try {
            return bH.get(str, true);
        } catch (Exception e) {
            X.e(e, "Failed to get string id: " + str, new Object[0]);
            return 0;
        }
    }

    public static int styleID(String str) {
        try {
            return bI.get(str, true);
        } catch (Exception e) {
            X.e(e, "Failed to get style id: " + str, new Object[0]);
            return 0;
        }
    }
}
