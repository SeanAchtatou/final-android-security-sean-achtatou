package com.papaya.si;

import com.papaya.si.bt;
import java.net.URL;
import java.util.HashMap;
import org.json.JSONObject;

/* renamed from: com.papaya.si.bh  reason: case insensitive filesystem */
public final class C0036bh implements aM, aW, bt.a {
    private HashMap<String, String> li = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<String, bv> lj = new HashMap<>();
    /* access modifiers changed from: private */
    public bC lk;

    public C0036bh(bC bCVar) {
        this.lk = bCVar;
    }

    /* access modifiers changed from: private */
    public void clearOMT() {
        for (bv cancel : this.lj.values()) {
            cancel.cancel();
        }
        this.lj.clear();
        this.li.clear();
    }

    public final void cancelRequest(final String str) {
        C0030bb.runInHandlerThread(new Runnable() {
            public final void run() {
                bv bvVar = (bv) C0036bh.this.lj.remove(str);
                if (bvVar != null) {
                    bvVar.cancel();
                }
            }
        });
    }

    public final void clear() {
        if (C0030bb.isMainThread()) {
            clearOMT();
        } else {
            C0030bb.runInHandlerThread(new Runnable() {
                public final void run() {
                    C0036bh.this.clearOMT();
                }
            });
        }
    }

    public final synchronized void connectionFailed(bt btVar, int i) {
        bv request = btVar.getRequest();
        if (request instanceof bi) {
            bi biVar = (bi) request;
            if (biVar == this.lj.get(biVar.kj)) {
                this.lj.remove(biVar.kj);
                if (biVar.lv && this.lk.getWebView() != null) {
                    this.lk.getWebView().callJSFunc("ppy_onAjaxFinished('%s', %d, '%s')", biVar.kj, 0, C0034bf.escapeJS(biVar.lw));
                }
            }
        } else if (request instanceof br) {
            br brVar = (br) request;
            if (brVar == this.lj.get(brVar.getID())) {
                this.lj.remove(brVar.getID());
                if (this.lk.getWebView() != null) {
                    this.lk.getWebView().callJSFunc("ppy_onAjaxFinished('%s', %d, '%s')", brVar.getID(), 0, C0034bf.escapeJS(brVar.mv.optString("url")));
                }
            }
        }
    }

    public final void connectionFinished(bt btVar) {
        bv request = btVar.getRequest();
        if (request instanceof bi) {
            bi biVar = (bi) request;
            if (biVar == this.lj.get(biVar.kj)) {
                this.lj.remove(biVar.kj);
                if (biVar.lv && this.lk.getWebView() != null) {
                    this.li.put(biVar.kj, aV.utf8String(btVar.getData(), ""));
                    this.lk.getWebView().callJSFunc("ppy_onAjaxFinished('%s', %d, '%s')", biVar.kj, 1, C0034bf.escapeJS(biVar.lw));
                }
            }
        } else if (request instanceof br) {
            br brVar = (br) request;
            if (brVar == this.lj.get(brVar.getID())) {
                this.lj.remove(brVar.getID());
                if (this.lk.getWebView() != null) {
                    this.li.put(brVar.getID(), aV.utf8String(btVar.getData(), ""));
                }
                this.lk.getWebView().callJSFunc("ppy_onAjaxFinished('%s', %d, '%s')", brVar.getID(), 1, C0034bf.escapeJS(brVar.mv.optString("url")));
            }
        }
    }

    public final String newRemoveContent(String str) {
        return this.li.remove(str);
    }

    public final void startPost(final JSONObject jSONObject) {
        C0030bb.runInHandlerThread(new Runnable() {
            public final void run() {
                br brVar = new br(jSONObject);
                brVar.setRequireSid(C0036bh.this.lk.getWebView().isRequireSid());
                brVar.setDelegate(C0036bh.this);
                C0036bh.this.lj.put(brVar.getID(), brVar);
                brVar.start(false);
            }
        });
    }

    public final void startRequest(String str, String str2, boolean z, String str3, int i, String str4, int i2) {
        final String str5 = str;
        final String str6 = str2;
        final String str7 = str3;
        final String str8 = str4;
        final int i3 = i;
        final int i4 = i2;
        final boolean z2 = z;
        C0030bb.runInHandlerThread(new Runnable() {
            public final void run() {
                String str = str5 == null ? "" : str5;
                C0036bh.this.cancelRequest(str);
                bk webView = C0036bh.this.lk.getWebView();
                if (webView != null) {
                    String str2 = str6;
                    if (!(str7 == null || str8 == null)) {
                        StringBuilder append = aV.acquireStringBuilder(str6.length()).append(str6);
                        if (str6.contains("?")) {
                            append.append('&');
                        } else {
                            append.append('?');
                        }
                        append.append("__db_cache=");
                        try {
                            append.append(C0034bf.encodeUriComponent(new JSONObject().put("name", str7).put("scope", i3).put("key", str8).put("life", i4).toString()));
                        } catch (Exception e) {
                            X.e(e, "Failed to compose db cache json", new Object[0]);
                        }
                        str2 = aV.releaseStringBuilder(append);
                    }
                    URL createURL = C0034bf.createURL(str2, webView.getPapayaURL());
                    if (createURL != null) {
                        bi biVar = new bi();
                        biVar.setUrl(createURL);
                        biVar.setConnectionType(1);
                        biVar.setDelegate(C0036bh.this);
                        biVar.setCacheable(false);
                        biVar.kj = str;
                        biVar.lv = z2;
                        biVar.lw = str6;
                        biVar.setRequireSid(webView.isRequireSid());
                        C0036bh.this.lj.put(str, biVar);
                        biVar.start(false);
                        return;
                    }
                    X.e("Ajax URL is null, %s, %s", str, str6);
                }
            }
        });
    }
}
