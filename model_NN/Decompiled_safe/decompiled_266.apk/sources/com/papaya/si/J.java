package com.papaya.si;

import com.papaya.chat.ChatActivity;
import com.papaya.view.Action;
import com.papaya.view.ActionsImageButton;

public class J implements ActionsImageButton.Delegate {
    private /* synthetic */ ChatActivity cO;

    public J(ChatActivity chatActivity) {
        this.cO = chatActivity;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void onActionSelected(ActionsImageButton actionsImageButton, Action action) {
        C0008ag agVar;
        A session = C0037c.getSession();
        try {
            S s = (S) actionsImageButton.hI;
            switch (action.id) {
                case 1:
                    this.cO.dismissDialog(2);
                    C0028b.openHome(this.cO, s.cW);
                    return;
                case 2:
                    this.cO.dismissDialog(2);
                    session.getPrivateChatWhiteList().add(Integer.valueOf(s.cW));
                    D findByUserID = session.getFriends().findByUserID(s.cW);
                    if (findByUserID == null) {
                        findByUserID = session.getPrivateChats().findBySID(s.dm);
                    }
                    if (findByUserID == null) {
                        C0008ag agVar2 = new C0008ag();
                        agVar2.dn = s.dn;
                        agVar2.cW = s.cW;
                        agVar2.dm = s.dm;
                        session.getPrivateChats().add(agVar2);
                        agVar = agVar2;
                    } else {
                        agVar = findByUserID;
                    }
                    session.getChattings().add(agVar);
                    this.cO.refreshWithCard(agVar, false);
                    agVar.fireDataStateChanged();
                    session.fireDataStateChanged();
                    return;
                case 3:
                default:
                    X.e("unknown action id: " + action.id, new Object[0]);
                    return;
                case 4:
                    this.cO.dismissDialog(2);
                    if (this.cO.cI instanceof Q) {
                        C0037c.send(48, Integer.valueOf(((Q) this.cO.cI).df), Integer.valueOf(s.cW));
                        return;
                    }
                    return;
            }
        } catch (Exception e) {
            X.e(e, "Failed in onActionSelected", new Object[0]);
        }
        X.e(e, "Failed in onActionSelected", new Object[0]);
    }
}
