package com.papaya.si;

import com.inmobi.androidsdk.InMobiAdDelegate;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

/* renamed from: com.papaya.si.bg  reason: case insensitive filesystem */
public final class C0035bg {
    private static String[] hv = {"91", "62", "254", "92"};
    private static long[] hw = null;

    /* renamed from: com.papaya.si.bg$a */
    static final class a {
        private byte[] hA;
        private int hx;
        private int hy;
        private int hz;

        /* synthetic */ a() {
            this((byte) 0);
        }

        private a(byte b) {
        }

        private final int a(int i) {
            while (this.hx < i) {
                int i2 = this.hy;
                byte[] bArr = this.hA;
                int i3 = this.hz;
                this.hz = i3 + 1;
                this.hy = i2 | ((bArr[i3] & 255) << this.hx);
                this.hx += 8;
            }
            int i4 = this.hy & ((1 << i) - 1);
            this.hy >>= i;
            this.hx -= i;
            return i4;
        }

        private final int a(int[][] iArr) {
            int i = 0;
            int i2 = 0;
            while (true) {
                i2 = (i2 << 1) | a(1);
                i++;
                int i3 = 0;
                while (true) {
                    if (i3 < iArr.length) {
                        if (iArr[i3][0] == i && iArr[i3][1] == i2) {
                            return i3;
                        }
                        i3++;
                    }
                }
            }
        }

        private static int[][] a(int[] iArr) {
            int i = 0;
            for (int i2 = 0; i2 < iArr.length; i2++) {
                if (iArr[i2] > i) {
                    i = iArr[i2];
                }
            }
            int i3 = i + 1;
            int[] iArr2 = new int[i3];
            int[] iArr3 = new int[i3];
            for (int i4 = 0; i4 < i3; i4++) {
                iArr2[i4] = 0;
                iArr3[i4] = 0;
            }
            for (int i5 : iArr) {
                iArr2[i5] = iArr2[i5] + 1;
            }
            for (int i6 = 2; i6 < i3; i6++) {
                iArr3[i6] = (iArr3[i6 - 1] + iArr2[i6 - 1]) << 1;
            }
            int[][] iArr4 = (int[][]) Array.newInstance(Integer.TYPE, iArr.length, 2);
            for (int i7 = 0; i7 < iArr.length; i7++) {
                iArr4[i7][0] = iArr[i7];
                int[] iArr5 = iArr4[i7];
                int i8 = iArr[i7];
                int i9 = iArr3[i8];
                iArr3[i8] = i9 + 1;
                iArr5[1] = i9;
            }
            return iArr4;
        }

        /* JADX WARN: Type inference failed for: r11v9, types: [int] */
        /* access modifiers changed from: package-private */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final byte[] a(byte[] r24, int r25) {
            /*
                r23 = this;
                r5 = 29
                int[] r5 = new int[r5]
                r5 = {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0} // fill-array
                r6 = 29
                int[] r6 = new int[r6]
                r6 = {3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31, 35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258} // fill-array
                r7 = 30
                int[] r7 = new int[r7]
                r7 = {0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13} // fill-array
                r8 = 30
                int[] r8 = new int[r8]
                r8 = {1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97, 129, 193, 257, 385, 513, 769, 1025, 1537, 2049, 3073, 4097, 6145, 8193, 12289, 16385, 24577} // fill-array
                r9 = 19
                int[] r9 = new int[r9]
                r9 = {16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15} // fill-array
                r10 = 0
                r11 = 0
                r12 = 2048(0x800, float:2.87E-42)
                byte[] r12 = new byte[r12]
                r13 = 0
                r0 = r13
                r1 = r23
                r1.hx = r0
                r13 = 0
                r0 = r13
                r1 = r23
                r1.hy = r0
                r0 = r24
                r1 = r23
                r1.hA = r0
                r0 = r25
                r1 = r23
                r1.hz = r0
                r22 = r12
                r12 = r10
                r10 = r22
            L_0x0046:
                if (r12 != 0) goto L_0x0302
                r12 = 1
                r0 = r23
                r1 = r12
                int r12 = r0.a(r1)
                r13 = 2
                r0 = r23
                r1 = r13
                int r13 = r0.a(r1)
                if (r13 != 0) goto L_0x00d0
                r13 = 0
                r0 = r13
                r1 = r23
                r1.hx = r0
                r0 = r13
                r1 = r23
                r1.hy = r0
                r0 = r23
                byte[] r0 = r0.hA
                r13 = r0
                r0 = r23
                int r0 = r0.hz
                r14 = r0
                byte r13 = r13[r14]
                r13 = r13 & 255(0xff, float:3.57E-43)
                r0 = r23
                byte[] r0 = r0.hA
                r14 = r0
                r0 = r23
                int r0 = r0.hz
                r15 = r0
                int r15 = r15 + 1
                byte r14 = r14[r15]
                r14 = r14 & 255(0xff, float:3.57E-43)
                int r14 = r14 << 8
                r13 = r13 | r14
                r0 = r23
                int r0 = r0.hz
                r14 = r0
                int r14 = r14 + 4
                r0 = r14
                r1 = r23
                r1.hz = r0
                r14 = 0
            L_0x0093:
                if (r14 >= r13) goto L_0x0046
                int r15 = r11 + 1
                r0 = r23
                byte[] r0 = r0.hA
                r16 = r0
                r0 = r23
                int r0 = r0.hz
                r17 = r0
                int r18 = r17 + 1
                r0 = r18
                r1 = r23
                r1.hz = r0
                byte r16 = r16[r17]
                r10[r11] = r16
                int r11 = r10.length
                if (r15 < r11) goto L_0x00cb
                int r11 = r10.length
                int r11 = r11 + 2048
                byte[] r11 = new byte[r11]
                r16 = 0
                r17 = 0
                r0 = r10
                int r0 = r0.length
                r18 = r0
                r0 = r10
                r1 = r16
                r2 = r11
                r3 = r17
                r4 = r18
                java.lang.System.arraycopy(r0, r1, r2, r3, r4)
                r10 = r11
            L_0x00cb:
                int r11 = r14 + 1
                r14 = r11
                r11 = r15
                goto L_0x0093
            L_0x00d0:
                r14 = 2
                if (r13 != r14) goto L_0x0229
                r13 = 5
                r0 = r23
                r1 = r13
                int r13 = r0.a(r1)
                r14 = 5
                r0 = r23
                r1 = r14
                int r14 = r0.a(r1)
                r15 = 4
                r0 = r23
                r1 = r15
                int r15 = r0.a(r1)
                r16 = 19
                r0 = r16
                int[] r0 = new int[r0]
                r16 = r0
                r17 = 0
            L_0x00f5:
                r18 = 19
                r0 = r17
                r1 = r18
                if (r0 >= r1) goto L_0x0104
                r18 = 0
                r16[r17] = r18
                int r17 = r17 + 1
                goto L_0x00f5
            L_0x0104:
                r17 = 0
            L_0x0106:
                int r18 = r15 + 4
                r0 = r17
                r1 = r18
                if (r0 >= r1) goto L_0x011f
                r18 = r9[r17]
                r19 = 3
                r0 = r23
                r1 = r19
                int r19 = r0.a(r1)
                r16[r18] = r19
                int r17 = r17 + 1
                goto L_0x0106
            L_0x011f:
                int[][] r15 = a(r16)
                int r16 = r13 + r14
                r0 = r16
                int r0 = r0 + 258
                r16 = r0
                r0 = r16
                int[] r0 = new int[r0]
                r16 = r0
                r17 = 0
            L_0x0133:
                r0 = r16
                int r0 = r0.length
                r18 = r0
                r0 = r17
                r1 = r18
                if (r0 >= r1) goto L_0x01ae
                r0 = r23
                r1 = r15
                int r18 = r0.a(r1)
                r19 = 16
                r0 = r18
                r1 = r19
                if (r0 >= r1) goto L_0x0154
                int r19 = r17 + 1
                r16[r17] = r18
                r17 = r19
                goto L_0x0133
            L_0x0154:
                r19 = 16
                r0 = r18
                r1 = r19
                if (r0 != r1) goto L_0x0181
                r18 = 2
                r0 = r23
                r1 = r18
                int r18 = r0.a(r1)
                int r18 = r18 + 3
                r19 = 1
                int r19 = r17 - r19
                r19 = r16[r19]
                r16[r17] = r19
            L_0x0170:
                r19 = 1
            L_0x0172:
                r0 = r19
                r1 = r18
                if (r0 >= r1) goto L_0x01ab
                int r20 = r17 + r19
                r21 = r16[r17]
                r16[r20] = r21
                int r19 = r19 + 1
                goto L_0x0172
            L_0x0181:
                r19 = 17
                r0 = r18
                r1 = r19
                if (r0 != r1) goto L_0x019a
                r18 = 3
                r0 = r23
                r1 = r18
                int r18 = r0.a(r1)
                int r18 = r18 + 3
                r19 = 0
                r16[r17] = r19
                goto L_0x0170
            L_0x019a:
                r18 = 7
                r0 = r23
                r1 = r18
                int r18 = r0.a(r1)
                int r18 = r18 + 11
                r19 = 0
                r16[r17] = r19
                goto L_0x0170
            L_0x01ab:
                int r17 = r17 + r18
                goto L_0x0133
            L_0x01ae:
                int r15 = r13 + 257
                int[] r15 = new int[r15]
                int r17 = r14 + 1
                r0 = r17
                int[] r0 = new int[r0]
                r17 = r0
                r18 = 0
                r19 = 0
                r0 = r13
                int r0 = r0 + 257
                r20 = r0
                r0 = r16
                r1 = r18
                r2 = r15
                r3 = r19
                r4 = r20
                java.lang.System.arraycopy(r0, r1, r2, r3, r4)
                int r13 = r13 + 257
                r18 = 0
                int r14 = r14 + 1
                r0 = r16
                r1 = r13
                r2 = r17
                r3 = r18
                r4 = r14
                java.lang.System.arraycopy(r0, r1, r2, r3, r4)
                int[][] r13 = a(r15)
                int[][] r14 = a(r17)
                r22 = r14
                r14 = r11
                r11 = r13
                r13 = r10
                r10 = r22
            L_0x01ef:
                r0 = r23
                r1 = r11
                int r15 = r0.a(r1)
                r16 = 256(0x100, float:3.59E-43)
                r0 = r15
                r1 = r16
                if (r0 == r1) goto L_0x02fe
                r16 = 256(0x100, float:3.59E-43)
                r0 = r15
                r1 = r16
                if (r0 >= r1) goto L_0x02af
                int r16 = r14 + 1
                byte r15 = (byte) r15
                r13[r14] = r15
                int r14 = r13.length
                r0 = r16
                r1 = r14
                if (r0 < r1) goto L_0x0313
                int r14 = r13.length
                int r14 = r14 + 2048
                byte[] r14 = new byte[r14]
                r15 = 0
                r17 = 0
                r0 = r13
                int r0 = r0.length
                r18 = r0
                r0 = r13
                r1 = r15
                r2 = r14
                r3 = r17
                r4 = r18
                java.lang.System.arraycopy(r0, r1, r2, r3, r4)
                r13 = r14
                r14 = r16
                goto L_0x01ef
            L_0x0229:
                r13 = 288(0x120, float:4.04E-43)
                r14 = 2
                int[] r13 = new int[]{r13, r14}
                java.lang.Class r14 = java.lang.Integer.TYPE
                java.lang.Object r24 = java.lang.reflect.Array.newInstance(r14, r13)
                int[][] r24 = (int[][]) r24
                r13 = 30
                r14 = 2
                int[] r13 = new int[]{r13, r14}
                java.lang.Class r14 = java.lang.Integer.TYPE
                java.lang.Object r25 = java.lang.reflect.Array.newInstance(r14, r13)
                int[][] r25 = (int[][]) r25
                r13 = 0
            L_0x0248:
                r14 = 288(0x120, float:4.04E-43)
                if (r13 >= r14) goto L_0x029b
                r14 = 143(0x8f, float:2.0E-43)
                if (r13 > r14) goto L_0x0261
                r14 = r24[r13]
                r15 = 0
                r16 = 8
                r14[r15] = r16
                r14 = r24[r13]
                r15 = 1
                int r16 = r13 + 48
                r14[r15] = r16
            L_0x025e:
                int r13 = r13 + 1
                goto L_0x0248
            L_0x0261:
                r14 = 255(0xff, float:3.57E-43)
                if (r13 > r14) goto L_0x0277
                r14 = r24[r13]
                r15 = 0
                r16 = 9
                r14[r15] = r16
                r14 = r24[r13]
                r15 = 1
                r0 = r13
                int r0 = r0 + 256
                r16 = r0
                r14[r15] = r16
                goto L_0x025e
            L_0x0277:
                r14 = 279(0x117, float:3.91E-43)
                if (r13 > r14) goto L_0x028c
                r14 = r24[r13]
                r15 = 0
                r16 = 7
                r14[r15] = r16
                r14 = r24[r13]
                r15 = 1
                r16 = 256(0x100, float:3.59E-43)
                int r16 = r13 - r16
                r14[r15] = r16
                goto L_0x025e
            L_0x028c:
                r14 = r24[r13]
                r15 = 0
                r16 = 8
                r14[r15] = r16
                r14 = r24[r13]
                r15 = 1
                int r16 = r13 + -88
                r14[r15] = r16
                goto L_0x025e
            L_0x029b:
                r13 = 0
            L_0x029c:
                r14 = 30
                if (r13 >= r14) goto L_0x0317
                r14 = r25[r13]
                r15 = 0
                r16 = 5
                r14[r15] = r16
                r14 = r25[r13]
                r15 = 1
                r14[r15] = r13
                int r13 = r13 + 1
                goto L_0x029c
            L_0x02af:
                int r15 = r15 + -257
                r16 = r6[r15]
                r15 = r5[r15]
                r0 = r23
                r1 = r15
                int r15 = r0.a(r1)
                int r15 = r15 + r16
                r0 = r23
                r1 = r10
                int r16 = r0.a(r1)
                r17 = r8[r16]
                r16 = r7[r16]
                r0 = r23
                r1 = r16
                int r16 = r0.a(r1)
                int r16 = r16 + r17
            L_0x02d3:
                int r17 = r15 + -1
                if (r15 <= 0) goto L_0x01ef
                int r15 = r14 - r16
                byte r15 = r13[r15]
                r13[r14] = r15
                int r14 = r14 + 1
                int r15 = r13.length
                if (r14 < r15) goto L_0x0310
                int r15 = r13.length
                int r15 = r15 + 2048
                byte[] r15 = new byte[r15]
                r18 = 0
                r19 = 0
                r0 = r13
                int r0 = r0.length
                r20 = r0
                r0 = r13
                r1 = r18
                r2 = r15
                r3 = r19
                r4 = r20
                java.lang.System.arraycopy(r0, r1, r2, r3, r4)
                r13 = r15
                r15 = r17
                goto L_0x02d3
            L_0x02fe:
                r10 = r13
                r11 = r14
                goto L_0x0046
            L_0x0302:
                r5 = 0
                r0 = r5
                r1 = r23
                r1.hA = r0
                byte[] r5 = new byte[r11]
                r6 = 0
                r7 = 0
                java.lang.System.arraycopy(r10, r6, r5, r7, r11)
                return r5
            L_0x0310:
                r15 = r17
                goto L_0x02d3
            L_0x0313:
                r14 = r16
                goto L_0x01ef
            L_0x0317:
                r13 = r10
                r14 = r11
                r10 = r25
                r11 = r24
                goto L_0x01ef
            */
            throw new UnsupportedOperationException("Method not decompiled: com.papaya.si.C0035bg.a.a(byte[], int):byte[]");
        }
    }

    /* renamed from: com.papaya.si.bg$b */
    static final class b {
        private static byte[] hB = {Byte.MIN_VALUE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        private byte[] buffer;
        private int[] hC;
        private long hD;
        private byte[] hE;

        public b() {
            this.hC = null;
            this.hD = 0;
            this.buffer = null;
            this.hE = null;
            this.hC = new int[4];
            this.buffer = new byte[64];
            this.hD = 0;
            this.hC[0] = 1732584193;
            this.hC[1] = -271733879;
            this.hC[2] = -1732584194;
            this.hC[3] = 271733878;
        }

        private static int a(int i, int i2) {
            return (i << i2) | (i >>> (32 - i2));
        }

        private static int a(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
            return a(((i2 & i3) | ((i2 ^ -1) & i4)) + i5 + i7 + i, i6) + i2;
        }

        private static int b(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
            return a(((i2 & i4) | ((i4 ^ -1) & i3)) + i5 + i7 + i, i6) + i2;
        }

        private final void b(byte[] bArr, int i) {
            int i2 = this.hC[0];
            int i3 = this.hC[1];
            int i4 = this.hC[2];
            int i5 = this.hC[3];
            int[] iArr = new int[16];
            int i6 = 0;
            for (int i7 = 0; i7 < 64; i7 += 4) {
                iArr[i6] = (bArr[i + i7] & 255) | ((bArr[(i + i7) + 1] & 255) << 8) | ((bArr[(i + i7) + 2] & 255) << 16) | ((bArr[(i + i7) + 3] & 255) << 24);
                i6++;
            }
            int a = a(i2, i3, i4, i5, iArr[0], 7, -680876936);
            int a2 = a(i5, a, i3, i4, iArr[1], 12, -389564586);
            int a3 = a(i4, a2, a, i3, iArr[2], 17, 606105819);
            int a4 = a(i3, a3, a2, a, iArr[3], 22, -1044525330);
            int a5 = a(a, a4, a3, a2, iArr[4], 7, -176418897);
            int a6 = a(a2, a5, a4, a3, iArr[5], 12, 1200080426);
            int a7 = a(a3, a6, a5, a4, iArr[6], 17, -1473231341);
            int a8 = a(a4, a7, a6, a5, iArr[7], 22, -45705983);
            int a9 = a(a5, a8, a7, a6, iArr[8], 7, 1770035416);
            int a10 = a(a6, a9, a8, a7, iArr[9], 12, -1958414417);
            int a11 = a(a7, a10, a9, a8, iArr[10], 17, -42063);
            int a12 = a(a8, a11, a10, a9, iArr[11], 22, -1990404162);
            int a13 = a(a9, a12, a11, a10, iArr[12], 7, 1804603682);
            int a14 = a(a10, a13, a12, a11, iArr[13], 12, -40341101);
            int a15 = a(a11, a14, a13, a12, iArr[14], 17, -1502002290);
            int a16 = a(a12, a15, a14, a13, iArr[15], 22, 1236535329);
            int b = b(a13, a16, a15, a14, iArr[1], 5, -165796510);
            int b2 = b(a14, b, a16, a15, iArr[6], 9, -1069501632);
            int b3 = b(a15, b2, b, a16, iArr[11], 14, 643717713);
            int b4 = b(a16, b3, b2, b, iArr[0], 20, -373897302);
            int b5 = b(b, b4, b3, b2, iArr[5], 5, -701558691);
            int b6 = b(b2, b5, b4, b3, iArr[10], 9, 38016083);
            int b7 = b(b3, b6, b5, b4, iArr[15], 14, -660478335);
            int b8 = b(b4, b7, b6, b5, iArr[4], 20, -405537848);
            int b9 = b(b5, b8, b7, b6, iArr[9], 5, 568446438);
            int b10 = b(b6, b9, b8, b7, iArr[14], 9, -1019803690);
            int b11 = b(b7, b10, b9, b8, iArr[3], 14, -187363961);
            int b12 = b(b8, b11, b10, b9, iArr[8], 20, 1163531501);
            int b13 = b(b9, b12, b11, b10, iArr[13], 5, -1444681467);
            int b14 = b(b10, b13, b12, b11, iArr[2], 9, -51403784);
            int b15 = b(b11, b14, b13, b12, iArr[7], 14, 1735328473);
            int b16 = b(b12, b15, b14, b13, iArr[12], 20, -1926607734);
            int c = c(b13, b16, b15, b14, iArr[5], 4, -378558);
            int c2 = c(b14, c, b16, b15, iArr[8], 11, -2022574463);
            int c3 = c(b15, c2, c, b16, iArr[11], 16, 1839030562);
            int c4 = c(b16, c3, c2, c, iArr[14], 23, -35309556);
            int c5 = c(c, c4, c3, c2, iArr[1], 4, -1530992060);
            int c6 = c(c2, c5, c4, c3, iArr[4], 11, 1272893353);
            int c7 = c(c3, c6, c5, c4, iArr[7], 16, -155497632);
            int c8 = c(c4, c7, c6, c5, iArr[10], 23, -1094730640);
            int c9 = c(c5, c8, c7, c6, iArr[13], 4, 681279174);
            int c10 = c(c6, c9, c8, c7, iArr[0], 11, -358537222);
            int c11 = c(c7, c10, c9, c8, iArr[3], 16, -722521979);
            int c12 = c(c8, c11, c10, c9, iArr[6], 23, 76029189);
            int c13 = c(c9, c12, c11, c10, iArr[9], 4, -640364487);
            int c14 = c(c10, c13, c12, c11, iArr[12], 11, -421815835);
            int c15 = c(c11, c14, c13, c12, iArr[15], 16, 530742520);
            int c16 = c(c12, c15, c14, c13, iArr[2], 23, -995338651);
            int d = d(c13, c16, c15, c14, iArr[0], 6, -198630844);
            int d2 = d(c14, d, c16, c15, iArr[7], 10, 1126891415);
            int d3 = d(c15, d2, d, c16, iArr[14], 15, -1416354905);
            int d4 = d(c16, d3, d2, d, iArr[5], 21, -57434055);
            int d5 = d(d, d4, d3, d2, iArr[12], 6, 1700485571);
            int d6 = d(d2, d5, d4, d3, iArr[3], 10, -1894986606);
            int d7 = d(d3, d6, d5, d4, iArr[10], 15, -1051523);
            int d8 = d(d4, d7, d6, d5, iArr[1], 21, -2054922799);
            int d9 = d(d5, d8, d7, d6, iArr[8], 6, 1873313359);
            int d10 = d(d6, d9, d8, d7, iArr[15], 10, -30611744);
            int d11 = d(d7, d10, d9, d8, iArr[6], 15, -1560198380);
            int d12 = d(d8, d11, d10, d9, iArr[13], 21, 1309151649);
            int d13 = d(d9, d12, d11, d10, iArr[4], 6, -145523070);
            int d14 = d(d10, d13, d12, d11, iArr[11], 10, -1120210379);
            int d15 = d(d11, d14, d13, d12, iArr[2], 15, 718787259);
            int d16 = d(d12, d15, d14, d13, iArr[9], 21, -343485551);
            int[] iArr2 = this.hC;
            iArr2[0] = iArr2[0] + d13;
            int[] iArr3 = this.hC;
            iArr3[1] = d16 + iArr3[1];
            int[] iArr4 = this.hC;
            iArr4[2] = iArr4[2] + d15;
            int[] iArr5 = this.hC;
            iArr5[3] = iArr5[3] + d14;
        }

        private static int c(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
            return a(((i2 ^ i3) ^ i4) + i5 + i7 + i, i6) + i2;
        }

        /* access modifiers changed from: private */
        public final void c(byte[] bArr, int i) {
            int i2;
            int i3;
            int i4 = ((int) (this.hD >> 3)) & 63;
            this.hD += (long) (i << 3);
            int i5 = 64 - i4;
            if (i >= i5) {
                System.arraycopy(bArr, 0, this.buffer, i4, i5);
                b(this.buffer, 0);
                i3 = i5;
                while (i3 + 63 < i) {
                    b(bArr, i3);
                    i3 += 64;
                }
                i2 = 0;
            } else {
                i2 = i4;
                i3 = 0;
            }
            System.arraycopy(bArr, i3, this.buffer, i2, i - i3);
        }

        private static int d(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
            return a((((i4 ^ -1) | i2) ^ i3) + i5 + i7 + i, i6) + i2;
        }

        public final String c() {
            if (this.hE == null) {
                byte[] bArr = new byte[8];
                for (int i = 0; i < 8; i++) {
                    bArr[i] = (byte) ((int) ((this.hD >>> (i * 8)) & 255));
                }
                int i2 = ((int) (this.hD >> 3)) & 63;
                c(hB, i2 < 56 ? 56 - i2 : 120 - i2);
                c(bArr, 8);
                int[] iArr = this.hC;
                byte[] bArr2 = new byte[16];
                int i3 = 0;
                for (int i4 = 0; i4 < 16; i4 += 4) {
                    bArr2[i4] = (byte) (iArr[i3] & 255);
                    bArr2[i4 + 1] = (byte) ((iArr[i3] >> 8) & 255);
                    bArr2[i4 + 2] = (byte) ((iArr[i3] >> 16) & 255);
                    bArr2[i4 + 3] = (byte) ((iArr[i3] >> 24) & 255);
                    i3++;
                }
                this.hE = bArr2;
            }
            byte[] bArr3 = this.hE;
            StringBuilder acquireStringBuilder = aV.acquireStringBuilder(bArr3.length * 2);
            for (int i5 = 0; i5 < bArr3.length; i5++) {
                int i6 = (bArr3[i5] & 240) >> 4;
                byte b = bArr3[i5] & 15;
                acquireStringBuilder.append(new Character((char) (i6 > 9 ? (i6 + 97) - 10 : i6 + 48)));
                acquireStringBuilder.append(new Character((char) (b > 9 ? (b + 97) - 10 : b + 48)));
            }
            return aV.releaseStringBuilder(acquireStringBuilder);
        }

        public final void close() {
            this.buffer = null;
            this.hC = null;
            this.hE = null;
        }
    }

    public static final String URLQueryEncode(String str) {
        int length = str.length();
        StringBuilder acquireStringBuilder = aV.acquireStringBuilder(0);
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if ((charAt >= 'a' && charAt <= 'z') || ((charAt >= 'A' && charAt <= 'Z') || ((charAt >= '0' && charAt <= '9') || "-_.!~*'()%".indexOf(charAt) != -1))) {
                acquireStringBuilder.append(charAt);
            } else if (charAt < 128) {
                acquireStringBuilder.append(hex(charAt));
            } else if (charAt <= 127 || charAt >= 2048) {
                acquireStringBuilder.append(hex((char) ((charAt >> 12) | 224)));
                acquireStringBuilder.append(hex((char) (((charAt >> 6) & 63) | 128)));
                acquireStringBuilder.append(hex((char) ((charAt & '?') | 128)));
            } else {
                acquireStringBuilder.append(hex((char) ((charAt >> 6) | 192)));
                acquireStringBuilder.append(hex((char) ((charAt & '?') | 128)));
            }
        }
        return aV.releaseStringBuilder(acquireStringBuilder);
    }

    public static final HashMap copy(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        for (Object next : hashMap.keySet()) {
            hashMap2.put(next, hashMap.get(next));
        }
        return hashMap2;
    }

    public static final Vector copy(Vector vector) {
        Vector vector2 = new Vector();
        for (int i = 0; i < vector.size(); i++) {
            vector2.addElement(vector.elementAt(i));
        }
        return vector2;
    }

    public static final byte[] crc32(byte[] bArr, int i, int i2) {
        long update_crc = update_crc(4294967295L, bArr, i, i2) ^ 4294967295L;
        byte[] bArr2 = new byte[4];
        for (int i3 = 0; i3 < 4; i3++) {
            bArr2[i3] = (byte) ((int) ((update_crc >> ((3 - i3) << 3)) & 255));
        }
        return bArr2;
    }

    public static final String cutzero(String str, String str2) {
        String str3 = str2;
        for (String startsWith : hv) {
            if (str.startsWith(startsWith)) {
                while (str3.startsWith("0")) {
                    str3 = str3.substring(1);
                }
            }
        }
        return str + str3;
    }

    public static final String decode(String str) {
        String trim = str.trim();
        while (true) {
            int indexOf = trim.indexOf(10);
            if (indexOf == -1) {
                break;
            }
            trim = trim.substring(0, indexOf) + trim.substring(indexOf + 1);
        }
        while (true) {
            int indexOf2 = trim.indexOf(13);
            if (indexOf2 == -1) {
                break;
            }
            trim = trim.substring(0, indexOf2) + trim.substring(indexOf2 + 1);
        }
        String str2 = trim;
        int i = 0;
        while (true) {
            int indexOf3 = str2.indexOf(38, i);
            if (indexOf3 == -1) {
                return str2;
            }
            int indexOf4 = str2.indexOf(59, indexOf3);
            if (indexOf4 == -1 || indexOf4 > indexOf3 + 10) {
                i = indexOf3 + 1;
            } else {
                String substring = str2.substring(indexOf3 + 1, indexOf4);
                if (substring.length() != 0) {
                    if (substring.charAt(0) == '#') {
                        if (substring.length() != 1) {
                            if (substring.charAt(1) == 'x') {
                                substring = new String(new char[]{(char) Integer.parseInt(substring.substring(2), 16)});
                            } else {
                                substring = new String(new char[]{(char) Integer.parseInt(substring.substring(1))});
                            }
                        }
                    } else if (substring.equals("nbsp")) {
                        substring = " ";
                    } else if (substring.equals("amp")) {
                        substring = "&";
                    } else if (substring.equals("apos")) {
                        substring = "'";
                    } else if (substring.equals("gt")) {
                        substring = ">";
                    } else if (substring.equals("lt")) {
                        substring = "<";
                    } else if (substring.equals("quot")) {
                        substring = "\"";
                    } else if (substring.equals("shy")) {
                        substring = "-";
                    }
                    substring = "";
                }
                str2 = str2.substring(0, indexOf3) + substring + str2.substring(indexOf4 + 1);
                i = indexOf3 + substring.length();
            }
        }
    }

    public static final byte[] deflate(byte[] bArr) {
        return new a().a(bArr, 0);
    }

    public static final void del(String str) {
        C0037c.getApplicationContext().deleteFile(str);
    }

    public static final int dumplen(Object obj) {
        if (obj == null) {
            return 1;
        }
        if (obj instanceof Integer) {
            int intValue = ((Integer) obj).intValue();
            if (intValue < -128 || intValue > 127) {
                return (intValue < -32768 || intValue > 32767) ? 5 : 3;
            }
            return 2;
        } else if (obj instanceof Vector) {
            Vector vector = (Vector) obj;
            int i = vector.size() <= 255 ? 2 : 3;
            for (int i2 = 0; i2 < vector.size(); i2++) {
                i += dumplen(vector.elementAt(i2));
            }
            return i;
        } else if (obj instanceof byte[]) {
            byte[] bArr = (byte[]) obj;
            return (bArr.length <= 255 ? 2 : bArr.length <= 65535 ? 3 : 5) + bArr.length;
        } else if (obj instanceof int[]) {
            return (((int[]) obj).length * 4) + 3;
        } else {
            if (obj instanceof String) {
                return (((String) obj).length() * 2) + 3;
            }
            if (obj instanceof String[]) {
                String[] strArr = (String[]) obj;
                int i3 = 3;
                for (int i4 = 0; i4 < strArr.length; i4++) {
                    i3 = strArr[i4] != null ? i3 + (strArr[i4].length() * 2) + 2 : i3 + 2;
                }
                return i3;
            } else if (obj instanceof Long) {
                return 9;
            } else {
                if (!(obj instanceof HashMap)) {
                    return 0;
                }
                HashMap hashMap = (HashMap) obj;
                int i5 = hashMap.size() <= 255 ? 2 : 3;
                for (Object next : hashMap.keySet()) {
                    i5 += dumplen(hashMap.get(next)) + dumplen(next);
                }
                return i5;
            }
        }
    }

    public static final void dumps(OutputStream outputStream, Object obj) throws IOException {
        if (obj == null) {
            outputStream.write(0);
        } else if (obj instanceof Integer) {
            int intValue = ((Integer) obj).intValue();
            if (intValue >= -128 && intValue <= 127) {
                outputStream.write(1);
                outputStream.write(intValue & 255);
            } else if (intValue < -32768 || intValue > 32767) {
                outputStream.write(3);
                outputStream.write(intValue & 255);
                outputStream.write((intValue >> 8) & 255);
                outputStream.write((intValue >> 16) & 255);
                outputStream.write((intValue >> 24) & 255);
            } else {
                outputStream.write(2);
                outputStream.write(intValue & 255);
                outputStream.write((intValue >> 8) & 255);
            }
        } else if (obj instanceof Vector) {
            Vector vector = (Vector) obj;
            if (vector.size() <= 255) {
                outputStream.write(4);
                outputStream.write(vector.size());
            } else {
                outputStream.write(5);
                outputStream.write(vector.size() & 255);
                outputStream.write((vector.size() >> 8) & 255);
            }
            for (int i = 0; i < vector.size(); i++) {
                dumps(outputStream, vector.elementAt(i));
            }
        } else if (obj instanceof byte[]) {
            byte[] bArr = (byte[]) obj;
            if (bArr.length <= 255) {
                outputStream.write(6);
                outputStream.write(bArr.length);
            } else if (bArr.length <= 65535) {
                outputStream.write(7);
                outputStream.write(bArr.length & 255);
                outputStream.write((bArr.length >> 8) & 255);
            } else {
                outputStream.write(8);
                outputStream.write(bArr.length & 255);
                outputStream.write((bArr.length >> 8) & 255);
                outputStream.write((bArr.length >> 16) & 255);
                outputStream.write((bArr.length >> 24) & 255);
            }
            outputStream.write(bArr);
        } else if (obj instanceof int[]) {
            int[] iArr = (int[]) obj;
            outputStream.write(9);
            outputStream.write(iArr.length & 255);
            outputStream.write((iArr.length >> 8) & 255);
            for (int i2 = 0; i2 < iArr.length; i2++) {
                outputStream.write(iArr[i2]);
                outputStream.write(iArr[i2] >> 8);
                outputStream.write(iArr[i2] >> 16);
                outputStream.write(iArr[i2] >> 24);
            }
        } else if (obj instanceof String) {
            String str = (String) obj;
            outputStream.write(10);
            outputStream.write(str.length() & 255);
            outputStream.write((str.length() >> 8) & 255);
            for (int i3 = 0; i3 < str.length(); i3++) {
                outputStream.write(str.charAt(i3));
                outputStream.write(str.charAt(i3) >> 8);
            }
        } else if (obj instanceof String[]) {
            String[] strArr = (String[]) obj;
            outputStream.write(11);
            outputStream.write(strArr.length & 255);
            outputStream.write((strArr.length >> 8) & 255);
            for (int i4 = 0; i4 < strArr.length; i4++) {
                if (strArr[i4] != null) {
                    String str2 = strArr[i4];
                    outputStream.write(str2.length() & 255);
                    outputStream.write((str2.length() >> 8) & 255);
                    for (int i5 = 0; i5 < str2.length(); i5++) {
                        outputStream.write(str2.charAt(i5));
                        outputStream.write(str2.charAt(i5) >> 8);
                    }
                } else {
                    outputStream.write(0);
                    outputStream.write(0);
                }
            }
        } else if (obj instanceof Long) {
            long longValue = ((Long) obj).longValue();
            outputStream.write(12);
            outputStream.write((int) (longValue & 255));
            outputStream.write((int) ((longValue >> 8) & 255));
            outputStream.write((int) ((longValue >> 16) & 255));
            outputStream.write((int) ((longValue >> 24) & 255));
            outputStream.write((int) ((longValue >> 32) & 255));
            outputStream.write((int) ((longValue >> 40) & 255));
            outputStream.write((int) ((longValue >> 48) & 255));
            outputStream.write((int) ((longValue >> 56) & 255));
        } else if (obj instanceof HashMap) {
            HashMap hashMap = (HashMap) obj;
            if (hashMap.size() <= 255) {
                outputStream.write(13);
                outputStream.write(hashMap.size());
            } else {
                outputStream.write(14);
                outputStream.write(hashMap.size() & 255);
                outputStream.write((hashMap.size() >> 8) & 255);
            }
            for (Object next : hashMap.keySet()) {
                dumps(outputStream, next);
                dumps(outputStream, hashMap.get(next));
            }
        }
    }

    public static final byte[] dumps(Object obj) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            dumps(byteArrayOutputStream, obj);
            byteArrayOutputStream.close();
        } catch (IOException e) {
        }
        return byteArrayOutputStream.toByteArray();
    }

    public static final boolean empty(char c) {
        return c == ' ' || c == 0 || c == 10 || c == 13 || c == 9;
    }

    public static final boolean empty(Object obj) {
        if (obj == null) {
            return true;
        }
        if (obj instanceof Vector) {
            Vector vector = (Vector) obj;
            if (vector.size() == 0) {
                return true;
            }
            for (int i = 0; i < vector.size(); i++) {
                if (!empty(vector.elementAt(i))) {
                    return false;
                }
            }
            return true;
        } else if (obj instanceof byte[]) {
            return ((byte[]) obj).length == 0;
        } else {
            if (obj instanceof int[]) {
                return ((int[]) obj).length == 0;
            }
            if (obj instanceof String) {
                String str = (String) obj;
                for (int i2 = 0; i2 < str.length(); i2++) {
                    if (!empty(str.charAt(i2))) {
                        return false;
                    }
                }
                return true;
            } else if (!(obj instanceof String[])) {
                return false;
            } else {
                String[] strArr = (String[]) obj;
                if (strArr.length == 0) {
                    return true;
                }
                for (String empty : strArr) {
                    if (!empty(empty)) {
                        return false;
                    }
                }
                return true;
            }
        }
    }

    public static final boolean exist(String str) {
        return C0037c.getApplicationContext().getFileStreamPath(str).exists();
    }

    public static final int find(byte[] bArr, byte[] bArr2, int i) {
        int length = bArr2.length;
        int length2 = bArr.length - length;
        int i2 = i;
        while (i2 < length2) {
            int i3 = 0;
            while (i3 < length) {
                if (bArr[i2 + i3] == bArr2[i3]) {
                    i3++;
                } else {
                    i2++;
                }
            }
            return i2;
        }
        return -1;
    }

    public static final String formatphone(String str) {
        if (str.length() == 0) {
            return str;
        }
        int verifyphone = verifyphone(str);
        if (verifyphone <= 0) {
            return str + C0037c.getApplicationContext().getString(C0060z.stringID("badformat"));
        }
        String str2 = "+" + str.substring(0, verifyphone) + "-" + str.substring(verifyphone);
        return verifyphonelength(str) <= 0 ? str2 + C0037c.getApplicationContext().getString(C0060z.stringID("badformat")) : str2;
    }

    public static final String getdate(long j) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(new Date(j));
        int i = instance.get(5);
        String str = (i < 10 ? "" + "0" + i : "" + i) + "/";
        int i2 = instance.get(2) + 1;
        return ((i2 < 10 ? str + "0" + i2 : str + i2) + "/") + instance.get(1);
    }

    public static byte[] getmd5(byte[] bArr) {
        b bVar = new b();
        try {
            bVar.c(bArr, bArr.length);
            return utf8_encode(bVar.c());
        } finally {
            bVar.close();
        }
    }

    public static final String gettime(long j) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(new Date(j));
        int i = instance.get(11);
        String str = (i < 10 ? "" + "0" + i : "" + i) + ":";
        int i2 = instance.get(12);
        String str2 = (i2 < 10 ? str + "0" + i2 : str + i2) + " ";
        int i3 = instance.get(5);
        String str3 = (i3 < 10 ? str2 + "0" + i3 : str2 + i3) + "/";
        int i4 = instance.get(2) + 1;
        return ((i4 < 10 ? str3 + "0" + i4 : str3 + i4) + "/") + instance.get(1);
    }

    public static final byte[] gzip(byte[] bArr) {
        a aVar = new a();
        int i = 10;
        if (bArr[3] == 8) {
            while (bArr[i] != 0) {
                i++;
            }
            i++;
        }
        return aVar.a(bArr, i);
    }

    private static final String hex(char c) {
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        return "%" + cArr[c / 16] + cArr[c % 16];
    }

    public static final boolean iconExist(String str) {
        return false;
    }

    public static final String[] list() {
        return C0037c.getApplicationContext().fileList();
    }

    public static final Object loads(ByteArrayInputStream byteArrayInputStream) {
        int read = byteArrayInputStream.read();
        switch (read) {
            case 0:
            default:
                return null;
            case 1:
                return new Integer((byte) byteArrayInputStream.read());
            case 2:
                return new Integer((short) (byteArrayInputStream.read() | (byteArrayInputStream.read() << 8)));
            case 3:
                return new Integer(byteArrayInputStream.read() | (byteArrayInputStream.read() << 8) | (byteArrayInputStream.read() << 16) | (byteArrayInputStream.read() << 24));
            case 4:
            case 5:
                int read2 = byteArrayInputStream.read();
                if (read == 5) {
                    read2 |= byteArrayInputStream.read() << 8;
                }
                Vector vector = new Vector(read2);
                for (int i = 0; i < read2; i++) {
                    vector.addElement(loads(byteArrayInputStream));
                }
                return vector;
            case 6:
            case 7:
            case 8:
                int read3 = byteArrayInputStream.read();
                if (read == 7) {
                    read3 |= byteArrayInputStream.read() << 8;
                } else if (read == 8) {
                    read3 = read3 | (byteArrayInputStream.read() << 8) | (byteArrayInputStream.read() << 16) | (byteArrayInputStream.read() << 24);
                }
                byte[] bArr = new byte[read3];
                try {
                    byteArrayInputStream.read(bArr);
                    return bArr;
                } catch (IOException e) {
                    return bArr;
                }
            case 9:
                int read4 = byteArrayInputStream.read() | (byteArrayInputStream.read() << 8);
                int[] iArr = new int[read4];
                for (int i2 = 0; i2 < read4; i2++) {
                    iArr[i2] = byteArrayInputStream.read();
                    iArr[i2] = iArr[i2] | (byteArrayInputStream.read() << 8);
                    iArr[i2] = iArr[i2] | (byteArrayInputStream.read() << 16);
                    iArr[i2] = iArr[i2] | (byteArrayInputStream.read() << 24);
                }
                return iArr;
            case 10:
                int read5 = byteArrayInputStream.read() | (byteArrayInputStream.read() << 8);
                StringBuilder sb = new StringBuilder(read5);
                for (int i3 = 0; i3 < read5; i3++) {
                    sb.append((char) (((char) byteArrayInputStream.read()) | (((char) byteArrayInputStream.read()) << 8)));
                }
                return sb.toString();
            case 11:
                int read6 = byteArrayInputStream.read() | (byteArrayInputStream.read() << 8);
                String[] strArr = new String[read6];
                for (int i4 = 0; i4 < read6; i4++) {
                    StringBuilder sb2 = new StringBuilder();
                    int read7 = byteArrayInputStream.read() | (byteArrayInputStream.read() << 8);
                    for (int i5 = 0; i5 < read7; i5++) {
                        sb2.append((char) (((char) byteArrayInputStream.read()) | (((char) byteArrayInputStream.read()) << 8)));
                    }
                    strArr[i4] = sb2.toString();
                }
                return strArr;
            case 12:
                return new Long(((long) byteArrayInputStream.read()) | (((long) byteArrayInputStream.read()) << 8) | (((long) byteArrayInputStream.read()) << 16) | (((long) byteArrayInputStream.read()) << 24) | (((long) byteArrayInputStream.read()) << 32) | (((long) byteArrayInputStream.read()) << 40) | (((long) byteArrayInputStream.read()) << 48) | (((long) byteArrayInputStream.read()) << 56));
            case InMobiAdDelegate.INMOBI_AD_UNIT_120X600:
            case 14:
                int read8 = byteArrayInputStream.read();
                if (read == 14) {
                    read8 |= byteArrayInputStream.read() << 8;
                }
                HashMap hashMap = new HashMap(read8);
                for (int i6 = 0; i6 < read8; i6++) {
                    hashMap.put(loads(byteArrayInputStream), loads(byteArrayInputStream));
                }
                return hashMap;
        }
    }

    public static final Object loads(byte[] bArr) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        Object loads = loads(byteArrayInputStream);
        try {
            byteArrayInputStream.close();
        } catch (IOException e) {
        }
        return loads;
    }

    private static final void make_crc_table() {
        hw = new long[256];
        for (int i = 0; i < 256; i++) {
            long j = (long) i;
            for (int i2 = 0; i2 < 8; i2++) {
                j = (1 & j) != 0 ? (j >> 1) ^ 3988292384L : j >> 1;
            }
            hw[i] = j;
        }
    }

    public static final int next(String str) {
        boolean z = false;
        int i = 0;
        while (i < str.length()) {
            char charAt = str.charAt(i);
            if (z) {
                if (charAt == '\"') {
                    z = false;
                }
            } else if (charAt == '\"') {
                z = true;
            } else if (charAt == ' ' || charAt == 9) {
                return i;
            }
            i++;
        }
        return i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0021 A[SYNTHETIC, Splitter:B:18:0x0021] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final byte[] read(java.lang.String r4) {
        /*
            r2 = 0
            android.app.Application r0 = com.papaya.si.C0037c.getApplicationContext()     // Catch:{ Exception -> 0x0014, all -> 0x001d }
            java.io.FileInputStream r0 = r0.openFileInput(r4)     // Catch:{ Exception -> 0x0014, all -> 0x001d }
            byte[] r1 = readAll(r0)     // Catch:{ Exception -> 0x0030, all -> 0x002b }
            if (r0 == 0) goto L_0x0012
            r0.close()     // Catch:{ Exception -> 0x0025 }
        L_0x0012:
            r0 = r1
        L_0x0013:
            return r0
        L_0x0014:
            r0 = move-exception
            r0 = r2
        L_0x0016:
            if (r0 == 0) goto L_0x001b
            r0.close()     // Catch:{ Exception -> 0x0027 }
        L_0x001b:
            r0 = r2
            goto L_0x0013
        L_0x001d:
            r0 = move-exception
            r1 = r2
        L_0x001f:
            if (r1 == 0) goto L_0x0024
            r1.close()     // Catch:{ Exception -> 0x0029 }
        L_0x0024:
            throw r0
        L_0x0025:
            r0 = move-exception
            goto L_0x0012
        L_0x0027:
            r0 = move-exception
            goto L_0x001b
        L_0x0029:
            r1 = move-exception
            goto L_0x0024
        L_0x002b:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x001f
        L_0x0030:
            r1 = move-exception
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.papaya.si.C0035bg.read(java.lang.String):byte[]");
    }

    public static final byte[] readAll(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
            byteArrayOutputStream.flush();
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            try {
                byteArrayOutputStream.close();
            } catch (Exception e) {
            }
            try {
                inputStream.close();
            } catch (Exception e2) {
            }
            return byteArray;
        } catch (Exception e3) {
            try {
                byteArrayOutputStream.close();
            } catch (Exception e4) {
            }
            try {
                inputStream.close();
            } catch (Exception e5) {
            }
            return null;
        } catch (Throwable th) {
            try {
                byteArrayOutputStream.close();
            } catch (Exception e6) {
            }
            try {
                inputStream.close();
            } catch (Exception e7) {
            }
            throw th;
        }
    }

    public static final int readInt(byte[] bArr, int i) {
        int i2 = i + 1;
        int i3 = i2 + 1;
        return ((bArr[i2] << 16) & 16711680) | (bArr[i] << 24) | ((bArr[i3] << 8) & 65280) | (bArr[i3 + 1] & 255);
    }

    public static final short readShort(byte[] bArr, int i) {
        return (short) ((bArr[i + 1] & 255) | ((bArr[i] << 8) & 65280));
    }

    public static final int readUShort(byte[] bArr, int i) {
        return (bArr[i + 1] & 255) | ((bArr[i] << 8) & 65280);
    }

    public static final void replace(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        int length = bArr2.length;
        int length2 = bArr.length - length;
        int i = 0;
        while (i < length2) {
            int i2 = 0;
            while (true) {
                if (i2 < length) {
                    if (bArr[i + i2] != bArr2[i2]) {
                        break;
                    }
                    i2++;
                } else {
                    System.arraycopy(bArr3, 0, bArr, i, length);
                    i += length;
                    break;
                }
            }
            i++;
        }
    }

    public static final byte[] resize(byte[] bArr, int i) {
        byte[] bArr2 = new byte[i];
        System.arraycopy(bArr, 0, bArr2, 0, Math.min(i, bArr.length));
        return bArr2;
    }

    public static final int[] resize(int[] iArr, int i) {
        int[] iArr2 = new int[i];
        System.arraycopy(iArr, 0, iArr2, 0, Math.min(i, iArr.length));
        return iArr2;
    }

    public static final boolean same(Object obj, Object obj2, boolean z) {
        if (obj == obj2) {
            return true;
        }
        if (empty(obj) && empty(obj2)) {
            return true;
        }
        if ((obj instanceof Integer) && (obj2 instanceof Integer) && ((Integer) obj).intValue() == ((Integer) obj2).intValue()) {
            return true;
        }
        if ((obj instanceof Vector) && (obj2 instanceof Vector)) {
            Vector vector = (Vector) obj;
            Vector vector2 = (Vector) obj2;
            for (int i = 0; i < vector.size(); i++) {
                if ((i >= vector2.size() && !empty(vector.elementAt(i))) || (i < vector2.size() && !same(vector.elementAt(i), vector2.elementAt(i), z))) {
                    return false;
                }
            }
            for (int size = vector.size(); size < vector2.size(); size++) {
                if (!empty(vector2.elementAt(size))) {
                    return false;
                }
            }
            return true;
        } else if ((obj instanceof Long) && (obj2 instanceof Long) && Math.abs(((((Long) obj).longValue() / 3600) / 24) - ((((Long) obj2).longValue() / 3600) / 24)) < 800) {
            return true;
        } else {
            if ((obj instanceof byte[]) && (obj2 instanceof byte[])) {
                byte[] bArr = (byte[]) obj;
                byte[] bArr2 = (byte[]) obj2;
                if (bArr.length != bArr2.length) {
                    return false;
                }
                for (int i2 = 0; i2 < bArr.length; i2++) {
                    if (bArr[i2] != bArr2[i2]) {
                        return false;
                    }
                }
                return true;
            } else if ((obj instanceof int[]) && (obj2 instanceof int[])) {
                int[] iArr = (int[]) obj;
                int[] iArr2 = (int[]) obj2;
                if (iArr.length != iArr2.length) {
                    return false;
                }
                for (int i3 = 0; i3 < iArr.length; i3++) {
                    if (iArr[i3] != iArr2[i3]) {
                        return false;
                    }
                }
                return true;
            } else if ((obj instanceof String) && (obj2 instanceof String) && !z && obj.equals(obj2)) {
                return true;
            } else {
                if ((obj instanceof String) && (obj2 instanceof String) && z) {
                    String str = (String) obj;
                    String str2 = (String) obj2;
                    int i4 = 0;
                    int i5 = 0;
                    while (i5 < str.length()) {
                        while (i5 < str.length() && empty(str.charAt(i5))) {
                            i5++;
                        }
                        while (i4 < str2.length() && empty(str2.charAt(i4))) {
                            i4++;
                        }
                        if (i5 < str.length()) {
                            if (i4 >= str2.length() || str.charAt(i5) != str2.charAt(i4)) {
                                return false;
                            }
                            i5++;
                            i4++;
                        }
                    }
                    while (i4 < str2.length() && empty(str2.charAt(i4))) {
                        i4++;
                    }
                    return i4 >= str2.length();
                } else if (!(obj instanceof String[]) || !(obj2 instanceof String[])) {
                    return false;
                } else {
                    String[] strArr = (String[]) obj;
                    String[] strArr2 = (String[]) obj2;
                    for (int i6 = 0; i6 < strArr.length; i6++) {
                        if ((i6 >= strArr2.length && !empty(strArr[i6])) || (i6 < strArr2.length && !same(strArr[i6], strArr2[i6], z))) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
    }

    public static final String timestring(long j) {
        return j == 0 ? C0037c.getApplicationContext().getString(C0060z.stringID("currently")) : j < 60 ? C0037c.getApplicationContext().getString(C0060z.stringID("momentago")) : j < 3600 ? "" + (j / 60) + C0037c.getApplicationContext().getString(C0060z.stringID("minago")) : j < 86400 ? "" + (j / 3600) + C0037c.getApplicationContext().getString(C0060z.stringID("hourago")) : j < 2592000 ? "" + ((j / 3600) / 24) + C0037c.getApplicationContext().getString(C0060z.stringID("dayago")) : "" + (((j / 3600) / 24) / 30) + C0037c.getApplicationContext().getString(C0060z.stringID("monthago"));
    }

    private static long update_crc(long j, byte[] bArr, int i, int i2) {
        if (hw == null) {
            make_crc_table();
        }
        int i3 = i + i2;
        long j2 = j;
        for (int i4 = i; i4 < i3; i4++) {
            j2 = (j2 >> 8) ^ hw[(int) ((((long) bArr[i4]) ^ j2) & 255)];
        }
        return j2;
    }

    public static final String utf8_decode(Object obj) {
        byte[] bArr = (byte[]) obj;
        return utf8_decode(bArr, 0, bArr.length);
    }

    public static final String utf8_decode(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            return null;
        }
        int i3 = i + i2;
        char[] cArr = new char[i2];
        int i4 = i;
        int i5 = 0;
        while (i4 < i3) {
            byte b2 = bArr[i4];
            if ((b2 >> 7) == 0) {
                cArr[i5] = (char) b2;
                i5++;
            } else if ((b2 >> 5) == -2) {
                cArr[i5] = (char) (((char) (b2 & 31)) << 6);
                i4++;
                cArr[i5] = (char) (cArr[i5] + (bArr[i4] & '?'));
                i5++;
            } else if ((b2 >> 4) == -2) {
                cArr[i5] = (char) (((char) (b2 & 15)) << 12);
                int i6 = i4 + 1;
                cArr[i5] = (char) (cArr[i5] + ((char) (((char) (bArr[i6] & 63)) << 6)));
                i4 = i6 + 1;
                cArr[i5] = (char) (cArr[i5] + (bArr[i4] & '?'));
                i5++;
            }
            i4++;
        }
        return new String(cArr, 0, i5);
    }

    public static final byte[] utf8_encode(String str) {
        if (str == null) {
            return null;
        }
        int length = str.length();
        char[] charArray = str.toCharArray();
        byte[] bArr = new byte[(length * 3)];
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            char c = charArray[i2];
            if (c < 128) {
                bArr[i] = (byte) c;
                i++;
            } else if (c <= 127 || c >= 2048) {
                int i3 = i + 1;
                bArr[i] = (byte) ((c >> 12) | 224);
                int i4 = i3 + 1;
                bArr[i3] = (byte) (((c >> 6) & 63) | 128);
                bArr[i4] = (byte) ((c & '?') | 128);
                i = i4 + 1;
            } else {
                int i5 = i + 1;
                bArr[i] = (byte) ((c >> 6) | 192);
                i = i5 + 1;
                bArr[i5] = (byte) ((c & '?') | 128);
            }
        }
        byte[] bArr2 = new byte[i];
        System.arraycopy(bArr, 0, bArr2, 0, i);
        return bArr2;
    }

    public static final int verifyphone(String str) {
        if (str.length() < 8) {
            return 0;
        }
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) < '0' || str.charAt(i) > '9') {
                return 0;
            }
        }
        int[] iArr = {28, 83, 89};
        int[] iArr2 = {20, 27, 30, 31, 32, 33, 34, 36, 39, 40, 41, 43, 44, 45, 46, 47, 48, 49, 51, 52, 53, 54, 55, 56, 57, 58, 60, 61, 62, 63, 64, 65, 66, 81, 82, 84, 86, 90, 91, 92, 93, 94, 95, 98};
        switch (str.charAt(0) - '0') {
            case 0:
                return 0;
            case 1:
            case 7:
                return 1;
            default:
                int parseInt = Integer.parseInt(str.substring(0, 2));
                for (int i2 : iArr) {
                    if (i2 == parseInt) {
                        return 0;
                    }
                }
                for (int i3 : iArr2) {
                    if (i3 == parseInt) {
                        return 2;
                    }
                }
                return 3;
        }
    }

    public static final int verifyphonelength(String str) {
        int verifyphone = verifyphone(str);
        if (verifyphone <= 0) {
            return verifyphone;
        }
        int parseInt = Integer.parseInt(str.substring(0, verifyphone));
        if (parseInt == 1) {
            if (str.length() != 11) {
                return 0;
            }
            int parseInt2 = Integer.parseInt(str.substring(1, 4));
            if (parseInt2 <= 200) {
                return 0;
            }
            if (parseInt2 % 100 >= 90) {
                return 0;
            }
            if (parseInt2 % 100 == 11) {
                return 0;
            }
            return parseInt2;
        } else if (parseInt != 86) {
            return verifyphone;
        } else {
            if (str.length() != 13) {
                return 0;
            }
            if (str.charAt(2) != '1') {
                return 0;
            }
            return verifyphone;
        }
    }

    public static final int wrapword(String str, int i) {
        if (i < str.length() && i > 0 && ((str.charAt(i) >= 'a' && str.charAt(i) <= 'z') || ((str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') || (str.charAt(i) >= '0' && str.charAt(i) <= '9')))) {
            int i2 = i;
            while (i2 > 0 && ((str.charAt(i2 - 1) >= 'a' && str.charAt(i2 - 1) <= 'z') || ((str.charAt(i2 - 1) >= 'A' && str.charAt(i2 - 1) <= 'Z') || (str.charAt(i2 - 1) >= '0' && str.charAt(i2 - 1) <= '9')))) {
                i2--;
            }
            if (i2 > 0) {
                return i2;
            }
        }
        return i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
        if (r0 != null) goto L_0x001b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0020, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0021, code lost:
        r4 = r1;
        r1 = r0;
        r0 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0026 A[SYNTHETIC, Splitter:B:17:0x0026] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0018 A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:1:0x0005] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final boolean write(java.lang.String r5, byte[] r6) {
        /*
            r3 = 0
            del(r5)
            r0 = 0
            android.app.Application r1 = com.papaya.si.C0037c.getApplicationContext()     // Catch:{ Exception -> 0x0018, all -> 0x0020 }
            r2 = 0
            java.io.FileOutputStream r0 = r1.openFileOutput(r5, r2)     // Catch:{ Exception -> 0x0018, all -> 0x0020 }
            r0.write(r6)     // Catch:{ Exception -> 0x0018, all -> 0x0030 }
            if (r0 == 0) goto L_0x0016
            r0.close()     // Catch:{ Exception -> 0x002a }
        L_0x0016:
            r0 = 1
        L_0x0017:
            return r0
        L_0x0018:
            r1 = move-exception
            if (r0 == 0) goto L_0x001e
            r0.close()     // Catch:{ Exception -> 0x002c }
        L_0x001e:
            r0 = r3
            goto L_0x0017
        L_0x0020:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0024:
            if (r1 == 0) goto L_0x0029
            r1.close()     // Catch:{ Exception -> 0x002e }
        L_0x0029:
            throw r0
        L_0x002a:
            r0 = move-exception
            goto L_0x0016
        L_0x002c:
            r0 = move-exception
            goto L_0x001e
        L_0x002e:
            r1 = move-exception
            goto L_0x0029
        L_0x0030:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.papaya.si.C0035bg.write(java.lang.String, byte[]):boolean");
    }

    public static final void writeInt(int i, byte[] bArr, int i2) {
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >> 24) & 255);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i >> 16) & 255);
        bArr[i4] = (byte) ((i >> 8) & 255);
        bArr[i4 + 1] = (byte) (i & 255);
    }

    public static final void writeShort(int i, byte[] bArr, int i2) {
        bArr[i2] = (byte) ((i >> 8) & 255);
        bArr[i2 + 1] = (byte) (i & 255);
    }
}
