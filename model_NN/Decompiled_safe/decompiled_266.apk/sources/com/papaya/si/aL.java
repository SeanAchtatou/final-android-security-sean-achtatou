package com.papaya.si;

public abstract class aL implements Runnable {
    public boolean eX = false;

    public static void cancelTask(aL aLVar) {
        if (aLVar != null) {
            aLVar.eX = true;
        }
    }

    public void run() {
        if (!this.eX) {
            try {
                runTask();
            } catch (Exception e) {
                X.w(e, "Failed to runTask", new Object[0]);
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void runTask();
}
