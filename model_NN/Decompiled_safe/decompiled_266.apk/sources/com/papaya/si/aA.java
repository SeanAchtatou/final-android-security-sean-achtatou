package com.papaya.si;

import android.net.Uri;
import com.papaya.purchase.PPYPayment;
import com.papaya.purchase.PPYPaymentDelegate;
import com.papaya.si.bt;
import org.json.JSONObject;

public final class aA extends bv implements aW, bt.a {
    private PPYPaymentDelegate eD;
    private PPYPayment fQ;
    private long fR = System.currentTimeMillis();

    public aA(PPYPayment pPYPayment, PPYPaymentDelegate pPYPaymentDelegate) {
        this.fQ = pPYPayment;
        this.eD = pPYPaymentDelegate;
        if (this.eD == null) {
            this.eD = pPYPayment.getDelegate();
        }
        setCacheable(false);
        setDispatchable(true);
        setRequireSid(true);
        setDelegate(this);
        StringBuilder sb = new StringBuilder("json_iap?x=");
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", this.fQ.getName());
            jSONObject.put("desc", this.fQ.getDescription());
            jSONObject.put("ppys", this.fQ.getPapayas());
            if (this.fQ.getPayload() != null) {
                jSONObject.put("payload", this.fQ.getPayload().toString());
            }
            jSONObject.put("secret", (int) (((double) System.currentTimeMillis()) / 1000.0d));
        } catch (Exception e) {
            X.e("Failed to construct payment request: " + e, new Object[0]);
        }
        sb.append(Uri.encode(C0034bf.encrypt(jSONObject.toString())));
        this.url = C0034bf.createURL(sb.toString());
        this.fR = System.currentTimeMillis();
    }

    public final void connectionFailed(bt btVar, int i) {
        if (this.eD != null) {
            this.eD.onPaymentFailed(this.fQ, 0, "Failed to finish payment");
        }
    }

    public final void connectionFinished(bt btVar) {
        if (this.eD != null) {
            try {
                JSONObject parseJsonObject = C0034bf.parseJsonObject(C0034bf.decrypt(aV.utf8String(btVar.getData(), null)));
                int optInt = parseJsonObject.optInt("status", 0);
                if (optInt > 0) {
                    this.fQ.setTransactionID(parseJsonObject.optString("tid", null));
                    this.fQ.setReceipt(parseJsonObject.optString("receipt", null));
                    this.eD.onPaymentFinished(this.fQ);
                    return;
                }
                this.eD.onPaymentFailed(this.fQ, optInt, parseJsonObject.optString("error", "Invalid server response"));
            } catch (Exception e) {
                X.w(e, "Failed to process", new Object[0]);
                this.eD.onPaymentFailed(this.fQ, 0, "Internal error: " + e);
            }
        }
    }

    public final boolean isExpired() {
        return System.currentTimeMillis() - this.fR > 15000;
    }
}
