package com.papaya.si;

import java.util.TreeMap;

public final class aX<K, V> {
    private int gQ = 10;
    private TreeMap<K, V> gR;

    public aX(int i) {
        int i2 = 10;
        this.gQ = i > 0 ? i : i2;
        this.gR = new TreeMap<>();
    }

    public final void clear() {
        this.gR.clear();
    }

    public final synchronized V get(K k) {
        return this.gR.get(k);
    }

    public final int getMaxSize() {
        return this.gQ;
    }

    public final synchronized void put(K k, V v) {
        this.gR.put(k, v);
        if (this.gR.size() > this.gQ) {
            this.gR.remove(this.gR.firstKey());
        }
    }

    public final synchronized V remove(K k) {
        return this.gR.remove(k);
    }

    public final synchronized int size() {
        return this.gR.size();
    }
}
