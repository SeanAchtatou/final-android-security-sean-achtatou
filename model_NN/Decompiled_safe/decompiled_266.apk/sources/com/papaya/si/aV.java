package com.papaya.si;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.view.WindowManager;
import java.lang.ref.Reference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

public final class aV {
    private static final aZ gN = new aZ();
    private static final aJ gO = new aJ();
    private static final aR gP = new aR();

    private aV() {
    }

    public static byte[] acquireBytes(int i) {
        return gO.acquire(i);
    }

    public static StringBuilder acquireStringBuilder(int i) {
        return gN.acquire(i);
    }

    public static int bitSet(int i, int i2, int i3) {
        return i3 > 0 ? (1 << i2) | i : ((1 << i2) ^ -1) & i;
    }

    public static boolean bitTest(int i, int i2) {
        return ((1 << i2) & i) > 0;
    }

    public static void clear() {
        gO.clear();
        gN.clear();
        gP.clear();
    }

    public static <T> void clearReferences(List<? extends Reference<T>> list) {
        Iterator<? extends Reference<T>> it = list.iterator();
        while (it.hasNext()) {
            if (((Reference) it.next()).get() == null) {
                it.remove();
            }
        }
    }

    public static <K, V> void clearReferences(Map<K, ? extends Reference<V>> map) {
        Iterator<Map.Entry<K, ? extends Reference<V>>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry next = it.next();
            if (next.getValue() == null || ((Reference) next.getValue()).get() == null) {
                it.remove();
            }
        }
    }

    public static String concatIntArrays(String str, int... iArr) {
        StringBuilder acquireStringBuilder = acquireStringBuilder(0);
        for (int i = 0; i < iArr.length; i++) {
            if (i > 0) {
                acquireStringBuilder.append(str);
            }
            acquireStringBuilder.append(String.valueOf(i));
        }
        return releaseStringBuilder(acquireStringBuilder);
    }

    public static String concatStrings(String str, int i, String... strArr) {
        StringBuilder acquireStringBuilder = acquireStringBuilder(0);
        int i2 = 0;
        for (String str2 : strArr) {
            if (i == 2 || ((i == 1 && !isEmpty(str2)) || (i == 0 && str2 != null))) {
                if (i2 > 0) {
                    acquireStringBuilder.append(str);
                }
                acquireStringBuilder.append(str2);
                i2++;
            }
        }
        return releaseStringBuilder(acquireStringBuilder);
    }

    public static String concatStrings(String str, String... strArr) {
        return concatStrings(str, 0, strArr);
    }

    public static String emptyAsNull(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        return str;
    }

    public static boolean equal(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static boolean existClass(String str) {
        try {
            Class.forName(str);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    public static String format(String str, Object... objArr) {
        aS acquire = gP.acquire();
        String format = acquire.format(str, objArr);
        gP.release(acquire);
        return format;
    }

    public static byte[] getBytes(String str) {
        if (str != null) {
            try {
                return str.getBytes("UTF-8");
            } catch (Exception e) {
                X.w(e, "Failed to getBytes: " + str, new Object[0]);
            }
        }
        return new byte[0];
    }

    public static String getDeviceID(Context context) {
        if (context == null) {
            return "";
        }
        String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
        return (string == null || string.length() == 0) ? "emulator" : string;
    }

    public static JSONObject getSystemInfo(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2 = jSONObject == null ? new JSONObject() : jSONObject;
        try {
            jSONObject2.putOpt("A_ID", getDeviceID(context));
            jSONObject2.putOpt("B.ID", Build.ID);
            jSONObject2.putOpt("B.PRODUCT", Build.PRODUCT);
            jSONObject2.putOpt("B.DEVICE", Build.DEVICE);
            jSONObject2.putOpt("B.BOARD", Build.BOARD);
            jSONObject2.putOpt("B.BRAND", Build.BRAND);
            jSONObject2.putOpt("B.MODEL", Build.MODEL);
            jSONObject2.putOpt("B.TYPE", Build.TYPE);
            jSONObject2.putOpt("B.TAGS", Build.TAGS);
            jSONObject2.putOpt("B.V.INCREMENTAL", Build.VERSION.INCREMENTAL);
            jSONObject2.putOpt("B.V.RELEASE", Build.VERSION.RELEASE);
            jSONObject2.putOpt("B.V.SDK", Build.VERSION.SDK);
            jSONObject2.putOpt("Locale", Locale.getDefault().toString());
            Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
            jSONObject2.putOpt("D.width", Integer.valueOf(defaultDisplay.getWidth()));
            jSONObject2.putOpt("D.height", Integer.valueOf(defaultDisplay.getHeight()));
            jSONObject2.putOpt("P.id", context.getApplicationContext().getPackageName());
            jSONObject2.putOpt("P.v", Integer.valueOf(C0056v.bc));
            jSONObject2.putOpt("P.s", C0056v.be);
            jSONObject2.putOpt("P.l", C0056v.bd);
        } catch (Exception e) {
            X.w("Failed to get system info: " + e, new Object[0]);
        }
        return jSONObject2;
    }

    public static JSONObject getTelephonyInfo(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2 = jSONObject == null ? new JSONObject() : jSONObject;
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            jSONObject2.putOpt("T.DeviceId", emptyAsNull(telephonyManager.getDeviceId()));
            jSONObject2.putOpt("T.NetworkType", Integer.valueOf(telephonyManager.getNetworkType()));
            jSONObject2.putOpt("T.PhoneType", Integer.valueOf(telephonyManager.getPhoneType()));
            jSONObject2.putOpt("T.NetworkCountryIso", emptyAsNull(telephonyManager.getNetworkCountryIso()));
            jSONObject2.putOpt("T.NetworkOperator", emptyAsNull(telephonyManager.getNetworkOperator()));
            jSONObject2.putOpt("T.NetworkOperatorName", emptyAsNull(telephonyManager.getNetworkOperatorName()));
            jSONObject2.putOpt("T.SimCountryIso", emptyAsNull(telephonyManager.getSimCountryIso()));
            jSONObject2.putOpt("T.SimOperator", emptyAsNull(telephonyManager.getSimOperator()));
            jSONObject2.putOpt("T.SimOperatorName", emptyAsNull(telephonyManager.getSimOperatorName()));
            jSONObject2.putOpt("T.SubscriberId", emptyAsNull(telephonyManager.getSubscriberId()));
        } catch (Exception e) {
            X.w("Failed to get telephony info: " + e, new Object[0]);
        }
        return jSONObject2;
    }

    public static String headString(String str, int i) {
        return str.length() <= i ? str : str.substring(0, i);
    }

    public static int intValue(Object obj) {
        return intValue(obj, -1);
    }

    public static int intValue(Object obj, int i) {
        if (obj == null) {
            return i;
        }
        if (obj instanceof Number) {
            return ((Number) obj).intValue();
        }
        if (obj instanceof String) {
            return parseInt((String) obj, i);
        }
        X.w("unknown type to get int, %s", obj);
        return i;
    }

    public static boolean isEmpty(CharSequence charSequence) {
        return charSequence == null || charSequence.length() == 0;
    }

    public static boolean isEmulator(Context context) {
        return getDeviceID(context).equals("emulator") || "sdk".equals(Build.MODEL) || "google_sdk".equals(Build.MODEL);
    }

    public static boolean isNotEmpty(CharSequence charSequence) {
        return charSequence != null && charSequence.length() > 0;
    }

    public static String leftTrim(String str) {
        char[] charArray = str.toCharArray();
        int i = 0;
        while (i < charArray.length && charArray[i] == ' ') {
            i++;
        }
        return new String(charArray, i, charArray.length - i);
    }

    public static <T> T newInstance(String str) {
        try {
            return Class.forName(str).newInstance();
        } catch (Throwable th) {
            X.w(th, "Failed to get instance of " + str, new Object[0]);
            return null;
        }
    }

    public static String nonNullString(String str, String str2) {
        return str == null ? str2 : str;
    }

    public static String nullAsEmpty(CharSequence charSequence) {
        return charSequence == null ? "" : charSequence.toString();
    }

    public static String objectToString(Object obj) {
        int i = 0;
        if (obj == null) {
            return "";
        }
        if (obj instanceof byte[]) {
            return utf8_decode(obj);
        }
        if (obj instanceof List) {
            List<Object> list = (List) obj;
            String str = "" + "[";
            int i2 = 0;
            for (Object objectToString : list) {
                str = str + objectToString(objectToString);
                if (i2 < list.size() - 1) {
                    str = str + ",";
                }
                i2++;
            }
            return str + "]";
        } else if (obj instanceof String[]) {
            String[] strArr = (String[]) obj;
            String str2 = "" + "[";
            for (int i3 = 0; i3 < strArr.length; i3++) {
                str2 = str2 + strArr[i3];
                if (i3 < strArr.length - 1) {
                    str2 = str2 + ",";
                }
            }
            return str2 + "]";
        } else if (obj instanceof Integer[]) {
            Integer[] numArr = (Integer[]) obj;
            String str3 = "" + "[";
            for (int i4 = 0; i4 < numArr.length; i4++) {
                str3 = str3 + numArr[i4];
                if (i4 < numArr.length - 1) {
                    str3 = str3 + ",";
                }
            }
            return str3 + "]";
        } else if (obj instanceof Map) {
            Map map = (Map) obj;
            String str4 = "" + "{";
            Iterator it = map.entrySet().iterator();
            while (true) {
                String str5 = str4;
                if (!it.hasNext()) {
                    return str5 + "}";
                }
                Map.Entry entry = (Map.Entry) it.next();
                str4 = ((str5 + objectToString(entry.getKey())) + ":") + objectToString(entry.getValue());
                if (i < map.size() - 1) {
                    str4 = str4 + ",";
                }
                i++;
            }
        } else if (!(obj instanceof HashSet)) {
            return obj.toString();
        } else {
            HashSet hashSet = (HashSet) obj;
            Iterator it2 = hashSet.iterator();
            String str6 = "" + "[";
            int i5 = 0;
            while (it2.hasNext()) {
                str6 = str6 + objectToString(it2.next());
                if (i5 < hashSet.size() - 1) {
                    str6 = str6 + ",";
                }
                i5++;
            }
            return str6 + "]";
        }
    }

    public static int parseInt(String str) {
        return parseInt(str, -1);
    }

    public static int parseInt(String str, int i) {
        if (str == null) {
            return i;
        }
        try {
            return Integer.parseInt(str);
        } catch (Exception e) {
            X.w(e, "failed to parse int [%s], using default value [%s]", str, Integer.valueOf(i));
            return i;
        }
    }

    public static void releaseBytes(byte[] bArr) {
        gO.release(bArr);
    }

    public static void releaseOnly(StringBuilder sb) {
        gN.releaseOnly(sb);
    }

    public static String releaseStringBuilder(StringBuilder sb) {
        return gN.release(sb);
    }

    public static void safeSleep(long j) {
        try {
            Thread.sleep(j);
        } catch (InterruptedException e) {
            X.i("Failed to sleep:" + e, new Object[0]);
        }
    }

    public static <T> T sget(List list, int i) {
        return sget(list, i, null);
    }

    public static <T> T sget(List list, int i, T t) {
        if (list == null || list.size() <= i) {
            return t;
        }
        try {
            return list.get(i);
        } catch (Exception e) {
            X.w("failed to get value of %d from list %s: %s", Integer.valueOf(i), list, e);
            return t;
        }
    }

    public static int sgetInt(List list, int i) {
        return intValue(sget(list, i, null), 0);
    }

    public static int sgetInt(List list, int i, int i2) {
        return intValue(sget(list, i, null), i2);
    }

    public static String stringValue(Object obj) {
        if (obj == null) {
            return null;
        }
        return obj instanceof byte[] ? utf8String((byte[]) obj, null) : obj.toString();
    }

    public static String toString(Object obj) {
        return obj == null ? "" : obj.toString();
    }

    public static String toString(Object obj, String str) {
        return obj == null ? str : obj.toString();
    }

    public static String toString(Object[] objArr) {
        if (objArr == null) {
            return "null";
        }
        StringBuilder acquireStringBuilder = acquireStringBuilder(0);
        acquireStringBuilder.append('[');
        for (int i = 0; i < objArr.length; i++) {
            if (objArr[i] == null) {
                acquireStringBuilder.append("null");
            }
            if (i < objArr.length - 1) {
                acquireStringBuilder.append(",");
            }
        }
        acquireStringBuilder.append(']');
        return releaseStringBuilder(acquireStringBuilder);
    }

    public static String utf8String(byte[] bArr, String str) {
        if (bArr == null) {
            return str;
        }
        try {
            int i = (bArr.length >= 3 && bArr[0] == -17 && bArr[1] == -69 && bArr[2] == -65) ? 3 : 0;
            return new String(bArr, i, bArr.length - i, "UTF-8");
        } catch (Exception e) {
            X.e(e, "Failed to get utf8String", new Object[0]);
            return str;
        }
    }

    public static String utf8_decode(Object obj) {
        return obj instanceof byte[] ? utf8String((byte[]) obj, null) : toString(obj);
    }

    public static byte[] utf8_encode(String str) {
        return getBytes(str);
    }
}
