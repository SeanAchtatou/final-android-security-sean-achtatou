package com.papaya.si;

import android.content.Context;
import com.papaya.si.bt;
import com.papaya.view.OverlayMessage;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: com.papaya.si.ao  reason: case insensitive filesystem */
public final class C0016ao implements aM, aW, bt.a {
    private static HashSet<String> ei;
    private static C0016ao ej = new C0016ao();
    /* access modifiers changed from: private */
    public C0015an ek;
    private int el;
    private HashMap<String, String> em = new HashMap<>();
    private long en;

    static {
        HashSet<String> hashSet = new HashSet<>();
        ei = hashSet;
        hashSet.add("static_gameengine");
        ei.add("static_localleaderboard");
        ei.add("static_welcome");
    }

    private C0016ao() {
    }

    public static synchronized C0016ao getInstance() {
        C0016ao aoVar;
        synchronized (C0016ao.class) {
            aoVar = ej;
        }
        return aoVar;
    }

    public static boolean isSessionContent(String str) {
        return str != null && str.startsWith("__");
    }

    public static boolean isSessionLess(String str) {
        return ei.contains(str);
    }

    public final void checkUpdateRequest(Vector vector) {
        if (this.el != 1 && vector.size() > 1 && this.ek != null && this.ek.getVersion() != aV.intValue(vector.get(1))) {
            sendUpdateRequest();
        }
    }

    public final void clear() {
        C0028b.removeOverlayDialog(10);
        this.el = 0;
        if (this.ek != null) {
            this.ek.close();
            this.ek = null;
        }
        this.em.clear();
    }

    public final synchronized void connectionFailed(bt btVar, int i) {
        C0028b.removeOverlayDialog(10);
        setUpdateStatus(2);
    }

    public final synchronized void connectionFinished(bt btVar) {
        JSONArray optJSONArray;
        C0028b.removeOverlayDialog(10);
        X.i("got update response. time: %dms", Long.valueOf(System.currentTimeMillis() - this.en));
        if (!(btVar == null || this.ek == null)) {
            long currentTimeMillis = System.currentTimeMillis();
            try {
                JSONObject parseJsonObject = C0034bf.parseJsonObject(aV.utf8String(btVar.getData(), null));
                int optInt = parseJsonObject.optInt("version", -1);
                int version = this.ek.getVersion();
                if (!(optInt == -1 || version == optInt || (optJSONArray = parseJsonObject.optJSONArray("updates")) == null)) {
                    this.ek.update("BEGIN TRANSACTION", new Object[0]);
                    for (int i = 0; i < optJSONArray.length(); i += 2) {
                        String optString = optJSONArray.optString(i);
                        int optInt2 = optJSONArray.optInt(i + 1);
                        if (optString != null) {
                            this.ek.updatePage(optString, optInt2);
                        }
                    }
                    this.ek.setVersion(optInt);
                    this.ek.update("END TRANSACTION", new Object[0]);
                    X.i("updated db from %d to %d, %d updates", Integer.valueOf(version), Integer.valueOf(optInt), Integer.valueOf(optJSONArray.length()));
                }
            } catch (Exception e) {
                X.e(e, "Failed to update pages", new Object[0]);
            }
            X.i("update total time: %d", Long.valueOf(System.currentTimeMillis() - currentTimeMillis));
        }
        setUpdateStatus(2);
        return;
    }

    public final C0015an getPageDB() {
        return this.ek;
    }

    public final int getUpdateStatus() {
        return this.el;
    }

    public final int getVersion() {
        if (this.ek != null) {
            return this.ek.getVersion();
        }
        return -1;
    }

    public final void initialize(Context context) {
        final File databasePath = context.getDatabasePath("com.papaya.socialsdk.corona.171.webpage.db");
        this.el = 0;
        if (!databasePath.exists()) {
            new Thread() {
                public final void run() {
                    long currentTimeMillis = System.currentTimeMillis();
                    aK aKVar = new aK("com.papaya.socialsdk.corona.171.webpage.db.bin");
                    File file = new File(databasePath.getAbsolutePath() + ".tmp");
                    if (!aU.decompressGZipToFile(aKVar.openInput(), file)) {
                        X.e("Failed to decompress page db", new Object[0]);
                    } else if (!file.renameTo(databasePath)) {
                        X.e("Failed to rename %s to %s", file, databasePath);
                    } else {
                        C0015an unused = C0016ao.this.ek = new C0015an("com.papaya.socialsdk.corona.171.webpage.db");
                        C0030bb.runInHandlerThread(new Runnable() {
                            public final void run() {
                                C0016ao.this.sendUpdateRequest();
                            }
                        });
                    }
                    X.i("decompress db time: %dms", Long.valueOf(System.currentTimeMillis() - currentTimeMillis));
                }
            }.start();
            return;
        }
        this.ek = new C0015an("com.papaya.socialsdk.corona.171.webpage.db");
        X.i("Local page db: %d pages, version %d", Integer.valueOf(this.ek.countForTable("page")), Integer.valueOf(this.ek.getVersion()));
    }

    public final boolean isReady() {
        return this.ek == null || this.el == 2;
    }

    public final synchronized String newPageContent(String str, boolean z) {
        return isSessionContent(str) ? this.em.get(str) : this.ek != null ? this.ek.newPageContent(str, z) : null;
    }

    public final synchronized void savePage(String str, String str2) {
        if (isSessionContent(str)) {
            saveSessionPage(str, str2);
        } else if (this.ek != null) {
            this.ek.savePage(str, str2);
        }
    }

    public final synchronized void saveSessionPage(String str, String str2) {
        if (!aV.isEmpty(str) && !aV.isEmpty(str2)) {
            this.em.put(str, str2);
        }
    }

    public final void sendUpdateRequest() {
        if (this.ek == null || this.el == 1) {
            X.i("skip sendUpdateRequest: page db %s, status %d", this.ek, Integer.valueOf(this.el));
            return;
        }
        this.en = System.currentTimeMillis();
        URL createURL = C0034bf.createURL(aV.format("misc/json_update?version=%d&identifier=%s&db_version=%d", 171, "com.papaya.socialsdk.corona", Integer.valueOf(this.ek.getVersion())));
        if (createURL != null) {
            X.i("sending update request...", new Object[0]);
            setUpdateStatus(1);
            C0028b.showOverlayDialog(10);
            bv bvVar = new bv(createURL, false);
            bvVar.mL = OverlayMessage.DEFAULT_TIMEOUT;
            bvVar.mM = OverlayMessage.DEFAULT_TIMEOUT;
            bvVar.setRequireSid(false);
            bvVar.setDelegate(this);
            bvVar.start(true);
            return;
        }
        X.e("invalid update url", new Object[0]);
    }

    public final void setUpdateStatus(int i) {
        this.el = i;
        if (i == 2) {
            C0030bb.runInHandlerThread(new Runnable() {
                public final void run() {
                    bH.getInstance().startAllControllers();
                }
            });
        }
    }

    public final void setVersion(int i) {
        if (this.ek != null) {
            this.ek.setVersion(i);
        }
    }

    public final synchronized void updatePages(JSONArray jSONArray) {
        try {
            int i = jSONArray.getInt(1);
            if (this.ek != null) {
                X.i("Begin to update client db: %d", Integer.valueOf(jSONArray.length() - 2));
                if (jSONArray.length() > 2) {
                    JSONArray optJSONArray = jSONArray.optJSONArray(2);
                    this.ek.update("BEGIN TRANSACTION", new Object[0]);
                    int i2 = 0;
                    while (i2 < optJSONArray.length()) {
                        try {
                            String string = optJSONArray.getString(i2);
                            int i3 = optJSONArray.getInt(i2 + 1);
                            optJSONArray.getInt(i2 + 2);
                            String string2 = optJSONArray.getString(i2 + 3);
                            if (i3 == 1 || i3 == 2) {
                                C0001a.getWebCache().saveCacheWebFile(string, aV.getBytes(string2));
                            } else {
                                this.ek.savePage(string, string2);
                            }
                            i2 += 4;
                        } catch (Exception e) {
                            X.w("Failed to update data: " + e, new Object[0]);
                        }
                    }
                    this.ek.update("END TRANSACTION", new Object[0]);
                }
                this.ek.setVersion(i);
            }
        } catch (Exception e2) {
            X.w("Failed to update database: %s", e2);
        }
        return;
    }
}
