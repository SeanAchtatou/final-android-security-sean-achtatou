package com.papaya.si;

import android.os.Build;
import com.papaya.social.PPYSocial;
import java.util.Locale;
import org.apache.http.HttpHost;

/* renamed from: com.papaya.si.q  reason: case insensitive filesystem */
public final class C0051q implements C0048n {
    public static final HttpHost ay = new HttpHost("www.google-analytics.com", 80);
    private String ai;
    private C0049o az;

    public C0051q() {
        this("GoogleAnalytics", "1.0");
    }

    public C0051q(String str, String str2) {
        Locale locale = Locale.getDefault();
        Object[] objArr = new Object[7];
        objArr[0] = str;
        objArr[1] = str2;
        objArr[2] = Build.VERSION.RELEASE;
        objArr[3] = locale.getLanguage() == null ? PPYSocial.LANG_EN : locale.getLanguage().toLowerCase();
        objArr[4] = locale.getCountry() == null ? "" : locale.getCountry().toLowerCase();
        objArr[5] = Build.MODEL;
        objArr[6] = Build.ID;
        this.ai = String.format("%s/%s (Linux; U; Android %s; %s-%s; %s; Build/%s)", objArr);
    }

    public final void dispatchEvents(C0050p[] pVarArr) {
        if (this.az != null) {
            this.az.dispatchEvents(pVarArr);
        }
    }

    public final void init$5698b443(C0039e eVar, C0041g gVar, String str) {
        stop();
        this.az = new C0049o(eVar, gVar, str, this.ai);
        this.az.start();
    }

    public final void init$67669206(C0039e eVar, String str) {
        stop();
        this.az = new C0049o(eVar, str, this.ai);
        this.az.start();
    }

    public final void stop() {
        try {
            if (this.az != null && this.az.getLooper() != null) {
                try {
                    this.az.getLooper().quit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                this.az = null;
            }
        } catch (Exception e2) {
            X.e("Failed to stop dispatcher: " + e2, new Object[0]);
        }
    }

    public final void waitForThreadLooper() {
        this.az.getLooper();
    }
}
