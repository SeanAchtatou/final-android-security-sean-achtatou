package com.papaya.si;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import com.papaya.chat.ChatActivity;

public class G implements aN<T> {
    private /* synthetic */ ChatActivity cO;

    private G() {
    }

    public G(ChatActivity chatActivity) {
        this.cO = chatActivity;
    }

    public static Button getPaymentButton(Activity activity, int i, int i2) {
        return null;
    }

    public static void hideButton(View view) {
    }

    public static void initialize(Context context) {
    }

    public static void onActivityFinished(int i, Intent intent) {
    }

    private boolean onDataStateChanged(T t) {
        if (t.size() == 0) {
            this.cO.finish();
        } else {
            if (this.cO.cI == null) {
                D unused = this.cO.cI = t.get(0);
            }
            if (!t.contains(this.cO.cI)) {
                this.cO.closeActiveChat();
            } else {
                this.cO.refreshChattingBar(t);
            }
        }
        return false;
    }

    public static void showCheckoutButton(bk bkVar, int i, int i2, int i3, String str) {
    }

    public static void showPayment(Activity activity, float f, String str) {
    }

    public final /* bridge */ /* synthetic */ boolean onDataStateChanged(aP aPVar) {
        T t = (T) aPVar;
        if (t.size() == 0) {
            this.cO.finish();
        } else {
            if (this.cO.cI == null) {
                D unused = this.cO.cI = t.get(0);
            }
            if (!t.contains(this.cO.cI)) {
                this.cO.closeActiveChat();
            } else {
                this.cO.refreshChattingBar(t);
            }
        }
        return false;
    }
}
