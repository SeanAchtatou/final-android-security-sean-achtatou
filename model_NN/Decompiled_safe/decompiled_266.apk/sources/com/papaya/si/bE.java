package com.papaya.si;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import com.adknowledge.superrewards.model.SROffer;
import java.io.File;
import org.json.JSONObject;

public final class bE {
    private static bk nX;
    private static JSONObject nY;

    public static void onPhotoTaken(Activity activity, int i, int i2, Intent intent) {
        try {
            if (nX == null || nY == null) {
                X.e("webview or config is null", new Object[0]);
                nX = null;
                nY = null;
            }
            if (activity != nX.getOwnerActivity()) {
                X.dw("activity is not equal!", new Object[0]);
            }
            String optString = nY.optString("callback");
            String optString2 = nY.optString(SROffer.ID, "");
            if (i2 == -1) {
                int optInt = nY.optInt("width", 128);
                int optInt2 = nY.optInt("height", 128);
                Bitmap cameraBitmap = i == 11 ? C0030bb.getCameraBitmap(activity, intent, optInt, optInt2, true) : C0030bb.createScaledBitmap(activity.getContentResolver(), intent.getData(), optInt, optInt2, true);
                if (cameraBitmap != null) {
                    File cacheFile = C0001a.getWebCache().getCacheFile("__pria_take_" + optString2);
                    aU.saveBitmap(cameraBitmap, cacheFile, "png".equals(nY.optString("format")) ? Bitmap.CompressFormat.PNG : Bitmap.CompressFormat.JPEG, nY.optInt("quality", 35));
                    nX.callJSFunc("%s('%s', %d, '%s')", optString, optString2, 1, bz.mW + cacheFile.getName());
                } else {
                    nX.callJSFunc("%s('%s', %d, null)", optString, optString2, 0);
                }
                nX = null;
                nY = null;
            }
            nX.callJSFunc("%s('%s', %d, null)", optString, optString2, 0);
            nX = null;
            nY = null;
        } catch (Exception e) {
            X.e(e, "Failed to process taken picture", new Object[0]);
        }
    }

    public static void takePhoto(bk bkVar, JSONObject jSONObject) {
        if (!(nX == null && nY == null)) {
            X.dw("invalid states for takePhoto", new Object[0]);
        }
        nX = bkVar;
        nY = jSONObject;
        try {
            Activity ownerActivity = bkVar.getOwnerActivity();
            if (ownerActivity == null) {
                return;
            }
            if (jSONObject.optInt("source", 0) == 0) {
                C0030bb.startCameraActivity(ownerActivity, 11);
            } else {
                C0030bb.startGalleryActivity(ownerActivity, 12);
            }
        } catch (Exception e) {
            X.e(e, "Failed to start take picture", new Object[0]);
        }
    }
}
