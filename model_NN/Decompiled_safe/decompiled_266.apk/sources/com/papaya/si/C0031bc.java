package com.papaya.si;

import java.lang.ref.WeakReference;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: com.papaya.si.bc  reason: case insensitive filesystem */
public final class C0031bc<E> extends AbstractList<E> {
    private static final WeakReference[] hh = new WeakReference[0];
    /* access modifiers changed from: private */
    public int hi;
    transient WeakReference[] hj;

    /* renamed from: com.papaya.si.bc$a */
    class a implements Iterator<E> {
        private int hk;
        private int hl;
        private int hm;

        /* synthetic */ a(C0031bc bcVar) {
            this((byte) 0);
        }

        private a(byte b) {
            this.hk = C0031bc.this.hi;
            this.hl = -1;
            this.hm = C0031bc.this.modCount;
        }

        public final boolean hasNext() {
            return this.hk != 0;
        }

        public final E next() {
            C0031bc bcVar = C0031bc.this;
            int i = this.hk;
            if (bcVar.modCount != this.hm) {
                throw new ConcurrentModificationException();
            } else if (i == 0) {
                throw new NoSuchElementException();
            } else {
                this.hk = i - 1;
                int access$100 = bcVar.hi - i;
                this.hl = access$100;
                return bcVar.get(access$100);
            }
        }

        public final void remove() {
            WeakReference[] weakReferenceArr = C0031bc.this.hj;
            int i = this.hl;
            if (C0031bc.this.modCount != this.hm) {
                throw new ConcurrentModificationException();
            } else if (i < 0) {
                throw new IllegalStateException();
            } else {
                System.arraycopy(weakReferenceArr, i + 1, weakReferenceArr, i, this.hk);
                weakReferenceArr[C0031bc.access$106(C0031bc.this)] = null;
                this.hl = -1;
                this.hm = C0031bc.access$504(C0031bc.this);
            }
        }
    }

    public C0031bc() {
        this.hj = hh;
    }

    public C0031bc(int i) {
        if (i < 0) {
            throw new IllegalArgumentException();
        }
        this.hj = i == 0 ? hh : new WeakReference[i];
    }

    static /* synthetic */ int access$106(C0031bc bcVar) {
        int i = bcVar.hi - 1;
        bcVar.hi = i;
        return i;
    }

    static /* synthetic */ int access$504(C0031bc bcVar) {
        int i = bcVar.modCount + 1;
        bcVar.modCount = i;
        return i;
    }

    private static int newCapacity(int i) {
        return (i < 6 ? 12 : i >> 1) + i;
    }

    private static void throwIndexOutOfBoundsException(int i, int i2) {
        throw new IndexOutOfBoundsException("Invalid index " + i + ", size is " + i2);
    }

    public final void add(int i, E e) {
        WeakReference[] weakReferenceArr = this.hj;
        int i2 = this.hi;
        if (i > i2) {
            throwIndexOutOfBoundsException(i, i2);
        }
        if (i2 < weakReferenceArr.length) {
            System.arraycopy(weakReferenceArr, i, weakReferenceArr, i + 1, i2 - i);
        } else {
            WeakReference[] weakReferenceArr2 = new WeakReference[newCapacity(i2)];
            System.arraycopy(weakReferenceArr, 0, weakReferenceArr2, 0, i);
            System.arraycopy(weakReferenceArr, i, weakReferenceArr2, i + 1, i2 - i);
            this.hj = weakReferenceArr2;
            weakReferenceArr = weakReferenceArr2;
        }
        weakReferenceArr[i] = new WeakReference(e);
        this.hi = i2 + 1;
        this.modCount++;
    }

    public final boolean add(E e) {
        WeakReference[] weakReferenceArr = this.hj;
        int i = this.hi;
        if (i == weakReferenceArr.length) {
            WeakReference[] weakReferenceArr2 = new WeakReference[((i < 6 ? 12 : i >> 1) + i)];
            System.arraycopy(weakReferenceArr, 0, weakReferenceArr2, 0, i);
            this.hj = weakReferenceArr2;
            weakReferenceArr = weakReferenceArr2;
        }
        weakReferenceArr[i] = new WeakReference(e);
        this.hi = i + 1;
        this.modCount++;
        return true;
    }

    public final void clear() {
        if (this.hi != 0) {
            Arrays.fill(this.hj, 0, this.hi, (Object) null);
            this.hi = 0;
            this.modCount++;
        }
    }

    public final boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    public final void ensureCapacity(int i) {
        WeakReference[] weakReferenceArr = this.hj;
        if (weakReferenceArr.length < i) {
            WeakReference[] weakReferenceArr2 = new WeakReference[i];
            System.arraycopy(weakReferenceArr, 0, weakReferenceArr2, 0, this.hi);
            this.hj = weakReferenceArr2;
            this.modCount++;
        }
    }

    public final E get(int i) {
        if (i >= this.hi) {
            throwIndexOutOfBoundsException(i, this.hi);
        }
        WeakReference weakReference = this.hj[i];
        if (weakReference == null) {
            return null;
        }
        return weakReference.get();
    }

    public final int indexOf(Object obj) {
        WeakReference[] weakReferenceArr = this.hj;
        int i = this.hi;
        for (int i2 = 0; i2 < i; i2++) {
            WeakReference weakReference = weakReferenceArr[i2];
            if (obj != null) {
                if (weakReference != null && obj.equals(weakReference.get())) {
                    return i2;
                }
            } else if (weakReference != null && weakReference.get() == null) {
                return i2;
            }
        }
        return -1;
    }

    public final boolean isEmpty() {
        trimGarbage();
        return this.hi == 0;
    }

    public final Iterator<E> iterator() {
        return new a(this);
    }

    public final E remove(int i) {
        WeakReference[] weakReferenceArr = this.hj;
        int i2 = this.hi;
        if (i >= i2) {
            throwIndexOutOfBoundsException(i, i2);
        }
        E e = get(i);
        int i3 = i2 - 1;
        System.arraycopy(weakReferenceArr, i + 1, weakReferenceArr, i, i3 - i);
        weakReferenceArr[i3] = null;
        this.hi = i3;
        this.modCount++;
        return e;
    }

    public final boolean remove(Object obj) {
        int indexOf = indexOf(obj);
        if (indexOf == -1) {
            return false;
        }
        remove(indexOf);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        WeakReference[] weakReferenceArr = this.hj;
        int i3 = this.hi;
        if (i >= i3) {
            throw new IndexOutOfBoundsException("fromIndex " + i + " >= size " + this.hi);
        } else if (i2 > i3) {
            throw new IndexOutOfBoundsException("toIndex " + i2 + " > size " + this.hi);
        } else if (i > i2) {
            throw new IndexOutOfBoundsException("fromIndex " + i + " > toIndex " + i2);
        } else {
            System.arraycopy(weakReferenceArr, i2, weakReferenceArr, i, i3 - i2);
            int i4 = i2 - i;
            Arrays.fill(weakReferenceArr, i3 - i4, i3, (Object) null);
            this.hi = i3 - i4;
            this.modCount++;
        }
    }

    public final E set(int i, E e) {
        WeakReference[] weakReferenceArr = this.hj;
        if (i >= this.hi) {
            throwIndexOutOfBoundsException(i, this.hi);
        }
        E e2 = get(i);
        weakReferenceArr[i] = new WeakReference(e);
        return e2;
    }

    public final int size() {
        trimGarbage();
        return this.hi;
    }

    public final void trimGarbage() {
        Iterator it = iterator();
        while (it.hasNext()) {
            if (it.next() == null) {
                it.remove();
            }
        }
    }
}
