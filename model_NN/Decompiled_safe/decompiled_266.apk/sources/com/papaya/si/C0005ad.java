package com.papaya.si;

import com.papaya.view.Action;
import java.util.ArrayList;

/* renamed from: com.papaya.si.ad  reason: case insensitive filesystem */
public final class C0005ad extends E<C0006ae> {
    public int dO;
    public int dP;
    public String dQ;
    public String dR;
    private boolean dS = true;
    private int dT = 1;
    private ArrayList<C0006ae> dU = new ArrayList<>();

    public C0005ad() {
        setReserveGroupHeader(true);
        this.cs = new ArrayList(3);
        setImState(1);
    }

    private Action<C0005ad> createAction(int i, String str) {
        Action<C0005ad> action = new Action<>(i, null, str);
        action.data = this;
        return action;
    }

    public final boolean add(C0006ae aeVar) {
        if (aeVar.getState() == 1) {
            this.dU.add(aeVar);
        }
        return super.add((D) aeVar);
    }

    public final C0006ae get(int i) {
        return this.dS ? (C0006ae) this.cq.get(i) : this.dU.get(i);
    }

    public final C0006ae getIMUser(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.cq.size()) {
                C0006ae aeVar = (C0006ae) this.cq.get(i2);
                if (aV.equal(aeVar.dQ, str)) {
                    return aeVar;
                }
                i = i2 + 1;
            } else {
                C0006ae aeVar2 = new C0006ae();
                aeVar2.dQ = str;
                aeVar2.dX = this;
                insertSort(aeVar2);
                return aeVar2;
            }
        }
    }

    public final int getImState() {
        return this.dT;
    }

    public final String getName() {
        return this.dQ;
    }

    public final C0006ae remove(int i) {
        C0006ae aeVar = (C0006ae) super.remove(i);
        this.dU.remove(aeVar);
        return aeVar;
    }

    public final boolean remove(C0006ae aeVar) {
        this.dU.remove(aeVar);
        return super.remove((D) aeVar);
    }

    public final void setImState(int i) {
        this.dT = i;
        if (this.dT == 1) {
            this.cq.clear();
            this.dU.clear();
        }
        this.cs.clear();
        if (this.dT == 3) {
            this.cs.add(createAction(3, C0037c.getString("action_im_sign_out")));
        } else if (this.dT == 1 || this.dT == 4) {
            this.cs.add(createAction(2, C0037c.getString("action_im_sign_in")));
            this.cs.add(createAction(5, C0037c.getString("action_im_delete")));
        } else if (this.dT == 2) {
            Action<C0005ad> createAction = createAction(4, C0037c.getString("action_im_signing_in"));
            createAction.enabled = false;
            this.cs.add(createAction);
        }
    }

    public final int size() {
        return this.dS ? this.cq.size() : this.dU.size();
    }
}
