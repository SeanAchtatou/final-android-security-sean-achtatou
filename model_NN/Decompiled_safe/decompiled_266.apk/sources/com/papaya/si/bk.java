package com.papaya.si;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.openfeint.internal.request.multipart.StringPart;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

public final class bk extends WebView implements bm {
    private static final Pattern lN = Pattern.compile("&");
    private static int lO = 0;
    /* access modifiers changed from: private */
    public boolean cl = false;
    private long fR;
    private WeakReference<a> hp;
    private bG lP;
    private bG lQ;
    private URL lR;
    private bl lS = null;
    /* access modifiers changed from: private */
    public String lT = null;
    private boolean lU = false;
    private String lV;
    private bC lW;
    private boolean lX = true;
    private boolean lY = true;
    private boolean lZ = true;
    private boolean ma = false;
    private boolean mb = true;
    private bB mc;

    public interface a {
        void onPageFinished(bk bkVar, String str);

        void onPageStarted(bk bkVar, String str, Bitmap bitmap);

        void onReceivedError(bk bkVar, int i, String str, String str2);

        void onWebLoaded(bk bkVar);

        boolean shouldOverrideUrlLoading(bk bkVar, String str);
    }

    final class b extends WebChromeClient {
        /* synthetic */ b() {
            this((byte) 0);
        }

        private b(byte b) {
        }
    }

    static final class c extends WebViewClient {
        /* synthetic */ c() {
            this((byte) 0);
        }

        private c(byte b) {
        }

        public final void onPageFinished(WebView webView, final String str) {
            final bk bkVar = (bk) webView;
            final a delegate = bkVar.getDelegate();
            if (delegate != null) {
                C0030bb.postDelayed(new Runnable() {
                    public final void run() {
                        a.this.onPageFinished(bkVar, str);
                    }
                }, 200);
            }
        }

        public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            bk bkVar = (bk) webView;
            a delegate = bkVar.getDelegate();
            if (delegate != null) {
                delegate.onPageStarted(bkVar, str, bitmap);
            }
        }

        public final void onReceivedError(WebView webView, int i, String str, String str2) {
            bk bkVar = (bk) webView;
            a delegate = bkVar.getDelegate();
            if (delegate != null) {
                delegate.onReceivedError(bkVar, i, str, str2);
            }
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            bk bkVar = (bk) webView;
            a delegate = bkVar.getDelegate();
            if (delegate != null) {
                return delegate.shouldOverrideUrlLoading(bkVar, str);
            }
            X.w("webview shouldOverrideUrlLoading: delegate is null?", new Object[0]);
            return true;
        }
    }

    public bk(Context context) {
        super(context);
        setupHooks();
        lO++;
    }

    public bk(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setupHooks();
    }

    public bk(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setupHooks();
    }

    /* access modifiers changed from: private */
    public void callJSOMT(String str) {
        if (!this.cl) {
            loadUrl("javascript:" + str);
        }
    }

    private void setupHooks() {
        setScrollBarStyle(0);
        this.lV = getContext().getString(C0060z.stringID("web_default_title"));
        WebSettings settings = getSettings();
        settings.setCacheMode(2);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);
        settings.setJavaScriptEnabled(true);
        settings.setLightTouchEnabled(true);
        settings.setSaveFormData(false);
        settings.setSavePassword(false);
        settings.setLoadsImagesAutomatically(true);
        setWebChromeClient(new b());
        setWebViewClient(new c());
        this.lW = new bC(this);
        addJavascriptInterface(this.lW, "Papaya");
        setOnTouchListener(new View.OnTouchListener() {
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                try {
                    switch (motionEvent.getAction()) {
                        case 0:
                        case 1:
                            if (!view.hasFocus()) {
                                view.requestFocus();
                                break;
                            }
                            break;
                    }
                } catch (Exception e) {
                    X.e(e, "Failed in PPYWebView.onTouch", new Object[0]);
                }
                return false;
            }
        });
    }

    public final void callJS(final String str) {
        if (!this.cl) {
            if (C0030bb.isMainThread()) {
                callJSOMT(str);
            } else {
                C0030bb.runInHandlerThread(new Runnable() {
                    public final void run() {
                        if (!bk.this.cl) {
                            bk.this.callJSOMT(str);
                        }
                    }
                });
            }
        }
    }

    public final void callJSFunc(String str, Object... objArr) {
        callJS(aV.format(str, objArr));
    }

    public final void changeOrientation(int i) {
        noWarnCallJS("changeorientation", "changeorientation(" + i + ')');
    }

    public final void close() {
        lO--;
        this.cl = true;
        try {
            this.lW.close();
            setDelegate(null);
            stopLoading();
        } catch (Exception e) {
            X.e(e, "Failed to close webview", new Object[0]);
        }
        this.lP = null;
        this.lQ = null;
        try {
            destroy();
        } catch (Exception e2) {
            X.e(e2, "Failed to destroy webview", new Object[0]);
        }
    }

    public final void fireLoadError(URL url, int i, String str) {
        a delegate = getDelegate();
        if (delegate != null) {
            delegate.onReceivedError(this, i, str, url.toString());
        }
    }

    public final void fireStartLoad(URL url) {
        a delegate = getDelegate();
        if (delegate != null) {
            delegate.onPageStarted(this, url.toString(), null);
        }
    }

    public final bG getController() {
        return this.lP;
    }

    public final String getDefaultTitle() {
        return this.lV;
    }

    public final a getDelegate() {
        if (this.hp != null) {
            return this.hp.get();
        }
        return null;
    }

    public final bB getHistory() {
        return this.mc;
    }

    public final bG getLastController() {
        return this.lQ;
    }

    public final int getOrientation() {
        return C0030bb.getOrientation(getOwnerActivity());
    }

    public final Activity getOwnerActivity() {
        if (this.lP != null) {
            return this.lP.getOwnerActivity();
        }
        if (getContext() instanceof Activity) {
            return (Activity) getContext();
        }
        return null;
    }

    public final String getPageName() {
        return this.lT;
    }

    public final bC getPapayaScript() {
        return this.lW;
    }

    public final URL getPapayaURL() {
        return this.lR;
    }

    public final long getStartTime() {
        return this.fR;
    }

    public final boolean isClosed() {
        return this.cl;
    }

    public final boolean isGlobalReusable() {
        return this.ma;
    }

    public final boolean isLoadFromString() {
        return this.lU;
    }

    public final boolean isMenuEnabled() {
        return this.mb;
    }

    public final boolean isRecylable() {
        return this.lZ;
    }

    public final boolean isRequireSid() {
        return this.lX;
    }

    public final boolean isReusable() {
        return this.lY;
    }

    public final void loadPapayaURL(URL url) {
        if (url != null) {
            if (this.lS != null) {
                this.lS.cancel();
                this.lS.setDelegate(null);
                this.lS = null;
            }
            if (aV.isNotEmpty(this.lT)) {
                noWarnCallJS("webdestroyed", "webdestroyed();");
            }
            this.lW.setRequestJson(null);
            this.lY = true;
            this.ma = false;
            this.lZ = true;
            this.mb = true;
            String path = url.getPath();
            try {
                if (!aV.isEmpty(path)) {
                    String query = url.getQuery();
                    JSONObject jSONObject = new JSONObject();
                    if (!aV.isEmpty(query)) {
                        String[] split = lN.split(query);
                        for (String str : split) {
                            int indexOf = str.indexOf("=");
                            int indexOf2 = str.indexOf("#");
                            if (indexOf2 != -1) {
                                jSONObject.put("__anchor__", str.substring(indexOf2 + 1));
                            }
                            if (indexOf != -1) {
                                String substring = str.substring(0, indexOf);
                                String substring2 = indexOf2 == -1 ? str.substring(indexOf + 1) : str.substring(indexOf + 1, indexOf2);
                                if (!aV.isEmpty(substring) && !aV.isEmpty(substring2)) {
                                    jSONObject.put(substring, Uri.decode(substring2));
                                }
                            }
                        }
                        jSONObject.put("__current__", path + '?' + query);
                    } else {
                        jSONObject.put("__current__", path);
                    }
                    jSONObject.put("__page__", path);
                    this.lW.setRequestJson(jSONObject.toString());
                }
            } catch (JSONException e) {
                X.w("Failed to parse page url: " + e, new Object[0]);
            }
            this.lU = false;
            fireStartLoad(url);
            this.lS = new bl(url, this.lX);
            this.lS.setDelegate(this);
            this.lS.start();
        }
    }

    public final URL newURI(String str) {
        if (aV.isEmpty(str)) {
            return null;
        }
        return this.lR != null ? C0034bf.createURL(str, this.lR) : str.contains("://") ? C0034bf.createURL(str, null) : C0034bf.createURL(C0056v.bi + str, null);
    }

    public final void noWarnCallJS(String str, String str2) {
        callJS(aV.format("if (typeof %s != 'undefined') %s;", str, str2));
    }

    public final void onPageContentLoadFailed(bl blVar) {
        if (blVar == this.lS) {
            this.lS = null;
            fireLoadError(blVar.getUrl(), -1, null);
        }
    }

    public final void onPageContentLoaded(bl blVar) {
        if (!this.cl) {
            this.lT = this.lS.getPageName();
            C0026ay webCache = C0001a.getWebCache();
            String pageContent = blVar.getPageContent();
            this.lS = null;
            this.lU = true;
            URL redirectUrl = blVar.getRedirectUrl();
            if (redirectUrl != null) {
                this.lR = redirectUrl;
            } else {
                this.lR = blVar.getUrl();
            }
            this.fR = System.currentTimeMillis();
            String createLocalRefHtml = webCache.createLocalRefHtml(pageContent, this.lR, true, this.lX);
            C0030bb.assertMainThread();
            loadDataWithBaseURL(this.lR.toString(), createLocalRefHtml, StringPart.DEFAULT_CONTENT_TYPE, "UTF-8", null);
        }
    }

    public final void openUriString(String str) {
        loadUrl(str);
    }

    public final void setClosed(boolean z) {
        this.cl = z;
    }

    public final void setController(bG bGVar) {
        this.lP = bGVar;
        if (bGVar != null) {
            this.lQ = bGVar;
        }
    }

    public final void setDefaultTitle(String str) {
        this.lV = str;
    }

    public final void setDelegate(a aVar) {
        this.hp = new WeakReference<>(aVar);
    }

    public final void setGlobalReusable(boolean z) {
        this.ma = z;
    }

    public final void setHistory(bB bBVar) {
        this.mc = bBVar;
    }

    public final void setLastController(bG bGVar) {
        this.lQ = bGVar;
    }

    public final void setLoadFromString(boolean z) {
        this.lU = z;
    }

    public final void setMenuEnabled(boolean z) {
        this.mb = z;
    }

    public final void setPapayaScript(bC bCVar) {
        this.lW = bCVar;
    }

    public final void setPapayaURL(URL url) {
        this.lR = url;
        if (this.mc != null) {
            this.mc.setURL(url);
        }
    }

    public final void setRecylable(boolean z) {
        this.lZ = z;
    }

    public final void setRequireSid(boolean z) {
        this.lX = z;
    }

    public final void setReusable(boolean z) {
        this.lY = z;
    }

    public final void setStartTime(long j) {
        this.fR = j;
    }

    public final void setVisibility(int i) {
        super.setVisibility(i);
        this.lW.updateViewsVisibility(i);
        if (i == 0) {
            requestFocus(130);
            bG bGVar = this.lP;
            if (bGVar == null) {
                X.w("controller is null, parent %s", getParent());
            } else if (getParent() != bGVar.getWebContentView()) {
                X.w("inconsistent parent views, %s, %s", getParent(), bGVar.getWebContentView());
                C0030bb.addView(bGVar.getWebContentView(), this, true);
            }
        }
    }

    public final String toString() {
        return "PPYWebView [controller=" + this.lP + ", lastController=" + this.lQ + ", papayaURL=" + this.lR + ", loadFromString=" + this.lU + ", closed=" + this.cl + ", reusable=" + this.lY + ", recylable=" + this.lZ + ", globalReusable=" + this.ma + "]";
    }

    public final void updateTitleFromHTML() {
        callJS("window.Papaya.updateTitle_(document.title)");
    }
}
