package com.papaya.si;

import android.graphics.drawable.Drawable;

public final class Q extends D {
    public int df;
    public String dg;
    public int dh;
    public int di;
    public boolean dj;
    public boolean dk = false;
    public E<S> dl = new E<>();
    public String name;

    public final void addUser(S s) {
        removeUser(s.cW);
        this.dl.add(s);
        this.dh = this.dl.size();
    }

    public final S findUser(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.dl.size()) {
                return null;
            }
            S s = this.dl.get(i3);
            if (s.cW == i) {
                return s;
            }
            i2 = i3 + 1;
        }
    }

    public final Drawable getDefaultDrawable() {
        return C0037c.getBitmapDrawable("chatroom_default");
    }

    public final String getTimeLabel() {
        return null;
    }

    public final String getTitle() {
        return aV.format("%s (%d/%d)", this.name, Integer.valueOf(this.dh), Integer.valueOf(this.di));
    }

    public final boolean isGrayScaled() {
        return this.state == 0;
    }

    public final void logout() {
        this.state = 0;
        this.dl.clear();
        if (this.dh > 1) {
            this.dh--;
        }
    }

    public final S removeUser(int i) {
        S s;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.dl.size()) {
                s = null;
                break;
            } else if (this.dl.get(i3).cW == i) {
                s = this.dl.remove(i3);
                break;
            } else {
                i2 = i3 + 1;
            }
        }
        this.dh = this.dl.size();
        return s;
    }
}
