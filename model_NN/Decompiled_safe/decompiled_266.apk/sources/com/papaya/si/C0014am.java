package com.papaya.si;

import android.content.Context;
import java.util.HashMap;
import java.util.Vector;

/* renamed from: com.papaya.si.am  reason: case insensitive filesystem */
public final class C0014am implements aM, C0058x {
    private static C0014am eg;
    private HashMap<Integer, HashMap<String, C0013al>> eh = new HashMap<>();

    private C0014am() {
    }

    public static synchronized C0014am getInstance() {
        C0014am amVar;
        synchronized (C0014am.class) {
            if (eg == null) {
                eg = new C0014am();
            }
            amVar = eg;
        }
        return amVar;
    }

    public final synchronized void addDatabase(C0013al alVar) {
        if (alVar != null) {
            if (alVar.getDbId() != null) {
                HashMap<String, C0013al> ensureMap = ensureMap(alVar.getScope());
                if (!ensureMap.containsKey(alVar.getDbId())) {
                    ensureMap.put(alVar.getDbId(), alVar);
                } else {
                    X.w("key already exists %s", alVar.getDbId());
                }
            }
        }
        X.e("db or db name is null %s", alVar);
    }

    public final synchronized void clear() {
        try {
            if (C0037c.A != null) {
                C0037c.A.removeConnectionDelegate(this);
            }
        } catch (Exception e) {
            X.w(e, "Failed to removeConnectionDelegate", new Object[0]);
        }
        try {
            for (HashMap<String, C0013al> values : this.eh.values()) {
                for (C0013al close : values.values()) {
                    close.close();
                }
            }
            this.eh.clear();
        } catch (Exception e2) {
            X.e("Failed to close databases: " + e2, new Object[0]);
        }
        return;
    }

    public final C0013al connectionDB() {
        return openDatabase("__connection__", 3);
    }

    /* access modifiers changed from: protected */
    public final C0013al createDatabase(String str, int i) {
        String format;
        C0013al openFileDatabase;
        if (i <= 3) {
            if (i != 3) {
                X.w("DB with scope below connection SHOULD be managed at other places, %s, %d", str, Integer.valueOf(i));
            }
            openFileDatabase = C0013al.openMemoryDatabase();
            openFileDatabase.setDbId(str);
            openFileDatabase.setScope(i);
        } else {
            String md5 = aU.md5(str);
            if (i != 6) {
                format = aV.format("%d.%d.%s.db", Integer.valueOf(i), Integer.valueOf(C0037c.getSession().getUserID()), md5);
            } else {
                format = aV.format("%d.%s.db", Integer.valueOf(i), md5);
            }
            openFileDatabase = C0013al.openFileDatabase(format);
            openFileDatabase.setDbId(str);
            openFileDatabase.setScope(i);
            if (i <= 4) {
                openFileDatabase.setSynchronousLevel(0);
            }
        }
        addDatabase(openFileDatabase);
        return openFileDatabase;
    }

    /* access modifiers changed from: protected */
    public final HashMap<String, C0013al> ensureMap(int i) {
        HashMap<String, C0013al> hashMap = this.eh.get(Integer.valueOf(i));
        if (hashMap != null) {
            return hashMap;
        }
        HashMap<String, C0013al> hashMap2 = new HashMap<>();
        this.eh.put(Integer.valueOf(i), hashMap2);
        return hashMap2;
    }

    public final synchronized int featureVisible(String str) {
        int kvInt;
        kvInt = sessionDB().kvInt("k_visible_" + str, -1);
        if (kvInt == -1) {
            C0037c.A.send(109, str);
            kvInt = 0;
        }
        return kvInt;
    }

    public final synchronized C0013al findDatabase(String str, int i) {
        HashMap hashMap;
        hashMap = this.eh.get(Integer.valueOf(i));
        return hashMap != null ? (C0013al) hashMap.get(str) : null;
    }

    public final void initialize(Context context) {
        try {
            String[] databaseList = context.databaseList();
            for (String str : databaseList) {
                if (str.startsWith("4.") && str.endsWith(".db")) {
                    if (!context.deleteDatabase(str)) {
                        X.w("Failed to delete database %s", str);
                    } else {
                        X.d("Deleted database %s", str);
                    }
                }
            }
            C0037c.A.addConnectionDelegate(this);
        } catch (Exception e) {
            X.e(e, "Failed in WebDatabaseManager.initialize", new Object[0]);
        }
    }

    public final void onConnectionEstablished() {
    }

    public final synchronized void onConnectionLost() {
        HashMap hashMap = this.eh.get(3);
        if (hashMap != null) {
            for (C0013al close : hashMap.values()) {
                close.close();
            }
            this.eh.remove(3);
        }
    }

    public final synchronized C0013al openDatabase(String str, int i) {
        C0013al findDatabase;
        findDatabase = findDatabase(str, i);
        if (findDatabase == null) {
            findDatabase = createDatabase(str, i);
        }
        return findDatabase;
    }

    public final synchronized void serverPushedUpdate(Vector vector) {
        Object[] objArr;
        String str = (String) vector.get(1);
        int intValue = aV.intValue(vector.get(2));
        if (intValue >= 3) {
            C0013al openDatabase = openDatabase(str, intValue);
            if (vector.size() >= 5) {
                Vector vector2 = (Vector) vector.get(5);
                objArr = vector2.toArray(new Object[vector2.size()]);
            } else {
                objArr = null;
            }
            openDatabase.update((String) vector.get(3), objArr);
        }
    }

    public final C0013al sessionDB() {
        return openDatabase("__session__", 4);
    }

    public final C0013al settingDB() {
        return openDatabase("__setting__", 6);
    }

    public final synchronized void updateFeatureVisible(Vector vector) {
        int i = 0;
        synchronized (this) {
            Vector vector2 = (Vector) vector.get(1);
            C0013al sessionDB = sessionDB();
            while (true) {
                int i2 = i;
                if (i2 < vector2.size()) {
                    sessionDB.kvSaveInt("k_visible_" + ((String) vector2.get(i2)), aV.intValue(vector2.get(i2 + 1), 0), -1);
                    i = i2 + 2;
                }
            }
        }
    }
}
