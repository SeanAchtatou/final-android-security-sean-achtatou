package com.papaya.si;

import android.net.Uri;
import com.papaya.si.bt;
import java.net.URL;
import java.util.ArrayList;

public final class bl extends C0033be<bm> {
    /* access modifiers changed from: private */
    public String lT;
    private boolean lX;
    /* access modifiers changed from: private */
    public URL mh;
    /* access modifiers changed from: private */
    public bv mi;
    /* access modifiers changed from: private */
    public b mj;
    /* access modifiers changed from: private */
    public String mk;
    /* access modifiers changed from: private */
    public ArrayList ml;
    /* access modifiers changed from: private */
    public ArrayList<bv> mm;
    private c mn;
    private URL url;

    static final class a extends bv {
        String name;

        public a(String str) {
            this.name = str;
            URL createURL = C0034bf.createURL(aV.format("misc/include?name=%s&identifier=%s&version=%d&m=%s&d=%d", Uri.encode(str), "com.papaya.socialsdk.corona", 171, "social_170", 0));
            if (createURL == null) {
                X.e("invalid include URL: " + str, new Object[0]);
            }
            setUrl(createURL);
            setCacheable(false);
        }
    }

    final class b implements bt.a {
        /* synthetic */ b(bl blVar) {
            this((byte) 0);
        }

        private b(byte b) {
        }

        public final void connectionFailed(bt btVar, int i) {
            bv unused = bl.this.mi = null;
            b unused2 = bl.this.mj = null;
            String unused3 = bl.this.mk = C0016ao.getInstance().newPageContent(bl.this.lT, false);
            if (aV.isEmpty(bl.this.mk)) {
                bm bmVar = (bm) bl.this.getDelegate();
                if (bmVar != null) {
                    bmVar.onPageContentLoadFailed(bl.this);
                    return;
                }
                return;
            }
            bl.this.analyzePageContent();
        }

        public final void connectionFinished(bt btVar) {
            bv unused = bl.this.mi = null;
            b unused2 = bl.this.mj = null;
            String unused3 = bl.this.mk = aV.utf8String(btVar.getData(), null);
            if (bl.this.lT != null && bl.this.lT.startsWith("static_") && !aV.isEmpty(bl.this.mk)) {
                C0016ao.getInstance().savePage(bl.this.lT, bl.this.mk);
            }
            URL unused4 = bl.this.mh = btVar.getRedirectUrl();
            bl.this.analyzePageContent();
        }
    }

    final class c implements bt.a {
        /* synthetic */ c(bl blVar) {
            this((byte) 0);
        }

        private c(byte b) {
        }

        public final void connectionFailed(bt btVar, int i) {
            synchronized (bl.this.mm) {
                if (btVar != null) {
                    a aVar = (a) btVar.getRequest();
                    bl.this.mm.remove(aVar);
                    int indexOf = bl.this.ml.indexOf(aVar);
                    if (indexOf != -1) {
                        String newPageContent = C0016ao.getInstance().newPageContent(aVar.name, false);
                        if (aV.isEmpty(newPageContent)) {
                            bl.this.ml.remove(aVar);
                        } else {
                            bl.this.ml.set(indexOf, newPageContent);
                        }
                    } else {
                        X.w("Can't find segment: " + aVar, new Object[0]);
                    }
                    X.w("Failed to load include segment: " + aVar.getUrl(), new Object[0]);
                }
                if (bl.this.mm.isEmpty()) {
                    bl.this.combineSegments();
                }
            }
        }

        public final void connectionFinished(bt btVar) {
            synchronized (bl.this.mm) {
                if (btVar != null) {
                    a aVar = (a) btVar.getRequest();
                    bl.this.mm.remove(aVar);
                    int indexOf = bl.this.ml.indexOf(aVar);
                    if (indexOf != -1) {
                        String utf8String = aV.utf8String(btVar.getData(), "");
                        if (!aV.isEmpty(utf8String)) {
                            C0016ao.getInstance().savePage(aVar.name, utf8String);
                        }
                        bl.this.ml.set(indexOf, utf8String);
                    } else {
                        X.w("Can't find segment: " + aVar, new Object[0]);
                    }
                }
                if (bl.this.mm.isEmpty()) {
                    bl.this.combineSegments();
                }
            }
        }
    }

    public bl(URL url2, boolean z) {
        this.url = url2;
        this.lX = z;
    }

    /* access modifiers changed from: protected */
    public final void analyzePageContent() {
        if (this.mk != null) {
            this.ml = new ArrayList();
            this.mm = new ArrayList<>();
            this.mn = new c(this);
            C0016ao instance = C0016ao.getInstance();
            boolean z = false;
            int i = 0;
            do {
                int indexOf = this.mk.indexOf("<!---include(\"", i);
                if (indexOf != -1) {
                    int indexOf2 = this.mk.indexOf("\")-->", indexOf);
                    if (indexOf2 != -1) {
                        this.ml.add(this.mk.substring(i, indexOf));
                        i = "\")-->".length() + indexOf2;
                        String substring = this.mk.substring("<!---include(\"".length() + indexOf, indexOf2);
                        String newPageContent = instance.newPageContent(substring, true);
                        if (newPageContent != null) {
                            this.ml.add(newPageContent);
                        } else {
                            a aVar = new a(substring);
                            aVar.setDelegate(this.mn);
                            aVar.setRequireSid(this.lX);
                            this.ml.add(aVar);
                            this.mm.add(aVar);
                        }
                    } else {
                        int indexOf3 = this.mk.indexOf("-->", indexOf);
                        if (indexOf3 != -1) {
                            i = "-->".length() + indexOf3;
                        } else {
                            z = true;
                        }
                    }
                }
                if (indexOf == -1) {
                    break;
                }
            } while (!z);
            if (z) {
                X.e("Failed to parse include syntax: " + this.url, new Object[0]);
            }
            if (i == 0) {
                this.ml.add(this.mk);
            } else if (i < this.mk.length()) {
                this.ml.add(this.mk.substring(i, this.mk.length()));
            }
            if (this.mm.isEmpty()) {
                combineSegments();
            } else {
                C0001a.insertRequests(this.mm);
            }
        }
    }

    public final void cancel() {
    }

    /* access modifiers changed from: protected */
    public final void combineSegments() {
        if (this.mm.isEmpty()) {
            if (this.ml.size() == 1) {
                this.mk = (String) this.ml.get(0);
            } else {
                int i = 0;
                int i2 = 0;
                while (i < this.ml.size()) {
                    i++;
                    i2 = ((String) this.ml.get(i)).length() + i2;
                }
                StringBuilder acquireStringBuilder = aV.acquireStringBuilder(i2);
                for (int i3 = 0; i3 < this.ml.size(); i3++) {
                    acquireStringBuilder.append((String) this.ml.get(i3));
                }
                this.mk = aV.releaseStringBuilder(acquireStringBuilder);
            }
            if (C0030bb.isMainThread()) {
                bm bmVar = (bm) getDelegate();
                if (bmVar != null) {
                    bmVar.onPageContentLoaded(this);
                    return;
                }
                return;
            }
            C0030bb.runInHandlerThread(new Runnable() {
                public final void run() {
                    bm bmVar = (bm) bl.this.getDelegate();
                    if (bmVar != null) {
                        bmVar.onPageContentLoaded(bl.this);
                    }
                }
            });
            return;
        }
        X.w("segment requests are not empty: " + this.mm, new Object[0]);
    }

    public final String getPageContent() {
        return this.mk;
    }

    public final String getPageName() {
        return this.lT;
    }

    public final URL getRedirectUrl() {
        return this.mh;
    }

    public final URL getUrl() {
        return this.url;
    }

    /* access modifiers changed from: protected */
    public final void parseRequires() {
    }

    public final void start() {
        if (this.mk == null && this.mi == null) {
            String path = this.url.getPath();
            int indexOf = path.indexOf("/static_");
            if (indexOf != -1) {
                this.lT = path.substring(indexOf + 1);
                this.mk = C0016ao.getInstance().newPageContent(this.lT, true);
            }
            if (this.mk == null) {
                if (!aV.isEmpty(this.lT)) {
                    URL createURL = C0034bf.createURL(aV.format("misc/page?name=%s&identifier=%s&version=%d&m=%s&d=%d", Uri.encode(this.lT), "com.papaya.socialsdk.corona", 171, "social_170", 0));
                    if (createURL != null) {
                        this.mi = new bv(createURL, false);
                    } else {
                        X.e("page url is null " + this.lT, new Object[0]);
                    }
                }
                if (this.mi == null) {
                    this.mi = new bv(this.url, false);
                }
                this.mi.setRequireSid(this.lX);
                this.mj = new b(this);
                this.mi.setDelegate(this.mj);
                this.mi.start(true);
                return;
            }
            analyzePageContent();
        }
    }
}
