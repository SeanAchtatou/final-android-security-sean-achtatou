package com.papaya.si;

import android.graphics.drawable.Drawable;
import android.util.SparseArray;
import java.lang.ref.WeakReference;

public final class L extends D {
    public int cS;
    public boolean cT;
    public int cU;
    private SparseArray<WeakReference<N>> cV;
    public String description;
    public String name;

    public L() {
        this.state = 1;
    }

    public final Drawable getDefaultDrawable() {
        return C0037c.getDrawable("chatgroup_default_1");
    }

    public final CharSequence getSubtitle() {
        return this.cT ? C0037c.getString("administrator") : this.description;
    }

    public final String getTimeLabel() {
        return null;
    }

    public final String getTitle() {
        return this.name;
    }

    public final N getUserCard(int i, String str) {
        N n;
        if (this.cV == null) {
            this.cV = new SparseArray<>();
        }
        WeakReference weakReference = this.cV.get(i);
        if (weakReference != null) {
            n = (N) weakReference.get();
            if (n == null) {
                this.cV.remove(i);
            }
        } else {
            n = null;
        }
        if (n == null) {
            n = new N();
            n.cW = i;
            n.name = str;
            this.cV.put(i, new WeakReference(n));
        }
        n.name = str;
        return n;
    }

    public final boolean isGrayScaled() {
        return false;
    }
}
