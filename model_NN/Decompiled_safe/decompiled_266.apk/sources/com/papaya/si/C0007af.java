package com.papaya.si;

/* renamed from: com.papaya.si.af  reason: case insensitive filesystem */
public final class C0007af extends E<C0008ag> {
    public final C0008ag findBySID(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.cq.size()) {
                return null;
            }
            C0008ag agVar = (C0008ag) this.cq.get(i3);
            if (agVar.dm == i) {
                return agVar;
            }
            i2 = i3 + 1;
        }
    }
}
