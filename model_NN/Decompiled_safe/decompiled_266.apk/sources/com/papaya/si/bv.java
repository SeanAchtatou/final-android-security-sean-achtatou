package com.papaya.si;

import com.papaya.si.bt;
import java.io.File;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.HashMap;

public class bv {
    private WeakReference<bt.a> hp;
    protected boolean lX = true;
    private int mF = 0;
    private int mG = 0;
    protected boolean mH = false;
    protected boolean mI = false;
    private File mJ;
    protected HashMap<String, aY<Integer, Object>> mK;
    public int mL = -1;
    public int mM = -1;
    protected URL url;

    public bv() {
    }

    public bv(URL url2, boolean z) {
        this.url = url2;
        this.mH = z;
    }

    public void addPostParam(String str, Object obj, int i) {
        if (str != null && obj != null) {
            if (this.mK == null) {
                this.mK = new HashMap<>();
            }
            this.mK.put(str, new aY(Integer.valueOf(i), obj));
        }
    }

    public void addPostParam(String str, String str2) {
        addPostParam(str, str2, 0);
    }

    public void cancel() {
        setDelegate(null);
        try {
            C0001a.getWebCache().removeRequest(this);
        } catch (Exception e) {
            X.e(e, "Failed to cancel request", new Object[0]);
        }
    }

    public int getConnectionType() {
        return this.mG;
    }

    public bt.a getDelegate() {
        if (this.hp != null) {
            return this.hp.get();
        }
        return null;
    }

    public int getRequestType() {
        return this.mF;
    }

    public File getSaveFile() {
        return this.mJ;
    }

    public URL getUrl() {
        return this.url;
    }

    public boolean isCacheable() {
        return this.mH;
    }

    public boolean isDispatchable() {
        return this.mI;
    }

    public boolean isRequireSid() {
        return this.lX;
    }

    public void setCacheable(boolean z) {
        this.mH = z;
    }

    public void setConnectionType(int i) {
        this.mG = i;
    }

    public void setDelegate(bt.a aVar) {
        if (aVar == null) {
            this.hp = null;
        } else {
            this.hp = new WeakReference<>(aVar);
        }
    }

    public void setDispatchable(boolean z) {
        this.mI = z;
    }

    public void setRequestType(int i) {
        this.mF = i;
    }

    public void setRequireSid(boolean z) {
        this.lX = z;
    }

    public void setSaveFile(File file) {
        this.mJ = file;
    }

    public void setUrl(URL url2) {
        this.url = url2;
    }

    public void start(boolean z) {
        if (z) {
            C0001a.getWebCache().insertRequest(this);
        } else {
            C0001a.getWebCache().appendRequest(this);
        }
    }

    public String toString() {
        return "UrlRequest{_url=" + this.url + ", _requestType=" + this.mF + ", _connectionType=" + this.mG + ", _cacheable=" + this.mH + ", _requireSid=" + this.lX + ", _delegateRef=" + this.hp + '}';
    }
}
