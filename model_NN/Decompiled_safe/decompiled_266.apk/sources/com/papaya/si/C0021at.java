package com.papaya.si;

import com.papaya.social.PPYSocialChallengeRecord;
import com.papaya.social.PPYSocialChallengeService;
import java.util.ArrayList;

/* renamed from: com.papaya.si.at  reason: case insensitive filesystem */
public final class C0021at {
    private static C0021at fg = new C0021at();
    private ArrayList<PPYSocialChallengeService.Delegate> fh = new ArrayList<>();

    private C0021at() {
    }

    public static C0021at instance() {
        return fg;
    }

    public final synchronized void addDelegate(PPYSocialChallengeService.Delegate delegate) {
        if (delegate != null) {
            if (!this.fh.contains(delegate)) {
                this.fh.add(delegate);
            }
        }
    }

    public final synchronized void fireChallengeAccepted(PPYSocialChallengeRecord pPYSocialChallengeRecord) {
        int i = 0;
        synchronized (this) {
            while (true) {
                int i2 = i;
                if (i2 < this.fh.size()) {
                    PPYSocialChallengeService.Delegate delegate = this.fh.get(i2);
                    try {
                        delegate.onChallengeAccepted(pPYSocialChallengeRecord);
                    } catch (Exception e) {
                        X.w(e, "Failed to invoke onChallengeAccepted of " + delegate, new Object[0]);
                    }
                    i = i2 + 1;
                }
            }
        }
    }

    public final synchronized void removedelegate(PPYSocialChallengeService.Delegate delegate) {
        this.fh.remove(delegate);
    }
}
