package com.papaya.si;

import com.papaya.si.bt;

public final class bs extends bt implements Runnable {
    private boolean mw = false;

    public bs(bv bvVar) {
        super(bvVar);
    }

    public bs(bv bvVar, bt.a aVar) {
        super(bvVar, aVar);
    }

    public final void cancel() {
        this.mw = true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:106:0x0230 A[Catch:{ Exception -> 0x0108 }] */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x025f A[Catch:{ Exception -> 0x0108 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00dd A[Catch:{ Exception -> 0x0108 }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01d0 A[Catch:{ Exception -> 0x0108 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:111:0x0247=Splitter:B:111:0x0247, B:80:0x01bc=Splitter:B:80:0x01bc} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r14 = this;
            r12 = 1
            r11 = -1
            r9 = 0
            r10 = 0
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r3 = com.papaya.si.aV.acquireBytes(r0)
            r0 = 0
            r14.mz = r0     // Catch:{ Exception -> 0x0108 }
            com.papaya.si.bv r0 = r14.jn     // Catch:{ Exception -> 0x0108 }
            java.net.URL r4 = r0.getUrl()     // Catch:{ Exception -> 0x0108 }
            com.papaya.si.bv r0 = r14.jn     // Catch:{ Exception -> 0x0108 }
            java.util.HashMap<java.lang.String, com.papaya.si.aY<java.lang.Integer, java.lang.Object>> r0 = r0.mK     // Catch:{ Exception -> 0x0108 }
            if (r0 == 0) goto L_0x00eb
            java.net.URLConnection r0 = com.papaya.si.bj.createConnection(r4)     // Catch:{ Exception -> 0x0108 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0108 }
            java.lang.String r1 = com.papaya.si.bj.createBoundary()     // Catch:{ Exception -> 0x0108 }
            java.lang.String r2 = "Content-Type"
            java.lang.String r5 = com.papaya.si.bj.getContentType(r1)     // Catch:{ Exception -> 0x0108 }
            r0.setRequestProperty(r2, r5)     // Catch:{ Exception -> 0x0108 }
            java.lang.String r2 = "Connection"
            java.lang.String r5 = "Keep-Alive"
            r0.setRequestProperty(r2, r5)     // Catch:{ Exception -> 0x0108 }
            java.lang.String r2 = "Cache-Control"
            java.lang.String r5 = "no-cache"
            r0.setRequestProperty(r2, r5)     // Catch:{ Exception -> 0x0108 }
            r5 = r0
            r0 = r1
        L_0x003d:
            com.papaya.si.bv r1 = r14.jn     // Catch:{ Exception -> 0x0108 }
            boolean r1 = r1.isRequireSid()     // Catch:{ Exception -> 0x0108 }
            if (r1 == 0) goto L_0x0064
            com.papaya.si.ax r1 = com.papaya.si.C0025ax.getInstance()     // Catch:{ Exception -> 0x0108 }
            java.lang.String r1 = r1.getSessionKey()     // Catch:{ Exception -> 0x0108 }
            if (r1 != 0) goto L_0x0057
            java.lang.String r1 = "sid should NOT be null"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0108 }
            com.papaya.si.X.e(r1, r2)     // Catch:{ Exception -> 0x0108 }
        L_0x0057:
            java.lang.String r1 = "social-sid"
            com.papaya.si.ax r2 = com.papaya.si.C0025ax.getInstance()     // Catch:{ Exception -> 0x0108 }
            java.lang.String r2 = r2.getSessionKey()     // Catch:{ Exception -> 0x0108 }
            r5.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x0108 }
        L_0x0064:
            java.lang.String r1 = "Accept-Encoding"
            java.lang.String r2 = "gzip"
            r5.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x0108 }
            com.papaya.si.bv r1 = r14.jn     // Catch:{ Exception -> 0x0108 }
            int r1 = r1.mL     // Catch:{ Exception -> 0x0108 }
            if (r1 <= 0) goto L_0x00f5
            com.papaya.si.bv r1 = r14.jn     // Catch:{ Exception -> 0x0108 }
            int r1 = r1.mL     // Catch:{ Exception -> 0x0108 }
        L_0x0075:
            r5.setConnectTimeout(r1)     // Catch:{ Exception -> 0x0108 }
            com.papaya.si.bv r1 = r14.jn     // Catch:{ Exception -> 0x0108 }
            int r1 = r1.mM     // Catch:{ Exception -> 0x0108 }
            if (r1 <= 0) goto L_0x00fa
            com.papaya.si.bv r1 = r14.jn     // Catch:{ Exception -> 0x0108 }
            int r1 = r1.mM     // Catch:{ Exception -> 0x0108 }
            r5.setReadTimeout(r1)     // Catch:{ Exception -> 0x0108 }
        L_0x0085:
            com.papaya.si.bv r1 = r14.jn     // Catch:{ Exception -> 0x0108 }
            java.util.HashMap<java.lang.String, com.papaya.si.aY<java.lang.Integer, java.lang.Object>> r1 = r1.mK     // Catch:{ Exception -> 0x0108 }
            if (r1 != 0) goto L_0x011c
            r5.connect()     // Catch:{ Exception -> 0x0108 }
        L_0x008e:
            int r0 = r5.getResponseCode()     // Catch:{ Exception -> 0x0108 }
            r1 = 200(0xc8, float:2.8E-43)
            if (r0 != r1) goto L_0x026a
            com.papaya.si.bv r0 = r14.jn     // Catch:{ Exception -> 0x0108 }
            java.io.File r0 = r0.getSaveFile()     // Catch:{ Exception -> 0x0108 }
            if (r0 != 0) goto L_0x01db
            java.io.InputStream r0 = com.papaya.si.C0034bf.getInputStream(r5)     // Catch:{ Exception -> 0x0294, all -> 0x01c6 }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0299, all -> 0x0285 }
            int r2 = r5.getContentLength()     // Catch:{ Exception -> 0x0299, all -> 0x0285 }
            if (r2 != r11) goto L_0x01b1
            r2 = 1024(0x400, float:1.435E-42)
        L_0x00ac:
            r1.<init>(r2)     // Catch:{ Exception -> 0x0299, all -> 0x0285 }
        L_0x00af:
            boolean r2 = r14.mw     // Catch:{ Exception -> 0x00c4, all -> 0x028b }
            if (r2 != 0) goto L_0x01b7
            int r2 = r0.read(r3)     // Catch:{ Exception -> 0x00c4, all -> 0x028b }
            if (r2 == r11) goto L_0x01b7
            long r5 = r14.mz     // Catch:{ Exception -> 0x00c4, all -> 0x028b }
            long r7 = (long) r2     // Catch:{ Exception -> 0x00c4, all -> 0x028b }
            long r5 = r5 + r7
            r14.mz = r5     // Catch:{ Exception -> 0x00c4, all -> 0x028b }
            r5 = 0
            r1.write(r3, r5, r2)     // Catch:{ Exception -> 0x00c4, all -> 0x028b }
            goto L_0x00af
        L_0x00c4:
            r2 = move-exception
            r13 = r2
            r2 = r0
            r0 = r13
        L_0x00c8:
            java.lang.String r5 = "Failed to get data from %s"
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ all -> 0x0291 }
            r7 = 0
            r6[r7] = r4     // Catch:{ all -> 0x0291 }
            com.papaya.si.X.w(r0, r5, r6)     // Catch:{ all -> 0x0291 }
            com.papaya.si.aU.close(r2)     // Catch:{ Exception -> 0x0108 }
            com.papaya.si.aU.close(r1)     // Catch:{ Exception -> 0x0108 }
            r0 = r1
            r1 = r10
        L_0x00db:
            if (r1 == 0) goto L_0x01d0
            byte[] r0 = r0.toByteArray()     // Catch:{ Exception -> 0x0108 }
            r14.setData(r0)     // Catch:{ Exception -> 0x0108 }
            r14.fireConnectionFinished()     // Catch:{ Exception -> 0x0108 }
        L_0x00e7:
            com.papaya.si.aV.releaseBytes(r3)
            return
        L_0x00eb:
            java.net.URLConnection r0 = r4.openConnection()     // Catch:{ Exception -> 0x0108 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0108 }
            r5 = r0
            r0 = r9
            goto L_0x003d
        L_0x00f5:
            r1 = 45000(0xafc8, float:6.3058E-41)
            goto L_0x0075
        L_0x00fa:
            com.papaya.si.bv r1 = r14.jn     // Catch:{ Exception -> 0x0108 }
            boolean r1 = r1.mH     // Catch:{ Exception -> 0x0108 }
            if (r1 != 0) goto L_0x0114
            r1 = 60000(0xea60, float:8.4078E-41)
            r5.setReadTimeout(r1)     // Catch:{ Exception -> 0x0108 }
            goto L_0x0085
        L_0x0108:
            r0 = move-exception
            java.lang.String r1 = "http connection failed"
            java.lang.Object[] r2 = new java.lang.Object[r10]
            com.papaya.si.X.w(r0, r1, r2)
            r14.fireConnectionFailed(r11)
            goto L_0x00e7
        L_0x0114:
            r1 = 180000(0x2bf20, float:2.52234E-40)
            r5.setReadTimeout(r1)     // Catch:{ Exception -> 0x0108 }
            goto L_0x0085
        L_0x011c:
            com.papaya.si.bj r6 = new com.papaya.si.bj     // Catch:{ Exception -> 0x0108 }
            java.io.OutputStream r1 = r5.getOutputStream()     // Catch:{ Exception -> 0x0108 }
            r6.<init>(r1, r0)     // Catch:{ Exception -> 0x0108 }
            com.papaya.si.bv r0 = r14.jn     // Catch:{ Exception -> 0x0108 }
            java.util.HashMap<java.lang.String, com.papaya.si.aY<java.lang.Integer, java.lang.Object>> r0 = r0.mK     // Catch:{ Exception -> 0x0108 }
            java.util.Set r0 = r0.entrySet()     // Catch:{ Exception -> 0x0108 }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ Exception -> 0x0108 }
        L_0x0131:
            boolean r0 = r7.hasNext()     // Catch:{ Exception -> 0x0108 }
            if (r0 == 0) goto L_0x01ac
            java.lang.Object r0 = r7.next()     // Catch:{ Exception -> 0x0108 }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ Exception -> 0x0108 }
            java.lang.Object r1 = r0.getKey()     // Catch:{ Exception -> 0x0108 }
            if (r1 == 0) goto L_0x0131
            java.lang.Object r1 = r0.getValue()     // Catch:{ Exception -> 0x0108 }
            com.papaya.si.aY r1 = (com.papaya.si.aY) r1     // Catch:{ Exception -> 0x0108 }
            A r2 = r1.first     // Catch:{ Exception -> 0x0108 }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Exception -> 0x0108 }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x0108 }
            if (r2 != 0) goto L_0x0161
            java.lang.Object r0 = r0.getKey()     // Catch:{ Exception -> 0x0108 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0108 }
            B r1 = r1.second     // Catch:{ Exception -> 0x0108 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0108 }
            r6.writeField(r0, r1)     // Catch:{ Exception -> 0x0108 }
            goto L_0x0131
        L_0x0161:
            A r2 = r1.first     // Catch:{ Exception -> 0x0108 }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Exception -> 0x0108 }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x0108 }
            if (r2 != r12) goto L_0x0189
            java.lang.Object r2 = r0.getKey()     // Catch:{ Exception -> 0x0108 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x0108 }
            java.lang.String r8 = "binary"
            java.lang.Object r0 = r0.getKey()     // Catch:{ Exception -> 0x0108 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0108 }
            B r1 = r1.second     // Catch:{ Exception -> 0x0108 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0108 }
            com.papaya.si.aK r1 = com.papaya.si.aK.fromPriaContentUrl(r1)     // Catch:{ Exception -> 0x0108 }
            java.io.InputStream r1 = r1.openInput()     // Catch:{ Exception -> 0x0108 }
            r6.writeFile(r2, r8, r0, r1)     // Catch:{ Exception -> 0x0108 }
            goto L_0x0131
        L_0x0189:
            A r2 = r1.first     // Catch:{ Exception -> 0x0108 }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Exception -> 0x0108 }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x0108 }
            r8 = 2
            if (r2 != r8) goto L_0x0131
            java.lang.Object r2 = r0.getKey()     // Catch:{ Exception -> 0x0108 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x0108 }
            java.lang.String r8 = "binary"
            java.lang.Object r0 = r0.getKey()     // Catch:{ Exception -> 0x0108 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0108 }
            B r1 = r1.second     // Catch:{ Exception -> 0x0108 }
            byte[] r1 = (byte[]) r1     // Catch:{ Exception -> 0x0108 }
            byte[] r1 = (byte[]) r1     // Catch:{ Exception -> 0x0108 }
            r6.writeFile(r2, r8, r0, r1)     // Catch:{ Exception -> 0x0108 }
            goto L_0x0131
        L_0x01ac:
            r6.close()     // Catch:{ Exception -> 0x0108 }
            goto L_0x008e
        L_0x01b1:
            int r2 = r5.getContentLength()     // Catch:{ Exception -> 0x0299, all -> 0x0285 }
            goto L_0x00ac
        L_0x01b7:
            boolean r2 = r14.mw     // Catch:{ Exception -> 0x00c4, all -> 0x028b }
            if (r2 != 0) goto L_0x02a1
            r2 = r12
        L_0x01bc:
            com.papaya.si.aU.close(r0)     // Catch:{ Exception -> 0x0108 }
            com.papaya.si.aU.close(r1)     // Catch:{ Exception -> 0x0108 }
            r0 = r1
            r1 = r2
            goto L_0x00db
        L_0x01c6:
            r0 = move-exception
            r1 = r9
            r2 = r9
        L_0x01c9:
            com.papaya.si.aU.close(r2)     // Catch:{ Exception -> 0x0108 }
            com.papaya.si.aU.close(r1)     // Catch:{ Exception -> 0x0108 }
            throw r0     // Catch:{ Exception -> 0x0108 }
        L_0x01d0:
            r0 = -1
            r14.setDataLength(r0)     // Catch:{ Exception -> 0x0108 }
            r0 = -2
            r14.fireConnectionFailed(r0)     // Catch:{ Exception -> 0x0108 }
            goto L_0x00e7
        L_0x01db:
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0108 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0108 }
            r2.<init>()     // Catch:{ Exception -> 0x0108 }
            java.lang.String r4 = r0.getAbsolutePath()     // Catch:{ Exception -> 0x0108 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0108 }
            java.lang.String r4 = ".tmp"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0108 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0108 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0108 }
            java.io.InputStream r2 = com.papaya.si.C0034bf.getInputStream(r5)     // Catch:{ Exception -> 0x027c, all -> 0x0252 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0280, all -> 0x026f }
            r4.<init>(r1)     // Catch:{ Exception -> 0x0280, all -> 0x026f }
        L_0x0200:
            boolean r5 = r14.mw     // Catch:{ Exception -> 0x0215, all -> 0x0273 }
            if (r5 != 0) goto L_0x023c
            int r5 = r2.read(r3)     // Catch:{ Exception -> 0x0215, all -> 0x0273 }
            if (r5 == r11) goto L_0x023c
            long r6 = r14.mz     // Catch:{ Exception -> 0x0215, all -> 0x0273 }
            long r8 = (long) r5     // Catch:{ Exception -> 0x0215, all -> 0x0273 }
            long r6 = r6 + r8
            r14.mz = r6     // Catch:{ Exception -> 0x0215, all -> 0x0273 }
            r6 = 0
            r4.write(r3, r6, r5)     // Catch:{ Exception -> 0x0215, all -> 0x0273 }
            goto L_0x0200
        L_0x0215:
            r5 = move-exception
            r13 = r5
            r5 = r2
            r2 = r13
        L_0x0219:
            java.lang.String r6 = "Failed to save to file %s"
            r7 = 1
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x0278 }
            r8 = 0
            r7[r8] = r0     // Catch:{ all -> 0x0278 }
            com.papaya.si.X.e(r2, r6, r7)     // Catch:{ all -> 0x0278 }
            com.papaya.si.aU.close(r5)     // Catch:{ Exception -> 0x0108 }
            com.papaya.si.aU.close(r4)     // Catch:{ Exception -> 0x0108 }
            com.papaya.si.aU.deleteFile(r1)     // Catch:{ Exception -> 0x0108 }
            r1 = r10
        L_0x022e:
            if (r1 == 0) goto L_0x025f
            long r0 = r0.length()     // Catch:{ Exception -> 0x0108 }
            r14.setDataLength(r0)     // Catch:{ Exception -> 0x0108 }
            r14.fireConnectionFinished()     // Catch:{ Exception -> 0x0108 }
            goto L_0x00e7
        L_0x023c:
            boolean r5 = r14.mw     // Catch:{ Exception -> 0x0215, all -> 0x0273 }
            if (r5 != 0) goto L_0x029f
            com.papaya.si.aU.deleteFile(r0)     // Catch:{ Exception -> 0x0215, all -> 0x0273 }
            boolean r5 = r1.renameTo(r0)     // Catch:{ Exception -> 0x0215, all -> 0x0273 }
        L_0x0247:
            com.papaya.si.aU.close(r2)     // Catch:{ Exception -> 0x0108 }
            com.papaya.si.aU.close(r4)     // Catch:{ Exception -> 0x0108 }
            com.papaya.si.aU.deleteFile(r1)     // Catch:{ Exception -> 0x0108 }
            r1 = r5
            goto L_0x022e
        L_0x0252:
            r0 = move-exception
            r2 = r9
            r4 = r9
        L_0x0255:
            com.papaya.si.aU.close(r4)     // Catch:{ Exception -> 0x0108 }
            com.papaya.si.aU.close(r2)     // Catch:{ Exception -> 0x0108 }
            com.papaya.si.aU.deleteFile(r1)     // Catch:{ Exception -> 0x0108 }
            throw r0     // Catch:{ Exception -> 0x0108 }
        L_0x025f:
            r0 = -1
            r14.setDataLength(r0)     // Catch:{ Exception -> 0x0108 }
            r0 = -2
            r14.fireConnectionFailed(r0)     // Catch:{ Exception -> 0x0108 }
            goto L_0x00e7
        L_0x026a:
            r14.fireConnectionFailed(r0)     // Catch:{ Exception -> 0x0108 }
            goto L_0x00e7
        L_0x026f:
            r0 = move-exception
            r4 = r2
            r2 = r9
            goto L_0x0255
        L_0x0273:
            r0 = move-exception
            r13 = r4
            r4 = r2
            r2 = r13
            goto L_0x0255
        L_0x0278:
            r0 = move-exception
            r2 = r4
            r4 = r5
            goto L_0x0255
        L_0x027c:
            r2 = move-exception
            r4 = r9
            r5 = r9
            goto L_0x0219
        L_0x0280:
            r4 = move-exception
            r5 = r2
            r2 = r4
            r4 = r9
            goto L_0x0219
        L_0x0285:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r9
            goto L_0x01c9
        L_0x028b:
            r2 = move-exception
            r13 = r2
            r2 = r0
            r0 = r13
            goto L_0x01c9
        L_0x0291:
            r0 = move-exception
            goto L_0x01c9
        L_0x0294:
            r0 = move-exception
            r1 = r9
            r2 = r9
            goto L_0x00c8
        L_0x0299:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r9
            goto L_0x00c8
        L_0x029f:
            r5 = r10
            goto L_0x0247
        L_0x02a1:
            r2 = r10
            goto L_0x01bc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.papaya.si.bs.run():void");
    }
}
