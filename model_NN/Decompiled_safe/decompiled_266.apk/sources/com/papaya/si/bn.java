package com.papaya.si;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import com.papaya.si.bt;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;

public final class bn implements bt.a {
    private WeakReference<a> hp;
    /* access modifiers changed from: private */
    public ArrayList<bv> kp = new ArrayList<>(4);
    /* access modifiers changed from: private */
    public Context lf;
    /* access modifiers changed from: private */
    public ArrayList<URL> mp = new ArrayList<>(4);
    /* access modifiers changed from: private */
    public ArrayList<String> mq = new ArrayList<>(4);

    public interface a {
        void onPhotoSaved(URL url, URL url2, String str, boolean z);
    }

    public bn(Context context) {
        this.lf = context;
    }

    public final void connectionFailed(final bt btVar, int i) {
        C0030bb.runInHandlerThread(new Runnable() {
            public final void run() {
                synchronized (this) {
                    bv request = btVar.getRequest();
                    int indexOf = bn.this.kp.indexOf(request);
                    if (indexOf > 0) {
                        a delegate = bn.this.getDelegate();
                        if (delegate != null) {
                            delegate.onPhotoSaved(request.getUrl(), (URL) bn.this.mp.get(indexOf), (String) bn.this.mq.get(indexOf), false);
                        }
                        bn.this.kp.remove(indexOf);
                        bn.this.mp.remove(indexOf);
                        bn.this.mq.remove(indexOf);
                    }
                }
            }
        });
    }

    public final void connectionFinished(final bt btVar) {
        C0030bb.runInHandlerThread(new Runnable() {
            public final void run() {
                byte[] data = btVar.getData();
                MediaStore.Images.Media.insertImage(bn.this.lf.getContentResolver(), BitmapFactory.decodeByteArray(data, 0, data.length), "Title", "papaya");
                synchronized (this) {
                    bv request = btVar.getRequest();
                    int indexOf = bn.this.kp.indexOf(request);
                    if (indexOf > 0) {
                        a delegate = bn.this.getDelegate();
                        if (delegate != null) {
                            delegate.onPhotoSaved(request.getUrl(), (URL) bn.this.mp.get(indexOf), (String) bn.this.mq.get(indexOf), true);
                        }
                        bn.this.kp.remove(indexOf);
                        bn.this.mp.remove(indexOf);
                        bn.this.mq.remove(indexOf);
                    }
                }
            }
        });
    }

    public final a getDelegate() {
        if (this.hp == null) {
            return null;
        }
        return this.hp.get();
    }

    public final int saveToPictures(String str, URL url, String str2) {
        if (str == null || str.length() == 0) {
            return -1;
        }
        bv bvVar = new bv();
        bvVar.setDelegate(this);
        C0026ay webCache = C0001a.getWebCache();
        aK fdFromPapayaUri = webCache.fdFromPapayaUri(str, url, bvVar);
        if (fdFromPapayaUri != null) {
            MediaStore.Images.Media.insertImage(this.lf.getContentResolver(), C0030bb.bitmapFromFD(fdFromPapayaUri), "Title", "papaya");
            return 1;
        } else if (bvVar.getUrl() == null) {
            return -1;
        } else {
            synchronized (this) {
                this.kp.add(bvVar);
                this.mp.add(url);
                this.mq.add(str2);
            }
            webCache.insertRequest(bvVar);
            return 0;
        }
    }

    public final void setDelegate(a aVar) {
        if (aVar == null) {
            this.hp = null;
        } else {
            this.hp = new WeakReference<>(aVar);
        }
    }
}
