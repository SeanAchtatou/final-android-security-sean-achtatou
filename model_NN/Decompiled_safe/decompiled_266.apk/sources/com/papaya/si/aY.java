package com.papaya.si;

public final class aY<A, B> {
    public A first;
    public B second;

    public aY() {
    }

    public aY(A a, B b) {
        this.first = a;
        this.second = b;
    }

    public static <F, S> aY<F, S> make(F f, S s) {
        return new aY<>(f, s);
    }

    public final String toString() {
        return "Pair{a=" + ((Object) this.first) + ", b=" + ((Object) this.second) + '}';
    }
}
