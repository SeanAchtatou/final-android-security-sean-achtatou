package com.papaya.si;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import com.papaya.si.bt;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;

public final class bo implements aW, bt.a {
    private WeakReference<a> es;
    private ArrayList<bv> kp = new ArrayList<>(4);
    private Context lf;
    private ArrayList<URL> mp = new ArrayList<>(4);
    private ArrayList<String> mq = new ArrayList<>(4);

    public interface a {
        void onPhotoPicasa(URL url, URL url2, String str, boolean z);
    }

    public bo(Context context) {
        this.lf = context;
    }

    public final synchronized void connectionFailed(bt btVar, int i) {
        bv request = btVar.getRequest();
        int indexOf = this.kp.indexOf(request);
        if (indexOf > 0) {
            a delegate = getDelegate();
            if (delegate != null) {
                delegate.onPhotoPicasa(request.getUrl(), this.mp.get(indexOf), this.mq.get(indexOf), false);
            }
            this.kp.remove(indexOf);
            this.mp.remove(indexOf);
            this.mq.remove(indexOf);
        }
    }

    public final synchronized void connectionFinished(bt btVar) {
        byte[] data = btVar.getData();
        uploadtoPicasa(Uri.parse(MediaStore.Images.Media.insertImage(this.lf.getContentResolver(), BitmapFactory.decodeByteArray(data, 0, data.length), "Title", "papaya")));
        synchronized (this) {
            bv request = btVar.getRequest();
            int indexOf = this.kp.indexOf(request);
            if (indexOf > 0) {
                a delegate = getDelegate();
                if (delegate != null) {
                    delegate.onPhotoPicasa(request.getUrl(), this.mp.get(indexOf), this.mq.get(indexOf), true);
                }
                this.kp.remove(indexOf);
                this.mp.remove(indexOf);
                this.mq.remove(indexOf);
            }
        }
    }

    public final a getDelegate() {
        if (this.es == null) {
            return null;
        }
        return this.es.get();
    }

    public final void setDelegate(a aVar) {
        if (aVar == null) {
            this.es = null;
        } else {
            this.es = new WeakReference<>(aVar);
        }
    }

    public final int uploadToPicasa(String str, URL url, String str2) {
        if (str == null || str.length() == 0) {
            return -1;
        }
        if (!C0029ba.supportPicasa()) {
            C0030bb.showToast("Google Picasa is not supported on your device.", 0);
            return -1;
        }
        bv bvVar = new bv();
        bvVar.setDelegate(this);
        C0026ay webCache = C0001a.getWebCache();
        aK fdFromPapayaUri = webCache.fdFromPapayaUri(str, url, bvVar);
        if (fdFromPapayaUri != null) {
            uploadtoPicasa(Uri.parse(MediaStore.Images.Media.insertImage(this.lf.getContentResolver(), C0030bb.bitmapFromFD(fdFromPapayaUri), "Title", "papaya")));
            return 1;
        } else if (bvVar.getUrl() == null) {
            return -1;
        } else {
            synchronized (this) {
                this.kp.add(bvVar);
                this.mp.add(url);
                this.mq.add(str2);
            }
            webCache.insertRequest(bvVar);
            return 0;
        }
    }

    public final void uploadtoPicasa(Uri uri) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("image/jpeg");
        intent.addFlags(1);
        intent.putExtra("android.intent.extra.STREAM", uri);
        intent.setComponent(new ComponentName("com.google.android.apps.uploader", "com.google.android.apps.uploader.picasa.PicasaSettingsActivity"));
        C0028b.startActivity(intent);
    }
}
