package com.papaya.si;

import java.util.HashMap;
import java.util.Vector;

public final class C extends aG implements aW, C0054t {
    private HashMap<String, String> cm = new HashMap<>();

    public C() {
        C0037c.A.registerCmds(this, 322);
    }

    public final String getLabel(String str, String str2) {
        String str3 = this.cm.get(str);
        return str3 == null ? str2 : str3;
    }

    public final void handleServerResponse(Vector<Object> vector) {
        int sgetInt = aV.sgetInt(vector, 0);
        if (sgetInt == 322) {
            int i = 1;
            while (true) {
                int i2 = i;
                if (i2 < vector.size()) {
                    this.cm.put((String) vector.get(i2), (String) vector.get(i2 + 1));
                    i = i2 + 2;
                } else {
                    fireDataStateChanged();
                    return;
                }
            }
        } else {
            X.e("unknown cmd for TabBadgeValues: " + sgetInt, new Object[0]);
        }
    }

    public final void setLabel(String str, String str2) {
        this.cm.put(str, str2);
    }
}
