package com.papaya.si;

import android.graphics.drawable.Drawable;

public final class N extends D {
    public int cW;
    public String name;

    public final Drawable getDefaultDrawable() {
        return C0037c.getBitmapDrawable("avatar_unknown");
    }

    public final String getTimeLabel() {
        return null;
    }

    public final String getTitle() {
        return this.name;
    }

    public final boolean isGrayScaled() {
        return this.state == 0;
    }
}
