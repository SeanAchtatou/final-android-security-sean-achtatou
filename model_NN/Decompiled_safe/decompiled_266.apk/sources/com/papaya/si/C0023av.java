package com.papaya.si;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ProviderInfo;
import android.net.Uri;
import android.view.View;
import android.widget.Toast;
import com.adknowledge.superrewards.model.SROffer;
import com.papaya.achievement.PPYAchievementDelegate;
import com.papaya.purchase.PPYPayment;
import com.papaya.social.PPYSession;
import com.papaya.social.PPYSocial;
import com.papaya.social.PPYSocialChallengeRecord;
import com.papaya.social.PPYSocialQuery;
import com.papaya.social.SocialRegistrationActivity;
import com.papaya.view.LazyImageView;
import com.papaya.view.OverlayMessage;
import com.papaya.view.OverlayTitleMessageView;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import org.codehaus.jackson.Base64Variant;
import org.json.JSONArray;

/* renamed from: com.papaya.si.av  reason: case insensitive filesystem */
public final class C0023av {
    private static final HashMap<Integer, a> fj;
    private static C0023av fk = new C0023av();
    private ArrayList<PPYSocial.Delegate> fh = new ArrayList<>(4);
    private String fl;
    private C0024aw fm;
    private C0013al fn;
    private C0020as fo;
    private HashMap<String, C0024aw> fp = new HashMap<>(4);
    private boolean fq = true;
    private PPYSocial.Config fr;

    /* renamed from: com.papaya.si.av$a */
    static class a {
        public final String fA;
        public boolean fB;
        public final String fz;

        /* synthetic */ a(String str) {
            this(str, (byte) 0);
        }

        private a(String str, byte b) {
            this.fB = true;
            this.fz = str;
            this.fA = null;
            this.fB = false;
        }

        /* synthetic */ a(String str, String str2) {
            this(str, str2, (byte) 0);
        }

        private a(String str, String str2, byte b) {
            this.fB = true;
            this.fz = str;
            this.fA = str2;
        }
    }

    static {
        HashMap<Integer, a> hashMap = new HashMap<>(10);
        fj = hashMap;
        hashMap.put(0, new a("static_home", "home"));
        fj.put(1, new a("static_friends", "home"));
        fj.put(2, new a("static_photos", (String) null));
        fj.put(3, new a("static_mail", (String) null));
        fj.put(4, new a("static_newavatarnavi", (String) null));
        fj.put(5, new a("static_leaderboard", (String) null));
        fj.put(6, new a("static_localleaderboard"));
        fj.put(7, new a("static_invite", (String) null));
        fj.put(8, new a("static_achievement", (String) null));
        fj.put(9, new a("static_mycircles", "circle"));
        fj.put(10, new a("static_mylocation", "location"));
        fj.put(11, new a("static_challenge_list", (String) null));
        fj.put(12, new a("static_moreapps", (String) null));
    }

    private C0023av() {
    }

    private String computeScoreSignature(String str, int i) {
        return aU.md5(aV.format("%s_%s_%d", "papaya social 1.7", aV.nullAsEmpty(str), Integer.valueOf(i)));
    }

    private PPYSocial.Config copyConfig(PPYSocial.Config config) {
        final int i = 3;
        final String apiKey = config.getApiKey();
        final String preferredLanguage = config.getPreferredLanguage();
        final String androidMapsAPIKey = config.getAndroidMapsAPIKey();
        if (config.timeToShowRegistration() <= 3) {
            i = config.timeToShowRegistration();
        }
        final int billingChannels = config.getBillingChannels();
        final boolean isLeaderboardVisible = config.isLeaderboardVisible();
        final Class rClass = config.getRClass();
        return new PPYSocial.Config() {
            public final String getAndroidMapsAPIKey() {
                return androidMapsAPIKey;
            }

            public final String getApiKey() {
                return apiKey;
            }

            public final int getBillingChannels() {
                return billingChannels;
            }

            public final String getPreferredLanguage() {
                return preferredLanguage;
            }

            public final Class getRClass() {
                return rClass;
            }

            public final boolean isLeaderboardVisible() {
                return isLeaderboardVisible;
            }

            public final int timeToShowRegistration() {
                return i;
            }
        };
    }

    private void fireAccountChanged(int i, int i2) {
        ArrayList arrayList = new ArrayList(this.fh);
        for (int i3 = 0; i3 < arrayList.size(); i3++) {
            try {
                ((PPYSocial.Delegate) arrayList.get(i3)).onAccountChanged(i, i2);
            } catch (Exception e) {
                X.w(e, "Failed to invoke onAccountChanged", new Object[0]);
            }
        }
        arrayList.clear();
    }

    private void fireScoreUpdated() {
        ArrayList arrayList = new ArrayList(this.fh);
        for (int i = 0; i < arrayList.size(); i++) {
            try {
                ((PPYSocial.Delegate) arrayList.get(i)).onScoreUpdated();
            } catch (Exception e) {
                X.w(e, "Failed to invoke onScoreUpdated", new Object[0]);
            }
        }
        arrayList.clear();
    }

    private void fireSessionUpdated() {
        ArrayList arrayList = new ArrayList(this.fh);
        for (int i = 0; i < arrayList.size(); i++) {
            try {
                ((PPYSocial.Delegate) arrayList.get(i)).onSessionUpdated();
            } catch (Exception e) {
                X.w(e, "Failed to invoke onSessionUpdated", new Object[0]);
            }
        }
        arrayList.clear();
    }

    public static C0023av getInstance() {
        return fk;
    }

    public final synchronized void addDelegate(PPYSocial.Delegate delegate) {
        if (!this.fh.contains(delegate)) {
            this.fh.add(delegate);
        }
    }

    public final void addPayment(Context context, PPYPayment pPYPayment) {
        if (isForceShowWelcome()) {
            showWelcome(context);
        } else {
            new aB(context, pPYPayment).show();
        }
    }

    public final OverlayMessage createOverlayMessage(String str, String str2, View.OnClickListener onClickListener) {
        if (C0037c.getApplicationContext() == null) {
            return null;
        }
        Application applicationContext = C0037c.getApplicationContext();
        OverlayTitleMessageView overlayTitleMessageView = new OverlayTitleMessageView(applicationContext, new LazyImageView(applicationContext));
        ((LazyImageView) overlayTitleMessageView.getImageView()).setImageUrl(str);
        overlayTitleMessageView.getTextView().setText(str2);
        OverlayMessage overlayMessage = new OverlayMessage(applicationContext, overlayTitleMessageView);
        overlayMessage.setOnClickListener(onClickListener);
        return overlayMessage;
    }

    public final void dispose() {
        C0037c.quit();
    }

    public final C0020as getAchievementDatabase() {
        return this.fo;
    }

    public final void getAchievementList(PPYAchievementDelegate pPYAchievementDelegate) {
        new C0027az(pPYAchievementDelegate).getAchievementList();
    }

    public final String getApiKey() {
        return this.fl;
    }

    public final String getAvatarUrlString(int i) {
        return C0056v.bi + "getavatarhead?uid=" + i;
    }

    public final C0024aw getScoreDatabase() {
        return this.fm;
    }

    public final C0024aw getScoreDatabase(String str) {
        if (aV.isEmpty(str)) {
            return this.fm;
        }
        C0024aw awVar = this.fp.get(str);
        if (awVar != null) {
            return awVar;
        }
        C0024aw awVar2 = new C0024aw("com.papaya.social.score" + aU.md5(str));
        this.fp.put(str, awVar2);
        return awVar2;
    }

    public final PPYSocial.Config getSocialConfig() {
        return this.fr;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void handleResponse(JSONArray jSONArray) {
        if (jSONArray != null) {
            C0025ax instance = C0025ax.getInstance();
            try {
                switch (jSONArray.optInt(0)) {
                    case -3:
                        File file = new File(C0037c.getApplicationContext().getFilesDir(), "__ppy_secret");
                        String optString = jSONArray.optString(1, null);
                        if (aV.isEmpty(optString)) {
                            aU.deleteFile(file);
                            return;
                        } else {
                            aU.writeBytesToFile(file, aV.getBytes(optString));
                            return;
                        }
                    case Base64Variant.BASE64_VALUE_PADDING /*-2*/:
                        this.fq = false;
                        return;
                    case -1:
                        C0016ao.getInstance().updatePages(jSONArray);
                        return;
                    case 0:
                        int uid = instance.getUID();
                        instance.setUID(jSONArray.optInt(1));
                        instance.setSessionKey(jSONArray.optString(2));
                        instance.setExpirationDate((System.currentTimeMillis() / 1000) + ((long) jSONArray.optInt(3)));
                        instance.setNickname(jSONArray.optString(4));
                        instance.setAppID(jSONArray.optInt(6));
                        instance.setDev(jSONArray.optInt(7, 0) > 0);
                        instance.fJ = jSONArray.optString(8, null);
                        instance.setSessionSecret(jSONArray.optString(9, null));
                        instance.setSessionReceipt(jSONArray.optString(10, null));
                        instance.save();
                        if (instance.getUID() != uid) {
                            fireAccountChanged(uid, instance.getUID());
                        }
                        C0037c.getSession().fireDataStateChanged();
                        fireSessionUpdated();
                        return;
                    case 1:
                        C0037c.getSession().reupdateFriendList(jSONArray);
                        return;
                    case 2:
                        return;
                    case 3:
                        instance.setNickname(jSONArray.optString(1, instance.getNickname()));
                        fireSessionUpdated();
                        return;
                    case 4:
                        if (instance.isConnected()) {
                            JSONArray optJSONArray = jSONArray.optJSONArray(1);
                            for (int i = 0; i < optJSONArray.length(); i += 2) {
                                instance.setScore(optJSONArray.optString(i, null), optJSONArray.optInt(i + 1, 0));
                            }
                            fireScoreUpdated();
                            return;
                        }
                        return;
                    case 5:
                        OverlayMessage.showTitleMessage(jSONArray.optString(1), jSONArray.optString(2), jSONArray.optString(3), jSONArray.optString(4), jSONArray.optInt(5, OverlayMessage.DEFAULT_TIMEOUT), C0030bb.toGravity(jSONArray.optString(6, "bottom"), 80), jSONArray.optInt(7, 100));
                        return;
                    case 6:
                        OverlayMessage.showMessage(jSONArray.optString(1), jSONArray.optString(2), jSONArray.optString(3), jSONArray.optInt(4, OverlayMessage.DEFAULT_TIMEOUT), C0030bb.toGravity(jSONArray.optString(5, "bottom"), 80), jSONArray.optInt(6, 100));
                        return;
                    case 71:
                        int optInt = jSONArray.optInt(1, 0);
                        String[] split = jSONArray.optString(2).split("-");
                        aV.intValue(split[0]);
                        int intValue = aV.intValue(split[1]);
                        if (optInt == 1) {
                            bp.onImageUploaded(intValue, jSONArray.optInt(3));
                            return;
                        } else {
                            bp.onImageUploadFailed(intValue);
                            return;
                        }
                    default:
                        return;
                }
            } catch (Exception e) {
                X.e("Failed to process payload %s: %s", jSONArray, e);
            }
            X.e("Failed to process payload %s: %s", jSONArray, e);
        }
    }

    public final void initialize(Context context, PPYSocial.Config config) {
        boolean z;
        try {
            if (C0037c.getApplicationContext() != context.getApplicationContext()) {
                Context applicationContext = context.getApplicationContext();
                this.fr = copyConfig(config);
                C0037c.initialize(applicationContext);
                C0043i.trackPageView("/sdk_init");
                C0043i.trackEvent("sdk", "init", applicationContext.getPackageName(), 0);
                applicationContext.getResources().getDisplayMetrics();
                try {
                    ProviderInfo[] providerInfoArr = applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 8).providers;
                    if (providerInfoArr != null) {
                        int length = providerInfoArr.length;
                        int i = 0;
                        while (true) {
                            if (i < length) {
                                ProviderInfo providerInfo = providerInfoArr[i];
                                if (providerInfo != null && "com.papaya.social.PPYSocialContentProvider".equals(providerInfo.name)) {
                                    bz.mW = "content://" + providerInfo.authority + "/cache/";
                                    z = true;
                                    break;
                                }
                                i++;
                            } else {
                                break;
                            }
                        }
                    }
                    z = false;
                } catch (Exception e) {
                    X.e("Failed to read the configuration of PPYSocialContentProvider: %s", e);
                    z = false;
                }
                if (!z) {
                    X.e("Failed to find the configuration of PPYSocialContentProvider", new Object[0]);
                }
                this.fl = this.fr.getApiKey();
                C0025ax.getInstance().setApiKey(this.fl);
                this.fm = new C0024aw("com.papaya.social.score");
                this.fn = C0014am.getInstance().settingDB();
                this.fo = new C0020as("com.papaya.social.achievement");
                C0025ax.getInstance().setDev(false);
                if (this.fl.startsWith("dev-")) {
                    C0025ax.getInstance().setDev(true);
                    C0030bb.showToast("Enabled dev mode of Papaya Social SDK", 1);
                }
                int kvInt = this.fn.kvInt("init_times", 0);
                this.fn.kvSaveInt("init_times", kvInt + 1, -1);
                if (kvInt < this.fr.timeToShowRegistration()) {
                    C0001a.getWebCache().tryLogin();
                } else if (this.fn.kvInt("welcome_page", 0) > 0 || aE.hasCredentialFile()) {
                    C0001a.getWebCache().tryLogin();
                } else {
                    showWelcome(context);
                }
                X.i("Papaya Social SDK (%s/%s) is initialized. LBS support (%b)", "Global_Corona", "1.71", false);
            }
        } catch (Exception e2) {
            Toast.makeText(context, "Failed to initialize Papaya Social", 0).show();
            X.e(e2, "Error occurred while initializing Papaya Social SDK", new Object[0]);
        }
    }

    public final boolean isForceShowWelcome() {
        return !this.fq && !PPYSession.getInstance().isConnected() && !C0001a.getWebCache().isLoggingin();
    }

    public final boolean isInitialized() {
        return this.fl != null;
    }

    public final void loadAchievement(int i, PPYAchievementDelegate pPYAchievementDelegate) {
        new C0027az(pPYAchievementDelegate).loadAchievement(i);
    }

    public final void postNewsfeed(String str, String str2) {
        postNewsfeed(str, str2, 0);
    }

    public final void postNewsfeed(String str, String str2, int i) {
        if (!aV.isEmpty(str)) {
            StringBuilder append = new StringBuilder("json_postnewsfeed?message=").append(Uri.encode(str));
            if (str2 != null) {
                append.append("&uri=").append(Uri.encode(str2)).append("&type=").append(i);
            }
            bv bvVar = new bv(C0034bf.createURL(append.toString()), false);
            bvVar.setDispatchable(true);
            bvVar.start(true);
        }
    }

    public final void recommendMyApp(String str) {
        new aC(C0037c.getApplicationContext(), str).show();
    }

    public final synchronized void removeDelegate(PPYSocial.Delegate delegate) {
        this.fh.remove(delegate);
    }

    public final PPYSocialQuery sendChallenge(PPYSocialChallengeRecord pPYSocialChallengeRecord, PPYSocialQuery.QueryDelegate queryDelegate) {
        PPYSocialQuery pPYSocialQuery = new PPYSocialQuery("w_send_challenge", queryDelegate);
        pPYSocialQuery.put("cid", pPYSocialChallengeRecord.getChallengeDefinitionID()).put("rid", pPYSocialChallengeRecord.getReceiverUserID()).put("msg", pPYSocialChallengeRecord.getMessage());
        if (pPYSocialChallengeRecord.getPayloadType() == 0) {
            pPYSocialQuery.put("payload", pPYSocialChallengeRecord.getPayload()).put(SROffer.TYPE, pPYSocialChallengeRecord.getPayloadType());
        } else {
            pPYSocialQuery.put("payload", pPYSocialChallengeRecord.getPayloadBinary()).put(SROffer.TYPE, pPYSocialChallengeRecord.getPayloadType());
        }
        submitQuery(pPYSocialQuery);
        return pPYSocialQuery;
    }

    public final void setScore(int i, String str) {
        StringBuilder sb = new StringBuilder("json_updatescore?update=1&value=");
        sb.append(i);
        if (str != null) {
            sb.append("&name=").append(str);
        }
        sb.append("&sig=").append(Uri.encode(computeScoreSignature(str, i)));
        bv bvVar = new bv(C0034bf.createURL(sb.toString()), false);
        bvVar.setDispatchable(true);
        bvVar.start(true);
        getScoreDatabase(str).addScore(i);
        C0025ax.getInstance().setScore(str, i);
    }

    public final void show(Context context, int i) {
        if (i == 10) {
            showLBS(context);
            return;
        }
        a aVar = fj.get(Integer.valueOf(i));
        showWebActivity(context, aVar.fz, aVar.fA, aVar.fB);
    }

    public final void showChat(Context context) {
        if (isForceShowWelcome()) {
            showWelcome(context);
        } else {
            C0028b.openPRIALink(context, "friends://", "chat");
        }
    }

    public final void showHome(Context context, int i) {
        C0028b.openHome(C0030bb.contextAsActivity(context), i);
    }

    public final void showLBS(Context context) {
        if (isForceShowWelcome()) {
            showWelcome(context);
        } else {
            C0030bb.showToast("LBS is not enabled", 1);
        }
    }

    public final void showLeaderboard(Context context, String str, boolean z) {
        a aVar = fj.get(Integer.valueOf(z ? 5 : 6));
        String str2 = aVar.fz;
        if (str != null) {
            str2 = str2 + "?name=" + str;
        }
        showWebActivity(context, str2, aVar.fA, aVar.fB);
    }

    public final void showURL(Context context, String str) {
        try {
            if (!aV.isNotEmpty(str)) {
                return;
            }
            if (str.startsWith("chat://")) {
                showChat(context);
            } else if (str.startsWith("lbs://")) {
                showLBS(context);
            } else if (str.startsWith("login://")) {
                showWelcome(context);
            } else {
                showWebActivity(context, str, null, true);
            }
        } catch (Exception e) {
            X.w(e, "failed to showURL: " + str, new Object[0]);
        }
    }

    public final void showWebActivity(Context context, String str, String str2, boolean z) {
        try {
            if (isForceShowWelcome()) {
                showWelcome(context);
            } else {
                C0028b.openPRIALink(context, str, str2, z, null);
            }
        } catch (Exception e) {
            X.e(e, "Failed to show web activity", new Object[0]);
        }
    }

    public final void showWelcome(Context context) {
        C0028b.startActivity(context, new Intent(C0037c.getApplicationContext(), SocialRegistrationActivity.class));
    }

    public final aF submitQuery(PPYSocialQuery pPYSocialQuery) {
        aF aFVar = new aF(pPYSocialQuery);
        aFVar.start(false);
        return aFVar;
    }

    public final PPYSocialQuery updateChallengeStatus(int i, int i2, PPYSocialQuery.QueryDelegate queryDelegate) {
        PPYSocialQuery pPYSocialQuery = new PPYSocialQuery("w_update_challenge_status", queryDelegate);
        pPYSocialQuery.put("rid", i).put("status", i2);
        submitQuery(pPYSocialQuery);
        return pPYSocialQuery;
    }

    public final void updateScore(int i, String str) {
        StringBuilder sb = new StringBuilder("json_updatescore?update=0&value=");
        sb.append(i);
        if (str != null) {
            sb.append("&name=").append(str);
        }
        sb.append("&sig=").append(Uri.encode(computeScoreSignature(str, i)));
        bv bvVar = new bv(C0034bf.createURL(sb.toString()), false);
        bvVar.setDispatchable(true);
        bvVar.start(true);
        C0025ax instance = C0025ax.getInstance();
        instance.setScore(str, instance.getScore(str) + i);
    }
}
