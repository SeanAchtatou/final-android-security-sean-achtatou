package com.papaya.si;

import android.content.Context;
import android.net.Uri;
import com.papaya.si.bt;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import org.codehaus.jackson.util.BufferRecycler;
import org.json.JSONObject;

public class by extends B implements bt.a {
    private HashSet<String> mR = new HashSet<>((int) BufferRecycler.DEFAULT_WRITE_CONCAT_BUFFER_LEN);
    private LinkedList<bv> mS = new LinkedList<>();
    private ArrayList<bs> mT = new ArrayList<>(4);
    private ArrayList<bu> mU = new ArrayList<>(4);
    private ThreadPoolExecutor mV;

    public by(String str, Context context) {
        super(str, context);
    }

    private void collectAssets() {
        try {
            aU.linesFromStream(this.ck.getAssets().open("web-resources.lst"), this.mR);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isContentUrl(String str) {
        return str != null && (str.startsWith("file:///android_asset/") || str.startsWith(bz.mW));
    }

    public static String normalizeBundleUri(String str) {
        if (!str.contains("/")) {
            return "web-resources/" + str;
        }
        String[] split = str.split("/");
        StringBuilder acquireStringBuilder = aV.acquireStringBuilder(str.length());
        acquireStringBuilder.append("web-resources").append('/');
        int length = split.length;
        for (int i = 0; i < length; i++) {
            String str2 = split[i];
            if (!str2.equals("..") && !str2.equals(".")) {
                acquireStringBuilder.append(str2);
                if (i < length - 1) {
                    acquireStringBuilder.append('/');
                }
            }
        }
        return aV.releaseStringBuilder(acquireStringBuilder);
    }

    private void setup() {
        this.mV = (ThreadPoolExecutor) Executors.newFixedThreadPool(4);
    }

    private static String stripHost(String str) {
        return str.startsWith(C0056v.bf) ? str.substring(C0056v.bf.length()) : str;
    }

    public synchronized void appendRequest(bv bvVar) {
        try {
            this.mS.add(bvVar);
        } catch (Exception e) {
            X.e(e, "Failed to appendRequest: " + bvVar, new Object[0]);
        }
        return;
    }

    public synchronized void appendRequests(List<bv> list) {
        int i = 0;
        synchronized (this) {
            while (true) {
                try {
                    int i2 = i;
                    if (i2 >= list.size()) {
                        break;
                    }
                    bv bvVar = list.get(i2);
                    if (bvVar != null) {
                        this.mS.addLast(bvVar);
                    }
                    i = i2 + 1;
                } catch (Exception e) {
                    X.e(e, "Failed to appendRequests", new Object[0]);
                }
            }
        }
        return;
    }

    public String bundleContentUri(String str) {
        return bundleContentUri(str, true);
    }

    public String bundleContentUri(String str, boolean z) {
        String bundleFilename = bundleFilename(str, z);
        if (bundleFilename != null) {
            return "file:///android_asset/" + bundleFilename;
        }
        return null;
    }

    public aK bundleFD(String str) {
        String bundleFilename = bundleFilename(str, false);
        if (bundleFilename != null) {
            return new aK(bundleFilename);
        }
        return null;
    }

    public String bundleFilename(String str, boolean z) {
        String normalizeBundleUri = normalizeBundleUri(str);
        if (this.mR.contains(normalizeBundleUri)) {
            return normalizeBundleUri;
        }
        if (z) {
            X.w("not found in bundle %s", str);
        }
        return null;
    }

    public String bundleOrCacheContentUri(String str, String str2) {
        String bundleContentUri = bundleContentUri(str2, false);
        return bundleContentUri != null ? bundleContentUri : cacheContentUri(str);
    }

    public synchronized String cacheContentUri(String str) {
        String str2;
        try {
            File cachedFile = cachedFile(str, true);
            str2 = cachedFile != null ? bz.mW + cachedFile.getName() : null;
        } catch (Exception e) {
            e.printStackTrace();
            str2 = null;
        }
        return str2;
    }

    public String cacheOrBundleContentUri(String str, String str2) {
        String cacheContentUri = cacheContentUri(str);
        return cacheContentUri != null ? cacheContentUri : bundleContentUri(str2, false);
    }

    public aK cacheOrBundleFD(String str, String str2) {
        aK cachedFD = cachedFD(str);
        return cachedFD == null ? bundleFD(str2) : cachedFD;
    }

    public String cacheOrBundleFilename(String str, String str2) {
        File cachedFile = cachedFile(str, false);
        return (cachedFile == null || !cachedFile.exists()) ? bundleFilename(str2, false) : cachedFile.getName();
    }

    public synchronized aK cachedFD(String str) {
        aK aKVar;
        try {
            File cachedFile = cachedFile(str, true);
            aKVar = cachedFile != null ? new aK(cachedFile) : null;
        } catch (Exception e) {
            e.printStackTrace();
            aKVar = null;
        }
        return aKVar;
    }

    public synchronized File cachedFile(String str, boolean z) {
        File file;
        try {
            file = getCacheFile(str);
            if (z && !file.exists()) {
                file = null;
            }
        } catch (Exception e) {
            X.e(e, "Failed to invoke cachedFile", new Object[0]);
            file = null;
        }
        return file;
    }

    public synchronized void connectionFailed(bt btVar, int i) {
        try {
            X.w("connection failed %d, %s", Integer.valueOf(i), btVar);
            this.mT.remove(btVar);
            this.mU.remove(btVar);
        } catch (Exception e) {
            e.printStackTrace();
        }
        btVar.fireConnectionFailedToRequest(i);
        return;
    }

    public void connectionFinished(bt btVar) {
        String url;
        int indexOf;
        synchronized (this) {
            try {
                bv request = btVar.getRequest();
                if (btVar.getDataLength() <= 0) {
                    X.w("null data from %s", request.getUrl());
                } else if (request.isCacheable() && request.getSaveFile() == null) {
                    saveBytesWithKey(request.getUrl().toString(), btVar.getData());
                }
                if (!request.isCacheable()) {
                    try {
                        byte[] data = btVar.getData();
                        if (data != null && (indexOf = (url = request.getUrl().toString()).indexOf("__db_cache=")) >= 0) {
                            int indexOf2 = url.indexOf(38, indexOf);
                            if (indexOf2 == -1) {
                                indexOf2 = url.length();
                            }
                            JSONObject parseJsonObject = C0034bf.parseJsonObject(Uri.decode(url.substring("__db_cache=".length() + indexOf, indexOf2)));
                            if (parseJsonObject != null) {
                                String utf8String = aV.utf8String(data, null);
                                if (utf8String != null) {
                                    int optInt = parseJsonObject.optInt("life", 0);
                                    if (utf8String.contains("__life__")) {
                                        optInt = C0034bf.parseJsonObject(utf8String).optInt("life", 0);
                                    }
                                    C0013al findDatabase = C0014am.getInstance().findDatabase(parseJsonObject.optString("name", ""), parseJsonObject.optInt("scope", 0));
                                    if (findDatabase == null) {
                                        X.w("db is null %s", parseJsonObject);
                                    } else if (utf8String.contains("__redirect__")) {
                                        findDatabase.kvSave(parseJsonObject.optString("key"), null, 0);
                                    } else {
                                        findDatabase.kvSave(parseJsonObject.optString("key"), utf8String, optInt);
                                    }
                                } else {
                                    X.w("content is null", new Object[0]);
                                }
                            } else {
                                X.w("invalid db cache json", new Object[0]);
                            }
                        }
                    } catch (Exception e) {
                        X.w("failed to handle db cache: %s", e);
                    }
                }
                this.mT.remove(btVar);
                this.mU.remove(btVar);
            } catch (Exception e2) {
                X.w(e2, "error occurred in connectionFinished", new Object[0]);
                e2.printStackTrace();
                this.mT.remove(btVar);
                this.mU.remove(btVar);
            } catch (Throwable th) {
                this.mT.remove(btVar);
                this.mU.remove(btVar);
                throw th;
            }
        }
        try {
            btVar.fireConnectionFinishedToRequest();
            return;
        } catch (Exception e3) {
            X.w(e3, "Failed in fireConnectionFinishedToRequest", new Object[0]);
            return;
        }
    }

    public String contentUriFromPapayaUri(String str, URL url, bv bvVar) {
        String str2 = null;
        int indexOf = str.indexOf("://");
        if (indexOf == -1) {
            URL createURL = C0034bf.createURL(str, url);
            if (createURL == null) {
                X.w("malformed url %s, base %s", str, url);
            } else if (bvVar != null) {
                bvVar.setUrl(createURL);
                bvVar.setCacheable(false);
            }
        } else {
            String substring = str.substring(0, indexOf);
            String substring2 = str.substring(indexOf + 3);
            if ("papaya_cache_bundle".equals(substring)) {
                str2 = bundleContentUri(substring2, true);
                if (str2 == null) {
                    URL createURL2 = C0034bf.createURL(substring2, url);
                    if (createURL2 == null) {
                        X.w("malformed url %s, base %s", substring2, url);
                    } else if (bvVar != null) {
                        bvVar.setUrl(createURL2);
                        bvVar.setCacheable(false);
                    }
                }
            } else if ("papaya_cache_file".equals(substring)) {
                URL createURL3 = C0034bf.createURL(substring2, url);
                if (createURL3 != null) {
                    str2 = cacheOrBundleContentUri(createURL3.toString(), substring2);
                    if (str2 == null && bvVar != null) {
                        bvVar.setUrl(createURL3);
                        bvVar.setCacheable(true);
                    }
                } else {
                    X.w("malformed url %s, base %s", substring2, url);
                }
            } else {
                X.w("unsupported scheme %s, %s", substring, str);
            }
        }
        return str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder}
     arg types: [java.lang.String, int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(char[], int, int):java.lang.StringBuilder}
      ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder} */
    public String createLocalRefHtml(String str, URL url, boolean z, boolean z2) {
        if (!C0037c.isInitialized()) {
            return "";
        }
        if (str == null || str.length() == 0) {
            X.w("empty html content", new Object[0]);
            return "";
        }
        ArrayList arrayList = new ArrayList(8);
        StringBuilder acquireStringBuilder = aV.acquireStringBuilder(str.length());
        int indexOf = str.indexOf("\"papaya_cache");
        int i = 0;
        while (indexOf != -1) {
            acquireStringBuilder.append((CharSequence) str, i, indexOf);
            int indexOf2 = str.indexOf(34, indexOf + 1);
            if (indexOf2 != -1) {
                int i2 = indexOf2 + 1;
                String substring = str.substring(indexOf + 1, indexOf2);
                int indexOf3 = substring.indexOf("://");
                if (indexOf3 != -1) {
                    String substring2 = substring.substring(0, indexOf3);
                    String substring3 = substring.substring(indexOf3 + 3);
                    try {
                        URL createURL = C0034bf.createURL(substring3, url);
                        if (createURL != null) {
                            String url2 = createURL.toString();
                            if ("papaya_cache_file".equals(substring2)) {
                                String cacheOrBundleContentUri = cacheOrBundleContentUri(url2, substring3);
                                if (cacheOrBundleContentUri != null) {
                                    acquireStringBuilder.append('\"').append(cacheOrBundleContentUri).append('\"');
                                    i = i2;
                                } else {
                                    bv bvVar = new bv(createURL, true);
                                    bvVar.setRequireSid(z2);
                                    if (substring3.endsWith(".js")) {
                                        bvVar.setConnectionType(1);
                                    }
                                    arrayList.add(bvVar);
                                    bvVar.setSaveFile(new File(getCacheDir(), keyToStoreName(url2)));
                                    StringBuilder append = acquireStringBuilder.append('\"');
                                    if (z) {
                                        substring3 = url2;
                                    }
                                    append.append(substring3).append('\"');
                                    i = i2;
                                }
                            } else if ("papaya_cache_bundle".equals(substring2)) {
                                String bundleOrCacheContentUri = bundleOrCacheContentUri(url2, substring3);
                                if (bundleOrCacheContentUri != null) {
                                    acquireStringBuilder.append('\"').append(bundleOrCacheContentUri).append('\"');
                                    i = i2;
                                } else {
                                    StringBuilder append2 = acquireStringBuilder.append('\"');
                                    if (z) {
                                        substring3 = url2;
                                    }
                                    append2.append(substring3).append('\"');
                                    i = i2;
                                }
                            } else if ("papaya_cache_css".equals(substring2)) {
                                X.e("papaya_cache_css is not supported", new Object[0]);
                                StringBuilder append3 = acquireStringBuilder.append('\"');
                                if (z) {
                                    substring3 = url2;
                                }
                                append3.append(substring3).append('\"');
                                i = i2;
                            } else {
                                X.e("unknown papaya_cache scheme: " + substring2, new Object[0]);
                                i = i2;
                            }
                        } else {
                            X.e("uri is null: " + substring3, new Object[0]);
                            i = i2;
                        }
                    } catch (Exception e) {
                        X.e(e, "Failed to parse html", new Object[0]);
                        i = i2;
                    }
                } else {
                    i = i2;
                }
            } else {
                i = indexOf;
            }
            indexOf = str.indexOf("\"papaya_cache", indexOf + 1);
        }
        if (i < str.length()) {
            acquireStringBuilder.append((CharSequence) str, i, str.length());
        }
        if (!arrayList.isEmpty()) {
            insertRequests(arrayList);
        }
        return aV.releaseStringBuilder(acquireStringBuilder);
    }

    /* access modifiers changed from: protected */
    public void doClose() {
        for (int i = 0; i < this.mT.size(); i++) {
            bs bsVar = this.mT.get(i);
            try {
                bsVar.setDelegate(null);
            } catch (Exception e) {
            }
            try {
                bsVar.getRequest().setDelegate(null);
            } catch (Exception e2) {
            }
        }
        this.mT.clear();
        try {
            if (this.mV != null) {
                this.mV.shutdownNow();
            }
            this.mV = null;
        } catch (Exception e3) {
            X.w(e3, "failed to shutdown the httpservice", new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public boolean doInitCache() {
        collectAssets();
        setup();
        return true;
    }

    public boolean encapsuleHttpInTcp() {
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0029, code lost:
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0030, code lost:
        if (r1 >= r4.mU.size()) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0046, code lost:
        if (com.papaya.si.C0034bf.urlEquals(r4.mU.get(r1).getRequest().getUrl(), r5) == false) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0048, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004a, code lost:
        r1 = r1 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004e, code lost:
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0055, code lost:
        if (r1 >= r4.mS.size()) goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0067, code lost:
        if (com.papaya.si.C0034bf.urlEquals(r4.mS.get(r1).getUrl(), r5) == false) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0069, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x006b, code lost:
        r1 = r1 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006f, code lost:
        r0 = false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean existInQueue(java.net.URL r5) {
        /*
            r4 = this;
            r2 = 1
            r3 = 0
            monitor-enter(r4)
            r1 = r3
        L_0x0004:
            java.util.ArrayList<com.papaya.si.bs> r0 = r4.mT     // Catch:{ Exception -> 0x0071 }
            int r0 = r0.size()     // Catch:{ Exception -> 0x0071 }
            if (r1 >= r0) goto L_0x0029
            java.util.ArrayList<com.papaya.si.bs> r0 = r4.mT     // Catch:{ Exception -> 0x0071 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Exception -> 0x0071 }
            com.papaya.si.bs r0 = (com.papaya.si.bs) r0     // Catch:{ Exception -> 0x0071 }
            com.papaya.si.bv r0 = r0.getRequest()     // Catch:{ Exception -> 0x0071 }
            java.net.URL r0 = r0.getUrl()     // Catch:{ Exception -> 0x0071 }
            boolean r0 = com.papaya.si.C0034bf.urlEquals(r0, r5)     // Catch:{ Exception -> 0x0071 }
            if (r0 == 0) goto L_0x0025
            r0 = r2
        L_0x0023:
            monitor-exit(r4)
            return r0
        L_0x0025:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0004
        L_0x0029:
            r1 = r3
        L_0x002a:
            java.util.ArrayList<com.papaya.si.bu> r0 = r4.mU     // Catch:{ Exception -> 0x0071 }
            int r0 = r0.size()     // Catch:{ Exception -> 0x0071 }
            if (r1 >= r0) goto L_0x004e
            java.util.ArrayList<com.papaya.si.bu> r0 = r4.mU     // Catch:{ Exception -> 0x0071 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Exception -> 0x0071 }
            com.papaya.si.bu r0 = (com.papaya.si.bu) r0     // Catch:{ Exception -> 0x0071 }
            com.papaya.si.bv r0 = r0.getRequest()     // Catch:{ Exception -> 0x0071 }
            java.net.URL r0 = r0.getUrl()     // Catch:{ Exception -> 0x0071 }
            boolean r0 = com.papaya.si.C0034bf.urlEquals(r0, r5)     // Catch:{ Exception -> 0x0071 }
            if (r0 == 0) goto L_0x004a
            r0 = r2
            goto L_0x0023
        L_0x004a:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x002a
        L_0x004e:
            r1 = r3
        L_0x004f:
            java.util.LinkedList<com.papaya.si.bv> r0 = r4.mS     // Catch:{ Exception -> 0x0071 }
            int r0 = r0.size()     // Catch:{ Exception -> 0x0071 }
            if (r1 >= r0) goto L_0x006f
            java.util.LinkedList<com.papaya.si.bv> r0 = r4.mS     // Catch:{ Exception -> 0x0071 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Exception -> 0x0071 }
            com.papaya.si.bv r0 = (com.papaya.si.bv) r0     // Catch:{ Exception -> 0x0071 }
            java.net.URL r0 = r0.getUrl()     // Catch:{ Exception -> 0x0071 }
            boolean r0 = com.papaya.si.C0034bf.urlEquals(r0, r5)     // Catch:{ Exception -> 0x0071 }
            if (r0 == 0) goto L_0x006b
            r0 = r2
            goto L_0x0023
        L_0x006b:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x004f
        L_0x006f:
            r0 = r3
            goto L_0x0023
        L_0x0071:
            r0 = move-exception
            java.lang.String r1 = "Failed in existQueue"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x007c }
            com.papaya.si.X.e(r0, r1, r2)     // Catch:{ all -> 0x007c }
            r0 = r3
            goto L_0x0023
        L_0x007c:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.papaya.si.by.existInQueue(java.net.URL):boolean");
    }

    public aK fdFromContentUrl(String str) {
        if (str == null) {
            return null;
        }
        if (str.startsWith("file:///android_asset/")) {
            return new aK(str.substring("file:///android_asset/".length()));
        }
        if (str.startsWith(bz.mW)) {
            return new aK(new File(this.cj, str.substring(bz.mW.length())));
        }
        return null;
    }

    public aK fdFromPapayaUri(String str, URL url, bv bvVar) {
        int indexOf = str.indexOf("://");
        if (indexOf == -1) {
            URL createURL = C0034bf.createURL(str, url);
            if (createURL == null) {
                X.w("malformed url %s, base %s", str, url);
                return null;
            } else if (bvVar == null) {
                return null;
            } else {
                bvVar.setUrl(createURL);
                bvVar.setCacheable(false);
                return null;
            }
        } else {
            String substring = str.substring(0, indexOf);
            String substring2 = str.substring(indexOf + 3);
            if ("papaya_cache_bundle".equals(substring)) {
                aK bundleFD = bundleFD(substring2);
                if (bundleFD != null) {
                    return bundleFD;
                }
                URL createURL2 = C0034bf.createURL(substring2, url);
                if (createURL2 == null) {
                    X.w("malformed url %s, base %s", substring2, url);
                    return bundleFD;
                } else if (bvVar == null) {
                    return bundleFD;
                } else {
                    bvVar.setUrl(createURL2);
                    bvVar.setCacheable(false);
                    return bundleFD;
                }
            } else if ("papaya_cache_file".equals(substring)) {
                URL createURL3 = C0034bf.createURL(substring2, url);
                if (createURL3 != null) {
                    aK cacheOrBundleFD = cacheOrBundleFD(createURL3.toString(), substring2);
                    if (cacheOrBundleFD != null || bvVar == null) {
                        return cacheOrBundleFD;
                    }
                    bvVar.setUrl(createURL3);
                    bvVar.setCacheable(true);
                    return cacheOrBundleFD;
                }
                X.w("malformed url %s, base %s", substring2, url);
                return null;
            } else if ("http".equals(substring)) {
                if (bvVar == null) {
                    return null;
                }
                bvVar.setUrl(C0034bf.createURL(str, url));
                return null;
            } else if ("content".equals(substring)) {
                File file = new File(this.cj, str.substring(bz.mW.length()));
                if (file.exists()) {
                    return new aK(file);
                }
                X.w("cache file doesn't exist %s", file);
                return null;
            } else if (str.startsWith("file:///android_asset/")) {
                return new aK(str.substring("file:///android_asset/".length()));
            } else {
                X.w("unsupported scheme %s, %s", substring, str);
                return null;
            }
        }
    }

    public synchronized void insertRequest(bv bvVar) {
        try {
            this.mS.add(0, bvVar);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public synchronized void insertRequests(List<bv> list) {
        try {
            for (int size = list.size() - 1; size >= 0; size--) {
                bv bvVar = list.get(size);
                if (bvVar != null) {
                    this.mS.addFirst(bvVar);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    /* access modifiers changed from: protected */
    public String keyToStoreName(String str) {
        return aU.md5(stripHost(str));
    }

    public void poll() {
        bv bvVar;
        int i = 0;
        if (!this.cl) {
            synchronized (this) {
                try {
                    String sessionKey = C0025ax.getInstance().getSessionKey();
                    if (!this.mS.isEmpty()) {
                        while (this.mT.size() < 4 && !this.mS.isEmpty() && i != -1) {
                            int i2 = i;
                            while (true) {
                                if (i2 >= this.mS.size()) {
                                    bvVar = null;
                                    break;
                                }
                                bvVar = this.mS.get(i2);
                                if ((!encapsuleHttpInTcp() || bvVar.getConnectionType() == 0) && (sessionKey != null || !bvVar.isRequireSid())) {
                                    this.mS.remove(i2);
                                    i = i2;
                                } else {
                                    i2++;
                                }
                            }
                            this.mS.remove(i2);
                            i = i2;
                            if (bvVar == null) {
                                i = -1;
                            } else {
                                File cachedFile = bvVar.isCacheable() ? cachedFile(bvVar.getUrl().toString(), true) : null;
                                if (cachedFile == null) {
                                    bs bsVar = new bs(bvVar, this);
                                    this.mT.add(bsVar);
                                    this.mV.submit(bsVar);
                                } else if (bvVar.getDelegate() != null) {
                                    byte[] dataFromFile = aU.dataFromFile(cachedFile);
                                    bs bsVar2 = new bs(bvVar);
                                    bsVar2.setData(dataFromFile);
                                    bsVar2.fireConnectionFinishedToRequest();
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    X.w(e, "error occurred in web cache loop", new Object[0]);
                }
            }
        }
    }

    public String relativeUriFromPapayaUri(String str) {
        int indexOf = str.indexOf("://");
        return indexOf == -1 ? str : str.substring(indexOf + 3);
    }

    public boolean removeRequest(bv bvVar) {
        if (bvVar == null) {
            return false;
        }
        synchronized (this) {
            if (this.mS.contains(bvVar)) {
                this.mS.remove(bvVar);
                return true;
            }
            int i = 0;
            while (i < this.mT.size()) {
                try {
                    if (this.mT.get(i).getRequest() == bvVar) {
                        this.mT.remove(i);
                        return true;
                    }
                    i++;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }
            for (int i2 = 0; i2 < this.mU.size(); i2++) {
                if (this.mU.get(i2).getRequest() == bvVar) {
                    this.mU.remove(i2);
                    return true;
                }
            }
            return false;
        }
    }

    public synchronized void saveCacheWebFile(String str, byte[] bArr) {
        try {
            aU.writeBytesToFile(cachedFile(str, false), bArr);
        } catch (Exception e) {
            X.e(e, "Failed to saveCacheWebFile", new Object[0]);
        }
        return;
    }
}
