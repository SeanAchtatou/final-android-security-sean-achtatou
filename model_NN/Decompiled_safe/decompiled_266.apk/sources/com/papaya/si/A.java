package com.papaya.si;

import android.content.Intent;
import com.inmobi.androidsdk.InMobiAdDelegate;
import com.inmobi.androidsdk.impl.Constants;
import com.papaya.base.EntryActivity;
import com.papaya.chat.ChatActivity;
import com.papaya.si.C0060z;
import com.papaya.social.BillingChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.json.JSONArray;

public final class A extends aG implements aW, C0054t, C0058x {
    public static final C0011aj bL = new C0011aj(-1, C0037c.getString("me"));
    private String bM;
    private String bN;
    private int bO = -1;
    private C0012ak bP;
    private V bQ;
    private U bR;
    private T bS;
    private R bT;
    private M bU;
    private C0007af bV;
    private C0004ac bW;
    private V bX;
    private V bY;
    private List<E> bZ;
    private HashSet<Integer> ca;
    private HashSet<Integer> cb;
    private C0012ak cc;
    private List<List> cd;
    private C0031bc<C0059y> ce;

    public A() {
        new C0012ak();
        this.bP = new C0012ak();
        this.bQ = new V();
        this.bR = new U();
        this.bS = new T();
        this.bT = new R();
        this.bU = new M();
        this.bV = new C0007af();
        this.bW = new C0004ac();
        this.bX = new V();
        this.bY = new V();
        this.bZ = new ArrayList();
        this.ca = new HashSet<>(100);
        this.cb = new HashSet<>(4);
        new HashMap();
        this.cc = new C0012ak();
        this.cd = new ArrayList();
        this.ce = new C0031bc<>(4);
        C0037c.A.addConnectionDelegate(this);
    }

    private void addChatGroups(List list) {
        for (int i = 1; i < list.size(); i++) {
            List list2 = (List) aV.sget(list, i);
            L l = new L();
            l.cS = aV.sgetInt(list2, 0);
            l.cT = aV.sgetInt(list2, 1) == 1;
            aV.sgetInt(list2, 2);
            l.name = (String) aV.sget(list2, 3);
            aV.sgetInt(list2, 4);
            aV.sgetInt(list2, 5);
            l.description = (String) aV.sget(list2, 6);
            l.cU = aV.sgetInt(list2, 7);
            this.bU.add(l);
        }
        this.bU.fireDataStateChanged();
        fireDataStateChanged();
    }

    private void clearRMSData() {
        C0035bg.del("papayacontact1");
        C0035bg.del("papayaaccount1");
        C0035bg.del("papayaim1");
        C0035bg.del("papayafact");
        C0035bg.del("papayacontactorder");
        C0035bg.del("papayaregister");
    }

    private void clearSessionData() {
        this.bP.clear();
        this.bQ.clear();
        this.bR.clear();
        this.bS.clear();
        this.bW.clear();
        refreshCardLists();
        this.bO = -1;
        this.bM = null;
        bL.setUserID(0);
    }

    private void firePotpSessionUpdated(final String str, final String str2) {
        int size = this.ce.size();
        for (int i = 0; i < size; i++) {
            final C0059y yVar = this.ce.get(i);
            if (yVar != null) {
                if (yVar instanceof aW) {
                    C0030bb.runInHandlerThread(new Runnable() {
                        public final void run() {
                            C0059y.this.onPotpSessionUpdated(str, str2);
                        }
                    });
                } else {
                    yVar.onPotpSessionUpdated(str, str2);
                }
            }
        }
    }

    private void loadCards() {
    }

    private boolean loadLogin() {
        return false;
    }

    private void registerCommands() {
        C0037c.A.registerCmds(this, 0, 1, 2, 3, 5, 6, 7, 8, 11, 13, 14, 15, 16, 17, 25, 26, 27, 28, 29, 34, 37, 56, 57, 58, 59, 72, 80, 90, 91, 92, 93, Integer.valueOf((int) Constants.HTTP_SUCCESS_CODE), 300, 303, 305, 306, 307, 309, 310, 311, 312, 313, 314, 317, 600, 601, 10001);
    }

    private void removeChatGroup(int i) {
        L findByGroupID = this.bU.findByGroupID(i);
        if (findByGroupID != null) {
            this.bS.remove(findByGroupID);
            this.bU.remove(findByGroupID);
            this.bS.fireDataStateChanged();
            fireDataStateChanged();
        }
    }

    private void saveCards() {
    }

    public final void addIMAccount(int i, String str, String str2) {
        if (findIM(i, str) == null) {
            C0005ad adVar = new C0005ad();
            adVar.dO = i;
            adVar.dQ = str;
            adVar.dR = str2;
            this.bW.add(adVar);
            refreshCardLists();
            C0037c.send(4, Integer.valueOf(i), str, str2);
            adVar.setImState(2);
            fireDataStateChanged();
            this.bW.saveToFile("papayaim1");
        }
    }

    public final void addSessionDelegate(C0059y yVar) {
        if (yVar != null) {
            this.ce.add(yVar);
        }
    }

    public final void closeIM(C0005ad adVar) {
        Iterator it = adVar.toList().iterator();
        while (it.hasNext()) {
            this.bS.remove((C0006ae) it.next());
        }
        adVar.setImState(1);
        this.bS.fireDataStateChanged();
        fireDataStateChanged();
    }

    public final C0005ad findIM(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.bW.size()) {
                return null;
            }
            C0005ad adVar = (C0005ad) this.bW.get(i3);
            if (adVar.dP == i) {
                return adVar;
            }
            i2 = i3 + 1;
        }
    }

    public final C0005ad findIM(int i, String str) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.bW.size()) {
                return null;
            }
            C0005ad adVar = (C0005ad) this.bW.get(i3);
            if (adVar.dO == i && aV.equal(adVar.dQ, str)) {
                return adVar;
            }
            i2 = i3 + 1;
        }
    }

    public final HashSet<Integer> getAcceptedChatRoomRules() {
        return this.cb;
    }

    public final C0012ak getAllContacts() {
        return this.bP;
    }

    public final V getAppFriends() {
        return this.bX;
    }

    public final List<E> getCardLists() {
        return this.bZ;
    }

    public final M getChatGroups() {
        return this.bU;
    }

    public final R getChatRooms() {
        return this.bT;
    }

    public final T getChattings() {
        return this.bS;
    }

    public final U getContacts() {
        return this.bR;
    }

    public final String getDispname() {
        return this.bN;
    }

    public final V getFriends() {
        return this.bQ;
    }

    public final C0004ac getIms() {
        return this.bW;
    }

    public final V getNonappFriends() {
        return this.bY;
    }

    public final HashSet<Integer> getPrivateChatWhiteList() {
        return this.ca;
    }

    public final C0007af getPrivateChats() {
        return this.bV;
    }

    public final int getUserID() {
        return bL.getUserID();
    }

    public final C0012ak getWaitAdds() {
        return this.cc;
    }

    public final void handleServerResponse(Vector<Object> vector) {
        L l;
        Q findByRoomID;
        C0008ag agVar;
        S s;
        D iMUser;
        int sgetInt = aV.sgetInt(vector, 0);
        switch (sgetInt) {
            case 0:
            case 1:
            case 2:
            case 17:
            case 57:
            case Constants.HTTP_SUCCESS_CODE:
            case 601:
            case 10001:
                return;
            case 3:
                String str = (String) aV.sget(vector, 2);
                C0011aj ajVar = null;
                if (!(vector.get(1) instanceof String) && (vector.get(1) instanceof Integer)) {
                    ajVar = this.bQ.findByUserID(aV.sgetInt(vector, 1, 0));
                }
                if (ajVar != null) {
                    ajVar.addChatMessage(str, ajVar, aV.sgetInt(vector, 3, 0));
                    this.bS.add(ajVar);
                    this.bS.fireDataStateChanged();
                    ajVar.fireDataStateChanged();
                    return;
                }
                return;
            case 5:
                C0005ad findIM = findIM(aV.sgetInt(vector, 2), (String) aV.sget(vector, 3));
                if (findIM != null) {
                    findIM.dP = aV.sgetInt(vector, 1);
                    if (findIM.dP > 0) {
                        findIM.setImState(3);
                    } else {
                        if (findIM.getImState() == 2) {
                            C0028b.showInfo(aV.format(C0037c.getString("info_im_failed_login"), findIM.dQ));
                        }
                        findIM.setImState(1);
                    }
                    fireDataStateChanged();
                    return;
                }
                return;
            case 6:
                C0005ad findIM2 = findIM(aV.sgetInt(vector, 1));
                if (findIM2 != null) {
                    C0006ae iMUser2 = findIM2.getIMUser((String) vector.get(2));
                    iMUser2.dV = (String) aV.sget(vector, 3);
                    iMUser2.setState(aV.sgetInt(vector, 4));
                    if (vector.size() > 5) {
                        iMUser2.dW = (String) aV.sget(vector, 5);
                    }
                    iMUser2.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 7:
                this.cd.add((List) vector.get(1));
                return;
            case 8:
                C0005ad findIM3 = findIM(aV.sgetInt(vector, 1));
                if (findIM3 != null) {
                    C0006ae iMUser3 = findIM3.getIMUser((String) vector.get(2));
                    iMUser3.addChatMessage((String) vector.get(3), iMUser3, 0);
                    iMUser3.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 11:
                int sgetInt2 = aV.sgetInt(vector, 1);
                if (sgetInt2 == -1) {
                    iMUser = this.bQ.findByUserID(aV.sgetInt(vector, 2));
                } else if (sgetInt2 == -2) {
                    iMUser = this.bU.findByGroupID(aV.sgetInt(vector, 2));
                } else {
                    C0005ad findIM4 = findIM(sgetInt2);
                    iMUser = findIM4 != null ? findIM4.getIMUser((String) vector.get(2)) : null;
                }
                if (iMUser != null) {
                    iMUser.addSelfMessage((String) vector.get(3));
                    iMUser.fireDataStateChanged();
                    return;
                }
                return;
            case InMobiAdDelegate.INMOBI_AD_UNIT_120X600:
                for (int i = 0; i < vector.size(); i++) {
                    C0011aj findByUserID = this.bQ.findByUserID(aV.sgetInt(vector, i));
                    if (findByUserID != null) {
                        findByUserID.updateOnline(true);
                        this.bQ.remove(findByUserID);
                        this.bQ.insertSort(findByUserID);
                        findByUserID.fireDataStateChanged();
                    }
                }
                this.bQ.fireDataStateChanged();
                fireDataStateChanged();
                return;
            case 14:
                for (int i2 = 0; i2 < vector.size(); i2++) {
                    C0011aj findByUserID2 = this.bQ.findByUserID(aV.sgetInt(vector, i2));
                    if (findByUserID2 != null) {
                        findByUserID2.updateOnline(false);
                        this.bQ.remove(findByUserID2);
                        this.bQ.insertSort(findByUserID2);
                        findByUserID2.fireDataStateChanged();
                    }
                }
                this.bQ.fireDataStateChanged();
                fireDataStateChanged();
                return;
            case 15:
                C0011aj findByUserID3 = this.bQ.findByUserID(aV.sgetInt(vector, 1));
                if (findByUserID3 != null) {
                    findByUserID3.setMiniblog((String) vector.get(3));
                    findByUserID3.setMiniblogTime(System.currentTimeMillis() / 1000);
                    this.bQ.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case BillingChannel.REDEEM /*16*/:
                List list = (List) aV.sget(vector, 1);
                for (int i3 = 0; i3 < list.size(); i3 += 4) {
                    C0011aj findByUserID4 = this.bQ.findByUserID(aV.sgetInt(list, i3));
                    if (findByUserID4 != null) {
                        findByUserID4.setMiniblog((String) list.get(i3 + 2));
                        findByUserID4.setMiniblogTime((System.currentTimeMillis() / 1000) - ((long) aV.sgetInt(list, i3 + 3)));
                    }
                    this.bQ.fireDataStateChanged();
                    fireDataStateChanged();
                }
                this.bQ.fireDataStateChanged();
                return;
            case 25:
                Q findByRoomID2 = this.bT.findByRoomID(aV.sgetInt(vector, 1));
                if (findByRoomID2 != null) {
                    List list2 = (List) aV.sget(vector, 2);
                    String str2 = (String) aV.sget(list2, 0);
                    int sgetInt3 = aV.sgetInt(list2, 1);
                    String str3 = (String) aV.sget(list2, 3);
                    if ("login".equals(str2)) {
                        S s2 = new S();
                        s2.cW = sgetInt3;
                        s2.dm = aV.sgetInt(list2, 4);
                        s2.dn = str3;
                        s2.f0do = this.bQ.isFriend(sgetInt3);
                        aV.sgetInt(list2, 5);
                        findByRoomID2.addUser(s2);
                        findByRoomID2.addSystemMessage(aV.format(C0037c.getString("chat_msg_sys_enter"), str3));
                    } else if ("logout".equals(str2)) {
                        findByRoomID2.removeUser(sgetInt3);
                        findByRoomID2.addSystemMessage(aV.format(C0037c.getString("chat_msg_sys_left"), str3));
                    } else {
                        X.e("unknown chatroom action: %s", str2);
                    }
                    findByRoomID2.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 26:
                int sgetInt4 = aV.sgetInt(vector, 1);
                int sgetInt5 = aV.sgetInt(vector, 2);
                Q findByRoomID3 = this.bT.findByRoomID(sgetInt4);
                if (findByRoomID3 != null) {
                    if (sgetInt5 < 0) {
                        findByRoomID3.addSystemMessage((String) aV.sget(vector, 3));
                    } else {
                        S findUser = findByRoomID3.findUser(sgetInt5);
                        if (findUser == null) {
                            S s3 = new S();
                            s3.cW = sgetInt5;
                            s3.dn = (String) aV.sget(vector, 3);
                            s = s3;
                        } else {
                            s = findUser;
                        }
                        findByRoomID3.addChatMessage((String) aV.sget(vector, 4), s, aV.sgetInt(vector, 5, 0));
                    }
                    findByRoomID3.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 27:
                Q findByRoomID4 = this.bT.findByRoomID(aV.sgetInt(vector, 1));
                if (findByRoomID4 != null) {
                    findByRoomID4.dl.clear();
                    findByRoomID4.setState(1);
                    List list3 = (List) aV.sget(vector, 2);
                    for (int i4 = 0; i4 < list3.size(); i4++) {
                        List list4 = (List) list3.get(i4);
                        S s4 = new S();
                        s4.cW = aV.sgetInt(list4, 0);
                        s4.dm = aV.sgetInt(list4, 4);
                        s4.f0do = this.bQ.isFriend(s4.cW);
                        s4.dn = (String) aV.sget(list4, 2);
                        aV.sgetInt(list4, 5);
                        findByRoomID4.addUser(s4);
                    }
                    findByRoomID4.addSystemMessage(C0037c.getString("chat_msg_sys_you_joined") + findByRoomID4.name);
                    findByRoomID4.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 28:
                for (int i5 = 1; i5 < vector.size(); i5++) {
                    C0011aj findByUserID5 = this.bQ.findByUserID(aV.intValue(vector.get(i5)));
                    if (findByUserID5 != null) {
                        findByUserID5.setState(2);
                    }
                }
                fireDataStateChanged();
                return;
            case 29:
                for (int i6 = 1; i6 < vector.size(); i6++) {
                    C0011aj findByUserID6 = this.bQ.findByUserID(aV.intValue(vector.get(i6)));
                    if (findByUserID6 != null) {
                        findByUserID6.setState(1);
                    }
                }
                fireDataStateChanged();
                return;
            case 34:
                Q findByRoomID5 = this.bT.findByRoomID(aV.sgetInt(vector, 1));
                if (findByRoomID5 != null) {
                    findByRoomID5.logout();
                    findByRoomID5.addSystemMessage((String) aV.sget(vector, 3));
                    findByRoomID5.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 37:
                int sgetInt6 = aV.sgetInt(vector, 1);
                int sgetInt7 = aV.sgetInt(vector, 2);
                String str4 = (String) aV.sget(vector, 3);
                List list5 = (List) aV.sget(vector, 4);
                if (!this.ca.contains(Integer.valueOf(sgetInt7))) {
                    C0037c.send(62, Integer.valueOf(sgetInt6));
                    return;
                }
                C0008ag findBySID = this.bV.findBySID(sgetInt6);
                if (findBySID == null) {
                    C0008ag agVar2 = new C0008ag();
                    agVar2.dm = sgetInt6;
                    agVar2.cW = sgetInt7;
                    agVar2.dn = str4;
                    this.bS.add(agVar2);
                    this.bV.add(agVar2);
                    this.bS.fireDataStateChanged();
                    fireDataStateChanged();
                    agVar = agVar2;
                } else {
                    agVar = findBySID;
                }
                if (agVar != null) {
                    int sgetInt8 = aV.sgetInt(list5, 0);
                    String str5 = (String) aV.sget(list5, 1);
                    if (sgetInt8 == -1) {
                        agVar.addChatMessage(str5, agVar, aV.sgetInt(vector, 5));
                    } else {
                        agVar.addSystemMessage(str5);
                    }
                    agVar.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 56:
                List list6 = (List) aV.sget(vector, 1);
                int sgetInt9 = aV.sgetInt(list6, 0);
                String str6 = (String) aV.sget(list6, 1);
                int sgetInt10 = aV.sgetInt(list6, 2);
                int sgetInt11 = aV.sgetInt(vector, 3);
                Q findByRoomID6 = this.bT.findByRoomID(sgetInt9);
                if (findByRoomID6 != null) {
                    if (sgetInt10 > 0) {
                        findByRoomID6.removeUser(sgetInt11);
                        findByRoomID6.addSystemMessage(str6 + C0037c.getString("base_kickout"));
                    } else {
                        findByRoomID6.addSystemMessage(C0037c.getString("chat_msg_sys_fail_kick") + str6);
                    }
                    findByRoomID6.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 58:
                C0011aj findByUserID7 = this.bQ.findByUserID(aV.intValue(vector.get(1)));
                if (findByUserID7 != null) {
                    findByUserID7.addSystemMessage((String) vector.get(2));
                    findByUserID7.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 59:
                Q findByRoomID7 = this.bT.findByRoomID(aV.sgetInt(vector, 1));
                if (findByRoomID7 != null) {
                    findByRoomID7.logout();
                    findByRoomID7.fireDataStateChanged();
                    findByRoomID7.addSystemMessage(C0037c.getString("base_meout"));
                    fireDataStateChanged();
                    return;
                }
                return;
            case 72:
                for (int i7 = 1; i7 < vector.size(); i7++) {
                    C0011aj findByUserID8 = this.bQ.findByUserID(aV.intValue(vector.get(i7)));
                    if (findByUserID8 != null) {
                        findByUserID8.setState(3);
                    }
                }
                fireDataStateChanged();
                return;
            case 80:
                List list7 = (List) vector.get(1);
                for (int i8 = 0; i8 < list7.size(); i8 += 3) {
                    this.bQ.updateMiniblog(aV.intValue(list7.get(i8)), (String) list7.get(i8 + 1), aV.intValue(list7.get(i8 + 2)));
                }
                fireDataStateChanged();
                return;
            case 90:
                this.bT.clear();
                List list8 = (List) vector.get(1);
                for (int i9 = 0; i9 < list8.size(); i9++) {
                    List list9 = (List) aV.sget(list8, i9);
                    Q q = new Q();
                    q.df = aV.sgetInt(list9, 0);
                    q.name = (String) aV.sget(list9, 1);
                    q.dh = aV.sgetInt(list9, 2);
                    q.di = aV.sgetInt(list9, 3);
                    aV.sgetInt(list9, 4);
                    q.dj = aV.sgetInt(list9, 5) > 0;
                    q.dg = (String) aV.sget(list9, 6);
                    this.bT.add(q);
                }
                fireDataStateChanged();
                return;
            case 91:
                for (int i10 = 0; i10 < vector.size(); i10++) {
                    if (i10 % 2 == 1 && (findByRoomID = this.bT.findByRoomID(aV.sgetInt(vector, i10))) != null) {
                        findByRoomID.dh = aV.sgetInt(vector, i10 + 1);
                        findByRoomID.fireDataStateChanged();
                    }
                }
                fireDataStateChanged();
                return;
            case 92:
                for (int i11 = 1; i11 < vector.size(); i11++) {
                    if (i11 % 2 == 1) {
                        int sgetInt12 = aV.sgetInt(vector, 1);
                        String str7 = (String) aV.sget(vector, i11 + 1);
                        Q findByRoomID8 = this.bT.findByRoomID(sgetInt12);
                        if (findByRoomID8 != null) {
                            findByRoomID8.dg = str7;
                            findByRoomID8.fireDataStateChanged();
                        }
                    }
                }
                return;
            case 93:
                aV.sgetInt(vector, 1);
                aV.stringValue(aV.sget(vector, 2));
                return;
            case 300:
                this.bU.clear();
                addChatGroups(vector);
                return;
            case 303:
                if (aV.sgetInt(vector, 2) != 0) {
                    String str8 = (String) aV.sget(vector, 3);
                    C0028b.showInfo(str8 == null ? C0037c.getString("alert_chatgroup_failed_leave") : str8);
                    return;
                }
                return;
            case 306:
                removeChatGroup(aV.sgetInt(vector, 1));
                return;
            case 307:
                removeChatGroup(aV.sgetInt(vector, 1));
                return;
            case 309:
                L findByGroupID = this.bU.findByGroupID(aV.sgetInt(vector, 1));
                if (findByGroupID != null) {
                    for (int i12 = 2; i12 < vector.size(); i12++) {
                        List list10 = (List) aV.sget(vector, i12);
                        N userCard = findByGroupID.getUserCard(aV.sgetInt(list10, 0), (String) list10.get(1));
                        userCard.setState(1);
                        findByGroupID.addChatMessage((String) aV.sget(list10, 2), userCard, aV.sgetInt(list10, 3));
                    }
                    findByGroupID.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 312:
                for (int i13 = 1; i13 < vector.size(); i13++) {
                    List list11 = (List) aV.sget(vector, i13);
                    L findByGroupID2 = this.bU.findByGroupID(aV.sgetInt(list11, 0));
                    if (findByGroupID2 == null) {
                        L l2 = new L();
                        this.bU.add(l2);
                        l = l2;
                    } else {
                        l = findByGroupID2;
                    }
                    l.cT = aV.sgetInt(list11, 1) == 1;
                    aV.sgetInt(list11, 2);
                    l.name = (String) aV.sget(list11, 3);
                    aV.sgetInt(list11, 4);
                    aV.sgetInt(list11, 5);
                    l.description = (String) aV.sget(list11, 6);
                    l.cU = aV.sgetInt(list11, 7);
                    l.fireDataStateChanged();
                }
                fireDataStateChanged();
                return;
            case 313:
                addChatGroups(vector);
                return;
            case 314:
                removeChatGroup(aV.sgetInt(vector, 1));
                return;
            case 317:
                L findByGroupID3 = this.bU.findByGroupID(aV.sgetInt(vector, 1));
                if (findByGroupID3 != null) {
                    findByGroupID3.addSystemMessage((String) vector.get(2));
                    findByGroupID3.fireDataStateChanged();
                    return;
                }
                return;
            case 600:
                String str9 = this.bM;
                this.bM = (String) aV.sget(vector, 1);
                firePotpSessionUpdated(str9, this.bM);
                bL.setState(1);
                bL.fireDataStateChanged();
                C0011aj findByUserID9 = this.bQ.findByUserID(bL.getUserID());
                if (findByUserID9 != null) {
                    this.bQ.remove(findByUserID9);
                    this.bQ.insertSort(findByUserID9);
                }
                C0037c.send(300, new Object[0]);
                C0037c.send(82, new Object[0]);
                C0028b.removeOverlayDialog(5);
                return;
            default:
                X.e("Unknown cmd: " + sgetInt, new Object[0]);
                return;
        }
    }

    public final void increaseRevision() {
        this.bO++;
    }

    public final void initialize() {
        registerCommands();
        refreshCardLists();
    }

    public final void insertUserCard(C0011aj ajVar) {
        if (ajVar != null) {
            this.bP.insertSort(ajVar);
            if (ajVar.getUserID() != 0) {
                this.bQ.insertSort(ajVar);
            } else {
                this.bR.insertSort(ajVar);
            }
        }
    }

    public final boolean isLoggedIn() {
        return !aV.isEmpty(this.bM);
    }

    public final synchronized void loadCoreData() {
        loadLogin();
    }

    public final void loadMiscData() {
        try {
            this.bW.loadFromFile("papayaim1");
            refreshCardLists();
            fireDataStateChanged();
        } catch (Exception e) {
            X.e(e, "Failed in loadMiscData", new Object[0]);
        }
    }

    public final void login() {
        if (C0037c.A.isRunning() && C0025ax.getInstance().isConnected()) {
            C0028b.showOverlayDialog(5);
            C0037c.send(600, 9, 144, C0029ba.ANDROID_ID, C0023av.getInstance().getApiKey(), 0, C0056v.bl, 0, C0056v.bd, Integer.valueOf(getUserID()), C0025ax.getInstance().getSessionKey());
        }
    }

    public final void logout() {
        X.i("Logging out papaya!", new Object[0]);
        C0060z.a.logout();
        clearRMSData();
        clearSessionData();
    }

    public final void onConnectionEstablished() {
    }

    public final void onConnectionLost() {
        this.bS.clear();
        for (int i = 0; i < this.bW.size(); i++) {
            ((C0005ad) this.bW.get(i)).setImState(1);
        }
        bL.setState(0);
        this.bU.clear();
        this.bT.setCardStates(0);
        this.bQ.setCardStates(0);
        EntryActivity.a.refreshUnread();
        this.bS.fireDataStateChanged();
        bL.fireDataStateChanged();
        fireDataStateChanged();
    }

    public final void quit() {
        if (isLoggedIn()) {
            saveData();
        }
        if (C0037c.A != null) {
            C0037c.A.removeConnectionDelegate(this);
        }
    }

    public final void refreshCardLists() {
        this.bZ.clear();
        this.bZ.add(this.bS);
        this.bZ.addAll(this.bW);
        this.bZ.add(this.bU);
        this.bZ.add(this.bT);
        this.bZ.add(this.bQ);
    }

    public final void removeSessionDelegate(C0059y yVar) {
        if (yVar != null) {
            this.ce.remove(yVar);
        }
    }

    public final void removeUserCard(C0011aj ajVar) {
        if (ajVar != null) {
            this.bP.remove(ajVar);
            if (ajVar.getUserID() != 0) {
                this.bQ.remove(ajVar);
            } else {
                this.bR.remove(ajVar);
            }
        }
    }

    public final void reupdateFriendList(JSONArray jSONArray) {
        this.bQ.clear();
        this.bX.clear();
        this.bY.clear();
        this.bQ.addUserFromJSON(jSONArray.optJSONArray(1), 0);
        this.bX.addUserFromJSON(jSONArray.optJSONArray(1), 0);
        this.bQ.addUserFromJSON(jSONArray.optJSONArray(2), 0);
        this.bY.addUserFromJSON(jSONArray.optJSONArray(2), 0);
        this.bQ.fireDataStateChanged();
        fireDataStateChanged();
    }

    public final void saveData() {
        if (isLoggedIn()) {
            this.bW.saveToFile("papayaim1");
        }
    }

    public final void saveGameGuide() {
    }

    public final void saveLogin() {
    }

    public final void sendChatMessage(D d, String str) {
        if (d instanceof C0011aj) {
            C0037c.send(6, -1, Integer.valueOf(((C0011aj) d).getUserID()), str);
        } else if (d instanceof C0006ae) {
            C0037c.send(6, Integer.valueOf(((C0006ae) d).dX.dP), ((C0006ae) d).dQ, str);
        } else if (d instanceof Q) {
            C0037c.send(22, Integer.valueOf(((Q) d).df), str);
        } else if (d instanceof C0008ag) {
            C0037c.send(32, Integer.valueOf(((C0008ag) d).dm), str);
        } else if (d instanceof L) {
            C0037c.send(308, Integer.valueOf(((L) d).cS), str);
        } else {
            X.e("Unknown card to send text: " + d, new Object[0]);
        }
    }

    public final void sendPhoto(D d, byte[] bArr, String str) {
        boolean z;
        if (d instanceof C0011aj) {
            C0037c.send(7, -1, Integer.valueOf(((C0011aj) d).getUserID()), bArr, str);
            z = true;
        } else if (d instanceof C0006ae) {
            C0037c.send(7, Integer.valueOf(((C0006ae) d).dX.dP), ((C0006ae) d).dQ, bArr, str);
            z = true;
        } else if (d instanceof L) {
            C0037c.send(7, -2, Integer.valueOf(((L) d).cS), bArr, str);
            z = true;
        } else {
            z = false;
        }
        if (z) {
            d.addSystemMessage(C0037c.getString("chat_msg_sending_photo"));
            d.fireDataStateChanged();
        }
    }

    public final void setDispname(String str) {
        this.bN = str;
    }

    public final void startChat(int i) {
        C0011aj findByUserID = this.bQ.findByUserID(i);
        if (findByUserID != null) {
            this.bS.add(findByUserID);
            C0028b.startActivity(new Intent(C0037c.getApplicationContext(), ChatActivity.class).putExtra("active", this.bS.indexOf(findByUserID)));
        }
    }
}
