package com.papaya.service;

import android.accounts.Account;
import android.accounts.AccountManager;
import com.papaya.si.C0037c;
import com.papaya.si.X;

public class AccountManagerWrapper2x extends AccountManagerWrapper {
    private static AppAccount convert(Account account) {
        return new AppAccount(account.name, account.type);
    }

    public AppAccount[] listAccounts() {
        try {
            Account[] accounts = AccountManager.get(C0037c.getApplicationContext()).getAccounts();
            AppAccount[] appAccountArr = new AppAccount[accounts.length];
            for (int i = 0; i < accounts.length; i++) {
                appAccountArr[i] = convert(accounts[i]);
            }
            return appAccountArr;
        } catch (Throwable th) {
            X.e(th, "Failed to listAccounts", new Object[0]);
            return new AppAccount[0];
        }
    }

    public AppAccount[] listAccountsByType(String str) {
        try {
            Account[] accountsByType = AccountManager.get(C0037c.getApplicationContext()).getAccountsByType(str);
            AppAccount[] appAccountArr = new AppAccount[accountsByType.length];
            for (int i = 0; i < accountsByType.length; i++) {
                appAccountArr[i] = convert(accountsByType[i]);
            }
            return appAccountArr;
        } catch (Exception e) {
            X.e(e, "Failed to listAccountsByType: " + str, new Object[0]);
            return new AppAccount[0];
        }
    }
}
