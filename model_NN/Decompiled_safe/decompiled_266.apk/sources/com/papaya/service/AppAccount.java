package com.papaya.service;

import com.adknowledge.superrewards.model.SROffer;
import com.papaya.si.X;
import org.json.JSONArray;
import org.json.JSONObject;

public class AppAccount {
    private String name;
    private String type;

    public AppAccount() {
    }

    public AppAccount(String str, String str2) {
        this.name = str;
        this.type = str2;
    }

    public static JSONArray toJSONArray(AppAccount[] appAccountArr) {
        JSONArray jSONArray = new JSONArray();
        if (appAccountArr != null) {
            try {
                for (AppAccount appAccount : appAccountArr) {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("name", appAccount.name);
                    jSONObject.put(SROffer.TYPE, appAccount.type);
                    jSONArray.put(jSONObject);
                }
            } catch (Exception e) {
                X.w(e, "Failed in AppAccount.toJSONArray", new Object[0]);
            }
        }
        return jSONArray;
    }
}
