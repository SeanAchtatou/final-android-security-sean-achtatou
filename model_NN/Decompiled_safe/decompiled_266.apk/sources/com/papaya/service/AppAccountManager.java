package com.papaya.service;

import com.papaya.si.aV;

public class AppAccountManager {
    private static AccountManagerWrapper eG;

    static {
        if (aV.existClass("android.accounts.AccountManager")) {
            eG = (AccountManagerWrapper) aV.newInstance("com.papaya.service.AccountManagerWrapper2x");
        }
        if (eG == null) {
            eG = new AccountManagerWrapper();
        }
    }

    public static AccountManagerWrapper getWrapper() {
        return eG;
    }
}
