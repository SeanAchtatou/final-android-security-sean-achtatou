package com.papaya.chat;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;
import com.papaya.base.PotpActivity;
import com.papaya.si.A;
import com.papaya.si.C0011aj;
import com.papaya.si.C0028b;
import com.papaya.si.C0030bb;
import com.papaya.si.C0037c;
import com.papaya.si.C0060z;
import com.papaya.si.X;
import com.papaya.si.Y;
import com.papaya.si.aN;
import com.papaya.si.aW;
import com.papaya.view.CardImageView;
import com.papaya.view.CustomDialog;

public class FriendsActivity extends PotpActivity implements aW {
    /* access modifiers changed from: private */
    public CardImageView ds;
    private View dt;
    /* access modifiers changed from: private */
    public TextView du;
    private ImageButton dv;
    private ExpandableListView dw;
    private Y dx;
    private aN<C0011aj> dy = new X(this);

    /* access modifiers changed from: protected */
    public int myLayout() {
        return C0060z.layoutID("friends");
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        A.bL.registerMonitor(this.dy);
        this.ds = (CardImageView) C0030bb.find(this, "user_image");
        this.ds.setDefaultDrawable(C0037c.getDrawable("avatar_default"));
        this.dt = (View) C0030bb.find(this, "spinner");
        this.du = (TextView) C0030bb.find(this.dt, "text");
        this.dt.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                FriendsActivity.this.showDialog(0);
            }
        });
        C0030bb.find(this, "search_button");
        this.dv = (ImageButton) C0030bb.find(this, "add_im_button");
        this.dv.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                C0028b.openPRIALink(FriendsActivity.this, "static_addim");
            }
        });
        this.dw = (ExpandableListView) C0030bb.find(this, "friends_list");
        this.dw.setDivider(null);
        this.dx = new Y(this);
        this.dw.setAdapter(this.dx);
        this.dw.setGroupIndicator(null);
        this.dw.setOnChildClickListener(this.dx);
        this.dw.setDivider(new ColorDrawable(0));
        this.dw.setChildDivider(new ColorDrawable(Color.parseColor("#D4D4D4")));
        this.dw.setDividerHeight(1);
        this.dy.onDataStateChanged(A.bL);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        if (i != 0) {
            return null;
        }
        return new CustomDialog.Builder(this).setItems(new CharSequence[]{C0037c.getString("state_online"), C0037c.getString("state_busy"), C0037c.getString("state_idle")}, new DialogInterface.OnClickListener() {
            public final void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case 0:
                        A.bL.setState(1);
                        C0037c.send(24, 29);
                        break;
                    case 1:
                        A.bL.setState(3);
                        C0037c.send(24, 72);
                        break;
                    case 2:
                        A.bL.setState(2);
                        C0037c.send(24, 28);
                        break;
                    default:
                        X.e("unknown state: " + i, new Object[0]);
                        break;
                }
                A.bL.fireDataStateChanged();
            }
        }).create();
    }

    public void onDestroy() {
        super.onDestroy();
        A.bL.unregisterMonitor(this.dy);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.dx.setPaused(true);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.dx.setPaused(false);
    }
}
