package com.papaya.chat;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.papaya.base.PotpActivity;
import com.papaya.si.A;
import com.papaya.si.C0006ae;
import com.papaya.si.C0008ag;
import com.papaya.si.C0011aj;
import com.papaya.si.C0030bb;
import com.papaya.si.C0037c;
import com.papaya.si.C0060z;
import com.papaya.si.D;
import com.papaya.si.F;
import com.papaya.si.G;
import com.papaya.si.H;
import com.papaya.si.I;
import com.papaya.si.J;
import com.papaya.si.K;
import com.papaya.si.L;
import com.papaya.si.Q;
import com.papaya.si.S;
import com.papaya.si.T;
import com.papaya.si.X;
import com.papaya.si.aN;
import com.papaya.si.aV;
import com.papaya.view.Action;
import com.papaya.view.ActionsImageButton;
import com.papaya.view.CardImageView;
import com.papaya.view.ChatGroupUserListView;
import com.papaya.view.ChatRoomUserListView;
import com.papaya.view.CustomDialog;
import com.papaya.view.EmoticonPanelView;

public class ChatActivity extends PotpActivity implements ChatRoomUserListView.Delegate {
    private CardImageView cA;
    private TextView cB;
    private ListView cC;
    private ImageButton cD;
    private ImageButton cE;
    /* access modifiers changed from: private */
    public EditText cF;
    private ActionsImageButton cG;
    private K cH;
    /* access modifiers changed from: private */
    public D cI;
    private EmoticonPanelView.Delegate cJ = new F(this);
    private aN<T> cK = new G(this);
    private aN<D> cL = new H(this);
    private ActionsImageButton.Delegate cM = new I(this);
    private ActionsImageButton.Delegate cN = new J(this);
    private ImageView ct;
    private ImageView cu;
    private View cv;
    private View cw;
    private ActionsImageButton cx;
    private CardImageView cy;
    private CardImageView cz;

    /* access modifiers changed from: private */
    public void closeActiveChat() {
        T chattings = C0037c.getSession().getChattings();
        int indexOf = chattings.indexOf(this.cI);
        chattings.remove(this.cI);
        if (indexOf >= chattings.size()) {
            indexOf--;
        }
        this.cI.fireDataStateChanged();
        if (indexOf >= 0 && indexOf < chattings.size()) {
            refreshWithCard(chattings.get(indexOf), false);
        }
        chattings.fireDataStateChanged();
    }

    /* access modifiers changed from: private */
    public void goNext() {
        T chattings = C0037c.getSession().getChattings();
        int indexOf = chattings.indexOf(this.cI) + 1;
        if (indexOf < chattings.size()) {
            refreshWithCard(chattings.get(indexOf), false);
        }
    }

    /* access modifiers changed from: private */
    public void goPrevious() {
        T chattings = C0037c.getSession().getChattings();
        int indexOf = chattings.indexOf(this.cI) - 1;
        if (indexOf < 0) {
            indexOf = 0;
        }
        if (indexOf < chattings.size()) {
            refreshWithCard(chattings.get(indexOf), false);
        }
    }

    private void refreshActionsButton() {
        this.cx.clearActions();
        this.cx.addAction(new Action(0, C0037c.getDrawable("chat_icon_close"), C0037c.getString("close")));
        if (this.cI instanceof C0011aj) {
            C0011aj ajVar = (C0011aj) this.cI;
            if (ajVar.getUserID() > 0 && ajVar.getUserID() != C0037c.getSession().getUserID()) {
                this.cx.addAction(new Action(1, null, C0037c.getString("action_visit_home")));
                this.cx.addAction(new Action(3, null, C0037c.getString("action_remove_friend")));
            }
        } else if (this.cI instanceof Q) {
            this.cx.addAction(new Action(5, null, C0037c.getString("action_chatroom_users")));
            if (((Q) this.cI).dk) {
                this.cx.addAction(new Action(6, null, C0037c.getString("action_disallow_private_chat")));
            } else {
                this.cx.addAction(new Action(7, null, C0037c.getString("action_allow_private_chat")));
            }
        } else if (this.cI instanceof C0008ag) {
            if (C0037c.getSession().getPrivateChatWhiteList().contains(Integer.valueOf(((C0008ag) this.cI).cW))) {
                this.cx.addAction(new Action(6, null, C0037c.getString("action_disallow_private_chat")));
            } else {
                this.cx.addAction(new Action(7, null, C0037c.getString("action_allow_private_chat")));
            }
        } else if (this.cI instanceof L) {
            L l = (L) this.cI;
            if (l.cT) {
                this.cx.addAction(new Action(9, null, C0037c.getString("action_chatgroup_manage")));
            }
            this.cx.addAction(new Action(10, null, C0037c.getString("action_chatgroup_users")));
            if (aV.bitTest(l.cU, 0)) {
                this.cx.addAction(new Action(12, null, C0037c.getString("action_chatgroup_unblock")));
            } else {
                this.cx.addAction(new Action(11, null, C0037c.getString("action_chatgroup_block")));
            }
            if (!l.cT) {
                this.cx.addAction(new Action(8, null, C0037c.getString("action_chatgroup_leave")));
            } else {
                this.cx.addAction(new Action(8, null, C0037c.getString("action_chatgroup_admin_leave")));
            }
        }
        this.cG.clearActions();
        if (this.cI instanceof Q) {
            this.cG.addAction(new Action(1, null, C0037c.getString("action_visit_home")));
            this.cG.addAction(new Action(2, null, C0037c.getString("action_private_chat")));
            if (((Q) this.cI).dj) {
                this.cG.addAction(new Action(4, null, C0037c.getString("action_kick")));
            }
        }
    }

    /* access modifiers changed from: private */
    public void refreshChattingBar(T t) {
        int indexOf = t.indexOf(this.cI);
        if (indexOf == -1) {
            indexOf = 0;
        }
        this.ct.setVisibility(indexOf == 0 ? 4 : 0);
        this.cy.setVisibility(indexOf == 0 ? 4 : 0);
        if (indexOf - 1 < 0 || indexOf - 1 >= t.size()) {
            this.cy.refreshWithCard(null);
        } else {
            this.cy.refreshWithCard(t.get(indexOf - 1));
        }
        refreshActionsButton();
        if (this.cI != null) {
            this.cz.refreshWithCard(this.cI);
            this.cB.setText(this.cI.getTitle());
            setTitle(this.cI.getTitle());
        } else {
            this.cz.refreshWithCard(null);
            this.cB.setText((CharSequence) null);
            setTitle(getHintedTitle());
        }
        this.cu.setVisibility(indexOf + 1 < t.size() ? 0 : 4);
        this.cA.setVisibility(indexOf + 1 < t.size() ? 0 : 4);
        if (indexOf + 1 < t.size()) {
            this.cA.refreshWithCard(t.get(indexOf + 1));
        } else {
            this.cA.refreshWithCard(null);
        }
    }

    private void refreshInputArea() {
        this.cD.setVisibility((((this.cI instanceof C0011aj) || (this.cI instanceof C0006ae) || (this.cI instanceof L)) && this.cI.getState() != 0) ? 0 : 8);
    }

    /* access modifiers changed from: private */
    public void refreshWithCard(D d, boolean z) {
        try {
            if (this.cI != d) {
                if (this.cI != null) {
                    this.cI.setChatActive(false);
                    this.cI.unregisterMonitor(this.cL);
                }
                this.cI = d;
                this.cI.registerMonitor(this.cL);
            }
            this.cI.setChatActive(true);
            if (!z) {
                refreshChattingBar(C0037c.getSession().getChattings());
            }
            refreshInputArea();
            this.cH.refreshWithCard(this.cI);
        } catch (Exception e) {
            X.e(e, "failed to refresh card", new Object[0]);
        }
    }

    /* access modifiers changed from: private */
    public void sendText() {
        Editable text = this.cF.getText();
        String obj = text == null ? null : text.toString();
        if (!aV.isEmpty(obj) && this.cI != null) {
            if (this.cI.state != 0) {
                this.cI.addSelfMessage(obj);
                C0037c.getSession().sendChatMessage(this.cI, obj);
                this.cI.fireDataStateChanged();
                this.cF.setText((CharSequence) null);
            } else if (this.cI instanceof C0011aj) {
                this.cI.addSystemMessage(((C0011aj) this.cI).getName() + C0037c.getString("base_status_offline"));
                this.cI.fireDataStateChanged();
            }
        }
    }

    /* access modifiers changed from: protected */
    public int myLayout() {
        return C0060z.layoutID("chats");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        Bitmap bitmap = null;
        if (i2 == -1) {
            if (i == 1) {
                try {
                    bitmap = C0030bb.getCameraBitmap(this, intent, 240, 240, true);
                } catch (Exception e) {
                    X.e(e, "Failed to send photo", new Object[0]);
                    return;
                }
            } else if (i == 2) {
                bitmap = C0030bb.createScaledBitmap(getContentResolver(), intent.getData(), 240, 240, true);
            }
            if (bitmap != null) {
                byte[] compressBitmap = C0030bb.compressBitmap(bitmap, Bitmap.CompressFormat.JPEG, 35);
                if (compressBitmap != null && compressBitmap.length > 0) {
                    C0037c.getSession().sendPhoto(this.cI, compressBitmap, "jpg");
                }
                bitmap.recycle();
            }
        }
    }

    public void onChatroomUserSelected(ChatRoomUserListView chatRoomUserListView, S s) {
        if (s.cW != A.bL.getUserID()) {
            this.cG.hI = s;
            this.cG.onClick(this.cG);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.ct = (ImageView) C0030bb.find(this, "previous");
        this.cu = (ImageView) C0030bb.find(this, "next");
        this.cv = (View) C0030bb.find(this, "previous_content");
        this.cw = (View) C0030bb.find(this, "next_content");
        this.cv.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                ChatActivity.this.goPrevious();
            }
        });
        this.cw.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                ChatActivity.this.goNext();
            }
        });
        this.cx = (ActionsImageButton) C0030bb.find(this, "actions");
        this.cx.setActionDelegate(this.cM);
        this.cx.setCombinedDrawable(C0037c.getDrawable("chat_icon_dropdown"));
        this.cy = (CardImageView) C0030bb.find(this, "left_image");
        this.cz = (CardImageView) C0030bb.find(this, "middle_image");
        this.cA = (CardImageView) C0030bb.find(this, "right_image");
        this.cB = (TextView) C0030bb.find(this, "name");
        this.cC = (ListView) C0030bb.find(this, "messages");
        this.cH = new K(this);
        this.cC.setAdapter((ListAdapter) this.cH);
        this.cC.setOnItemClickListener(this.cH);
        this.cD = (ImageButton) C0030bb.find(this, "photo");
        this.cE = (ImageButton) C0030bb.find(this, "emoticon");
        this.cF = (EditText) C0030bb.find(this, "input");
        this.cD.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                if (ChatActivity.this.cI != null && ChatActivity.this.cI.getState() != 0) {
                    ChatActivity.this.showDialog(1);
                }
            }
        });
        this.cF.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i != 4) {
                    return true;
                }
                ChatActivity.this.sendText();
                return true;
            }
        });
        this.cF.setOnKeyListener(new View.OnKeyListener() {
            public final boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i != 23 && i != 66) {
                    return false;
                }
                ChatActivity.this.sendText();
                return false;
            }
        });
        this.cE.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                ChatActivity.this.showDialog(0);
            }
        });
        this.cG = new ActionsImageButton(this);
        this.cG.setActionDelegate(this.cN);
        T chattings = C0037c.getSession().getChattings();
        chattings.registerMonitor(this.cK);
        int intExtra = getIntent().getIntExtra("active", 0);
        if (intExtra >= 0 && intExtra < chattings.size()) {
            refreshWithCard(chattings.get(intExtra), false);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        CustomDialog create;
        switch (i) {
            case 0:
                EmoticonPanelView emoticonPanelView = new EmoticonPanelView(this);
                emoticonPanelView.setDelegate(this.cJ);
                create = new CustomDialog.Builder(this).setView(emoticonPanelView).create();
                break;
            case 1:
                create = new CustomDialog.Builder(this).setItems(new CharSequence[]{C0037c.getString("web_up_photo_camera"), C0037c.getString("web_up_photo_pictures")}, new DialogInterface.OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        if (i == 0) {
                            C0030bb.startCameraActivity(ChatActivity.this, 1);
                        } else {
                            C0030bb.startGalleryActivity(ChatActivity.this, 2);
                        }
                    }
                }).create();
                break;
            case 2:
                ChatRoomUserListView chatRoomUserListView = new ChatRoomUserListView(this);
                chatRoomUserListView.setDelegate(this);
                create = new CustomDialog.Builder(this).setView(chatRoomUserListView).create();
                break;
            case 3:
                create = new CustomDialog.Builder(this).setView(new ChatGroupUserListView(this)).create();
                break;
            case 4:
                create = new CustomDialog.Builder(this).setPositiveButton(C0037c.getString("btn_leave"), new DialogInterface.OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        if (ChatActivity.this.cI instanceof L) {
                            L l = (L) ChatActivity.this.cI;
                            ChatActivity.this.closeActiveChat();
                            C0037c.getSession().getChatGroups().remove(l);
                            C0037c.getSession().fireDataStateChanged();
                            if (l.cT) {
                                C0037c.send(306, Integer.valueOf(l.cS));
                                return;
                            }
                            C0037c.send(303, Integer.valueOf(l.cS));
                        }
                    }
                }).setNegativeButton(C0037c.getString("btn_cancel"), (DialogInterface.OnClickListener) null).create();
                break;
            case 5:
                create = new CustomDialog.Builder(this).setPositiveButton(C0037c.getString("btn_remove"), new DialogInterface.OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        if (ChatActivity.this.cI instanceof C0011aj) {
                            C0011aj ajVar = (C0011aj) ChatActivity.this.cI;
                            C0037c.getSession().removeUserCard(ajVar);
                            C0037c.getSession().increaseRevision();
                            C0037c.send(601, Integer.valueOf(ajVar.getUserID()));
                            ChatActivity.this.closeActiveChat();
                        }
                    }
                }).setNegativeButton(C0037c.getString("btn_cancel"), (DialogInterface.OnClickListener) null).create();
                break;
            default:
                X.e("unknown dialog id: " + i, new Object[0]);
                create = null;
                break;
        }
        if (create != null) {
            create.setCanceledOnTouchOutside(true);
        }
        return create;
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.cI != null) {
            this.cI.setChatActive(false);
            this.cI.unregisterMonitor(this.cL);
        }
        C0037c.getSession().getChattings().unregisterMonitor(this.cK);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.cH.setPaused(true);
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i, Dialog dialog) {
        super.onPrepareDialog(i, dialog);
        switch (i) {
            case 2:
                if (this.cI instanceof Q) {
                    ((ChatRoomUserListView) ((CustomDialog) dialog).getCustomView()).refreshWithCard((Q) this.cI);
                    return;
                }
                return;
            case 3:
                if (this.cI instanceof L) {
                    ((ChatGroupUserListView) ((CustomDialog) dialog).getCustomView()).refreshWithGroup((L) this.cI);
                    return;
                }
                return;
            case 4:
                if (this.cI instanceof L) {
                    L l = (L) this.cI;
                    if (l.cT) {
                        ((CustomDialog) dialog).setMessage(aV.format(C0037c.getString("alert_chatgroup_admin_leave_confirm"), l.name));
                        return;
                    }
                    ((CustomDialog) dialog).setMessage(aV.format(C0037c.getString("alert_chatgroup_leave_confirm"), l.name));
                    return;
                }
                return;
            case 5:
                if (this.cI instanceof C0011aj) {
                    ((CustomDialog) dialog).setMessage(aV.format(C0037c.getString("alert_remove_friend"), ((C0011aj) this.cI).getName()));
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.cH.setPaused(false);
    }
}
