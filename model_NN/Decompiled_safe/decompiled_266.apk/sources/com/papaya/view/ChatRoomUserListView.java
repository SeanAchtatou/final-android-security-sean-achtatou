package com.papaya.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.papaya.si.A;
import com.papaya.si.C0037c;
import com.papaya.si.C0060z;
import com.papaya.si.Q;
import com.papaya.si.S;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class ChatRoomUserListView extends ListView implements AdapterView.OnItemClickListener {
    private WeakReference<Delegate> hp;
    /* access modifiers changed from: private */
    public ArrayList<S> ii = new ArrayList<>();
    private a il;

    public interface Delegate {
        void onChatroomUserSelected(ChatRoomUserListView chatRoomUserListView, S s);
    }

    class a extends BaseAdapter {
        /* synthetic */ a(ChatRoomUserListView chatRoomUserListView) {
            this((byte) 0);
        }

        private a(byte b) {
        }

        public final int getCount() {
            return ChatRoomUserListView.this.ii.size();
        }

        public final Object getItem(int i) {
            return Integer.valueOf(i);
        }

        public final long getItemId(int i) {
            return (long) i;
        }

        public final View getView(int i, View view, ViewGroup viewGroup) {
            View view2;
            TextView textView;
            String str;
            if (view == null) {
                View inflate = View.inflate(ChatRoomUserListView.this.getContext(), C0060z.layoutID("list_item_4"), null);
                inflate.setTag(new ListItem4ViewHolder(inflate));
                view2 = inflate;
            } else {
                view2 = view;
            }
            ListItem4ViewHolder listItem4ViewHolder = (ListItem4ViewHolder) view2.getTag();
            S s = (S) ChatRoomUserListView.this.ii.get(i);
            listItem4ViewHolder.dq.refreshWithCard(s);
            listItem4ViewHolder.cn.setText(s.getTitle());
            if (s.cW == A.bL.getUserID()) {
                textView = listItem4ViewHolder.dH;
                str = C0037c.getString("label_chatroom_user_list_you");
            } else {
                TextView textView2 = listItem4ViewHolder.dH;
                if (s.f0do) {
                    textView = textView2;
                    str = C0037c.getString("label_chatroom_user_list_friend");
                } else {
                    textView = textView2;
                    str = null;
                }
            }
            textView.setText(str);
            listItem4ViewHolder.js.setVisibility(8);
            return view2;
        }
    }

    public ChatRoomUserListView(Context context) {
        super(context);
        setCacheColorHint(-1);
        this.il = new a(this);
        setAdapter((ListAdapter) this.il);
        setOnItemClickListener(this);
    }

    public Delegate getDelegate() {
        if (this.hp == null) {
            return null;
        }
        return this.hp.get();
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        Delegate delegate = getDelegate();
        if (delegate != null) {
            delegate.onChatroomUserSelected(this, this.ii.get(i));
        }
    }

    public void refreshWithCard(Q q) {
        if (q != null) {
            this.ii = q.dl.toList();
            this.il.notifyDataSetChanged();
        }
    }

    public void setDelegate(Delegate delegate) {
        this.hp = new WeakReference<>(delegate);
    }
}
