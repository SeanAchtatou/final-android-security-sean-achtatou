package com.papaya.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.papaya.si.C0030bb;
import com.papaya.si.C0060z;

public class TabItem extends RelativeLayout {
    private TextView eL;
    private ImageView jR;
    private BadgeView kb;

    public TabItem(Context context) {
        super(context);
        setBackgroundResource(C0060z.drawableID("tab_indicator"));
        setPadding(0, 0, 0, 0);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setPadding(0, 0, 0, 0);
        linearLayout.setOrientation(1);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(14);
        layoutParams.addRule(12);
        layoutParams.bottomMargin = C0030bb.rp(3);
        addView(linearLayout, layoutParams);
        this.jR = new ImageView(context);
        this.jR.setPadding(0, 0, 0, 0);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(C0030bb.rp(40), C0030bb.rp(39));
        layoutParams2.gravity = 1;
        linearLayout.addView(this.jR, layoutParams2);
        this.eL = new TextView(context);
        this.eL.setPadding(0, 0, 0, 0);
        this.eL.setIncludeFontPadding(false);
        this.eL.setTextSize(12.0f);
        this.eL.setTypeface(Typeface.DEFAULT_BOLD);
        this.eL.setTextColor(Color.rgb(185, 185, 185));
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams3.gravity = 1;
        linearLayout.addView(this.eL, layoutParams3);
        this.kb = new BadgeView(context);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(C0030bb.rp(32), C0030bb.rp(30));
        layoutParams4.addRule(10);
        layoutParams4.addRule(11);
        addView(this.kb, layoutParams4);
    }

    public BadgeView getBadgeView() {
        return this.kb;
    }

    public ImageView getImageView() {
        return this.jR;
    }

    public TextView getTextView() {
        return this.eL;
    }

    public void setSelected(boolean z) {
        super.setSelected(z);
        this.jR.setSelected(z);
    }
}
