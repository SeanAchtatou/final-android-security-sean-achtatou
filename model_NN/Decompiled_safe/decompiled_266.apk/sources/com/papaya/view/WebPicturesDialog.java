package com.papaya.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.adknowledge.superrewards.model.SROffer;
import com.papaya.si.C0001a;
import com.papaya.si.C0026ay;
import com.papaya.si.C0030bb;
import com.papaya.si.C0034bf;
import com.papaya.si.C0037c;
import com.papaya.si.C0060z;
import com.papaya.si.aK;
import com.papaya.si.aU;
import com.papaya.si.aV;
import com.papaya.si.bk;
import com.papaya.si.bt;
import com.papaya.si.bv;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebPicturesDialog extends CustomDialog implements View.OnClickListener, bt.a {
    private JSONObject iT;
    private bk ia;
    /* access modifiers changed from: private */
    public ArrayList<bv> kp = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<Drawable> kq = new ArrayList<>();
    private GridView la;
    /* access modifiers changed from: private */
    public int lb;
    /* access modifiers changed from: private */
    public int lc;
    /* access modifiers changed from: private */
    public a ld;

    class a extends BaseAdapter {
        private Context lf;

        public a(Context context) {
            this.lf = context;
        }

        public final int getCount() {
            return WebPicturesDialog.this.kq.size();
        }

        public final Object getItem(int i) {
            return Integer.valueOf(i);
        }

        public final long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: android.widget.ImageButton} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: android.widget.ImageButton} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: android.widget.ImageButton} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final android.view.View getView(int r7, android.view.View r8, android.view.ViewGroup r9) {
            /*
                r6 = this;
                com.papaya.view.WebPicturesDialog r1 = com.papaya.view.WebPicturesDialog.this
                java.util.ArrayList r1 = r1.kq
                java.lang.Object r1 = r1.get(r7)
                android.graphics.drawable.Drawable r1 = (android.graphics.drawable.Drawable) r1
                if (r1 != 0) goto L_0x003c
                com.papaya.view.WebPicturesDialog r2 = com.papaya.view.WebPicturesDialog.this
                java.util.ArrayList r2 = r2.kp
                java.lang.Object r2 = r2.get(r7)
                if (r2 == 0) goto L_0x003c
                if (r8 == 0) goto L_0x0020
                boolean r1 = r8 instanceof android.widget.ProgressBar
                if (r1 != 0) goto L_0x007f
            L_0x0020:
                android.widget.ProgressBar r1 = new android.widget.ProgressBar
                android.content.Context r2 = r6.lf
                r1.<init>(r2)
                android.widget.AbsListView$LayoutParams r2 = new android.widget.AbsListView$LayoutParams
                com.papaya.view.WebPicturesDialog r3 = com.papaya.view.WebPicturesDialog.this
                int r3 = r3.lb
                com.papaya.view.WebPicturesDialog r4 = com.papaya.view.WebPicturesDialog.this
                int r4 = r4.lc
                r2.<init>(r3, r4)
                r1.setLayoutParams(r2)
            L_0x003b:
                return r1
            L_0x003c:
                if (r8 == 0) goto L_0x0042
                boolean r2 = r8 instanceof android.widget.ImageButton
                if (r2 != 0) goto L_0x007d
            L_0x0042:
                android.widget.ImageButton r2 = new android.widget.ImageButton
                android.content.Context r3 = r6.lf
                r2.<init>(r3)
                android.widget.AbsListView$LayoutParams r3 = new android.widget.AbsListView$LayoutParams
                com.papaya.view.WebPicturesDialog r4 = com.papaya.view.WebPicturesDialog.this
                int r4 = r4.lb
                com.papaya.view.WebPicturesDialog r5 = com.papaya.view.WebPicturesDialog.this
                int r5 = r5.lc
                r3.<init>(r4, r5)
                r2.setLayoutParams(r3)
                r3 = 0
                r2.setAdjustViewBounds(r3)
                android.widget.ImageView$ScaleType r3 = android.widget.ImageView.ScaleType.CENTER_CROP
                r2.setScaleType(r3)
                com.papaya.view.WebPicturesDialog r3 = com.papaya.view.WebPicturesDialog.this
                r2.setOnClickListener(r3)
                java.lang.Integer r3 = java.lang.Integer.valueOf(r7)
                r2.setTag(r3)
            L_0x0072:
                if (r1 == 0) goto L_0x007b
                r0 = r2
                android.widget.ImageButton r0 = (android.widget.ImageButton) r0
                r6 = r0
                r6.setImageDrawable(r1)
            L_0x007b:
                r1 = r2
                goto L_0x003b
            L_0x007d:
                r2 = r8
                goto L_0x0072
            L_0x007f:
                r1 = r8
                goto L_0x003b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.papaya.view.WebPicturesDialog.a.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
        }
    }

    public WebPicturesDialog(Context context, bk bkVar, String str) {
        super(context);
        this.ia = bkVar;
        this.iT = C0034bf.parseJsonObject(str);
        if (this.iT != null) {
            this.lb = C0034bf.getJsonInt(this.iT, "width", 120);
            this.lc = C0034bf.getJsonInt(this.iT, "height", 60);
            JSONArray jsonArray = C0034bf.getJsonArray(this.iT, "icons");
            C0026ay webCache = C0001a.getWebCache();
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    String jsonString = C0034bf.getJsonString(jsonArray, i);
                    bv bvVar = new bv();
                    bvVar.setDelegate(this);
                    aK fdFromPapayaUri = webCache.fdFromPapayaUri(jsonString, this.ia.getPapayaURL(), bvVar);
                    if (fdFromPapayaUri != null) {
                        this.kq.add(Drawable.createFromStream(fdFromPapayaUri.openInput(), SROffer.ICON));
                        this.kp.add(null);
                    } else {
                        this.kq.add(null);
                        this.kp.add(bvVar);
                        webCache.appendRequest(bvVar);
                    }
                }
            }
        }
        this.la = (GridView) LayoutInflater.from(context).inflate(C0060z.layoutID("picdlgview"), (ViewGroup) null);
        this.la.setNumColumns(-1);
        this.la.setColumnWidth(this.lb);
        this.la.setStretchMode(2);
        this.ld = new a(context);
        this.la.setAdapter((ListAdapter) this.ld);
        setView(this.la);
        setTitle(C0037c.getString("group_id"));
    }

    public void connectionFailed(final bt btVar, int i) {
        C0030bb.runInHandlerThread(new Runnable() {
            public final void run() {
                int indexOf = WebPicturesDialog.this.kp.indexOf(btVar.getRequest());
                if (indexOf != -1) {
                    WebPicturesDialog.this.kp.set(indexOf, null);
                    WebPicturesDialog.this.ld.notifyDataSetChanged();
                }
            }
        });
    }

    public void connectionFinished(final bt btVar) {
        C0030bb.runInHandlerThread(new Runnable() {
            public final void run() {
                int indexOf = WebPicturesDialog.this.kp.indexOf(btVar.getRequest());
                if (indexOf != -1) {
                    WebPicturesDialog.this.kp.set(indexOf, null);
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(btVar.getData());
                    try {
                        WebPicturesDialog.this.kq.set(indexOf, Drawable.createFromStream(byteArrayInputStream, SROffer.ICON));
                        WebPicturesDialog.this.ld.notifyDataSetChanged();
                    } finally {
                        aU.close(byteArrayInputStream);
                    }
                }
            }
        });
    }

    public void dismiss() {
        C0026ay webCache = C0001a.getWebCache();
        Iterator<bv> it = this.kp.iterator();
        while (it.hasNext()) {
            bv next = it.next();
            if (next != null) {
                webCache.removeRequest(next);
                next.setDelegate(null);
            }
        }
        this.kp.clear();
        this.kq.clear();
        super.dismiss();
    }

    public void onClick(View view) {
        String jsonString = C0034bf.getJsonString(this.iT, "action");
        if (jsonString != null) {
            JSONArray jsonArray = C0034bf.getJsonArray(this.iT, "icons");
            int intValue = ((Integer) view.getTag()).intValue();
            String jsonString2 = C0034bf.getJsonString(jsonArray, intValue);
            String contentUriFromPapayaUri = C0001a.getWebCache().contentUriFromPapayaUri(jsonString2, this.ia.getPapayaURL(), null);
            if (contentUriFromPapayaUri != null) {
                jsonString2 = contentUriFromPapayaUri;
            }
            this.ia.callJS(aV.format("%s(%d, '%s')", jsonString, Integer.valueOf(intValue), jsonString2));
        }
        dismiss();
    }
}
