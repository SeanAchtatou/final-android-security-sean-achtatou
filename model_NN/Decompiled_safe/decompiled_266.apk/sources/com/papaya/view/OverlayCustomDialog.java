package com.papaya.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.papaya.si.C0028b;
import com.papaya.si.C0030bb;
import com.papaya.si.C0056v;
import com.papaya.si.C0060z;
import com.papaya.si.X;
import com.papaya.si.aV;
import org.codehaus.jackson.Base64Variant;

public class OverlayCustomDialog extends FrameLayout implements View.OnClickListener {
    private float alpha = 0.95f;
    private ListView cC;
    private ImageView dK;
    private int gravity = 17;
    private boolean iG = true;
    private LinearLayout in;
    private LinearLayout io;
    private FrameLayout ip;
    private View iq;
    private LinearLayout ir;
    Button is;
    Button it;
    Button iu;
    DynamicTextView jA;
    private DynamicTextView jB;
    private OnClickListener jC;
    private OnClickListener jD;
    private OnClickListener jE;
    private View jy;
    private WindowManager jz;

    public static class Builder {
        private Drawable G;
        private CharSequence H = C0056v.bj;
        private Context cP;
        private CharSequence db;
        private int ey;
        private CharSequence iA;
        private CharSequence iB;
        private CharSequence iC;
        private boolean iG = true;
        private CharSequence[] iJ;
        private ListAdapter iL;
        private AdapterView.OnItemSelectedListener iM;
        private View iq;
        private OnClickListener jH;
        private OnClickListener jI;
        private OnClickListener jJ;
        private View.OnKeyListener jK;
        private OnClickListener jL;

        public Builder(Context context) {
            this.cP = context;
        }

        public OverlayCustomDialog create() {
            OverlayCustomDialog overlayCustomDialog = new OverlayCustomDialog(this.cP);
            overlayCustomDialog.jA.setText(this.H);
            if (this.ey != 0) {
                overlayCustomDialog.setIcon(this.ey);
            }
            if (this.G != null) {
                overlayCustomDialog.setIcon(this.G);
            }
            overlayCustomDialog.setView(this.iq);
            overlayCustomDialog.setMessage(this.db);
            if (this.iA != "") {
                overlayCustomDialog.setButton(-1, this.iA, this.jH);
            } else {
                overlayCustomDialog.is.setVisibility(8);
            }
            if (this.iB != "") {
                overlayCustomDialog.setButton(-2, this.iB, this.jI);
            } else {
                overlayCustomDialog.iu.setVisibility(8);
            }
            if (this.iC != "") {
                overlayCustomDialog.setButton(-3, this.iC, this.jJ);
            } else {
                overlayCustomDialog.it.setVisibility(8);
            }
            overlayCustomDialog.setCancelable(this.iG);
            if (this.jK != null) {
                overlayCustomDialog.setOnKeyListener(this.jK);
            }
            if (this.iL == null && this.iJ != null) {
                this.iL = new ArrayAdapter(this.cP, 17367057, 16908308, this.iJ);
            }
            if (this.iL != null) {
                overlayCustomDialog.initListView(this.iL, this.jL, this.iM);
            }
            return overlayCustomDialog;
        }

        public Builder setAdapter(ListAdapter listAdapter, OnClickListener onClickListener) {
            this.iL = listAdapter;
            this.jL = onClickListener;
            return this;
        }

        public Builder setCancelable(boolean z) {
            this.iG = z;
            return this;
        }

        public Builder setIcon(int i) {
            this.ey = i;
            return this;
        }

        public Builder setIcon(Drawable drawable) {
            this.G = drawable;
            return this;
        }

        public Builder setItems(int i, OnClickListener onClickListener) {
            this.iJ = this.cP.getResources().getTextArray(i);
            this.jL = onClickListener;
            return this;
        }

        public Builder setItems(CharSequence[] charSequenceArr, OnClickListener onClickListener) {
            this.iJ = charSequenceArr;
            this.jL = onClickListener;
            return this;
        }

        public Builder setMessage(int i) {
            this.db = this.cP.getText(i);
            return this;
        }

        public Builder setMessage(CharSequence charSequence) {
            this.db = charSequence;
            return this;
        }

        public Builder setNegativeButton(int i, OnClickListener onClickListener) {
            this.iB = this.cP.getText(i);
            this.jI = onClickListener;
            return this;
        }

        public Builder setNegativeButton(CharSequence charSequence, OnClickListener onClickListener) {
            this.iB = charSequence;
            this.jI = onClickListener;
            return this;
        }

        public Builder setNeutralButton(int i, OnClickListener onClickListener) {
            this.iC = this.cP.getText(i);
            this.jJ = onClickListener;
            return this;
        }

        public Builder setNeutralButton(CharSequence charSequence, OnClickListener onClickListener) {
            this.iC = charSequence;
            this.jJ = onClickListener;
            return this;
        }

        public Builder setOnItemSelectedListener(AdapterView.OnItemSelectedListener onItemSelectedListener) {
            this.iM = onItemSelectedListener;
            return this;
        }

        public Builder setOnKeyListener(View.OnKeyListener onKeyListener) {
            this.jK = onKeyListener;
            return this;
        }

        public Builder setPositiveButton(int i, OnClickListener onClickListener) {
            this.iA = this.cP.getText(i);
            this.jH = onClickListener;
            return this;
        }

        public Builder setPositiveButton(CharSequence charSequence, OnClickListener onClickListener) {
            this.iA = charSequence;
            this.jH = onClickListener;
            return this;
        }

        public Builder setTitle(int i) {
            this.H = this.cP.getText(i);
            return this;
        }

        public Builder setTitle(CharSequence charSequence) {
            this.H = charSequence;
            return this;
        }

        public Builder setView(View view) {
            this.iq = view;
            return this;
        }

        public OverlayCustomDialog show() {
            OverlayCustomDialog create = create();
            create.show();
            return create;
        }
    }

    public interface OnClickListener {
        void onClick(OverlayCustomDialog overlayCustomDialog, int i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, com.papaya.view.OverlayCustomDialog, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public OverlayCustomDialog(Context context) {
        super(context);
        this.jz = C0030bb.getWindowManager(context);
        this.jy = getLayoutInflater().inflate(C0060z.layoutID("custom_dialog"), (ViewGroup) this, true);
        this.in = (LinearLayout) f("dialog_title_content");
        this.dK = (ImageView) f("dialog_icon");
        this.jA = (DynamicTextView) f("dialog_title");
        this.io = (LinearLayout) f("dialog_content");
        this.jB = (DynamicTextView) f("dialog_message");
        this.jB.setMovementMethod(new ScrollingMovementMethod());
        this.ip = (FrameLayout) f("dialog_custom_content");
        this.ir = (LinearLayout) f("dialog_button_content");
        this.is = (Button) f("dialog_button_positive");
        this.it = (Button) f("dialog_button_neutral");
        this.iu = (Button) f("dialog_button_negative");
        this.is.setOnClickListener(this);
        this.it.setOnClickListener(this);
        this.iu.setOnClickListener(this);
    }

    private void dismiss() {
        hide();
    }

    private <T> T f(String str) {
        T findViewById = findViewById(C0060z.id(str));
        if (findViewById == null) {
            X.w("can't find view with id %s", str);
        }
        return findViewById;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (super.dispatchKeyEvent(keyEvent)) {
            return true;
        }
        return keyEvent.dispatch(this);
    }

    public Button getButton(int i) {
        switch (i) {
            case -3:
                return this.it;
            case Base64Variant.BASE64_VALUE_PADDING /*-2*/:
                return this.iu;
            case -1:
                return this.is;
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public int getDefaultVisibility() {
        return C0028b.getActiveActivity() != null ? 0 : 8;
    }

    /* access modifiers changed from: protected */
    public LayoutInflater getLayoutInflater() {
        return LayoutInflater.from(getContext());
    }

    public ListView getListView() {
        return this.cC;
    }

    public DynamicTextView getMessageView() {
        return this.jB;
    }

    public DynamicTextView getTitleView() {
        return this.jA;
    }

    public void hide() {
        try {
            setVisibility(8);
            if (this.jz != null && getParent() != null) {
                this.jz.removeView(this);
            }
        } catch (Exception e) {
        }
    }

    public void hideLoading() {
        this.is.setEnabled(true);
        this.it.setEnabled(true);
        this.iu.setEnabled(true);
    }

    /* access modifiers changed from: package-private */
    public void initListView(ListAdapter listAdapter, final OnClickListener onClickListener, AdapterView.OnItemSelectedListener onItemSelectedListener) {
        this.cC = (ListView) getLayoutInflater().inflate(C0060z.layoutID("list_dialog"), (ViewGroup) null);
        this.cC.setAdapter(listAdapter);
        if (onItemSelectedListener != null) {
            this.cC.setOnItemSelectedListener(onItemSelectedListener);
        } else if (onClickListener != null) {
            this.cC.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    OverlayCustomDialog.this.hide();
                    onClickListener.onClick(OverlayCustomDialog.this, i);
                }
            });
        }
        this.io.removeAllViews();
        this.io.addView(this.cC);
    }

    public boolean isCancelable() {
        return this.iG;
    }

    public boolean isLoading() {
        return !this.is.isEnabled();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        try {
            super.onAttachedToWindow();
            if (this.jy != null && this.jz != null && getParent() != null) {
                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                layoutParams.width = -2;
                layoutParams.height = -2;
                layoutParams.gravity = this.gravity;
                layoutParams.format = -3;
                layoutParams.flags = 2;
                layoutParams.dimAmount = 0.4f;
                layoutParams.alpha = this.alpha;
                this.jz.updateViewLayout(this, layoutParams);
            }
        } catch (Exception e) {
        }
    }

    public void onClick(View view) {
        hide();
        if (view == this.is) {
            if (this.jC != null) {
                this.jC.onClick(this, -1);
            }
        } else if (view == this.iu) {
            if (this.jE != null) {
                this.jE.onClick(this, -2);
            }
        } else if (view == this.it && this.jD != null) {
            this.jD.onClick(this, -3);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || isLoading() || !this.iG) {
            return super.onKeyDown(i, keyEvent);
        }
        if (this.jE != null) {
            this.jE.onClick(this, -2);
        }
        hide();
        return true;
    }

    public void setButton(int i, CharSequence charSequence, OnClickListener onClickListener) {
        Button button = getButton(i);
        if (button != null) {
            button.setText(charSequence);
            switch (i) {
                case -3:
                    this.jD = onClickListener;
                    return;
                case Base64Variant.BASE64_VALUE_PADDING /*-2*/:
                    this.jE = onClickListener;
                    return;
                case -1:
                    this.jC = onClickListener;
                    return;
                default:
                    return;
            }
        }
    }

    public void setCancelable(boolean z) {
        this.iG = z;
    }

    public void setIcon(int i) {
        this.dK.setImageResource(i);
    }

    public void setIcon(Drawable drawable) {
        this.dK.setImageDrawable(drawable);
    }

    public void setMessage(int i) {
        getMessageView().setText(i);
    }

    public void setMessage(CharSequence charSequence) {
        getMessageView().setText(charSequence);
    }

    public void setNegativeButton(CharSequence charSequence, OnClickListener onClickListener) {
        setButton(-2, charSequence, onClickListener);
    }

    public void setNeutralButton(CharSequence charSequence, OnClickListener onClickListener) {
        setButton(-3, charSequence, onClickListener);
    }

    public void setPositiveButton(CharSequence charSequence, OnClickListener onClickListener) {
        setButton(-1, charSequence, onClickListener);
    }

    public void setTitle(int i) {
        this.jA.setText(i);
    }

    public void setTitle(CharSequence charSequence) {
        this.jA.setText(charSequence);
    }

    public void setTitleBgRes(int i) {
        this.in.setBackgroundResource(i);
    }

    public void setTitleColor(int i) {
        this.jA.setTextColor(i);
    }

    public void setView(View view) {
        this.iq = view;
        this.ip.removeAllViews();
        if (this.iq != null) {
            this.ip.addView(this.iq);
        }
    }

    public void show() {
        show(null);
    }

    public void show(Context context) {
        updateVisibility();
        setVisibility(getDefaultVisibility());
        try {
            if (getParent() != null) {
                this.jz.removeView(this);
            }
        } catch (Exception e) {
            X.e(e, "Failed to removeView in show", new Object[0]);
        }
        if (context != null) {
            this.jz = C0030bb.getWindowManager(context);
        }
        try {
            if (this.jz != null) {
                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                layoutParams.width = -2;
                layoutParams.height = -2;
                layoutParams.verticalMargin = 0.08f;
                layoutParams.alpha = this.alpha;
                layoutParams.gravity = this.gravity;
                layoutParams.format = -3;
                layoutParams.type = 2005;
                layoutParams.windowAnimations = 16973828;
                this.jz.addView(this, layoutParams);
                return;
            }
            X.w("wm is null", new Object[0]);
        } catch (Exception e2) {
            X.w(e2, "Failed to showInContext", new Object[0]);
        }
    }

    public void showLoading() {
        this.is.setEnabled(false);
        this.it.setEnabled(false);
        this.iu.setEnabled(false);
    }

    /* access modifiers changed from: package-private */
    public void updateVisibility() {
        if (this.iq != null) {
            this.ip.setVisibility(0);
            this.io.setVisibility(8);
        } else {
            this.ip.setVisibility(8);
            if (!aV.isEmpty(this.jB.getText()) || this.cC != null) {
                this.io.setVisibility(0);
            } else {
                this.io.setVisibility(8);
            }
        }
        if (!aV.isEmpty(this.is.getText()) || !aV.isEmpty(this.it.getText()) || !aV.isEmpty(this.iu.getText())) {
            this.ir.setVisibility(0);
            this.is.setVisibility(aV.isEmpty(this.is.getText()) ? 8 : 0);
            this.iu.setVisibility(aV.isEmpty(this.iu.getText()) ? 8 : 0);
            this.it.setVisibility(aV.isEmpty(this.it.getText()) ? 8 : 0);
            return;
        }
        this.ir.setVisibility(8);
    }
}
