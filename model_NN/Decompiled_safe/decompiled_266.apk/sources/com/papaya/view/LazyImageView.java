package com.papaya.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import com.papaya.si.C0001a;
import com.papaya.si.C0030bb;
import com.papaya.si.C0034bf;
import com.papaya.si.X;
import com.papaya.si.aW;
import com.papaya.si.bt;
import com.papaya.si.bv;
import com.papaya.si.by;
import java.net.URL;

public class LazyImageView extends ImageView implements aW, bt.a {
    public static final ColorMatrixColorFilter GREYSCALE_FILTER = new ColorMatrixColorFilter(new float[]{0.3f, 0.59f, 0.11f, 0.0f, 0.0f, 0.3f, 0.59f, 0.11f, 0.0f, 0.0f, 0.3f, 0.59f, 0.11f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f});
    private String dc;
    private bv jn;
    private Drawable jo;
    private boolean jp = false;
    private int jq = 1;
    private boolean jr = false;

    public LazyImageView(Context context) {
        super(context);
        setScaleType(ImageView.ScaleType.FIT_CENTER);
    }

    public LazyImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setScaleType(ImageView.ScaleType.FIT_CENTER);
    }

    public LazyImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setScaleType(ImageView.ScaleType.FIT_CENTER);
    }

    public void connectionFailed(bt btVar, int i) {
        this.jn = null;
    }

    public void connectionFinished(bt btVar) {
        if (btVar.getRequest() == this.jn) {
            try {
                this.jn = null;
                setBitmapWithAnimation(btVar.getBitmap());
            } catch (Exception e) {
                X.w("Failed to execute bitmap callback: %s", e);
            }
        }
    }

    public String getImageUrl() {
        return this.dc;
    }

    public int getMaxAnimationCount() {
        return this.jq;
    }

    public boolean isGrayscaled() {
        return this.jr;
    }

    public boolean isRoundedCorner() {
        return this.jp;
    }

    /* access modifiers changed from: protected */
    public void onAnimationEnd() {
        super.onAnimationEnd();
        setAnimation(null);
    }

    /* access modifiers changed from: protected */
    public void setBitmapWithAnimation(Bitmap bitmap) {
        setImageDrawable(new BitmapDrawable(bitmap));
        if (this.jq != 0) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(667);
            startAnimation(alphaAnimation);
            if (this.jq > 0) {
                this.jq--;
            }
        }
    }

    public void setDefaultDrawable(Drawable drawable) {
        if (getDrawable() == null || getDrawable() == this.jo) {
            setImageDrawable(drawable);
        }
        this.jo = drawable;
    }

    public void setGrayScaled(boolean z) {
        this.jr = z;
        if (z) {
            setColorFilter(GREYSCALE_FILTER);
        } else {
            setColorFilter((ColorFilter) null);
        }
    }

    public void setImageUrl(String str) {
        try {
            if (this.dc == null || !this.dc.equals(str)) {
                this.dc = str;
                setImageDrawable(this.jo);
                if (this.jn != null) {
                    this.jn.cancel();
                    this.jn = null;
                }
                if (str != null && str.length() > 0) {
                    if (by.isContentUrl(str)) {
                        setBitmapWithAnimation(C0030bb.bitmapFromFD(C0001a.getWebCache().fdFromContentUrl(str)));
                        return;
                    }
                    URL createURL = C0034bf.createURL(str);
                    if (createURL != null) {
                        Bitmap cachedBitmap = bt.getCachedBitmap(createURL);
                        if (cachedBitmap != null) {
                            setBitmapWithAnimation(cachedBitmap);
                            return;
                        }
                        this.jn = new bv(createURL, true);
                        this.jn.setRequireSid(false);
                        this.jn.setDelegate(this);
                        this.jn.start(false);
                    }
                }
            }
        } catch (Exception e) {
            X.w("Failed to setImageUrl: %s", e);
        }
    }

    public void setMaxAnimationCount(int i) {
        this.jq = i;
    }

    public void setRoundedCorner(boolean z) {
        this.jp = z;
    }
}
