package com.papaya.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.papaya.si.C0056v;
import com.papaya.si.C0060z;
import com.papaya.si.X;
import com.papaya.si.aV;
import org.codehaus.jackson.Base64Variant;

public class CustomDialog extends Dialog implements DialogInterface, View.OnClickListener {
    private ListView cC;
    TextView cn = ((TextView) f("dialog_title"));
    private ImageView dK = ((ImageView) f("dialog_icon"));
    private TextView dr = ((TextView) f("dialog_message"));
    private LinearLayout in = ((LinearLayout) f("dialog_title_content"));
    private LinearLayout io = ((LinearLayout) f("dialog_content"));
    FrameLayout ip;
    private View iq;
    private LinearLayout ir;
    Button is;
    Button it;
    Button iu;
    private DialogInterface.OnClickListener iv;
    private DialogInterface.OnClickListener iw;
    private DialogInterface.OnClickListener ix;

    public static class Builder {
        private Drawable G;
        private CharSequence H = C0056v.bj;
        private Context cP;
        private CharSequence db;
        private int ey;
        private CharSequence iA;
        private CharSequence iB;
        private CharSequence iC;
        private DialogInterface.OnClickListener iD;
        private DialogInterface.OnClickListener iE;
        private DialogInterface.OnClickListener iF;
        private boolean iG = true;
        private DialogInterface.OnCancelListener iH;
        private DialogInterface.OnKeyListener iI;
        private CharSequence[] iJ;
        private DialogInterface.OnClickListener iK;
        private ListAdapter iL;
        private AdapterView.OnItemSelectedListener iM;
        private View iq;

        public Builder(Context context) {
            this.cP = context;
        }

        public CustomDialog create() {
            CustomDialog customDialog = new CustomDialog(this.cP);
            customDialog.cn.setText(this.H);
            if (this.ey != 0) {
                customDialog.setIcon(this.ey);
            }
            if (this.G != null) {
                customDialog.setIcon(this.G);
            }
            customDialog.setView(this.iq);
            customDialog.setMessage(this.db);
            if (this.iA != "") {
                customDialog.setButton(-1, this.iA, this.iD);
            } else {
                customDialog.is.setVisibility(8);
            }
            if (this.iB != "") {
                customDialog.setButton(-2, this.iB, this.iE);
            } else {
                customDialog.iu.setVisibility(8);
            }
            if (this.iC != "") {
                customDialog.setButton(-3, this.iC, this.iF);
            } else {
                customDialog.it.setVisibility(8);
            }
            customDialog.setCancelable(this.iG);
            customDialog.setOnCancelListener(this.iH);
            if (this.iI != null) {
                customDialog.setOnKeyListener(this.iI);
            }
            if (this.iL == null && this.iJ != null) {
                this.iL = new ArrayAdapter(this.cP, 17367057, 16908308, this.iJ);
            }
            if (this.iL != null) {
                customDialog.initListView(this.iL, this.iK, this.iM);
            }
            return customDialog;
        }

        public Builder setAdapter(ListAdapter listAdapter, DialogInterface.OnClickListener onClickListener) {
            this.iL = listAdapter;
            this.iK = onClickListener;
            return this;
        }

        public Builder setCancelable(boolean z) {
            this.iG = z;
            return this;
        }

        public Builder setIcon(int i) {
            this.ey = i;
            return this;
        }

        public Builder setIcon(Drawable drawable) {
            this.G = drawable;
            return this;
        }

        public Builder setItems(int i, DialogInterface.OnClickListener onClickListener) {
            this.iJ = this.cP.getResources().getTextArray(i);
            this.iK = onClickListener;
            return this;
        }

        public Builder setItems(CharSequence[] charSequenceArr, DialogInterface.OnClickListener onClickListener) {
            this.iJ = charSequenceArr;
            this.iK = onClickListener;
            return this;
        }

        public Builder setMessage(int i) {
            this.db = this.cP.getText(i);
            return this;
        }

        public Builder setMessage(CharSequence charSequence) {
            this.db = charSequence;
            return this;
        }

        public Builder setNegativeButton(int i, DialogInterface.OnClickListener onClickListener) {
            this.iB = this.cP.getText(i);
            this.iE = onClickListener;
            return this;
        }

        public Builder setNegativeButton(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            this.iB = charSequence;
            this.iE = onClickListener;
            return this;
        }

        public Builder setNeutralButton(int i, DialogInterface.OnClickListener onClickListener) {
            this.iC = this.cP.getText(i);
            this.iF = onClickListener;
            return this;
        }

        public Builder setNeutralButton(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            this.iC = charSequence;
            this.iF = onClickListener;
            return this;
        }

        public Builder setOnCancelListener(DialogInterface.OnCancelListener onCancelListener) {
            this.iH = onCancelListener;
            return this;
        }

        public Builder setOnItemSelectedListener(AdapterView.OnItemSelectedListener onItemSelectedListener) {
            this.iM = onItemSelectedListener;
            return this;
        }

        public Builder setOnKeyListener(DialogInterface.OnKeyListener onKeyListener) {
            this.iI = onKeyListener;
            return this;
        }

        public Builder setPositiveButton(int i, DialogInterface.OnClickListener onClickListener) {
            this.iA = this.cP.getText(i);
            this.iD = onClickListener;
            return this;
        }

        public Builder setPositiveButton(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            this.iA = charSequence;
            this.iD = onClickListener;
            return this;
        }

        public Builder setTitle(int i) {
            this.H = this.cP.getText(i);
            return this;
        }

        public Builder setTitle(CharSequence charSequence) {
            this.H = charSequence;
            return this;
        }

        public Builder setView(View view) {
            this.iq = view;
            return this;
        }

        public CustomDialog show() {
            CustomDialog create = create();
            create.show();
            return create;
        }
    }

    public CustomDialog(Context context) {
        super(context, C0060z.styleID("DialogTheme"));
        requestWindowFeature(1);
        setContentView(C0060z.layoutID("custom_dialog"));
        this.dr.setMovementMethod(new ScrollingMovementMethod());
        this.ip = (FrameLayout) f("dialog_custom_content");
        this.ir = (LinearLayout) f("dialog_button_content");
        this.is = (Button) f("dialog_button_positive");
        this.it = (Button) f("dialog_button_neutral");
        this.iu = (Button) f("dialog_button_negative");
        this.is.setOnClickListener(this);
        this.it.setOnClickListener(this);
        this.iu.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public <T> T f(String str) {
        T findViewById = findViewById(C0060z.id(str));
        if (findViewById == null) {
            X.w("can't find view with id %s", str);
        }
        return findViewById;
    }

    public Button getButton(int i) {
        switch (i) {
            case -3:
                return this.it;
            case Base64Variant.BASE64_VALUE_PADDING /*-2*/:
                return this.iu;
            case -1:
                return this.is;
            default:
                return null;
        }
    }

    public View getCustomView() {
        return this.iq;
    }

    public ListView getListView() {
        return this.cC;
    }

    /* access modifiers changed from: package-private */
    public void initListView(ListAdapter listAdapter, final DialogInterface.OnClickListener onClickListener, AdapterView.OnItemSelectedListener onItemSelectedListener) {
        this.cC = (ListView) getLayoutInflater().inflate(C0060z.layoutID("list_dialog"), (ViewGroup) null);
        this.cC.setAdapter(listAdapter);
        if (onItemSelectedListener != null) {
            this.cC.setOnItemSelectedListener(onItemSelectedListener);
        } else if (onClickListener != null) {
            this.cC.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    CustomDialog.this.dismiss();
                    onClickListener.onClick(CustomDialog.this, i);
                }
            });
        }
        this.io.removeAllViews();
        this.io.addView(this.cC);
    }

    public void onClick(View view) {
        dismiss();
        if (view == this.is) {
            if (this.iv != null) {
                this.iv.onClick(this, -1);
            }
        } else if (view == this.iu) {
            if (this.ix != null) {
                this.ix.onClick(this, -2);
            }
        } else if (view == this.it && this.iw != null) {
            this.iw.onClick(this, -3);
        }
    }

    public void setButton(int i, CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        Button button = getButton(i);
        if (button != null) {
            button.setText(charSequence);
            switch (i) {
                case -3:
                    this.iw = onClickListener;
                    return;
                case Base64Variant.BASE64_VALUE_PADDING /*-2*/:
                    this.ix = onClickListener;
                    return;
                case -1:
                    this.iv = onClickListener;
                    return;
                default:
                    return;
            }
        }
    }

    public void setIcon(int i) {
        this.dK.setImageResource(i);
    }

    public void setIcon(Drawable drawable) {
        this.dK.setImageDrawable(drawable);
    }

    public void setMessage(CharSequence charSequence) {
        this.dr.setText(charSequence);
    }

    public void setNegativeButton(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        setButton(-2, charSequence, onClickListener);
    }

    public void setNeutralButton(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        setButton(-3, charSequence, onClickListener);
    }

    public void setPositiveButton(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        setButton(-1, charSequence, onClickListener);
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        this.cn.setText(charSequence);
    }

    public void setTitleBgRes(int i) {
        this.in.setBackgroundResource(i);
    }

    public void setTitleColor(int i) {
        this.cn.setTextColor(i);
    }

    public void setView(View view) {
        this.iq = view;
        this.ip.removeAllViews();
        if (this.iq != null) {
            this.ip.addView(this.iq);
        }
    }

    public void show() {
        updateVisibility();
        super.show();
    }

    /* access modifiers changed from: package-private */
    public void updateVisibility() {
        if (this.iq != null) {
            this.ip.setVisibility(0);
            this.io.setVisibility(8);
        } else {
            this.ip.setVisibility(8);
            if (!aV.isEmpty(this.dr.getText()) || this.cC != null) {
                this.io.setVisibility(0);
            } else {
                this.io.setVisibility(8);
            }
        }
        if (!aV.isEmpty(this.is.getText()) || !aV.isEmpty(this.it.getText()) || !aV.isEmpty(this.iu.getText())) {
            this.ir.setVisibility(0);
            this.is.setVisibility(aV.isEmpty(this.is.getText()) ? 8 : 0);
            this.iu.setVisibility(aV.isEmpty(this.iu.getText()) ? 8 : 0);
            this.it.setVisibility(aV.isEmpty(this.it.getText()) ? 8 : 0);
            return;
        }
        this.ir.setVisibility(8);
    }
}
