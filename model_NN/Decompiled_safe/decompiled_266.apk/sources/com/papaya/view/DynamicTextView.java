package com.papaya.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import com.papaya.si.aL;

public class DynamicTextView extends TextView {
    /* access modifiers changed from: private */
    public boolean iN = false;
    /* access modifiers changed from: private */
    public CharSequence[] iO;
    private int iP;
    private a iQ;
    /* access modifiers changed from: private */
    public int index;

    class a extends aL {
        /* synthetic */ a(DynamicTextView dynamicTextView) {
            this((byte) 0);
        }

        private a(byte b) {
        }

        /* access modifiers changed from: protected */
        public final void runTask() {
            if (DynamicTextView.this.iO != null) {
                DynamicTextView.access$108(DynamicTextView.this);
                if (DynamicTextView.this.index < DynamicTextView.this.iO.length) {
                    DynamicTextView.this.setText2(DynamicTextView.this.iO[DynamicTextView.this.index]);
                    boolean unused = DynamicTextView.this.iN = false;
                }
            }
        }
    }

    public DynamicTextView(Context context) {
        super(context);
    }

    public DynamicTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public DynamicTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    static /* synthetic */ int access$108(DynamicTextView dynamicTextView) {
        int i = dynamicTextView.index;
        dynamicTextView.index = i + 1;
        return i;
    }

    private void postShowMessage() {
        stopMessageTask();
        this.iQ = new a(this);
        postDelayed(this.iQ, (long) this.iP);
    }

    /* access modifiers changed from: private */
    public void setText2(CharSequence charSequence) {
        this.iN = true;
        setText(charSequence);
        this.iN = false;
    }

    private void stopMessageTask() {
        if (this.iQ != null) {
            this.iQ.eX = true;
            this.iQ = null;
        }
    }

    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        super.setText(charSequence, bufferType);
        if (!this.iN) {
            stopMessageTask();
            this.iO = null;
            this.index = 0;
        }
    }

    public void setTexts(int i, int... iArr) {
        this.iO = new CharSequence[iArr.length];
        for (int i2 = 0; i2 < iArr.length; i2++) {
            this.iO[i2] = getContext().getResources().getString(iArr[i2]);
        }
        setTexts(i, this.iO);
    }

    public void setTexts(int i, CharSequence... charSequenceArr) {
        this.iO = charSequenceArr;
        this.iP = i;
        stopMessageTask();
        if (this.iO.length > 0) {
            setText2(this.iO[0]);
            if (this.iO.length > 1) {
                postShowMessage();
            }
        }
    }
}
