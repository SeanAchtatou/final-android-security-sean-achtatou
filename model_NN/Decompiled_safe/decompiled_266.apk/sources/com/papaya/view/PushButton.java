package com.papaya.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.StateListDrawable;
import android.widget.TextView;

public class PushButton extends TextView {
    private static final int[] jU = {16842912};
    private boolean jT = false;

    public PushButton(Context context, int i, int i2) {
        super(context);
        StateListDrawable stateListDrawable = new StateListDrawable();
        Resources resources = getContext().getResources();
        stateListDrawable.addState(new int[]{16842919}, resources.getDrawable(i2));
        stateListDrawable.addState(new int[]{16842912}, resources.getDrawable(i2));
        stateListDrawable.addState(new int[0], resources.getDrawable(i));
        setBackgroundDrawable(stateListDrawable);
    }

    public boolean isSelected() {
        return this.jT;
    }

    /* access modifiers changed from: protected */
    public int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 1);
        if (this.jT) {
            mergeDrawableStates(onCreateDrawableState, jU);
        }
        return onCreateDrawableState;
    }

    public void setSelected(boolean z) {
        if (this.jT != z) {
            this.jT = z;
            refreshDrawableState();
        }
    }
}
