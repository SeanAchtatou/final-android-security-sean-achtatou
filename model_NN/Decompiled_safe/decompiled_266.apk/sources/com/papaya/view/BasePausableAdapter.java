package com.papaya.view;

import android.widget.BaseAdapter;

public abstract class BasePausableAdapter extends BaseAdapter implements PausableAdapter {
    private boolean bq;

    public boolean isPaused() {
        return this.bq;
    }

    public void notifyDataSetChanged() {
        if (!this.bq) {
            super.notifyDataSetChanged();
        }
    }

    public void setPaused(boolean z) {
        this.bq = z;
        if (!z) {
            notifyDataSetChanged();
        }
    }
}
