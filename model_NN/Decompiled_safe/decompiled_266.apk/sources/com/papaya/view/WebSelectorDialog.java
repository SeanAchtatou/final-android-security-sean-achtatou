package com.papaya.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.adknowledge.superrewards.model.SROffer;
import com.papaya.si.C0001a;
import com.papaya.si.C0026ay;
import com.papaya.si.C0030bb;
import com.papaya.si.C0034bf;
import com.papaya.si.C0060z;
import com.papaya.si.aK;
import com.papaya.si.aM;
import com.papaya.si.aU;
import com.papaya.si.aV;
import com.papaya.si.bk;
import com.papaya.si.bt;
import com.papaya.si.bv;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebSelectorDialog extends CustomDialog implements AdapterView.OnItemClickListener, aM, bt.a, JsonConfigurable {
    private JSONObject iT;
    private bk ia;
    private String ja;
    /* access modifiers changed from: private */
    public ArrayList<bv> kp = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<Drawable> kq = new ArrayList<>();
    /* access modifiers changed from: private */
    public JSONArray kr;
    private ListView ks;
    /* access modifiers changed from: private */
    public a lg;

    class a extends BaseAdapter {
        private LayoutInflater kw;

        /* synthetic */ a(WebSelectorDialog webSelectorDialog, LayoutInflater layoutInflater) {
            this(layoutInflater, (byte) 0);
        }

        private a(LayoutInflater layoutInflater, byte b) {
            this.kw = layoutInflater;
        }

        public final int getCount() {
            if (WebSelectorDialog.this.kr == null) {
                return 0;
            }
            return WebSelectorDialog.this.kr.length();
        }

        public final Object getItem(int i) {
            return Integer.valueOf(i);
        }

        public final long getItemId(int i) {
            return (long) i;
        }

        public final View getView(int i, View view, ViewGroup viewGroup) {
            b bVar;
            View view2;
            if (view == null) {
                View inflate = this.kw.inflate(C0060z.layoutID("list_item_3_part_inverse"), (ViewGroup) null);
                b bVar2 = new b();
                bVar2.kx = (ImageView) inflate.findViewById(C0060z.id("list_item_3_header"));
                bVar2.ky = (TextView) inflate.findViewById(C0060z.id("list_item_3_content"));
                bVar2.kz = (ImageView) inflate.findViewById(C0060z.id("list_item_3_accessory"));
                inflate.setTag(bVar2);
                b bVar3 = bVar2;
                view2 = inflate;
                bVar = bVar3;
            } else {
                bVar = (b) view.getTag();
                view2 = view;
            }
            JSONObject jsonObject = C0034bf.getJsonObject(WebSelectorDialog.this.kr, i);
            bVar.ky.setText(C0034bf.getJsonString(jsonObject, "text"));
            Drawable drawable = (Drawable) WebSelectorDialog.this.kq.get(i);
            if (drawable != null) {
                bVar.kx.setImageDrawable(drawable);
                bVar.kx.setVisibility(0);
                bVar.kx.setBackgroundColor(0);
            } else {
                bVar.kx.setVisibility(4);
            }
            if (aV.intValue(C0034bf.getJsonString(jsonObject, "selected"), -1) == 1) {
                bVar.kz.setVisibility(0);
                bVar.kz.setImageDrawable(this.kw.getContext().getResources().getDrawable(C0060z.drawableID("ic_check_mark_light")));
                bVar.kz.setBackgroundColor(0);
            } else {
                bVar.kz.setVisibility(4);
            }
            return view2;
        }
    }

    static class b {
        ImageView kx;
        TextView ky;
        ImageView kz;

        /* synthetic */ b() {
            this((byte) 0);
        }

        private b(byte b) {
        }
    }

    public WebSelectorDialog(Context context) {
        super(context);
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
        this.ks = (ListView) layoutInflater.inflate(C0060z.layoutID("list_dialog"), (ViewGroup) null);
        this.lg = new a(this, layoutInflater);
        this.ks.setAdapter((ListAdapter) this.lg);
        this.ks.setBackgroundResource(17170447);
        this.ks.setOnItemClickListener(this);
        setView(this.ks);
    }

    public void clear() {
        C0026ay webCache = C0001a.getWebCache();
        Iterator<bv> it = this.kp.iterator();
        while (it.hasNext()) {
            bv next = it.next();
            if (next != null) {
                webCache.removeRequest(next);
                next.setDelegate(null);
            }
        }
        this.kp.clear();
        this.kq.clear();
    }

    public void connectionFailed(final bt btVar, int i) {
        C0030bb.post(new Runnable() {
            public final void run() {
                int indexOf = WebSelectorDialog.this.kp.indexOf(btVar.getRequest());
                if (indexOf != -1) {
                    WebSelectorDialog.this.kp.set(indexOf, null);
                }
            }
        });
    }

    public void connectionFinished(final bt btVar) {
        C0030bb.post(new Runnable() {
            public final void run() {
                int indexOf = WebSelectorDialog.this.kp.indexOf(btVar.getRequest());
                if (indexOf != -1) {
                    WebSelectorDialog.this.kp.set(indexOf, null);
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(btVar.getData());
                    try {
                        WebSelectorDialog.this.kq.set(indexOf, Drawable.createFromStream(byteArrayInputStream, SROffer.ICON));
                        WebSelectorDialog.this.lg.notifyDataSetChanged();
                    } finally {
                        aU.close(byteArrayInputStream);
                    }
                }
            }
        });
    }

    public String getViewId() {
        return this.ja;
    }

    public bk getWebView() {
        return this.ia;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        JSONObject jsonObject = C0034bf.getJsonObject(this.kr, i);
        if (this.ia != null) {
            String jsonString = C0034bf.getJsonString(this.iT, "action");
            if (!aV.isEmpty(jsonString)) {
                Object jsonValue = C0034bf.getJsonValue(jsonObject, "value");
                if (jsonValue == null) {
                    this.ia.callJSFunc("%s(%d)", jsonString, Integer.valueOf(i));
                } else if (jsonValue instanceof Number) {
                    this.ia.callJSFunc("%s(%s)", jsonString, jsonValue);
                } else {
                    this.ia.callJSFunc("%s('%s')", jsonString, C0034bf.escapeJS(jsonValue.toString()));
                }
            } else {
                Object jsonValue2 = C0034bf.getJsonValue(jsonObject, "value");
                if (jsonValue2 == null) {
                    this.ia.callJSFunc("onSelectorDialogTapped('%s', %d)", this.ja, Integer.valueOf(i));
                } else if (jsonValue2 instanceof Number) {
                    this.ia.callJSFunc("onSelectorDialogTapped('%s', %d, %s)", this.ja, Integer.valueOf(i), jsonValue2);
                } else {
                    this.ia.callJSFunc("onSelectorDialogTapped('%s', %d, '%s')", this.ja, Integer.valueOf(i), C0034bf.escapeJS(jsonValue2.toString()));
                }
            }
        }
    }

    public void refreshWithCtx(JSONObject jSONObject) {
        String jsonString;
        this.iT = jSONObject;
        String jsonString2 = C0034bf.getJsonString(this.iT, SROffer.TITLE);
        if (jsonString2 == null) {
            jsonString2 = getContext().getString(C0060z.stringID("web_selector_title"));
        }
        setTitle(jsonString2);
        clear();
        this.kr = C0034bf.getJsonArray(this.iT, "options");
        URL papayaURL = this.ia.getPapayaURL();
        if (this.kr != null) {
            C0026ay webCache = C0001a.getWebCache();
            for (int i = 0; i < this.kr.length(); i++) {
                this.kq.add(null);
                this.kp.add(null);
                JSONObject jsonObject = C0034bf.getJsonObject(this.kr, i);
                if (!"separator".equals(C0034bf.getJsonString(jsonObject, SROffer.TYPE)) && (jsonString = C0034bf.getJsonString(jsonObject, (String) SROffer.ICON)) != null) {
                    bv bvVar = new bv();
                    bvVar.setDelegate(this);
                    aK fdFromPapayaUri = webCache.fdFromPapayaUri(jsonString, papayaURL, bvVar);
                    if (fdFromPapayaUri != null) {
                        this.kq.set(i, C0030bb.drawableFromFD(fdFromPapayaUri));
                    } else if (bvVar.getUrl() != null) {
                        this.kp.set(i, bvVar);
                    }
                }
            }
            webCache.insertRequests(this.kp);
        }
        this.lg.notifyDataSetChanged();
    }

    public void setViewId(String str) {
        this.ja = str;
    }

    public void setWebView(bk bkVar) {
        this.ia = bkVar;
    }
}
