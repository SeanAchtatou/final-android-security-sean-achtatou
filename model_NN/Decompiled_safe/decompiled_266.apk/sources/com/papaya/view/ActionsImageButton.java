package com.papaya.view;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import com.papaya.view.CustomDialog;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class ActionsImageButton extends ImageButton implements DialogInterface.OnClickListener, View.OnClickListener {
    private ArrayList<Action> hF = new ArrayList<>(4);
    private WeakReference<Delegate> hG;
    private Drawable hH = null;
    public Object hI;

    public interface Delegate {
        void onActionSelected(ActionsImageButton actionsImageButton, Action action);
    }

    public ActionsImageButton(Context context) {
        super(context);
        setOnClickListener(this);
        refreshSelf();
    }

    public ActionsImageButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setOnClickListener(this);
        refreshSelf();
    }

    public ActionsImageButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setOnClickListener(this);
        refreshSelf();
    }

    private void fireAction(Action action) {
        Delegate actionDelegate = getActionDelegate();
        if (actionDelegate != null) {
            actionDelegate.onActionSelected(this, action);
        }
    }

    private void refreshSelf() {
        if (this.hF.isEmpty()) {
            setVisibility(4);
            return;
        }
        if (this.hF.size() == 1) {
            setImageDrawable(this.hF.get(0).icon);
        } else {
            setImageDrawable(this.hH);
        }
        setVisibility(0);
    }

    public void addAction(Action action) {
        this.hF.add(action);
        refreshSelf();
    }

    public void clearActions() {
        this.hF.clear();
        refreshSelf();
    }

    public Delegate getActionDelegate() {
        if (this.hG == null) {
            return null;
        }
        return this.hG.get();
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        fireAction(this.hF.get(i));
    }

    public void onClick(View view) {
        if (this.hF.size() == 1) {
            fireAction(this.hF.get(0));
        } else if (this.hF.size() > 1) {
            CharSequence[] charSequenceArr = new CharSequence[this.hF.size()];
            for (int i = 0; i < this.hF.size(); i++) {
                charSequenceArr[i] = this.hF.get(i).label;
            }
            new CustomDialog.Builder(getContext()).setItems(charSequenceArr, this).create().show();
        }
    }

    public void setActionDelegate(Delegate delegate) {
        this.hG = new WeakReference<>(delegate);
    }

    public void setCombinedDrawable(Drawable drawable) {
        this.hH = drawable;
        refreshSelf();
    }
}
