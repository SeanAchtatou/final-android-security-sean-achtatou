package com.papaya.view;

import android.content.DialogInterface;
import com.adknowledge.superrewards.model.SROffer;
import com.papaya.si.C0034bf;
import com.papaya.si.C0060z;
import com.papaya.si.X;
import com.papaya.si.bk;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebAlertDialogWrapper implements DialogInterface.OnClickListener {
    private JSONObject iT;
    private bk ia;
    private CustomDialog kk;

    public WebAlertDialogWrapper(CustomDialog customDialog, JSONObject jSONObject) {
        this.kk = customDialog;
        this.iT = jSONObject;
        configureWithJson(this.iT);
    }

    private void configureWithJson(JSONObject jSONObject) {
        this.iT = jSONObject;
        this.kk.setTitle(C0034bf.getJsonString(jSONObject, SROffer.TITLE));
        String jsonString = C0034bf.getJsonString(jSONObject, "text");
        if (jsonString == null) {
            jsonString = "Unknown message";
        }
        this.kk.setMessage(jsonString);
        int jsonInt = C0034bf.getJsonInt(jSONObject, SROffer.ICON, -1);
        switch (jsonInt) {
            case -1:
                this.kk.setIcon(0);
                break;
            case 0:
                this.kk.setIcon(C0060z.drawableID("alert_icon_check"));
                break;
            case 1:
                this.kk.setIcon(C0060z.drawableID("alert_icon_warning"));
                break;
            case 2:
                this.kk.setIcon(C0060z.drawableID("alert_icon_help"));
                break;
            default:
                X.w("unknown icon id %d", Integer.valueOf(jsonInt));
                this.kk.setIcon(0);
                break;
        }
        JSONArray jsonArray = C0034bf.getJsonArray(jSONObject, "buttons");
        if (jsonArray == null || jsonArray.length() <= 0) {
            this.kk.setButton(-1, this.kk.getContext().getString(C0060z.stringID("alert_button_ok")), this);
            return;
        }
        for (int i = 0; i < Math.min(jsonArray.length(), 3); i++) {
            String jsonString2 = C0034bf.getJsonString(C0034bf.getJsonObject(jsonArray, i), "text");
            if (i == 0) {
                this.kk.setButton(-1, jsonString2, this);
            } else if (i == 1) {
                this.kk.setButton(-2, jsonString2, this);
            } else if (i == 2) {
                this.kk.setButton(-3, jsonString2, this);
            }
        }
    }

    public CustomDialog getAlert() {
        return this.kk;
    }

    public JSONObject getCtx() {
        return this.iT;
    }

    public bk getWebView() {
        return this.ia;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        JSONArray jsonArray = C0034bf.getJsonArray(this.iT, "buttons");
        if (jsonArray != null && jsonArray.length() > 0) {
            String jsonString = C0034bf.getJsonString(C0034bf.getJsonObject(jsonArray, i == -1 ? 0 : i == -2 ? 1 : i == -3 ? 2 : 0), "action");
            if (jsonString != null && this.ia != null) {
                this.ia.callJS(jsonString);
            }
        }
    }

    public void setAlert(CustomDialog customDialog) {
        this.kk = customDialog;
    }

    public void setWebView(bk bkVar) {
        this.ia = bkVar;
    }
}
