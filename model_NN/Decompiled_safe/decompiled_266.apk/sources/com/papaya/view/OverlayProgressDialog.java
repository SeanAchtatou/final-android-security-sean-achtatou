package com.papaya.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.papaya.si.C0060z;

public class OverlayProgressDialog extends OverlayCustomDialog {
    private DynamicTextView jB;

    public OverlayProgressDialog(Context context) {
        super(context);
        View inflate = getLayoutInflater().inflate(C0060z.layoutID("progressview"), (ViewGroup) null);
        setView(inflate);
        this.jB = (DynamicTextView) inflate.findViewById(C0060z.id("message"));
    }

    public DynamicTextView getMessageView() {
        return this.jB;
    }

    public void setMessage(CharSequence charSequence) {
        this.jB.setText(charSequence);
    }
}
