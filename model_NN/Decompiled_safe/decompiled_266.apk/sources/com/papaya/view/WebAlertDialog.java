package com.papaya.view;

import android.content.Context;
import android.content.DialogInterface;
import com.adknowledge.superrewards.model.SROffer;
import com.adknowledge.superrewards.model.SRParams;
import com.papaya.si.C0034bf;
import com.papaya.si.C0060z;
import com.papaya.si.X;
import com.papaya.si.aV;
import com.papaya.si.bk;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebAlertDialog extends CustomDialog implements DialogInterface.OnClickListener, JsonConfigurable {
    private JSONObject kh;
    private bk ki;
    private String kj;

    public WebAlertDialog(Context context) {
        super(context);
    }

    public String getViewId() {
        return this.kj;
    }

    public bk getWebView() {
        return this.ki;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (this.ki != null) {
            JSONArray jsonArray = C0034bf.getJsonArray(this.kh, "buttons");
            int dialogButtonToWebIndex = C0034bf.dialogButtonToWebIndex(i);
            String jsonString = jsonArray != null ? C0034bf.getJsonString(C0034bf.getJsonObject(jsonArray, dialogButtonToWebIndex), "action") : null;
            if (aV.isEmpty(jsonString)) {
                String jsonString2 = C0034bf.getJsonString(this.kh, "action");
                bk bkVar = this.ki;
                String str = aV.isEmpty(jsonString2) ? "onAlertViewButtonTapped" : jsonString2;
                Object[] objArr = new Object[3];
                if (aV.isEmpty(jsonString2)) {
                    jsonString2 = "onAlertViewButtonTapped";
                }
                objArr[0] = jsonString2;
                objArr[1] = this.kj == null ? "" : this.kj;
                objArr[2] = Integer.valueOf(dialogButtonToWebIndex);
                bkVar.noWarnCallJS(str, aV.format("%s('%s', %d)", objArr));
                return;
            }
            this.ki.callJS(jsonString);
        }
    }

    public void refreshWithCtx(JSONObject jSONObject) {
        this.kh = jSONObject;
        if (C0034bf.getJsonString(jSONObject, SROffer.TITLE) != null) {
            setTitle(C0034bf.getJsonString(jSONObject, SROffer.TITLE));
        }
        String jsonString = C0034bf.getJsonString(jSONObject, "text");
        if (jsonString != null) {
            setMessage(jsonString);
        }
        int jsonInt = C0034bf.getJsonInt(jSONObject, SROffer.ICON, -1);
        if (jsonInt != -1) {
            setIcon(jsonInt);
        }
        JSONArray jsonArray = C0034bf.getJsonArray(jSONObject, "buttons");
        if (jsonArray == null || jsonArray.length() <= 0) {
            setButton(-2, "OK", this);
            return;
        }
        for (int i = 0; i < Math.min(jsonArray.length(), 3); i++) {
            String jsonString2 = C0034bf.getJsonString(C0034bf.getJsonObject(jsonArray, i), "text");
            if (aV.isEmpty(jsonString2)) {
                jsonString2 = "Unknown";
            }
            if (i == 0) {
                setButton(-2, jsonString2, this);
            } else if (i == 1) {
                setButton(-3, jsonString2, this);
            } else if (i == 2) {
                setButton(-1, jsonString2, this);
            }
        }
    }

    public void setDefaultTitle(int i) {
        if (aV.isEmpty(null)) {
            switch (i) {
                case -1:
                    setTitle((CharSequence) null);
                    return;
                case 0:
                    setTitle(C0060z.stringID("note"));
                    return;
                case 1:
                    setTitle(C0060z.stringID("warning"));
                    return;
                case 2:
                    setTitle(C0060z.stringID(SRParams.HELP));
                    return;
                default:
                    X.w("unknown icon id %d", Integer.valueOf(i));
                    setTitle((CharSequence) null);
                    return;
            }
        }
    }

    public void setIcon(int i) {
        int drawableID;
        setDefaultTitle(i);
        switch (i) {
            case -1:
                drawableID = 0;
                break;
            case 0:
                drawableID = C0060z.drawableID("alert_icon_check");
                break;
            case 1:
                drawableID = C0060z.drawableID("alert_icon_warning");
                break;
            case 2:
                drawableID = C0060z.drawableID("alert_icon_help");
                break;
            default:
                X.w("unknown icon id %d", Integer.valueOf(i));
                drawableID = 0;
                break;
        }
        super.setIcon(drawableID);
    }

    public void setText(String str) {
        setMessage(str);
    }

    public void setViewId(String str) {
        this.kj = str;
    }

    public void setWebView(bk bkVar) {
        this.ki = bkVar;
    }

    public void show() {
        if (this.kh == null) {
            setButton(-2, "OK", this);
        }
        super.show();
    }
}
