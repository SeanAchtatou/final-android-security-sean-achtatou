package com.papaya.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import com.papaya.si.C0001a;
import com.papaya.si.C0008ag;
import com.papaya.si.C0011aj;
import com.papaya.si.C0030bb;
import com.papaya.si.C0037c;
import com.papaya.si.C0055u;
import com.papaya.si.D;
import com.papaya.si.L;
import com.papaya.si.N;
import com.papaya.si.S;
import com.papaya.si.aV;

public class CardImageView extends LazyImageView implements C0055u.a {
    private D cR;
    private int id = -1;
    private Drawable ih;
    private int type = -1;

    public CardImageView(Context context) {
        super(context);
    }

    public CardImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CardImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.ih != null) {
            int width = getWidth();
            int height = getHeight();
            int min = Math.min(C0030bb.rp(16), width / 3);
            this.ih.setBounds(width - min, height - min, width, height);
            this.ih.draw(canvas);
        }
    }

    public boolean onImageUpdated(int i, int i2, int i3) {
        if (this.type == i && this.id == i2) {
            if (i == 2) {
                if (i3 > 0) {
                    setImageUrl(aV.format("getavatarhead?uid=%d&amp;v=%d", Integer.valueOf(i2), Integer.valueOf(i3)));
                } else {
                    setDefaultDrawable(C0037c.getBitmapDrawable("avatar_default"));
                }
            } else if (i == 4 && i3 > 0) {
                setImageUrl(aV.format("getgroupicon?cid=%d&amp;v=%d", Integer.valueOf(i2), Integer.valueOf(i3)));
            }
        }
        return true;
    }

    public void refreshWithCard(D d) {
        if (d == this.cR) {
            setStateDrawable(d == null ? 1 : d.getState());
            setGrayScaled(d != null && d.isGrayScaled());
            return;
        }
        this.cR = d;
        C0055u imageVersion = C0001a.getImageVersion();
        if (d == null) {
            setDefaultDrawable(null);
            setImageDrawable(null);
            setGrayScaled(false);
            setStateDrawable(1);
            imageVersion.removeDelegate(this, this.type, this.id);
            return;
        }
        setDefaultDrawable(d.getDefaultDrawable());
        setImageUrl(d.getImageUrl());
        setStateDrawable(d.getState());
        this.type = -1;
        this.id = -1;
        if (d instanceof C0011aj) {
            C0011aj ajVar = (C0011aj) d;
            if (ajVar.getUserID() != 0) {
                setUserID(ajVar.getUserID());
            }
        } else if (d instanceof S) {
            setUserID(((S) d).cW);
        } else if (d instanceof C0008ag) {
            setUserID(((C0008ag) d).cW);
        } else if (d instanceof N) {
            setUserID(((N) d).cW);
        } else if (d instanceof L) {
            setChatGroupID(((L) d).cS);
        }
        setGrayScaled(d.isGrayScaled());
    }

    /* access modifiers changed from: protected */
    public void setChatGroupID(int i) {
        this.type = 4;
        this.id = i;
        C0055u imageVersion = C0001a.getImageVersion();
        int version = imageVersion.getVersion(this.type, i);
        if (version > 0) {
            setImageUrl(aV.format("getgroupicon?cid=%d&amp;v=%d", Integer.valueOf(i), Integer.valueOf(version)));
        } else if (version == -1) {
            imageVersion.addDelegate(this, this.type, i);
        }
    }

    /* access modifiers changed from: protected */
    public void setStateDrawable(int i) {
        this.ih = null;
        if (i == 3) {
            this.ih = C0037c.getDrawable("state_3");
        } else if (i == 2) {
            this.ih = C0037c.getDrawable("state_2");
        }
    }

    /* access modifiers changed from: protected */
    public void setUserID(int i) {
        this.type = 2;
        this.id = i;
        C0055u imageVersion = C0001a.getImageVersion();
        int version = imageVersion.getVersion(this.type, i);
        if (version > 0) {
            setImageUrl(aV.format("getavatarhead?uid=%d&amp;v=%d", Integer.valueOf(i), Integer.valueOf(version)));
        } else if (version == 0) {
            setDefaultDrawable(C0037c.getBitmapDrawable("avatar_default"));
        } else if (version == -1) {
            imageVersion.addDelegate(this, this.type, i);
        }
    }
}
