package com.papaya.view;

import android.app.DatePickerDialog;
import android.widget.DatePicker;
import com.adknowledge.superrewards.model.SROffer;
import com.papaya.si.C0034bf;
import com.papaya.si.aV;
import com.papaya.si.bk;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.json.JSONObject;

public class WebDatePickerDialogWrapper implements DatePickerDialog.OnDateSetListener {
    private JSONObject iT;
    private bk ia;
    private DatePickerDialog kl;
    private DateFormat km = new SimpleDateFormat("MMM d, yyyy");

    public WebDatePickerDialogWrapper(bk bkVar, JSONObject jSONObject) {
        this.ia = bkVar;
        this.iT = jSONObject;
        setupViews();
    }

    private void setupViews() {
        Calendar instance = Calendar.getInstance();
        int intValue = aV.intValue(C0034bf.getJsonString(this.iT, "initYear"), instance.get(1));
        int intValue2 = aV.intValue(C0034bf.getJsonString(this.iT, "initMonth"), instance.get(2));
        int intValue3 = aV.intValue(C0034bf.getJsonString(this.iT, "initDay"), instance.get(5));
        String jsonString = C0034bf.getJsonString(this.iT, SROffer.TITLE);
        this.kl = new DatePickerDialog(this.ia.getOwnerActivity(), this, intValue, intValue2, intValue3);
        this.kl.setTitle(jsonString);
    }

    public DatePickerDialog getDialog() {
        return this.kl;
    }

    public void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
        int i4 = i2 + 1;
        String jsonString = C0034bf.getJsonString(this.iT, "action");
        if (jsonString != null) {
            this.ia.callJS(aV.format("%s(%d, %d, %d)", jsonString, Integer.valueOf(i), Integer.valueOf(i4), Integer.valueOf(i3)));
        }
        String jsonString2 = C0034bf.getJsonString(this.iT, "year");
        if (jsonString2 != null) {
            this.ia.callJS(jsonString2 + "=" + i);
        }
        String jsonString3 = C0034bf.getJsonString(this.iT, "month");
        if (jsonString3 != null) {
            this.ia.callJS(jsonString3 + "=" + i4);
        }
        String jsonString4 = C0034bf.getJsonString(this.iT, "day");
        if (jsonString4 != null) {
            this.ia.callJS(jsonString4 + "=" + i3);
        }
        String jsonString5 = C0034bf.getJsonString(this.iT, "datetext");
        if (jsonString5 != null) {
            this.ia.callJS(aV.format("%s=''", jsonString5));
            this.ia.callJS(aV.format("%s='%s'", jsonString5, this.km.format(new Date(i - 1900, i4 - 1, i3))));
        }
    }
}
