package com.papaya.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import com.papaya.si.C0037c;
import com.papaya.si.aH;
import com.papaya.si.aN;
import com.papaya.si.aP;

public class AppIconView extends LazyImageView implements aN {
    private int dd;

    public AppIconView(Context context) {
        super(context);
    }

    public AppIconView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AppIconView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public static String composeAppIconUrl(int i) {
        return "appicon?id=" + i;
    }

    public int getAppID() {
        return this.dd;
    }

    public boolean onDataStateChanged(aP aPVar) {
        if (!C0037c.B.contains(this.dd)) {
            return false;
        }
        setImageUrl(composeAppIconUrl(this.dd));
        return true;
    }

    public void setApp(int i) {
        this.dd = i;
        aH aHVar = C0037c.B;
        if (i == 0) {
            setImageUrl(null);
            aHVar.unregisterMonitor(this);
        } else if (aHVar.contains(i)) {
            setImageUrl(composeAppIconUrl(i));
        } else {
            aHVar.registerMonitor(this);
        }
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        setVisibility(drawable == null ? 8 : 0);
    }
}
