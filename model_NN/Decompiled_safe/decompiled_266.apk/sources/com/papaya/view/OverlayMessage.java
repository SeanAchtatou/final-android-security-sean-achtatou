package com.papaya.view;

import android.app.Application;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.widget.FrameLayout;
import com.papaya.si.C0023av;
import com.papaya.si.C0030bb;
import com.papaya.si.C0037c;
import com.papaya.si.C0056v;
import com.papaya.si.X;
import com.papaya.si.aL;
import com.papaya.si.aV;

public class OverlayMessage extends FrameLayout implements View.OnClickListener {
    public static final int DEFAULT_TIMEOUT = 5000;
    private float alpha = 0.95f;
    private int gravity = 48;
    private int jM = DEFAULT_TIMEOUT;
    private a jN;
    private View.OnClickListener jO;
    private View jy;
    private WindowManager jz;

    class a extends aL {
        /* synthetic */ a(OverlayMessage overlayMessage) {
            this((byte) 0);
        }

        private a(byte b) {
        }

        /* access modifiers changed from: protected */
        public final void runTask() {
            OverlayMessage.this.hide();
        }
    }

    public OverlayMessage(Context context, View view) {
        super(context);
        this.jz = C0030bb.getWindowManager(context);
        this.jy = view;
        addView(view, new FrameLayout.LayoutParams(-2, -2));
        super.setOnClickListener(this);
    }

    public static void showMessage(String str, String str2, final String str3, int i, int i2, int i3) {
        try {
            final Application applicationContext = C0037c.getApplicationContext();
            if (applicationContext != null) {
                OverlayMessageView overlayMessageView = new OverlayMessageView(applicationContext, new LazyImageView(applicationContext));
                ((LazyImageView) overlayMessageView.getImageView()).setImageUrl(str);
                overlayMessageView.getTextView().setText(str2);
                OverlayMessage overlayMessage = new OverlayMessage(applicationContext, overlayMessageView);
                overlayMessage.setGravity(i2);
                overlayMessage.alpha = (((float) i3) + 0.0f) / 100.0f;
                overlayMessage.jM = i;
                if (!aV.isEmpty(str3)) {
                    overlayMessageView.getAccessoryView().setVisibility(0);
                    overlayMessage.setOnClickListener(new View.OnClickListener() {
                        public final void onClick(View view) {
                            C0023av.getInstance().showURL(applicationContext, str3);
                        }
                    });
                } else {
                    overlayMessageView.getAccessoryView().setVisibility(8);
                }
                overlayMessage.show();
            }
        } catch (Exception e) {
        }
    }

    public static void showTitleMessage(String str, String str2, String str3, final String str4, int i, int i2, int i3) {
        try {
            final Application applicationContext = C0037c.getApplicationContext();
            if (applicationContext != null) {
                OverlayTitleMessageView overlayTitleMessageView = new OverlayTitleMessageView(applicationContext, new LazyImageView(applicationContext));
                overlayTitleMessageView.getTitleView().setText(aV.isEmpty(str) ? C0056v.bj : str);
                overlayTitleMessageView.getTitleView().setText(C0056v.bj);
                ((LazyImageView) overlayTitleMessageView.getImageView()).setImageUrl(str2);
                overlayTitleMessageView.getTextView().setText(str3);
                OverlayMessage overlayMessage = new OverlayMessage(applicationContext, overlayTitleMessageView);
                overlayMessage.setGravity(i2);
                overlayMessage.alpha = (((float) i3) + 0.0f) / 100.0f;
                overlayMessage.jM = i;
                if (!aV.isEmpty(str4)) {
                    overlayTitleMessageView.getAccessoryView().setVisibility(0);
                    overlayMessage.setOnClickListener(new View.OnClickListener() {
                        public final void onClick(View view) {
                            C0023av.getInstance().showURL(applicationContext, str4);
                        }
                    });
                } else {
                    overlayTitleMessageView.getAccessoryView().setVisibility(8);
                }
                overlayMessage.show();
            }
        } catch (Exception e) {
        }
    }

    public int getGravity() {
        return this.gravity;
    }

    public int getHideTimeout() {
        return this.jM;
    }

    public void hide() {
        aL.cancelTask(this.jN);
        try {
            if (this.jz != null && getParent() != null) {
                this.jz.removeView(this);
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        try {
            super.onAttachedToWindow();
            if (this.jy != null && this.jz != null) {
                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                layoutParams.width = -2;
                layoutParams.height = -2;
                layoutParams.gravity = this.gravity;
                layoutParams.windowAnimations = 16973828;
                layoutParams.verticalMargin = 0.08f;
                layoutParams.format = -3;
                layoutParams.flags = 40;
                layoutParams.alpha = this.alpha;
                this.jz.updateViewLayout(this, layoutParams);
                AnimationSet makeSlideAnimation = C0030bb.makeSlideAnimation(this.gravity);
                makeSlideAnimation.setDuration(667);
                setLayoutAnimation(new LayoutAnimationController(makeSlideAnimation));
                startLayoutAnimation();
            }
        } catch (Exception e) {
            X.w(e, "Failed to onAttachedToWindow", new Object[0]);
        }
    }

    public void onClick(View view) {
        try {
            hide();
            if (this.jO != null) {
                this.jO.onClick(this);
            }
        } catch (Exception e) {
        }
    }

    public void setGravity(int i) {
        this.gravity = i;
    }

    public void setHideTimeout(int i) {
        this.jM = i;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.jO = onClickListener;
    }

    public void show() {
        show(null);
    }

    public void show(Context context) {
        try {
            if (getParent() != null) {
                this.jz.removeView(this);
            }
        } catch (Exception e) {
        }
        if (context != null) {
            this.jz = C0030bb.getWindowManager(context);
        }
        try {
            if (this.jz != null) {
                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                layoutParams.width = -2;
                layoutParams.height = -2;
                layoutParams.verticalMargin = 0.08f;
                layoutParams.alpha = this.alpha;
                layoutParams.gravity = this.gravity;
                layoutParams.windowAnimations = 16973828;
                layoutParams.format = -3;
                layoutParams.type = 2005;
                this.jz.addView(this, layoutParams);
            } else {
                X.w("wm is null", new Object[0]);
            }
        } catch (Exception e2) {
            X.w(e2, "Failed to showInContext", new Object[0]);
        }
        aL.cancelTask(this.jN);
        if (this.jM > 0) {
            this.jN = new a(this);
            C0030bb.postDelayed(this.jN, (long) this.jM);
        }
    }
}
