package com.papaya.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.papaya.si.A;
import com.papaya.si.C0028b;
import com.papaya.si.C0037c;
import com.papaya.si.C0054t;
import com.papaya.si.C0060z;
import com.papaya.si.L;
import com.papaya.si.N;
import com.papaya.si.X;
import com.papaya.si.aV;
import com.papaya.si.aW;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class ChatGroupUserListView extends ListView implements AdapterView.OnItemClickListener, aW, C0054t {
    /* access modifiers changed from: private */
    public ArrayList<N> ii = new ArrayList<>();
    private a ij;

    class a extends BaseAdapter {
        /* synthetic */ a(ChatGroupUserListView chatGroupUserListView) {
            this((byte) 0);
        }

        private a(byte b) {
        }

        public final int getCount() {
            return ChatGroupUserListView.this.ii.size();
        }

        public final Object getItem(int i) {
            return Integer.valueOf(i);
        }

        public final long getItemId(int i) {
            return (long) i;
        }

        public final View getView(int i, View view, ViewGroup viewGroup) {
            View view2;
            TextView textView;
            String str;
            if (view == null) {
                View inflate = View.inflate(ChatGroupUserListView.this.getContext(), C0060z.layoutID("list_item_4"), null);
                inflate.setTag(new ListItem4ViewHolder(inflate));
                view2 = inflate;
            } else {
                view2 = view;
            }
            ListItem4ViewHolder listItem4ViewHolder = (ListItem4ViewHolder) view2.getTag();
            N n = (N) ChatGroupUserListView.this.ii.get(i);
            listItem4ViewHolder.dq.refreshWithCard(n);
            listItem4ViewHolder.cn.setText(n.getTitle());
            if (n.cW == A.bL.getUserID()) {
                textView = listItem4ViewHolder.dH;
                str = C0037c.getString("label_chatroom_user_list_you");
            } else {
                TextView textView2 = listItem4ViewHolder.dH;
                if (C0037c.getSession().getFriends().isFriend(n.cW)) {
                    textView = textView2;
                    str = C0037c.getString("label_chatroom_user_list_friend");
                } else {
                    textView = textView2;
                    str = null;
                }
            }
            textView.setText(str);
            listItem4ViewHolder.js.setVisibility(8);
            return view2;
        }
    }

    public ChatGroupUserListView(Context context) {
        super(context);
        setCacheColorHint(-1);
        setOnItemClickListener(this);
        this.ij = new a(this);
        setAdapter((ListAdapter) this.ij);
    }

    public void close() {
        C0037c.A.unregisterCmd(this, 305);
    }

    public void handleServerResponse(Vector<Object> vector) {
        int sgetInt = aV.sgetInt(vector, 0);
        switch (sgetInt) {
            case 305:
                this.ii.clear();
                for (int i = 2; i < vector.size(); i++) {
                    List list = (List) aV.sget(vector, i);
                    N n = new N();
                    n.cW = aV.sgetInt(list, 0);
                    n.name = (String) aV.sget(list, 1);
                    n.setState(aV.sgetInt(list, 2) > 0 ? 1 : 0);
                    aV.sgetInt(list, 3);
                    this.ii.add(n);
                }
                this.ij.notifyDataSetChanged();
                C0037c.A.unregisterCmd(this, 305);
                return;
            default:
                X.e("unknown cmd " + sgetInt, new Object[0]);
                return;
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        N n = this.ii.get(i);
        if (n.cW != A.bL.getUserID()) {
            C0028b.openHome(null, n.cW);
        }
    }

    public void refreshWithGroup(L l) {
        if (l != null) {
            C0037c.send(305, Integer.valueOf(l.cS), 0);
            C0037c.A.registerCmds(this, 305);
        }
    }
}
