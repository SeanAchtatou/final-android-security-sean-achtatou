package com.papaya.view;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.papaya.si.C0030bb;
import com.papaya.si.C0034bf;
import com.papaya.si.C0060z;
import com.papaya.si.X;
import com.papaya.view.HorizontalScrollView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class HorizontalBar extends RelativeLayout implements View.OnClickListener, HorizontalScrollView.Delegate, JsonConfigurable {
    private WeakReference<Delegate> es;
    private JSONObject iT;
    private ArrayList<PushButton> iU = new ArrayList<>(10);
    private HorizontalScrollView iV;
    private LinearLayout iW;
    private ImageButton iX;
    private ImageButton iY;
    private boolean iZ = false;
    private String ja;

    public interface Delegate {
        void barButtonClicked(HorizontalBar horizontalBar, int i, JSONObject jSONObject);
    }

    public HorizontalBar(Context context, AbsoluteLayout.LayoutParams layoutParams) {
        super(context);
        setLayoutParams(layoutParams);
        setupViews();
    }

    private void centerSelected() {
        int i;
        int i2 = 0;
        while (true) {
            if (i2 >= this.iU.size()) {
                i = -1;
                break;
            } else if (this.iU.get(i2).isSelected()) {
                i = i2;
                break;
            } else {
                i2++;
            }
        }
        if (i != -1) {
            PushButton pushButton = this.iU.get(i);
            float left = ((float) pushButton.getLeft()) + (((float) pushButton.getWidth()) / 2.0f);
            float left2 = (float) this.iU.get(0).getLeft();
            float right = (float) this.iU.get(this.iU.size() - 1).getRight();
            float scrollX = (float) this.iV.getScrollX();
            float width = (float) this.iV.getWidth();
            if (left - scrollX > width / 2.0f) {
                float f = left - (width / 2.0f);
                this.iV.smoothScrollTo((int) (right - f < width ? right - width : f), 0);
            } else if (left - scrollX < width / 2.0f) {
                float f2 = left - (width / 2.0f);
                if (f2 < 0.0f || left2 - f2 < 0.0f) {
                    f2 = left2;
                }
                this.iV.smoothScrollTo((int) f2, 0);
            }
        }
    }

    private void refreshArrows() {
        int scrollX = this.iV.getScrollX();
        if (scrollX == 0) {
            this.iX.setVisibility(8);
        } else {
            this.iX.setVisibility(0);
        }
        if (this.iU.size() <= 0) {
            this.iY.setVisibility(8);
        } else if (this.iU.get(this.iU.size() - 1).getRight() - scrollX > this.iV.getWidth()) {
            this.iY.setVisibility(0);
        } else {
            this.iY.setVisibility(8);
        }
    }

    private void setupViews() {
        if (this.iV == null) {
            this.iV = new HorizontalScrollView(getContext());
            this.iV.setDelegate(this);
            this.iV.setHorizontalScrollBarEnabled(false);
            this.iV.setFadingEdgeLength(10);
            this.iV.setHorizontalFadingEdgeEnabled(true);
            this.iX = new ImageButton(getContext());
            this.iY = new ImageButton(getContext());
            this.iX.setImageDrawable(getContext().getResources().getDrawable(C0060z.drawableID("hori_arrow_l")));
            this.iX.setBackgroundColor(0);
            this.iY.setImageDrawable(getContext().getResources().getDrawable(C0060z.drawableID("hori_arrow_r")));
            this.iY.setBackgroundColor(0);
            this.iW = new LinearLayout(getContext());
            this.iW.setBackgroundColor(0);
            this.iV.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            this.iV.addView(this.iW);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(34, 36);
            layoutParams.addRule(9);
            layoutParams.addRule(15);
            this.iX.setLayoutParams(layoutParams);
            this.iX.setOnClickListener(this);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(34, 36);
            layoutParams2.addRule(11);
            layoutParams2.addRule(15);
            this.iY.setLayoutParams(layoutParams2);
            this.iY.setOnClickListener(this);
            addView(this.iV);
            addView(this.iX);
            addView(this.iY);
        }
    }

    public Delegate getDelegate() {
        if (this.es != null) {
            return this.es.get();
        }
        return null;
    }

    public String getViewId() {
        return this.ja;
    }

    public void onClick(View view) {
        if (view == this.iX) {
            this.iV.arrowScroll(17);
        } else if (view == this.iY) {
            this.iV.arrowScroll(66);
        } else {
            Delegate delegate = getDelegate();
            if (delegate != null) {
                int indexOf = this.iU.indexOf(view);
                delegate.barButtonClicked(this, indexOf, C0034bf.getJsonObject(C0034bf.getJsonArray(this.iT, "tabs"), indexOf));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        refreshArrows();
        if (!this.iZ) {
            centerSelected();
            this.iZ = true;
        }
    }

    public void refreshWithCtx(JSONObject jSONObject) {
        this.iT = jSONObject;
        for (int i = 0; i < this.iU.size(); i++) {
            PushButton pushButton = this.iU.get(i);
            pushButton.setOnClickListener(null);
            C0030bb.removeFromSuperView(pushButton);
        }
        this.iU.clear();
        JSONArray jsonArray = C0034bf.getJsonArray(this.iT, "tabs");
        if (jsonArray != null) {
            int i2 = 0;
            while (i2 < jsonArray.length()) {
                try {
                    JSONObject jSONObject2 = jsonArray.getJSONObject(i2);
                    PushButton pushButton2 = i2 == 0 ? new PushButton(getContext(), C0060z.drawableID("hori_btn_left"), C0060z.drawableID("hori_btn_hover_left")) : i2 == jsonArray.length() - 1 ? new PushButton(getContext(), C0060z.drawableID("hori_btn_right"), C0060z.drawableID("hori_btn_hover_right")) : new PushButton(getContext(), C0060z.drawableID("hori_btn"), C0060z.drawableID("hori_btn_hover"));
                    pushButton2.setText(C0034bf.getJsonString(jSONObject2, "text"));
                    pushButton2.setTextColor(-16777216);
                    pushButton2.setTypeface(Typeface.DEFAULT_BOLD);
                    pushButton2.setGravity(16);
                    pushButton2.setPadding(C0030bb.rp(10), C0030bb.rp(6), C0030bb.rp(10), C0030bb.rp(10));
                    if (C0034bf.getJsonInt(jSONObject2, "active") == 1) {
                        pushButton2.setSelected(true);
                    }
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                    layoutParams.rightMargin = 1;
                    pushButton2.setLayoutParams(layoutParams);
                    this.iW.addView(pushButton2);
                    pushButton2.setOnClickListener(this);
                    this.iU.add(pushButton2);
                } catch (Exception e) {
                    X.w(e, "Failed to config with ctx", new Object[0]);
                }
                i2++;
            }
        }
    }

    public void scrollChanged(int i, int i2, int i3, int i4) {
        refreshArrows();
    }

    public void setActiveButton(int i) {
        int i2 = 0;
        while (i2 < this.iU.size()) {
            this.iU.get(i2).setSelected(i == i2);
            i2++;
        }
    }

    public void setDelegate(Delegate delegate) {
        if (delegate == null) {
            this.es = null;
        } else {
            this.es = new WeakReference<>(delegate);
        }
    }

    public void setViewId(String str) {
        this.ja = str;
    }
}
