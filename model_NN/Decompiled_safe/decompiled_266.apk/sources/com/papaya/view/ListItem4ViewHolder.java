package com.papaya.view;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import com.adknowledge.superrewards.model.SROffer;
import com.papaya.si.C0030bb;

public class ListItem4ViewHolder {
    public TextView cn;
    public TextView dH;
    public CardImageView dq;
    public ImageButton js;

    public ListItem4ViewHolder(View view) {
        this.dq = (CardImageView) C0030bb.find(view, SROffer.IMAGE);
        this.cn = (TextView) C0030bb.find(view, SROffer.TITLE);
        this.dH = (TextView) C0030bb.find(view, "subtitle");
        this.js = (ImageButton) C0030bb.find(view, "accessory");
    }
}
