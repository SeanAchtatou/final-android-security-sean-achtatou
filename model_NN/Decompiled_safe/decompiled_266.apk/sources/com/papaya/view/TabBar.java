package com.papaya.view;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import com.papaya.si.C0030bb;
import com.papaya.si.C0060z;

public class TabBar extends LinearLayout {
    private TabBarContentView jV;
    private View jW;

    public TabBar(Context context) {
        super(context);
        setOrientation(1);
        this.jV = new TabBarContentView(context);
        addView(this.jV, new LinearLayout.LayoutParams(-1, C0030bb.rp(63)));
        this.jW = new View(context);
        this.jW.setBackgroundResource(C0060z.drawableID("tab_bottom_bg"));
        addView(this.jW, new LinearLayout.LayoutParams(-1, C0030bb.rp(5)));
    }

    public TabBarContentView getTabsView() {
        return this.jV;
    }
}
