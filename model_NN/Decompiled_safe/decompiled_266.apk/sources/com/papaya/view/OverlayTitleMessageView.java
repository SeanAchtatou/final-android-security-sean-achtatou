package com.papaya.view;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class OverlayTitleMessageView<IV extends ImageView> extends LinearLayout {
    private TextView cn;
    private TextView eL;
    private IV jR;
    private ImageView jS;

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: IV
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public OverlayTitleMessageView(android.content.Context r10, IV r11) {
        /*
            r9 = this;
            r8 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r7 = 16
            r3 = 0
            r6 = 5
            r5 = -2
            r9.<init>(r10)
            r9.jR = r11
            r9.setBackgroundColor(r3)
            r0 = 1
            r9.setOrientation(r0)
            android.widget.TextView r0 = new android.widget.TextView
            r0.<init>(r10)
            r9.cn = r0
            android.widget.TextView r0 = r9.cn
            java.lang.String r1 = "message_title_bg"
            int r1 = com.papaya.si.C0060z.drawableID(r1)
            r0.setBackgroundResource(r1)
            android.widget.TextView r0 = r9.cn
            r0.setTextColor(r8)
            android.widget.TextView r0 = r9.cn
            r1 = 1094713344(0x41400000, float:12.0)
            r0.setTextSize(r1)
            android.widget.TextView r0 = r9.cn
            r1 = 17
            r0.setGravity(r1)
            android.widget.TextView r0 = r9.cn
            android.text.TextUtils$TruncateAt r1 = android.text.TextUtils.TruncateAt.MIDDLE
            r0.setEllipsize(r1)
            android.widget.TextView r0 = r9.cn
            r1 = 20
            int r1 = com.papaya.si.C0030bb.rp(r1)
            r2 = 3
            int r2 = com.papaya.si.C0030bb.rp(r2)
            r0.setPadding(r3, r1, r3, r2)
            android.widget.TextView r0 = r9.cn
            r1 = 200(0xc8, float:2.8E-43)
            int r1 = com.papaya.si.C0030bb.rp(r1)
            r0.setMinimumWidth(r1)
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            r1 = -1
            r0.<init>(r1, r5)
            android.widget.TextView r1 = r9.cn
            r9.addView(r1, r0)
            android.widget.LinearLayout r0 = new android.widget.LinearLayout
            r0.<init>(r10)
            java.lang.String r1 = "message_body_bg"
            int r1 = com.papaya.si.C0060z.drawableID(r1)
            r0.setBackgroundResource(r1)
            int r1 = r0.getPaddingLeft()
            int r2 = com.papaya.si.C0030bb.rp(r3)
            int r3 = r0.getPaddingRight()
            r4 = 18
            int r4 = com.papaya.si.C0030bb.rp(r4)
            r0.setPadding(r1, r2, r3, r4)
            r1 = 200(0xc8, float:2.8E-43)
            int r1 = com.papaya.si.C0030bb.rp(r1)
            r0.setMinimumWidth(r1)
            android.widget.LinearLayout$LayoutParams r1 = new android.widget.LinearLayout$LayoutParams
            r1.<init>(r5, r5)
            r2 = 17
            r1.gravity = r2
            r9.addView(r0, r1)
            android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_CENTER
            r11.setScaleType(r1)
            android.widget.LinearLayout$LayoutParams r1 = new android.widget.LinearLayout$LayoutParams
            r2 = 24
            int r2 = com.papaya.si.C0030bb.rp(r10, r2)
            r3 = 24
            int r3 = com.papaya.si.C0030bb.rp(r10, r3)
            r1.<init>(r2, r3)
            r1.gravity = r7
            int r2 = com.papaya.si.C0030bb.rp(r6)
            r1.leftMargin = r2
            int r2 = com.papaya.si.C0030bb.rp(r6)
            r1.rightMargin = r2
            IV r2 = r9.jR
            r0.addView(r2, r1)
            android.widget.TextView r1 = new android.widget.TextView
            r1.<init>(r10)
            r9.eL = r1
            android.widget.TextView r1 = r9.eL
            r1.setTextColor(r8)
            android.widget.TextView r1 = r9.eL
            android.graphics.Typeface r2 = android.graphics.Typeface.DEFAULT_BOLD
            r1.setTypeface(r2)
            android.widget.LinearLayout$LayoutParams r1 = new android.widget.LinearLayout$LayoutParams
            r1.<init>(r5, r5)
            r1.gravity = r7
            int r2 = com.papaya.si.C0030bb.rp(r6)
            r1.rightMargin = r2
            android.widget.TextView r2 = r9.eL
            r0.addView(r2, r1)
            android.widget.ImageView r1 = new android.widget.ImageView
            r1.<init>(r10)
            r9.jS = r1
            android.widget.ImageView r1 = r9.jS
            java.lang.String r2 = "chat_icon_fold"
            int r2 = com.papaya.si.C0060z.drawableID(r2)
            r1.setImageResource(r2)
            android.widget.LinearLayout$LayoutParams r1 = new android.widget.LinearLayout$LayoutParams
            r2 = 11
            int r2 = com.papaya.si.C0030bb.rp(r10, r2)
            r3 = 20
            int r3 = com.papaya.si.C0030bb.rp(r10, r3)
            r1.<init>(r2, r3)
            r1.gravity = r7
            int r2 = com.papaya.si.C0030bb.rp(r6)
            r1.leftMargin = r2
            int r2 = com.papaya.si.C0030bb.rp(r6)
            r1.rightMargin = r2
            android.widget.ImageView r2 = r9.jS
            r0.addView(r2, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.papaya.view.OverlayTitleMessageView.<init>(android.content.Context, android.widget.ImageView):void");
    }

    public ImageView getAccessoryView() {
        return this.jS;
    }

    public IV getImageView() {
        return this.jR;
    }

    public TextView getTextView() {
        return this.eL;
    }

    public TextView getTitleView() {
        return this.cn;
    }
}
