package com.papaya.view;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.papaya.si.C0030bb;
import com.papaya.si.C0037c;
import com.papaya.si.C0060z;

public class LoadingView extends LinearLayout {
    private TextView eL;
    private ProgressBar jt;

    public LoadingView(Context context) {
        super(context);
        setupViews(context);
    }

    private void setupViews(Context context) {
        this.jt = new ProgressBar(context);
        this.eL = new TextView(context, null, 16842817);
        this.eL.setBackgroundColor(0);
        this.eL.setText(C0037c.getApplicationContext().getString(C0060z.stringID("web_loading")));
        this.eL.setGravity(17);
        setBackgroundResource(17301504);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(C0030bb.rp(28), C0030bb.rp(28));
        layoutParams.setMargins(C0030bb.rp(5), C0030bb.rp(5), C0030bb.rp(5), C0030bb.rp(5));
        addView(this.jt, layoutParams);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -1);
        layoutParams2.rightMargin = C0030bb.rp(5);
        addView(this.eL, layoutParams2);
    }

    public void fixAnimationBug() {
        this.jt.setVisibility(8);
        this.jt.setVisibility(0);
    }

    public ProgressBar getProgressBar() {
        return this.jt;
    }

    public TextView getTextView() {
        return this.eL;
    }
}
