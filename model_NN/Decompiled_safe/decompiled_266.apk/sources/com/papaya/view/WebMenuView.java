package com.papaya.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsoluteLayout;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.papaya.si.C0034bf;
import com.papaya.si.aV;
import com.papaya.si.bk;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebMenuView extends ListView {
    private JSONObject iT;
    /* access modifiers changed from: private */
    public bk ia;
    private a kA = new a(this);
    private AbsoluteLayout.LayoutParams kB;
    /* access modifiers changed from: private */
    public String kC;
    /* access modifiers changed from: private */
    public JSONArray kD;
    /* access modifiers changed from: private */
    public int kE;
    /* access modifiers changed from: private */
    public int kF;

    class a extends BaseAdapter {
        /* synthetic */ a(WebMenuView webMenuView) {
            this((byte) 0);
        }

        private a(byte b) {
        }

        public final int getCount() {
            if (WebMenuView.this.kD != null) {
                return WebMenuView.this.kD.length();
            }
            return 0;
        }

        public final Object getItem(int i) {
            return C0034bf.getJsonObject(WebMenuView.this.kD, i);
        }

        public final long getItemId(int i) {
            return (long) i;
        }

        public final View getView(int i, View view, ViewGroup viewGroup) {
            TextView textView;
            if (view == null) {
                textView = new TextView(viewGroup.getContext());
                textView.setLayoutParams(new AbsListView.LayoutParams(-1, WebMenuView.this.kE));
                textView.setGravity(16);
            } else {
                textView = (TextView) view;
            }
            textView.setText(C0034bf.getJsonString(C0034bf.getJsonObject(WebMenuView.this.kD, i), "text"));
            textView.setTextColor(-16777216);
            textView.setTextSize((float) WebMenuView.this.kF);
            return textView;
        }
    }

    public WebMenuView(Context context, String str, AbsoluteLayout.LayoutParams layoutParams) {
        super(context, null, 16842868);
        setClickable(true);
        setCacheColorHint(0);
        this.kC = str;
        setAdapter((ListAdapter) this.kA);
        this.kB = layoutParams;
        setLayoutParams(layoutParams);
        setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                if (WebMenuView.this.ia != null) {
                    String jsonString = C0034bf.getJsonString(C0034bf.getJsonObject(WebMenuView.this.kD, i), "action");
                    if (jsonString == null) {
                        WebMenuView.this.ia.callJS(aV.format("menutapped('%s', '%d')", WebMenuView.this.kC, Integer.valueOf(i)));
                    } else {
                        WebMenuView.this.ia.callJS(jsonString);
                    }
                }
                WebMenuView.this.setSelection(i);
            }
        });
    }

    public String getName() {
        return this.kC;
    }

    public bk getWebView() {
        return this.ia;
    }

    public void pack() {
        AbsoluteLayout.LayoutParams layoutParams = (AbsoluteLayout.LayoutParams) getLayoutParams();
        int dividerHeight = (this.kE + getDividerHeight()) * this.kA.getCount();
        if (layoutParams.height > dividerHeight) {
            setLayoutParams(new AbsoluteLayout.LayoutParams(layoutParams.width, dividerHeight, layoutParams.x, layoutParams.y));
        }
    }

    public void refreshWithCtx(JSONObject jSONObject) {
        this.iT = jSONObject;
        setLayoutParams(this.kB);
        this.kE = C0034bf.getJsonInt(this.iT, "rowHeight", 24);
        this.kF = C0034bf.getJsonInt(this.iT, "fontSize");
        this.kD = C0034bf.getJsonArray(this.iT, "items");
        this.kA.notifyDataSetChanged();
        if (this.kD != null) {
            for (int i = 0; i < this.kD.length(); i++) {
                if (C0034bf.getJsonInt(C0034bf.getJsonObject(this.kD, i), "selected", 0) != 0) {
                    setSelection(i);
                }
            }
        }
    }

    public void setName(String str) {
        this.kC = str;
    }

    public void setWebView(bk bkVar) {
        this.ia = bkVar;
    }
}
