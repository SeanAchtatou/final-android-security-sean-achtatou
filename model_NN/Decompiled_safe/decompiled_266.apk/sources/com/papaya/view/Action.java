package com.papaya.view;

import android.graphics.drawable.Drawable;

public final class Action<T> {
    public T data;
    public boolean enabled = true;
    public Drawable icon;
    public int id;
    public String label;

    public Action() {
    }

    public Action(int i, Drawable drawable, String str) {
        this.id = i;
        this.icon = drawable;
        this.label = str;
    }
}
