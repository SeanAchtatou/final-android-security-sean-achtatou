package com.papaya.view;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import com.papaya.si.C0030bb;
import com.papaya.si.C0037c;
import com.papaya.si.C0060z;
import com.papaya.si.bA;

public class WebDialog extends CustomDialog implements DialogInterface.OnClickListener {
    private bA kn = new bA(C0037c.getApplicationContext(), null);

    public WebDialog(Context context) {
        super(context);
        this.kn.setDialog(this);
        setContentView(this.kn.getContentLayout());
        if (context instanceof Activity) {
            this.kn.setOwnerActivity((Activity) context);
        }
        setButton(-1, C0037c.getApplicationContext().getString(C0060z.stringID("close")), this);
    }

    public void dismiss() {
        this.kn.close();
        C0030bb.removeFromSuperView(this.kn.getContentLayout());
        super.dismiss();
    }

    public bA getController() {
        return this.kn;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
    }
}
