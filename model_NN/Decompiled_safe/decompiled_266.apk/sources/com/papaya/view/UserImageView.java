package com.papaya.view;

import android.content.Context;
import android.util.AttributeSet;
import com.papaya.si.C0001a;
import com.papaya.si.C0037c;

public class UserImageView extends CardImageView {
    public UserImageView(Context context) {
        super(context);
    }

    public UserImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public UserImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setUserID(int i) {
        setDefaultDrawable(C0037c.getBitmapDrawable("avatar_unknown"));
        setImageDrawable(null);
        setGrayScaled(false);
        setStateDrawable(1);
        C0001a.getImageVersion().removeDelegate(this, 2, i);
        super.setUserID(i);
    }
}
