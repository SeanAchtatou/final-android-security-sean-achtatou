package com.papaya.view;

import android.content.Context;
import android.graphics.PointF;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import com.papaya.si.X;

public class PPYAbsoluteLayout extends ViewGroup {

    public static class LayoutParams extends ViewGroup.LayoutParams {
        public int x;
        public int y;

        public LayoutParams(int i, int i2, int i3, int i4) {
            super(i, i2);
            this.x = i3;
            this.y = i4;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public float centerX() {
            return ((float) this.x) + (((float) this.width) / 2.0f);
        }

        public float centerY() {
            return ((float) this.y) + (((float) this.height) / 2.0f);
        }

        public PointF getCenter() {
            return new PointF(((float) this.x) + (((float) this.width) / 2.0f), ((float) this.y) + (((float) this.height) / 2.0f));
        }

        public void setCenter(float f, float f2) {
            this.x = Math.round(f - (((float) this.width) / 2.0f));
            this.y = Math.round(f2 - (((float) this.height) / 2.0f));
        }

        public void setCenter(PointF pointF) {
            setCenter(pointF.x, pointF.y);
        }
    }

    public PPYAbsoluteLayout(Context context) {
        super(context);
    }

    public static PointF getCenter(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof LayoutParams) {
            return ((LayoutParams) layoutParams).getCenter();
        }
        X.w("invalid layoutparams type %s", layoutParams);
        return null;
    }

    public static void setCenter(View view, PointF pointF) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams instanceof LayoutParams) {
            LayoutParams layoutParams2 = (LayoutParams) layoutParams;
            layoutParams2.setCenter(pointF);
            view.setLayoutParams(layoutParams2);
            return;
        }
        X.w("invalid layoutparams type %s", layoutParams);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-2, -2, 0, 0);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (!(layoutParams instanceof AbsoluteLayout.LayoutParams)) {
            return new LayoutParams(layoutParams);
        }
        AbsoluteLayout.LayoutParams layoutParams2 = (AbsoluteLayout.LayoutParams) layoutParams;
        return new LayoutParams(layoutParams2.width, layoutParams2.height, layoutParams2.x, layoutParams2.y);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int childCount = getChildCount();
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt = getChildAt(i5);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                int paddingLeft = getPaddingLeft() + layoutParams.x;
                int paddingTop = layoutParams.y + getPaddingTop();
                childAt.layout(paddingLeft, paddingTop, childAt.getMeasuredWidth() + paddingLeft, childAt.getMeasuredHeight() + paddingTop);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int childCount = getChildCount();
        measureChildren(i, i2);
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        while (i5 < childCount) {
            View childAt = getChildAt(i5);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                int max = Math.max(i6, layoutParams.x + childAt.getMeasuredWidth());
                i4 = Math.max(i7, layoutParams.y + childAt.getMeasuredHeight());
                i3 = max;
            } else {
                i3 = i6;
                i4 = i7;
            }
            i5++;
            i7 = i4;
            i6 = i3;
        }
        setMeasuredDimension(resolveSize(Math.max(getPaddingLeft() + getPaddingRight() + i6, getSuggestedMinimumWidth()), i), resolveSize(Math.max(getPaddingTop() + getPaddingBottom() + i7, getSuggestedMinimumHeight()), i2));
    }
}
