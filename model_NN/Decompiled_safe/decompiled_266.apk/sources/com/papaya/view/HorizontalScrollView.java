package com.papaya.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.Scroller;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class HorizontalScrollView extends FrameLayout {
    private WeakReference<Delegate> es;
    private long jb;
    private final Rect jc;
    private Scroller jd;
    private boolean je;
    private float jf;
    private boolean jg;
    private View jh;
    private boolean ji;
    private VelocityTracker jj;
    private boolean jk;
    private boolean jl;
    private int jm;

    public interface Delegate {
        void scrollChanged(int i, int i2, int i3, int i4);
    }

    public HorizontalScrollView(Context context) {
        this(context, null);
    }

    public HorizontalScrollView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842880);
    }

    public HorizontalScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.jc = new Rect();
        this.jg = true;
        this.jh = null;
        this.ji = false;
        this.jl = true;
        initScrollView();
        setFillViewport(false);
    }

    private boolean canScroll() {
        View childAt = getChildAt(0);
        if (childAt == null) {
            return false;
        }
        return getWidth() < (childAt.getWidth() + getPaddingLeft()) + getPaddingRight();
    }

    private int clamp(int i, int i2, int i3) {
        if (i2 >= i3 || i < 0) {
            return 0;
        }
        return i2 + i > i3 ? i3 - i2 : i;
    }

    private void doScrollX(int i) {
        if (i == 0) {
            return;
        }
        if (this.jl) {
            smoothScrollBy(i, 0);
        } else {
            scrollBy(i, 0);
        }
    }

    private View findFocusableViewInBounds(boolean z, int i, int i2) {
        ArrayList focusables = getFocusables(2);
        int size = focusables.size();
        boolean z2 = false;
        View view = null;
        for (int i3 = 0; i3 < size; i3++) {
            View view2 = (View) focusables.get(i3);
            int left = view2.getLeft();
            int right = view2.getRight();
            if (i < right && left < i2) {
                boolean z3 = i < left && right < i2;
                if (view == null) {
                    z2 = z3;
                    view = view2;
                } else {
                    boolean z4 = (z && left < view.getLeft()) || (!z && right > view.getRight());
                    if (z2) {
                        if (z3 && z4) {
                            view = view2;
                        }
                    } else if (z3) {
                        z2 = true;
                        view = view2;
                    } else if (z4) {
                        view = view2;
                    }
                }
            }
        }
        return view;
    }

    private View findFocusableViewInMyBounds(boolean z, int i, View view) {
        int horizontalFadingEdgeLength = getHorizontalFadingEdgeLength() / 2;
        int i2 = i + horizontalFadingEdgeLength;
        int width = (getWidth() + i) - horizontalFadingEdgeLength;
        return (view == null || view.getLeft() >= width || view.getRight() <= i2) ? findFocusableViewInBounds(z, i2, width) : view;
    }

    private void initScrollView() {
        this.jd = new Scroller(getContext());
        setFocusable(true);
        setDescendantFocusability(262144);
        setWillNotDraw(false);
        this.jm = ViewConfiguration.getTouchSlop();
    }

    private boolean isOffScreen(View view) {
        return !isWithinDeltaOfScreen(view, 0);
    }

    private boolean isViewDescendantOf(View view, View view2) {
        if (view == view2) {
            return true;
        }
        ViewParent parent = view.getParent();
        return (parent instanceof ViewGroup) && isViewDescendantOf((View) parent, view2);
    }

    private boolean isWithinDeltaOfScreen(View view, int i) {
        view.getDrawingRect(this.jc);
        offsetDescendantRectToMyCoords(view, this.jc);
        return this.jc.right + i >= getScrollX() && this.jc.left - i <= getScrollX() + getWidth();
    }

    private boolean scrollAndFocus(int i, int i2, int i3) {
        boolean z;
        int width = getWidth();
        int scrollX = getScrollX();
        int i4 = width + scrollX;
        boolean z2 = i == 17;
        View findFocusableViewInBounds = findFocusableViewInBounds(z2, i2, i3);
        if (findFocusableViewInBounds == null) {
            findFocusableViewInBounds = this;
        }
        if (i2 < scrollX || i3 > i4) {
            doScrollX(z2 ? i2 - scrollX : i3 - i4);
            z = true;
        } else {
            z = false;
        }
        if (findFocusableViewInBounds != findFocus() && findFocusableViewInBounds.requestFocus(i)) {
            this.je = true;
            this.je = false;
        }
        return z;
    }

    private void scrollToChild(View view) {
        view.getDrawingRect(this.jc);
        offsetDescendantRectToMyCoords(view, this.jc);
        int computeScrollDeltaToGetChildRectOnScreen = computeScrollDeltaToGetChildRectOnScreen(this.jc);
        if (computeScrollDeltaToGetChildRectOnScreen != 0) {
            scrollBy(computeScrollDeltaToGetChildRectOnScreen, 0);
        }
    }

    private boolean scrollToChildRect(Rect rect, boolean z) {
        int computeScrollDeltaToGetChildRectOnScreen = computeScrollDeltaToGetChildRectOnScreen(rect);
        boolean z2 = computeScrollDeltaToGetChildRectOnScreen != 0;
        if (z2) {
            if (z) {
                scrollBy(computeScrollDeltaToGetChildRectOnScreen, 0);
            } else {
                smoothScrollBy(computeScrollDeltaToGetChildRectOnScreen, 0);
            }
        }
        return z2;
    }

    public void addView(View view) {
        if (getChildCount() > 0) {
            throw new IllegalStateException("HorizontalScrollView can host only one direct child");
        }
        super.addView(view);
    }

    public void addView(View view, int i) {
        if (getChildCount() > 0) {
            throw new IllegalStateException("HorizontalScrollView can host only one direct child");
        }
        super.addView(view, i);
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        if (getChildCount() > 0) {
            throw new IllegalStateException("HorizontalScrollView can host only one direct child");
        }
        super.addView(view, i, layoutParams);
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        if (getChildCount() > 0) {
            throw new IllegalStateException("HorizontalScrollView can host only one direct child");
        }
        super.addView(view, layoutParams);
    }

    public boolean arrowScroll(int i) {
        int i2;
        View findFocus = findFocus();
        if (findFocus == this) {
            findFocus = null;
        }
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, findFocus, i);
        int maxScrollAmount = getMaxScrollAmount();
        if (findNextFocus == null || !isWithinDeltaOfScreen(findNextFocus, maxScrollAmount)) {
            if (i != 17 || getScrollX() >= maxScrollAmount) {
                if (i == 66) {
                    int right = getChildAt(getChildCount() - 1).getRight();
                    int scrollX = getScrollX() + getWidth();
                    if (right - scrollX < maxScrollAmount) {
                        i2 = right - scrollX;
                    }
                }
                i2 = maxScrollAmount;
            } else {
                i2 = getScrollX();
            }
            if (i2 == 0) {
                return false;
            }
            if (i != 66) {
                i2 = -i2;
            }
            doScrollX(i2);
        } else {
            findNextFocus.getDrawingRect(this.jc);
            offsetDescendantRectToMyCoords(findNextFocus, this.jc);
            doScrollX(computeScrollDeltaToGetChildRectOnScreen(this.jc));
            findNextFocus.requestFocus(i);
        }
        if (findFocus != null && findFocus.isFocused() && isOffScreen(findFocus)) {
            int descendantFocusability = getDescendantFocusability();
            setDescendantFocusability(131072);
            requestFocus();
            setDescendantFocusability(descendantFocusability);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public int computeHorizontalScrollRange() {
        return getChildCount() == 0 ? getWidth() : getChildAt(0).getRight();
    }

    public void computeScroll() {
        int i;
        int i2;
        if (this.jd.computeScrollOffset()) {
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            int currX = this.jd.getCurrX();
            int currY = this.jd.getCurrY();
            if (getChildCount() > 0) {
                View childAt = getChildAt(0);
                int clamp = clamp(currX, (getWidth() - getPaddingRight()) - getPaddingLeft(), childAt.getWidth());
                int clamp2 = clamp(currY, (getHeight() - getPaddingBottom()) - getPaddingTop(), childAt.getHeight());
                i = clamp;
                i2 = clamp2;
            } else {
                int i3 = currY;
                i = currX;
                i2 = i3;
            }
            super.scrollTo(i, i2);
            if (!(scrollX == getScrollX() && scrollY == getScrollY())) {
                onScrollChanged(getScrollX(), getScrollY(), scrollX, scrollY);
            }
            postInvalidate();
        }
    }

    /* access modifiers changed from: protected */
    public int computeScrollDeltaToGetChildRectOnScreen(Rect rect) {
        int width = getWidth();
        int scrollX = getScrollX();
        int i = scrollX + width;
        int horizontalFadingEdgeLength = getHorizontalFadingEdgeLength();
        if (rect.left > 0) {
            scrollX += horizontalFadingEdgeLength;
        }
        if (rect.right < getChildAt(0).getWidth()) {
            i -= horizontalFadingEdgeLength;
        }
        if (rect.right > i && rect.left > scrollX) {
            return Math.min(rect.width() > width ? (rect.left - scrollX) + 0 : (rect.right - i) + 0, getChildAt(getChildCount() - 1).getRight() - i);
        } else if (rect.left >= scrollX || rect.right >= i) {
            return 0;
        } else {
            return Math.max(rect.width() > width ? 0 - (i - rect.right) : 0 - (scrollX - rect.left), -getScrollX());
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (super.dispatchKeyEvent(keyEvent)) {
            return true;
        }
        return executeKeyEvent(keyEvent);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean executeKeyEvent(KeyEvent keyEvent) {
        this.jc.setEmpty();
        if (canScroll()) {
            if (keyEvent.getAction() == 0) {
                switch (keyEvent.getKeyCode()) {
                    case 21:
                        return !keyEvent.isAltPressed() ? arrowScroll(17) : fullScroll(17);
                    case 22:
                        return !keyEvent.isAltPressed() ? arrowScroll(66) : fullScroll(66);
                    case 62:
                        pageScroll(keyEvent.isShiftPressed() ? 17 : 66);
                        break;
                }
            }
            return false;
        } else if (!isFocused()) {
            return false;
        } else {
            View findFocus = findFocus();
            if (findFocus == this) {
                findFocus = null;
            }
            View findNextFocus = FocusFinder.getInstance().findNextFocus(this, findFocus, 66);
            return (findNextFocus == null || findNextFocus == this || !findNextFocus.requestFocus(66)) ? false : true;
        }
    }

    public void fling(int i) {
        this.jd.fling(getScrollX(), getScrollY(), i, 0, 0, getChildAt(0).getWidth() - ((getWidth() - getPaddingRight()) - getPaddingLeft()), 0, 0);
        boolean z = i > 0;
        View findFocusableViewInMyBounds = findFocusableViewInMyBounds(z, this.jd.getFinalX(), findFocus());
        if (findFocusableViewInMyBounds == null) {
            findFocusableViewInMyBounds = this;
        }
        if (findFocusableViewInMyBounds != findFocus()) {
            if (findFocusableViewInMyBounds.requestFocus(z ? 66 : 17)) {
                this.je = true;
                this.je = false;
            }
        }
        invalidate();
    }

    public boolean fullScroll(int i) {
        int childCount;
        boolean z = i == 66;
        int width = getWidth();
        this.jc.left = 0;
        this.jc.right = width;
        if (z && (childCount = getChildCount()) > 0) {
            this.jc.right = getChildAt(childCount - 1).getRight();
            this.jc.left = this.jc.right - width;
        }
        return scrollAndFocus(i, this.jc.left, this.jc.right);
    }

    public Delegate getDelegate() {
        if (this.es != null) {
            return this.es.get();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public float getLeftFadingEdgeStrength() {
        if (getChildCount() == 0) {
            return 0.0f;
        }
        int horizontalFadingEdgeLength = getHorizontalFadingEdgeLength();
        if (getScrollX() < horizontalFadingEdgeLength) {
            return ((float) getScrollX()) / ((float) horizontalFadingEdgeLength);
        }
        return 1.0f;
    }

    public int getMaxScrollAmount() {
        return (int) (0.5f * ((float) (getRight() - getLeft())));
    }

    /* access modifiers changed from: protected */
    public float getRightFadingEdgeStrength() {
        if (getChildCount() == 0) {
            return 0.0f;
        }
        int horizontalFadingEdgeLength = getHorizontalFadingEdgeLength();
        int right = (getChildAt(0).getRight() - getScrollX()) - (getWidth() - getPaddingRight());
        if (right < horizontalFadingEdgeLength) {
            return ((float) right) / ((float) horizontalFadingEdgeLength);
        }
        return 1.0f;
    }

    public boolean isFillViewport() {
        return this.jk;
    }

    public boolean isSmoothScrollingEnabled() {
        return this.jl;
    }

    /* access modifiers changed from: protected */
    public void measureChild(View view, int i, int i2) {
        view.measure(View.MeasureSpec.makeMeasureSpec(0, 0), getChildMeasureSpec(i2, getPaddingTop() + getPaddingBottom(), view.getLayoutParams().height));
    }

    /* access modifiers changed from: protected */
    public void measureChildWithMargins(View view, int i, int i2, int i3, int i4) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        view.measure(View.MeasureSpec.makeMeasureSpec(marginLayoutParams.rightMargin + marginLayoutParams.leftMargin, 0), getChildMeasureSpec(i3, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i4, marginLayoutParams.height));
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 2 && this.ji) {
            return true;
        }
        if (!canScroll()) {
            this.ji = false;
            return false;
        }
        float x = motionEvent.getX();
        switch (action) {
            case 0:
                this.jf = x;
                this.ji = !this.jd.isFinished();
                break;
            case 1:
            case 3:
                this.ji = false;
                break;
            case 2:
                if (((int) Math.abs(x - this.jf)) > this.jm) {
                    this.ji = true;
                    if (getParent() != null) {
                        getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                    }
                }
                break;
        }
        return this.ji;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.jg = false;
        if (this.jh != null && isViewDescendantOf(this.jh, this)) {
            scrollToChild(this.jh);
        }
        this.jh = null;
        scrollTo(getScrollX(), getScrollY());
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.jk && View.MeasureSpec.getMode(i) != 0) {
            View childAt = getChildAt(0);
            int measuredWidth = getMeasuredWidth();
            if (childAt.getMeasuredHeight() < measuredWidth) {
                childAt.measure(View.MeasureSpec.makeMeasureSpec((measuredWidth - getPaddingLeft()) - getPaddingRight(), 1073741824), getChildMeasureSpec(i2, getPaddingTop() + getPaddingBottom(), ((FrameLayout.LayoutParams) childAt.getLayoutParams()).height));
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i, Rect rect) {
        int i2 = i == 2 ? 66 : i == 1 ? 17 : i;
        View findNextFocus = rect == null ? FocusFinder.getInstance().findNextFocus(this, null, i2) : FocusFinder.getInstance().findNextFocusFromRect(this, rect, i2);
        if (findNextFocus == null) {
            return false;
        }
        if (isOffScreen(findNextFocus)) {
            return false;
        }
        return findNextFocus.requestFocus(i2, rect);
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        Delegate delegate = getDelegate();
        if (delegate != null) {
            delegate.scrollChanged(i, i2, i3, i4);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        View findFocus = findFocus();
        if (findFocus != null && this != findFocus && isWithinDeltaOfScreen(findFocus, getRight() - getLeft())) {
            findFocus.getDrawingRect(this.jc);
            offsetDescendantRectToMyCoords(findFocus, this.jc);
            doScrollX(computeScrollDeltaToGetChildRectOnScreen(this.jc));
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int right;
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        if (!canScroll()) {
            return false;
        }
        if (this.jj == null) {
            this.jj = VelocityTracker.obtain();
        }
        this.jj.addMovement(motionEvent);
        int action = motionEvent.getAction();
        float x = motionEvent.getX();
        switch (action) {
            case 0:
                if (!this.jd.isFinished()) {
                    this.jd.abortAnimation();
                }
                this.jf = x;
                break;
            case 1:
                VelocityTracker velocityTracker = this.jj;
                velocityTracker.computeCurrentVelocity(1000);
                int xVelocity = (int) velocityTracker.getXVelocity();
                if (Math.abs(xVelocity) > ViewConfiguration.getMinimumFlingVelocity() && getChildCount() > 0) {
                    fling(-xVelocity);
                }
                if (this.jj != null) {
                    this.jj.recycle();
                    this.jj = null;
                    break;
                }
                break;
            case 2:
                int i = (int) (this.jf - x);
                this.jf = x;
                if (i >= 0) {
                    if (i > 0 && (right = (getChildAt(0).getRight() - getScrollX()) - (getWidth() - getPaddingRight())) > 0) {
                        scrollBy(Math.min(right, i), 0);
                        break;
                    }
                } else if (getScrollX() > 0) {
                    scrollBy(i, 0);
                    break;
                }
                break;
        }
        return true;
    }

    public boolean pageScroll(int i) {
        boolean z = i == 66;
        int width = getWidth();
        if (z) {
            this.jc.left = getScrollX() + width;
            int childCount = getChildCount();
            if (childCount > 0) {
                View childAt = getChildAt(childCount - 1);
                if (this.jc.left + width > childAt.getRight()) {
                    this.jc.left = childAt.getRight() - width;
                }
            }
        } else {
            this.jc.left = getScrollX() - width;
            if (this.jc.left < 0) {
                this.jc.left = 0;
            }
        }
        this.jc.right = width + this.jc.left;
        return scrollAndFocus(i, this.jc.left, this.jc.right);
    }

    public void requestChildFocus(View view, View view2) {
        if (!this.je) {
            if (!this.jg) {
                scrollToChild(view2);
            } else {
                this.jh = view2;
            }
        }
        super.requestChildFocus(view, view2);
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z) {
        rect.offset(view.getLeft() - view.getScrollX(), view.getTop() - view.getScrollY());
        return scrollToChildRect(rect, z);
    }

    public void requestLayout() {
        this.jg = true;
        super.requestLayout();
    }

    public void scrollTo(int i, int i2) {
        if (getChildCount() > 0) {
            View childAt = getChildAt(0);
            int clamp = clamp(i, (getWidth() - getPaddingRight()) - getPaddingLeft(), childAt.getWidth());
            int clamp2 = clamp(i2, (getHeight() - getPaddingBottom()) - getPaddingTop(), childAt.getHeight());
            if (clamp != getScrollX() || clamp2 != getScrollY()) {
                super.scrollTo(clamp, clamp2);
            }
        }
    }

    public void setDelegate(Delegate delegate) {
        if (delegate == null) {
            this.es = null;
        } else {
            this.es = new WeakReference<>(delegate);
        }
    }

    public void setFillViewport(boolean z) {
        if (z != this.jk) {
            this.jk = z;
            requestLayout();
        }
    }

    public void setSmoothScrollingEnabled(boolean z) {
        this.jl = z;
    }

    public final void smoothScrollBy(int i, int i2) {
        if (AnimationUtils.currentAnimationTimeMillis() - this.jb > 250) {
            this.jd.startScroll(getScrollX(), getScrollY(), i, i2);
            invalidate();
        } else {
            if (!this.jd.isFinished()) {
                this.jd.abortAnimation();
            }
            scrollBy(i, i2);
        }
        this.jb = AnimationUtils.currentAnimationTimeMillis();
    }

    public final void smoothScrollTo(int i, int i2) {
        smoothScrollBy(i - getScrollX(), i2 - getScrollY());
    }
}
