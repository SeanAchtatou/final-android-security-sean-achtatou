package com.papaya.view;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.text.InputFilter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.adknowledge.superrewards.model.SROffer;
import com.papaya.si.C0030bb;
import com.papaya.si.C0034bf;
import com.papaya.si.C0037c;
import com.papaya.si.C0056v;
import com.papaya.si.C0060z;
import com.papaya.si.X;
import com.papaya.si.aV;
import com.papaya.si.bk;
import com.papaya.view.CustomDialog;
import com.papaya.view.TakePhotoBridge;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebMixedInputDialog extends CustomDialog implements DialogInterface.OnClickListener, JsonConfigurable, TakePhotoBridge.Receiver {
    private EditText kH;
    private View kI;
    private EditText kJ;
    private View kK;
    private LinearLayout kL;
    private LinearLayout kM;
    private ImageButton kN;
    private TextView kO;
    private int kP;
    /* access modifiers changed from: private */
    public boolean kQ;
    private int kR;
    private int kS;
    private String kT;
    private JSONArray kU = new JSONArray();
    private a kV = new a(this);
    /* access modifiers changed from: private */
    public int kg;
    private JSONObject kh;
    /* access modifiers changed from: private */
    public bk ki;
    private String kj;
    /* access modifiers changed from: private */
    public int maxHeight;
    /* access modifiers changed from: private */
    public int maxWidth;

    final class a implements View.OnClickListener {
        /* synthetic */ a(WebMixedInputDialog webMixedInputDialog) {
            this((byte) 0);
        }

        private a(byte b) {
        }

        public final void onClick(View view) {
            new CustomDialog.Builder(WebMixedInputDialog.this.getContext()).setItems(new CharSequence[]{"From camera", "From gallery"}, new DialogInterface.OnClickListener() {
                public final void onClick(DialogInterface dialogInterface, int i) {
                    TakePhotoBridge.startTakenPhoto(WebMixedInputDialog.this.ki.getOwnerActivity(), WebMixedInputDialog.this, new TakePhotoBridge.Config(i, WebMixedInputDialog.this.kQ ? Bitmap.CompressFormat.PNG : Bitmap.CompressFormat.JPEG, WebMixedInputDialog.this.maxWidth, WebMixedInputDialog.this.maxHeight, WebMixedInputDialog.this.kg));
                }
            }).show();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.FrameLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public WebMixedInputDialog(Context context, String str, bk bkVar) {
        super(context);
        this.kj = str;
        this.ki = bkVar;
        setView(getLayoutInflater().inflate(C0060z.layoutID("mixed_input_view"), (ViewGroup) this.ip, false));
        this.kH = (EditText) f("singleedittext");
        this.kI = (View) f("separator1");
        this.kJ = (EditText) f("edittext");
        this.kK = (View) f("separator2");
        this.kL = (LinearLayout) f("photos_layout");
        this.kM = (LinearLayout) f("photos_preview");
        this.kN = (ImageButton) f("photo_upload");
        this.kO = (TextView) f("photo_hint");
        this.kN.setOnClickListener(this.kV);
        this.kO.setOnClickListener(this.kV);
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        int dialogButtonToWebIndex = C0034bf.dialogButtonToWebIndex(i);
        if (this.ki == null || aV.isEmpty(this.kT)) {
            X.d("empty callback : webView %s, callback %s", this.ki, this.kT);
            return;
        }
        this.ki.callJSFunc("%s('%s', %d, '%s', '%s', '%s')", this.kT, this.kj, Integer.valueOf(dialogButtonToWebIndex), C0034bf.escapeJS(this.kH.getText()), C0034bf.escapeJS(this.kJ.getText()), C0034bf.escapeJS(this.kU.toString()));
    }

    public void onPhotoCancel() {
        C0030bb.showToast("Cancelled", 1);
    }

    public void onPhotoTaken(String str) {
        this.kU.put(str);
        LazyImageView lazyImageView = new LazyImageView(getContext());
        lazyImageView.setImageUrl(str);
        lazyImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(C0030bb.rp(this.kR), C0030bb.rp(this.kS));
        layoutParams.leftMargin = C0030bb.rp(5);
        this.kM.addView(lazyImageView, layoutParams);
        this.kO.setVisibility(8);
        if (this.kU.length() >= this.kP) {
            this.kN.setVisibility(8);
        }
    }

    public void refreshWithCtx(JSONObject jSONObject) {
        this.kh = jSONObject;
        setTitle(this.kh.optString(SROffer.TITLE, C0056v.bj.toString()));
        int optInt = this.kh.optInt("single_max_length", 0);
        if (optInt > 0) {
            this.kH.setFilters(new InputFilter[]{new InputFilter.LengthFilter(optInt)});
        } else {
            this.kH.setFilters(new InputFilter[0]);
            this.kH.setVisibility(optInt == 0 ? 8 : 0);
        }
        this.kH.setInputType(this.kh.optInt("single_input_type", 1));
        this.kH.setHint(this.kh.optString("single_hint"));
        this.kH.setTextColor(Color.parseColor(this.kh.optString("single_text_color", "#000000")));
        this.kH.setTextSize((float) this.kh.optInt("single_text_size", 16));
        String optString = this.kh.optString("single_text", "");
        this.kH.setText(optString);
        this.kH.setSelection(Math.min(this.kh.optInt("single_cursor", optString.length()), optString.length()));
        int optInt2 = this.kh.optInt("max_length", -1);
        if (optInt2 > 0) {
            this.kJ.setFilters(new InputFilter[]{new InputFilter.LengthFilter(optInt2)});
        } else {
            this.kJ.setFilters(new InputFilter[0]);
            this.kJ.setVisibility(optInt2 == 0 ? 8 : 0);
        }
        this.kJ.setInputType(this.kh.optInt("input_type", 1));
        this.kJ.setHint(this.kh.optString("hint"));
        this.kJ.setTextColor(Color.parseColor(this.kh.optString("text_color", "#929292")));
        this.kJ.setTextSize((float) this.kh.optInt("text_size", 14));
        int optInt3 = this.kh.optInt("min_lines", 1);
        this.kJ.setVisibility(optInt3 <= 0 ? 8 : 0);
        this.kJ.setMinLines(optInt3);
        int optInt4 = this.kh.optInt("max_lines", optInt3);
        this.kJ.setSingleLine(optInt4 <= 1);
        this.kJ.setMaxLines(optInt4);
        String optString2 = this.kh.optString("text", "");
        this.kJ.setText(optString2);
        this.kJ.setSelection(Math.min(this.kh.optInt("cursor", optString2.length()), optString2.length()));
        this.kP = this.kh.optInt("max_photos");
        this.kL.setVisibility(this.kP > 0 ? 0 : 8);
        this.kO.setText(this.kh.optString("photo_hint", C0037c.getString("photo_input_hint")));
        this.kO.setVisibility(0);
        this.kN.setVisibility(0);
        this.maxWidth = this.kh.optInt("max_width", 70);
        this.maxHeight = this.kh.optInt("max_height", 45);
        this.kQ = "png".equals(this.kh.optString("format"));
        this.kg = this.kh.optInt("quality", 35);
        this.kR = this.kh.optInt("preview_width", 70);
        this.kS = this.kh.optInt("preview_width", 45);
        setPositiveButton(this.kh.optString("positive_button", C0037c.getString("default_post_label")), this);
        setNegativeButton(this.kh.optString("negative_button", C0037c.getString("default_cancel_label")), this);
        setNeutralButton(this.kh.optString("neutral_button"), this);
        this.kI.setVisibility(8);
        this.kK.setVisibility(8);
        if (this.kH.getVisibility() == 0 && (this.kJ.getVisibility() == 0 || this.kL.getVisibility() == 0)) {
            this.kI.setVisibility(0);
        }
        if (this.kL.getVisibility() == 0 && (this.kH.getVisibility() == 0 || this.kJ.getVisibility() == 0)) {
            this.kK.setVisibility(0);
        }
        this.kT = this.kh.optString("callback");
        this.kU = new JSONArray();
        this.kM.removeAllViews();
    }

    public void show() {
        if (this.kh != null) {
            refreshWithCtx(this.kh);
        }
        super.show();
    }
}
