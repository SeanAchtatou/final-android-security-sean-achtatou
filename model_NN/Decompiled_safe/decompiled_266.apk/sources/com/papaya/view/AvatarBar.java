package com.papaya.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.adknowledge.superrewards.model.SROffer;
import com.papaya.si.C0001a;
import com.papaya.si.C0026ay;
import com.papaya.si.C0030bb;
import com.papaya.si.C0034bf;
import com.papaya.si.C0060z;
import com.papaya.si.X;
import com.papaya.si.aK;
import com.papaya.si.aU;
import com.papaya.si.aV;
import com.papaya.si.aW;
import com.papaya.si.bk;
import com.papaya.si.bt;
import com.papaya.si.bv;
import com.papaya.view.PPYAbsoluteLayout;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class AvatarBar extends RelativeLayout implements View.OnClickListener, aW, bt.a {
    private int hK;
    private int hL;
    private URL hM;
    private String hN;
    private JSONObject hO;
    private ArrayList<AvatarItem> hP = new ArrayList<>();
    private ArrayList<ImageButton> hQ = new ArrayList<>();
    private ArrayList<bv> hR = new ArrayList<>();
    private ArrayList<bv> hS = new ArrayList<>();
    private bv hT;
    private int hU;
    private HorizontalScrollView hV;
    private PPYAbsoluteLayout hW;
    private ImageView hX;
    private TextView hY;
    private ProgressBar hZ;
    private bk ia;

    public static final class AvatarItem {
        public int ib;
        String ic;
        String ie;

        /* renamed from: if  reason: not valid java name */
        int f1if;
        int ig;
        int x;
        int y;
    }

    public AvatarBar(Context context) {
        super(context);
        setBackgroundColor(C0030bb.color(8881537));
        setupViews(context);
    }

    private void clearImageRequests() {
        C0026ay webCache = C0001a.getWebCache();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.hS.size()) {
                bv bvVar = this.hS.get(i2);
                if (bvVar != null) {
                    bvVar.setDelegate(null);
                    webCache.removeRequest(bvVar);
                }
                i = i2 + 1;
            } else {
                this.hS.clear();
                this.hU = -1;
                return;
            }
        }
    }

    public void clearResources() {
        C0026ay webCache = C0001a.getWebCache();
        for (int i = 0; i < this.hQ.size(); i++) {
            C0030bb.removeFromSuperView(this.hQ.get(i));
        }
        this.hQ.clear();
        for (int i2 = 0; i2 < this.hR.size(); i2++) {
            bv bvVar = this.hR.get(i2);
            if (bvVar != null) {
                bvVar.setDelegate(null);
                webCache.removeRequest(bvVar);
            }
        }
        this.hR.clear();
        clearImageRequests();
        this.hP.clear();
        if (this.hT != null) {
            this.hT.setDelegate(null);
            webCache.removeRequest(this.hT);
            this.hT = null;
        }
        this.hO = null;
        this.hV.scrollTo(0, 0);
    }

    public void connectionFailed(bt btVar, int i) {
        bv request = btVar.getRequest();
        X.w("failed to finish %s", request);
        if (request == this.hT) {
            this.hT = null;
            this.hZ.setVisibility(8);
            this.hY.setText(C0060z.stringID("avatarbar_items_load_fail"));
            this.hY.setVisibility(0);
        } else if (this.hR.contains(request)) {
            this.hR.set(this.hR.indexOf(request), null);
        } else if (this.hS.contains(request)) {
            this.hS.remove(request);
        }
    }

    /* JADX INFO: finally extract failed */
    public void connectionFinished(bt btVar) {
        bv request = btVar.getRequest();
        if (request == this.hT) {
            this.hZ.setVisibility(8);
            this.hO = C0034bf.parseJsonObject(aU.decodeData(btVar.getData(), "UTF-8"));
            if (this.hO.length() > 0) {
                JSONArray jsonArray = C0034bf.getJsonArray(this.hO, "items");
                this.hK = C0034bf.getJsonInt(this.hO, "flag");
                if (jsonArray != null) {
                    int length = jsonArray.length();
                    for (int i = 0; i < length; i += 7) {
                        AvatarItem avatarItem = new AvatarItem();
                        avatarItem.ib = C0034bf.getJsonInt(jsonArray, i);
                        avatarItem.ic = C0034bf.getJsonString(jsonArray, i + 1);
                        avatarItem.ie = C0034bf.getJsonString(jsonArray, i + 2);
                        avatarItem.f1if = C0034bf.getJsonInt(jsonArray, i + 3);
                        avatarItem.x = C0034bf.getJsonInt(jsonArray, i + 4);
                        avatarItem.y = C0034bf.getJsonInt(jsonArray, i + 5);
                        avatarItem.ig = C0034bf.getJsonInt(jsonArray, i + 6);
                        this.hP.add(avatarItem);
                    }
                }
            }
            if (this.hP.isEmpty()) {
                this.hY.setText(C0060z.stringID("avatarbar_empty"));
                this.hY.setVisibility(0);
                return;
            }
            C0026ay webCache = C0001a.getWebCache();
            Resources resources = getContext().getResources();
            for (int i2 = 0; i2 < this.hP.size(); i2++) {
                ImageButton imageButton = new ImageButton(getContext());
                imageButton.setScaleType(ImageView.ScaleType.FIT_CENTER);
                imageButton.setBackgroundDrawable(null);
                imageButton.setPadding(0, 0, 0, 0);
                bv bvVar = new bv();
                bvVar.setDelegate(this);
                aK fdFromPapayaUri = webCache.fdFromPapayaUri(this.hP.get(i2).ic, this.hM, bvVar);
                if (fdFromPapayaUri != null) {
                    imageButton.setImageDrawable(C0030bb.drawableFromFD(fdFromPapayaUri));
                    imageButton.setEnabled(true);
                } else {
                    imageButton.setImageDrawable(resources.getDrawable(C0060z.drawableID("avatar_item_default")));
                    imageButton.setEnabled(false);
                }
                if (bvVar.getUrl() != null) {
                    bvVar.setSaveFile(webCache.cachedFile(bvVar.getUrl().toString(), false));
                    this.hR.add(bvVar);
                } else {
                    this.hR.add(null);
                }
                imageButton.setLayoutParams(C0030bb.rawPPYAbsoluteLayoutParams(getContext(), 54, 54, (i2 * 58) + 3, 3));
                imageButton.setOnClickListener(this);
                this.hW.addView(imageButton);
                this.hQ.add(imageButton);
            }
            webCache.insertRequests(this.hR);
        } else if (this.hR.contains(request)) {
            int indexOf = this.hR.indexOf(request);
            this.hR.set(indexOf, null);
            ImageButton imageButton2 = this.hQ.get(indexOf);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(btVar.getData());
            try {
                imageButton2.setImageDrawable(Drawable.createFromStream(byteArrayInputStream, SROffer.ICON));
                aU.close(byteArrayInputStream);
                imageButton2.setEnabled(true);
            } catch (Throwable th) {
                aU.close(byteArrayInputStream);
                throw th;
            }
        } else {
            this.hS.remove(request);
            if (this.hU >= 0 && this.hS.isEmpty()) {
                C0026ay webCache2 = C0001a.getWebCache();
                AvatarItem avatarItem2 = this.hP.get(this.hU);
                ArrayList arrayList = new ArrayList();
                for (String contentUriFromPapayaUri : avatarItem2.ie.split("~")) {
                    String contentUriFromPapayaUri2 = webCache2.contentUriFromPapayaUri(contentUriFromPapayaUri, this.hM, null);
                    if (contentUriFromPapayaUri2 != null) {
                        arrayList.add(contentUriFromPapayaUri2);
                    } else {
                        X.w("should not be null here", new Object[0]);
                    }
                }
                String contentUriFromPapayaUri3 = webCache2.contentUriFromPapayaUri(avatarItem2.ic, this.hM, null);
                if (contentUriFromPapayaUri3 == null) {
                    contentUriFromPapayaUri3 = webCache2.relativeUriFromPapayaUri(avatarItem2.ic);
                }
                String concatStrings = aV.concatStrings("~", (String[]) arrayList.toArray(new String[arrayList.size()]));
                if (this.ia != null) {
                    bk bkVar = this.ia;
                    Object[] objArr = new Object[9];
                    objArr[0] = Integer.valueOf(this.hU == this.hL ? 1 : 0);
                    objArr[1] = Integer.valueOf(this.hK);
                    objArr[2] = Integer.valueOf(avatarItem2.ib);
                    objArr[3] = concatStrings;
                    objArr[4] = contentUriFromPapayaUri3;
                    objArr[5] = Integer.valueOf(avatarItem2.f1if);
                    objArr[6] = Integer.valueOf(avatarItem2.x);
                    objArr[7] = Integer.valueOf(avatarItem2.y);
                    objArr[8] = Integer.valueOf(avatarItem2.ig);
                    bkVar.callJS(aV.format("avatarbartapped(%d, %d, %d, '%s', '%s', %d, %d, %d, %d)", objArr));
                }
            }
        }
    }

    public int getSelectedIndex() {
        return this.hL;
    }

    public bk getWebView() {
        return this.ia;
    }

    public void onClick(View view) {
        int indexOf = this.hQ.indexOf(view);
        if (this.hR.get(indexOf) != null) {
            X.w("request is not null %d", Integer.valueOf(indexOf));
            return;
        }
        PPYAbsoluteLayout.LayoutParams layoutParams = (PPYAbsoluteLayout.LayoutParams) this.hX.getLayoutParams();
        PPYAbsoluteLayout.LayoutParams layoutParams2 = (PPYAbsoluteLayout.LayoutParams) ((ImageButton) view).getLayoutParams();
        if (Math.abs(layoutParams.centerX() - layoutParams2.centerX()) >= 1.0f) {
            layoutParams.setCenter(layoutParams2.getCenter());
            this.hX.setVisibility(0);
            this.hX.setLayoutParams(layoutParams);
        } else if (this.hX.getVisibility() == 0) {
            this.hX.setVisibility(4);
        } else {
            this.hX.setVisibility(0);
        }
        this.hL = this.hX.getVisibility() == 0 ? indexOf : -1;
        clearImageRequests();
        C0026ay webCache = C0001a.getWebCache();
        ArrayList arrayList = new ArrayList();
        AvatarItem avatarItem = this.hP.get(indexOf);
        if (avatarItem.ie != null) {
            for (String contentUriFromPapayaUri : avatarItem.ie.split("~")) {
                bv bvVar = new bv();
                bvVar.setDelegate(this);
                String contentUriFromPapayaUri2 = webCache.contentUriFromPapayaUri(contentUriFromPapayaUri, this.hM, bvVar);
                if (contentUriFromPapayaUri2 != null) {
                    arrayList.add(contentUriFromPapayaUri2);
                } else if (bvVar.getUrl() != null) {
                    this.hS.add(bvVar);
                }
            }
            String contentUriFromPapayaUri3 = webCache.contentUriFromPapayaUri(avatarItem.ic, this.hM, null);
            if (contentUriFromPapayaUri3 == null) {
                contentUriFromPapayaUri3 = webCache.relativeUriFromPapayaUri(avatarItem.ic);
            }
            if (this.hS.isEmpty()) {
                String concatStrings = aV.concatStrings("~", (String[]) arrayList.toArray(new String[arrayList.size()]));
                if (this.ia != null) {
                    bk bkVar = this.ia;
                    Object[] objArr = new Object[9];
                    objArr[0] = Integer.valueOf(indexOf == this.hL ? 1 : 0);
                    objArr[1] = Integer.valueOf(this.hK);
                    objArr[2] = Integer.valueOf(avatarItem.ib);
                    objArr[3] = concatStrings;
                    objArr[4] = contentUriFromPapayaUri3;
                    objArr[5] = Integer.valueOf(avatarItem.f1if);
                    objArr[6] = Integer.valueOf(avatarItem.x);
                    objArr[7] = Integer.valueOf(avatarItem.y);
                    objArr[8] = Integer.valueOf(avatarItem.ig);
                    bkVar.callJS(aV.format("avatarbartapped(%d, %d, %d, '%s', '%s', %d, %d, %d, %d)", objArr));
                    return;
                }
                return;
            }
            webCache.insertRequests(this.hS);
            this.hU = indexOf;
            if (this.ia != null) {
                bk bkVar2 = this.ia;
                Object[] objArr2 = new Object[8];
                objArr2[0] = Integer.valueOf(indexOf == this.hL ? 1 : 0);
                objArr2[1] = Integer.valueOf(this.hK);
                objArr2[2] = Integer.valueOf(avatarItem.ib);
                objArr2[3] = contentUriFromPapayaUri3;
                objArr2[4] = Integer.valueOf(avatarItem.f1if);
                objArr2[5] = Integer.valueOf(avatarItem.x);
                objArr2[6] = Integer.valueOf(avatarItem.y);
                objArr2[7] = Integer.valueOf(avatarItem.ig);
                bkVar2.callJS(aV.format("avatarbartapped(%d, %d, %d, '', '%s', %d, %d, %d, %d)", objArr2));
            }
        }
    }

    public void refreshWithCtx(JSONObject jSONObject, URL url) {
        this.hM = url;
        this.hX.setVisibility(8);
        this.hZ.setVisibility(8);
        this.hY.setVisibility(8);
        this.hY.setText(C0060z.stringID("avatarbar_empty"));
        this.hL = -1;
        this.hK = -1;
        clearResources();
        this.hN = C0034bf.getJsonString(jSONObject, "items_url");
        if (this.hN != null) {
            C0026ay webCache = C0001a.getWebCache();
            this.hM = C0034bf.createURL(this.hN, url);
            if (this.hM != null) {
                this.hT = new bv(this.hM, false);
                this.hT.setConnectionType(1);
                this.hT.setDelegate(this);
                webCache.insertRequest(this.hT);
                this.hZ.setVisibility(0);
                return;
            }
            return;
        }
        this.hY.setVisibility(0);
    }

    public void scrollToItem(int i, boolean z) {
        PPYAbsoluteLayout.LayoutParams layoutParams = (PPYAbsoluteLayout.LayoutParams) this.hQ.get(i).getLayoutParams();
        if (!z) {
            this.hV.scrollTo(layoutParams.x, 0);
        } else {
            this.hV.smoothScrollTo(layoutParams.x, 0);
        }
    }

    public void selectButton(int i, boolean z) {
        if (this.hL == i) {
            if (!z) {
                this.hL = -1;
                this.hX.setVisibility(8);
            }
        } else if (z) {
            PPYAbsoluteLayout.setCenter(this.hX, PPYAbsoluteLayout.getCenter(this.hQ.get(i).getLayoutParams()));
            this.hX.setVisibility(0);
            this.hL = i;
            scrollToItem(i, true);
        }
    }

    public void setSelectedIndex(int i) {
        this.hL = i;
    }

    public void setWebView(bk bkVar) {
        this.ia = bkVar;
    }

    public void setupViews(Context context) {
        Resources resources = context.getResources();
        this.hV = new HorizontalScrollView(context);
        this.hV.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.hV.setHorizontalScrollBarEnabled(true);
        this.hV.setScrollBarStyle(50331648);
        addView(this.hV);
        this.hZ = new ProgressBar(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(C0030bb.rp(50), -1);
        layoutParams.addRule(13);
        this.hZ.setLayoutParams(layoutParams);
        this.hZ.setIndeterminate(true);
        addView(this.hZ);
        this.hZ.setVisibility(8);
        this.hY = new TextView(context);
        this.hY.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.hY.setGravity(17);
        this.hY.setText(C0060z.stringID("avatarbar_empty"));
        this.hY.setTextSize(1, 24.0f);
        this.hY.setVisibility(8);
        addView(this.hY);
        this.hW = new PPYAbsoluteLayout(context);
        this.hV.addView(this.hW, new FrameLayout.LayoutParams(-2, -1));
        this.hX = new ImageView(context);
        this.hX.setImageDrawable(resources.getDrawable(C0060z.drawableID("avatar_selected")));
        this.hX.setLayoutParams(new PPYAbsoluteLayout.LayoutParams(C0030bb.rp(60), C0030bb.rp(60), 0, 0));
        this.hW.addView(this.hX);
    }
}
