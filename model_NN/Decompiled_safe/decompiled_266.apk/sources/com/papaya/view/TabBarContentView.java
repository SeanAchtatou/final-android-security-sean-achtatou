package com.papaya.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.LinearLayout;
import com.papaya.si.C0030bb;
import java.util.ArrayList;
import java.util.Iterator;

public class TabBarContentView extends LinearLayout {
    /* access modifiers changed from: private */
    public OnTabSelectionListener jX;
    /* access modifiers changed from: private */
    public ArrayList<TabItem> jY = new ArrayList<>();
    private int jZ = 0;

    public interface OnTabSelectionListener {
        void onTabSelected(TabBarContentView tabBarContentView, int i);
    }

    class a implements View.OnClickListener {
        private int index;

        /* synthetic */ a(TabBarContentView tabBarContentView, int i) {
            this(i, (byte) 0);
        }

        private a(int i, byte b) {
            this.index = i;
        }

        public final void onClick(View view) {
            TabBarContentView.this.setFocusedTab(TabBarContentView.this.jY.indexOf(view));
            if (TabBarContentView.this.jX != null) {
                TabBarContentView.this.jX.onTabSelected(TabBarContentView.this, this.index);
            }
        }
    }

    public TabBarContentView(Context context) {
        super(context);
        initTabBar();
    }

    public void addTab(Drawable drawable, String str) {
        TabItem tabItem = new TabItem(getContext());
        tabItem.getImageView().setImageDrawable(drawable);
        tabItem.getTextView().setText(str);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, C0030bb.rp(63), 1.0f);
        layoutParams.setMargins(0, 0, 0, 0);
        tabItem.setLayoutParams(layoutParams);
        tabItem.setFocusable(true);
        tabItem.setClickable(true);
        tabItem.setOnClickListener(new a(this, this.jY.size()));
        this.jY.add(tabItem);
        addView(tabItem);
    }

    public void childDrawableStateChanged(View view) {
        if (this.jY.size() > 0 && view == this.jY.get(this.jZ)) {
            invalidate();
        }
        super.childDrawableStateChanged(view);
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i, int i2) {
        return i2 == i - 1 ? this.jZ : i2 >= this.jZ ? i2 + 1 : i2;
    }

    public OnTabSelectionListener getOnTabSelectionListener() {
        return this.jX;
    }

    public int getTabCount() {
        return this.jY.size();
    }

    public TabItem getTabItem(int i) {
        return this.jY.get(i);
    }

    /* access modifiers changed from: protected */
    public void initTabBar() {
        setOrientation(0);
        setMinimumHeight(C0030bb.rp(63));
    }

    public void setFocusedTab(int i) {
        if (i >= 0 && i < this.jY.size()) {
            Iterator<TabItem> it = this.jY.iterator();
            while (it.hasNext()) {
                it.next().setSelected(false);
            }
            this.jY.get(i).setSelected(true);
            this.jZ = i;
        }
    }

    public void setOnTabSelectionListener(OnTabSelectionListener onTabSelectionListener) {
        this.jX = onTabSelectionListener;
    }
}
