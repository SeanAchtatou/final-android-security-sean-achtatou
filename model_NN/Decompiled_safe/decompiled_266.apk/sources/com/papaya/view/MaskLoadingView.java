package com.papaya.view;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import android.widget.RelativeLayout;
import com.papaya.si.C0030bb;
import com.papaya.si.C0060z;

public class MaskLoadingView extends RelativeLayout implements ViewControl {
    /* access modifiers changed from: private */
    public LoadingView ju;
    private int jv;
    private a jw;
    private int position;

    public static final class Position {
        public static final int BOTTOM = 2;
        public static final int CENTER = 0;
        public static final int TOP = 1;
    }

    public static final class Style {
        public static final int FILL = 0;
        public static final int WRAP = 1;
    }

    class a implements Runnable {
        boolean eX;

        /* synthetic */ a(MaskLoadingView maskLoadingView) {
            this((byte) 0);
        }

        private a(byte b) {
            this.eX = false;
        }

        public final void run() {
            if (!this.eX) {
                MaskLoadingView.this.ju.getTextView().setText(C0060z.stringID("web_loading_timeout"));
            }
        }
    }

    public MaskLoadingView(Context context) {
        this(context, 1, 2);
    }

    public MaskLoadingView(Context context, int i, int i2) {
        super(context);
        setBackgroundColor(0);
        setClickable(true);
        this.position = i2;
        this.jv = i;
        setupViews();
    }

    private void setupViews() {
        this.ju = new LoadingView(getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.jv == 0 ? -1 : -2, -2);
        if (this.position == 0) {
            layoutParams.addRule(14);
            layoutParams.addRule(15);
        } else {
            layoutParams.addRule(this.position == 2 ? 12 : 10);
            layoutParams.addRule(14);
        }
        layoutParams.setMargins(0, 0, 0, C0030bb.rp(20));
        addView(this.ju, layoutParams);
    }

    /* access modifiers changed from: package-private */
    public void cancelTimeoutTask() {
        if (this.jw != null) {
            this.jw.eX = true;
            this.jw = null;
        }
    }

    public LoadingView getLoadingView() {
        return this.ju;
    }

    public void hide(boolean z) {
        C0030bb.removeFromSuperView(this);
        cancelTimeoutTask();
    }

    public void setText(String str) {
        this.ju.getTextView().setText(str);
    }

    public void setVisibility(int i) {
        super.setVisibility(i);
        if (i == 0) {
            this.ju.fixAnimationBug();
        }
        this.ju.getTextView().setText(C0060z.stringID("web_loading"));
        if (i != 0) {
            cancelTimeoutTask();
        } else {
            startTimeoutTask();
        }
    }

    public void showInView(ViewGroup viewGroup, boolean z) {
        if (viewGroup instanceof AbsoluteLayout) {
            setLayoutParams(new AbsoluteLayout.LayoutParams(-1, -1, 0, 0));
        } else if (viewGroup instanceof RelativeLayout) {
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        }
        C0030bb.addView(viewGroup, this, true);
        startTimeoutTask();
        this.ju.fixAnimationBug();
    }

    /* access modifiers changed from: package-private */
    public void startTimeoutTask() {
        cancelTimeoutTask();
        this.jw = new a(this);
        C0030bb.postDelayed(this.jw, 10000);
    }
}
