package com.papaya.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import com.papaya.si.C0030bb;
import com.papaya.si.C0037c;
import com.papaya.si.O;

public class EmoticonPanelView extends GridView implements AdapterView.OnItemClickListener {
    private Delegate iS;

    public interface Delegate {
        void onEmoticonSelected(EmoticonPanelView emoticonPanelView, int i, String str);
    }

    public static class IconAdapter extends BaseAdapter {
        private Context ck;

        public IconAdapter(Context context) {
            this.ck = context;
        }

        public int getCount() {
            return 42;
        }

        public Object getItem(int i) {
            return null;
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ImageView imageView;
            if (view == null) {
                imageView = new ImageView(this.ck);
                imageView.setLayoutParams(new AbsListView.LayoutParams(C0030bb.rp(32), C0030bb.rp(32)));
                imageView.setAdjustViewBounds(false);
                imageView.setPadding(C0030bb.rp(2), C0030bb.rp(2), C0030bb.rp(2), C0030bb.rp(2));
            } else {
                imageView = (ImageView) view;
            }
            imageView.setImageDrawable(C0037c.getDrawable("e" + i));
            return imageView;
        }
    }

    public EmoticonPanelView(Context context) {
        super(context);
        setPadding(C0030bb.rp(10), C0030bb.rp(10), C0030bb.rp(10), C0030bb.rp(10));
        setVerticalSpacing(C0030bb.rp(10));
        setHorizontalSpacing(C0030bb.rp(10));
        setColumnWidth(C0030bb.rp(32));
        setStretchMode(2);
        setGravity(80);
        setNumColumns(-1);
        setAdapter((ListAdapter) new IconAdapter(context));
        setOnItemClickListener(this);
    }

    public Delegate getDelegate() {
        return this.iS;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        if (this.iS != null) {
            this.iS.onEmoticonSelected(this, i, O.getEmoticonString(i));
        }
    }

    public void setDelegate(Delegate delegate) {
        this.iS = delegate;
    }
}
