package com.papaya.view;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.adknowledge.superrewards.model.SROffer;
import com.papaya.si.C0034bf;
import com.papaya.si.C0037c;
import com.papaya.si.C0060z;
import com.papaya.si.aV;
import com.papaya.si.bk;
import org.json.JSONObject;

public class WebMultipleInputView extends CustomDialog implements DialogInterface.OnClickListener {
    private EditText kJ;
    private Button kY;
    /* access modifiers changed from: private */
    public JSONObject kh;
    /* access modifiers changed from: private */
    public bk ki;

    public WebMultipleInputView(Context context) {
        super(context);
        View inflate = LayoutInflater.from(context).inflate(C0060z.layoutID("multilayout"), (ViewGroup) null);
        setView(inflate);
        this.kY = (Button) inflate.findViewById(C0060z.id("lbspic"));
        this.kJ = (EditText) inflate.findViewById(C0060z.id("multitext"));
        setButton(-1, C0037c.getApplicationContext().getString(C0060z.stringID("base_cancel")), this);
    }

    public void configureWithJson(JSONObject jSONObject) {
        this.kh = jSONObject;
        if (C0034bf.getJsonString(jSONObject, "initValue") != null) {
            this.kJ.setHint(C0034bf.getJsonString(jSONObject, "initValue"));
        }
        String jsonString = C0034bf.getJsonString(jSONObject, "actionbtn", C0037c.getApplicationContext().getString(C0060z.stringID("done")));
        if (jsonString == null) {
            jsonString = C0037c.getApplicationContext().getString(C0060z.stringID("done"));
        }
        setButton(-2, jsonString, this);
        if (C0034bf.getJsonString(jSONObject, "attach") != null && "1".equals(C0034bf.getJsonString(jSONObject, "attach", "0"))) {
            this.kY.setVisibility(0);
            this.kY.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    WebMultipleInputView.this.dismiss();
                    WebMultipleInputView.this.ki.callJS(aV.format("%s('%s')", C0034bf.getJsonString(WebMultipleInputView.this.kh, "action"), C0034bf.getJsonString(WebMultipleInputView.this.kh, "lcid")));
                }
            });
        }
        setTitle(C0034bf.getJsonString(jSONObject, SROffer.TITLE));
    }

    public bk getWebView() {
        return this.ki;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == -2) {
            this.ki.callJS(aV.format("%s('%s','%s')", C0034bf.getJsonString(this.kh, "callback"), C0034bf.getJsonString(this.kh, (String) SROffer.ID), C0034bf.escapeJS(this.kJ.getText().toString())));
        }
    }

    public void setWebView(bk bkVar) {
        this.ki = bkVar;
    }
}
