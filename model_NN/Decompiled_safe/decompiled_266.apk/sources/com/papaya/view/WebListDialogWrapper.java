package com.papaya.view;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.adknowledge.superrewards.model.SROffer;
import com.papaya.si.C0001a;
import com.papaya.si.C0026ay;
import com.papaya.si.C0030bb;
import com.papaya.si.C0034bf;
import com.papaya.si.C0060z;
import com.papaya.si.aK;
import com.papaya.si.aU;
import com.papaya.si.aV;
import com.papaya.si.bk;
import com.papaya.si.bt;
import com.papaya.si.bv;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebListDialogWrapper implements AdapterView.OnItemClickListener, bt.a {
    private JSONObject iT;
    private bk ia;
    private CustomDialog ko;
    /* access modifiers changed from: private */
    public ArrayList<bv> kp = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<Drawable> kq = new ArrayList<>();
    /* access modifiers changed from: private */
    public JSONArray kr;
    private ListView ks;
    /* access modifiers changed from: private */
    public a kt;

    class a extends BaseAdapter {
        private LayoutInflater kw;

        /* synthetic */ a(WebListDialogWrapper webListDialogWrapper, LayoutInflater layoutInflater) {
            this(layoutInflater, (byte) 0);
        }

        private a(LayoutInflater layoutInflater, byte b) {
            this.kw = layoutInflater;
        }

        public final int getCount() {
            if (WebListDialogWrapper.this.kr == null) {
                return 0;
            }
            return WebListDialogWrapper.this.kr.length();
        }

        public final Object getItem(int i) {
            return Integer.valueOf(i);
        }

        public final long getItemId(int i) {
            return (long) i;
        }

        public final View getView(int i, View view, ViewGroup viewGroup) {
            b bVar;
            View view2;
            if (view == null) {
                View inflate = this.kw.inflate(C0060z.layoutID("list_item_3_part_inverse"), (ViewGroup) null);
                b bVar2 = new b();
                bVar2.kx = (ImageView) inflate.findViewById(C0060z.id("list_item_3_header"));
                bVar2.ky = (TextView) inflate.findViewById(C0060z.id("list_item_3_content"));
                bVar2.kz = (ImageView) inflate.findViewById(C0060z.id("list_item_3_accessory"));
                inflate.setTag(bVar2);
                b bVar3 = bVar2;
                view2 = inflate;
                bVar = bVar3;
            } else {
                bVar = (b) view.getTag();
                view2 = view;
            }
            JSONObject jsonObject = C0034bf.getJsonObject(WebListDialogWrapper.this.kr, i);
            bVar.ky.setText(C0034bf.getJsonString(jsonObject, "text"));
            Drawable drawable = (Drawable) WebListDialogWrapper.this.kq.get(i);
            if (drawable != null) {
                bVar.kx.setImageDrawable(drawable);
                bVar.kx.setVisibility(0);
                bVar.kx.setBackgroundColor(0);
            } else {
                bVar.kx.setVisibility(4);
            }
            if (aV.intValue(C0034bf.getJsonString(jsonObject, "selected"), -1) == 1) {
                bVar.kz.setVisibility(0);
                bVar.kz.setImageDrawable(this.kw.getContext().getResources().getDrawable(C0060z.drawableID("ic_check_mark_light")));
                bVar.kz.setBackgroundColor(0);
            } else {
                bVar.kz.setVisibility(4);
            }
            return view2;
        }
    }

    static class b {
        ImageView kx;
        TextView ky;
        ImageView kz;

        /* synthetic */ b() {
            this((byte) 0);
        }

        private b(byte b) {
        }
    }

    public WebListDialogWrapper(CustomDialog customDialog, bk bkVar, JSONObject jSONObject) {
        this.ko = customDialog;
        this.ia = bkVar;
        this.iT = jSONObject;
        configure();
    }

    private void clearResources() {
        C0026ay webCache = C0001a.getWebCache();
        Iterator<bv> it = this.kp.iterator();
        while (it.hasNext()) {
            bv next = it.next();
            if (next != null) {
                webCache.removeRequest(next);
                next.setDelegate(null);
            }
        }
        this.kp.clear();
        this.kq.clear();
    }

    private void configure() {
        String jsonString = C0034bf.getJsonString(this.iT, SROffer.TITLE);
        if (jsonString == null) {
            jsonString = this.ko.getContext().getString(C0060z.stringID("web_selector_title"));
        }
        this.ko.setTitle(jsonString);
        this.ko.setIcon(C0060z.drawableID("alert_icon_check"));
        LayoutInflater layoutInflater = (LayoutInflater) this.ko.getContext().getSystemService("layout_inflater");
        this.ks = (ListView) layoutInflater.inflate(C0060z.layoutID("list_dialog"), (ViewGroup) null);
        this.kr = C0034bf.getJsonArray(this.iT, "options");
        URL papayaURL = this.ia.getPapayaURL();
        if (this.kr != null) {
            C0026ay webCache = C0001a.getWebCache();
            for (int i = 0; i < this.kr.length(); i++) {
                this.kq.add(null);
                this.kp.add(null);
                JSONObject jsonObject = C0034bf.getJsonObject(this.kr, i);
                if (!"separator".equals(C0034bf.getJsonString(jsonObject, SROffer.TYPE))) {
                    String jsonString2 = C0034bf.getJsonString(jsonObject, SROffer.ICON);
                    if (!aV.isEmpty(jsonString2)) {
                        bv bvVar = new bv();
                        bvVar.setDelegate(this);
                        aK fdFromPapayaUri = webCache.fdFromPapayaUri(jsonString2, papayaURL, bvVar);
                        if (fdFromPapayaUri != null) {
                            this.kq.set(i, C0030bb.drawableFromFD(fdFromPapayaUri));
                        } else if (bvVar.getUrl() != null) {
                            this.kp.set(i, bvVar);
                        }
                    }
                }
            }
            webCache.insertRequests(this.kp);
        }
        this.kt = new a(this, layoutInflater);
        this.ks.setAdapter((ListAdapter) this.kt);
        this.ks.setBackgroundResource(17170447);
        this.ks.setOnItemClickListener(this);
        this.ko.setView(this.ks);
    }

    public void connectionFailed(final bt btVar, int i) {
        C0030bb.post(new Runnable() {
            public final void run() {
                int indexOf = WebListDialogWrapper.this.kp.indexOf(btVar.getRequest());
                if (indexOf != -1) {
                    WebListDialogWrapper.this.kp.set(indexOf, null);
                }
            }
        });
    }

    public void connectionFinished(final bt btVar) {
        C0030bb.post(new Runnable() {
            public final void run() {
                int indexOf = WebListDialogWrapper.this.kp.indexOf(btVar.getRequest());
                if (indexOf != -1) {
                    WebListDialogWrapper.this.kp.set(indexOf, null);
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(btVar.getData());
                    try {
                        WebListDialogWrapper.this.kq.set(indexOf, Drawable.createFromStream(byteArrayInputStream, SROffer.ICON));
                        WebListDialogWrapper.this.kt.notifyDataSetChanged();
                    } finally {
                        aU.close(byteArrayInputStream);
                    }
                }
            }
        });
    }

    public CustomDialog getDialog() {
        return this.ko;
    }

    public bk getWebView() {
        return this.ia;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        JSONObject jsonObject = C0034bf.getJsonObject(this.kr, i);
        if (this.ia != null) {
            Object jsonValue = C0034bf.getJsonValue(jsonObject, "value");
            String jsonString = C0034bf.getJsonString(jsonObject, "text");
            String jsonString2 = C0034bf.getJsonString(jsonObject, SROffer.ICON);
            String jsonString3 = C0034bf.getJsonString(this.iT, "valueid");
            String jsonString4 = C0034bf.getJsonString(this.iT, "textid");
            String jsonString5 = C0034bf.getJsonString(this.iT, "action");
            if (!(jsonString3 == null || jsonValue == null)) {
                if (jsonValue instanceof String) {
                    this.ia.callJS(aV.format("%s='%s'", jsonString3, C0034bf.escapeJS((String) jsonValue)));
                } else {
                    this.ia.callJS(aV.format("%s=%s", jsonString3, jsonValue));
                }
            }
            if (jsonString4 != null) {
                if (jsonString != null) {
                    this.ia.callJS(aV.format("%s='%s'", jsonString4, C0034bf.escapeJS(jsonString)));
                } else {
                    this.ia.callJS(aV.format("%s='%s'", jsonString4, C0034bf.escapeJS(jsonString2)));
                }
            }
            if (!(jsonString5 == null || jsonValue == null)) {
                if (jsonValue instanceof String) {
                    this.ia.callJS(aV.format("%s('%s')", jsonString5, C0034bf.escapeJS((String) jsonValue)));
                } else {
                    this.ia.callJS(aV.format("%s(%s)", jsonString5, jsonValue));
                }
            }
        }
        this.ko.dismiss();
        clearResources();
    }

    public void setWebView(bk bkVar) {
        this.ia = bkVar;
    }
}
