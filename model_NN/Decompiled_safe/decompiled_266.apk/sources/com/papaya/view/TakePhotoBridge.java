package com.papaya.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import com.papaya.si.C0001a;
import com.papaya.si.C0026ay;
import com.papaya.si.C0030bb;
import com.papaya.si.X;
import com.papaya.si.aU;
import com.papaya.si.bz;
import java.io.File;

public class TakePhotoBridge {
    private static Receiver kc;
    private static Config kd;

    public static final class Config {
        public int height;
        public int ke;
        public Bitmap.CompressFormat kf;
        public int kg;
        public int width;

        public Config(int i, Bitmap.CompressFormat compressFormat, int i2, int i3, int i4) {
            this.ke = i;
            this.kf = compressFormat;
            this.width = i2;
            this.height = i3;
            this.kg = i4;
        }
    }

    public interface Receiver {
        void onPhotoCancel();

        void onPhotoTaken(String str);
    }

    public static void clear() {
        kd = null;
        kc = null;
    }

    public static void onPhtotoTaken(Activity activity, int i, int i2, Intent intent) {
        Bitmap createScaledBitmap;
        if (kc == null) {
            X.e("photo receiver is null", new Object[0]);
            return;
        }
        if (i2 == -1) {
            if (i == 13) {
                try {
                    createScaledBitmap = C0030bb.getCameraBitmap(activity, intent, kd.width, kd.height, true);
                } catch (Exception e) {
                    X.e(e, "Failed to process taken photo in bridge", new Object[0]);
                }
            } else {
                createScaledBitmap = C0030bb.createScaledBitmap(activity.getContentResolver(), intent.getData(), kd.width, kd.height, true);
            }
            C0026ay webCache = C0001a.getWebCache();
            if (createScaledBitmap != null) {
                File cacheFile = webCache.getCacheFile(String.valueOf(System.currentTimeMillis()));
                aU.saveBitmap(createScaledBitmap, cacheFile, kd.kf, kd.kg);
                kc.onPhotoTaken(bz.mW + cacheFile.getName());
            } else {
                kc.onPhotoCancel();
            }
        } else {
            kc.onPhotoCancel();
        }
        kc = null;
    }

    public static void startTakenPhoto(Activity activity, Receiver receiver, Config config) {
        kc = receiver;
        kd = config;
        if (config.ke == 0) {
            C0030bb.startCameraActivity(activity, 13);
        } else {
            C0030bb.startGalleryActivity(activity, 14);
        }
    }
}
