package com.papaya.view;

import android.widget.AbsoluteLayout;
import com.papaya.si.bk;

public class AdWrapperView {
    public static final int PPY_Ad_Admob = 0;
    public static final int PPY_Ad_Adsense = 2;
    public static final int PPY_Ad_Flurry = 3;
    public static final int PPY_Ad_Show_Mode_Fix = 0;
    public static final int PPY_Ad_Show_Mode_Float_Bottom = 1;
    public static final int PPY_Ad_Tapjoy = 1;
    public static boolean runTapjoyInstance = false;
    public int hJ;

    public AdWrapperView(int i) {
        this.hJ = i;
    }

    public AdWrapperView(int i, AbsoluteLayout.LayoutParams layoutParams) {
        this.hJ = i;
    }

    public void addToView(bk bkVar) {
        if (bkVar != null && bkVar.getOwnerActivity() == null) {
        }
    }

    public void removeFromSuperView() {
    }

    public void setParams(AbsoluteLayout.LayoutParams layoutParams) {
    }
}
