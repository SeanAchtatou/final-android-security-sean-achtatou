package com.papaya.view;

import android.view.ViewGroup;

public interface ViewControl {
    void hide(boolean z);

    void showInView(ViewGroup viewGroup, boolean z);
}
