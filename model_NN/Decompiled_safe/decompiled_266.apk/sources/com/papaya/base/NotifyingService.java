package com.papaya.base;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import java.util.Vector;

public class NotifyingService extends Service {

    public static class BootReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
        }
    }

    public static class ClickService extends Service {
        public IBinder onBind(Intent intent) {
            return null;
        }

        public void onStart(Intent intent, int i) {
            super.onStart(intent, i);
        }
    }

    static {
        new Vector();
    }

    public void clearNotification() {
    }

    public void getMailUpdate() {
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
    }

    public void showNotification(Vector vector) {
    }

    public void startCheckService() {
    }

    public void stopCheckService() {
    }
}
