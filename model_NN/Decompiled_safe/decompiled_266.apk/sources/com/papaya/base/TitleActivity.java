package com.papaya.base;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.adknowledge.superrewards.model.SROffer;
import com.papaya.si.C0028b;
import com.papaya.si.C0056v;
import com.papaya.si.C0060z;
import com.papaya.si.aV;

public class TitleActivity extends Activity {
    private TextView cn;

    /* access modifiers changed from: protected */
    public View createContentView(Bundle bundle) {
        View inflate = getLayoutInflater().inflate(C0060z.layoutID(SROffer.TITLE), (ViewGroup) null);
        ViewGroup viewGroup = (ViewGroup) inflate.findViewById(C0060z.id(SROffer.TITLE));
        int myLayout = myLayout();
        if (myLayout != 0) {
            getLayoutInflater().inflate(myLayout, viewGroup);
        }
        return inflate;
    }

    public void finish() {
        C0028b.onFinished(this);
        super.finish();
    }

    /* access modifiers changed from: protected */
    public CharSequence getHintedTitle() {
        String stringExtra = getIntent().getStringExtra("hinted_title");
        return aV.isEmpty(stringExtra) ? C0056v.bj : stringExtra;
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    /* access modifiers changed from: protected */
    public int myLayout() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public boolean needCustomTitle() {
        return !(getParent() instanceof EntryActivity);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C0028b.onCreated(this);
        if (!needCustomTitle()) {
            setContentView(createContentView(bundle));
            return;
        }
        requestWindowFeature(1);
        LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(C0060z.layoutID("content_notabbar"), (ViewGroup) null);
        this.cn = (TextView) linearLayout.findViewById(C0060z.id("custom_title"));
        linearLayout.addView(createContentView(bundle), new LinearLayout.LayoutParams(-1, -1));
        setContentView(linearLayout);
        setTitle(getHintedTitle());
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public void onDestroy() {
        C0028b.onDestroyed(this);
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        if (getParent() instanceof EntryActivity) {
            return false;
        }
        finish();
        return true;
    }

    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        if (menuItem.getItemId() == 1) {
            C0028b.openPRIALink(this, "static_more");
        }
        return true;
    }

    public void onPause() {
        C0028b.onPaused(this);
        super.onPause();
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        menu.add(0, 1, 0, C0060z.stringID("base_menu_5")).setIcon(C0060z.drawableID("menu_more"));
        return true;
    }

    public void onResume() {
        C0028b.onResumed(this);
        super.onResume();
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        if (this.cn != null) {
            this.cn.setText(charSequence);
        }
    }

    /* access modifiers changed from: protected */
    public String title() {
        return null;
    }
}
