package com.papaya.base;

import android.os.Bundle;
import com.papaya.si.C0037c;
import com.papaya.si.C0058x;
import com.papaya.si.C0059y;

public class PotpActivity extends TitleActivity implements C0058x, C0059y {
    public void finish() {
        super.finish();
        C0037c.A.removeConnectionDelegate(this);
    }

    public void onConnectionEstablished() {
    }

    public void onConnectionLost() {
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C0037c.A.addConnectionDelegate(this);
        C0037c.getSession().addSessionDelegate(this);
    }

    public void onDestroy() {
        super.onDestroy();
        C0037c.A.removeConnectionDelegate(this);
        C0037c.getSession().removeSessionDelegate(this);
    }

    public void onPotpSessionUpdated(String str, String str2) {
    }

    public void onResume() {
        super.onResume();
        C0037c.A.start();
    }
}
