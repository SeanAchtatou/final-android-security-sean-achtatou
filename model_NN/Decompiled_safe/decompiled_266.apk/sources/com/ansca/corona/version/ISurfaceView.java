package com.ansca.corona.version;

import android.view.View;

public interface ISurfaceView {
    void clearNeedsSwap();

    View getView();

    void onPause();

    void onResume();

    void requestRender();

    void setNeedsSwap();
}
