package com.ansca.corona.version.android8;

import android.graphics.Typeface;
import android.os.Environment;
import android.view.MotionEvent;
import com.ansca.corona.CoronaActivity;
import com.ansca.corona.version.IAndroidVersionSpecific;
import com.ansca.corona.version.ISurfaceView;
import java.io.File;

public class AndroidVersionSpecific implements IAndroidVersionSpecific {
    public int apiVersion() {
        return 8;
    }

    public Typeface typefaceCreateFromFile(String path) {
        if (new File(path).exists()) {
            return Typeface.createFromFile(path);
        }
        return null;
    }

    public ISurfaceView createCoronaGLSurfaceView(CoronaActivity activity) {
        return new CoronaGLSurfaceView(activity);
    }

    public int audioChannelMono() {
        return 16;
    }

    public String getPictureStorageDirectory() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath();
    }

    public int motionEventGetAction(MotionEvent event) {
        return event.getAction() & 255;
    }

    public int motionEventGetActionIndex(MotionEvent event) {
        return event.getActionIndex();
    }

    public int motionEventGetPointerCount(MotionEvent event) {
        return event.getPointerCount();
    }

    public int motionEventGetPointerId(MotionEvent event, int pointerIndex) {
        return event.getPointerId(pointerIndex);
    }

    public float motionEventGetX(MotionEvent event, int pointerIndex) {
        return event.getX(pointerIndex);
    }

    public float motionEventGetY(MotionEvent event, int pointerIndex) {
        return event.getY(pointerIndex);
    }

    public int motionEventACTION_POINTER_DOWN() {
        return 5;
    }

    public int motionEventACTION_POINTER_UP() {
        return 6;
    }

    public int inputTypeFlagsNoSuggestions() {
        return 524288;
    }
}
