package com.ansca.corona.version.android8;

import android.opengl.derived.SwapGLSurfaceView;
import com.ansca.corona.CoronaActivity;
import com.ansca.corona.CoronaRenderer;

class CoronaGLSurfaceView extends SwapGLSurfaceView {
    private CoronaRenderer myRenderer;

    public CoronaGLSurfaceView(CoronaActivity activity) {
        super(activity);
        this.myRenderer = new CoronaRenderer(activity);
        setRenderer(this.myRenderer);
        setRenderMode(0);
        setDebugFlags(3);
    }

    public void clearFirstSurface() {
        this.myRenderer.clearFirstSurface();
    }

    public void onPause() {
        setRenderMode(1);
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        setRenderMode(0);
    }
}
