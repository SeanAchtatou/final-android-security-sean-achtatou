package com.ansca.corona.version;

import android.graphics.Typeface;
import android.view.MotionEvent;
import com.ansca.corona.CoronaActivity;

public interface IAndroidVersionSpecific {
    int apiVersion();

    int audioChannelMono();

    ISurfaceView createCoronaGLSurfaceView(CoronaActivity coronaActivity);

    String getPictureStorageDirectory();

    int inputTypeFlagsNoSuggestions();

    int motionEventACTION_POINTER_DOWN();

    int motionEventACTION_POINTER_UP();

    int motionEventGetAction(MotionEvent motionEvent);

    int motionEventGetActionIndex(MotionEvent motionEvent);

    int motionEventGetPointerCount(MotionEvent motionEvent);

    int motionEventGetPointerId(MotionEvent motionEvent, int i);

    float motionEventGetX(MotionEvent motionEvent, int i);

    float motionEventGetY(MotionEvent motionEvent, int i);

    Typeface typefaceCreateFromFile(String str);
}
