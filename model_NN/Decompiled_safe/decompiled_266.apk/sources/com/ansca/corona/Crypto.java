package com.ansca.corona;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class Crypto {
    public static final String ALGORITHM_HMAC_MD4 = "HmacMD4";
    public static final String ALGORITHM_HMAC_MD5 = "HmacMD5";
    public static final String ALGORITHM_HMAC_SHA1A = "HmacSHA1A";
    public static final String ALGORITHM_HMAC_SHA224A = "HmacSHA224A";
    public static final String ALGORITHM_HMAC_SHA256A = "HmacSHA256A";
    public static final String ALGORITHM_HMAC_SHA384 = "HmacSHA384";
    public static final String ALGORITHM_HMAC_SHA512 = "HmacSHA512";
    public static final String ALGORITHM_MD4 = "MD4";
    public static final String ALGORITHM_MD5 = "MD5";
    public static final String ALGORITHM_SHA1A = "SHA1A";
    public static final String ALGORITHM_SHA224A = "SHA224A";
    public static final String ALGORITHM_SHA256A = "SHA256A";
    public static final String ALGORITHM_SHA384 = "SHA384";
    public static final String ALGORITHM_SHA512 = "SHA512";

    public static int GetDigestLength(String algorithm) {
        try {
            return MessageDigest.getInstance(algorithm).getDigestLength();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static byte[] CalculateDigest(String algorithm, byte[] data) {
        try {
            return MessageDigest.getInstance(algorithm).digest(data);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] CalculateHMAC(String algorithm, String key, byte[] data) {
        try {
            Mac hmac = Mac.getInstance(algorithm);
            hmac.init(new SecretKeySpec(key.getBytes("UTF-8"), "RAW"));
            return hmac.doFinal(data);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } catch (InvalidKeyException e2) {
            e2.printStackTrace();
            return null;
        } catch (UnsupportedEncodingException e3) {
            e3.printStackTrace();
            return null;
        }
    }
}
