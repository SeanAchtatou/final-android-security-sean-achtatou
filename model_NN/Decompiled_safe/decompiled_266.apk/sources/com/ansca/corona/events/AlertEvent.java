package com.ansca.corona.events;

import com.ansca.corona.Controller;

public class AlertEvent extends Event {
    private int myButtonIndex;

    AlertEvent(int which) {
        this.myButtonIndex = which;
    }

    public void Send() {
        Controller.getPortal().alertCallback(this.myButtonIndex);
    }
}
