package com.ansca.corona.events;

import com.ansca.corona.Controller;

public class GyroscopeEvent extends Event {
    private double myDeltaTime;
    private double myXRotation;
    private double myYRotation;
    private double myZRotation;

    GyroscopeEvent(double xRotation, double yRotation, double zRotation, double deltaTime) {
        this.myXRotation = xRotation;
        this.myYRotation = yRotation;
        this.myZRotation = zRotation;
        this.myDeltaTime = deltaTime;
    }

    public void Send() {
        Controller.getPortal().gyroscopeEvent(this.myXRotation, this.myYRotation, this.myZRotation, this.myDeltaTime);
    }
}
