package com.ansca.corona.events;

import com.ansca.corona.Controller;

public class TapEvent extends Event {
    private int myCount;
    private int myX;
    private int myY;

    TapEvent(int x, int y, int count) {
        this.myX = x;
        this.myY = y;
        this.myCount = count;
    }

    public void Send() {
        Controller.getPortal().tapEvent(this.myX, this.myY, this.myCount);
    }
}
