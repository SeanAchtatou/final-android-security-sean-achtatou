package com.ansca.corona.events;

import com.ansca.corona.Controller;

public class AccelerometerEvent extends Event {
    private double myDeltaTime;
    private double myX;
    private double myY;
    private double myZ;

    AccelerometerEvent(double x, double y, double z, double deltaTime) {
        this.myX = x;
        this.myY = y;
        this.myZ = z;
        this.myDeltaTime = deltaTime;
    }

    public void Send() {
        Controller.getPortal().accelerometerEvent(this.myX, this.myY, this.myZ, this.myDeltaTime);
    }
}
