package com.ansca.corona.events;

abstract class Event {
    public abstract void Send();

    Event() {
    }
}
