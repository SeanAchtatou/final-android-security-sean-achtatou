package com.ansca.corona.events;

import com.ansca.corona.Controller;

public class CreditsRequestEvent extends Event {
    private int myNewPoints;
    private int myTotalPoints;

    CreditsRequestEvent(int newPoints, int totalPoints) {
        this.myNewPoints = newPoints;
        this.myTotalPoints = totalPoints;
    }

    public void Send() {
        Controller.getPortal().creditsRequestEvent(this.myNewPoints, this.myTotalPoints);
    }
}
