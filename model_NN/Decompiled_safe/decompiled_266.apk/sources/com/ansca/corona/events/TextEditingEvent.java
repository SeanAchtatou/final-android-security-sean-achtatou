package com.ansca.corona.events;

import com.ansca.corona.Controller;

public class TextEditingEvent extends Event {
    private int myEditTextId;
    private String newCharacters;
    private String newString;
    private int numDeleted;
    private String oldString;
    private int startPos;

    public TextEditingEvent(int id, int start_pos, int num_deleted, String new_characters, String old_string, String new_string) {
        this.myEditTextId = id;
        this.startPos = start_pos;
        this.numDeleted = num_deleted;
        this.newCharacters = new_characters;
        this.oldString = old_string;
        this.newString = new_string;
    }

    public void Send() {
        Controller.getPortal().textEditingEvent(this.myEditTextId, this.startPos, this.numDeleted, this.newCharacters, this.oldString, this.newString);
    }
}
