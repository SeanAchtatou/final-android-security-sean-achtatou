package com.ansca.corona.events;

import com.ansca.corona.Controller;

public class ResizeEvent extends Event {
    private String myCacheDir;
    private String myFilesDir;
    private int myH;
    private int myOrientation;
    private String myPackageName;
    private int myW;

    ResizeEvent(String name, String filesDir, String cacheDir, int w, int h, int orientation) {
        this.myPackageName = name;
        this.myFilesDir = filesDir;
        this.myCacheDir = cacheDir;
        this.myW = w;
        this.myH = h;
        this.myOrientation = orientation;
    }

    public void Send() {
        Controller.getPortal().resize(this.myPackageName, this.myFilesDir, this.myCacheDir, this.myW, this.myH, this.myOrientation);
    }
}
