package com.ansca.corona.events;

import com.ansca.corona.Controller;

public class DidFailLoadUrlEvent extends Event {
    String myDescription;
    int myErrorCode;
    String myFailingUrl;
    int myId;

    DidFailLoadUrlEvent(int id, String failingUrl, String description, int errorCode) {
        this.myId = id;
        this.myFailingUrl = failingUrl;
        this.myDescription = description;
        this.myErrorCode = errorCode;
    }

    public void Send() {
        if (Controller.getPortal().webPopupDidFailLoadUrl(this.myId, this.myFailingUrl, this.myDescription, this.myErrorCode)) {
        }
    }
}
