package com.ansca.corona.events;

import com.ansca.corona.Controller;

public class TextEvent extends Event {
    private int myEditTextId;
    private boolean myHasFocus;
    private boolean myIsDone;

    public TextEvent(int id, boolean f, boolean done) {
        this.myEditTextId = id;
        this.myHasFocus = f;
        this.myIsDone = done;
    }

    public void Send() {
        Controller.getPortal().textEvent(this.myEditTextId, this.myHasFocus, this.myIsDone);
    }
}
