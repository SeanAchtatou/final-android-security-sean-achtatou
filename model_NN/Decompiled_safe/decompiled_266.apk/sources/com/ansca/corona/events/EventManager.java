package com.ansca.corona.events;

import android.webkit.WebView;
import com.ansca.corona.Controller;
import com.ansca.corona.events.MediaEvent;
import java.util.LinkedList;
import java.util.Queue;

public class EventManager {
    private Queue<Event> myEvents = new LinkedList();

    public synchronized void addEvent(Event e) {
        this.myEvents.add(e);
        Controller.requestEventRender();
    }

    public static int touchActionToPhase(int action) {
        switch (action) {
            case 0:
            case 5:
                return 0;
            case 1:
            case 6:
                return 3;
            case 2:
                return 1;
            case 3:
                return 4;
            case 4:
            default:
                System.err.println("Unknown touch phase " + action);
                return 0;
        }
    }

    public MultitouchEvent newMultitouchEvent() {
        return new MultitouchEvent();
    }

    public void accelerometerEvent(double x, double y, double z, double deltaTime) {
        addEvent(new AccelerometerEvent(x, y, z, deltaTime));
    }

    public void gyroscopeEvent(double x, double y, double z, double deltaTime) {
        addEvent(new GyroscopeEvent(x, y, z, deltaTime));
    }

    public void locationEvent(double latitude, double longitude, double altitude, double accuracy, double speed, double bearing, long time) {
        addEvent(new LocationEvent(latitude, longitude, altitude, accuracy, speed, bearing, time));
    }

    public void headingEvent(float heading) {
        addEvent(new LocationEvent(0.0d, 0.0d, 0.0d, 0.0d, 0.0d, (double) heading, 0));
    }

    public void touchEvent(TouchData t) {
        addEvent(new TouchEvent(t));
    }

    public void orientationChanged(int newOrientation, int oldOrientation) {
        addEvent(new OrientationEvent(newOrientation, oldOrientation));
    }

    public void nativeAlertResult(int which) {
        addEvent(new AlertEvent(which));
    }

    public void tapEvent(int x, int y, int count) {
        addEvent(new TapEvent(x, y, count));
    }

    public void resize(String name, String filesDir, String cacheDir, int w, int h, int orientation) {
        addEvent(new ResizeEvent(name, filesDir, cacheDir, w, h, orientation));
    }

    public void loadSound(int id, String name) {
        Controller.getMediaManager().loadSound(id, name);
    }

    public void loadEventSound(int id, String name) {
        Controller.getMediaManager().loadEventSound(id, name);
    }

    public void playSound(int id, String soundName, boolean loop) {
        MediaEvent e = new MediaEvent(id, soundName, MediaEvent.Event.PlaySound);
        e.setLooping(loop);
        addEvent(e);
    }

    public void stopSound(int id) {
        addEvent(new MediaEvent(id, MediaEvent.Event.StopSound));
    }

    public void pauseSound(int id) {
        addEvent(new MediaEvent(id, MediaEvent.Event.PauseSound));
    }

    public void resumeSound(int id) {
        addEvent(new MediaEvent(id, MediaEvent.Event.ResumeSound));
    }

    public void playVideo(int id, String name) {
        addEvent(new MediaEvent(id, name, MediaEvent.Event.PlayVideo));
    }

    public void soundEnded(int id) {
        addEvent(new MediaEvent(id, MediaEvent.Event.SoundEnded));
    }

    public void videoEnded(int id) {
        addEvent(new MediaEvent(id, MediaEvent.Event.VideoEnded));
    }

    public void textEvent(int id, boolean hasFocus, boolean isDone) {
        addEvent(new TextEvent(id, hasFocus, isDone));
    }

    public void textEditingEvent(int id, int start_pos, int num_deleted, String new_characters, String old_string, String new_string) {
        addEvent(new TextEditingEvent(id, start_pos, num_deleted, new_characters, old_string, new_string));
    }

    public void shouldLoadUrlEvent(int id, WebView webView, String originalUrl, String url) {
        addEvent(new ShouldLoadUrlEvent(id, webView, originalUrl, url));
    }

    public void didFailLoadUrlEvent(int id, String failingUrl, String description, int errorCode) {
        addEvent(new DidFailLoadUrlEvent(id, failingUrl, description, errorCode));
    }

    public synchronized void fbConnectSessionEvent(int phase) {
        addEvent(new FBConnectEvent(phase));
    }

    public synchronized void fbConnectSessionEventError(String msg) {
        addEvent(new FBConnectEvent(msg));
    }

    public synchronized void fbConnectRequestEvent(String msg, boolean isError) {
        addEvent(new FBConnectEvent(msg, isError));
    }

    public synchronized void fbConnectRequestEvent(String msg, boolean isError, boolean didComplete) {
        addEvent(new FBConnectEvent(msg, isError, didComplete));
    }

    public void networkRequestEvent(int listenerId, String response, boolean isError) {
        addEvent(new NetworkRequestEvent(listenerId, response, isError));
    }

    public synchronized void sendEvents() {
        while (true) {
            Event e = this.myEvents.poll();
            if (e != null) {
                e.Send();
            }
        }
    }

    public void creditsRequestEvent(int newPoints, int totalPoints) {
        addEvent(new CreditsRequestEvent(newPoints, totalPoints));
    }
}
