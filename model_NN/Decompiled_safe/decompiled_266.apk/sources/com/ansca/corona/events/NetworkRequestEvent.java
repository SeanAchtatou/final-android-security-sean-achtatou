package com.ansca.corona.events;

import com.ansca.corona.Controller;

public class NetworkRequestEvent extends Event {
    private boolean myIsError;
    private int myListenerId;
    private String myResponse;

    NetworkRequestEvent(int listenerId, String response, boolean isError) {
        this.myListenerId = listenerId;
        this.myResponse = response;
        this.myIsError = isError;
    }

    public void Send() {
        Controller.getPortal().networkRequestEvent(this.myListenerId, this.myResponse, this.myIsError);
    }
}
