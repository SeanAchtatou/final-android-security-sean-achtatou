package com.ansca.corona.events;

import android.webkit.WebView;
import com.ansca.corona.Controller;

public class ShouldLoadUrlEvent extends Event {
    int myId;
    String myOriginalUrl;
    String myUrl;
    WebView myWebView;

    ShouldLoadUrlEvent(int id, WebView webView, String originalUrl, String url) {
        this.myId = id;
        this.myOriginalUrl = originalUrl;
        this.myWebView = webView;
        this.myUrl = url;
    }

    public void Send() {
        if (Controller.getPortal().webPopupShouldLoadUrl(this.myId, this.myUrl)) {
            Controller.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    ShouldLoadUrlEvent.this.myWebView.loadUrl(ShouldLoadUrlEvent.this.myUrl);
                }
            });
        }
    }
}
