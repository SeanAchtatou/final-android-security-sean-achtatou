package com.ansca.corona.events;

import com.ansca.corona.Controller;

public class KeyboardEvent extends Event {
    private String fKeyName;
    private final Phase fPhase;
    private boolean fResult = false;

    public enum Phase {
        DOWN(0),
        UP(1);
        
        private final int value;

        private Phase(int v) {
            this.value = v;
        }

        public int getValue() {
            return this.value;
        }
    }

    public KeyboardEvent(Phase phase, String keyName) {
        this.fPhase = phase;
        this.fKeyName = new String(keyName);
    }

    public boolean getResult() {
        return this.fResult;
    }

    public void Send() {
        this.fResult = Controller.getPortal().keyEvent(this.fPhase.getValue(), this.fKeyName);
    }
}
