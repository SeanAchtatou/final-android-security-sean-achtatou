package com.ansca.corona.events;

import com.ansca.corona.Controller;

public class LocationEvent extends Event {
    private double myAccuracy;
    private double myAltitude;
    private double myBearing;
    private double myLatitude;
    private double myLongitude;
    private double mySpeed;
    private long myTime;

    LocationEvent(double latitude, double longitude, double altitude, double accuracy, double speed, double bearing, long time) {
        this.myLatitude = latitude;
        this.myLongitude = longitude;
        this.myAltitude = altitude;
        this.myAccuracy = accuracy;
        this.mySpeed = speed;
        this.myBearing = bearing;
        this.myTime = time;
    }

    public void Send() {
        Controller.getPortal().locationEvent(this.myLatitude, this.myLongitude, this.myAltitude, this.myAccuracy, this.mySpeed, this.myBearing, this.myTime);
    }
}
