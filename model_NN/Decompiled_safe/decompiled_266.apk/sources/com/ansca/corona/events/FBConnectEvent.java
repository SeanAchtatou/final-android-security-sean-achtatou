package com.ansca.corona.events;

import com.ansca.corona.Controller;

public class FBConnectEvent extends Event {
    private static final int REQUEST = 2;
    private static final int SESSION = 0;
    private static final int SESSION_ERROR = 1;
    private int myDidComplete;
    private boolean myIsError;
    private String myMsg;
    private int myPhase;
    private int myType = 0;

    FBConnectEvent(int phase) {
        this.myPhase = phase;
    }

    FBConnectEvent(String msg) {
        this.myMsg = msg;
    }

    FBConnectEvent(String msg, boolean isError) {
        this.myMsg = msg;
        this.myIsError = isError;
        this.myDidComplete = -1;
    }

    FBConnectEvent(String msg, boolean isError, boolean didComplete) {
        this.myMsg = msg;
        this.myIsError = isError;
        this.myDidComplete = didComplete ? 1 : 0;
    }

    public void Send() {
        switch (this.myType) {
            case 0:
                Controller.getPortal().fbConnectSessionEvent(this.myPhase);
                return;
            case 1:
                Controller.getPortal().fbConnectSessionEventError(this.myMsg);
                return;
            case 2:
                Controller.getPortal().fbConnectRequestEvent(this.myMsg, this.myIsError, this.myDidComplete);
                return;
            default:
                return;
        }
    }
}
