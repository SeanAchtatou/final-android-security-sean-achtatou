package com.ansca.corona.events;

import com.ansca.corona.Controller;

public class OrientationEvent extends Event {
    int myNewOrientation;
    int myOldOrientation;

    OrientationEvent(int newOrientation, int oldOrientation) {
        this.myNewOrientation = newOrientation;
        this.myOldOrientation = oldOrientation;
    }

    public void Send() {
        Controller.getPortal().orientationChanged(this.myNewOrientation, this.myOldOrientation);
    }
}
