package com.ansca.corona.events;

import com.ansca.corona.Controller;
import java.util.ArrayList;
import java.util.List;

public class MultitouchEvent extends Event {
    public static final int PHASE_BEGAN = 0;
    public static final int PHASE_CANCELLED = 4;
    public static final int PHASE_ENDED = 3;
    public static final int PHASE_MOVED = 1;
    public static final int PHASE_STATIONARY = 2;
    private List<TouchData> myTouches = new ArrayList();

    MultitouchEvent() {
    }

    public void add(TouchData t) {
        this.myTouches.add(new TouchData(t));
    }

    public void Send() {
        Controller.getPortal().multitouchEventCallback(this.myTouches.toArray());
    }
}
