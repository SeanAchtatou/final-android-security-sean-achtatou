package com.ansca.corona.events;

import com.ansca.corona.Controller;

public class TouchEvent extends Event {
    private TouchData myData;

    TouchEvent(TouchData t) {
        this.myData = new TouchData(t);
    }

    public void Send() {
        TouchData t = this.myData;
        Controller.getPortal().touchEvent(t.getX(), t.getY(), t.getStartX(), t.getStartY(), t.getPhase(), t.getId());
    }
}
