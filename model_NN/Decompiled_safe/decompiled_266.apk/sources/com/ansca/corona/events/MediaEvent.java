package com.ansca.corona.events;

import com.ansca.corona.Controller;

public class MediaEvent extends Event {
    Event myEvent;
    boolean myLooping = false;
    int myMediaId;
    String myMediaName;

    enum Event {
        LoadSound,
        PlaySound,
        StopSound,
        PauseSound,
        ResumeSound,
        PlayVideo,
        SoundEnded,
        VideoEnded
    }

    MediaEvent(String name, Event event) {
        this.myEvent = event;
        this.myMediaName = name;
    }

    MediaEvent(int id, Event event) {
        this.myEvent = event;
        this.myMediaId = id;
    }

    MediaEvent(int id, String name, Event event) {
        this.myEvent = event;
        this.myMediaId = id;
        this.myMediaName = name;
    }

    public void setLooping(boolean l) {
        this.myLooping = l;
    }

    public void Send() {
        switch (this.myEvent) {
            case LoadSound:
            default:
                return;
            case PlaySound:
                Controller.getMediaManager().playMedia(this.myMediaId, this.myLooping);
                return;
            case StopSound:
                Controller.getMediaManager().stopMedia(this.myMediaId);
                return;
            case PauseSound:
                Controller.getMediaManager().pauseMedia(this.myMediaId);
                return;
            case ResumeSound:
                Controller.getMediaManager().resumeMedia(this.myMediaId);
                return;
            case PlayVideo:
                Controller.getMediaManager().playVideo(this.myMediaId, this.myMediaName, true);
                return;
            case SoundEnded:
                Controller.getPortal().soundEndCallback(this.myMediaId);
                return;
            case VideoEnded:
                Controller.getPortal().videoEndCallback(this.myMediaId);
                return;
        }
    }
}
