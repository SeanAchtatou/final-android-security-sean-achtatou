package com.ansca.corona.events;

public class TouchData {
    private int myId;
    private int myPhase;
    private float myStartX;
    private float myStartY;
    private float myX;
    private float myY;

    public TouchData(int id, float x, float y, int phase) {
        this.myPhase = 0;
        this.myId = id;
        this.myX = x;
        this.myY = y;
        this.myStartX = x;
        this.myStartY = y;
        this.myPhase = phase;
    }

    public TouchData(TouchData rhs) {
        this(rhs.myId, rhs.myX, rhs.myY, rhs.myPhase);
        this.myStartX = rhs.myStartX;
        this.myStartY = rhs.myStartY;
    }

    public int getId() {
        return this.myId;
    }

    public int getX() {
        return (int) this.myX;
    }

    public int getY() {
        return (int) this.myY;
    }

    public void setX(float x) {
        this.myX = x;
    }

    public void setY(float y) {
        this.myY = y;
    }

    public int getStartX() {
        return (int) this.myStartX;
    }

    public int getStartY() {
        return (int) this.myStartY;
    }

    public int getPhase() {
        return this.myPhase;
    }

    public void setPhase(int phase) {
        this.myPhase = phase;
    }
}
