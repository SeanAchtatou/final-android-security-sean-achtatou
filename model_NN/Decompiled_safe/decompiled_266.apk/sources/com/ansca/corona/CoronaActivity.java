package com.ansca.corona;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import com.ansca.corona.events.EventManager;
import com.ansca.corona.events.KeyboardEvent;
import com.ansca.corona.events.MultitouchEvent;
import com.ansca.corona.events.TouchData;
import com.ansca.corona.version.IAndroidVersionSpecific;
import com.ansca.corona.version.ISurfaceView;
import java.util.HashMap;

public class CoronaActivity extends Activity {
    public static final int MESSAGE_RUNNABLE = 1;
    public static final int OPEN_URL = 2;
    public static final int PLAY_VIDEO = 1;
    public static final int SEND_MAIL = 3;
    private static int sId = -1;
    private static int sTouchId = 1;
    private static HashMap<Integer, TouchData> sTouches = new HashMap<>();
    private float myFirstTapX = 0.0f;
    private float myFirstTapY = 0.0f;
    private ISurfaceView myGLView;
    private Handler myHandler;
    private int myImmediateTapCount = 0;
    private long myLastTapTime = 0;
    private int myOrientation;
    private int myPreviousOrientation;
    private boolean myTapHasStarted = false;
    private float myTapStartX = 0.0f;
    private float myTapStartY = 0.0f;

    static {
        System.loadLibrary("openal");
        System.loadLibrary("mpg123");
        System.loadLibrary("vorbisidec");
        System.loadLibrary("almixer");
        System.loadLibrary("corona");
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        getWindow().setSoftInputMode(18);
        Controller.create(this);
        this.myGLView = Controller.getAndroidVersionSpecific().createCoronaGLSurfaceView(this);
        ViewManager.initialize(this);
        ViewManager.getViewManager().setGLView(this.myGLView.getView());
        setContentView(ViewManager.getViewManager().getViewGroup());
        Controller.getPortal().init();
        this.myHandler = new Handler() {
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }
        };
        setVolumeControlStream(3);
        int i = getResources().getConfiguration().orientation;
        this.myPreviousOrientation = i;
        this.myOrientation = i;
    }

    public View getGLView() {
        return this.myGLView.getView();
    }

    public Handler getHandler() {
        return this.myHandler;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.myGLView = null;
        Controller.getController().destroy();
        ViewManager.destroy();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        Controller.getController().start();
        System.out.println("Corona version = " + Controller.getPortal().getVersion());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.myGLView.onResume();
        Controller.getController().resume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Controller.getController().pause();
        this.myGLView.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        Controller.getController().stop();
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        Controller.getController().restart();
        super.onRestart();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (!(newConfig.orientation == this.myOrientation || Controller.getController() == null)) {
            this.myPreviousOrientation = this.myOrientation;
            this.myOrientation = newConfig.orientation;
        }
        if (newConfig.keyboardHidden != 2 && newConfig.keyboardHidden == 1) {
        }
        if (newConfig.hardKeyboardHidden != 1 && newConfig.hardKeyboardHidden == 2) {
        }
    }

    public void requestRender() {
        if (Controller.isRunning()) {
            this.myGLView.requestRender();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        IAndroidVersionSpecific version = Controller.getAndroidVersionSpecific();
        EventManager manager = Controller.getEventManager();
        boolean isMultitouch = Controller.isMultitouchEnabled();
        MultitouchEvent multitouch = isMultitouch ? Controller.getEventManager().newMultitouchEvent() : null;
        int action = version.motionEventGetAction(event);
        switch (action) {
            case 0:
                sTouches.clear();
                TouchData touchData = new TouchData(sTouchId, event.getX(), event.getY(), 0);
                if (isMultitouch) {
                    multitouch.add(touchData);
                } else {
                    manager.touchEvent(touchData);
                }
                sId = sTouchId;
                sTouchId++;
                sTouches.put(Integer.valueOf(version.motionEventGetPointerId(event, 0)), touchData);
                break;
            case 1:
            case 2:
            case 3:
                int numPointers = version.motionEventGetPointerCount(event);
                boolean isMoved = 2 == action;
                int phase = 1;
                if (1 == action) {
                    phase = 3;
                } else if (3 == action) {
                    phase = 4;
                }
                int numAdded = 0;
                for (int i = 0; i < numPointers; i++) {
                    int pointerId = version.motionEventGetPointerId(event, i);
                    TouchData t = sTouches.get(Integer.valueOf(pointerId));
                    if (t != null) {
                        float x = version.motionEventGetX(event, pointerId);
                        float y = version.motionEventGetY(event, pointerId);
                        if (isMoved) {
                            int oldX = t.getX();
                            int oldY = t.getY();
                            if (oldX == ((int) x) && oldY == ((int) y)) {
                            }
                        }
                        t.setX(x);
                        t.setY(y);
                        t.setPhase(phase);
                        if (isMultitouch) {
                            multitouch.add(t);
                            numAdded++;
                            if (sId == t.getId()) {
                            }
                        } else {
                            manager.touchEvent(t);
                        }
                    } else if (isMultitouch) {
                        Log.v("Corona", "ERROR: unknown touch detected while multitouch was enabled. numpointers(" + numPointers + ") pointerId(" + pointerId + ") i( " + i + ")");
                    }
                }
                if (numAdded <= 0) {
                    multitouch = null;
                }
                if (!isMoved) {
                    sTouches.clear();
                    break;
                }
                break;
            default:
                if (isMultitouch) {
                    if (version.motionEventACTION_POINTER_DOWN() != action) {
                        if (version.motionEventACTION_POINTER_UP() == action) {
                            int pointerId2 = version.motionEventGetPointerId(event, version.motionEventGetActionIndex(event));
                            TouchData t2 = sTouches.get(Integer.valueOf(pointerId2));
                            if (t2 != null) {
                                float x2 = version.motionEventGetX(event, pointerId2);
                                float y2 = version.motionEventGetY(event, pointerId2);
                                t2.setX(x2);
                                t2.setY(y2);
                                t2.setPhase(3);
                                multitouch.add(t2);
                                sTouches.remove(Integer.valueOf(pointerId2));
                                break;
                            } else {
                                Log.v("Corona", "ERROR: ACTION_POINTER_DOWN unknown touch detected while multitouch was enabled. pointerId(" + pointerId2 + ")");
                                multitouch = null;
                                break;
                            }
                        }
                    } else {
                        int pointerId3 = version.motionEventGetPointerId(event, version.motionEventGetActionIndex(event));
                        TouchData touchData2 = new TouchData(sTouchId, version.motionEventGetX(event, pointerId3), version.motionEventGetY(event, pointerId3), 0);
                        sTouchId++;
                        sTouches.put(Integer.valueOf(pointerId3), touchData2);
                        multitouch.add(touchData2);
                        break;
                    }
                }
                break;
        }
        if (isMultitouch && multitouch != null) {
            manager.addEvent(multitouch);
        }
        if (version.motionEventGetPointerCount(event) != 1) {
            this.myTapHasStarted = false;
            this.myImmediateTapCount = 0;
            return true;
        } else if (action == 0) {
            this.myTapStartX = event.getX();
            this.myTapStartY = event.getY();
            this.myTapHasStarted = true;
            if (areCoordinatesWithinTapBounds(this.myTapStartX, this.myTapStartY, this.myFirstTapX, this.myFirstTapY)) {
                return true;
            }
            this.myImmediateTapCount = 0;
            this.myFirstTapX = this.myTapStartX;
            this.myFirstTapY = this.myTapStartY;
            return true;
        } else if (!this.myTapHasStarted || 1 != action) {
            return true;
        } else {
            if (!areCoordinatesWithinTapBounds(event.getX(), event.getY(), this.myTapStartX, this.myTapStartY)) {
                return true;
            }
            if (event.getEventTime() - this.myLastTapTime > 500) {
                this.myImmediateTapCount = 1;
            } else if (this.myImmediateTapCount < Integer.MAX_VALUE) {
                this.myImmediateTapCount = this.myImmediateTapCount + 1;
            }
            this.myLastTapTime = event.getEventTime();
            this.myTapHasStarted = false;
            manager.tapEvent((int) event.getX(), (int) event.getY(), this.myImmediateTapCount);
            return true;
        }
    }

    private boolean areCoordinatesWithinTapBounds(float x1, float y1, float x2, float y2) {
        return Math.abs(x2 - x1) <= 40.0f && Math.abs(y2 - y1) <= 40.0f;
    }

    public void setNeedsSwap() {
        if (this.myGLView != null) {
            this.myGLView.setNeedsSwap();
        }
    }

    public void clearNeedsSwap() {
        if (this.myGLView != null) {
            this.myGLView.clearNeedsSwap();
        }
    }

    public int getOrientation() {
        return this.myOrientation;
    }

    public int getPreviousOrientation() {
        return this.myPreviousOrientation;
    }

    public String getFilesDirAsString() {
        return getDir("data", 0).getAbsolutePath();
    }

    public String getCacheDirAsString() {
        return getCacheDir().getAbsolutePath();
    }

    /* access modifiers changed from: protected */
    public String getStringForKeyCode(int keyCode) {
        switch (keyCode) {
            case 4:
                return "back";
            case 19:
                return "up";
            case 20:
                return "down";
            case 21:
                return "left";
            case 22:
                return "right";
            case 23:
                return "center";
            case 24:
                return "volumeUp";
            case 25:
                return "volumeDown";
            case 27:
                return "camera";
            case 82:
                return "menu";
            case 84:
                return "search";
            default:
                return null;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getRepeatCount() > 0) {
            return true;
        }
        String keyName = getStringForKeyCode(keyCode);
        if (keyName != null) {
            KeyboardEvent e = new KeyboardEvent(KeyboardEvent.Phase.DOWN, keyName);
            e.Send();
            if (e.getResult()) {
                return true;
            }
        }
        if (keyCode != 4 || !ViewManager.getViewManager().goBack()) {
            return super.onKeyDown(keyCode, event);
        }
        return true;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        String keyName = getStringForKeyCode(keyCode);
        if (keyName != null) {
            KeyboardEvent e = new KeyboardEvent(KeyboardEvent.Phase.UP, keyName);
            e.Send();
            if (e.getResult()) {
                return true;
            }
        }
        return super.onKeyUp(keyCode, event);
    }
}
