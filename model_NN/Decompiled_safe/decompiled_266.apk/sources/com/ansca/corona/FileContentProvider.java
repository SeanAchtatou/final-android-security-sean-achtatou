package com.ansca.corona;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import java.io.File;
import java.io.FileNotFoundException;

public class FileContentProvider extends ContentProvider {
    public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException {
        if (uri == null) {
            throw new IllegalArgumentException();
        }
        File file = new File("/data/data/" + getContext().getPackageName() + uri.getPath());
        if (file != null) {
            return ParcelFileDescriptor.open(file, 268435456);
        }
        throw new FileNotFoundException();
    }

    public AssetFileDescriptor openAssetFile(Uri uri, String mode) throws FileNotFoundException {
        if (uri == null) {
            throw new IllegalArgumentException();
        }
        try {
            String filePath = uri.getPath();
            int index = filePath.indexOf("files/coronaResources/");
            if (index < 0 || "files/coronaResources/".length() + index >= filePath.length()) {
                int index2 = filePath.indexOf("android_asset/");
                if (index2 >= 0 && "android_asset/".length() + index2 < filePath.length()) {
                    filePath = filePath.substring("android_asset/".length() + index2);
                }
            } else {
                filePath = filePath.substring("files/coronaResources/".length() + index);
            }
            if (filePath.startsWith(File.separator)) {
                filePath = filePath.substring(1);
            }
            return getContext().getAssets().openFd(filePath);
        } catch (Exception e) {
            throw new FileNotFoundException();
        }
    }

    public boolean onCreate() {
        return true;
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("Not supported by this provider");
    }

    public String getType(Uri uri) {
        throw new UnsupportedOperationException("Not supported by this provider");
    }

    public Uri insert(Uri uri, ContentValues values) {
        throw new UnsupportedOperationException("Not supported by this provider");
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        throw new UnsupportedOperationException("Not supported by this provider");
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("Not supported by this provider");
    }
}
