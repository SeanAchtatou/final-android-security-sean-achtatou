package com.ansca.corona;

public class JavaToNativeShim {
    public static final int EventTypeAccelerometer = 1;
    public static final int EventTypeGyroscope = 2;
    public static final int EventTypeHeading = 4;
    public static final int EventTypeLocation = 3;
    public static final int EventTypeMultitouch = 5;
    public static final int EventTypeNumTypes = 6;
    public static final int EventTypeOrientation = 0;
    public static final int EventTypeUnknown = -1;
    public static final int FBConnectSessionEventLogin = 0;
    public static final int FBConnectSessionEventLoginCancelled = 2;
    public static final int FBConnectSessionEventLoginFailed = 1;
    public static final int FBConnectSessionEventLogout = 3;
    public static final int TouchEventPhaseBegan = 0;
    public static final int TouchEventPhaseCancelled = 4;
    public static final int TouchEventPhaseEnded = 3;
    public static final int TouchEventPhaseMoved = 1;
    public static final int TouchEventPhaseStationary = 2;

    private static native void nativeAccelerometerEvent(double d, double d2, double d3, double d4);

    private static native void nativeAlertCallback(int i);

    private static native void nativeCreditsRequestEvent(int i, int i2);

    private static native void nativeDone();

    private static native void nativeFBConnectRequestEvent(String str, boolean z, int i);

    private static native void nativeFBConnectSessionEvent(int i);

    private static native void nativeFBConnectSessionEventError(String str);

    private static native String nativeGetVersion();

    private static native void nativeGyroscopeEvent(double d, double d2, double d3, double d4);

    private static native void nativeInit();

    private static native boolean nativeKeyEvent(int i, String str);

    private static native void nativeLocationEvent(double d, double d2, double d3, double d4, double d5, double d6, long j);

    private static native void nativeMultitouchEventCallback(Object[] objArr);

    private static native void nativeNetworkRequestEvent(int i, String str, boolean z);

    private static native void nativeOrientationChanged(int i, int i2);

    private static native void nativePause();

    private static native void nativeRecordCallback(int i, int i2);

    private static native void nativeReinitializeRenderer();

    private static native void nativeRender();

    private static native void nativeResize(String str, String str2, String str3, int i, int i2, int i3);

    private static native void nativeResume();

    private static native void nativeSoundEndCallback(int i);

    private static native void nativeTapEvent(int i, int i2, int i3);

    private static native void nativeTextEditingEvent(int i, int i2, int i3, String str, String str2, String str3);

    private static native void nativeTextEvent(int i, boolean z, boolean z2);

    private static native void nativeTouchEvent(int i, int i2, int i3, int i4, int i5, int i6);

    private static native void nativeVideoEndCallback(int i);

    private static native boolean nativeWebPopupDidFailLoadUrl(int i, String str, String str2, int i2);

    private static native boolean nativeWebPopupShouldLoadUrl(int i, String str);

    public synchronized String getVersion() {
        return nativeGetVersion();
    }

    public synchronized void pause() {
        nativePause();
    }

    public synchronized void resume() {
        nativeResume();
    }

    public synchronized void init() {
        nativeInit();
    }

    public synchronized void render() {
        nativeRender();
    }

    public synchronized void reinitializeRenderer() {
        nativeReinitializeRenderer();
    }

    public synchronized void destroy() {
        nativeDone();
    }

    public synchronized void resize(String packageName, String filesDir, String cacheDir, int w, int h, int orientation) {
        nativeResize(packageName, filesDir, cacheDir, w, h, orientation);
    }

    public synchronized void tapEvent(int x, int y, int count) {
        nativeTapEvent(x, y, count);
    }

    public synchronized void touchEvent(int x, int y, int xStart, int yStart, int type, int id) {
        nativeTouchEvent(x, y, xStart, yStart, type, id);
    }

    public synchronized boolean keyEvent(int phase, String keyName) {
        return nativeKeyEvent(phase, keyName);
    }

    public synchronized void accelerometerEvent(double x, double y, double z, double deltaTime) {
        nativeAccelerometerEvent(x, y, z, deltaTime);
    }

    public synchronized void gyroscopeEvent(double x, double y, double z, double deltaTime) {
        nativeGyroscopeEvent(x, y, z, deltaTime);
    }

    public synchronized void locationEvent(double latitude, double longitude, double altitude, double accuracy, double speed, double bearing, long time) {
        nativeLocationEvent(latitude, longitude, altitude, accuracy, speed, bearing, time);
    }

    public synchronized void orientationChanged(int newOrientation, int oldOrientation) {
        nativeOrientationChanged(newOrientation, oldOrientation);
    }

    public synchronized void alertCallback(int buttonIndex) {
        nativeAlertCallback(buttonIndex);
    }

    public synchronized void soundEndCallback(int id) {
        nativeSoundEndCallback(id);
    }

    public synchronized void videoEndCallback(int id) {
        nativeVideoEndCallback(id);
    }

    public synchronized void recordCallback(int id, int status) {
        nativeRecordCallback(id, status);
    }

    public synchronized void textEvent(int id, boolean hasFocus, boolean isDone) {
        nativeTextEvent(id, hasFocus, isDone);
    }

    public synchronized void textEditingEvent(int id, int startPos, int numDeleted, String newCharacters, String oldString, String newString) {
        nativeTextEditingEvent(id, startPos, numDeleted, newCharacters, oldString, newString);
    }

    public synchronized void multitouchEventCallback(Object[] touches) {
        nativeMultitouchEventCallback(touches);
    }

    public synchronized boolean webPopupShouldLoadUrl(int id, String url) {
        return nativeWebPopupShouldLoadUrl(id, url);
    }

    public synchronized boolean webPopupDidFailLoadUrl(int id, String url, String msg, int code) {
        return nativeWebPopupDidFailLoadUrl(id, url, msg, code);
    }

    public synchronized void fbConnectSessionEvent(int phase) {
        nativeFBConnectSessionEvent(phase);
    }

    public synchronized void fbConnectSessionEventError(String msg) {
        nativeFBConnectSessionEventError(msg);
    }

    public synchronized void fbConnectRequestEvent(String msg, boolean isError, int didComplete) {
        nativeFBConnectRequestEvent(msg, isError, didComplete);
    }

    public synchronized void networkRequestEvent(int listenerId, String response, boolean isError) {
        nativeNetworkRequestEvent(listenerId, response, isError);
    }

    public synchronized void creditsRequestEvent(int newPoints, int totalPoints) {
        nativeCreditsRequestEvent(newPoints, totalPoints);
    }
}
