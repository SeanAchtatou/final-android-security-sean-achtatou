package com.ansca.corona;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

class CoronaSensorManager {
    /* access modifiers changed from: private */
    public int myAccelerometerIntervalInHz = 10;
    /* access modifiers changed from: private */
    public Sensor myAccelerometerSensor;
    /* access modifiers changed from: private */
    public SensorEventListener myAccelerometerSensorListener;
    private boolean[] myActiveSensors = new boolean[6];
    /* access modifiers changed from: private */
    public int myGyroscopeIntervalInHz = 10;
    /* access modifiers changed from: private */
    public Sensor myGyroscopeSensor;
    /* access modifiers changed from: private */
    public SensorEventListener myGyroscopeSensorListener;
    /* access modifiers changed from: private */
    public long myLastAccelerometerTime = 0;
    /* access modifiers changed from: private */
    public long myLastGyroscopeTime = 0;
    /* access modifiers changed from: private */
    public float myLastHeading = -1.0f;
    /* access modifiers changed from: private */
    public LocationListener myLocationListener;
    /* access modifiers changed from: private */
    public LocationManager myLocationManager;
    /* access modifiers changed from: private */
    public double myLocationThreshold = 1.0d;
    /* access modifiers changed from: private */
    public Sensor myOrientationSensor;
    /* access modifiers changed from: private */
    public SensorEventListener myOrientationSensorListener;
    /* access modifiers changed from: private */
    public SensorManager mySensorManager;

    CoronaSensorManager() {
    }

    public void init() {
        if (Controller.isValid()) {
            this.mySensorManager = (SensorManager) Controller.getActivity().getSystemService("sensor");
            this.myLocationManager = (LocationManager) Controller.getActivity().getSystemService("location");
            for (int i = 0; i < this.myActiveSensors.length; i++) {
                this.myActiveSensors[i] = false;
            }
        }
    }

    public void setEventNotification(int eventType, boolean enable) {
        if (enable) {
            start(eventType);
        } else {
            stop(eventType);
        }
    }

    private void startType(final int eventType) {
        if (this.mySensorManager != null && Controller.isValid()) {
            Controller.getActivity().getHandler().post(new Runnable() {
                public void run() {
                    switch (eventType) {
                        case 0:
                        case 5:
                        default:
                            return;
                        case 1:
                            Sensor unused = CoronaSensorManager.this.myAccelerometerSensor = CoronaSensorManager.this.mySensorManager.getDefaultSensor(1);
                            SensorEventListener unused2 = CoronaSensorManager.this.myAccelerometerSensorListener = new SensorEventListener() {
                                /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
                                    return;
                                 */
                                /* Code decompiled incorrectly, please refer to instructions dump. */
                                public void onSensorChanged(android.hardware.SensorEvent r19) {
                                    /*
                                        r18 = this;
                                        monitor-enter(r18)
                                        r0 = r18
                                        com.ansca.corona.CoronaSensorManager$1 r0 = com.ansca.corona.CoronaSensorManager.AnonymousClass1.this     // Catch:{ all -> 0x004a }
                                        r2 = r0
                                        com.ansca.corona.CoronaSensorManager r2 = com.ansca.corona.CoronaSensorManager.this     // Catch:{ all -> 0x004a }
                                        long r2 = r2.myLastAccelerometerTime     // Catch:{ all -> 0x004a }
                                        r4 = 0
                                        int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                                        if (r2 > 0) goto L_0x0023
                                        r0 = r18
                                        com.ansca.corona.CoronaSensorManager$1 r0 = com.ansca.corona.CoronaSensorManager.AnonymousClass1.this     // Catch:{ all -> 0x004a }
                                        r2 = r0
                                        com.ansca.corona.CoronaSensorManager r2 = com.ansca.corona.CoronaSensorManager.this     // Catch:{ all -> 0x004a }
                                        r0 = r19
                                        long r0 = r0.timestamp     // Catch:{ all -> 0x004a }
                                        r3 = r0
                                        long unused = r2.myLastAccelerometerTime = r3     // Catch:{ all -> 0x004a }
                                        monitor-exit(r18)     // Catch:{ all -> 0x004a }
                                    L_0x0022:
                                        return
                                    L_0x0023:
                                        r0 = r19
                                        long r0 = r0.timestamp     // Catch:{ all -> 0x004a }
                                        r2 = r0
                                        r0 = r18
                                        com.ansca.corona.CoronaSensorManager$1 r0 = com.ansca.corona.CoronaSensorManager.AnonymousClass1.this     // Catch:{ all -> 0x004a }
                                        r4 = r0
                                        com.ansca.corona.CoronaSensorManager r4 = com.ansca.corona.CoronaSensorManager.this     // Catch:{ all -> 0x004a }
                                        long r4 = r4.myLastAccelerometerTime     // Catch:{ all -> 0x004a }
                                        long r11 = r2 - r4
                                        r2 = 1000(0x3e8, float:1.401E-42)
                                        r0 = r18
                                        com.ansca.corona.CoronaSensorManager$1 r0 = com.ansca.corona.CoronaSensorManager.AnonymousClass1.this     // Catch:{ all -> 0x004a }
                                        r3 = r0
                                        com.ansca.corona.CoronaSensorManager r3 = com.ansca.corona.CoronaSensorManager.this     // Catch:{ all -> 0x004a }
                                        int r3 = r3.myAccelerometerIntervalInHz     // Catch:{ all -> 0x004a }
                                        int r2 = r2 / r3
                                        long r13 = (long) r2     // Catch:{ all -> 0x004a }
                                        int r2 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
                                        if (r2 >= 0) goto L_0x004d
                                        monitor-exit(r18)     // Catch:{ all -> 0x004a }
                                        goto L_0x0022
                                    L_0x004a:
                                        r2 = move-exception
                                        monitor-exit(r18)     // Catch:{ all -> 0x004a }
                                        throw r2
                                    L_0x004d:
                                        r0 = r19
                                        long r0 = r0.timestamp     // Catch:{ all -> 0x004a }
                                        r2 = r0
                                        r0 = r18
                                        com.ansca.corona.CoronaSensorManager$1 r0 = com.ansca.corona.CoronaSensorManager.AnonymousClass1.this     // Catch:{ all -> 0x004a }
                                        r4 = r0
                                        com.ansca.corona.CoronaSensorManager r4 = com.ansca.corona.CoronaSensorManager.this     // Catch:{ all -> 0x004a }
                                        long r4 = r4.myLastAccelerometerTime     // Catch:{ all -> 0x004a }
                                        long r2 = r2 - r4
                                        double r2 = (double) r2     // Catch:{ all -> 0x004a }
                                        r4 = 4472406533629990549(0x3e112e0be826d695, double:1.0E-9)
                                        double r9 = r2 * r4
                                        r0 = r18
                                        com.ansca.corona.CoronaSensorManager$1 r0 = com.ansca.corona.CoronaSensorManager.AnonymousClass1.this     // Catch:{ all -> 0x004a }
                                        r2 = r0
                                        com.ansca.corona.CoronaSensorManager r2 = com.ansca.corona.CoronaSensorManager.this     // Catch:{ all -> 0x004a }
                                        r0 = r19
                                        long r0 = r0.timestamp     // Catch:{ all -> 0x004a }
                                        r3 = r0
                                        long unused = r2.myLastAccelerometerTime = r3     // Catch:{ all -> 0x004a }
                                        r0 = r19
                                        float[] r0 = r0.values     // Catch:{ all -> 0x004a }
                                        r2 = r0
                                        r3 = 0
                                        r2 = r2[r3]     // Catch:{ all -> 0x004a }
                                        float r2 = -r2
                                        r3 = 1092616192(0x41200000, float:10.0)
                                        float r15 = r2 / r3
                                        r0 = r19
                                        float[] r0 = r0.values     // Catch:{ all -> 0x004a }
                                        r2 = r0
                                        r3 = 1
                                        r2 = r2[r3]     // Catch:{ all -> 0x004a }
                                        float r2 = -r2
                                        r3 = 1092616192(0x41200000, float:10.0)
                                        float r16 = r2 / r3
                                        r0 = r19
                                        float[] r0 = r0.values     // Catch:{ all -> 0x004a }
                                        r2 = r0
                                        r3 = 2
                                        r2 = r2[r3]     // Catch:{ all -> 0x004a }
                                        float r2 = -r2
                                        r3 = 1092616192(0x41200000, float:10.0)
                                        float r17 = r2 / r3
                                        boolean r2 = com.ansca.corona.Controller.isValid()     // Catch:{ all -> 0x004a }
                                        if (r2 == 0) goto L_0x00b2
                                        com.ansca.corona.events.EventManager r2 = com.ansca.corona.Controller.getEventManager()     // Catch:{ all -> 0x004a }
                                        double r3 = (double) r15     // Catch:{ all -> 0x004a }
                                        r0 = r16
                                        double r0 = (double) r0     // Catch:{ all -> 0x004a }
                                        r5 = r0
                                        r0 = r17
                                        double r0 = (double) r0     // Catch:{ all -> 0x004a }
                                        r7 = r0
                                        r2.accelerometerEvent(r3, r5, r7, r9)     // Catch:{ all -> 0x004a }
                                    L_0x00b2:
                                        monitor-exit(r18)     // Catch:{ all -> 0x004a }
                                        goto L_0x0022
                                    */
                                    throw new UnsupportedOperationException("Method not decompiled: com.ansca.corona.CoronaSensorManager.AnonymousClass1.AnonymousClass1.onSensorChanged(android.hardware.SensorEvent):void");
                                }

                                public void onAccuracyChanged(Sensor arg0, int arg1) {
                                }
                            };
                            CoronaSensorManager.this.mySensorManager.registerListener(CoronaSensorManager.this.myAccelerometerSensorListener, CoronaSensorManager.this.myAccelerometerSensor, CoronaSensorManager.this.getSensorDelayFromFrequency(CoronaSensorManager.this.myAccelerometerIntervalInHz));
                            return;
                        case 2:
                            Sensor unused3 = CoronaSensorManager.this.myGyroscopeSensor = CoronaSensorManager.this.mySensorManager.getDefaultSensor(4);
                            SensorEventListener unused4 = CoronaSensorManager.this.myGyroscopeSensorListener = new SensorEventListener() {
                                /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
                                    return;
                                 */
                                /* Code decompiled incorrectly, please refer to instructions dump. */
                                public void onSensorChanged(android.hardware.SensorEvent r14) {
                                    /*
                                        r13 = this;
                                        monitor-enter(r13)
                                        com.ansca.corona.CoronaSensorManager$1 r0 = com.ansca.corona.CoronaSensorManager.AnonymousClass1.this     // Catch:{ all -> 0x0038 }
                                        com.ansca.corona.CoronaSensorManager r0 = com.ansca.corona.CoronaSensorManager.this     // Catch:{ all -> 0x0038 }
                                        long r0 = r0.myLastGyroscopeTime     // Catch:{ all -> 0x0038 }
                                        r2 = 0
                                        int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                                        if (r0 > 0) goto L_0x001a
                                        com.ansca.corona.CoronaSensorManager$1 r0 = com.ansca.corona.CoronaSensorManager.AnonymousClass1.this     // Catch:{ all -> 0x0038 }
                                        com.ansca.corona.CoronaSensorManager r0 = com.ansca.corona.CoronaSensorManager.this     // Catch:{ all -> 0x0038 }
                                        long r1 = r14.timestamp     // Catch:{ all -> 0x0038 }
                                        long unused = r0.myLastGyroscopeTime = r1     // Catch:{ all -> 0x0038 }
                                        monitor-exit(r13)     // Catch:{ all -> 0x0038 }
                                    L_0x0019:
                                        return
                                    L_0x001a:
                                        long r0 = r14.timestamp     // Catch:{ all -> 0x0038 }
                                        com.ansca.corona.CoronaSensorManager$1 r2 = com.ansca.corona.CoronaSensorManager.AnonymousClass1.this     // Catch:{ all -> 0x0038 }
                                        com.ansca.corona.CoronaSensorManager r2 = com.ansca.corona.CoronaSensorManager.this     // Catch:{ all -> 0x0038 }
                                        long r2 = r2.myLastGyroscopeTime     // Catch:{ all -> 0x0038 }
                                        long r9 = r0 - r2
                                        r0 = 1000(0x3e8, float:1.401E-42)
                                        com.ansca.corona.CoronaSensorManager$1 r1 = com.ansca.corona.CoronaSensorManager.AnonymousClass1.this     // Catch:{ all -> 0x0038 }
                                        com.ansca.corona.CoronaSensorManager r1 = com.ansca.corona.CoronaSensorManager.this     // Catch:{ all -> 0x0038 }
                                        int r1 = r1.myGyroscopeIntervalInHz     // Catch:{ all -> 0x0038 }
                                        int r0 = r0 / r1
                                        long r11 = (long) r0     // Catch:{ all -> 0x0038 }
                                        int r0 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
                                        if (r0 >= 0) goto L_0x003b
                                        monitor-exit(r13)     // Catch:{ all -> 0x0038 }
                                        goto L_0x0019
                                    L_0x0038:
                                        r0 = move-exception
                                        monitor-exit(r13)     // Catch:{ all -> 0x0038 }
                                        throw r0
                                    L_0x003b:
                                        long r0 = r14.timestamp     // Catch:{ all -> 0x0038 }
                                        com.ansca.corona.CoronaSensorManager$1 r2 = com.ansca.corona.CoronaSensorManager.AnonymousClass1.this     // Catch:{ all -> 0x0038 }
                                        com.ansca.corona.CoronaSensorManager r2 = com.ansca.corona.CoronaSensorManager.this     // Catch:{ all -> 0x0038 }
                                        long r2 = r2.myLastGyroscopeTime     // Catch:{ all -> 0x0038 }
                                        long r0 = r0 - r2
                                        double r0 = (double) r0     // Catch:{ all -> 0x0038 }
                                        r2 = 4472406533629990549(0x3e112e0be826d695, double:1.0E-9)
                                        double r7 = r0 * r2
                                        com.ansca.corona.CoronaSensorManager$1 r0 = com.ansca.corona.CoronaSensorManager.AnonymousClass1.this     // Catch:{ all -> 0x0038 }
                                        com.ansca.corona.CoronaSensorManager r0 = com.ansca.corona.CoronaSensorManager.this     // Catch:{ all -> 0x0038 }
                                        long r1 = r14.timestamp     // Catch:{ all -> 0x0038 }
                                        long unused = r0.myLastGyroscopeTime = r1     // Catch:{ all -> 0x0038 }
                                        boolean r0 = com.ansca.corona.Controller.isValid()     // Catch:{ all -> 0x0038 }
                                        if (r0 == 0) goto L_0x0076
                                        com.ansca.corona.events.EventManager r0 = com.ansca.corona.Controller.getEventManager()     // Catch:{ all -> 0x0038 }
                                        float[] r1 = r14.values     // Catch:{ all -> 0x0038 }
                                        r2 = 0
                                        r1 = r1[r2]     // Catch:{ all -> 0x0038 }
                                        double r1 = (double) r1     // Catch:{ all -> 0x0038 }
                                        float[] r3 = r14.values     // Catch:{ all -> 0x0038 }
                                        r4 = 1
                                        r3 = r3[r4]     // Catch:{ all -> 0x0038 }
                                        double r3 = (double) r3     // Catch:{ all -> 0x0038 }
                                        float[] r5 = r14.values     // Catch:{ all -> 0x0038 }
                                        r6 = 2
                                        r5 = r5[r6]     // Catch:{ all -> 0x0038 }
                                        double r5 = (double) r5     // Catch:{ all -> 0x0038 }
                                        r0.gyroscopeEvent(r1, r3, r5, r7)     // Catch:{ all -> 0x0038 }
                                    L_0x0076:
                                        monitor-exit(r13)     // Catch:{ all -> 0x0038 }
                                        goto L_0x0019
                                    */
                                    throw new UnsupportedOperationException("Method not decompiled: com.ansca.corona.CoronaSensorManager.AnonymousClass1.AnonymousClass2.onSensorChanged(android.hardware.SensorEvent):void");
                                }

                                public void onAccuracyChanged(Sensor arg0, int arg1) {
                                }
                            };
                            CoronaSensorManager.this.mySensorManager.registerListener(CoronaSensorManager.this.myGyroscopeSensorListener, CoronaSensorManager.this.myGyroscopeSensor, CoronaSensorManager.this.getSensorDelayFromFrequency(CoronaSensorManager.this.myGyroscopeIntervalInHz));
                            return;
                        case 3:
                            LocationListener unused5 = CoronaSensorManager.this.myLocationListener = new LocationListener() {
                                public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
                                }

                                public void onProviderEnabled(String arg0) {
                                }

                                public void onProviderDisabled(String arg0) {
                                }

                                public void onLocationChanged(Location loc) {
                                    Controller.getEventManager().locationEvent(loc.getLatitude(), loc.getLongitude(), loc.getAltitude(), (double) loc.getAccuracy(), (double) loc.getSpeed(), (double) loc.getBearing(), loc.getTime());
                                }
                            };
                            CoronaSensorManager.this.myLocationManager.requestLocationUpdates("gps", 1000, (float) CoronaSensorManager.this.myLocationThreshold, CoronaSensorManager.this.myLocationListener);
                            return;
                        case 4:
                            Sensor unused6 = CoronaSensorManager.this.myOrientationSensor = CoronaSensorManager.this.mySensorManager.getDefaultSensor(3);
                            SensorEventListener unused7 = CoronaSensorManager.this.myOrientationSensorListener = new SensorEventListener() {
                                public void onSensorChanged(SensorEvent event) {
                                    if (CoronaSensorManager.this.myLastHeading != event.values[0]) {
                                        float unused = CoronaSensorManager.this.myLastHeading = event.values[0];
                                        Controller.getEventManager().headingEvent(CoronaSensorManager.this.myLastHeading);
                                    }
                                }

                                public void onAccuracyChanged(Sensor arg0, int arg1) {
                                }
                            };
                            CoronaSensorManager.this.mySensorManager.registerListener(CoronaSensorManager.this.myOrientationSensorListener, CoronaSensorManager.this.myOrientationSensor, 2);
                            return;
                    }
                }
            });
        }
    }

    private void stopType(final int eventType) {
        if (Controller.isValid()) {
            Controller.getActivity().getHandler().post(new Runnable() {
                public void run() {
                    switch (eventType) {
                        case 0:
                        case 5:
                        default:
                            return;
                        case 1:
                            if (CoronaSensorManager.this.myAccelerometerSensorListener != null) {
                                CoronaSensorManager.this.mySensorManager.unregisterListener(CoronaSensorManager.this.myAccelerometerSensorListener);
                                SensorEventListener unused = CoronaSensorManager.this.myAccelerometerSensorListener = null;
                                long unused2 = CoronaSensorManager.this.myLastAccelerometerTime = 0;
                                return;
                            }
                            return;
                        case 2:
                            if (CoronaSensorManager.this.myGyroscopeSensorListener != null) {
                                CoronaSensorManager.this.mySensorManager.unregisterListener(CoronaSensorManager.this.myGyroscopeSensorListener);
                                SensorEventListener unused3 = CoronaSensorManager.this.myGyroscopeSensorListener = null;
                                long unused4 = CoronaSensorManager.this.myLastGyroscopeTime = 0;
                                return;
                            }
                            return;
                        case 3:
                            if (CoronaSensorManager.this.myLocationListener != null) {
                                CoronaSensorManager.this.myLocationManager.removeUpdates(CoronaSensorManager.this.myLocationListener);
                                LocationListener unused5 = CoronaSensorManager.this.myLocationListener = null;
                                return;
                            }
                            return;
                        case 4:
                            if (CoronaSensorManager.this.myOrientationSensorListener != null) {
                                CoronaSensorManager.this.mySensorManager.unregisterListener(CoronaSensorManager.this.myOrientationSensorListener);
                                SensorEventListener unused6 = CoronaSensorManager.this.myOrientationSensorListener = null;
                                float unused7 = CoronaSensorManager.this.myLastHeading = -1.0f;
                                return;
                            }
                            return;
                    }
                }
            });
        }
    }

    public boolean isActive(int eventType) {
        return this.myActiveSensors[eventType];
    }

    public void start(int eventType) {
        if (!this.myActiveSensors[eventType]) {
            startType(eventType);
            this.myActiveSensors[eventType] = true;
        }
    }

    public void stop(int eventType) {
        if (this.myActiveSensors[eventType]) {
            stopType(eventType);
            this.myActiveSensors[eventType] = false;
        }
    }

    public void stop() {
        for (int i = 0; i < 6; i++) {
            stop(i);
        }
    }

    public void setAccelerometerInterval(int frequency) {
        if (this.myAccelerometerIntervalInHz != frequency) {
            this.myAccelerometerIntervalInHz = frequency;
        }
    }

    public void setGyroscopeInterval(int frequency) {
        if (this.myGyroscopeIntervalInHz != frequency) {
            this.myGyroscopeIntervalInHz = frequency;
        }
    }

    public boolean hasAccelerometer() {
        if (this.mySensorManager != null) {
            return this.mySensorManager.getSensorList(1).size() > 0;
        }
        return false;
    }

    public boolean hasGyroscope() {
        if (this.mySensorManager != null) {
            return this.mySensorManager.getSensorList(4).size() > 0;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public int getSensorDelayFromFrequency(int frequency) {
        if (frequency <= 10) {
            return 2;
        }
        if (frequency <= 30) {
            return 3;
        }
        if (frequency <= 60) {
            return 1;
        }
        return 0;
    }

    public void pause() {
        for (int i = 0; i < 6; i++) {
            stopType(i);
        }
    }

    public void resume() {
        for (int i = 0; i < 6; i++) {
            if (this.myActiveSensors[i]) {
                startType(i);
            }
        }
    }

    public void setLocationAccuracy(double meters) {
    }

    public void setLocationThreshold(double meters) {
        this.myLocationThreshold = meters;
    }
}
