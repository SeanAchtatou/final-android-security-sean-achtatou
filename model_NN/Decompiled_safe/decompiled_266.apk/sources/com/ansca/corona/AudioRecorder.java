package com.ansca.corona;

import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.Process;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;

public class AudioRecorder {
    private static final int ENCODING = 2;
    private static final int SAMPLE_RATE = 8000;
    private static final int STATUS_ERROR = -1;
    private static final int STATUS_OK = 0;
    AudioByteBufferHolder myCurrentBuffer;
    private Handler myHandler;
    /* access modifiers changed from: private */
    public int myId;
    boolean myIsNotifying = false;
    /* access modifiers changed from: private */
    public boolean myIsRunning = false;
    private MediaRecorder myMediaRecordInstance;
    private AudioRecorderThread myRecorderThread;

    AudioRecorder(Handler h) {
        this.myHandler = h;
    }

    public void setId(int id) {
        this.myId = id;
    }

    public void startRecording(String path) {
        if (!this.myIsRunning) {
            if (path == null || path.equals("")) {
                this.myRecorderThread = new AudioRecorderThread();
                this.myRecorderThread.startRecording();
                this.myIsRunning = true;
                return;
            }
            int outputFormat = 0;
            int audioEncoder = 0;
            if (path.endsWith(".3gp")) {
                outputFormat = 1;
                audioEncoder = 1;
            }
            try {
                File parentDir = new File(path).getParentFile();
                if (!parentDir.exists()) {
                    parentDir.mkdirs();
                }
                this.myMediaRecordInstance = new MediaRecorder();
                this.myMediaRecordInstance.setAudioSource(1);
                this.myMediaRecordInstance.setOutputFormat(outputFormat);
                this.myMediaRecordInstance.setAudioEncoder(audioEncoder);
                this.myMediaRecordInstance.setOutputFile(path);
                this.myMediaRecordInstance.setOnErrorListener(new MediaRecorder.OnErrorListener() {
                    public void onError(MediaRecorder arg0, int arg1, int arg2) {
                        System.out.println("MediaRecorder error " + arg1 + " " + arg2);
                        AudioRecorder.this.stopRecording();
                    }
                });
                this.myMediaRecordInstance.setOnInfoListener(new MediaRecorder.OnInfoListener() {
                    public void onInfo(MediaRecorder arg0, int arg1, int arg2) {
                        System.out.println("MediaRecorder info " + arg1 + " " + arg2);
                    }
                });
                this.myMediaRecordInstance.prepare();
                this.myMediaRecordInstance.start();
                this.myIsRunning = true;
            } catch (IOException e) {
                postStatus(-1);
            }
        }
    }

    public void stopRecording() {
        if (this.myIsRunning) {
            if (this.myMediaRecordInstance != null) {
                this.myMediaRecordInstance.stop();
                this.myMediaRecordInstance.release();
                this.myMediaRecordInstance = null;
            }
            if (this.myRecorderThread != null) {
                this.myRecorderThread.stopRecording();
                this.myRecorderThread = null;
            }
        }
        this.myIsRunning = false;
    }

    public static class AudioByteBufferHolder {
        ByteBuffer myBuffer;
        int myValidBytes = 0;

        AudioByteBufferHolder(int capacity) {
            this.myBuffer = ByteBuffer.allocateDirect(capacity);
        }
    }

    private class AudioRecorderThread extends Thread {
        static final int MAX_BUFFERS = 5;
        private AudioRecord myAudioRecordInstance;
        private int myBufferSize;
        private LinkedList<AudioByteBufferHolder> myBuffers = new LinkedList<>();
        private float myFrameSeconds = 0.1f;
        private LinkedList<AudioByteBufferHolder> myFreeBuffers = new LinkedList<>();

        AudioRecorderThread() {
        }

        /* access modifiers changed from: package-private */
        public void startRecording() {
            start();
        }

        /* access modifiers changed from: package-private */
        public void stopRecording() {
            boolean unused = AudioRecorder.this.myIsRunning = false;
        }

        public void run() {
            try {
                Process.setThreadPriority(-19);
                int minBufferSize = AudioRecord.getMinBufferSize(AudioRecorder.SAMPLE_RATE, Controller.getAndroidVersionSpecific().audioChannelMono(), 2);
                int bufferSize = (int) (16000.0f * this.myFrameSeconds);
                if (bufferSize < minBufferSize) {
                    bufferSize = minBufferSize;
                }
                this.myBufferSize = bufferSize;
                this.myAudioRecordInstance = new AudioRecord(1, AudioRecorder.SAMPLE_RATE, Controller.getAndroidVersionSpecific().audioChannelMono(), 2, this.myBufferSize);
                this.myFreeBuffers.add(new AudioByteBufferHolder(this.myBufferSize));
                this.myAudioRecordInstance.startRecording();
                while (AudioRecorder.this.myIsRunning) {
                    AudioByteBufferHolder readBuffer = null;
                    synchronized (this) {
                        if (!this.myFreeBuffers.isEmpty()) {
                            readBuffer = this.myFreeBuffers.remove();
                        } else if (this.myBuffers.size() < 5) {
                            readBuffer = new AudioByteBufferHolder(this.myBufferSize);
                        }
                    }
                    if (readBuffer != null) {
                        int bytesRead = this.myAudioRecordInstance.read(readBuffer.myBuffer, this.myBufferSize);
                        synchronized (this) {
                            if (bytesRead > 0) {
                                readBuffer.myValidBytes = bytesRead;
                                this.myBuffers.add(readBuffer);
                                AudioRecorder.this.postStatus(bytesRead);
                            } else {
                                this.myFreeBuffers.add(readBuffer);
                            }
                        }
                    }
                    sleep(15);
                }
                this.myAudioRecordInstance.stop();
                AudioRecorder.this.postStatus(0);
            } catch (Exception e) {
                AudioRecorder.this.postStatus(-1);
            }
        }

        /* access modifiers changed from: package-private */
        public AudioByteBufferHolder getNextBuffer() {
            synchronized (this.myBuffers) {
                if (this.myBuffers.isEmpty()) {
                    return null;
                }
                AudioByteBufferHolder result = this.myBuffers.remove();
                return result;
            }
        }

        /* access modifiers changed from: package-private */
        public void releaseBuffer(AudioByteBufferHolder buffer) {
            synchronized (this.myFreeBuffers) {
                this.myFreeBuffers.add(buffer);
                if (!AudioRecorder.this.myIsRunning) {
                    this.myFreeBuffers.clear();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void postStatus(final int status) {
        if (!this.myIsNotifying) {
            this.myIsNotifying = true;
            this.myHandler.post(new Runnable() {
                public void run() {
                    Controller.getPortal().recordCallback(status, AudioRecorder.this.myId);
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    public AudioByteBufferHolder getNextBuffer() {
        if (this.myRecorderThread != null) {
            this.myCurrentBuffer = this.myRecorderThread.getNextBuffer();
        } else {
            this.myCurrentBuffer = null;
        }
        return this.myCurrentBuffer;
    }

    /* access modifiers changed from: package-private */
    public AudioByteBufferHolder getCurrentBuffer() {
        return this.myCurrentBuffer;
    }

    /* access modifiers changed from: package-private */
    public synchronized void releaseCurrentBuffer() {
        if (this.myCurrentBuffer != null) {
            this.myRecorderThread.releaseBuffer(this.myCurrentBuffer);
            this.myCurrentBuffer = null;
        }
        this.myIsNotifying = false;
    }
}
