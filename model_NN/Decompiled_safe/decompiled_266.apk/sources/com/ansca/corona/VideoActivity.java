package com.ansca.corona;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoActivity extends Activity {
    /* access modifiers changed from: private */
    public int myVideoId;
    private VideoView myVideoView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.corona_video_view);
        this.myVideoView = (VideoView) findViewById(R.id.corona_video_view);
        this.myVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer player) {
                Controller.getEventManager().videoEnded(VideoActivity.this.myVideoId);
                Controller.getMediaManager().resumeAll();
                VideoActivity.this.finish();
            }
        });
        this.myVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int arg1, int arg2) {
                Controller.getEventManager().videoEnded(VideoActivity.this.myVideoId);
                Controller.getMediaManager().resumeAll();
                VideoActivity.this.finish();
                return false;
            }
        });
        if (getIntent().getExtras().getBoolean("media_controls_enabled")) {
            this.myVideoView.setMediaController(new MediaController(this));
        }
        this.myVideoView.setVideoURI(Uri.parse(getIntent().getExtras().getString("video_uri")));
        this.myVideoId = getIntent().getExtras().getInt("video_id");
        this.myVideoView.start();
    }
}
