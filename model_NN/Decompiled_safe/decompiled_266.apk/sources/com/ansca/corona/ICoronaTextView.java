package com.ansca.corona;

import android.widget.TextView;

public interface ICoronaTextView {
    void destroying();

    TextView getTextView();

    String getTextViewAlign();

    int getTextViewColor();

    String getTextViewInputType();

    boolean getTextViewPassword();

    float getTextViewSize();

    void setTextViewAlign(String str);

    void setTextViewColor(int i);

    void setTextViewFont(String str, float f, CoronaActivity coronaActivity);

    void setTextViewInputType(String str);

    void setTextViewPassword(boolean z);

    void setTextViewSize(float f);
}
