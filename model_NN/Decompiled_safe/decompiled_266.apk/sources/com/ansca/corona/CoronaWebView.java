package com.ansca.corona;

import android.content.Context;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class CoronaWebView extends WebView {
    /* access modifiers changed from: private */
    public int myId;
    /* access modifiers changed from: private */
    public String myOriginalUrl;

    private class CoronaWebViewClient extends WebViewClient {
        private CoronaWebViewClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            synchronized (this) {
                Controller.getEventManager().shouldLoadUrlEvent(CoronaWebView.this.myId, view, CoronaWebView.this.myOriginalUrl, url);
            }
            return true;
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            synchronized (this) {
                Controller.getEventManager().didFailLoadUrlEvent(CoronaWebView.this.myId, failingUrl, description, errorCode);
            }
        }
    }

    public CoronaWebView(Context context, int id, boolean background) {
        super(context);
        this.myId = id;
        setBackgroundOpaque(background);
        setWebViewClient(new CoronaWebViewClient());
    }

    public void requestLoadUrl(String url) {
        this.myOriginalUrl = url;
        synchronized (this) {
            Controller.getEventManager().shouldLoadUrlEvent(this.myId, this, this.myOriginalUrl, url);
        }
    }

    public void setBackgroundOpaque(boolean bg) {
        if (bg) {
            setBackgroundColor(-1);
        } else {
            setBackgroundColor(0);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || canGoBack()) {
            return super.onKeyDown(keyCode, event);
        }
        ViewManager.getViewManager().closeWebView(this.myId);
        return true;
    }
}
