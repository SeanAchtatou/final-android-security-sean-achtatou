package com.ansca.corona;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.MailTo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Toast;
import com.adknowledge.superrewards.SuperRewardsImpl;
import com.adknowledge.superrewards.model.SROffer;
import com.adknowledge.superrewards.model.SRUserPoints;
import com.adknowledge.superrewards.tracking.SRAppInstallTracker;
import com.ansca.corona.events.EventManager;
import com.ansca.corona.version.AndroidVersionSpecificFactory;
import com.ansca.corona.version.IAndroidVersionSpecific;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.flurry.android.FlurryAgent;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;
import com.openfeint.api.resource.Achievement;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;
import com.openfeint.api.ui.Dashboard;
import com.papaya.achievement.PPYAchievement;
import com.papaya.achievement.PPYAchievementDelegate;
import com.papaya.social.PPYSocial;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

public class Controller {
    private static final String FACEBOOK_SESSION_PREFERENCES_NAME = "facebook-session";
    private static final String FACEBOOK_SESSION_PREFERENCE_ACCESS_TOKEN_KEY = "access_token";
    private static final String FACEBOOK_SESSION_PREFERENCE_EXPIRATION_TIME_KEY = "expires_in";
    static final int kLocaleCountry = 2;
    static final int kLocaleIdentifier = 0;
    static final int kLocaleLanguage = 1;
    static final int kUILanguage = 3;
    static Controller theController;
    /* access modifiers changed from: private */
    public boolean fPapayaInited = false;
    /* access modifiers changed from: private */
    public CoronaActivity myActivity;
    private AdManager myAdManager;
    /* access modifiers changed from: private */
    public AlertDialog myAlertDialog;
    private IAndroidVersionSpecific myAndroidVersion;
    private NativeToJavaBridge myBridge;
    private EventManager myEventManager;
    /* access modifiers changed from: private */
    public Facebook myFacebook;
    private AsyncFacebookRunner myFacebookRunner;
    private boolean myFlurrySessionStarted = false;
    private boolean myIdleEnabled;
    private boolean myInitialResume;
    private boolean myIsResumed = false;
    private MediaManager myMediaManager;
    /* access modifiers changed from: private */
    public boolean myOpenFeintInited = false;
    private JavaToNativeShim myPortal;
    private boolean myResumed;
    /* access modifiers changed from: private */
    public String myRewardsAppId;
    private boolean myRewardsInitialized = false;
    /* access modifiers changed from: private */
    public SRAppInstallTracker myRewardsTracker;
    /* access modifiers changed from: private */
    public String myRewardsUid;
    private CoronaSensorManager mySensorManager;
    /* access modifiers changed from: private */
    public Handler myTimerHandler = new Handler();
    /* access modifiers changed from: private */
    public int myTimerMilliseconds;
    private Runnable myTimerTask;

    public static void create(CoronaActivity activity) {
        if (theController == null) {
            theController = new Controller(activity);
            theController.init();
        }
    }

    public static Controller getController() {
        return theController;
    }

    private Controller(CoronaActivity activity) {
        this.myActivity = activity;
        this.myBridge = new NativeToJavaBridge(activity);
        this.myPortal = new JavaToNativeShim();
        this.myAdManager = new AdManager(activity);
        this.myMediaManager = new MediaManager(activity);
        this.mySensorManager = new CoronaSensorManager();
        this.myEventManager = new EventManager();
    }

    private synchronized void init() {
        this.myMediaManager.init();
        this.mySensorManager.init();
        this.myTimerMilliseconds = 0;
        this.myInitialResume = true;
        this.myResumed = false;
        this.myAndroidVersion = AndroidVersionSpecificFactory.create();
        this.myIdleEnabled = true;
    }

    public static IAndroidVersionSpecific getAndroidVersionSpecific() {
        if (theController == null) {
            return null;
        }
        return theController.myAndroidVersion;
    }

    public static boolean isMultitouchEnabled() {
        if (theController == null) {
            return false;
        }
        return getAndroidVersionSpecific().apiVersion() >= 5 && theController.mySensorManager.isActive(5);
    }

    public synchronized void resume() {
        if (!this.myIsResumed) {
            this.myIsResumed = true;
            if (!this.myInitialResume) {
                getPortal().resume();
                this.myResumed = true;
            } else {
                this.myInitialResume = false;
            }
            this.myMediaManager.resumeAll();
            this.mySensorManager.resume();
            startTimer();
            openFeintResume();
            internalSetIdleTimer(this.myIdleEnabled);
        }
    }

    public synchronized void pause() {
        if (this.myIsResumed) {
            stopTimer();
            this.mySensorManager.pause();
            getPortal().pause();
            this.myMediaManager.pauseAll();
            internalSetIdleTimer(true);
            this.myIsResumed = false;
        }
    }

    public void restart() {
    }

    public void start() {
    }

    public void stop() {
        flurryStop();
    }

    public synchronized void destroy() {
        stopTimer();
        this.myAdManager.hideAllBanners();
        this.mySensorManager.stop();
        this.myMediaManager.release();
        this.myPortal.destroy();
        theController = null;
    }

    public static boolean isValid() {
        return theController != null;
    }

    public static void onDrawFrame() {
        if (theController != null) {
            synchronized (theController) {
                if (isRunning()) {
                    getEventManager().sendEvents();
                    getPortal().render();
                }
            }
        }
    }

    public static boolean isRunning() {
        return theController != null && theController.myIsResumed;
    }

    public static CoronaActivity getActivity() {
        if (theController == null) {
            return null;
        }
        return theController.myActivity;
    }

    public static EventManager getEventManager() {
        if (theController == null) {
            return null;
        }
        return theController.myEventManager;
    }

    public static NativeToJavaBridge getBridge() {
        if (theController == null) {
            return null;
        }
        return theController.myBridge;
    }

    public static AdManager getAdManager() {
        if (theController == null) {
            return null;
        }
        return theController.myAdManager;
    }

    public static MediaManager getMediaManager() {
        if (theController == null) {
            return null;
        }
        return theController.myMediaManager;
    }

    public static JavaToNativeShim getPortal() {
        if (theController == null) {
            return null;
        }
        return theController.myPortal;
    }

    public static void requestRender() {
        if (isValid() && theController.myActivity != null) {
            theController.myActivity.requestRender();
        }
    }

    public static void requestEventRender() {
        if (isValid() && theController.myTimerTask == null) {
            theController.myActivity.requestRender();
        }
    }

    public void setTimer(int milliseconds) {
        this.myTimerMilliseconds = milliseconds;
        startTimer();
    }

    public void startTimer() {
        if (this.myTimerMilliseconds != 0 && this.myTimerTask == null) {
            this.myTimerTask = new Runnable() {
                public void run() {
                    if (Controller.this.myTimerMilliseconds != 0) {
                        Controller.this.myTimerHandler.postDelayed(this, (long) Controller.this.myTimerMilliseconds);
                        if (Controller.isRunning()) {
                            Controller.requestRender();
                        }
                    }
                }
            };
            this.myTimerHandler.postDelayed(this.myTimerTask, (long) this.myTimerMilliseconds);
        }
    }

    public void cancelTimer() {
        stopTimer();
        this.myTimerMilliseconds = 0;
    }

    public void stopTimer() {
        if (this.myTimerTask != null) {
            this.myTimerHandler.removeCallbacks(this.myTimerTask);
            this.myTimerTask = null;
        }
    }

    public void displayUpdate() {
        if (!this.myResumed) {
            this.myActivity.setNeedsSwap();
        }
        this.myResumed = false;
    }

    public void httpPost(String url, String key, String value) {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpContext localContext = new BasicHttpContext();
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
        try {
            httpPost.setEntity(new StringEntity(key + "=" + value, "UTF-8"));
            httpClient.execute(httpPost, localContext);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }

    public void openUrl(String url) {
        String localFilePath;
        String action = "android.intent.action.VIEW";
        if (url != null && url.length() > 0) {
            Uri uri = Uri.parse(url);
            String applicationPath = "/data/data/" + this.myActivity.getApplicationContext().getPackageName();
            if (url.indexOf(applicationPath) >= 0 || url.startsWith("content://")) {
                if (uri.getScheme() == null || !uri.getScheme().equals("content")) {
                    int index = url.lastIndexOf(applicationPath);
                    if (index < 0 || applicationPath.length() + index >= url.length()) {
                        localFilePath = url;
                    } else {
                        localFilePath = url.substring(applicationPath.length() + index);
                    }
                    uri = Uri.parse("content://" + this.myActivity.getApplicationContext().getPackageName() + ".files" + localFilePath);
                }
                String mimeTypeName = null;
                int index2 = url.lastIndexOf(46);
                if (index2 >= 0 && index2 + 1 < url.length()) {
                    String fileExtension = url.substring(index2 + 1);
                    MimeTypeMap mapping = MimeTypeMap.getSingleton();
                    if (mapping.hasExtension(fileExtension)) {
                        mimeTypeName = mapping.getMimeTypeFromExtension(fileExtension);
                    }
                    if (mimeTypeName == null) {
                        mimeTypeName = "application/" + fileExtension;
                    }
                }
                try {
                    Intent intent = new Intent("android.intent.action.VIEW", uri);
                    if (mimeTypeName != null) {
                        intent.setDataAndType(uri, mimeTypeName);
                    }
                    this.myActivity.startActivity(intent);
                } catch (Exception e) {
                }
            } else if (MailTo.isMailTo(url)) {
                MailTo mailSettings = MailTo.parse(url);
                Intent intent2 = new Intent("android.intent.action.SEND");
                intent2.setType("plain/text");
                if (mailSettings.getTo() != null) {
                    intent2.putExtra("android.intent.extra.EMAIL", mailSettings.getTo().split(","));
                }
                if (mailSettings.getCc() != null) {
                    intent2.putExtra("android.intent.extra.CC", mailSettings.getCc().split(","));
                }
                if (mailSettings.getSubject() != null) {
                    intent2.putExtra("android.intent.extra.SUBJECT", mailSettings.getSubject());
                }
                if (mailSettings.getBody() != null) {
                    intent2.putExtra("android.intent.extra.TEXT", mailSettings.getBody());
                }
                for (Map.Entry<String, String> item : mailSettings.getHeaders().entrySet()) {
                    if (((String) item.getKey()).toLowerCase().equals("bcc")) {
                        intent2.putExtra("android.intent.extra.BCC", ((String) item.getValue()).split(","));
                    }
                }
                try {
                    this.myActivity.startActivityForResult(Intent.createChooser(intent2, "Send mail..."), 3);
                } catch (Exception e2) {
                }
            } else {
                if (uri.getScheme() == null) {
                    if (!url.startsWith("http://")) {
                        uri = Uri.parse("http://" + url);
                    }
                } else if (uri.getScheme().equals("tel")) {
                    action = "android.intent.action.CALL";
                }
                try {
                    this.myActivity.startActivityForResult(new Intent(action, uri), 2);
                } catch (Exception e3) {
                }
            }
        }
    }

    public void saveBitmap(Bitmap bitmap, String name) {
        ContentValues values = new ContentValues();
        values.put(SROffer.TITLE, name);
        values.put("date_added", Long.valueOf(System.currentTimeMillis()));
        values.put("mime_type", "image/jpeg");
        try {
            OutputStream outStream = this.myActivity.getContentResolver().openOutputStream(this.myActivity.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outStream);
            outStream.flush();
            outStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private void internalSetIdleTimer(boolean enabled) {
        if (enabled) {
            this.myActivity.runOnUiThread(new Runnable() {
                public void run() {
                    Window window = Controller.this.myActivity.getWindow();
                    if (window != null) {
                        window.clearFlags(128);
                    }
                }
            });
        } else {
            this.myActivity.runOnUiThread(new Runnable() {
                public void run() {
                    Window window = Controller.this.myActivity.getWindow();
                    if (window != null) {
                        window.addFlags(128);
                    }
                }
            });
        }
    }

    public void setIdleTimer(boolean enabled) {
        internalSetIdleTimer(enabled);
        this.myIdleEnabled = enabled;
    }

    public boolean getIdleTimer() {
        return this.myIdleEnabled;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
        if (r9.length >= 1) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001b, code lost:
        r0.setTitle(r7);
        r0.setMessage(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0021, code lost:
        r6.myActivity.runOnUiThread(new com.ansca.corona.Controller.AnonymousClass5(r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0030, code lost:
        if (r9.length < 1) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0034, code lost:
        if (r9.length > 3) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0036, code lost:
        r0.setTitle(r7);
        r0.setMessage(r8);
        r0.setPositiveButton(r9[0], r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0043, code lost:
        if (r9.length <= 1) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0045, code lost:
        r0.setNeutralButton(r9[1], r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004b, code lost:
        if (r9.length <= 2) goto L_0x0021;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004d, code lost:
        r0.setNegativeButton(r9[2], r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0053, code lost:
        r0.setTitle(r7 + ": " + r8);
        r0.setItems(r9, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000a, code lost:
        r0 = new android.app.AlertDialog.Builder(r6.myActivity);
        r1 = new com.ansca.corona.Controller.AnonymousClass4(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0016, code lost:
        if (r9 == null) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void showNativeAlert(java.lang.String r7, java.lang.String r8, java.lang.String[] r9) {
        /*
            r6 = this;
            r5 = 2
            r4 = 1
            monitor-enter(r6)
            android.app.AlertDialog r2 = r6.myAlertDialog     // Catch:{ all -> 0x002c }
            if (r2 == 0) goto L_0x0009
            monitor-exit(r6)     // Catch:{ all -> 0x002c }
        L_0x0008:
            return
        L_0x0009:
            monitor-exit(r6)     // Catch:{ all -> 0x002c }
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            com.ansca.corona.CoronaActivity r2 = r6.myActivity
            r0.<init>(r2)
            com.ansca.corona.Controller$4 r1 = new com.ansca.corona.Controller$4
            r1.<init>()
            if (r9 == 0) goto L_0x001b
            int r2 = r9.length
            if (r2 >= r4) goto L_0x002f
        L_0x001b:
            r0.setTitle(r7)
            r0.setMessage(r8)
        L_0x0021:
            com.ansca.corona.CoronaActivity r2 = r6.myActivity
            com.ansca.corona.Controller$5 r3 = new com.ansca.corona.Controller$5
            r3.<init>(r0)
            r2.runOnUiThread(r3)
            goto L_0x0008
        L_0x002c:
            r2 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x002c }
            throw r2
        L_0x002f:
            int r2 = r9.length
            if (r2 < r4) goto L_0x0053
            int r2 = r9.length
            r3 = 3
            if (r2 > r3) goto L_0x0053
            r0.setTitle(r7)
            r0.setMessage(r8)
            r2 = 0
            r2 = r9[r2]
            r0.setPositiveButton(r2, r1)
            int r2 = r9.length
            if (r2 <= r4) goto L_0x004a
            r2 = r9[r4]
            r0.setNeutralButton(r2, r1)
        L_0x004a:
            int r2 = r9.length
            if (r2 <= r5) goto L_0x0021
            r2 = r9[r5]
            r0.setNegativeButton(r2, r1)
            goto L_0x0021
        L_0x0053:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r2 = r2.append(r7)
            java.lang.String r3 = ": "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r8)
            java.lang.String r2 = r2.toString()
            r0.setTitle(r2)
            r0.setItems(r9, r1)
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ansca.corona.Controller.showNativeAlert(java.lang.String, java.lang.String, java.lang.String[]):void");
    }

    public void cancelNativeAlert(final int buttonIndex) {
        final AlertDialog theDialog;
        if (this.myAlertDialog != null) {
            synchronized (this) {
                theDialog = this.myAlertDialog;
                this.myAlertDialog = null;
            }
            this.myActivity.runOnUiThread(new Runnable() {
                public void run() {
                    theDialog.cancel();
                    Controller.getEventManager().nativeAlertResult(buttonIndex);
                }
            });
        }
    }

    public void showTrialAlert() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this.myActivity);
        DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int which) {
                Controller.this.openUrl("http://www.anscamobile.com/corona/?utm_source=corona-sdk&utm_medium=corona-sdk&utm_campaign=trial-popup");
            }
        };
        builder.setTitle("Corona SDK Trial");
        builder.setMessage("This message only appears in the trial version");
        builder.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
        builder.setNeutralButton("Learn More", clickListener);
        this.myActivity.runOnUiThread(new Runnable() {
            public void run() {
                builder.show();
            }
        });
    }

    public void setAccelerometerInterval(int frequency) {
        this.mySensorManager.setAccelerometerInterval(frequency);
    }

    public void setGyroscopeInterval(int frequency) {
        this.mySensorManager.setGyroscopeInterval(frequency);
    }

    public boolean hasAccelerometer() {
        return this.mySensorManager.hasAccelerometer();
    }

    public boolean hasGyroscope() {
        return this.mySensorManager.hasGyroscope();
    }

    public void setEventNotification(int eventType, boolean enable) {
        this.mySensorManager.setEventNotification(eventType, enable);
    }

    public void vibrate() {
        ((Vibrator) this.myActivity.getSystemService("vibrator")).vibrate(100);
    }

    public String getModel() {
        return Build.MODEL;
    }

    public String getName() {
        return "unknown";
    }

    public String getUniqueIdentifier() {
        return ((TelephonyManager) this.myActivity.getSystemService("phone")).getDeviceId();
    }

    public String getPlatformVersion() {
        return Build.VERSION.RELEASE;
    }

    public String getProductName() {
        return Build.PRODUCT;
    }

    public String getPreference(int category) {
        switch (category) {
            case 0:
                return Locale.getDefault().toString();
            case 1:
                return Locale.getDefault().getLanguage();
            case 2:
                return Locale.getDefault().getCountry();
            case 3:
                return Locale.getDefault().getDisplayLanguage();
            default:
                System.err.println("getPreference: Unknown category " + category);
                return "";
        }
    }

    public void facebookLogin(String appId, final String[] permissions) {
        SharedPreferences savedSession;
        if (this.myActivity != null && appId != null) {
            if (this.myFacebook == null) {
                try {
                    this.myFacebook = new Facebook(appId);
                    this.myFacebookRunner = new AsyncFacebookRunner(this.myFacebook);
                } catch (Exception e) {
                    this.myFacebook = null;
                    this.myFacebookRunner = null;
                    getEventManager().fbConnectSessionEventError(e.getLocalizedMessage());
                    return;
                }
            }
            if (!appId.equals(this.myFacebook.getAppId())) {
                Log.v("Corona", "ERROR: In previous call to facebook.login(), the appId was " + this.myFacebook.getAppId() + " not " + appId + ". The app id will be the former not the latter.");
            }
            if (!this.myFacebook.isSessionValid() && (savedSession = this.myActivity.getSharedPreferences(FACEBOOK_SESSION_PREFERENCES_NAME, 0)) != null) {
                this.myFacebook.setAccessToken(savedSession.getString("access_token", null));
                this.myFacebook.setAccessExpires(savedSession.getLong("expires_in", 0));
            }
            if (this.myFacebook.isSessionValid()) {
                getEventManager().fbConnectSessionEvent(0);
            } else {
                this.myActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        Controller.this.myFacebook.authorize(Controller.this.myActivity, permissions, -1, new Facebook.DialogListener() {
                            public void onComplete(Bundle values) {
                                if (!Controller.isValid()) {
                                    return;
                                }
                                if (Controller.this.myFacebook.isSessionValid()) {
                                    storeLoginInfo();
                                    Controller.getEventManager().fbConnectSessionEvent(0);
                                    return;
                                }
                                onCancel();
                            }

                            public void onFacebookError(FacebookError e) {
                                if (Controller.isValid()) {
                                    storeLoginInfo();
                                    Controller.getEventManager().fbConnectSessionEventError(e.getLocalizedMessage());
                                }
                            }

                            public void onError(DialogError e) {
                                if (Controller.isValid()) {
                                    storeLoginInfo();
                                    Controller.getEventManager().fbConnectSessionEventError(e.getLocalizedMessage());
                                }
                            }

                            public void onCancel() {
                                if (Controller.isValid()) {
                                    storeLoginInfo();
                                    Controller.getEventManager().fbConnectSessionEvent(2);
                                }
                            }

                            private void storeLoginInfo() {
                                SharedPreferences.Editor editor;
                                if (Controller.this.myActivity != null && (editor = Controller.this.myActivity.getSharedPreferences(Controller.FACEBOOK_SESSION_PREFERENCES_NAME, 0).edit()) != null) {
                                    editor.putString("access_token", Controller.this.myFacebook.getAccessToken());
                                    editor.putLong("expires_in", Controller.this.myFacebook.getAccessExpires());
                                    editor.commit();
                                }
                            }
                        });
                    }
                });
            }
        }
    }

    public void facebookLogout() {
        if (this.myActivity != null) {
            SharedPreferences.Editor editor = this.myActivity.getSharedPreferences(FACEBOOK_SESSION_PREFERENCES_NAME, 0).edit();
            if (editor != null) {
                editor.clear();
                editor.commit();
            }
            if (this.myFacebook == null) {
                return;
            }
            if (this.myFacebook.isSessionValid()) {
                this.myFacebookRunner.logout(this.myActivity, new AsyncFacebookRunner.RequestListener() {
                    public void onComplete(String response, Object state) {
                        if (Controller.isValid()) {
                            Controller.getEventManager().fbConnectSessionEvent(3);
                        }
                    }

                    public void onIOException(IOException e, Object state) {
                        onThrowable(e);
                    }

                    public void onFileNotFoundException(FileNotFoundException e, Object state) {
                        onThrowable(e);
                    }

                    public void onMalformedURLException(MalformedURLException e, Object state) {
                        onThrowable(e);
                    }

                    public void onFacebookError(FacebookError e, Object state) {
                        onThrowable(e);
                    }

                    private void onThrowable(Throwable e) {
                        if (Controller.isValid()) {
                            Controller.getEventManager().fbConnectSessionEventError(e.getLocalizedMessage());
                        }
                    }
                });
            } else {
                getEventManager().fbConnectSessionEvent(3);
            }
        }
    }

    protected static Bundle createBundle(HashMap map) {
        Set<Map.Entry<String, String>> s;
        Bundle result = new Bundle();
        if (!(map == null || (s = map.entrySet()) == null)) {
            for (Map.Entry<String, String> entry : s) {
                result.putString((String) entry.getKey(), (String) entry.getValue());
            }
        }
        return result;
    }

    public void facebookRequest(String path, String method, HashMap params) {
        if (this.myFacebookRunner != null) {
            AsyncFacebookRunner.RequestListener listener = new AsyncFacebookRunner.RequestListener() {
                public void onComplete(String response, Object state) {
                    if (Controller.isValid()) {
                        Controller.getEventManager().fbConnectRequestEvent(response, false);
                    }
                }

                public void onIOException(IOException e, Object state) {
                    onThrowable(e);
                }

                public void onFileNotFoundException(FileNotFoundException e, Object state) {
                    onThrowable(e);
                }

                public void onMalformedURLException(MalformedURLException e, Object state) {
                    onThrowable(e);
                }

                public void onFacebookError(FacebookError e, Object state) {
                    onThrowable(e);
                }

                private void onThrowable(Throwable e) {
                    if (Controller.isValid()) {
                        Controller.getEventManager().fbConnectRequestEvent(e.getLocalizedMessage(), true);
                    }
                }
            };
            this.myFacebookRunner.request(path, createBundle(params), method, listener, null);
        }
    }

    public void facebookDialog(final String action, final HashMap params) {
        if (this.myFacebook != null) {
            this.myActivity.runOnUiThread(new Runnable() {
                public void run() {
                    Controller.this.myFacebook.dialog(Controller.this.myActivity, action, Controller.createBundle(params), new Facebook.DialogListener() {
                        public void onComplete(Bundle values) {
                            if (!Controller.isValid()) {
                                return;
                            }
                            if (values == null || values.size() <= 0) {
                                onCancel();
                            } else {
                                Controller.getEventManager().fbConnectRequestEvent("", false, true);
                            }
                        }

                        public void onFacebookError(FacebookError e) {
                            if (Controller.isValid()) {
                                Controller.getEventManager().fbConnectRequestEvent(e.getLocalizedMessage(), true, false);
                            }
                        }

                        public void onError(DialogError e) {
                            if (Controller.isValid()) {
                                Controller.getEventManager().fbConnectRequestEvent(e.getLocalizedMessage(), true, false);
                            }
                        }

                        public void onCancel() {
                            if (Controller.isValid()) {
                                Controller.getEventManager().fbConnectRequestEvent("Dialog cancelled", true, false);
                            }
                        }
                    });
                }
            });
        }
    }

    public void flurryInit(final String applicationId) {
        if (!this.myFlurrySessionStarted) {
            this.myFlurrySessionStarted = true;
            this.myActivity.runOnUiThread(new Runnable() {
                public void run() {
                    synchronized (this) {
                        FlurryAgent.onStartSession(Controller.this.myActivity, applicationId);
                    }
                }
            });
        }
    }

    public void flurryEvent(final String eventId) {
        if (this.myFlurrySessionStarted) {
            this.myActivity.runOnUiThread(new Runnable() {
                public void run() {
                    synchronized (this) {
                        FlurryAgent.onEvent(eventId);
                    }
                }
            });
        }
    }

    public void flurryStop() {
        if (this.myFlurrySessionStarted) {
            FlurryAgent.onEndSession(this.myActivity);
            this.myFlurrySessionStarted = false;
        }
    }

    public void papayaInit(final String apiKey, final String mapsApiKey) {
        if (this.myActivity != null && !this.fPapayaInited) {
            this.myActivity.runOnUiThread(new Runnable() {
                public void run() {
                    synchronized (this) {
                        if (Controller.isValid()) {
                            PPYSocial.initWithConfig(Controller.this.myActivity, new PPYSocial.Config() {
                                public String getApiKey() {
                                    return apiKey;
                                }

                                public String getAndroidMapsAPIKey() {
                                    return mapsApiKey != null ? mapsApiKey : super.getAndroidMapsAPIKey();
                                }

                                public String getPreferredLanguage() {
                                    return PPYSocial.LANG_AUTO;
                                }

                                public Class getRClass() {
                                    return R.class;
                                }
                            });
                            boolean unused = Controller.this.fPapayaInited = true;
                        }
                    }
                }
            });
        }
    }

    public void papayaShow(String name, String data) {
        int interfaceId;
        boolean isRequestingChat = false;
        if (this.myActivity != null && this.fPapayaInited) {
            String lowerCaseName = name != null ? name.trim().toLowerCase() : "";
            if (lowerCaseName.equals("achievements")) {
                interfaceId = 8;
            } else if (lowerCaseName.equals("avatar")) {
                interfaceId = 4;
            } else if (lowerCaseName.equals("challenges")) {
                interfaceId = 11;
            } else if (lowerCaseName.equals("chat")) {
                interfaceId = 0;
                isRequestingChat = true;
            } else if (lowerCaseName.equals("circles")) {
                interfaceId = 9;
            } else if (lowerCaseName.equals("friends")) {
                interfaceId = 1;
            } else if (lowerCaseName.equals("home") || lowerCaseName.length() <= 0) {
                interfaceId = 0;
            } else if (lowerCaseName.equals("invites")) {
                interfaceId = 7;
            } else if (lowerCaseName.equals("leaderboards")) {
                interfaceId = 5;
            } else {
                Log.v("Corona", "Unknown Papaya dashboard name '" + name + "'. Defaulting to home dashboard.");
                return;
            }
            final int finalInterfaceId = interfaceId;
            final String finalDataText = data != null ? data.trim() : "";
            final boolean finalIsRequestingChat = isRequestingChat;
            this.myActivity.runOnUiThread(new Runnable() {
                public void run() {
                    synchronized (this) {
                        if (Controller.isValid()) {
                            if (finalIsRequestingChat) {
                                PPYSocial.showChat(Controller.this.myActivity);
                            } else if (finalInterfaceId != 5 || finalDataText.length() <= 0) {
                                PPYSocial.showSocial(Controller.this.myActivity, finalInterfaceId);
                            } else {
                                PPYSocial.showLeaderboard(Controller.this.myActivity, finalDataText, true);
                            }
                        }
                    }
                }
            });
        }
    }

    public void papayaUnlockAchievement(String achievementId) {
        if (this.myActivity != null && this.fPapayaInited && achievementId != null && achievementId.length() > 0) {
            try {
                final int integerId = Integer.parseInt(achievementId.trim());
                final PPYAchievementDelegate delegate = new PPYAchievementDelegate() {
                    private PPYAchievement fAchievement = null;

                    public void onLoadSuccess(PPYAchievement achievement) {
                        if (!achievement.isUnlocked()) {
                            this.fAchievement = achievement;
                            achievement.unlock(this);
                        }
                    }

                    public void onUnlockSuccess(Boolean newUnlock) {
                        synchronized (this) {
                            if (newUnlock.booleanValue() && this.fAchievement != null && Controller.isValid()) {
                                Toast toastBox = Toast.makeText(Controller.this.myActivity.getApplicationContext(), "Achievement Unlocked\n" + ((Object) this.fAchievement.getTitle()), 0);
                                toastBox.setGravity(81, 0, 0);
                                toastBox.show();
                            }
                        }
                    }

                    public void onListSuccess(List<PPYAchievement> list) {
                    }

                    public void onListFailed() {
                    }

                    public void onDownloadIconSuccess(Bitmap icon) {
                    }
                };
                this.myActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        synchronized (this) {
                            if (Controller.isValid()) {
                                PPYSocial.loadAchievement(integerId, delegate);
                            }
                        }
                    }
                });
            } catch (Exception e) {
                Log.v("Corona", "Cannot unlock Papaya achievement ID '" + achievementId + "'. It must be an integer.");
            }
        }
    }

    public void papayaSetHighScore(String leaderboardId, final int score) {
        if (this.myActivity != null && this.fPapayaInited) {
            final String finalLeaderboardId = leaderboardId != null ? leaderboardId.trim() : "";
            if (finalLeaderboardId.length() > 0) {
                this.myActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        synchronized (this) {
                            if (Controller.isValid()) {
                                PPYSocial.setScore(score, finalLeaderboardId);
                            }
                        }
                    }
                });
            }
        }
    }

    public void openFeintInit(String appName, String appKey, String appSecret, String appId) {
        final String str = appName;
        final String str2 = appKey;
        final String str3 = appSecret;
        final String str4 = appId;
        this.myActivity.runOnUiThread(new Runnable() {
            public void run() {
                synchronized (this) {
                    Map<String, Object> options = new HashMap<>();
                    options.put(OpenFeintSettings.SettingCloudStorageCompressionStrategy, OpenFeintSettings.CloudStorageCompressionStrategyDefault);
                    OpenFeint.initialize(Controller.this.myActivity, new OpenFeintSettings(str, str2, str3, str4, options), new OpenFeintDelegate() {
                    });
                    boolean unused = Controller.this.myOpenFeintInited = true;
                }
            }
        });
    }

    public void openFeintResume() {
    }

    public void openFeintLaunchDashboard(final String dashboardName) {
        this.myActivity.runOnUiThread(new Runnable() {
            public void run() {
                synchronized (this) {
                    String lowerCaseDashboardName = dashboardName.toLowerCase();
                    if ("leaderboards".equals(lowerCaseDashboardName)) {
                        Dashboard.openLeaderboards();
                    } else if ("achievements".equals(lowerCaseDashboardName)) {
                        Dashboard.openAchievements();
                    } else if (!"challenges".equals(lowerCaseDashboardName) && !"friends".equals(lowerCaseDashboardName) && !"playing".equals(lowerCaseDashboardName) && !"highscore".equals(lowerCaseDashboardName)) {
                        Dashboard.open();
                    }
                }
            }
        });
    }

    public void openFeintUnlockAchievement(final String achievementId) {
        this.myActivity.runOnUiThread(new Runnable() {
            public void run() {
                synchronized (this) {
                    new Achievement(achievementId).unlock(new Achievement.UnlockCB() {
                        public void onSuccess(boolean newUnlock) {
                        }

                        public void onFailure(String exceptionMessage) {
                            System.out.println("OpenFeint: Failed to unlock achievement. (" + exceptionMessage + ")");
                        }
                    });
                }
            }
        });
    }

    public void openFeintSetHighScore(String leaderboardId, long score, String displayText) {
        final String str = displayText;
        final long j = score;
        final String str2 = leaderboardId;
        this.myActivity.runOnUiThread(new Runnable() {
            public void run() {
                Score ofScore;
                synchronized (this) {
                    if (str == null || str.length() <= 0) {
                        ofScore = new Score(j);
                    } else {
                        ofScore = new Score(j, str);
                    }
                    ofScore.submitTo(new Leaderboard(str2), new Score.SubmitToCB() {
                        public void onSuccess(boolean newHighScore) {
                        }

                        public void onFailure(String exceptionMessage) {
                            System.out.println("OpenFeint: Failed to post high score. (" + exceptionMessage + ")");
                        }
                    });
                }
            }
        });
    }

    public void superRewardsInit(String appId, String uid) {
        if (!this.myRewardsInitialized) {
            this.myRewardsAppId = new String(appId);
            this.myRewardsUid = new String(uid);
            this.myRewardsInitialized = true;
            this.myActivity.runOnUiThread(new Runnable() {
                public void run() {
                    synchronized (this) {
                        SRAppInstallTracker unused = Controller.this.myRewardsTracker = SRAppInstallTracker.getInstance(Controller.this.myActivity.getApplicationContext(), Controller.this.myRewardsAppId);
                        Controller.this.myRewardsTracker.track();
                    }
                }
            });
        }
    }

    public void superRewardsShowOffers() {
        if (this.myRewardsInitialized) {
            this.myActivity.runOnUiThread(new Runnable() {
                public void run() {
                    synchronized (this) {
                        new SuperRewardsImpl(R.class).showOffers(Controller.this.myActivity, Controller.this.myRewardsAppId, Controller.this.myRewardsUid);
                    }
                }
            });
        }
    }

    private class SuperRewardsRequestUpdateTask extends AsyncTask<SRUserPoints, Void, SRUserPoints> {
        private String myAppId;
        private String myUid;

        public SuperRewardsRequestUpdateTask(String appId, String uid) {
            this.myAppId = appId;
            this.myUid = uid;
        }

        /* access modifiers changed from: protected */
        public SRUserPoints doInBackground(SRUserPoints... user) {
            user[0].updatePoints(this.myAppId, this.myUid);
            return user[0];
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(SRUserPoints result) {
            final int newPoints = result.getNewPoints();
            final int totalPoints = result.getTotalPoints();
            Controller.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    synchronized (this) {
                        Controller.getEventManager().creditsRequestEvent(newPoints, totalPoints);
                    }
                }
            });
        }
    }

    public void superRewardsRequestUpdate() {
        if (this.myRewardsInitialized) {
            this.myActivity.runOnUiThread(new Runnable() {
                public void run() {
                    synchronized (this) {
                        SRUserPoints user = new SRUserPoints(Controller.this.myActivity);
                        new SuperRewardsRequestUpdateTask(Controller.this.myRewardsAppId, Controller.this.myRewardsUid).execute(user);
                    }
                }
            });
        }
    }
}
