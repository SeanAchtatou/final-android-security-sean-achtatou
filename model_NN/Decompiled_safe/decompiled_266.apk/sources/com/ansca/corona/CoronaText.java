package com.ansca.corona;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextPaint;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CoronaText {
    private static byte[] ourBits = new byte[1];
    private static int[] ourCachedInt = new int[ourCachedSize];
    private static int ourCachedSize = 1;
    private static Rect ourRect = new Rect();
    private static HashMap<String, TextInfo> ourTypefaces = new HashMap<>();
    protected int myHeight;
    private TextInfo myTextInfo;
    protected int myWidth;

    public static CoronaText getCoronaText(String fontName, float fontSize, CoronaActivity activity) {
        return new CoronaText(fontName, fontSize, activity);
    }

    static class TextInfo {
        TextPaint myTextPaint = new TextPaint();
        Typeface myTypeface;

        TextInfo(Typeface typeface) {
            this.myTypeface = typeface;
            this.myTextPaint.setARGB(255, 255, 255, 255);
            this.myTextPaint.setAntiAlias(true);
            this.myTextPaint.setTextAlign(Paint.Align.LEFT);
            this.myTextPaint.setTypeface(typeface);
        }
    }

    public static Typeface getTypeface(String fontName, CoronaActivity activity) {
        TextInfo fontInfo = ourTypefaces.get(fontName);
        if (fontInfo != null) {
            return fontInfo.myTypeface;
        }
        Typeface typeface = null;
        if ("".equals(fontName) || fontName == null) {
            typeface = Typeface.create((String) null, 0);
            fontName = "";
        } else {
            AssetManager assetManager = activity.getAssets();
            try {
                String fontPath = fontName + ".ttf";
                InputStream is = assetManager.open(fontPath);
                if (is.available() > 0) {
                    is.close();
                    typeface = Typeface.createFromAsset(assetManager, fontPath);
                }
            } catch (IOException e) {
            }
            if (typeface == null) {
                typeface = Controller.getAndroidVersionSpecific().typefaceCreateFromFile("/system/fonts/" + fontName + ".ttf");
            }
            if (typeface == null) {
                System.out.println("WARNING: Could not load font " + fontName + ". Using default.");
                typeface = Typeface.create((String) null, 0);
            }
        }
        if (typeface != null) {
            ourTypefaces.put(fontName, new TextInfo(typeface));
        }
        return typeface;
    }

    CoronaText(String fontName, float fontSize, CoronaActivity activity) {
        Typeface typeface = getTypeface(fontName, activity);
        this.myTextInfo = ourTypefaces.get(fontName);
    }

    public void render(String text, float fontSize, CoronaActivity activity) {
        TextPaint textPaint = this.myTextInfo.myTextPaint;
        textPaint.setTextSize(fontSize);
        textPaint.getTextBounds(text, 0, text.length(), ourRect);
        this.myWidth = ourRect.right;
        if (this.myWidth < 1) {
            this.myWidth = 1;
        }
        Paint.FontMetricsInt metrics = textPaint.getFontMetricsInt();
        this.myHeight = Math.abs(metrics.top) + Math.abs(metrics.bottom);
        if (this.myHeight < 1) {
            this.myHeight = 1;
        }
        if ((this.myWidth & 3) != 0) {
            this.myWidth = (this.myWidth + 3) & -4;
        }
        Bitmap bitmap = Bitmap.createBitmap(this.myWidth, this.myHeight, Bitmap.Config.ARGB_8888);
        bitmap.eraseColor(0);
        new Canvas(bitmap).drawText(text, 0.0f, (float) (this.myHeight - Math.abs(metrics.bottom)), textPaint);
        int newSize = this.myWidth * this.myHeight;
        if (newSize > ourCachedSize) {
            ourCachedInt = new int[newSize];
            ourCachedSize = newSize;
        }
        int[] intBits = ourCachedInt;
        if (newSize != ourBits.length) {
            ourBits = new byte[newSize];
        }
        byte[] bytes = ourBits;
        bitmap.getPixels(intBits, 0, this.myWidth, 0, 0, this.myWidth, this.myHeight);
        for (int y = 0; y < this.myHeight; y++) {
            int start = y * this.myWidth;
            for (int x = 0; x < this.myWidth; x++) {
                bytes[start + x] = (byte) (Color.alpha(intBits[start + x]) & 255);
            }
        }
    }

    public int getWidth() {
        return this.myWidth;
    }

    public int getHeight() {
        return this.myHeight;
    }

    public byte[] getBits() {
        return ourBits;
    }

    public void release() {
    }

    public static String[] getFonts() {
        List<String> fontNames = new ArrayList<>();
        for (File font : new File("/system/fonts/").listFiles(new FilenameFilter() {
            public boolean accept(File f, String name) {
                return name.endsWith(".ttf");
            }
        })) {
            String fontName = font.getName();
            fontNames.add(fontName.subSequence(0, fontName.lastIndexOf(".ttf")).toString());
        }
        return (String[]) fontNames.toArray(new String[fontNames.size()]);
    }
}
