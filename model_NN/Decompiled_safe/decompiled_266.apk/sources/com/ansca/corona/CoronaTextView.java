package com.ansca.corona;

import android.content.Context;
import android.widget.TextView;

public class CoronaTextView extends TextView implements ICoronaTextView {
    private int myTextColor = 0;

    public CoronaTextView(Context context) {
        super(context);
    }

    public void setTextViewAlign(String align) {
        int gravity = 3;
        if ("left".equals(align)) {
            gravity = 3;
        } else if ("center".equals(align)) {
            gravity = 1;
        } else if ("right".equals(align)) {
            gravity = 5;
        }
        setGravity(gravity);
    }

    public String getTextViewAlign() {
        switch (getGravity()) {
            case 1:
                return "center";
            case 2:
            case 4:
            default:
                return "unknown";
            case 3:
                return "left";
            case 5:
                return "right";
        }
    }

    public void setTextViewColor(int color) {
        this.myTextColor = color;
        setTextColor(color);
    }

    public int getTextViewColor() {
        return this.myTextColor;
    }

    public void setTextViewSize(float size) {
        setTextSize(size);
    }

    public float getTextViewSize() {
        return getTextSize();
    }

    public void setTextViewFont(String fontName, float fontSize, CoronaActivity activity) {
        setTypeface(CoronaText.getTypeface(fontName, activity), 0);
    }

    public void destroying() {
    }

    public TextView getTextView() {
        return this;
    }

    public String getTextViewInputType() {
        return null;
    }

    public boolean getTextViewPassword() {
        return false;
    }

    public void setTextViewInputType(String inputType) {
    }

    public void setTextViewPassword(boolean isPassword) {
    }
}
