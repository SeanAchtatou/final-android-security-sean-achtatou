package com.ansca.corona;

import android.opengl.GLSurfaceView;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class CoronaRenderer implements GLSurfaceView.Renderer {
    private static boolean myFirstSurface = true;
    CoronaActivity myActivity;

    public CoronaRenderer(CoronaActivity activity) {
        this.myActivity = activity;
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        if (!myFirstSurface) {
            this.myActivity.runOnUiThread(new Runnable() {
                public void run() {
                    Controller.getPortal().reinitializeRenderer();
                    CoronaRenderer.this.myActivity.setNeedsSwap();
                }
            });
        }
        myFirstSurface = false;
    }

    public void onSurfaceChanged(GL10 gl, int w, int h) {
        synchronized (this) {
            Controller.getEventManager().resize(this.myActivity.getPackageName(), this.myActivity.getFilesDirAsString(), this.myActivity.getCacheDirAsString(), w, h, this.myActivity.getOrientation());
            Controller.getEventManager().orientationChanged(this.myActivity.getOrientation(), this.myActivity.getPreviousOrientation());
        }
    }

    public void onDrawFrame(GL10 gl) {
        Controller.onDrawFrame();
    }

    public void clearFirstSurface() {
        myFirstSurface = true;
    }
}
