package com.ansca.corona;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.webkit.URLUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

public class MediaManager {
    private static final int SOUNDPOOL_STREAMS = 4;
    /* access modifiers changed from: private */
    public CoronaActivity myActivity;
    private AudioRecorder myAudioRecorder;
    private HashMap<Integer, Integer> myIdToSoundPoolIdMap;
    /* access modifiers changed from: private */
    public HashMap<Integer, MediaPlayer> myMediaPlayers;
    private SoundPool mySoundPool;
    /* access modifiers changed from: private */
    public int myVideoId;
    private float myVolume;

    public MediaManager(CoronaActivity activity) {
        this.myActivity = activity;
    }

    public void init() {
        this.myIdToSoundPoolIdMap = new HashMap<>();
        this.mySoundPool = new SoundPool(4, 3, 0);
        this.myMediaPlayers = new HashMap<>();
    }

    public void release() {
        if (this.mySoundPool != null) {
            this.mySoundPool.release();
            this.mySoundPool = null;
            this.myMediaPlayers = null;
            this.myIdToSoundPoolIdMap = null;
        }
    }

    public void loadSound(final int id, String soundName) {
        MediaPlayer mp = null;
        try {
            if (soundName.startsWith("/") || soundName.startsWith("http:")) {
                if (!URLUtil.isNetworkUrl(soundName)) {
                    File file = new File(soundName);
                    if (!file.exists()) {
                        System.err.println("Could not load sound " + soundName);
                        return;
                    }
                    FileInputStream fis = new FileInputStream(file);
                    MediaPlayer mp2 = new MediaPlayer();
                    try {
                        mp2.setDataSource(fis.getFD());
                        mp2.setAudioStreamType(3);
                        mp2.prepare();
                        mp = mp2;
                    } catch (Exception e) {
                        return;
                    }
                } else {
                    mp = MediaPlayer.create(Controller.getActivity(), Uri.parse(soundName));
                }
            }
            if (mp == null) {
                System.err.println("Could not load sound " + soundName);
                return;
            }
            mp.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    mp.release();
                    if (MediaManager.this.myMediaPlayers != null) {
                        MediaManager.this.myMediaPlayers.remove(new Integer(id));
                    }
                    Controller.getEventManager().soundEnded(id);
                    return true;
                }
            });
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                    if (MediaManager.this.myMediaPlayers != null) {
                        MediaManager.this.myMediaPlayers.remove(new Integer(id));
                    }
                    Controller.getEventManager().soundEnded(id);
                }
            });
            this.myMediaPlayers.put(new Integer(id), mp);
        } catch (Exception e2) {
        }
    }

    public void loadEventSound(int id, String inSoundName) {
        if (inSoundName.startsWith("/")) {
            this.myIdToSoundPoolIdMap.put(new Integer(id), Integer.valueOf(this.mySoundPool.load(inSoundName, 1)));
            return;
        }
        try {
            this.myIdToSoundPoolIdMap.put(new Integer(id), Integer.valueOf(this.mySoundPool.load(this.myActivity.getAssets().openFd(inSoundName), 1)));
        } catch (IOException e) {
            System.out.println("loadSound requested nonexistent: " + inSoundName);
        }
    }

    public void playMedia(int id, boolean loop) {
        MediaPlayer mp = this.myMediaPlayers.get(new Integer(id));
        if (mp != null) {
            mp.setLooping(loop);
            mp.start();
            return;
        }
        Integer soundId = this.myIdToSoundPoolIdMap.get(new Integer(id));
        if (soundId != null) {
            AudioManager mgr = (AudioManager) this.myActivity.getSystemService("audio");
            float volume = ((float) mgr.getStreamVolume(3)) / ((float) mgr.getStreamMaxVolume(3));
            if (this.mySoundPool.play(soundId.intValue(), volume, volume, 1, 0, 1.0f) == 0) {
                System.out.println("playSound failed " + soundId);
            }
        }
    }

    public void stopMedia(int id) {
        MediaPlayer mp = this.myMediaPlayers.get(new Integer(id));
        if (mp != null) {
            mp.stop();
            return;
        }
        Integer soundId = this.myIdToSoundPoolIdMap.get(new Integer(id));
        if (soundId != null) {
            this.mySoundPool.stop(soundId.intValue());
        }
    }

    public void pauseMedia(int id) {
        MediaPlayer mp = this.myMediaPlayers.get(Integer.valueOf(id));
        if (mp != null) {
            try {
                mp.pause();
            } catch (IllegalStateException e) {
            }
        } else {
            Integer soundId = this.myIdToSoundPoolIdMap.get(new Integer(id));
            if (soundId != null) {
                this.mySoundPool.pause(soundId.intValue());
            }
        }
    }

    public void resumeMedia(int id) {
        MediaPlayer mp = this.myMediaPlayers.get(Integer.valueOf(id));
        if (mp != null) {
            try {
                mp.start();
            } catch (IllegalStateException e) {
            }
        } else {
            Integer soundId = this.myIdToSoundPoolIdMap.get(new Integer(id));
            if (soundId != null) {
                this.mySoundPool.resume(soundId.intValue());
            }
        }
    }

    public void pauseAll() {
        for (Integer key : this.myMediaPlayers.keySet()) {
            pauseMedia(key.intValue());
        }
        for (Integer key2 : this.myIdToSoundPoolIdMap.keySet()) {
            pauseMedia(key2.intValue());
        }
    }

    public void resumeAll() {
        for (Integer key : this.myMediaPlayers.keySet()) {
            resumeMedia(key.intValue());
        }
        for (Integer key2 : this.myIdToSoundPoolIdMap.keySet()) {
            resumeMedia(key2.intValue());
        }
    }

    public void playVideo(int id, final String path, final boolean mediaControlsEnabled) {
        pauseAll();
        this.myVideoId = id;
        this.myActivity.getHandler().post(new Runnable() {
            public void run() {
                Uri videoUri;
                String packageName = MediaManager.this.myActivity.getPackageName();
                if (URLUtil.isNetworkUrl(path)) {
                    videoUri = Uri.parse(path);
                } else {
                    String localFilePath = path;
                    String absolutePathPrefix = "/data/data/" + MediaManager.this.myActivity.getApplicationContext().getPackageName();
                    int index = path.indexOf(absolutePathPrefix);
                    if (index < 0 || absolutePathPrefix.length() + index >= path.length()) {
                        int index2 = path.indexOf("file:///android_asset");
                        if (index2 >= 0 && "file:///android_asset".length() + index2 < path.length()) {
                            localFilePath = path.substring("file:///android_asset".length() + index2);
                        }
                    } else {
                        localFilePath = path.substring(absolutePathPrefix.length() + index);
                    }
                    if (!localFilePath.startsWith(File.separator)) {
                        localFilePath = File.separator + localFilePath;
                    }
                    videoUri = Uri.parse("content://" + MediaManager.this.myActivity.getApplicationContext().getPackageName() + ".files" + localFilePath);
                }
                if (videoUri != null || 0 != 0) {
                    Intent intent = new Intent(MediaManager.this.myActivity, VideoActivity.class);
                    intent.putExtra("video_uri", videoUri.toString());
                    intent.putExtra("video_id", MediaManager.this.myVideoId);
                    intent.putExtra("media_controls_enabled", mediaControlsEnabled);
                    MediaManager.this.myActivity.startActivity(intent);
                }
            }
        });
    }

    public void setVolume(int id, float volume) {
        if (volume < 0.0f) {
            volume = 0.0f;
        }
        if (volume > 1.0f) {
            volume = 1.0f;
        }
        MediaPlayer mp = this.myMediaPlayers.get(new Integer(id));
        if (mp != null) {
            mp.setVolume(volume, volume);
        }
        this.myVolume = volume;
    }

    public float getVolume(int id) {
        return this.myVolume;
    }

    public AudioRecorder getAudioRecorder(int id) {
        if (this.myAudioRecorder == null) {
            this.myAudioRecorder = new AudioRecorder(this.myActivity.getHandler());
        }
        this.myAudioRecorder.setId(id);
        return this.myAudioRecorder;
    }
}
