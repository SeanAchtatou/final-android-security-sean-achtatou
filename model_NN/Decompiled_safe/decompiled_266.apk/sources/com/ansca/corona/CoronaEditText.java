package com.ansca.corona;

import android.content.Context;
import android.graphics.Rect;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.text.method.PasswordTransformationMethod;
import android.text.method.SingleLineTransformationMethod;
import android.text.method.TextKeyListener;
import android.text.method.TransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.papaya.social.BillingChannel;

public class CoronaEditText extends EditText implements ICoronaTextView {
    /* access modifiers changed from: private */
    public int editingAfter = 0;
    /* access modifiers changed from: private */
    public int editingBefore = 0;
    /* access modifiers changed from: private */
    public int editingNumDeleted = 0;
    /* access modifiers changed from: private */
    public int editingStart = 0;
    /* access modifiers changed from: private */
    public boolean isEditing = false;
    private boolean myClearingFocus = false;
    /* access modifiers changed from: private */
    public int myId;
    /* access modifiers changed from: private */
    public boolean myIsPassword = false;
    private int myTextColor = 0;
    /* access modifiers changed from: private */
    public String oldString = null;

    public CoronaEditText(Context context, int id) {
        super(context);
        this.myId = id;
        setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, boolean hasFocus) {
                if (Controller.isValid() && CoronaEditText.this.myId != 0) {
                    if (hasFocus) {
                        boolean unused = CoronaEditText.this.isEditing = true;
                    }
                    Controller.getEventManager().textEvent(CoronaEditText.this.myId, hasFocus, false);
                }
            }
        });
        setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView view, int actionId, KeyEvent arg2) {
                if (CoronaEditText.this.isEditing) {
                    boolean unused = CoronaEditText.this.isEditing = false;
                }
                if (CoronaEditText.this.myId != 0 && actionId == 6 && Controller.isValid()) {
                    Controller.getEventManager().textEvent(CoronaEditText.this.myId, false, true);
                }
                return false;
            }
        });
        addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence text, int start, int count, int after) {
                if (CoronaEditText.this.isEditing) {
                    String unused = CoronaEditText.this.oldString = new String(text.toString());
                    int unused2 = CoronaEditText.this.editingNumDeleted = count;
                }
            }

            public void afterTextChanged(Editable text) {
                if (CoronaEditText.this.isEditing && Controller.isValid() && CoronaEditText.this.myId != 0) {
                    String newstring = null;
                    String newchars = null;
                    if (text != null) {
                        newstring = text.toString();
                        newchars = text.subSequence(CoronaEditText.this.editingStart, CoronaEditText.this.editingStart + CoronaEditText.this.editingAfter).toString();
                    }
                    Controller.getEventManager().textEditingEvent(CoronaEditText.this.myId, CoronaEditText.this.editingStart, CoronaEditText.this.editingNumDeleted, newchars, CoronaEditText.this.oldString, newstring);
                }
            }

            public void onTextChanged(CharSequence text, int start, int before, int after) {
                if (CoronaEditText.this.isEditing) {
                    int unused = CoronaEditText.this.editingStart = start;
                    int unused2 = CoronaEditText.this.editingBefore = before;
                    int unused3 = CoronaEditText.this.editingAfter = after;
                }
            }
        });
        setKeyListener(new TextKeyListener(TextKeyListener.Capitalize.NONE, false) {
            public int getInputType() {
                int type;
                if (CoronaEditText.this.myIsPassword) {
                    type = 1 | 128;
                } else {
                    type = 1 | 0;
                }
                return type | Controller.getAndroidVersionSpecific().inputTypeFlagsNoSuggestions();
            }
        });
        setTransformationMethod(SingleLineTransformationMethod.getInstance());
    }

    public void destroying() {
        this.myId = 0;
    }

    public void clearFocus() {
        this.myClearingFocus = true;
        super.clearFocus();
        this.myClearingFocus = false;
    }

    public boolean requestFocus(int direction, Rect previouslyFocusedRect) {
        if (this.myClearingFocus) {
            return false;
        }
        return super.requestFocus(direction, previouslyFocusedRect);
    }

    public void setTextViewInputType(String inputType) {
        int type;
        if ("number".equals(inputType)) {
            type = 2;
        } else if ("phone".equals(inputType)) {
            type = 3;
        } else if ("url".equals(inputType)) {
            type = 17;
        } else if ("email".equals(inputType)) {
            type = 33;
        } else {
            type = 1;
            if (!this.myIsPassword) {
                type = 1 | 0;
            }
        }
        if (this.myIsPassword) {
            type |= 128;
        }
        int type2 = type | Controller.getAndroidVersionSpecific().inputTypeFlagsNoSuggestions();
        if (type2 != 0) {
            setInputType(type2);
        }
    }

    public String getTextViewInputType() {
        KeyListener listener = getKeyListener();
        if (listener == null) {
            return "unknown";
        }
        int inputType = listener.getInputType();
        switch (inputType & 15) {
            case 1:
                switch (inputType & 4080) {
                    case BillingChannel.REDEEM /*16*/:
                        return "url";
                    case 32:
                        return "email";
                    default:
                        return "default";
                }
            case 2:
                return "number";
            case 3:
                return "phone";
            default:
                return "default";
        }
    }

    public void setTextViewPassword(boolean isPassword) {
        TransformationMethod method;
        if (isPassword) {
            method = PasswordTransformationMethod.getInstance();
        } else {
            method = SingleLineTransformationMethod.getInstance();
        }
        setTransformationMethod(method);
        this.myIsPassword = isPassword;
    }

    public boolean getTextViewPassword() {
        return this.myIsPassword;
    }

    public void setTextViewAlign(String align) {
        int gravity = 3;
        if ("left".equals(align)) {
            gravity = 3;
        } else if ("center".equals(align)) {
            gravity = 1;
        } else if ("right".equals(align)) {
            gravity = 5;
        }
        setGravity(gravity);
    }

    public String getTextViewAlign() {
        switch (getGravity()) {
            case 1:
                return "center";
            case 2:
            case 4:
            default:
                return "unknown";
            case 3:
                return "left";
            case 5:
                return "right";
        }
    }

    public void setTextViewColor(int color) {
        this.myTextColor = color;
        setTextColor(color);
    }

    public int getTextViewColor() {
        return this.myTextColor;
    }

    public void setTextViewSize(float fontSize) {
        setTextSize(1, fontSize);
    }

    public float getTextViewSize() {
        return getTextSize();
    }

    public void setTextViewFont(String fontName, float fontSize, CoronaActivity activity) {
        setTypeface(CoronaText.getTypeface(fontName, activity), 0);
        setTextSize(1, fontSize);
    }

    public TextView getTextView() {
        return this;
    }

    public void setText(CharSequence text, TextView.BufferType type) {
        boolean savedEditing = this.isEditing;
        this.isEditing = false;
        super.setText(text, type);
        this.isEditing = savedEditing;
    }
}
