package com.ansca.corona;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.util.Log;
import com.ansca.corona.AudioRecorder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.HashMap;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.entity.BufferedHttpEntity;

public class NativeToJavaBridge {
    private CoronaActivity myActivity;
    private int myLastBitmapHeight = 0;
    private int myLastBitmapWidth = 0;

    NativeToJavaBridge(CoronaActivity activity) {
        this.myActivity = activity;
    }

    public CoronaActivity getActivity() {
        return this.myActivity;
    }

    protected static void ping() {
        System.out.println("NativeToJavaBridge.ping()");
    }

    private static NativeToJavaBridge getBridge() {
        return Controller.getBridge();
    }

    private boolean getRawAssetExists(String assetName) {
        boolean result = false;
        try {
            InputStream is = this.myActivity.getAssets().open(assetName, 3);
            result = is != null;
            if (result) {
                is.close();
            }
        } catch (IOException e) {
            Log.v("Corona", "WARNING: asset file " + assetName + " does not exist");
        }
        return result;
    }

    protected static boolean callGetRawAssetExists(String assetName) {
        return getBridge().getRawAssetExists(assetName);
    }

    private byte[] getRawAsset(String assetName) {
        int numBytes;
        try {
            InputStream is = this.myActivity.getAssets().open(assetName, 3);
            if (is == null || (numBytes = is.available()) <= 0) {
                return null;
            }
            byte[] bytes = new byte[numBytes];
            if (is.read(bytes) != numBytes) {
                return null;
            }
            return bytes;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    protected static byte[] callGetRawAsset(String assetName) {
        return getBridge().getRawAsset(assetName);
    }

    private String externalizeAsset(String assetName) {
        File of;
        try {
            if (assetName.startsWith("/")) {
                of = new File(assetName);
            } else {
                File ofdir = this.myActivity.getFileStreamPath("coronaResources");
                ofdir.mkdir();
                of = new File(ofdir, assetName);
            }
            if (of.exists()) {
                return of.getAbsolutePath();
            }
            InputStream is = this.myActivity.getAssets().open(assetName, 3);
            if (is == null) {
                return null;
            }
            FileOutputStream os = new FileOutputStream(of);
            int numBytes = is.available();
            if (numBytes <= 0) {
                return null;
            }
            byte[] bytes = new byte[1000];
            while (numBytes > 0) {
                int bytesToRead = 1000;
                if (1000 > numBytes) {
                    bytesToRead = numBytes;
                }
                int numRead = is.read(bytes, 0, bytesToRead);
                os.write(bytes, 0, numRead);
                numBytes -= numRead;
            }
            os.close();
            return of.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    protected static String callExternalizeResource(String assetName) {
        return getBridge().externalizeAsset(assetName);
    }

    private static Bitmap createGrayscaleBitmap(Bitmap bitmap) {
        Bitmap result = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(result);
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0.0f);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        Paint paint = new Paint();
        paint.setColorFilter(f);
        c.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        return result;
    }

    private Bitmap createBitmap(String assetName) {
        InputStream is;
        AssetManager assetManager = this.myActivity.getAssets();
        try {
            if (!assetName.startsWith("/")) {
                is = assetManager.open(assetName, 3);
            } else {
                is = new FileInputStream(assetName);
            }
            if (is == null) {
                return null;
            }
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            Bitmap bitmap = BitmapFactory.decodeStream(is, null, options);
            if (bitmap != null) {
                return bitmap;
            }
            System.out.println("getBitmapAsset: Couldn't decode " + assetName);
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private int[] getBitmapAsset(String assetName) {
        int[] result = null;
        Bitmap bitmap = createBitmap(assetName);
        if (bitmap != null) {
            this.myLastBitmapWidth = bitmap.getWidth();
            this.myLastBitmapHeight = bitmap.getHeight();
            try {
                result = new int[(bitmap.getWidth() * bitmap.getHeight())];
            } catch (OutOfMemoryError e) {
                System.out.println("getBitmapAsset: Ran out of memory in the Java VM loading an image (" + assetName + ") of size " + this.myLastBitmapWidth + "x" + this.myLastBitmapHeight);
                e.printStackTrace();
            }
            bitmap.getPixels(result, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
            bitmap.recycle();
        }
        return result;
    }

    private byte[] getBitmapMaskAsset(String assetName) {
        byte[] result = null;
        Bitmap bitmap = createBitmap(assetName);
        if (bitmap != null) {
            Bitmap oldBitmap = bitmap;
            Bitmap bitmap2 = createGrayscaleBitmap(oldBitmap);
            oldBitmap.recycle();
            this.myLastBitmapWidth = bitmap2.getWidth();
            this.myLastBitmapHeight = bitmap2.getHeight();
            int numPixels = bitmap2.getWidth() * bitmap2.getHeight();
            int[] pixels = new int[numPixels];
            bitmap2.getPixels(pixels, 0, bitmap2.getWidth(), 0, 0, bitmap2.getWidth(), bitmap2.getHeight());
            bitmap2.recycle();
            result = new byte[numPixels];
            for (int i = 0; i < numPixels; i++) {
                result[i] = (byte) (Color.red(pixels[i]) & 255);
            }
        }
        return result;
    }

    private class NetworkRequestHandler extends AsyncHttpResponseHandler {
        private String fFilePath;
        private int fListenerId;

        public NetworkRequestHandler(int listenerId, String filePath) {
            this.fListenerId = listenerId;
            this.fFilePath = (filePath == null || filePath.equals("")) ? null : new String(filePath);
        }

        /* access modifiers changed from: protected */
        public InputStream getResponseContent(HttpResponse response) throws IOException {
            HttpEntity entity = null;
            HttpEntity temp = response.getEntity();
            if (temp != null) {
                entity = new BufferedHttpEntity(temp);
            }
            if (entity != null) {
                return entity.getContent();
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void handleResponseMessage(HttpResponse response) {
            if (this.fFilePath == null) {
                super.handleResponseMessage(response);
                return;
            }
            StatusLine status = response.getStatusLine();
            if (status.getStatusCode() >= 300) {
                onFailure(new HttpResponseException(status.getStatusCode(), status.getReasonPhrase()));
                return;
            }
            try {
                InputStream in = getResponseContent(response);
                onSuccess(in);
                in.close();
            } catch (IOException e) {
                onFailure(e);
            }
        }

        public void onSuccess(InputStream in) {
            if (this.fListenerId != 0) {
                String response = this.fFilePath;
                boolean isError = false;
                try {
                    FileOutputStream out = new FileOutputStream(new File(this.fFilePath));
                    byte[] buf = new byte[1024];
                    int len = in.read(buf);
                    while (len > 0) {
                        out.write(buf, 0, len);
                        len = in.read(buf);
                    }
                    out.flush();
                    out.close();
                } catch (Throwable th) {
                    isError = true;
                    response = th.getMessage();
                }
                Controller.getEventManager().networkRequestEvent(this.fListenerId, response, isError);
            }
        }

        public void onSuccess(String response) {
            if (this.fListenerId != 0) {
                Controller.getEventManager().networkRequestEvent(this.fListenerId, response, false);
            }
        }

        public void onFailure(Throwable error) {
            if (this.fListenerId != 0) {
                Controller.getEventManager().networkRequestEvent(this.fListenerId, error.getMessage(), true);
            }
        }
    }

    private void networkRequest(String url, String method, int listenerId, HashMap headers, String body, String path) {
        AsyncHttpClient client = new AsyncHttpClient("My User Agent");
        AsyncHttpResponseHandler handler = new NetworkRequestHandler(listenerId, path);
        try {
            new URL(url);
            if (method.equalsIgnoreCase("get")) {
                client.get(url, headers, handler);
            } else if (method.equalsIgnoreCase("post")) {
                client.post(url, headers, body, handler);
            } else {
                throw new Exception("HTTP method '" + method + "' not supported");
            }
        } catch (Exception e) {
            Exception ex = e;
            if (listenerId != 0) {
                Controller.getEventManager().networkRequestEvent(listenerId, ex.getMessage(), true);
            }
        }
    }

    protected static int[] callGetBitmapAsset(String assetName) {
        return getBridge().getBitmapAsset(assetName);
    }

    protected static byte[] callGetBitmapMaskAsset(String assetName) {
        return getBridge().getBitmapMaskAsset(assetName);
    }

    protected static boolean callSaveBitmap(int[] pixels, int width, int height, int quality, String path) {
        Bitmap.CompressFormat format;
        try {
            int length = pixels.length;
            Bitmap bm = Bitmap.createBitmap(pixels, width, height, Bitmap.Config.ARGB_8888);
            boolean result = false;
            if (path == null || "".equals(path)) {
                path = Controller.getAndroidVersionSpecific().getPictureStorageDirectory() + "corona_image.png";
            }
            if (path.toLowerCase().endsWith(".png")) {
                format = Bitmap.CompressFormat.PNG;
            } else {
                format = Bitmap.CompressFormat.JPEG;
            }
            try {
                result = bm.compress(format, quality, new FileOutputStream(path));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return result;
        } catch (IllegalArgumentException e2) {
            System.out.println(e2.toString());
            return false;
        }
    }

    protected static byte[] callGetText(String text, String fontName, float fontSize) {
        NativeToJavaBridge bridge = getBridge();
        CoronaText theText = CoronaText.getCoronaText(fontName, fontSize, bridge.getActivity());
        theText.render(text, fontSize, bridge.getActivity());
        bridge.myLastBitmapWidth = theText.getWidth();
        bridge.myLastBitmapHeight = theText.getHeight();
        byte[] result = theText.getBits();
        theText.release();
        return result;
    }

    protected static String[] callGetFonts() {
        return CoronaText.getFonts();
    }

    protected static int callGetLastGraphicWidth() {
        return getBridge().myLastBitmapWidth;
    }

    protected static int callGetLastGraphicHeight() {
        return getBridge().myLastBitmapHeight;
    }

    protected static void callSetTimer(int milliseconds) {
        Controller.getController().setTimer(milliseconds);
    }

    protected static void callCancelTimer() {
        Controller.getController().cancelTimer();
    }

    protected static void callLoadSound(int id, String soundName) {
        Controller.getEventManager().loadSound(id, soundName);
    }

    protected static void callLoadEventSound(int id, String soundName) {
        Controller.getEventManager().loadEventSound(id, soundName);
    }

    protected static void callPlaySound(int id, String soundName, boolean loop) {
        Controller.getEventManager().playSound(id, soundName, loop);
    }

    protected static void callStopSound(int id) {
        Controller.getEventManager().stopSound(id);
    }

    protected static void callPauseSound(int id) {
        Controller.getEventManager().pauseSound(id);
    }

    protected static void callResumeSound(int id) {
        Controller.getEventManager().resumeSound(id);
    }

    protected static void callHttpPost(String url, String key, String value) {
        Controller.getController().httpPost(url, key, value);
    }

    protected static void callPlayVideo(int id, String url, boolean mediaControlsEnabled) {
        Controller.getMediaManager().playVideo(id, url, mediaControlsEnabled);
    }

    protected static void callOpenUrl(String url) {
        Controller.getController().openUrl(url);
    }

    protected static void callSetIdleTimer(boolean enabled) {
        Controller.getController().setIdleTimer(enabled);
    }

    protected static boolean callGetIdleTimer() {
        return Controller.getController().getIdleTimer();
    }

    protected static void callShowNativeAlert(String title, String msg, String[] buttonLabels) {
        Controller.getController().showNativeAlert(title, msg, buttonLabels);
    }

    protected static void callCancelNativeAlert(int which) {
        Controller.getController().cancelNativeAlert(which);
    }

    protected static void callShowTrialAlert() {
        Controller.getController().showTrialAlert();
    }

    protected static void callDisplayUpdate() {
        Controller.getController().displayUpdate();
    }

    protected static void callSetAccelerometerInterval(int frequencyInHz) {
        Controller.getController().setAccelerometerInterval(frequencyInHz);
    }

    protected static void callSetGyroscopeInterval(int frequencyInHz) {
        Controller.getController().setGyroscopeInterval(frequencyInHz);
    }

    protected static boolean callHasAccelerometer() {
        return Controller.getController().hasAccelerometer();
    }

    protected static boolean callHasGyroscope() {
        return Controller.getController().hasGyroscope();
    }

    protected static void callSetEventNotification(int eventType, boolean enable) {
        Controller.getController().setEventNotification(eventType, enable);
    }

    protected static String callGetModel() {
        return Controller.getController().getModel();
    }

    protected static String callGetName() {
        return Controller.getController().getName();
    }

    protected static String callGetUniqueIdentifier() {
        return Controller.getController().getUniqueIdentifier();
    }

    protected static String callGetPlatformVersion() {
        return Controller.getController().getPlatformVersion();
    }

    protected static String callGetProductName() {
        return Controller.getController().getProductName();
    }

    protected static String callGetPreference(int category) {
        return Controller.getController().getPreference(category);
    }

    protected static void callVibrate() {
        Controller.getController().vibrate();
    }

    protected static void callSetLocationAccuracy(double meters) {
    }

    protected static void callSetLocationThreshold(double meters) {
    }

    protected static float callGetVolume(int id) {
        return Controller.getMediaManager().getVolume(id);
    }

    protected static void callSetVolume(int id, float v) {
        Controller.getMediaManager().setVolume(id, v);
    }

    protected static int callTextFieldCreate(int id, int left, int top, int width, int height, boolean isEditable) {
        ViewManager.getViewManager().addTextView(id, left, top, width, height, isEditable);
        return 1;
    }

    protected static void callTextFieldDestroy(int id) {
        ViewManager.getViewManager().destroyTextView(id);
    }

    protected static void callTextFieldSetColor(int id, int r, int g, int b, int a) {
        ViewManager.getViewManager().setTextViewColor(id, Color.argb(a, r, g, b));
    }

    protected static void callTextFieldSetText(int id, String text) {
        ViewManager.getViewManager().setTextViewText(id, text);
    }

    protected static void callTextFieldSetSize(int id, float fontSize) {
        ViewManager.getViewManager().setTextViewSize(id, fontSize);
    }

    protected static void callTextFieldSetFont(int id, String fontName, float fontSize) {
        ViewManager.getViewManager().setTextViewFont(id, fontName, fontSize, getBridge().getActivity());
    }

    protected static void callTextFieldSetAlign(int id, String align) {
        ViewManager.getViewManager().setTextViewAlign(id, align);
    }

    protected static void callTextFieldSetSecure(int id, boolean isSecure) {
        ViewManager.getViewManager().setTextViewPassword(id, isSecure);
    }

    protected static void callTextFieldSetInputType(int id, String inputType) {
        ViewManager.getViewManager().setTextViewInputType(id, inputType);
    }

    protected static int[] callTextFieldGetColor(int id) {
        int argb = ViewManager.getViewManager().getTextViewColor(id);
        return new int[]{Color.red(argb), Color.green(argb), Color.blue(argb), Color.alpha(argb)};
    }

    protected static String callTextFieldGetText(int id) {
        return ViewManager.getViewManager().getTextViewText(id);
    }

    protected static float callTextFieldGetSize(int id) {
        return ViewManager.getViewManager().getTextViewSize(id);
    }

    protected static String callTextFieldGetFont(int id) {
        return "";
    }

    protected static String callTextFieldGetAlign(int id) {
        return ViewManager.getViewManager().getTextViewAlign(id);
    }

    protected static boolean callTextFieldGetSecure(int id) {
        return ViewManager.getViewManager().getTextViewPassword(id);
    }

    protected static String callTextFieldGetInputType(int id) {
        return ViewManager.getViewManager().getTextViewInputType(id);
    }

    protected static void callFBConnectLogin(int id, String appId, String[] permissions) {
        if (permissions == null) {
            permissions = new String[0];
        }
        Controller.getController().facebookLogin(appId, permissions);
    }

    protected static void callFBConnectLogout(int id) {
        Controller.getController().facebookLogout();
    }

    protected static void callFBConnectRequest(int id, String path, String method, HashMap params) {
        Controller.getController().facebookRequest(path, method, params);
    }

    protected static void callFBShowDialog(int id, String action, HashMap params) {
        Controller.getController().facebookDialog(action, params);
    }

    protected static void callDisplayObjectSetVisible(int id, boolean visible) {
        ViewManager.getViewManager().setDisplayObjectVisible(id, visible);
    }

    protected static void callDisplayObjectUpdateScreenBounds(int id, int x, int y, int w, int h) {
        ViewManager.getViewManager().displayObjectUpdateScreenBounds(id, x, y, w, h);
    }

    protected static void callDisplayObjectSetAlpha(int id, int alpha) {
    }

    protected static void callDisplayObjectSetBackground(int id, boolean bg) {
        ViewManager.getViewManager().setDisplayObjectBackground(id, bg);
    }

    protected static boolean callDisplayObjectGetVisible(int id) {
        return ViewManager.getViewManager().getDisplayObjectVisible(id);
    }

    protected static int callDisplayObjectGetAlpha(int id) {
        return 0;
    }

    protected static boolean callDisplayObjectGetBackground(int id) {
        return ViewManager.getViewManager().getDisplayObjectBackground(id);
    }

    protected static void callDisplayObjectSetFocus(int id, boolean focus) {
        ViewManager.getViewManager().setTextViewFocus(id, focus);
    }

    protected static void callRecordStart(String file, int id) {
        Controller.getMediaManager().getAudioRecorder(id).startRecording(file);
    }

    protected static void callRecordStop(int id) {
        Controller.getMediaManager().getAudioRecorder(id).stopRecording();
    }

    protected static ByteBuffer callRecordGetBytes(int id) {
        AudioRecorder.AudioByteBufferHolder buffer = Controller.getMediaManager().getAudioRecorder(id).getNextBuffer();
        if (buffer != null) {
            return buffer.myBuffer;
        }
        return null;
    }

    protected static int callRecordGetCurrentByteCount(int id) {
        AudioRecorder.AudioByteBufferHolder buffer = Controller.getMediaManager().getAudioRecorder(id).getCurrentBuffer();
        if (buffer != null) {
            return buffer.myValidBytes;
        }
        return 0;
    }

    protected static void callRecordReleaseCurrentBuffer(int id) {
        Controller.getMediaManager().getAudioRecorder(id).releaseCurrentBuffer();
    }

    protected static int callWebViewCreate(int id, int left, int top, int width, int height, boolean background) {
        ViewManager.getViewManager().addWebView(id, left, top, width, height, background);
        return 1;
    }

    protected static void callWebViewDestroy(int id) {
        ViewManager.getViewManager().destroyWebView(id);
    }

    protected static void callWebViewShow(int id, String url) {
        ViewManager.getViewManager().showWebView(id, url);
    }

    protected static boolean callWebViewClose(int id) {
        return ViewManager.getViewManager().closeWebView(id);
    }

    protected static int callCryptoGetDigestLength(String algorithm) {
        return Crypto.GetDigestLength(algorithm);
    }

    protected static byte[] callCryptoCalculateDigest(String algorithm, byte[] data) {
        return Crypto.CalculateDigest(algorithm, data);
    }

    protected static byte[] callCryptoCalculateHMAC(String algorithm, String key, byte[] data) {
        return Crypto.CalculateHMAC(algorithm, key, data);
    }

    protected static void callNetworkRequest(String url, String method, int listenerId, HashMap params, String body, String path) {
        getBridge().networkRequest(url, method, listenerId, params, body, path);
    }

    protected static void callFlurryInit(String applicationId) {
        Controller.getController().flurryInit(applicationId);
    }

    protected static void callFlurryEvent(String eventId) {
        Controller.getController().flurryEvent(eventId);
    }

    protected static void callPapayaInit(String apiKey, String mapsApiKey) {
        Controller.getController().papayaInit(apiKey, mapsApiKey);
    }

    protected static void callPapayaShow(String name, String data) {
        Controller.getController().papayaShow(name, data);
    }

    protected static void callPapayaUnlockAchievement(String achievementId) {
        Controller.getController().papayaUnlockAchievement(achievementId);
    }

    protected static void callPapayaSetHighScore(String leaderboardId, int score) {
        Controller.getController().papayaSetHighScore(leaderboardId, score);
    }

    protected static void callOpenFeintInit(String appName, String appKey, String appSecret, String appId) {
        Controller.getController().openFeintInit(appName, appKey, appSecret, appId);
    }

    protected static void callOpenFeintLaunchDashboard(String dashboard) {
        Controller.getController().openFeintLaunchDashboard(dashboard);
    }

    protected static void callOpenFeintUnlockAchievement(String achievementId) {
        Controller.getController().openFeintUnlockAchievement(achievementId);
    }

    protected static void callOpenFeintSetHighScore(String leaderboardId, int score, String displayText) {
        Controller.getController().openFeintSetHighScore(leaderboardId, (long) score, displayText);
    }

    protected static void callSuperRewardsInit(String appId, String uid) {
        Controller.getController().superRewardsInit(appId, uid);
    }

    protected static void callSuperRewardsShowOffers() {
        Controller.getController().superRewardsShowOffers();
    }

    protected static void callSuperRewardsRequestUpdate() {
        Controller.getController().superRewardsRequestUpdate();
    }

    protected static void callInMobiInit(String applicationId) {
        Controller.getController();
        AdManager manager = Controller.getAdManager();
        manager.hideInMobiBanner();
        manager.setInMobiApplicationId(applicationId);
    }

    protected static void callInMobiShow(String bannerTypeName, float x, float y, double intervalInSeconds, boolean testModeEnabled) {
        Controller.getController();
        Controller.getAdManager().showInMobiBanner(bannerTypeName, x, y, intervalInSeconds, testModeEnabled);
    }

    protected static void callInMobiHide() {
        Controller.getController();
        Controller.getAdManager().hideInMobiBanner();
    }
}
