package com.ansca.corona;

import android.app.Activity;
import android.location.Location;
import android.util.Log;
import android.view.ViewGroup;
import com.inmobi.androidsdk.EducationType;
import com.inmobi.androidsdk.EthnicityType;
import com.inmobi.androidsdk.GenderType;
import com.inmobi.androidsdk.InMobiAdDelegate;
import com.inmobi.androidsdk.impl.InMobiAdView;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class AdManager {
    /* access modifiers changed from: private */
    public String fInMobiApplicationId;
    /* access modifiers changed from: private */
    public InMobiAdView fInMobiBannerView;
    /* access modifiers changed from: private */
    public Timer fInMobiTimer;
    /* access modifiers changed from: private */
    public Activity fParentActivity;

    public AdManager(Activity activity) {
        if (activity == null) {
            throw new IllegalArgumentException("Activity cannot be null.");
        }
        this.fParentActivity = activity;
        this.fInMobiApplicationId = "";
        this.fInMobiBannerView = null;
        this.fInMobiTimer = null;
    }

    public void hideAllBanners() {
        hideInMobiBanner();
    }

    private class CoronaInMobiAdDelegate implements InMobiAdDelegate {
        private String fApplicationId;
        private boolean fIsTestModeEnabled;

        public CoronaInMobiAdDelegate(String applicationId, boolean testModeEnabled) {
            this.fApplicationId = applicationId;
            this.fIsTestModeEnabled = testModeEnabled;
        }

        public void adRequestCompleted(InMobiAdView arg0) {
        }

        public void adRequestFailed(InMobiAdView arg0) {
            Log.v("Corona", "InMobi ad request failed");
        }

        public String siteId() {
            return AdManager.this.fInMobiApplicationId;
        }

        public int age() {
            return 0;
        }

        public String areaCode() {
            return null;
        }

        public Location currentLocation() {
            return null;
        }

        public Date dateOfBirth() {
            return null;
        }

        public EducationType education() {
            return null;
        }

        public EthnicityType ethnicity() {
            return null;
        }

        public GenderType gender() {
            return null;
        }

        public int income() {
            return 0;
        }

        public String interests() {
            return null;
        }

        public boolean isLocationInquiryAllowed() {
            return false;
        }

        public boolean isPublisherProvidingLocation() {
            return false;
        }

        public String keywords() {
            return null;
        }

        public String postalCode() {
            return null;
        }

        public String searchString() {
            return null;
        }

        public boolean testMode() {
            return this.fIsTestModeEnabled;
        }
    }

    public void setInMobiApplicationId(String id) {
        this.fInMobiApplicationId = id != null ? id : "";
    }

    public String getInMobiApplicationId() {
        return this.fInMobiApplicationId;
    }

    public void showInMobiBanner(String bannerTypeName, float x, float y, double intervalInSeconds, boolean testModeEnabled) {
        if (this.fParentActivity != null && this.fInMobiApplicationId != null && this.fInMobiApplicationId.length() > 0) {
            final String copyOfInMobiApplicationId = this.fInMobiApplicationId;
            final String str = bannerTypeName;
            final boolean z = testModeEnabled;
            final float f = x;
            final float f2 = y;
            final double d = intervalInSeconds;
            this.fParentActivity.runOnUiThread(new Runnable() {
                public void run() {
                    int bannerTypeId;
                    AdManager.this.hideInMobiBanner();
                    String lowerCaseBannerTypeName = str.trim().toLowerCase();
                    if (lowerCaseBannerTypeName.equals("banner320x48")) {
                        bannerTypeId = 9;
                    } else if (lowerCaseBannerTypeName.equals("banner300x250")) {
                        bannerTypeId = 10;
                    } else if (lowerCaseBannerTypeName.equals("banner728x90")) {
                        bannerTypeId = 11;
                    } else if (lowerCaseBannerTypeName.equals("banner468x60")) {
                        bannerTypeId = 12;
                    } else if (lowerCaseBannerTypeName.equals("banner120x600")) {
                        bannerTypeId = 13;
                    } else {
                        bannerTypeId = 9;
                        Log.v("Corona", "Unknown banner name '" + str + "' given to adNetwork.show() function. Defaulting to 'banner320x48'.");
                    }
                    InMobiAdView unused = AdManager.this.fInMobiBannerView = InMobiAdView.requestAdUnitWithDelegate(AdManager.this.fParentActivity.getApplicationContext(), new CoronaInMobiAdDelegate(copyOfInMobiApplicationId, z), AdManager.this.fParentActivity, bannerTypeId);
                    if (AdManager.this.fInMobiBannerView != null) {
                        AdManager.this.fInMobiBannerView.offsetLeftAndRight(Math.max((int) f, 0));
                        AdManager.this.fInMobiBannerView.offsetTopAndBottom(Math.max((int) f2, 0));
                        AdManager.this.fInMobiBannerView.loadNewAd();
                        long intervalInMilliseconds = ((long) d) * 1000;
                        Timer unused2 = AdManager.this.fInMobiTimer = new Timer();
                        AdManager.this.fInMobiTimer.schedule(new TimerTask() {
                            public void run() {
                                synchronized (this) {
                                    if (AdManager.this.fInMobiBannerView != null) {
                                        AdManager.this.fInMobiBannerView.loadNewAd();
                                    }
                                }
                            }
                        }, intervalInMilliseconds, intervalInMilliseconds);
                    }
                }
            });
        }
    }

    public void hideInMobiBanner() {
        if (this.fParentActivity != null && this.fInMobiBannerView != null) {
            this.fParentActivity.runOnUiThread(new Runnable() {
                public void run() {
                    if (AdManager.this.fInMobiTimer != null) {
                        AdManager.this.fInMobiTimer.cancel();
                        AdManager.this.fInMobiTimer.purge();
                        Timer unused = AdManager.this.fInMobiTimer = null;
                    }
                    if (AdManager.this.fInMobiBannerView != null) {
                        AdManager.this.fInMobiBannerView.stopReceivingNotifications();
                        ViewGroup group = (ViewGroup) AdManager.this.fInMobiBannerView.getParent();
                        if (group != null) {
                            group.removeView(AdManager.this.fInMobiBannerView);
                        }
                    }
                    InMobiAdView unused2 = AdManager.this.fInMobiBannerView = null;
                }
            });
        }
    }

    public boolean isInMobiBannerShown() {
        return this.fInMobiBannerView != null;
    }
}
