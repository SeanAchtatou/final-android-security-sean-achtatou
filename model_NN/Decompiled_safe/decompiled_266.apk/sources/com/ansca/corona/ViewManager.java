package com.ansca.corona;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsoluteLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.HashMap;

public class ViewManager {
    private static ViewManager ourViewManager;
    /* access modifiers changed from: private */
    public CoronaActivity myActivity;
    /* access modifiers changed from: private */
    public HashMap<Integer, View> myObjectViews = new HashMap<>();
    /* access modifiers changed from: private */
    public CoronaVideoView myVideoView;
    /* access modifiers changed from: private */
    public ViewGroup myViewGroup;
    /* access modifiers changed from: private */
    public CoronaWebView myWebView;

    private ViewManager(CoronaActivity activity) {
        this.myActivity = activity;
    }

    /* access modifiers changed from: private */
    public ViewGroup getObjectGroup() {
        return this.myViewGroup;
    }

    public static void initialize(CoronaActivity activity) {
        if (ourViewManager == null) {
            ourViewManager = new ViewManager(activity);
        }
    }

    public static void destroy() {
        if (ourViewManager != null) {
            ourViewManager = null;
        }
    }

    public static ViewManager getViewManager() {
        return ourViewManager;
    }

    /* access modifiers changed from: private */
    public ICoronaTextView getTextView(int id) {
        return (ICoronaTextView) this.myObjectViews.get(new Integer(id));
    }

    public boolean goBack() {
        if (this.myWebView != null && this.myWebView.canGoBack()) {
            this.myWebView.goBack();
            return true;
        } else if (this.myVideoView == null) {
            return false;
        } else {
            destroyVideoView();
            return true;
        }
    }

    public void addTextView(int id, int left, int top, int width, int height, boolean isEditable) {
        final int i = width;
        final int i2 = height;
        final int i3 = left;
        final int i4 = top;
        final boolean z = isEditable;
        final int i5 = id;
        this.myActivity.getHandler().post(new Runnable() {
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: com.ansca.corona.CoronaTextView} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: com.ansca.corona.CoronaTextView} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.ansca.corona.CoronaEditText} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: com.ansca.corona.CoronaTextView} */
            /* JADX WARNING: Multi-variable type inference failed */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r9 = this;
                    r8 = -16777216(0xffffffffff000000, float:-1.7014118E38)
                    android.widget.AbsoluteLayout$LayoutParams r1 = new android.widget.AbsoluteLayout$LayoutParams
                    int r4 = r2
                    int r5 = r3
                    int r6 = r4
                    int r7 = r5
                    r1.<init>(r4, r5, r6, r7)
                    boolean r4 = r6
                    if (r4 == 0) goto L_0x0044
                    com.ansca.corona.CoronaEditText r0 = new com.ansca.corona.CoronaEditText
                    com.ansca.corona.ViewManager r4 = com.ansca.corona.ViewManager.this
                    com.ansca.corona.CoronaActivity r4 = r4.myActivity
                    int r5 = r7
                    r0.<init>(r4, r5)
                    com.ansca.corona.ViewManager r4 = com.ansca.corona.ViewManager.this
                    android.view.ViewGroup r4 = r4.getObjectGroup()
                    r4.addView(r0, r1)
                    r0.setTextColor(r8)
                    r0.setSingleLine()
                    r3 = r0
                L_0x0030:
                    r3.bringToFront()
                    com.ansca.corona.ViewManager r4 = com.ansca.corona.ViewManager.this
                    java.util.HashMap r4 = r4.myObjectViews
                    java.lang.Integer r5 = new java.lang.Integer
                    int r6 = r7
                    r5.<init>(r6)
                    r4.put(r5, r3)
                    return
                L_0x0044:
                    com.ansca.corona.CoronaTextView r2 = new com.ansca.corona.CoronaTextView
                    com.ansca.corona.ViewManager r4 = com.ansca.corona.ViewManager.this
                    com.ansca.corona.CoronaActivity r4 = r4.myActivity
                    r2.<init>(r4)
                    com.ansca.corona.ViewManager r4 = com.ansca.corona.ViewManager.this
                    android.view.ViewGroup r4 = r4.getObjectGroup()
                    r4.addView(r2, r1)
                    r2.setTextColor(r8)
                    r4 = -1
                    r2.setBackgroundColor(r4)
                    r3 = r2
                    goto L_0x0030
                */
                throw new UnsupportedOperationException("Method not decompiled: com.ansca.corona.ViewManager.AnonymousClass1.run():void");
            }
        });
    }

    public void destroyTextView(final int id) {
        this.myActivity.getHandler().post(new Runnable() {
            public void run() {
                ICoronaTextView view = ViewManager.this.getTextView(id);
                if (view != null) {
                    if (view instanceof CoronaEditText) {
                        ((CoronaEditText) view).destroying();
                    }
                    ViewManager.this.myObjectViews.remove(new Integer(id));
                    ViewManager.this.getObjectGroup().removeView((View) view);
                }
            }
        });
    }

    public void setTextViewInputType(final int id, final String inputType) {
        this.myActivity.getHandler().post(new Runnable() {
            public void run() {
                ICoronaTextView view = ViewManager.this.getTextView(id);
                if (view != null) {
                    view.setTextViewInputType(inputType);
                }
            }
        });
    }

    public String getTextViewInputType(int id) {
        ICoronaTextView view = getTextView(id);
        if (view == null) {
            return "error";
        }
        return view.getTextViewInputType();
    }

    public void setTextViewPassword(final int id, final boolean isPassword) {
        this.myActivity.getHandler().post(new Runnable() {
            public void run() {
                ICoronaTextView view = ViewManager.this.getTextView(id);
                if (view != null) {
                    view.setTextViewPassword(isPassword);
                }
            }
        });
    }

    public boolean getTextViewPassword(int id) {
        ICoronaTextView view = getTextView(id);
        if (view == null) {
            return false;
        }
        return view.getTextViewPassword();
    }

    public void setTextViewAlign(final int id, final String align) {
        this.myActivity.getHandler().post(new Runnable() {
            public void run() {
                ICoronaTextView view = ViewManager.this.getTextView(id);
                if (view != null) {
                    view.setTextViewAlign(align);
                }
            }
        });
    }

    public String getTextViewAlign(int id) {
        ICoronaTextView view = getTextView(id);
        if (view == null) {
            return "";
        }
        return view.getTextViewAlign();
    }

    public void setTextViewColor(final int id, final int color) {
        this.myActivity.getHandler().post(new Runnable() {
            public void run() {
                ICoronaTextView view = ViewManager.this.getTextView(id);
                if (view != null) {
                    view.setTextViewColor(color);
                }
            }
        });
    }

    public int getTextViewColor(int id) {
        ICoronaTextView view = getTextView(id);
        if (view == null) {
            return 0;
        }
        return view.getTextViewColor();
    }

    public void setTextViewSize(final int id, final float size) {
        this.myActivity.getHandler().post(new Runnable() {
            public void run() {
                ICoronaTextView view = ViewManager.this.getTextView(id);
                if (view != null) {
                    view.setTextViewSize(size);
                }
            }
        });
    }

    public float getTextViewSize(int id) {
        ICoronaTextView view = getTextView(id);
        if (view == null) {
            return 0.0f;
        }
        return view.getTextViewSize();
    }

    public void setTextViewFocus(final int id, final boolean focus) {
        this.myActivity.getHandler().post(new Runnable() {
            public void run() {
                ICoronaTextView view = null;
                if (id != 0) {
                    view = ViewManager.this.getTextView(id);
                }
                if (view != null) {
                    TextView textView = view.getTextView();
                    if (focus) {
                        textView.requestFocus();
                        ((InputMethodManager) ViewManager.this.myActivity.getSystemService("input_method")).showSoftInput(textView, 2);
                        return;
                    }
                    textView.clearFocus();
                    ((InputMethodManager) ViewManager.this.myActivity.getSystemService("input_method")).hideSoftInputFromWindow(textView.getApplicationWindowToken(), 0);
                    return;
                }
                View focusView = ViewManager.this.myViewGroup.findFocus();
                if (focusView != null) {
                    focusView.clearFocus();
                    ((InputMethodManager) ViewManager.this.myActivity.getSystemService("input_method")).hideSoftInputFromWindow(focusView.getApplicationWindowToken(), 0);
                }
            }
        });
    }

    public void setTextViewText(final int id, final String text) {
        this.myActivity.getHandler().post(new Runnable() {
            public void run() {
                ICoronaTextView view = ViewManager.this.getTextView(id);
                if (view != null) {
                    view.getTextView().setText(text);
                }
            }
        });
    }

    public void setTextViewFont(int id, String fontName, float fontSize, CoronaActivity activity) {
        final int i = id;
        final String str = fontName;
        final float f = fontSize;
        final CoronaActivity coronaActivity = activity;
        this.myActivity.getHandler().post(new Runnable() {
            public void run() {
                ICoronaTextView view = ViewManager.this.getTextView(i);
                if (view != null) {
                    view.setTextViewFont(str, f, coronaActivity);
                }
            }
        });
    }

    public String getTextViewText(int id) {
        ICoronaTextView view = getTextView(id);
        if (view == null) {
            return "";
        }
        return view.getTextView().getText().toString();
    }

    public void setGLView(View glView) {
        this.myViewGroup = new AbsoluteLayout(this.myActivity);
        this.myViewGroup.addView(glView);
    }

    public ViewGroup getViewGroup() {
        return this.myViewGroup;
    }

    public void setDisplayObjectVisible(final int id, final boolean visible) {
        this.myActivity.getHandler().post(new Runnable() {
            public void run() {
                View view = (View) ViewManager.this.myObjectViews.get(new Integer(id));
                if (view != null) {
                    view.setVisibility(visible ? 0 : 4);
                }
            }
        });
    }

    public void displayObjectUpdateScreenBounds(int id, int left, int top, int width, int height) {
        final int i = id;
        final int i2 = width;
        final int i3 = height;
        final int i4 = left;
        final int i5 = top;
        this.myActivity.getHandler().post(new Runnable() {
            public void run() {
                View view = (View) ViewManager.this.myObjectViews.get(new Integer(i));
                if (view != null) {
                    view.setLayoutParams(new AbsoluteLayout.LayoutParams(i2, i3, i4, i5));
                }
            }
        });
    }

    public boolean getDisplayObjectVisible(int id) {
        View view = this.myObjectViews.get(new Integer(id));
        if (view != null) {
            return view.getVisibility() == 0;
        }
        return false;
    }

    public void setDisplayObjectBackground(final int id, final boolean bg) {
        this.myActivity.getHandler().post(new Runnable() {
            public void run() {
                View view = (View) ViewManager.this.myObjectViews.get(new Integer(id));
                if (view == null) {
                    return;
                }
                if (bg) {
                    view.setBackgroundColor(-1);
                } else {
                    view.setBackgroundDrawable(null);
                }
            }
        });
    }

    public boolean getDisplayObjectBackground(int id) {
        View view = this.myObjectViews.get(new Integer(id));
        if (view == null || view.getBackground() == null) {
            return false;
        }
        return true;
    }

    public CoronaVideoView createVideoView() {
        this.myVideoView = new CoronaVideoView(this.myActivity);
        RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(-1, -1);
        relativeParams.addRule(13);
        getObjectGroup().addView(this.myVideoView, relativeParams);
        this.myActivity.getGLView().setVisibility(4);
        return this.myVideoView;
    }

    public void destroyVideoView() {
        this.myActivity.getHandler().post(new Runnable() {
            public void run() {
                if (ViewManager.this.myVideoView != null) {
                    ViewManager.this.getObjectGroup().removeView(ViewManager.this.myVideoView);
                    ViewManager.this.myActivity.getGLView().setVisibility(0);
                    ViewManager.this.myActivity.getGLView().requestFocus();
                    CoronaVideoView unused = ViewManager.this.myVideoView = null;
                }
            }
        });
    }

    public void addWebView(int id, int left, int top, int width, int height, boolean background) {
        if (this.myWebView == null) {
            final int i = width;
            final int i2 = height;
            final int i3 = left;
            final int i4 = top;
            final int i5 = id;
            final boolean z = background;
            this.myActivity.getHandler().post(new Runnable() {
                /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: android.widget.AbsoluteLayout$LayoutParams} */
                /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: android.widget.AbsoluteLayout$LayoutParams} */
                /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: android.widget.RelativeLayout$LayoutParams} */
                /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: android.widget.AbsoluteLayout$LayoutParams} */
                /* JADX WARNING: Multi-variable type inference failed */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r7 = this;
                        r3 = -1
                        int r2 = r2
                        if (r2 != 0) goto L_0x0041
                        int r2 = r3
                        if (r2 != 0) goto L_0x0041
                        android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
                        r1.<init>(r3, r3)
                        r2 = 13
                        r1.addRule(r2)
                        r0 = r1
                    L_0x0014:
                        com.ansca.corona.ViewManager r2 = com.ansca.corona.ViewManager.this
                        com.ansca.corona.CoronaWebView r3 = new com.ansca.corona.CoronaWebView
                        com.ansca.corona.ViewManager r4 = com.ansca.corona.ViewManager.this
                        com.ansca.corona.CoronaActivity r4 = r4.myActivity
                        int r5 = r6
                        boolean r6 = r7
                        r3.<init>(r4, r5, r6)
                        com.ansca.corona.CoronaWebView unused = r2.myWebView = r3
                        com.ansca.corona.ViewManager r2 = com.ansca.corona.ViewManager.this
                        android.view.ViewGroup r2 = r2.getObjectGroup()
                        com.ansca.corona.ViewManager r3 = com.ansca.corona.ViewManager.this
                        com.ansca.corona.CoronaWebView r3 = r3.myWebView
                        r2.addView(r3, r0)
                        com.ansca.corona.ViewManager r2 = com.ansca.corona.ViewManager.this
                        com.ansca.corona.CoronaWebView r2 = r2.myWebView
                        r2.bringToFront()
                        return
                    L_0x0041:
                        android.widget.AbsoluteLayout$LayoutParams r0 = new android.widget.AbsoluteLayout$LayoutParams
                        int r2 = r2
                        int r3 = r3
                        int r4 = r4
                        int r5 = r5
                        r0.<init>(r2, r3, r4, r5)
                        goto L_0x0014
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.ansca.corona.ViewManager.AnonymousClass15.run():void");
                }
            });
        }
    }

    public void destroyWebView(final int id) {
        this.myActivity.getHandler().post(new Runnable() {
            public void run() {
                if (ViewManager.this.myWebView != null) {
                    ViewManager.this.myObjectViews.remove(new Integer(id));
                    ViewManager.this.getObjectGroup().removeView(ViewManager.this.myWebView);
                    CoronaWebView unused = ViewManager.this.myWebView = null;
                }
            }
        });
    }

    public void showWebView(int id, final String url) {
        this.myActivity.getHandler().post(new Runnable() {
            public void run() {
                if (ViewManager.this.myWebView != null) {
                    ViewManager.this.myWebView.getSettings().setJavaScriptEnabled(true);
                    ViewManager.this.myWebView.getSettings().setSupportZoom(true);
                    ViewManager.this.myWebView.getSettings().setBuiltInZoomControls(true);
                    ViewManager.this.myWebView.getSettings().setLoadWithOverviewMode(true);
                    ViewManager.this.myWebView.getSettings().setUseWideViewPort(true);
                    ViewManager.this.myWebView.requestLoadUrl(url);
                    ViewManager.this.myWebView.requestFocus(130);
                    ViewManager.this.myWebView.setOnTouchListener(new View.OnTouchListener() {
                        public boolean onTouch(View v, MotionEvent event) {
                            switch (event.getAction()) {
                                case 0:
                                case 1:
                                    if (v.hasFocus()) {
                                        return false;
                                    }
                                    v.requestFocus();
                                    return false;
                                default:
                                    return false;
                            }
                        }
                    });
                }
            }
        });
    }

    public boolean closeWebView(int id) {
        if (this.myWebView == null) {
            return false;
        }
        destroyWebView(id);
        return true;
    }
}
