package com.google.i18n.phonenumbers;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.Phonemetadata;
import com.google.i18n.phonenumbers.Phonenumber;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumberUtil {
    private static final Map<Character, Character> ALL_NORMALIZATION_MAPPINGS;
    private static final Map<Character, Character> ALPHA_MAPPINGS;
    private static final Pattern CAPTURING_DIGIT_PATTERN = Pattern.compile("([" + VALID_DIGITS + "])");
    private static final Pattern CC_PATTERN = Pattern.compile("\\$CC");
    static final String COUNTRY_CODE_TO_REGION_CODE_MAP_CLASS_NAME = "CountryCodeToRegionCodeMap";
    private static final String DEFAULT_EXTN_PREFIX = " ext. ";
    static final Map<Character, Character> DIGIT_MAPPINGS;
    private static final Pattern EXTN_PATTERN = Pattern.compile("(?:" + KNOWN_EXTN_PATTERNS + ")$", REGEX_FLAGS);
    private static final Pattern FG_PATTERN = Pattern.compile("\\$FG");
    private static final Pattern FIRST_GROUP_PATTERN = Pattern.compile("(\\$1)");
    static final String KNOWN_EXTN_PATTERNS = ("[  \\t,]*(?:ext(?:ensi(?:ó?|ó))?n?|ｅｘｔｎ?|[,xｘ#＃~～]|int|anexo|ｉｎｔ)[:\\.．]?[  \\t,-]*([" + VALID_DIGITS + "]{1,7})#?|[- ]+([" + VALID_DIGITS + "]{1,5})#");
    private static final Set<Integer> LEADING_ZERO_COUNTRIES;
    private static final Logger LOGGER = Logger.getLogger(PhoneNumberUtil.class.getName());
    static final int MAX_LENGTH_COUNTRY_CODE = 3;
    static final int MAX_LENGTH_FOR_NSN = 15;
    static final String META_DATA_FILE_PREFIX = "/com/google/i18n/phonenumbers/data/PhoneNumberMetadataProto";
    private static final int MIN_LENGTH_FOR_NSN = 3;
    private static final int NANPA_COUNTRY_CODE = 1;
    private static final Pattern NON_DIGITS_PATTERN = Pattern.compile("(\\D+)");
    private static final Pattern NP_PATTERN = Pattern.compile("\\$NP");
    static final String PLUS_CHARS = "+＋";
    private static final Pattern PLUS_CHARS_PATTERN = Pattern.compile("[+＋]+");
    static final char PLUS_SIGN = '+';
    static final int REGEX_FLAGS = 66;
    private static final String SECOND_NUMBER_START = "[\\\\/] *x";
    static final Pattern SECOND_NUMBER_START_PATTERN = Pattern.compile(SECOND_NUMBER_START);
    private static final Pattern UNIQUE_INTERNATIONAL_PREFIX = Pattern.compile("[\\d]+(?:[~⁓∼～][\\d]+)?");
    private static final String UNKNOWN_REGION = "ZZ";
    private static final String UNWANTED_END_CHARS = "[[\\P{N}&&\\P{L}]&&[^#]]+$";
    private static final Pattern UNWANTED_END_CHAR_PATTERN = Pattern.compile(UNWANTED_END_CHARS);
    private static final String VALID_ALPHA = (Arrays.toString(ALPHA_MAPPINGS.keySet().toArray()).replaceAll("[, \\[\\]]", "") + Arrays.toString(ALPHA_MAPPINGS.keySet().toArray()).toLowerCase().replaceAll("[, \\[\\]]", ""));
    private static final Pattern VALID_ALPHA_PHONE_PATTERN = Pattern.compile("(?:.*?[A-Za-z]){3}.*");
    private static final String VALID_DIGITS = Arrays.toString(DIGIT_MAPPINGS.keySet().toArray()).replaceAll("[, \\[\\]]", "");
    private static final String VALID_PHONE_NUMBER = ("[+＋]*(?:[-x‐-―−ー－-／  ​⁠　()（）［］.\\[\\]/~⁓∼～]*[" + VALID_DIGITS + "]){3,}[" + VALID_PUNCTUATION + VALID_ALPHA + VALID_DIGITS + "]*");
    private static final Pattern VALID_PHONE_NUMBER_PATTERN = Pattern.compile(VALID_PHONE_NUMBER + "(?:" + KNOWN_EXTN_PATTERNS + ")?", REGEX_FLAGS);
    static final String VALID_PUNCTUATION = "-x‐-―−ー－-／  ​⁠　()（）［］.\\[\\]/~⁓∼～";
    private static final String VALID_START_CHAR = ("[+＋" + VALID_DIGITS + "]");
    static final Pattern VALID_START_CHAR_PATTERN = Pattern.compile(VALID_START_CHAR);
    private static PhoneNumberUtil instance = null;
    private Map<Integer, List<String>> countryCodeToRegionCodeMap = null;
    private Map<String, Phonemetadata.PhoneMetadata> countryToMetadataMap = new HashMap();
    private String currentFilePrefix = META_DATA_FILE_PREFIX;
    private final Set<String> nanpaCountries = new HashSet(35);
    private RegexCache regexCache = new RegexCache(100);
    private final Set<String> supportedCountries = new HashSet(300);

    public enum MatchType {
        NOT_A_NUMBER,
        NO_MATCH,
        SHORT_NSN_MATCH,
        NSN_MATCH,
        EXACT_MATCH
    }

    public enum PhoneNumberFormat {
        E164,
        INTERNATIONAL,
        NATIONAL
    }

    public enum PhoneNumberType {
        FIXED_LINE,
        MOBILE,
        FIXED_LINE_OR_MOBILE,
        TOLL_FREE,
        PREMIUM_RATE,
        SHARED_COST,
        VOIP,
        PERSONAL_NUMBER,
        PAGER,
        UAN,
        UNKNOWN
    }

    public enum ValidationResult {
        IS_POSSIBLE,
        INVALID_COUNTRY_CODE,
        TOO_SHORT,
        TOO_LONG
    }

    static {
        HashMap hashMap = new HashMap(50);
        hashMap.put('0', '0');
        hashMap.put(65296, '0');
        hashMap.put(1632, '0');
        hashMap.put(1776, '0');
        hashMap.put('1', '1');
        hashMap.put(65297, '1');
        hashMap.put(1633, '1');
        hashMap.put(1777, '1');
        hashMap.put('2', '2');
        hashMap.put(65298, '2');
        hashMap.put(1634, '2');
        hashMap.put(1778, '2');
        hashMap.put('3', '3');
        hashMap.put(65299, '3');
        hashMap.put(1635, '3');
        hashMap.put(1779, '3');
        hashMap.put('4', '4');
        hashMap.put(65300, '4');
        hashMap.put(1636, '4');
        hashMap.put(1780, '4');
        hashMap.put('5', '5');
        hashMap.put(65301, '5');
        hashMap.put(1637, '5');
        hashMap.put(1781, '5');
        hashMap.put('6', '6');
        hashMap.put(65302, '6');
        hashMap.put(1638, '6');
        hashMap.put(1782, '6');
        hashMap.put('7', '7');
        hashMap.put(65303, '7');
        hashMap.put(1639, '7');
        hashMap.put(1783, '7');
        hashMap.put('8', '8');
        hashMap.put(65304, '8');
        hashMap.put(1640, '8');
        hashMap.put(1784, '8');
        hashMap.put('9', '9');
        hashMap.put(65305, '9');
        hashMap.put(1641, '9');
        hashMap.put(1785, '9');
        DIGIT_MAPPINGS = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap(40);
        hashMap2.put('A', '2');
        hashMap2.put('B', '2');
        hashMap2.put('C', '2');
        hashMap2.put('D', '3');
        hashMap2.put('E', '3');
        hashMap2.put('F', '3');
        hashMap2.put('G', '4');
        hashMap2.put('H', '4');
        hashMap2.put('I', '4');
        hashMap2.put('J', '5');
        hashMap2.put('K', '5');
        hashMap2.put('L', '5');
        hashMap2.put('M', '6');
        hashMap2.put('N', '6');
        hashMap2.put('O', '6');
        hashMap2.put('P', '7');
        hashMap2.put('Q', '7');
        hashMap2.put('R', '7');
        hashMap2.put('S', '7');
        hashMap2.put('T', '8');
        hashMap2.put('U', '8');
        hashMap2.put('V', '8');
        hashMap2.put('W', '9');
        hashMap2.put('X', '9');
        hashMap2.put('Y', '9');
        hashMap2.put('Z', '9');
        ALPHA_MAPPINGS = Collections.unmodifiableMap(hashMap2);
        HashMap hashMap3 = new HashMap(100);
        hashMap3.putAll(hashMap2);
        hashMap3.putAll(hashMap);
        ALL_NORMALIZATION_MAPPINGS = Collections.unmodifiableMap(hashMap3);
        HashSet hashSet = new HashSet(10);
        hashSet.add(39);
        hashSet.add(47);
        hashSet.add(225);
        hashSet.add(227);
        hashSet.add(228);
        hashSet.add(241);
        hashSet.add(242);
        hashSet.add(268);
        hashSet.add(378);
        hashSet.add(379);
        hashSet.add(501);
        LEADING_ZERO_COUNTRIES = Collections.unmodifiableSet(hashSet);
    }

    private PhoneNumberUtil() {
    }

    private boolean checkRegionForParsing(String str, String str2) {
        return isValidRegionCode(str2) || !(str == null || str.length() == 0 || !PLUS_CHARS_PATTERN.matcher(str).lookingAt());
    }

    public static String convertAlphaCharactersInNumber(String str) {
        return normalizeHelper(str, ALL_NORMALIZATION_MAPPINGS, false);
    }

    static String extractPossibleNumber(String str) {
        Matcher matcher = VALID_START_CHAR_PATTERN.matcher(str);
        if (!matcher.find()) {
            return "";
        }
        String substring = str.substring(matcher.start());
        Matcher matcher2 = UNWANTED_END_CHAR_PATTERN.matcher(substring);
        if (matcher2.find()) {
            substring = substring.substring(0, matcher2.start());
        }
        Matcher matcher3 = SECOND_NUMBER_START_PATTERN.matcher(substring);
        return matcher3.find() ? substring.substring(0, matcher3.start()) : substring;
    }

    private String formatAccordingToFormats(String str, List<Phonemetadata.NumberFormat> list, PhoneNumberFormat phoneNumberFormat) {
        return formatAccordingToFormats(str, list, phoneNumberFormat, null);
    }

    private String formatAccordingToFormats(String str, List<Phonemetadata.NumberFormat> list, PhoneNumberFormat phoneNumberFormat, String str2) {
        for (Phonemetadata.NumberFormat next : list) {
            int leadingDigitsPatternCount = next.getLeadingDigitsPatternCount();
            if (leadingDigitsPatternCount == 0 || this.regexCache.getPatternForRegex(next.getLeadingDigitsPattern(leadingDigitsPatternCount - 1)).matcher(str).lookingAt()) {
                Matcher matcher = this.regexCache.getPatternForRegex(next.getPattern()).matcher(str);
                String format = next.getFormat();
                if (matcher.matches()) {
                    String replaceFirst = (str2 == null || str2.length() <= 0 || next.getDomesticCarrierCodeFormattingRule().length() <= 0) ? format : FIRST_GROUP_PATTERN.matcher(format).replaceFirst(CC_PATTERN.matcher(next.getDomesticCarrierCodeFormattingRule()).replaceFirst(str2));
                    String nationalPrefixFormattingRule = next.getNationalPrefixFormattingRule();
                    return (phoneNumberFormat != PhoneNumberFormat.NATIONAL || nationalPrefixFormattingRule == null || nationalPrefixFormattingRule.length() <= 0) ? matcher.replaceAll(replaceFirst) : matcher.replaceAll(FIRST_GROUP_PATTERN.matcher(replaceFirst).replaceFirst(nationalPrefixFormattingRule));
                }
            }
        }
        return str;
    }

    private String formatNationalNumber(String str, String str2, PhoneNumberFormat phoneNumberFormat) {
        return formatNationalNumber(str, str2, phoneNumberFormat, null);
    }

    private String formatNationalNumber(String str, String str2, PhoneNumberFormat phoneNumberFormat, String str3) {
        Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(str2);
        return formatAccordingToFormats(str, (metadataForRegion.getIntlNumberFormatList().size() == 0 || phoneNumberFormat == PhoneNumberFormat.NATIONAL) ? metadataForRegion.getNumberFormatList() : metadataForRegion.getIntlNumberFormatList(), phoneNumberFormat, str3);
    }

    private void formatNumberByFormat(int i, PhoneNumberFormat phoneNumberFormat, StringBuffer stringBuffer) {
        switch (phoneNumberFormat) {
            case E164:
                stringBuffer.insert(0, i).insert(0, (char) PLUS_SIGN);
                return;
            case INTERNATIONAL:
                stringBuffer.insert(0, " ").insert(0, i).insert(0, (char) PLUS_SIGN);
                return;
            default:
                return;
        }
    }

    public static synchronized PhoneNumberUtil getInstance() {
        PhoneNumberUtil instance2;
        synchronized (PhoneNumberUtil.class) {
            instance2 = instance == null ? getInstance(META_DATA_FILE_PREFIX, CountryCodeToRegionCodeMap.getCountryCodeToRegionCodeMap()) : instance;
        }
        return instance2;
    }

    static synchronized PhoneNumberUtil getInstance(String str, Map<Integer, List<String>> map) {
        PhoneNumberUtil phoneNumberUtil;
        synchronized (PhoneNumberUtil.class) {
            if (instance == null) {
                instance = new PhoneNumberUtil();
                instance.countryCodeToRegionCodeMap = map;
                instance.init(str);
            }
            phoneNumberUtil = instance;
        }
        return phoneNumberUtil;
    }

    public static String getNationalSignificantNumber(Phonenumber.PhoneNumber phoneNumber) {
        StringBuffer stringBuffer = new StringBuffer((!phoneNumber.hasItalianLeadingZero() || !phoneNumber.getItalianLeadingZero() || !isLeadingZeroCountry(phoneNumber.getCountryCode())) ? "" : "0");
        stringBuffer.append(phoneNumber.getNationalNumber());
        return stringBuffer.toString();
    }

    private PhoneNumberType getNumberTypeHelper(String str, Phonemetadata.PhoneMetadata phoneMetadata) {
        Phonemetadata.PhoneNumberDesc generalDesc = phoneMetadata.getGeneralDesc();
        return (!generalDesc.hasNationalNumberPattern() || !isNumberMatchingDesc(str, generalDesc)) ? PhoneNumberType.UNKNOWN : isNumberMatchingDesc(str, phoneMetadata.getPremiumRate()) ? PhoneNumberType.PREMIUM_RATE : isNumberMatchingDesc(str, phoneMetadata.getTollFree()) ? PhoneNumberType.TOLL_FREE : isNumberMatchingDesc(str, phoneMetadata.getSharedCost()) ? PhoneNumberType.SHARED_COST : isNumberMatchingDesc(str, phoneMetadata.getVoip()) ? PhoneNumberType.VOIP : isNumberMatchingDesc(str, phoneMetadata.getPersonalNumber()) ? PhoneNumberType.PERSONAL_NUMBER : isNumberMatchingDesc(str, phoneMetadata.getPager()) ? PhoneNumberType.PAGER : isNumberMatchingDesc(str, phoneMetadata.getUan()) ? PhoneNumberType.UAN : isNumberMatchingDesc(str, phoneMetadata.getFixedLine()) ? phoneMetadata.getSameMobileAndFixedLinePattern() ? PhoneNumberType.FIXED_LINE_OR_MOBILE : isNumberMatchingDesc(str, phoneMetadata.getMobile()) ? PhoneNumberType.FIXED_LINE_OR_MOBILE : PhoneNumberType.FIXED_LINE : (phoneMetadata.getSameMobileAndFixedLinePattern() || !isNumberMatchingDesc(str, phoneMetadata.getMobile())) ? PhoneNumberType.UNKNOWN : PhoneNumberType.MOBILE;
    }

    private String getRegionCodeForNumberFromRegionList(Phonenumber.PhoneNumber phoneNumber, List<String> list) {
        String valueOf = String.valueOf(phoneNumber.getNationalNumber());
        for (String next : list) {
            Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(next);
            if (metadataForRegion.hasLeadingDigits()) {
                if (this.regexCache.getPatternForRegex(metadataForRegion.getLeadingDigits()).matcher(valueOf).lookingAt()) {
                    return next;
                }
            } else if (getNumberTypeHelper(valueOf, metadataForRegion) != PhoneNumberType.UNKNOWN) {
                return next;
            }
        }
        return null;
    }

    private void init(String str) {
        this.currentFilePrefix = str;
        for (List<String> addAll : this.countryCodeToRegionCodeMap.values()) {
            this.supportedCountries.addAll(addAll);
        }
        this.nanpaCountries.addAll(this.countryCodeToRegionCodeMap.get(1));
    }

    public static boolean isLeadingZeroCountry(int i) {
        return LEADING_ZERO_COUNTRIES.contains(Integer.valueOf(i));
    }

    private boolean isNationalNumberSuffixOfTheOther(Phonenumber.PhoneNumber phoneNumber, Phonenumber.PhoneNumber phoneNumber2) {
        String valueOf = String.valueOf(phoneNumber.getNationalNumber());
        String valueOf2 = String.valueOf(phoneNumber2.getNationalNumber());
        return valueOf.endsWith(valueOf2) || valueOf2.endsWith(valueOf);
    }

    private boolean isNumberMatchingDesc(String str, Phonemetadata.PhoneNumberDesc phoneNumberDesc) {
        return this.regexCache.getPatternForRegex(phoneNumberDesc.getPossibleNumberPattern()).matcher(str).matches() && this.regexCache.getPatternForRegex(phoneNumberDesc.getNationalNumberPattern()).matcher(str).matches();
    }

    private boolean isValidRegionCode(String str) {
        return str != null && this.supportedCountries.contains(str.toUpperCase());
    }

    static boolean isViablePhoneNumber(String str) {
        if (str.length() < 3) {
            return false;
        }
        return VALID_PHONE_NUMBER_PATTERN.matcher(str).matches();
    }

    private void loadMetadataForRegionFromFile(String str, String str2) {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(PhoneNumberUtil.class.getResourceAsStream(str + "_" + str2));
            Phonemetadata.PhoneMetadataCollection phoneMetadataCollection = new Phonemetadata.PhoneMetadataCollection();
            phoneMetadataCollection.readExternal(objectInputStream);
            for (Phonemetadata.PhoneMetadata put : phoneMetadataCollection.getMetadataList()) {
                this.countryToMetadataMap.put(str2, put);
            }
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, e.toString());
        }
    }

    private void maybeGetFormattedExtension(Phonenumber.PhoneNumber phoneNumber, String str, StringBuffer stringBuffer) {
        if (phoneNumber.hasExtension()) {
            Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(str);
            if (metadataForRegion.hasPreferredExtnPrefix()) {
                stringBuffer.append(metadataForRegion.getPreferredExtnPrefix());
            } else {
                stringBuffer.append(DEFAULT_EXTN_PREFIX);
            }
            stringBuffer.append(phoneNumber.getExtension());
        }
    }

    static String normalize(String str) {
        return VALID_ALPHA_PHONE_PATTERN.matcher(str).matches() ? normalizeHelper(str, ALL_NORMALIZATION_MAPPINGS, true) : normalizeHelper(str, DIGIT_MAPPINGS, true);
    }

    static void normalize(StringBuffer stringBuffer) {
        stringBuffer.replace(0, stringBuffer.length(), normalize(stringBuffer.toString()));
    }

    public static String normalizeDigitsOnly(String str) {
        return normalizeHelper(str, DIGIT_MAPPINGS, true);
    }

    private static String normalizeHelper(String str, Map<Character, Character> map, boolean z) {
        StringBuffer stringBuffer = new StringBuffer(str.length());
        for (char c : str.toCharArray()) {
            Character ch = map.get(Character.valueOf(Character.toUpperCase(c)));
            if (ch != null) {
                stringBuffer.append(ch);
            } else if (!z) {
                stringBuffer.append(c);
            }
        }
        return stringBuffer.toString();
    }

    private void parseHelper(String str, String str2, boolean z, boolean z2, Phonenumber.PhoneNumber phoneNumber) throws NumberParseException {
        Phonemetadata.PhoneMetadata phoneMetadata;
        if (str == null) {
            throw new NumberParseException(NumberParseException.ErrorType.NOT_A_NUMBER, "The phone number supplied was null.");
        }
        String extractPossibleNumber = extractPossibleNumber(str);
        if (!isViablePhoneNumber(extractPossibleNumber)) {
            throw new NumberParseException(NumberParseException.ErrorType.NOT_A_NUMBER, "The string supplied did not seem to be a phone number.");
        } else if (!z2 || checkRegionForParsing(extractPossibleNumber, str2)) {
            if (z) {
                phoneNumber.setRawInput(str);
            }
            StringBuffer stringBuffer = new StringBuffer(extractPossibleNumber);
            String maybeStripExtension = maybeStripExtension(stringBuffer);
            if (maybeStripExtension.length() > 0) {
                phoneNumber.setExtension(maybeStripExtension);
            }
            Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(str2);
            StringBuffer stringBuffer2 = new StringBuffer();
            int maybeExtractCountryCode = maybeExtractCountryCode(stringBuffer.toString(), metadataForRegion, stringBuffer2, z, phoneNumber);
            if (maybeExtractCountryCode != 0) {
                String regionCodeForCountryCode = getRegionCodeForCountryCode(maybeExtractCountryCode);
                if (!regionCodeForCountryCode.equals(str2)) {
                    phoneMetadata = getMetadataForRegion(regionCodeForCountryCode);
                }
                phoneMetadata = metadataForRegion;
            } else {
                normalize(stringBuffer);
                stringBuffer2.append(stringBuffer);
                if (str2 != null) {
                    maybeExtractCountryCode = metadataForRegion.getCountryCode();
                    phoneNumber.setCountryCode(maybeExtractCountryCode);
                    phoneMetadata = metadataForRegion;
                } else {
                    if (z) {
                        phoneNumber.clearCountryCodeSource();
                    }
                    phoneMetadata = metadataForRegion;
                }
            }
            if (stringBuffer2.length() < 3) {
                throw new NumberParseException(NumberParseException.ErrorType.TOO_SHORT_NSN, "The string supplied is too short to be a phone number.");
            }
            if (phoneMetadata != null) {
                maybeStripNationalPrefix(stringBuffer2, phoneMetadata.getNationalPrefixForParsing(), phoneMetadata.getNationalPrefixTransformRule(), this.regexCache.getPatternForRegex(phoneMetadata.getGeneralDesc().getNationalNumberPattern()));
            }
            int length = stringBuffer2.length();
            if (length < 3) {
                throw new NumberParseException(NumberParseException.ErrorType.TOO_SHORT_NSN, "The string supplied is too short to be a phone number.");
            } else if (length > MAX_LENGTH_FOR_NSN) {
                throw new NumberParseException(NumberParseException.ErrorType.TOO_LONG, "The string supplied is too long to be a phone number.");
            } else {
                if (stringBuffer2.charAt(0) == '0' && isLeadingZeroCountry(maybeExtractCountryCode)) {
                    phoneNumber.setItalianLeadingZero(true);
                }
                phoneNumber.setNationalNumber(Long.parseLong(stringBuffer2.toString()));
            }
        } else {
            throw new NumberParseException(NumberParseException.ErrorType.INVALID_COUNTRY_CODE, "Missing or invalid default country.");
        }
    }

    private boolean parsePrefixAsIdd(Pattern pattern, StringBuffer stringBuffer) {
        Matcher matcher = pattern.matcher(stringBuffer);
        if (!matcher.lookingAt()) {
            return false;
        }
        int end = matcher.end();
        Matcher matcher2 = CAPTURING_DIGIT_PATTERN.matcher(stringBuffer.substring(end));
        if (matcher2.find() && normalizeHelper(matcher2.group(1), DIGIT_MAPPINGS, true).equals("0")) {
            return false;
        }
        stringBuffer.delete(0, end);
        return true;
    }

    static synchronized void resetInstance() {
        synchronized (PhoneNumberUtil.class) {
            instance = null;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean canBeInternationallyDialled(Phonenumber.PhoneNumber phoneNumber) {
        String regionCodeForNumber = getRegionCodeForNumber(phoneNumber);
        String nationalSignificantNumber = getNationalSignificantNumber(phoneNumber);
        if (!isValidRegionCode(regionCodeForNumber)) {
            return true;
        }
        return !isNumberMatchingDesc(nationalSignificantNumber, getMetadataForRegion(regionCodeForNumber).getNoInternationalDialling());
    }

    /* access modifiers changed from: package-private */
    public int extractCountryCode(StringBuffer stringBuffer, StringBuffer stringBuffer2) {
        int length = stringBuffer.length();
        int i = 1;
        while (i <= 3 && i <= length) {
            int parseInt = Integer.parseInt(stringBuffer.substring(0, i));
            if (this.countryCodeToRegionCodeMap.containsKey(Integer.valueOf(parseInt))) {
                stringBuffer2.append(stringBuffer.substring(i));
                return parseInt;
            }
            i++;
        }
        return 0;
    }

    public String format(Phonenumber.PhoneNumber phoneNumber, PhoneNumberFormat phoneNumberFormat) {
        StringBuffer stringBuffer = new StringBuffer(20);
        format(phoneNumber, phoneNumberFormat, stringBuffer);
        return stringBuffer.toString();
    }

    public void format(Phonenumber.PhoneNumber phoneNumber, PhoneNumberFormat phoneNumberFormat, StringBuffer stringBuffer) {
        stringBuffer.setLength(0);
        int countryCode = phoneNumber.getCountryCode();
        String nationalSignificantNumber = getNationalSignificantNumber(phoneNumber);
        if (phoneNumberFormat == PhoneNumberFormat.E164) {
            stringBuffer.append(nationalSignificantNumber);
            formatNumberByFormat(countryCode, PhoneNumberFormat.E164, stringBuffer);
            return;
        }
        String regionCodeForCountryCode = getRegionCodeForCountryCode(countryCode);
        if (!isValidRegionCode(regionCodeForCountryCode)) {
            stringBuffer.append(nationalSignificantNumber);
            return;
        }
        stringBuffer.append(formatNationalNumber(nationalSignificantNumber, regionCodeForCountryCode, phoneNumberFormat));
        maybeGetFormattedExtension(phoneNumber, regionCodeForCountryCode, stringBuffer);
        formatNumberByFormat(countryCode, phoneNumberFormat, stringBuffer);
    }

    public String formatByPattern(Phonenumber.PhoneNumber phoneNumber, PhoneNumberFormat phoneNumberFormat, List<Phonemetadata.NumberFormat> list) {
        int countryCode = phoneNumber.getCountryCode();
        String nationalSignificantNumber = getNationalSignificantNumber(phoneNumber);
        String regionCodeForCountryCode = getRegionCodeForCountryCode(countryCode);
        if (!isValidRegionCode(regionCodeForCountryCode)) {
            return nationalSignificantNumber;
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (Phonemetadata.NumberFormat next : list) {
            String nationalPrefixFormattingRule = next.getNationalPrefixFormattingRule();
            if (nationalPrefixFormattingRule.length() > 0) {
                Phonemetadata.NumberFormat numberFormat = new Phonemetadata.NumberFormat();
                numberFormat.mergeFrom(next);
                String nationalPrefix = getMetadataForRegion(regionCodeForCountryCode).getNationalPrefix();
                if (nationalPrefix.length() > 0) {
                    numberFormat.setNationalPrefixFormattingRule(FG_PATTERN.matcher(NP_PATTERN.matcher(nationalPrefixFormattingRule).replaceFirst(nationalPrefix)).replaceFirst("\\$1"));
                } else {
                    numberFormat.clearNationalPrefixFormattingRule();
                }
                arrayList.add(numberFormat);
            } else {
                arrayList.add(next);
            }
        }
        StringBuffer stringBuffer = new StringBuffer(formatAccordingToFormats(nationalSignificantNumber, arrayList, phoneNumberFormat));
        maybeGetFormattedExtension(phoneNumber, regionCodeForCountryCode, stringBuffer);
        formatNumberByFormat(countryCode, phoneNumberFormat, stringBuffer);
        return stringBuffer.toString();
    }

    public String formatInOriginalFormat(Phonenumber.PhoneNumber phoneNumber, String str) {
        if (!phoneNumber.hasCountryCodeSource()) {
            return format(phoneNumber, PhoneNumberFormat.NATIONAL);
        }
        switch (phoneNumber.getCountryCodeSource()) {
            case FROM_NUMBER_WITH_PLUS_SIGN:
                return format(phoneNumber, PhoneNumberFormat.INTERNATIONAL);
            case FROM_NUMBER_WITH_IDD:
                return formatOutOfCountryCallingNumber(phoneNumber, str);
            case FROM_NUMBER_WITHOUT_PLUS_SIGN:
                return format(phoneNumber, PhoneNumberFormat.INTERNATIONAL).substring(1);
            default:
                return format(phoneNumber, PhoneNumberFormat.NATIONAL);
        }
    }

    public String formatNationalNumberWithCarrierCode(Phonenumber.PhoneNumber phoneNumber, String str) {
        int countryCode = phoneNumber.getCountryCode();
        String nationalSignificantNumber = getNationalSignificantNumber(phoneNumber);
        String regionCodeForCountryCode = getRegionCodeForCountryCode(countryCode);
        if (!isValidRegionCode(regionCodeForCountryCode)) {
            return nationalSignificantNumber;
        }
        StringBuffer stringBuffer = new StringBuffer(20);
        stringBuffer.append(formatNationalNumber(nationalSignificantNumber, regionCodeForCountryCode, PhoneNumberFormat.NATIONAL, str));
        maybeGetFormattedExtension(phoneNumber, regionCodeForCountryCode, stringBuffer);
        formatNumberByFormat(countryCode, PhoneNumberFormat.NATIONAL, stringBuffer);
        return stringBuffer.toString();
    }

    public String formatOutOfCountryCallingNumber(Phonenumber.PhoneNumber phoneNumber, String str) {
        if (!isValidRegionCode(str)) {
            return format(phoneNumber, PhoneNumberFormat.INTERNATIONAL);
        }
        int countryCode = phoneNumber.getCountryCode();
        String regionCodeForCountryCode = getRegionCodeForCountryCode(countryCode);
        String nationalSignificantNumber = getNationalSignificantNumber(phoneNumber);
        if (!isValidRegionCode(regionCodeForCountryCode)) {
            return nationalSignificantNumber;
        }
        if (countryCode == 1) {
            if (isNANPACountry(str)) {
                return countryCode + " " + format(phoneNumber, PhoneNumberFormat.NATIONAL);
            }
        } else if (countryCode == getCountryCodeForRegion(str)) {
            return format(phoneNumber, PhoneNumberFormat.NATIONAL);
        }
        String formatNationalNumber = formatNationalNumber(nationalSignificantNumber, regionCodeForCountryCode, PhoneNumberFormat.INTERNATIONAL);
        Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(str);
        String internationalPrefix = metadataForRegion.getInternationalPrefix();
        String preferredInternationalPrefix = UNIQUE_INTERNATIONAL_PREFIX.matcher(internationalPrefix).matches() ? internationalPrefix : metadataForRegion.hasPreferredInternationalPrefix() ? metadataForRegion.getPreferredInternationalPrefix() : "";
        StringBuffer stringBuffer = new StringBuffer(formatNationalNumber);
        maybeGetFormattedExtension(phoneNumber, regionCodeForCountryCode, stringBuffer);
        if (preferredInternationalPrefix.length() > 0) {
            stringBuffer.insert(0, " ").insert(0, countryCode).insert(0, " ").insert(0, preferredInternationalPrefix);
        } else {
            formatNumberByFormat(countryCode, PhoneNumberFormat.INTERNATIONAL, stringBuffer);
        }
        return stringBuffer.toString();
    }

    public AsYouTypeFormatter getAsYouTypeFormatter(String str) {
        return new AsYouTypeFormatter(str);
    }

    public int getCountryCodeForRegion(String str) {
        if (!isValidRegionCode(str)) {
            return 0;
        }
        Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(str);
        if (metadataForRegion == null) {
            return 0;
        }
        return metadataForRegion.getCountryCode();
    }

    public Phonenumber.PhoneNumber getExampleNumber(String str) {
        return getExampleNumberForType(str, PhoneNumberType.FIXED_LINE);
    }

    public Phonenumber.PhoneNumber getExampleNumberForType(String str, PhoneNumberType phoneNumberType) {
        Phonemetadata.PhoneNumberDesc numberDescByType = getNumberDescByType(getMetadataForRegion(str), phoneNumberType);
        try {
            if (numberDescByType.hasExampleNumber()) {
                return parse(numberDescByType.getExampleNumber(), str);
            }
        } catch (NumberParseException e) {
            LOGGER.log(Level.SEVERE, e.toString());
        }
        return null;
    }

    public int getLengthOfGeographicalAreaCode(Phonenumber.PhoneNumber phoneNumber) {
        String regionCodeForNumber = getRegionCodeForNumber(phoneNumber);
        if (!isValidRegionCode(regionCodeForNumber)) {
            return 0;
        }
        Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(regionCodeForNumber);
        if (!metadataForRegion.hasNationalPrefix()) {
            return 0;
        }
        PhoneNumberType numberTypeHelper = getNumberTypeHelper(getNationalSignificantNumber(phoneNumber), metadataForRegion);
        if (numberTypeHelper == PhoneNumberType.FIXED_LINE || numberTypeHelper == PhoneNumberType.FIXED_LINE_OR_MOBILE) {
            return getLengthOfNationalDestinationCode(phoneNumber);
        }
        return 0;
    }

    public int getLengthOfNationalDestinationCode(Phonenumber.PhoneNumber phoneNumber) {
        Phonenumber.PhoneNumber phoneNumber2;
        if (phoneNumber.hasExtension()) {
            phoneNumber2 = new Phonenumber.PhoneNumber();
            phoneNumber2.mergeFrom(phoneNumber);
            phoneNumber2.clearExtension();
        } else {
            phoneNumber2 = phoneNumber;
        }
        String[] split = NON_DIGITS_PATTERN.split(format(phoneNumber2, PhoneNumberFormat.INTERNATIONAL));
        if (split.length <= 3) {
            return 0;
        }
        return (!getRegionCodeForNumber(phoneNumber).equals("AR") || getNumberType(phoneNumber) != PhoneNumberType.MOBILE) ? split[2].length() : split[3].length() + 1;
    }

    /* access modifiers changed from: package-private */
    public Phonemetadata.PhoneMetadata getMetadataForRegion(String str) {
        if (!isValidRegionCode(str)) {
            return null;
        }
        String upperCase = str.toUpperCase();
        if (!this.countryToMetadataMap.containsKey(upperCase)) {
            loadMetadataForRegionFromFile(this.currentFilePrefix, upperCase);
        }
        return this.countryToMetadataMap.get(upperCase);
    }

    public String getNddPrefixForRegion(String str, boolean z) {
        if (!isValidRegionCode(str)) {
            LOGGER.log(Level.SEVERE, "Invalid or missing country code provided.");
            return null;
        }
        Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(str);
        if (metadataForRegion == null) {
            LOGGER.log(Level.SEVERE, "Unsupported country code provided.");
            return null;
        }
        String nationalPrefix = metadataForRegion.getNationalPrefix();
        if (nationalPrefix.length() == 0) {
            return null;
        }
        return z ? nationalPrefix.replace("~", "") : nationalPrefix;
    }

    /* access modifiers changed from: package-private */
    public Phonemetadata.PhoneNumberDesc getNumberDescByType(Phonemetadata.PhoneMetadata phoneMetadata, PhoneNumberType phoneNumberType) {
        switch (phoneNumberType) {
            case PREMIUM_RATE:
                return phoneMetadata.getPremiumRate();
            case TOLL_FREE:
                return phoneMetadata.getTollFree();
            case MOBILE:
                return phoneMetadata.getMobile();
            case FIXED_LINE:
            case FIXED_LINE_OR_MOBILE:
                return phoneMetadata.getFixedLine();
            case SHARED_COST:
                return phoneMetadata.getSharedCost();
            case VOIP:
                return phoneMetadata.getVoip();
            case PERSONAL_NUMBER:
                return phoneMetadata.getPersonalNumber();
            case PAGER:
                return phoneMetadata.getPager();
            case UAN:
                return phoneMetadata.getUan();
            default:
                return phoneMetadata.getGeneralDesc();
        }
    }

    public PhoneNumberType getNumberType(Phonenumber.PhoneNumber phoneNumber) {
        String regionCodeForNumber = getRegionCodeForNumber(phoneNumber);
        return !isValidRegionCode(regionCodeForNumber) ? PhoneNumberType.UNKNOWN : getNumberTypeHelper(getNationalSignificantNumber(phoneNumber), getMetadataForRegion(regionCodeForNumber));
    }

    public String getRegionCodeForCountryCode(int i) {
        List list = this.countryCodeToRegionCodeMap.get(Integer.valueOf(i));
        return list == null ? UNKNOWN_REGION : (String) list.get(0);
    }

    public String getRegionCodeForNumber(Phonenumber.PhoneNumber phoneNumber) {
        List list = this.countryCodeToRegionCodeMap.get(Integer.valueOf(phoneNumber.getCountryCode()));
        if (list == null) {
            return null;
        }
        return list.size() == 1 ? (String) list.get(0) : getRegionCodeForNumberFromRegionList(phoneNumber, list);
    }

    public Set<String> getSupportedCountries() {
        return this.supportedCountries;
    }

    public boolean isNANPACountry(String str) {
        return str != null && this.nanpaCountries.contains(str.toUpperCase());
    }

    public MatchType isNumberMatch(Phonenumber.PhoneNumber phoneNumber, Phonenumber.PhoneNumber phoneNumber2) {
        Phonenumber.PhoneNumber phoneNumber3 = new Phonenumber.PhoneNumber();
        phoneNumber3.mergeFrom(phoneNumber);
        Phonenumber.PhoneNumber phoneNumber4 = new Phonenumber.PhoneNumber();
        phoneNumber4.mergeFrom(phoneNumber2);
        phoneNumber3.clearRawInput();
        phoneNumber3.clearCountryCodeSource();
        phoneNumber4.clearRawInput();
        phoneNumber4.clearCountryCodeSource();
        if (phoneNumber3.hasExtension() && phoneNumber3.getExtension().length() == 0) {
            phoneNumber3.clearExtension();
        }
        if (phoneNumber4.hasExtension() && phoneNumber4.getExtension().length() == 0) {
            phoneNumber4.clearExtension();
        }
        if (phoneNumber3.hasExtension() && phoneNumber4.hasExtension() && !phoneNumber3.getExtension().equals(phoneNumber4.getExtension())) {
            return MatchType.NO_MATCH;
        }
        int countryCode = phoneNumber3.getCountryCode();
        int countryCode2 = phoneNumber4.getCountryCode();
        if (countryCode != 0 && countryCode2 != 0) {
            return phoneNumber3.exactlySameAs(phoneNumber4) ? MatchType.EXACT_MATCH : (countryCode != countryCode2 || !isNationalNumberSuffixOfTheOther(phoneNumber3, phoneNumber4)) ? MatchType.NO_MATCH : MatchType.SHORT_NSN_MATCH;
        }
        phoneNumber3.setCountryCode(countryCode2);
        return phoneNumber3.exactlySameAs(phoneNumber4) ? MatchType.NSN_MATCH : isNationalNumberSuffixOfTheOther(phoneNumber3, phoneNumber4) ? MatchType.SHORT_NSN_MATCH : MatchType.NO_MATCH;
    }

    public MatchType isNumberMatch(Phonenumber.PhoneNumber phoneNumber, String str) {
        try {
            return isNumberMatch(phoneNumber, parse(str, UNKNOWN_REGION));
        } catch (NumberParseException e) {
            if (e.getErrorType() == NumberParseException.ErrorType.INVALID_COUNTRY_CODE) {
                String regionCodeForCountryCode = getRegionCodeForCountryCode(phoneNumber.getCountryCode());
                try {
                    if (!regionCodeForCountryCode.equals(UNKNOWN_REGION)) {
                        MatchType isNumberMatch = isNumberMatch(phoneNumber, parse(str, regionCodeForCountryCode));
                        return isNumberMatch == MatchType.EXACT_MATCH ? MatchType.NSN_MATCH : isNumberMatch;
                    }
                    Phonenumber.PhoneNumber phoneNumber2 = new Phonenumber.PhoneNumber();
                    parseHelper(str, null, false, false, phoneNumber2);
                    return isNumberMatch(phoneNumber, phoneNumber2);
                } catch (NumberParseException e2) {
                    return MatchType.NOT_A_NUMBER;
                }
            }
            return MatchType.NOT_A_NUMBER;
        }
    }

    public MatchType isNumberMatch(String str, String str2) {
        try {
            return isNumberMatch(parse(str, UNKNOWN_REGION), str2);
        } catch (NumberParseException e) {
            if (e.getErrorType() == NumberParseException.ErrorType.INVALID_COUNTRY_CODE) {
                try {
                    return isNumberMatch(parse(str2, UNKNOWN_REGION), str);
                } catch (NumberParseException e2) {
                    if (e2.getErrorType() == NumberParseException.ErrorType.INVALID_COUNTRY_CODE) {
                        try {
                            Phonenumber.PhoneNumber phoneNumber = new Phonenumber.PhoneNumber();
                            Phonenumber.PhoneNumber phoneNumber2 = new Phonenumber.PhoneNumber();
                            parseHelper(str, null, false, false, phoneNumber);
                            parseHelper(str2, null, false, false, phoneNumber2);
                            return isNumberMatch(phoneNumber, phoneNumber2);
                        } catch (NumberParseException e3) {
                        }
                    }
                }
            }
            return MatchType.NOT_A_NUMBER;
        }
    }

    public boolean isPossibleNumber(Phonenumber.PhoneNumber phoneNumber) {
        return isPossibleNumberWithReason(phoneNumber) == ValidationResult.IS_POSSIBLE;
    }

    public boolean isPossibleNumber(String str, String str2) {
        try {
            return isPossibleNumber(parse(str, str2));
        } catch (NumberParseException e) {
            return false;
        }
    }

    public ValidationResult isPossibleNumberWithReason(Phonenumber.PhoneNumber phoneNumber) {
        String regionCodeForCountryCode = getRegionCodeForCountryCode(phoneNumber.getCountryCode());
        if (!isValidRegionCode(regionCodeForCountryCode)) {
            return ValidationResult.INVALID_COUNTRY_CODE;
        }
        String nationalSignificantNumber = getNationalSignificantNumber(phoneNumber);
        Phonemetadata.PhoneNumberDesc generalDesc = getMetadataForRegion(regionCodeForCountryCode).getGeneralDesc();
        if (!generalDesc.hasNationalNumberPattern()) {
            LOGGER.log(Level.FINER, "Checking if number is possible with incomplete metadata.");
            int length = nationalSignificantNumber.length();
            return length < 3 ? ValidationResult.TOO_SHORT : length > MAX_LENGTH_FOR_NSN ? ValidationResult.TOO_LONG : ValidationResult.IS_POSSIBLE;
        }
        Matcher matcher = this.regexCache.getPatternForRegex(generalDesc.getPossibleNumberPattern()).matcher(nationalSignificantNumber);
        return matcher.lookingAt() ? matcher.end() == nationalSignificantNumber.length() ? ValidationResult.IS_POSSIBLE : ValidationResult.TOO_LONG : ValidationResult.TOO_SHORT;
    }

    public boolean isValidNumber(Phonenumber.PhoneNumber phoneNumber) {
        String regionCodeForNumber = getRegionCodeForNumber(phoneNumber);
        return isValidRegionCode(regionCodeForNumber) && isValidNumberForRegion(phoneNumber, regionCodeForNumber);
    }

    public boolean isValidNumberForRegion(Phonenumber.PhoneNumber phoneNumber, String str) {
        if (phoneNumber.getCountryCode() != getCountryCodeForRegion(str)) {
            return false;
        }
        Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(str);
        Phonemetadata.PhoneNumberDesc generalDesc = metadataForRegion.getGeneralDesc();
        String nationalSignificantNumber = getNationalSignificantNumber(phoneNumber);
        if (generalDesc.hasNationalNumberPattern()) {
            return getNumberTypeHelper(nationalSignificantNumber, metadataForRegion) != PhoneNumberType.UNKNOWN;
        }
        int length = nationalSignificantNumber.length();
        return length > 3 && length <= MAX_LENGTH_FOR_NSN;
    }

    /* access modifiers changed from: package-private */
    public int maybeExtractCountryCode(String str, Phonemetadata.PhoneMetadata phoneMetadata, StringBuffer stringBuffer, boolean z, Phonenumber.PhoneNumber phoneNumber) throws NumberParseException {
        if (str.length() == 0) {
            return 0;
        }
        StringBuffer stringBuffer2 = new StringBuffer(str);
        String str2 = "NonMatch";
        if (phoneMetadata != null) {
            str2 = phoneMetadata.getInternationalPrefix();
        }
        Phonenumber.PhoneNumber.CountryCodeSource maybeStripInternationalPrefixAndNormalize = maybeStripInternationalPrefixAndNormalize(stringBuffer2, str2);
        if (z) {
            phoneNumber.setCountryCodeSource(maybeStripInternationalPrefixAndNormalize);
        }
        if (maybeStripInternationalPrefixAndNormalize == Phonenumber.PhoneNumber.CountryCodeSource.FROM_DEFAULT_COUNTRY) {
            if (phoneMetadata != null) {
                Phonemetadata.PhoneNumberDesc generalDesc = phoneMetadata.getGeneralDesc();
                Pattern patternForRegex = this.regexCache.getPatternForRegex(generalDesc.getNationalNumberPattern());
                if (!patternForRegex.matcher(stringBuffer2).matches()) {
                    int countryCode = phoneMetadata.getCountryCode();
                    String valueOf = String.valueOf(countryCode);
                    String stringBuffer3 = stringBuffer2.toString();
                    if (stringBuffer3.startsWith(valueOf)) {
                        StringBuffer stringBuffer4 = new StringBuffer(stringBuffer3.substring(valueOf.length()));
                        maybeStripNationalPrefix(stringBuffer4, phoneMetadata.getNationalPrefixForParsing(), phoneMetadata.getNationalPrefixTransformRule(), patternForRegex);
                        Matcher matcher = this.regexCache.getPatternForRegex(generalDesc.getPossibleNumberPattern()).matcher(stringBuffer4);
                        if (patternForRegex.matcher(stringBuffer4).matches() || (matcher.lookingAt() && matcher.end() != stringBuffer4.length())) {
                            stringBuffer.append(stringBuffer4);
                            if (z) {
                                phoneNumber.setCountryCodeSource(Phonenumber.PhoneNumber.CountryCodeSource.FROM_NUMBER_WITHOUT_PLUS_SIGN);
                            }
                            phoneNumber.setCountryCode(countryCode);
                            return countryCode;
                        }
                    }
                }
            }
            phoneNumber.setCountryCode(0);
            return 0;
        } else if (stringBuffer2.length() < 3) {
            throw new NumberParseException(NumberParseException.ErrorType.TOO_SHORT_AFTER_IDD, "Phone number had an IDD, but after this was not long enough to be a viable phone number.");
        } else {
            int extractCountryCode = extractCountryCode(stringBuffer2, stringBuffer);
            if (extractCountryCode != 0) {
                phoneNumber.setCountryCode(extractCountryCode);
                return extractCountryCode;
            }
            throw new NumberParseException(NumberParseException.ErrorType.INVALID_COUNTRY_CODE, "Country code supplied was not recognised.");
        }
    }

    /* access modifiers changed from: package-private */
    public String maybeStripExtension(StringBuffer stringBuffer) {
        Matcher matcher = EXTN_PATTERN.matcher(stringBuffer);
        if (matcher.find() && isViablePhoneNumber(stringBuffer.substring(0, matcher.start()))) {
            int groupCount = matcher.groupCount();
            for (int i = 1; i <= groupCount; i++) {
                if (matcher.group(i) != null) {
                    String group = matcher.group(i);
                    stringBuffer.delete(matcher.start(), stringBuffer.length());
                    return group;
                }
            }
        }
        return "";
    }

    /* access modifiers changed from: package-private */
    public Phonenumber.PhoneNumber.CountryCodeSource maybeStripInternationalPrefixAndNormalize(StringBuffer stringBuffer, String str) {
        if (stringBuffer.length() == 0) {
            return Phonenumber.PhoneNumber.CountryCodeSource.FROM_DEFAULT_COUNTRY;
        }
        Matcher matcher = PLUS_CHARS_PATTERN.matcher(stringBuffer);
        if (matcher.lookingAt()) {
            stringBuffer.delete(0, matcher.end());
            normalize(stringBuffer);
            return Phonenumber.PhoneNumber.CountryCodeSource.FROM_NUMBER_WITH_PLUS_SIGN;
        }
        Pattern patternForRegex = this.regexCache.getPatternForRegex(str);
        if (parsePrefixAsIdd(patternForRegex, stringBuffer)) {
            normalize(stringBuffer);
            return Phonenumber.PhoneNumber.CountryCodeSource.FROM_NUMBER_WITH_IDD;
        }
        normalize(stringBuffer);
        return parsePrefixAsIdd(patternForRegex, stringBuffer) ? Phonenumber.PhoneNumber.CountryCodeSource.FROM_NUMBER_WITH_IDD : Phonenumber.PhoneNumber.CountryCodeSource.FROM_DEFAULT_COUNTRY;
    }

    /* access modifiers changed from: package-private */
    public void maybeStripNationalPrefix(StringBuffer stringBuffer, String str, String str2, Pattern pattern) {
        int length = stringBuffer.length();
        if (length != 0 && str.length() != 0) {
            Matcher matcher = this.regexCache.getPatternForRegex(str).matcher(stringBuffer);
            if (!matcher.lookingAt()) {
                return;
            }
            if (str2 != null && str2.length() != 0 && matcher.group(1) != null) {
                StringBuffer stringBuffer2 = new StringBuffer(stringBuffer);
                stringBuffer2.replace(0, length, matcher.replaceFirst(str2));
                if (pattern.matcher(stringBuffer2.toString()).matches()) {
                    stringBuffer.replace(0, stringBuffer.length(), stringBuffer2.toString());
                }
            } else if (pattern.matcher(stringBuffer.substring(matcher.end())).matches()) {
                stringBuffer.delete(0, matcher.end());
            }
        }
    }

    public Phonenumber.PhoneNumber parse(String str, String str2) throws NumberParseException {
        Phonenumber.PhoneNumber phoneNumber = new Phonenumber.PhoneNumber();
        parse(str, str2, phoneNumber);
        return phoneNumber;
    }

    public void parse(String str, String str2, Phonenumber.PhoneNumber phoneNumber) throws NumberParseException {
        parseHelper(str, str2, false, true, phoneNumber);
    }

    public Phonenumber.PhoneNumber parseAndKeepRawInput(String str, String str2) throws NumberParseException {
        Phonenumber.PhoneNumber phoneNumber = new Phonenumber.PhoneNumber();
        parseAndKeepRawInput(str, str2, phoneNumber);
        return phoneNumber;
    }

    public void parseAndKeepRawInput(String str, String str2, Phonenumber.PhoneNumber phoneNumber) throws NumberParseException {
        parseHelper(str, str2, true, true, phoneNumber);
    }

    public boolean truncateTooLongNumber(Phonenumber.PhoneNumber phoneNumber) {
        if (isValidNumber(phoneNumber)) {
            return true;
        }
        Phonenumber.PhoneNumber phoneNumber2 = new Phonenumber.PhoneNumber();
        phoneNumber2.mergeFrom(phoneNumber);
        long nationalNumber = phoneNumber.getNationalNumber();
        do {
            nationalNumber /= 10;
            phoneNumber2.setNationalNumber(nationalNumber);
            if (isPossibleNumberWithReason(phoneNumber2) == ValidationResult.TOO_SHORT || nationalNumber == 0) {
                return false;
            }
        } while (!isValidNumber(phoneNumber2));
        phoneNumber.setNationalNumber(nationalNumber);
        return true;
    }
}
