package com.google.i18n.phonenumbers;

import com.google.i18n.phonenumbers.Phonemetadata;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AsYouTypeFormatter {
    private static final int MIN_LEADING_DIGITS_LENGTH = 3;
    private static final Pattern STANDALONE_DIGIT_PATTERN = Pattern.compile("\\d(?=[^,}][^,}])");
    private final Pattern CHARACTER_CLASS_PATTERN = Pattern.compile("\\[([^\\[\\]])*\\]");
    private boolean ableToFormat = true;
    private StringBuffer accruedInput = new StringBuffer();
    private StringBuffer accruedInputWithoutFormatting = new StringBuffer();
    private String currentFormattingPattern = "";
    private Phonemetadata.PhoneMetadata currentMetaData;
    private String currentOutput = "";
    private String defaultCountry;
    private Phonemetadata.PhoneMetadata defaultMetaData;
    private Pattern digitPattern = Pattern.compile(this.digitPlaceholder);
    private String digitPlaceholder = " ";
    private StringBuffer formattingTemplate = new StringBuffer();
    private Pattern internationalPrefix;
    private boolean isExpectingCountryCode = false;
    private boolean isInternationalFormatting = false;
    private int lastMatchPosition = 0;
    private StringBuffer nationalNumber = new StringBuffer();
    private Pattern nationalPrefixForParsing;
    private int originalPosition = 0;
    private final PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    private int positionToRemember = 0;
    private List<Phonemetadata.NumberFormat> possibleFormats = new ArrayList();
    private StringBuffer prefixBeforeNationalNumber = new StringBuffer();
    private RegexCache regexCache = new RegexCache(64);

    AsYouTypeFormatter(String str) {
        this.defaultCountry = str;
        initializeCountrySpecificInfo(this.defaultCountry);
        this.defaultMetaData = this.currentMetaData;
    }

    private String attemptToChooseFormattingPattern() {
        if (this.nationalNumber.length() < 3) {
            return ((Object) this.prefixBeforeNationalNumber) + this.nationalNumber.toString();
        }
        getAvailableFormats(this.nationalNumber.substring(0, 3));
        maybeCreateNewTemplate();
        return inputAccruedNationalNumber();
    }

    private boolean attemptToExtractCountryCode() {
        if (this.nationalNumber.length() == 0) {
            return false;
        }
        StringBuffer stringBuffer = new StringBuffer();
        int extractCountryCode = this.phoneUtil.extractCountryCode(this.nationalNumber, stringBuffer);
        if (extractCountryCode == 0) {
            return false;
        }
        this.nationalNumber.setLength(0);
        this.nationalNumber.append(stringBuffer);
        String regionCodeForCountryCode = this.phoneUtil.getRegionCodeForCountryCode(extractCountryCode);
        if (!regionCodeForCountryCode.equals(this.defaultCountry)) {
            initializeCountrySpecificInfo(regionCodeForCountryCode);
        }
        this.prefixBeforeNationalNumber.append(Integer.toString(extractCountryCode)).append(" ");
        return true;
    }

    private boolean attemptToExtractIdd() {
        Matcher matcher = this.internationalPrefix.matcher(this.accruedInputWithoutFormatting);
        if (!matcher.lookingAt()) {
            return false;
        }
        this.isInternationalFormatting = true;
        int end = matcher.end();
        this.nationalNumber.setLength(0);
        this.nationalNumber.append(this.accruedInputWithoutFormatting.substring(end));
        this.prefixBeforeNationalNumber.append(this.accruedInputWithoutFormatting.substring(0, end));
        if (this.accruedInputWithoutFormatting.charAt(0) != '+') {
            this.prefixBeforeNationalNumber.append(" ");
        }
        return true;
    }

    private boolean createFormattingTemplate(Phonemetadata.NumberFormat numberFormat) {
        String format = numberFormat.getFormat();
        String pattern = numberFormat.getPattern();
        if (pattern.indexOf(124) != -1) {
            return false;
        }
        String replaceAll = STANDALONE_DIGIT_PATTERN.matcher(this.CHARACTER_CLASS_PATTERN.matcher(pattern).replaceAll("\\\\d")).replaceAll("\\\\d");
        this.formattingTemplate.setLength(0);
        this.formattingTemplate.append(getFormattingTemplate(replaceAll, format));
        return true;
    }

    private void getAvailableFormats(String str) {
        this.possibleFormats.addAll((!this.isInternationalFormatting || this.currentMetaData.getIntlNumberFormatCount() <= 0) ? this.currentMetaData.getNumberFormatList() : this.currentMetaData.getIntlNumberFormatList());
        narrowDownPossibleFormats(str);
    }

    private String getFormattingTemplate(String str, String str2) {
        Matcher matcher = this.regexCache.getPatternForRegex(str).matcher("999999999999999");
        matcher.find();
        return matcher.group().replaceAll(str, str2).replaceAll("9", this.digitPlaceholder);
    }

    private void initializeCountrySpecificInfo(String str) {
        this.currentMetaData = this.phoneUtil.getMetadataForRegion(str);
        this.nationalPrefixForParsing = this.regexCache.getPatternForRegex(this.currentMetaData.getNationalPrefixForParsing());
        this.internationalPrefix = this.regexCache.getPatternForRegex("\\+|" + this.currentMetaData.getInternationalPrefix());
    }

    private String inputAccruedNationalNumber() {
        int length = this.nationalNumber.length();
        if (length <= 0) {
            return this.prefixBeforeNationalNumber.toString();
        }
        String str = "";
        for (int i = 0; i < length; i++) {
            str = inputDigitHelper(this.nationalNumber.charAt(i));
        }
        return this.ableToFormat ? ((Object) this.prefixBeforeNationalNumber) + str : str;
    }

    private String inputDigitHelper(char c) {
        Matcher matcher = this.digitPattern.matcher(this.formattingTemplate);
        if (matcher.find(this.lastMatchPosition)) {
            String replaceFirst = matcher.replaceFirst(Character.toString(c));
            this.formattingTemplate.replace(0, replaceFirst.length(), replaceFirst);
            this.lastMatchPosition = matcher.start();
            return this.formattingTemplate.substring(0, this.lastMatchPosition + 1);
        }
        this.ableToFormat = false;
        return this.accruedInput.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00d1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String inputDigitWithOptionToRememberPosition(char r4, boolean r5) {
        /*
            r3 = this;
            r2 = 0
            java.lang.StringBuffer r0 = r3.accruedInput
            r0.append(r4)
            if (r5 == 0) goto L_0x0010
            java.lang.StringBuffer r0 = r3.accruedInput
            int r0 = r0.length()
            r3.originalPosition = r0
        L_0x0010:
            java.util.regex.Pattern r0 = com.google.i18n.phonenumbers.PhoneNumberUtil.VALID_START_CHAR_PATTERN
            java.lang.String r1 = java.lang.Character.toString(r4)
            java.util.regex.Matcher r0 = r0.matcher(r1)
            boolean r0 = r0.matches()
            if (r0 != 0) goto L_0x0022
            r3.ableToFormat = r2
        L_0x0022:
            boolean r0 = r3.ableToFormat
            if (r0 != 0) goto L_0x002d
            java.lang.StringBuffer r0 = r3.accruedInput
            java.lang.String r0 = r0.toString()
        L_0x002c:
            return r0
        L_0x002d:
            char r0 = r3.normalizeAndAccrueDigitsAndPlusSign(r4, r5)
            java.lang.StringBuffer r1 = r3.accruedInputWithoutFormatting
            int r1 = r1.length()
            switch(r1) {
                case 0: goto L_0x0052;
                case 1: goto L_0x0052;
                case 2: goto L_0x0052;
                case 3: goto L_0x0059;
                case 4: goto L_0x0062;
                case 5: goto L_0x0062;
                case 6: goto L_0x0090;
                default: goto L_0x003a;
            }
        L_0x003a:
            java.util.List<com.google.i18n.phonenumbers.Phonemetadata$NumberFormat> r1 = r3.possibleFormats
            int r1 = r1.size()
            if (r1 <= 0) goto L_0x00d1
            java.lang.String r0 = r3.inputDigitHelper(r0)
            java.lang.String r1 = r3.attemptToFormatAccruedDigits()
            int r2 = r1.length()
            if (r2 <= 0) goto L_0x00a3
            r0 = r1
            goto L_0x002c
        L_0x0052:
            java.lang.StringBuffer r0 = r3.accruedInput
            java.lang.String r0 = r0.toString()
            goto L_0x002c
        L_0x0059:
            boolean r1 = r3.attemptToExtractIdd()
            if (r1 == 0) goto L_0x0088
            r1 = 1
            r3.isExpectingCountryCode = r1
        L_0x0062:
            boolean r1 = r3.isExpectingCountryCode
            if (r1 == 0) goto L_0x0090
            boolean r0 = r3.attemptToExtractCountryCode()
            if (r0 == 0) goto L_0x006e
            r3.isExpectingCountryCode = r2
        L_0x006e:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuffer r1 = r3.prefixBeforeNationalNumber
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuffer r1 = r3.nationalNumber
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            goto L_0x002c
        L_0x0088:
            r3.removeNationalPrefixFromNationalNumber()
            java.lang.String r0 = r3.attemptToChooseFormattingPattern()
            goto L_0x002c
        L_0x0090:
            boolean r1 = r3.isExpectingCountryCode
            if (r1 == 0) goto L_0x003a
            boolean r1 = r3.attemptToExtractCountryCode()
            if (r1 != 0) goto L_0x003a
            r3.ableToFormat = r2
            java.lang.StringBuffer r0 = r3.accruedInput
            java.lang.String r0 = r0.toString()
            goto L_0x002c
        L_0x00a3:
            java.lang.StringBuffer r1 = r3.nationalNumber
            java.lang.String r1 = r1.toString()
            r3.narrowDownPossibleFormats(r1)
            boolean r1 = r3.maybeCreateNewTemplate()
            if (r1 == 0) goto L_0x00b8
            java.lang.String r0 = r3.inputAccruedNationalNumber()
            goto L_0x002c
        L_0x00b8:
            boolean r1 = r3.ableToFormat
            if (r1 == 0) goto L_0x002c
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuffer r2 = r3.prefixBeforeNationalNumber
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            goto L_0x002c
        L_0x00d1:
            java.lang.String r0 = r3.attemptToChooseFormattingPattern()
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.i18n.phonenumbers.AsYouTypeFormatter.inputDigitWithOptionToRememberPosition(char, boolean):java.lang.String");
    }

    private boolean maybeCreateNewTemplate() {
        for (Phonemetadata.NumberFormat next : this.possibleFormats) {
            String pattern = next.getPattern();
            if (this.currentFormattingPattern.equals(pattern)) {
                return false;
            }
            if (createFormattingTemplate(next)) {
                this.currentFormattingPattern = pattern;
                return true;
            }
        }
        this.ableToFormat = false;
        return false;
    }

    private void narrowDownPossibleFormats(String str) {
        int length = str.length() - 3;
        Iterator<Phonemetadata.NumberFormat> it = this.possibleFormats.iterator();
        while (it.hasNext()) {
            Phonemetadata.NumberFormat next = it.next();
            if (next.getLeadingDigitsPatternCount() > length && !this.regexCache.getPatternForRegex(next.getLeadingDigitsPattern(length)).matcher(str).lookingAt()) {
                it.remove();
            }
        }
    }

    private char normalizeAndAccrueDigitsAndPlusSign(char c, boolean z) {
        char c2;
        if (c == '+') {
            this.accruedInputWithoutFormatting.append(c);
        }
        if (PhoneNumberUtil.DIGIT_MAPPINGS.containsKey(Character.valueOf(c))) {
            c2 = PhoneNumberUtil.DIGIT_MAPPINGS.get(Character.valueOf(c)).charValue();
            this.accruedInputWithoutFormatting.append(c2);
            this.nationalNumber.append(c2);
        } else {
            c2 = c;
        }
        if (z) {
            this.positionToRemember = this.accruedInputWithoutFormatting.length();
        }
        return c2;
    }

    private void removeNationalPrefixFromNationalNumber() {
        int i;
        if (this.currentMetaData.getCountryCode() == 1 && this.nationalNumber.charAt(0) == '1') {
            this.prefixBeforeNationalNumber.append("1 ");
            this.isInternationalFormatting = true;
            i = 1;
        } else {
            if (this.currentMetaData.hasNationalPrefix()) {
                Matcher matcher = this.nationalPrefixForParsing.matcher(this.nationalNumber);
                if (matcher.lookingAt()) {
                    this.isInternationalFormatting = true;
                    i = matcher.end();
                    this.prefixBeforeNationalNumber.append(this.nationalNumber.substring(0, i));
                }
            }
            i = 0;
        }
        this.nationalNumber.delete(0, i);
    }

    /* access modifiers changed from: package-private */
    public String attemptToFormatAccruedDigits() {
        for (Phonemetadata.NumberFormat next : this.possibleFormats) {
            Matcher matcher = this.regexCache.getPatternForRegex(next.getPattern()).matcher(this.nationalNumber);
            if (matcher.matches()) {
                return ((Object) this.prefixBeforeNationalNumber) + matcher.replaceAll(next.getFormat());
            }
        }
        return "";
    }

    public void clear() {
        this.currentOutput = "";
        this.accruedInput.setLength(0);
        this.accruedInputWithoutFormatting.setLength(0);
        this.formattingTemplate.setLength(0);
        this.lastMatchPosition = 0;
        this.currentFormattingPattern = "";
        this.prefixBeforeNationalNumber.setLength(0);
        this.nationalNumber.setLength(0);
        this.ableToFormat = true;
        this.positionToRemember = 0;
        this.originalPosition = 0;
        this.isInternationalFormatting = false;
        this.isExpectingCountryCode = false;
        this.possibleFormats.clear();
        if (!this.currentMetaData.equals(this.defaultMetaData)) {
            initializeCountrySpecificInfo(this.defaultCountry);
        }
    }

    public int getRememberedPosition() {
        int i = 0;
        if (!this.ableToFormat) {
            return this.originalPosition;
        }
        int length = this.currentOutput.length();
        int i2 = 0;
        while (i2 < this.positionToRemember && i < length) {
            if (this.accruedInputWithoutFormatting.charAt(i2) == this.currentOutput.charAt(i)) {
                i2++;
                i++;
            } else {
                i++;
            }
        }
        return i;
    }

    public String inputDigit(char c) {
        this.currentOutput = inputDigitWithOptionToRememberPosition(c, false);
        return this.currentOutput;
    }

    public String inputDigitAndRememberPosition(char c) {
        this.currentOutput = inputDigitWithOptionToRememberPosition(c, true);
        return this.currentOutput;
    }
}
