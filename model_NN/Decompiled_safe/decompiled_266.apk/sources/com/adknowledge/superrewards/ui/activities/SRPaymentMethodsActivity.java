package com.adknowledge.superrewards.ui.activities;

import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import com.adknowledge.superrewards.SRResources;
import com.adknowledge.superrewards.SuperRewardsImpl;
import com.adknowledge.superrewards.Utils;
import com.adknowledge.superrewards.model.SRImageByName;
import com.adknowledge.superrewards.model.SROffer;
import com.adknowledge.superrewards.model.SROfferType;
import com.adknowledge.superrewards.model.SRParams;
import com.adknowledge.superrewards.model.SRRequestBuilder;
import com.adknowledge.superrewards.tracking.SRAppInstallTracker;
import com.adknowledge.superrewards.ui.adapter.SRDirectPaymentAdapter;
import com.adknowledge.superrewards.ui.adapter.SRInstallPaymentAdapter;
import com.adknowledge.superrewards.ui.adapter.SROfferPaymentAdapter;
import com.adknowledge.superrewards.ui.adapter.item.SRDirectPaymentItem;
import com.adknowledge.superrewards.ui.adapter.item.SROfferPaymentItem;
import com.zong.android.engine.ZpMoConst;
import com.zong.android.engine.activities.ZongPaymentRequest;
import com.zong.android.engine.web.ZongWebView;
import java.util.ArrayList;
import java.util.List;

public class SRPaymentMethodsActivity extends TabActivity {
    /* access modifiers changed from: private */
    public boolean customTitleSupported;
    /* access modifiers changed from: private */
    public SRDirectPaymentAdapter directAdapter;
    /* access modifiers changed from: private */
    public ArrayList<SRDirectPaymentItem> directItemList;
    /* access modifiers changed from: private */
    public List<SROffer> directPaymentList;
    /* access modifiers changed from: private */
    public SRInstallPaymentAdapter installAdapter;
    /* access modifiers changed from: private */
    public ArrayList<SROfferPaymentItem> installItemList;
    /* access modifiers changed from: private */
    public List<SROffer> installPaymentList;
    /* access modifiers changed from: private */
    public TabHost.TabSpec mDirectTabSpec;
    /* access modifiers changed from: private */
    public TabHost.TabSpec mInstallTabSpec;
    /* access modifiers changed from: private */
    public TabHost.TabSpec mOfferTabSpec;
    /* access modifiers changed from: private */
    public TabHost.TabSpec mSupportTabSpec;
    /* access modifiers changed from: private */
    public SROfferPaymentAdapter offerAdapter;
    /* access modifiers changed from: private */
    public ArrayList<SROfferPaymentItem> offerItemList;
    /* access modifiers changed from: private */
    public List<SROffer> offerPaymentList;
    /* access modifiers changed from: private */
    public List<SROffer> paymentMethodList;
    /* access modifiers changed from: private */
    public TabHost tabHost;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("SR", "Setting up Payment Methods");
        this.customTitleSupported = requestWindowFeature(7);
        if (!Utils.isOnline(getApplicationContext())) {
            Toast.makeText(getApplicationContext(), "\tThis Application \nRequires Internet Access.", 1).show();
            finish();
        }
        setContentView(SRResources.layout.sr_payment_methods_activity_layout);
        Bundle extras = getIntent().getExtras();
        String h = extras.getString("h");
        String uid = extras.getString("uid");
        String cc = extras.getString("cc");
        String string = extras.getString("xml");
        String nOffers = extras.getString("nOffers");
        this.offerAdapter = new SROfferPaymentAdapter(this);
        this.directAdapter = new SRDirectPaymentAdapter(this);
        this.installAdapter = new SRInstallPaymentAdapter(this);
        ListView directListView = (ListView) findViewById(SRResources.id.SRPaymentMethodsActivityDirectListView);
        directListView.setAdapter((ListAdapter) this.directAdapter);
        ((ListView) findViewById(SRResources.id.SRPaymentMethodsActivityOfferListView)).setAdapter((ListAdapter) this.offerAdapter);
        ((ListView) findViewById(SRResources.id.SRPaymentMethodsActivityInstallListView)).setAdapter((ListAdapter) this.installAdapter);
        if (!Utils.checkRunFlag(getApplicationContext())) {
            SRAppInstallTracker.getInstance(getApplicationContext(), h).track();
        }
        this.tabHost = getTabHost();
        this.tabHost.setup();
        this.mInstallTabSpec = this.tabHost.newTabSpec("INSTALL").setIndicator("install", getResources().getDrawable(SRResources.drawable.sr_install_tab_icon)).setContent(SRResources.id.SRPaymentMethodsActivityInstallListView);
        this.tabHost.addTab(this.mInstallTabSpec);
        this.mDirectTabSpec = this.tabHost.newTabSpec("DIRECT").setIndicator("buy", getResources().getDrawable(SRResources.drawable.sr_buy_tab_icon)).setContent(SRResources.id.SRPaymentMethodsActivityDirectListView);
        this.tabHost.addTab(this.mDirectTabSpec);
        this.mOfferTabSpec = this.tabHost.newTabSpec("OFFER").setIndicator("earn", getResources().getDrawable(SRResources.drawable.sr_earn_tab_icon)).setContent(SRResources.id.SRPaymentMethodsActivityOfferListView);
        this.tabHost.addTab(this.mOfferTabSpec);
        new PaymentMethodsTask().execute(h, uid, cc, "3", SRParams.v, nOffers, null);
        Log.i("SR", "Finished Setting up Payment Methods");
        directListView.setOnItemClickListener(getDirectPaymentOnItemClickListener());
        setResult(-1);
    }

    public class PaymentMethodsTask extends AsyncTask<String, Void, Void> {
        ProgressDialog myProgressDialog = ProgressDialog.show(SRPaymentMethodsActivity.this, "Please wait...", "Loading data from Super Rewards", false);

        public PaymentMethodsTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(String... params) {
            Log.i("SR", "In Payment Methods parent thread");
            List unused = SRPaymentMethodsActivity.this.paymentMethodList = new SuperRewardsImpl().getOffers(params[0], params[1], params[2], params[3], params[4], params[5], null, SRPaymentMethodsActivity.this.getApplicationContext());
            if (SRPaymentMethodsActivity.this.paymentMethodList == null) {
                this.myProgressDialog.dismiss();
                SRPaymentMethodsActivity.this.setResult(0);
                SRPaymentMethodsActivity.this.finish();
                cancel(true);
            }
            if (isCancelled()) {
                return null;
            }
            List unused2 = SRPaymentMethodsActivity.this.directPaymentList = SRPaymentMethodsActivity.this.filterOffers(SRPaymentMethodsActivity.this.paymentMethodList, SROfferType.DIRECT);
            List unused3 = SRPaymentMethodsActivity.this.offerPaymentList = SRPaymentMethodsActivity.this.filterOffers(SRPaymentMethodsActivity.this.paymentMethodList, SROfferType.OFFER);
            List unused4 = SRPaymentMethodsActivity.this.installPaymentList = SRPaymentMethodsActivity.this.filterOffers(SRPaymentMethodsActivity.this.paymentMethodList, SROfferType.INSTALL);
            ArrayList unused5 = SRPaymentMethodsActivity.this.directItemList = new ArrayList(SRPaymentMethodsActivity.this.directPaymentList.size());
            ArrayList unused6 = SRPaymentMethodsActivity.this.offerItemList = new ArrayList(SRPaymentMethodsActivity.this.offerPaymentList.size());
            ArrayList unused7 = SRPaymentMethodsActivity.this.installItemList = new ArrayList(SRPaymentMethodsActivity.this.installPaymentList.size());
            for (SROffer srDirect : SRPaymentMethodsActivity.this.directPaymentList) {
                if (SRImageByName.getDirectPayImageByName(srDirect.getName()) != 0) {
                    SRPaymentMethodsActivity.this.directItemList.add(new SRDirectPaymentItem(SRPaymentMethodsActivity.this.getResources().getDrawable(SRImageByName.getDirectPayImageByName(srDirect.getName())), srDirect.getName()));
                } else {
                    SRPaymentMethodsActivity.this.directItemList.add(new SRDirectPaymentItem(Utils.getImage(SRPaymentMethodsActivity.this.getApplicationContext(), srDirect.getIcon()), srDirect.getName()));
                }
                Log.i("SR", "Set Direct Payment Offer: " + srDirect.getName());
            }
            for (SROffer srOffer : SRPaymentMethodsActivity.this.offerPaymentList) {
                SRPaymentMethodsActivity.this.offerItemList.add(new SROfferPaymentItem(Utils.getImage(SRPaymentMethodsActivity.this.getApplicationContext(), srOffer.getIcon()), Utils.getImage(SRPaymentMethodsActivity.this.getApplicationContext(), srOffer.getImage()), srOffer));
                Log.i("SR", "Set Offer: " + srOffer.getName());
            }
            for (SROffer srOffer2 : SRPaymentMethodsActivity.this.installPaymentList) {
                SRPaymentMethodsActivity.this.installItemList.add(new SROfferPaymentItem(Utils.getImage(SRPaymentMethodsActivity.this.getApplicationContext(), srOffer2.getIcon()), Utils.getImage(SRPaymentMethodsActivity.this.getApplicationContext(), srOffer2.getImage()), srOffer2));
                Log.i("SR", "Set Offer: " + srOffer2.getName());
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void nothing) {
            if (!isCancelled()) {
                SRPaymentMethodsActivity.this.customTitleBar("get more " + SRParams.currency.toLowerCase(), false, Boolean.valueOf(SRPaymentMethodsActivity.this.customTitleSupported));
                SRPaymentMethodsActivity.this.tabHost.clearAllTabs();
                if (SRPaymentMethodsActivity.this.installItemList.size() != 0) {
                    SRPaymentMethodsActivity.this.tabHost.addTab(SRPaymentMethodsActivity.this.mInstallTabSpec);
                }
                if (SRPaymentMethodsActivity.this.directItemList.size() != 0) {
                    SRPaymentMethodsActivity.this.tabHost.addTab(SRPaymentMethodsActivity.this.mDirectTabSpec);
                }
                if (SRPaymentMethodsActivity.this.offerItemList.size() != 0) {
                    SRPaymentMethodsActivity.this.tabHost.addTab(SRPaymentMethodsActivity.this.mOfferTabSpec);
                }
                Intent i = new Intent(SRPaymentMethodsActivity.this.getApplicationContext(), SRWebViewActivity.class);
                i.putExtra(SRWebViewActivity.URL, SRParams.help);
                TabHost.TabSpec unused = SRPaymentMethodsActivity.this.mSupportTabSpec = SRPaymentMethodsActivity.this.tabHost.newTabSpec("SUPPORT").setIndicator("support", SRPaymentMethodsActivity.this.getResources().getDrawable(SRResources.drawable.sr_support_tab_icon)).setContent(i);
                SRPaymentMethodsActivity.this.tabHost.addTab(SRPaymentMethodsActivity.this.mSupportTabSpec);
                SRPaymentMethodsActivity.this.getTabWidget().setCurrentTab(0);
                SRPaymentMethodsActivity.this.directAdapter.setOffers(SRPaymentMethodsActivity.this.directItemList);
                SRPaymentMethodsActivity.this.offerAdapter.setOffers(SRPaymentMethodsActivity.this.offerItemList);
                SRPaymentMethodsActivity.this.installAdapter.setOffers(SRPaymentMethodsActivity.this.installItemList);
                SRPaymentMethodsActivity.this.directAdapter.notifyDataSetChanged();
                SRPaymentMethodsActivity.this.offerAdapter.notifyDataSetChanged();
                SRPaymentMethodsActivity.this.installAdapter.notifyDataSetChanged();
                this.myProgressDialog.dismiss();
                return;
            }
            Toast.makeText(SRPaymentMethodsActivity.this.getApplicationContext(), "Sorry, an error occurred. \nPlease try again later.", 1).show();
        }
    }

    /* access modifiers changed from: private */
    public List<SROffer> filterOffers(List<SROffer> offers, SROfferType type) {
        List<SROffer> filteredOffers = new ArrayList<>();
        for (SROffer srOffer : offers) {
            if (srOffer.getOfferType().equals(type)) {
                filteredOffers.add(srOffer);
            }
        }
        return filteredOffers;
    }

    /* access modifiers changed from: protected */
    public AdapterView.OnItemClickListener getDirectPaymentOnItemClickListener() {
        return new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                SROffer selectedOffer = (SROffer) SRPaymentMethodsActivity.this.directPaymentList.get(position);
                if (selectedOffer.getName().equalsIgnoreCase("zong")) {
                    ZongPaymentRequest paymentRequest = new SRRequestBuilder(SRPaymentMethodsActivity.this.getApplicationContext(), selectedOffer).getRequest();
                    Intent intent = new Intent(SRPaymentMethodsActivity.this.getApplicationContext(), ZongWebView.class);
                    intent.putExtra(ZpMoConst.ZONG_MOBILE_PAYMENT_BUNDLE_KEY, paymentRequest);
                    SRPaymentMethodsActivity.this.startActivityForResult(intent, 1);
                    return;
                }
                Intent intent2 = new Intent("android.intent.action.VIEW");
                intent2.putExtra(SROffer.OFFER, selectedOffer);
                intent2.setClassName(arg0.getContext(), SRDirectPaymentActivity.class.getName());
                SRPaymentMethodsActivity.this.startActivity(intent2);
            }
        };
    }

    public void customTitleBar(String blurb, boolean spinner, Boolean customTitleSupported2) {
        if (customTitleSupported2.booleanValue()) {
            getWindow().setFeatureInt(7, SRResources.layout.sr_custom_title);
            TextView SRCustomTitleLeft = (TextView) findViewById(SRResources.id.SRCustomTitleLeft);
            SRCustomTitleLeft.setText(blurb);
            ProgressBar titleProgressBar = (ProgressBar) findViewById(SRResources.id.SRTitleProgressBar);
            titleProgressBar.setVisibility(8);
            if (spinner) {
                SRCustomTitleLeft.setText("Loading...");
                titleProgressBar.setVisibility(0);
            }
        }
    }
}
