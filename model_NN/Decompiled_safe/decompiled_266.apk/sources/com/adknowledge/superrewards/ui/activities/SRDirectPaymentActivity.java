package com.adknowledge.superrewards.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.adknowledge.superrewards.SRResources;
import com.adknowledge.superrewards.model.SRImageByName;
import com.adknowledge.superrewards.model.SROffer;
import com.adknowledge.superrewards.model.SRParams;
import com.adknowledge.superrewards.model.SRPricePoint;
import com.adknowledge.superrewards.tracking.SRAppInstallTracker;
import com.adknowledge.superrewards.web.SRClient;
import com.adknowledge.superrewards.web.SRRequest;

public class SRDirectPaymentActivity extends Activity {
    /* access modifiers changed from: private */
    public SROffer directPaymentMethod;
    private ImageView imageView;
    /* access modifiers changed from: private */
    public Spinner spinner;
    private TextView textButton;
    private TextView textView;
    private TextView textView2;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean customTitleSupported = requestWindowFeature(7);
        setContentView(SRResources.layout.sr_direct_payment_activity_layout);
        this.directPaymentMethod = (SROffer) getIntent().getSerializableExtra(SROffer.OFFER);
        this.textView = (TextView) findViewById(SRResources.id.SRDirectPaymentName);
        this.textView.setText(this.directPaymentMethod.getName().trim().toLowerCase());
        this.imageView = (ImageView) findViewById(SRResources.id.SRDirectPaymentLargeIcon);
        this.spinner = (Spinner) findViewById(SRResources.id.SRDirectPaymentPricePoints);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, 17367048);
        arrayAdapter.setDropDownViewResource(17367049);
        this.spinner.setAdapter((SpinnerAdapter) arrayAdapter);
        for (SRPricePoint point : this.directPaymentMethod.getPricePoints()) {
            arrayAdapter.add(point.getLocalText());
        }
        this.textView2 = (TextView) findViewById(SRResources.id.SRDirectPaymentDescription);
        this.textView2.setText(this.directPaymentMethod.getDescription().trim().toLowerCase().replace("%points%", SRParams.currency.trim().toLowerCase()).replace("%POINTS%", SRParams.currency.trim().toLowerCase()));
        if (SRImageByName.getDirectPayImageByName(this.directPaymentMethod.getName()) != 0) {
            this.imageView.setImageDrawable(getResources().getDrawable(SRImageByName.getDirectPayImageByName(this.directPaymentMethod.getName())));
        }
        this.textButton = (TextView) findViewById(SRResources.id.SRDirectPaymentBuyButton);
        this.textButton.setText("buy " + SRParams.currency.trim().toLowerCase());
        this.textButton.setOnClickListener(getBuyButtonOnCLickListener());
        customTitleBar("buy more " + SRParams.currency.trim().toLowerCase(), false, Boolean.valueOf(customTitleSupported));
    }

    private View.OnClickListener getBuyButtonOnCLickListener() {
        return new View.OnClickListener() {
            public void onClick(View arg0) {
                String pricePointSelecetedText = (String) SRDirectPaymentActivity.this.spinner.getSelectedItem();
                for (SRPricePoint pricePoint : SRDirectPaymentActivity.this.directPaymentMethod.getPricePoints()) {
                    if (pricePointSelecetedText.equals(pricePoint.getLocalText())) {
                        SRRequest request = SRClient.getInstance().createRequest();
                        request.setCommand(SRRequest.Command.METHOD);
                        request.setCall(SRRequest.Call.CLICK);
                        request.addInnerParam(SRPricePoint.AMOUNT, pricePoint.getAmount());
                        request.addInnerParam(SROffer.CLICK_URL, SRDirectPaymentActivity.this.directPaymentMethod.getUrl() + "&amount=" + pricePoint.getAmount() + "&custom_param_sr_mobile=true");
                        Intent i = new Intent("android.intent.action.VIEW");
                        i.setData(Uri.parse(request.getUrlWithJson(SRDirectPaymentActivity.this.getApplicationContext(), SRAppInstallTracker.h)));
                        SRDirectPaymentActivity.this.startActivity(i);
                        return;
                    }
                }
            }
        };
    }

    public void customTitleBar(String blurb, boolean spinner2, Boolean customTitleSupported) {
        if (customTitleSupported.booleanValue()) {
            getWindow().setFeatureInt(7, SRResources.layout.sr_custom_title);
            TextView SRCustomTitleLeft = (TextView) findViewById(SRResources.id.SRCustomTitleLeft);
            SRCustomTitleLeft.setText(blurb);
            ProgressBar titleProgressBar = (ProgressBar) findViewById(SRResources.id.SRTitleProgressBar);
            titleProgressBar.setVisibility(8);
            if (spinner2) {
                SRCustomTitleLeft.setText("Loading...");
                titleProgressBar.setVisibility(0);
            }
        }
    }
}
