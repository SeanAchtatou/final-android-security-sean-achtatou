package com.adknowledge.superrewards.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.adknowledge.superrewards.SRResources;
import com.adknowledge.superrewards.ui.adapter.item.SRDirectPaymentItem;
import java.util.ArrayList;
import java.util.List;

public class SRDirectPaymentAdapter extends BaseAdapter {
    private final Context context;
    private List<SRDirectPaymentItem> directs = new ArrayList();

    public SRDirectPaymentAdapter(Context context2) {
        this.context = context2;
    }

    public void setOffers(List<SRDirectPaymentItem> offers) {
        this.directs = offers;
    }

    public int getCount() {
        return this.directs.size();
    }

    public SRDirectPaymentItem getItem(int position) {
        try {
            return this.directs.get(position);
        } catch (Exception e) {
            return null;
        }
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(SRResources.layout.sr_list_direct_item_template, (ViewGroup) null);
        }
        SRDirectPaymentItem srOfferItem = getItem(position);
        if (srOfferItem != null) {
            ImageView srListItemImageView = (ImageView) v.findViewById(SRResources.id.SRDirectListItemImageView);
            TextView srTextView = (TextView) v.findViewById(SRResources.id.SRDirectListItemTextView);
            if (srListItemImageView != null) {
                srListItemImageView.setImageDrawable(srOfferItem.getOfferIcon());
            }
            if (srTextView != null) {
                srTextView.setText(srOfferItem.getOfferName().toLowerCase());
            }
        }
        return v;
    }
}
