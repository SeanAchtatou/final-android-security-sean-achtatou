package com.adknowledge.superrewards.ui.adapter.item;

import android.graphics.drawable.Drawable;

public class SRDirectPaymentItem {
    private Drawable offerIcon;
    private String offerName;

    public Drawable getOfferIcon() {
        return this.offerIcon;
    }

    public void setOfferIcon(Drawable offerIcon2) {
        this.offerIcon = offerIcon2;
    }

    public String getOfferName() {
        if (this.offerName.toLowerCase().equals("paypalec")) {
            this.offerName = "Paypal";
        }
        if (this.offerName.toLowerCase().equals("gate2shop")) {
            this.offerName = "Credit Card";
        }
        if (this.offerName.toLowerCase().equals("google")) {
            this.offerName = "Google";
        }
        return this.offerName;
    }

    public void setOfferName(String offerName2) {
        this.offerName = offerName2;
    }

    public SRDirectPaymentItem(Drawable offerIcon2, String offerName2) {
        this.offerIcon = offerIcon2;
        this.offerName = offerName2;
    }
}
