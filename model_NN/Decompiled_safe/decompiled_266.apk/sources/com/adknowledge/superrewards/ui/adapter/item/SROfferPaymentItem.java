package com.adknowledge.superrewards.ui.adapter.item;

import android.graphics.drawable.Drawable;
import com.adknowledge.superrewards.model.SROffer;

public class SROfferPaymentItem {
    private Drawable icon;
    private Drawable image;
    private SROffer offer;

    public Drawable getIcon() {
        return this.icon;
    }

    public Drawable getImage() {
        return this.image;
    }

    public void setIcon(Drawable icon2) {
        this.icon = icon2;
    }

    public void setImage(Drawable image2) {
        this.image = image2;
    }

    public SROffer getOffer() {
        return this.offer;
    }

    public void setOffer(SROffer offer2) {
        this.offer = offer2;
    }

    public SROfferPaymentItem(Drawable icon2, Drawable image2, SROffer offer2) {
        this.icon = icon2;
        this.offer = offer2;
        this.image = image2;
    }
}
