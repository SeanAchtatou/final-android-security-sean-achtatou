package com.adknowledge.superrewards.model;

import java.io.Serializable;
import java.util.List;

public class SROffer implements Serializable {
    public static final String CLICK_URL = "click_url";
    public static final String CURRENCY = "currency";
    public static final String DESCRIPTION = "description";
    public static final String GEO_AVAILABILITY = "geo_availability";
    public static final String ICON = "icon";
    public static final String ICON_LARGE = "icon_large";
    public static final String ID = "id";
    public static final String IMAGE = "image";
    public static final String LONG_DESCRIPTION = "long_description";
    public static final String METHOD_TYPES = "method_types";
    public static final String NAME = "name";
    public static final String N_PAGES = "n_pages";
    public static final String OFFER = "offer";
    public static final String OFFERS = "offers";
    public static final String OFFER_TYPE = "offer_type";
    public static final String PAYOUT = "payout";
    public static final String PRICE_POINTS = "price_points";
    public static final String PROBLEMS_PAGE = "problems_page";
    public static final String REQUIREMENTS = "requirements";
    public static final String SHORT_DESCRIPTION = "short_description";
    public static final String SHORT_NAME = "short_name";
    public static final String SRXML = "srxml";
    public static final String TESTOFFER = "testoffer";
    public static final String TITLE = "title";
    public static final String TYPE = "type";
    private static final long serialVersionUID = -7384379450200430392L;
    private final String DefaultImageFree = "http://cdn.superrewards-offers.com/img/offerwall/v2/images/off_image_free.png";
    private final String DefaultImagePurchase = "http://cdn.superrewards-offers.com/img/offerwall/v2/images/off_image_purchase.png";
    private String currency;
    private String description;
    private String geo_availability;
    private String icon;
    private String iconLarge;
    private String id;
    private String image;
    private String longDescription;
    private List<SRMethodType> methodTypes;
    private String name;
    private String npages;
    private SROfferType offerType;
    private String payout;
    private List<SRPricePoint> pricePoints;
    private String problemsPage;
    private String requirements;
    private String shortDescription;
    private String shortName;
    private SRType type;
    private String url;

    public SROffer(String currency2, String npages2, String problemsPage2) {
        getClass();
        this.image = "http://cdn.superrewards-offers.com/img/offerwall/v2/images/off_image_purchase.png";
        this.currency = currency2;
        this.npages = npages2;
        this.problemsPage = problemsPage2;
    }

    public SROffer() {
        getClass();
        this.image = "http://cdn.superrewards-offers.com/img/offerwall/v2/images/off_image_purchase.png";
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency2) {
        this.currency = currency2;
    }

    public String getNpages() {
        return this.npages;
    }

    public void setNpages(String npages2) {
        this.npages = npages2;
    }

    public String getProblemsPage() {
        return this.problemsPage;
    }

    public void setProblemsPage(String problemsPage2) {
        this.problemsPage = problemsPage2;
    }

    public void setGeoAvailability(String geo) {
        this.geo_availability = geo;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
        if (name2.toLowerCase().equals("paypalec")) {
            this.name = "Paypal";
        }
        if (name2.toLowerCase().equals("gate2shop")) {
            this.name = "Credit Card";
        }
        if (name2.toLowerCase().equals("google")) {
            this.name = "Google";
        }
        if (name2.toLowerCase().equals("zongplus")) {
            this.name = "zong";
        }
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public String getRequirements() {
        return this.requirements;
    }

    public void setRequirements(String requirements2) {
        this.requirements = requirements2;
    }

    public String getPayout() {
        return this.payout;
    }

    public void setPayout(String payout2) {
        this.payout = payout2;
    }

    public SRType getType() {
        return this.type;
    }

    public boolean isFree() {
        if (this.type.toString().toLowerCase().equals("free")) {
            return true;
        }
        return false;
    }

    public void setType(SRType type2) {
        this.type = type2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public SROfferType getOfferType() {
        return this.offerType;
    }

    public void setOfferType(SROfferType offerType2) {
        this.offerType = offerType2;
    }

    public String getIcon() {
        return this.icon;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image2) {
        this.image = image2;
    }

    public void setFreeImage() {
        getClass();
        this.image = "http://cdn.superrewards-offers.com/img/offerwall/v2/images/off_image_free.png";
    }

    public void setIcon(String icon2) {
        this.icon = null;
    }

    public String getIconLarge() {
        return this.iconLarge;
    }

    public void setIconLarge(String iconLarge2) {
        this.iconLarge = iconLarge2;
    }

    public String getShortName() {
        return this.shortName;
    }

    public void setShortName(String shortName2) {
        this.shortName = shortName2;
    }

    public String getShortDescription() {
        return this.shortDescription;
    }

    public void setShortDescription(String shortDescription2) {
        this.shortDescription = shortDescription2;
    }

    public String getLongDescription() {
        return this.longDescription;
    }

    public void setLongDescription(String longDescription2) {
        this.longDescription = longDescription2;
    }

    public List<SRPricePoint> getPricePoints() {
        return this.pricePoints;
    }

    public void setPricePoints(List<SRPricePoint> pricePoints2) {
        this.pricePoints = pricePoints2;
    }

    public List<SRMethodType> getMethodTypes() {
        return this.methodTypes;
    }

    public void setMethodTypes(List<SRMethodType> methodTypes2) {
        this.methodTypes = methodTypes2;
    }

    public int hashCode() {
        int i = 1 * 31;
        return (((((((((((((((((((((((((((((((((((((this.currency == null ? 0 : this.currency.hashCode()) + 31) * 31) + (this.description == null ? 0 : this.description.hashCode())) * 31) + (this.icon == null ? 0 : this.icon.hashCode())) * 31) + (this.image == null ? 0 : this.image.hashCode())) * 31) + (this.iconLarge == null ? 0 : this.iconLarge.hashCode())) * 31) + (this.id == null ? 0 : this.id.hashCode())) * 31) + (this.longDescription == null ? 0 : this.longDescription.hashCode())) * 31) + (this.methodTypes == null ? 0 : this.methodTypes.hashCode())) * 31) + (this.name == null ? 0 : this.name.hashCode())) * 31) + (this.npages == null ? 0 : this.npages.hashCode())) * 31) + (this.offerType == null ? 0 : this.offerType.hashCode())) * 31) + (this.payout == null ? 0 : this.payout.hashCode())) * 31) + (this.pricePoints == null ? 0 : this.pricePoints.hashCode())) * 31) + (this.problemsPage == null ? 0 : this.problemsPage.hashCode())) * 31) + (this.requirements == null ? 0 : this.requirements.hashCode())) * 31) + (this.shortDescription == null ? 0 : this.shortDescription.hashCode())) * 31) + (this.shortName == null ? 0 : this.shortName.hashCode())) * 31) + (this.type == null ? 0 : this.type.hashCode())) * 31) + (this.url == null ? 0 : this.url.hashCode());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SROffer other = (SROffer) obj;
        if (this.currency == null) {
            if (other.currency != null) {
                return false;
            }
        } else if (!this.currency.equals(other.currency)) {
            return false;
        }
        if (this.description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!this.description.equals(other.description)) {
            return false;
        }
        if (this.icon == null) {
            if (other.icon != null) {
                return false;
            }
        } else if (!this.icon.equals(other.icon)) {
            return false;
        }
        if (this.iconLarge == null) {
            if (other.iconLarge != null) {
                return false;
            }
        } else if (!this.iconLarge.equals(other.iconLarge)) {
            return false;
        }
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        if (this.longDescription == null) {
            if (other.longDescription != null) {
                return false;
            }
        } else if (!this.longDescription.equals(other.longDescription)) {
            return false;
        }
        if (this.methodTypes == null) {
            if (other.methodTypes != null) {
                return false;
            }
        } else if (!this.methodTypes.equals(other.methodTypes)) {
            return false;
        }
        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!this.name.equals(other.name)) {
            return false;
        }
        if (this.npages == null) {
            if (other.npages != null) {
                return false;
            }
        } else if (!this.npages.equals(other.npages)) {
            return false;
        }
        if (this.offerType == null) {
            if (other.offerType != null) {
                return false;
            }
        } else if (!this.offerType.equals(other.offerType)) {
            return false;
        }
        if (this.payout == null) {
            if (other.payout != null) {
                return false;
            }
        } else if (!this.payout.equals(other.payout)) {
            return false;
        }
        if (this.pricePoints == null) {
            if (other.pricePoints != null) {
                return false;
            }
        } else if (!this.pricePoints.equals(other.pricePoints)) {
            return false;
        }
        if (this.problemsPage == null) {
            if (other.problemsPage != null) {
                return false;
            }
        } else if (!this.problemsPage.equals(other.problemsPage)) {
            return false;
        }
        if (this.requirements == null) {
            if (other.requirements != null) {
                return false;
            }
        } else if (!this.requirements.equals(other.requirements)) {
            return false;
        }
        if (this.shortDescription == null) {
            if (other.shortDescription != null) {
                return false;
            }
        } else if (!this.shortDescription.equals(other.shortDescription)) {
            return false;
        }
        if (this.shortName == null) {
            if (other.shortName != null) {
                return false;
            }
        } else if (!this.shortName.equals(other.shortName)) {
            return false;
        }
        if (this.type == null) {
            if (other.type != null) {
                return false;
            }
        } else if (!this.type.equals(other.type)) {
            return false;
        }
        if (this.url == null) {
            if (other.url != null) {
                return false;
            }
        } else if (!this.url.equals(other.url)) {
            return false;
        }
        return true;
    }
}
