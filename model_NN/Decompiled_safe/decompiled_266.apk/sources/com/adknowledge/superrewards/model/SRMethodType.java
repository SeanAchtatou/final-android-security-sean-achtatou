package com.adknowledge.superrewards.model;

public enum SRMethodType {
    CREDITCARD("Creditcard"),
    PAYPAL("PayPal"),
    BANK("Bank"),
    PHONE("Phone"),
    PREPAID("Prepaid"),
    CASH("Cash"),
    OTHER("Other"),
    MOBILE("Mobile");
    
    private String displayName;

    private SRMethodType(String displayName2) {
        this.displayName = displayName2;
    }

    public String getDisplayName() {
        return this.displayName;
    }
}
