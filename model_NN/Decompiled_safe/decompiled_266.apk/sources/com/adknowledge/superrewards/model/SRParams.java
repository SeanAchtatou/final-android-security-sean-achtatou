package com.adknowledge.superrewards.model;

import java.math.BigDecimal;

public class SRParams {
    public static final String APPINFO = "appinfo";
    public static final String APPINFO_NAME = "name";
    public static final String CURRENCY = "currency";
    public static final String CURRENCY_CODE = "currency_code";
    public static final String CURRENCY_ICON = "currency_icon";
    public static final String CURRENCY_SYMBOL = "currency_symbol";
    public static final String DIRECTPAY = "directpay";
    public static final String DIRECTPAY_AVAILABLE = "da";
    public static final String HELP = "help";
    public static final String LINKS = "links";
    public static final String LOCAL_RATE = "local_rate";
    public static final String NAME = "name";
    public static final String OFFERS_AVAILABLE = "oa";
    public static final String PARAMS = "params";
    public static final String PROVIDER = "provider";
    public static final String PROVIDERS = "providers";
    public static final String RATE = "rate";
    public static final String SHORT_NAME = "short_name";
    public static final String SRV = "srv";
    public static final String TO_US_RATE = "to_us_rate";
    public static final String TO_US_TEXT = "to_us_text";
    public static final String V = "v";
    public static String currency = null;
    public static String currency_code = null;
    public static String currency_icon = null;
    public static String currency_symbol = null;
    public static boolean directpay_available = false;
    public static int directpay_count = 0;
    public static String help = null;
    public static String name = null;
    public static boolean offers_available = false;
    public static BigDecimal rate;
    public static int srv = 0;
    public static BigDecimal to_us_rate;
    public static String to_us_text = null;
    public static String v = "3";
    public static String version_number = "1.1.02";
}
