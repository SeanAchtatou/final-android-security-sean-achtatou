package com.adknowledge.superrewards.model;

import android.content.Context;
import android.util.Log;
import com.adknowledge.superrewards.Utils;
import com.adknowledge.superrewards.tracking.SRAppInstallTracker;
import com.adknowledge.superrewards.web.SRClient;
import com.adknowledge.superrewards.web.SRRequest;
import com.papaya.social.PPYSocial;
import com.zong.android.engine.activities.ZongPaymentRequest;
import com.zong.android.engine.provider.ZongPhoneManager;
import java.net.URLDecoder;

public class SRRequestBuilder {
    Context ctx;
    String pattern = "?purchaseKey=";
    SROffer zongoffer;

    public SRRequestBuilder(Context ctx2, SROffer zongoffer2) {
        this.ctx = ctx2;
        this.zongoffer = zongoffer2;
    }

    public ZongPaymentRequest getRequest() {
        String phoneNumber = ZongPhoneManager.getInstance().getPhoneState(this.ctx).getMsisdn(Utils.getCountryCode());
        String simOperator = ZongPhoneManager.getInstance().getPhoneState(this.ctx).getSimOp();
        ZongPaymentRequest paymentRequest = new ZongPaymentRequest();
        paymentRequest.setDebugMode(false);
        paymentRequest.setSimulationMode(Boolean.valueOf(1 == 0));
        paymentRequest.setAppName("Test");
        paymentRequest.setLang(PPYSocial.LANG_EN);
        paymentRequest.setCountry(Utils.getCountryCode());
        paymentRequest.setCurrency("USD");
        paymentRequest.setPhoneNumber(phoneNumber);
        paymentRequest.setMno(simOperator);
        paymentRequest.setUrl("https://pay01.zong.com/zongpay/actions/processing");
        SRRequest request = SRClient.getInstance().createRequest();
        request.setCommand(SRRequest.Command.METHOD);
        request.setCall(SRRequest.Call.GET_SUB);
        request.addInnerParam(SROffer.OFFER, "3851");
        request.addInnerParam("uid", SRClient.uid);
        request.execute(this.ctx, SRAppInstallTracker.h);
        paymentRequest.setTransactionRef(request.getResult());
        paymentRequest.setCustomerKey("srptand");
        for (SRPricePoint item : this.zongoffer.getPricePoints()) {
            String entryPointUrl = item.getEntrypointurl();
            Log.i("SR", "entrypointUrl: " + entryPointUrl);
            int leftStart = entryPointUrl.indexOf(this.pattern);
            String purchaseKey = URLDecoder.decode(entryPointUrl.substring(entryPointUrl.indexOf(this.pattern) + this.pattern.length()));
            String url = entryPointUrl.substring(0, leftStart);
            float amount = Float.parseFloat(item.getUsAmount().replace("$", ""));
            Integer quantity = Integer.valueOf(item.getPoints());
            String label = item.getLocalText();
            paymentRequest.setUrl(url);
            paymentRequest.addPricePoint(purchaseKey, amount, quantity.intValue(), "currency", label);
        }
        return paymentRequest;
    }
}
