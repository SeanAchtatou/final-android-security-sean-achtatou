package com.adknowledge.superrewards.model;

import java.io.Serializable;

public class SRPricePoint implements Serializable {
    public static final String AMOUNT = "amount";
    public static final String ENTRYPOINTURL = "entrypointUrl";
    public static final String LOCAL_AMOUNT = "local_amount";
    public static final String LOCAL_TEXT = "local_text";
    public static final String POINTS = "points";
    public static final String PRICE = "price";
    public static final String US_AMOUNT = "us_amount";
    public static final String US_TEXT = "us_text";
    private static final long serialVersionUID = -105326348563870968L;
    private String amount;
    private String entrypointurl;
    private String local_amount;
    private String local_currency;
    private String local_text;
    private int points;
    private String text;
    private String us_amount;
    private String us_text;

    public String getUsText() {
        return this.us_text;
    }

    public void setUsText(String text2) {
        this.us_text = text2;
    }

    public String getLocalText() {
        return this.local_text;
    }

    public void setLocalText(String text2) {
        this.local_text = text2;
    }

    public void setPoints(String points2) {
        this.points = Integer.parseInt(points2);
    }

    public int getPoints() {
        return this.points;
    }

    public void setLocalAmount(String local_amount2) {
        this.local_amount = local_amount2;
    }

    public String getLocalAmount() {
        return this.local_amount;
    }

    public void setUsAmount(String us_amount2) {
        this.us_amount = us_amount2;
    }

    public String getUsAmount() {
        return this.us_amount;
    }

    public void setEntrypointurl(String url) {
        this.entrypointurl = url;
    }

    public String getEntrypointurl() {
        return this.entrypointurl;
    }

    public String getAmount() {
        return this.amount;
    }

    public void setAmount(String amount2) {
        this.amount = amount2;
    }

    public int hashCode() {
        int i = 1 * 31;
        return (((this.amount == null ? 0 : this.amount.hashCode()) + 31) * 31) + (this.text == null ? 0 : this.text.hashCode());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SRPricePoint other = (SRPricePoint) obj;
        if (this.amount == null) {
            if (other.amount != null) {
                return false;
            }
        } else if (!this.amount.equals(other.amount)) {
            return false;
        }
        if (this.text == null) {
            if (other.text != null) {
                return false;
            }
        } else if (!this.text.equals(other.text)) {
            return false;
        }
        return true;
    }
}
