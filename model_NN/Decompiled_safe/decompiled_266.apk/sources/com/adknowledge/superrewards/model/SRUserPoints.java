package com.adknowledge.superrewards.model;

import android.content.Context;
import android.widget.Toast;
import com.adknowledge.superrewards.Utils;
import com.adknowledge.superrewards.tracking.SRAppInstallTracker;
import com.adknowledge.superrewards.web.SRClient;
import com.adknowledge.superrewards.web.SRRequest;
import org.codehaus.jackson.util.BufferRecycler;
import org.json.JSONException;
import org.json.JSONObject;

public class SRUserPoints {
    private Context ctx;
    private int newpoints = 0;
    private int totalpoints = 0;

    public SRUserPoints(Context ctx2) {
        this.ctx = ctx2;
    }

    public boolean updatePoints(String hparam, String uid) {
        SRAppInstallTracker instance = SRAppInstallTracker.getInstance(this.ctx, hparam);
        SRRequest request = SRClient.getInstance().createRequest();
        request.setCommand(SRRequest.Command.METHOD);
        request.setCall(SRRequest.Call.CHECK_POINTS);
        request.addInnerParam("uid", uid);
        if (!Utils.isOnline(this.ctx) || !request.execute(this.ctx, hparam)) {
            return false;
        }
        try {
            JSONObject innerjson = new JSONObject(request.getResult()).getJSONObject("data");
            this.newpoints = innerjson.getInt("new");
            this.totalpoints = innerjson.getInt("total");
            return true;
        } catch (JSONException e) {
            Toast.makeText(this.ctx, "There was a communication problem. Please try again later.", (int) BufferRecycler.DEFAULT_WRITE_CONCAT_BUFFER_LEN).show();
            return false;
        }
    }

    public int getNewPoints() {
        return this.newpoints;
    }

    public int getTotalPoints() {
        return this.totalpoints;
    }
}
