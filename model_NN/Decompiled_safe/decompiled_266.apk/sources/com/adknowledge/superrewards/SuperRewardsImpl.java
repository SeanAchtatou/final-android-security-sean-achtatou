package com.adknowledge.superrewards;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import com.adknowledge.superrewards.model.SROffer;
import com.adknowledge.superrewards.model.SRParams;
import com.adknowledge.superrewards.ui.activities.SRPaymentMethodsActivity;
import com.adknowledge.superrewards.web.SRClient;
import com.adknowledge.superrewards.web.SRRequest;
import com.adknowledge.superrewards.xml.parser.OffersXMLParser;
import java.io.IOException;
import java.util.List;
import org.codehaus.jackson.util.BufferRecycler;

public class SuperRewardsImpl implements SuperRewards {
    private static List<SROffer> list = null;
    private String xmlresponse;

    public SuperRewardsImpl() {
    }

    public SuperRewardsImpl(Class<? extends Object> generatedResourceClass) {
        ResourceDelegator.delegateValues(generatedResourceClass, SRResources.class);
    }

    public List<SROffer> getOffers(String h, String uid, String cc, String xml, String v, String nOffers, String mode, Context ctx) {
        if (list != null) {
            return list;
        }
        SRClient.uid = uid;
        if (Utils.checkCacheTimestamp(ctx)) {
            this.xmlresponse = Utils.getXMLFromPrefs(ctx);
            Log.i("SR", "XML retrieved FROM CACHE, passing to parse()");
        } else {
            SRRequest request = SRClient.getInstance().createRequest();
            request.setCommand(SRRequest.Command.METHOD);
            request.setCall(SRRequest.Call.GET_OFFERS);
            request.addInnerParam("h", h);
            request.addInnerParam("uid", uid);
            request.addInnerParam("cc", cc);
            request.addInnerParam("xml", xml);
            request.addInnerParam("mobile", "true");
            request.addInnerParam(SRParams.V, v);
            request.setUid(uid);
            if (!TextUtils.isEmpty(nOffers)) {
                request.addInnerParam("nOffers", nOffers);
            }
            if (!TextUtils.isEmpty(mode)) {
                request.addInnerParam("mode", mode);
            }
            if (!request.execute(ctx, h)) {
                return null;
            }
            Log.i("SR", "XML retrieved FROM SR, passing to parse()");
            this.xmlresponse = request.getResult();
            try {
                Utils.setXMLIntoPrefs(ctx, this.xmlresponse);
            } catch (IOException e) {
                Toast.makeText(ctx, "There was a communication problem. Please try again later.", (int) BufferRecycler.DEFAULT_WRITE_CONCAT_BUFFER_LEN).show();
            }
        }
        list = new OffersXMLParser().parse(this.xmlresponse);
        return list;
    }

    public List<SROffer> getOffers(String h, String uid) {
        return getOffers(h, uid, Utils.getCountryCode(), "1", "2", null, null, null);
    }

    public List<SROffer> getOffers(String h, Context context) {
        return getOffers(h, Utils.getDeviceUID(context));
    }

    public void showOffers(Activity parentActivity, String h, String uid, String cc) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.putExtra("h", h);
        intent.putExtra("uid", uid);
        intent.putExtra("cc", cc);
        intent.setClass(parentActivity, SRPaymentMethodsActivity.class);
        parentActivity.startActivity(intent);
    }

    public void showOffers(Activity parentActivity, String h, String uid) {
        showOffers(parentActivity, h, uid, Utils.getCountryCode());
    }

    public void showOffers(Activity parentActivity, String h) {
        showOffers(parentActivity, h, Utils.getDeviceUID(parentActivity.getApplicationContext()));
    }
}
