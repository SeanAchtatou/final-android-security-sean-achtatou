package com.adknowledge.superrewards;

import java.lang.reflect.Field;

public class ResourceDelegator {
    /* JADX INFO: Multiple debug info for r15v1 java.lang.Class<?>[]: [D('toClass' java.lang.Class<?>), D('arr$' java.lang.Class[])] */
    /* JADX INFO: Multiple debug info for r1v3 int: [D('i$' int), D('arr$' java.lang.reflect.Field[])] */
    /* JADX INFO: Multiple debug info for r1v5 java.lang.reflect.Field[]: [D('toSubClassFields' java.lang.reflect.Field[]), D('arr$' java.lang.reflect.Field[])] */
    public static void delegateValues(Class<?> fromClass, Class<?> toClass) {
        for (Class cls : toClass.getClasses()) {
            for (Class cls2 : fromClass.getClasses()) {
                if (cls.getSimpleName().equals(cls2.getSimpleName())) {
                    for (Field toSubClassField : cls.getDeclaredFields()) {
                        Field fromSubClassField = null;
                        try {
                            fromSubClassField = cls2.getField(toSubClassField.getName());
                        } catch (NoSuchFieldException | SecurityException e) {
                        }
                        if (fromSubClassField != null && fromSubClassField.getType().equals(toSubClassField.getType())) {
                            try {
                                toSubClassField.set(toSubClassField, fromSubClassField.get(cls2));
                            } catch (IllegalAccessException | IllegalArgumentException e2) {
                            }
                        }
                    }
                }
            }
        }
    }
}
