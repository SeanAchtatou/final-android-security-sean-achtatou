package com.adknowledge.superrewards.web;

import java.io.IOException;
import javax.net.ssl.SSLHandshakeException;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.HttpVersion;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HttpContext;

public class SRClient {
    private static SRClient instance;
    public static String uid;
    protected DefaultHttpClient httpClient = getClient();
    HttpRequestRetryHandler myRetryHandler = new HttpRequestRetryHandler() {
        public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
            boolean idempotent;
            if (executionCount >= 5) {
                return false;
            }
            if (exception instanceof NoHttpResponseException) {
                return true;
            }
            if (exception instanceof SSLHandshakeException) {
                return false;
            }
            if (!(((HttpRequest) context.getAttribute("http.request")) instanceof HttpEntityEnclosingRequest)) {
                idempotent = true;
            } else {
                idempotent = false;
            }
            if (idempotent) {
                return true;
            }
            return false;
        }
    };

    private SRClient() {
    }

    public static SRClient getInstance() {
        if (instance == null) {
            instance = new SRClient();
        }
        return instance;
    }

    public SRRequest createRequest() {
        return new SRRequest(this.httpClient);
    }

    public DefaultHttpClient getClient() {
        HttpParams params = new BasicHttpParams();
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(params, "utf-8");
        params.setBooleanParameter("http.protocol.expect-continue", false);
        SchemeRegistry registry = new SchemeRegistry();
        registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        SSLSocketFactory sslSocketFactory = SSLSocketFactory.getSocketFactory();
        sslSocketFactory.setHostnameVerifier(SSLSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
        registry.register(new Scheme("https", sslSocketFactory, 443));
        DefaultHttpClient ret = new DefaultHttpClient(new ThreadSafeClientConnManager(params, registry), params);
        ret.setHttpRequestRetryHandler(this.myRetryHandler);
        return ret;
    }
}
