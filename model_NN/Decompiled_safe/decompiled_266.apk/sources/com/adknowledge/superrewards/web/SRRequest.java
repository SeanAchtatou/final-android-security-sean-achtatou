package com.adknowledge.superrewards.web;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.adknowledge.superrewards.Utils;
import com.adknowledge.superrewards.tracking.SRAppInstallTracker;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.util.BufferRecycler;
import org.json.JSONException;
import org.json.JSONObject;

public class SRRequest {
    public static HttpPost finalRequest;
    private String call;
    private String command;
    protected HttpGet getRequest;
    protected DefaultHttpClient httpClient;
    public HashMap<String, String> innerparams = new HashMap<>();
    protected String method;
    protected List<NameValuePair> params;
    protected HttpPost postRequest;
    protected HttpResponse response;
    protected String result;
    protected InputStream streamresult;
    protected String type = "none";
    private String uid = "";
    protected String url = "http://super.kitnmedia.com/mobile/rpc";

    public enum Call {
        GET_OFFERS,
        INSTALL,
        CLICK,
        GET_SUB,
        CHECK_POINTS
    }

    public enum Command {
        METHOD,
        PARAMS,
        DEVICE,
        RESPONSE,
        GEO
    }

    public SRRequest(DefaultHttpClient client) {
        this.httpClient = client;
        this.params = new ArrayList(2);
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setMethod(String method2) {
        this.method = method2;
    }

    public void setCommand(Command command2) {
        this.command = command2.toString().toLowerCase();
    }

    public void setCall(Call call2) {
        this.call = call2.toString().toLowerCase();
    }

    public String getMethod() {
        return this.method;
    }

    public void addParam(String key, String value) {
        this.params.add(new BasicNameValuePair(key, value));
    }

    public void setUid(String uid2) {
        this.uid = uid2;
    }

    public void addInnerParam(String key, String value) {
        this.innerparams.put(key, value);
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public void setMap(HashMap<String, String> map) {
        this.innerparams = map;
    }

    public String getType() {
        return this.type;
    }

    public String getResult() {
        return this.result;
    }

    public InputStream getStream() {
        return this.streamresult;
    }

    public String toJSON(Context ctx, String h) {
        JSONObject outer = new JSONObject();
        JSONObject inner = new JSONObject();
        JSONObject geo = new JSONObject();
        JSONObject parameters = new JSONObject();
        JSONObject device = new JSONObject();
        try {
            outer.put(Command.PARAMS.toString().toLowerCase(), inner);
            inner.put(Command.GEO.toString().toLowerCase(), geo);
            inner.put(Command.DEVICE.toString().toLowerCase(), device);
            inner.put(this.call, parameters);
            inner.put("h", h);
            Log.i("SR", "Inserted h param " + h);
            if (SRClient.uid != null) {
                inner.put("uid", SRClient.uid);
            } else if (!this.uid.equals("")) {
                inner.put("uid", this.uid);
            }
            SRAppInstallTracker instance = SRAppInstallTracker.getInstance(ctx, h);
            Map<?, ?> params2 = SRAppInstallTracker.map;
            if (params2.size() > 0) {
                for (Map.Entry pairs : params2.entrySet()) {
                    if (pairs.getKey().toString().equals("ip")) {
                        geo.put("ip", pairs.getValue().toString());
                    }
                    if (pairs.getKey().toString().equals("cc")) {
                        geo.put("cc", pairs.getValue().toString());
                    }
                    device.put((String) pairs.getKey(), (String) pairs.getValue());
                    if (this.call.equalsIgnoreCase(Call.INSTALL.toString())) {
                        parameters.put((String) pairs.getKey(), (String) pairs.getValue());
                    }
                }
            }
            if (parameters.names() == null) {
                for (Map.Entry pairs2 : this.innerparams.entrySet()) {
                    parameters.put((String) pairs2.getKey(), (String) pairs2.getValue());
                }
            }
            outer.put(this.command, this.call);
        } catch (JSONException e) {
            Toast.makeText(ctx, "There was a communication problem. Please try again later.", (int) BufferRecycler.DEFAULT_WRITE_CONCAT_BUFFER_LEN).show();
        }
        return outer.toString();
    }

    public boolean execute(Context ctx, String h) {
        try {
            addParam("json", toJSON(ctx, h));
            this.postRequest = new HttpPost(getUrl());
            this.postRequest.setEntity(new UrlEncodedFormEntity(this.params));
            this.response = this.httpClient.execute(this.postRequest);
            HttpEntity entity = this.response.getEntity();
            if (entity != null) {
                this.result = new JSONObject(Utils.inputStreamToString(new BufferedHttpEntity(entity).getContent())).getString("response");
            }
            return true;
        } catch (ClientProtocolException e) {
            return false;
        } catch (IOException e2) {
            return false;
        } catch (JSONException e3) {
            return false;
        } catch (Exception e4) {
            return false;
        }
    }

    public Void executeToStream(Context ctx, String h) {
        try {
            String json = toJSON(ctx, h);
            this.getRequest = new HttpGet();
            this.getRequest.setURI(new URI(this.url + "?json=" + json));
            HttpEntity entity = this.response.getEntity();
            if (entity == null) {
                return null;
            }
            this.streamresult = new BufferedHttpEntity(entity).getContent();
            return null;
        } catch (ClientProtocolException e) {
            Toast.makeText(ctx, "There was a communication problem. Please try again later.", (int) BufferRecycler.DEFAULT_WRITE_CONCAT_BUFFER_LEN).show();
            return null;
        } catch (IOException e2) {
            Toast.makeText(ctx, "There was a communication problem. Please try again later.", (int) BufferRecycler.DEFAULT_WRITE_CONCAT_BUFFER_LEN).show();
            return null;
        } catch (URISyntaxException e3) {
            Toast.makeText(ctx, "There was a communication problem. Please try again later.", (int) BufferRecycler.DEFAULT_WRITE_CONCAT_BUFFER_LEN).show();
            return null;
        }
    }

    public String getUrlWithJson(Context ctx, String h) {
        return this.url + "?json=" + URLEncoder.encode(toJSON(ctx, h));
    }
}
