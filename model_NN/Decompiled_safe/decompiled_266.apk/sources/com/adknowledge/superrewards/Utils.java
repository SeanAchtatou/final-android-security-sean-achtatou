package com.adknowledge.superrewards;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Locale;

public class Utils {
    public static final String HAS_RUN_KEY = "superrewards_has_run";
    public static final String SR_PREF_FILE = "superrewards_prefs";
    public static final String TIMESTAMP_KEY = "superrewards_timestamp_xml";
    public static final String XML_KEY = "superrewards_xml";

    public static String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (enumIpAddr.hasMoreElements()) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e) {
        }
        return null;
    }

    public static String getCountryCode() {
        return Locale.getDefault().getCountry();
    }

    public static boolean isOnline(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService("connectivity");
        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        }
        Log.v("SR", "Internet Connection Not Present");
        return false;
    }

    public static String getLocalIpAddressViaRequest(String url) {
        try {
            return new Socket(url, 80).getLocalAddress().toString();
        } catch (Exception e) {
            return null;
        }
    }

    public static String getDeviceUID(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }

    public static String getAndroidID(Context context) {
        return Settings.System.getString(context.getContentResolver(), "android_id");
    }

    public static Drawable getImage(Context ctx, String url) {
        if (url == null) {
            return null;
        }
        try {
            Log.i("SR", "Getting Image at " + url);
            return Drawable.createFromStream((InputStream) fetch(url), "src");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static Object fetch(String address) throws MalformedURLException, IOException {
        return new URL(address).getContent();
    }

    public static boolean checkCacheTimestamp(Context ctx) {
        if (System.currentTimeMillis() > 900000 + ctx.getSharedPreferences(SR_PREF_FILE, 0).getLong(TIMESTAMP_KEY, 0)) {
            return false;
        }
        return true;
    }

    public static String inputStreamToString(InputStream is) throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        StringBuilder total = new StringBuilder();
        while (true) {
            String line = r.readLine();
            if (line == null) {
                return total.toString();
            }
            total.append(line);
        }
    }

    public static ByteArrayInputStream stringToInputStream(String str) {
        return new ByteArrayInputStream(str.getBytes());
    }

    public static String getXMLFromPrefs(Context ctx) {
        Log.i("SR", "In GetXMLFromPrefs");
        return ctx.getSharedPreferences(SR_PREF_FILE, 0).getString(XML_KEY, "none");
    }

    public static void setXMLIntoPrefs(Context ctx, String xml) throws IOException {
        ctx.getSharedPreferences(SR_PREF_FILE, 0).edit().putString(XML_KEY, xml).putLong(TIMESTAMP_KEY, System.currentTimeMillis()).commit();
    }

    public static boolean checkRunFlag(Context ctx) {
        if (!ctx.getSharedPreferences(SR_PREF_FILE, 0).getBoolean(HAS_RUN_KEY, false)) {
            return false;
        }
        return true;
    }

    public static void setRunFlag(Context ctx) {
        ctx.getSharedPreferences(SR_PREF_FILE, 0).edit().putBoolean(HAS_RUN_KEY, true).commit();
    }

    public static void clearCache(Context ctx) {
        ctx.getSharedPreferences(SR_PREF_FILE, 0).edit().clear().commit();
    }
}
