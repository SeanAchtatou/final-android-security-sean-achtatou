package com.inmobi.androidsdk.impl;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.inmobi.androidsdk.InMobiAdDelegate;
import com.inmobi.androidsdk.impl.AdUnit;
import com.inmobi.androidsdk.impl.net.ConnectionException;
import com.inmobi.androidsdk.impl.net.RequestResponseManager;
import com.openfeint.internal.request.multipart.StringPart;
import java.util.Calendar;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

public final class InMobiAdView extends RelativeLayout implements Animation.AnimationListener, View.OnTouchListener {
    private static volatile boolean adRedirectionInProgress = false;
    private BackgroundWorker adFetchWorker;
    private String adUnit = null;
    private Animation animFirstHalf;
    private Animation animSecHalf;
    private Context appContext;
    private AdUnit currentAdUnit = null;
    private BackgroundWorker deviceDataUploadWorker;
    private DisplayMetrics displayMetrics = null;
    private AtomicBoolean downloadingNewAd = new AtomicBoolean();
    private Timer gpsRefreshTimer;
    private LinearLayout hideWebView;
    private InMobiAdDelegate inmobiAdDelegate = null;
    private boolean isFinished = false;
    private boolean loadingWebView1 = true;
    private boolean notificationEnabled = true;
    private Activity parentActivity;
    private AtomicBoolean processingAdClick = new AtomicBoolean();
    /* access modifiers changed from: private */
    public UserInfo userInfo;
    private WebView webView1;
    private WebView webView2;
    private InMobiWebViewClient webViewClient;

    public static InMobiAdView requestAdUnitWithDelegate(Context context, InMobiAdDelegate delegate, Activity parentActivity2, int adSlot) {
        return new InMobiAdView(context, delegate, parentActivity2, adSlot);
    }

    private InMobiAdView(Context context) {
        super(context);
    }

    private InMobiAdView(Context context, InMobiAdDelegate delegate, Activity activity, int adSlot) {
        super(context);
        initialize(context, delegate, activity, adSlot);
    }

    public InMobiAdView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setRefTagParam(String key, String value) {
        getUserInfo().setRefTagKey(key);
        getUserInfo().setRefTagValue(value);
    }

    public Map<String, String> getRequestParams() {
        return getUserInfo().getRequestParams();
    }

    public void setRequestParams(Map<String, String> requestParams) {
        getUserInfo().setRequestParams(requestParams);
    }

    public void initialize(Context context, InMobiAdDelegate delegate, Activity activity, int adSlot) {
        if (context == null || delegate == null || activity == null || adSlot < 0) {
            throw new NullPointerException("Valid references must be passed to InMobiAdView");
        } else if (delegate.siteId() == null) {
            throw new NullPointerException("site-id cannot be null");
        } else if (delegate.siteId().trim().equalsIgnoreCase("")) {
            throw new IllegalArgumentException("site-id is invalid");
        } else {
            this.appContext = context;
            this.inmobiAdDelegate = delegate;
            this.parentActivity = activity;
            this.adUnit = String.valueOf(adSlot);
            DisplayMetrics metrics = new DisplayMetrics();
            this.parentActivity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            setDisplayMetrics(metrics);
            this.webViewClient = new InMobiWebViewClient(this, null);
            if (this.webView1 == null) {
                this.webView1 = initializeWebView();
            }
            if (this.webView2 == null) {
                this.webView2 = initializeWebView();
                addView(this.webView2);
            }
            if (this.hideWebView == null) {
                this.hideWebView = new LinearLayout(this.appContext);
                this.hideWebView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
                this.hideWebView.setOnTouchListener(this);
                this.hideWebView.setBackgroundColor(0);
                addView(this.hideWebView);
            }
            if (this.userInfo == null) {
                this.userInfo = new UserInfo(getAppContext());
                this.userInfo.getPhoneDefaultUserAgent();
            }
            this.userInfo.setAdUnitSlot(this.adUnit);
            float density = getDisplayMetrics().density;
            Display display = ((WindowManager) this.parentActivity.getSystemService("window")).getDefaultDisplay();
            int width = display.getWidth();
            int height = display.getHeight();
            this.userInfo.setScreenDensity(String.valueOf(density));
            this.userInfo.setScreenSize(width + "X" + height);
        }
    }

    private WebView initializeWebView() {
        WebView webview = new WebView(this.appContext);
        webview.setVerticalScrollBarEnabled(false);
        webview.setHorizontalScrollBarEnabled(false);
        webview.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        webview.setWebViewClient(this.webViewClient);
        webview.setBackgroundColor(0);
        webview.setOnTouchListener(this);
        webview.getSettings().setJavaScriptEnabled(true);
        return webview;
    }

    public void stopReceivingNotifications() {
        if (this.gpsRefreshTimer != null) {
            this.gpsRefreshTimer.cancel();
            this.gpsRefreshTimer = null;
        }
        setNotificationEnabled(false);
    }

    public void startReceivingNotifications() {
        setNotificationEnabled(true);
    }

    /* access modifiers changed from: private */
    public boolean fetchAndDisplayAd() {
        String adServerUrl;
        try {
            if (isProcessingAdClick()) {
                return false;
            }
            setDownloadingNewAd(true);
            RequestResponseManager requestResponseManager = new RequestResponseManager(getAppContext());
            if (getUserInfo().isTestMode()) {
                adServerUrl = Constants.AD_TEST_SERVER_URL;
            } else {
                SharedPreferences prefs = getAppContext().getSharedPreferences("InMobi_Prefs_key", 0);
                String cachedServerUrl = prefs.getString(Constants.AD_SERVER_CACHED_URL, null);
                if (cachedServerUrl != null) {
                    if (Calendar.getInstance().getTimeInMillis() - prefs.getLong(Constants.CACHED_AD_SERVER_TIMESTAMP, 0) > prefs.getLong(Constants.AD_SERVER_CACHED_LIFE, 0)) {
                        adServerUrl = Constants.AD_SERVER_URL;
                    } else {
                        adServerUrl = cachedServerUrl;
                    }
                } else {
                    adServerUrl = Constants.AD_SERVER_URL;
                }
            }
            AdUnit newAd = requestResponseManager.requestAd(adServerUrl, this.userInfo, RequestResponseManager.ActionType.AdRequest);
            if (newAd == null || AdUnit.AdTypes.AdType_None == newAd.getAdType() || newAd.getCDATABlock() == null) {
                return false;
            }
            setCurrentAdUnit(newAd);
            scheduleDeviceInfoUpload(newAd);
            String htmlString = getCurrentAdUnit().getCDATABlock().replaceAll("%", "%25");
            if (isLoadingWebView1()) {
                this.webView1.loadData("<html><meta http-equiv=\"Content-Type\" content=\"text/html charset=utf-16le\"><body>" + htmlString + "</body></html>", StringPart.DEFAULT_CONTENT_TYPE, null);
            } else {
                this.webView2.loadData("<html><meta http-equiv=\"Content-Type\" content=\"text/html charset=utf-16le\"><body>" + htmlString + "</body></html>", StringPart.DEFAULT_CONTENT_TYPE, null);
            }
            this.isFinished = false;
            while (!this.isFinished) {
                try {
                    Thread.sleep(500);
                    if (this.webView1.getProgress() >= 100) {
                        this.isFinished = true;
                    }
                } catch (InterruptedException e) {
                    Log.w(Constants.LOGGING_TAG, "Interrupted exception while retrieving ad", e);
                }
            }
            this.parentActivity.runOnUiThread(new Runnable() {
                public void run() {
                }
            });
            return true;
        } catch (ConnectionException e2) {
            Log.w(Constants.LOGGING_TAG, "Exception retrieving ad", e2);
            return false;
        } catch (AdConstructionException e3) {
            Log.w(Constants.LOGGING_TAG, "Exception constructing ad from response", e3);
            return false;
        } catch (Exception e4) {
            Log.w(Constants.LOGGING_TAG, "Exception fetching and displaying ad", e4);
            return false;
        }
    }

    private void updateUserInfo() {
        if (this.userInfo == null) {
            this.userInfo = new UserInfo(getAppContext());
        }
        if (this.gpsRefreshTimer == null) {
            this.gpsRefreshTimer = new Timer();
            this.gpsRefreshTimer.schedule(new LocationRefreshTimerTask(this, null), 300000, 300000);
        }
        this.userInfo.updateInfo(getInMobiAdDelegate());
    }

    private void setupAdRequest() {
        new Thread() {
            public void run() {
                if (InMobiAdView.this.fetchAndDisplayAd()) {
                    this.getParentActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            InMobiAdView.this.performAdSuccessNotification();
                            InMobiAdView.this.updateAdViewForAdUnit();
                        }
                    });
                } else {
                    this.getParentActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            InMobiAdView.this.performAdFailedNotification();
                            InMobiAdView.this.setDownloadingNewAd(false);
                        }
                    });
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void updateAdViewForAdUnit() {
        Rotate3dAnimation rotation1 = new Rotate3dAnimation(0.0f, 90.0f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, 0.0f, true);
        rotation1.setDuration(500);
        rotation1.setFillAfter(false);
        rotation1.setAnimationListener(this);
        rotation1.setInterpolator(new AccelerateInterpolator());
        Rotate3dAnimation rotation2 = new Rotate3dAnimation(270.0f, 360.0f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, 0.0f, true);
        rotation2.setDuration(500);
        rotation2.setFillAfter(false);
        rotation2.setAnimationListener(this);
        rotation2.setInterpolator(new DecelerateInterpolator());
        setAnimFirstHalf(rotation1);
        setAnimSecHalf(rotation2);
        startAnimation(getAnimFirstHalf());
    }

    private void scheduleDeviceInfoUpload(AdUnit newAd) {
        if (newAd.isSendDeviceInfo()) {
            final String uploadURL = newAd.getDeviceInfoUploadUrl();
            if (uploadURL != null) {
                this.deviceDataUploadWorker.getMessageHandler().post(new Runnable() {
                    public void run() {
                        InMobiAdView.this.startDeviceInfoUpload(uploadURL);
                    }
                });
            }
            newAd.setSendDeviceInfo(false);
            newAd.setDeviceInfoUploadUrl(null);
        }
    }

    /* access modifiers changed from: package-private */
    public void startDeviceInfoUpload(String url) {
        try {
            new RequestResponseManager(getAppContext()).uploadDeviceInfo(url, getUserInfo(), RequestResponseManager.ActionType.DeviceInfoUpload);
        } catch (ConnectionException e) {
        }
    }

    public void loadNewAd() {
        if (isDownloadingNewAd() || isProcessingAdClick() || !isNotificationEnabled()) {
            Log.v(Constants.LOGGING_TAG, "Your request cannot be processed at this time.Please try again later");
            performAdFailedNotification();
            return;
        }
        if (getCurrentAdUnit() != null && getCurrentAdUnit().getAdActionType() == AdUnit.AdActionTypes.AdActionType_Search) {
            ((InputMethodManager) getAppContext().getSystemService("input_method")).hideSoftInputFromWindow(getApplicationWindowToken(), 0);
        }
        setDownloadingNewAd(true);
        updateUserInfo();
        setupAdRequest();
    }

    private void startBackgroundWorkers() {
        if (this.adFetchWorker == null) {
            this.adFetchWorker = new BackgroundWorker();
            this.adFetchWorker.start();
        }
        if (this.deviceDataUploadWorker == null) {
            this.deviceDataUploadWorker = new BackgroundWorker();
            this.deviceDataUploadWorker.start();
        }
    }

    /* access modifiers changed from: package-private */
    public InMobiAdDelegate getInMobiAdDelegate() {
        return this.inmobiAdDelegate;
    }

    /* access modifiers changed from: package-private */
    public void setInMobiAdDelegate(InMobiAdDelegate InMobiAdDelegate) {
        this.inmobiAdDelegate = InMobiAdDelegate;
    }

    /* access modifiers changed from: package-private */
    public String getAdUnit() {
        return this.adUnit;
    }

    /* access modifiers changed from: package-private */
    public void setAdUnit(String adUnit2) {
        this.adUnit = adUnit2;
    }

    /* access modifiers changed from: package-private */
    public boolean isFinished() {
        return this.isFinished;
    }

    /* access modifiers changed from: package-private */
    public void setFinished(boolean isFinished2) {
        this.isFinished = isFinished2;
    }

    /* access modifiers changed from: package-private */
    public UserInfo getUserInfo() {
        return this.userInfo;
    }

    /* access modifiers changed from: package-private */
    public void setUserInfo(UserInfo userInfo2) {
        this.userInfo = userInfo2;
    }

    /* access modifiers changed from: package-private */
    public boolean isDownloadingNewAd() {
        return this.downloadingNewAd.get();
    }

    /* access modifiers changed from: package-private */
    public void setDownloadingNewAd(boolean downloadingNewAd2) {
        this.downloadingNewAd.set(downloadingNewAd2);
    }

    /* access modifiers changed from: package-private */
    public boolean isProcessingAdClick() {
        return this.processingAdClick.get();
    }

    /* access modifiers changed from: package-private */
    public void setProcessingAdClick(boolean processingAdClick2) {
        this.processingAdClick.set(processingAdClick2);
    }

    /* access modifiers changed from: package-private */
    public Activity getParentActivity() {
        return this.parentActivity;
    }

    /* access modifiers changed from: package-private */
    public void setParentActivity(Activity parentActivity2) {
        this.parentActivity = parentActivity2;
    }

    /* access modifiers changed from: package-private */
    public Context getAppContext() {
        return this.appContext;
    }

    /* access modifiers changed from: package-private */
    public void setAppContext(Context appContext2) {
        this.appContext = appContext2;
    }

    /* access modifiers changed from: package-private */
    public InMobiAdDelegate getInmobiAdDelegate() {
        return this.inmobiAdDelegate;
    }

    /* access modifiers changed from: package-private */
    public void setInmobiAdDelegate(InMobiAdDelegate inmobiAdDelegate2) {
        this.inmobiAdDelegate = inmobiAdDelegate2;
    }

    /* access modifiers changed from: package-private */
    public AtomicBoolean getDownloadingNewAd() {
        return this.downloadingNewAd;
    }

    /* access modifiers changed from: package-private */
    public void setDownloadingNewAd(AtomicBoolean downloadingNewAd2) {
        this.downloadingNewAd = downloadingNewAd2;
    }

    /* access modifiers changed from: package-private */
    public boolean isNotificationEnabled() {
        return this.notificationEnabled;
    }

    /* access modifiers changed from: package-private */
    public void setNotificationEnabled(boolean notificationEnabled2) {
        this.notificationEnabled = notificationEnabled2;
    }

    /* access modifiers changed from: package-private */
    public boolean isLoadingWebView1() {
        return this.loadingWebView1;
    }

    /* access modifiers changed from: package-private */
    public void setLoadingWebView1(boolean loadingWebView12) {
        this.loadingWebView1 = loadingWebView12;
    }

    /* access modifiers changed from: package-private */
    public Animation getAnimFirstHalf() {
        return this.animFirstHalf;
    }

    /* access modifiers changed from: package-private */
    public void setAnimFirstHalf(Animation animFirstHalf2) {
        this.animFirstHalf = animFirstHalf2;
    }

    /* access modifiers changed from: package-private */
    public Animation getAnimSecHalf() {
        return this.animSecHalf;
    }

    /* access modifiers changed from: package-private */
    public void setAnimSecHalf(Animation animSecHalf2) {
        this.animSecHalf = animSecHalf2;
    }

    /* access modifiers changed from: package-private */
    public AdUnit getCurrentAdUnit() {
        return this.currentAdUnit;
    }

    /* access modifiers changed from: package-private */
    public void setCurrentAdUnit(AdUnit currentAdUnit2) {
        this.currentAdUnit = currentAdUnit2;
    }

    static boolean isAdRedirectionInProgress() {
        return adRedirectionInProgress;
    }

    static void setAdRedirectionInProgress(boolean adRedirectionInProgress2) {
        adRedirectionInProgress = adRedirectionInProgress2;
    }

    /* access modifiers changed from: package-private */
    public DisplayMetrics getDisplayMetrics() {
        return this.displayMetrics;
    }

    /* access modifiers changed from: package-private */
    public void setDisplayMetrics(DisplayMetrics displayMetrics2) {
        this.displayMetrics = displayMetrics2;
    }

    /* access modifiers changed from: private */
    public void performAdFailedNotification() {
        this.inmobiAdDelegate.adRequestFailed(this);
    }

    /* access modifiers changed from: private */
    public void performAdSuccessNotification() {
        this.inmobiAdDelegate.adRequestCompleted(this);
    }

    public void onAnimationEnd(Animation anim) {
        if (anim.equals(getAnimFirstHalf())) {
            removeAllViews();
            if (isLoadingWebView1()) {
                addView(this.webView1);
            } else {
                addView(this.webView2);
            }
            if (getCurrentAdUnit().getAdActionType() != AdUnit.AdActionTypes.AdActionType_Search) {
                addView(this.hideWebView);
            }
            startAnimation(getAnimSecHalf());
            return;
        }
        setLoadingWebView1(!isLoadingWebView1());
        setDownloadingNewAd(false);
        setNormalBGColor();
    }

    public void onAnimationRepeat(Animation anim) {
    }

    public void onAnimationStart(Animation anim) {
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (v.equals(this.webView1) || v.equals(this.webView2)) {
            v.requestFocusFromTouch();
            return false;
        } else if (event.getAction() == 1) {
            setNormalBGColor();
            adClicked();
            return true;
        } else if (event.getAction() != 0) {
            if (event.getAction() == 3) {
                setNormalBGColor();
            } else if (event.getAction() == 4) {
                setNormalBGColor();
            }
            return false;
        } else if (isProcessingAdClick() || isDownloadingNewAd() || isAdRedirectionInProgress() || getCurrentAdUnit() == null) {
            setNormalBGColor();
            return false;
        } else {
            setHighlightedBGColor();
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void adClicked() {
        try {
            if (getCurrentAdUnit() != null) {
                if (!isAdRedirectionInProgress()) {
                    setAdRedirectionInProgress(true);
                    if (!isProcessingAdClick()) {
                        setProcessingAdClick(true);
                        if (getCurrentAdUnit().getTargetUrl() != null) {
                            new ClickProcessingTask(this, getCurrentAdUnit(), getUserInfo(), getAppContext()).execute(null);
                        }
                    }
                } else {
                    return;
                }
            }
        } catch (Exception e) {
            Log.w(Constants.LOGGING_TAG, "Exception processing ad click", e);
            setProcessingAdClick(false);
            setAdRedirectionInProgress(false);
        }
        setNormalBGColor();
    }

    private void setHighlightedBGColor() {
        int bgColor = Color.argb(100, 0, 0, 0);
        this.webView2.setBackgroundColor(bgColor);
        this.webView1.setBackgroundColor(bgColor);
        this.hideWebView.setBackgroundColor(bgColor);
    }

    private void setNormalBGColor() {
        this.webView1.setBackgroundColor(0);
        this.webView2.setBackgroundColor(0);
        this.hideWebView.setBackgroundColor(0);
    }

    private class LocationRefreshTimerTask extends TimerTask {
        private LocationRefreshTimerTask() {
        }

        /* synthetic */ LocationRefreshTimerTask(InMobiAdView inMobiAdView, LocationRefreshTimerTask locationRefreshTimerTask) {
            this();
        }

        public void run() {
            InMobiAdView.this.userInfo.setValidGeoInfo(false);
            InMobiAdView.this.userInfo.switchOnLocUpdate();
        }
    }

    private class InMobiWebViewClient extends WebViewClient {
        private InMobiWebViewClient() {
        }

        /* synthetic */ InMobiWebViewClient(InMobiAdView inMobiAdView, InMobiWebViewClient inMobiWebViewClient) {
            this();
        }

        public void onLoadResource(WebView view, String url) {
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
        }

        public void onPageFinished(WebView view, String url) {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            String queryString;
            if (InMobiAdView.this.isDownloadingNewAd()) {
                return false;
            }
            view.stopLoading();
            int index = url.indexOf("?");
            if (index > 0 && (queryString = url.substring(index)) != null) {
                AdUnit currentAd = InMobiAdView.this.getCurrentAdUnit();
                currentAd.setTargetUrl(String.valueOf(currentAd.getDefaultTargetUrl()) + queryString);
            }
            InMobiAdView.this.adClicked();
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopReceivingNotifications();
    }
}
