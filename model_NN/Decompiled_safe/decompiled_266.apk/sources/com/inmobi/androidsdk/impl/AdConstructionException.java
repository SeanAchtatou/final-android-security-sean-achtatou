package com.inmobi.androidsdk.impl;

import org.xmlpull.v1.XmlPullParserException;

public final class AdConstructionException extends Exception {
    public AdConstructionException(String string, XmlPullParserException parseException) {
        super(string, parseException);
    }

    public AdConstructionException(String string) {
        super(string);
    }
}
