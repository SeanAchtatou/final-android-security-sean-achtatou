package com.inmobi.androidsdk.impl;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.TextUtils;
import android.webkit.WebView;
import com.inmobi.androidsdk.EducationType;
import com.inmobi.androidsdk.EthnicityType;
import com.inmobi.androidsdk.GenderType;
import com.inmobi.androidsdk.InMobiAdDelegate;
import com.papaya.social.PPYSocial;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public final class UserInfo implements LocationListener {
    private String adUnitSlot = null;
    private int age;
    private String appBId;
    private String appDisplayName;
    private String appVer;
    private Context applicationContext;
    private String areaCode;
    private String currentSDKVersion;
    private String dateOfBirth;
    private String deviceBTHW;
    private String deviceId;
    private String deviceMachineHW;
    private String deviceModel;
    private String deviceName;
    private String deviceStorageSize;
    private String deviceSystemName;
    private String deviceSystemVersion;
    private EducationType education;
    private EthnicityType ethnicity;
    private GenderType gender;
    private int income;
    private String interests;
    private String keywords;
    private long lastLocationUpdateDT = 0;
    private double lat;
    private double locAccuracy;
    private String localization;
    boolean locationDeniedByUser;
    boolean locationInquiryAllowed;
    private LocationManager locationManager;
    boolean locationMethodImplemented;
    private boolean loctionEngineSwitchedON;
    private double lon;
    private String networkType;
    private String phoneDefaultUserAgent;
    private String postalCode;
    private String refTagKey = null;
    private String refTagValue = null;
    private Map<String, String> requestParams;
    private String screenDensity = null;
    private String screenSize = null;
    private String searchString;
    private String siteId;
    private boolean testMode;
    private String testModeAdActionType;
    private boolean validGeoInfo;

    UserInfo(Context appct) {
        this.applicationContext = appct;
    }

    /* access modifiers changed from: package-private */
    public Context getApplicationContext() {
        return this.applicationContext;
    }

    /* access modifiers changed from: package-private */
    public void setApplicationContext(Context applicationContext2) {
        this.applicationContext = applicationContext2;
    }

    public String getAppBId() {
        return this.appBId;
    }

    /* access modifiers changed from: package-private */
    public void setAppBId(String appBId2) {
        this.appBId = appBId2;
    }

    public String getAppDisplayName() {
        return this.appDisplayName;
    }

    /* access modifiers changed from: package-private */
    public void setAppDisplayName(String appDisplayName2) {
        this.appDisplayName = appDisplayName2;
    }

    public String getAppVer() {
        return this.appVer;
    }

    /* access modifiers changed from: package-private */
    public void setAppVer(String appVer2) {
        this.appVer = appVer2;
    }

    public String getNetworkType() {
        return this.networkType;
    }

    /* access modifiers changed from: package-private */
    public void setNetworkType(String networkType2) {
        this.networkType = networkType2;
    }

    public String getDeviceId() {
        return this.deviceId;
    }

    /* access modifiers changed from: package-private */
    public void setDeviceId(String deviceId2) {
        this.deviceId = deviceId2;
    }

    public String getLocalization() {
        return this.localization;
    }

    /* access modifiers changed from: package-private */
    public void setLocalization(String localization2) {
        this.localization = localization2;
    }

    public String getDeviceName() {
        return this.deviceName;
    }

    /* access modifiers changed from: package-private */
    public void setDeviceName(String deviceName2) {
        this.deviceName = deviceName2;
    }

    public String getDeviceModel() {
        return this.deviceModel;
    }

    /* access modifiers changed from: package-private */
    public void setDeviceModel(String deviceModel2) {
        this.deviceModel = deviceModel2;
    }

    public String getDeviceSystemName() {
        return this.deviceSystemName;
    }

    /* access modifiers changed from: package-private */
    public void setDeviceSystemName(String deviceSystemName2) {
        this.deviceSystemName = deviceSystemName2;
    }

    public String getDeviceSystemVersion() {
        return this.deviceSystemVersion;
    }

    /* access modifiers changed from: package-private */
    public void setDeviceSystemVersion(String deviceSystemVersion2) {
        this.deviceSystemVersion = deviceSystemVersion2;
    }

    public String getCurrentSDKVersion() {
        return this.currentSDKVersion;
    }

    /* access modifiers changed from: package-private */
    public void setCurrentSDKVersion(String currentSDKVersion2) {
        this.currentSDKVersion = currentSDKVersion2;
    }

    public Map<String, String> getRequestParams() {
        return this.requestParams;
    }

    public void setRequestParams(Map<String, String> requestParams2) {
        this.requestParams = requestParams2;
    }

    public String getUserAgent() {
        return "InMobi_androidsdk=2.0";
    }

    /* access modifiers changed from: package-private */
    public void setUserAgent(String userAgent) {
    }

    public String getDeviceStorageSize() {
        return this.deviceStorageSize;
    }

    /* access modifiers changed from: package-private */
    public void setDeviceStorageSize(String deviceStorageSize2) {
        this.deviceStorageSize = deviceStorageSize2;
    }

    public String getDeviceMachineHW() {
        return this.deviceMachineHW;
    }

    /* access modifiers changed from: package-private */
    public void setDeviceMachineHW(String deviceMachineHW2) {
        this.deviceMachineHW = deviceMachineHW2;
    }

    public void setDeviceBTHW(String deviceBTHW2) {
        this.deviceBTHW = deviceBTHW2;
    }

    public String getDeviceBTHW() {
        return this.deviceBTHW;
    }

    public String getSiteId() {
        return this.siteId;
    }

    /* access modifiers changed from: package-private */
    public void setSiteId(String siteId2) {
        this.siteId = siteId2;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    /* access modifiers changed from: package-private */
    public void setPostalCode(String postalCode2) {
        this.postalCode = postalCode2;
    }

    public String getAreaCode() {
        return this.areaCode;
    }

    /* access modifiers changed from: package-private */
    public void setAreaCode(String areaCode2) {
        this.areaCode = areaCode2;
    }

    public String getDateOfBirth() {
        return this.dateOfBirth;
    }

    /* access modifiers changed from: package-private */
    public void setDateOfBirth(Date dateOfBirth2) {
        String dob = null;
        if (dateOfBirth2 != null) {
            Calendar cald = Calendar.getInstance();
            cald.setTimeInMillis(dateOfBirth2.getTime());
            dob = String.valueOf(cald.get(1)) + "-" + (cald.get(2) + 1) + "-" + cald.get(5);
        }
        this.dateOfBirth = dob;
    }

    public GenderType getGender() {
        return this.gender;
    }

    /* access modifiers changed from: package-private */
    public void setGender(GenderType gender2) {
        this.gender = gender2;
    }

    public String getKeywords() {
        return this.keywords;
    }

    /* access modifiers changed from: package-private */
    public void setKeywords(String keywords2) {
        this.keywords = keywords2;
    }

    public String getSearchString() {
        return this.searchString;
    }

    /* access modifiers changed from: package-private */
    public void setSearchString(String searchString2) {
        this.searchString = searchString2;
    }

    public int getIncome() {
        return this.income;
    }

    /* access modifiers changed from: package-private */
    public void setIncome(int income2) {
        this.income = income2;
    }

    public EducationType getEducation() {
        return this.education;
    }

    /* access modifiers changed from: package-private */
    public void setEducation(EducationType education2) {
        this.education = education2;
    }

    public EthnicityType getEthnicity() {
        return this.ethnicity;
    }

    /* access modifiers changed from: package-private */
    public void setEthnicity(EthnicityType ethnicity2) {
        this.ethnicity = ethnicity2;
    }

    public int getAge() {
        return this.age;
    }

    /* access modifiers changed from: package-private */
    public void setAge(int age2) {
        this.age = age2;
    }

    public String getInterests() {
        return this.interests;
    }

    /* access modifiers changed from: package-private */
    public void setInterests(String interests2) {
        this.interests = interests2;
    }

    /* access modifiers changed from: package-private */
    public synchronized LocationManager getLocationManager() {
        return this.locationManager;
    }

    /* access modifiers changed from: package-private */
    public synchronized void setLocationManager(LocationManager locationManager2) {
        this.locationManager = locationManager2;
    }

    /* access modifiers changed from: package-private */
    public boolean isLocationInquiryAllowed() {
        return this.locationInquiryAllowed;
    }

    /* access modifiers changed from: package-private */
    public void setLocationInquiryAllowed(boolean locationInquiryAllowed2) {
        this.locationInquiryAllowed = locationInquiryAllowed2;
    }

    /* access modifiers changed from: package-private */
    public boolean isLocationMethodImplemented() {
        return this.locationMethodImplemented;
    }

    /* access modifiers changed from: package-private */
    public void setLocationMethodImplemented(boolean locationMethodImplemented2) {
        this.locationMethodImplemented = locationMethodImplemented2;
    }

    /* access modifiers changed from: package-private */
    public boolean isLocationDeniedByUser() {
        return this.locationDeniedByUser;
    }

    /* access modifiers changed from: package-private */
    public void setLocationDeniedByUser(boolean locationDeniedByUser2) {
        this.locationDeniedByUser = locationDeniedByUser2;
    }

    public double getLat() {
        return this.lat;
    }

    /* access modifiers changed from: package-private */
    public void setLat(double lat2) {
        this.lat = lat2;
    }

    public double getLon() {
        return this.lon;
    }

    /* access modifiers changed from: package-private */
    public void setLon(double lon2) {
        this.lon = lon2;
    }

    public double getLocAccuracy() {
        return this.locAccuracy;
    }

    /* access modifiers changed from: package-private */
    public void setLocAccuracy(double locAccuracy2) {
        this.locAccuracy = locAccuracy2;
    }

    public boolean isValidGeoInfo() {
        return this.validGeoInfo;
    }

    /* access modifiers changed from: package-private */
    public void setValidGeoInfo(boolean validGeoInfo2) {
        this.validGeoInfo = validGeoInfo2;
    }

    /* access modifiers changed from: package-private */
    public long getLastLocationUpdateDT() {
        return this.lastLocationUpdateDT;
    }

    /* access modifiers changed from: package-private */
    public void setLastLocationUpdateDT(long lastLocationUpdateDT2) {
        this.lastLocationUpdateDT = lastLocationUpdateDT2;
    }

    /* access modifiers changed from: package-private */
    public boolean isLoctionEngineSwitchedON() {
        return this.loctionEngineSwitchedON;
    }

    /* access modifiers changed from: package-private */
    public void setLoctionEngineSwitchedON(boolean loctionEngineSwitchedON2) {
        this.loctionEngineSwitchedON = loctionEngineSwitchedON2;
    }

    public boolean isTestMode() {
        return this.testMode;
    }

    /* access modifiers changed from: package-private */
    public void setTestMode(boolean testMode2) {
        this.testMode = testMode2;
    }

    public String getTestModeAdActionType() {
        return this.testModeAdActionType;
    }

    /* access modifiers changed from: package-private */
    public void setTestModeAdActionType(String testModeAdActionType2) {
        this.testModeAdActionType = testModeAdActionType2;
    }

    public String getPhoneDefaultUserAgent() {
        if (TextUtils.isEmpty(this.phoneDefaultUserAgent)) {
            setPhoneDefaultUserAgent(getApplicationContext());
        }
        if (this.phoneDefaultUserAgent == null) {
            return "";
        }
        return this.phoneDefaultUserAgent;
    }

    /* access modifiers changed from: package-private */
    public void setPhoneDefaultUserAgent(Context ctx) {
        try {
            this.phoneDefaultUserAgent = new WebView(ctx).getSettings().getUserAgentString();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void updateInfo(InMobiAdDelegate delegate) {
        fillDeviceInfo();
        setSiteId(delegate.siteId());
        setTestMode(delegate.testMode());
        setPostalCode(delegate.postalCode());
        setAreaCode(delegate.areaCode());
        setDateOfBirth(delegate.dateOfBirth());
        setGender(delegate.gender());
        setKeywords(delegate.keywords());
        setSearchString(delegate.searchString());
        setIncome(delegate.income());
        setEducation(delegate.education());
        setEthnicity(delegate.ethnicity());
        setAge(delegate.age());
        setInterests(delegate.interests());
        setLocationInquiryAllowed(delegate.isLocationInquiryAllowed());
        if (!isLocationInquiryAllowed()) {
            setLocationDeniedByUser(true);
        } else if (delegate.isPublisherProvidingLocation()) {
            fillLocationInfo(delegate.currentLocation());
        } else {
            verifyLocationPermission();
            if (!isLocationDeniedByUser()) {
                switchOnLocUpdate();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void tryLastKnownLocation() {
        double[] loc = getLocationCoordinates();
        if (loc != null) {
            Location newLocation = new Location("last_known_loc");
            newLocation.setLatitude(loc[0]);
            newLocation.setLongitude(loc[1]);
            newLocation.setAccuracy((float) loc[2]);
            newLocation.setTime((long) loc[3]);
            fillLocationInfo(newLocation);
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void switchOnLocUpdate() {
        if (getLocationManager() == null) {
            setLocationManager((LocationManager) getApplicationContext().getSystemService("location"));
        }
        if (getLocationManager() != null) {
            LocationManager locManager = getLocationManager();
            Criteria criteria = new Criteria();
            if (getApplicationContext().checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") != 0) {
                criteria.setAccuracy(2);
            } else if (getApplicationContext().checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") != 0) {
                criteria.setAccuracy(1);
            }
            criteria.setCostAllowed(false);
            String provider = locManager.getBestProvider(criteria, true);
            if (!isValidGeoInfo() && !isLoctionEngineSwitchedON() && !TextUtils.isEmpty(provider) && locManager.isProviderEnabled(provider)) {
                locManager.requestLocationUpdates(provider, 0, 0.0f, this, getApplicationContext().getMainLooper());
                setLoctionEngineSwitchedON(true);
            }
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void switchOffLocUpdate() {
        if (isLoctionEngineSwitchedON()) {
            getLocationManager().removeUpdates(this);
            setLocationManager(null);
            setLoctionEngineSwitchedON(false);
        }
    }

    private void fillLocationInfo(Location newLocation) {
        if (newLocation != null) {
            setValidGeoInfo(true);
            setLat(newLocation.getLatitude());
            setLon(newLocation.getLongitude());
            setLocAccuracy((double) newLocation.getAccuracy());
            setLastLocationUpdateDT(newLocation.getTime());
            switchOffLocUpdate();
        }
    }

    private void verifyLocationPermission() {
        int coarseLocationAccessPermission = getApplicationContext().checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION");
        int fineLocationAccessPermission = getApplicationContext().checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION");
        if (coarseLocationAccessPermission == 0 || fineLocationAccessPermission == 0) {
            setLocationDeniedByUser(false);
        } else {
            setLocationDeniedByUser(true);
        }
    }

    public void onLocationChanged(Location location) {
        fillLocationInfo(location);
    }

    public void onProviderDisabled(String provider) {
        switchOffLocUpdate();
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
        if (status == 0) {
            switchOffLocUpdate();
        }
    }

    private double[] getLocationCoordinates() {
        Location location = null;
        LocationManager locManager = (LocationManager) getApplicationContext().getSystemService("location");
        List<String> providers = locManager.getProviders(true);
        for (int i = providers.size() - 1; i >= 0; i--) {
            String provider = providers.get(i);
            if (locManager.isProviderEnabled(provider) && (location = locManager.getLastKnownLocation(provider)) != null) {
                break;
            }
        }
        return location != null ? new double[]{location.getLatitude(), location.getLongitude(), (double) location.getAccuracy(), (double) location.getTime()} : null;
    }

    private void fillDeviceInfo() {
        String str;
        String str2;
        if (getDeviceId() == null) {
            setDeviceId(getUserId());
            setDeviceName(Build.BRAND);
            setDeviceModel(Build.MODEL);
            if (TextUtils.isEmpty(Build.ID.trim())) {
                str = Build.ID;
            } else {
                str = "BASE";
            }
            setDeviceSystemName(str);
            if (TextUtils.isEmpty(Build.VERSION.RELEASE.trim())) {
                str2 = Build.VERSION.RELEASE;
            } else {
                str2 = "1.0";
            }
            setDeviceSystemVersion(str2);
            long inMem = MemoryStatus.getTotalInternalMemorySize();
            long extMem = MemoryStatus.getTotalExternalMemorySize();
            String deviceStorage = "InBuilt:";
            if (inMem > 0) {
                deviceStorage = String.valueOf(deviceStorage) + MemoryStatus.formatSize(inMem);
            }
            if (extMem > 0) {
                deviceStorage = String.valueOf(deviceStorage) + ",Ext:" + MemoryStatus.formatSize(extMem);
            }
            setDeviceStorageSize(deviceStorage);
            String machineOSHW = (String) System.getProperties().get("os.name");
            String machineVerHW = (String) System.getProperties().get("os.version");
            if (!(machineOSHW == null || machineVerHW == null)) {
                setDeviceMachineHW(String.valueOf(machineOSHW) + "(Android:" + machineVerHW + ")");
            }
            setPhoneDefaultUserAgent(getApplicationContext());
            long sinceBTmilliseconds = SystemClock.elapsedRealtime();
            Calendar cald = Calendar.getInstance();
            cald.setTimeInMillis(System.currentTimeMillis() - sinceBTmilliseconds);
            setDeviceBTHW(cald.getTime().toString());
            Locale linfo = Locale.getDefault();
            String language = linfo.getLanguage();
            if (language != null) {
                language = language.toLowerCase();
                String country = linfo.getCountry();
                if (country != null) {
                    language = String.valueOf(language) + "_" + country.toLowerCase();
                }
            } else {
                String lang = (String) System.getProperties().get("user.language");
                String region = (String) System.getProperties().get("user.region");
                if (!(lang == null || region == null)) {
                    language = String.valueOf(lang) + "_" + region;
                }
                if (language == null) {
                    language = PPYSocial.LANG_EN;
                }
            }
            setLocalization(language);
            setCurrentSDKVersion(Constants.SDK_VERSION);
            StringBuilder uagent = new StringBuilder();
            uagent.append("InMobi_androidwebsdk=");
            uagent.append(Constants.SDK_VERSION);
            uagent.append(" (");
            uagent.append(getDeviceModel());
            uagent.append("; ");
            uagent.append(getDeviceSystemName());
            uagent.append(" ");
            uagent.append(getDeviceSystemVersion());
            uagent.append("; HW ");
            uagent.append(getDeviceMachineHW());
            uagent.append(")");
            setUserAgent(uagent.toString());
            try {
                Context context = getApplicationContext();
                PackageManager manager = context.getPackageManager();
                ApplicationInfo info = manager.getApplicationInfo(context.getPackageName(), 128);
                if (info != null) {
                    setAppBId(info.packageName);
                    setAppDisplayName(info.loadLabel(manager).toString());
                }
                PackageInfo pInfo = manager.getPackageInfo(context.getPackageName(), 128);
                String ver = null;
                if (pInfo != null && ((ver = pInfo.versionName) == null || ver.equals(""))) {
                    ver = new StringBuilder(String.valueOf(pInfo.versionCode)).toString();
                }
                if (ver != null && !ver.equals("")) {
                    setAppVer(ver);
                }
            } catch (Exception e) {
            }
            if (getDeviceId() == null) {
                setDeviceId("");
            }
        }
        String netTypeName = null;
        try {
            ConnectivityManager comMgr = (ConnectivityManager) getApplicationContext().getSystemService("connectivity");
            if (comMgr != null) {
                NetworkInfo netInfo = comMgr.getActiveNetworkInfo();
                int netType = netInfo.getType();
                int netSubType = netInfo.getSubtype();
                if (netType == 1) {
                    netTypeName = "wifi";
                } else if (netType == 0) {
                    netTypeName = "carrier";
                    if (netSubType == 1) {
                        netTypeName = "gprs";
                    } else if (netSubType == 2) {
                        netTypeName = "edge";
                    } else if (netSubType == 3) {
                        netTypeName = "umts";
                    } else if (netSubType == 0) {
                        netTypeName = "carrier";
                    }
                }
            }
        } catch (Exception e2) {
        }
        setNetworkType(netTypeName);
    }

    private String getUserId() {
        String android_id = null;
        try {
            android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), "android_id");
        } catch (Exception e) {
        }
        if (android_id != null) {
            return android_id;
        }
        try {
            return Settings.System.getString(getApplicationContext().getContentResolver(), "android_id");
        } catch (Exception e2) {
            return android_id;
        }
    }

    public String getRefTagKey() {
        return this.refTagKey;
    }

    public void setRefTagKey(String refTagKey2) {
        this.refTagKey = refTagKey2;
    }

    public String getRefTagValue() {
        return this.refTagValue;
    }

    public void setRefTagValue(String refTagValue2) {
        this.refTagValue = refTagValue2;
    }

    public void setPhoneDefaultUserAgent(String phoneDefaultUserAgent2) {
        this.phoneDefaultUserAgent = phoneDefaultUserAgent2;
    }

    public String getAdUnitSlot() {
        return this.adUnitSlot;
    }

    public void setAdUnitSlot(String adUnitSlot2) {
        this.adUnitSlot = adUnitSlot2;
    }

    public void setDateOfBirth(String dateOfBirth2) {
        this.dateOfBirth = dateOfBirth2;
    }

    public String getScreenSize() {
        return this.screenSize;
    }

    public void setScreenSize(String screenSize2) {
        this.screenSize = screenSize2;
    }

    public String getScreenDensity() {
        return this.screenDensity;
    }

    public void setScreenDensity(String screenDensity2) {
        this.screenDensity = screenDensity2;
    }
}
