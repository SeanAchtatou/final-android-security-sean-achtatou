package com.inmobi.androidsdk.impl;

import android.util.Log;
import com.adknowledge.superrewards.model.SROffer;
import java.io.IOException;
import java.io.Reader;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public final class XMLParser {
    private AdUnit ad;

    public AdUnit buildAdUnitFromResponseData(Reader reader) throws IOException, AdConstructionException {
        this.ad = new AdUnit();
        boolean isAdPresent = false;
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(reader);
            int eventType = xpp.getEventType();
            String startName = null;
            while (eventType != 1) {
                if (eventType != 0) {
                    if (eventType == 2) {
                        startName = xpp.getName();
                        if (startName != null && startName.equalsIgnoreCase("Ad")) {
                            isAdPresent = true;
                            this.ad.setWidth(Integer.parseInt(xpp.getAttributeValue(null, "width")));
                            this.ad.setHeight(Integer.parseInt(xpp.getAttributeValue(null, "height")));
                            this.ad.setAdActionType(AdUnit.adActionTypefromString(xpp.getAttributeValue(null, "actionName")));
                            this.ad.setAdType(AdUnit.adTypefromString(xpp.getAttributeValue(null, SROffer.TYPE)));
                        }
                    } else if (eventType == 3) {
                        startName = null;
                    } else if (eventType == 5) {
                        this.ad.setCDATABlock(xpp.getText());
                    } else if (eventType == 4 && startName != null) {
                        if (startName.equalsIgnoreCase("AdURL")) {
                            this.ad.setTargetUrl(xpp.getText());
                            this.ad.setDefaultTargetUrl(xpp.getText());
                        }
                    }
                }
                try {
                    eventType = xpp.nextToken();
                } catch (IOException e) {
                    Log.w(Constants.LOGGING_TAG, "Exception occured while parsing" + e);
                }
            }
            if (!isAdPresent) {
                Log.w(Constants.LOGGING_TAG, "No Fill. Please try again after sometime.");
            }
            return this.ad;
        } catch (XmlPullParserException e2) {
            throw new AdConstructionException("Exception constructing Ad", e2);
        }
    }
}
