package com.inmobi.androidsdk.impl;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

class AdViewCtrlsHolder extends RelativeLayout {
    public AdViewCtrlsHolder(Context context) {
        super(context);
    }

    public AdViewCtrlsHolder(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AdViewCtrlsHolder(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
