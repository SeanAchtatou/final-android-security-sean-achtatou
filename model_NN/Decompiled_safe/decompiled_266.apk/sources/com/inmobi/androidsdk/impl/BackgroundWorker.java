package com.inmobi.androidsdk.impl;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

class BackgroundWorker {
    private Looper looper;
    /* access modifiers changed from: private */
    public Handler messageHandler;

    BackgroundWorker() {
    }

    /* access modifiers changed from: package-private */
    public void start() {
        new Thread(new Runnable() {
            public void run() {
                Looper.prepare();
                BackgroundWorker.this.messageHandler = new Handler() {
                    public void handleMessage(Message msg) {
                        try {
                            ((Runnable) msg.obj).run();
                        } catch (Exception e) {
                            Log.w(Constants.LOGGING_TAG, "Caught exception while downloading and displaying ad", e);
                        }
                    }
                };
                Looper.loop();
            }
        }).start();
    }

    /* access modifiers changed from: package-private */
    public Handler getMessageHandler() {
        return this.messageHandler;
    }

    /* access modifiers changed from: package-private */
    public void shutdown() {
        this.looper.quit();
    }
}
