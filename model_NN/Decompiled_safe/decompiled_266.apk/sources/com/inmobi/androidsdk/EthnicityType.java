package com.inmobi.androidsdk;

public enum EthnicityType {
    Eth_None,
    Eth_Mixed,
    Eth_Asian,
    Eth_Black,
    Eth_Hispanic,
    Eth_NativeAmerican,
    Eth_White,
    Eth_Other
}
