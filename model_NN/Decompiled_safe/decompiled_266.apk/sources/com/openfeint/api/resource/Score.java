package com.openfeint.api.resource;

import com.ansca.corona.R;
import com.openfeint.api.Notification;
import com.openfeint.internal.APICallback;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.notifications.SimpleNotification;
import com.openfeint.internal.offline.OfflineSupport;
import com.openfeint.internal.request.BlobPostRequest;
import com.openfeint.internal.request.CompressedBlobDownloadRequest;
import com.openfeint.internal.request.CompressedBlobPostRequest;
import com.openfeint.internal.request.IRawRequestDelegate;
import com.openfeint.internal.request.JSONRequest;
import com.openfeint.internal.request.OrderedArgList;
import com.openfeint.internal.resource.BlobUploadParameters;
import com.openfeint.internal.resource.DoubleResourceProperty;
import com.openfeint.internal.resource.IntResourceProperty;
import com.openfeint.internal.resource.LongResourceProperty;
import com.openfeint.internal.resource.NestedResourceProperty;
import com.openfeint.internal.resource.Resource;
import com.openfeint.internal.resource.ResourceClass;
import com.openfeint.internal.resource.ScoreBlobDelegate;
import com.openfeint.internal.resource.StringResourceProperty;
import java.util.List;

public class Score extends Resource {
    public byte[] blob;
    /* access modifiers changed from: private */
    public BlobUploadParameters blobUploadParameters;
    /* access modifiers changed from: private */
    public String blobUrl;
    public String customData;
    public String displayText;
    public double latitude;
    public int leaderboardId;
    public double longitude;
    public int rank;
    public long score;
    public User user;

    public static abstract class DownloadBlobCB extends APICallback {
        public abstract void onSuccess();
    }

    public Score(long score2) {
        this.score = score2;
    }

    public Score(long score2, String displayText2) {
        this.score = score2;
        this.displayText = displayText2;
    }

    public boolean hasBlob() {
        return this.blobUrl != null;
    }

    public static abstract class SubmitToCB extends APICallback {
        public abstract void onSuccess(boolean z);

        public void onBlobUploadSuccess() {
        }

        public void onBlobUploadFailure(String exceptionMessage) {
        }
    }

    public void submitTo(Leaderboard leaderboard, SubmitToCB cb) {
        submitToInternal(leaderboard, null, cb, false);
    }

    public void submitToFromOffline(Leaderboard leaderboard, String timestamp, SubmitToCB cb) {
        submitToInternal(leaderboard, timestamp, cb, true);
    }

    private void submitToInternal(Leaderboard leaderboard, String timestamp, SubmitToCB cb, boolean fromOffline) {
        final boolean uploadBlob;
        if (leaderboard == null || leaderboard.resourceID() == null || leaderboard.resourceID().length() == 0) {
            if (cb != null) {
                cb.onFailure("No leaderboard ID provided.  Please provide a leaderboard ID from the Dev Dashboard.");
            }
        } else if (!OpenFeintInternal.getInstance().isUserLoggedIn()) {
            OfflineSupport.postOfflineScore(this, leaderboard);
            if (cb != null) {
                cb.onSuccess(false);
            }
        } else {
            final String path = "/xp/games/" + OpenFeintInternal.getInstance().getAppID() + "/leaderboards/" + leaderboard.resourceID() + "/high_scores";
            OrderedArgList args = new OrderedArgList();
            args.put("high_score[score]", new Long(this.score).toString());
            if (this.displayText != null) {
                args.put("high_score[display_text]", this.displayText);
            }
            if (this.blob != null) {
                uploadBlob = true;
            } else {
                uploadBlob = false;
            }
            args.put("high_score[has_blob]", uploadBlob ? "1" : "0");
            if (timestamp != null) {
                args.put("high_score[timestamp]", timestamp);
            }
            final boolean z = fromOffline;
            final SubmitToCB submitToCB = cb;
            final Leaderboard leaderboard2 = leaderboard;
            new JSONRequest(args) {
                public String method() {
                    return "POST";
                }

                public String path() {
                    return path;
                }

                /* access modifiers changed from: protected */
                public void onResponse(int responseCode, Object responseBody) {
                    if (201 == responseCode) {
                        if (!z) {
                            SimpleNotification.show(OpenFeintInternal.getInstance().getContext().getResources().getString(R.string.of_score_submitted_notification), "@drawable/of_icon_highscore_notification", Notification.Category.HighScore, Notification.Type.Success);
                        }
                        if (submitToCB != null) {
                            submitToCB.onSuccess(true);
                        }
                        perhapsUploadBlob(uploadBlob, responseBody);
                    } else if (200 > responseCode || responseCode >= 300) {
                        if ((responseCode == 0 || 500 <= responseCode) && !z) {
                            OfflineSupport.postOfflineScore(Score.this, leaderboard2);
                            if (submitToCB != null) {
                                submitToCB.onSuccess(false);
                                return;
                            }
                            return;
                        }
                        onFailure(responseBody);
                    } else if (submitToCB != null) {
                        submitToCB.onSuccess(false);
                    }
                }

                private final void perhapsUploadBlob(boolean uploadBlob, Object responseBody) {
                    if (uploadBlob && (responseBody instanceof List)) {
                        Score s = (Score) ((List) responseBody).get(0);
                        BlobPostRequest postRequest = new CompressedBlobPostRequest(s.blobUploadParameters, String.format("blob.%s.bin", s.resourceID()), Score.this.blob);
                        if (submitToCB != null) {
                            postRequest.setDelegate(new IRawRequestDelegate() {
                                public void onResponse(int responseCode, String responseBody) {
                                    if (200 > responseCode || responseCode >= 300) {
                                        submitToCB.onBlobUploadFailure(responseBody);
                                    } else {
                                        submitToCB.onBlobUploadSuccess();
                                    }
                                }
                            });
                        }
                        postRequest.launch();
                    }
                }

                public void onFailure(String exceptionMessage) {
                    super.onFailure(exceptionMessage);
                    if (submitToCB != null) {
                        submitToCB.onFailure(exceptionMessage);
                    }
                }
            }.launch();
        }
    }

    public void downloadBlob(final DownloadBlobCB cb) {
        if (hasBlob()) {
            new CompressedBlobDownloadRequest() {
                public boolean signed() {
                    return false;
                }

                public String url() {
                    return Score.this.blobUrl;
                }

                public String path() {
                    return "";
                }

                /* access modifiers changed from: protected */
                public void onSuccessDecompress(byte[] bodyData) {
                    Score.this.blob = bodyData;
                    if (cb != null) {
                        cb.onSuccess();
                    }
                }

                public void onFailure(String exceptionMessage) {
                    super.onFailure(exceptionMessage);
                    if (cb != null) {
                        cb.onFailure(exceptionMessage);
                    }
                }
            }.launch();
        } else if (cb != null) {
            cb.onFailure(OpenFeintInternal.getRString(R.string.of_no_blob));
        }
    }

    public static abstract class BlobDownloadedDelegate {
        public void blobDownloadedForScore(Score score) {
        }
    }

    public static void setBlobDownloadedDelegate(BlobDownloadedDelegate delegate) {
        ScoreBlobDelegate.sBlobDownloadedDelegate = delegate;
    }

    public Score() {
    }

    public static ResourceClass getResourceClass() {
        ResourceClass klass = new ResourceClass(Score.class, "high_score") {
            public Resource factory() {
                return new Score();
            }
        };
        klass.mProperties.put("score", new LongResourceProperty() {
            public long get(Resource obj) {
                return ((Score) obj).score;
            }

            public void set(Resource obj, long val) {
                ((Score) obj).score = val;
            }
        });
        klass.mProperties.put("rank", new IntResourceProperty() {
            public int get(Resource obj) {
                return ((Score) obj).rank;
            }

            public void set(Resource obj, int val) {
                ((Score) obj).rank = val;
            }
        });
        klass.mProperties.put("leaderboard_id", new IntResourceProperty() {
            public int get(Resource obj) {
                return ((Score) obj).leaderboardId;
            }

            public void set(Resource obj, int val) {
                ((Score) obj).leaderboardId = val;
            }
        });
        klass.mProperties.put("display_text", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((Score) obj).displayText;
            }

            public void set(Resource obj, String val) {
                ((Score) obj).displayText = val;
            }
        });
        klass.mProperties.put("custom_data", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((Score) obj).customData;
            }

            public void set(Resource obj, String val) {
                ((Score) obj).customData = val;
            }
        });
        klass.mProperties.put("lat", new DoubleResourceProperty() {
            public double get(Resource obj) {
                return ((Score) obj).latitude;
            }

            public void set(Resource obj, double val) {
                ((Score) obj).latitude = val;
            }
        });
        klass.mProperties.put("lng", new DoubleResourceProperty() {
            public double get(Resource obj) {
                return ((Score) obj).longitude;
            }

            public void set(Resource obj, double val) {
                ((Score) obj).longitude = val;
            }
        });
        klass.mProperties.put("user", new NestedResourceProperty(User.class) {
            public Resource get(Resource obj) {
                return ((Score) obj).user;
            }

            public void set(Resource obj, Resource val) {
                ((Score) obj).user = (User) val;
            }
        });
        klass.mProperties.put("blob_url", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((Score) obj).blobUrl;
            }

            public void set(Resource obj, String val) {
                String unused = ((Score) obj).blobUrl = val;
            }
        });
        klass.mProperties.put("blob_upload_parameters", new NestedResourceProperty(BlobUploadParameters.class) {
            public Resource get(Resource obj) {
                return ((Score) obj).blobUploadParameters;
            }

            public void set(Resource obj, Resource val) {
                BlobUploadParameters unused = ((Score) obj).blobUploadParameters = (BlobUploadParameters) val;
            }
        });
        return klass;
    }
}
