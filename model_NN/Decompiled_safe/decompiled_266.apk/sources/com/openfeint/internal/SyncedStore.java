package com.openfeint.internal;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class SyncedStore {
    private static final String FILENAME = "of_prefs";
    private static final String TAG = "DistributedPrefs";
    private Context mContext;
    /* access modifiers changed from: private */
    public ReentrantReadWriteLock mLock = new ReentrantReadWriteLock();
    /* access modifiers changed from: private */
    public HashMap<String, String> mMap = new HashMap<>();

    public class Editor {
        public Editor() {
        }

        public void putString(String k, String v) {
            SyncedStore.this.mMap.put(k, v);
        }

        public void remove(String k) {
            SyncedStore.this.mMap.remove(k);
        }

        public Set<String> keySet() {
            return new HashSet(SyncedStore.this.mMap.keySet());
        }

        public void commit() {
            SyncedStore.this.save();
            SyncedStore.this.mLock.writeLock().unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public Editor edit() {
        this.mLock.writeLock().lock();
        return new Editor();
    }

    public class Reader {
        public Reader() {
        }

        public String getString(String k, String defValue) {
            String rv = (String) SyncedStore.this.mMap.get(k);
            return rv != null ? rv : defValue;
        }

        public Set<String> keySet() {
            return SyncedStore.this.mMap.keySet();
        }

        public void complete() {
            SyncedStore.this.mLock.readLock().unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public Reader read() {
        this.mLock.readLock().lock();
        return new Reader();
    }

    public SyncedStore(Context c) {
        this.mContext = c;
        load();
    }

    public void load() {
        this.mMap = null;
        boolean mustSaveAfterLoad = false;
        long start = System.currentTimeMillis();
        File myStore = this.mContext.getFileStreamPath(FILENAME);
        this.mLock.writeLock().lock();
        try {
            List<ApplicationInfo> apps = this.mContext.getPackageManager().getInstalledApplications(0);
            ApplicationInfo myInfo = null;
            Iterator i$ = apps.iterator();
            while (true) {
                if (!i$.hasNext()) {
                    break;
                }
                ApplicationInfo ai = i$.next();
                if (ai.packageName.equals(this.mContext.getPackageName())) {
                    myInfo = ai;
                    break;
                }
            }
            String myStoreCPath = myStore.getCanonicalPath();
            if (myInfo != null) {
                if (myStoreCPath.startsWith(myInfo.dataDir)) {
                    String underDataDir = myStoreCPath.substring(myInfo.dataDir.length());
                    for (ApplicationInfo ai2 : apps) {
                        File file = new File(ai2.dataDir, underDataDir);
                        if (myStore.lastModified() < file.lastModified()) {
                            mustSaveAfterLoad = true;
                            myStore = file;
                        }
                    }
                    this.mMap = mapFromStore(myStore);
                }
            }
            if (this.mMap == null) {
                this.mMap = new HashMap<>();
            }
        } catch (IOException e) {
            OpenFeintInternal.log(TAG, "broken");
        } finally {
            this.mLock.writeLock().unlock();
        }
        if (mustSaveAfterLoad) {
            save();
        }
        OpenFeintInternal.log(TAG, "Loading prefs took " + new Long(System.currentTimeMillis() - start).toString() + " millis");
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0057 A[SYNTHETIC, Splitter:B:34:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0076 A[SYNTHETIC, Splitter:B:47:0x0076] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0095 A[SYNTHETIC, Splitter:B:60:0x0095] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00b4 A[SYNTHETIC, Splitter:B:73:0x00b4] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x00cd A[SYNTHETIC, Splitter:B:82:0x00cd] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x00d1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.HashMap<java.lang.String, java.lang.String> mapFromStore(java.io.File r10) {
        /*
            r9 = this;
            r1 = 0
            r4 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x004c, StreamCorruptedException -> 0x006b, IOException -> 0x008a, ClassNotFoundException -> 0x00a9 }
            r2.<init>(r10)     // Catch:{ FileNotFoundException -> 0x004c, StreamCorruptedException -> 0x006b, IOException -> 0x008a, ClassNotFoundException -> 0x00a9 }
            java.io.ObjectInputStream r5 = new java.io.ObjectInputStream     // Catch:{ FileNotFoundException -> 0x0105, StreamCorruptedException -> 0x00fa, IOException -> 0x00f1, ClassNotFoundException -> 0x00e8, all -> 0x00e1 }
            r5.<init>(r2)     // Catch:{ FileNotFoundException -> 0x0105, StreamCorruptedException -> 0x00fa, IOException -> 0x00f1, ClassNotFoundException -> 0x00e8, all -> 0x00e1 }
            java.lang.Object r3 = r5.readObject()     // Catch:{ FileNotFoundException -> 0x010a, StreamCorruptedException -> 0x00ff, IOException -> 0x00f5, ClassNotFoundException -> 0x00ec, all -> 0x00e4 }
            if (r3 == 0) goto L_0x0031
            boolean r6 = r3 instanceof java.util.HashMap     // Catch:{ FileNotFoundException -> 0x010a, StreamCorruptedException -> 0x00ff, IOException -> 0x00f5, ClassNotFoundException -> 0x00ec, all -> 0x00e4 }
            if (r6 == 0) goto L_0x0031
            java.util.HashMap r3 = (java.util.HashMap) r3     // Catch:{ FileNotFoundException -> 0x010a, StreamCorruptedException -> 0x00ff, IOException -> 0x00f5, ClassNotFoundException -> 0x00ec, all -> 0x00e4 }
            if (r5 == 0) goto L_0x0021
            r5.close()     // Catch:{ IOException -> 0x0027 }
        L_0x001d:
            r4 = r5
            r1 = r2
            r6 = r3
        L_0x0020:
            return r6
        L_0x0021:
            if (r2 == 0) goto L_0x001d
            r2.close()     // Catch:{ IOException -> 0x0027 }
            goto L_0x001d
        L_0x0027:
            r6 = move-exception
            r0 = r6
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)
            goto L_0x001d
        L_0x0031:
            if (r5 == 0) goto L_0x003a
            r5.close()     // Catch:{ IOException -> 0x0040 }
        L_0x0036:
            r4 = r5
            r1 = r2
        L_0x0038:
            r6 = 0
            goto L_0x0020
        L_0x003a:
            if (r2 == 0) goto L_0x0036
            r2.close()     // Catch:{ IOException -> 0x0040 }
            goto L_0x0036
        L_0x0040:
            r6 = move-exception
            r0 = r6
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)
            r4 = r5
            r1 = r2
            goto L_0x0038
        L_0x004c:
            r6 = move-exception
            r0 = r6
        L_0x004e:
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "Couldn't open of_prefs"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)     // Catch:{ all -> 0x00ca }
            if (r4 == 0) goto L_0x0065
            r4.close()     // Catch:{ IOException -> 0x005b }
            goto L_0x0038
        L_0x005b:
            r6 = move-exception
            r0 = r6
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)
            goto L_0x0038
        L_0x0065:
            if (r1 == 0) goto L_0x0038
            r1.close()     // Catch:{ IOException -> 0x005b }
            goto L_0x0038
        L_0x006b:
            r6 = move-exception
            r0 = r6
        L_0x006d:
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "StreamCorruptedException"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)     // Catch:{ all -> 0x00ca }
            if (r4 == 0) goto L_0x0084
            r4.close()     // Catch:{ IOException -> 0x007a }
            goto L_0x0038
        L_0x007a:
            r6 = move-exception
            r0 = r6
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)
            goto L_0x0038
        L_0x0084:
            if (r1 == 0) goto L_0x0038
            r1.close()     // Catch:{ IOException -> 0x007a }
            goto L_0x0038
        L_0x008a:
            r6 = move-exception
            r0 = r6
        L_0x008c:
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "IOException while reading"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)     // Catch:{ all -> 0x00ca }
            if (r4 == 0) goto L_0x00a3
            r4.close()     // Catch:{ IOException -> 0x0099 }
            goto L_0x0038
        L_0x0099:
            r6 = move-exception
            r0 = r6
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)
            goto L_0x0038
        L_0x00a3:
            if (r1 == 0) goto L_0x0038
            r1.close()     // Catch:{ IOException -> 0x0099 }
            goto L_0x0038
        L_0x00a9:
            r6 = move-exception
            r0 = r6
        L_0x00ab:
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "ClassNotFoundException"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)     // Catch:{ all -> 0x00ca }
            if (r4 == 0) goto L_0x00c3
            r4.close()     // Catch:{ IOException -> 0x00b8 }
            goto L_0x0038
        L_0x00b8:
            r6 = move-exception
            r0 = r6
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)
            goto L_0x0038
        L_0x00c3:
            if (r1 == 0) goto L_0x0038
            r1.close()     // Catch:{ IOException -> 0x00b8 }
            goto L_0x0038
        L_0x00ca:
            r6 = move-exception
        L_0x00cb:
            if (r4 == 0) goto L_0x00d1
            r4.close()     // Catch:{ IOException -> 0x00d7 }
        L_0x00d0:
            throw r6
        L_0x00d1:
            if (r1 == 0) goto L_0x00d0
            r1.close()     // Catch:{ IOException -> 0x00d7 }
            goto L_0x00d0
        L_0x00d7:
            r7 = move-exception
            r0 = r7
            java.lang.String r7 = "DistributedPrefs"
            java.lang.String r8 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r7, r8)
            goto L_0x00d0
        L_0x00e1:
            r6 = move-exception
            r1 = r2
            goto L_0x00cb
        L_0x00e4:
            r6 = move-exception
            r4 = r5
            r1 = r2
            goto L_0x00cb
        L_0x00e8:
            r6 = move-exception
            r0 = r6
            r1 = r2
            goto L_0x00ab
        L_0x00ec:
            r6 = move-exception
            r0 = r6
            r4 = r5
            r1 = r2
            goto L_0x00ab
        L_0x00f1:
            r6 = move-exception
            r0 = r6
            r1 = r2
            goto L_0x008c
        L_0x00f5:
            r6 = move-exception
            r0 = r6
            r4 = r5
            r1 = r2
            goto L_0x008c
        L_0x00fa:
            r6 = move-exception
            r0 = r6
            r1 = r2
            goto L_0x006d
        L_0x00ff:
            r6 = move-exception
            r0 = r6
            r4 = r5
            r1 = r2
            goto L_0x006d
        L_0x0105:
            r6 = move-exception
            r0 = r6
            r1 = r2
            goto L_0x004e
        L_0x010a:
            r6 = move-exception
            r0 = r6
            r4 = r5
            r1 = r2
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.SyncedStore.mapFromStore(java.io.File):java.util.HashMap");
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x005d A[SYNTHETIC, Splitter:B:27:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0091 A[SYNTHETIC, Splitter:B:43:0x0091] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x009e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void save() {
        /*
            r7 = this;
            r3 = 0
            r1 = 0
            java.util.concurrent.locks.ReentrantReadWriteLock r4 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r4 = r4.readLock()
            r4.lock()
            android.content.Context r4 = r7.mContext     // Catch:{ IOException -> 0x0052 }
            java.lang.String r5 = "of_prefs"
            r6 = 1
            java.io.FileOutputStream r3 = r4.openFileOutput(r5, r6)     // Catch:{ IOException -> 0x0052 }
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x0052 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0052 }
            java.util.HashMap<java.lang.String, java.lang.String> r4 = r7.mMap     // Catch:{ IOException -> 0x00c5, all -> 0x00c2 }
            r2.writeObject(r4)     // Catch:{ IOException -> 0x00c5, all -> 0x00c2 }
            if (r2 == 0) goto L_0x002e
            r2.close()     // Catch:{ IOException -> 0x0034 }
        L_0x0023:
            java.util.concurrent.locks.ReentrantReadWriteLock r4 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r4 = r4.readLock()
            r4.unlock()
        L_0x002c:
            r1 = r2
        L_0x002d:
            return
        L_0x002e:
            if (r3 == 0) goto L_0x0023
            r3.close()     // Catch:{ IOException -> 0x0034 }
            goto L_0x0023
        L_0x0034:
            r4 = move-exception
            r0 = r4
            java.lang.String r4 = "DistributedPrefs"
            java.lang.String r5 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r4, r5)     // Catch:{ all -> 0x0047 }
            java.util.concurrent.locks.ReentrantReadWriteLock r4 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r4 = r4.readLock()
            r4.unlock()
            goto L_0x002c
        L_0x0047:
            r4 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r5 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r5 = r5.readLock()
            r5.unlock()
            throw r4
        L_0x0052:
            r4 = move-exception
            r0 = r4
        L_0x0054:
            java.lang.String r4 = "DistributedPrefs"
            java.lang.String r5 = "Couldn't open of_prefs for writing"
            com.openfeint.internal.OpenFeintInternal.log(r4, r5)     // Catch:{ all -> 0x008e }
            if (r1 == 0) goto L_0x006a
            r1.close()     // Catch:{ IOException -> 0x0070 }
        L_0x0060:
            java.util.concurrent.locks.ReentrantReadWriteLock r4 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r4 = r4.readLock()
            r4.unlock()
            goto L_0x002d
        L_0x006a:
            if (r3 == 0) goto L_0x0060
            r3.close()     // Catch:{ IOException -> 0x0070 }
            goto L_0x0060
        L_0x0070:
            r4 = move-exception
            r0 = r4
            java.lang.String r4 = "DistributedPrefs"
            java.lang.String r5 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r4, r5)     // Catch:{ all -> 0x0083 }
            java.util.concurrent.locks.ReentrantReadWriteLock r4 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r4 = r4.readLock()
            r4.unlock()
            goto L_0x002d
        L_0x0083:
            r4 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r5 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r5 = r5.readLock()
            r5.unlock()
            throw r4
        L_0x008e:
            r4 = move-exception
        L_0x008f:
            if (r1 == 0) goto L_0x009e
            r1.close()     // Catch:{ IOException -> 0x00a4 }
        L_0x0094:
            java.util.concurrent.locks.ReentrantReadWriteLock r5 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r5 = r5.readLock()
            r5.unlock()
        L_0x009d:
            throw r4
        L_0x009e:
            if (r3 == 0) goto L_0x0094
            r3.close()     // Catch:{ IOException -> 0x00a4 }
            goto L_0x0094
        L_0x00a4:
            r5 = move-exception
            r0 = r5
            java.lang.String r5 = "DistributedPrefs"
            java.lang.String r6 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r5, r6)     // Catch:{ all -> 0x00b7 }
            java.util.concurrent.locks.ReentrantReadWriteLock r5 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r5 = r5.readLock()
            r5.unlock()
            goto L_0x009d
        L_0x00b7:
            r4 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r5 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r5 = r5.readLock()
            r5.unlock()
            throw r4
        L_0x00c2:
            r4 = move-exception
            r1 = r2
            goto L_0x008f
        L_0x00c5:
            r4 = move-exception
            r0 = r4
            r1 = r2
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.SyncedStore.save():void");
    }
}
