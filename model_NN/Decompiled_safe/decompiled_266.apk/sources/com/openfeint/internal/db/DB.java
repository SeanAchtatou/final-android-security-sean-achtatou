package com.openfeint.internal.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDiskIOException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.os.Environment;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;
import com.openfeint.internal.ui.WebViewCache;
import java.io.File;

public class DB {
    public static final String DBNAME = "manifest.db";
    private static final String DBPATH = "/openfeint/webui/manifest.db";
    private static final String TAG = "SQL";
    private static final int VERSION = 3;
    public static DataStorageHelperX storeHelper;

    private static boolean removeDB(Context ctx) {
        if (Util.noSdcardPermission()) {
            return ctx.getDatabasePath(DBNAME).delete();
        }
        if ("mounted".equals(Environment.getExternalStorageState())) {
            return new File(Environment.getExternalStorageDirectory(), DBPATH).delete();
        }
        return ctx.getDatabasePath(DBNAME).delete();
    }

    public static void createDB(Context ctx) {
        if (Util.noSdcardPermission()) {
            storeHelper = new DataStorageHelperX(ctx);
        } else if ("mounted".equals(Environment.getExternalStorageState())) {
            storeHelper = new DataStorageHelperX(Environment.getExternalStorageDirectory().getAbsolutePath() + DBPATH);
        } else {
            storeHelper = new DataStorageHelperX(ctx);
        }
    }

    public static boolean recover(Context ctx) {
        if (storeHelper != null) {
            storeHelper.close();
        }
        boolean success = removeDB(ctx);
        if (!success) {
            return success;
        }
        createDB(ctx);
        return storeHelper != null;
    }

    public static void setClientManifestBatch(String[] paths, String[] clientHashes) {
        SQLiteDatabase db = null;
        if (paths.length == clientHashes.length) {
            try {
                SQLiteDatabase db2 = storeHelper.getWritableDatabase();
                SQLiteStatement statement = db2.compileStatement("INSERT OR REPLACE INTO manifest(path, hash) VALUES(?,?)");
                db2.beginTransaction();
                for (int i = 0; i < paths.length; i++) {
                    statement.bindString(1, paths[i]);
                    statement.bindString(2, clientHashes[i]);
                    statement.execute();
                }
                statement.close();
                db2.setTransactionSuccessful();
                try {
                    db2.endTransaction();
                } catch (Exception e) {
                }
            } catch (SQLiteDiskIOException e2) {
                WebViewCache.diskError();
                try {
                    db.endTransaction();
                } catch (Exception e3) {
                }
            } catch (Exception e4) {
                OpenFeintInternal.log(TAG, e4.toString());
                try {
                    db.endTransaction();
                } catch (Exception e5) {
                }
            } catch (Throwable th) {
                try {
                    db.endTransaction();
                } catch (Exception e6) {
                }
                throw th;
            }
        }
    }

    public static void setClientManifest(String path, String clientHash) {
        try {
            storeHelper.getWritableDatabase().execSQL("INSERT OR REPLACE INTO manifest(path, hash) VALUES(?,?)", new String[]{path, clientHash});
        } catch (SQLiteDiskIOException e) {
            WebViewCache.diskError();
        } catch (Exception e2) {
            OpenFeintInternal.log(TAG, e2.toString());
        }
    }

    /* access modifiers changed from: private */
    public static void onCreate(SQLiteDatabase db) {
        onUpgrade(db, 0, 3);
    }

    /* access modifiers changed from: private */
    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion == 0) {
            db.execSQL("CREATE TABLE IF NOT EXISTS manifest (path TEXT PRIMARY KEY, hash TEXT);");
            oldVersion++;
        }
        if (oldVersion == 1) {
            db.execSQL("CREATE TABLE IF NOT EXISTS store (ID TEXT PRIMARY KEY, VALUE TEXT);");
            oldVersion++;
        }
        if (oldVersion == 2) {
            db.execSQL("CREATE TABLE IF NOT EXISTS server_manifest (path TEXT PRIMARY KEY NOT NULL, hash TEXT DEFAULT NULL, is_global INTEGER DEFAULT 0);");
            db.execSQL("CREATE TABLE IF NOT EXISTS dependencies (path TEXT NOT NULL, has_dependency TEXT NOT NULL);");
            oldVersion++;
        }
        if (oldVersion != newVersion) {
            OpenFeintInternal.log(TAG, String.format("Unable to upgrade DB from %d to %d.", Integer.valueOf(oldVersion), Integer.valueOf(newVersion)));
        }
    }

    public static class DataStorageHelperX extends SQLiteOpenHelperX {
        DataStorageHelperX(Context context) {
            super(new DataStorageHelper(context));
        }

        DataStorageHelperX(String path) {
            super(path, 3);
        }

        public void onCreate(SQLiteDatabase db) {
            DB.onCreate(db);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            DB.onUpgrade(db, oldVersion, newVersion);
        }
    }

    public static class DataStorageHelper extends SQLiteOpenHelper {
        DataStorageHelper(Context context) {
            super(context, DB.DBNAME, (SQLiteDatabase.CursorFactory) null, 3);
        }

        public void onCreate(SQLiteDatabase db) {
            DB.onCreate(db);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            DB.onUpgrade(db, oldVersion, newVersion);
        }
    }
}
