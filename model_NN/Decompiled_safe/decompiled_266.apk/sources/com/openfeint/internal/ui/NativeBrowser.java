package com.openfeint.internal.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.util.concurrent.atomic.AtomicBoolean;

public class NativeBrowser extends NestedWindow {
    public static final String INTENT_ARG_PREFIX = "com.openfeint.internal.ui.NativeBrowser.argument.";
    /* access modifiers changed from: private */
    public AtomicBoolean mFinished = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public Runnable mTimeoutWatchdog;

    public final class JSInterface {
        public JSInterface() {
        }

        public void returnValue(final String returnValue) {
            NativeBrowser.this.runOnUiThread(new Runnable() {
                public void run() {
                    if (NativeBrowser.this.mFinished.compareAndSet(false, true)) {
                        Intent returnIntent = new Intent();
                        if (returnValue != null) {
                            returnIntent.putExtra("com.openfeint.internal.ui.NativeBrowser.argument.result", returnValue);
                        }
                        NativeBrowser.this.setResult(-1, returnIntent);
                        NativeBrowser.this.finish();
                    }
                }
            });
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        String src = extras.getString("com.openfeint.internal.ui.NativeBrowser.argument.src");
        String timeout = extras.getString("com.openfeint.internal.ui.NativeBrowser.argument.timeout");
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.addJavascriptInterface(new JSInterface(), "NativeBrowser");
        this.mWebView.setWebViewClient(new WebViewClient() {
            private void clearTimeout() {
                if (NativeBrowser.this.mHandler != null && NativeBrowser.this.mTimeoutWatchdog != null) {
                    NativeBrowser.this.mHandler.removeCallbacks(NativeBrowser.this.mTimeoutWatchdog);
                    Handler unused = NativeBrowser.this.mHandler = null;
                    Runnable unused2 = NativeBrowser.this.mTimeoutWatchdog = null;
                }
            }

            public void onPageFinished(WebView view, String url) {
                clearTimeout();
                super.onPageFinished(view, url);
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                clearTimeout();
                super.onReceivedError(view, errorCode, description, failingUrl);
                Intent returnIntent = new Intent();
                returnIntent.putExtra("com.openfeint.internal.ui.NativeBrowser.argument.failed", true);
                returnIntent.putExtra("com.openfeint.internal.ui.NativeBrowser.argument.failure_code", errorCode);
                returnIntent.putExtra("com.openfeint.internal.ui.NativeBrowser.argument.failure_desc", description);
                NativeBrowser.this.setResult(-1, returnIntent);
                NativeBrowser.this.finish();
            }
        });
        this.mWebView.setWebChromeClient(new WebChromeClient());
        this.mWebView.loadUrl(src);
        if (timeout != null) {
            this.mHandler = new Handler();
            this.mTimeoutWatchdog = new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                 arg types: [java.lang.String, int]
                 candidates:
                  ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
                public void run() {
                    if (NativeBrowser.this.mFinished.compareAndSet(false, true)) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("com.openfeint.internal.ui.NativeBrowser.argument.failed", true);
                        returnIntent.putExtra("com.openfeint.internal.ui.NativeBrowser.argument.failure_code", 0);
                        returnIntent.putExtra("com.openfeint.internal.ui.NativeBrowser.argument.failure_desc", "Timeout");
                        NativeBrowser.this.setResult(-1, returnIntent);
                        NativeBrowser.this.finish();
                    }
                }
            };
            this.mHandler.postDelayed(this.mTimeoutWatchdog, (long) Integer.parseInt(timeout));
        }
        fade(true);
    }
}
