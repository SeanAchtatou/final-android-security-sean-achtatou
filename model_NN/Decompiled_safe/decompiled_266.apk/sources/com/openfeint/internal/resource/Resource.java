package com.openfeint.internal.resource;

import com.adknowledge.superrewards.model.SROffer;
import com.openfeint.api.resource.Achievement;
import com.openfeint.api.resource.CurrentUser;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;
import com.openfeint.api.resource.ServerTimestamp;
import com.openfeint.api.resource.User;
import com.openfeint.internal.OpenFeintInternal;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;

public abstract class Resource {
    private static final String TAG = "Resource";
    private static HashMap<String, ResourceClass> sNameMap = new HashMap<>();
    private static HashMap<Class<? extends Resource>, ResourceClass> sSubclassMap = new HashMap<>();
    String mID;

    public String resourceID() {
        return this.mID;
    }

    public void setResourceID(String id) {
        this.mID = id;
    }

    static {
        registerSubclass(getResourceClass());
        registerSubclass(ServerException.getResourceClass());
        registerSubclass(ServerTimestamp.getResourceClass());
        registerSubclass(BlobUploadParameters.getResourceClass());
        registerSubclass(Achievement.getResourceClass());
        registerSubclass(Leaderboard.getResourceClass());
        registerSubclass(Score.getResourceClass());
        registerSubclass(User.getResourceClass());
        registerSubclass(CurrentUser.getResourceClass());
    }

    public static void registerSubclass(ResourceClass klass) {
        sSubclassMap.put(klass.mObjectClass, klass);
        if (klass.mResourceName != null) {
            sNameMap.put(klass.mResourceName, klass);
        }
    }

    public static ResourceClass getKlass(Class<?> subclass) {
        return sSubclassMap.get(subclass);
    }

    public static ResourceClass getKlass(String klassName) {
        return sNameMap.get(klassName);
    }

    public static ResourceClass getResourceClass() {
        ResourceClass klass = new ResourceClass(Resource.class, null) {
            public Resource factory() {
                return null;
            }
        };
        klass.mProperties.put(SROffer.ID, new StringResourceProperty() {
            public String get(Resource obj) {
                return obj.mID;
            }

            public void set(Resource obj, String val) {
                obj.mID = val;
            }
        });
        return klass;
    }

    public final void generateToStream(OutputStream out) throws IOException {
        JsonGenerator jg = new JsonFactory().createJsonGenerator(out, JsonEncoding.UTF8);
        generate(jg);
        jg.close();
    }

    public final String generate() {
        StringWriter sw = new StringWriter();
        try {
            JsonGenerator jg = new JsonFactory().createJsonGenerator(sw);
            generate(jg);
            jg.close();
            return sw.toString();
        } catch (IOException e) {
            OpenFeintInternal.log(TAG, e.getMessage());
            return null;
        }
    }

    public final void generate(JsonGenerator generator) throws JsonGenerationException, IOException {
        HashMap<String, Integer> r;
        ResourceClass rc = getKlass(getClass());
        generator.writeStartObject();
        generator.writeFieldName(rc.mResourceName);
        generator.writeStartObject();
        for (Map.Entry<String, ResourceProperty> e : rc.mProperties.entrySet()) {
            ResourceProperty rp = (ResourceProperty) e.getValue();
            if (rp instanceof PrimitiveResourceProperty) {
                ((PrimitiveResourceProperty) rp).generate(this, generator, (String) e.getKey());
            } else if (rp instanceof ArrayResourceProperty) {
                ArrayResourceProperty arp = (ArrayResourceProperty) rp;
                List<? extends Resource> resources = arp.get(this);
                if (resources != null) {
                    generator.writeFieldName((String) e.getKey());
                    ResourceClass erc = getKlass(arp.elementType());
                    generator.writeStartObject();
                    generator.writeFieldName(erc.mResourceName + "s");
                    generator.writeStartArray();
                    for (Resource r2 : resources) {
                        r2.generate(generator);
                    }
                    generator.writeEndArray();
                    generator.writeEndObject();
                }
            } else if (rp instanceof NestedResourceProperty) {
                Resource r3 = ((NestedResourceProperty) rp).get(this);
                if (r3 != null) {
                    generator.writeFieldName((String) e.getKey());
                    r3.generate(generator);
                }
            } else if ((rp instanceof HashIntResourceProperty) && (r = ((HashIntResourceProperty) rp).get(this)) != null && r.size() > 0) {
                generator.writeFieldName((String) e.getKey());
                generator.writeStartObject();
                for (Map.Entry<String, Integer> entry : r.entrySet()) {
                    generator.writeFieldName((String) entry.getKey());
                    generator.writeNumber(((Integer) entry.getValue()).intValue());
                }
                generator.writeEndObject();
            }
        }
        generator.writeEndObject();
        generator.writeEndObject();
    }

    public final void shallowCopy(Resource otherResource) {
        if (otherResource.getClass() != getClass()) {
            throw new UnsupportedOperationException("You can only shallowCopy the same type of resource");
        }
        unguardedShallowCopy(otherResource);
    }

    public final void shallowCopyAncestorType(Resource otherResource) {
        Class<?> c = getClass();
        Class<?> o = otherResource.getClass();
        if (o != Resource.class) {
            while (c != o && c != Resource.class) {
                c = c.getSuperclass();
            }
            if (c == Resource.class) {
                throw new UnsupportedOperationException(o.getName() + " is not a superclass of " + getClass().getName());
            }
        }
        unguardedShallowCopy(otherResource);
    }

    private final void unguardedShallowCopy(Resource otherResource) {
        for (Map.Entry<String, ResourceProperty> e : getKlass(otherResource.getClass()).mProperties.entrySet()) {
            ResourceProperty rp = (ResourceProperty) e.getValue();
            if (rp instanceof PrimitiveResourceProperty) {
                ((PrimitiveResourceProperty) rp).copy(this, otherResource);
            } else if (rp instanceof ArrayResourceProperty) {
                ((ArrayResourceProperty) rp).set(this, ((ArrayResourceProperty) rp).get(otherResource));
            } else if (rp instanceof NestedResourceProperty) {
                ((NestedResourceProperty) rp).set(this, ((NestedResourceProperty) rp).get(otherResource));
            }
        }
    }
}
