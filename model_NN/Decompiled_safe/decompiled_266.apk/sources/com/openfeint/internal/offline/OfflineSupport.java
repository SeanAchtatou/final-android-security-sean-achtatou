package com.openfeint.internal.offline;

import android.content.Context;
import com.ansca.corona.R;
import com.openfeint.api.Notification;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;
import com.openfeint.internal.notifications.SimpleNotification;
import com.openfeint.internal.request.BaseRequest;
import com.openfeint.internal.request.OrderedArgList;
import com.openfeint.internal.resource.DateResourceProperty;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.codec.binary.Hex;

public class OfflineSupport {
    private static final String TAG = "OfflineSupport";
    private static final String TEMPORARY_USER_ID = "0";
    /* access modifiers changed from: private */
    public static DB db = null;
    /* access modifiers changed from: private */
    public static AtomicBoolean updateInProgress = new AtomicBoolean(false);
    private static String userID = null;

    /* access modifiers changed from: private */
    public static boolean streq(String lhs, String rhs) {
        if (lhs == null) {
            return rhs == null;
        }
        return lhs.equals(rhs);
    }

    public static class OfflineAchievement {
        public float clientCompletionPercentage;
        public String resourceID;
        public float serverCompletionPercentage;
        public String timestamp;

        public OfflineAchievement dup() {
            OfflineAchievement rv = new OfflineAchievement();
            rv.resourceID = this.resourceID;
            rv.clientCompletionPercentage = this.clientCompletionPercentage;
            rv.serverCompletionPercentage = this.serverCompletionPercentage;
            rv.timestamp = this.timestamp;
            return rv;
        }

        public boolean eq(OfflineAchievement rhs) {
            return OfflineSupport.streq(this.resourceID, rhs.resourceID) && this.clientCompletionPercentage == rhs.clientCompletionPercentage && this.serverCompletionPercentage == rhs.serverCompletionPercentage && OfflineSupport.streq(this.timestamp, rhs.timestamp);
        }
    }

    public static class OfflineScore {
        public String blobFileName;
        public String customData;
        public String displayText;
        public String leaderboardID;
        public long score;
        public String timestamp;

        public OfflineScore dup() {
            OfflineScore rv = new OfflineScore();
            rv.leaderboardID = this.leaderboardID;
            rv.score = this.score;
            rv.displayText = this.displayText;
            rv.customData = this.customData;
            rv.blobFileName = this.blobFileName;
            rv.timestamp = this.timestamp;
            return rv;
        }

        public boolean eq(OfflineScore rhs) {
            return OfflineSupport.streq(this.leaderboardID, rhs.leaderboardID) && this.score == rhs.score && OfflineSupport.streq(this.displayText, rhs.displayText) && OfflineSupport.streq(this.customData, rhs.customData) && OfflineSupport.streq(this.blobFileName, rhs.blobFileName) && OfflineSupport.streq(this.timestamp, rhs.timestamp);
        }
    }

    public static class DB {
        private static final int STREAM_VERSION = 0;
        public ArrayList<OfflineAchievement> achievements = new ArrayList<>();
        public ArrayList<OfflineScore> scores = new ArrayList<>();

        public DB dup() {
            DB rv = new DB();
            Iterator i$ = this.scores.iterator();
            while (i$.hasNext()) {
                rv.scores.add(i$.next().dup());
            }
            Iterator i$2 = this.achievements.iterator();
            while (i$2.hasNext()) {
                rv.achievements.add(i$2.next().dup());
            }
            return rv;
        }

        public void merge(DB newUserDB) {
            Iterator i$ = newUserDB.achievements.iterator();
            while (i$.hasNext()) {
                OfflineAchievement newUserAchievement = i$.next();
                OfflineAchievement currentUserAchievement = findAchievement(newUserAchievement.resourceID);
                if (currentUserAchievement == null) {
                    this.achievements.add(newUserAchievement.dup());
                } else {
                    if (currentUserAchievement.clientCompletionPercentage < newUserAchievement.clientCompletionPercentage) {
                        currentUserAchievement.clientCompletionPercentage = newUserAchievement.clientCompletionPercentage;
                        currentUserAchievement.timestamp = newUserAchievement.timestamp;
                    }
                    currentUserAchievement.serverCompletionPercentage = Math.max(currentUserAchievement.serverCompletionPercentage, newUserAchievement.serverCompletionPercentage);
                }
            }
            this.scores.addAll(newUserDB.scores);
        }

        public void updateOnUpload(DB otherDB) {
            Iterator i$ = otherDB.achievements.iterator();
            while (i$.hasNext()) {
                OfflineAchievement otherAchievement = i$.next();
                OfflineAchievement myAchievement = findAchievement(otherAchievement.resourceID);
                if (myAchievement == null) {
                    myAchievement = otherAchievement.dup();
                    this.achievements.add(myAchievement);
                }
                myAchievement.serverCompletionPercentage = Math.max(myAchievement.serverCompletionPercentage, otherAchievement.clientCompletionPercentage);
            }
            ArrayList<OfflineScore> oldScores = this.scores;
            this.scores = new ArrayList<>();
            Iterator i$2 = oldScores.iterator();
            while (i$2.hasNext()) {
                OfflineScore myScore = i$2.next();
                if (myScore.blobFileName != null) {
                    this.scores.add(myScore);
                } else {
                    boolean found = false;
                    Iterator i$3 = otherDB.scores.iterator();
                    while (true) {
                        if (i$3.hasNext()) {
                            if (myScore.eq(i$3.next())) {
                                found = true;
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                    if (!found) {
                        this.scores.add(myScore);
                    }
                }
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:15:0x0065 A[SYNTHETIC, Splitter:B:15:0x0065] */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x00e6 A[SYNTHETIC, Splitter:B:34:0x00e6] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static com.openfeint.internal.offline.OfflineSupport.DB load(java.lang.String r15) {
            /*
                com.openfeint.internal.offline.OfflineSupport$DB r7 = new com.openfeint.internal.offline.OfflineSupport$DB
                r7.<init>()
                r5 = 0
                if (r15 == 0) goto L_0x0068
                com.openfeint.internal.OpenFeintInternal r10 = com.openfeint.internal.OpenFeintInternal.getInstance()     // Catch:{ Exception -> 0x00f2 }
                android.content.Context r10 = r10.getContext()     // Catch:{ Exception -> 0x00f2 }
                java.io.FileInputStream r10 = r10.openFileInput(r15)     // Catch:{ Exception -> 0x00f2 }
                javax.crypto.CipherInputStream r2 = com.openfeint.internal.Encryption.decryptionWrap(r10)     // Catch:{ Exception -> 0x00f2 }
                java.io.ObjectInputStream r6 = new java.io.ObjectInputStream     // Catch:{ Exception -> 0x00f2 }
                r6.<init>(r2)     // Catch:{ Exception -> 0x00f2 }
                int r9 = r6.readInt()     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                switch(r9) {
                    case 0: goto L_0x0069;
                    default: goto L_0x0024;
                }     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
            L_0x0024:
                java.lang.Exception r10 = new java.lang.Exception     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                java.lang.String r11 = "Unrecognized stream version %d"
                r12 = 1
                java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                r13 = 0
                java.lang.Integer r14 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                r12[r13] = r14     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                java.lang.String r11 = java.lang.String.format(r11, r12)     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                r10.<init>(r11)     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                throw r10     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
            L_0x003a:
                r10 = move-exception
                r1 = r10
                r5 = r6
            L_0x003d:
                java.lang.String r10 = "OfflineSupport"
                java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e3 }
                r11.<init>()     // Catch:{ all -> 0x00e3 }
                java.lang.String r12 = "Couldn't load offline achievements - "
                java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x00e3 }
                java.lang.String r12 = r1.getMessage()     // Catch:{ all -> 0x00e3 }
                java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x00e3 }
                java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x00e3 }
                com.openfeint.internal.OpenFeintInternal.log(r10, r11)     // Catch:{ all -> 0x00e3 }
                java.util.ArrayList<com.openfeint.internal.offline.OfflineSupport$OfflineAchievement> r10 = r7.achievements     // Catch:{ all -> 0x00e3 }
                r10.clear()     // Catch:{ all -> 0x00e3 }
                java.util.ArrayList<com.openfeint.internal.offline.OfflineSupport$OfflineScore> r10 = r7.scores     // Catch:{ all -> 0x00e3 }
                r10.clear()     // Catch:{ all -> 0x00e3 }
                if (r5 == 0) goto L_0x0068
                r5.close()     // Catch:{ IOException -> 0x00ea }
            L_0x0068:
                return r7
            L_0x0069:
                int r3 = r6.readInt()     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
            L_0x006d:
                if (r3 <= 0) goto L_0x0098
                com.openfeint.internal.offline.OfflineSupport$OfflineAchievement r0 = new com.openfeint.internal.offline.OfflineSupport$OfflineAchievement     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                r0.<init>()     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                java.lang.Object r15 = r6.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                java.lang.String r15 = (java.lang.String) r15     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                r0.resourceID = r15     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                float r10 = r6.readFloat()     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                r0.clientCompletionPercentage = r10     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                float r10 = r6.readFloat()     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                r0.serverCompletionPercentage = r10     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                java.lang.Object r15 = r6.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                java.lang.String r15 = (java.lang.String) r15     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                r0.timestamp = r15     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                java.util.ArrayList<com.openfeint.internal.offline.OfflineSupport$OfflineAchievement> r10 = r7.achievements     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                r10.add(r0)     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                int r3 = r3 + -1
                goto L_0x006d
            L_0x0098:
                int r4 = r6.readInt()     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
            L_0x009c:
                if (r4 <= 0) goto L_0x00d9
                com.openfeint.internal.offline.OfflineSupport$OfflineScore r8 = new com.openfeint.internal.offline.OfflineSupport$OfflineScore     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                r8.<init>()     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                java.lang.Object r15 = r6.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                java.lang.String r15 = (java.lang.String) r15     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                r8.leaderboardID = r15     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                long r10 = r6.readLong()     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                r8.score = r10     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                java.lang.Object r15 = r6.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                java.lang.String r15 = (java.lang.String) r15     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                r8.displayText = r15     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                java.lang.Object r15 = r6.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                java.lang.String r15 = (java.lang.String) r15     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                r8.customData = r15     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                java.lang.Object r15 = r6.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                java.lang.String r15 = (java.lang.String) r15     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                r8.blobFileName = r15     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                java.lang.Object r15 = r6.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                java.lang.String r15 = (java.lang.String) r15     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                r8.timestamp = r15     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                java.util.ArrayList<com.openfeint.internal.offline.OfflineSupport$OfflineScore> r10 = r7.scores     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                r10.add(r8)     // Catch:{ Exception -> 0x003a, all -> 0x00ef }
                int r4 = r4 + -1
                goto L_0x009c
            L_0x00d9:
                if (r6 == 0) goto L_0x00de
                r6.close()     // Catch:{ IOException -> 0x00e0 }
            L_0x00de:
                r5 = r6
                goto L_0x0068
            L_0x00e0:
                r10 = move-exception
                r5 = r6
                goto L_0x0068
            L_0x00e3:
                r10 = move-exception
            L_0x00e4:
                if (r5 == 0) goto L_0x00e9
                r5.close()     // Catch:{ IOException -> 0x00ed }
            L_0x00e9:
                throw r10
            L_0x00ea:
                r10 = move-exception
                goto L_0x0068
            L_0x00ed:
                r11 = move-exception
                goto L_0x00e9
            L_0x00ef:
                r10 = move-exception
                r5 = r6
                goto L_0x00e4
            L_0x00f2:
                r10 = move-exception
                r1 = r10
                goto L_0x003d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.offline.OfflineSupport.DB.load(java.lang.String):com.openfeint.internal.offline.OfflineSupport$DB");
        }

        /* JADX WARNING: Removed duplicated region for block: B:12:0x0050 A[SYNTHETIC, Splitter:B:12:0x0050] */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0055 A[SYNTHETIC, Splitter:B:15:0x0055] */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0097 A[SYNTHETIC, Splitter:B:25:0x0097] */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x009c A[SYNTHETIC, Splitter:B:28:0x009c] */
        /* JADX WARNING: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void save(java.lang.String r9) {
            /*
                r8 = this;
                r2 = 0
                r4 = 0
                com.openfeint.internal.OpenFeintInternal r6 = com.openfeint.internal.OpenFeintInternal.getInstance()     // Catch:{ Exception -> 0x00be, all -> 0x00bc }
                android.content.Context r6 = r6.getContext()     // Catch:{ Exception -> 0x00be, all -> 0x00bc }
                r7 = 0
                java.io.FileOutputStream r6 = r6.openFileOutput(r9, r7)     // Catch:{ Exception -> 0x00be, all -> 0x00bc }
                javax.crypto.CipherOutputStream r4 = com.openfeint.internal.Encryption.encryptionWrap(r6)     // Catch:{ Exception -> 0x00be, all -> 0x00bc }
                java.io.ObjectOutputStream r3 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x00be, all -> 0x00bc }
                r3.<init>(r4)     // Catch:{ Exception -> 0x00be, all -> 0x00bc }
                r6 = 0
                r3.writeInt(r6)     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                java.util.ArrayList<com.openfeint.internal.offline.OfflineSupport$OfflineAchievement> r6 = r8.achievements     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                int r6 = r6.size()     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                r3.writeInt(r6)     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                java.util.ArrayList<com.openfeint.internal.offline.OfflineSupport$OfflineAchievement> r6 = r8.achievements     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                java.util.Iterator r1 = r6.iterator()     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
            L_0x002b:
                boolean r6 = r1.hasNext()     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                if (r6 == 0) goto L_0x0059
                java.lang.Object r0 = r1.next()     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                com.openfeint.internal.offline.OfflineSupport$OfflineAchievement r0 = (com.openfeint.internal.offline.OfflineSupport.OfflineAchievement) r0     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                java.lang.String r6 = r0.resourceID     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                r3.writeObject(r6)     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                float r6 = r0.clientCompletionPercentage     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                r3.writeFloat(r6)     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                float r6 = r0.serverCompletionPercentage     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                r3.writeFloat(r6)     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                java.lang.String r6 = r0.timestamp     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                r3.writeObject(r6)     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                goto L_0x002b
            L_0x004c:
                r6 = move-exception
                r2 = r3
            L_0x004e:
                if (r2 == 0) goto L_0x0053
                r2.close()     // Catch:{ IOException -> 0x00b4 }
            L_0x0053:
                if (r4 == 0) goto L_0x0058
                r4.close()     // Catch:{ IOException -> 0x00b6 }
            L_0x0058:
                return
            L_0x0059:
                java.util.ArrayList<com.openfeint.internal.offline.OfflineSupport$OfflineScore> r6 = r8.scores     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                int r6 = r6.size()     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                r3.writeInt(r6)     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                java.util.ArrayList<com.openfeint.internal.offline.OfflineSupport$OfflineScore> r6 = r8.scores     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                java.util.Iterator r1 = r6.iterator()     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
            L_0x0068:
                boolean r6 = r1.hasNext()     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                if (r6 == 0) goto L_0x00a0
                java.lang.Object r5 = r1.next()     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                com.openfeint.internal.offline.OfflineSupport$OfflineScore r5 = (com.openfeint.internal.offline.OfflineSupport.OfflineScore) r5     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                java.lang.String r6 = r5.leaderboardID     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                r3.writeObject(r6)     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                long r6 = r5.score     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                r3.writeLong(r6)     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                java.lang.String r6 = r5.displayText     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                r3.writeObject(r6)     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                java.lang.String r6 = r5.customData     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                r3.writeObject(r6)     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                java.lang.String r6 = r5.blobFileName     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                r3.writeObject(r6)     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                java.lang.String r6 = r5.timestamp     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                r3.writeObject(r6)     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                goto L_0x0068
            L_0x0093:
                r6 = move-exception
                r2 = r3
            L_0x0095:
                if (r2 == 0) goto L_0x009a
                r2.close()     // Catch:{ IOException -> 0x00b8 }
            L_0x009a:
                if (r4 == 0) goto L_0x009f
                r4.close()     // Catch:{ IOException -> 0x00ba }
            L_0x009f:
                throw r6
            L_0x00a0:
                r3.close()     // Catch:{ Exception -> 0x004c, all -> 0x0093 }
                if (r3 == 0) goto L_0x00a8
                r3.close()     // Catch:{ IOException -> 0x00b2 }
            L_0x00a8:
                if (r4 == 0) goto L_0x00ad
                r4.close()     // Catch:{ IOException -> 0x00af }
            L_0x00ad:
                r2 = r3
                goto L_0x0058
            L_0x00af:
                r6 = move-exception
                r2 = r3
                goto L_0x0058
            L_0x00b2:
                r6 = move-exception
                goto L_0x00a8
            L_0x00b4:
                r6 = move-exception
                goto L_0x0053
            L_0x00b6:
                r6 = move-exception
                goto L_0x0058
            L_0x00b8:
                r7 = move-exception
                goto L_0x009a
            L_0x00ba:
                r7 = move-exception
                goto L_0x009f
            L_0x00bc:
                r6 = move-exception
                goto L_0x0095
            L_0x00be:
                r6 = move-exception
                goto L_0x004e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.offline.OfflineSupport.DB.save(java.lang.String):void");
        }

        public void removeReferencedBlobs() {
            Iterator i$ = this.scores.iterator();
            while (i$.hasNext()) {
                OfflineSupport.deleteDataFile(i$.next().blobFileName);
            }
        }

        public void clear() {
            this.achievements.clear();
            this.scores.clear();
        }

        public OfflineAchievement findAchievement(String resourceID) {
            Iterator i$ = this.achievements.iterator();
            while (i$.hasNext()) {
                OfflineAchievement a = i$.next();
                if (a.resourceID.equals(resourceID)) {
                    return a;
                }
            }
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static void deleteDataFile(String fileName) {
        try {
            Context context = OpenFeintInternal.getInstance().getContext();
            context.deleteFile(context.getFileStreamPath(fileName).getPath());
        } catch (Exception e) {
        }
    }

    private static String fullPath(String fileName) {
        return OpenFeintInternal.getInstance().getContext().getFileStreamPath(fileName).getPath();
    }

    private static String filename(String forUserID) {
        if (forUserID == null) {
            return null;
        }
        String appID = OpenFeintInternal.getInstance().getAppID();
        if (appID == null) {
            return null;
        }
        return "of.offline." + forUserID + "." + appID;
    }

    private static String now() {
        return DateResourceProperty.sDateParser.format(new Date());
    }

    public static void setUserDeclined() {
        setUserID(null);
    }

    public static void setUserTemporary() {
        setUserID(TEMPORARY_USER_ID);
    }

    private static boolean isUserTemporary() {
        return TEMPORARY_USER_ID.equals(userID);
    }

    private static void removeDBForUser(String forUserID) {
        deleteDataFile(filename(forUserID));
    }

    public static void setUserID(String newUserID) {
        if (newUserID == null || !newUserID.equals(userID)) {
            DB newUserDB = DB.load(filename(newUserID));
            if (isUserTemporary()) {
                db.merge(newUserDB);
                removeDBForUser(TEMPORARY_USER_ID);
            } else {
                db = newUserDB;
            }
            userID = newUserID;
            trySubmitOfflineData();
        }
    }

    public static void trySubmitOfflineData() {
        if (userID != null && !userID.equals(TEMPORARY_USER_ID) && OpenFeint.isUserLoggedIn()) {
            updateToServer();
        }
    }

    static final void removeAndUploadNext(OfflineScore os) {
        db.scores.remove(os);
        save();
        uploadScoresWithBlobs();
    }

    /* access modifiers changed from: private */
    public static void uploadScoresWithBlobs() {
        Iterator i$ = db.scores.iterator();
        while (i$.hasNext()) {
            final OfflineScore os = i$.next();
            if (os.blobFileName != null) {
                Score s = new Score(os.score);
                s.customData = os.customData;
                s.displayText = os.displayText;
                try {
                    s.blob = Util.readWholeFile(fullPath(os.blobFileName));
                } catch (IOException e) {
                }
                if (s.blob == null) {
                    removeAndUploadNext(os);
                    return;
                } else {
                    s.submitToFromOffline(new Leaderboard(os.leaderboardID), os.timestamp, new Score.SubmitToCB() {
                        public void onSuccess(boolean newHighScore) {
                            OfflineSupport.removeAndUploadNext(os);
                        }

                        public void onFailure(String failureMessage) {
                            OfflineSupport.updateInProgress.set(false);
                        }
                    });
                    return;
                }
            }
        }
        updateInProgress.set(false);
    }

    private static synchronized void updateToServer() {
        synchronized (OfflineSupport.class) {
            if (updateInProgress.compareAndSet(false, true)) {
                final DB clonedDB = db.dup();
                OrderedArgList oal = new OrderedArgList();
                int currAchievement = 0;
                Iterator i$ = clonedDB.achievements.iterator();
                while (i$.hasNext()) {
                    OfflineAchievement oa = i$.next();
                    if (oa.clientCompletionPercentage != oa.serverCompletionPercentage) {
                        OpenFeintInternal.log(TAG, String.format("Updating achievement %s from known %f to %f completion", oa.resourceID, Float.valueOf(oa.serverCompletionPercentage), Float.valueOf(oa.clientCompletionPercentage)));
                        oal.put(String.format("achievements[%d][id]", Integer.valueOf(currAchievement)), oa.resourceID);
                        oal.put(String.format("achievements[%d][percent_complete]", Integer.valueOf(currAchievement)), Float.toString(oa.clientCompletionPercentage));
                        oal.put(String.format("achievements[%d][timestamp]", Integer.valueOf(currAchievement)), oa.timestamp);
                        currAchievement++;
                    }
                }
                int currScore = 0;
                Iterator i$2 = clonedDB.scores.iterator();
                while (i$2.hasNext()) {
                    OfflineScore os = i$2.next();
                    if (os.blobFileName == null) {
                        OpenFeintInternal.log(TAG, String.format("Posting score %d to leaderboard %s", Long.valueOf(os.score), os.leaderboardID));
                        oal.put(String.format("high_scores[%d][leaderboard_id]", Integer.valueOf(currScore)), os.leaderboardID);
                        oal.put(String.format("high_scores[%d][score]", Integer.valueOf(currScore)), Long.toString(os.score));
                        if (os.displayText != null) {
                            oal.put(String.format("high_scores[%d][display_text]", Integer.valueOf(currScore)), os.displayText);
                        }
                        if (os.customData != null) {
                            oal.put(String.format("high_scores[%d][custom_data]", Integer.valueOf(currScore)), os.customData);
                        }
                        oal.put(String.format("high_scores[%d][timestamp]", Integer.valueOf(currScore)), os.timestamp);
                        currScore++;
                    }
                }
                if (currAchievement == 0 && currScore == 0) {
                    uploadScoresWithBlobs();
                } else {
                    final String path = "/xp/games/" + OpenFeintInternal.getInstance().getAppID() + "/offline_syncs";
                    new BaseRequest(oal) {
                        public String method() {
                            return "POST";
                        }

                        public String path() {
                            return path;
                        }

                        public void onResponse(int responseCode, byte[] body) {
                        }

                        public void onResponseOffMainThread(int responseCode, byte[] body) {
                            if (200 > responseCode || responseCode >= 300) {
                                if (responseCode != 0 && 500 > responseCode) {
                                    OfflineSupport.db.removeReferencedBlobs();
                                    OfflineSupport.db.clear();
                                    OfflineSupport.save();
                                }
                                OfflineSupport.updateInProgress.set(false);
                                return;
                            }
                            OfflineSupport.db.updateOnUpload(clonedDB);
                            OfflineSupport.save();
                            OfflineSupport.uploadScoresWithBlobs();
                        }
                    }.launch();
                }
            }
        }
    }

    public static void updateClientCompletionPercentage(String resourceID, float completionPercentage) {
        if (userID != null) {
            OfflineAchievement a = db.findAchievement(resourceID);
            if (a == null) {
                OfflineAchievement a2 = new OfflineAchievement();
                a2.resourceID = resourceID;
                a2.serverCompletionPercentage = 0.0f;
                a2.clientCompletionPercentage = completionPercentage;
                a2.timestamp = now();
                db.achievements.add(a2);
                save();
            } else if (a.clientCompletionPercentage < completionPercentage) {
                a.clientCompletionPercentage = completionPercentage;
                a.timestamp = now();
                save();
            }
        }
    }

    public static void updateServerCompletionPercentage(String resourceID, float completionPercentage) {
        if (userID != null) {
            OfflineAchievement a = db.findAchievement(resourceID);
            if (a == null) {
                a = new OfflineAchievement();
                a.resourceID = resourceID;
                a.clientCompletionPercentage = completionPercentage;
                db.achievements.add(a);
            }
            a.serverCompletionPercentage = completionPercentage;
            a.timestamp = now();
            save();
        }
    }

    public static float getClientCompletionPercentage(String resourceID) {
        OfflineAchievement a = db.findAchievement(resourceID);
        if (a == null) {
            return 0.0f;
        }
        return a.clientCompletionPercentage;
    }

    public static void postOfflineScore(Score s, Leaderboard l) {
        if (userID != null) {
            OfflineScore os = new OfflineScore();
            os.leaderboardID = l.resourceID();
            os.score = s.score;
            os.displayText = s.displayText;
            os.customData = s.customData;
            os.timestamp = now();
            if (s.blob != null) {
                OfflineScore existingScore = null;
                Iterator i$ = db.scores.iterator();
                while (true) {
                    if (!i$.hasNext()) {
                        break;
                    }
                    OfflineScore scan = i$.next();
                    if (os.leaderboardID.equals(scan.leaderboardID)) {
                        existingScore = scan;
                        break;
                    }
                }
                if (existingScore != null) {
                    if (l.allowsWorseScores || ((l.descendingSortOrder && existingScore.score < s.score) || (!l.descendingSortOrder && existingScore.score > s.score))) {
                        deleteDataFile(existingScore.blobFileName);
                        db.scores.remove(existingScore);
                    } else {
                        return;
                    }
                }
                String filename = "unknown.blob";
                try {
                    MessageDigest md = MessageDigest.getInstance("SHA1");
                    md.update(s.blob);
                    filename = String.format("%s.blob", new String(Hex.encodeHex(md.digest())));
                } catch (NoSuchAlgorithmException e) {
                }
                try {
                    Util.saveFile(s.blob, fullPath(filename));
                    os.blobFileName = filename;
                } catch (IOException e2) {
                    return;
                }
            }
            db.scores.add(os);
            save();
            if (!isUserTemporary()) {
                SimpleNotification.show(OpenFeintInternal.getRString(R.string.of_score_submitted_notification), "@drawable/of_icon_highscore_notification", Notification.Category.HighScore, Notification.Type.Success);
            }
        }
    }

    /* access modifiers changed from: private */
    public static void save() {
        String fileName = filename(userID);
        if (fileName != null) {
            db.save(fileName);
        }
    }
}
