package com.openfeint.internal.request;

import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;

public class CacheFileRequest extends CacheRequest {
    private static final String TAG = "CacheFile";
    protected String path;
    protected String url;

    public CacheFileRequest(String path2, String url2, String key) {
        super(key);
        this.path = path2;
        this.url = url2;
    }

    public void onResponse(int responseCode, byte[] body) {
        if (responseCode == 200) {
            try {
                Util.saveFile(body, this.path);
                super.on200Response();
            } catch (Exception e) {
                OpenFeintInternal.log(TAG, e.toString());
            }
        }
    }

    public String path() {
        return this.url;
    }
}
