package com.openfeint.internal.request.multipart;

import java.io.IOException;
import java.io.OutputStream;

public class StringPart extends PartBase {
    public static final String DEFAULT_CHARSET = "UTF-8";
    public static final String DEFAULT_CONTENT_TYPE = "text/html";
    public static final String DEFAULT_TRANSFER_ENCODING = "8bit";
    private byte[] content;
    private String value;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StringPart(String name, String value2, String charset) {
        super(name, DEFAULT_CONTENT_TYPE, charset == null ? "UTF-8" : charset, DEFAULT_TRANSFER_ENCODING);
        if (value2 == null) {
            throw new IllegalArgumentException("Value may not be null");
        } else if (value2.indexOf(0) != -1) {
            throw new IllegalArgumentException("NULs may not be present in string parts");
        } else {
            this.value = value2;
        }
    }

    public StringPart(String name, String value2) {
        this(name, value2, null);
    }

    private byte[] getContent() {
        if (this.content == null) {
            this.content = EncodingUtil.getBytes(this.value, getCharSet());
        }
        return this.content;
    }

    /* access modifiers changed from: protected */
    public void sendData(OutputStream out) throws IOException {
        out.write(getContent());
    }

    /* access modifiers changed from: protected */
    public long lengthOfData() throws IOException {
        return (long) getContent().length;
    }

    public void setCharSet(String charSet) {
        super.setCharSet(charSet);
        this.content = null;
    }
}
