package com.openfeint.internal.request;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

public class Signer {
    private String mAccessToken;
    private String mKey;
    private String mSecret;
    private String mSigningKey = (this.mSecret + "&");

    public String getKey() {
        return this.mKey;
    }

    public Signer(String key, String secret) {
        this.mKey = key;
        this.mSecret = secret;
    }

    public void setAccessToken(String token, String tokenSecret) {
        this.mAccessToken = token;
        this.mSigningKey = this.mSecret + "&" + tokenSecret;
    }

    public String sign(String path, String method, long secondsSinceEpoch, OrderedArgList unsignedParams) {
        if (this.mAccessToken != null) {
            unsignedParams.put("token", this.mAccessToken);
        }
        StringBuilder sigbase = new StringBuilder();
        sigbase.append(path);
        sigbase.append('+');
        sigbase.append(this.mSecret);
        sigbase.append('+');
        sigbase.append(method);
        sigbase.append('+');
        String argString = unsignedParams.getArgString();
        sigbase.append(argString == null ? "" : argString);
        try {
            SecretKeySpec key = new SecretKeySpec(this.mSigningKey.getBytes("UTF-8"), "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(key);
            return new String(Base64.encodeBase64(mac.doFinal(sigbase.toString().getBytes("UTF-8")))).replace("\r\n", "");
        } catch (UnsupportedEncodingException | InvalidKeyException | NoSuchAlgorithmException e) {
            return null;
        }
    }
}
