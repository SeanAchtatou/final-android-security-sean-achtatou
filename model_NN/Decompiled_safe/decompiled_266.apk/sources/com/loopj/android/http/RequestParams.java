package com.loopj.android.http;

import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

public class RequestParams {
    private static String ENCODING = "UTF-8";
    private ConcurrentHashMap<String, String> urlParams;

    public RequestParams() {
        init();
    }

    public RequestParams(String str, String str2) {
        init();
        put(str, str2);
    }

    private List<BasicNameValuePair> getParamsList() {
        LinkedList linkedList = new LinkedList();
        for (Map.Entry next : this.urlParams.entrySet()) {
            linkedList.add(new BasicNameValuePair((String) next.getKey(), (String) next.getValue()));
        }
        return linkedList;
    }

    private void init() {
        this.urlParams = new ConcurrentHashMap<>();
    }

    public HttpEntity getEntity() {
        try {
            return new UrlEncodedFormEntity(getParamsList(), ENCODING);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getParamString() {
        return URLEncodedUtils.format(getParamsList(), ENCODING);
    }

    public void put(String str, String str2) {
        this.urlParams.put(str, str2);
    }

    public void remove(String str) {
        this.urlParams.remove(str);
    }
}
