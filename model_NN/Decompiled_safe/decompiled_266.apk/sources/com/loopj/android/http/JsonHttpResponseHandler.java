package com.loopj.android.http;

import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.json.JSONException;
import org.json.JSONTokener;

public class JsonHttpResponseHandler extends AsyncHttpResponseHandler {
    /* access modifiers changed from: protected */
    public void handleResponseMessage(HttpResponse httpResponse) {
        StatusLine statusLine = httpResponse.getStatusLine();
        if (statusLine.getStatusCode() >= 300) {
            onFailure(new HttpResponseException(statusLine.getStatusCode(), statusLine.getReasonPhrase()));
            return;
        }
        try {
            String responseBody = getResponseBody(httpResponse);
            onSuccess(responseBody);
            onSuccess(new JSONTokener(responseBody).nextValue());
        } catch (JSONException e) {
            onFailure(e);
        } catch (IOException e2) {
            onFailure(e2);
        }
    }

    public void onSuccess(Object obj) {
    }
}
