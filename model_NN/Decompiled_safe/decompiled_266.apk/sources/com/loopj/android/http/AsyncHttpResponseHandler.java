package com.loopj.android.http;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.util.EntityUtils;

public class AsyncHttpResponseHandler extends Handler {
    private static final int ERROR_MESSAGE = 1;
    private static final int FINISH_MESSAGE = 3;
    private static final int RESPONSE_MESSAGE = 0;
    private static final int START_MESSAGE = 2;

    public AsyncHttpResponseHandler() {
        super(Looper.getMainLooper());
    }

    /* access modifiers changed from: protected */
    public String getResponseBody(HttpResponse httpResponse) throws IOException {
        BufferedHttpEntity bufferedHttpEntity = null;
        HttpEntity entity = httpResponse.getEntity();
        if (entity != null) {
            bufferedHttpEntity = new BufferedHttpEntity(entity);
        }
        return EntityUtils.toString(bufferedHttpEntity);
    }

    /* access modifiers changed from: protected */
    public void handleErrorMessage(Throwable th) {
        onFailure(th);
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                handleResponseMessage((HttpResponse) message.obj);
                return;
            case 1:
                handleErrorMessage((Throwable) message.obj);
                return;
            case 2:
                onStart();
                return;
            case 3:
                onFinish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void handleResponseMessage(HttpResponse httpResponse) {
        StatusLine statusLine = httpResponse.getStatusLine();
        if (statusLine.getStatusCode() >= 300) {
            onFailure(new HttpResponseException(statusLine.getStatusCode(), statusLine.getReasonPhrase()));
            return;
        }
        try {
            onSuccess(getResponseBody(httpResponse));
        } catch (IOException e) {
            onFailure(e);
        }
    }

    public void onFailure(Throwable th) {
    }

    public void onFinish() {
    }

    public void onStart() {
    }

    public void onSuccess(String str) {
    }

    public void sendErrorMessage(Throwable th) {
        sendMessage(obtainMessage(1, th));
    }

    public void sendFinishMessage() {
        sendMessage(obtainMessage(3));
    }

    public void sendResponseMessage(HttpResponse httpResponse) {
        sendMessage(obtainMessage(0, httpResponse));
    }

    public void sendStartMessage() {
        sendMessage(obtainMessage(2));
    }
}
