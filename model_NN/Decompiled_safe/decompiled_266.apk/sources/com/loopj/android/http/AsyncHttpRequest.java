package com.loopj.android.http;

import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.protocol.HttpContext;

public class AsyncHttpRequest implements Runnable {
    private HttpClient client;
    private HttpContext context;
    private HttpUriRequest request;
    private AsyncHttpResponseHandler responseHandler;

    public AsyncHttpRequest(HttpClient httpClient, HttpContext httpContext, HttpUriRequest httpUriRequest, AsyncHttpResponseHandler asyncHttpResponseHandler) {
        this.client = httpClient;
        this.context = httpContext;
        this.request = httpUriRequest;
        this.responseHandler = asyncHttpResponseHandler;
    }

    public void run() {
        try {
            if (this.responseHandler != null) {
                this.responseHandler.sendStartMessage();
            }
            HttpResponse execute = this.client.execute(this.request, this.context);
            if (this.responseHandler != null) {
                this.responseHandler.sendFinishMessage();
                this.responseHandler.sendResponseMessage(execute);
            }
        } catch (IOException e) {
            if (this.responseHandler != null) {
                this.responseHandler.sendFinishMessage();
                this.responseHandler.sendErrorMessage(e);
            }
        }
    }
}
