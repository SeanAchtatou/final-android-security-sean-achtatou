package com.zong.android.engine.process;

import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import com.zong.android.engine.activities.ZongPaymentRequest;
import com.zong.android.engine.activities.ZongPricePointsElement;
import com.zong.android.engine.provider.PhoneState;
import com.zong.android.engine.provider.ZongPhoneManager;
import com.zong.android.engine.sms.ZongSmsReceiver;
import com.zong.android.engine.sms.a;
import com.zong.android.engine.task.PaymentProcessorTask;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.StringTokenizer;
import zongfuscated.C0065e;
import zongfuscated.D;
import zongfuscated.F;
import zongfuscated.n;
import zongfuscated.o;
import zongfuscated.q;

public class ZongServiceProcess extends Service {
    /* access modifiers changed from: private */
    public static final String a = ZongServiceProcess.class.getSimpleName();
    private boolean b;
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public ContentValues d;
    /* access modifiers changed from: private */
    public HashMap<String, HashSet<String>> e;
    /* access modifiers changed from: private */
    public HashMap<String, HashSet<String>> f;
    private ZongSmsReceiver g;
    /* access modifiers changed from: private */
    public C0065e h;
    /* access modifiers changed from: private */
    public PaymentProcessorTask i;
    /* access modifiers changed from: private */
    public ZongPaymentRequest j = null;
    private StringBuilder k;
    private StringBuilder l;
    /* access modifiers changed from: private */
    public boolean m;
    /* access modifiers changed from: private */
    public Message n;
    /* access modifiers changed from: private */
    public Messenger o;
    private Handler p = new Handler(new c(this));
    private Messenger q = new Messenger(this.p);
    /* access modifiers changed from: private */
    public Handler r = new Handler(new a(this));

    static /* synthetic */ void a(ContentValues contentValues, String str) {
        if (!o.a(str)) {
            StringTokenizer stringTokenizer = new StringTokenizer(str, "|");
            while (stringTokenizer.hasMoreElements()) {
                StringTokenizer stringTokenizer2 = new StringTokenizer((String) stringTokenizer.nextElement(), "=");
                contentValues.put((String) stringTokenizer2.nextElement(), (String) stringTokenizer2.nextElement());
            }
        }
    }

    static /* synthetic */ void a(HashMap hashMap, String str) {
        if (!o.a(str)) {
            StringTokenizer stringTokenizer = new StringTokenizer(str, "|");
            while (stringTokenizer.hasMoreElements()) {
                HashSet hashSet = new HashSet();
                StringTokenizer stringTokenizer2 = new StringTokenizer((String) stringTokenizer.nextElement(), ",");
                String str2 = (String) stringTokenizer2.nextElement();
                while (stringTokenizer2.hasMoreElements()) {
                    hashSet.add((String) stringTokenizer2.nextElement());
                }
                hashMap.put(str2, hashSet);
            }
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        q.a(a, "Starting Application");
        if (!this.b) {
            this.b = true;
            this.h = new C0065e(this.j.getUrl());
            this.i = new PaymentProcessorTask(this, this.r, this.h);
            this.g = new ZongSmsReceiver(this.i.a());
            ZongSmsReceiver zongSmsReceiver = this.g;
            q.a(a, "Registring Handler");
            Context applicationContext = getApplicationContext();
            if (applicationContext != null) {
                try {
                    IntentFilter intentFilter = new IntentFilter();
                    intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
                    intentFilter.setPriority(999);
                    applicationContext.registerReceiver(zongSmsReceiver, intentFilter);
                } catch (Exception e2) {
                    q.a(a, "Failed to register SMS Receiver", e2);
                }
            } else {
                q.c(a, "Register failed due to missing context");
            }
            if (q.a()) {
                a.a().a(getApplicationContext());
            }
        }
    }

    static /* synthetic */ boolean c(ZongServiceProcess zongServiceProcess, HashMap hashMap) {
        if (hashMap.containsKey(zongServiceProcess.j.getCustomerKey()) || hashMap.containsKey("FORCE_ALL")) {
            if (hashMap.containsKey("FORCE_ALL")) {
                q.a(a, "FORCING DEBUG MODE ON FORCE ALL");
                return true;
            }
            HashSet hashSet = (HashSet) hashMap.get(zongServiceProcess.j.getCustomerKey());
            if (hashSet.size() <= 0) {
                q.a(a, "FORCING DEBUG MODE ON CUSTOMER KEY", zongServiceProcess.j.getCustomerKey());
                return true;
            } else if (hashSet.contains(zongServiceProcess.j.getPhoneNumber())) {
                q.a(a, "FORCING DEBUG MODE ON MSISDN", zongServiceProcess.j.getPhoneNumber());
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void d() {
        this.k = null;
        this.l = null;
        ArrayList<ZongPricePointsElement> pricepointsList = this.j.getPricepointsList();
        int size = pricepointsList.size();
        if (size > 0) {
            NumberFormat currencyFormatter = this.j.getCurrencyFormatter();
            this.l = new StringBuilder(pricepointsList.get(0).c());
            this.k = new StringBuilder(currencyFormatter.format((double) pricepointsList.get(0).getAmount()));
            for (int i2 = 1; i2 < size; i2++) {
                this.l.append("|").append(pricepointsList.get(i2).c());
                this.k.append("|").append(currencyFormatter.format((double) pricepointsList.get(i2).getAmount()));
            }
        }
        final F f2 = new F();
        f2.a(this.l.toString());
        f2.b(this.k.toString());
        f2.a(this.h);
        Thread thread = new Thread(new Runnable() {
            public final void run() {
                String str;
                HashMap hashMap = new HashMap();
                hashMap.put("lang", o.b(ZongServiceProcess.this.j.getLang()));
                hashMap.put("country", o.b(ZongServiceProcess.this.j.getCountry()));
                hashMap.put("msisdn", o.b(ZongServiceProcess.this.j.getPhoneNumber()));
                hashMap.put("operator", o.b(ZongServiceProcess.this.j.getMno()));
                hashMap.put("customerKey", o.b(ZongServiceProcess.this.j.getCustomerKey()));
                PhoneState phoneState = ZongPhoneManager.getInstance().getPhoneState(ZongServiceProcess.this);
                hashMap.put("lineNumber", o.b(phoneState.getLineNumber()));
                hashMap.put("imeaId", o.b(phoneState.getImeaId()));
                hashMap.put("netIsoCountry", o.b(phoneState.getNetIsoCountry()));
                hashMap.put("netOp", o.b(phoneState.getNetOp()));
                hashMap.put("netOpName", o.b(phoneState.getNetOpName()));
                hashMap.put("netType", o.b(phoneState.getNetType().toString()));
                hashMap.put("simIsoCountry", o.b(phoneState.getSimIsoCountry()));
                hashMap.put("simOp", o.b(phoneState.getSimOp()));
                hashMap.put("simOpName", o.b(phoneState.getSimOpName()));
                hashMap.put("subscribeId", o.b(phoneState.getSubsciberId()));
                hashMap.put("simSerial", o.b(phoneState.getSimSerial()));
                try {
                    o a2 = new n(ZongServiceProcess.this.h.a("/zongpay/actions/androgyne"), hashMap).a();
                    if (a2 != null) {
                        ArrayList<D> e = a2.a().e();
                        boolean a3 = q.a();
                        if (e != null) {
                            ZongServiceProcess.this.d = new ContentValues();
                            ZongServiceProcess.this.e = new HashMap();
                            ZongServiceProcess.this.f = new HashMap();
                            Iterator<D> it = e.iterator();
                            boolean z = a3;
                            String str2 = null;
                            while (it.hasNext()) {
                                D next = it.next();
                                if ("msisdnValid".equalsIgnoreCase(next.a())) {
                                    ZongServiceProcess.this.c = Boolean.parseBoolean(next.b());
                                } else if (!"countryPrefix".equalsIgnoreCase(next.a())) {
                                    if ("supportedCountries".equalsIgnoreCase(next.a())) {
                                        str2 = o.b(next.b()).trim();
                                    } else if (!"localPrefix".equalsIgnoreCase(next.a()) && !"localPhoneMinLength".equalsIgnoreCase(next.a()) && !"localPhoneMaxLength".equalsIgnoreCase(next.a())) {
                                        if ("errorMessages".equalsIgnoreCase(next.a())) {
                                            ZongServiceProcess.a(ZongServiceProcess.this.d, o.b(next.b()).trim());
                                        } else if ("debugMode".equalsIgnoreCase(next.a())) {
                                            z = Boolean.valueOf(o.b(next.b()).trim()).booleanValue();
                                        } else if ("debugList".equalsIgnoreCase(next.a())) {
                                            ZongServiceProcess.a(ZongServiceProcess.this.e, o.b(next.b()).trim());
                                        } else if ("blackList".equalsIgnoreCase(next.a())) {
                                            ZongServiceProcess.a(ZongServiceProcess.this.f, o.b(next.b()).trim());
                                        }
                                    }
                                }
                            }
                            if (ZongServiceProcess.c(ZongServiceProcess.this, ZongServiceProcess.this.e)) {
                                q.a(z);
                            }
                            ZongServiceProcess.c(ZongServiceProcess.this, ZongServiceProcess.this.f);
                            if (!ZongServiceProcess.this.c) {
                                ZongServiceProcess.this.j.setPhoneNumber(com.zong.android.engine.task.a.b(ZongServiceProcess.this.getApplicationContext(), ZongServiceProcess.this.j.getPhoneNumber()));
                            }
                            str = str2;
                        } else {
                            str = null;
                        }
                        f2.a(Boolean.valueOf(ZongServiceProcess.this.c));
                        f2.c(str);
                        f2.a(ZongServiceProcess.this.d);
                        f2.d(a2.b());
                        ZongServiceProcess.this.r.sendMessage(ZongServiceProcess.this.r.obtainMessage(4, f2));
                    }
                } catch (Exception e2) {
                    q.a(ZongServiceProcess.a, "ZongWebView.applicationStart()", e2);
                }
            }
        });
        thread.setName("HTML Page Loader");
        thread.setDaemon(true);
        thread.start();
    }

    private void e() {
        q.a(a, "Stop Application");
        this.b = false;
        this.i.b();
        ZongSmsReceiver zongSmsReceiver = this.g;
        q.a(a, "Un-Registring Handler");
        if (zongSmsReceiver != null) {
            Context applicationContext = getApplicationContext();
            if (applicationContext != null) {
                try {
                    applicationContext.unregisterReceiver(zongSmsReceiver);
                } catch (Exception e2) {
                    q.a(a, "Failed to un-register SMS Receiver", e2);
                }
            } else {
                q.c(a, "Un-Register failed due to missing context");
            }
        } else {
            q.a(a, "Un-Register failed no receiver registered");
        }
        if (q.a()) {
            a.a().b(getApplicationContext());
        }
        this.i = null;
        this.g = null;
    }

    static /* synthetic */ void j(ZongServiceProcess zongServiceProcess) {
        q.a(a, "Stop And Go Application");
        zongServiceProcess.e();
        zongServiceProcess.c();
    }

    public final void a() {
        if (this.g != null) {
            this.g.abortBroadcast();
            q.a(a, "Aborting SMS Broadcast");
        }
    }

    public IBinder onBind(Intent intent) {
        q.a(a, "LifeCycle Event: onBind");
        return this.q.getBinder();
    }

    public void onCreate() {
        super.onCreate();
        this.b = false;
        q.a(a, "LifeCycle Event: onCreate");
    }

    public void onDestroy() {
        super.onDestroy();
        q.a(a, "LifeCycle Event: onDestroy");
        e();
    }

    public void onRebind(Intent intent) {
        super.onRebind(intent);
        q.a(a, "LifeCycle Event: onRebind");
    }

    public boolean onUnbind(Intent intent) {
        q.a(a, "LifeCycle Event: onUnbind");
        return super.onUnbind(intent);
    }
}
