package com.zong.android.engine.task;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.zong.android.engine.activities.ZongPaymentRequest;
import com.zong.android.engine.process.ZongServiceProcess;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import zongfuscated.A;
import zongfuscated.C;
import zongfuscated.C0061a;
import zongfuscated.C0062b;
import zongfuscated.C0065e;
import zongfuscated.D;
import zongfuscated.h;
import zongfuscated.j;
import zongfuscated.l;
import zongfuscated.o;
import zongfuscated.p;
import zongfuscated.q;
import zongfuscated.s;
import zongfuscated.u;

public class PaymentProcessorTask extends Thread {
    /* access modifiers changed from: private */
    public static final String b = PaymentProcessorTask.class.getSimpleName();
    private long A = System.currentTimeMillis();
    ZongServiceProcess a;
    /* access modifiers changed from: private */
    public boolean c;
    private String d;
    /* access modifiers changed from: private */
    public String e;
    private String f;
    private j g;
    private boolean h;
    private boolean i;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;
    private String o;
    /* access modifiers changed from: private */
    public A p;
    private C0065e q;
    private ExecutorService r;
    private Future<?> s = null;
    /* access modifiers changed from: private */
    public LinkedBlockingQueue<h> t;
    private final CountDownLatch u;
    private Handler v = null;
    private Handler w = null;
    private Context x;
    private String y = "";
    private int z = 0;

    enum ActionType {
        WAIT("wait"),
        SEND_KEYWORD("send-phone-info"),
        POLLING("polling"),
        ENTER_PINCODE("enter-pincode-fr"),
        PROGRESS("progress"),
        SENDFLOW("sendflow"),
        SEND_PINCODE("send-pincode"),
        COMPLETED_TX("complete-transaction"),
        REDIRECT("redirect_client"),
        ERROR("show-error");
        
        private final String tag;

        private ActionType(String str) {
            this.tag = str;
        }

        public final boolean match(String str) {
            return this.tag.equals(str);
        }
    }

    enum Task {
        TIME_OUT(60000),
        DELAY(2000);
        
        private final long time;

        private Task(long j) {
            this.time = j;
        }

        public final long getTime() {
            return this.time;
        }
    }

    private static final class a implements ThreadFactory {
        /* synthetic */ a() {
            this((byte) 0);
        }

        private a(byte b) {
        }

        public final Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable);
            thread.setDaemon(true);
            return thread;
        }
    }

    public PaymentProcessorTask(ZongServiceProcess zongServiceProcess, Handler handler, C0065e eVar) {
        super("PaymentProcessorTask");
        this.a = zongServiceProcess;
        this.x = zongServiceProcess.getApplicationContext();
        this.g = new j();
        this.f = this.x.getClass().getCanonicalName();
        this.g.a(this.f, this.x);
        this.v = handler;
        this.q = eVar;
        this.u = new CountDownLatch(1);
        start();
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        boolean z2 = false;
        this.h = true;
        if (i2 == 0) {
            z2 = true;
        }
        C0062b bVar = new C0062b();
        bVar.a(z2);
        bVar.a(i2);
        bVar.a(this.j);
        bVar.f(this.o);
        bVar.b(this.k);
        bVar.c(this.l);
        bVar.e(this.n);
        bVar.d(this.m);
        a(3, bVar);
    }

    private void a(int i2, Object obj) {
        this.v.sendMessage(this.v.obtainMessage(i2, obj));
    }

    private void a(Message message, long j2) {
        if (this.h) {
            return;
        }
        if (j2 > 0) {
            this.w.sendMessageDelayed(message, j2);
        } else {
            this.w.sendMessage(message);
        }
    }

    static /* synthetic */ void a(PaymentProcessorTask paymentProcessorTask, Message message) {
        if (paymentProcessorTask.h) {
            return;
        }
        if (message.what == 0) {
            Bundle data = message.getData();
            String string = data.getString("originatingAddr");
            String string2 = data.getString("body");
            paymentProcessorTask.t.add(new h(string, string2));
            q.a(b, "processPremiumSMS (shortcode, body)", string, string2);
        } else if (message.what == 2) {
            q.c(b, "processTimeout Action");
            paymentProcessorTask.a(2);
        } else if (message.what == 3) {
            o oVar = (o) message.obj;
            C a2 = oVar.a();
            q.a(b, "Executing Scheduled Action", a2.b(), a2.c());
            paymentProcessorTask.p.a(oVar, null);
        } else if (message.what == 4) {
            o oVar2 = (o) message.obj;
            if (oVar2 == null) {
                paymentProcessorTask.a(2);
            }
            String a3 = oVar2.a().a();
            if (a3.equals(paymentProcessorTask.y)) {
                if (System.currentTimeMillis() - paymentProcessorTask.A < Task.access$2(Task.DELAY)) {
                    paymentProcessorTask.z++;
                    q.a(b, "Thresold broken for action", a3, Integer.toString(paymentProcessorTask.z));
                    if (paymentProcessorTask.z > 20) {
                        q.c(b, "Multi Action Threshold exhausted");
                        paymentProcessorTask.a(2);
                    }
                }
            } else {
                paymentProcessorTask.y = a3;
                paymentProcessorTask.z = 0;
            }
            paymentProcessorTask.A = System.currentTimeMillis();
            if (ActionType.WAIT.match(a3)) {
                paymentProcessorTask.a(oVar2);
            } else if (ActionType.SEND_KEYWORD.match(a3)) {
                C a4 = oVar2.a();
                q.a(b, "processPhoneInfo Action", a4.b(), a4.c());
                HashMap hashMap = new HashMap();
                hashMap.put("mno", paymentProcessorTask.d);
                q.a(b, "Sendig MNO to server", paymentProcessorTask.d);
                paymentProcessorTask.p.a(oVar2, hashMap);
            } else if (ActionType.POLLING.match(a3)) {
                paymentProcessorTask.a(oVar2);
            } else if (ActionType.ENTER_PINCODE.match(a3)) {
                paymentProcessorTask.b(oVar2);
            } else if (ActionType.PROGRESS.match(a3)) {
                paymentProcessorTask.d(oVar2);
            } else if (ActionType.SENDFLOW.match(a3)) {
                paymentProcessorTask.c(oVar2);
            } else if (ActionType.SEND_PINCODE.match(a3)) {
                paymentProcessorTask.e(oVar2);
            } else if (ActionType.COMPLETED_TX.match(a3)) {
                C a5 = oVar2.a();
                q.a(b, "processCompleteTransaction Action", a5.b(), a5.c());
                paymentProcessorTask.j = o.b(oVar2.b()).trim();
                q.a(b, "Completion exit enabled");
                paymentProcessorTask.p.a(oVar2);
                paymentProcessorTask.a(0);
            } else if (ActionType.REDIRECT.match(a3)) {
                paymentProcessorTask.f(oVar2);
            } else if (ActionType.ERROR.match(a3)) {
                paymentProcessorTask.g(oVar2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2) {
        if (!this.c) {
            com.zong.android.engine.sms.a.a().a(str, str2);
            q.a(b, "Sent shortcode / keyword to server", str, str2);
        }
    }

    private void a(o oVar) {
        C a2 = oVar.a();
        q.a(b, "processPolling Action", a2.b(), a2.c());
        q.a(b, "processPolling Message", a2.d());
        a(2, new p(a2.d()));
        a(this.w.obtainMessage(3, 0, 0, oVar), Task.DELAY.getTime());
    }

    private void a(final u uVar, final String str, final o oVar) {
        this.s = this.r.submit(new Runnable() {
            public final void run() {
                try {
                    q.a(PaymentProcessorTask.b, "Waiting for SMS");
                    int i = 0;
                    boolean z = false;
                    while (!z && i < 3) {
                        int i2 = i + 1;
                        uVar.a((h) PaymentProcessorTask.this.t.take());
                        q.a(PaymentProcessorTask.b, "Got a SMS in the Queue");
                        String a2 = uVar.a(PaymentProcessorTask.this.c, PaymentProcessorTask.this.e);
                        if (a2 != null) {
                            if (!PaymentProcessorTask.this.c) {
                                PaymentProcessorTask.this.a.a();
                            }
                            HashMap hashMap = new HashMap();
                            hashMap.put(str, a2);
                            q.a(PaymentProcessorTask.b, "Parse pincode", a2);
                            PaymentProcessorTask.this.p.a(oVar, hashMap);
                            q.a(PaymentProcessorTask.b, "Sent Exchange Message - DONE");
                            i = i2;
                            z = true;
                        } else {
                            i = i2;
                        }
                    }
                    if (!z) {
                        q.c(PaymentProcessorTask.b, "PANIC ERROR NO VALID SMS(3) ARRIVED");
                        PaymentProcessorTask.this.a(2);
                    }
                    q.a(PaymentProcessorTask.b, "Found Valid SMS");
                } catch (InterruptedException e) {
                    q.a(PaymentProcessorTask.b, "queue.take", e);
                }
            }
        });
    }

    private void b(o oVar) {
        Integer num;
        String str;
        String str2;
        String str3;
        String str4 = null;
        C a2 = oVar.a();
        q.a(b, "processSendKeywordMessage Action", a2.b(), a2.c());
        q.a(b, "processSendKeywordMessage Message", a2.d());
        a(2, new p(a2.d()));
        ArrayList<D> e2 = a2.e();
        if (e2 != null) {
            Iterator<D> it = e2.iterator();
            Integer num2 = null;
            String str5 = null;
            String str6 = null;
            while (it.hasNext()) {
                D next = it.next();
                if ("shortcode".equalsIgnoreCase(next.a())) {
                    str4 = o.b(next.b()).trim();
                } else if ("keyword".equalsIgnoreCase(next.a())) {
                    str6 = o.b(next.b()).trim();
                } else if ("messageExpression".equalsIgnoreCase(next.a())) {
                    str5 = o.b(next.b()).trim();
                } else if ("groupId".equalsIgnoreCase(next.a())) {
                    num2 = Integer.valueOf(o.b(next.b()).trim());
                }
            }
            q.a(b, "processSendKeywordMessage Params ", str4, str6, str5, num2.toString());
            num = num2;
            str = str5;
            str2 = str6;
            str3 = str4;
        } else {
            num = null;
            str = null;
            str2 = null;
            str3 = null;
        }
        d();
        if (str3 == null || str2 == null || str == null || num == null) {
            a(2);
            return;
        }
        q.a(b, "processSendKeywordMessage", "Sending Keyword SMS");
        a(str3, str2);
        a(new C0061a(num, str), "code", oVar);
        q.a(b, "Synchronizing on SMS for pincode code");
    }

    private void c(o oVar) {
        final String str;
        final String str2;
        C a2 = oVar.a();
        q.a(b, "processSendFlow Action", a2.b(), a2.c());
        q.a(b, "processSendFlow Message", a2.d());
        a(2, new p(a2.d()));
        ArrayList<D> e2 = a2.e();
        if (e2 != null) {
            Iterator<D> it = e2.iterator();
            String str3 = null;
            String str4 = null;
            while (it.hasNext()) {
                D next = it.next();
                if ("shortcode".equalsIgnoreCase(next.a())) {
                    str4 = o.b(next.b()).trim();
                } else if ("keyword".equalsIgnoreCase(next.a())) {
                    str3 = o.b(next.b()).trim();
                }
            }
            q.a(b, "processSendFlow Params ", str4, str3);
            str = str3;
            str2 = str4;
        } else {
            str = null;
            str2 = null;
        }
        if (this.i) {
            this.i = false;
            d();
            if (str2 == null || str == null) {
                a(2);
            } else {
                this.s = this.r.submit(new Runnable() {
                    public final void run() {
                        PaymentProcessorTask.this.a(str2, str);
                        q.a(PaymentProcessorTask.b, "Keyword Reply Sent");
                    }
                });
            }
        }
        a(this.w.obtainMessage(3, 0, 0, oVar), Task.DELAY.getTime());
    }

    private void d() {
        if (this.c) {
            try {
                this.t.put(new h());
                q.a(b, "processSendPincode.isSimulationMode()", "Sending empty SMS Message");
            } catch (InterruptedException e2) {
                q.a(b, "Cannot insert into the Queue empty SMS Message", e2);
            }
        }
    }

    private void d(o oVar) {
        final String str;
        final String str2;
        C a2 = oVar.a();
        q.a(b, "processCtaFlow Action", a2.b(), a2.c());
        q.a(b, "processCtaFlow Message", a2.d());
        a(2, new p(a2.d()));
        ArrayList<D> e2 = a2.e();
        if (e2 != null) {
            Iterator<D> it = e2.iterator();
            String str3 = null;
            String str4 = null;
            while (it.hasNext()) {
                D next = it.next();
                if ("shortcode".equalsIgnoreCase(next.a())) {
                    str4 = o.b(next.b()).trim();
                } else if ("keyword".equalsIgnoreCase(next.a())) {
                    str3 = o.b(next.b()).trim();
                }
            }
            q.a(b, "processCtaFlow Params ", str4, str3);
            str = str3;
            str2 = str4;
        } else {
            str = null;
            str2 = null;
        }
        if (this.i) {
            this.i = false;
            d();
            if (str2 == null || str == null) {
                a(2);
            } else {
                this.s = this.r.submit(new Runnable() {
                    public final void run() {
                        try {
                            q.a(PaymentProcessorTask.b, "Waiting for SMS");
                            h hVar = (h) PaymentProcessorTask.this.t.take();
                            if (!PaymentProcessorTask.this.c && hVar.a().equals(str2)) {
                                PaymentProcessorTask.this.a.a();
                            }
                            PaymentProcessorTask.this.a(str2, str);
                            q.a(PaymentProcessorTask.b, "Keyword Reply Sent");
                        } catch (InterruptedException e) {
                            q.a(PaymentProcessorTask.b, "queue.take", e);
                        }
                    }
                });
                q.a(b, "Synchronizing on SMS arrival for Reply");
            }
        }
        a(this.w.obtainMessage(3, 0, 0, oVar), Task.DELAY.getTime());
    }

    private void e(o oVar) {
        Integer num;
        String str;
        C a2 = oVar.a();
        q.a(b, "processSendPincode Action", a2.b(), a2.c());
        ArrayList<D> e2 = a2.e();
        if (e2 != null) {
            Iterator<D> it = e2.iterator();
            Integer num2 = null;
            String str2 = null;
            String str3 = null;
            while (it.hasNext()) {
                D next = it.next();
                if ("shortcode".equalsIgnoreCase(next.a())) {
                    str3 = o.b(next.b()).trim();
                } else if ("messageExpression".equalsIgnoreCase(next.a())) {
                    str2 = o.b(next.b()).trim();
                } else if ("groupId".equalsIgnoreCase(next.a())) {
                    num2 = Integer.valueOf(o.b(next.b()).trim());
                }
            }
            q.a(b, "processSendPincode Params ", str3, str2, num2.toString());
            num = num2;
            str = str2;
        } else {
            num = null;
            str = null;
        }
        d();
        if (str == null || num == null) {
            a(2);
            return;
        }
        a(new s(num, str), "pincode", oVar);
        q.a(b, "Synchronizing on SMS for pincode");
    }

    private void f(o oVar) {
        ArrayList<D> e2 = oVar.a().e();
        if (e2 != null) {
            Iterator<D> it = e2.iterator();
            while (it.hasNext()) {
                D next = it.next();
                q.a(b, "processRedirect Params ", next.a(), next.b());
            }
        }
        q.a(b, "Redirect exit enabled");
        a(0);
    }

    private void g(o oVar) {
        C a2 = oVar.a();
        q.a(b, "processError Action");
        ArrayList<D> e2 = a2.e();
        if (e2 != null) {
            Iterator<D> it = e2.iterator();
            while (it.hasNext()) {
                D next = it.next();
                if ("cause".equalsIgnoreCase(next.a())) {
                    this.l = o.b(next.b()).trim();
                } else if ("details".equalsIgnoreCase(next.a())) {
                    this.n = o.b(next.b()).trim();
                } else if ("message".equalsIgnoreCase(next.a())) {
                    this.m = o.b(next.b()).trim();
                } else if ("label".equalsIgnoreCase(next.a())) {
                    this.k = o.b(next.b()).trim();
                } else if ("code".equalsIgnoreCase(next.a())) {
                    this.o = o.b(next.b()).trim();
                }
                q.a(b, "processError Params ", next.a(), next.b());
            }
        }
        this.j = o.b(oVar.b()).trim();
        a(1);
    }

    public final Handler a() {
        try {
            this.u.await();
        } catch (InterruptedException e2) {
        }
        return this.w;
    }

    public final void a(l lVar) {
        q.b(b, "Starting Pay Service");
        if (this.g.a(this.f) != null) {
            this.v.sendEmptyMessage(1);
            this.h = false;
            this.i = true;
            this.j = null;
            ZongPaymentRequest a2 = lVar.a();
            this.c = a2.getSimulationMode().booleanValue();
            this.d = a2.getMno();
            this.e = a2.getPhoneNumber();
            HashMap hashMap = new HashMap();
            hashMap.put("purchaseKey", lVar.b());
            hashMap.put("transactionRef", a2.getTransactionRef());
            hashMap.put("itemDesc", lVar.c());
            hashMap.put("msisdn", a2.getPhoneNumber());
            hashMap.put("lang", a2.getLang());
            this.p.a(hashMap);
            return;
        }
        q.c(b, "processPayment failed due to missing context");
        a(2);
    }

    public final void b() {
        q.a(b, "Stopping Payment Processor");
        this.h = true;
        this.w.removeMessages(0);
        this.w.removeMessages(1);
        this.w.removeMessages(2);
        this.w.removeMessages(3);
        this.w.removeMessages(4);
        this.w.getLooper().quit();
        this.w = null;
        this.p.a();
        if (this.s != null) {
            this.s.cancel(true);
        }
        this.s = null;
        this.g.b(this.f);
        this.g = null;
        ExecutorService executorService = this.r;
        q.a(b, "Pool Shutdown");
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(2, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
                if (!executorService.awaitTermination(2, TimeUnit.SECONDS)) {
                    System.err.println("Pool did not terminate");
                }
            }
        } catch (InterruptedException e2) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }

    public void run() {
        Looper.prepare();
        this.w = new Handler() {
            public final void handleMessage(Message message) {
                PaymentProcessorTask.a(PaymentProcessorTask.this, message);
            }
        };
        this.u.countDown();
        this.r = Executors.newCachedThreadPool(new a());
        this.p = new A(this.r, this.q, a());
        this.t = new LinkedBlockingQueue<>(8);
        Looper.loop();
    }
}
