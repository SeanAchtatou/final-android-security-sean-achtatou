package com.zong.android.engine.task;

import android.content.Context;
import android.content.SharedPreferences;
import zongfuscated.q;

public class a {
    private static final String a = a.class.getSimpleName();

    /* synthetic */ a() {
        this((byte) 0);
    }

    private a(byte b) {
    }

    public static void a(Context context, String str) {
        q.a(a, "SAVING MSISDN " + str);
        SharedPreferences.Editor edit = context.getSharedPreferences("PhoneContext", 0).edit();
        edit.putString("msisdn", str);
        edit.commit();
    }

    public static String b(Context context, String str) {
        q.a(a, "GETTING MSISDN " + str);
        SharedPreferences sharedPreferences = context.getSharedPreferences("PhoneContext", 0);
        if (sharedPreferences != null) {
            return sharedPreferences.getString("msisdn", str);
        }
        return null;
    }
}
