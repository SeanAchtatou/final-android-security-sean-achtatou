package com.zong.android.engine.sms;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import zongfuscated.q;

public class a {
    /* access modifiers changed from: private */
    public static final String a = a.class.getSimpleName();
    private SmsManager b = SmsManager.getDefault();
    private PendingIntent c;
    private PendingIntent d;
    private b e = null;
    private c f = null;

    /* renamed from: com.zong.android.engine.sms.a$a  reason: collision with other inner class name */
    private static class C0000a {
        /* access modifiers changed from: private */
        public static final a a = new a();

        private C0000a() {
        }
    }

    private static class b extends BroadcastReceiver {
        /* synthetic */ b() {
            this((byte) 0);
        }

        private b(byte b) {
        }

        public final void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case -1:
                    q.a(a.a, "<SentReceiver> ********************** SMS sent");
                    return;
                case 0:
                default:
                    return;
                case 1:
                    q.a(a.a, "<SentReceiver> ********************** Generic failure");
                    return;
                case 2:
                    q.a(a.a, "<SentReceiver> ********************** Radio off");
                    return;
                case 3:
                    q.a(a.a, "<SentReceiver> ********************** Null PDU");
                    return;
                case 4:
                    q.a(a.a, "<SentReceiver> ********************** No service");
                    return;
            }
        }
    }

    private static class c extends BroadcastReceiver {
        /* synthetic */ c() {
            this((byte) 0);
        }

        private c(byte b) {
        }

        public final void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case -1:
                    q.a(a.a, "<SentReceiver> ********************** SMS delivered");
                    return;
                case 0:
                    q.a(a.a, "<SentReceiver> ********************** SMS not delivered");
                    return;
                default:
                    return;
            }
        }
    }

    public static a a() {
        return C0000a.a;
    }

    public final void a(Context context) {
        this.c = PendingIntent.getBroadcast(context, 0, new Intent("SMS_SENT"), 0);
        this.e = new b();
        this.d = PendingIntent.getBroadcast(context, 0, new Intent("SMS_DELIVERED"), 0);
        this.f = new c();
        q.a(a, "Registering SMS Broadcat receiver");
        context.registerReceiver(this.e, new IntentFilter("SMS_SENT"));
        context.registerReceiver(this.f, new IntentFilter("SMS_DELIVERED"));
    }

    public final void a(String str, String str2) {
        q.a(a, "SMS Sending ......................................");
        this.b.sendTextMessage(str, null, str2, this.c, this.d);
        q.a(a, "...................................... SMS Sent");
    }

    public final void b(Context context) {
        q.a(a, "Releasing SMS Broadcat receiver");
        context.unregisterReceiver(this.e);
        context.unregisterReceiver(this.f);
        this.e = null;
        this.f = null;
    }
}
