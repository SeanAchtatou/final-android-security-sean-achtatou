package com.zong.android.engine.xml.pojo.lookup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class ZongPricePoint implements Serializable {
    private static final long serialVersionUID = 2370090782716445022L;
    String countryCode;
    float exchangeRate;
    ArrayList<ZongPricePointItem> items = new ArrayList<>();
    String localCurrency;
    HashMap<String, String> providers = new HashMap<>();

    public String getCountryCode() {
        return this.countryCode;
    }

    public float getExchangeRate() {
        return this.exchangeRate;
    }

    public ArrayList<ZongPricePointItem> getItems() {
        return this.items;
    }

    public String getLocalCurrency() {
        return this.localCurrency;
    }

    public HashMap<String, String> getProviders() {
        return this.providers;
    }

    public void putProvider(String str, String str2) {
        this.providers.put(str, str2);
    }

    public void setCountryCode(String str) {
        this.countryCode = str;
    }

    public void setExchangeRate(float f) {
        this.exchangeRate = f;
    }

    public void setItems(ArrayList<ZongPricePointItem> arrayList) {
        this.items = arrayList;
    }

    public void setLocalCurrency(String str) {
        this.localCurrency = str;
    }
}
