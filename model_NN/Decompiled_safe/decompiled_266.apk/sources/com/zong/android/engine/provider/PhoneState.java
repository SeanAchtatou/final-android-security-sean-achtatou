package com.zong.android.engine.provider;

import android.telephony.TelephonyManager;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import java.util.regex.Pattern;
import zongfuscated.q;

public class PhoneState {
    private static final String a = PhoneState.class.getSimpleName();
    private static boolean b = true;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private Integer h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;

    public PhoneState(TelephonyManager telephonyManager) {
        this.c = telephonyManager.getDeviceId();
        this.d = telephonyManager.getLine1Number();
        this.e = telephonyManager.getNetworkCountryIso();
        this.f = telephonyManager.getNetworkOperator();
        this.g = telephonyManager.getNetworkOperatorName();
        this.h = Integer.valueOf(telephonyManager.getNetworkType());
        this.i = telephonyManager.getSimCountryIso();
        this.j = telephonyManager.getSimOperator();
        this.k = telephonyManager.getSimOperatorName();
        this.l = telephonyManager.getSimSerialNumber();
        this.m = telephonyManager.getSubscriberId();
    }

    private static String a(String str, String str2) {
        if (str != null) {
            try {
                PhoneNumberUtil instance = PhoneNumberUtil.getInstance();
                q.a(a, "(G) Starts parsing phone number");
                Phonenumber.PhoneNumber parse = instance.parse(str, str2);
                if (instance.isValidNumber(parse)) {
                    String format = instance.format(parse, PhoneNumberUtil.PhoneNumberFormat.E164);
                    q.a(a, "(G) Phone number matching accept", format);
                    return format;
                }
            } catch (Exception e2) {
                return null;
            }
        }
        return str;
    }

    private static String b(String str, String str2) {
        if (str == null) {
            return str;
        }
        try {
            q.a(a, "Phone Number exist", str);
            String replace = str.trim().replace(".", "").replace("-", "").replace("(", "").replace(")", "").replace("[", "").replace("]", "");
            q.a(a, "Phone Number filter", replace);
            if (!replace.startsWith("+")) {
                q.a(a, "Doesn't have a (+)");
                E164 fromIsoCountryCode = E164.fromIsoCountryCode(str2);
                if (replace.startsWith(Integer.toString(fromIsoCountryCode.getCountryPrefix()))) {
                    q.a(a, "Starts with country code");
                    replace = "+" + replace;
                } else {
                    q.a(a, "Doen't starts with country code");
                    String localPrefix = fromIsoCountryCode.getLocalPrefix();
                    if (localPrefix != null) {
                        q.a(a, "Local prefix for that country");
                        if (replace.startsWith(localPrefix)) {
                            q.a(a, "Starts with local prefix");
                            replace = String.valueOf(E164.getCountryPrefix(str2)) + replace.substring(localPrefix.length());
                        } else {
                            q.a(a, "Doesn't start with local prefix");
                            replace = String.valueOf(E164.getCountryPrefix(str2)) + replace;
                        }
                    } else {
                        q.a(a, "No local prefix for that country");
                        replace = String.valueOf(E164.getCountryPrefix(str2)) + replace;
                    }
                }
            }
            if (!Pattern.compile("(\\+[0-9]{7,})").matcher(replace).find()) {
                q.a(a, "Final matching reject");
                return null;
            }
            q.a(a, "Final matching accept", replace);
            return replace;
        } catch (Exception e2) {
            return null;
        }
    }

    public String getImeaId() {
        return this.c;
    }

    public String getLineNumber() {
        return this.d;
    }

    @Deprecated
    public String getMsisdn() {
        String lineNumber = getLineNumber();
        return lineNumber != null ? !lineNumber.startsWith("+") ? "+" + lineNumber : lineNumber : "null";
    }

    public String getMsisdn(String str) {
        return b ? a(getLineNumber(), str) : b(getLineNumber(), str);
    }

    public String getMsisdn(String str, String str2) {
        return b ? a(str, str2) : b(str, str2);
    }

    public String getNetIsoCountry() {
        return this.e;
    }

    public String getNetOp() {
        return this.f;
    }

    public String getNetOpName() {
        return this.g;
    }

    public Integer getNetType() {
        return this.h;
    }

    public String getSimIsoCountry() {
        return this.i;
    }

    public String getSimOp() {
        return this.j;
    }

    public String getSimOpName() {
        return this.k;
    }

    public String getSimSerial() {
        return this.l;
    }

    public String getSubsciberId() {
        return this.m;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("PHONE STATE:\n");
        sb.append(" imeaId(").append(this.c).append(")\n");
        sb.append(" lineNumber(").append(this.d).append(")\n");
        sb.append(" netIsoCountry(").append(this.e).append(")\n");
        sb.append(" netOp(").append(this.f).append(")\n");
        sb.append(" netOpName(").append(this.g).append(")\n");
        sb.append(" netType(").append(this.h).append(")\n");
        sb.append(" simIsoCountry(").append(this.i).append(")\n");
        sb.append(" simOp(").append(this.j).append(")\n");
        sb.append(" simOpName(").append(this.k).append(")\n");
        sb.append(" simSerial(").append(this.l).append(")\n");
        sb.append(" subsciberId(").append(this.m).append(")\n");
        return sb.toString();
    }
}
