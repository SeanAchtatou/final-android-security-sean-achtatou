package com.zong.android.engine.activities;

import android.os.Parcel;
import android.os.Parcelable;

class b implements Parcelable.Creator<ZongPaymentRequest> {
    b() {
    }

    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new ZongPaymentRequest(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new ZongPaymentRequest[i];
    }
}
