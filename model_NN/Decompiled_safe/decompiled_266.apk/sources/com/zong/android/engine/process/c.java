package com.zong.android.engine.process;

import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import com.zong.android.engine.activities.ZongPaymentRequest;
import zongfuscated.F;
import zongfuscated.l;
import zongfuscated.q;

class c implements Handler.Callback {
    private /* synthetic */ ZongServiceProcess a;

    c(ZongServiceProcess zongServiceProcess) {
        this.a = zongServiceProcess;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, boolean):void
     arg types: [com.zong.android.engine.process.ZongServiceProcess, int]
     candidates:
      com.zong.android.engine.process.ZongServiceProcess.a(android.content.ContentValues, java.lang.String):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, android.content.ContentValues):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, android.os.Message):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, android.os.Messenger):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, com.zong.android.engine.activities.ZongPaymentRequest):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, java.util.HashMap):void
      com.zong.android.engine.process.ZongServiceProcess.a(java.util.HashMap, java.lang.String):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, boolean):void */
    private void a() {
        this.a.m = false;
        if (this.a.o != null && this.a.n != null) {
            q.a(ZongServiceProcess.a, "Restoring Messaging to Client", Integer.toString(this.a.n.what));
            try {
                this.a.o.send(this.a.n);
            } catch (RemoteException e) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, boolean):void
     arg types: [com.zong.android.engine.process.ZongServiceProcess, int]
     candidates:
      com.zong.android.engine.process.ZongServiceProcess.a(android.content.ContentValues, java.lang.String):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, android.content.ContentValues):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, android.os.Message):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, android.os.Messenger):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, com.zong.android.engine.activities.ZongPaymentRequest):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, java.util.HashMap):void
      com.zong.android.engine.process.ZongServiceProcess.a(java.util.HashMap, java.lang.String):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, boolean):void */
    public final boolean handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.a.m = false;
                this.a.o = message.replyTo;
                this.a.j = (ZongPaymentRequest) message.obj;
                q.a(this.a.j.getDebugMode().booleanValue());
                this.a.c();
                this.a.d();
                break;
            case 2:
                this.a.m = false;
                this.a.o = message.replyTo;
                this.a.j = (ZongPaymentRequest) message.obj;
                q.a(this.a.j.getDebugMode().booleanValue());
                this.a.c();
                this.a.r.sendMessage(this.a.r.obtainMessage(4, new F()));
                break;
            case 3:
                this.a.m = false;
                this.a.o = message.replyTo;
                this.a.j = (ZongPaymentRequest) message.obj;
                q.a(this.a.j.getDebugMode().booleanValue());
                this.a.c();
                this.a.d();
                break;
            case 4:
                this.a.o = (Messenger) null;
                break;
            case 5:
                this.a.m = true;
                break;
            case 6:
                if (this.a.m) {
                    a();
                    break;
                }
                break;
            case 7:
                a();
                break;
            case 8:
                this.a.i.a((l) message.obj);
                break;
            case 9:
                ZongServiceProcess.j(this.a);
                break;
        }
        return false;
    }
}
