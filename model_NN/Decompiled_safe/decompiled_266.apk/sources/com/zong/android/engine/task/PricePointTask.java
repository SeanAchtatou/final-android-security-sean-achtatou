package com.zong.android.engine.task;

import android.content.Context;
import android.content.SharedPreferences;
import com.zong.android.engine.xml.pojo.lookup.ZongPricePoint;
import com.zong.android.engine.xml.pojo.lookup.ZongPricePointItem;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import zongfuscated.g;
import zongfuscated.q;
import zongfuscated.y;

public class PricePointTask {
    private static final String a = PricePointTask.class.getSimpleName();

    private static class a {
        /* access modifiers changed from: private */
        public static final PricePointTask a = new PricePointTask();

        private a() {
        }
    }

    /* synthetic */ PricePointTask() {
        this((byte) 0);
    }

    private PricePointTask(byte b) {
    }

    private static ZongPricePoint a(String str) {
        ZongPricePoint zongPricePoint;
        Exception e;
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(g.a(str)));
            ZongPricePoint zongPricePoint2 = (ZongPricePoint) objectInputStream.readObject();
            try {
                objectInputStream.close();
                return zongPricePoint2;
            } catch (Exception e2) {
                e = e2;
                zongPricePoint = zongPricePoint2;
                q.a(a, "Failed extractFromBlob", e);
                return zongPricePoint;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            zongPricePoint = null;
            e = exc;
            q.a(a, "Failed extractFromBlob", e);
            return zongPricePoint;
        }
    }

    private static String a(ZongPricePoint zongPricePoint) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(zongPricePoint);
            objectOutputStream.flush();
            objectOutputStream.close();
            return new String(g.a(byteArrayOutputStream.toByteArray()));
        } catch (IOException e) {
            q.a(a, "Failed makeBlob", e);
            return null;
        }
    }

    public static PricePointTask getInstance() {
        return a.a;
    }

    public void flushCache(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("PricePointsCache", 0);
        if (sharedPreferences != null) {
            sharedPreferences.edit().clear().commit();
        }
    }

    public ZongPricePoint getPricePoints(Context context, boolean z, String str, String str2, String str3, String str4) {
        ZongPricePoint zongPricePoint = null;
        if (z) {
            q.a(a, "Enable PricePoints CACHE");
            SharedPreferences sharedPreferences = context.getSharedPreferences("PricePointsCache", 0);
            if (sharedPreferences != null) {
                if (System.currentTimeMillis() - Long.valueOf(sharedPreferences.getLong("time", 0)).longValue() < 21600000) {
                    String string = sharedPreferences.getString("country", null);
                    String string2 = sharedPreferences.getString("currencyCode", null);
                    String string3 = sharedPreferences.getString("clientRef", null);
                    if (!string.equalsIgnoreCase(str3) || !string2.equalsIgnoreCase(str4) || !string3.equalsIgnoreCase(str2)) {
                        q.a(a, "Owner change:: Flushing PricePoints CACHE");
                    } else {
                        q.a(a, "Using PricePoints CACHE");
                        zongPricePoint = a(sharedPreferences.getString("pricePoints", null));
                    }
                } else {
                    q.a(a, "Time-to-Live expired:: Flushing PricePoints CACHE");
                }
            }
        } else {
            q.a(a, "Disable PricePoints CACHE");
        }
        if (zongPricePoint != null) {
            return zongPricePoint;
        }
        y yVar = new y(str, str2, str3, str4);
        ZongPricePoint zongPricePoint2 = zongPricePoint;
        int i = 0;
        while (zongPricePoint2 == null && i < 3) {
            i++;
            zongPricePoint2 = yVar.a();
            if (zongPricePoint2 != null) {
                q.a(a, "Ctry", zongPricePoint2.getCountryCode());
                q.a(a, "Cur", zongPricePoint2.getLocalCurrency());
                for (String next : zongPricePoint2.getProviders().keySet()) {
                    q.a(a, "Root Prov Key", next, zongPricePoint2.getProviders().get(next));
                }
                Iterator<ZongPricePointItem> it = zongPricePoint2.getItems().iterator();
                while (it.hasNext()) {
                    ZongPricePointItem next2 = it.next();
                    q.a(a, "Ref", next2.getItemRef());
                    q.a(a, "URL", next2.getEntrypointUrl());
                    if (next2.getProviders() != null) {
                        for (String next3 : next2.getProviders().keySet()) {
                            q.a(a, "Item Key", next3, zongPricePoint2.getProviders().get(next3));
                        }
                    }
                }
                if (z) {
                    q.a(a, "Created PricePoints CACHE");
                    flushCache(context);
                    SharedPreferences.Editor edit = context.getSharedPreferences("PricePointsCache", 0).edit();
                    edit.putLong("time", System.currentTimeMillis());
                    edit.putString("clientRef", str2);
                    edit.putString("country", str3);
                    edit.putString("currencyCode", str4);
                    edit.putString("pricePoints", a(zongPricePoint2));
                    edit.commit();
                }
            }
        }
        return zongPricePoint2;
    }
}
