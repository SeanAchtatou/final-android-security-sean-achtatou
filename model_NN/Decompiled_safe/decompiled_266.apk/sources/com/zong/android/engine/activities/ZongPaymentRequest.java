package com.zong.android.engine.activities;

import android.os.Parcel;
import android.os.Parcelable;
import com.papaya.social.PPYSocial;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import zongfuscated.q;

public class ZongPaymentRequest implements Parcelable {
    public static final Parcelable.Creator<ZongPaymentRequest> CREATOR = new b();
    private static final String a = ZongPaymentRequest.class.getSimpleName();
    private Boolean b;
    private Boolean c;
    private Boolean d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;
    private ArrayList<ZongPricePointsElement> n;

    public ZongPaymentRequest() {
        this.b = false;
        this.c = false;
        this.d = false;
        this.e = "ZongApp";
        this.h = PPYSocial.LANG_EN;
        this.i = "US";
        this.j = "USD";
        this.n = null;
        this.n = new ArrayList<>();
    }

    /* synthetic */ ZongPaymentRequest(Parcel parcel) {
        this(parcel, (byte) 0);
    }

    private ZongPaymentRequest(Parcel parcel, byte b2) {
        this.b = false;
        this.c = false;
        this.d = false;
        this.e = "ZongApp";
        this.h = PPYSocial.LANG_EN;
        this.i = "US";
        this.j = "USD";
        this.n = null;
        this.b = Boolean.valueOf(Boolean.parseBoolean(parcel.readString()));
        this.c = Boolean.valueOf(Boolean.parseBoolean(parcel.readString()));
        this.d = Boolean.valueOf(Boolean.parseBoolean(parcel.readString()));
        this.e = parcel.readString();
        this.f = parcel.readString();
        this.g = parcel.readString();
        this.h = parcel.readString();
        this.i = parcel.readString();
        this.j = parcel.readString();
        this.k = parcel.readString();
        this.l = parcel.readString();
        this.m = parcel.readString();
        ZongPricePointsElement[] zongPricePointsElementArr = (ZongPricePointsElement[]) ZongPricePointsElement.CREATOR.newArray(parcel.readInt());
        parcel.readTypedArray(zongPricePointsElementArr, ZongPricePointsElement.CREATOR);
        this.n = new ArrayList<>();
        for (ZongPricePointsElement add : zongPricePointsElementArr) {
            this.n.add(add);
        }
    }

    public void addPricePoint(String str, float f2, int i2, String str2, String str3) {
        this.n.add(new ZongPricePointsElement(str, f2, i2, str2, str3));
    }

    public int describeContents() {
        return 0;
    }

    public void flushPricepointsList() {
        if (this.n != null) {
            this.n.clear();
        }
    }

    public String getAppName() {
        return this.e;
    }

    public String getCountry() {
        return this.i;
    }

    public String getCurrency() {
        return this.j;
    }

    public NumberFormat getCurrencyFormatter() {
        NumberFormat currencyInstance = NumberFormat.getCurrencyInstance(new Locale(getLang(), getCountry()));
        currencyInstance.setMinimumFractionDigits(2);
        currencyInstance.setMaximumFractionDigits(2);
        return currencyInstance;
    }

    public String getCustomerKey() {
        return this.f;
    }

    public Boolean getDebugMode() {
        return this.c;
    }

    public String getLang() {
        return this.h;
    }

    public String getMno() {
        if (this.l != null) {
            return this.l;
        }
        if (getCountry().equalsIgnoreCase("CA")) {
            q.a(a, "Using MNO", "CA - Bell Mobility - 302610");
            return "302610";
        } else if (getCountry().equalsIgnoreCase("IT")) {
            q.a(a, "Using MNO", "IT - TIM - 22201");
            return "22201";
        } else if (getCountry().equalsIgnoreCase("ZA")) {
            q.a(a, "Using MNO", "ZA - Vodacom - 65501");
            return "65501";
        } else if (getCountry().equalsIgnoreCase("NZ")) {
            q.a(a, "Using MNO", "NZ - Telecom NZ - 53003");
            return "53003";
        } else if (getCountry().equalsIgnoreCase("SE")) {
            q.a(a, "Using MNO", "SE - Hi3G Access AB - 24002");
            return "24002";
        } else if (getCountry().equalsIgnoreCase("DK")) {
            q.a(a, "Using MNO", "DK - TDC Mobil - 23801");
            return "23801";
        } else {
            q.a(a, "Dummy MNO", "0123456789");
            return "0123456789";
        }
    }

    public String getPhoneNumber() {
        return this.k;
    }

    public ArrayList<ZongPricePointsElement> getPricepointsList() {
        return this.n;
    }

    public Boolean getSimulationMode() {
        return this.b;
    }

    public Boolean getTraceMode() {
        return this.d;
    }

    public String getTransactionRef() {
        return this.m;
    }

    public String getUrl() {
        return this.g;
    }

    public void setAppName(String str) {
        this.e = str;
    }

    public void setCountry(String str) {
        this.i = str;
    }

    public void setCurrency(String str) {
        this.j = str;
    }

    public void setCustomerKey(String str) {
        this.f = str;
    }

    public void setDebugMode(Boolean bool) {
        this.c = bool;
    }

    public void setLang(String str) {
        this.h = str;
    }

    public void setMno(String str) {
        this.l = str;
    }

    public void setPhoneNumber(String str) {
        this.k = str;
    }

    public void setPricepointsList(ArrayList<ZongPricePointsElement> arrayList) {
        this.n = arrayList;
    }

    public void setSimulationMode(Boolean bool) {
        this.b = bool;
    }

    public void setTraceMode(Boolean bool) {
        this.d = bool;
    }

    public void setTransactionRef(String str) {
        this.m = str;
    }

    public void setUrl(String str) {
        this.g = str;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("\n");
        sb.append("simulationMode: ").append(this.b).append("\n");
        sb.append("debugMode: ").append(this.c).append("\n");
        sb.append("traceMode: ").append(this.d).append("\n");
        sb.append("appName: ").append(this.e).append("\n");
        sb.append("customerKey: ").append(this.f).append("\n");
        sb.append("url: ").append(this.g).append("\n");
        sb.append("lang: ").append(this.h).append("\n");
        sb.append("country: ").append(this.i).append("\n");
        sb.append("currency: ").append(this.j).append("\n");
        sb.append("phoneNumber: ").append(this.k).append("\n");
        sb.append("mno: ").append(this.l).append("\n");
        sb.append("transactionRef: ").append(this.m).append("\n");
        sb.append("Price Point List: ").append("\n");
        Iterator<ZongPricePointsElement> it = this.n.iterator();
        while (it.hasNext()) {
            sb.append("---------").append("\n");
            sb.append(it.next().toString()).append("\n");
        }
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.b.toString());
        parcel.writeString(this.c.toString());
        parcel.writeString(this.d.toString());
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        parcel.writeString(this.h);
        parcel.writeString(this.i);
        parcel.writeString(this.j);
        parcel.writeString(this.k);
        parcel.writeString(this.l);
        parcel.writeString(this.m);
        int size = this.n.size();
        parcel.writeInt(size);
        parcel.writeTypedArray((ZongPricePointsElement[]) this.n.toArray(new ZongPricePointsElement[size]), i2);
    }
}
