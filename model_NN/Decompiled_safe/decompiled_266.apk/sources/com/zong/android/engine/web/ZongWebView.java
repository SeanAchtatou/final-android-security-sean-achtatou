package com.zong.android.engine.web;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.openfeint.internal.request.multipart.StringPart;
import com.zong.android.engine.ZpMoConst;
import com.zong.android.engine.activities.ZongPaymentRequest;
import com.zong.android.engine.activities.ZongPricePointsElement;
import com.zong.android.engine.process.ZongActivityProcess;
import com.zong.android.engine.provider.ZongPhoneManager;
import com.zong.android.engine.task.a;
import java.text.NumberFormat;
import zongfuscated.C0062b;
import zongfuscated.C0063c;
import zongfuscated.F;
import zongfuscated.p;
import zongfuscated.q;

public class ZongWebView extends ZongActivityProcess {
    /* access modifiers changed from: private */
    public static final String c = ZongWebView.class.getSimpleName();
    private static /* synthetic */ boolean g = (!ZongWebView.class.desiredAssertionStatus());
    /* access modifiers changed from: private */
    public NumberFormat d;
    /* access modifiers changed from: private */
    public WebView e;
    private C0063c f;

    public final class ZgJavaScriptInterface {
        ZgJavaScriptInterface() {
        }

        public final void buy(final int i, final String str) {
            ZongWebView.this.runOnUiThread(new Runnable() {
                public final void run() {
                    ZongWebView.this.a(i);
                    Context applicationContext = ZongWebView.this.getApplicationContext();
                    String msisdn = ZongPhoneManager.getInstance().getPhoneState(applicationContext).getMsisdn(str, ZongWebView.this.h().getCountry());
                    ZongWebView.this.h().setPhoneNumber(msisdn);
                    a.a(applicationContext, ZongWebView.this.h().getPhoneNumber());
                    q.a(ZongWebView.c, "BUY Selection", Integer.toString(i), str, msisdn);
                    ZongWebView.this.b();
                }
            });
        }

        public final void error() {
            q.a(ZongWebView.c, "Error JavaScript Executed");
            ZongWebView.this.a();
        }

        public final void finish() {
            q.a(ZongWebView.c, "Finish JavaScript Executed");
            ZongWebView.this.c();
        }

        public final String getPricePointsDescArray() {
            return ZongWebView.this.g().b();
        }

        public final String getPricePointsValueArray() {
            return ZongWebView.this.g().c();
        }

        public final void loadCompletedView() {
            ZongWebView.this.runOnUiThread(new Runnable() {
                public final void run() {
                    q.a(ZongWebView.c, "Load Completed View JavaScript Executed");
                    ZongPricePointsElement a2 = ZongWebView.this.a(ZongWebView.this.h());
                    ZongWebView.this.e.loadUrl("javascript:initCompletedView('" + a2.c() + "','" + ZongWebView.this.d.format((double) a2.getAmount()) + "')");
                }
            });
        }

        public final void loadPayView() {
            ZongWebView.this.runOnUiThread(new Runnable() {
                public final void run() {
                    q.a(ZongWebView.c, "Load Pay View JavaScript Executed");
                    ZongWebView.this.e.loadUrl("javascript:initIntlPayView('" + Boolean.toString(ZongWebView.this.g().a().booleanValue()) + "', '" + ZongWebView.this.h().getPhoneNumber() + "', '" + ZongWebView.this.h().getMno() + "', '" + ZongWebView.this.h().getCountry() + "', '" + ZongWebView.this.h().getLang() + "', '" + ZongWebView.this.h().getCurrency() + "', '" + ZongWebView.this.h().getCustomerKey() + "')");
                }
            });
        }
    }

    private void c(String str) {
        q.a(c, "Loading HTML page in viewer");
        this.e.loadDataWithBaseURL(g().g().a(ZpMoConst.ZONG_PAY_ROOT_CONTEXT), str, StringPart.DEFAULT_CONTENT_TYPE, "utf-8", null);
    }

    public final void a(F f2) {
        super.a(f2);
        this.f.a(a("android.zong.gui.wait"));
        c(f2.e());
    }

    public final void a(C0062b bVar) {
        super.a(bVar);
        if (e()) {
            q.a(c, "Payment Status OK");
            c(bVar.c());
        } else if (f() == 1) {
            q.a(c, "Payment Status ERROR");
            c(bVar.c());
        } else {
            q.a(c, "Payment Status STOP");
            new AlertDialog.Builder(this).setMessage(a("android.zong.gui.error")).setPositiveButton("OK", (DialogInterface.OnClickListener) null).show();
            a();
        }
    }

    public final void a(p pVar) {
        super.a(pVar);
        String a = pVar.a();
        if (a != null) {
            this.e.loadUrl("javascript:setStatusBar('" + a + "')");
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.e.loadUrl("javascript:setProgressDescription('" + this.d.format(Float.valueOf(a(h()).getAmount())) + "')");
        super.b();
    }

    public final void d() {
        super.d();
        q.a(c, "Starting Application Listenner");
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        q.a(c, "LifeCycle Event: onCreate");
        this.a = 1;
        this.e = new WebView(this);
        this.e.setId(100809);
        this.e.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.e.setVerticalScrollBarEnabled(false);
        this.e.setVisibility(0);
        setContentView(this.e);
        WebSettings settings = this.e.getSettings();
        settings.setSavePassword(false);
        settings.setSaveFormData(false);
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(false);
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        settings.setCacheMode(1);
        this.f = new C0063c(this);
        this.e.setWebViewClient(this.f);
        this.e.addJavascriptInterface(new ZgJavaScriptInterface(), "zong");
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            b((ZongPaymentRequest) extras.getParcelable(ZpMoConst.ZONG_MOBILE_PAYMENT_BUNDLE_KEY));
            if (g || h() != null) {
                q.a(h().getDebugMode().booleanValue());
                this.d = h().getCurrencyFormatter();
                return;
            }
            throw new AssertionError();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        q.a(c, "LifeCycle Event: onKeyDown");
        if (i != 4 || keyEvent.getRepeatCount() != 0) {
            return super.onKeyDown(i, keyEvent);
        }
        q.a(c, "Exiting App with onKeyDown Event");
        c();
        return true;
    }
}
