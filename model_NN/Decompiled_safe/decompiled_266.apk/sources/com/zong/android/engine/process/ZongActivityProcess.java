package com.zong.android.engine.process;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import com.zong.android.engine.ZpMoConst;
import com.zong.android.engine.activities.ZongPaymentRequest;
import com.zong.android.engine.activities.ZongPricePointsElement;
import java.util.HashSet;
import java.util.StringTokenizer;
import zongfuscated.C0062b;
import zongfuscated.F;
import zongfuscated.l;
import zongfuscated.p;
import zongfuscated.q;

public abstract class ZongActivityProcess extends Activity {
    /* access modifiers changed from: private */
    public static final String c = ZongActivityProcess.class.getSimpleName();
    protected int a = 0;
    Messenger b = null;
    private boolean d;
    private int e;
    private int f = 0;
    private String g;
    private String h;
    private HashSet<String> i;
    private ZongPaymentRequest j;
    private F k;
    private final Handler l = new Handler(new b(this));
    private Messenger m = new Messenger(this.l);
    private boolean n;
    private ServiceConnection o = null;

    private final class a implements ServiceConnection {
        private int a;

        public a(int i) {
            this.a = i;
        }

        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            ZongActivityProcess.this.b = new Messenger(iBinder);
            q.a(ZongActivityProcess.c, "Service LifeCycle Event: onServiceConnected");
            ZongActivityProcess.this.a(this.a, ZongActivityProcess.this.h());
        }

        public final void onServiceDisconnected(ComponentName componentName) {
            ZongActivityProcess.this.b = null;
            q.a(ZongActivityProcess.c, "Service LifeCycle Event: onServiceDisconnected");
        }
    }

    private void b(int i2) {
        q.a(c, "LifeCycle Event: onStart");
        if (!this.n) {
            this.o = new a(i2);
            this.n = bindService(new Intent(this, ZongServiceProcess.class), this.o, 1);
        }
    }

    /* access modifiers changed from: protected */
    public final ZongPricePointsElement a(ZongPaymentRequest zongPaymentRequest) {
        return zongPaymentRequest.getPricepointsList().get(this.f);
    }

    /* access modifiers changed from: protected */
    public final String a(String str) {
        ContentValues f2 = this.k.f();
        return (f2 == null || !f2.containsKey(str)) ? str : (String) f2.get(str);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        a(9, null);
    }

    public final void a(int i2) {
        this.f = i2;
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, Object obj) {
        Message obtain;
        if (this.b != null) {
            if (obj == null) {
                try {
                    obtain = Message.obtain((Handler) null, i2);
                } catch (RemoteException e2) {
                    return;
                }
            } else {
                obtain = Message.obtain(null, i2, obj);
            }
            obtain.replyTo = this.m;
            this.b.send(obtain);
        }
    }

    public void a(F f2) {
        this.k = f2;
        if (f2.d() != null) {
            this.i = new HashSet<>();
            StringTokenizer stringTokenizer = new StringTokenizer(f2.d(), "|");
            while (stringTokenizer.hasMoreTokens()) {
                this.i.add(stringTokenizer.nextToken());
            }
        }
    }

    public void a(C0062b bVar) {
        this.d = bVar.a();
        this.e = bVar.b();
        if (this.d) {
            return;
        }
        if (this.e == 1) {
            this.g = bVar.e();
            this.h = bVar.d();
            return;
        }
        this.g = "error.UNKNOWED";
        this.h = "";
    }

    public void a(p pVar) {
    }

    /* access modifiers changed from: protected */
    public void b() {
        l lVar = new l();
        lVar.a(this.j);
        ZongPricePointsElement a2 = a(this.j);
        String a3 = a2.a();
        String b2 = a2.b();
        lVar.a(a3);
        lVar.b(b2);
        q.a(c, this.j.toString());
        a(8, lVar);
        q.a(c, "Excute Payment");
    }

    public final void b(ZongPaymentRequest zongPaymentRequest) {
        this.j = zongPaymentRequest;
    }

    /* access modifiers changed from: protected */
    public final void b(String str) {
        this.d = false;
        this.e = 2;
        this.g = "User";
        this.h = str;
        c();
    }

    /* access modifiers changed from: protected */
    public final void c() {
        Intent intent = new Intent();
        if (this.d) {
            intent.putExtra(ZpMoConst.ZONG_MOBILE_RESPONSE_PRICEPOINT_INDEX, this.f);
            intent.putExtra(ZpMoConst.ZONG_MOBILE_RESPONSE_REQUEST_OBJECT, this.j);
            setResult(-1, intent);
        } else {
            intent.putExtra(ZpMoConst.ZONG_MOBILE_RESPONSE_ERROR_CODE, this.g);
            intent.putExtra(ZpMoConst.ZONG_MOBILE_RESPONSE_ERROR_LABEL, this.h);
            setResult(this.e, intent);
        }
        finish();
    }

    public void d() {
    }

    public final boolean e() {
        return this.d;
    }

    public final int f() {
        return this.e;
    }

    public final F g() {
        return this.k;
    }

    public final ZongPaymentRequest h() {
        return this.j;
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        q.a(c, "LifeCycle Event: onConfigurationChanged");
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        q.a(c, "LifeCycle Event: onCreate");
        this.n = false;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        q.a(c, "LifeCycle Event: onDestroy");
        if (this.n) {
            a(4, null);
            unbindService(this.o);
            this.n = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        q.a(c, "LifeCycle Event: onPause");
        a(5, null);
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        q.a(c, "LifeCycle Event: onRestart");
        a(7, null);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        q.a(c, "LifeCycle Event: onResume");
        a(6, null);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        q.a(c, "LifeCycle Event: onStart");
        switch (this.a) {
            case 0:
                b(2);
                return;
            case 1:
                b(1);
                return;
            case 2:
                b(3);
                return;
            default:
                return;
        }
    }
}
