package com.zong.android.engine.process;

import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import zongfuscated.q;

class a implements Handler.Callback {
    private /* synthetic */ ZongServiceProcess a;

    a(ZongServiceProcess zongServiceProcess) {
        this.a = zongServiceProcess;
    }

    public final boolean handleMessage(Message message) {
        Message obtain;
        switch (message.what) {
            case 1:
                obtain = Message.obtain(null, message.what, null);
                break;
            case 2:
                obtain = Message.obtain(null, message.what, message.obj);
                break;
            case 3:
                obtain = Message.obtain(null, message.what, message.obj);
                break;
            case 4:
                obtain = Message.obtain(null, message.what, message.obj);
                break;
            default:
                obtain = null;
                break;
        }
        this.a.n = obtain;
        if (!(this.a.m || this.a.o == null || obtain == null)) {
            q.a(ZongServiceProcess.a, "ReplyingTo Client", Integer.toString(message.what));
            try {
                this.a.o.send(obtain);
                this.a.n = (Message) null;
            } catch (RemoteException e) {
            }
        }
        return false;
    }
}
