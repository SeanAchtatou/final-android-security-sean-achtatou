package com.zong.android.engine.provider;

import com.ansca.corona.version.AndroidVersion;

public enum E164 {
    AD(376),
    AE(971),
    AF(93),
    AG(1),
    AL(355),
    AM(374),
    AN(599),
    AO(244),
    AQ(672),
    AR(54, "0", new String[]{"11", "22", "23", "24", "26", "29", "33", "34", "35", "37", "38"}, 11, 14),
    AS(684),
    AT(43, "0", new String[]{"6"}, 11, 15),
    AU(61, "0", new String[]{"4"}, 11, 11),
    AW(297),
    AZ(994),
    BA(387),
    BB(1246),
    BD(880),
    BE(32, "0", new String[]{"4"}, 11, 11),
    BF(226),
    BG(359),
    BH(973),
    BI(257),
    BJ(229),
    BM(1441),
    BN(673),
    BO(591),
    BR(55, "0", null, 12, 12),
    BS(1242),
    BT(975),
    BW(267),
    BY(375),
    BZ(501),
    CA,
    CD(243),
    CF(236),
    CG(242),
    CH(41, "0", new String[]{"76", "77", "78", "79"}, 11, 11),
    CI(225),
    CK(682),
    CL(56, null, new String[]{"92", "93", "94", "95", "96", "97", "98", "99"}, 8, 11),
    CM(237),
    CN(86),
    CO(57, null, new String[]{"30", "31", "32"}, 12, 12, true),
    CR(506),
    CU(53),
    CV(238),
    CY(357),
    CZ(420, null, new String[]{"6", "7"}, 12, 12),
    DE(49, "0", new String[]{"1"}, 12, 13),
    DJ(253),
    DK(45, null, new String[]{"2", "3", "4", "5", "6", "7"}, 9, 10, true),
    DO(1),
    DZ(213),
    EC(593),
    EE(372),
    EG(20),
    ER(291),
    ES(34, null, new String[]{"6"}, 11, 11),
    ET(251),
    FI(358, "0", new String[]{"4", "5"}, 9, 14),
    FJ(679),
    FM(691),
    FO(298),
    FR(33, "0", new String[]{"6", "7"}, 11, 11),
    GA(241),
    GB(44, "0", new String[]{"7"}, 12, 12),
    GE(995),
    GF(594),
    GH(233),
    GI(350),
    GL(299),
    GM(220),
    GN(224),
    GP(590),
    GQ(240),
    GR(30, null, new String[]{"693", "694", "695", "697", "698", "690", "699"}, 12, 12),
    GT(502),
    GW(245),
    GY(592),
    HK(852, null, null, 11, 11),
    HN(504),
    HR(385),
    HT(509),
    HU(36, "06", new String[]{"20", "30", "60", "70"}, 10, 11),
    ID(62, null, new String[]{"2", AndroidVersion.apiVersion}, 11, 14),
    IE(353, "0", new String[]{AndroidVersion.apiVersion}, 12, 13),
    IL(972),
    IN(91, "0", new String[]{"7", AndroidVersion.apiVersion, "9"}, 12, 12),
    IQ(964),
    IR(98),
    IS(354),
    IT(39, null, new String[]{"3"}, 11, 12, true),
    JM(1),
    JO(962),
    JP(81),
    KE(254),
    KG(996),
    KH(855),
    KI(686),
    KM(269),
    KP(850),
    KR(82),
    KW(965),
    KY(1345),
    KZ(7),
    LA(856),
    LB(961),
    LI(423),
    LK(94),
    LR(231),
    LS(266),
    LT(370),
    LU(352),
    LV(371),
    LY(218),
    MA(212),
    MC(377),
    MD(373),
    MG(261),
    MK(389),
    ML(223),
    MM(95),
    MN(976),
    MO(853),
    MR(222),
    MS(1664),
    MT(356),
    MU(230),
    MV(960),
    MW(265),
    MX(52, null, new String[]{"2", "3", "4", "5", "6", "7", AndroidVersion.apiVersion, "9"}, 12, 12, true),
    MY(60, "0", new String[]{"10", "11", "12", "13", "14", "15", "16", "17", "18", "19"}, 11, 12),
    MZ(258),
    NA(264),
    NC(687),
    NE(227),
    NF(672),
    NG(234),
    NI(505),
    NL(31, "0", new String[]{"6"}, 11, 11),
    NO(47, null, new String[]{"4", "9"}, 10, 12),
    NP(977),
    NR(674),
    NZ(64, "0", new String[]{"21", "25", "26", "27", "28", "29"}, 10, 12),
    OM(968),
    PA(507),
    PE(51, "0", new String[]{"9"}, 11, 11),
    PF(689),
    PG(675),
    PH(63, "0", new String[]{"9"}, 11, 12),
    PK(92),
    PL(48, "0", new String[]{"5", "6", "7", AndroidVersion.apiVersion}, 11, 11),
    PM(508),
    PS(970),
    PT(351, null, new String[]{"9"}, 12, 12),
    PY(595),
    QA(974),
    RE(262),
    RO(40),
    RU(7, null, new String[]{"9"}, 11, 11),
    RW(250),
    SA(966),
    SB(677),
    SC(248),
    SD(249),
    SE(46, "0", new String[]{"7"}, 11, 11, true),
    SG(65, null, null, 9, 10),
    SI(386),
    SK(421),
    SL(232),
    SM(378),
    SN(221),
    SO(252),
    SR(597),
    ST(239),
    SV(503),
    SY(963),
    SZ(268),
    TC(1649),
    TD(235),
    TG(228),
    TH(66),
    TJ(992),
    TL(670),
    TM(993),
    TN(216),
    TO(676),
    TR(90, null, new String[]{"50", "53", "54", "55"}, 12, 12, true),
    TT(1868),
    TW(886, "0", new String[]{"91", "92", "93", "95", "96", "97", "98"}, 11, 13),
    TZ(255),
    UA(380),
    UG(256),
    US,
    UY(598),
    UZ(998),
    VA(379),
    VE(58, "0", new String[]{"412", "414", "424", "415", "416", "426", "417", "418"}, 12, 12),
    VN(84),
    VU(678),
    WF(681),
    WS(685),
    YE(967),
    ZA(27, "0", new String[]{"7", AndroidVersion.apiVersion}, 7, 14, true),
    ZM(260),
    ZW(263);
    
    private final int countryPrefix;
    private final String localPrefix;
    private final int maxLength;
    private final int minLength;
    private final boolean needMno;
    private final String[] prefixes;

    private E164() {
        this(r10, 33, 1, null, null, 11, 11, true);
    }

    private E164(int i) {
        this(r10, r11, i, null, null, 0, 20, false);
    }

    private E164(int i, String str, String[] strArr, int i2, int i3) {
        this(r10, r11, i, str, strArr, i2, i3, false);
    }

    private E164(int i, String str, String[] strArr, int i2, int i3, boolean z) {
        this.countryPrefix = i;
        this.localPrefix = str;
        this.prefixes = strArr;
        this.minLength = i2;
        this.maxLength = i3;
        this.needMno = z;
    }

    public static E164 fromIsoCountryCode(String str) {
        return valueOf(str);
    }

    public static String getCountryPrefix(String str) {
        E164 e164 = getE164(str);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append('+');
        stringBuffer.append(e164.getCountryPrefix());
        return stringBuffer.toString();
    }

    public static E164 getE164(String str) {
        E164 valueOf = valueOf(str);
        if (valueOf != null) {
            return valueOf;
        }
        throw new IllegalArgumentException("country: " + str);
    }

    public static String getLocalPrefix(String str) {
        return getE164(str).getLocalPrefix();
    }

    public final int getCountryPrefix() {
        return this.countryPrefix;
    }

    public final String getLocalPrefix() {
        if (this.localPrefix == null || this.localPrefix.equals("")) {
            return null;
        }
        return this.localPrefix.trim();
    }

    public final int getMaxLength() {
        return this.maxLength;
    }

    public final int getMinLength() {
        return this.minLength;
    }

    public final int getPrefixLength() {
        if (getPrefixes() == null) {
            return 0;
        }
        return getPrefixes()[0].length();
    }

    public final String[] getPrefixes() {
        return this.prefixes;
    }

    public final boolean isNeedMno() {
        return this.needMno;
    }
}
