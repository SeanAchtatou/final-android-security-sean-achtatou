package com.zong.android.engine.process;

import android.os.Handler;
import android.os.Message;
import zongfuscated.C0062b;
import zongfuscated.F;
import zongfuscated.p;

class b implements Handler.Callback {
    private /* synthetic */ ZongActivityProcess a;

    b(ZongActivityProcess zongActivityProcess) {
        this.a = zongActivityProcess;
    }

    public final boolean handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.a.d();
                return false;
            case 2:
                this.a.a((p) message.obj);
                return false;
            case 3:
                this.a.a((C0062b) message.obj);
                return false;
            case 4:
                this.a.a((F) message.obj);
                return false;
            default:
                return false;
        }
    }
}
