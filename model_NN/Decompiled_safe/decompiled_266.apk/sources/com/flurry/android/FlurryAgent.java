package com.flurry.android;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.SystemClock;
import com.openfeint.internal.request.multipart.FilePart;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public class FlurryAgent implements LocationListener {
    private static volatile String a = null;
    /* access modifiers changed from: private */
    public static final FlurryAgent b = new FlurryAgent();
    private static long c = 10000;
    private static long d = 20000;
    private static boolean e = true;
    private static boolean f = false;
    private static boolean g = true;
    private static Criteria h = null;
    private static volatile String kInsecureReportUrl = "http://data.flurry.com/aar.do";
    private static volatile String kSecureReportUrl = "https://data.flurry.com/aar.do";
    private long A;
    private String B;
    private int C;
    private Location D;
    private Map E;
    private List F;
    private boolean G;
    private int H;
    private List I;
    private int J;
    private final Handler i;
    private File j = null;
    private boolean k = false;
    private boolean l = false;
    private long m;
    private Map n = new WeakHashMap();
    private String o;
    private String p;
    private String q;
    private boolean r = true;
    private List s;
    private LocationManager t;
    private String u;
    private boolean v;
    private long w;
    private List x = new ArrayList();
    private long y;
    private long z;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.b(android.content.Context, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.flurry.android.FlurryAgent.b(java.lang.String, java.util.Map):byte[]
      com.flurry.android.FlurryAgent.b(android.content.Context, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(Throwable th) {
        onError("uncaught", th.getMessage(), th.getClass().toString());
        this.n.clear();
        b((Context) null, true);
    }

    private FlurryAgent() {
        HandlerThread handlerThread = new HandlerThread("FlurryAgent");
        handlerThread.start();
        this.i = new Handler(handlerThread.getLooper());
    }

    public static void onStartSession(Context context, String str) {
        if (context == null) {
            throw new NullPointerException("Null context");
        } else if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("Api key not specified");
        } else {
            try {
                b.a(context, str);
            } catch (Throwable th) {
                Flog.b("FlurryAgent", "", th);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.b(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.flurry.android.FlurryAgent.b(java.lang.String, java.util.Map):byte[]
      com.flurry.android.FlurryAgent.b(android.content.Context, boolean):void */
    public static void onEndSession(Context context) {
        if (context == null) {
            throw new NullPointerException("Null context");
        }
        try {
            b.b(context, false);
        } catch (Throwable th) {
            Flog.b("FlurryAgent", "", th);
        }
    }

    public static void onPageView() {
        try {
            b.d();
        } catch (Throwable th) {
            Flog.b("FlurryAgent", "", th);
        }
    }

    public static void onEvent(String str) {
        try {
            b.a(str, (Map) null);
        } catch (Throwable th) {
            Flog.b("FlurryAgent", "", th);
        }
    }

    public static void onEvent(String str, Map map) {
        try {
            b.a(str, map);
        } catch (Throwable th) {
            Flog.b("FlurryAgent", "", th);
        }
    }

    public static void onError(String str, String str2, String str3) {
        try {
            b.a(str, str2, str3);
        } catch (Throwable th) {
            Flog.b("FlurryAgent", "", th);
        }
    }

    public static void setReportUrl(String str) {
        a = str;
    }

    public static void setVersionName(String str) {
        synchronized (b) {
            b.q = str;
        }
    }

    public static void setReportLocation(boolean z2) {
        synchronized (b) {
            b.r = false;
        }
    }

    public static void setLocationCriteria(Criteria criteria) {
        synchronized (b) {
            h = criteria;
        }
    }

    public static boolean getForbidPlaintextFallback() {
        return false;
    }

    public static void setLogEnabled(boolean z2) {
        synchronized (b) {
            if (z2) {
                Flog.enableLog();
            } else {
                Flog.disableLog();
            }
        }
    }

    public static void setContinueSessionMillis(long j2) {
        synchronized (b) {
            c = j2;
        }
    }

    public static void setLocationFixTimeoutMillis(long j2) {
        synchronized (b) {
            d = j2;
        }
    }

    public static void setLogEvents(boolean z2) {
        synchronized (b) {
            e = z2;
        }
    }

    public static void setUserId(String str) {
        synchronized (b) {
            b.B = a(str);
        }
    }

    public static void setCaptureUncaughtExceptions(boolean z2) {
        synchronized (b) {
            if (b.k) {
                Flog.b("FlurryAgent", "Cannot setCaptureUncaughtExceptions after onSessionStart");
            } else {
                g = z2;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x0149 A[Catch:{ Throwable -> 0x01a6 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:70:0x01a2=Splitter:B:70:0x01a2, B:38:0x0132=Splitter:B:38:0x0132, B:44:0x0145=Splitter:B:44:0x0145, B:36:0x012f=Splitter:B:36:0x012f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void a(android.content.Context r10, java.lang.String r11) {
        /*
            r9 = this;
            r8 = 0
            r7 = 1
            monitor-enter(r9)
            java.lang.String r0 = r9.o     // Catch:{ all -> 0x019d }
            if (r0 == 0) goto L_0x0033
            java.lang.String r0 = r9.o     // Catch:{ all -> 0x019d }
            boolean r0 = r0.equals(r11)     // Catch:{ all -> 0x019d }
            if (r0 != 0) goto L_0x0033
            java.lang.String r0 = "FlurryAgent"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x019d }
            r1.<init>()     // Catch:{ all -> 0x019d }
            java.lang.String r2 = "onStartSession called with different api keys: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x019d }
            java.lang.String r2 = r9.o     // Catch:{ all -> 0x019d }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x019d }
            java.lang.String r2 = " and "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x019d }
            java.lang.StringBuilder r1 = r1.append(r11)     // Catch:{ all -> 0x019d }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x019d }
            com.flurry.android.Flog.b(r0, r1)     // Catch:{ all -> 0x019d }
        L_0x0033:
            java.util.Map r0 = r9.n     // Catch:{ all -> 0x019d }
            java.lang.Object r0 = r0.put(r10, r10)     // Catch:{ all -> 0x019d }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ all -> 0x019d }
            if (r0 == 0) goto L_0x0044
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r1 = "onStartSession called with duplicate context, use a specific Activity or Service as context instead of using a global context"
            com.flurry.android.Flog.c(r0, r1)     // Catch:{ all -> 0x019d }
        L_0x0044:
            boolean r0 = r9.k     // Catch:{ all -> 0x019d }
            if (r0 != 0) goto L_0x018e
            r9.o = r11     // Catch:{ all -> 0x019d }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x019d }
            r0.<init>()     // Catch:{ all -> 0x019d }
            java.lang.String r1 = ".flurryagent."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x019d }
            java.lang.String r1 = r9.o     // Catch:{ all -> 0x019d }
            int r1 = r1.hashCode()     // Catch:{ all -> 0x019d }
            r2 = 16
            java.lang.String r1 = java.lang.Integer.toString(r1, r2)     // Catch:{ all -> 0x019d }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x019d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x019d }
            java.io.File r0 = r10.getFileStreamPath(r0)     // Catch:{ all -> 0x019d }
            r9.j = r0     // Catch:{ all -> 0x019d }
            boolean r0 = com.flurry.android.FlurryAgent.g     // Catch:{ all -> 0x019d }
            if (r0 == 0) goto L_0x007b
            com.flurry.android.d r0 = new com.flurry.android.d     // Catch:{ all -> 0x019d }
            r0.<init>()     // Catch:{ all -> 0x019d }
            java.lang.Thread.setDefaultUncaughtExceptionHandler(r0)     // Catch:{ all -> 0x019d }
        L_0x007b:
            android.content.Context r0 = r10.getApplicationContext()     // Catch:{ all -> 0x019d }
            r1 = 1
            r9.k = r1     // Catch:{ all -> 0x019d }
            java.lang.String r1 = r9.q     // Catch:{ all -> 0x019d }
            if (r1 != 0) goto L_0x008c
            java.lang.String r1 = b(r0)     // Catch:{ all -> 0x019d }
            r9.q = r1     // Catch:{ all -> 0x019d }
        L_0x008c:
            java.lang.String r1 = r0.getPackageName()     // Catch:{ all -> 0x019d }
            java.lang.String r2 = r9.p     // Catch:{ all -> 0x019d }
            if (r2 == 0) goto L_0x00c0
            java.lang.String r2 = r9.p     // Catch:{ all -> 0x019d }
            boolean r2 = r2.equals(r1)     // Catch:{ all -> 0x019d }
            if (r2 != 0) goto L_0x00c0
            java.lang.String r2 = "FlurryAgent"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x019d }
            r3.<init>()     // Catch:{ all -> 0x019d }
            java.lang.String r4 = "onStartSession called from different application packages: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x019d }
            java.lang.String r4 = r9.p     // Catch:{ all -> 0x019d }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x019d }
            java.lang.String r4 = " and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x019d }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ all -> 0x019d }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x019d }
            com.flurry.android.Flog.b(r2, r3)     // Catch:{ all -> 0x019d }
        L_0x00c0:
            r9.p = r1     // Catch:{ all -> 0x019d }
            long r1 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x019d }
            long r3 = r9.m     // Catch:{ all -> 0x019d }
            long r3 = r1 - r3
            long r5 = com.flurry.android.FlurryAgent.c     // Catch:{ all -> 0x019d }
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x01cd
            java.lang.String r3 = "FlurryAgent"
            java.lang.String r4 = "Starting new session"
            com.flurry.android.Flog.a(r3, r4)     // Catch:{ all -> 0x019d }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x019d }
            r9.y = r3     // Catch:{ all -> 0x019d }
            r9.z = r1     // Catch:{ all -> 0x019d }
            r1 = -1
            r9.A = r1     // Catch:{ all -> 0x019d }
            java.lang.String r1 = ""
            r9.B = r1     // Catch:{ all -> 0x019d }
            r1 = 0
            r9.C = r1     // Catch:{ all -> 0x019d }
            r1 = 0
            r9.D = r1     // Catch:{ all -> 0x019d }
            java.util.HashMap r1 = new java.util.HashMap     // Catch:{ all -> 0x019d }
            r1.<init>()     // Catch:{ all -> 0x019d }
            r9.E = r1     // Catch:{ all -> 0x019d }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x019d }
            r1.<init>()     // Catch:{ all -> 0x019d }
            r9.F = r1     // Catch:{ all -> 0x019d }
            r1 = 1
            r9.G = r1     // Catch:{ all -> 0x019d }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x019d }
            r1.<init>()     // Catch:{ all -> 0x019d }
            r9.I = r1     // Catch:{ all -> 0x019d }
            r1 = 0
            r9.H = r1     // Catch:{ all -> 0x019d }
            r1 = 0
            r9.J = r1     // Catch:{ all -> 0x019d }
            boolean r1 = r9.l     // Catch:{ all -> 0x019d }
            if (r1 != 0) goto L_0x0182
            java.io.File r1 = r9.j     // Catch:{ all -> 0x019d }
            boolean r1 = r1.exists()     // Catch:{ all -> 0x019d }
            if (r1 == 0) goto L_0x0145
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x0190, all -> 0x01a0 }
            java.io.File r2 = r9.j     // Catch:{ Throwable -> 0x0190, all -> 0x01a0 }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0190, all -> 0x01a0 }
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x0190, all -> 0x01a0 }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x0190, all -> 0x01a0 }
            int r1 = r2.readUnsignedShort()     // Catch:{ Throwable -> 0x01e4 }
            r3 = 46586(0xb5fa, float:6.5281E-41)
            if (r1 != r3) goto L_0x012f
            r9.a(r2)     // Catch:{ Throwable -> 0x01e4 }
        L_0x012f:
            a(r2)     // Catch:{ all -> 0x019d }
        L_0x0132:
            boolean r1 = r9.l     // Catch:{ Throwable -> 0x01a6 }
            if (r1 != 0) goto L_0x0145
            java.io.File r1 = r9.j     // Catch:{ Throwable -> 0x01a6 }
            boolean r1 = r1.delete()     // Catch:{ Throwable -> 0x01a6 }
            if (r1 != 0) goto L_0x0145
            java.lang.String r1 = "FlurryAgent"
            java.lang.String r2 = "Cannot delete persistence file"
            com.flurry.android.Flog.b(r1, r2)     // Catch:{ Throwable -> 0x01a6 }
        L_0x0145:
            boolean r1 = r9.l     // Catch:{ all -> 0x019d }
            if (r1 != 0) goto L_0x0182
            r1 = 0
            r9.v = r1     // Catch:{ all -> 0x019d }
            long r1 = r9.y     // Catch:{ all -> 0x019d }
            r9.w = r1     // Catch:{ all -> 0x019d }
            android.content.ContentResolver r1 = r0.getContentResolver()     // Catch:{ all -> 0x019d }
            java.lang.String r2 = "android_id"
            java.lang.String r1 = android.provider.Settings.System.getString(r1, r2)     // Catch:{ all -> 0x019d }
            if (r1 == 0) goto L_0x01af
            int r2 = r1.length()     // Catch:{ all -> 0x019d }
            if (r2 <= 0) goto L_0x01af
            java.lang.String r2 = "null"
            boolean r2 = r1.equals(r2)     // Catch:{ all -> 0x019d }
            if (r2 != 0) goto L_0x01af
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019d }
            r2.<init>()     // Catch:{ all -> 0x019d }
            java.lang.String r3 = "AND"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019d }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ all -> 0x019d }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x019d }
        L_0x017d:
            r9.u = r1     // Catch:{ all -> 0x019d }
            r1 = 1
            r9.l = r1     // Catch:{ all -> 0x019d }
        L_0x0182:
            boolean r1 = r9.r     // Catch:{ all -> 0x019d }
            android.os.Handler r2 = r9.i     // Catch:{ all -> 0x019d }
            com.flurry.android.c r3 = new com.flurry.android.c     // Catch:{ all -> 0x019d }
            r3.<init>(r9, r0, r1)     // Catch:{ all -> 0x019d }
            r2.post(r3)     // Catch:{ all -> 0x019d }
        L_0x018e:
            monitor-exit(r9)
            return
        L_0x0190:
            r1 = move-exception
            r2 = r8
        L_0x0192:
            java.lang.String r3 = "FlurryAgent"
            java.lang.String r4 = ""
            com.flurry.android.Flog.b(r3, r4, r1)     // Catch:{ all -> 0x01e1 }
            a(r2)     // Catch:{ all -> 0x019d }
            goto L_0x0132
        L_0x019d:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x01a0:
            r0 = move-exception
            r1 = r8
        L_0x01a2:
            a(r1)     // Catch:{ all -> 0x019d }
            throw r0     // Catch:{ all -> 0x019d }
        L_0x01a6:
            r1 = move-exception
            java.lang.String r2 = "FlurryAgent"
            java.lang.String r3 = ""
            com.flurry.android.Flog.b(r2, r3, r1)     // Catch:{ all -> 0x019d }
            goto L_0x0145
        L_0x01af:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x019d }
            r1.<init>()     // Catch:{ all -> 0x019d }
            java.lang.String r2 = "ID"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x019d }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x019d }
            r4 = 16
            java.lang.String r2 = java.lang.Long.toString(r2, r4)     // Catch:{ all -> 0x019d }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x019d }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x019d }
            goto L_0x017d
        L_0x01cd:
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r1 = "Continuing previous session"
            com.flurry.android.Flog.a(r0, r1)     // Catch:{ all -> 0x019d }
            java.util.List r0 = r9.x     // Catch:{ all -> 0x019d }
            java.util.List r1 = r9.x     // Catch:{ all -> 0x019d }
            int r1 = r1.size()     // Catch:{ all -> 0x019d }
            int r1 = r1 - r7
            r0.remove(r1)     // Catch:{ all -> 0x019d }
            goto L_0x018e
        L_0x01e1:
            r0 = move-exception
            r1 = r2
            goto L_0x01a2
        L_0x01e4:
            r1 = move-exception
            goto L_0x0192
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String):void");
    }

    private synchronized void b(Context context, boolean z2) {
        if (context != null) {
            if (((Context) this.n.remove(context)) == null) {
                Flog.c("FlurryAgent", "onEndSession called without context from corresponding onStartSession");
            }
        }
        if (this.k && this.n.isEmpty()) {
            Flog.a("FlurryAgent", "Ending session");
            a();
            Context applicationContext = context == null ? null : context.getApplicationContext();
            if (context != null) {
                String packageName = applicationContext.getPackageName();
                if (!this.p.equals(packageName)) {
                    Flog.b("FlurryAgent", "onEndSession called from different application package, expected: " + this.p + " actual: " + packageName);
                }
            }
            this.k = false;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.m = elapsedRealtime;
            this.A = elapsedRealtime - this.z;
            c();
            g();
            if (!z2) {
                this.i.postDelayed(new a(this, applicationContext), c);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Context context) {
        boolean z2;
        try {
            synchronized (this) {
                z2 = !this.k && SystemClock.elapsedRealtime() - this.m > c && this.x.size() > 0;
            }
            if (z2) {
                f();
            }
        } catch (Throwable th) {
            Flog.b("FlurryAgent", "", th);
        }
    }

    private void c() {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
            dataOutputStream.writeUTF(this.q);
            dataOutputStream.writeLong(this.y);
            dataOutputStream.writeLong(this.A);
            dataOutputStream.writeUTF(this.B);
            if (this.D == null) {
                dataOutputStream.writeBoolean(false);
            } else {
                dataOutputStream.writeBoolean(true);
                dataOutputStream.writeDouble(this.D.getLatitude());
                dataOutputStream.writeDouble(this.D.getLongitude());
                dataOutputStream.writeFloat(this.D.getAccuracy());
            }
            dataOutputStream.writeInt(this.J);
            dataOutputStream.writeByte(-1);
            dataOutputStream.writeShort(this.E.size());
            for (Map.Entry entry : this.E.entrySet()) {
                dataOutputStream.writeUTF((String) entry.getKey());
                dataOutputStream.writeInt(((e) entry.getValue()).a);
            }
            dataOutputStream.writeShort(this.F.size());
            for (byte[] write : this.F) {
                dataOutputStream.write(write);
            }
            dataOutputStream.writeBoolean(this.G);
            dataOutputStream.writeInt(this.C);
            dataOutputStream.writeShort(this.I.size());
            for (f fVar : this.I) {
                dataOutputStream.writeLong(fVar.a);
                dataOutputStream.writeUTF(fVar.b);
                dataOutputStream.writeUTF(fVar.c);
                dataOutputStream.writeUTF(fVar.d);
            }
            dataOutputStream.writeShort(0);
            this.x.add(byteArrayOutputStream.toByteArray());
        } catch (IOException e2) {
            Flog.b("FlurryAgent", "", e2);
        }
    }

    private synchronized void d() {
        this.J++;
    }

    private synchronized void a(String str, Map map) {
        Map map2;
        if (this.F == null) {
            Flog.b("FlurryAgent", "onEvent called before onStartSession.  Event: " + str);
        } else {
            String a2 = a(str);
            if (a2.length() != 0) {
                e eVar = (e) this.E.get(a2);
                if (eVar != null) {
                    eVar.a++;
                } else if (this.E.size() < 100) {
                    e eVar2 = new e();
                    eVar2.a = 1;
                    this.E.put(a2, eVar2);
                } else if (Flog.a("FlurryAgent")) {
                    Flog.a("FlurryAgent", "MaxEventIds exceeded: " + a2);
                }
                if (!e || this.F.size() >= 100 || this.H >= 8000) {
                    this.G = false;
                } else {
                    if (map == null) {
                        map2 = Collections.emptyMap();
                    } else {
                        map2 = map;
                    }
                    if (map2.size() <= 10) {
                        byte[] b2 = b(a2, map2);
                        if (b2.length + this.H <= 8000) {
                            this.F.add(b2);
                            this.H = b2.length + this.H;
                        } else {
                            this.H = 8000;
                            this.G = false;
                        }
                    } else if (Flog.a("FlurryAgent")) {
                        Flog.a("FlurryAgent", "MaxEventParams exceeded: " + map2.size());
                    }
                }
            }
        }
    }

    private static byte[] b(String str, Map map) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
            dataOutputStream.writeUTF(str);
            dataOutputStream.writeShort(map.size());
            for (Map.Entry entry : map.entrySet()) {
                dataOutputStream.writeUTF(a((String) entry.getKey()));
                dataOutputStream.writeUTF(a((String) entry.getValue()));
            }
            dataOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e2) {
            return new byte[0];
        }
    }

    private synchronized void a(String str, String str2, String str3) {
        if (this.I == null) {
            Flog.b("FlurryAgent", "onError called before onStartSession.  Error: " + str);
        } else {
            this.C++;
            if (this.I.size() < 10) {
                f fVar = new f();
                fVar.a = System.currentTimeMillis();
                fVar.b = a(str);
                fVar.c = a(str2);
                fVar.d = a(str3);
                this.I.add(fVar);
            }
        }
    }

    private static String a(String str) {
        if (str == null) {
            return "";
        }
        if (str.length() <= 255) {
            return str;
        }
        return str.substring(0, 255);
    }

    private synchronized byte[] e() {
        byte[] bArr;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
            dataOutputStream.writeShort(8);
            dataOutputStream.writeShort(0);
            dataOutputStream.writeShort(3);
            dataOutputStream.writeShort(30);
            dataOutputStream.writeLong(System.currentTimeMillis());
            dataOutputStream.writeUTF(this.o);
            dataOutputStream.writeUTF(this.q);
            dataOutputStream.writeUTF(this.u);
            dataOutputStream.writeLong(this.w);
            dataOutputStream.writeLong(this.y);
            dataOutputStream.writeShort(4);
            dataOutputStream.writeUTF("device.model");
            dataOutputStream.writeUTF(Build.MODEL);
            dataOutputStream.writeUTF("build.brand");
            dataOutputStream.writeUTF(Build.BRAND);
            dataOutputStream.writeUTF("build.id");
            dataOutputStream.writeUTF(Build.ID);
            dataOutputStream.writeUTF("version.release");
            dataOutputStream.writeUTF(Build.VERSION.RELEASE);
            int size = this.x.size();
            dataOutputStream.writeShort(size);
            for (int i2 = 0; i2 < size; i2++) {
                dataOutputStream.write((byte[]) this.x.get(i2));
            }
            this.s = new ArrayList(this.x);
            dataOutputStream.close();
            bArr = byteArrayOutputStream.toByteArray();
        } catch (IOException e2) {
            Flog.b("FlurryAgent", "", e2);
            bArr = null;
        }
        return bArr;
    }

    private boolean a(byte[] bArr) {
        boolean z2;
        String str = a != null ? a : kInsecureReportUrl;
        if (str == null) {
            return false;
        }
        try {
            z2 = a(bArr, str);
        } catch (Exception e2) {
            z2 = false;
        }
        if (z2 || a != null) {
        }
        return z2;
    }

    private boolean a(byte[] bArr, String str) {
        Flog.a("FlurryAgent", "Sending report to: " + str);
        boolean z2 = false;
        ByteArrayEntity byteArrayEntity = new ByteArrayEntity(bArr);
        byteArrayEntity.setContentType(FilePart.DEFAULT_CONTENT_TYPE);
        HttpPost httpPost = new HttpPost(str);
        httpPost.setEntity(byteArrayEntity);
        int statusCode = new DefaultHttpClient().execute(httpPost).getStatusLine().getStatusCode();
        synchronized (this) {
            if (statusCode == 200) {
                Flog.a("FlurryAgent", "Report successful");
                this.v = true;
                this.x.removeAll(this.s);
                z2 = true;
            } else {
                Flog.a("FlurryAgent", "Report failed");
            }
        }
        this.s = null;
        return z2;
    }

    private void f() {
        try {
            byte[] e2 = e();
            if (e2 != null && a(e2)) {
                g();
            }
        } catch (IOException e3) {
            Flog.a("FlurryAgent", "", e3);
        } catch (Throwable th) {
            Flog.b("FlurryAgent", "", th);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Context context, boolean z2) {
        boolean z3;
        Location location = null;
        if (z2) {
            try {
                location = c(context);
            } catch (Throwable th) {
                Flog.b("FlurryAgent", "", th);
                return;
            }
        }
        synchronized (this) {
            this.D = location;
            z3 = !this.v || this.x.size() > 0;
        }
        if (z3) {
            f();
        }
    }

    private synchronized void a(DataInputStream dataInputStream) {
        int readUnsignedShort = dataInputStream.readUnsignedShort();
        if (readUnsignedShort > 2) {
            throw new IOException("Unknown agent file version: " + readUnsignedShort);
        } else if (readUnsignedShort >= 2) {
            String readUTF = dataInputStream.readUTF();
            if (readUTF.equals(this.o)) {
                this.u = dataInputStream.readUTF();
                this.v = dataInputStream.readBoolean();
                this.w = dataInputStream.readLong();
                while (true) {
                    int readUnsignedShort2 = dataInputStream.readUnsignedShort();
                    if (readUnsignedShort2 == 0) {
                        break;
                    }
                    byte[] bArr = new byte[readUnsignedShort2];
                    dataInputStream.readFully(bArr);
                    this.x.add(0, bArr);
                }
                this.l = true;
            } else {
                Flog.a("FlurryAgent", "Api keys do not match, old: " + readUTF + ", new: " + this.o);
            }
        }
    }

    private synchronized void g() {
        DataOutputStream dataOutputStream;
        try {
            File parentFile = this.j.getParentFile();
            if (parentFile.mkdirs() || parentFile.exists()) {
                dataOutputStream = new DataOutputStream(new FileOutputStream(this.j));
                try {
                    dataOutputStream.writeShort(46586);
                    dataOutputStream.writeShort(2);
                    dataOutputStream.writeUTF(this.o);
                    dataOutputStream.writeUTF(this.u);
                    dataOutputStream.writeBoolean(this.v);
                    dataOutputStream.writeLong(this.w);
                    for (int size = this.x.size() - 1; size >= 0; size--) {
                        byte[] bArr = (byte[]) this.x.get(size);
                        int length = bArr.length;
                        if (length + 2 + dataOutputStream.size() > 50000) {
                            break;
                        }
                        dataOutputStream.writeShort(length);
                        dataOutputStream.write(bArr);
                    }
                    dataOutputStream.writeShort(0);
                    a(dataOutputStream);
                } catch (Throwable th) {
                    th = th;
                    try {
                        Flog.b("FlurryAgent", "", th);
                        a(dataOutputStream);
                    } catch (Throwable th2) {
                        th = th2;
                        a(dataOutputStream);
                        throw th;
                    }
                }
            } else {
                Flog.b("FlurryAgent", "Unable to create persistent dir: " + parentFile);
                a((Closeable) null);
            }
        } catch (Throwable th3) {
            th = th3;
            dataOutputStream = null;
            a(dataOutputStream);
            throw th;
        }
    }

    private static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Throwable th) {
            }
        }
    }

    private static String b(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            if (packageInfo.versionName != null) {
                return packageInfo.versionName;
            }
            if (packageInfo.versionCode != 0) {
                return Integer.toString(packageInfo.versionCode);
            }
            return "Unknown";
        } catch (Throwable th) {
            Flog.b("FlurryAgent", "", th);
        }
    }

    private Location c(Context context) {
        String bestProvider;
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0 || context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            synchronized (this) {
                if (this.t == null) {
                    this.t = locationManager;
                } else {
                    locationManager = this.t;
                }
                Criteria criteria = h;
                if (criteria == null) {
                    criteria = new Criteria();
                }
                bestProvider = locationManager.getBestProvider(criteria, true);
                if (bestProvider != null) {
                    locationManager.requestLocationUpdates(bestProvider, 0, 0.0f, this, Looper.getMainLooper());
                    this.i.postDelayed(new b(this), d);
                }
            }
            if (bestProvider != null) {
                return locationManager.getLastKnownLocation(bestProvider);
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a() {
        if (this.t != null) {
            this.t.removeUpdates(this);
        }
    }

    public synchronized void onLocationChanged(Location location) {
        try {
            this.D = location;
            a();
        } catch (Throwable th) {
            Flog.b("FlurryAgent", "", th);
        }
        return;
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i2, Bundle bundle) {
    }
}
