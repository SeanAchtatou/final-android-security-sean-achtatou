package com.flurry.android;

import android.util.Log;

public class Flog {
    public static final int ASSERT = 7;
    public static final int DEBUG = 3;
    public static final int ERROR = 6;
    public static final int INFO = 4;
    public static final int VERBOSE = 2;
    public static final int WARN = 5;
    private static boolean a = false;

    public static void disableLog() {
        a = true;
    }

    public static void enableLog() {
        a = false;
    }

    static boolean a(String str) {
        return Log.isLoggable(str, 3);
    }

    static int a(String str, String str2, Throwable th) {
        if (a) {
            return 0;
        }
        return Log.d(str, str2, th);
    }

    static int a(String str, String str2) {
        if (a) {
            return 0;
        }
        return Log.d(str, str2);
    }

    static int b(String str, String str2, Throwable th) {
        if (a) {
            return 0;
        }
        return Log.e(str, str2, th);
    }

    static int b(String str, String str2) {
        if (a) {
            return 0;
        }
        return Log.e(str, str2);
    }

    static int c(String str, String str2) {
        if (a) {
            return 0;
        }
        return Log.w(str, str2);
    }
}
