package org.apache.commons.codec.binary;

import com.inmobi.androidsdk.InMobiAdDelegate;
import com.papaya.social.BillingChannel;
import java.math.BigInteger;
import org.apache.commons.codec.BinaryDecoder;
import org.apache.commons.codec.BinaryEncoder;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.EncoderException;

public class Base64 implements BinaryEncoder, BinaryDecoder {
    static final byte[] CHUNK_SEPARATOR = {13, 10};
    static final int CHUNK_SIZE = 76;
    private static final byte[] DECODE_TABLE = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, 62, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, PAD, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51};
    private static final int DEFAULT_BUFFER_RESIZE_FACTOR = 2;
    private static final int DEFAULT_BUFFER_SIZE = 8192;
    private static final int MASK_6BITS = 63;
    private static final int MASK_8BITS = 255;
    private static final byte PAD = 61;
    private static final byte[] STANDARD_ENCODE_TABLE = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] URL_SAFE_ENCODE_TABLE = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private byte[] buffer;
    private int currentLinePos;
    private final int decodeSize;
    private final int encodeSize;
    private final byte[] encodeTable;
    private boolean eof;
    private final int lineLength;
    private final byte[] lineSeparator;
    private int modulus;
    private int pos;
    private int readPos;
    private int x;

    public Base64() {
        this(false);
    }

    public Base64(boolean urlSafe) {
        this(CHUNK_SIZE, CHUNK_SEPARATOR, urlSafe);
    }

    public Base64(int lineLength2) {
        this(lineLength2, CHUNK_SEPARATOR);
    }

    public Base64(int lineLength2, byte[] lineSeparator2) {
        this(lineLength2, lineSeparator2, false);
    }

    public Base64(int lineLength2, byte[] lineSeparator2, boolean urlSafe) {
        int i;
        if (lineSeparator2 == null) {
            lineLength2 = 0;
            lineSeparator2 = CHUNK_SEPARATOR;
        }
        if (lineLength2 > 0) {
            i = (lineLength2 / 4) * 4;
        } else {
            i = 0;
        }
        this.lineLength = i;
        this.lineSeparator = new byte[lineSeparator2.length];
        System.arraycopy(lineSeparator2, 0, this.lineSeparator, 0, lineSeparator2.length);
        if (lineLength2 > 0) {
            this.encodeSize = lineSeparator2.length + 4;
        } else {
            this.encodeSize = 4;
        }
        this.decodeSize = this.encodeSize - 1;
        if (containsBase64Byte(lineSeparator2)) {
            throw new IllegalArgumentException("lineSeperator must not contain base64 characters: [" + StringUtils.newStringUtf8(lineSeparator2) + "]");
        }
        this.encodeTable = urlSafe ? URL_SAFE_ENCODE_TABLE : STANDARD_ENCODE_TABLE;
    }

    public boolean isUrlSafe() {
        return this.encodeTable == URL_SAFE_ENCODE_TABLE;
    }

    /* access modifiers changed from: package-private */
    public boolean hasData() {
        return this.buffer != null;
    }

    /* access modifiers changed from: package-private */
    public int avail() {
        if (this.buffer != null) {
            return this.pos - this.readPos;
        }
        return 0;
    }

    private void resizeBuffer() {
        if (this.buffer == null) {
            this.buffer = new byte[DEFAULT_BUFFER_SIZE];
            this.pos = 0;
            this.readPos = 0;
            return;
        }
        byte[] b = new byte[(this.buffer.length * 2)];
        System.arraycopy(this.buffer, 0, b, 0, this.buffer.length);
        this.buffer = b;
    }

    /* access modifiers changed from: package-private */
    public int readResults(byte[] b, int bPos, int bAvail) {
        if (this.buffer == null) {
            return this.eof ? -1 : 0;
        }
        int len = Math.min(avail(), bAvail);
        if (this.buffer != b) {
            System.arraycopy(this.buffer, this.readPos, b, bPos, len);
            this.readPos += len;
            if (this.readPos >= this.pos) {
                this.buffer = null;
            }
        } else {
            this.buffer = null;
        }
        return len;
    }

    /* access modifiers changed from: package-private */
    public void setInitialBuffer(byte[] out, int outPos, int outAvail) {
        if (out != null && out.length == outAvail) {
            this.buffer = out;
            this.pos = outPos;
            this.readPos = outPos;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: byte} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void encode(byte[] r10, int r11, int r12) {
        /*
            r9 = this;
            r8 = 61
            r7 = 0
            boolean r3 = r9.eof
            if (r3 == 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            if (r12 >= 0) goto L_0x00d1
            r3 = 1
            r9.eof = r3
            byte[] r3 = r9.buffer
            if (r3 == 0) goto L_0x001b
            byte[] r3 = r9.buffer
            int r3 = r3.length
            int r4 = r9.pos
            int r3 = r3 - r4
            int r4 = r9.encodeSize
            if (r3 >= r4) goto L_0x001e
        L_0x001b:
            r9.resizeBuffer()
        L_0x001e:
            int r3 = r9.modulus
            switch(r3) {
                case 1: goto L_0x0040;
                case 2: goto L_0x0083;
                default: goto L_0x0023;
            }
        L_0x0023:
            int r3 = r9.lineLength
            if (r3 <= 0) goto L_0x0007
            int r3 = r9.pos
            if (r3 <= 0) goto L_0x0007
            byte[] r3 = r9.lineSeparator
            byte[] r4 = r9.buffer
            int r5 = r9.pos
            byte[] r6 = r9.lineSeparator
            int r6 = r6.length
            java.lang.System.arraycopy(r3, r7, r4, r5, r6)
            int r3 = r9.pos
            byte[] r4 = r9.lineSeparator
            int r4 = r4.length
            int r3 = r3 + r4
            r9.pos = r3
            goto L_0x0007
        L_0x0040:
            byte[] r3 = r9.buffer
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            byte[] r5 = r9.encodeTable
            int r6 = r9.x
            int r6 = r6 >> 2
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.buffer
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            byte[] r5 = r9.encodeTable
            int r6 = r9.x
            int r6 = r6 << 4
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.encodeTable
            byte[] r4 = org.apache.commons.codec.binary.Base64.STANDARD_ENCODE_TABLE
            if (r3 != r4) goto L_0x0023
            byte[] r3 = r9.buffer
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            r3[r4] = r8
            byte[] r3 = r9.buffer
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            r3[r4] = r8
            goto L_0x0023
        L_0x0083:
            byte[] r3 = r9.buffer
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            byte[] r5 = r9.encodeTable
            int r6 = r9.x
            int r6 = r6 >> 10
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.buffer
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            byte[] r5 = r9.encodeTable
            int r6 = r9.x
            int r6 = r6 >> 4
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.buffer
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            byte[] r5 = r9.encodeTable
            int r6 = r9.x
            int r6 = r6 << 2
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.encodeTable
            byte[] r4 = org.apache.commons.codec.binary.Base64.STANDARD_ENCODE_TABLE
            if (r3 != r4) goto L_0x0023
            byte[] r3 = r9.buffer
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            r3[r4] = r8
            goto L_0x0023
        L_0x00d1:
            r1 = 0
            r2 = r11
        L_0x00d3:
            if (r1 >= r12) goto L_0x017c
            byte[] r3 = r9.buffer
            if (r3 == 0) goto L_0x00e3
            byte[] r3 = r9.buffer
            int r3 = r3.length
            int r4 = r9.pos
            int r3 = r3 - r4
            int r4 = r9.encodeSize
            if (r3 >= r4) goto L_0x00e6
        L_0x00e3:
            r9.resizeBuffer()
        L_0x00e6:
            int r3 = r9.modulus
            int r3 = r3 + 1
            r9.modulus = r3
            int r3 = r3 % 3
            r9.modulus = r3
            int r11 = r2 + 1
            byte r0 = r10[r2]
            if (r0 >= 0) goto L_0x00f8
            int r0 = r0 + 256
        L_0x00f8:
            int r3 = r9.x
            int r3 = r3 << 8
            int r3 = r3 + r0
            r9.x = r3
            int r3 = r9.modulus
            if (r3 != 0) goto L_0x0177
            byte[] r3 = r9.buffer
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            byte[] r5 = r9.encodeTable
            int r6 = r9.x
            int r6 = r6 >> 18
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.buffer
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            byte[] r5 = r9.encodeTable
            int r6 = r9.x
            int r6 = r6 >> 12
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.buffer
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            byte[] r5 = r9.encodeTable
            int r6 = r9.x
            int r6 = r6 >> 6
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.buffer
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            byte[] r5 = r9.encodeTable
            int r6 = r9.x
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            int r3 = r9.currentLinePos
            int r3 = r3 + 4
            r9.currentLinePos = r3
            int r3 = r9.lineLength
            if (r3 <= 0) goto L_0x0177
            int r3 = r9.lineLength
            int r4 = r9.currentLinePos
            if (r3 > r4) goto L_0x0177
            byte[] r3 = r9.lineSeparator
            byte[] r4 = r9.buffer
            int r5 = r9.pos
            byte[] r6 = r9.lineSeparator
            int r6 = r6.length
            java.lang.System.arraycopy(r3, r7, r4, r5, r6)
            int r3 = r9.pos
            byte[] r4 = r9.lineSeparator
            int r4 = r4.length
            int r3 = r3 + r4
            r9.pos = r3
            r9.currentLinePos = r7
        L_0x0177:
            int r1 = r1 + 1
            r2 = r11
            goto L_0x00d3
        L_0x017c:
            r11 = r2
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.codec.binary.Base64.encode(byte[], int, int):void");
    }

    /* access modifiers changed from: package-private */
    public void decode(byte[] in, int inPos, int inAvail) {
        byte b;
        if (!this.eof) {
            if (inAvail < 0) {
                this.eof = true;
            }
            int i = 0;
            int inPos2 = inPos;
            while (true) {
                if (i >= inAvail) {
                    break;
                }
                if (this.buffer == null || this.buffer.length - this.pos < this.decodeSize) {
                    resizeBuffer();
                }
                int inPos3 = inPos2 + 1;
                byte b2 = in[inPos2];
                if (b2 == 61) {
                    this.eof = true;
                    break;
                }
                if (b2 >= 0 && b2 < DECODE_TABLE.length && (b = DECODE_TABLE[b2]) >= 0) {
                    int i2 = this.modulus + 1;
                    this.modulus = i2;
                    this.modulus = i2 % 4;
                    this.x = (this.x << 6) + b;
                    if (this.modulus == 0) {
                        byte[] bArr = this.buffer;
                        int i3 = this.pos;
                        this.pos = i3 + 1;
                        bArr[i3] = (byte) ((this.x >> 16) & MASK_8BITS);
                        byte[] bArr2 = this.buffer;
                        int i4 = this.pos;
                        this.pos = i4 + 1;
                        bArr2[i4] = (byte) ((this.x >> 8) & MASK_8BITS);
                        byte[] bArr3 = this.buffer;
                        int i5 = this.pos;
                        this.pos = i5 + 1;
                        bArr3[i5] = (byte) (this.x & MASK_8BITS);
                    }
                }
                i++;
                inPos2 = inPos3;
            }
            if (this.eof && this.modulus != 0) {
                this.x <<= 6;
                switch (this.modulus) {
                    case 2:
                        this.x <<= 6;
                        byte[] bArr4 = this.buffer;
                        int i6 = this.pos;
                        this.pos = i6 + 1;
                        bArr4[i6] = (byte) ((this.x >> 16) & MASK_8BITS);
                        return;
                    case 3:
                        byte[] bArr5 = this.buffer;
                        int i7 = this.pos;
                        this.pos = i7 + 1;
                        bArr5[i7] = (byte) ((this.x >> 16) & MASK_8BITS);
                        byte[] bArr6 = this.buffer;
                        int i8 = this.pos;
                        this.pos = i8 + 1;
                        bArr6[i8] = (byte) ((this.x >> 8) & MASK_8BITS);
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public static boolean isBase64(byte octet) {
        return octet == 61 || (octet >= 0 && octet < DECODE_TABLE.length && DECODE_TABLE[octet] != -1);
    }

    public static boolean isArrayByteBase64(byte[] arrayOctet) {
        for (int i = 0; i < arrayOctet.length; i++) {
            if (!isBase64(arrayOctet[i]) && !isWhiteSpace(arrayOctet[i])) {
                return false;
            }
        }
        return true;
    }

    private static boolean containsBase64Byte(byte[] arrayOctet) {
        for (byte isBase64 : arrayOctet) {
            if (isBase64(isBase64)) {
                return true;
            }
        }
        return false;
    }

    public static byte[] encodeBase64(byte[] binaryData) {
        return encodeBase64(binaryData, false);
    }

    public static String encodeBase64String(byte[] binaryData) {
        return StringUtils.newStringUtf8(encodeBase64(binaryData, true));
    }

    public static byte[] encodeBase64URLSafe(byte[] binaryData) {
        return encodeBase64(binaryData, false, true);
    }

    public static String encodeBase64URLSafeString(byte[] binaryData) {
        return StringUtils.newStringUtf8(encodeBase64(binaryData, false, true));
    }

    public static byte[] encodeBase64Chunked(byte[] binaryData) {
        return encodeBase64(binaryData, true);
    }

    public Object decode(Object pObject) throws DecoderException {
        if (pObject instanceof byte[]) {
            return decode((byte[]) ((byte[]) pObject));
        }
        if (pObject instanceof String) {
            return decode((String) pObject);
        }
        throw new DecoderException("Parameter supplied to Base64 decode is not a byte[] or a String");
    }

    public byte[] decode(String pArray) {
        return decode(StringUtils.getBytesUtf8(pArray));
    }

    public byte[] decode(byte[] pArray) {
        reset();
        if (pArray == null || pArray.length == 0) {
            return pArray;
        }
        byte[] buf = new byte[((int) ((long) ((pArray.length * 3) / 4)))];
        setInitialBuffer(buf, 0, buf.length);
        decode(pArray, 0, pArray.length);
        decode(pArray, 0, -1);
        byte[] result = new byte[this.pos];
        readResults(result, 0, result.length);
        return result;
    }

    public static byte[] encodeBase64(byte[] binaryData, boolean isChunked) {
        return encodeBase64(binaryData, isChunked, false);
    }

    public static byte[] encodeBase64(byte[] binaryData, boolean isChunked, boolean urlSafe) {
        return encodeBase64(binaryData, isChunked, urlSafe, BillingChannel.ALL);
    }

    public static byte[] encodeBase64(byte[] binaryData, boolean isChunked, boolean urlSafe, int maxResultSize) {
        if (binaryData == null || binaryData.length == 0) {
            return binaryData;
        }
        long len = getEncodeLength(binaryData, CHUNK_SIZE, CHUNK_SEPARATOR);
        if (len > ((long) maxResultSize)) {
            throw new IllegalArgumentException("Input array too big, the output array would be bigger (" + len + ") than the specified maxium size of " + maxResultSize);
        }
        return (isChunked ? new Base64(urlSafe) : new Base64(0, CHUNK_SEPARATOR, urlSafe)).encode(binaryData);
    }

    public static byte[] decodeBase64(String base64String) {
        return new Base64().decode(base64String);
    }

    public static byte[] decodeBase64(byte[] base64Data) {
        return new Base64().decode(base64Data);
    }

    static byte[] discardWhitespace(byte[] data) {
        byte[] groomedData = new byte[data.length];
        int bytesCopied = 0;
        for (int i = 0; i < data.length; i++) {
            switch (data[i]) {
                case 9:
                case 10:
                case InMobiAdDelegate.INMOBI_AD_UNIT_120X600:
                case 32:
                    break;
                default:
                    groomedData[bytesCopied] = data[i];
                    bytesCopied++;
                    break;
            }
        }
        byte[] packedData = new byte[bytesCopied];
        System.arraycopy(groomedData, 0, packedData, 0, bytesCopied);
        return packedData;
    }

    private static boolean isWhiteSpace(byte byteToCheck) {
        switch (byteToCheck) {
            case 9:
            case 10:
            case InMobiAdDelegate.INMOBI_AD_UNIT_120X600:
            case 32:
                return true;
            default:
                return false;
        }
    }

    public Object encode(Object pObject) throws EncoderException {
        if (pObject instanceof byte[]) {
            return encode((byte[]) ((byte[]) pObject));
        }
        throw new EncoderException("Parameter supplied to Base64 encode is not a byte[]");
    }

    public String encodeToString(byte[] pArray) {
        return StringUtils.newStringUtf8(encode(pArray));
    }

    public byte[] encode(byte[] pArray) {
        reset();
        if (pArray == null || pArray.length == 0) {
            return pArray;
        }
        byte[] buf = new byte[((int) getEncodeLength(pArray, this.lineLength, this.lineSeparator))];
        setInitialBuffer(buf, 0, buf.length);
        encode(pArray, 0, pArray.length);
        encode(pArray, 0, -1);
        if (this.buffer != buf) {
            readResults(buf, 0, buf.length);
        }
        if (isUrlSafe() && this.pos < buf.length) {
            byte[] smallerBuf = new byte[this.pos];
            System.arraycopy(buf, 0, smallerBuf, 0, this.pos);
            buf = smallerBuf;
        }
        return buf;
    }

    private static long getEncodeLength(byte[] pArray, int chunkSize, byte[] chunkSeparator) {
        int chunkSize2 = (chunkSize / 4) * 4;
        long len = (long) ((pArray.length * 4) / 3);
        long mod = len % 4;
        if (mod != 0) {
            len += 4 - mod;
        }
        if (chunkSize2 <= 0) {
            return len;
        }
        boolean lenChunksPerfectly = len % ((long) chunkSize2) == 0;
        long len2 = len + ((len / ((long) chunkSize2)) * ((long) chunkSeparator.length));
        if (!lenChunksPerfectly) {
            return len2 + ((long) chunkSeparator.length);
        }
        return len2;
    }

    public static BigInteger decodeInteger(byte[] pArray) {
        return new BigInteger(1, decodeBase64(pArray));
    }

    public static byte[] encodeInteger(BigInteger bigInt) {
        if (bigInt != null) {
            return encodeBase64(toIntegerBytes(bigInt), false);
        }
        throw new NullPointerException("encodeInteger called with null parameter");
    }

    static byte[] toIntegerBytes(BigInteger bigInt) {
        int bitlen = ((bigInt.bitLength() + 7) >> 3) << 3;
        byte[] bigBytes = bigInt.toByteArray();
        if (bigInt.bitLength() % 8 != 0 && (bigInt.bitLength() / 8) + 1 == bitlen / 8) {
            return bigBytes;
        }
        int startSrc = 0;
        int len = bigBytes.length;
        if (bigInt.bitLength() % 8 == 0) {
            startSrc = 1;
            len--;
        }
        byte[] resizedBytes = new byte[(bitlen / 8)];
        System.arraycopy(bigBytes, startSrc, resizedBytes, (bitlen / 8) - len, len);
        return resizedBytes;
    }

    private void reset() {
        this.buffer = null;
        this.pos = 0;
        this.readPos = 0;
        this.currentLinePos = 0;
        this.modulus = 0;
        this.eof = false;
    }
}
