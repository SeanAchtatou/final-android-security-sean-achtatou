package org.codehaus.jackson.impl;

import com.inmobi.androidsdk.InMobiAdDelegate;
import java.io.IOException;
import java.io.InputStream;
import org.codehaus.jackson.Base64Variant;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.ObjectCodec;
import org.codehaus.jackson.io.IOContext;
import org.codehaus.jackson.sym.BytesToNameCanonicalizer;
import org.codehaus.jackson.sym.Name;
import org.codehaus.jackson.util.ByteArrayBuilder;
import org.codehaus.jackson.util.CharTypes;

public final class Utf8StreamParser extends Utf8NumericParser {
    static final byte BYTE_LF = 10;
    protected ObjectCodec _objectCodec;
    protected int[] _quadBuffer = new int[32];
    protected final BytesToNameCanonicalizer _symbols;

    public Utf8StreamParser(IOContext ctxt, int features, InputStream in, ObjectCodec codec, BytesToNameCanonicalizer sym, byte[] inputBuffer, int start, int end, boolean bufferRecyclable) {
        super(ctxt, features, in, inputBuffer, start, end, bufferRecyclable);
        this._objectCodec = codec;
        this._symbols = sym;
        if (!JsonParser.Feature.CANONICALIZE_FIELD_NAMES.enabledIn(features)) {
            _throwInternal();
        }
    }

    public ObjectCodec getCodec() {
        return this._objectCodec;
    }

    public void setCodec(ObjectCodec c) {
        this._objectCodec = c;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public JsonToken nextToken() throws IOException, JsonParseException {
        JsonToken t;
        if (this._currToken == JsonToken.FIELD_NAME) {
            return _nextAfterName();
        }
        if (this._tokenIncomplete) {
            _skipString();
        }
        int i = _skipWSOrEnd();
        if (i < 0) {
            close();
            this._currToken = null;
            return null;
        }
        this._tokenInputTotal = (this._currInputProcessed + ((long) this._inputPtr)) - 1;
        this._tokenInputRow = this._currInputRow;
        this._tokenInputCol = (this._inputPtr - this._currInputRowStart) - 1;
        this._binaryValue = null;
        if (i == 93) {
            if (!this._parsingContext.inArray()) {
                _reportMismatchedEndMarker(i, '}');
            }
            this._parsingContext = this._parsingContext.getParent();
            JsonToken jsonToken = JsonToken.END_ARRAY;
            this._currToken = jsonToken;
            return jsonToken;
        } else if (i == 125) {
            if (!this._parsingContext.inObject()) {
                _reportMismatchedEndMarker(i, ']');
            }
            this._parsingContext = this._parsingContext.getParent();
            JsonToken jsonToken2 = JsonToken.END_OBJECT;
            this._currToken = jsonToken2;
            return jsonToken2;
        } else {
            if (this._parsingContext.expectComma()) {
                if (i != 44) {
                    _reportUnexpectedChar(i, "was expecting comma to separate " + this._parsingContext.getTypeDesc() + " entries");
                }
                i = _skipWS();
            }
            boolean inObject = this._parsingContext.inObject();
            if (inObject) {
                this._parsingContext.setCurrentName(_parseFieldName(i).getName());
                this._currToken = JsonToken.FIELD_NAME;
                int i2 = _skipWS();
                if (i2 != 58) {
                    _reportUnexpectedChar(i2, "was expecting a colon to separate field name and value");
                }
                i = _skipWS();
            }
            switch (i) {
                case 34:
                    this._tokenIncomplete = true;
                    t = JsonToken.VALUE_STRING;
                    break;
                case 45:
                case 48:
                case 49:
                case 50:
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                    t = parseNumberText(i);
                    break;
                case 91:
                    if (!inObject) {
                        this._parsingContext = this._parsingContext.createChildArrayContext(this._tokenInputRow, this._tokenInputCol);
                    }
                    t = JsonToken.START_ARRAY;
                    break;
                case 93:
                case 125:
                    _reportUnexpectedChar(i, "expected a value");
                    _matchToken(JsonToken.VALUE_TRUE);
                    t = JsonToken.VALUE_TRUE;
                    break;
                case 102:
                    _matchToken(JsonToken.VALUE_FALSE);
                    t = JsonToken.VALUE_FALSE;
                    break;
                case 110:
                    _matchToken(JsonToken.VALUE_NULL);
                    t = JsonToken.VALUE_NULL;
                    break;
                case 116:
                    _matchToken(JsonToken.VALUE_TRUE);
                    t = JsonToken.VALUE_TRUE;
                    break;
                case 123:
                    if (!inObject) {
                        this._parsingContext = this._parsingContext.createChildObjectContext(this._tokenInputRow, this._tokenInputCol);
                    }
                    t = JsonToken.START_OBJECT;
                    break;
                default:
                    t = _handleUnexpectedValue(i);
                    break;
            }
            if (inObject) {
                this._nextToken = t;
                return this._currToken;
            }
            this._currToken = t;
            return t;
        }
    }

    private final JsonToken _nextAfterName() {
        this._nameCopied = false;
        JsonToken t = this._nextToken;
        this._nextToken = null;
        if (t == JsonToken.START_ARRAY) {
            this._parsingContext = this._parsingContext.createChildArrayContext(this._tokenInputRow, this._tokenInputCol);
        } else if (t == JsonToken.START_OBJECT) {
            this._parsingContext = this._parsingContext.createChildObjectContext(this._tokenInputRow, this._tokenInputCol);
        }
        this._currToken = t;
        return t;
    }

    public void close() throws IOException {
        super.close();
        this._symbols.release();
    }

    /* access modifiers changed from: protected */
    public final Name _parseFieldName(int i) throws IOException, JsonParseException {
        if (i != 34) {
            return _handleUnusualFieldName(i);
        }
        if (this._inputEnd - this._inputPtr < 9) {
            return slowParseFieldName();
        }
        int[] codes = CharTypes.getInputCodeLatin1();
        byte[] bArr = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        int q = bArr[i2] & 255;
        if (codes[q] == 0) {
            byte[] bArr2 = this._inputBuffer;
            int i3 = this._inputPtr;
            this._inputPtr = i3 + 1;
            int i4 = bArr2[i3] & 255;
            if (codes[i4] == 0) {
                int q2 = (q << 8) | i4;
                byte[] bArr3 = this._inputBuffer;
                int i5 = this._inputPtr;
                this._inputPtr = i5 + 1;
                int i6 = bArr3[i5] & 255;
                if (codes[i6] == 0) {
                    int q3 = (q2 << 8) | i6;
                    byte[] bArr4 = this._inputBuffer;
                    int i7 = this._inputPtr;
                    this._inputPtr = i7 + 1;
                    int i8 = bArr4[i7] & 255;
                    if (codes[i8] == 0) {
                        int q4 = (q3 << 8) | i8;
                        byte[] bArr5 = this._inputBuffer;
                        int i9 = this._inputPtr;
                        this._inputPtr = i9 + 1;
                        int i10 = bArr5[i9] & 255;
                        if (codes[i10] == 0) {
                            return parseMediumFieldName(q4, i10);
                        }
                        if (i10 == 34) {
                            return findName(q4, 4);
                        }
                        return parseFieldName(q4, i10, 4);
                    } else if (i8 == 34) {
                        return findName(q3, 3);
                    } else {
                        return parseFieldName(q3, i8, 3);
                    }
                } else if (i6 == 34) {
                    return findName(q2, 2);
                } else {
                    return parseFieldName(q2, i6, 2);
                }
            } else if (i4 == 34) {
                return findName(q, 1);
            } else {
                return parseFieldName(q, i4, 1);
            }
        } else if (q == 34) {
            return BytesToNameCanonicalizer.getEmptyName();
        } else {
            return parseFieldName(0, q, 0);
        }
    }

    /* access modifiers changed from: protected */
    public Name parseMediumFieldName(int q1, int q2) throws IOException, JsonParseException {
        int[] codes = CharTypes.getInputCodeLatin1();
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        int i2 = bArr[i] & 255;
        if (codes[i2] == 0) {
            int q22 = (q2 << 8) | i2;
            byte[] bArr2 = this._inputBuffer;
            int i3 = this._inputPtr;
            this._inputPtr = i3 + 1;
            int i4 = bArr2[i3] & 255;
            if (codes[i4] == 0) {
                int q23 = (q22 << 8) | i4;
                byte[] bArr3 = this._inputBuffer;
                int i5 = this._inputPtr;
                this._inputPtr = i5 + 1;
                int i6 = bArr3[i5] & 255;
                if (codes[i6] == 0) {
                    int q24 = (q23 << 8) | i6;
                    byte[] bArr4 = this._inputBuffer;
                    int i7 = this._inputPtr;
                    this._inputPtr = i7 + 1;
                    int i8 = bArr4[i7] & 255;
                    if (codes[i8] == 0) {
                        this._quadBuffer[0] = q1;
                        this._quadBuffer[1] = q24;
                        return parseLongFieldName(i8);
                    } else if (i8 == 34) {
                        return findName(q1, q24, 4);
                    } else {
                        return parseFieldName(q1, q24, i8, 4);
                    }
                } else if (i6 == 34) {
                    return findName(q1, q23, 3);
                } else {
                    return parseFieldName(q1, q23, i6, 3);
                }
            } else if (i4 == 34) {
                return findName(q1, q22, 2);
            } else {
                return parseFieldName(q1, q22, i4, 2);
            }
        } else if (i2 == 34) {
            return findName(q1, q2, 1);
        } else {
            return parseFieldName(q1, q2, i2, 1);
        }
    }

    /* access modifiers changed from: protected */
    public Name parseLongFieldName(int q) throws IOException, JsonParseException {
        int[] codes = CharTypes.getInputCodeLatin1();
        int qlen = 2;
        while (this._inputEnd - this._inputPtr >= 4) {
            byte[] bArr = this._inputBuffer;
            int i = this._inputPtr;
            this._inputPtr = i + 1;
            int i2 = bArr[i] & 255;
            if (codes[i2] == 0) {
                int q2 = (q << 8) | i2;
                byte[] bArr2 = this._inputBuffer;
                int i3 = this._inputPtr;
                this._inputPtr = i3 + 1;
                int i4 = bArr2[i3] & 255;
                if (codes[i4] == 0) {
                    int q3 = (q2 << 8) | i4;
                    byte[] bArr3 = this._inputBuffer;
                    int i5 = this._inputPtr;
                    this._inputPtr = i5 + 1;
                    int i6 = bArr3[i5] & 255;
                    if (codes[i6] == 0) {
                        int q4 = (q3 << 8) | i6;
                        byte[] bArr4 = this._inputBuffer;
                        int i7 = this._inputPtr;
                        this._inputPtr = i7 + 1;
                        int i8 = bArr4[i7] & 255;
                        if (codes[i8] == 0) {
                            if (qlen >= this._quadBuffer.length) {
                                this._quadBuffer = growArrayBy(this._quadBuffer, qlen);
                            }
                            this._quadBuffer[qlen] = q4;
                            q = i8;
                            qlen++;
                        } else if (i8 == 34) {
                            return findName(this._quadBuffer, qlen, q4, 4);
                        } else {
                            return parseEscapedFieldName(this._quadBuffer, qlen, q4, i8, 4);
                        }
                    } else if (i6 == 34) {
                        return findName(this._quadBuffer, qlen, q3, 3);
                    } else {
                        return parseEscapedFieldName(this._quadBuffer, qlen, q3, i6, 3);
                    }
                } else if (i4 == 34) {
                    return findName(this._quadBuffer, qlen, q2, 2);
                } else {
                    return parseEscapedFieldName(this._quadBuffer, qlen, q2, i4, 2);
                }
            } else if (i2 == 34) {
                return findName(this._quadBuffer, qlen, q, 1);
            } else {
                return parseEscapedFieldName(this._quadBuffer, qlen, q, i2, 1);
            }
        }
        return parseEscapedFieldName(this._quadBuffer, qlen, 0, q, 0);
    }

    /* access modifiers changed from: protected */
    public Name slowParseFieldName() throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd && !loadMore()) {
            _reportInvalidEOF(": was expecting closing '\"' for name");
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        int i2 = bArr[i] & 255;
        if (i2 == 34) {
            return BytesToNameCanonicalizer.getEmptyName();
        }
        return parseEscapedFieldName(this._quadBuffer, 0, 0, i2, 0);
    }

    private final Name parseFieldName(int q1, int ch, int lastQuadBytes) throws IOException, JsonParseException {
        return parseEscapedFieldName(this._quadBuffer, 0, q1, ch, lastQuadBytes);
    }

    private final Name parseFieldName(int q1, int q2, int ch, int lastQuadBytes) throws IOException, JsonParseException {
        this._quadBuffer[0] = q1;
        return parseEscapedFieldName(this._quadBuffer, 1, q2, ch, lastQuadBytes);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00b3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.codehaus.jackson.sym.Name parseEscapedFieldName(int[] r8, int r9, int r10, int r11, int r12) throws java.io.IOException, org.codehaus.jackson.JsonParseException {
        /*
            r7 = this;
            r6 = 4
            int[] r0 = org.codehaus.jackson.util.CharTypes.getInputCodeLatin1()
        L_0x0005:
            r3 = r0[r11]
            if (r3 == 0) goto L_0x00c8
            r3 = 34
            if (r11 != r3) goto L_0x002b
            if (r12 <= 0) goto L_0x001e
            int r3 = r8.length
            if (r9 < r3) goto L_0x0019
            int r3 = r8.length
            int[] r8 = growArrayBy(r8, r3)
            r7._quadBuffer = r8
        L_0x0019:
            int r2 = r9 + 1
            r8[r9] = r10
            r9 = r2
        L_0x001e:
            org.codehaus.jackson.sym.BytesToNameCanonicalizer r3 = r7._symbols
            org.codehaus.jackson.sym.Name r1 = r3.findName(r8, r9)
            if (r1 != 0) goto L_0x002a
            org.codehaus.jackson.sym.Name r1 = r7.addName(r8, r9, r12)
        L_0x002a:
            return r1
        L_0x002b:
            r3 = 92
            if (r11 == r3) goto L_0x0085
            java.lang.String r3 = "name"
            r7._throwUnquotedSpace(r11, r3)
        L_0x0034:
            r3 = 127(0x7f, float:1.78E-43)
            if (r11 <= r3) goto L_0x00c8
            if (r12 < r6) goto L_0x00c6
            int r3 = r8.length
            if (r9 < r3) goto L_0x0044
            int r3 = r8.length
            int[] r8 = growArrayBy(r8, r3)
            r7._quadBuffer = r8
        L_0x0044:
            int r2 = r9 + 1
            r8[r9] = r10
            r10 = 0
            r12 = 0
        L_0x004a:
            r3 = 2048(0x800, float:2.87E-42)
            if (r11 >= r3) goto L_0x008a
            int r3 = r10 << 8
            int r4 = r11 >> 6
            r4 = r4 | 192(0xc0, float:2.69E-43)
            r10 = r3 | r4
            int r12 = r12 + 1
            r9 = r2
        L_0x0059:
            r3 = r11 & 63
            r11 = r3 | 128(0x80, float:1.794E-43)
            r2 = r9
        L_0x005e:
            if (r12 >= r6) goto L_0x00b3
            int r12 = r12 + 1
            int r3 = r10 << 8
            r10 = r3 | r11
            r9 = r2
        L_0x0067:
            int r3 = r7._inputPtr
            int r4 = r7._inputEnd
            if (r3 < r4) goto L_0x0078
            boolean r3 = r7.loadMore()
            if (r3 != 0) goto L_0x0078
            java.lang.String r3 = " in field name"
            r7._reportInvalidEOF(r3)
        L_0x0078:
            byte[] r3 = r7._inputBuffer
            int r4 = r7._inputPtr
            int r5 = r4 + 1
            r7._inputPtr = r5
            byte r3 = r3[r4]
            r11 = r3 & 255(0xff, float:3.57E-43)
            goto L_0x0005
        L_0x0085:
            char r11 = r7._decodeEscaped()
            goto L_0x0034
        L_0x008a:
            int r3 = r10 << 8
            int r4 = r11 >> 12
            r4 = r4 | 224(0xe0, float:3.14E-43)
            r10 = r3 | r4
            int r12 = r12 + 1
            if (r12 < r6) goto L_0x00c4
            int r3 = r8.length
            if (r2 < r3) goto L_0x00a0
            int r3 = r8.length
            int[] r8 = growArrayBy(r8, r3)
            r7._quadBuffer = r8
        L_0x00a0:
            int r9 = r2 + 1
            r8[r2] = r10
            r10 = 0
            r12 = 0
        L_0x00a6:
            int r3 = r10 << 8
            int r4 = r11 >> 6
            r4 = r4 & 63
            r4 = r4 | 128(0x80, float:1.794E-43)
            r10 = r3 | r4
            int r12 = r12 + 1
            goto L_0x0059
        L_0x00b3:
            int r3 = r8.length
            if (r2 < r3) goto L_0x00bd
            int r3 = r8.length
            int[] r8 = growArrayBy(r8, r3)
            r7._quadBuffer = r8
        L_0x00bd:
            int r9 = r2 + 1
            r8[r2] = r10
            r10 = r11
            r12 = 1
            goto L_0x0067
        L_0x00c4:
            r9 = r2
            goto L_0x00a6
        L_0x00c6:
            r2 = r9
            goto L_0x004a
        L_0x00c8:
            r2 = r9
            goto L_0x005e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.impl.Utf8StreamParser.parseEscapedFieldName(int[], int, int, int, int):org.codehaus.jackson.sym.Name");
    }

    /* access modifiers changed from: protected */
    public final Name _handleUnusualFieldName(int ch) throws IOException, JsonParseException {
        if (ch == 39 && isEnabled(JsonParser.Feature.ALLOW_SINGLE_QUOTES)) {
            return _parseApostropheFieldName();
        }
        if (!isEnabled(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES)) {
            _reportUnexpectedChar(ch, "was expecting double-quote to start field name");
        }
        int[] codes = CharTypes.getInputCodeUtf8JsNames();
        if (codes[ch] != 0) {
            _reportUnexpectedChar(ch, "was expecting either valid name character (for unquoted name) or double-quote (for quoted) to start field name");
        }
        int[] quads = this._quadBuffer;
        int qlen = 0;
        int currQuad = 0;
        int currQuadBytes = 0;
        while (true) {
            int qlen2 = qlen;
            if (currQuadBytes < 4) {
                currQuadBytes++;
                currQuad = (currQuad << 8) | ch;
                qlen = qlen2;
            } else {
                if (qlen2 >= quads.length) {
                    quads = growArrayBy(quads, quads.length);
                    this._quadBuffer = quads;
                }
                qlen = qlen2 + 1;
                quads[qlen2] = currQuad;
                currQuad = ch;
                currQuadBytes = 1;
            }
            if (this._inputPtr >= this._inputEnd && !loadMore()) {
                _reportInvalidEOF(" in field name");
            }
            ch = this._inputBuffer[this._inputPtr] & 255;
            if (codes[ch] != 0) {
                break;
            }
            this._inputPtr++;
        }
        if (currQuadBytes > 0) {
            if (qlen >= quads.length) {
                quads = growArrayBy(quads, quads.length);
                this._quadBuffer = quads;
            }
            quads[qlen] = currQuad;
            qlen++;
        }
        Name name = this._symbols.findName(quads, qlen);
        if (name == null) {
            name = addName(quads, qlen, currQuadBytes);
        }
        return name;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* access modifiers changed from: protected */
    public final Name _parseApostropheFieldName() throws IOException, JsonParseException {
        int currQuadBytes;
        int currQuad;
        int qlen;
        int qlen2;
        int currQuad2;
        int qlen3;
        if (this._inputPtr >= this._inputEnd && !loadMore()) {
            _reportInvalidEOF(": was expecting closing ''' for name");
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        int ch = bArr[i] & 255;
        if (ch == 39) {
            return BytesToNameCanonicalizer.getEmptyName();
        }
        int[] quads = this._quadBuffer;
        int currQuad3 = 0;
        int currQuadBytes2 = 0;
        int[] codes = CharTypes.getInputCodeLatin1();
        int qlen4 = 0;
        while (ch != 39) {
            if (!(ch == 34 || codes[ch] == 0)) {
                if (ch != 92) {
                    _throwUnquotedSpace(ch, "name");
                } else {
                    ch = _decodeEscaped();
                }
                if (ch > 127) {
                    if (currQuadBytes >= 4) {
                        if (qlen4 >= quads.length) {
                            quads = growArrayBy(quads, quads.length);
                            this._quadBuffer = quads;
                        }
                        quads[qlen4] = currQuad;
                        currQuad = 0;
                        currQuadBytes = 0;
                        qlen4++;
                    }
                    if (ch < 2048) {
                        currQuad = (currQuad << 8) | (ch >> 6) | 192;
                        currQuadBytes++;
                        qlen3 = qlen4;
                    } else {
                        int currQuad4 = (currQuad << 8) | (ch >> 12) | 224;
                        int currQuadBytes3 = currQuadBytes + 1;
                        if (currQuadBytes3 >= 4) {
                            if (qlen4 >= quads.length) {
                                quads = growArrayBy(quads, quads.length);
                                this._quadBuffer = quads;
                            }
                            qlen3 = qlen4 + 1;
                            quads[qlen4] = currQuad4;
                            currQuad4 = 0;
                            currQuadBytes3 = 0;
                        } else {
                            qlen3 = qlen4;
                        }
                        currQuad = (currQuad4 << 8) | ((ch >> 6) & 63) | 128;
                        currQuadBytes = currQuadBytes3 + 1;
                    }
                    ch = (ch & 63) | 128;
                    qlen4 = qlen3;
                }
            }
            if (currQuadBytes < 4) {
                currQuadBytes2 = currQuadBytes + 1;
                currQuad2 = (currQuad << 8) | ch;
                qlen2 = qlen4;
            } else {
                if (qlen4 >= quads.length) {
                    quads = growArrayBy(quads, quads.length);
                    this._quadBuffer = quads;
                }
                qlen2 = qlen4 + 1;
                quads[qlen4] = currQuad;
                currQuad2 = ch;
                currQuadBytes2 = 1;
            }
            if (this._inputPtr >= this._inputEnd && !loadMore()) {
                _reportInvalidEOF(" in field name");
            }
            byte[] bArr2 = this._inputBuffer;
            int i2 = this._inputPtr;
            this._inputPtr = i2 + 1;
            ch = bArr2[i2] & 255;
            qlen4 = qlen2;
            currQuad3 = currQuad2;
        }
        if (currQuadBytes > 0) {
            if (qlen4 >= quads.length) {
                quads = growArrayBy(quads, quads.length);
                this._quadBuffer = quads;
            }
            qlen = qlen4 + 1;
            quads[qlen4] = currQuad;
        } else {
            qlen = qlen4;
        }
        Name name = this._symbols.findName(quads, qlen);
        if (name == null) {
            name = addName(quads, qlen, currQuadBytes);
        }
        return name;
    }

    private final Name findName(int q1, int lastQuadBytes) throws JsonParseException {
        Name name = this._symbols.findName(q1);
        if (name != null) {
            return name;
        }
        this._quadBuffer[0] = q1;
        return addName(this._quadBuffer, 1, lastQuadBytes);
    }

    private final Name findName(int q1, int q2, int lastQuadBytes) throws JsonParseException {
        Name name = this._symbols.findName(q1, q2);
        if (name != null) {
            return name;
        }
        this._quadBuffer[0] = q1;
        this._quadBuffer[1] = q2;
        return addName(this._quadBuffer, 2, lastQuadBytes);
    }

    private final Name findName(int[] quads, int qlen, int lastQuad, int lastQuadBytes) throws JsonParseException {
        if (qlen >= quads.length) {
            quads = growArrayBy(quads, quads.length);
            this._quadBuffer = quads;
        }
        int qlen2 = qlen + 1;
        quads[qlen] = lastQuad;
        Name name = this._symbols.findName(quads, qlen2);
        if (name == null) {
            return addName(quads, qlen2, lastQuadBytes);
        }
        return name;
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00f3  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00fc A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final org.codehaus.jackson.sym.Name addName(int[] r18, int r19, int r20) throws org.codehaus.jackson.JsonParseException {
        /*
            r17 = this;
            int r15 = r19 << 2
            r16 = 4
            int r15 = r15 - r16
            int r6 = r15 + r20
            r15 = 4
            r0 = r20
            r1 = r15
            if (r0 >= r1) goto L_0x0103
            r15 = 1
            int r15 = r19 - r15
            r13 = r18[r15]
            r15 = 1
            int r15 = r19 - r15
            r16 = 4
            int r16 = r16 - r20
            int r16 = r16 << 3
            int r16 = r13 << r16
            r18[r15] = r16
        L_0x0020:
            r0 = r17
            org.codehaus.jackson.util.TextBuffer r0 = r0._textBuffer
            r15 = r0
            char[] r7 = r15.emptyAndGetCurrentSegment()
            r10 = 0
            r12 = 0
            r11 = r10
        L_0x002c:
            if (r12 >= r6) goto L_0x012c
            int r15 = r12 >> 2
            r8 = r18[r15]
            r5 = r12 & 3
            r15 = 3
            int r15 = r15 - r5
            int r15 = r15 << 3
            int r15 = r8 >> r15
            r8 = r15 & 255(0xff, float:3.57E-43)
            int r12 = r12 + 1
            r15 = 127(0x7f, float:1.78E-43)
            if (r8 <= r15) goto L_0x014d
            r15 = r8 & 224(0xe0, float:3.14E-43)
            r16 = 192(0xc0, float:2.69E-43)
            r0 = r15
            r1 = r16
            if (r0 != r1) goto L_0x0106
            r8 = r8 & 31
            r14 = 1
        L_0x004e:
            int r15 = r12 + r14
            if (r15 <= r6) goto L_0x005a
            java.lang.String r15 = " in field name"
            r0 = r17
            r1 = r15
            r0._reportInvalidEOF(r1)
        L_0x005a:
            int r15 = r12 >> 2
            r9 = r18[r15]
            r5 = r12 & 3
            r15 = 3
            int r15 = r15 - r5
            int r15 = r15 << 3
            int r9 = r9 >> r15
            int r12 = r12 + 1
            r15 = r9 & 192(0xc0, float:2.69E-43)
            r16 = 128(0x80, float:1.794E-43)
            r0 = r15
            r1 = r16
            if (r0 == r1) goto L_0x0076
            r0 = r17
            r1 = r9
            r0._reportInvalidOther(r1)
        L_0x0076:
            int r15 = r8 << 6
            r16 = r9 & 63
            r8 = r15 | r16
            r15 = 1
            if (r14 <= r15) goto L_0x00c8
            int r15 = r12 >> 2
            r9 = r18[r15]
            r5 = r12 & 3
            r15 = 3
            int r15 = r15 - r5
            int r15 = r15 << 3
            int r9 = r9 >> r15
            int r12 = r12 + 1
            r15 = r9 & 192(0xc0, float:2.69E-43)
            r16 = 128(0x80, float:1.794E-43)
            r0 = r15
            r1 = r16
            if (r0 == r1) goto L_0x009b
            r0 = r17
            r1 = r9
            r0._reportInvalidOther(r1)
        L_0x009b:
            int r15 = r8 << 6
            r16 = r9 & 63
            r8 = r15 | r16
            r15 = 2
            if (r14 <= r15) goto L_0x00c8
            int r15 = r12 >> 2
            r9 = r18[r15]
            r5 = r12 & 3
            r15 = 3
            int r15 = r15 - r5
            int r15 = r15 << 3
            int r9 = r9 >> r15
            int r12 = r12 + 1
            r15 = r9 & 192(0xc0, float:2.69E-43)
            r16 = 128(0x80, float:1.794E-43)
            r0 = r15
            r1 = r16
            if (r0 == r1) goto L_0x00c2
            r15 = r9 & 255(0xff, float:3.57E-43)
            r0 = r17
            r1 = r15
            r0._reportInvalidOther(r1)
        L_0x00c2:
            int r15 = r8 << 6
            r16 = r9 & 63
            r8 = r15 | r16
        L_0x00c8:
            r15 = 2
            if (r14 <= r15) goto L_0x014d
            r15 = 65536(0x10000, float:9.18355E-41)
            int r8 = r8 - r15
            int r15 = r7.length
            if (r11 < r15) goto L_0x00da
            r0 = r17
            org.codehaus.jackson.util.TextBuffer r0 = r0._textBuffer
            r15 = r0
            char[] r7 = r15.expandCurrentSegment()
        L_0x00da:
            int r10 = r11 + 1
            r15 = 55296(0xd800, float:7.7486E-41)
            int r16 = r8 >> 10
            int r15 = r15 + r16
            char r15 = (char) r15
            r7[r11] = r15
            r15 = 56320(0xdc00, float:7.8921E-41)
            r0 = r8
            r0 = r0 & 1023(0x3ff, float:1.434E-42)
            r16 = r0
            r8 = r15 | r16
        L_0x00f0:
            int r15 = r7.length
            if (r10 < r15) goto L_0x00fc
            r0 = r17
            org.codehaus.jackson.util.TextBuffer r0 = r0._textBuffer
            r15 = r0
            char[] r7 = r15.expandCurrentSegment()
        L_0x00fc:
            int r11 = r10 + 1
            char r15 = (char) r8
            r7[r10] = r15
            goto L_0x002c
        L_0x0103:
            r13 = 0
            goto L_0x0020
        L_0x0106:
            r15 = r8 & 240(0xf0, float:3.36E-43)
            r16 = 224(0xe0, float:3.14E-43)
            r0 = r15
            r1 = r16
            if (r0 != r1) goto L_0x0114
            r8 = r8 & 15
            r14 = 2
            goto L_0x004e
        L_0x0114:
            r15 = r8 & 248(0xf8, float:3.48E-43)
            r16 = 240(0xf0, float:3.36E-43)
            r0 = r15
            r1 = r16
            if (r0 != r1) goto L_0x0122
            r8 = r8 & 7
            r14 = 3
            goto L_0x004e
        L_0x0122:
            r0 = r17
            r1 = r8
            r0._reportInvalidInitial(r1)
            r8 = 1
            r14 = r8
            goto L_0x004e
        L_0x012c:
            java.lang.String r4 = new java.lang.String
            r15 = 0
            r4.<init>(r7, r15, r11)
            r15 = 4
            r0 = r20
            r1 = r15
            if (r0 >= r1) goto L_0x013d
            r15 = 1
            int r15 = r19 - r15
            r18[r15] = r13
        L_0x013d:
            r0 = r17
            org.codehaus.jackson.sym.BytesToNameCanonicalizer r0 = r0._symbols
            r15 = r0
            r0 = r15
            r1 = r4
            r2 = r18
            r3 = r19
            org.codehaus.jackson.sym.Name r15 = r0.addName(r1, r2, r3)
            return r15
        L_0x014d:
            r10 = r11
            goto L_0x00f0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.impl.Utf8StreamParser.addName(int[], int, int):org.codehaus.jackson.sym.Name");
    }

    /* access modifiers changed from: protected */
    public void _finishString() throws IOException, JsonParseException {
        int ptr;
        int outPtr;
        int outPtr2;
        int outPtr3 = 0;
        char[] outBuf = this._textBuffer.emptyAndGetCurrentSegment();
        int[] codes = CharTypes.getInputCodeUtf8();
        byte[] inputBuffer = this._inputBuffer;
        while (true) {
            int ptr2 = this._inputPtr;
            if (ptr2 >= this._inputEnd) {
                loadMoreGuaranteed();
                ptr2 = this._inputPtr;
            }
            if (outPtr3 >= outBuf.length) {
                outBuf = this._textBuffer.finishCurrentSegment();
                outPtr3 = 0;
            }
            int max = this._inputEnd;
            int max2 = ptr2 + (outBuf.length - outPtr3);
            if (max2 < max) {
                max = max2;
                ptr = ptr2;
                outPtr = outPtr3;
            } else {
                ptr = ptr2;
                outPtr = outPtr3;
            }
            while (true) {
                if (ptr < max) {
                    int ptr3 = ptr + 1;
                    int c = inputBuffer[ptr] & 255;
                    if (codes[c] != 0) {
                        this._inputPtr = ptr3;
                        if (c == 34) {
                            this._textBuffer.setCurrentLength(outPtr);
                            return;
                        }
                        switch (codes[c]) {
                            case 1:
                                c = _decodeEscaped();
                                outPtr2 = outPtr;
                                break;
                            case 2:
                                c = _decodeUtf8_2(c);
                                outPtr2 = outPtr;
                                break;
                            case 3:
                                if (this._inputEnd - this._inputPtr < 2) {
                                    c = _decodeUtf8_3(c);
                                    outPtr2 = outPtr;
                                    break;
                                } else {
                                    c = _decodeUtf8_3fast(c);
                                    outPtr2 = outPtr;
                                    break;
                                }
                            case 4:
                                int c2 = _decodeUtf8_4(c);
                                outPtr2 = outPtr + 1;
                                outBuf[outPtr] = (char) (55296 | (c2 >> 10));
                                if (outPtr2 >= outBuf.length) {
                                    outBuf = this._textBuffer.finishCurrentSegment();
                                    outPtr2 = 0;
                                }
                                c = 56320 | (c2 & 1023);
                                break;
                            default:
                                if (c >= 32) {
                                    _reportInvalidChar(c);
                                    outPtr2 = outPtr;
                                    break;
                                } else {
                                    _throwUnquotedSpace(c, "string value");
                                    outPtr2 = outPtr;
                                    break;
                                }
                        }
                        if (outPtr2 >= outBuf.length) {
                            outBuf = this._textBuffer.finishCurrentSegment();
                            outPtr2 = 0;
                        }
                        outBuf[outPtr2] = (char) c;
                        outPtr3 = outPtr2 + 1;
                    } else {
                        outBuf[outPtr] = (char) c;
                        ptr = ptr3;
                        outPtr++;
                    }
                } else {
                    this._inputPtr = ptr;
                    outPtr3 = outPtr;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x0019  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void _skipString() throws java.io.IOException, org.codehaus.jackson.JsonParseException {
        /*
            r7 = this;
            r6 = 0
            r7._tokenIncomplete = r6
            int[] r1 = org.codehaus.jackson.util.CharTypes.getInputCodeUtf8()
            byte[] r2 = r7._inputBuffer
        L_0x0009:
            int r4 = r7._inputPtr
            int r3 = r7._inputEnd
            if (r4 < r3) goto L_0x0050
            r7.loadMoreGuaranteed()
            int r4 = r7._inputPtr
            int r3 = r7._inputEnd
            r5 = r4
        L_0x0017:
            if (r5 >= r3) goto L_0x002a
            int r4 = r5 + 1
            byte r6 = r2[r5]
            r0 = r6 & 255(0xff, float:3.57E-43)
            r6 = r1[r0]
            if (r6 == 0) goto L_0x0050
            r7._inputPtr = r4
            r6 = 34
            if (r0 != r6) goto L_0x002d
            return
        L_0x002a:
            r7._inputPtr = r5
            goto L_0x0009
        L_0x002d:
            r6 = r1[r0]
            switch(r6) {
                case 1: goto L_0x003c;
                case 2: goto L_0x0040;
                case 3: goto L_0x0044;
                case 4: goto L_0x0048;
                default: goto L_0x0032;
            }
        L_0x0032:
            r6 = 32
            if (r0 >= r6) goto L_0x004c
            java.lang.String r6 = "string value"
            r7._throwUnquotedSpace(r0, r6)
            goto L_0x0009
        L_0x003c:
            r7._decodeEscaped()
            goto L_0x0009
        L_0x0040:
            r7._skipUtf8_2(r0)
            goto L_0x0009
        L_0x0044:
            r7._skipUtf8_3(r0)
            goto L_0x0009
        L_0x0048:
            r7._skipUtf8_4(r0)
            goto L_0x0009
        L_0x004c:
            r7._reportInvalidChar(r0)
            goto L_0x0009
        L_0x0050:
            r5 = r4
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.impl.Utf8StreamParser._skipString():void");
    }

    /* access modifiers changed from: protected */
    public final JsonToken _handleUnexpectedValue(int c) throws IOException, JsonParseException {
        if (c != 39 || !isEnabled(JsonParser.Feature.ALLOW_SINGLE_QUOTES)) {
            _reportUnexpectedChar(c, "expected a valid value (number, String, array, object, 'true', 'false' or 'null')");
        }
        int outPtr = 0;
        char[] outBuf = this._textBuffer.emptyAndGetCurrentSegment();
        int[] codes = CharTypes.getInputCodeUtf8();
        byte[] inputBuffer = this._inputBuffer;
        while (true) {
            if (this._inputPtr >= this._inputEnd) {
                loadMoreGuaranteed();
            }
            if (outPtr >= outBuf.length) {
                outBuf = this._textBuffer.finishCurrentSegment();
                outPtr = 0;
            }
            int max = this._inputEnd;
            int max2 = this._inputPtr + (outBuf.length - outPtr);
            if (max2 < max) {
                max = max2;
            }
            while (true) {
                if (this._inputPtr < max) {
                    int i = this._inputPtr;
                    this._inputPtr = i + 1;
                    int c2 = inputBuffer[i] & 255;
                    if (c2 != 39 && codes[c2] == 0) {
                        outBuf[outPtr] = (char) c2;
                        outPtr++;
                    } else if (c2 == 39) {
                        this._textBuffer.setCurrentLength(outPtr);
                        return JsonToken.VALUE_STRING;
                    } else {
                        switch (codes[c2]) {
                            case 1:
                                if (c2 != 34) {
                                    c2 = _decodeEscaped();
                                    break;
                                }
                                break;
                            case 2:
                                c2 = _decodeUtf8_2(c2);
                                break;
                            case 3:
                                if (this._inputEnd - this._inputPtr < 2) {
                                    c2 = _decodeUtf8_3(c2);
                                    break;
                                } else {
                                    c2 = _decodeUtf8_3fast(c2);
                                    break;
                                }
                            case 4:
                                int c3 = _decodeUtf8_4(c2);
                                int outPtr2 = outPtr + 1;
                                outBuf[outPtr] = (char) (55296 | (c3 >> 10));
                                if (outPtr2 >= outBuf.length) {
                                    outBuf = this._textBuffer.finishCurrentSegment();
                                    outPtr = 0;
                                } else {
                                    outPtr = outPtr2;
                                }
                                c2 = 56320 | (c3 & 1023);
                                break;
                            default:
                                if (c2 < 32) {
                                    _throwUnquotedSpace(c2, "string value");
                                }
                                _reportInvalidChar(c2);
                                break;
                        }
                        if (outPtr >= outBuf.length) {
                            outBuf = this._textBuffer.finishCurrentSegment();
                            outPtr = 0;
                        }
                        outBuf[outPtr] = (char) c2;
                        outPtr++;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void _matchToken(JsonToken token) throws IOException, JsonParseException {
        byte[] matchBytes = token.asByteArray();
        int len = matchBytes.length;
        for (int i = 1; i < len; i++) {
            if (this._inputPtr >= this._inputEnd) {
                loadMoreGuaranteed();
            }
            if (matchBytes[i] != this._inputBuffer[this._inputPtr]) {
                _reportInvalidToken(token.asString().substring(0, i));
            }
            this._inputPtr++;
        }
    }

    private void _reportInvalidToken(String matchedPart) throws IOException, JsonParseException {
        StringBuilder sb = new StringBuilder(matchedPart);
        while (true) {
            if (this._inputPtr >= this._inputEnd && !loadMore()) {
                break;
            }
            byte[] bArr = this._inputBuffer;
            int i = this._inputPtr;
            this._inputPtr = i + 1;
            char c = (char) _decodeCharForError(bArr[i]);
            if (!Character.isJavaIdentifierPart(c)) {
                break;
            }
            this._inputPtr++;
            sb.append(c);
        }
        _reportError("Unrecognized token '" + sb.toString() + "': was expecting 'null', 'true' or 'false'");
    }

    private final int _skipWS() throws IOException, JsonParseException {
        while (true) {
            if (this._inputPtr < this._inputEnd || loadMore()) {
                byte[] bArr = this._inputBuffer;
                int i = this._inputPtr;
                this._inputPtr = i + 1;
                int i2 = bArr[i] & 255;
                if (i2 > 32) {
                    if (i2 != 47) {
                        return i2;
                    }
                    _skipComment();
                } else if (i2 != 32) {
                    if (i2 == 10) {
                        _skipLF();
                    } else if (i2 == 13) {
                        _skipCR();
                    } else if (i2 != 9) {
                        _throwInvalidSpace(i2);
                    }
                }
            } else {
                throw _constructError("Unexpected end-of-input within/between " + this._parsingContext.getTypeDesc() + " entries");
            }
        }
    }

    private final int _skipWSOrEnd() throws IOException, JsonParseException {
        while (true) {
            if (this._inputPtr < this._inputEnd || loadMore()) {
                byte[] bArr = this._inputBuffer;
                int i = this._inputPtr;
                this._inputPtr = i + 1;
                int i2 = bArr[i] & 255;
                if (i2 > 32) {
                    if (i2 != 47) {
                        return i2;
                    }
                    _skipComment();
                } else if (i2 != 32) {
                    if (i2 == 10) {
                        _skipLF();
                    } else if (i2 == 13) {
                        _skipCR();
                    } else if (i2 != 9) {
                        _throwInvalidSpace(i2);
                    }
                }
            } else {
                _handleEOF();
                return -1;
            }
        }
    }

    private final void _skipComment() throws IOException, JsonParseException {
        if (!isEnabled(JsonParser.Feature.ALLOW_COMMENTS)) {
            _reportUnexpectedChar(47, "maybe a (non-standard) comment? (not recognized as one since Feature 'ALLOW_COMMENTS' not enabled for parser)");
        }
        if (this._inputPtr >= this._inputEnd && !loadMore()) {
            _reportInvalidEOF(" in a comment");
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        int c = bArr[i] & 255;
        if (c == 47) {
            _skipCppComment();
        } else if (c == 42) {
            _skipCComment();
        } else {
            _reportUnexpectedChar(c, "was expecting either '*' or '/' for a comment");
        }
    }

    private final void _skipCComment() throws IOException, JsonParseException {
        int[] codes = CharTypes.getInputCodeComment();
        while (true) {
            if (this._inputPtr < this._inputEnd || loadMore()) {
                byte[] bArr = this._inputBuffer;
                int i = this._inputPtr;
                this._inputPtr = i + 1;
                int i2 = bArr[i] & 255;
                int code = codes[i2];
                if (code != 0) {
                    switch (code) {
                        case 10:
                            _skipLF();
                            continue;
                        case InMobiAdDelegate.INMOBI_AD_UNIT_120X600:
                            _skipCR();
                            continue;
                        case 42:
                            if (this._inputBuffer[this._inputPtr] == 47) {
                                this._inputPtr++;
                                return;
                            }
                            continue;
                        default:
                            _reportInvalidChar(i2);
                            continue;
                    }
                }
            } else {
                _reportInvalidEOF(" in a comment");
                return;
            }
        }
    }

    private final void _skipCppComment() throws IOException, JsonParseException {
        int[] codes = CharTypes.getInputCodeComment();
        while (true) {
            if (this._inputPtr < this._inputEnd || loadMore()) {
                byte[] bArr = this._inputBuffer;
                int i = this._inputPtr;
                this._inputPtr = i + 1;
                int i2 = bArr[i] & 255;
                int code = codes[i2];
                if (code != 0) {
                    switch (code) {
                        case 10:
                            _skipLF();
                            return;
                        case InMobiAdDelegate.INMOBI_AD_UNIT_120X600:
                            _skipCR();
                            return;
                        case 42:
                            break;
                        default:
                            _reportInvalidChar(i2);
                            continue;
                    }
                }
            } else {
                return;
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final char _decodeEscaped() throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd && !loadMore()) {
            _reportInvalidEOF(" in character escape sequence");
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i];
        switch (b) {
            case 34:
            case 47:
            case 92:
                return (char) b;
            case 98:
                return 8;
            case 102:
                return 12;
            case 110:
                return 10;
            case 114:
                return 13;
            case 116:
                return 9;
            case 117:
                break;
            default:
                _reportError("Unrecognized character escape (\\ followed by " + _getCharDesc(_decodeCharForError(b)) + ")");
                break;
        }
        int value = 0;
        for (int i2 = 0; i2 < 4; i2++) {
            if (this._inputPtr >= this._inputEnd && !loadMore()) {
                _reportInvalidEOF(" in character escape sequence");
            }
            byte[] bArr2 = this._inputBuffer;
            int i3 = this._inputPtr;
            this._inputPtr = i3 + 1;
            byte b2 = bArr2[i3];
            int digit = CharTypes.charToHex(b2);
            if (digit < 0) {
                _reportUnexpectedChar(b2, "expected a hex-digit for character escape sequence");
            }
            value = (value << 4) | digit;
        }
        return (char) value;
    }

    /* access modifiers changed from: protected */
    public int _decodeCharForError(int firstByte) throws IOException, JsonParseException {
        int needed;
        int c = firstByte;
        if (c >= 0) {
            return c;
        }
        if ((c & 224) == 192) {
            c &= 31;
            needed = 1;
        } else if ((c & 240) == 224) {
            c &= 15;
            needed = 2;
        } else if ((c & 248) == 240) {
            c &= 7;
            needed = 3;
        } else {
            _reportInvalidInitial(c & 255);
            needed = 1;
        }
        int d = nextByte();
        if ((d & 192) != 128) {
            _reportInvalidOther(d & 255);
        }
        int c2 = (c << 6) | (d & 63);
        if (needed <= 1) {
            return c2;
        }
        int d2 = nextByte();
        if ((d2 & 192) != 128) {
            _reportInvalidOther(d2 & 255);
        }
        int c3 = (c2 << 6) | (d2 & 63);
        if (needed <= 2) {
            return c3;
        }
        int d3 = nextByte();
        if ((d3 & 192) != 128) {
            _reportInvalidOther(d3 & 255);
        }
        return (c3 << 6) | (d3 & 63);
    }

    private final int _decodeUtf8_2(int c) throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & 255, this._inputPtr);
        }
        return ((c & 31) << 6) | (b & 63);
    }

    private final int _decodeUtf8_3(int c1) throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        int c12 = c1 & 15;
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & 255, this._inputPtr);
        }
        int c = (c12 << 6) | (b & 63);
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr2 = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b2 = bArr2[i2];
        if ((b2 & 192) != 128) {
            _reportInvalidOther(b2 & 255, this._inputPtr);
        }
        return (c << 6) | (b2 & 63);
    }

    private final int _decodeUtf8_3fast(int c1) throws IOException, JsonParseException {
        int c12 = c1 & 15;
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & 255, this._inputPtr);
        }
        int c = (c12 << 6) | (b & 63);
        byte[] bArr2 = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b2 = bArr2[i2];
        if ((b2 & 192) != 128) {
            _reportInvalidOther(b2 & 255, this._inputPtr);
        }
        return (c << 6) | (b2 & 63);
    }

    private final int _decodeUtf8_4(int c) throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & 255, this._inputPtr);
        }
        int c2 = ((c & 7) << 6) | (b & 63);
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr2 = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b2 = bArr2[i2];
        if ((b2 & 192) != 128) {
            _reportInvalidOther(b2 & 255, this._inputPtr);
        }
        int c3 = (c2 << 6) | (b2 & 63);
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr3 = this._inputBuffer;
        int i3 = this._inputPtr;
        this._inputPtr = i3 + 1;
        byte b3 = bArr3[i3];
        if ((b3 & 192) != 128) {
            _reportInvalidOther(b3 & 255, this._inputPtr);
        }
        return ((c3 << 6) | (b3 & 63)) - 65536;
    }

    private final void _skipUtf8_2(int c) throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & 255, this._inputPtr);
        }
    }

    private final void _skipUtf8_3(int c) throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & 255, this._inputPtr);
        }
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr2 = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b2 = bArr2[i2];
        if ((b2 & 192) != 128) {
            _reportInvalidOther(b2 & 255, this._inputPtr);
        }
    }

    private final void _skipUtf8_4(int c) throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & 255, this._inputPtr);
        }
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        if ((b & 192) != 128) {
            _reportInvalidOther(b & 255, this._inputPtr);
        }
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr2 = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b2 = bArr2[i2];
        if ((b2 & 192) != 128) {
            _reportInvalidOther(b2 & 255, this._inputPtr);
        }
    }

    /* access modifiers changed from: protected */
    public final void _skipCR() throws IOException {
        if ((this._inputPtr < this._inputEnd || loadMore()) && this._inputBuffer[this._inputPtr] == 10) {
            this._inputPtr++;
        }
        this._currInputRow++;
        this._currInputRowStart = this._inputPtr;
    }

    /* access modifiers changed from: protected */
    public final void _skipLF() throws IOException {
        this._currInputRow++;
        this._currInputRowStart = this._inputPtr;
    }

    private int nextByte() throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        return bArr[i] & 255;
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidChar(int c) throws JsonParseException {
        if (c < 32) {
            _throwInvalidSpace(c);
        }
        _reportInvalidInitial(c);
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidInitial(int mask) throws JsonParseException {
        _reportError("Invalid UTF-8 start byte 0x" + Integer.toHexString(mask));
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidOther(int mask) throws JsonParseException {
        _reportError("Invalid UTF-8 middle byte 0x" + Integer.toHexString(mask));
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidOther(int mask, int ptr) throws JsonParseException {
        this._inputPtr = ptr;
        _reportInvalidOther(mask);
    }

    public static int[] growArrayBy(int[] arr, int more) {
        if (arr == null) {
            return new int[more];
        }
        int[] old = arr;
        int len = arr.length;
        int[] arr2 = new int[(len + more)];
        System.arraycopy(old, 0, arr2, 0, len);
        return arr2;
    }

    /* access modifiers changed from: protected */
    public byte[] _decodeBase64(Base64Variant b64variant) throws IOException, JsonParseException {
        ByteArrayBuilder builder = _getByteArrayBuilder();
        while (true) {
            if (this._inputPtr >= this._inputEnd) {
                loadMoreGuaranteed();
            }
            byte[] bArr = this._inputBuffer;
            int i = this._inputPtr;
            this._inputPtr = i + 1;
            int ch = bArr[i] & 255;
            if (ch > 32) {
                int bits = b64variant.decodeBase64Char(ch);
                if (bits >= 0) {
                    int decodedData = bits;
                    if (this._inputPtr >= this._inputEnd) {
                        loadMoreGuaranteed();
                    }
                    byte[] bArr2 = this._inputBuffer;
                    int i2 = this._inputPtr;
                    this._inputPtr = i2 + 1;
                    int ch2 = bArr2[i2] & 255;
                    int bits2 = b64variant.decodeBase64Char(ch2);
                    if (bits2 < 0) {
                        throw reportInvalidChar(b64variant, ch2, 1);
                    }
                    int decodedData2 = (decodedData << 6) | bits2;
                    if (this._inputPtr >= this._inputEnd) {
                        loadMoreGuaranteed();
                    }
                    byte[] bArr3 = this._inputBuffer;
                    int i3 = this._inputPtr;
                    this._inputPtr = i3 + 1;
                    int ch3 = bArr3[i3] & 255;
                    int bits3 = b64variant.decodeBase64Char(ch3);
                    if (bits3 >= 0) {
                        int decodedData3 = (decodedData2 << 6) | bits3;
                        if (this._inputPtr >= this._inputEnd) {
                            loadMoreGuaranteed();
                        }
                        byte[] bArr4 = this._inputBuffer;
                        int i4 = this._inputPtr;
                        this._inputPtr = i4 + 1;
                        int ch4 = bArr4[i4] & 255;
                        int bits4 = b64variant.decodeBase64Char(ch4);
                        if (bits4 >= 0) {
                            builder.appendThreeBytes((decodedData3 << 6) | bits4);
                        } else if (bits4 != -2) {
                            throw reportInvalidChar(b64variant, ch4, 3);
                        } else {
                            builder.appendTwoBytes(decodedData3 >> 2);
                        }
                    } else if (bits3 != -2) {
                        throw reportInvalidChar(b64variant, ch3, 2);
                    } else {
                        if (this._inputPtr >= this._inputEnd) {
                            loadMoreGuaranteed();
                        }
                        byte[] bArr5 = this._inputBuffer;
                        int i5 = this._inputPtr;
                        this._inputPtr = i5 + 1;
                        int ch5 = bArr5[i5] & 255;
                        if (!b64variant.usesPaddingChar(ch5)) {
                            throw reportInvalidChar(b64variant, ch5, 3, "expected padding character '" + b64variant.getPaddingChar() + "'");
                        }
                        builder.append(decodedData2 >> 4);
                    }
                } else if (ch == 34) {
                    return builder.toByteArray();
                } else {
                    throw reportInvalidChar(b64variant, ch, 0);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public IllegalArgumentException reportInvalidChar(Base64Variant b64variant, int ch, int bindex) throws IllegalArgumentException {
        return reportInvalidChar(b64variant, ch, bindex, null);
    }

    /* access modifiers changed from: protected */
    public IllegalArgumentException reportInvalidChar(Base64Variant b64variant, int ch, int bindex, String msg) throws IllegalArgumentException {
        String base;
        if (ch <= 32) {
            base = "Illegal white space character (code 0x" + Integer.toHexString(ch) + ") as character #" + (bindex + 1) + " of 4-char base64 unit: can only used between units";
        } else if (b64variant.usesPaddingChar(ch)) {
            base = "Unexpected padding character ('" + b64variant.getPaddingChar() + "') as character #" + (bindex + 1) + " of 4-char base64 unit: padding only legal as 3rd or 4th character";
        } else if (!Character.isDefined(ch) || Character.isISOControl(ch)) {
            base = "Illegal character (code 0x" + Integer.toHexString(ch) + ") in base64 content";
        } else {
            base = "Illegal character '" + ((char) ch) + "' (code 0x" + Integer.toHexString(ch) + ") in base64 content";
        }
        if (msg != null) {
            base = base + ": " + msg;
        }
        return new IllegalArgumentException(base);
    }
}
