package org.codehaus.jackson.impl;

import java.io.IOException;
import java.io.Reader;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.io.IOContext;

public abstract class ReaderBasedNumericParser extends ReaderBasedParserBase {
    public ReaderBasedNumericParser(IOContext pc, int features, Reader r) {
        super(pc, features, r);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004b, code lost:
        if (r12 != '.') goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004d, code lost:
        if (r7 < r2) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0068, code lost:
        r6 = r7 + 1;
        r12 = r11._inputBuffer[r7];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0070, code lost:
        if (r12 < '0') goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0074, code lost:
        if (r12 <= '9') goto L_0x008b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0076, code lost:
        if (r1 != 0) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0078, code lost:
        reportUnexpectedNumberChar(r12, "Decimal point not followed by a digit");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x007d, code lost:
        r7 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x007e, code lost:
        r0 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0081, code lost:
        if (r12 == 'e') goto L_0x0087;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0085, code lost:
        if (r12 != 'E') goto L_0x00c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0087, code lost:
        if (r7 < r2) goto L_0x008f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x008b, code lost:
        r1 = r1 + 1;
        r7 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x008f, code lost:
        r6 = r7 + 1;
        r12 = r11._inputBuffer[r7];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0097, code lost:
        if (r12 == '-') goto L_0x009d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x009b, code lost:
        if (r12 != '+') goto L_0x00da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x009d, code lost:
        if (r6 >= r2) goto L_0x0013;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x009f, code lost:
        r7 = r6 + 1;
        r12 = r11._inputBuffer[r6];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00a7, code lost:
        if (r12 > '9') goto L_0x00bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00ab, code lost:
        if (r12 < '0') goto L_0x00bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00ad, code lost:
        r0 = r0 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00af, code lost:
        if (r7 < r2) goto L_0x00b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00b4, code lost:
        r12 = r11._inputBuffer[r7];
        r7 = r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00bc, code lost:
        if (r0 != 0) goto L_0x00c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00be, code lost:
        reportUnexpectedNumberChar(r12, "Exponent indicator not followed by a digit");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00c3, code lost:
        r6 = r7 - 1;
        r11._inputPtr = r6;
        r11._textBuffer.resetWithShared(r11._inputBuffer, r8, r6 - r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00da, code lost:
        r7 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        return reset(r5, r3, r1, r0);
     */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00d7  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0015  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final org.codehaus.jackson.JsonToken parseNumberText(int r12) throws java.io.IOException, org.codehaus.jackson.JsonParseException {
        /*
            r11 = this;
            r9 = 45
            if (r12 != r9) goto L_0x001e
            r9 = 1
            r5 = r9
        L_0x0006:
            int r6 = r11._inputPtr
            r9 = 1
            int r8 = r6 - r9
            int r2 = r11._inputEnd
            if (r5 == 0) goto L_0x0035
            int r9 = r11._inputEnd
            if (r6 < r9) goto L_0x0021
        L_0x0013:
            if (r5 == 0) goto L_0x00d7
            int r9 = r8 + 1
        L_0x0017:
            r11._inputPtr = r9
            org.codehaus.jackson.JsonToken r9 = r11.parseNumberText2(r5)
        L_0x001d:
            return r9
        L_0x001e:
            r9 = 0
            r5 = r9
            goto L_0x0006
        L_0x0021:
            char[] r9 = r11._inputBuffer
            int r7 = r6 + 1
            char r12 = r9[r6]
            r9 = 57
            if (r12 > r9) goto L_0x002f
            r9 = 48
            if (r12 >= r9) goto L_0x0034
        L_0x002f:
            java.lang.String r9 = "expected digit (0-9) to follow minus sign, for valid numeric value"
            r11.reportUnexpectedNumberChar(r12, r9)
        L_0x0034:
            r6 = r7
        L_0x0035:
            r3 = 1
        L_0x0036:
            int r9 = r11._inputEnd
            if (r6 >= r9) goto L_0x0013
            char[] r9 = r11._inputBuffer
            int r7 = r6 + 1
            char r12 = r9[r6]
            r9 = 48
            if (r12 < r9) goto L_0x0048
            r9 = 57
            if (r12 <= r9) goto L_0x0051
        L_0x0048:
            r1 = 0
            r9 = 46
            if (r12 != r9) goto L_0x007e
        L_0x004d:
            if (r7 < r2) goto L_0x0068
            r6 = r7
            goto L_0x0013
        L_0x0051:
            int r3 = r3 + 1
            r9 = 2
            if (r3 != r9) goto L_0x00dc
            char[] r9 = r11._inputBuffer
            r10 = 2
            int r10 = r7 - r10
            char r9 = r9[r10]
            r10 = 48
            if (r9 != r10) goto L_0x00dc
            java.lang.String r9 = "Leading zeroes not allowed"
            r11.reportInvalidNumber(r9)
            r6 = r7
            goto L_0x0036
        L_0x0068:
            char[] r9 = r11._inputBuffer
            int r6 = r7 + 1
            char r12 = r9[r7]
            r9 = 48
            if (r12 < r9) goto L_0x0076
            r9 = 57
            if (r12 <= r9) goto L_0x008b
        L_0x0076:
            if (r1 != 0) goto L_0x007d
            java.lang.String r9 = "Decimal point not followed by a digit"
            r11.reportUnexpectedNumberChar(r12, r9)
        L_0x007d:
            r7 = r6
        L_0x007e:
            r0 = 0
            r9 = 101(0x65, float:1.42E-43)
            if (r12 == r9) goto L_0x0087
            r9 = 69
            if (r12 != r9) goto L_0x00c3
        L_0x0087:
            if (r7 < r2) goto L_0x008f
            r6 = r7
            goto L_0x0013
        L_0x008b:
            int r1 = r1 + 1
            r7 = r6
            goto L_0x004d
        L_0x008f:
            char[] r9 = r11._inputBuffer
            int r6 = r7 + 1
            char r12 = r9[r7]
            r9 = 45
            if (r12 == r9) goto L_0x009d
            r9 = 43
            if (r12 != r9) goto L_0x00da
        L_0x009d:
            if (r6 >= r2) goto L_0x0013
            char[] r9 = r11._inputBuffer
            int r7 = r6 + 1
            char r12 = r9[r6]
        L_0x00a5:
            r9 = 57
            if (r12 > r9) goto L_0x00bc
            r9 = 48
            if (r12 < r9) goto L_0x00bc
            int r0 = r0 + 1
            if (r7 < r2) goto L_0x00b4
            r6 = r7
            goto L_0x0013
        L_0x00b4:
            char[] r9 = r11._inputBuffer
            int r6 = r7 + 1
            char r12 = r9[r7]
            r7 = r6
            goto L_0x00a5
        L_0x00bc:
            if (r0 != 0) goto L_0x00c3
            java.lang.String r9 = "Exponent indicator not followed by a digit"
            r11.reportUnexpectedNumberChar(r12, r9)
        L_0x00c3:
            r6 = r7
            int r6 = r6 + -1
            r11._inputPtr = r6
            int r4 = r6 - r8
            org.codehaus.jackson.util.TextBuffer r9 = r11._textBuffer
            char[] r10 = r11._inputBuffer
            r9.resetWithShared(r10, r8, r4)
            org.codehaus.jackson.JsonToken r9 = r11.reset(r5, r3, r1, r0)
            goto L_0x001d
        L_0x00d7:
            r9 = r8
            goto L_0x0017
        L_0x00da:
            r7 = r6
            goto L_0x00a5
        L_0x00dc:
            r6 = r7
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.impl.ReaderBasedNumericParser.parseNumberText(int):org.codehaus.jackson.JsonToken");
    }

    private final JsonToken parseNumberText2(boolean negative) throws IOException, JsonParseException {
        char c;
        int outPtr;
        char c2;
        int outPtr2;
        char[] outBuf = this._textBuffer.emptyAndGetCurrentSegment();
        int outPtr3 = 0;
        if (negative) {
            outBuf[0] = '-';
            outPtr3 = 0 + 1;
        }
        int intLen = 0;
        boolean eof = false;
        while (true) {
            if (this._inputPtr >= this._inputEnd && !loadMore()) {
                c = 0;
                eof = true;
                break;
            }
            char[] cArr = this._inputBuffer;
            int i = this._inputPtr;
            this._inputPtr = i + 1;
            c = cArr[i];
            if (c < '0' || c > '9') {
                break;
            }
            intLen++;
            if (intLen == 2 && outBuf[outPtr3 - 1] == '0') {
                reportInvalidNumber("Leading zeroes not allowed");
            }
            if (outPtr3 >= outBuf.length) {
                outBuf = this._textBuffer.finishCurrentSegment();
                outPtr3 = 0;
            }
            outBuf[outPtr3] = c;
            outPtr3++;
        }
        if (intLen == 0) {
            reportInvalidNumber("Missing integer part (next char " + _getCharDesc(c) + ")");
        }
        int fractLen = 0;
        if (c == '.') {
            int outPtr4 = outPtr3 + 1;
            outBuf[outPtr3] = c;
            while (true) {
                outPtr3 = outPtr4;
                if (this._inputPtr >= this._inputEnd && !loadMore()) {
                    eof = true;
                    break;
                }
                char[] cArr2 = this._inputBuffer;
                int i2 = this._inputPtr;
                this._inputPtr = i2 + 1;
                c = cArr2[i2];
                if (c < '0' || c > '9') {
                    break;
                }
                fractLen++;
                if (outPtr3 >= outBuf.length) {
                    outBuf = this._textBuffer.finishCurrentSegment();
                    outPtr3 = 0;
                }
                outPtr4 = outPtr3 + 1;
                outBuf[outPtr3] = c;
            }
            if (fractLen == 0) {
                reportUnexpectedNumberChar(c, "Decimal point not followed by a digit");
            }
        }
        int expLen = 0;
        if (c == 'e' || c == 'E') {
            if (outPtr >= outBuf.length) {
                outBuf = this._textBuffer.finishCurrentSegment();
                outPtr = 0;
            }
            int outPtr5 = outPtr + 1;
            outBuf[outPtr] = c;
            if (this._inputPtr < this._inputEnd) {
                char[] cArr3 = this._inputBuffer;
                int i3 = this._inputPtr;
                this._inputPtr = i3 + 1;
                c2 = cArr3[i3];
            } else {
                c2 = getNextChar("expected a digit for number exponent");
            }
            if (c2 == '-' || c2 == '+') {
                if (outPtr5 >= outBuf.length) {
                    outBuf = this._textBuffer.finishCurrentSegment();
                    outPtr2 = 0;
                } else {
                    outPtr2 = outPtr5;
                }
                outPtr5 = outPtr2 + 1;
                outBuf[outPtr2] = c2;
                if (this._inputPtr < this._inputEnd) {
                    char[] cArr4 = this._inputBuffer;
                    int i4 = this._inputPtr;
                    this._inputPtr = i4 + 1;
                    c2 = cArr4[i4];
                } else {
                    c2 = getNextChar("expected a digit for number exponent");
                }
            }
            while (true) {
                outPtr = outPtr5;
                if (c2 > '9' || c2 < '0') {
                    break;
                }
                expLen++;
                if (outPtr >= outBuf.length) {
                    outBuf = this._textBuffer.finishCurrentSegment();
                    outPtr = 0;
                }
                outPtr5 = outPtr + 1;
                outBuf[outPtr] = c2;
                if (this._inputPtr >= this._inputEnd && !loadMore()) {
                    eof = true;
                    outPtr = outPtr5;
                    break;
                }
                char[] cArr5 = this._inputBuffer;
                int i5 = this._inputPtr;
                this._inputPtr = i5 + 1;
                c2 = cArr5[i5];
            }
            if (expLen == 0) {
                reportUnexpectedNumberChar(c2, "Exponent indicator not followed by a digit");
            }
        }
        if (!eof) {
            this._inputPtr--;
        }
        this._textBuffer.setCurrentLength(outPtr);
        return reset(negative, intLen, fractLen, expLen);
    }
}
