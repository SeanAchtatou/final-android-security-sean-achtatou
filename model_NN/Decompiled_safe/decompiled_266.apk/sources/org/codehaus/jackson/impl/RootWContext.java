package org.codehaus.jackson.impl;

/* compiled from: JsonWriteContext */
final class RootWContext extends JsonWriteContext {
    public RootWContext() {
        super(0, null);
    }

    public String getCurrentName() {
        return null;
    }

    public int writeFieldName(String name) {
        return 4;
    }

    public int writeValue() {
        this._index++;
        return this._index == 0 ? 0 : 3;
    }

    /* access modifiers changed from: protected */
    public void appendDesc(StringBuilder sb) {
        sb.append("/");
    }
}
