package org.codehaus.jackson.util;

import com.inmobi.androidsdk.impl.Constants;

public final class BufferRecycler {
    public static final int DEFAULT_WRITE_CONCAT_BUFFER_LEN = 2000;
    protected final byte[][] mByteBuffers = new byte[ByteBufferType.values().length][];
    protected final char[][] mCharBuffers = new char[CharBufferType.values().length][];

    public enum ByteBufferType {
        READ_IO_BUFFER(4000),
        WRITE_ENCODING_BUFFER(4000),
        WRITE_CONCAT_BUFFER(BufferRecycler.DEFAULT_WRITE_CONCAT_BUFFER_LEN);
        
        private final int size;

        private ByteBufferType(int size2) {
            this.size = size2;
        }
    }

    public enum CharBufferType {
        TOKEN_BUFFER(BufferRecycler.DEFAULT_WRITE_CONCAT_BUFFER_LEN),
        CONCAT_BUFFER(BufferRecycler.DEFAULT_WRITE_CONCAT_BUFFER_LEN),
        TEXT_BUFFER(Constants.HTTP_SUCCESS_CODE),
        NAME_COPY_BUFFER(Constants.HTTP_SUCCESS_CODE);
        
        private final int size;

        private CharBufferType(int size2) {
            this.size = size2;
        }
    }

    public byte[] allocByteBuffer(ByteBufferType type) {
        int ix = type.ordinal();
        byte[] buffer = this.mByteBuffers[ix];
        if (buffer == null) {
            return balloc(ByteBufferType.access$000(type));
        }
        this.mByteBuffers[ix] = null;
        return buffer;
    }

    public void releaseByteBuffer(ByteBufferType type, byte[] buffer) {
        this.mByteBuffers[type.ordinal()] = buffer;
    }

    public char[] allocCharBuffer(CharBufferType type) {
        return allocCharBuffer(type, 0);
    }

    public char[] allocCharBuffer(CharBufferType type, int minSize) {
        if (CharBufferType.access$100(type) > minSize) {
            minSize = CharBufferType.access$100(type);
        }
        int ix = type.ordinal();
        char[] buffer = this.mCharBuffers[ix];
        if (buffer == null || buffer.length < minSize) {
            return calloc(minSize);
        }
        this.mCharBuffers[ix] = null;
        return buffer;
    }

    public void releaseCharBuffer(CharBufferType type, char[] buffer) {
        this.mCharBuffers[type.ordinal()] = buffer;
    }

    private byte[] balloc(int size) {
        return new byte[size];
    }

    private char[] calloc(int size) {
        return new char[size];
    }
}
