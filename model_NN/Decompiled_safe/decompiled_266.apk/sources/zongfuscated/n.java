package zongfuscated;

import android.os.Handler;
import android.os.Message;
import com.zong.android.engine.ZpMoConst;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

public class n implements Runnable {
    private static final String a = n.class.getSimpleName();
    private boolean b;
    private HttpGet c;
    private Handler d;

    public n(Handler handler, String str, HashMap<String, String> hashMap, boolean z) {
        this.b = false;
        this.c = null;
        this.d = null;
        this.b = z;
        this.d = handler;
        String str2 = String.valueOf(str) + a(str, hashMap);
        q.a(a, "Request URI", str2);
        this.c = new HttpGet(str2);
        this.c.addHeader("User-Agent", ZpMoConst.ANDROID_USER_AGENT);
    }

    public n(String str, HashMap<String, String> hashMap) {
        this(null, str, hashMap, false);
    }

    private static String a(String str, HashMap<String, String> hashMap) {
        String encode;
        if (hashMap == null || hashMap.size() <= 0) {
            return "";
        }
        StringBuilder sb = str.contains("?") ? new StringBuilder("&") : new StringBuilder("?");
        int i = 0;
        for (String next : hashMap.keySet()) {
            if (i > 0) {
                sb.append("&");
            }
            try {
                encode = URLEncoder.encode(hashMap.get(next), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                q.a(a, "Default Encoding Scheme used", e);
                encode = URLEncoder.encode(hashMap.get(next));
            }
            sb.append(next).append("=").append(encode);
            i++;
        }
        String sb2 = sb.toString();
        q.a(a, "Params List partURL", sb2);
        return sb2;
    }

    public final o a() throws Exception {
        InputStream content;
        int i = 0;
        o oVar = null;
        HttpClient a2 = C0064d.a();
        while (oVar == null && i < 3) {
            i++;
            try {
                HttpEntity entity = a2.execute(this.c).getEntity();
                if (entity != null) {
                    if (q.a()) {
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        entity.writeTo(byteArrayOutputStream);
                        content = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
                        q.a(a, "Call() Output Stream", byteArrayOutputStream.toString());
                    } else {
                        content = entity.getContent();
                    }
                    oVar = new o(k.a(content));
                }
            } catch (Exception e) {
                q.a(a, "call()", e);
                Thread.sleep(3000);
            }
        }
        if (oVar != null) {
            return oVar;
        }
        throw new Exception("Failed to ZongService.call() ... HTTP Connection Failure ");
    }

    public void run() {
        try {
            o a2 = a();
            if (this.b) {
                q.a(a, "Message Call was FLUSH");
            } else if (a2 != null) {
                q.a(a, "Message Call was Sucessful");
                Message obtainMessage = this.d.obtainMessage(4, a2);
                if (obtainMessage != null) {
                    this.d.sendMessage(obtainMessage);
                    q.a(a, "Message Send was Sucessful");
                    return;
                }
                q.a(a, "Message Send Failure");
            } else {
                q.a(a, "Message Call Failure");
            }
        } catch (Exception e) {
            q.a(a, "RUN()", e);
        }
    }
}
