package zongfuscated;

import com.zong.android.engine.ZpMoConst;
import com.zong.android.engine.xml.pojo.lookup.ZongPricePoint;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

public class y {
    private static final String a = y.class.getSimpleName();
    private boolean b;
    private HttpPost c;
    private List<NameValuePair> d;
    private HttpContext e;
    private ZongPricePoint f = null;

    public y(String str, String str2, String str3, String str4) {
        StringBuilder sb = new StringBuilder("<requestMobilePaymentProcessEntrypoints xmlns=\"http://pay01.zong.com/zongpay\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://pay01.zong.com/zongpay/zongpay.xsd\">");
        sb.append("<customerKey>").append(str2).append("</customerKey>");
        sb.append("<countryCode>").append(str3).append("</countryCode>");
        sb.append("<items currency=\"").append(str4).append("\" />");
        sb.append("</requestMobilePaymentProcessEntrypoints>");
        q.a(a, sb.toString());
        this.c = new HttpPost(str);
        this.c.setHeader("Accept", "text/html,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5");
        this.c.setHeader("Content-Type", "application/x-www-form-urlencoded");
        this.c.addHeader("User-Agent", ZpMoConst.ANDROID_USER_AGENT);
        this.d = new ArrayList(2);
        this.d.add(new BasicNameValuePair("method", "lookup"));
        this.d.add(new BasicNameValuePair("request", sb.toString()));
        this.e = new BasicHttpContext();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: zongfuscated.q.a(java.lang.String, java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.String, org.apache.http.client.ClientProtocolException]
     candidates:
      zongfuscated.q.a(java.lang.String, java.lang.String, java.lang.String[]):void
      zongfuscated.q.a(java.lang.String, java.lang.String, java.lang.Throwable):void */
    public final ZongPricePoint a() {
        this.b = true;
        try {
            this.c.setEntity(new UrlEncodedFormEntity(this.d, "UTF-8"));
            HttpEntity entity = C0064d.a().execute(this.c, this.e).getEntity();
            if (entity != null) {
                this.f = w.a(entity.getContent());
            }
        } catch (UnsupportedEncodingException e2) {
            q.a(a, "UnsupportedEncodingException", e2);
            this.b = false;
        } catch (ClientProtocolException e3) {
            q.a(a, "ClientProtocolException", (Throwable) e3);
            this.b = false;
        } catch (IOException e4) {
            q.a(a, "IOException", e4);
            this.b = false;
        }
        if (!this.b) {
            this.f = null;
        }
        return this.f;
    }
}
