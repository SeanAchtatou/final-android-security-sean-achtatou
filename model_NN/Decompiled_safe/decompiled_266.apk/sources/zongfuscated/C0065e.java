package zongfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import java.net.URI;
import java.net.URISyntaxException;

/* renamed from: zongfuscated.e  reason: case insensitive filesystem */
public class C0065e implements Parcelable {
    public static final Parcelable.Creator<C0065e> CREATOR = new C0066f();
    private static final String a = C0065e.class.getSimpleName();
    private String b;
    private String c;
    private int d;
    private String e;

    static {
        String[] strArr = {"192.168.1.20", "192.168.1.59", "192.168.1.84", "10.6.0.41"};
    }

    /* synthetic */ C0065e(Parcel parcel) {
        this(parcel, (byte) 0);
    }

    private C0065e(Parcel parcel, byte b2) {
        this.b = parcel.readString();
        this.c = parcel.readString();
        this.d = parcel.readInt();
        this.e = parcel.readString();
        q.a(a, "URI Dump", this.b, this.c, Integer.toString(this.d), this.e);
    }

    public C0065e(String str) {
        try {
            URI uri = new URI(str);
            this.b = uri.getScheme();
            this.c = uri.getHost();
            this.d = uri.getPort();
            this.e = uri.getPath();
            q.a(a, "URI Dump", this.b, this.c, Integer.toString(this.d), this.e);
        } catch (URISyntaxException e2) {
            q.a(a, "URI Syntax Exception", e2);
        }
    }

    private String b() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.b).append("://").append(this.c);
        if (this.d != -1) {
            sb.append(":").append(this.d);
        }
        return sb.toString();
    }

    public final String a() {
        return b() + this.e;
    }

    public final String a(String str) {
        return b() + str;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeInt(this.d);
        parcel.writeString(this.e);
    }
}
