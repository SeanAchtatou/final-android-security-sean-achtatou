package zongfuscated;

import android.content.Context;
import java.lang.ref.WeakReference;
import java.util.HashMap;

public class j {
    private static final String a = j.class.getSimpleName();
    private HashMap<String, WeakReference<Context>> b = new HashMap<>();

    public final synchronized Context a(String str) {
        Context context;
        WeakReference weakReference = this.b.get(str);
        if (weakReference == null) {
            q.a(a, "Invalid Context", str);
            context = null;
        } else {
            q.a(a, "Getting Context", str);
            context = (Context) weakReference.get();
        }
        return context;
    }

    public final synchronized void a(String str, Context context) {
        this.b.put(str, new WeakReference(context));
        q.a(a, "Setting Context", str);
    }

    public final synchronized void b(String str) {
        this.b.remove(str);
        q.a(a, "Reseting Context", str);
    }
}
