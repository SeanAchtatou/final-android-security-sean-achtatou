package zongfuscated;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class r implements u {
    private static final String a = r.class.getSimpleName();
    private Integer b;
    private String c;
    private h d;

    public r(Integer num, String str) {
        this.b = num;
        this.c = str;
    }

    public final String a() {
        q.a(a, "parseSMS", this.b.toString(), this.c, this.d.a);
        Matcher matcher = Pattern.compile(this.c).matcher(this.d.a);
        if (matcher.find()) {
            String group = matcher.group(this.b.intValue());
            q.a(a, "SMS pincode was validated", group);
            return group;
        }
        q.a(a, "Pincode not found");
        return null;
    }

    public final void a(h hVar) {
        this.d = hVar;
    }
}
