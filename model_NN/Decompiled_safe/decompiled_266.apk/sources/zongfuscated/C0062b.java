package zongfuscated;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: zongfuscated.b  reason: case insensitive filesystem */
public final class C0062b implements Parcelable {
    public static final Parcelable.Creator<C0062b> CREATOR = new t();
    private Boolean a;
    private int b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;

    public C0062b() {
    }

    /* synthetic */ C0062b(Parcel parcel) {
        this(parcel, (byte) 0);
    }

    private C0062b(Parcel parcel, byte b2) {
        this.a = Boolean.valueOf(Boolean.parseBoolean(parcel.readString()));
        this.b = parcel.readInt();
        this.c = parcel.readString();
        this.d = parcel.readString();
        this.e = parcel.readString();
        this.f = parcel.readString();
        this.g = parcel.readString();
        this.h = parcel.readString();
    }

    public final void a(int i) {
        this.b = i;
    }

    public final void a(String str) {
        this.c = str;
    }

    public final void a(boolean z) {
        this.a = Boolean.valueOf(z);
    }

    public final boolean a() {
        return this.a.booleanValue();
    }

    public final int b() {
        return this.b;
    }

    public final void b(String str) {
        this.d = str;
    }

    public final String c() {
        return this.c;
    }

    public final void c(String str) {
        this.e = str;
    }

    public final String d() {
        return this.d;
    }

    public final void d(String str) {
        this.f = str;
    }

    public final int describeContents() {
        return 0;
    }

    public final String e() {
        return this.h;
    }

    public final void e(String str) {
        this.g = str;
    }

    public final void f(String str) {
        this.h = str;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a.toString());
        parcel.writeInt(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        parcel.writeString(this.h);
    }
}
