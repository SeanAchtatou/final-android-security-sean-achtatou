package zongfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public final class B implements Parcelable {
    public static final Parcelable.Creator<B> CREATOR = new E();
    private ArrayList<C> a;
    private String b;

    public B() {
        this.a = new ArrayList<>();
    }

    public B(Parcel parcel) {
        this.b = parcel.readString();
        int readInt = parcel.readInt();
        if (readInt > 0) {
            C[] cArr = (C[]) C.CREATOR.newArray(readInt);
            parcel.readTypedArray(cArr, C.CREATOR);
            this.a = new ArrayList<>();
            for (C add : cArr) {
                this.a.add(add);
            }
        }
    }

    public final ArrayList<C> a() {
        return this.a;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final void a(ArrayList<C> arrayList) {
        this.a = arrayList;
    }

    public final String b() {
        return this.b;
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.b);
        int size = this.a.size();
        parcel.writeInt(size);
        if (size > 0) {
            parcel.writeTypedArray((C[]) this.a.toArray(new C[size]), i);
        }
    }
}
