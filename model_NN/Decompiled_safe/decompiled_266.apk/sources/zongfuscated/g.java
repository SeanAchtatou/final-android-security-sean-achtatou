package zongfuscated;

public final class g {
    private static char[] a = new char[64];
    private static byte[] b = new byte[128];

    static {
        char c = 'A';
        int i = 0;
        while (c <= 'Z') {
            a[i] = c;
            c = (char) (c + 1);
            i++;
        }
        char c2 = 'a';
        while (c2 <= 'z') {
            a[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = '0';
        while (c3 <= '9') {
            a[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        a[i] = '+';
        a[i + 1] = '/';
        for (int i2 = 0; i2 < b.length; i2++) {
            b[i2] = -1;
        }
        for (int i3 = 0; i3 < 64; i3++) {
            b[a[i3]] = (byte) i3;
        }
    }

    private g() {
    }

    public static byte[] a(String str) {
        int i;
        char c;
        int i2;
        char c2;
        int i3 = 0;
        char[] charArray = str.toCharArray();
        int length = charArray.length;
        if (length % 4 != 0) {
            throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
        }
        while (length > 0 && charArray[length - 1] == '=') {
            length--;
        }
        int i4 = (length * 3) / 4;
        byte[] bArr = new byte[i4];
        int i5 = 0;
        while (i5 < length) {
            int i6 = i5 + 1;
            char c3 = charArray[i5];
            int i7 = i6 + 1;
            char c4 = charArray[i6];
            if (i7 < length) {
                i = i7 + 1;
                c = charArray[i7];
            } else {
                i = i7;
                c = 'A';
            }
            if (i < length) {
                i2 = i + 1;
                c2 = charArray[i];
            } else {
                i2 = i;
                c2 = 'A';
            }
            if (c3 > 127 || c4 > 127 || c > 127 || c2 > 127) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            byte b2 = b[c3];
            byte b3 = b[c4];
            byte b4 = b[c];
            byte b5 = b[c2];
            if (b2 < 0 || b3 < 0 || b4 < 0 || b5 < 0) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            int i8 = (b2 << 2) | (b3 >>> 4);
            int i9 = ((b3 & 15) << 4) | (b4 >>> 2);
            byte b6 = ((b4 & 3) << 6) | b5;
            int i10 = i3 + 1;
            bArr[i3] = (byte) i8;
            if (i10 < i4) {
                i3 = i10 + 1;
                bArr[i10] = (byte) i9;
            } else {
                i3 = i10;
            }
            if (i3 < i4) {
                bArr[i3] = (byte) b6;
                i3++;
                i5 = i2;
            } else {
                i5 = i2;
            }
        }
        return bArr;
    }

    public static char[] a(byte[] bArr) {
        int i;
        byte b2;
        int i2;
        byte b3;
        int length = bArr.length;
        int i3 = ((length * 4) + 2) / 3;
        char[] cArr = new char[(((length + 2) / 3) * 4)];
        int i4 = 0;
        int i5 = 0;
        while (i5 < length) {
            int i6 = i5 + 1;
            byte b4 = bArr[i5] & 255;
            if (i6 < length) {
                i = i6 + 1;
                b2 = bArr[i6] & 255;
            } else {
                i = i6;
                b2 = 0;
            }
            if (i < length) {
                i2 = i + 1;
                b3 = bArr[i] & 255;
            } else {
                i2 = i;
                b3 = 0;
            }
            int i7 = b4 >>> 2;
            int i8 = ((b4 & 3) << 4) | (b2 >>> 4);
            int i9 = ((b2 & 15) << 2) | (b3 >>> 6);
            byte b5 = b3 & 63;
            int i10 = i4 + 1;
            cArr[i4] = a[i7];
            int i11 = i10 + 1;
            cArr[i10] = a[i8];
            cArr[i11] = i11 < i3 ? a[i9] : '=';
            int i12 = i11 + 1;
            cArr[i12] = i12 < i3 ? a[b5] : '=';
            i4 = i12 + 1;
            i5 = i2;
        }
        return cArr;
    }
}
