package zongfuscated;

import java.util.ArrayList;

public final class o {
    C a;
    private B b;

    public o() {
    }

    public o(B b2) {
        this.b = b2;
        ArrayList<C> a2 = b2.a();
        this.a = (a2 == null || a2.size() <= 0) ? new C() : a2.get(a2.size() - 1);
    }

    public static boolean a(String str) {
        return str == null || str.equals("");
    }

    public static String b(String str) {
        return str != null ? str : "";
    }

    public final C a() {
        return this.a;
    }

    public final String b() {
        return this.b.b();
    }
}
