package zongfuscated;

import android.os.Handler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class A {
    private static final String a = A.class.getSimpleName();
    private C0065e b;
    private Handler c;
    private final ExecutorService d;
    private Future<?> e = null;

    public A(ExecutorService executorService, C0065e eVar, Handler handler) {
        this.d = executorService;
        this.b = eVar;
        this.c = handler;
    }

    private void a(String str, HashMap<String, String> hashMap, boolean z) {
        n nVar = new n(this.c, str, hashMap, z);
        q.a(a, "Service Loader Created for URL", str);
        try {
            if (!this.d.isShutdown()) {
                q.a(a, "Pool Running");
                this.e = this.d.submit(nVar);
                q.a(a, "Loader Executed");
                return;
            }
            q.a(a, "Pool Shutdown");
        } catch (Exception e2) {
            q.a(a, "ZongExchange.process", e2);
        }
    }

    private void a(o oVar, HashMap<String, String> hashMap, boolean z) {
        C c2 = oVar.a;
        String a2 = this.b.a(c2.b().trim());
        HashMap hashMap2 = new HashMap();
        if (c2.c() != null) {
            hashMap2.put("_eventId", c2.c());
        }
        ArrayList<D> e2 = c2.e();
        if (!(e2 == null || hashMap == null)) {
            Iterator<D> it = e2.iterator();
            while (it.hasNext()) {
                D next = it.next();
                String a3 = next.a();
                if (next.c() && hashMap.get(a3) != null) {
                    q.a(a, "Request Params", a3, hashMap.get(a3));
                    hashMap2.put(a3, hashMap.get(a3));
                }
            }
        }
        q.a(a, "Request Params", hashMap2.toString());
        a(a2, hashMap2, z);
    }

    public final void a() {
        if (this.e != null) {
            this.e.cancel(true);
        }
        this.e = null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: zongfuscated.A.a(java.lang.String, java.util.HashMap<java.lang.String, java.lang.String>, boolean):void
     arg types: [java.lang.String, java.util.HashMap<java.lang.String, java.lang.String>, int]
     candidates:
      zongfuscated.A.a(zongfuscated.o, java.util.HashMap<java.lang.String, java.lang.String>, boolean):void
      zongfuscated.A.a(java.lang.String, java.util.HashMap<java.lang.String, java.lang.String>, boolean):void */
    public final void a(HashMap<String, String> hashMap) {
        a(this.b.a(), hashMap, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: zongfuscated.A.a(zongfuscated.o, java.util.HashMap<java.lang.String, java.lang.String>, boolean):void
     arg types: [zongfuscated.o, ?[OBJECT, ARRAY], int]
     candidates:
      zongfuscated.A.a(java.lang.String, java.util.HashMap<java.lang.String, java.lang.String>, boolean):void
      zongfuscated.A.a(zongfuscated.o, java.util.HashMap<java.lang.String, java.lang.String>, boolean):void */
    public final void a(o oVar) {
        a(oVar, (HashMap<String, String>) null, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: zongfuscated.A.a(zongfuscated.o, java.util.HashMap<java.lang.String, java.lang.String>, boolean):void
     arg types: [zongfuscated.o, java.util.HashMap<java.lang.String, java.lang.String>, int]
     candidates:
      zongfuscated.A.a(java.lang.String, java.util.HashMap<java.lang.String, java.lang.String>, boolean):void
      zongfuscated.A.a(zongfuscated.o, java.util.HashMap<java.lang.String, java.lang.String>, boolean):void */
    public final void a(o oVar, HashMap<String, String> hashMap) {
        a(oVar, hashMap, false);
    }
}
