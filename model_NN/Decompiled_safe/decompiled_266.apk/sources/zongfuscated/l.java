package zongfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.zong.android.engine.activities.ZongPaymentRequest;

public final class l implements Parcelable {
    public static final Parcelable.Creator<l> CREATOR = new v();
    private ZongPaymentRequest a;
    private String b;
    private String c;

    public l() {
    }

    /* synthetic */ l(Parcel parcel) {
        this(parcel, (byte) 0);
    }

    private l(Parcel parcel, byte b2) {
        this.a = (ZongPaymentRequest) parcel.readParcelable(getClass().getClassLoader());
        this.b = parcel.readString();
        this.c = parcel.readString();
    }

    public final ZongPaymentRequest a() {
        return this.a;
    }

    public final void a(ZongPaymentRequest zongPaymentRequest) {
        this.a = zongPaymentRequest;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.c = str;
    }

    public final String c() {
        return this.c;
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.a, i);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
    }
}
