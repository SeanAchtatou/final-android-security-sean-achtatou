package zongfuscated;

public class s extends r {
    private static final String a = s.class.getSimpleName();

    public s(Integer num, String str) {
        super(num, str);
    }

    public final String a(boolean z, String str) {
        if (z) {
            q.a(a, "FlowPincode::Simulation mode pinCode generator");
            return "9999";
        }
        String a2 = a();
        q.a(a, "FlowPincode::Live mode pinCode parsing");
        return a2;
    }
}
