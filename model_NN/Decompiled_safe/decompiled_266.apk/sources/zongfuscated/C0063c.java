package zongfuscated;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.zong.android.engine.web.ZongWebView;

/* renamed from: zongfuscated.c  reason: case insensitive filesystem */
public class C0063c extends WebViewClient {
    private static final String a = C0063c.class.getSimpleName();
    private String b = "Zong Mobile Payments";
    private ZongWebView c;
    private ProgressDialog d = null;

    public C0063c(ZongWebView zongWebView) {
        this.c = zongWebView;
    }

    public final void a(String str) {
        this.b = str;
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        q.a(a, "On Page Finished", str);
        if (this.d != null) {
            this.d.dismiss();
        }
        this.d = null;
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        q.a(a, "On Page Started", str);
        if (this.d == null && !this.c.isFinishing()) {
            this.d = ProgressDialog.show(this.c, "", this.b, true);
        }
    }
}
