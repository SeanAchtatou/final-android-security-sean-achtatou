package zongfuscated;

import android.sax.Element;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import android.sax.StartElementListener;
import android.util.Xml;
import com.adknowledge.superrewards.model.SROffer;
import com.adknowledge.superrewards.model.SRParams;
import java.io.InputStream;
import java.util.ArrayList;
import org.xml.sax.Attributes;

public class k {
    private static final String a = k.class.getSimpleName();

    public static B a(InputStream inputStream) {
        final B b = new B();
        b.a(new ArrayList());
        RootElement rootElement = new RootElement("ZgRespond");
        Element child = rootElement.getChild("actions");
        Element child2 = rootElement.getChild("view");
        Element child3 = child.getChild("action");
        Element child4 = child3.getChild("message");
        Element child5 = child3.getChild(SRParams.PARAMS);
        child3.setStartElementListener(new StartElementListener() {
            public final void start(Attributes attributes) {
                C c = new C();
                String value = attributes.getValue(SROffer.TYPE);
                String value2 = attributes.getValue("url");
                String value3 = attributes.getValue("eventId");
                c.a(value);
                c.b(value2);
                c.c(value3);
                c.a(new ArrayList());
                B.this.a().add(c);
            }
        });
        child4.setEndTextElementListener(new EndTextElementListener() {
            public final void end(String str) {
                B.this.a().get(B.this.a().size() - 1).d(str.trim());
            }
        });
        child5.getChild("param").setStartElementListener(new StartElementListener() {
            public final void start(Attributes attributes) {
                D d = new D();
                String value = attributes.getValue("name");
                String value2 = attributes.getValue("value");
                String value3 = attributes.getValue("isAndroidParameter");
                d.a(value);
                d.b(value2);
                d.c(value3);
                B.this.a().get(B.this.a().size() - 1).e().add(d);
            }
        });
        child2.setEndTextElementListener(new EndTextElementListener() {
            public final void end(String str) {
                B.this.a(str);
            }
        });
        try {
            Xml.parse(inputStream, Xml.Encoding.UTF_8, rootElement.getContentHandler());
            return b;
        } catch (Exception e) {
            q.a(a, "Parse", e);
            return null;
        }
    }
}
