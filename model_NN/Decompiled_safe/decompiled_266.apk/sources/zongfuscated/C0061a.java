package zongfuscated;

/* renamed from: zongfuscated.a  reason: case insensitive filesystem */
public class C0061a extends r {
    private static final String a = C0061a.class.getSimpleName();

    public C0061a(Integer num, String str) {
        super(num, str);
    }

    public final String a(boolean z, String str) {
        if (z) {
            int length = str.length() - 3;
            String substring = str.substring(length);
            String str2 = substring.contains("00") ? "9999" + str.substring(length + 1) : "999" + substring;
            q.a(a, "FlowFR::Simulation mode pinCode generator");
            return str2;
        }
        String a2 = a();
        q.a(a, "FlowFR::Live mode pinCode parsing");
        return a2;
    }
}
