package zongfuscated;

import android.os.Parcel;
import android.os.Parcelable;

public final class p implements Parcelable {
    public static final Parcelable.Creator<p> CREATOR = new z();
    private String a;

    /* synthetic */ p(Parcel parcel) {
        this(parcel, (byte) 0);
    }

    private p(Parcel parcel, byte b) {
        this.a = parcel.readString();
    }

    public p(String str) {
        this.a = str;
    }

    public final String a() {
        return this.a;
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
    }
}
