package zongfuscated;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;

public final class F implements Parcelable {
    public static final Parcelable.Creator<F> CREATOR = new i();
    private Boolean a;
    private String b;
    private String c;
    private String d;
    private String e;
    private ContentValues f;
    private C0065e g;

    public F() {
    }

    /* synthetic */ F(Parcel parcel) {
        this(parcel, (byte) 0);
    }

    private F(Parcel parcel, byte b2) {
        this.a = Boolean.valueOf(Boolean.parseBoolean(parcel.readString()));
        this.b = parcel.readString();
        this.c = parcel.readString();
        this.e = parcel.readString();
        this.f = (ContentValues) parcel.readParcelable(getClass().getClassLoader());
        this.g = (C0065e) parcel.readParcelable(getClass().getClassLoader());
    }

    public final Boolean a() {
        return this.a;
    }

    public final void a(ContentValues contentValues) {
        this.f = contentValues;
    }

    public final void a(Boolean bool) {
        this.a = bool;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final void a(C0065e eVar) {
        this.g = eVar;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.c = str;
    }

    public final String c() {
        return this.c;
    }

    public final void c(String str) {
        this.d = str;
    }

    public final String d() {
        return this.d;
    }

    public final void d(String str) {
        this.e = str;
    }

    public final int describeContents() {
        return 0;
    }

    public final String e() {
        return this.e;
    }

    public final ContentValues f() {
        return this.f;
    }

    public final C0065e g() {
        return this.g;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a.toString());
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.e);
        parcel.writeParcelable(this.f, i);
        parcel.writeParcelable(this.g, i);
    }
}
