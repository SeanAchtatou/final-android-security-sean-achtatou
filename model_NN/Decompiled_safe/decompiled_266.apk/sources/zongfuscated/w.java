package zongfuscated;

import android.sax.Element;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import android.sax.StartElementListener;
import android.util.Xml;
import com.adknowledge.superrewards.model.SROffer;
import com.adknowledge.superrewards.model.SRParams;
import com.adknowledge.superrewards.model.SRPricePoint;
import com.zong.android.engine.xml.pojo.lookup.ZongPricePoint;
import com.zong.android.engine.xml.pojo.lookup.ZongPricePointItem;
import java.io.InputStream;
import org.xml.sax.Attributes;

public class w {
    private static final String a = w.class.getSimpleName();

    public static ZongPricePoint a(InputStream inputStream) {
        final ZongPricePoint zongPricePoint = new ZongPricePoint();
        RootElement rootElement = new RootElement("http://pay01.zong.com/zongpay", "responseMobilePaymentProcessEntrypoints");
        Element child = rootElement.getChild("http://pay01.zong.com/zongpay", SRParams.PROVIDERS);
        Element child2 = rootElement.getChild("http://pay01.zong.com/zongpay", "items");
        rootElement.getChild("http://pay01.zong.com/zongpay", "countryCode").setEndTextElementListener(new EndTextElementListener() {
            public final void end(String str) {
                ZongPricePoint.this.setCountryCode(str);
            }
        });
        rootElement.getChild("http://pay01.zong.com/zongpay", "localCurrency").setEndTextElementListener(new EndTextElementListener() {
            public final void end(String str) {
                ZongPricePoint.this.setLocalCurrency(str);
            }
        });
        rootElement.getChild("http://pay01.zong.com/zongpay", "exchangeRate").setEndTextElementListener(new EndTextElementListener() {
            public final void end(String str) {
                ZongPricePoint.this.setExchangeRate(Float.valueOf(str).floatValue());
            }
        });
        child.getChild("http://pay01.zong.com/zongpay", SRParams.PROVIDER).setStartElementListener(new StartElementListener() {
            public final void start(Attributes attributes) {
                ZongPricePoint.this.putProvider(attributes.getValue(SROffer.ID), attributes.getValue("name"));
            }
        });
        Element child3 = child2.getChild("http://pay01.zong.com/zongpay", "item");
        Element child4 = child3.getChild("http://pay01.zong.com/zongpay", "supportedProviders");
        Element child5 = child3.getChild("http://pay01.zong.com/zongpay", SRPricePoint.ENTRYPOINTURL);
        child3.setStartElementListener(new StartElementListener() {
            public final void start(Attributes attributes) {
                String value = attributes.getValue("priceMatched");
                String value2 = attributes.getValue("exactPrice");
                String value3 = attributes.getValue("workingPrice");
                String value4 = attributes.getValue("outPayment");
                String value5 = attributes.getValue("requestCurrencyOutpayment");
                String value6 = attributes.getValue("itemRef");
                String value7 = attributes.getValue("zongPlusOnly");
                ZongPricePointItem zongPricePointItem = new ZongPricePointItem();
                zongPricePointItem.setPriceMatched(Boolean.valueOf(value).booleanValue());
                zongPricePointItem.setExactPrice(Float.valueOf(value2).floatValue());
                zongPricePointItem.setWorkingPrice(Float.valueOf(value3).floatValue());
                zongPricePointItem.setOutPayment(Float.valueOf(value4).floatValue());
                zongPricePointItem.setRequestCurrencyOutpayment(Float.valueOf(value5).floatValue());
                zongPricePointItem.setItemRef(value6);
                zongPricePointItem.setZongPlusOnly(Boolean.valueOf(value7).booleanValue());
                ZongPricePoint.this.getItems().add(zongPricePointItem);
            }
        });
        child4.getChild("http://pay01.zong.com/zongpay", SRParams.PROVIDER).setStartElementListener(new StartElementListener() {
            public final void start(Attributes attributes) {
                ZongPricePoint.this.getItems().get(ZongPricePoint.this.getItems().size() - 1).putProvider(attributes.getValue(SROffer.ID), attributes.getValue("name"));
            }
        });
        child5.setEndTextElementListener(new EndTextElementListener() {
            public final void end(String str) {
                ZongPricePoint.this.getItems().get(ZongPricePoint.this.getItems().size() - 1).setEntrypointUrl(str);
            }
        });
        try {
            Xml.parse(inputStream, Xml.Encoding.UTF_8, rootElement.getContentHandler());
            return zongPricePoint;
        } catch (Exception e) {
            q.a(a, "Parse", e);
            return null;
        }
    }
}
