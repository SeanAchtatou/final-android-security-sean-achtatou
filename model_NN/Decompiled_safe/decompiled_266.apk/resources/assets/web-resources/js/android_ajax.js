var ppy_ajax_callback_funcs = {};
var ppy_ajax_in_redirect = false;

var ppy_ajax_helper = {
	start : function(id, url, callback, db, key, life) {
		ppy_ajax_in_redirect = false;
		ppy_ajax_callback_funcs[id] = {'callback' : callback, 'url' : url, 'db' : db, 'key' : key, 'life' : life};
		if (!db || !key)
			window.Papaya.ajaxStart_url_callback_(id, url, callback ? 1 : 0);
		else {
			db.kvExpire(key);
			db.kvExpireBatch(key + "__tmpl__%");
			window.Papaya.ajaxStart_url_callback_db_scope_key_life_(id, url, callback ? 1 : 0, db.id, db.scope, key, life == undefined ? -1 : life);
		}
	},
	startWithForm : function(id, el, callback, db, key, life) {
		var action = el.getAttribute('action');
		if (!action) return;
		var inputs = el.getElementsByTagName('input');
		var sels = el.getElementsByTagName('select');
		var textareas = el.getElementsByTagName('textarea');
		var allels = [];
		var i;
		for (i = 0; i < inputs.length; i++) allels.push(inputs[i]);
		for (i = 0; i < sels.length; i++) allels.push(sels[i]);
		for (i = 0; i < textareas.length; i++) allels.push(textareas[i]);
		var qs = [];
		qs.push(action);
		for (i = 0; i < allels.length; i++) {
			var name = allels[i].name;
			var value = ppy_html.getValue(allels[i]);
			if (value === false || !name || allels[i].disabled) {
				continue;
			} else {
				if (qs.length == 1) {
					if (action.indexOf('?') < 0)
						qs.push('?');
				} else if (qs.length > 1)
					qs.push('&');
				qs.push(encodeURIComponent(name));
				qs.push('=');
				qs.push(encodeURIComponent(value));
			}
		}

		ppy_ajax_helper.start(id, qs.join(""), callback, db, key, life);
	},
	startWithCache : function (id, url, callback, db, key, life) {
		if (!db || !key) {
			ppy_ajax_helper.start(id, url, callback, db, key, life);
			return true;
		} else {
			var value = db.kvLoad(key);
			if (value) {
				if (callback)
					__ppy_ajax_lateInvoke(id, PPYAjaxResponse.SUCC, {'callback':callback, 'db' : db, 'key' : key, 'life' : life}, ppy_funcs.convertHtml(value), true);
				return false;
			} else {
				ppy_ajax_helper.start(id, url, callback, db, key, life);
				return true;
			}
		}
	},
	startWithCacheAsync : function (id, url, callback, db, key, life) {
		if (!db || !key) {
			ppy_ajax_helper.start(id, url, callback, db, key, life);
			return true;
		} else {
			var value = db.kvLoad(key);
			if (value) {
				if (callback)
					setTimeout(__ppy_ajax_lateInvoke, 0, id, PPYAjaxResponse.SUCC, {'callback':callback, 'db' : db, 'key' : key, 'life' : life}, ppy_funcs.convertHtml(value), true);
				return false;
			} else {
				ppy_ajax_helper.start(id, url, callback, db, key, life);
				return true;
			}
		}
	},
	cancel : function (id) {
		ppy_ajax_callback_funcs[id] = null;
		window.Papaya.ajaxCancel_(id);
	},
	checkVisible : function(feature, callback, db) {
		db = db == undefined ? new PPYDatabase(PPYDatabase.NAME_SESSION, PPYDatabase.SCOPE_SESSION) : db;
		this.startWithCache("visible_" + feature, 'json_visible?feature=' + feature, callback, db, 'k_visible_' + feature, -1);
	},	
	startPost : function(cfg, callback) {
		ppy_ajax_in_redirect = false;
		ppy_ajax_callback_funcs[cfg['id']] = {'callback' : callback, 'url' : cfg['url']};
		window.Papaya.startPost(toJSONString(cfg));
	}
};

function PPYAjaxResponse(id, status, content, cache, ctx) {
	this.id = id;
	this.status = status;
	this.content = content;
	this.cache = cache;
	this.__ctx = ctx;

	this.getJson = function() {
		this.__json = this.__json || evalJSON(this.content);
		return this.__json;
	};

	this.getTmplContent = function(tmpl) {
		var tmplContent;
		var tmplKey = [this.__ctx.key, "__tmpl__", tmpl].join("");
		if (this.cache)
			tmplContent = this.__ctx.db.kvLoad(tmplKey);
		if (!tmplContent) {
			if (this.getJson()) {
				tmplContent = ppy_tmpl(tmpl, this.getJson());
				if (this.__ctx && this.__ctx.db && this.__ctx.key)
					this.__ctx.db.kvSave(tmplKey, tmplContent, this.__ctx.life);
			}
		}

		return tmplContent;
	};
}

PPYAjaxResponse.FAILED = 0;
PPYAjaxResponse.SUCC = 1;

function __ppy_ajax_lateInvoke(id, status, ctx, content, cache) {
	if (ctx.callback) {
		if (!status)
			ppy_funcs.switchReusable(false);
		eval(ctx.callback)(new PPYAjaxResponse(id, status, content, cache, ctx));
	}
}

function ppy_onAjaxFinished(id, status, url) {
	if (ppy_ajax_in_redirect) return;
	var ctx = undefined;
	var json = undefined;
	var content = undefined;
	if (status == PPYAjaxResponse.SUCC) {
		content = ppy_string(window.Papaya.ajaxGet_convert_(id, 1));
		if (content) {
			if (content.indexOf("__redirect__") >= 0) {
				json = evalJSON(content);
				if (json) {
					var redirect = json['__redirect__'];

					var msg = json['__message__'];
					if (msg)
						ppy_funcs.info(msg, json['__message_type__']);

					if (redirect && redirect.length > 0) {
						ppy_ajax_in_redirect = true;
						ppy_funcs.switchReusable(false);
						document.location = redirect;
						return;
					}
				}
			} else if (content.indexOf("__post_error__") >= 0) {
				json = evalJSON(content);
				if (json) {
					var msg = json['__message__'];
					if (msg)
						ppy_funcs.info(msg, json['__message_type__']);
					var snippet = json['__post_error__'];
					if (snippet)
						eval(snippet);
				}
			} else if (content.indexOf("__message__") >= 0) {
				json = evalJSON(content);
				if (json) {
					var msg = json['__message__'];
					if (msg)
						ppy_funcs.info(msg, json['__message_type__']);
				}
			}
		}
	}

	ctx = ppy_ajax_callback_funcs[id];
	ppy_ajax_callback_funcs[id] = undefined;
	if (ctx && ctx.callback && ctx.url == url)
		__ppy_ajax_lateInvoke(id, status, ctx, content, false);
}