package FindTheSame.Chinese;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int about = 2130837504;
        public static final int arrow = 2130837505;
        public static final int blackbg = 2130837506;
        public static final int butterfly1 = 2130837507;
        public static final int butterfly10 = 2130837508;
        public static final int butterfly11 = 2130837509;
        public static final int butterfly12 = 2130837510;
        public static final int butterfly13 = 2130837511;
        public static final int butterfly14 = 2130837512;
        public static final int butterfly15 = 2130837513;
        public static final int butterfly16 = 2130837514;
        public static final int butterfly17 = 2130837515;
        public static final int butterfly18 = 2130837516;
        public static final int butterfly19 = 2130837517;
        public static final int butterfly2 = 2130837518;
        public static final int butterfly3 = 2130837519;
        public static final int butterfly4 = 2130837520;
        public static final int butterfly5 = 2130837521;
        public static final int butterfly6 = 2130837522;
        public static final int butterfly7 = 2130837523;
        public static final int butterfly8 = 2130837524;
        public static final int butterfly9 = 2130837525;
        public static final int challenge = 2130837526;
        public static final int circle = 2130837527;
        public static final int clock = 2130837528;
        public static final int crystal_ball1 = 2130837529;
        public static final int crystal_ball10 = 2130837530;
        public static final int crystal_ball11 = 2130837531;
        public static final int crystal_ball12 = 2130837532;
        public static final int crystal_ball13 = 2130837533;
        public static final int crystal_ball14 = 2130837534;
        public static final int crystal_ball15 = 2130837535;
        public static final int crystal_ball16 = 2130837536;
        public static final int crystal_ball17 = 2130837537;
        public static final int crystal_ball18 = 2130837538;
        public static final int crystal_ball19 = 2130837539;
        public static final int crystal_ball2 = 2130837540;
        public static final int crystal_ball3 = 2130837541;
        public static final int crystal_ball4 = 2130837542;
        public static final int crystal_ball5 = 2130837543;
        public static final int crystal_ball6 = 2130837544;
        public static final int crystal_ball7 = 2130837545;
        public static final int crystal_ball8 = 2130837546;
        public static final int crystal_ball9 = 2130837547;
        public static final int easy = 2130837548;
        public static final int exit = 2130837549;
        public static final int fish1 = 2130837550;
        public static final int fish10 = 2130837551;
        public static final int fish11 = 2130837552;
        public static final int fish12 = 2130837553;
        public static final int fish13 = 2130837554;
        public static final int fish14 = 2130837555;
        public static final int fish15 = 2130837556;
        public static final int fish16 = 2130837557;
        public static final int fish17 = 2130837558;
        public static final int fish18 = 2130837559;
        public static final int fish19 = 2130837560;
        public static final int fish2 = 2130837561;
        public static final int fish3 = 2130837562;
        public static final int fish4 = 2130837563;
        public static final int fish5 = 2130837564;
        public static final int fish6 = 2130837565;
        public static final int fish7 = 2130837566;
        public static final int fish8 = 2130837567;
        public static final int fish9 = 2130837568;
        public static final int flower1 = 2130837569;
        public static final int flower10 = 2130837570;
        public static final int flower11 = 2130837571;
        public static final int flower12 = 2130837572;
        public static final int flower13 = 2130837573;
        public static final int flower14 = 2130837574;
        public static final int flower15 = 2130837575;
        public static final int flower16 = 2130837576;
        public static final int flower17 = 2130837577;
        public static final int flower18 = 2130837578;
        public static final int flower19 = 2130837579;
        public static final int flower2 = 2130837580;
        public static final int flower3 = 2130837581;
        public static final int flower4 = 2130837582;
        public static final int flower5 = 2130837583;
        public static final int flower6 = 2130837584;
        public static final int flower7 = 2130837585;
        public static final int flower8 = 2130837586;
        public static final int flower9 = 2130837587;
        public static final int frame = 2130837588;
        public static final int gamebg = 2130837589;
        public static final int hard = 2130837590;
        public static final int help = 2130837591;
        public static final int help_image = 2130837592;
        public static final int icon = 2130837593;
        public static final int medium = 2130837594;
        public static final int menubg = 2130837595;
        public static final int pause = 2130837596;
        public static final int pause_menu = 2130837597;
        public static final int play = 2130837598;
        public static final int practise = 2130837599;
        public static final int ready = 2130837600;
        public static final int ring1 = 2130837601;
        public static final int ring10 = 2130837602;
        public static final int ring11 = 2130837603;
        public static final int ring12 = 2130837604;
        public static final int ring13 = 2130837605;
        public static final int ring14 = 2130837606;
        public static final int ring15 = 2130837607;
        public static final int ring16 = 2130837608;
        public static final int ring17 = 2130837609;
        public static final int ring18 = 2130837610;
        public static final int ring19 = 2130837611;
        public static final int ring2 = 2130837612;
        public static final int ring3 = 2130837613;
        public static final int ring4 = 2130837614;
        public static final int ring5 = 2130837615;
        public static final int ring6 = 2130837616;
        public static final int ring7 = 2130837617;
        public static final int ring8 = 2130837618;
        public static final int ring9 = 2130837619;
        public static final int rock1 = 2130837620;
        public static final int rock10 = 2130837621;
        public static final int rock11 = 2130837622;
        public static final int rock12 = 2130837623;
        public static final int rock13 = 2130837624;
        public static final int rock14 = 2130837625;
        public static final int rock15 = 2130837626;
        public static final int rock16 = 2130837627;
        public static final int rock17 = 2130837628;
        public static final int rock18 = 2130837629;
        public static final int rock19 = 2130837630;
        public static final int rock2 = 2130837631;
        public static final int rock3 = 2130837632;
        public static final int rock4 = 2130837633;
        public static final int rock5 = 2130837634;
        public static final int rock6 = 2130837635;
        public static final int rock7 = 2130837636;
        public static final int rock8 = 2130837637;
        public static final int rock9 = 2130837638;
        public static final int score_board = 2130837639;
        public static final int soundoff = 2130837640;
        public static final int soundon = 2130837641;
    }

    public static final class id {
        public static final int ad_zone = 2131165184;
        public static final int back_arrow = 2131165212;
        public static final int back_arrow_help = 2131165216;
        public static final int button_backToMenu = 2131165197;
        public static final int button_tryAgain = 2131165196;
        public static final int circle = 2131165201;
        public static final int complexity_menu = 2131165207;
        public static final int game_zone = 2131165185;
        public static final int help_window = 2131165213;
        public static final int highest_score = 2131165195;
        public static final int imageView1 = 2131165214;
        public static final int item = 2131165200;
        public static final int lose_window = 2131165190;
        public static final int main_menu = 2131165202;
        public static final int menu_about = 2131165205;
        public static final int menu_challenge = 2131165211;
        public static final int menu_easy = 2131165209;
        public static final int menu_exit = 2131165206;
        public static final int menu_hard = 2131165210;
        public static final int menu_help = 2131165204;
        public static final int menu_pause = 2131165217;
        public static final int menu_play = 2131165203;
        public static final int menu_sound = 2131165218;
        public static final int menu_try = 2131165208;
        public static final int pause_window = 2131165199;
        public static final int player_score = 2131165193;
        public static final int ready_window = 2131165198;
        public static final int score = 2131165187;
        public static final int score_and_time = 2131165186;
        public static final int show_window = 2131165189;
        public static final int textView1 = 2131165215;
        public static final int textView2 = 2131165192;
        public static final int textView3 = 2131165194;
        public static final int textview1 = 2131165191;
        public static final int time = 2131165188;
    }

    public static final class layout {
        public static final int game_layout = 2130903040;
        public static final int item = 2130903041;
        public static final int mainmenu = 2130903042;
    }

    public static final class menu {
        public static final int menu = 2131099648;
    }

    public static final class raw {
        public static final int error = 2130968576;
        public static final int lose = 2130968577;
        public static final int menu_btn = 2130968578;
        public static final int timer = 2130968579;
        public static final int win = 2130968580;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
        public static final int str_about = 2131034123;
        public static final int str_about_title = 2131034124;
        public static final int str_back = 2131034114;
        public static final int str_backToMenu = 2131034119;
        public static final int str_confirm = 2131034125;
        public static final int str_help = 2131034128;
        public static final int str_highscore = 2131034117;
        public static final int str_no = 2131034127;
        public static final int str_pause = 2131034120;
        public static final int str_return = 2131034129;
        public static final int str_return_info = 2131034130;
        public static final int str_soundoff = 2131034122;
        public static final int str_soundon = 2131034121;
        public static final int str_timeup = 2131034115;
        public static final int str_tryagain = 2131034118;
        public static final int str_yes = 2131034126;
        public static final int str_yourscore = 2131034116;
    }
}
