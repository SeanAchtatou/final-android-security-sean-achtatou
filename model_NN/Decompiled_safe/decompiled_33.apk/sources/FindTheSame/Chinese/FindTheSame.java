package FindTheSame.Chinese;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import cn.domob.android.ads.DomobAdManager;
import cn.domob.android.ads.DomobAdView;
import com.mobclick.android.MobclickAgent;
import java.util.HashMap;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class FindTheSame extends Activity implements View.OnClickListener, View.OnTouchListener {
    public static boolean playSound = true;
    private final int COLUMN_NUMBER;
    private final int ERROR_SOUND;
    private final String[] HIGHEST_SCORE = {"score_easy", "score_challenge", "score_hard"};
    private final String ICICLE_KEY = "saved_bundle";
    /* access modifiers changed from: private */
    public final int[][] ITEMS;
    private final int ITEM_NUMBER;
    private final int ITEM_NUMBER_EACH;
    private final int LOSE_SOUND;
    private final int MESSAGE_LOSE = 1;
    private final int MESSAGE_TIMECOUNT = 0;
    private final int MESSAGE_WIN = 2;
    private final int MODE_LOSE = 4;
    private final int MODE_PAUSE = 1;
    private final int MODE_PLAY = 5;
    private final int MODE_READY = 0;
    private final int MODE_STOP = 2;
    private final int MODE_WIN = 3;
    private final String PREFERENCE = "FindTheSame_Preference";
    private final int[] RATIO_LEVEL;
    private final Random RNG;
    private final int[] SOUND;
    private final String TAG = "FindTheSame";
    private final int TIMER_SOUND;
    private final int[] TIME_LEVEL;
    private final int WIN_SOUND;
    /* access modifiers changed from: private */
    public int[] choice;
    /* access modifiers changed from: private */
    public int complexity;
    /* access modifiers changed from: private */
    public int currentItem;
    /* access modifiers changed from: private */
    public ItemAdapter.ItemHolder first;
    /* access modifiers changed from: private */
    public int gameMode;
    /* access modifiers changed from: private */
    public GridView gridView;
    /* access modifiers changed from: private */
    public Handler handler;
    /* access modifiers changed from: private */
    public int hasChosen;
    private TextView highest_score;
    /* access modifiers changed from: private */
    public int increase_step;
    /* access modifiers changed from: private */
    public ItemAdapter itemAdapter;
    /* access modifiers changed from: private */
    public View lose_window;
    private View pause_window;
    private TextView player_score;
    /* access modifiers changed from: private */
    public int position1;
    /* access modifiers changed from: private */
    public int position2;
    private SharedPreferences preference;
    /* access modifiers changed from: private */
    public ItemAdapter.ItemHolder previous;
    /* access modifiers changed from: private */
    public int previousMode;
    private View ready_window;
    /* access modifiers changed from: private */
    public int same_item;
    private int score;
    private View score_and_time;
    private TextView score_text;
    /* access modifiers changed from: private */
    public ItemAdapter.ItemHolder second;
    private SoundPool soundPool;
    private HashMap<Integer, Integer> soundPoolMap;
    /* access modifiers changed from: private */
    public int time;
    private TextView time_text;
    private Timer timer;

    public FindTheSame() {
        int[] iArr = new int[4];
        iArr[0] = 40;
        iArr[1] = 180;
        iArr[2] = 25;
        this.TIME_LEVEL = iArr;
        int[] iArr2 = new int[4];
        iArr2[0] = 1;
        iArr2[1] = 1;
        iArr2[2] = 4;
        this.RATIO_LEVEL = iArr2;
        this.ITEM_NUMBER = 6;
        this.COLUMN_NUMBER = 4;
        this.ITEM_NUMBER_EACH = 19;
        this.ITEMS = new int[][]{new int[]{R.drawable.flower1, R.drawable.flower2, R.drawable.flower3, R.drawable.flower4, R.drawable.flower5, R.drawable.flower6, R.drawable.flower7, R.drawable.flower8, R.drawable.flower9, R.drawable.flower10, R.drawable.flower11, R.drawable.flower12, R.drawable.flower13, R.drawable.flower14, R.drawable.flower15, R.drawable.flower16, R.drawable.flower17, R.drawable.flower18, R.drawable.flower19}, new int[]{R.drawable.rock1, R.drawable.rock2, R.drawable.rock3, R.drawable.rock4, R.drawable.rock5, R.drawable.rock6, R.drawable.rock7, R.drawable.rock8, R.drawable.rock9, R.drawable.rock10, R.drawable.rock11, R.drawable.rock12, R.drawable.rock13, R.drawable.rock14, R.drawable.rock15, R.drawable.rock16, R.drawable.rock17, R.drawable.rock18, R.drawable.rock19}, new int[]{R.drawable.butterfly1, R.drawable.butterfly2, R.drawable.butterfly3, R.drawable.butterfly4, R.drawable.butterfly5, R.drawable.butterfly6, R.drawable.butterfly7, R.drawable.butterfly8, R.drawable.butterfly9, R.drawable.butterfly10, R.drawable.butterfly11, R.drawable.butterfly12, R.drawable.butterfly13, R.drawable.butterfly14, R.drawable.butterfly15, R.drawable.butterfly16, R.drawable.butterfly17, R.drawable.butterfly18, R.drawable.butterfly19}, new int[]{R.drawable.fish1, R.drawable.fish2, R.drawable.fish3, R.drawable.fish4, R.drawable.fish5, R.drawable.fish6, R.drawable.fish7, R.drawable.fish8, R.drawable.fish9, R.drawable.fish10, R.drawable.fish11, R.drawable.fish12, R.drawable.fish13, R.drawable.fish14, R.drawable.fish15, R.drawable.fish16, R.drawable.fish17, R.drawable.fish18, R.drawable.fish19}, new int[]{R.drawable.crystal_ball1, R.drawable.crystal_ball2, R.drawable.crystal_ball3, R.drawable.crystal_ball4, R.drawable.crystal_ball5, R.drawable.crystal_ball6, R.drawable.crystal_ball7, R.drawable.crystal_ball8, R.drawable.crystal_ball9, R.drawable.crystal_ball10, R.drawable.crystal_ball11, R.drawable.crystal_ball12, R.drawable.crystal_ball13, R.drawable.crystal_ball14, R.drawable.crystal_ball15, R.drawable.crystal_ball16, R.drawable.crystal_ball17, R.drawable.crystal_ball18, R.drawable.crystal_ball19}, new int[]{R.drawable.ring1, R.drawable.ring2, R.drawable.ring3, R.drawable.ring4, R.drawable.ring5, R.drawable.ring6, R.drawable.ring7, R.drawable.ring8, R.drawable.ring9, R.drawable.ring10, R.drawable.ring11, R.drawable.ring12, R.drawable.ring13, R.drawable.ring14, R.drawable.ring15, R.drawable.ring16, R.drawable.ring17, R.drawable.ring18, R.drawable.ring19}};
        this.RNG = new Random();
        this.SOUND = new int[]{R.raw.win, R.raw.lose, R.raw.timer, R.raw.error};
        this.WIN_SOUND = 0;
        this.LOSE_SOUND = 1;
        this.TIMER_SOUND = 2;
        this.ERROR_SOUND = 3;
        this.increase_step = 1;
        this.currentItem = this.RNG.nextInt(6);
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        if (FindTheSame.this.gameMode == 5) {
                            FindTheSame findTheSame = FindTheSame.this;
                            findTheSame.time = findTheSame.time - 1;
                            FindTheSame.this.setTime();
                            if (FindTheSame.this.time <= 6 && FindTheSame.this.time > 2 && FindTheSame.this.time % 2 == 0) {
                                FindTheSame.this.playSound(2);
                            }
                            if (FindTheSame.this.time <= 0) {
                                FindTheSame.this.setMode(4);
                                return;
                            }
                            return;
                        }
                        return;
                    case 1:
                        FindTheSame.this.lose_window.setVisibility(0);
                        return;
                    case 2:
                        FindTheSame.this.initGame();
                        FindTheSame.this.itemAdapter = new ItemAdapter(FindTheSame.this);
                        FindTheSame.this.gridView.setAdapter((ListAdapter) FindTheSame.this.itemAdapter);
                        if (FindTheSame.this.gameMode != 1 && FindTheSame.this.previousMode != 1) {
                            FindTheSame.this.setMode(5);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        };
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        getWindow().setFlags(128, 128);
        setContentView((int) R.layout.game_layout);
        DomobAdView adview = new DomobAdView(this);
        adview.setOnClickListener(this);
        DomobAdManager.setPublisherId("56OJydfYuMQydTRjJL");
        DomobAdManager.setIsTestMode(false);
        adview.setRequestInterval(35);
        ((FrameLayout) findViewById(R.id.ad_zone)).addView(adview);
        this.ready_window = findViewById(R.id.ready_window);
        this.ready_window.setOnTouchListener(this);
        this.pause_window = findViewById(R.id.pause_window);
        this.pause_window.setOnTouchListener(this);
        this.score_and_time = findViewById(R.id.score_and_time);
        findViewById(R.id.button_tryAgain).setOnClickListener(this);
        findViewById(R.id.button_backToMenu).setOnClickListener(this);
        this.preference = getSharedPreferences("FindTheSame_Preference", 0);
        this.lose_window = findViewById(R.id.lose_window);
        this.player_score = (TextView) findViewById(R.id.player_score);
        this.highest_score = (TextView) findViewById(R.id.highest_score);
        this.timer = new Timer();
        this.score_text = (TextView) findViewById(R.id.score);
        this.time_text = (TextView) findViewById(R.id.time);
        this.complexity = getIntent().getIntExtra(MainMenu.COMPLEXITY, 0);
        if (this.complexity == 3) {
            this.score_and_time.setVisibility(4);
        }
        if (this.complexity == 1) {
            this.time = this.TIME_LEVEL[this.complexity];
            setTime();
        }
        initSounds();
        if (savedInstanceState == null) {
            setMode(0);
            initGame();
            this.score = 0;
            setScore();
        } else {
            Bundle map = savedInstanceState.getBundle("saved_bundle");
            if (map != null) {
                restoreState(map);
            } else {
                setMode(0);
                initGame();
                this.score = 0;
                setScore();
            }
        }
        this.gridView = (GridView) findViewById(R.id.show_window);
        this.itemAdapter = new ItemAdapter(this);
        this.gridView.setAdapter((ListAdapter) this.itemAdapter);
        this.gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (FindTheSame.this.gameMode == 5) {
                    ItemAdapter.ItemHolder holder = (ItemAdapter.ItemHolder) view.getTag();
                    holder.chosen = !holder.chosen;
                    if (holder.chosen) {
                        holder.circle.setVisibility(0);
                        FindTheSame.this.choice[FindTheSame.this.hasChosen] = position;
                        FindTheSame findTheSame = FindTheSame.this;
                        findTheSame.hasChosen = findTheSame.hasChosen + 1;
                        if (FindTheSame.this.hasChosen == 2) {
                            if (FindTheSame.this.correct()) {
                                FindTheSame.this.setMode(3);
                                return;
                            }
                            FindTheSame.this.playSound(3);
                            holder.chosen = false;
                            holder.circle.setVisibility(4);
                            FindTheSame.this.previous.chosen = false;
                            FindTheSame.this.previous.circle.setVisibility(4);
                            FindTheSame.this.hasChosen = 0;
                        } else if (FindTheSame.this.hasChosen == 1) {
                            FindTheSame.this.previous = holder;
                        }
                    } else {
                        holder.circle.setVisibility(4);
                        FindTheSame findTheSame2 = FindTheSame.this;
                        findTheSame2.hasChosen = findTheSame2.hasChosen - 1;
                    }
                }
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.getItem(1);
        if (playSound) {
            item.setIcon(getResources().getDrawable(R.drawable.soundoff));
            item.setTitle(getResources().getText(R.string.str_soundoff));
        } else {
            item.setIcon(getResources().getDrawable(R.drawable.soundon));
            item.setTitle(getResources().getText(R.string.str_soundon));
        }
        return true;
    }

    public void onPause() {
        super.onPause();
        setMode(1);
        MobclickAgent.onPause(this);
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_pause:
                setMode(1);
                break;
            case R.id.menu_sound:
                playSound = !playSound;
                break;
        }
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return false;
        }
        setMode(1);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getText(R.string.str_return));
        builder.setMessage(getResources().getString(R.string.str_return_info));
        builder.setPositiveButton(getResources().getText(R.string.str_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (FindTheSame.this.complexity != 3) {
                    FindTheSame.this.checkScore();
                }
                FindTheSame.this.finish();
            }
        });
        builder.setNegativeButton(getResources().getText(R.string.str_no), (DialogInterface.OnClickListener) null);
        builder.show();
        return false;
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putBundle("saved_bundle", saveState());
    }

    private Bundle saveState() {
        Bundle map = new Bundle();
        map.putIntArray("choice", this.choice);
        map.putInt("currentItem", this.currentItem);
        map.putInt("time", this.time);
        map.putInt("score", this.score);
        map.putInt("position1", this.position1);
        map.putInt("position2", this.position2);
        map.putInt("sameItem", this.same_item);
        map.putInt("gameMode", this.gameMode == 5 ? 1 : this.gameMode);
        return map;
    }

    private void restoreState(Bundle map) {
        this.choice = map.getIntArray("choice");
        this.currentItem = map.getInt("currentItem");
        this.time = map.getInt("time");
        this.score = map.getInt("score");
        this.position1 = map.getInt("position1");
        this.position2 = map.getInt("position2");
        this.same_item = map.getInt("sameItem");
        this.gameMode = map.getInt("gameMode");
        setTime();
        setScore();
        this.second = null;
        this.first = null;
        this.hasChosen = 0;
        setMode(this.gameMode);
    }

    private void initSounds() {
        this.soundPool = new SoundPool(4, 3, 100);
        this.soundPoolMap = new HashMap<>();
        for (int i = 0; i < this.SOUND.length; i++) {
            this.soundPoolMap.put(Integer.valueOf(i), Integer.valueOf(this.soundPool.load(this, this.SOUND[i], 1)));
        }
    }

    /* access modifiers changed from: private */
    public void playSound(int sound) {
        if (playSound) {
            AudioManager mgr = (AudioManager) getSystemService(DomobAdManager.ACTION_AUDIO);
            float volume = ((float) mgr.getStreamVolume(3)) / ((float) mgr.getStreamMaxVolume(3));
            this.soundPool.play(this.soundPoolMap.get(Integer.valueOf(sound)).intValue(), volume, volume, 1, 0, 1.0f);
        }
    }

    /* access modifiers changed from: private */
    public void initGame() {
        this.currentItem = (this.currentItem + 1) % 6;
        if (this.complexity != 1) {
            this.time = this.TIME_LEVEL[this.complexity];
            setTime();
        }
        this.second = null;
        this.first = null;
        this.increase_step = 1;
        this.hasChosen = 0;
        this.choice = new int[2];
        this.same_item = this.RNG.nextInt(19);
        this.position1 = this.RNG.nextInt(19 + 1);
        while (true) {
            this.position2 = this.RNG.nextInt(19 + 1);
            if (this.position2 == this.position1 || ((this.complexity == 2 || this.complexity == 1) && adjacent(this.position1, this.position2))) {
            }
        }
        Log.i("FindTheSame", "same_item:" + this.same_item + " p1:" + this.position1 + " p2:" + this.position2);
    }

    private boolean adjacent(int p1, int p2) {
        int column1 = p1 % 4;
        int row1 = p1 / 4;
        int column2 = p2 % 4;
        int row2 = p2 / 4;
        return (column1 == column2 && Math.abs(row1 - row2) == 1) || (row1 == row2 && Math.abs(column1 - column2) == 1);
    }

    /* access modifiers changed from: private */
    public boolean correct() {
        return (this.choice[0] == this.position1 && this.choice[1] == this.position2) || (this.choice[0] == this.position2 && this.choice[1] == this.position1);
    }

    private int getHighestScore() {
        return this.preference.getInt(this.HIGHEST_SCORE[this.complexity], 0);
    }

    private void updateHighestScore() {
        SharedPreferences.Editor editor = this.preference.edit();
        editor.putInt(this.HIGHEST_SCORE[this.complexity], this.score);
        editor.commit();
    }

    private void setScore() {
        this.score_text.setText(new StringBuilder().append(this.score).toString());
    }

    /* access modifiers changed from: private */
    public void setTime() {
        int minute = this.time / 60;
        int second2 = this.time % 60;
        if (second2 > 9) {
            this.time_text.setText(String.valueOf(minute) + ":" + second2);
        } else {
            this.time_text.setText(String.valueOf(minute) + ":0" + second2);
        }
    }

    /* access modifiers changed from: private */
    public void setMode(int newMode) {
        int oldMode = this.gameMode;
        this.previousMode = oldMode;
        this.gameMode = newMode;
        if (this.gameMode == 3) {
            this.timer.cancel();
            playSound(0);
            if (this.complexity == 1) {
                this.score++;
                this.time += 15;
                setTime();
            } else if (this.complexity != 3) {
                this.score += this.time * this.RATIO_LEVEL[this.complexity];
            }
            setScore();
            this.handler.sendMessageDelayed(this.handler.obtainMessage(2), 1000);
        } else if (newMode == 5 && this.complexity != 3) {
            this.timer = new Timer();
            this.timer.schedule(new TimerTask() {
                public void run() {
                    if (FindTheSame.this.gameMode == 5) {
                        FindTheSame.this.handler.sendMessage(FindTheSame.this.handler.obtainMessage(0));
                    }
                }
            }, 1000, 1000);
        } else if (newMode == 4) {
            showAnswer();
            this.timer.cancel();
            playSound(1);
            checkScore();
            this.handler.sendMessageDelayed(this.handler.obtainMessage(1), 3000);
        } else if (newMode == 0) {
            this.ready_window.setVisibility(0);
        } else if (newMode == 1 && oldMode != 4) {
            this.timer.cancel();
            this.pause_window.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void checkScore() {
        int highestScore = getHighestScore();
        if (this.score > highestScore) {
            highestScore = this.score;
            updateHighestScore();
        }
        this.player_score.setText(new StringBuilder().append(this.score).toString());
        this.highest_score.setText(new StringBuilder().append(highestScore).toString());
    }

    private void showAnswer() {
        if (this.first != null && this.second != null) {
            this.first.circle.setVisibility(0);
            this.second.circle.setVisibility(0);
        }
    }

    private class ItemAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public ItemAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return 20;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ItemHolder holder;
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.item, (ViewGroup) null);
                holder = new ItemHolder(this, null);
                holder.item = (ImageView) convertView.findViewById(R.id.item);
                holder.circle = (ImageView) convertView.findViewById(R.id.circle);
                convertView.setTag(holder);
            } else {
                holder = (ItemHolder) convertView.getTag();
            }
            if (!holder.set) {
                holder.set = true;
                if (position == FindTheSame.this.position1 || position == FindTheSame.this.position2) {
                    holder.item.setImageResource(FindTheSame.this.ITEMS[FindTheSame.this.currentItem][FindTheSame.this.same_item]);
                    if (FindTheSame.this.first == null) {
                        FindTheSame.this.first = holder;
                    } else if (FindTheSame.this.second == null) {
                        FindTheSame.this.second = holder;
                    }
                } else {
                    holder.item.setImageResource(FindTheSame.this.ITEMS[FindTheSame.this.currentItem][(FindTheSame.this.same_item + FindTheSame.this.increase_step) % 19]);
                    FindTheSame findTheSame = FindTheSame.this;
                    findTheSame.increase_step = findTheSame.increase_step + 1;
                }
            }
            return convertView;
        }

        private class ItemHolder {
            boolean chosen;
            ImageView circle;
            ImageView item;
            boolean set;

            private ItemHolder() {
                this.set = false;
                this.chosen = false;
            }

            /* synthetic */ ItemHolder(ItemAdapter itemAdapter, ItemHolder itemHolder) {
                this();
            }
        }
    }

    public void onClick(View v) {
        if (v instanceof DomobAdView) {
            setMode(1);
            return;
        }
        switch (v.getId()) {
            case R.id.button_tryAgain:
                this.score = 0;
                setScore();
                if (this.complexity == 1) {
                    this.time = this.TIME_LEVEL[this.complexity];
                    setTime();
                }
                initGame();
                this.itemAdapter = new ItemAdapter(this);
                this.gridView.setAdapter((ListAdapter) this.itemAdapter);
                this.lose_window.setVisibility(4);
                setMode(5);
                return;
            case R.id.button_backToMenu:
                finish();
                return;
            default:
                return;
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() != 0) {
            return true;
        }
        switch (v.getId()) {
            case R.id.ready_window:
                this.ready_window.setVisibility(4);
                setMode(5);
                return true;
            case R.id.pause_window:
                this.pause_window.setVisibility(4);
                setMode(5);
                return true;
            default:
                return true;
        }
    }
}
