package FindTheSame.Chinese;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import cn.domob.android.ads.DomobAdManager;

public class MainMenu extends Activity implements View.OnClickListener {
    public static final String COMPLEXITY = "complexity_level";
    public static final int COMPLEXITY_CHALLENGE = 1;
    public static final int COMPLEXITY_EASY = 0;
    public static final int COMPLEXITY_HARD = 2;
    public static final int COMPLEXITY_TRY = 3;
    private final String TAG = "FindTheSame";
    private View complexity_menu;
    private View help_window;
    private View main_menu;
    private int sound = 0;
    private SoundPool soundPool;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.mainmenu);
        this.soundPool = new SoundPool(4, 3, 100);
        this.sound = this.soundPool.load(this, R.raw.menu_btn, 1);
        this.main_menu = findViewById(R.id.main_menu);
        this.complexity_menu = findViewById(R.id.complexity_menu);
        this.help_window = findViewById(R.id.help_window);
        findViewById(R.id.menu_about).setOnClickListener(this);
        findViewById(R.id.menu_exit).setOnClickListener(this);
        findViewById(R.id.menu_help).setOnClickListener(this);
        findViewById(R.id.menu_play).setOnClickListener(this);
        findViewById(R.id.menu_easy).setOnClickListener(this);
        findViewById(R.id.menu_challenge).setOnClickListener(this);
        findViewById(R.id.menu_hard).setOnClickListener(this);
        findViewById(R.id.menu_try).setOnClickListener(this);
        findViewById(R.id.back_arrow).setOnClickListener(this);
        findViewById(R.id.back_arrow_help).setOnClickListener(this);
    }

    public void onResume() {
        super.onResume();
    }

    public void onClick(View v) {
        playSound();
        switch (v.getId()) {
            case R.id.menu_play /*2131165203*/:
                this.main_menu.setVisibility(4);
                this.complexity_menu.setVisibility(0);
                return;
            case R.id.menu_help /*2131165204*/:
                this.main_menu.setVisibility(4);
                this.help_window.setVisibility(0);
                return;
            case R.id.menu_about /*2131165205*/:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getString(R.string.str_about_title));
                builder.setMessage(getString(R.string.str_about));
                builder.setPositiveButton(getString(R.string.str_confirm), (DialogInterface.OnClickListener) null);
                builder.show();
                return;
            case R.id.menu_exit /*2131165206*/:
                finish();
                return;
            case R.id.complexity_menu /*2131165207*/:
            case R.id.help_window /*2131165213*/:
            case R.id.imageView1 /*2131165214*/:
            case R.id.textView1 /*2131165215*/:
            default:
                return;
            case R.id.menu_try /*2131165208*/:
                Intent intent = new Intent(this, FindTheSame.class);
                intent.putExtra(COMPLEXITY, 3);
                startActivity(intent);
                return;
            case R.id.menu_easy /*2131165209*/:
                Intent intent2 = new Intent(this, FindTheSame.class);
                intent2.putExtra(COMPLEXITY, 0);
                startActivity(intent2);
                return;
            case R.id.menu_hard /*2131165210*/:
                Intent intent3 = new Intent(this, FindTheSame.class);
                intent3.putExtra(COMPLEXITY, 2);
                startActivity(intent3);
                return;
            case R.id.menu_challenge /*2131165211*/:
                Intent intent4 = new Intent(this, FindTheSame.class);
                intent4.putExtra(COMPLEXITY, 1);
                startActivity(intent4);
                return;
            case R.id.back_arrow /*2131165212*/:
                this.main_menu.setVisibility(0);
                this.complexity_menu.setVisibility(4);
                this.help_window.setVisibility(4);
                return;
            case R.id.back_arrow_help /*2131165216*/:
                this.main_menu.setVisibility(0);
                this.help_window.setVisibility(4);
                return;
        }
    }

    private void playSound() {
        if (FindTheSame.playSound) {
            AudioManager mgr = (AudioManager) getSystemService(DomobAdManager.ACTION_AUDIO);
            float volume = ((float) mgr.getStreamVolume(3)) / ((float) mgr.getStreamMaxVolume(3));
            this.soundPool.play(this.sound, volume, volume, 1, 0, 1.0f);
        }
    }
}
