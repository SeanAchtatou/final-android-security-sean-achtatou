package cn.domob.android.download;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import cn.domob.android.ads.Constants;
import cn.domob.android.ads.DomobActivity;
import java.util.Hashtable;

public class AppExchangeDownloader {
    public static final String Act_Type = "actType";
    public static final String App_Download_Path = "downloadPath";
    public static final String App_Id = "appId";
    public static final String App_Name = "appName";
    public static final String App_Notify_Id = "notifyId";
    public static Hashtable<String, AppExchangeDownloader> Download_Map = new Hashtable<>();
    public static final String Type_Cancel = "typeCancel";
    public static final String Type_Install = "typeInstall";
    /* access modifiers changed from: private */
    public static Context a = null;
    private static int h = 0;
    private static Hashtable<String, Integer> i = new Hashtable<>();
    /* access modifiers changed from: private */
    public Notification b = null;
    /* access modifiers changed from: private */
    public NotificationManager c = null;
    /* access modifiers changed from: private */
    public int d = 0;
    /* access modifiers changed from: private */
    public int e = 0;
    /* access modifiers changed from: private */
    public AppExchangeDownloaderListener f = null;
    /* access modifiers changed from: private */
    public String g = "";
    /* access modifiers changed from: private */
    public int j = 0;
    private String k = "";
    /* access modifiers changed from: private */
    public String l = "";
    /* access modifiers changed from: private */
    public String m = "";
    /* access modifiers changed from: private */
    public PendingIntent n;
    /* access modifiers changed from: private */
    public Handler o = new a(this);
    private DownloadTask p;

    public static void downloadApp(String downloadUrl, String appName, Context context, AppExchangeDownloaderListener listener) {
        String substring = downloadUrl.substring(downloadUrl.lastIndexOf("/"));
        if (Download_Map.containsKey(substring)) {
            listener.onDownloadFailed(2, "当前应用已在下载");
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "app" + appName + " is downloading");
            }
        } else if (Download_Map.size() == 3) {
            listener.onDownloadFailed(3, "最大下载数为3个");
            Log.e("download", "max");
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "max download is 3");
            }
        } else {
            AppExchangeDownloader appExchangeDownloader = new AppExchangeDownloader(downloadUrl, appName, substring, context);
            appExchangeDownloader.f = listener;
            appExchangeDownloader.b = new Notification();
            appExchangeDownloader.b.icon = 17301633;
            appExchangeDownloader.b.tickerText = String.valueOf(appExchangeDownloader.l) + "正在下载，请稍候...";
            Context context2 = a;
            int i2 = appExchangeDownloader.d;
            Intent intent = new Intent();
            intent.setClass(a, DomobActivity.class);
            intent.putExtra(App_Name, appExchangeDownloader.l);
            intent.putExtra(App_Id, appExchangeDownloader.m);
            intent.putExtra(Act_Type, Type_Cancel);
            appExchangeDownloader.n = PendingIntent.getActivity(context2, i2, intent, 134217728);
            appExchangeDownloader.b.setLatestEventInfo(a, String.valueOf(appExchangeDownloader.l) + "正在下载", "已下载%0", appExchangeDownloader.n);
            appExchangeDownloader.c = (NotificationManager) a.getSystemService("notification");
            appExchangeDownloader.c.notify(appExchangeDownloader.d, appExchangeDownloader.b);
            new b(a, appExchangeDownloader.m, appExchangeDownloader.k, new b()).start();
            listener.onStartDownload();
        }
    }

    private AppExchangeDownloader(String downloadUrl, String appName, String fileName, Context context) {
        a = context.getApplicationContext();
        this.k = downloadUrl;
        this.l = appName;
        this.m = fileName;
        if (!i.containsKey(fileName)) {
            h++;
            i.put(fileName, Integer.valueOf(h));
            this.d = h;
        } else {
            this.d = i.get(fileName).intValue();
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, String.valueOf(appName) + " notification_id is " + this.d);
        }
    }

    static Intent a(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse("file://" + str), "application/vnd.android.package-archive");
        return intent;
    }

    class b {
        b() {
        }

        public void a(String str) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.i(Constants.LOG, String.valueOf(AppExchangeDownloader.this.l) + " already exists in " + str);
            }
            AppExchangeDownloader.this.g = str;
            AppExchangeDownloader.b(AppExchangeDownloader.this, AppExchangeDownloader.this.g);
        }

        public void c(String str) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.i(Constants.LOG, String.valueOf(AppExchangeDownloader.this.l) + " is download but not finished in " + str);
            }
            AppExchangeDownloader.this.g = str;
            AppExchangeDownloader.b(AppExchangeDownloader.this, AppExchangeDownloader.this.g);
        }

        public void b(String str) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.i(Constants.LOG, String.valueOf(AppExchangeDownloader.this.l) + " is  not download,it will download in " + str);
            }
            AppExchangeDownloader.this.g = str;
            AppExchangeDownloader.b(AppExchangeDownloader.this, AppExchangeDownloader.this.g);
        }

        public void a(long j, long j2) {
            Log.e(Constants.LOG, String.valueOf(AppExchangeDownloader.this.l) + "not enough size sdsize=" + j + " romsize=" + j2);
            if (AppExchangeDownloader.this.f != null) {
                AppExchangeDownloader.this.f.onDownloadFailed(1, "空间不足");
            }
        }

        public void a() {
            Log.e(Constants.LOG, String.valueOf(AppExchangeDownloader.this.l) + "rom can't chmod");
            if (AppExchangeDownloader.this.f != null) {
                AppExchangeDownloader.this.f.onDownloadFailed(5, "请插入sd卡");
            }
        }

        public void d(String str) {
            Log.e(Constants.LOG, String.valueOf(AppExchangeDownloader.this.l) + "无法连接的下载地址");
            if (AppExchangeDownloader.this.f != null) {
                AppExchangeDownloader.this.f.onDownloadFailed(5, String.valueOf(AppExchangeDownloader.this.l) + str);
            }
        }
    }

    static /* synthetic */ void b(AppExchangeDownloader appExchangeDownloader, String str) {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "begin download in " + str);
        }
        appExchangeDownloader.p = new DownloadTask(appExchangeDownloader.k, str, new a());
        appExchangeDownloader.p.start();
        Download_Map.put(appExchangeDownloader.m, appExchangeDownloader);
    }

    class a {
        a() {
        }

        public void a(int i) {
            AppExchangeDownloader.this.o.sendEmptyMessage(i);
        }

        public void a() {
            AppExchangeDownloader.this.stopDownload();
            PendingIntent activity = PendingIntent.getActivity(AppExchangeDownloader.a, AppExchangeDownloader.this.d, new Intent(), 134217728);
            AppExchangeDownloader.this.b.icon = 17301624;
            AppExchangeDownloader.this.b.tickerText = String.valueOf(AppExchangeDownloader.this.l) + "下载失败";
            AppExchangeDownloader.this.b.setLatestEventInfo(AppExchangeDownloader.a, String.valueOf(AppExchangeDownloader.this.l) + "下载失败", "", activity);
            AppExchangeDownloader.this.b.flags = 16;
            AppExchangeDownloader.this.c.notify(AppExchangeDownloader.this.d, AppExchangeDownloader.this.b);
        }
    }

    public static Intent appIsDownload(Context context, String downloadUrl, String appName) {
        b bVar = new b(context, downloadUrl.substring(downloadUrl.lastIndexOf("/")), downloadUrl, null);
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, String.valueOf(appName) + " is  exsist");
        }
        String a2 = bVar.a();
        if (a2 == null) {
            return null;
        }
        return a(a2);
    }

    public void stopDownload() {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "stop download  cancel notify " + this.d);
        }
        if (this.p != null) {
            this.p.a();
        }
        this.c.cancel(this.d);
        Download_Map.remove(this.m);
    }
}
