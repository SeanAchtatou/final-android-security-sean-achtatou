package cn.domob.android.download;

import android.os.Handler;

class a extends Handler {
    private /* synthetic */ AppExchangeDownloader a;

    a(AppExchangeDownloader appExchangeDownloader) {
        this.a = appExchangeDownloader;
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: cn.domob.android.download.AppExchangeDownloader.a(cn.domob.android.download.AppExchangeDownloader, java.lang.String):void in method: cn.domob.android.download.a.handleMessage(android.os.Message):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:59)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: cn.domob.android.download.AppExchangeDownloader.a(cn.domob.android.download.AppExchangeDownloader, java.lang.String):void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:528)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 6 more
        */
    public final void handleMessage(android.os.Message r1) {
        /*
            r7 = this;
            r6 = 100
            r5 = 0
            super.handleMessage(r8)
            int r0 = r8.what
            cn.domob.android.download.AppExchangeDownloader r1 = r7.a
            int r1 = r1.j
            int r0 = r0 - r1
            r1 = 5
            if (r0 >= r1) goto L_0x0017
            int r0 = r8.what
            if (r0 == r6) goto L_0x0017
        L_0x0016:
            return
        L_0x0017:
            cn.domob.android.download.AppExchangeDownloader r0 = r7.a
            int r1 = r8.what
            r0.j = r1
            cn.domob.android.download.AppExchangeDownloader r0 = r7.a
            int r1 = r8.what
            r0.e = r1
            cn.domob.android.download.AppExchangeDownloader r0 = r7.a
            android.app.Notification r0 = r0.b
            android.content.Context r1 = cn.domob.android.download.AppExchangeDownloader.a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            cn.domob.android.download.AppExchangeDownloader r3 = r7.a
            java.lang.String r3 = r3.l
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r2.<init>(r3)
            java.lang.String r3 = "正在下载"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "已下载%"
            r3.<init>(r4)
            int r4 = r8.what
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            cn.domob.android.download.AppExchangeDownloader r4 = r7.a
            android.app.PendingIntent r4 = r4.n
            r0.setLatestEventInfo(r1, r2, r3, r4)
            cn.domob.android.download.AppExchangeDownloader r0 = r7.a
            android.app.NotificationManager r0 = r0.c
            cn.domob.android.download.AppExchangeDownloader r1 = r7.a
            int r1 = r1.d
            cn.domob.android.download.AppExchangeDownloader r2 = r7.a
            android.app.Notification r2 = r2.b
            r0.notify(r1, r2)
            cn.domob.android.download.AppExchangeDownloader r0 = r7.a
            int r0 = r0.e
            if (r0 != r6) goto L_0x0016
            java.lang.String r0 = "DomobSDK"
            r1 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r1)
            if (r0 == 0) goto L_0x00a6
            java.lang.String r0 = "DomobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            cn.domob.android.download.AppExchangeDownloader r2 = r7.a
            java.lang.String r2 = r2.l
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r1.<init>(r2)
            java.lang.String r2 = " download success"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.d(r0, r1)
        L_0x00a6:
            java.io.File r0 = new java.io.File
            cn.domob.android.download.AppExchangeDownloader r1 = r7.a
            java.lang.String r1 = r1.g
            r0.<init>(r1)
            cn.domob.android.download.AppExchangeDownloader r1 = r7.a
            cn.domob.android.download.AppExchangeDownloader r2 = r7.a
            java.lang.String r2 = r2.g
            cn.domob.android.download.AppExchangeDownloader r3 = r7.a
            java.lang.String r3 = r3.g
            int r3 = r3.length()
            java.lang.String r4 = ".temp"
            int r4 = r4.length()
            int r3 = r3 - r4
            java.lang.String r2 = r2.substring(r5, r3)
            r1.g = r2
            java.io.File r1 = new java.io.File
            cn.domob.android.download.AppExchangeDownloader r2 = r7.a
            java.lang.String r2 = r2.g
            r1.<init>(r2)
            r0.renameTo(r1)
            java.util.Hashtable<java.lang.String, cn.domob.android.download.AppExchangeDownloader> r0 = cn.domob.android.download.AppExchangeDownloader.Download_Map
            cn.domob.android.download.AppExchangeDownloader r1 = r7.a
            java.lang.String r1 = r1.m
            r0.remove(r1)
            cn.domob.android.download.AppExchangeDownloader r0 = r7.a
            cn.domob.android.download.AppExchangeDownloaderListener r0 = r0.f
            if (r0 == 0) goto L_0x0101
            cn.domob.android.download.AppExchangeDownloader r0 = r7.a
            cn.domob.android.download.AppExchangeDownloaderListener r0 = r0.f
            cn.domob.android.download.AppExchangeDownloader r1 = r7.a
            java.lang.String r1 = r1.g
            r0.onDownloadSuccess(r1)
        L_0x0101:
            cn.domob.android.download.AppExchangeDownloader r0 = r7.a
            r0.e = r5
            cn.domob.android.download.AppExchangeDownloader r0 = r7.a
            android.app.Notification r0 = r0.b
            r1 = 17301634(0x1080082, float:2.497962E-38)
            r0.icon = r1
            cn.domob.android.download.AppExchangeDownloader r0 = r7.a
            android.app.Notification r0 = r0.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            cn.domob.android.download.AppExchangeDownloader r2 = r7.a
            java.lang.String r2 = r2.l
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r1.<init>(r2)
            java.lang.String r2 = "下载完毕"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.tickerText = r1
            cn.domob.android.download.AppExchangeDownloader r0 = r7.a
            android.app.Notification r0 = r0.b
            r1 = 16
            r0.flags = r1
            cn.domob.android.download.AppExchangeDownloader r0 = r7.a
            java.lang.String r0 = r0.g
            android.content.Intent r0 = cn.domob.android.download.AppExchangeDownloader.a(r0)
            cn.domob.android.download.AppExchangeDownloader r1 = r7.a
            android.content.Context r2 = cn.domob.android.download.AppExchangeDownloader.a
            cn.domob.android.download.AppExchangeDownloader r3 = r7.a
            int r3 = r3.d
            r4 = 134217728(0x8000000, float:3.85186E-34)
            android.app.PendingIntent r0 = android.app.PendingIntent.getActivity(r2, r3, r0, r4)
            r1.n = r0
            cn.domob.android.download.AppExchangeDownloader r0 = r7.a
            android.app.Notification r0 = r0.b
            android.content.Context r1 = cn.domob.android.download.AppExchangeDownloader.a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            cn.domob.android.download.AppExchangeDownloader r3 = r7.a
            java.lang.String r3 = r3.l
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r2.<init>(r3)
            java.lang.String r3 = "下载完毕"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.String r3 = ""
            cn.domob.android.download.AppExchangeDownloader r4 = r7.a
            android.app.PendingIntent r4 = r4.n
            r0.setLatestEventInfo(r1, r2, r3, r4)
            cn.domob.android.download.AppExchangeDownloader r0 = r7.a
            android.app.NotificationManager r0 = r0.c
            cn.domob.android.download.AppExchangeDownloader r1 = r7.a
            int r1 = r1.d
            cn.domob.android.download.AppExchangeDownloader r2 = r7.a
            android.app.Notification r2 = r2.b
            r0.notify(r1, r2)
            cn.domob.android.download.AppExchangeDownloader r0 = r7.a
            java.lang.String r0 = r0.g
            android.content.Intent r0 = cn.domob.android.download.AppExchangeDownloader.a(r0)
            r1 = 268435456(0x10000000, float:2.5243549E-29)
            r0.addFlags(r1)
            android.content.Context r1 = cn.domob.android.download.AppExchangeDownloader.a
            r1.startActivity(r0)
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.download.a.handleMessage(android.os.Message):void");
    }
}
