package cn.domob.android.download;

import android.util.Log;
import cn.domob.android.ads.Constants;
import cn.domob.android.download.AppExchangeDownloader;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;

final class DownloadTask extends Thread {
    private int a = 0;
    private int b;
    private int c;
    private int d = 1;
    private int e = 0;
    private AppExchangeDownloader.a f = null;
    private boolean g;
    private String h;
    private String i;
    private TaskState j = new TaskState(this);

    public DownloadTask(String urlStr, String fileName, AppExchangeDownloader.a listener) {
        this.h = urlStr;
        this.i = fileName;
        this.f = listener;
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "build DownloadTask url=" + urlStr + "fileName=" + fileName);
        }
    }

    public final void run() {
        FileDownloadThread fileDownloadThread;
        FileDownloadThread[] fileDownloadThreadArr = new FileDownloadThread[this.d];
        try {
            URL url = new URL(this.h);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode < 200 || responseCode >= 300) {
                this.f.a();
                return;
            }
            this.a = httpURLConnection.getContentLength();
            if (this.a <= 0) {
                this.f.a();
                return;
            }
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "fileSize:" + this.a + " downloadSizeMore:" + this.c);
            }
            this.b = this.a / this.d;
            this.c = this.a % this.d;
            File file = new File(this.i);
            for (int i2 = 0; i2 < this.d; i2++) {
                if (i2 != this.d - 1) {
                    fileDownloadThread = new FileDownloadThread(url, file, this.b * i2, ((i2 + 1) * this.b) - 1, this.j, this.f);
                } else {
                    fileDownloadThread = new FileDownloadThread(url, file, this.b * i2, (((i2 + 1) * this.b) - 1) + this.c, this.j, this.f);
                }
                fileDownloadThread.setName("Thread" + i2);
                fileDownloadThread.start();
                fileDownloadThreadArr[i2] = fileDownloadThread;
            }
            this.g = false;
            while (!this.g && !this.j.isStop) {
                this.e = this.c;
                this.g = true;
                for (int i3 = 0; i3 < fileDownloadThreadArr.length; i3++) {
                    this.e += fileDownloadThreadArr[i3].getDownloadSize();
                    if (!fileDownloadThreadArr[i3].isFinished()) {
                        this.g = false;
                    }
                }
                if (this.f != null) {
                    this.f.a(Double.valueOf((((double) this.e) / ((double) this.a)) * 100.0d).intValue());
                }
                sleep(1000);
            }
        } catch (Exception e2) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "download error by download task");
            }
            this.f.a();
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.j.isStop = true;
    }

    public class TaskState {
        protected boolean isStop = false;

        protected TaskState(DownloadTask downloadTask) {
        }
    }
}
