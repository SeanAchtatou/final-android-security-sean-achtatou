package cn.domob.android.download;

import android.util.Log;
import cn.domob.android.ads.Constants;
import cn.domob.android.download.AppExchangeDownloader;
import cn.domob.android.download.DownloadTask;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;

public class FileDownloadThread extends Thread {
    private URL a;
    private File b;
    private int c;
    private int d;
    private int e;
    private boolean f = false;
    private int g = 0;
    private DownloadTask.TaskState h;
    private AppExchangeDownloader.a i;

    FileDownloadThread(URL url, File file, int startPosition, int endPosition, DownloadTask.TaskState taskState, AppExchangeDownloader.a downloadListener) {
        this.a = url;
        this.b = file;
        this.c = startPosition;
        this.e = startPosition;
        this.d = endPosition;
        this.h = taskState;
        this.i = downloadListener;
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "download st:" + startPosition + "ed:" + endPosition);
        }
    }

    public void run() {
        byte[] bArr = new byte[10240];
        try {
            URLConnection openConnection = this.a.openConnection();
            openConnection.setConnectTimeout(40000);
            openConnection.setReadTimeout(60000);
            openConnection.setAllowUserInteraction(true);
            openConnection.setRequestProperty("Range", "bytes=" + this.c + "-" + this.d);
            RandomAccessFile randomAccessFile = new RandomAccessFile(this.b, "rw");
            if (this.b.getAbsoluteFile().toString().startsWith("/data/data/")) {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "download in rom change chmod " + this.b.getAbsolutePath());
                }
                Runtime.getRuntime().exec("chmod 777 " + this.b.getAbsolutePath());
            }
            randomAccessFile.seek((long) this.c);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(openConnection.getInputStream());
            while (this.e < this.d) {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d("DomobSDKdwnloadmessage", "downloading");
                }
                if (!this.h.isStop) {
                    int read = bufferedInputStream.read(bArr, 0, 10240);
                    if (read == -1) {
                        break;
                    }
                    randomAccessFile.write(bArr, 0, read);
                    this.e += read;
                    if (this.e > this.d) {
                        this.g = (read - (this.e - this.d)) + 1 + this.g;
                    } else {
                        this.g = read + this.g;
                    }
                    try {
                        Thread.sleep(30);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                        Log.e(Constants.LOG, "download Interrupt error:" + e2.getMessage());
                        this.i.a();
                    }
                } else {
                    return;
                }
            }
            this.f = true;
            bufferedInputStream.close();
            randomAccessFile.close();
        } catch (SocketTimeoutException e3) {
            Log.e(Constants.LOG, "download SocketTimeoutException ");
            this.i.a();
        } catch (IOException e4) {
            Log.e(Constants.LOG, "download IOException " + e4.getMessage());
            this.i.a();
        } catch (Exception e5) {
            Log.e(Constants.LOG, "download error " + e5.getMessage());
            this.i.a();
            e5.printStackTrace();
        }
    }

    public boolean isFinished() {
        return this.f;
    }

    public int getDownloadSize() {
        return this.g;
    }
}
