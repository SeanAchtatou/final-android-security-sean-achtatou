package cn.domob.android.ads;

import android.util.Log;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

final class q extends d {
    private HttpURLConnection k;

    protected q(String str, String str2, String str3, String str4, a aVar, int i, Map<String, String> map, String str5) {
        super(str2, str3, str4, aVar, i, null, str5);
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "connect url=" + str);
        }
        try {
            this.a = new URL(str);
        } catch (Exception e) {
            this.a = null;
            e.printStackTrace();
        }
        this.k = null;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0080  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a() {
        /*
            r7 = this;
            r6 = 0
            r5 = 1
            java.lang.String r0 = "DomobSDK"
            r1 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r1)
            if (r0 == 0) goto L_0x0012
            java.lang.String r0 = "DomobSDK"
            java.lang.String r1 = "initConn"
            android.util.Log.d(r0, r1)
        L_0x0012:
            java.net.URL r0 = r7.a
            if (r0 == 0) goto L_0x012c
            r0 = 1
            java.net.HttpURLConnection.setFollowRedirects(r0)     // Catch:{ Exception -> 0x006b }
            r7.f()     // Catch:{ Exception -> 0x006b }
            java.net.Proxy r0 = r7.b     // Catch:{ Exception -> 0x006b }
            if (r0 == 0) goto L_0x0089
            java.net.URL r0 = r7.a     // Catch:{ Exception -> 0x006b }
            java.net.Proxy r1 = r7.b     // Catch:{ Exception -> 0x006b }
            java.net.URLConnection r0 = r0.openConnection(r1)     // Catch:{ Exception -> 0x006b }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x006b }
            r7.k = r0     // Catch:{ Exception -> 0x006b }
        L_0x002d:
            java.net.HttpURLConnection r0 = r7.k     // Catch:{ Exception -> 0x006b }
            if (r0 == 0) goto L_0x00d9
            java.net.HttpURLConnection r0 = r7.k     // Catch:{ Exception -> 0x006b }
            int r1 = r7.e     // Catch:{ Exception -> 0x006b }
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x006b }
            java.net.HttpURLConnection r0 = r7.k     // Catch:{ Exception -> 0x006b }
            int r1 = r7.e     // Catch:{ Exception -> 0x006b }
            r0.setReadTimeout(r1)     // Catch:{ Exception -> 0x006b }
            java.util.Map r0 = r7.f     // Catch:{ Exception -> 0x006b }
            if (r0 == 0) goto L_0x0094
            java.util.Map r0 = r7.f     // Catch:{ Exception -> 0x006b }
            java.util.Set r0 = r0.keySet()     // Catch:{ Exception -> 0x006b }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ Exception -> 0x006b }
        L_0x004d:
            boolean r0 = r2.hasNext()     // Catch:{ Exception -> 0x006b }
            if (r0 == 0) goto L_0x0094
            java.lang.Object r0 = r2.next()     // Catch:{ Exception -> 0x006b }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x006b }
            if (r0 == 0) goto L_0x004d
            java.util.Map r1 = r7.f     // Catch:{ Exception -> 0x006b }
            java.lang.Object r1 = r1.get(r0)     // Catch:{ Exception -> 0x006b }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x006b }
            if (r1 == 0) goto L_0x004d
            java.net.HttpURLConnection r3 = r7.k     // Catch:{ Exception -> 0x006b }
            r3.addRequestProperty(r0, r1)     // Catch:{ Exception -> 0x006b }
            goto L_0x004d
        L_0x006b:
            r0 = move-exception
            java.lang.String r1 = "DomobSDK"
            java.lang.String r2 = "Error happened in connection."
            android.util.Log.e(r1, r2)
            r0.printStackTrace()
        L_0x0076:
            r0 = r6
        L_0x0077:
            if (r0 != 0) goto L_0x007c
            r1 = 0
            r7.h = r1
        L_0x007c:
            cn.domob.android.ads.a r1 = r7.c
            if (r1 == 0) goto L_0x0085
            cn.domob.android.ads.a r1 = r7.c
            r1.a(r7)
        L_0x0085:
            r7.f()
        L_0x0088:
            return r0
        L_0x0089:
            java.net.URL r0 = r7.a     // Catch:{ Exception -> 0x006b }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x006b }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x006b }
            r7.k = r0     // Catch:{ Exception -> 0x006b }
            goto L_0x002d
        L_0x0094:
            java.lang.String r0 = r7.g     // Catch:{ Exception -> 0x006b }
            if (r0 == 0) goto L_0x0121
            java.net.HttpURLConnection r0 = r7.k     // Catch:{ Exception -> 0x006b }
            java.lang.String r1 = "POST"
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x006b }
            java.net.HttpURLConnection r0 = r7.k     // Catch:{ Exception -> 0x006b }
            r1 = 1
            r0.setDoOutput(r1)     // Catch:{ Exception -> 0x006b }
            java.net.HttpURLConnection r0 = r7.k     // Catch:{ Exception -> 0x006b }
            java.lang.String r1 = "Content-Type"
            java.lang.String r2 = r7.d     // Catch:{ Exception -> 0x006b }
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x006b }
            java.net.HttpURLConnection r0 = r7.k     // Catch:{ Exception -> 0x006b }
            java.lang.String r1 = "Content-Length"
            java.lang.String r2 = r7.g     // Catch:{ Exception -> 0x006b }
            int r2 = r2.length()     // Catch:{ Exception -> 0x006b }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x006b }
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x006b }
            java.net.HttpURLConnection r0 = r7.k     // Catch:{ Exception -> 0x006b }
            java.io.OutputStream r0 = r0.getOutputStream()     // Catch:{ Exception -> 0x006b }
            java.io.BufferedWriter r1 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x006b }
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x006b }
            r2.<init>(r0)     // Catch:{ Exception -> 0x006b }
            r0 = 4096(0x1000, float:5.74E-42)
            r1.<init>(r2, r0)     // Catch:{ Exception -> 0x006b }
            java.lang.String r0 = r7.g     // Catch:{ Exception -> 0x006b }
            r1.write(r0)     // Catch:{ Exception -> 0x006b }
            r1.close()     // Catch:{ Exception -> 0x006b }
        L_0x00d9:
            java.net.HttpURLConnection r0 = r7.k     // Catch:{ Exception -> 0x006b }
            int r0 = r0.getResponseCode()     // Catch:{ Exception -> 0x006b }
            r7.j = r0     // Catch:{ Exception -> 0x006b }
            int r0 = r7.j     // Catch:{ Exception -> 0x006b }
            r1 = 200(0xc8, float:2.8E-43)
            if (r0 < r1) goto L_0x0076
            int r0 = r7.j     // Catch:{ Exception -> 0x006b }
            r1 = 300(0x12c, float:4.2E-43)
            if (r0 >= r1) goto L_0x0076
            java.net.HttpURLConnection r0 = r7.k     // Catch:{ Exception -> 0x006b }
            java.net.URL r0 = r0.getURL()     // Catch:{ Exception -> 0x006b }
            r7.a = r0     // Catch:{ Exception -> 0x006b }
            boolean r0 = r7.i     // Catch:{ Exception -> 0x006b }
            if (r0 == 0) goto L_0x011e
            java.io.BufferedInputStream r0 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x006b }
            java.net.HttpURLConnection r1 = r7.k     // Catch:{ Exception -> 0x006b }
            java.io.InputStream r1 = r1.getInputStream()     // Catch:{ Exception -> 0x006b }
            r2 = 4096(0x1000, float:5.74E-42)
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x006b }
            r1 = 4096(0x1000, float:5.74E-42)
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x006b }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x006b }
            r3 = 4096(0x1000, float:5.74E-42)
            r2.<init>(r3)     // Catch:{ Exception -> 0x006b }
        L_0x0111:
            int r3 = r0.read(r1)     // Catch:{ Exception -> 0x006b }
            r4 = -1
            if (r3 != r4) goto L_0x0127
            byte[] r0 = r2.toByteArray()     // Catch:{ Exception -> 0x006b }
            r7.h = r0     // Catch:{ Exception -> 0x006b }
        L_0x011e:
            r0 = r5
            goto L_0x0077
        L_0x0121:
            java.net.HttpURLConnection r0 = r7.k     // Catch:{ Exception -> 0x006b }
            r0.connect()     // Catch:{ Exception -> 0x006b }
            goto L_0x00d9
        L_0x0127:
            r4 = 0
            r2.write(r1, r4, r3)     // Catch:{ Exception -> 0x006b }
            goto L_0x0111
        L_0x012c:
            r0 = r6
            goto L_0x0088
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.q.a():boolean");
    }

    private void f() {
        if (this.k != null) {
            this.k.disconnect();
            this.k = null;
        }
    }

    public final void run() {
        try {
            a();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
