package cn.domob.android.ads;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import java.io.IOException;
import java.io.InputStream;

public class DomobInApp {
    private ProgressDialog a = null;
    private WebView b;
    /* access modifiers changed from: private */
    public String c = "";
    /* access modifiers changed from: private */
    public Context d = null;
    /* access modifiers changed from: private */
    public Dialog e = null;
    private RelativeLayout f;
    private RelativeLayout g;
    private Animation h;
    private Animation i;
    private Animation j;
    private Animation k;
    /* access modifiers changed from: private */
    public Handler l = new Handler();
    /* access modifiers changed from: private */
    public int m = 0;
    private int n;
    private int o;
    private float p = 1.0f;
    private ImageButton q;
    private ImageButton r;
    /* access modifiers changed from: private */
    public ImageView s;
    /* access modifiers changed from: private */
    public RotateAnimation t;
    /* access modifiers changed from: private */
    public int u;

    /* access modifiers changed from: protected */
    public Dialog bulidClickedDialog(Context context, String url, ProgressDialog pd, DomobAdView adview) throws Exception {
        this.d = context;
        DomobAdManager.getDeviceDensity(this.d);
        this.p = DomobAdManager.getCurrentDensity(this.d, adview);
        this.c = a(url);
        this.a = pd;
        this.f = new RelativeLayout(context);
        this.f.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        this.s = new ImageView(this.d);
        this.s.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeStream(this.d.getAssets().open("loading.png"))));
        this.s.setVisibility(8);
        this.t = new RotateAnimation(0.0f, 360.0f, 1, 0.5f, 1, 0.5f);
        this.t.setDuration(1000);
        this.t.setInterpolator(new LinearInterpolator());
        this.t.setRepeatCount(-1);
        RelativeLayout relativeLayout = this.f;
        this.b = new WebView(this.d);
        this.b.loadUrl(this.c);
        this.s.setVisibility(0);
        this.s.startAnimation(this.t);
        this.b.getSettings().setJavaScriptEnabled(true);
        this.b.setDownloadListener(new DownloadListener() {
            public final void onDownloadStart(String url, String str, String str2, String str3, long j) {
                try {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(url));
                    intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
                    DomobInApp.this.d.startActivity(intent);
                } catch (Exception e) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.e(Constants.LOG, "failed to intent " + url);
                    }
                }
            }
        });
        this.b.setWebViewClient(new WebViewClient() {
            public final boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (!url.startsWith("wtai://wp/") || !url.startsWith("wtai://wp/mc;")) {
                    try {
                        if (url.startsWith("market://search?q=")) {
                            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(url));
                            intent.addCategory("android.intent.category.BROWSABLE");
                            DomobInApp.this.d.startActivity(intent);
                            return true;
                        }
                    } catch (Exception e) {
                        if (Log.isLoggable(Constants.LOG, 3)) {
                            Log.e(Constants.LOG, "failed to intent market " + url);
                        }
                    }
                    if (url.startsWith("sms:") || url.startsWith("mailto:")) {
                        try {
                            DomobInApp.this.d.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                            return true;
                        } catch (Exception e2) {
                            if (Log.isLoggable(Constants.LOG, 3)) {
                                Log.e(Constants.LOG, "failed to intent " + url);
                            }
                        }
                    }
                    view.loadUrl(url);
                    return true;
                }
                Log.e("in", "tel");
                DomobInApp.this.d.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("tel:" + url.substring("wtai://wp/mc;".length()))));
                return true;
            }

            public final void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                DomobInApp.this.s.clearAnimation();
                DomobInApp.this.s.setVisibility(8);
                DomobInApp.this.b();
            }

            public final void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                DomobInApp.this.s.setVisibility(0);
                DomobInApp.this.s.startAnimation(DomobInApp.this.t);
                DomobInApp.this.b();
            }
        });
        this.b.setWebChromeClient(new WebChromeClient() {
            public final void onProgressChanged(WebView view, int newProgress) {
                DomobInApp.this.b();
                super.onProgressChanged(view, newProgress);
            }
        });
        this.b.clearCache(false);
        this.b.getSettings().setCacheMode(1);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
        this.b.setScrollBarStyle(0);
        layoutParams2.addRule(12);
        layoutParams2.bottomMargin = (int) (40.0f * this.p);
        this.b.setLayoutParams(layoutParams2);
        relativeLayout.addView(this.b);
        this.f.addView(this.s, layoutParams);
        LinearLayout linearLayout = new LinearLayout(this.d);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(17);
        linearLayout.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeStream(this.d.getAssets().open("banner.png"))));
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, (int) (40.0f * this.p));
        layoutParams3.addRule(12);
        this.q = new ImageButton(this.d);
        this.q.setLayoutParams(new LinearLayout.LayoutParams((int) (40.0f * this.p), (int) (40.0f * this.p)));
        String str = this.b.canGoBack() ? "preview.png" : "preview_off.png";
        this.q.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                DomobInApp.j(DomobInApp.this);
            }
        });
        linearLayout.addView(a(str, this.q));
        this.r = new ImageButton(this.d);
        this.r.setLayoutParams(new LinearLayout.LayoutParams((int) (40.0f * this.p), (int) (40.0f * this.p)));
        this.r.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                DomobInApp.k(DomobInApp.this);
            }
        });
        linearLayout.addView(a("next_off.png", this.r));
        ImageButton imageButton = new ImageButton(this.d);
        imageButton.setLayoutParams(new LinearLayout.LayoutParams((int) (40.0f * this.p), (int) (40.0f * this.p)));
        imageButton.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                try {
                    DomobInApp.n(DomobInApp.this);
                } catch (Exception e) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.e(Constants.LOG, "intent " + DomobInApp.this.c + " error");
                    }
                }
            }
        });
        linearLayout.addView(a("refresh.png", imageButton));
        ImageButton imageButton2 = new ImageButton(this.d);
        imageButton2.setLayoutParams(new LinearLayout.LayoutParams((int) (40.0f * this.p), (int) (40.0f * this.p)));
        imageButton2.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                try {
                    DomobInApp.this.d.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(DomobInApp.this.c)));
                } catch (Exception e) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.e(Constants.LOG, "intent " + DomobInApp.this.c + " error");
                    }
                }
            }
        });
        linearLayout.addView(a("out.png", imageButton2));
        linearLayout.addView(a());
        this.f.addView(linearLayout, layoutParams3);
        boolean z = false;
        if (context instanceof Activity) {
            z = a(((Activity) context).getWindow().getAttributes().flags, 1024);
        }
        if (z) {
            this.e = new Dialog(context, 16973841);
        } else {
            this.e = new Dialog(context, 16973840);
        }
        this.h = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
        this.h.setDuration(500);
        this.f.startAnimation(this.h);
        WindowManager.LayoutParams attributes = this.e.getWindow().getAttributes();
        attributes.width = -1;
        attributes.height = -1;
        attributes.dimAmount = 0.5f;
        this.e.getWindow().setAttributes(attributes);
        this.e.getWindow().setFlags(2, 2);
        this.e.getWindow().requestFeature(2);
        this.e.setContentView(this.f);
        return this.e;
    }

    /* access modifiers changed from: protected */
    public Dialog bulidClickedImage(DomobAdView adview, String url, ProgressDialog pd) throws Exception {
        RelativeLayout.LayoutParams layoutParams;
        boolean z;
        this.m = 1;
        this.d = adview.getContext();
        DomobAdManager.getDeviceDensity(this.d);
        this.p = DomobAdManager.getCurrentDensity(this.d, adview);
        this.c = a(url);
        this.a = pd;
        this.n = DomobAdManager.getScreenWidth(this.d, adview);
        this.o = DomobAdManager.getScreenHeight(this.d, adview);
        WindowManager.LayoutParams attributes = this.a.getWindow().getAttributes();
        attributes.gravity = 80;
        this.a.getWindow().setAttributes(attributes);
        this.a.setProgressStyle(0);
        this.g = new RelativeLayout(this.d);
        this.f = new RelativeLayout(this.d);
        this.f.setPadding(10, 10, 10, 10);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(13);
        this.s = new ImageView(this.d);
        this.s.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeStream(this.d.getAssets().open("loading.png"))));
        this.s.setVisibility(8);
        this.t = new RotateAnimation(0.0f, 360.0f, 1, 0.5f, 1, 0.5f);
        this.t.setDuration(1000);
        this.t.setInterpolator(new AccelerateInterpolator(0.5f));
        this.t.setRepeatCount(-1);
        RelativeLayout relativeLayout = this.f;
        this.b = new WebView(this.d);
        this.b.loadUrl(this.c);
        this.s.setVisibility(0);
        this.s.startAnimation(this.t);
        this.b.getSettings().setJavaScriptEnabled(true);
        this.b.setWebViewClient(new WebViewClient() {
            public final void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                DomobInApp.this.s.clearAnimation();
                DomobInApp.this.s.setVisibility(8);
            }

            public final void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                DomobInApp.this.s.setVisibility(0);
                DomobInApp.this.s.startAnimation(DomobInApp.this.t);
            }
        });
        this.b.setWebChromeClient(new WebChromeClient(this) {
            public final void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }
        });
        this.b.clearCache(true);
        this.b.getSettings().setCacheMode(1);
        if (DomobAdManager.getOrientation(this.d) != "v") {
            ((Activity) this.d).setRequestedOrientation(0);
            layoutParams = this.n / this.o > 0 ? ((float) (this.o - 20)) >= this.p * 300.0f ? new RelativeLayout.LayoutParams((int) (400.0f * this.p), (int) (this.p * 300.0f)) : new RelativeLayout.LayoutParams((int) (((float) ((((int) (((float) (this.o - 20)) * this.p)) << 2) / 3)) * this.p), (int) (((float) (this.o - 20)) * this.p)) : this.o - 20 >= 400 ? new RelativeLayout.LayoutParams((int) (this.p * 300.0f), (int) (400.0f * this.p)) : new RelativeLayout.LayoutParams((int) (((float) ((((int) (((float) (this.o - 20)) * this.p)) << 2) / 3)) * this.p), (int) (((float) (this.o - 20)) * this.p));
        } else if (this.o / this.n > 0) {
            ((Activity) this.d).setRequestedOrientation(1);
            layoutParams = ((float) (this.n - 20)) >= this.p * 300.0f ? new RelativeLayout.LayoutParams((int) (this.p * 300.0f), (int) (400.0f * this.p)) : new RelativeLayout.LayoutParams((int) (((float) (this.n - 20)) * this.p), (int) (((float) ((((int) (((float) (this.n - 20)) * this.p)) << 2) / 3)) * this.p));
        } else {
            layoutParams = this.o - 20 >= 400 ? new RelativeLayout.LayoutParams((int) (this.p * 300.0f), (int) (400.0f * this.p)) : new RelativeLayout.LayoutParams((int) (((float) ((((int) (((float) (this.o - 20)) * this.p)) * 3) / 4)) * this.p), (int) (((float) (this.o - 20)) * this.p));
        }
        layoutParams.addRule(15);
        this.b.setScrollBarStyle(0);
        this.b.setLayoutParams(layoutParams);
        relativeLayout.addView(this.b);
        this.f.addView(this.s, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(11);
        if (this.d instanceof Activity) {
            z = a(((Activity) this.d).getWindow().getAttributes().flags, 1024);
        } else {
            z = false;
        }
        if (z) {
            this.e = new Dialog(this.d, 16973841);
        } else {
            this.e = new Dialog(this.d, 16973840);
        }
        this.e.setCancelable(true);
        WindowManager.LayoutParams attributes2 = this.e.getWindow().getAttributes();
        this.u = ((Activity) this.d).getRequestedOrientation();
        if (DomobAdManager.getOrientation(this.d) == "v") {
            ((Activity) this.d).setRequestedOrientation(1);
            if (this.o / this.n > 0) {
                if (this.n - 20 >= 300) {
                    attributes2.width = (int) (320.0f * this.p);
                    attributes2.height = (int) (420.0f * this.p);
                } else {
                    attributes2.width = (int) (((float) ((this.n - 20) + 20)) * this.p);
                    attributes2.height = (int) (((float) ((((this.n - 20) + 20) << 2) / 3)) * this.p);
                }
            } else if (this.o - 20 >= 400) {
                attributes2.width = (int) (this.p * 300.0f);
                attributes2.height = (int) (400.0f * this.p);
            } else {
                attributes2.height = (int) (((float) (this.o - 20)) * this.p);
                attributes2.width = (int) (((float) ((attributes2.height * 3) / 4)) * this.p);
            }
        } else {
            ((Activity) this.d).setRequestedOrientation(0);
            if (this.n / this.o > 0) {
                if (this.o - 20 >= 400) {
                    attributes2.height = (int) (this.p * 300.0f);
                    attributes2.width = (int) (400.0f * this.p);
                } else {
                    attributes2.width = (int) (((float) (((this.o - 20) << 2) / 3)) * this.p);
                    attributes2.height = (int) (((float) (this.o - 20)) * this.p);
                }
            } else if (this.o - 20 >= 400) {
                attributes2.width = (int) (this.p * 300.0f);
                attributes2.height = (int) (400.0f * this.p);
            } else {
                attributes2.height = (int) (((float) (this.o - 20)) * this.p);
                attributes2.width = (int) (((float) ((attributes2.height * 3) / 4)) * this.p);
            }
        }
        attributes2.dimAmount = 0.7f;
        this.e.getWindow().setAttributes(attributes2);
        this.e.getWindow().setFlags(2, 2);
        this.f.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.g.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        RelativeLayout relativeLayout2 = this.g;
        Button button = new Button(this.d);
        button.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        button.setBackgroundColor(Color.argb(100, 0, 0, 0));
        relativeLayout2.addView(button);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams4.addRule(15);
        this.g.addView(this.f, layoutParams4);
        this.g.addView(a(), layoutParams3);
        this.e.setContentView(this.g);
        this.j = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, (float) (this.e.getWindow().getAttributes().width / 2), (float) (this.e.getWindow().getAttributes().height / 2));
        this.j.setDuration(500);
        this.g.startAnimation(this.j);
        return this.e;
    }

    private static boolean a(int i2, int i3) {
        int length = Integer.toBinaryString(1024).length() - 1;
        if ((i2 >>> length) % 2 == (1024 >>> length) % 2) {
            return true;
        }
        return false;
    }

    private static String a(String str) {
        String trim = str.trim();
        int indexOf = trim.indexOf(58);
        boolean z = true;
        String str2 = trim;
        for (int i2 = 0; i2 < indexOf; i2++) {
            char charAt = str2.charAt(i2);
            if (!Character.isLetter(charAt)) {
                break;
            }
            z &= Character.isLowerCase(charAt);
            if (i2 == indexOf - 1 && !z) {
                str2 = String.valueOf(str2.substring(0, indexOf).toLowerCase()) + str2.substring(indexOf);
            }
        }
        if (str2.startsWith("http://") || str2.startsWith("https://")) {
            return str2;
        }
        if (!str2.startsWith("http:") && !str2.startsWith("https:")) {
            return str2;
        }
        if (str2.startsWith("http:/") || str2.startsWith("https:/")) {
            return str2.replaceFirst("/", "//");
        }
        return str2.replaceFirst(":", "://");
    }

    private static Drawable a(String str, Context context) {
        InputStream inputStream;
        InputStream inputStream2;
        try {
            inputStream2 = context.getAssets().open(str);
            try {
                BitmapDrawable bitmapDrawable = new BitmapDrawable(BitmapFactory.decodeStream(inputStream2));
                try {
                    inputStream2.close();
                    return bitmapDrawable;
                } catch (Exception e2) {
                    Log.d(Constants.LOG, "asset stream close error");
                    return bitmapDrawable;
                }
            } catch (Exception e3) {
                try {
                    Log.d(Constants.LOG, "load " + str + " error");
                    try {
                        inputStream2.close();
                        return null;
                    } catch (Exception e4) {
                        Log.d(Constants.LOG, "asset stream close error");
                        return null;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    inputStream = inputStream2;
                    th = th2;
                    try {
                        inputStream.close();
                    } catch (Exception e5) {
                        Log.d(Constants.LOG, "asset stream close error");
                    }
                    throw th;
                }
            }
        } catch (Exception e6) {
            inputStream2 = null;
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
            inputStream.close();
            throw th;
        }
    }

    private LinearLayout a(String str, ImageButton imageButton) {
        LinearLayout.LayoutParams layoutParams;
        LinearLayout linearLayout = new LinearLayout(this.d);
        imageButton.setBackgroundDrawable(a(str, this.d));
        if (this.m == 1) {
            layoutParams = new LinearLayout.LayoutParams((int) (this.p * 30.0f), (int) (this.p * 30.0f));
        } else {
            layoutParams = new LinearLayout.LayoutParams((int) (this.p * 30.0f), (int) (this.p * 30.0f));
        }
        imageButton.setLayoutParams(layoutParams);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 1.0f));
        linearLayout.setGravity(17);
        linearLayout.addView(imageButton);
        return linearLayout;
    }

    class a implements Runnable {
        a() {
        }

        public final void run() {
            DomobInApp.this.e.dismiss();
            if (DomobInApp.this.m == 1) {
                ((Activity) DomobInApp.this.d).setRequestedOrientation(DomobInApp.this.u);
            }
        }
    }

    private LinearLayout a() {
        String str;
        LinearLayout.LayoutParams layoutParams;
        ImageButton imageButton = new ImageButton(this.d);
        if (this.m == 1) {
            str = "close.png";
            layoutParams = new LinearLayout.LayoutParams((int) (this.p * 30.0f), (int) (this.p * 30.0f));
        } else {
            str = "exit.png";
            layoutParams = new LinearLayout.LayoutParams((int) (this.p * 30.0f), (int) (this.p * 30.0f));
        }
        imageButton.setLayoutParams(layoutParams);
        imageButton.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                DomobInApp.h(DomobInApp.this);
                DomobInApp.this.l.postDelayed(new a(), 500);
            }
        });
        return a(str, imageButton);
    }

    static /* synthetic */ void h(DomobInApp domobInApp) {
        if (domobInApp.m == 0) {
            domobInApp.i = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
            domobInApp.i.setDuration(500);
            domobInApp.f.startAnimation(domobInApp.i);
        } else if (domobInApp.m == 1) {
            domobInApp.k = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, (float) (domobInApp.e.getWindow().getAttributes().width / 2), (float) (domobInApp.e.getWindow().getAttributes().height / 2));
            domobInApp.k.setDuration(500);
            domobInApp.g.startAnimation(domobInApp.k);
        }
    }

    static /* synthetic */ void j(DomobInApp domobInApp) {
        if (domobInApp.b != null && domobInApp.b.canGoBack()) {
            domobInApp.b.goBack();
            domobInApp.b();
        }
    }

    static /* synthetic */ void k(DomobInApp domobInApp) {
        if (domobInApp.b != null && domobInApp.b.canGoForward()) {
            domobInApp.b.goForward();
            domobInApp.b();
        }
    }

    static /* synthetic */ void n(DomobInApp domobInApp) {
        if (domobInApp.b != null) {
            domobInApp.b.reload();
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.b.canGoBack()) {
            try {
                this.q.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeStream(this.d.getAssets().open("preview.png"))));
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        } else {
            try {
                this.q.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeStream(this.d.getAssets().open("preview_off.png"))));
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        }
        if (this.b.canGoForward()) {
            try {
                this.r.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeStream(this.d.getAssets().open("next.png"))));
            } catch (IOException e4) {
                e4.printStackTrace();
            }
        } else {
            try {
                this.r.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeStream(this.d.getAssets().open("next_off.png"))));
            } catch (IOException e5) {
                e5.printStackTrace();
            }
        }
    }
}
