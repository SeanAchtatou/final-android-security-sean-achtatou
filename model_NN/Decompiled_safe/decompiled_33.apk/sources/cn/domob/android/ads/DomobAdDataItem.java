package cn.domob.android.ads;

import android.graphics.Bitmap;
import org.json.JSONObject;

public class DomobAdDataItem {
    public static final String IMAGE_TYPE = "image";
    public static final String TEXT_TYPE = "text";
    private String a = "";
    private String b = "nullad";
    private String c = TEXT_TYPE;
    private Bitmap d = null;
    private Bitmap e = null;
    private String f = "";
    private Bitmap g = null;
    private String h = "";
    private JSONObject i = null;
    private DomobAdEngine j = null;

    /* access modifiers changed from: protected */
    public DomobAdEngine getEngine() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public void setEngine(DomobAdEngine engine) {
        this.j = engine;
    }

    /* access modifiers changed from: protected */
    public String getAd_rp_url() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public void setAd_rp_url(String ad_rp_url) {
        this.a = ad_rp_url;
    }

    public String getAd_identifier() {
        return this.b;
    }

    public boolean isNullAd() {
        return this.b.equals("nullad");
    }

    /* access modifiers changed from: protected */
    public void setAd_identifier(String ad_identifier) {
        this.b = ad_identifier;
    }

    public String getAd_type() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void setAd_type(String ad_type) {
        this.c = ad_type;
    }

    public Bitmap getAd_icon() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public void setAd_icon(Bitmap ad_icon) {
        this.d = ad_icon;
    }

    public Bitmap getAd_action_icon() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public void setAd_action_icon(Bitmap ad_action_icon) {
        this.e = ad_action_icon;
    }

    public String getAd_text() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public void setAd_text(String ad_text) {
        this.f = ad_text;
    }

    public Bitmap getAdImage() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public void setAdImage(Bitmap adImage) {
        this.g = adImage;
    }

    public String getAlt_text() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public void setAlt_text(String alt_text) {
        this.h = alt_text;
    }

    /* access modifiers changed from: protected */
    public boolean checkDataValid() {
        if (this.b == null || this.a == null || this.c == null || this.i == null) {
            return false;
        }
        if (this.c.equals(TEXT_TYPE)) {
            if (this.d == null || this.e == null || this.f == null) {
                return false;
            }
        } else if (this.c.equals(IMAGE_TYPE) && this.g == null) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public JSONObject getAction() {
        return this.i;
    }

    /* access modifiers changed from: protected */
    public void setAction(JSONObject action) {
        this.i = action;
    }
}
