package cn.domob.android.ads;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.RelativeLayout;
import cn.domob.android.ads.DomobAdEngine;
import java.util.ArrayList;

public class DomobAdView extends RelativeLayout {
    public static final int ANIMATION_ALPHA = 3;
    public static final int ANIMATION_FRAGMENT = 9;
    public static final int ANIMATION_ROTATE = 1;
    public static final int ANIMATION_SCALE_FROM_MIDDLE = 5;
    public static final int ANIMATION_TRANSLATE = 7;
    static final Handler a = new Handler();
    private static Boolean b = null;
    private f A;
    private c B;
    private ArrayList<Integer> C;
    private boolean D;
    /* access modifiers changed from: private */
    public DomobAdBuilder c;
    private String d;
    private DomobAdEngine.RecvHandler e;
    private int f;
    private int g;
    private long h;
    private long i;
    private long j;
    private String k;
    private DomobAdListener l;
    private e m;
    protected boolean mNoAd;
    private boolean n;
    private boolean o;
    private boolean p;
    /* access modifiers changed from: private */
    public boolean q;
    private long r;
    private long s;
    /* access modifiers changed from: private */
    public int t;
    /* access modifiers changed from: private */
    public boolean u;
    /* access modifiers changed from: private */
    public Context v;
    private boolean w;
    private boolean x;
    private c y;
    private boolean z;

    public DomobAdView(Activity activity) {
        this(activity, null, 0);
    }

    protected DomobAdView(Activity activity, boolean dataMode, c adData, f dataSetting) {
        this(activity, null, 0);
        this.z = dataMode;
        this.y = adData;
        this.A = dataSetting;
    }

    public DomobAdView(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0);
    }

    public DomobAdView(Context context, AttributeSet attributeset, int defStyle) {
        super(context, attributeset, defStyle);
        this.i = -16777216;
        this.j = -1;
        this.mNoAd = true;
        this.w = false;
        this.x = false;
        this.y = new c();
        this.z = false;
        this.C = new ArrayList<>();
        this.D = false;
        Log.i(Constants.LOG, "current version is 20110613");
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "DomobAdView!");
        }
        setDescendantFocusability(262144);
        setClickable(true);
        setLongClickable(false);
        setGravity(17);
        this.f = 20000;
        if (attributeset != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            int attributeUnsignedIntValue = attributeset.getAttributeUnsignedIntValue(str, "backgroundColor", -1);
            if (attributeUnsignedIntValue != -1) {
                setBackgroundColor(attributeUnsignedIntValue);
            }
            int attributeUnsignedIntValue2 = attributeset.getAttributeUnsignedIntValue(str, "primaryTextColor", -1);
            if (attributeUnsignedIntValue2 != -1) {
                setPrimaryTextColor(attributeUnsignedIntValue2);
            }
            this.d = attributeset.getAttributeValue(str, "keywords");
            this.k = attributeset.getAttributeValue(str, "spots");
            setDefaultInterval(attributeset.getAttributeIntValue(str, "refreshInterval", 20));
        }
        this.v = context;
        this.c = null;
        this.e = null;
        if (b == null) {
            b = Boolean.valueOf(a(context));
        }
        if (!initByConf(context)) {
            this.q = false;
            this.r = 0;
            this.s = 0;
            setRequestInterval(1);
        }
        this.n = false;
        this.p = true;
        this.h = 0;
        this.u = true;
        this.t = 0;
        if (this.q) {
            long uptimeMillis = SystemClock.uptimeMillis();
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "mDisabled = " + this.q + " | mDisabledTime = " + this.r + " | mDisabledTimestamp = " + this.s);
                Log.d(Constants.LOG, "curr timestamp = " + uptimeMillis);
            }
            if (uptimeMillis - this.s >= this.r * 1000) {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "send detector because ad is disabled.");
                }
                sendDetector();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean initByConf(android.content.Context r7) {
        /*
            r6 = this;
            r3 = 1
            r4 = 0
            r0 = 0
            cn.domob.android.ads.DBHelper r1 = cn.domob.android.ads.DBHelper.a(r7)     // Catch:{ Exception -> 0x0074 }
            android.database.Cursor r0 = r1.b()     // Catch:{ Exception -> 0x0074 }
            if (r0 == 0) goto L_0x0013
            int r2 = r0.getCount()     // Catch:{ Exception -> 0x006a }
            if (r2 != 0) goto L_0x002d
        L_0x0013:
            java.lang.String r2 = "DomobSDK"
            r3 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r3)     // Catch:{ Exception -> 0x006a }
            if (r2 == 0) goto L_0x0023
            java.lang.String r2 = "DomobSDK"
            java.lang.String r3 = "no data in conf db, initialize now."
            android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x006a }
        L_0x0023:
            r1.c()     // Catch:{ Exception -> 0x006a }
            r1 = r4
        L_0x0027:
            if (r0 == 0) goto L_0x002c
            r0.close()
        L_0x002c:
            return r1
        L_0x002d:
            r0.moveToFirst()     // Catch:{ Exception -> 0x006a }
            java.lang.String r1 = "_dis_flag"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x006a }
            int r1 = r0.getInt(r1)     // Catch:{ Exception -> 0x006a }
            if (r1 != 0) goto L_0x0066
            r1 = 0
            r6.q = r1     // Catch:{ Exception -> 0x006a }
        L_0x003f:
            java.lang.String r1 = "_dis_time"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x006a }
            long r1 = r0.getLong(r1)     // Catch:{ Exception -> 0x006a }
            r6.r = r1     // Catch:{ Exception -> 0x006a }
            java.lang.String r1 = "_dis_timestamp"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x006a }
            long r1 = r0.getLong(r1)     // Catch:{ Exception -> 0x006a }
            r6.s = r1     // Catch:{ Exception -> 0x006a }
            java.lang.String r1 = "_interval"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x006a }
            int r1 = r0.getInt(r1)     // Catch:{ Exception -> 0x006a }
            r6.setRequestInterval(r1)     // Catch:{ Exception -> 0x006a }
            r1 = r3
            goto L_0x0027
        L_0x0066:
            r1 = 1
            r6.q = r1     // Catch:{ Exception -> 0x006a }
            goto L_0x003f
        L_0x006a:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x006e:
            r0.printStackTrace()
            r0 = r1
            r1 = r4
            goto L_0x0027
        L_0x0074:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.DomobAdView.initByConf(android.content.Context):boolean");
    }

    /* access modifiers changed from: protected */
    public void saveAvg2DB() {
        if (this.v != null) {
            long c2 = j.c();
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "The avg req time is:" + c2);
            }
            if (c2 != -1) {
                DBHelper a2 = DBHelper.a(this.v);
                ContentValues contentValues = new ContentValues();
                contentValues.put("_avg_time", Long.valueOf(c2));
                a2.a(contentValues);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int i2;
        DomobAdEngine b2;
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "DomobAdView onMeasure");
        }
        int size = View.MeasureSpec.getSize(widthMeasureSpec);
        int mode = View.MeasureSpec.getMode(widthMeasureSpec);
        int size2 = View.MeasureSpec.getSize(heightMeasureSpec);
        int mode2 = View.MeasureSpec.getMode(heightMeasureSpec);
        if (!(mode == Integer.MIN_VALUE || mode == 1073741824)) {
            size = DomobAdManager.getScreenCurrentHeight(getContext());
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "DomobAdView width is :" + size);
        }
        if (mode2 == 1073741824) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "heightMeasureSpec is android.view.View.MeasureSpec.EXACTLY!");
            }
            i2 = size2;
        } else if (this.c == null || (b2 = this.c.b()) == null) {
            i2 = 0;
        } else {
            i2 = b2.a(b2.c());
            if (mode2 == Integer.MIN_VALUE && size2 < i2) {
                Log.e(Constants.LOG, "Cannot display ad because its container is too small.  The ad is " + i2 + " pixels tall but is only given " + size2 + " at most to draw into.  Please make your view containing DomobAdView taller.");
                i2 = 0;
            }
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "DomobAdView height is :" + i2);
        }
        setMeasuredDimension(size, i2);
        if (this.mNoAd && !b.booleanValue()) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "request the first ad!");
            }
            ifRequestFreshAd();
        }
    }

    protected static void setNeedDetect(DomobAdView adview, boolean flag) {
        adview.p = flag;
    }

    public void requestFreshAd() {
        this.D = true;
        ifRequestFreshAd();
    }

    /* access modifiers changed from: protected */
    public boolean ifRequestFreshAd() {
        long uptimeMillis = (SystemClock.uptimeMillis() - this.h) / 1000;
        if (uptimeMillis < 20) {
            Log.w(Constants.LOG, "Ignoring requestFreshAd.  Called " + uptimeMillis + " seconds since last refresh.  " + "Refreshes must be at least " + 20 + " apart.");
            return false;
        } else if (this.n) {
            Log.w(Constants.LOG, "Ignoring doRefresh() because we are requesting an ad right now already.");
            return false;
        } else {
            a();
            return true;
        }
    }

    protected static void requestFreshAd(DomobAdView adview) {
        if (adview == null) {
            return;
        }
        if (!adview.D || adview.p || adview.mNoAd) {
            adview.a();
        }
    }

    /* access modifiers changed from: protected */
    public void sendDetector() {
        new i(this).start();
    }

    private void a() {
        if (this.q && (!DomobAdManager.isTestMode(this.v) || !DomobAdManager.isTestAllowed(this.v))) {
            Log.w(Constants.LOG, "AD has been disabled on web server, ignore doRefresh()");
        } else if (!this.mNoAd && super.getVisibility() != 0) {
            Log.w(Constants.LOG, "Cannot doRefresh() when the DomobAdView is not visible. Call DomobAdView.setVisibility(View.VISIBLE) first.");
        } else if (this.n) {
            Log.w(Constants.LOG, "Ignoring doRefresh() because we are requesting an ad right now already.");
        } else {
            this.n = true;
            this.h = SystemClock.uptimeMillis();
            if (this.p) {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "send detector!");
                }
                sendDetector();
                return;
            }
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "doRefresh now!");
            }
            new h(this).start();
        }
    }

    public int getRequestInterval() {
        return this.g / 1000;
    }

    public void setRequestInterval(int requestInterval) {
        if (requestInterval == 1) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "use the default interval(from xml).");
            }
            this.g = this.f;
        } else if (requestInterval == 0) {
            Log.w(Constants.LOG, "refreshInterval is 0, AD will not be refreshed any more.");
            this.g = requestInterval * 1000;
            b();
        } else {
            this.g = a(requestInterval) * 1000;
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "setRequestInterval:" + this.g);
        }
    }

    /* access modifiers changed from: protected */
    public void setDefaultInterval(int i2) {
        this.f = a(i2) * 1000;
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "setDefaultInterval:" + this.f);
        }
    }

    private static int a(int i2) {
        if (i2 < 20) {
            return 20;
        }
        if (i2 > 600) {
            return 600;
        }
        return i2;
    }

    private void a(boolean z2) {
        if (!this.z) {
            synchronized (this) {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "schedule for a fresh ad?" + z2);
                    Log.d(Constants.LOG, "check WindowsVisibility:" + this.t);
                    Log.d(Constants.LOG, "check WindowsFocus:" + this.u);
                    Log.d(Constants.LOG, "getVisibility:" + getVisibility());
                    Log.d(Constants.LOG, "mInterval:" + this.g);
                }
                boolean z3 = false;
                if (this.g > 0 || (this.g == 0 && this.mNoAd)) {
                    z3 = true;
                }
                if (z2 && z3 && getVisibility() == 0 && this.t == 0 && this.u) {
                    b();
                    this.B = new c(this);
                    if (this.g == 0) {
                        a.postDelayed(this.B, 20000);
                    } else {
                        a.postDelayed(this.B, (long) this.g);
                    }
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "Ad refresh scheduled for " + this.g + " from now.");
                    }
                } else if (!z2 || this.g == 0) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "just cancel the previous request!");
                    }
                    b();
                }
            }
        }
    }

    private void b() {
        if (a != null) {
            if (this.B != null) {
                this.B.a = true;
                a.removeCallbacks(this.B);
                this.B = null;
            }
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "Cancelled an ad refresh scheduled.");
            }
        }
    }

    class c implements Runnable {
        boolean a;

        /* synthetic */ c(DomobAdView domobAdView) {
            this((byte) 0);
        }

        private c(byte b2) {
        }

        public final void run() {
            try {
                DomobAdView domobAdView = DomobAdView.this;
                if (!this.a && domobAdView != null) {
                    int requestInterval = DomobAdView.getRequestInterval(domobAdView) / 1000;
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "Requesting a fresh ad because a request interval passed (" + requestInterval + " seconds).");
                    }
                    DomobAdView.requestFreshAd(domobAdView);
                }
            } catch (Exception e) {
                Log.e(Constants.LOG, "exception caught in RefreshThread.run()!");
                e.printStackTrace();
            }
        }
    }

    public void onWindowFocusChanged(boolean flag) {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "onWindowFocusChanged:" + flag);
        }
        this.u = flag;
        a(flag);
        super.onWindowFocusChanged(flag);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int v2) {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "onWindowVisibilityChanged:" + v2);
        }
        this.t = v2;
        a(v2 == 0);
        super.onWindowVisibilityChanged(v2);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "onAttachedToWindow");
        }
        this.o = true;
        a(true);
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "onDetachedFromWindow");
        }
        this.o = false;
        this.mNoAd = true;
        saveAvg2DB();
        a(false);
        if (this.c != null) {
            this.c.c();
        }
        j.b();
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public long getPrimaryTextColor() {
        return this.j;
    }

    public void setPrimaryTextColor(int color) {
        this.j = (long) (-16777216 | color);
        this.x = true;
    }

    /* access modifiers changed from: protected */
    public long getBackgroundColor() {
        return this.i;
    }

    public void setBackgroundColor(int color) {
        this.i = (long) (-16777216 | color);
        invalidate();
        this.w = true;
    }

    public String getKeywords() {
        return this.d;
    }

    public void setKeywords(String s2) {
        if (s2 == null || s2.length() <= 200) {
            this.d = s2;
        } else {
            Log.e(Constants.LOG, "The length of keywords cannot exceed 200!");
        }
    }

    public String getSpots() {
        return this.k;
    }

    public void setSpots(String s2) {
        if (s2 == null || s2.length() <= 200) {
            this.k = s2;
        } else {
            Log.e(Constants.LOG, "The length of spots cannot exceed 200!");
        }
    }

    public void setVisibility(int v2) {
        if (super.getVisibility() != v2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    getChildAt(i2).setVisibility(v2);
                }
                super.setVisibility(v2);
                invalidate();
            }
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "setVisibility:" + v2);
        }
        a(v2 == 0);
    }

    /* access modifiers changed from: protected */
    public void setDisabled(boolean flag, long time, long timestamp) {
        if (this.q != flag) {
            this.q = flag;
            if (this.q) {
                Log.w(Constants.LOG, "AD is disabled on web server.");
                this.r = time;
                this.s = timestamp;
            } else {
                this.r = 0;
                this.s = 0;
            }
            if (a != null) {
                a.post(new e(this));
            }
        }
    }

    public void setAdListener(DomobAdListener adlistener) {
        synchronized (this) {
            this.l = adlistener;
        }
    }

    public DomobAdListener getAdListener() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public void setDataAdListener(e adlistener) {
        synchronized (this) {
            this.m = adlistener;
        }
    }

    /* access modifiers changed from: protected */
    public e getDataAdListener() {
        return this.m;
    }

    public boolean hasAd() {
        return (this.c == null || this.c.b() == null) ? false : true;
    }

    public void cleanup() {
        if (this.c != null) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "clear the ad.");
            }
            this.c.c();
            this.c = null;
        }
    }

    private static boolean a(Context context) {
        try {
            if (Class.forName("org.json.JSONException") != null) {
                return false;
            }
        } catch (ClassNotFoundException e2) {
            e2.printStackTrace();
        }
        return DomobAdManager.getDeviceId(context) == null;
    }

    protected static DomobAdBuilder setBuilder(DomobAdView adview, DomobAdBuilder builder) {
        adview.c = builder;
        return builder;
    }

    protected static int getRequestInterval(DomobAdView adview) {
        return adview.g;
    }

    protected static DomobAdBuilder getBuilder(DomobAdView adview) {
        return adview.c;
    }

    protected static DomobAdListener getListener(DomobAdView adview) {
        return adview.l;
    }

    protected static String getKeywords(DomobAdView adview) {
        if (adview.d == null || adview.d.length() <= 200) {
            return adview.d;
        }
        Log.e(Constants.LOG, "The length of keywords cannot exceed 200!");
        return null;
    }

    protected static String getSpots(DomobAdView adview) {
        if (adview.k == null || adview.k.length() <= 200) {
            return adview.k;
        }
        Log.e(Constants.LOG, "The length of spots cannot exceed 200!");
        return null;
    }

    protected static void setRequesting(DomobAdView adview, boolean flag) {
        if (adview != null) {
            adview.n = flag;
        }
    }

    protected static void setSchedule(DomobAdView adview, boolean flag) {
        if (adview != null) {
            adview.a(flag);
        }
    }

    protected static long getRequestTimestamp(DomobAdView adview) {
        if (adview != null) {
            return adview.h;
        }
        return 0;
    }

    protected static DomobAdEngine.RecvHandler getReceiver(DomobAdView adview) {
        if (adview == null) {
            return null;
        }
        if (adview.e == null) {
            adview.e = new DomobAdEngine.RecvHandler(adview);
        }
        return adview.e;
    }

    protected static void failedToReceive(DomobAdView adview) {
        if (adview != null && adview.l != null && a != null) {
            Handler handler = a;
            adview.getClass();
            handler.post(new b(adview));
        }
    }

    class b implements Runnable {
        private DomobAdView a;

        public b(DomobAdView domobAdView) {
            this.a = domobAdView;
        }

        public final void run() {
            if (this.a != null) {
                try {
                    DomobAdView.getListener(this.a).onFailedToReceiveFreshAd(this.a);
                } catch (Exception e) {
                    Log.e(Constants.LOG, "Unhandled exception raised in onFailedToReceiveRefreshedAd.");
                    e.printStackTrace();
                }
            }
        }
    }

    protected static void receiveAd(DomobAdView adview, DomobAdEngine domobAdEngine) {
        if (adview != null && adview.l != null) {
            try {
                adview.l.onReceivedFreshAd(adview);
            } catch (Exception e2) {
                Log.e(Constants.LOG, "Unhandled exception raised in onReceivedFreshAd.");
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void constructView(DomobAdEngine engine, DomobAdBuilder builder) {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "construct ad view");
        }
        if (engine == null || builder == null) {
            Log.w(Constants.LOG, "failed to construct view!");
            return;
        }
        boolean z2 = this.mNoAd;
        this.mNoAd = false;
        builder.a(engine);
        int visibility = getVisibility();
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "this.getVisibility():" + visibility);
        }
        builder.setVisibility(visibility);
        builder.setGravity(17);
        engine.a(builder);
        builder.setLayoutParams(new RelativeLayout.LayoutParams(engine.a(engine.b()), engine.a(engine.c())));
        a.post(new d(builder, visibility, z2));
    }

    class e implements Runnable {
        /* synthetic */ e(DomobAdView domobAdView) {
            this((byte) 0);
        }

        private e(byte b) {
        }

        public final void run() {
            if (DomobAdView.this == null) {
                return;
            }
            if (!DomobAdView.this.q) {
                DomobAdView.this.setVisibility(0);
            } else if (!DomobAdManager.isTestMode(DomobAdView.this.v) || !DomobAdManager.isTestAllowed(DomobAdView.this.v)) {
                DomobAdView.this.setVisibility(8);
            }
        }
    }

    class d implements Runnable {
        private DomobAdView a;
        private DomobAdBuilder b;
        private int c;
        private boolean d;

        public d(DomobAdBuilder domobAdBuilder, int i, boolean z) {
            this.a = DomobAdView.this;
            this.b = domobAdBuilder;
            this.c = i;
            this.d = z;
        }

        public final void run() {
            try {
                if (!(this.a == null || this.b == null)) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "show ad!");
                        Log.d(Constants.LOG, "WindowVisibility:" + DomobAdView.this.t);
                        Log.d(Constants.LOG, "WindowFocus:" + DomobAdView.this.u);
                    }
                    if (DomobAdView.this.t == 0 && DomobAdView.this.u) {
                        this.b.setVisibility(4);
                        this.a.addView(this.b);
                        DomobAdView.receiveAd(this.a, this.b.b());
                        if (this.d) {
                            DomobAdView.startAlphaAnimation(this.a, this.b);
                            return;
                        } else if (this.c == 0) {
                            String a2 = this.b.a();
                            if (a2 == null || a2.equals("fr2l")) {
                                DomobAdView.startAnimation(this.a, this.b, p.a(this.a));
                                return;
                            }
                            return;
                        } else {
                            return;
                        }
                    }
                }
            } catch (Exception e2) {
                Log.e(Constants.LOG, "Unhandled exception in showAdThread.run() " + e2.getMessage());
                e2.printStackTrace();
            }
            if (this.b != null) {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "error or view is invisible, clear resources!");
                }
                this.a.removeView(this.b);
                this.b.c();
                this.b = null;
                if (this.d) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "reset mNoAd");
                    }
                    DomobAdView.this.mNoAd = true;
                }
            }
        }
    }

    protected static void startAlphaAnimation(DomobAdView adview, DomobAdBuilder builder) {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "startAlphaAnimation");
        }
        DomobAdBuilder domobAdBuilder = adview.c;
        builder.setVisibility(0);
        setBuilder(adview, builder);
        if (domobAdBuilder != null) {
            adview.removeView(domobAdBuilder);
            domobAdBuilder.c();
        }
        if (adview.o) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(233);
            alphaAnimation.startNow();
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new AccelerateInterpolator());
            adview.startAnimation(alphaAnimation);
        }
    }

    protected static void startAnimation(DomobAdView adview, DomobAdBuilder builder, int beginType) {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "start ad switch Animation");
        }
        if (beginType == 9) {
            builder.setVisibility(4);
            new m().a(adview, builder, adview.c);
            adview.c.setVisibility(8);
            return;
        }
        Animation a2 = p.a(beginType, adview);
        adview.getClass();
        a2.setAnimationListener(new a(adview, adview, builder, beginType));
        if (beginType == 5 || beginType == 7) {
            builder.setVisibility(0);
            builder.startAnimation(a2);
            return;
        }
        adview.startAnimation(a2);
    }

    class a implements Animation.AnimationListener {
        private DomobAdBuilder a;
        private DomobAdView b;
        private int c;

        a(DomobAdView domobAdView, DomobAdView domobAdView2, DomobAdBuilder domobAdBuilder, int i) {
            this.b = domobAdView2;
            this.a = domobAdBuilder;
            this.c = i;
        }

        public final void onAnimationStart(Animation animation) {
        }

        public final void onAnimationEnd(Animation animation) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "DomobAdView onAnimationEnd.");
            }
            if (this.a != null) {
                DomobAdBuilder e = this.b.c;
                this.a.setVisibility(0);
                DomobAdView.setBuilder(this.b, this.a);
                if (e != null) {
                    this.b.removeView(e);
                    e.c();
                }
                if (this.c != 5 && this.c != 7) {
                    this.b.startAnimation(p.a(this.c + 1, this.b));
                }
            }
        }

        public final void onAnimationRepeat(Animation animation) {
        }
    }

    /* access modifiers changed from: protected */
    public boolean getBackColorSetByClient() {
        return this.w;
    }

    /* access modifiers changed from: protected */
    public boolean getPrimTxtColorSetByClient() {
        return this.x;
    }

    /* access modifiers changed from: protected */
    public boolean isDataMode() {
        return this.z;
    }

    /* access modifiers changed from: protected */
    public c getAdData() {
        return this.y;
    }

    protected static void failedToReceiveAdData(DomobAdView adview, c cVar) {
        if (adview.z && adview.m != null) {
            e eVar = adview.m;
        }
    }

    protected static void receiveAdData(DomobAdView adview, c cVar) {
        if (adview.z && adview.m != null) {
            e eVar = adview.m;
            adview.mNoAd = false;
        }
    }

    /* access modifiers changed from: protected */
    public f getDataSetting() {
        return this.A;
    }

    /* access modifiers changed from: protected */
    public void setDataSetting(f dataSetting) {
        this.A = dataSetting;
    }

    public void SetAnimList(int[] animArray) {
        for (int valueOf : animArray) {
            this.C.add(Integer.valueOf(valueOf));
        }
    }

    /* access modifiers changed from: protected */
    public ArrayList<Integer> getAnimList() {
        return this.C;
    }
}
