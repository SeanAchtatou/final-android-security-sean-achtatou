package cn.domob.android.ads;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import com.mobclick.android.ReportPolicy;
import java.util.ArrayList;
import java.util.Random;

final class p {
    p() {
    }

    public static Animation a(int i, DomobAdView domobAdView) {
        switch (i) {
            case 1:
                o oVar = new o(0.0f, -90.0f, ((float) domobAdView.getWidth()) / 2.0f, ((float) domobAdView.getHeight()) / 2.0f, 0.0f, true);
                oVar.setDuration(500);
                oVar.setFillAfter(true);
                oVar.setInterpolator(new AccelerateInterpolator());
                return oVar;
            case 2:
                o oVar2 = new o(90.0f, 0.0f, ((float) domobAdView.getWidth()) / 2.0f, ((float) domobAdView.getHeight()) / 2.0f, 0.0f, false);
                oVar2.setDuration(500);
                oVar2.setFillAfter(true);
                oVar2.setInterpolator(new DecelerateInterpolator());
                return oVar2;
            case 3:
                AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.2f);
                alphaAnimation.setDuration(1000);
                alphaAnimation.setFillAfter(true);
                alphaAnimation.setInterpolator(new DecelerateInterpolator());
                return alphaAnimation;
            case ReportPolicy.DAILY /*4*/:
                AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.3f, 1.0f);
                alphaAnimation2.setDuration(1600);
                alphaAnimation2.setFillAfter(true);
                alphaAnimation2.setInterpolator(new DecelerateInterpolator());
                return alphaAnimation2;
            case 5:
                ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 0.5f);
                scaleAnimation.setDuration(1000);
                scaleAnimation.setFillAfter(false);
                scaleAnimation.setInterpolator(new DecelerateInterpolator());
                return scaleAnimation;
            case 6:
            default:
                return null;
            case DomobAdView.ANIMATION_TRANSLATE:
                TranslateAnimation translateAnimation = new Random().nextInt(3) == 0 ? new TranslateAnimation(1, -1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f) : new TranslateAnimation(1, 1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                translateAnimation.setDuration(1500);
                translateAnimation.setFillAfter(false);
                translateAnimation.setInterpolator(new DecelerateInterpolator());
                return translateAnimation;
        }
    }

    public static int a(DomobAdView domobAdView) {
        Random random = new Random();
        int nextInt = (random.nextInt(5) << 1) + 1;
        ArrayList<Integer> animList = domobAdView.getAnimList();
        while (animList.size() > 0 && !animList.contains(Integer.valueOf(nextInt))) {
            nextInt = (random.nextInt(5) << 1) + 1;
        }
        if (nextInt == 9) {
            return 7;
        }
        return nextInt;
    }
}
