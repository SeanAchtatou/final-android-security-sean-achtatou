package cn.domob.android.ads;

import android.content.Context;
import android.util.Log;

final class h extends Thread {
    private DomobAdView a;

    public h(DomobAdView domobAdView) {
        this.a = domobAdView;
    }

    public final void run() {
        if (this.a == null) {
            Log.e(Constants.LOG, "GetAdThread exit because adview is null!");
            return;
        }
        Context context = this.a.getContext();
        try {
            DomobAdBuilder domobAdBuilder = new DomobAdBuilder(null, context, this.a);
            j jVar = new j();
            if (jVar.a(DomobAdView.getReceiver(this.a), domobAdBuilder, DomobAdManager.getScreenWidth(context, this.a), DomobAdManager.getScreenHeight(context, this.a)) == null) {
                DomobAdView.failedToReceive(this.a);
                d a2 = jVar.a();
                if (a2 != null) {
                    int e = a2.e();
                    if (e < 200 || e >= 300) {
                        if (Log.isLoggable(Constants.LOG, 3)) {
                            Log.d(Constants.LOG, "connection return error:" + e + ", try detector next time.");
                        }
                        DomobAdView.setNeedDetect(this.a, true);
                    } else if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "connection is OK, continue ad request next time.");
                    }
                }
            }
            DomobAdView.setRequesting(this.a, false);
            DomobAdView.setSchedule(this.a, true);
        } catch (Exception e2) {
            if (DomobAdManager.getPublisherId(context) == null) {
                Log.e(Constants.LOG, "Please set your publisher ID first!");
            } else {
                Log.e(Constants.LOG, "Unkown exception happened!" + e2.getMessage());
            }
            e2.printStackTrace();
        }
    }
}
