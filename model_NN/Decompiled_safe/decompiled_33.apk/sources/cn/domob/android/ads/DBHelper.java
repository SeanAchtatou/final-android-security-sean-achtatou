package cn.domob.android.ads;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

final class DBHelper extends SQLiteOpenHelper {
    protected static Uri a = Uri.parse("content://domob/def_res");
    protected static Uri b = Uri.parse("content://domob/tmp_res");
    private static DBHelper d = null;
    private static Uri f = Uri.parse("content://domob/conf");
    private static final UriMatcher g;
    private Context c;
    private SQLiteDatabase e;

    static {
        UriMatcher uriMatcher = new UriMatcher(-1);
        g = uriMatcher;
        uriMatcher.addURI("domob", "def_res", 1);
        g.addURI("domob", "tmp_res", 2);
        g.addURI("domob", "conf", 3);
    }

    public static final class AdTable implements BaseColumns {
        protected AdTable() {
        }
    }

    public static final class ConfTable implements BaseColumns {
        protected ConfTable() {
        }
    }

    protected static synchronized DBHelper a(Context context) {
        DBHelper dBHelper;
        synchronized (DBHelper.class) {
            if (d == null) {
                d = new DBHelper(context);
            }
            dBHelper = d;
        }
        return dBHelper;
    }

    private DBHelper(Context context) {
        super(context, "domob_ads.db", (SQLiteDatabase.CursorFactory) null, 1);
        this.e = null;
        this.e = getWritableDatabase();
        this.c = context;
    }

    /* access modifiers changed from: protected */
    public final Context a() {
        return this.c;
    }

    public final void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS def_res(_id INTEGER NOT NULL PRIMARY KEY,_name TEXT ,_image BLOB ,_date LONG );");
        db.execSQL("CREATE TABLE IF NOT EXISTS tmp_res(_id INTEGER NOT NULL PRIMARY KEY,_name TEXT ,_image BLOB ,_date LONG );");
        db.execSQL("CREATE TABLE IF NOT EXISTS conf(_id INTEGER NOT NULL PRIMARY KEY,_conf_ver TEXT ,_res_ver TEXT ,_interval INTEGER ,_test_flag BOOLEAN ,_dis_flag BOOLEAN ,_dis_time LONG ,_dis_timestamp LONG ,_uuid TEXT ,_cid TEXT ,_avg_time LONG );");
    }

    public final void onUpgrade(SQLiteDatabase db, int i, int i2) {
        db.execSQL("DROP TABLE IF EXISTS def_res;");
        db.execSQL("DROP TABLE IF EXISTS tmp_res;");
        db.execSQL("DROP TABLE IF EXISTS conf;");
        onCreate(db);
    }

    private Cursor a(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        switch (g.match(uri)) {
            case 1:
                sQLiteQueryBuilder.setTables("def_res");
                break;
            case 2:
                sQLiteQueryBuilder.setTables("tmp_res");
                break;
            case 3:
                sQLiteQueryBuilder.setTables("conf");
                break;
            default:
                throw new UnsupportedOperationException("Query not supported for " + uri);
        }
        return sQLiteQueryBuilder.query(this.e, null, str, null, null, null, str2);
    }

    /* access modifiers changed from: protected */
    public final Uri a(Uri uri, ContentValues contentValues) {
        String str;
        switch (g.match(uri)) {
            case 1:
                str = "def_res";
                break;
            case 2:
                str = "tmp_res";
                break;
            case 3:
                str = "conf";
                break;
            default:
                throw new UnsupportedOperationException("Query not supported for " + uri);
        }
        long insert = this.e.insert(str, null, contentValues);
        if (insert > 0) {
            return ContentUris.withAppendedId(uri, insert);
        }
        Log.e(Constants.LOG, "Insert: failed! " + contentValues.toString());
        return null;
    }

    /* access modifiers changed from: protected */
    public final int a(Uri uri, String str) {
        String str2;
        switch (g.match(uri)) {
            case 1:
                str2 = "def_res";
                break;
            case 2:
                str2 = "tmp_res";
                break;
            case 3:
                str2 = "conf";
                break;
            default:
                throw new UnsupportedOperationException("Query not supported for " + uri);
        }
        return this.e.delete(str2, str, null);
    }

    private int a(Uri uri, ContentValues contentValues, String str) {
        String str2;
        switch (g.match(uri)) {
            case 1:
                str2 = "def_res";
                break;
            case 2:
                str2 = "tmp_res";
                break;
            case 3:
                str2 = "conf";
                break;
            default:
                throw new UnsupportedOperationException("Query not supported for " + uri);
        }
        return this.e.update(str2, contentValues, str, null);
    }

    /* access modifiers changed from: protected */
    public final Cursor b() {
        return a(f, null, null, null, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0081 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void c() {
        /*
            r5 = this;
            monitor-enter(r5)
            r0 = 0
            android.database.Cursor r0 = r5.b()     // Catch:{ Exception -> 0x00a3 }
            if (r0 == 0) goto L_0x000e
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x0097 }
            if (r1 != 0) goto L_0x0086
        L_0x000e:
            java.lang.String r1 = "DomobSDK"
            r2 = 3
            boolean r1 = android.util.Log.isLoggable(r1, r2)     // Catch:{ Exception -> 0x0097 }
            if (r1 == 0) goto L_0x001e
            java.lang.String r1 = "DomobSDK"
            java.lang.String r2 = "initializing conf db!"
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x0097 }
        L_0x001e:
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ Exception -> 0x0097 }
            r1.<init>()     // Catch:{ Exception -> 0x0097 }
            java.lang.String r2 = "2012-12-20:00:00:00"
            java.lang.String r3 = "_conf_ver"
            r1.put(r3, r2)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r3 = "_res_ver"
            r1.put(r3, r2)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r2 = "_dis_flag"
            r3 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0097 }
            r1.put(r2, r3)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r2 = "_dis_time"
            r3 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0097 }
            r1.put(r2, r3)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r2 = "_dis_timestamp"
            r3 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0097 }
            r1.put(r2, r3)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r2 = "_interval"
            r3 = 1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0097 }
            r1.put(r2, r3)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r2 = "_test_flag"
            r3 = 1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0097 }
            r1.put(r2, r3)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r2 = "_avg_time"
            r3 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0097 }
            r1.put(r2, r3)     // Catch:{ Exception -> 0x0097 }
            java.util.UUID r2 = java.util.UUID.randomUUID()     // Catch:{ Exception -> 0x0097 }
            if (r2 == 0) goto L_0x007a
            java.lang.String r3 = "_uuid"
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0097 }
            r1.put(r3, r2)     // Catch:{ Exception -> 0x0097 }
        L_0x007a:
            android.net.Uri r2 = cn.domob.android.ads.DBHelper.f     // Catch:{ Exception -> 0x0097 }
            r5.a(r2, r1)     // Catch:{ Exception -> 0x0097 }
        L_0x007f:
            if (r0 == 0) goto L_0x0084
            r0.close()     // Catch:{ all -> 0x00a0 }
        L_0x0084:
            monitor-exit(r5)
            return
        L_0x0086:
            java.lang.String r1 = "DomobSDK"
            r2 = 3
            boolean r1 = android.util.Log.isLoggable(r1, r2)     // Catch:{ Exception -> 0x0097 }
            if (r1 == 0) goto L_0x007f
            java.lang.String r1 = "DomobSDK"
            java.lang.String r2 = "conf db has already been initialized!"
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x0097 }
            goto L_0x007f
        L_0x0097:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x009b:
            r0.printStackTrace()     // Catch:{ all -> 0x00a0 }
            r0 = r1
            goto L_0x007f
        L_0x00a0:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x00a3:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x009b
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.DBHelper.c():void");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0024 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized int a(android.content.ContentValues r6) {
        /*
            r5 = this;
            r2 = 0
            monitor-enter(r5)
            r0 = -1
            android.database.Cursor r1 = r5.b()     // Catch:{ Exception -> 0x0047 }
            if (r1 == 0) goto L_0x000f
            int r2 = r1.getCount()     // Catch:{ Exception -> 0x0051 }
            if (r2 != 0) goto L_0x0029
        L_0x000f:
            java.lang.String r2 = "DomobSDK"
            r3 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r3)     // Catch:{ Exception -> 0x0051 }
            if (r2 == 0) goto L_0x0056
            java.lang.String r2 = "DomobSDK"
            java.lang.String r3 = "conf db is empty!"
            android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x0051 }
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0022:
            if (r0 == 0) goto L_0x0027
            r0.close()     // Catch:{ all -> 0x004e }
        L_0x0027:
            monitor-exit(r5)
            return r1
        L_0x0029:
            java.lang.String r2 = "DomobSDK"
            r3 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r3)     // Catch:{ Exception -> 0x0051 }
            if (r2 == 0) goto L_0x0039
            java.lang.String r2 = "DomobSDK"
            java.lang.String r3 = "update data in conf db."
            android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x0051 }
        L_0x0039:
            r1.moveToFirst()     // Catch:{ Exception -> 0x0051 }
            android.net.Uri r2 = cn.domob.android.ads.DBHelper.f     // Catch:{ Exception -> 0x0051 }
            r3 = 0
            int r0 = r5.a(r2, r6, r3)     // Catch:{ Exception -> 0x0051 }
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0022
        L_0x0047:
            r1 = move-exception
        L_0x0048:
            r1.printStackTrace()     // Catch:{ all -> 0x004e }
            r1 = r0
            r0 = r2
            goto L_0x0022
        L_0x004e:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x0051:
            r2 = move-exception
            r4 = r2
            r2 = r1
            r1 = r4
            goto L_0x0048
        L_0x0056:
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.DBHelper.a(android.content.ContentValues):int");
    }

    /* access modifiers changed from: protected */
    public final Cursor b(Uri uri, String str) {
        if (uri == null || str == null) {
            return null;
        }
        return a(uri, null, "_name=\"" + str + "\"", null, null);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x005c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(android.net.Uri r10) {
        /*
            r9 = this;
            r7 = 0
            r6 = -1
            java.lang.String r5 = "_date ASC"
            r2 = 0
            r3 = 0
            r4 = 0
            r0 = r9
            r1 = r10
            android.database.Cursor r0 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0060 }
            if (r0 == 0) goto L_0x006d
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x0068 }
            r2 = 20
            if (r1 < r2) goto L_0x006d
            r0.moveToFirst()     // Catch:{ Exception -> 0x0068 }
            java.lang.String r1 = "_name"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x0068 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0068 }
            java.lang.String r3 = "_name=\""
            r2.<init>(r3)     // Catch:{ Exception -> 0x0068 }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x0068 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Exception -> 0x0068 }
            java.lang.String r2 = "\""
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0068 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0068 }
            java.lang.String r2 = "DomobSDK"
            r3 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r3)     // Catch:{ Exception -> 0x0068 }
            if (r2 == 0) goto L_0x0056
            java.lang.String r2 = "DomobSDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0068 }
            java.lang.String r4 = "exceed to max, delete the old cache:"
            r3.<init>(r4)     // Catch:{ Exception -> 0x0068 }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ Exception -> 0x0068 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0068 }
            android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x0068 }
        L_0x0056:
            int r1 = r9.a(r10, r1)     // Catch:{ Exception -> 0x0068 }
        L_0x005a:
            if (r0 == 0) goto L_0x005f
            r0.close()
        L_0x005f:
            return r1
        L_0x0060:
            r0 = move-exception
            r1 = r7
        L_0x0062:
            r0.printStackTrace()
            r0 = r1
            r1 = r6
            goto L_0x005a
        L_0x0068:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0062
        L_0x006d:
            r1 = r6
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.DBHelper.a(android.net.Uri):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void a(cn.domob.android.ads.DBHelper r6, java.lang.String r7, byte[] r8, long r9) {
        /*
            r0 = 0
            android.net.Uri r1 = cn.domob.android.ads.DBHelper.a     // Catch:{ Exception -> 0x00a7 }
            android.database.Cursor r0 = r6.b(r1, r7)     // Catch:{ Exception -> 0x00a7 }
            if (r0 == 0) goto L_0x000f
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x009e }
            if (r1 != 0) goto L_0x004f
        L_0x000f:
            java.lang.String r1 = "DomobSDK"
            r2 = 3
            boolean r1 = android.util.Log.isLoggable(r1, r2)     // Catch:{ Exception -> 0x009e }
            if (r1 == 0) goto L_0x002c
            java.lang.String r1 = "DomobSDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009e }
            java.lang.String r3 = "insert def_res:"
            r2.<init>(r3)     // Catch:{ Exception -> 0x009e }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Exception -> 0x009e }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x009e }
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x009e }
        L_0x002c:
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ Exception -> 0x009e }
            r1.<init>()     // Catch:{ Exception -> 0x009e }
            java.lang.String r2 = "_name"
            r1.put(r2, r7)     // Catch:{ Exception -> 0x009e }
            java.lang.String r2 = "_date"
            java.lang.Long r3 = java.lang.Long.valueOf(r9)     // Catch:{ Exception -> 0x009e }
            r1.put(r2, r3)     // Catch:{ Exception -> 0x009e }
            java.lang.String r2 = "_image"
            r1.put(r2, r8)     // Catch:{ Exception -> 0x009e }
            android.net.Uri r2 = cn.domob.android.ads.DBHelper.a     // Catch:{ Exception -> 0x009e }
            r6.a(r2, r1)     // Catch:{ Exception -> 0x009e }
        L_0x0049:
            if (r0 == 0) goto L_0x004e
            r0.close()
        L_0x004e:
            return
        L_0x004f:
            java.lang.String r1 = "DomobSDK"
            r2 = 3
            boolean r1 = android.util.Log.isLoggable(r1, r2)     // Catch:{ Exception -> 0x009e }
            if (r1 == 0) goto L_0x006c
            java.lang.String r1 = "DomobSDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009e }
            java.lang.String r3 = "update def_res:"
            r2.<init>(r3)     // Catch:{ Exception -> 0x009e }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Exception -> 0x009e }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x009e }
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x009e }
        L_0x006c:
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ Exception -> 0x009e }
            r1.<init>()     // Catch:{ Exception -> 0x009e }
            java.lang.String r2 = "_image"
            r1.put(r2, r8)     // Catch:{ Exception -> 0x009e }
            java.lang.String r2 = "_date"
            java.lang.Long r3 = java.lang.Long.valueOf(r9)     // Catch:{ Exception -> 0x009e }
            r1.put(r2, r3)     // Catch:{ Exception -> 0x009e }
            android.net.Uri r2 = cn.domob.android.ads.DBHelper.a     // Catch:{ Exception -> 0x009e }
            if (r2 == 0) goto L_0x0049
            if (r7 == 0) goto L_0x0049
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009e }
            java.lang.String r4 = "_name=\""
            r3.<init>(r4)     // Catch:{ Exception -> 0x009e }
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ Exception -> 0x009e }
            java.lang.String r4 = "\""
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x009e }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x009e }
            r6.a(r2, r1, r3)     // Catch:{ Exception -> 0x009e }
            goto L_0x0049
        L_0x009e:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x00a2:
            r0.printStackTrace()
            r0 = r1
            goto L_0x0049
        L_0x00a7:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x00a2
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.DBHelper.a(cn.domob.android.ads.DBHelper, java.lang.String, byte[], long):void");
    }
}
