package cn.domob.android.ads;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import cn.domob.android.download.AppExchangeDownloader;

public class DomobActivity extends Activity {
    /* access modifiers changed from: private */
    public String a = "";
    private Context b = this;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        if (getIntent().getExtras() != null) {
            Intent intent = getIntent();
            String stringExtra = intent.getStringExtra(AppExchangeDownloader.App_Name);
            this.a = intent.getStringExtra(AppExchangeDownloader.App_Id);
            intent.getStringExtra(AppExchangeDownloader.App_Download_Path);
            intent.getIntExtra(AppExchangeDownloader.App_Notify_Id, -1024);
            String stringExtra2 = intent.getStringExtra(AppExchangeDownloader.Act_Type);
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, String.valueOf(stringExtra2) + " is showing");
            }
            if (stringExtra2 != null && stringExtra2.equals(AppExchangeDownloader.Type_Cancel)) {
                new AlertDialog.Builder(this.b).setTitle("取消").setMessage(String.valueOf(stringExtra) + "正在下载是否取消?").setNegativeButton("取消下载", new DialogInterface.OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        AppExchangeDownloader appExchangeDownloader = AppExchangeDownloader.Download_Map.get(DomobActivity.this.a);
                        if (appExchangeDownloader != null) {
                            appExchangeDownloader.stopDownload();
                        }
                        DomobActivity.this.finish();
                    }
                }).setPositiveButton("继续下载", new DialogInterface.OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        DomobActivity.this.finish();
                    }
                }).show();
            }
        }
    }
}
