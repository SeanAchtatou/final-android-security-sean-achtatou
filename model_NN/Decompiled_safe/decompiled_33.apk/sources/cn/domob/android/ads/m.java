package cn.domob.android.ads;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import java.lang.reflect.Array;
import java.util.ArrayList;

final class m extends Animation {
    private static int c;
    private static int d;
    /* access modifiers changed from: private */
    public ImageView[][] a = ((ImageView[][]) Array.newInstance(ImageView.class, 3, 16));
    private Handler b = new Handler();
    /* access modifiers changed from: private */
    public DomobAdView e;
    /* access modifiers changed from: private */
    public DomobAdBuilder f;
    /* access modifiers changed from: private */
    public DomobAdBuilder g;

    m() {
    }

    /* access modifiers changed from: protected */
    public final void a(DomobAdView domobAdView, DomobAdBuilder domobAdBuilder, DomobAdBuilder domobAdBuilder2) {
        c = domobAdBuilder2.getWidth();
        d = domobAdBuilder2.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(domobAdBuilder2.getWidth(), domobAdBuilder2.getHeight(), Bitmap.Config.RGB_565);
        domobAdView.draw(new Canvas(createBitmap));
        this.e = domobAdView;
        this.f = domobAdBuilder;
        this.g = domobAdBuilder2;
        int i = 0;
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < d; i2 += d / 3) {
            for (int i3 = 0; i3 < c; i3 += c / 16) {
                arrayList.add(Bitmap.createBitmap(createBitmap, i3, i2, c / 16, d / 3));
            }
        }
        int i4 = 1010101010;
        for (int i5 = 0; i5 < 3; i5++) {
            int i6 = 0;
            while (i6 < 16) {
                this.a[i5][i6] = new ImageView(domobAdView.getContext());
                this.a[i5][i6].setImageBitmap((Bitmap) arrayList.get(i));
                int i7 = i4 + 1;
                this.a[i5][i6].setId(i4);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                if (i5 != 0 || i6 != 0) {
                    if (i6 == 0) {
                        layoutParams.addRule(3, this.a[i5 - 1][i6].getId());
                    } else {
                        layoutParams.addRule(1, this.a[i5][i6 - 1].getId());
                        layoutParams.addRule(8, this.a[i5][i6 - 1].getId());
                    }
                }
                domobAdView.addView(this.a[i5][i6], layoutParams);
                i6++;
                i++;
                i4 = i7;
            }
        }
        int i8 = 0;
        while (true) {
            int i9 = i8;
            if (i9 >= 3) {
                this.b.postDelayed(new a(this), 2000);
                return;
            }
            int i10 = 0;
            while (true) {
                int i11 = i10;
                if (i11 >= 16) {
                    break;
                }
                if (i9 <= 0 && i11 < 8) {
                    a(new TranslateAnimation(1, 0.0f, 1, -1.0f + ((float) Math.random()), 1, 0.0f, 1, -1.0f + ((float) Math.random())), i9, i11);
                }
                if (i9 <= 0 && i11 >= 8) {
                    a(new TranslateAnimation(1, 0.0f, 1, 1.0f - ((float) Math.random()), 1, 0.0f, 1, -1.0f + ((float) Math.random())), i9, i11);
                }
                if (i9 > 0 && i11 < 8) {
                    a(new TranslateAnimation(1, 0.0f, 1, -1.0f + ((float) Math.random()), 1, 0.0f, 1, 1.0f - ((float) Math.random())), i9, i11);
                }
                if (i9 > 0 && i11 >= 8) {
                    a(new TranslateAnimation(1, 0.0f, 1, 1.0f - ((float) Math.random()), 1, 0.0f, 1, 1.0f - ((float) Math.random())), i9, i11);
                }
                i10 = i11 + 1;
            }
            i8 = i9 + 1;
        }
    }

    private void a(TranslateAnimation translateAnimation, int i, int i2) {
        translateAnimation.setDuration(2000);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(2000);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(translateAnimation);
        animationSet.addAnimation(alphaAnimation);
        this.a[i][i2].startAnimation(animationSet);
    }

    class a implements Runnable {
        /* synthetic */ a(m mVar) {
            this((byte) 0);
        }

        private a(byte b) {
        }

        public final void run() {
            for (int i = 0; i < 3; i++) {
                for (int i2 = 0; i2 < 16; i2++) {
                    m.this.e.removeView(m.this.a[i][i2]);
                }
            }
            try {
                m.this.f.setVisibility(0);
                DomobAdView.setBuilder(m.this.e, m.this.f);
                AlphaAnimation alphaAnimation = new AlphaAnimation(0.3f, 1.0f);
                alphaAnimation.setDuration(1600);
                m.this.f.startAnimation(alphaAnimation);
                m.this.e.removeView(m.this.g);
                m.this.g.c();
            } catch (Exception e) {
                Log.e(Constants.LOG, "ReplaceBuilderThread error " + e.getMessage());
            }
        }
    }
}
