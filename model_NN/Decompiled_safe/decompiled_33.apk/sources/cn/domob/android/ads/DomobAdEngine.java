package cn.domob.android.ads;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import cn.domob.android.ads.giftool.GifView;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class DomobAdEngine implements a {
    private static final int a = Color.rgb(102, 102, 102);
    private Rect b = null;
    private DomobAdBuilder c;
    private float d = -1.0f;
    private boolean e = false;
    private RecvHandler f = null;
    private e g = null;
    private long h = -1;
    private long i = -8682592;
    private boolean j;
    private boolean k;
    private int l = -1;
    private int m = -1;
    private Vector<Bitmap> n = new Vector<>();
    private Vector<d> o = new Vector<>();
    private Vector<Intent> p = new Vector<>();
    private HashSet<d> q = new HashSet<>();
    private Hashtable<String, byte[]> r = new Hashtable<>();
    private Hashtable<String, String> s = new Hashtable<>();
    private Hashtable<String, String> t = new Hashtable<>();
    private String u = null;
    private JSONObject v;
    private b w = new b();
    private HashMap<String, Boolean> x = new HashMap<>();
    private Vector<View> y = new Vector<>();

    interface e {
        void f();
    }

    class a implements a {
        a(DomobAdEngine domobAdEngine) {
        }

        a() {
        }

        protected static d a(String str, String str2, String str3, String str4, a aVar, int i, String str5) {
            return new q(str, str2, str3, str4, aVar, 30000, null, str5);
        }

        protected static d a(String str, String str2, String str3, String str4, a aVar, String str5) {
            return a(str, str2, str3, str4, aVar, 30000, str5);
        }

        protected static d a(String str, String str2, String str3, String str4, a aVar) {
            return a(str, str2, str3, str4, aVar, 30000, null);
        }

        protected static d a(String str, String str2, String str3, String str4) {
            return a(str, null, str3, str4, null);
        }

        protected static d a(String str, String str2) {
            return a(str, null, null, null, null, 30000, str2);
        }

        public void a(d dVar) {
        }
    }

    static class d {
        public String a;

        d() {
        }

        public d(String str, boolean z) {
            this.a = str;
        }
    }

    protected DomobAdEngine() {
    }

    protected static DomobAdEngine a(RecvHandler recvHandler, JSONObject jSONObject, DomobAdBuilder domobAdBuilder) {
        if (jSONObject == null || jSONObject.length() == 0) {
            Log.e(Constants.LOG, "failed to init engine, jsonobject is null!");
            return null;
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "init engine now.");
        }
        DomobAdEngine domobAdEngine = new DomobAdEngine();
        domobAdEngine.f = recvHandler;
        domobAdEngine.c = domobAdBuilder;
        if (recvHandler.getAdView().isDataMode()) {
            return domobAdEngine;
        }
        domobAdEngine.h = recvHandler.getAdView().getPrimaryTextColor();
        domobAdEngine.i = recvHandler.getAdView().getBackgroundColor();
        domobAdEngine.j = recvHandler.getAdView().getBackColorSetByClient();
        domobAdEngine.k = recvHandler.getAdView().getPrimTxtColorSetByClient();
        if (!domobAdEngine.c(jSONObject)) {
            return null;
        }
        if (!Log.isLoggable(Constants.LOG, 3)) {
            return domobAdEngine;
        }
        Log.d(Constants.LOG, "success to parse ad response.");
        return domobAdEngine;
    }

    /* access modifiers changed from: protected */
    public final void a(JSONObject jSONObject) {
        int i2 = 20;
        if (this.f.getAdView().isDataMode() && this.o.size() > 0) {
            String str = this.o.get(0).a;
            int length = str.length();
            if (length < 20) {
                i2 = length;
            }
            String substring = str.substring(length - i2, length);
            if (!this.x.containsKey(substring)) {
                this.x.put(substring, true);
                this.e = false;
            }
        }
        if (this.e) {
            Log.w(Constants.LOG, "already clicked, ignore it.");
        } else {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "report clicked now.");
            }
            this.e = true;
            if (!(this.c == null || jSONObject == null)) {
                Context context = this.c.getContext();
                Iterator<d> it = this.o.iterator();
                while (it.hasNext()) {
                    d next = it.next();
                    a aVar = new a(this);
                    String a2 = j.a(context, jSONObject.toString());
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "click report:" + a2);
                    }
                    d a3 = a.a(next.a, "click_tracking", j.a(context), DomobAdManager.getDeviceId(context), aVar, a2);
                    a3.a(context);
                    a3.b();
                }
            }
        }
        g();
        if (this.g != null) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "show click animation.");
            }
            this.g.f();
        }
    }

    private boolean f() {
        boolean z = this.q == null || this.q.size() == 0;
        if (!z && Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "there are " + this.q.size() + " connection left.");
        }
        return z;
    }

    private void g() {
        Context context;
        if (this.c != null && (context = this.c.getContext()) != null) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "do click action now.");
            }
            PackageManager packageManager = context.getPackageManager();
            Iterator<Intent> it = this.p.iterator();
            while (it.hasNext()) {
                Intent next = it.next();
                Bundle extras = next.getExtras();
                String string = extras.getString("opentype");
                String string2 = extras.getString("type");
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "intent's opentype:" + string);
                }
                if (packageManager.resolveActivity(next, 65536) != null) {
                    try {
                        if (!DomobAdManager.ACTION_URL.equals(string2)) {
                            context.startActivity(next);
                        } else if (string == null || string.equals("inapp_fs")) {
                            new DomobInApp().bulidClickedDialog(context, next.getDataString(), new ProgressDialog(context), this.c.b).show();
                            return;
                        } else if (string.equals("outapp")) {
                            context.startActivity(next);
                            return;
                        } else if (string.equals("inapp_dlg")) {
                            new DomobInApp().bulidClickedImage(this.c.b, next.getDataString(), new ProgressDialog(context)).show();
                            return;
                        } else {
                            return;
                        }
                    } catch (Exception e2) {
                        Log.e(Constants.LOG, "error happened in doAction!");
                        e2.printStackTrace();
                    }
                } else {
                    Log.e(Constants.LOG, "activity cannot be resolved, intent.getAction() =  " + next.getAction());
                }
            }
        }
    }

    private void b(JSONObject jSONObject) {
        String str;
        String str2;
        String str3;
        if (jSONObject != null) {
            String optString = jSONObject.optString("a", null);
            String optString2 = jSONObject.optString("opentype", null);
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "OpenType is:" + optString2);
            }
            if (optString == null) {
                Log.e(Constants.LOG, "invalid action type!");
                return;
            }
            String optString3 = jSONObject.optString("d", null);
            if (DomobAdManager.ACTION_MAP.equals(optString)) {
                if (optString3 == null) {
                    Log.e(Constants.LOG, "map data is null!");
                    return;
                }
                str = "android.intent.action.VIEW";
                str2 = "geo:" + optString3;
                str3 = null;
            } else if (DomobAdManager.ACTION_SMS.equals(optString)) {
                if (optString3 == null) {
                    Log.e(Constants.LOG, "smsto is null!");
                    return;
                }
                str = "android.intent.action.SENDTO";
                str2 = "smsto:" + optString3;
                str3 = "sms_body";
            } else if (DomobAdManager.ACTION_MAIL.equals(optString)) {
                if (optString3 == null) {
                    Log.e(Constants.LOG, "mailto is null!");
                    return;
                }
                str = "android.intent.action.SENDTO";
                str2 = "mailto:" + optString3;
                str3 = "android.intent.extra.TEXT";
            } else if (DomobAdManager.ACTION_URL.equals(optString) || DomobAdManager.ACTION_MARKET.equals(optString)) {
                if (optString3 == null) {
                    Log.e(Constants.LOG, "url is null!");
                    return;
                }
                str = "android.intent.action.VIEW";
                str2 = optString3;
                str3 = null;
            } else if (DomobAdManager.ACTION_CALL.equals(optString)) {
                if (optString3 == null) {
                    Log.e(Constants.LOG, "tel number is null!");
                    return;
                }
                str = "android.intent.action.DIAL";
                str2 = "tel:" + optString3;
                str3 = null;
            } else if (!DomobAdManager.ACTION_VIDEO.equals(optString) && !DomobAdManager.ACTION_AUDIO.equals(optString)) {
                Log.e(Constants.LOG, "unknown action type!");
                return;
            } else if (optString3 == null) {
                Log.e(Constants.LOG, "url is null!");
                return;
            } else {
                str = "android.intent.action.VIEW";
                str2 = optString3;
                str3 = null;
            }
            int optInt = jSONObject.optInt(DomobAdManager.GENDER_FEMALE, 268435456);
            String optString4 = jSONObject.optString("b", null);
            Uri parse = Uri.parse(str2);
            if (parse == null) {
                Log.e(Constants.LOG, "intent uri is null");
            } else if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "intent uri:" + parse.toString());
            }
            Intent intent = new Intent(str, parse);
            if (optInt != 0) {
                intent.addFlags(optInt);
            }
            if (DomobAdManager.ACTION_URL.equals(optString)) {
                Bundle bundle = new Bundle();
                bundle.putString("opentype", optString2);
                intent.putExtras(bundle);
            }
            Bundle bundle2 = new Bundle();
            bundle2.putString("type", optString);
            intent.putExtras(bundle2);
            if (!(optString4 == null || str3 == null)) {
                intent.putExtra(str3, optString4);
            }
            String optString5 = jSONObject.optString("s", null);
            if (DomobAdManager.ACTION_MAIL.equals(optString) && optString5 != null) {
                intent.putExtra("android.intent.extra.SUBJECT", optString5);
            }
            this.p.add(intent);
        }
    }

    /* access modifiers changed from: protected */
    public final int a(int i2) {
        return (int) (((double) this.d) <= 0.0d ? (float) i2 : ((float) i2) * this.d);
    }

    /* access modifiers changed from: protected */
    public final DomobAdBuilder a() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public final void a(DomobAdBuilder domobAdBuilder) {
        this.c = domobAdBuilder;
        if (this.g == null) {
            this.g = domobAdBuilder;
        }
    }

    /* access modifiers changed from: protected */
    public final int b() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public final int c() {
        return this.m;
    }

    /* access modifiers changed from: protected */
    public final Rect d() {
        if (this.b == null) {
            this.b = new Rect(0, 0, a(this.l), a(this.m));
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "getRect :" + this.b.left + "," + this.b.top + "," + this.b.right + "," + this.b.bottom);
        }
        return this.b;
    }

    /* access modifiers changed from: protected */
    public final void e() {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "clear engine resources.");
        }
        if (this.p != null) {
            this.p.clear();
            this.p = null;
        }
        if (this.o != null) {
            this.o.clear();
            this.o = null;
        }
        if (this.n != null) {
            Iterator<Bitmap> it = this.n.iterator();
            while (it.hasNext()) {
                Bitmap next = it.next();
                if (next != null) {
                    next.recycle();
                }
            }
            this.n.clear();
            this.n = null;
        }
        if (this.y != null) {
            Iterator<View> it2 = this.y.iterator();
            while (it2.hasNext()) {
                View next2 = it2.next();
                if (next2 != null) {
                    if (next2 instanceof GifView) {
                        ((GifView) next2).clear();
                    } else if (next2 instanceof ImageView) {
                        ((ImageView) next2).setImageBitmap(null);
                    }
                }
            }
            this.y.clear();
            this.y = null;
        }
    }

    private boolean c(JSONObject jSONObject) {
        String optString = jSONObject.optString("rp_url", null);
        if (optString == null) {
            Log.e(Constants.LOG, "there is no jsonpurl which is required!");
            return false;
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "parse response now.");
        }
        if (optString != null && !optString.equals("")) {
            this.o.add(new d(optString, false));
        }
        this.u = jSONObject.optString("identifier", null);
        if (this.u == null || this.u.equals("nullad")) {
            Log.e(Constants.LOG, "ad identifier is null, ignore it!");
            return false;
        }
        this.d = DomobAdBuilder.d();
        PointF a2 = a(jSONObject, "d", (PointF) null);
        if (a2 == null) {
            a2 = new PointF(320.0f, 48.0f);
        }
        if (a2.x < 0.0f || a2.y < 0.0f) {
            return false;
        }
        this.l = (int) a2.x;
        this.m = (int) a2.y;
        JSONObject optJSONObject = jSONObject.optJSONObject("ac");
        if (optJSONObject != null) {
            b(optJSONObject);
        }
        JSONArray optJSONArray = jSONObject.optJSONArray("ac");
        if (optJSONArray != null) {
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                try {
                    b(optJSONArray.getJSONObject(i2));
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
            }
        }
        JSONObject optJSONObject2 = jSONObject.optJSONObject("markup");
        if (optJSONObject2 == null) {
            Log.e(Constants.LOG, "there is no markup which is required!");
            return false;
        }
        this.v = optJSONObject2;
        try {
            h();
            i();
            if (f()) {
                j();
            }
            return true;
        } catch (Exception e3) {
            e3.printStackTrace();
            return false;
        }
    }

    private void h() throws JSONException {
        JSONObject optJSONObject = this.v.optJSONObject("$");
        if (optJSONObject != null) {
            Iterator<String> keys = optJSONObject.keys();
            if (this.q != null) {
                synchronized (this.q) {
                    if (this.c != null) {
                        Context context = this.c.getContext();
                        DBHelper a2 = DBHelper.a(context);
                        while (keys.hasNext()) {
                            String next = keys.next();
                            String optString = optJSONObject.getJSONObject(next).optString("u", null);
                            if (!(next == null || optString == null)) {
                                int lastIndexOf = optString.lastIndexOf(47);
                                if (lastIndexOf > 0 && lastIndexOf + 1 < optString.length()) {
                                    String substring = optString.substring(lastIndexOf + 1);
                                    if (Log.isLoggable(Constants.LOG, 3)) {
                                        Log.d(Constants.LOG, "image name:" + substring);
                                    }
                                    this.s.put(next, substring);
                                    this.t.put(next, optString.substring(optString.lastIndexOf(".") + 1, optString.length()));
                                    if (next.startsWith("def_")) {
                                        b bVar = this.w;
                                        if (b.b(a2, next, substring, this.r)) {
                                            if (Log.isLoggable(Constants.LOG, 3)) {
                                                Log.d(Constants.LOG, "load " + substring + " from resources.");
                                            }
                                        }
                                    } else {
                                        b bVar2 = this.w;
                                        if (b.a(a2, next, substring, this.r)) {
                                            if (Log.isLoggable(Constants.LOG, 3)) {
                                                Log.d(Constants.LOG, "load " + substring + " from cache.");
                                            }
                                        }
                                    }
                                }
                                if (Log.isLoggable(Constants.LOG, 3)) {
                                    Log.d(Constants.LOG, "need download " + next + " from server " + optString);
                                }
                                d a3 = a.a(optString, next, j.a(context), DomobAdManager.getDeviceId(context), this);
                                a3.a(context);
                                this.q.add(a3);
                            }
                        }
                    }
                }
            }
        }
    }

    private void i() {
        if (this.q != null) {
            synchronized (this.q) {
                Iterator<d> it = this.q.iterator();
                while (it.hasNext()) {
                    it.next().b();
                }
            }
        }
    }

    private void j() {
        k kVar;
        String str;
        Typeface typeface;
        int i2;
        if (!this.f.getAdView().isDataMode()) {
            if (this.v == null) {
                Log.e(Constants.LOG, "can not build view without markup!");
                return;
            }
            boolean z = true;
            try {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "build view with markup.");
                }
                JSONArray jSONArray = this.v.getJSONArray("v");
                if (jSONArray != null) {
                    b bVar = new b(this.c, this);
                    int i3 = 0;
                    while (true) {
                        if (i3 >= jSONArray.length()) {
                            break;
                        }
                        JSONObject jSONObject = jSONArray.getJSONObject(i3);
                        Vector vector = null;
                        String str2 = null;
                        String string = jSONObject.getString("t");
                        Rect a2 = a(jSONObject, DomobAdManager.GENDER_FEMALE, this.b);
                        if ("l".equals(string)) {
                            if (Log.isLoggable(Constants.LOG, 3)) {
                                Log.d(Constants.LOG, "build text view.");
                            }
                            if (this.c != null) {
                                float a3 = a(jSONObject, "fs", 14.0f);
                                JSONArray optJSONArray = jSONObject.optJSONArray("fa");
                                JSONArray optJSONArray2 = jSONObject.optJSONArray("x");
                                Typeface typeface2 = Typeface.DEFAULT;
                                if (optJSONArray != null) {
                                    int i4 = 0;
                                    for (int i5 = 0; i5 < optJSONArray.length(); i5++) {
                                        String string2 = optJSONArray.getString(i5);
                                        if (Log.isLoggable(Constants.LOG, 3)) {
                                            Log.d(Constants.LOG, "fa:[" + i5 + "] = " + string2);
                                        }
                                        if ("b".equals(string2)) {
                                            i4 |= 1;
                                        } else if ("i".equals(string2)) {
                                            i4 |= 2;
                                        } else if (DomobAdManager.GENDER_MALE.equals(string2)) {
                                            typeface2 = Typeface.MONOSPACE;
                                        } else if ("s".equals(string2)) {
                                            typeface2 = Typeface.SERIF;
                                        } else if ("ss".equals(string2)) {
                                            typeface2 = Typeface.SANS_SERIF;
                                        }
                                    }
                                    typeface = Typeface.create(typeface2, i4);
                                } else {
                                    typeface = typeface2;
                                }
                                String optString = jSONObject.optString("fc", null);
                                int i6 = (int) this.h;
                                if (optString == null || this.k) {
                                    i2 = i6;
                                } else {
                                    i2 = (int) Long.parseLong(optString, 16);
                                }
                                boolean optBoolean = jSONObject.optBoolean("dnv", true);
                                float a4 = a(jSONObject, "mfs", 12.0f);
                                if (jSONObject.has("at")) {
                                    str2 = jSONObject.getString("at");
                                }
                                if (optJSONArray2 != null) {
                                    if (optJSONArray2.length() > 1) {
                                        vector = new Vector();
                                    }
                                    k kVar2 = null;
                                    for (int i7 = 0; i7 < optJSONArray2.length(); i7++) {
                                        String string3 = optJSONArray2.getString(i7);
                                        k kVar3 = new k(this.c.getContext(), DomobAdBuilder.d());
                                        kVar3.b = optBoolean;
                                        kVar3.a = kVar3.c * a4;
                                        kVar3.setText(string3);
                                        kVar3.setTextColor(i2);
                                        kVar3.setTextSize(1, a3);
                                        kVar3.setTypeface(typeface);
                                        kVar3.setSingleLine();
                                        kVar3.setEllipsize(TextUtils.TruncateAt.END);
                                        if (vector != null) {
                                            vector.add(kVar3);
                                        } else {
                                            kVar2 = kVar3;
                                        }
                                    }
                                    str = str2;
                                    kVar = kVar2;
                                } else {
                                    String str3 = str2;
                                    kVar = null;
                                    str = str3;
                                }
                            }
                            kVar = null;
                            str = null;
                        } else if ("bg".equals(string)) {
                            if (Log.isLoggable(Constants.LOG, 3)) {
                                Log.d(Constants.LOG, "build background view.");
                            }
                            kVar = a(jSONObject, a2);
                            str = null;
                        } else {
                            if ("i".equals(string)) {
                                if (Log.isLoggable(Constants.LOG, 3)) {
                                    Log.d(Constants.LOG, "build image view.");
                                }
                                View d2 = d(jSONObject);
                                this.y.add(d2);
                                kVar = d2;
                                str = null;
                            }
                            kVar = null;
                            str = null;
                        }
                        if (kVar == null && vector == null) {
                            Log.d(Constants.LOG, "failed to build view");
                            z = false;
                        } else {
                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a2.width(), a2.height());
                            layoutParams.addRule(9);
                            layoutParams.addRule(10);
                            layoutParams.setMargins(a2.left, a2.top, 0, 0);
                            if (vector != null) {
                                Iterator it = vector.iterator();
                                while (it.hasNext()) {
                                    bVar.a.add(a((View) it.next(), layoutParams, str));
                                }
                            } else if (kVar != null) {
                                bVar.a.add(a(kVar, layoutParams, str));
                                if (jSONObject.optBoolean("cav") && this.c != null) {
                                    this.c.a(kVar);
                                }
                            }
                            z = true;
                        }
                        if (!z) {
                            Log.e(Constants.LOG, "failed to build view, callback!");
                            if (this.f != null) {
                                this.f.failed();
                            }
                        } else {
                            i3++;
                        }
                    }
                    if (z) {
                        if (this.c != null) {
                            this.c.a(this.v.optString("tat", null));
                        }
                        DomobAdView.a.post(bVar);
                    }
                } else if (this.f != null) {
                    Log.e(Constants.LOG, "can not build view because jason array of views is null!");
                    this.f.failed();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                z = false;
            }
            if (!z) {
                Log.d(Constants.LOG, "failed to build view, clear all bmps and views!");
                e();
            }
            if (this.q != null) {
                this.q.clear();
                this.q = null;
            }
            if (this.r != null) {
                this.r.clear();
                this.r = null;
            }
            if (this.s != null) {
                this.s.clear();
                this.s = null;
            }
        }
    }

    private static c a(View view, RelativeLayout.LayoutParams layoutParams, String str) {
        c cVar = new c();
        cVar.a = view;
        cVar.b = layoutParams;
        cVar.c = str;
        return cVar;
    }

    private View a(JSONObject jSONObject, Rect rect) throws JSONException {
        if (this.c != null) {
            try {
                float a2 = a(jSONObject, "ia", 1.0f);
                float a3 = a(jSONObject, "epy", 1.0f);
                int i2 = (int) this.i;
                String optString = jSONObject.optString("bc", null);
                if (optString != null && !optString.equals("") && !this.j) {
                    i2 = (int) Long.parseLong(optString, 16);
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "bgcolor is:" + i2);
                    }
                }
                Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
                this.n.add(createBitmap);
                Canvas canvas = new Canvas(createBitmap);
                int height = rect.top + ((int) (a3 * ((float) rect.height())));
                Rect rect2 = new Rect(rect.left, rect.top, rect.right, height);
                Paint paint = new Paint();
                paint.setColor(-1);
                paint.setStyle(Paint.Style.FILL);
                canvas.drawRect(rect2, paint);
                GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb((int) (a2 * 255.0f), Color.red(i2), Color.green(i2), Color.blue(i2)), i2});
                gradientDrawable.setBounds(rect2);
                gradientDrawable.draw(canvas);
                Rect rect3 = new Rect(rect.left, height, rect.right, rect.bottom);
                Paint paint2 = new Paint();
                paint2.setColor(i2);
                paint2.setStyle(Paint.Style.FILL);
                canvas.drawRect(rect3, paint2);
                View view = new View(this.c.getContext());
                view.setBackgroundDrawable(new BitmapDrawable(createBitmap));
                return view;
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return null;
    }

    private View d(JSONObject jSONObject) throws JSONException {
        DomobImageView domobImageView;
        DomobImageView domobImageView2;
        int i2;
        if (this.c != null) {
            DBHelper a2 = DBHelper.a(this.c.getContext());
            String string = jSONObject.getString("$");
            if (string != null) {
                if (string.startsWith("def_")) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "load " + string + " from resources.");
                    }
                    String str = String.valueOf(string) + ".png";
                    b bVar = this.w;
                    if (!b.b(a2, string, str, this.r)) {
                        if (Log.isLoggable(Constants.LOG, 3)) {
                            Log.d(Constants.LOG, "failed to load image from resources, try the backup.");
                        }
                        String optString = jSONObject.optString("def", null);
                        if (optString == null) {
                            Log.e(Constants.LOG, "no defined backup default resources!");
                            return null;
                        }
                        b bVar2 = this.w;
                        if (!b.b(a2, string, String.valueOf(optString) + ".png", this.r)) {
                            Log.e(Constants.LOG, "failed to load backup default resources!");
                            return null;
                        }
                    }
                }
                byte[] bArr = this.r.get(string);
                if (bArr != null) {
                    try {
                        Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
                        if (decodeByteArray == null) {
                            Log.e(Constants.LOG, "failed to decode Bitmap!");
                            return null;
                        }
                        if (this.t.get(string).endsWith("gif")) {
                            if (Log.isLoggable(Constants.LOG, 3)) {
                                Log.d(Constants.LOG, String.valueOf(string) + " is gif");
                            }
                            i2 = 1;
                        } else {
                            i2 = 0;
                        }
                        DomobImageView domobImageView3 = new DomobImageView(this.c.getContext(), i2);
                        try {
                            domobImageView3.setScaleType(ImageView.ScaleType.FIT_XY);
                            jSONObject.optBoolean("b", false);
                            this.n.add(decodeByteArray);
                            domobImageView3.setImageBitmap(decodeByteArray, bArr);
                            domobImageView = domobImageView3;
                        } catch (Throwable th) {
                            th = th;
                            domobImageView2 = domobImageView3;
                            th.printStackTrace();
                            domobImageView = domobImageView2;
                            return domobImageView.getCurrentView();
                        }
                        return domobImageView.getCurrentView();
                    } catch (Throwable th2) {
                        th = th2;
                        domobImageView2 = null;
                        th.printStackTrace();
                        domobImageView = domobImageView2;
                        return domobImageView.getCurrentView();
                    }
                }
            } else {
                Log.e(Constants.LOG, "can not create an imageView without $");
            }
        }
        domobImageView = null;
        return domobImageView.getCurrentView();
    }

    private static float a(JSONObject jSONObject, String str, float f2) {
        return (float) jSONObject.optDouble(str, (double) f2);
    }

    private Rect a(JSONObject jSONObject, String str, Rect rect) {
        Rect rect2;
        if (jSONObject == null || !jSONObject.has(str)) {
            return rect;
        }
        try {
            JSONArray jSONArray = jSONObject.getJSONArray(str);
            int i2 = (int) jSONArray.getDouble(0);
            int i3 = (int) jSONArray.getDouble(1);
            Rect rect3 = new Rect(a(i2), a(i3), a(((int) jSONArray.getDouble(2)) + i2), a(((int) jSONArray.getDouble(3)) + i3));
            try {
                if (!Log.isLoggable(Constants.LOG, 3)) {
                    return rect3;
                }
                Log.d(Constants.LOG, "getRect :" + rect3.left + "," + rect3.top + "," + rect3.right + "," + rect3.bottom);
                return rect3;
            } catch (JSONException e2) {
                e = e2;
                rect2 = rect3;
                e.printStackTrace();
                return rect2;
            }
        } catch (JSONException e3) {
            e = e3;
            rect2 = rect;
            e.printStackTrace();
            return rect2;
        }
    }

    private static PointF a(JSONObject jSONObject, String str, PointF pointF) {
        if (jSONObject != null && jSONObject.has(str)) {
            try {
                JSONArray jSONArray = jSONObject.getJSONArray(str);
                return new PointF((float) jSONArray.getDouble(0), (float) jSONArray.getDouble(1));
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        return pointF;
    }

    static class b implements Runnable {
        Vector<c> a = new Vector<>();
        private DomobAdBuilder b;
        private DomobAdEngine c;

        public b(DomobAdBuilder domobAdBuilder, DomobAdEngine domobAdEngine) {
            this.b = domobAdBuilder;
            this.c = domobAdEngine;
        }

        public final void run() {
            l lVar;
            try {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "AddViewThread run");
                }
                RelativeLayout.LayoutParams layoutParams = null;
                if (this.b != null) {
                    this.b.setPadding(0, 0, 0, 0);
                    Iterator<c> it = this.a.iterator();
                    l lVar2 = null;
                    while (it.hasNext()) {
                        c next = it.next();
                        if (next.a == null || next.b == null) {
                            if (Log.isLoggable(Constants.LOG, 3)) {
                                Log.d(Constants.LOG, "Basic View element is null, continue.");
                            }
                        } else if (next.c != null) {
                            if (lVar2 == null) {
                                lVar = new l(this.b.getContext());
                                AnimationSet animationSet = null;
                                if ("l2r".equals(next.c)) {
                                    if (Log.isLoggable(Constants.LOG, 3)) {
                                        Log.d(Constants.LOG, "set text anim:" + next.c);
                                    }
                                    TranslateAnimation translateAnimation = new TranslateAnimation(-100.0f, 0.0f, 0.0f, 0.0f);
                                    translateAnimation.setDuration(3000);
                                    translateAnimation.setInterpolator(new AccelerateInterpolator());
                                    translateAnimation.setFillAfter(true);
                                    AnimationSet animationSet2 = new AnimationSet(true);
                                    animationSet2.addAnimation(translateAnimation);
                                    animationSet = animationSet2;
                                } else if ("r2l".equals(next.c)) {
                                    if (Log.isLoggable(Constants.LOG, 3)) {
                                        Log.d(Constants.LOG, "set text anim:" + next.c);
                                    }
                                    TranslateAnimation translateAnimation2 = new TranslateAnimation(100.0f, 0.0f, 0.0f, 0.0f);
                                    translateAnimation2.setDuration(3000);
                                    translateAnimation2.setInterpolator(new AccelerateInterpolator());
                                    translateAnimation2.setFillAfter(true);
                                    AnimationSet animationSet3 = new AnimationSet(true);
                                    animationSet3.addAnimation(translateAnimation2);
                                    animationSet = animationSet3;
                                } else if ("t2b".equals(next.c)) {
                                    if (Log.isLoggable(Constants.LOG, 3)) {
                                        Log.d(Constants.LOG, "set text anim:" + next.c);
                                    }
                                    TranslateAnimation translateAnimation3 = new TranslateAnimation(0.0f, 0.0f, -50.0f, 0.0f);
                                    translateAnimation3.setDuration(2500);
                                    translateAnimation3.setInterpolator(new AccelerateInterpolator());
                                    translateAnimation3.setFillAfter(true);
                                    AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                                    alphaAnimation.setDuration(500);
                                    alphaAnimation.setInterpolator(new AccelerateInterpolator());
                                    alphaAnimation.setFillAfter(true);
                                    AnimationSet animationSet4 = new AnimationSet(true);
                                    animationSet4.addAnimation(translateAnimation3);
                                    animationSet4.addAnimation(alphaAnimation);
                                    animationSet = animationSet4;
                                } else if ("b2t".equals(next.c)) {
                                    if (Log.isLoggable(Constants.LOG, 3)) {
                                        Log.d(Constants.LOG, "set text anim:" + next.c);
                                    }
                                    TranslateAnimation translateAnimation4 = new TranslateAnimation(0.0f, 0.0f, 50.0f, 0.0f);
                                    translateAnimation4.setDuration(2500);
                                    translateAnimation4.setInterpolator(new AccelerateInterpolator());
                                    translateAnimation4.setFillAfter(true);
                                    AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
                                    alphaAnimation2.setDuration(500);
                                    alphaAnimation2.setInterpolator(new AccelerateInterpolator());
                                    alphaAnimation2.setFillAfter(true);
                                    AnimationSet animationSet5 = new AnimationSet(true);
                                    animationSet5.addAnimation(translateAnimation4);
                                    animationSet5.addAnimation(alphaAnimation2);
                                    animationSet = animationSet5;
                                } else if ("shx".equals(next.c)) {
                                    if (Log.isLoggable(Constants.LOG, 3)) {
                                        Log.d(Constants.LOG, "set text anim:" + next.c);
                                    }
                                    TranslateAnimation translateAnimation5 = new TranslateAnimation(30.0f, 50.0f, 0.0f, 0.0f);
                                    translateAnimation5.setDuration(3000);
                                    translateAnimation5.setInterpolator(new CycleInterpolator(2.0f));
                                    translateAnimation5.setFillAfter(true);
                                    AnimationSet animationSet6 = new AnimationSet(false);
                                    animationSet6.addAnimation(translateAnimation5);
                                    animationSet = animationSet6;
                                } else if ("shy".equals(next.c)) {
                                    if (Log.isLoggable(Constants.LOG, 3)) {
                                        Log.d(Constants.LOG, "set text anim:" + next.c);
                                    }
                                    TranslateAnimation translateAnimation6 = new TranslateAnimation(0.0f, 0.0f, -3.0f, 10.0f);
                                    translateAnimation6.setDuration(3000);
                                    translateAnimation6.setInterpolator(new CycleInterpolator(2.0f));
                                    translateAnimation6.setFillAfter(true);
                                    AnimationSet animationSet7 = new AnimationSet(false);
                                    animationSet7.addAnimation(translateAnimation6);
                                    animationSet = animationSet7;
                                }
                                lVar.setInAnimation(animationSet);
                            } else {
                                lVar = lVar2;
                            }
                            lVar.addView(next.a, next.b);
                            lVar2 = lVar;
                            layoutParams = next.b;
                        } else {
                            this.b.addView(next.a, next.b);
                        }
                    }
                    if (lVar2 != null) {
                        if (lVar2.getChildCount() > 1) {
                            lVar2.setFlipInterval(10000);
                        } else {
                            lVar2.setFlipInterval(600000);
                        }
                        if (Log.isLoggable(Constants.LOG, 3)) {
                            Log.d(Constants.LOG, "add flipper.");
                        }
                        lVar2.startFlipping();
                        this.b.addView(lVar2, layoutParams);
                    }
                    this.b.invalidate();
                    this.b.requestLayout();
                }
                if (this.c != null) {
                    DomobAdEngine.a(this.c);
                }
            } catch (Exception e) {
                Log.e(Constants.LOG, "failed to add view into builder!");
                e.printStackTrace();
                if (this.c != null) {
                    this.c.e();
                }
            }
            if (this.a != null) {
                this.a.clear();
            }
        }
    }

    static void a(DomobAdEngine domobAdEngine) {
        if (domobAdEngine.f != null) {
            domobAdEngine.f.received(domobAdEngine);
        }
    }

    public static class RecvHandler {
        private DomobAdView a = null;

        /* access modifiers changed from: protected */
        public DomobAdView getAdView() {
            return this.a;
        }

        public RecvHandler(DomobAdView view) {
            this.a = view;
        }

        public void failed() {
            if (this.a != null) {
                DomobAdView.failedToReceive(this.a);
            }
        }

        public void received(DomobAdEngine engine) {
            if (this.a != null) {
                synchronized (this.a) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "new ad, construct it.");
                    }
                    this.a.constructView(engine, engine.a());
                }
            }
        }
    }

    static class c {
        public View a;
        public RelativeLayout.LayoutParams b;
        public String c;

        c() {
        }
    }

    public final void a(d dVar) {
        String str;
        String c2 = dVar.c();
        byte[] d2 = dVar.d();
        if (d2 != null) {
            this.r.put(c2, d2);
            if (!(this.c == null || (str = this.s.get(c2)) == null)) {
                DBHelper a2 = DBHelper.a(this.c.getContext());
                if (str.startsWith("def_")) {
                    DBHelper.a(a2, str, d2, System.currentTimeMillis());
                } else {
                    b bVar = this.w;
                    b.a(a2, str, d2);
                }
            }
        } else if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "Failed reading asset(" + c2 + ") for ad");
        }
        if (this.q != null) {
            synchronized (this.q) {
                this.q.remove(dVar);
            }
        }
        if (f()) {
            j();
        }
    }
}
