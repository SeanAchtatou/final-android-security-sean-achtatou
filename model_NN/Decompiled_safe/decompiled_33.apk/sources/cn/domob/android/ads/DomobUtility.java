package cn.domob.android.ads;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class DomobUtility {
    private static String a = null;

    protected static String getUserAgent(Context context) {
        if (a == null) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("android");
            stringBuffer.append(",");
            stringBuffer.append(",");
            if (Build.VERSION.RELEASE.length() > 0) {
                stringBuffer.append(Build.VERSION.RELEASE.replaceAll(",", "_"));
            } else {
                stringBuffer.append("1.5");
            }
            stringBuffer.append(",");
            stringBuffer.append(",");
            String str = Build.MODEL;
            if (str.length() > 0) {
                stringBuffer.append(str.replaceAll(",", "_"));
            }
            stringBuffer.append(",");
            String networkOperatorName = ((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName();
            if (networkOperatorName != null) {
                stringBuffer.append(networkOperatorName.replaceAll(",", "_"));
            }
            stringBuffer.append(",");
            stringBuffer.append(",");
            stringBuffer.append(",");
            a = stringBuffer.toString();
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "getUserAgent:" + a);
            }
        }
        return a;
    }

    public boolean needDownload(Context context, String packageName, int versionCode) {
        try {
            if (context.getPackageManager().getPackageInfo(packageName, 0).versionCode >= versionCode) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return true;
        }
    }

    public static String getMD5Str(String inStr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.reset();
            instance.update(inStr.getBytes("UTF-8"));
            byte[] digest = instance.digest();
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                String hexString = Integer.toHexString(b & 255);
                if (hexString.length() == 1) {
                    sb.append("0").append(hexString);
                } else {
                    sb.append(hexString);
                }
            }
            return sb.toString();
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            return "";
        }
    }
}
