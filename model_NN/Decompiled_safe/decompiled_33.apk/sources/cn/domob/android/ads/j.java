package cn.domob.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import cn.domob.android.ads.DomobAdEngine;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Hashtable;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

final class j {
    private static String a = "http://r.domob.cn/a/";
    private static int b = 0;
    private static String c = null;
    private static long d = 0;
    private static long e = 0;
    private static boolean f = false;
    private d g;
    private DomobAdEngine h = null;
    private DomobAdView i;
    private Context j;

    protected j() {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "AD Url:" + a + " | Req Count:" + b);
        }
        this.g = null;
    }

    /* access modifiers changed from: protected */
    public final d a() {
        return this.g;
    }

    protected static void b() {
        a = "http://r.domob.cn/a/";
        b = 0;
        d = 0;
        e = 0;
        f = false;
    }

    /* access modifiers changed from: protected */
    public final DomobAdEngine a(DomobAdEngine.RecvHandler recvHandler, DomobAdBuilder domobAdBuilder, int i2, int i3) {
        this.i = recvHandler.getAdView();
        this.j = this.i.getContext();
        Context context = this.j;
        DomobAdView domobAdView = this.i;
        DomobAdManager.init(context);
        StringBuilder sb = new StringBuilder();
        a(context, sb, "1", domobAdView.isDataMode(), domobAdView.getDataSetting());
        long currentTimeMillis = System.currentTimeMillis();
        a(sb, "ts", String.valueOf(currentTimeMillis));
        a(sb, "so", DomobAdManager.getOrientation(context));
        if (i2 > 0) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "handset screen width is " + i2);
            }
            a(sb, "sw", String.valueOf(i2));
        }
        if (i3 > 0) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "handset screen height is " + i3);
            }
            a(sb, "sh", String.valueOf(i3));
        }
        a(sb, "sd", String.valueOf(DomobAdBuilder.d()));
        if (DomobAdManager.isTestAllowed(context) && DomobAdManager.isTestMode(context)) {
            a(sb, DomobAdManager.GENDER_MALE, "test");
            a(sb, "test_action", DomobAdManager.getTestAction());
        }
        a(sb, "k", DomobAdView.getKeywords(domobAdView));
        a(sb, "spot", DomobAdView.getSpots(domobAdView));
        a(sb, "dim", "320x48");
        String coord = DomobAdManager.getCoord(context);
        if (coord != null) {
            a(sb, "d[coord]", coord);
            a(sb, "d[coord_timestamp]", DomobAdManager.getCoordTimestamp());
        }
        a(sb, "d[pc]", DomobAdManager.getPostalCode());
        a(sb, "d[dob]", DomobAdManager.getBirthdayOfString());
        a(sb, "d[gender]", DomobAdManager.getGender());
        a(sb, "pb[identifier]", DomobAdManager.getApplicationPackageName(context));
        a(sb, "pb[version]", String.valueOf(DomobAdManager.getApplicationVersion(context)));
        int i4 = b + 1;
        b = i4;
        if (i4 == 1) {
            d = currentTimeMillis;
        }
        a(sb, "stat[reqs]", String.valueOf(b));
        a(sb, "stat[time]", String.valueOf(currentTimeMillis - d));
        d = currentTimeMillis;
        a(sb, "c", "gif,fsi,ltx");
        String sb2 = sb.toString();
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "ad url:" + a + "\nad req:" + sb2);
        }
        long currentTimeMillis2 = System.currentTimeMillis();
        this.g = DomobAdEngine.a.a(a, null, a(this.j), DomobAdManager.getDeviceId(this.j), null, 30000, sb2);
        this.g.a(this.j);
        String str = "";
        if (this.g.a()) {
            byte[] d2 = this.g.d();
            if (this.g.d() == null && this.i.isDataMode()) {
                DomobAdView.failedToReceiveAdData(this.i, this.i.getAdData());
            }
            try {
                str = new String(d2, "utf-8");
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
            if (!str.equals("")) {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "ad resp:" + str);
                }
                try {
                    this.h = DomobAdEngine.a(recvHandler, new JSONObject(new JSONTokener(str)), domobAdBuilder);
                } catch (Exception e3) {
                    Log.e(Constants.LOG, "failed to init ad engine!");
                    e3.printStackTrace();
                }
                if (this.i.isDataMode()) {
                    DomobAdView domobAdView2 = this.i;
                    if (a(str, domobAdView2)) {
                        if (Log.isLoggable(Constants.LOG, 3)) {
                            Log.d(Constants.LOG, "call success recvie dataAds");
                        }
                        DomobAdView.receiveAdData(domobAdView2, domobAdView2.getAdData());
                    } else {
                        if (Log.isLoggable(Constants.LOG, 3)) {
                            Log.d(Constants.LOG, "call failed recvie dataAds");
                        }
                        DomobAdView.failedToReceiveAdData(domobAdView2, domobAdView2.getAdData());
                    }
                }
            } else {
                Log.i(Constants.LOG, "ad resp is empty!");
            }
        }
        long currentTimeMillis3 = System.currentTimeMillis() - currentTimeMillis2;
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "ad response time is:" + currentTimeMillis3);
        }
        e = currentTimeMillis3 + e;
        return this.h;
    }

    private boolean a(String str, DomobAdView domobAdView) {
        JSONTokener jSONTokener = new JSONTokener(str);
        c adData = domobAdView.getAdData();
        if (adData == null) {
            return false;
        }
        adData.a().clear();
        try {
            JSONObject jSONObject = new JSONObject(jSONTokener);
            jSONObject.optString("slogan");
            JSONArray jSONArray = jSONObject.getJSONArray("v");
            if (jSONArray == null) {
                return false;
            }
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                DomobAdDataItem domobAdDataItem = new DomobAdDataItem();
                domobAdDataItem.setAd_identifier(jSONObject2.optString("identifier", null));
                domobAdDataItem.setAd_rp_url(jSONObject2.optString("rp_url", null));
                domobAdDataItem.setAd_type(jSONObject2.optString("type", null));
                JSONObject optJSONObject = jSONObject2.optJSONObject("icon");
                if (optJSONObject != null) {
                    domobAdDataItem.setAd_icon(a(optJSONObject.optString("u", null), domobAdView.getContext()));
                }
                JSONObject optJSONObject2 = jSONObject2.optJSONObject("action_icon");
                if (optJSONObject2 != null) {
                    domobAdDataItem.setAd_action_icon(a(optJSONObject2.optString("u", null), domobAdView.getContext()));
                }
                domobAdDataItem.setAd_text(jSONObject2.optString(DomobAdDataItem.TEXT_TYPE, null));
                domobAdDataItem.setAdImage(a(jSONObject2.optString(DomobAdDataItem.IMAGE_TYPE, null), domobAdView.getContext()));
                domobAdDataItem.setAlt_text(jSONObject2.optString("alt_text", ""));
                domobAdDataItem.setAction(jSONObject2.getJSONArray("ac").getJSONObject(0));
                domobAdDataItem.setEngine(this.h);
                if (domobAdDataItem.checkDataValid()) {
                    adData.a().add(domobAdDataItem);
                } else if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.w(Constants.LOG, (i2 + 1) + "th ad is valid");
                }
            }
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "adData get ad count is " + adData.a().size());
            }
            if (adData.a().size() > 0) {
                return true;
            }
            return false;
        } catch (Exception e2) {
            Log.e(Constants.LOG, "failed prase data ad!" + e2.getMessage());
            e2.printStackTrace();
        }
    }

    private Bitmap a(String str, Context context) {
        Hashtable hashtable = new Hashtable();
        DBHelper a2 = DBHelper.a(context);
        new b();
        String str2 = "";
        if (str == null) {
            return null;
        }
        int lastIndexOf = str.lastIndexOf(47);
        if (lastIndexOf > 0 && lastIndexOf + 1 < str.length()) {
            str2 = str.substring(lastIndexOf + 1);
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "image name:" + str2);
            }
            if (str2.startsWith("def_")) {
                if (b.b(a2, str2, str2, hashtable)) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "data ad load " + str2 + " from resources.");
                    }
                    return BitmapFactory.decodeByteArray((byte[]) hashtable.get(str2), 0, ((byte[]) hashtable.get(str2)).length);
                }
            } else if (b.a(a2, str2, str2, hashtable)) {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "data ad load " + str2 + " from cache.");
                }
                return BitmapFactory.decodeByteArray((byte[]) hashtable.get(str2), 0, ((byte[]) hashtable.get(str2)).length);
            }
        }
        String str3 = str2;
        d a3 = DomobAdEngine.a.a(str, null, a(this.j), DomobAdManager.getDeviceId(this.j));
        a3.a(this.j);
        a3.a();
        if (a3.d() == null) {
            return null;
        }
        if (str3.startsWith("def_")) {
            DBHelper.a(a2, str3, a3.d(), System.currentTimeMillis());
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "data ad save " + str3 + " to res.");
            }
        } else {
            b.a(a2, str3, a3.d());
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "data ad save " + str3 + " to cache.");
            }
        }
        return BitmapFactory.decodeByteArray(a3.d(), 0, a3.d().length);
    }

    /* access modifiers changed from: protected */
    public final g a(Context context, DomobAdView domobAdView) {
        g gVar;
        this.i = domobAdView;
        if (f) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "ignore, it is detecting now");
            }
            return null;
        }
        f = true;
        String b2 = b(context);
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "detector req:" + b2);
        }
        this.g = DomobAdEngine.a.a(a, null, a(context), DomobAdManager.getDeviceId(context), null, 30000, b2);
        this.g.a(context);
        if (this.g.a()) {
            String str = new String(this.g.d());
            if (!str.equals("")) {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "detector resp:" + str);
                }
                try {
                    gVar = g.a(context, new JSONObject(new JSONTokener(str)));
                } catch (Exception e2) {
                    Log.e(Constants.LOG, "failed to init detector!");
                    gVar = null;
                }
                if (gVar == null && this.i.isDataMode()) {
                    DomobAdView.failedToReceiveAdData(this.i, this.i.getAdData());
                }
                f = false;
                return gVar;
            }
            Log.i(Constants.LOG, "detector resp is empty!");
        }
        gVar = null;
        DomobAdView.failedToReceiveAdData(this.i, this.i.getAdData());
        f = false;
        return gVar;
    }

    private static void a(Context context, StringBuilder sb, String str, boolean z, f fVar) {
        String str2;
        sb.append("rt").append("=").append(str);
        String publisherId = DomobAdManager.getPublisherId(context);
        if (publisherId == null) {
            throw new IllegalStateException("Publisher ID is not set!");
        }
        a(sb, "ipb", publisherId);
        String a2 = a(context);
        if (a2 != null) {
            a(sb, "ua", a2);
        } else {
            a(sb, "ua", "unknown");
        }
        String language = Locale.getDefault().getLanguage();
        if (language != null) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(language.toLowerCase());
            String country = Locale.getDefault().getCountry();
            if (country != null) {
                stringBuffer.append("-");
                stringBuffer.append(country.toLowerCase());
            }
            str2 = stringBuffer.toString();
        } else {
            str2 = "zh-cn";
        }
        a(sb, "l", str2);
        if (z) {
            a(sb, DomobAdManager.GENDER_FEMALE, "json_data");
        } else {
            a(sb, DomobAdManager.GENDER_FEMALE, "jsonp");
        }
        if (z && fVar != null) {
            a(sb, "num", String.valueOf(fVar.a()));
        }
        a(sb, "e", "UTF-8");
        a(sb, "sdk", "1");
        a(sb, "v", "20110613-android-20110513");
        a(sb, "idv", DomobAdManager.getDeviceId(context));
        String networkType = DomobAdManager.getNetworkType(context);
        if (networkType != null) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "current network type:" + networkType);
            }
            a(sb, "network", networkType);
        }
        String cid = DomobAdManager.getCID(context);
        if (cid != null) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "CID:" + cid);
            }
            a(sb, "cid", cid);
        }
    }

    protected static String a(Context context) {
        if (c == null) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("android");
            stringBuffer.append(",");
            stringBuffer.append(",");
            if (Build.VERSION.RELEASE.length() > 0) {
                stringBuffer.append(Build.VERSION.RELEASE.replaceAll(",", "_"));
            } else {
                stringBuffer.append("1.5");
            }
            stringBuffer.append(",");
            stringBuffer.append(",");
            String str = Build.MODEL;
            if (str.length() > 0) {
                stringBuffer.append(str.replaceAll(",", "_"));
            }
            stringBuffer.append(",");
            String networkOperatorName = ((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName();
            if (networkOperatorName != null) {
                stringBuffer.append(networkOperatorName.replaceAll(",", "_"));
            }
            stringBuffer.append(",");
            stringBuffer.append(",");
            stringBuffer.append(",");
            c = stringBuffer.toString();
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "getUserAgent:" + c);
            }
        }
        return c;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x003e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String b(android.content.Context r7) {
        /*
            r4 = 0
            cn.domob.android.ads.DomobAdManager.init(r7)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "4"
            r2 = 0
            a(r7, r0, r1, r2, r4)
            long r1 = java.lang.System.currentTimeMillis()
            java.lang.String r3 = "ts"
            java.lang.String r1 = java.lang.String.valueOf(r1)
            a(r0, r3, r1)
            cn.domob.android.ads.DBHelper r1 = cn.domob.android.ads.DBHelper.a(r7)     // Catch:{ Exception -> 0x008e }
            android.database.Cursor r1 = r1.b()     // Catch:{ Exception -> 0x008e }
            if (r1 == 0) goto L_0x002c
            int r2 = r1.getCount()     // Catch:{ Exception -> 0x0085 }
            if (r2 != 0) goto L_0x0046
        L_0x002c:
            java.lang.String r2 = "DomobSDK"
            r3 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r3)     // Catch:{ Exception -> 0x0085 }
            if (r2 == 0) goto L_0x003c
            java.lang.String r2 = "DomobSDK"
            java.lang.String r3 = "conf db is empty!"
            android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x0085 }
        L_0x003c:
            if (r1 == 0) goto L_0x0041
            r1.close()
        L_0x0041:
            java.lang.String r0 = r0.toString()
            return r0
        L_0x0046:
            r1.moveToFirst()     // Catch:{ Exception -> 0x0085 }
            java.lang.String r2 = "_conf_ver"
            int r2 = r1.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0085 }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Exception -> 0x0085 }
            java.lang.String r3 = "lm[config]"
            a(r0, r3, r2)     // Catch:{ Exception -> 0x0085 }
            java.lang.String r2 = "_res_ver"
            int r2 = r1.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0085 }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Exception -> 0x0085 }
            java.lang.String r3 = "lm[res]"
            a(r0, r3, r2)     // Catch:{ Exception -> 0x0085 }
            long r2 = c()     // Catch:{ Exception -> 0x0085 }
            r4 = -1
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x007b
            java.lang.String r2 = "_avg_time"
            int r2 = r1.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0085 }
            long r2 = r1.getLong(r2)     // Catch:{ Exception -> 0x0085 }
        L_0x007b:
            java.lang.String r4 = "avg"
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x0085 }
            a(r0, r4, r2)     // Catch:{ Exception -> 0x0085 }
            goto L_0x003c
        L_0x0085:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
        L_0x0089:
            r1.printStackTrace()
            r1 = r2
            goto L_0x003c
        L_0x008e:
            r1 = move-exception
            r2 = r4
            goto L_0x0089
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.j.b(android.content.Context):java.lang.String");
    }

    protected static long c() {
        if (b > 0) {
            return e / ((long) b);
        }
        return -1;
    }

    protected static String a(Context context, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("jstr").append("=").append(str);
        String a2 = a(context);
        if (a2 != null) {
            a(sb, "ua", a2);
        } else {
            a(sb, "ua", "unknown");
        }
        String cid = DomobAdManager.getCID(context);
        if (cid != null) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "CID:" + cid);
            }
            a(sb, "cid", cid);
        }
        return sb.toString();
    }

    private static void a(StringBuilder sb, String str, String str2) {
        if (str2 != null && str2.length() != 0) {
            try {
                sb.append("&").append(URLEncoder.encode(str, "UTF-8")).append("=").append(URLEncoder.encode(str2, "UTF-8"));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    static void a(String str) {
        if (str != null) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "set ad url:" + str);
            }
            a = str;
        }
    }

    static String d() {
        return a;
    }
}
