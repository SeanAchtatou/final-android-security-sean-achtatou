package cn.domob.android.ads;

import android.content.Context;
import android.util.Log;
import cn.domob.android.ads.DomobAdEngine;
import com.mobclick.android.ReportPolicy;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class DomobReport {
    /* access modifiers changed from: private */
    public Context a;

    public class ActionType {
        public ActionType(DomobReport domobReport) {
        }
    }

    public class IDType {
        public IDType(DomobReport domobReport) {
        }
    }

    public class ReportInfo {
        public ReportInfo(DomobReport domobReport) {
        }
    }

    public DomobReport(Context context) {
        this.a = context;
    }

    static /* synthetic */ String a(DomobReport domobReport, int i) {
        switch (i) {
            case 0:
                return "download";
            case 1:
                return "download_finish";
            case 2:
                return "download_failed";
            case 3:
                return "download_cancel";
            case ReportPolicy.DAILY /*4*/:
                return "load_success";
            case 5:
                return "load_failed";
            case 6:
                return "close_lp";
            case DomobAdView.ANIMATION_TRANSLATE:
                return "install_success";
            case 8:
                return "run";
            default:
                return "";
        }
    }

    public void reportAction(final ReportInfo reportInfo) {
        new Thread() {
            public final void run() {
                d a2 = DomobAdEngine.a.a("http://e.domob.cn/report", DomobReport.a(DomobReport.a(DomobReport.this, reportInfo)));
                a2.a(DomobReport.this.a);
                if (a2.a()) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        StringBuilder sb = new StringBuilder("send report:");
                        DomobReport domobReport = DomobReport.this;
                        ReportInfo reportInfo = reportInfo;
                        Log.d(Constants.LOG, sb.append(DomobReport.a(domobReport, 0)).append(" success").toString());
                    }
                } else if (Log.isLoggable(Constants.LOG, 3)) {
                    StringBuilder sb2 = new StringBuilder("send report:");
                    DomobReport domobReport2 = DomobReport.this;
                    ReportInfo reportInfo2 = reportInfo;
                    Log.d(Constants.LOG, sb2.append(DomobReport.a(domobReport2, 0)).append(" failed").toString());
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public static String a(HashMap<String, String> hashMap) {
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (Map.Entry next : hashMap.entrySet()) {
            String str = (String) next.getKey();
            String str2 = (String) next.getValue();
            if (!(str == null || str.length() <= 0 || str2 == null)) {
                if (z) {
                    try {
                        sb.append(URLEncoder.encode(str, "UTF-8")).append("=").append(URLEncoder.encode(str2, "UTF-8"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    sb.append("&").append(URLEncoder.encode(str, "UTF-8")).append("=").append(URLEncoder.encode(str2, "UTF-8"));
                }
            }
            if (z) {
                z = false;
            }
        }
        String sb2 = sb.toString();
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "post params string:" + sb2);
        }
        return sb2;
    }

    static /* synthetic */ HashMap a(DomobReport domobReport, ReportInfo reportInfo) {
        HashMap hashMap = new HashMap();
        hashMap.put("rt", "1");
        String userAgent = DomobUtility.getUserAgent(domobReport.a);
        if (userAgent == null) {
            userAgent = "Android,,,,,,,,";
        }
        hashMap.put("ua", userAgent);
        String cid = DomobAdManager.getCID(domobReport.a);
        if (cid != null) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "CID:" + cid);
            }
            hashMap.put("cid", cid);
        }
        String publisherId = DomobAdManager.getPublisherId(domobReport.a);
        if (publisherId == null || publisherId.length() <= 0) {
            Log.e(Constants.LOG, "publisher id is null or empty!");
        } else {
            hashMap.put("ipb", publisherId);
        }
        String mD5Str = DomobUtility.getMD5Str(null);
        if (mD5Str != null) {
            hashMap.put("rp_md5", mD5Str);
        }
        return hashMap;
    }
}
