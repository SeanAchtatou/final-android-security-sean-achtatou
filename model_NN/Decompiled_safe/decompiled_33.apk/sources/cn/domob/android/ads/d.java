package cn.domob.android.ads;

import android.util.Log;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

abstract class d implements Runnable {
    private static Executor k = null;
    protected URL a;
    protected Proxy b;
    protected a c;
    protected String d;
    protected int e;
    protected Map<String, String> f;
    protected String g;
    protected byte[] h;
    protected boolean i;
    protected int j = 0;
    private String l;

    /* access modifiers changed from: package-private */
    public abstract boolean a();

    protected d(String str, String str2, String str3, a aVar, int i2, Map<String, String> map, String str4) {
        this.l = str;
        this.c = aVar;
        this.f = map;
        this.e = i2;
        if (str4 != null) {
            this.g = str4;
            this.d = "application/x-www-form-urlencoded";
        } else {
            this.g = null;
            this.d = null;
        }
        this.b = null;
        this.i = true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00d3 A[Catch:{ all -> 0x0139 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00df A[SYNTHETIC, Splitter:B:45:0x00df] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00ee A[Catch:{ all -> 0x0139 }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00fa A[SYNTHETIC, Splitter:B:56:0x00fa] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0109 A[Catch:{ all -> 0x013c }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0123 A[SYNTHETIC, Splitter:B:66:0x0123] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x012b A[Catch:{ Exception -> 0x00b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.content.Context r10) {
        /*
            r9 = this;
            r7 = 0
            if (r10 != 0) goto L_0x0004
        L_0x0003:
            return
        L_0x0004:
            android.database.Cursor r0 = cn.domob.android.ads.DomobAdManager.getCurrentApn(r10)     // Catch:{ Exception -> 0x0133 }
            if (r0 == 0) goto L_0x00c1
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x00b9 }
            if (r1 <= 0) goto L_0x00c1
            r0.moveToFirst()     // Catch:{ Exception -> 0x00b9 }
            java.lang.String r1 = "proxy"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x00b9 }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x00b9 }
            java.lang.String r2 = "port"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00b9 }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x00b9 }
            java.lang.String r3 = "apn"
            int r3 = r0.getColumnIndexOrThrow(r3)     // Catch:{ Exception -> 0x00b9 }
            java.lang.String r3 = r0.getString(r3)     // Catch:{ Exception -> 0x00b9 }
            java.lang.String r4 = "DomobSDK"
            r5 = 3
            boolean r4 = android.util.Log.isLoggable(r4, r5)     // Catch:{ Exception -> 0x00b9 }
            if (r4 == 0) goto L_0x004e
            java.lang.String r4 = "DomobSDK"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b9 }
            java.lang.String r6 = "current apnType is "
            r5.<init>(r6)     // Catch:{ Exception -> 0x00b9 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ Exception -> 0x00b9 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00b9 }
            android.util.Log.d(r4, r3)     // Catch:{ Exception -> 0x00b9 }
        L_0x004e:
            java.lang.String r3 = "DomobSDK"
            r4 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r4)     // Catch:{ Exception -> 0x00b9 }
            if (r3 == 0) goto L_0x0075
            java.lang.String r3 = "DomobSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b9 }
            java.lang.String r5 = "proxy:"
            r4.<init>(r5)     // Catch:{ Exception -> 0x00b9 }
            java.lang.StringBuilder r4 = r4.append(r1)     // Catch:{ Exception -> 0x00b9 }
            java.lang.String r5 = "| port:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00b9 }
            java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ Exception -> 0x00b9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00b9 }
            android.util.Log.d(r3, r4)     // Catch:{ Exception -> 0x00b9 }
        L_0x0075:
            if (r1 == 0) goto L_0x00c1
            java.lang.String r3 = ""
            boolean r3 = r1.equals(r3)     // Catch:{ Exception -> 0x00b9 }
            if (r3 != 0) goto L_0x00c1
            java.lang.String r3 = "r.domob.cn"
            java.net.InetAddress r3 = java.net.InetAddress.getByName(r3)     // Catch:{ UnknownHostException -> 0x00c8, SocketTimeoutException -> 0x00e3, Exception -> 0x00fe, all -> 0x0127 }
            java.net.Socket r4 = new java.net.Socket     // Catch:{ UnknownHostException -> 0x00c8, SocketTimeoutException -> 0x00e3, Exception -> 0x00fe, all -> 0x0127 }
            r4.<init>()     // Catch:{ UnknownHostException -> 0x00c8, SocketTimeoutException -> 0x00e3, Exception -> 0x00fe, all -> 0x0127 }
            java.net.InetSocketAddress r5 = new java.net.InetSocketAddress     // Catch:{ UnknownHostException -> 0x0144, SocketTimeoutException -> 0x0141, Exception -> 0x013e, all -> 0x0136 }
            r6 = 80
            r5.<init>(r3, r6)     // Catch:{ UnknownHostException -> 0x0144, SocketTimeoutException -> 0x0141, Exception -> 0x013e, all -> 0x0136 }
            r3 = 5000(0x1388, float:7.006E-42)
            r4.connect(r5, r3)     // Catch:{ UnknownHostException -> 0x0144, SocketTimeoutException -> 0x0141, Exception -> 0x013e, all -> 0x0136 }
            boolean r3 = r4.isConnected()     // Catch:{ UnknownHostException -> 0x0144, SocketTimeoutException -> 0x0141, Exception -> 0x013e, all -> 0x0136 }
            if (r3 == 0) goto L_0x012f
            java.lang.String r3 = "DomobSDK"
            r5 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r5)     // Catch:{ UnknownHostException -> 0x0144, SocketTimeoutException -> 0x0141, Exception -> 0x013e, all -> 0x0136 }
            if (r3 == 0) goto L_0x00ac
            java.lang.String r3 = "DomobSDK"
            java.lang.String r5 = "connected  not need use proxy"
            android.util.Log.d(r3, r5)     // Catch:{ UnknownHostException -> 0x0144, SocketTimeoutException -> 0x0141, Exception -> 0x013e, all -> 0x0136 }
        L_0x00ac:
            if (r0 == 0) goto L_0x00b1
            r0.close()     // Catch:{ UnknownHostException -> 0x0144, SocketTimeoutException -> 0x0141, Exception -> 0x013e, all -> 0x0136 }
        L_0x00b1:
            r4.close()     // Catch:{ UnknownHostException -> 0x0144, SocketTimeoutException -> 0x0141, Exception -> 0x013e, all -> 0x0136 }
            r4.close()     // Catch:{ Exception -> 0x00b9 }
            goto L_0x0003
        L_0x00b9:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x00bd:
            r0.printStackTrace()
            r0 = r1
        L_0x00c1:
            if (r0 == 0) goto L_0x0003
            r0.close()
            goto L_0x0003
        L_0x00c8:
            r3 = move-exception
            r3 = r7
        L_0x00ca:
            java.lang.String r4 = "DomobSDK"
            r5 = 3
            boolean r4 = android.util.Log.isLoggable(r4, r5)     // Catch:{ all -> 0x0139 }
            if (r4 == 0) goto L_0x00da
            java.lang.String r4 = "DomobSDK"
            java.lang.String r5 = "socket connect  is  throws UnknownHostException "
            android.util.Log.i(r4, r5)     // Catch:{ all -> 0x0139 }
        L_0x00da:
            r9.a(r1, r2)     // Catch:{ all -> 0x0139 }
            if (r3 == 0) goto L_0x00c1
            r3.close()     // Catch:{ Exception -> 0x00b9 }
            goto L_0x00c1
        L_0x00e3:
            r3 = move-exception
            r3 = r7
        L_0x00e5:
            java.lang.String r4 = "DomobSDK"
            r5 = 3
            boolean r4 = android.util.Log.isLoggable(r4, r5)     // Catch:{ all -> 0x0139 }
            if (r4 == 0) goto L_0x00f5
            java.lang.String r4 = "DomobSDK"
            java.lang.String r5 = "socket connect  is  throws SocketTimeoutException "
            android.util.Log.i(r4, r5)     // Catch:{ all -> 0x0139 }
        L_0x00f5:
            r9.a(r1, r2)     // Catch:{ all -> 0x0139 }
            if (r3 == 0) goto L_0x00c1
            r3.close()     // Catch:{ Exception -> 0x00b9 }
            goto L_0x00c1
        L_0x00fe:
            r1 = move-exception
            r2 = r7
        L_0x0100:
            java.lang.String r3 = "DomobSDK"
            r4 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r4)     // Catch:{ all -> 0x013c }
            if (r3 == 0) goto L_0x0121
            java.lang.String r3 = "DomobSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x013c }
            java.lang.String r5 = "socket exception "
            r4.<init>(r5)     // Catch:{ all -> 0x013c }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x013c }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x013c }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x013c }
            android.util.Log.e(r3, r1)     // Catch:{ all -> 0x013c }
        L_0x0121:
            if (r2 == 0) goto L_0x00c1
            r2.close()     // Catch:{ Exception -> 0x00b9 }
            goto L_0x00c1
        L_0x0127:
            r1 = move-exception
            r2 = r7
        L_0x0129:
            if (r2 == 0) goto L_0x012e
            r2.close()     // Catch:{ Exception -> 0x00b9 }
        L_0x012e:
            throw r1     // Catch:{ Exception -> 0x00b9 }
        L_0x012f:
            r4.close()     // Catch:{ Exception -> 0x00b9 }
            goto L_0x00c1
        L_0x0133:
            r0 = move-exception
            r1 = r7
            goto L_0x00bd
        L_0x0136:
            r1 = move-exception
            r2 = r4
            goto L_0x0129
        L_0x0139:
            r1 = move-exception
            r2 = r3
            goto L_0x0129
        L_0x013c:
            r1 = move-exception
            goto L_0x0129
        L_0x013e:
            r1 = move-exception
            r2 = r4
            goto L_0x0100
        L_0x0141:
            r3 = move-exception
            r3 = r4
            goto L_0x00e5
        L_0x0144:
            r3 = move-exception
            r3 = r4
            goto L_0x00ca
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.d.a(android.content.Context):void");
    }

    private final void a(String str, int i2) {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "setProxy -- proxy:" + str + "| port:" + i2);
        }
        this.b = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(str, i2));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (k == null) {
            k = Executors.newCachedThreadPool();
        }
        k.execute(this);
    }

    /* access modifiers changed from: protected */
    public final String c() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public final byte[] d() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public final int e() {
        return this.j;
    }
}
