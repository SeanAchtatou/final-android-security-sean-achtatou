package cn.domob.android.ads;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

final class o extends Animation {
    private final float a;
    private final float b;
    private final float c;
    private final float d;
    private final float e = 0.0f;
    private final boolean f;
    private Camera g;

    public o(float f2, float f3, float f4, float f5, float f6, boolean z) {
        this.a = f2;
        this.b = f3;
        this.c = f4;
        this.d = f5;
        this.f = z;
    }

    public final void initialize(int width, int height, int parentWidth, int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
        this.g = new Camera();
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float interpolatedTime, Transformation t) {
        float f2 = this.a;
        float f3 = f2 + ((this.b - f2) * interpolatedTime);
        float f4 = this.c;
        float f5 = this.d;
        Camera camera = this.g;
        Matrix matrix = t.getMatrix();
        camera.save();
        if (this.f) {
            camera.translate(0.0f, 0.0f, this.e * interpolatedTime);
        } else {
            camera.translate(0.0f, 0.0f, this.e * (1.0f - interpolatedTime));
        }
        camera.rotateY(f3);
        camera.getMatrix(matrix);
        camera.restore();
        matrix.preTranslate(-f4, -f5);
        matrix.postTranslate(f4, f5);
    }
}
