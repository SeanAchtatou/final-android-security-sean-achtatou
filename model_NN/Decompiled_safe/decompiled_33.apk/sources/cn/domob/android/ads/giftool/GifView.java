package cn.domob.android.ads.giftool;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import java.io.InputStream;

public class GifView extends View implements a {
    private static /* synthetic */ int[] k;
    /* access modifiers changed from: private */
    public b a;
    /* access modifiers changed from: private */
    public Bitmap b;
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public boolean d;
    private int e;
    private Rect f;
    /* access modifiers changed from: private */
    public View g;
    private a h;
    private GifImageType i;
    private a j;

    private static /* synthetic */ int[] a() {
        int[] iArr = k;
        if (iArr == null) {
            iArr = new int[GifImageType.values().length];
            try {
                iArr[GifImageType.COVER.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[GifImageType.SYNC_DECODER.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[GifImageType.WAIT_FINISH.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            k = iArr;
        }
        return iArr;
    }

    public enum GifImageType {
        WAIT_FINISH(0),
        SYNC_DECODER(1),
        COVER(2);

        private GifImageType(int i) {
        }
    }

    public GifView(Context context) {
        super(context);
        this.a = null;
        this.b = null;
        this.c = true;
        this.d = false;
        this.e = -1;
        this.f = null;
        this.g = this;
        this.h = null;
        this.i = GifImageType.SYNC_DECODER;
        this.j = null;
    }

    public GifView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GifView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.a = null;
        this.b = null;
        this.c = true;
        this.d = false;
        this.e = -1;
        this.f = null;
        this.g = this;
        this.h = null;
        this.i = GifImageType.SYNC_DECODER;
        this.j = null;
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        if (!hasWindowFocus) {
            this.d = true;
        } else {
            this.d = false;
        }
    }

    private void a(InputStream inputStream) {
        if (this.a != null) {
            this.a.a();
            this.a = null;
        }
        this.a = new b(inputStream, this);
        this.a.start();
    }

    public void setGifImage(byte[] gif) {
        if (this.a != null) {
            this.a.a();
            this.a = null;
        }
        this.a = new b(gif, this);
        this.a.start();
    }

    public void setGifImage(InputStream is) {
        a(is);
    }

    public void setGifImage(int resId) {
        a(getResources().openRawResource(resId));
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.a != null) {
            if (this.b == null) {
                this.b = this.a.c();
            }
            if (this.b != null) {
                int saveCount = canvas.getSaveCount();
                canvas.save();
                canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
                if (this.e == -1) {
                    canvas.drawBitmap(this.b, 0.0f, 0.0f, (Paint) null);
                } else {
                    canvas.drawBitmap(this.b, (Rect) null, this.f, (Paint) null);
                }
                canvas.restoreToCount(saveCount);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int i2;
        int i3 = 1;
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        if (this.a == null) {
            i2 = 1;
        } else {
            i3 = this.a.a;
            i2 = this.a.b;
        }
        setMeasuredDimension(resolveSize(Math.max(paddingLeft + paddingRight + i3, getSuggestedMinimumWidth()), widthMeasureSpec), resolveSize(Math.max(paddingTop + paddingBottom + i2, getSuggestedMinimumHeight()), heightMeasureSpec));
    }

    public void showCover() {
        if (this.a != null) {
            this.d = true;
            this.b = this.a.c();
            invalidate();
        }
    }

    public void showAnimation() {
        if (this.d) {
            this.d = false;
        }
    }

    public void setGifImageType(GifImageType type) {
        if (this.a == null) {
            this.i = type;
        }
    }

    public void setShowDimension(int width, int height) {
        if (width > 0 && height > 0) {
            this.e = width;
            this.f = new Rect();
            this.f.left = 0;
            this.f.top = 0;
            this.f.right = width;
            this.f.bottom = height;
        }
    }

    public void parseOk(boolean parseStatus, int frameIndex) {
        if (parseStatus && this.a != null) {
            switch (a()[this.i.ordinal()]) {
                case 1:
                    if (frameIndex != -1) {
                        return;
                    }
                    if (this.a.b() > 1) {
                        this.j = new a(this);
                        this.j.start();
                        return;
                    }
                    this.g.postInvalidate();
                    return;
                case 2:
                    if (frameIndex == 1) {
                        this.b = this.a.c();
                        this.g.postInvalidate();
                        return;
                    } else if (frameIndex == -1) {
                        this.g.postInvalidate();
                        return;
                    } else if (this.h == null) {
                        this.h = new a(this);
                        this.h.start();
                        return;
                    } else {
                        return;
                    }
                case 3:
                    if (frameIndex == 1) {
                        this.b = this.a.c();
                        this.g.postInvalidate();
                        return;
                    } else if (frameIndex != -1) {
                        return;
                    } else {
                        if (this.a.b() <= 1) {
                            this.g.postInvalidate();
                            return;
                        } else if (this.h == null) {
                            this.h = new a(this);
                            this.h.start();
                            return;
                        } else {
                            return;
                        }
                    }
                default:
                    return;
            }
        }
    }

    class a extends Thread {
        /* synthetic */ a(GifView gifView) {
            this((byte) 0);
        }

        private a(byte b) {
        }

        /* JADX WARNING: CFG modification limit reached, blocks count: 118 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void run() {
            /*
                r3 = this;
                cn.domob.android.ads.giftool.GifView r0 = cn.domob.android.ads.giftool.GifView.this
                cn.domob.android.ads.giftool.b r0 = r0.a
                if (r0 != 0) goto L_0x003b
            L_0x0008:
                return
            L_0x0009:
                cn.domob.android.ads.giftool.GifView r0 = cn.domob.android.ads.giftool.GifView.this
                boolean r0 = r0.d
                if (r0 != 0) goto L_0x0044
                cn.domob.android.ads.giftool.GifView r0 = cn.domob.android.ads.giftool.GifView.this
                cn.domob.android.ads.giftool.b r0 = r0.a
                cn.domob.android.ads.giftool.c r0 = r0.d()
                if (r0 == 0) goto L_0x0008
                cn.domob.android.ads.giftool.GifView r1 = cn.domob.android.ads.giftool.GifView.this
                android.graphics.Bitmap r2 = r0.a
                r1.b = r2
                int r0 = r0.b
                long r0 = (long) r0
                cn.domob.android.ads.giftool.GifView r2 = cn.domob.android.ads.giftool.GifView.this
                android.view.View r2 = r2.g
                if (r2 == 0) goto L_0x0008
                cn.domob.android.ads.giftool.GifView r2 = cn.domob.android.ads.giftool.GifView.this
                android.view.View r2 = r2.g
                r2.postInvalidate()
                android.os.SystemClock.sleep(r0)
            L_0x003b:
                cn.domob.android.ads.giftool.GifView r0 = cn.domob.android.ads.giftool.GifView.this
                boolean r0 = r0.c
                if (r0 != 0) goto L_0x0009
                goto L_0x0008
            L_0x0044:
                r0 = 50
                android.os.SystemClock.sleep(r0)
                goto L_0x003b
            */
            throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.giftool.GifView.a.run():void");
        }
    }

    public void clear() {
        if (this.a != null) {
            this.a.a();
            this.c = false;
            this.a = null;
        }
    }
}
