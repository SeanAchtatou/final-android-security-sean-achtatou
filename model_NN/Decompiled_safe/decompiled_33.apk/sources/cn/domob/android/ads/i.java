package cn.domob.android.ads;

import android.content.Context;
import android.util.Log;

final class i extends Thread {
    private DomobAdView a;

    public i(DomobAdView domobAdView) {
        this.a = domobAdView;
    }

    public final void run() {
        boolean z;
        boolean z2;
        if (this.a == null) {
            Log.e(Constants.LOG, "DomobGetDcThread exit because adview is null.");
            return;
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "DomobGetDcThread start");
        }
        Context context = this.a.getContext();
        try {
            j jVar = new j();
            g a2 = jVar.a(context, this.a);
            if (a2 == null) {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "detector is null.");
                }
                d a3 = jVar.a();
                if (a3 != null) {
                    int e = a3.e();
                    if (e < 200 || e >= 300) {
                        if (Log.isLoggable(Constants.LOG, 3)) {
                            Log.d(Constants.LOG, "connection return error:" + e + ", try again!");
                        }
                        z2 = false;
                    } else {
                        DomobAdView.setNeedDetect(this.a, false);
                        z2 = true;
                    }
                } else {
                    z2 = false;
                }
            } else {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "success to get detector.");
                }
                int a4 = a2.a();
                if (a4 == 0) {
                    z = false;
                } else {
                    z = true;
                }
                if (a4 != 1) {
                    this.a.setRequestInterval(a4);
                }
                this.a.setDisabled(a2.b(), a2.c(), a2.d());
                DomobAdView.setNeedDetect(this.a, false);
                z2 = z;
            }
            if (DomobAdManager.getCID(context) == null) {
                Log.w(Constants.LOG, "CID is null, continue detecting!");
                DomobAdView.setNeedDetect(this.a, true);
                z2 = false;
            }
            DomobAdView.setRequesting(this.a, false);
            if (z2) {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "request ad without delay.");
                }
                DomobAdView.requestFreshAd(this.a);
                return;
            }
            DomobAdView.setSchedule(this.a, true);
        } catch (Exception e2) {
            if (DomobAdManager.getPublisherId(context) == null) {
                Log.e(Constants.LOG, "Please set your publisher ID first!");
            } else {
                Log.e(Constants.LOG, "Unkown exception happened!" + e2.getMessage());
            }
            e2.printStackTrace();
        }
    }
}
