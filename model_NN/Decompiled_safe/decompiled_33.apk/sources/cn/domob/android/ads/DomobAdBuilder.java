package cn.domob.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;
import cn.domob.android.ads.DomobAdEngine;
import java.util.Collection;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

final class DomobAdBuilder extends RelativeLayout implements Animation.AnimationListener, DomobAdEngine.e, n {
    private static float c = -1.0f;
    private static Drawable i = null;
    protected DomobAdEngine a;
    final DomobAdView b;
    private View d;
    private long e = -1;
    private boolean f;
    private Vector<String> g;
    private View h;
    private String j;

    protected DomobAdBuilder(DomobAdEngine domobAdEngine, Context context, DomobAdView adview) {
        super(context);
        this.b = adview;
        this.j = "fr2l";
        if (c == -1.0f) {
            c = DomobAdManager.getCurrentDensity(context, adview);
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "sDensity=" + c);
            }
        }
        this.d = new View(context);
        this.d.setVisibility(4);
        addView(this.d, new RelativeLayout.LayoutParams(-1, -1));
        a((DomobAdEngine) null);
    }

    private static Drawable b(DomobAdEngine domobAdEngine) {
        if (domobAdEngine == null) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "failed to getBackground, engine is null!");
            }
            return null;
        } else if (i != null) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "already has background bitmap!");
            }
            return i;
        } else {
            try {
                Rect d2 = domobAdEngine.d();
                Bitmap createBitmap = Bitmap.createBitmap(d2.width(), d2.height(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(createBitmap);
                Paint paint = new Paint();
                paint.setColor(-1147097);
                paint.setStyle(Paint.Style.FILL);
                canvas.drawRect(d2, paint);
                GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb(Color.alpha(-1147097), Color.red(-1147097), Color.green(-1147097), Color.blue(-1147097)), -1147097});
                int height = ((int) (((double) d2.height()) * 0.4375d)) + d2.top;
                gradientDrawable.setBounds(d2.left, d2.top, d2.right, height);
                gradientDrawable.draw(canvas);
                canvas.drawRect(new Rect(d2.left, height, d2.right, d2.bottom), paint);
                BitmapDrawable bitmapDrawable = new BitmapDrawable(createBitmap);
                bitmapDrawable.setAlpha(128);
                return bitmapDrawable;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(DomobAdEngine domobAdEngine) {
        this.a = domobAdEngine;
        if (this.a == null) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "engine is null, focusable & clickable is false.");
            }
            setFocusable(false);
            setClickable(false);
            return;
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "focusable & clickable is true.");
        }
        this.a.a(this);
        setFocusable(true);
        setClickable(true);
    }

    /* access modifiers changed from: protected */
    public final void a(View view) {
        if (view != null && view != this.h) {
            this.h = view;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        if (str != null) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "setTransAnimType:" + str);
            }
            this.j = str;
        }
    }

    /* access modifiers changed from: protected */
    public final String a() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public final DomobAdEngine b() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        if (this.a != null) {
            this.a.e();
            this.a = null;
        }
    }

    protected static float d() {
        return c;
    }

    public static class ClickThread extends Thread {
        private JSONObject a;
        private DomobAdBuilder b;

        public ClickThread(JSONObject j, DomobAdBuilder b2) {
            this.a = j;
            this.b = b2;
        }

        public final void run() {
            try {
                if (this.b != null && this.b.a != null) {
                    this.b.a.a(this.a);
                    if (this.b.b != null) {
                        if (Log.isLoggable(Constants.LOG, 3)) {
                            Log.d(Constants.LOG, "performClick");
                        }
                        this.b.b.performClick();
                    }
                }
            } catch (Exception e) {
                Log.e(Constants.LOG, "failed to report click!");
                e.printStackTrace();
            }
        }
    }

    public static class ShowClickAnimViewThread implements Runnable {
        private DomobAdBuilder a = null;

        public ShowClickAnimViewThread(DomobAdBuilder b) {
            this.a = b;
        }

        public final void run() {
            if (this.a != null) {
                this.a.e();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void e() {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "show click anim view of builder");
        }
        this.f = false;
        if (this.h != null) {
            this.h.setVisibility(0);
        }
    }

    public final void f() {
        post(new ShowClickAnimViewThread(this));
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        if (isPressed() || isFocused()) {
            canvas.clipRect(3, 3, getWidth() - 3, getHeight() - 3);
        }
        super.onDraw(canvas);
        if (this.e == -1) {
            this.e = SystemClock.uptimeMillis();
        }
    }

    public final boolean onKeyDown(int keycode, KeyEvent keyevent) {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "onKeyDown:" + keycode + "|" + keyevent);
        }
        if (66 == keycode || 23 == keycode) {
            this.g = a(keyevent, this.g);
            setPressed(true);
        }
        return super.onKeyDown(keycode, keyevent);
    }

    public final boolean onKeyUp(int keycode, KeyEvent keyevent) {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "onKeyUp:" + keycode + "|" + keyevent);
        }
        if (i() && (66 == keycode || 23 == keycode)) {
            this.g = a(keyevent, this.g);
            j();
        }
        setPressed(false);
        return super.onKeyUp(keycode, keyevent);
    }

    public final boolean dispatchTouchEvent(MotionEvent motionevent) {
        boolean z;
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "dispatchTouchEvent");
        }
        if (i()) {
            if (this.a == null || this.a.d().contains((int) motionevent.getX(), (int) motionevent.getY())) {
                z = true;
            } else {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "dispatch touch event is out of ad rect!");
                }
                z = false;
            }
            int action = motionevent.getAction();
            if (z) {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "should record dispatch touch event!");
                }
                this.g = a(motionevent, this.g);
            }
            if (action == 0) {
                setPressed(z);
            } else if (2 == action) {
                setPressed(z);
            } else if (1 == action) {
                if (isPressed() && z) {
                    j();
                }
                setPressed(false);
            }
        }
        return super.onTouchEvent(motionevent);
    }

    public final boolean dispatchTrackballEvent(MotionEvent motionevent) {
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "dispatchTrackballEvent");
        }
        if (i()) {
            this.g = a(motionevent, this.g);
            int action = motionevent.getAction();
            if (action == 0) {
                setPressed(true);
            } else if (1 == action) {
                if (hasFocus()) {
                    j();
                }
                setPressed(false);
            }
        }
        return super.onTrackballEvent(motionevent);
    }

    private boolean i() {
        if (this.a == null || SystemClock.uptimeMillis() - this.e <= 0) {
            return false;
        }
        return true;
    }

    private void j() {
        if (this.a != null && isPressed()) {
            setPressed(false);
            if (!this.f) {
                this.f = true;
                JSONObject k = k();
                if (this.h != null) {
                    AnimationSet animationSet = new AnimationSet(true);
                    float width = ((float) this.h.getWidth()) / 2.0f;
                    float height = ((float) this.h.getHeight()) / 2.0f;
                    ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.2f, 1.0f, 1.2f, width, height);
                    scaleAnimation.setDuration(200);
                    animationSet.addAnimation(scaleAnimation);
                    ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.2f, 0.001f, 1.2f, 0.001f, width, height);
                    scaleAnimation2.setDuration(299);
                    scaleAnimation2.setStartOffset(200);
                    scaleAnimation2.setAnimationListener(this);
                    animationSet.addAnimation(scaleAnimation2);
                    postDelayed(new ClickThread(k, this), 500);
                    this.h.startAnimation(animationSet);
                    return;
                }
                post(new ClickThread(k, this));
            }
        }
    }

    private static Vector<String> a(int i2, int i3, int i4, long j2, Vector<String> vector) {
        Vector<String> vector2;
        String format;
        if (vector == null) {
            vector2 = new Vector<>();
        } else {
            vector2 = vector;
        }
        float f2 = ((float) j2) / 1000.0f;
        if (i3 == -1 || i4 == -1) {
            format = String.format("{%d, %f}", Integer.valueOf(i2), Float.valueOf(f2));
        } else {
            format = String.format("{%d, %d, %d, %f}", Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Float.valueOf(f2));
        }
        vector2.add(format);
        return vector2;
    }

    private Vector<String> a(MotionEvent motionEvent, Vector<String> vector) {
        return a(motionEvent.getAction(), (int) motionEvent.getX(), (int) motionEvent.getY(), motionEvent.getEventTime() - this.e, vector);
    }

    private Vector<String> a(KeyEvent keyEvent, Vector<String> vector) {
        int action = keyEvent.getAction();
        long eventTime = keyEvent.getEventTime() - this.e;
        if (action == 0 || 1 == action) {
            return a(action, -1, -1, eventTime, vector);
        }
        return vector;
    }

    public final void setPressed(boolean flag) {
        if (!this.f || !flag) {
            if (isPressed() != flag) {
                if (flag) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "press down, show hightlight");
                    }
                    if (this.d != null) {
                        Drawable b2 = b(this.a);
                        i = b2;
                        if (b2 != null) {
                            this.d.setBackgroundDrawable(i);
                        }
                        this.d.bringToFront();
                    }
                    this.d.setVisibility(0);
                } else {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "press up, hide highlight");
                    }
                    this.d.setVisibility(4);
                }
                super.setPressed(flag);
                invalidate();
            }
        } else if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "already pressed and clicked!");
        }
    }

    private JSONObject k() {
        JSONObject jSONObject;
        Exception e2;
        try {
            JSONObject jSONObject2 = new JSONObject();
            a(this, jSONObject2);
            jSONObject2.put("screen", "unknown");
            JSONObject jSONObject3 = new JSONObject();
            try {
                jSONObject3.put("interactions", jSONObject2);
                return jSONObject3;
            } catch (Exception e3) {
                e2 = e3;
                jSONObject = jSONObject3;
                Log.e(Constants.LOG, "failed to get interactions!");
                e2.printStackTrace();
                return jSONObject;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            jSONObject = null;
            e2 = exc;
            Log.e(Constants.LOG, "failed to get interactions!");
            e2.printStackTrace();
            return jSONObject;
        }
    }

    private JSONObject l() {
        JSONObject jSONObject = null;
        if (this.g != null) {
            jSONObject = new JSONObject();
            try {
                jSONObject.put("touches", new JSONArray((Collection) this.g));
            } catch (Exception e2) {
                Log.e(Constants.LOG, "failed to get touch events!");
                e2.printStackTrace();
            }
        }
        return jSONObject;
    }

    private static void a(View view, JSONObject jSONObject) {
        if (view instanceof n) {
            n nVar = (n) view;
            JSONObject h2 = nVar.h();
            String g2 = nVar.g();
            if (!(h2 == null || g2 == null)) {
                try {
                    jSONObject.put(g2, h2);
                } catch (Exception e2) {
                    Log.e(Constants.LOG, "failed to build tracker!");
                    e2.printStackTrace();
                }
            }
        }
        if (view instanceof ViewGroup) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "build tracker for view group!");
            }
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
                a(viewGroup.getChildAt(i2), jSONObject);
            }
        }
    }

    public final void onAnimationEnd(Animation animation) {
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final String g() {
        return "container";
    }

    public final JSONObject h() {
        return l();
    }
}
