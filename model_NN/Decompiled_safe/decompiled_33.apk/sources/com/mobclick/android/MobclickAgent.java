package com.mobclick.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import javax.microedition.khronos.opengles.GL10;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MobclickAgent implements e {
    public static boolean IMEI_MD5 = false;
    public static boolean LOCATION_OPEN = true;
    /* access modifiers changed from: private */
    public static final MobclickAgent a = new MobclickAgent();
    private static final String b = "MobclickAgent";
    private static final String c = "Android";
    private static final String d = "Android";
    private static final String e = "2.1.1";
    private static final long f = 30000;
    private static final String g = "http://www.umeng.com/app_logs";
    private static final String h = "http://www.umeng.com/api/check_app_update";
    private static final int i = 8;
    private static int j = 1;
    private static String m = "";
    private static String n = "";
    private static boolean o = true;
    private static UmengUpdateListener p = null;
    private static UmengFeedbackListener q = null;
    private static boolean r = true;
    private static JSONObject s = null;
    public static boolean updateAutoPopup = true;
    private Context k;
    private final Handler l;

    private MobclickAgent() {
        HandlerThread handlerThread = new HandlerThread(b);
        handlerThread.start();
        this.l = new Handler(handlerThread.getLooper());
    }

    private static String a(Context context) {
        try {
            return ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
        } catch (Exception e2) {
            Log.i(b, "Could not read MAC, forget to include ACCESS_WIFI_STATE permission?", e2);
            return null;
        }
    }

    private String a(Context context, SharedPreferences sharedPreferences) {
        Long valueOf = Long.valueOf(System.currentTimeMillis());
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putLong("start_millis", valueOf.longValue());
        edit.putLong("end_millis", -1);
        edit.commit();
        return sharedPreferences.getString("session_id", null);
    }

    private String a(Context context, String str, SharedPreferences sharedPreferences) {
        c(context, sharedPreferences);
        long currentTimeMillis = System.currentTimeMillis();
        String str2 = String.valueOf(str) + String.valueOf(currentTimeMillis);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("appkey", str);
        edit.putString("session_id", str2);
        edit.putLong("start_millis", currentTimeMillis);
        edit.putLong("end_millis", -1);
        edit.putLong("duration", 0);
        edit.putString("activities", "");
        edit.commit();
        b(context, sharedPreferences);
        return str2;
    }

    private static String a(Context context, JSONObject jSONObject, String str) {
        Log.i(b, jSONObject.toString());
        HttpPost httpPost = new HttpPost(str);
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 20000);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        try {
            String a2 = n.a(context);
            if (a2 != null) {
                Log.i("TAG", "Proxy IP:" + a2);
                defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(a2, 80));
            }
            ArrayList arrayList = new ArrayList(1);
            arrayList.add(new BasicNameValuePair("content", jSONObject.toString()));
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            if (execute.getStatusLine().getStatusCode() == 200) {
                Log.i(b, "Sent message to " + str);
                HttpEntity entity = execute.getEntity();
                if (entity != null) {
                    return a(entity.getContent());
                }
                return null;
            }
            Log.i(b, "Failed to send message.");
            return null;
        } catch (ClientProtocolException e2) {
            Log.i(b, "ClientProtocolException,Failed to send message.", e2);
            return null;
        } catch (IOException e3) {
            Log.i(b, "IOException,Failed to send message.", e3);
            return null;
        }
    }

    private static String a(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    try {
                        inputStream.close();
                        return sb.toString();
                    } catch (IOException e2) {
                        Log.e(b, "Caught IOException in convertStreamToString()", e2);
                        return null;
                    }
                } else {
                    sb.append(String.valueOf(readLine) + "\n");
                }
            } catch (IOException e3) {
                Log.e(b, "Caught IOException in convertStreamToString()", e3);
                try {
                    inputStream.close();
                    return null;
                } catch (IOException e4) {
                    Log.e(b, "Caught IOException in convertStreamToString()", e4);
                    return null;
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                    throw th;
                } catch (IOException e5) {
                    Log.e(b, "Caught IOException in convertStreamToString()", e5);
                    return null;
                }
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v6, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String a(java.lang.String r5) {
        /*
            java.lang.String r0 = "MD5"
            java.security.MessageDigest r0 = java.security.MessageDigest.getInstance(r0)     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            byte[] r1 = r5.getBytes()     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            r0.update(r1)     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            byte[] r0 = r0.digest()     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            java.lang.String r2 = ""
            r1.<init>(r2)     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            r2 = 0
        L_0x0019:
            int r3 = r0.length     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            if (r2 < r3) goto L_0x0021
            java.lang.String r0 = r1.toString()     // Catch:{ NoSuchAlgorithmException -> 0x003a }
        L_0x0020:
            return r0
        L_0x0021:
            byte r3 = r0[r2]     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            if (r3 >= 0) goto L_0x0027
            int r3 = r3 + 256
        L_0x0027:
            r4 = 16
            if (r3 >= r4) goto L_0x0030
            java.lang.String r4 = "0"
            r1.append(r4)     // Catch:{ NoSuchAlgorithmException -> 0x003a }
        L_0x0030:
            java.lang.String r3 = java.lang.Integer.toHexString(r3)     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            r1.append(r3)     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            int r2 = r2 + 1
            goto L_0x0019
        L_0x003a:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r0 = ""
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclick.android.MobclickAgent.a(java.lang.String):java.lang.String");
    }

    private void a(Context context, SharedPreferences sharedPreferences, String str, String str2, int i2) {
        String string = sharedPreferences.getString("session_id", "");
        String b2 = b();
        String str3 = b2.split(" ")[0];
        String str4 = b2.split(" ")[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", "event");
            jSONObject.put("session_id", string);
            jSONObject.put("date", str3);
            jSONObject.put("time", str4);
            jSONObject.put("tag", str);
            jSONObject.put("label", str2);
            jSONObject.put("acc", i2);
            this.l.post(new j(this, context, jSONObject));
        } catch (JSONException e2) {
        }
    }

    private synchronized void a(Context context, String str) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", "update");
            jSONObject.put("appkey", str);
            int i2 = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
            String packageName = context.getPackageName();
            jSONObject.put("version_code", i2);
            jSONObject.put("package", packageName);
            this.l.post(new j(this, context, jSONObject));
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(Context context, String str, String str2) {
        this.k = context;
        SharedPreferences m2 = m(context);
        if (m2 != null) {
            if (a(m2)) {
                Log.i(b, "Start new session: " + a(context, str, m2));
            } else {
                Log.i(b, "Extend current session: " + a(context, m2));
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(Context context, String str, String str2, String str3, int i2) {
        SharedPreferences m2 = m(context);
        if (m2 != null) {
            a(context, m2, str2, str3, i2);
        }
    }

    private synchronized void a(Context context, JSONObject jSONObject) {
        if (jSONObject != null) {
            b(context, jSONObject);
        }
    }

    private boolean a(SharedPreferences sharedPreferences) {
        return System.currentTimeMillis() - sharedPreferences.getLong("end_millis", -1) > f;
    }

    private static boolean a(String str, Context context) {
        if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) == 0 && !p(context)) {
            return false;
        }
        if (str == "update" || str == "feedback") {
            return true;
        }
        if (j == 3) {
            if (str == "flush") {
                return true;
            }
        } else if (str == "error") {
            return true;
        } else {
            if (j == 1 && str == "launch") {
                return true;
            }
            if (j == 2 && str == "terminate") {
                return true;
            }
            if (j == 0) {
                return true;
            }
            if (j == 4) {
                String string = l(context).getString(k.A(), "false");
                Log.i(b, "Log has been sent today: " + string + ";type:" + str);
                return !string.equals("true") && str.equals("launch");
            } else if (j == 5) {
                return q(context)[0].equals("Wi-Fi");
            }
        }
        return false;
    }

    private static String b() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }

    private static String b(Context context) {
        String str = null;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                String string = applicationInfo.metaData.getString("UMENG_APPKEY");
                if (string != null) {
                    str = string;
                } else {
                    Log.i(b, "Could not read UMENG_APPKEY meta-data from AndroidManifest.xml.");
                }
            }
        } catch (Exception e2) {
            Log.i(b, "Could not read UMENG_APPKEY meta-data from AndroidManifest.xml.", e2);
        }
        return str.trim();
    }

    private void b(Context context, SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString("session_id", null);
        if (string == null) {
            Log.e(b, "Missing session_id, ignore message");
            return;
        }
        String b2 = b();
        String str = b2.split(" ")[0];
        String str2 = b2.split(" ")[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", "launch");
            jSONObject.put("session_id", string);
            jSONObject.put("date", str);
            jSONObject.put("time", str2);
            this.l.post(new j(this, context, jSONObject));
        } catch (JSONException e2) {
        }
    }

    /* access modifiers changed from: private */
    public synchronized void b(Context context, String str) {
        String d2 = d(context);
        if (d2 != "" && d2.length() <= 10240) {
            c(context, d2);
        }
    }

    private void b(Context context, JSONObject jSONObject) {
        String b2 = b();
        String str = b2.split(" ")[0];
        String str2 = b2.split(" ")[1];
        try {
            jSONObject.put("type", "feedback");
            jSONObject.put("date", str);
            jSONObject.put("time", str2);
            this.l.post(new j(this, context, jSONObject));
        } catch (JSONException e2) {
            q.onFeedbackReturned(FeedbackStatus.FAILED);
        }
    }

    private static String c(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo == null || applicationInfo.metaData == null) {
                return "Unknown";
            }
            String string = applicationInfo.metaData.getString("UMENG_CHANNEL");
            if (string != null) {
                return string;
            }
            Log.i(b, "Could not read UMENG_CHANNEL meta-data from AndroidManifest.xml.");
            return "Unknown";
        } catch (Exception e2) {
            Log.i(b, "Could not read UMENG_CHANNEL meta-data from AndroidManifest.xml.", e2);
            return "Unknown";
        }
    }

    private void c(Context context, SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString("session_id", null);
        if (string == null) {
            Log.w(b, "Missing session_id, ignore message");
            return;
        }
        Long valueOf = Long.valueOf(sharedPreferences.getLong("duration", -1));
        if (valueOf.longValue() <= 0) {
            valueOf = 0L;
        }
        String b2 = b();
        String str = b2.split(" ")[0];
        String str2 = b2.split(" ")[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", "terminate");
            jSONObject.put("session_id", string);
            jSONObject.put("date", str);
            jSONObject.put("time", str2);
            jSONObject.put("duration", String.valueOf(valueOf.longValue() / 1000));
            if (o) {
                String[] split = sharedPreferences.getString("activities", "").split(";");
                JSONArray jSONArray = new JSONArray();
                for (String jSONArray2 : split) {
                    jSONArray.put(new JSONArray(jSONArray2));
                }
                jSONObject.put("activities", jSONArray);
            }
            this.l.post(new j(this, context, jSONObject));
        } catch (JSONException e2) {
        }
    }

    private void c(Context context, String str) {
        String b2 = b();
        String str2 = b2.split(" ")[0];
        String str3 = b2.split(" ")[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", "error");
            jSONObject.put("context", str);
            jSONObject.put("date", str2);
            jSONObject.put("time", str3);
            this.l.post(new j(this, context, jSONObject));
        } catch (JSONException e2) {
        }
    }

    /* access modifiers changed from: private */
    public void c(Context context, JSONObject jSONObject) {
        if (a("update", context)) {
            String a2 = a(context, jSONObject, h);
            Log.i(b, "return message from " + a2);
            if (a2 != null) {
                d(context, a2);
            } else if (p != null) {
                p.onUpdateReturned(UpdateStatus.Timeout);
            }
        } else if (p != null) {
            p.onUpdateReturned(UpdateStatus.No);
        }
    }

    private static AlertDialog d(Context context, JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("version");
            String string2 = jSONObject.getString("update_log");
            String string3 = jSONObject.getString("path");
            String str = "";
            if (!q(context)[0].equals("Wi-Fi")) {
                str = String.valueOf(k.j(context)) + "\n";
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(k.h(context)).setMessage(String.valueOf(str) + k.i(context) + string + "\n" + string2).setCancelable(false).setPositiveButton(k.k(context), new g(context, string3)).setNegativeButton(k.n(context), new h());
            AlertDialog create = builder.create();
            create.setCancelable(true);
            return create;
        } catch (Exception e2) {
            Log.e(b, "Fail to create update dialog box.", e2);
            return null;
        }
    }

    private static String d(Context context) {
        String str = "";
        try {
            String packageName = context.getPackageName();
            ArrayList arrayList = new ArrayList();
            arrayList.add("logcat");
            arrayList.add("-d");
            arrayList.add("-v");
            arrayList.add("raw");
            arrayList.add("-s");
            arrayList.add("AndroidRuntime:E");
            arrayList.add("-p");
            arrayList.add(packageName);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec((String[]) arrayList.toArray(new String[arrayList.size()])).getInputStream()), 1024);
            boolean z = false;
            String str2 = "";
            boolean z2 = false;
            for (String readLine = bufferedReader.readLine(); readLine != null; readLine = bufferedReader.readLine()) {
                if (readLine.indexOf("thread attach failed") < 0) {
                    str2 = String.valueOf(str2) + readLine + 10;
                }
                if (!z2 && readLine.toLowerCase().indexOf("exception") >= 0) {
                    z2 = true;
                }
                z = (z || readLine.indexOf(packageName) < 0) ? z : true;
            }
            if (str2.length() > 0 && z2 && z) {
                str = str2;
            }
            try {
                Runtime.getRuntime().exec("logcat -c");
            } catch (Exception e2) {
                Log.e(b, "Failed to clear log");
            }
        } catch (Exception e3) {
            Log.e(b, "Failed to catch error log");
        }
        return str;
    }

    private void d(Context context, String str) {
        try {
            s = new JSONObject(str);
            if (s.getString("update").equals("Yes")) {
                if (p != null) {
                    p.onUpdateReturned(UpdateStatus.Yes);
                }
                if (updateAutoPopup) {
                    showUpdateDialog(context);
                }
            } else if (p != null) {
                p.onUpdateReturned(UpdateStatus.No);
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: private */
    public synchronized void e(Context context) {
        if (this.k != context) {
            Log.e(b, "onPause() called without context from corresponding onResume()");
        } else {
            this.k = context;
            SharedPreferences m2 = m(context);
            if (m2 != null) {
                long j2 = m2.getLong("start_millis", -1);
                if (j2 == -1) {
                    Log.e(b, "onEndSession called before onStartSession");
                } else {
                    long currentTimeMillis = System.currentTimeMillis();
                    long j3 = currentTimeMillis - j2;
                    long j4 = m2.getLong("duration", 0);
                    SharedPreferences.Editor edit = m2.edit();
                    if (o) {
                        String string = m2.getString("activities", "");
                        String name = context.getClass().getName();
                        if (!"".equals(string)) {
                            string = String.valueOf(string) + ";";
                        }
                        edit.remove("activities");
                        edit.putString("activities", String.valueOf(string) + "[" + name + "," + (j3 / 1000) + "]");
                    }
                    edit.putLong("start_millis", -1);
                    edit.putLong("end_millis", currentTimeMillis);
                    edit.putLong("duration", j3 + j4);
                    edit.commit();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void e(Context context, JSONObject jSONObject) {
        JSONObject j2 = j(context);
        if (j2 == null) {
            Log.e(b, "Fail to construct message header");
            return;
        }
        JSONObject h2 = h(context);
        JSONObject jSONObject2 = new JSONObject();
        try {
            String string = jSONObject.getString("type");
            if (string != null) {
                if (string != "flush") {
                    jSONObject.remove("type");
                    if (h2 == null) {
                        h2 = new JSONObject();
                        JSONArray jSONArray = new JSONArray();
                        jSONArray.put(jSONObject);
                        h2.put(string, jSONArray);
                    } else if (h2.isNull(string)) {
                        JSONArray jSONArray2 = new JSONArray();
                        jSONArray2.put(jSONObject);
                        h2.put(string, jSONArray2);
                    } else {
                        h2.getJSONArray(string).put(jSONObject);
                    }
                }
                if (h2 == null) {
                    Log.w(b, "No cache message to flush");
                    return;
                }
                jSONObject2.put("header", j2);
                jSONObject2.put("body", h2);
                if (a(string, context)) {
                    if (a(context, jSONObject2, g) != null) {
                        Log.i(b, "send message succeed, clear cache");
                        if (string.equals("feedback")) {
                            q.onFeedbackReturned(FeedbackStatus.SUCCEED);
                        }
                        i(context);
                        if (j == 4) {
                            SharedPreferences.Editor edit = l(context).edit();
                            edit.putString(k.A(), "true");
                            edit.commit();
                            return;
                        }
                        return;
                    } else if (string.equals("feedback")) {
                        q.onFeedbackReturned(FeedbackStatus.FAILED);
                    }
                } else if (string.equals("feedback")) {
                    q.onFeedbackReturned(FeedbackStatus.DISCONNECT);
                }
                f(context, h2);
            }
        } catch (JSONException e2) {
            Log.e(b, "Fail to construct json message.");
            i(context);
        }
    }

    public static void enterPage(Context context, String str) {
        onEvent(context, "_PAGE_", str);
    }

    private synchronized void f(Context context) {
        g(context);
    }

    private static void f(Context context, JSONObject jSONObject) {
        try {
            FileOutputStream openFileOutput = context.openFileOutput(o(context), 0);
            openFileOutput.write(jSONObject.toString().getBytes());
            openFileOutput.close();
        } catch (FileNotFoundException | IOException e2) {
        }
    }

    public static void flush(Context context) {
        if (context == null) {
            try {
                Log.e(b, "unexpected null context");
            } catch (Exception e2) {
                Log.e(b, "Exception occurred in Mobclick.flush(). ");
                return;
            }
        }
        a.f(context);
    }

    private void g(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", "flush");
            this.l.post(new j(this, context, jSONObject));
        } catch (JSONException e2) {
        }
    }

    private static JSONObject h(Context context) {
        try {
            FileInputStream openFileInput = context.openFileInput(o(context));
            String str = "";
            byte[] bArr = new byte[16384];
            while (true) {
                int read = openFileInput.read(bArr);
                if (read == -1) {
                    break;
                }
                str = String.valueOf(str) + new String(bArr, 0, read);
            }
            if (str.length() == 0) {
                return null;
            }
            try {
                return new JSONObject(str);
            } catch (JSONException e2) {
                openFileInput.close();
                i(context);
                return null;
            }
        } catch (FileNotFoundException e3) {
            return null;
        } catch (IOException e4) {
            return null;
        }
    }

    private static void i(Context context) {
        context.deleteFile(n(context));
        context.deleteFile(o(context));
    }

    public static void initFeedback(Context context) {
        UmengFeedback.a(context);
        UmengFeedback.a(a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException} */
    private static JSONObject j(Context context) {
        SharedPreferences k2 = k(context);
        JSONObject jSONObject = new JSONObject();
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager == null) {
                Log.w(b, "No IMEI.");
                return null;
            }
            String str = "";
            try {
                if (l.a(context, "android.permission.READ_PHONE_STATE")) {
                    str = telephonyManager.getDeviceId();
                    if (IMEI_MD5) {
                        str = a(str);
                    }
                }
            } catch (Exception e2) {
            }
            if (TextUtils.isEmpty(str)) {
                Log.w(b, "No IMEI.");
                str = a(context);
                if (str == null) {
                    Log.w(b, "Failed to take mac as IMEI.");
                    return null;
                }
            }
            jSONObject.put("device_id", str);
            jSONObject.put("device_model", Build.MODEL);
            String b2 = b(context);
            if (b2 == null) {
                Log.e(b, "No appkey");
                return null;
            }
            jSONObject.put("appkey", b2);
            jSONObject.put("channel", c(context));
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                String str2 = packageInfo.versionName;
                int i2 = packageInfo.versionCode;
                jSONObject.put("app_version", str2);
                jSONObject.put("version_code", i2);
            } catch (PackageManager.NameNotFoundException e3) {
                jSONObject.put("app_version", "unknown");
                jSONObject.put("version_code", "unknown");
            }
            jSONObject.put("sdk_type", "Android");
            jSONObject.put("sdk_version", e);
            jSONObject.put("os", "Android");
            jSONObject.put("os_version", Build.VERSION.RELEASE);
            Configuration configuration = new Configuration();
            Settings.System.getConfiguration(context.getContentResolver(), configuration);
            if (configuration == null || configuration.locale == null) {
                jSONObject.put("country", "Unknown");
                jSONObject.put("language", "Unknown");
                jSONObject.put("timezone", (int) i);
            } else {
                jSONObject.put("country", configuration.locale.getCountry());
                jSONObject.put("language", configuration.locale.toString());
                Calendar instance = Calendar.getInstance(configuration.locale);
                if (instance != null) {
                    TimeZone timeZone = instance.getTimeZone();
                    if (timeZone != null) {
                        jSONObject.put("timezone", timeZone.getRawOffset() / 3600000);
                    } else {
                        jSONObject.put("timezone", (int) i);
                    }
                } else {
                    jSONObject.put("timezone", (int) i);
                }
            }
            try {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
                jSONObject.put("resolution", String.valueOf(String.valueOf(displayMetrics.heightPixels)) + "*" + String.valueOf(displayMetrics.widthPixels));
            } catch (Exception e4) {
                jSONObject.put("resolution", "Unknown");
            }
            try {
                String[] q2 = q(context);
                jSONObject.put("access", q2[0]);
                if (q2[0].equals("2G/3G")) {
                    jSONObject.put("access_subtype", q2[1]);
                }
            } catch (Exception e5) {
                jSONObject.put("access", "Unknown");
            }
            try {
                jSONObject.put("carrier", telephonyManager.getNetworkOperatorName());
            } catch (Exception e6) {
                jSONObject.put("carrier", "Unknown");
            }
            if (LOCATION_OPEN) {
                Location r2 = r(context);
                if (r2 != null) {
                    jSONObject.put("lat", String.valueOf(r2.getLatitude()));
                    jSONObject.put("lng", String.valueOf(r2.getLongitude()));
                } else {
                    jSONObject.put("lat", 0.0d);
                    jSONObject.put("lng", 0.0d);
                }
            }
            jSONObject.put("cpu", l.a());
            if (!m.equals("")) {
                jSONObject.put("gpu_vender", m);
            }
            if (!n.equals("")) {
                jSONObject.put("gpu_renderer", n);
            }
            SharedPreferences.Editor edit = k2.edit();
            edit.putString("header", jSONObject.toString());
            edit.commit();
            return jSONObject;
        } catch (JSONException e7) {
            return null;
        } catch (SecurityException e8) {
            Log.e(b, "Failed to get IMEI. Forget to add permission READ_PHONE_STATE? ", e8);
            return null;
        }
    }

    private static SharedPreferences k(Context context) {
        return context.getSharedPreferences("mobclick_agent_header_" + context.getPackageName(), 0);
    }

    private static SharedPreferences l(Context context) {
        return context.getSharedPreferences("mobclick_agent_update_" + context.getPackageName(), 0);
    }

    private static SharedPreferences m(Context context) {
        return context.getSharedPreferences("mobclick_agent_state_" + context.getPackageName(), 0);
    }

    private static String n(Context context) {
        return "mobclick_agent_header_" + context.getPackageName();
    }

    private static String o(Context context) {
        return "mobclick_agent_cached_" + context.getPackageName();
    }

    public static void onError(Context context) {
        try {
            String b2 = b(context);
            if (b2 == null || b2.length() == 0) {
                Log.e(b, "unexpected empty appkey");
            } else if (context == null) {
                Log.e(b, "unexpected null context");
            } else {
                new i(context, b2, 2).start();
            }
        } catch (Exception e2) {
            Log.e(b, "Exception occurred in Mobclick.onError()");
        }
    }

    public static void onEvent(Context context, String str) {
        onEvent(context, str, 1);
    }

    public static void onEvent(Context context, String str, int i2) {
        onEvent(context, str, str, i2);
    }

    public static void onEvent(Context context, String str, String str2) {
        onEvent(context, str, str2, 1);
    }

    public static void onEvent(Context context, String str, String str2, int i2) {
        try {
            String b2 = b(context);
            if (b2 == null || b2.length() == 0) {
                Log.e(b, "unexpected empty appkey");
            } else if (context == null) {
                Log.e(b, "unexpected null context");
            } else if (str == null || str == "") {
                Log.e(b, "tag is null or empty");
            } else if (str2 == null || str2 == "") {
                Log.e(b, "label is null or empty");
            } else if (i2 <= 0) {
                Log.e(b, "Illegal value of acc");
            } else {
                new i(context, b2, str, str2, i2, 3).start();
            }
        } catch (Exception e2) {
            Log.e(b, "Exception occurred in Mobclick.onEvent(). ");
        }
    }

    public static void onPause(Context context) {
        if (context == null) {
            try {
                Log.e(b, "unexpected null context");
            } catch (Exception e2) {
                Log.e(b, "Exception occurred in Mobclick.onRause(). ");
            }
        } else {
            new i(context, 0).start();
        }
    }

    public static void onResume(Context context) {
        onResume(context, b(context), c(context));
    }

    public static void onResume(Context context, String str, String str2) {
        if (context == null) {
            try {
                Log.e(b, "unexpected null context");
            } catch (Exception e2) {
                Log.e(b, "Exception occurred in Mobclick.onResume(). ");
            }
        } else if (str == null || str.length() == 0) {
            Log.e(b, "unexpected empty appkey");
        } else {
            new i(context, str, str2, 1).start();
        }
    }

    public static void openActivityDurationTrack(boolean z) {
        o = z;
    }

    public static void openFeedbackActivity(Context context) {
        UmengFeedback.a(context);
        UmengFeedback.a(a);
        context.startActivity(new Intent(context, UmengFeedback.class));
    }

    private static boolean p(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        if (connectivityManager.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }
        return connectivityManager.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED;
    }

    private static String[] q(Context context) {
        String[] strArr = {"Unknown", "Unknown"};
        if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) != 0) {
            strArr[0] = "Unknown";
        } else {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null) {
                strArr[0] = "Unknown";
            } else if (connectivityManager.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
                strArr[0] = "Wi-Fi";
            } else {
                NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
                if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                    strArr[0] = "2G/3G";
                    strArr[1] = networkInfo.getSubtypeName();
                }
            }
        }
        return strArr;
    }

    private static Location r(Context context) {
        return new f(context).a();
    }

    public static void setFeedbackListener(UmengFeedbackListener umengFeedbackListener) {
        q = umengFeedbackListener;
    }

    public static void setOpenGLContext(GL10 gl10) {
        if (gl10 != null) {
            String[] a2 = l.a(gl10);
            if (a2.length == 2) {
                m = a2[0];
                n = a2[1];
            }
        }
    }

    public static void setReportPolicy(int i2) {
        if (i2 < 0 || i2 > 5) {
            Log.e(b, "Illegal value of report policy");
        } else {
            j = i2;
        }
    }

    public static void setUpdateListener(UmengUpdateListener umengUpdateListener) {
        p = umengUpdateListener;
    }

    public static void setUpdateOnlyWifi(boolean z) {
        r = z;
    }

    public static void showUpdateDialog(Context context) {
        if (s != null) {
            d(context, s).show();
        }
    }

    public static void update(Context context) {
        try {
            if (r && !q(context)[0].equals("Wi-Fi")) {
                p.onUpdateReturned(UpdateStatus.NoneWifi);
            } else if (context == null) {
                p.onUpdateReturned(UpdateStatus.No);
                Log.e(b, "unexpected null context");
            } else {
                a.a(context, b(context));
            }
        } catch (Exception e2) {
            Log.e(b, "Exception occurred in Mobclick.update(). " + e2.getMessage());
        }
    }

    public void onFeedbackReturned(JSONObject jSONObject) {
        try {
            if (this.k != null) {
                a.a(this.k, jSONObject);
            }
        } catch (Exception e2) {
            Log.e(b, "Exception occurred while sending feedback.");
        }
    }
}
