package com.mdotm.android.ads;

/* compiled from: MdotMView */
class AdUnit {
    public static final int AD_BANNER = 1;
    public static final int AD_ICON_WITH_TEXT_MESSAGE = 2;
    public static final int AD_LAUNCH_TYPE_ON_CANVAS = 2;
    public static final int AD_LAUNCH_TYPE_ON_SAFARI = 1;
    public String adText;
    public int adType;
    public String dest_url;
    public String imageUrl;
    public String landingUrl;
    public int launchType;
}
