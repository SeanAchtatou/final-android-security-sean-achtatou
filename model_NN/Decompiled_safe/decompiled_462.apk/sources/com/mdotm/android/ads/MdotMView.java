package com.mdotm.android.ads;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.inmobi.androidsdk.impl.Constants;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

public class MdotMView extends RelativeLayout {
    private static final Typeface AD_FONT = Typeface.create(Typeface.SANS_SERIF, 1);
    private static final double GRADIENT_STOP = 0.4375d;
    private static final int GRADIENT_TOP_ALPHA = 127;
    private static final int HIGHLIGHT_BACKGROUND_COLOR = -1147097;
    private static final int HIGHLIGHT_COLOR = -19456;
    public static final String LOG_TAG = "MdotM SDK (MdotMView)";
    private static final String MDOTM_BASE_URL = "http://ads.mdotm.com/ads/feed.php?";
    /* access modifiers changed from: private */
    public ProgressBar activityIndicator;
    private String adLandingUrl;
    private Drawable adSelectedBackground;
    private boolean adSelectionInProgress;
    private int backgroundColor;
    /* access modifiers changed from: private */
    public Drawable defaultBackground;
    private String dest_url;
    private boolean isAdClicked;
    private MdotMActionListener listener;
    /* access modifiers changed from: private */
    public boolean onLandingPage;
    private int padding;
    private int textColor;

    public interface MdotMActionListener {
        void adRequestCompletedSuccessfully(MdotMView mdotMView);

        void adRequestFailed(MdotMView mdotMView);
    }

    public MdotMView(Context context, MdotMActionListener listener2) {
        super(context);
        this.onLandingPage = false;
        this.textColor = -1;
        this.backgroundColor = -16777216;
        this.isAdClicked = false;
        this.listener = listener2;
        init();
        setAdSelectionInProgress(false);
    }

    public MdotMView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MdotMView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.onLandingPage = false;
        this.textColor = -1;
        this.backgroundColor = -16777216;
        this.isAdClicked = false;
    }

    private void init() {
        fetchAd();
        setFocusable(true);
        setDescendantFocusability(262144);
        setClickable(true);
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        if (!this.isAdClicked && action == 1) {
            this.isAdClicked = true;
            clicked();
        }
        return super.dispatchKeyEvent(event);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        int left = getLeft();
        int top = getTop();
        int right = getRight();
        int bottom = getBottom();
        Log.i(LOG_TAG, "  Selected  ");
        if (x < ((float) left) || x > ((float) right) || y < ((float) top) || y > ((float) bottom)) {
            Log.i(LOG_TAG, "Always outside of ad display area ");
            if (!this.isAdClicked) {
                drawBackgroundView(true);
            }
        } else if (action == 1) {
            if (!this.isAdClicked) {
                this.isAdClicked = true;
                setClickable(false);
                clicked();
                drawBackgroundView(true);
            }
        } else if (action == 0 && !this.isAdClicked) {
            drawBackgroundView(false);
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    private void drawBackgroundView(boolean isDefaultBackground) {
        Log.d("X-Value", " drawBackgroundView   :: " + isDefaultBackground);
        if (this.defaultBackground == null) {
            this.defaultBackground = populateDrawableBackGround(-1, this.backgroundColor);
        }
        if (this.adSelectedBackground == null) {
            this.adSelectedBackground = populateDrawableBackGround(HIGHLIGHT_BACKGROUND_COLOR, HIGHLIGHT_COLOR);
        }
        if (isDefaultBackground) {
            setBackgroundDrawable(this.defaultBackground);
        } else {
            setBackgroundDrawable(this.adSelectedBackground);
        }
    }

    /* JADX INFO: Multiple debug info for r8v8 android.graphics.Paint: [D('stop' int), D('shadowPaint' android.graphics.Paint)] */
    private static Drawable populateDrawableBackGround(int backgroundColor2, int color) {
        Rect rect = new Rect(0, 0, Constants.INMOBI_ADVIEW_WIDTH, 48);
        Bitmap bitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(backgroundColor2);
        paint.setAntiAlias(true);
        canvas.drawRect(rect, paint);
        GradientDrawable shine = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb((int) GRADIENT_TOP_ALPHA, Color.red(color), Color.green(color), Color.blue(color)), color});
        int stop = ((int) (((double) rect.height()) * GRADIENT_STOP)) + rect.top;
        shine.setBounds(rect.left, rect.top, rect.right, stop);
        shine.draw(canvas);
        Rect shadowRect = new Rect(rect.left, stop, rect.right, rect.bottom);
        Paint shadowPaint = new Paint();
        shadowPaint.setColor(color);
        canvas.drawRect(shadowRect, shadowPaint);
        return new BitmapDrawable(bitmap);
    }

    private void clicked() {
        Log.i(LOG_TAG, "  Selected to clicked  ");
        if (this.adLandingUrl == null) {
            Log.w(LOG_TAG, "selected ad is null");
        } else if (isAdSelectionInProgress()) {
            Log.w(LOG_TAG, "ad selection under progress");
        } else if (this.dest_url.toLowerCase().startsWith("market", 0)) {
            try {
                HttpResponse httpResponse = new DefaultHttpClient().execute(new HttpGet(this.adLandingUrl));
                Log.d(LOG_TAG, httpResponse.getStatusLine().toString());
                if (httpResponse.getEntity() != null) {
                    getRootView().getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.dest_url)));
                }
            } catch (ClientProtocolException e) {
                Log.e(LOG_TAG, "Caught ClientProtocolException in getCustom()", e);
            } catch (IOException e2) {
                Log.e(LOG_TAG, "Caught IOException in getCustom()", e2);
            } catch (IllegalStateException e3) {
                IllegalStateException e4 = e3;
                Log.e(LOG_TAG, "Caught IllegalStateException in getCustom()", e4);
                if (e4.getMessage().contentEquals("Scheme 'market' not registered.")) {
                    getRootView().getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.dest_url)));
                }
            }
        } else {
            String clickedUrl = this.adLandingUrl;
            setAdSelectionInProgress(true);
            showActivityIndicator();
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            Activity rootActivity = (Activity) getRootView().getContext();
            WebView webView = new WebView(rootActivity);
            Button button = new Button(rootActivity);
            Button button2 = new Button(rootActivity);
            TextView textView = new TextView(rootActivity);
            rootActivity.addContentView(webView, layoutParams);
            webView.getSettings().setJavaScriptEnabled(true);
            Activity activity = rootActivity;
            final WebView wvLP = webView;
            final Button wvButton = button;
            Button wvMenuBar = button2;
            final TextView wvLoading = textView;
            final RelativeLayout relLay = new RelativeLayout(activity);
            final Activity activity2 = activity;
            webView.setWebChromeClient(new WebChromeClient() {
                public void onProgressChanged(WebView view, int progress) {
                    activity2.setProgress(progress * 1000);
                }
            });
            final Activity activity3 = activity;
            webView.setWebViewClient(new WebViewClient() {
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    Toast.makeText(activity3, "Oh no! " + description, 0).show();
                }
            });
            webView.setPictureListener(new WebView.PictureListener() {
                public void onNewPicture(WebView view, Picture picture) {
                    Button button = wvButton;
                    final WebView webView = wvLP;
                    final RelativeLayout relativeLayout = relLay;
                    button.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            webView.setVisibility(4);
                            relativeLayout.removeAllViews();
                            MdotMView.this.onLandingPage = false;
                        }
                    });
                    if (!MdotMView.this.onLandingPage) {
                        MdotMView.this.adNetworkCompleted();
                        wvLoading.setVisibility(4);
                        MdotMView.this.onLandingPage = true;
                    }
                }
            });
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
            RelativeLayout.LayoutParams buttonParams = new RelativeLayout.LayoutParams(-2, -2);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
            RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -2);
            buttonParams.addRule(12);
            buttonParams.height = 40;
            layoutParams4.addRule(12);
            layoutParams4.addRule(11);
            layoutParams4.height = 50;
            layoutParams3.addRule(12);
            layoutParams3.addRule(11);
            layoutParams3.rightMargin = 5;
            layoutParams3.bottomMargin = 5;
            wvButton.setText("BACK");
            wvButton.setTextSize(8.0f);
            wvButton.setLayoutParams(buttonParams);
            wvMenuBar.setBackgroundDrawable(populateDrawableBackGround(-1, -12303292));
            wvMenuBar.setLayoutParams(layoutParams4);
            wvMenuBar.setClickable(false);
            wvLoading.setText("loading ....");
            wvLoading.setLayoutParams(layoutParams3);
            relLay.addView(wvMenuBar);
            relLay.addView(wvButton);
            relLay.addView(wvLoading);
            activity.addContentView(relLay, layoutParams2);
            webView.loadUrl(clickedUrl);
        }
    }

    /* access modifiers changed from: private */
    public void adNetworkCompleted() {
        Log.w(LOG_TAG, "On ad network completed");
        setClickable(true);
        setAdSelectionInProgress(false);
        this.isAdClicked = false;
        hideActivityIndicator();
    }

    private void hideActivityIndicator() {
        post(new Thread() {
            public void run() {
                if (MdotMView.this.activityIndicator != null) {
                    MdotMView.this.activityIndicator.setVisibility(4);
                    MdotMView.this.setBackgroundDrawable(MdotMView.this.defaultBackground);
                }
            }
        });
    }

    private void showActivityIndicator() {
        post(new Thread() {
            public void run() {
                if (MdotMView.this.activityIndicator != null) {
                    MdotMView.this.activityIndicator.setVisibility(0);
                    MdotMView.this.activityIndicator.setBackgroundDrawable(MdotMView.this.defaultBackground);
                }
            }
        });
    }

    private void setAdSelectionInProgress(boolean isProgress) {
        this.adSelectionInProgress = isProgress;
    }

    private boolean isAdSelectionInProgress() {
        return this.adSelectionInProgress;
    }

    private String generateURLString() {
        StringBuilder urlBuilder = new StringBuilder(MDOTM_BASE_URL);
        urlBuilder.append("appver=");
        urlBuilder.append(260);
        urlBuilder.append("&v=");
        urlBuilder.append(MdotMManager.getSystemVersion());
        urlBuilder.append("&apikey=");
        urlBuilder.append("mdotm_android");
        urlBuilder.append("&appkey=");
        urlBuilder.append(MdotMManager.getAppKey());
        urlBuilder.append("&deviceid=");
        try {
            urlBuilder.append(MdotMManager.getDeviceId(getContext()));
        } catch (NoSuchAlgorithmException e) {
            urlBuilder.append("");
        }
        urlBuilder.append("&aid=");
        urlBuilder.append(MdotMManager.getAndroidId(getContext()));
        urlBuilder.append("&width=");
        urlBuilder.append((int) Constants.INMOBI_ADVIEW_WIDTH);
        urlBuilder.append("&height=");
        urlBuilder.append(50);
        urlBuilder.append("&rand=");
        urlBuilder.append(new Integer((int) (Math.random() * 1024.0d)));
        urlBuilder.append("&fmt=");
        urlBuilder.append("json");
        urlBuilder.append("&ua=");
        try {
            urlBuilder.append(URLEncoder.encode(MdotMManager.getUserAgent(), "UTF-8"));
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        urlBuilder.append("&test=");
        urlBuilder.append(MdotMManager.getTestModeValue());
        return urlBuilder.toString();
    }

    private void fetchAd() {
        Log.d(LOG_TAG, "  fetching Ad started ");
        String urlString = generateURLString();
        Log.d(LOG_TAG, "   Genrerated url " + urlString);
        try {
            HttpResponse httpResponse = new DefaultHttpClient().execute(new HttpGet(urlString));
            Log.d(LOG_TAG, httpResponse.getStatusLine().toString());
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                initializeAdView(generateAdUnitFromJsonString(convertStreamToString(entity.getContent())), getContext());
            }
        } catch (ClientProtocolException e) {
            Log.e(LOG_TAG, "Caught ClientProtocolException in getCustom()", e);
        } catch (IOException e2) {
            Log.e(LOG_TAG, "Caught IOException in getCustom()", e2);
        }
    }

    private void initializeAdView(AdUnit ad, Context context) {
        if (ad != null) {
            this.adLandingUrl = ad.landingUrl;
            this.dest_url = ad.dest_url;
            setFocusable(true);
            setClickable(true);
            Bitmap icon = fetchImage(ad.imageUrl, false);
            this.padding = 8;
            if (icon != null) {
                float density = getResources().getDisplayMetrics().density;
                this.activityIndicator = new ProgressBar(getContext());
                this.activityIndicator.setIndeterminate(false);
                RelativeLayout.LayoutParams activityParams = new RelativeLayout.LayoutParams(-2, -2);
                this.activityIndicator.setLayoutParams(activityParams);
                this.activityIndicator.setVisibility(4);
                this.activityIndicator.setMinimumHeight((int) (((float) 20) * density));
                this.activityIndicator.setMinimumWidth((int) (((float) 20) * density));
                this.activityIndicator.setMax(100);
                this.activityIndicator.setBackgroundDrawable(this.defaultBackground);
                activityParams.topMargin = 1;
                if (ad.adType == 1) {
                    createAdWithBannerView(icon);
                    if (this.activityIndicator != null) {
                        this.activityIndicator.setId(2);
                        activityParams.addRule(11);
                    }
                } else if (ad.adType == 2) {
                    createAdWithIconView(icon, ad);
                    if (this.activityIndicator != null) {
                        this.activityIndicator.setId(3);
                        activityParams.addRule(11);
                    }
                } else {
                    Log.w(LOG_TAG, "Woooo!! unable to display ad, We got unsupported ad type.");
                    onAdViewFailure();
                    return;
                }
                if (this.activityIndicator != null) {
                    addView(this.activityIndicator);
                }
            }
            drawBackgroundView(true);
            setVisibility(super.getVisibility());
            onAdViewSuccess();
            return;
        }
        Log.w(LOG_TAG, "Ad is not loaded");
        onAdViewFailure();
    }

    private void onAdViewSuccess() {
        if (this.listener != null) {
            this.listener.adRequestCompletedSuccessfully(this);
        } else {
            Log.d(LOG_TAG, "  Unable to call mdotmListenres while success of AdView ");
        }
    }

    private void onAdViewFailure() {
        if (this.listener != null) {
            this.listener.adRequestFailed(this);
        } else {
            Log.d(LOG_TAG, " Unable to call mdotmListenres  while failure of AdView");
        }
    }

    private void createAdWithBannerView(Bitmap icon) {
        ImageView bannerView = new ImageView(getContext());
        bannerView.setImageBitmap(icon);
        this.padding = (52 - icon.getHeight()) / 2;
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(-1, -1);
        params.setMargins(this.padding, this.padding, 0, this.padding);
        bannerView.setLayoutParams(params);
        bannerView.setId(1);
        addView(bannerView);
    }

    private void createAdWithIconView(Bitmap icon, AdUnit ad) {
        ImageView iconView = new ImageView(getContext());
        iconView.setImageBitmap(icon);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(icon.getWidth(), icon.getHeight());
        params.setMargins(this.padding, this.padding, 0, this.padding);
        iconView.setLayoutParams(params);
        iconView.setId(1);
        addView(iconView);
        TextView adTextView = new TextView(getContext());
        adTextView.setText(ad.adText);
        adTextView.setTypeface(AD_FONT);
        adTextView.setTextColor(this.textColor);
        adTextView.setTextSize(13.0f);
        adTextView.setId(2);
        RelativeLayout.LayoutParams adTextViewParams = new RelativeLayout.LayoutParams(-2, -2);
        adTextViewParams.addRule(1, 1);
        adTextViewParams.setMargins(20, 4, 10, 10);
        adTextViewParams.addRule(13);
        adTextView.setLayoutParams(adTextViewParams);
        addView(adTextView);
        setGravity(15);
    }

    private AdUnit generateAdUnitFromJsonString(String jsonString) {
        JSONObject jsonObject;
        if (jsonString == null) {
            return null;
        }
        String jsonString2 = jsonString.replace("[", "").replace("]", "");
        if (jsonString2.trim().length() < 1) {
            Log.d(LOG_TAG, "Neglecting, Invalid AD response.");
            return null;
        }
        try {
            jsonObject = new JSONObject(jsonString2);
        } catch (JSONException e) {
            Log.d(LOG_TAG, "Caught JSONException while creating JSON object from string:  " + jsonString2);
            jsonObject = null;
            e.printStackTrace();
        }
        if (jsonObject == null) {
            return null;
        }
        AdUnit adUnit = new AdUnit();
        try {
            adUnit.adType = jsonObject.getInt("ad_type");
            adUnit.launchType = jsonObject.getInt("launch_type");
            adUnit.adText = jsonObject.getString("ad_text");
            adUnit.imageUrl = jsonObject.getString("img_url");
            adUnit.landingUrl = jsonObject.getString("landing_url");
            adUnit.dest_url = jsonObject.getString("dest_url");
            Log.d(LOG_TAG, "Created MdotM adUnit successfully");
        } catch (JSONException e2) {
            JSONException e3 = e2;
            adUnit = null;
            Log.d(LOG_TAG, "Caught JSONException in generateAdUnitFromJsonString()", e3);
            e3.printStackTrace();
        }
        return adUnit;
    }

    private String convertStreamToString(InputStream inputStream) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                sb.append(String.valueOf(line) + "\n");
            } catch (IOException e) {
                Log.e(LOG_TAG, "Caught IOException in convertStreamToString()", e);
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e2) {
                        Log.e(LOG_TAG, "Caught IOException while closing inputStream in convertStreamToString()", e2);
                    }
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e3) {
                        Log.e(LOG_TAG, "Caught IOException while closing reader in convertStreamToString()", e3);
                    }
                }
                return null;
            } catch (Throwable th) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e4) {
                        Log.e(LOG_TAG, "Caught IOException while closing inputStream in convertStreamToString()", e4);
                    }
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e5) {
                        Log.e(LOG_TAG, "Caught IOException while closing reader in convertStreamToString()", e5);
                    }
                }
                throw th;
            }
        }
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e6) {
                Log.e(LOG_TAG, "Caught IOException while closing inputStream in convertStreamToString()", e6);
            }
        }
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e7) {
                Log.e(LOG_TAG, "Caught IOException while closing reader in convertStreamToString()", e7);
            }
        }
        if (sb.equals(null)) {
            return new String();
        }
        return sb.toString();
    }

    private static Bitmap fetchImage(String imageURL, boolean useCaches) {
        Bitmap image = null;
        if (imageURL != null) {
            InputStream is = null;
            try {
                URLConnection conn = new URL(imageURL).openConnection();
                conn.setConnectTimeout(0);
                conn.setReadTimeout(0);
                conn.setUseCaches(useCaches);
                conn.connect();
                is = conn.getInputStream();
                image = BitmapFactory.decodeStream(is);
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                    }
                }
            } catch (Throwable th) {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e2) {
                    }
                }
                throw th;
            }
        } else {
            Log.w(LOG_TAG, "Image url is null");
        }
        return image;
    }

    public void setTextColor(int textColor2) {
        this.textColor = textColor2;
    }

    public int getTextColor() {
        return this.textColor;
    }

    public void setBackgroundColor(int backgroundColor2) {
        this.backgroundColor = backgroundColor2;
    }

    public int getBackgroundColor() {
        return this.backgroundColor;
    }

    public void setListener(MdotMActionListener listener2) {
        this.listener = listener2;
    }

    public MdotMActionListener getListener() {
        return this.listener;
    }

    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        return false;
    }
}
