package com.google.ads;

import android.net.Uri;
import android.webkit.WebView;
import com.google.ads.AdSpec;
import java.net.URLDecoder;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class AdViewCommunicator {
    private static final String AFMA_MESSAGE_HOST = "afma.google.com";
    private static final String JS_PROTOCOL_PREFIX = "javascript: ";
    public static final String JS_SEND_FUNCTION = "adsense.mobileads.afmanotify.receiveMessage";
    private static final String RESPONSE_SCHEME = "gmsg";
    private Map<String, AdResponse> mAdResponses = new HashMap();
    private GoogleAdView mView;

    public enum JsMessageAction {
        JS_OUTSIDE_CLICK_MESSAGE("click_outside_ad"),
        JS_REPORT_INSTALL_STATE("report_application_installation_state");
        
        private String mMessageString;

        private JsMessageAction(String messageString) {
            this.mMessageString = messageString;
        }

        public String getMessageString() {
            return this.mMessageString;
        }
    }

    public AdViewCommunicator(GoogleAdView view) {
        this.mView = view;
    }

    public AdResponse registerAdResponse(String command, AdResponse response) {
        return this.mAdResponses.put(command, response);
    }

    public boolean testAndForwardMessage(Uri uri) {
        if (!isMessage(uri)) {
            throw new IllegalArgumentException("Invalid syntax in forwarded message: " + uri);
        }
        AdResponse response = this.mAdResponses.get(uri.getPath());
        if (response == null) {
            return false;
        }
        Map<String, String> paramList = generateParamMap(uri);
        if (paramList == null) {
            return false;
        }
        response.run(paramList, this.mView);
        return true;
    }

    public static boolean isMessage(Uri uri) {
        if (uri == null || !uri.isHierarchical() || uri.getScheme() == null || !uri.getScheme().equals(RESPONSE_SCHEME)) {
            return false;
        }
        String host = uri.getAuthority();
        if (host == null || !host.equals(AFMA_MESSAGE_HOST)) {
            return false;
        }
        return true;
    }

    private static Map<String, String> generateParamMap(Uri uri) {
        if (uri == null) {
            return null;
        }
        HashMap<String, String> map = new HashMap<>();
        String query = uri.getEncodedQuery();
        if (query == null) {
            return map;
        }
        String[] entries = query.split("&");
        for (int i = 0; i < entries.length; i++) {
            int keyEnd = entries[i].indexOf(61);
            if (keyEnd == -1) {
                return null;
            }
            if (entries[i].indexOf(61, keyEnd + 1) != -1) {
                return null;
            }
            map.put(URLDecoder.decode(entries[i].substring(0, keyEnd)), URLDecoder.decode(entries[i].substring(keyEnd + 1)));
        }
        return Collections.unmodifiableMap(map);
    }

    public static void sendJavaScriptMessage(WebView view, JsMessageAction action, List<AdSpec.Parameter> paramList) {
        if (paramList == null || view == null) {
            throw new NullPointerException();
        }
        view.loadUrl(JS_PROTOCOL_PREFIX + ("adsense.mobileads.afmanotify.receiveMessage(\"" + action.getMessageString() + "\", " + AdUtil.generateJSONParameters(paramList) + ");"));
    }
}
