package com.google.ads;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.google.ads.AdSpec;
import java.util.LinkedList;
import java.util.List;

public class InstalledApplications {
    private static final int NOT_INSTALLED = 0;
    private final PackageManager mPackageManager;

    private enum Application {
        YOUTUBE("app_youtube", "com.google.android.youtube");
        
        private String mPackageName;
        private String mTag;

        private Application(String tag, String packageName) {
            this.mTag = tag;
            this.mPackageName = packageName;
        }

        private String getTag() {
            return this.mTag;
        }

        private int getVersionCode(PackageManager packageManager) {
            try {
                PackageInfo info = packageManager.getPackageInfo(this.mPackageName, 0);
                if (info == null) {
                    return 0;
                }
                return info.versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                return 0;
            }
        }
    }

    public InstalledApplications(PackageManager packageManager) {
        this.mPackageManager = packageManager;
    }

    public List<AdSpec.Parameter> getInstallationState() {
        LinkedList<AdSpec.Parameter> state = new LinkedList<>();
        for (Application app : Application.values()) {
            state.add(new AdSpec.Parameter(Application.access$000(app), "" + Application.access$100(app, this.mPackageManager)));
        }
        return state;
    }
}
