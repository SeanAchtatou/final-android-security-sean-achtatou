package com.google.ads;

import com.google.ads.AdViewCommunicator;
import java.util.Map;

public class InstalledAppsResponse implements AdResponse {
    private final AdViewCommunicator mCommunicator;
    private final InstalledApplications mInstalledApps;

    public InstalledAppsResponse(InstalledApplications installedApps, AdViewCommunicator communicator) {
        this.mInstalledApps = installedApps;
        this.mCommunicator = communicator;
    }

    public void run(Map<String, String> map, GoogleAdView adView) {
        AdViewCommunicator.sendJavaScriptMessage(adView.getWebView(), AdViewCommunicator.JsMessageAction.JS_REPORT_INSTALL_STATE, this.mInstalledApps.getInstallationState());
    }
}
