package com.google.ads;

import android.content.Context;
import com.google.ads.AdSpec;
import com.inmobi.androidsdk.impl.Constants;
import java.util.ArrayList;
import java.util.List;

public class DoubleClickSpec implements AdSpec {
    private String mColorBackground;
    private boolean mCustomSizeDefined = false;
    private int mHeight = 0;
    private String mKeyname;
    private SizeProfile mSizeProfile;
    private int mWidth = 0;

    public DoubleClickSpec(String keyname) {
        this.mKeyname = keyname;
    }

    public List<AdSpec.Parameter> generateParameters(Context context) {
        ArrayList<AdSpec.Parameter> params = new ArrayList<>();
        params.add(new AdSpec.Parameter("k", this.mKeyname));
        if (this.mColorBackground != null) {
            params.add(new AdSpec.Parameter("color_bg", this.mColorBackground));
        }
        if (this.mSizeProfile != null) {
            params.add(new AdSpec.Parameter("sp", this.mSizeProfile.name().toLowerCase()));
        }
        return params;
    }

    public boolean getDebugMode() {
        return false;
    }

    public int getWidth() {
        if (this.mCustomSizeDefined) {
            return this.mWidth;
        }
        if (this.mSizeProfile != null) {
            return this.mSizeProfile.getWidth();
        }
        return SizeProfile.XL.getWidth();
    }

    public int getHeight() {
        if (this.mCustomSizeDefined) {
            return this.mHeight;
        }
        if (this.mSizeProfile != null) {
            return this.mSizeProfile.getHeight();
        }
        return SizeProfile.XL.getHeight();
    }

    public String getAdUrl() {
        return AdSpec.CONTENT_AD_URL;
    }

    public String getKeyname() {
        return this.mKeyname;
    }

    public DoubleClickSpec setKeyname(String keyname) {
        this.mKeyname = keyname;
        return this;
    }

    public String getColorBackground() {
        return this.mColorBackground;
    }

    public DoubleClickSpec setColorBackground(String colorBackground) {
        this.mColorBackground = colorBackground;
        return this;
    }

    public SizeProfile getSizeProfile() {
        return this.mSizeProfile;
    }

    public DoubleClickSpec setSizeProfile(SizeProfile sizeProfile) {
        this.mSizeProfile = sizeProfile;
        return this;
    }

    public DoubleClickSpec setCustomSize(int width, int height) {
        if (width < SizeProfile.S.getWidth()) {
            throw new IllegalArgumentException("Illegal width: " + width);
        } else if (height < SizeProfile.S.getHeight()) {
            throw new IllegalArgumentException("Illegal height: " + height);
        } else {
            this.mCustomSizeDefined = true;
            this.mWidth = width;
            this.mHeight = height;
            return this;
        }
    }

    public enum SizeProfile {
        T(Constants.INMOBI_ADVIEW_WIDTH, 50),
        S(120, 20),
        M(168, 28),
        L(216, 36),
        XL(Constants.INMOBI_ADVIEW_WIDTH, 50);
        
        private int mHeight;
        private int mWidth;

        private SizeProfile(int width, int height) {
            this.mWidth = width;
            this.mHeight = height;
        }

        public int getWidth() {
            return this.mWidth;
        }

        public int getHeight() {
            return this.mHeight;
        }
    }
}
