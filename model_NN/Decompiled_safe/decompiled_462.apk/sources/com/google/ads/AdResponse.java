package com.google.ads;

import java.util.Map;

interface AdResponse {
    void run(Map<String, String> map, GoogleAdView googleAdView);
}
