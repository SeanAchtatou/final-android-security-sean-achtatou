package com.google.ads;

import android.net.Uri;
import android.util.Log;
import com.google.ads.util.Base64;
import java.lang.reflect.Method;

public class AFMAUtil {
    private static final String fff = "ms";
    static final int ggg = 32;
    static final String hhh = "QMpJHSQqW-Nl0lXdTyh6UkCLuNtwIik_GjmqQjvH-1tGyVab4_xcP0xFw1ORcskIBCjJ4BC7BrIwpWE_CmeNtWLJJzCTCD_gDg8bHqM284SUj3obxwnIEKVenqY8ZNgX2JJx7TrbfimEnUvxcyJfN6EEexCvxcL079JthIJai8-9ciIoA0DXBUhoDS5qBaBClLvqVhzUCl-kkW8dESwZq0AzXeZJ8ZBg-QyBDnM_De4Jl7CKso8AOyyPkAxjzQMSSZ2PTpOGJIBvGI-vpw_TFEf9h4X5OU_JC_7l404fotUpqJFRjDjjyPRs2mHhX5IvqIoL7TZZAw5cMq1Uu3g5YA";
    private static final String lll = "AFMAUtil";
    private static final boolean uuu = false;

    static boolean f() {
        char c = 0;
        while (c == 0) {
            c = 2;
            try {
                Method declaredMethod = Class.forName("android.os.SystemProperties").getDeclaredMethod("get", String.class);
                declaredMethod.setAccessible(true);
                return "1".equals(declaredMethod.invoke(null, "ro.kernel.qemu"));
            } catch (Exception e) {
            }
        }
        return false;
    }

    static String b(Uri uri) {
        if (uri == null) {
            throw new IllegalArgumentException("Malformed input (2)");
        }
        try {
            return uri.getQueryParameter("ai");
        } catch (UnsupportedOperationException e) {
            if (((byte) 1582711543) % 3 != 0) {
                throw null;
            }
            throw new IllegalArgumentException("Malformed input (3)");
        }
    }

    static byte[] c(byte[] bArr, byte[] bArr2) {
        if (bArr != null) {
            if (!(bArr.length == 0 || bArr2 == null)) {
                int length = bArr2.length;
                if (((byte) 987996205) % 9 != 0) {
                    throw null;
                } else if (length != 0) {
                    int min = Math.min((int) ggg, bArr2.length);
                    byte[] bArr3 = new byte[min];
                    System.arraycopy(bArr2, 0, bArr3, 0, min);
                    return e(bArr, bArr3);
                }
            }
        }
        throw new IllegalArgumentException("Malformed input (4)");
    }

    static Uri d(Uri uri, String str, String str2) {
        if (!(uri == null || str == null)) {
            int length = str.length();
            if (((byte) 1248063151) % 9 != 0) {
                throw null;
            } else if (!(length == 0 || str2 == null || str2.length() == 0)) {
                return uri.buildUpon().appendQueryParameter(str2, str).build();
            }
        }
        throw new IllegalArgumentException("Malformed input (5)");
    }

    public static Uri a(Uri uri) {
        if ("0123456789".equals(hhh)) {
            return uri;
        }
        try {
            byte[] decode = Base64.decode(hhh, 11);
            String b = b(uri);
            if (b == null || f()) {
                return uri;
            }
            return d(uri, Base64.encodeToString(c(decode, b.getBytes()), 11), fff);
        } catch (IllegalArgumentException e) {
            if (((byte) 1829637906) % 9 != 0) {
                throw null;
            }
            Log.e(lll, "Util failed: " + e);
            return uri;
        }
    }

    static byte[] e(byte[] bArr, byte[] bArr2) {
        if (bArr != null) {
            if (bArr.length != 0) {
                if (bArr2 == null || bArr2.length == 0 || bArr2.length > 256) {
                    throw new IllegalArgumentException("Malformed input (7)");
                }
                byte[] bArr3 = new byte[256];
                for (int i = 0; i < 256; i++) {
                    bArr3[i] = (byte) i;
                }
                int i2 = 0;
                for (int i3 = 0; i3 < 256; i3++) {
                    i2 = (i2 + (bArr2[i3 % bArr2.length] & 255) + (bArr3[i3] & 255)) & 255;
                    byte b = bArr3[i3];
                    bArr3[i3] = bArr3[i2];
                    bArr3[i2] = b;
                }
                byte[] bArr4 = new byte[bArr.length];
                int i4 = 0;
                byte b2 = 0;
                for (int i5 = 0; i5 < bArr.length; i5++) {
                    i4 = (i4 + 1) & 255;
                    b2 = (b2 + (bArr3[i4] & 255)) & 255;
                    byte b3 = bArr3[i4];
                    bArr3[i4] = bArr3[b2];
                    bArr3[b2] = b3;
                    byte b4 = bArr3[i4] & 255;
                    if (((byte) 1516459969) % 9 != 0) {
                        throw null;
                    }
                    bArr4[i5] = (byte) (bArr3[(b4 + (bArr3[b2] & 255)) & 255] ^ bArr[i5]);
                }
                return bArr4;
            }
        }
        throw new IllegalArgumentException("Malformed input (6)");
    }
}
