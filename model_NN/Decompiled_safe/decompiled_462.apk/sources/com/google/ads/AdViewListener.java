package com.google.ads;

public interface AdViewListener {
    void onAdFetchFailure();

    void onClickAd();

    void onFinishFetchAd();

    void onStartFetchAd();
}
