package com.google.ads;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import com.google.ads.util.Base64;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

class LocationTracker {
    static String PROTO_TEMPLATE = "desc < role: 6 producer: 24 historical_role: 1 historical_producer: 12 timestamp: %d latlng < latitude_e7: %d longitude_e7: %d> radius: %d>";
    private Context mContext;

    LocationTracker(Context context) {
        this.mContext = context;
    }

    /* access modifiers changed from: package-private */
    public String getLocationParam() {
        List<Location> lastLocations = getLastKnownLocations();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < lastLocations.size(); i++) {
            String encoded = encodeProto(protoFromLocation(lastLocations.get(i)));
            if (encoded != null) {
                if (i == 0) {
                    builder.append("e1+");
                } else {
                    builder.append("+e1+");
                }
                builder.append(encoded);
            }
        }
        return builder.toString();
    }

    private List<Location> getLastKnownLocations() {
        LocationManager locationManager = (LocationManager) this.mContext.getSystemService("location");
        List<String> providers = locationManager.getProviders(false);
        List<Location> lastLocations = new ArrayList<>(providers.size());
        for (String provider : providers) {
            Location location = locationManager.getLastKnownLocation(provider);
            if (location != null && location.hasAccuracy()) {
                lastLocations.add(location);
            }
        }
        return lastLocations;
    }

    /* access modifiers changed from: package-private */
    public String protoFromLocation(Location location) {
        return String.format(PROTO_TEMPLATE, Long.valueOf(location.getTime() * 1000), Long.valueOf((long) (location.getLatitude() * 1.0E7d)), Long.valueOf((long) (location.getLongitude() * 1.0E7d)), Long.valueOf((long) (location.getAccuracy() * 1000.0f)));
    }

    /* access modifiers changed from: package-private */
    public String encodeProto(String proto) {
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(1, new SecretKeySpec(new byte[]{10, 55, -112, -47, -6, 7, 11, 75, -7, -121, 121, 69, 80, -61, 15, 5}, "AES"));
            byte[] a = cipher.getIV();
            byte[] b = cipher.doFinal(proto.getBytes());
            byte[] c = new byte[(a.length + b.length)];
            System.arraycopy(a, 0, c, 0, a.length);
            System.arraycopy(b, 0, c, a.length, b.length);
            return Base64.encodeToString(c, 11);
        } catch (GeneralSecurityException e) {
            return null;
        }
    }
}
