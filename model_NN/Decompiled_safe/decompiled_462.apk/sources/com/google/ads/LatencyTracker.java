package com.google.ads;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;

class LatencyTracker {
    private static final String CLICK_LATENCY = "click_latency";
    private static final String CLICK_START = "click_start";
    private static final String CLICK_STRING = "click_string";
    private static final String CURRENT_CLICK_STRING = "current_click_string";
    private static final String FETCH_LATENCY = "fetch_latency";
    private static final String FETCH_START = "fetch_start";
    private static final String LATENCY_PREFERENCES = "latency_preferences";
    private Clock mClock;
    private PersistentState mPersistent;
    private TransientState mTransient;

    interface Clock {
        long elapsedRealtime();
    }

    private class TransientState {
        private static final String CLICK_START = "click_start";
        private static final String CURRENT_CLICK_STRING = "current_click_string";
        private static final String FETCH_START = "fetch_start";
        /* access modifiers changed from: private */
        public long mAdClickStartTimestamp;
        /* access modifiers changed from: private */
        public long mAdFetchStartTimestamp;
        /* access modifiers changed from: private */
        public String mCurrentClickString;

        private TransientState() {
            this.mAdFetchStartTimestamp = -1;
            this.mAdClickStartTimestamp = -1;
            this.mCurrentClickString = null;
        }

        /* access modifiers changed from: private */
        public void save(Bundle bundle) {
            bundle.putLong(FETCH_START, this.mAdFetchStartTimestamp);
            bundle.putLong(CLICK_START, this.mAdClickStartTimestamp);
            bundle.putString(CURRENT_CLICK_STRING, this.mCurrentClickString);
        }

        /* access modifiers changed from: private */
        public void restore(Bundle bundle) {
            this.mAdFetchStartTimestamp = bundle.getLong(FETCH_START);
            this.mAdClickStartTimestamp = bundle.getLong(CLICK_START);
            this.mCurrentClickString = bundle.getString(CURRENT_CLICK_STRING);
        }
    }

    private class PersistentState {
        private static final String CLICK_LATENCY = "click_latency";
        private static final String CLICK_STRING = "click_string";
        private static final String FETCH_LATENCY = "fetch_latency";
        private static final String LATENCY_PREFERENCES = "google_ads.xml";
        /* access modifiers changed from: private */
        public int mAdClickLatency;
        /* access modifiers changed from: private */
        public int mAdFetchLatency;
        /* access modifiers changed from: private */
        public String mClickString;
        private SharedPreferences mSharedPreferences;

        private PersistentState(Context context) {
            this.mAdFetchLatency = -1;
            this.mAdClickLatency = -1;
            this.mClickString = null;
            this.mSharedPreferences = context.getSharedPreferences(LATENCY_PREFERENCES, 0);
        }

        private PersistentState(SharedPreferences sharedPreferences) {
            this.mAdFetchLatency = -1;
            this.mAdClickLatency = -1;
            this.mClickString = null;
            this.mSharedPreferences = sharedPreferences;
        }

        /* access modifiers changed from: private */
        public void save() {
            if (this.mSharedPreferences != null) {
                SharedPreferences.Editor editor = this.mSharedPreferences.edit();
                editor.putInt(FETCH_LATENCY, this.mAdFetchLatency);
                editor.putInt(CLICK_LATENCY, this.mAdClickLatency);
                editor.putString(CLICK_STRING, this.mClickString);
                editor.commit();
            }
        }

        /* access modifiers changed from: private */
        public void restore() {
            if (this.mSharedPreferences != null) {
                this.mAdFetchLatency = this.mSharedPreferences.getInt(FETCH_LATENCY, -1);
                this.mAdClickLatency = this.mSharedPreferences.getInt(CLICK_LATENCY, -1);
                this.mClickString = this.mSharedPreferences.getString(CLICK_STRING, null);
            }
        }

        /* access modifiers changed from: private */
        public void clear() {
            this.mAdFetchLatency = -1;
            this.mAdClickLatency = -1;
            this.mClickString = null;
            save();
        }
    }

    public LatencyTracker(Context context) {
        this.mClock = new RealClock();
        this.mTransient = new TransientState();
        this.mPersistent = new PersistentState(context);
        this.mPersistent.restore();
    }

    LatencyTracker(Clock clock, SharedPreferences sharedPreferences) {
        this.mClock = clock;
        this.mTransient = new TransientState();
        this.mPersistent = new PersistentState(sharedPreferences);
        this.mPersistent.restore();
    }

    public void onAdFetchStart() {
        long unused = this.mTransient.mAdFetchStartTimestamp = this.mClock.elapsedRealtime();
    }

    public void onAdFetchFinished() {
        if (this.mTransient.mAdFetchStartTimestamp != -1) {
            int unused = this.mPersistent.mAdFetchLatency = (int) (this.mClock.elapsedRealtime() - this.mTransient.mAdFetchStartTimestamp);
            long unused2 = this.mTransient.mAdFetchStartTimestamp = -1;
            this.mPersistent.save();
        }
    }

    public void onAdClickStart(String clickString) {
        long unused = this.mTransient.mAdClickStartTimestamp = this.mClock.elapsedRealtime();
        String unused2 = this.mTransient.mCurrentClickString = clickString;
    }

    public void onWindowGetFocus() {
        if (this.mTransient.mAdClickStartTimestamp != -1) {
            int unused = this.mPersistent.mAdClickLatency = (int) (this.mClock.elapsedRealtime() - this.mTransient.mAdClickStartTimestamp);
            String unused2 = this.mPersistent.mClickString = this.mTransient.mCurrentClickString;
            long unused3 = this.mTransient.mAdClickStartTimestamp = -1;
            this.mPersistent.save();
        }
    }

    public boolean hasAdFetchLatency() {
        return this.mPersistent.mAdFetchLatency != -1;
    }

    public int getAdFetchLatency() {
        return this.mPersistent.mAdFetchLatency;
    }

    public boolean hasAdClickLatency() {
        return this.mPersistent.mAdClickLatency != -1;
    }

    public int getAdClickLatency() {
        return this.mPersistent.mAdClickLatency;
    }

    public boolean hasClickString() {
        return this.mPersistent.mClickString != null && this.mPersistent.mClickString.length() > 0;
    }

    public String getClickString() {
        return this.mPersistent.mClickString;
    }

    public void clear() {
        this.mPersistent.clear();
    }

    public void saveTransientState(Bundle bundle) {
        this.mTransient.save(bundle);
    }

    public void restoreTransientState(Bundle bundle) {
        this.mTransient.restore(bundle);
    }

    public String toString() {
        return "Latency[fstart=" + this.mTransient.mAdFetchStartTimestamp + ", " + "cstart=" + this.mTransient.mAdClickStartTimestamp + ", " + "ccstr=" + this.mTransient.mCurrentClickString + ", " + "flat=" + this.mPersistent.mAdFetchLatency + ", " + "clat=" + this.mPersistent.mAdClickLatency + ", " + "cstr=" + this.mPersistent.mClickString + "]";
    }

    private class RealClock implements Clock {
        private RealClock() {
        }

        public long elapsedRealtime() {
            return SystemClock.elapsedRealtime();
        }
    }
}
