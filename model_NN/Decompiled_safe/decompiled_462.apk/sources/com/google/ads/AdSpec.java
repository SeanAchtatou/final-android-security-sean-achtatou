package com.google.ads;

import android.content.Context;
import java.util.List;

public interface AdSpec {
    public static final String CONTENT_AD_URL = "http://pagead2.googlesyndication.com/pagead/afma_load_ads.js";
    public static final String SEARCH_AD_URL = "http://www.gstatic.com/mobile/ads/safma_load_ads.js";

    List<Parameter> generateParameters(Context context);

    String getAdUrl();

    boolean getDebugMode();

    int getHeight();

    int getWidth();

    public static class Parameter {
        private final String mName;
        private final String mValue;

        public Parameter(String name, String value) {
            if (name == null) {
                throw new NullPointerException("Parameter name cannot be null.");
            } else if (value == null) {
                throw new NullPointerException("Parameter value cannot be null.");
            } else {
                this.mName = name;
                this.mValue = value;
            }
        }

        public String getName() {
            return this.mName;
        }

        public String getValue() {
            return this.mValue;
        }

        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof Parameter)) {
                return false;
            }
            Parameter p = (Parameter) o;
            return this.mName.equals(p.mName) && this.mValue.equals(p.mValue);
        }

        public int hashCode() {
            return (this.mName.hashCode() * 4999) + this.mValue.hashCode();
        }

        public String toString() {
            return "Parameter(" + this.mName + "," + this.mValue + ")";
        }
    }
}
