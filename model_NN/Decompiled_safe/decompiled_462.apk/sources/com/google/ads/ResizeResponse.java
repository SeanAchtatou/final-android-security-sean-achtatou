package com.google.ads;

import com.millennialmedia.android.MMAdView;
import java.util.Map;

class ResizeResponse implements AdResponse {
    ResizeResponse() {
    }

    public void run(Map<String, String> paramMap, GoogleAdView adView) {
        int width;
        String strWidth = paramMap.get(MMAdView.KEY_WIDTH);
        String strHeight = paramMap.get(MMAdView.KEY_HEIGHT);
        String strLeft = paramMap.get("left");
        String strRight = paramMap.get("right");
        String strTop = paramMap.get("top");
        String strBottom = paramMap.get("bottom");
        if (strWidth != null) {
            try {
                width = Integer.parseInt(strWidth);
            } catch (NumberFormatException e) {
                return;
            }
        } else {
            width = adView.getAdWidth();
        }
        int height = strWidth != null ? Integer.parseInt(strHeight) : adView.getAdHeight();
        int left = strLeft != null ? Integer.parseInt(strLeft) : 0;
        int right = strRight != null ? Integer.parseInt(strRight) : 0;
        int top = strTop != null ? Integer.parseInt(strTop) : 0;
        int bottom = strBottom != null ? Integer.parseInt(strBottom) : 0;
        if (width != adView.getAdWidth() || height != adView.getAdHeight()) {
            if (adView.isExpanded()) {
                adView.closeAdImmediately();
            }
            adView.resize(width, height);
        } else if (adView.isExpanded() && left == 0 && right == 0 && top == 0 && bottom == 0) {
            adView.retractAd();
        } else if (!adView.isExpanded() && validateExpandDirection(top, bottom, left, right)) {
            adView.expandAd(top, bottom, left, right);
        }
    }

    private static boolean validateExpandDirection(int top, int bottom, int left, int right) {
        return (top > 0 || bottom > 0 || left > 0 || right > 0) && top >= 0 && bottom >= 0 && left >= 0 && right >= 0;
    }
}
