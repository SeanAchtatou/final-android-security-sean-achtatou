package com.magmamobile.mmusia.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.magmamobile.mmusia.MCommon;
import com.magmamobile.mmusia.MMUSIA;
import com.magmamobile.mmusia.parser.data.ItemAppUpdate;
import com.magmamobile.mmusia.views.ItemView;
import java.text.SimpleDateFormat;

public class UpdatesListAdapterEx extends BaseAdapter {
    private SimpleDateFormat formaterDate = new SimpleDateFormat("yyyy-MM-dd");
    private Context mContext = null;
    private ItemAppUpdate[] myDatas = null;

    public static class ViewHolder {
        ImageView img;
        LinearLayout linearItem;
        TextView txtDate;
        TextView txtDesc;
        TextView txtTitle;
    }

    public static class ViewHolderLoading {
        TextView txtTitle;
    }

    public UpdatesListAdapterEx(Context context) {
        this.mContext = context;
    }

    public void setData(ItemAppUpdate[] data) {
        this.myDatas = data;
    }

    public int getCount() {
        return this.myDatas.length;
    }

    public ItemAppUpdate getItem(int position) {
        return this.myDatas[position];
    }

    public long getItemId(int position) {
        return (long) position;
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = new ItemView(this.mContext).getRootView();
            holder = new ViewHolder();
            holder.linearItem = (LinearLayout) convertView.findViewById(MMUSIA.RES_ID_ITEM_LINEARITEM);
            holder.txtTitle = (TextView) convertView.findViewById(MMUSIA.RES_ID_ITEM_TITLE);
            holder.txtDate = (TextView) convertView.findViewById(MMUSIA.RES_ID_ITEM_DATE);
            holder.txtDesc = (TextView) convertView.findViewById(MMUSIA.RES_ID_ITEM_DESC);
            holder.img = (ImageView) convertView.findViewById(MMUSIA.RES_ID_ITEM_IMG);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.linearItem.setVisibility(0);
        ItemAppUpdate item = this.myDatas[position];
        holder.txtTitle.setText(item.Name);
        holder.txtDesc.setText(item.VersionName);
        holder.txtDate.setText(this.formaterDate.format(item.UpdateDate));
        holder.img.setImageDrawable(MCommon.getAssetDrawable((Activity) this.mContext, "mussiamarket32.png"));
        return convertView;
    }
}
