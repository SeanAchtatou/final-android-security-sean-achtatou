package com.magmamobile.mmusia.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.magmamobile.mmusia.MCommon;
import com.magmamobile.mmusia.MMUSIA;
import com.magmamobile.mmusia.parser.data.ItemNews;
import com.magmamobile.mmusia.views.ImageViewEx;
import com.magmamobile.mmusia.views.ItemView;
import java.text.SimpleDateFormat;

public class NewsListAdapterEx extends BaseAdapter {
    private SimpleDateFormat formaterDate = new SimpleDateFormat("yyyy-MM-dd");
    private Context mContext = null;
    private ItemNews[] myDatas = null;

    public static class ViewHolder {
        ImageViewEx img;
        LinearLayout linearItem;
        TextView txtDate;
        TextView txtDesc;
        TextView txtTitle;
    }

    public static class ViewHolderLoading {
        TextView txtTitle;
    }

    public NewsListAdapterEx(Context context) {
        this.mContext = context;
    }

    public void setData(ItemNews[] data) {
        this.myDatas = data;
    }

    public int getCount() {
        return this.myDatas.length;
    }

    public ItemNews getItem(int position) {
        return this.myDatas[position];
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = new ItemView(this.mContext).getRootView();
            holder = new ViewHolder();
            holder.linearItem = (LinearLayout) convertView.findViewById(MMUSIA.RES_ID_ITEM_LINEARITEM);
            holder.txtTitle = (TextView) convertView.findViewById(MMUSIA.RES_ID_ITEM_TITLE);
            holder.txtDate = (TextView) convertView.findViewById(MMUSIA.RES_ID_ITEM_DATE);
            holder.txtDesc = (TextView) convertView.findViewById(MMUSIA.RES_ID_ITEM_DESC);
            holder.img = (ImageViewEx) convertView.findViewById(MMUSIA.RES_ID_ITEM_IMG);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.linearItem.setVisibility(0);
        ItemNews item = this.myDatas[position];
        holder.txtTitle.setText(item.NewsTitle);
        holder.txtDesc.setText(item.NewsDesc);
        holder.txtDate.setText(this.formaterDate.format(item.NewsDate));
        holder.img.setImageDrawable(MCommon.getAssetDrawable((Activity) this.mContext, "mussianews32.png"));
        if (!item.imgUrl.equals("")) {
            holder.img.setRemoteURI(item.imgUrl);
            holder.img.loadImage(this.mContext);
        }
        return convertView;
    }
}
