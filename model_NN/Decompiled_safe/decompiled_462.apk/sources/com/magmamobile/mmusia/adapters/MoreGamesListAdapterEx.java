package com.magmamobile.mmusia.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.magmamobile.mmusia.MCommon;
import com.magmamobile.mmusia.MMUSIA;
import com.magmamobile.mmusia.parser.data.ItemMoreGames;
import com.magmamobile.mmusia.views.ImageViewEx;
import com.magmamobile.mmusia.views.ItemMoreGameView;

public class MoreGamesListAdapterEx extends BaseAdapter {
    private Context mContext = null;
    private ItemMoreGames[] myDatas = null;

    public static class ViewHolder {
        ImageViewEx img;
        LinearLayout linearItem;
        TextView txtFree;
        TextView txtTitle;
    }

    public static class ViewHolderLoading {
        TextView txtTitle;
    }

    public MoreGamesListAdapterEx(Context context) {
        this.mContext = context;
    }

    public void setData(ItemMoreGames[] data) {
        this.myDatas = data;
    }

    public int getCount() {
        return this.myDatas.length;
    }

    public ItemMoreGames getItem(int position) {
        return this.myDatas[position];
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = new ItemMoreGameView(this.mContext).getRootView();
            holder = new ViewHolder();
            holder.linearItem = (LinearLayout) convertView.findViewById(MMUSIA.RES_ID_ITEM_LINEARITEM);
            holder.txtTitle = (TextView) convertView.findViewById(MMUSIA.RES_ID_ITEM_TITLE);
            holder.txtFree = (TextView) convertView.findViewById(MMUSIA.RES_ID_MOREGAMES_ITEM_FREE);
            holder.img = (ImageViewEx) convertView.findViewById(MMUSIA.RES_ID_ITEM_IMG);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.linearItem.setVisibility(0);
        ItemMoreGames item = this.myDatas[position];
        holder.txtTitle.setText(item.title);
        holder.img.setImageDrawable(MCommon.getAssetDrawable((Activity) this.mContext, "mussianews32.png"));
        if (item.free == 1) {
            holder.txtFree.setText("Free");
        } else {
            holder.txtFree.setText("");
        }
        if (!item.urlImg.equals("")) {
            holder.img.setRemoteURI(item.urlImg);
            holder.img.loadImage(this.mContext);
        }
        return convertView;
    }
}
