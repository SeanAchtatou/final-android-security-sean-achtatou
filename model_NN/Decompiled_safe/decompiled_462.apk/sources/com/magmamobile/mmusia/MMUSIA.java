package com.magmamobile.mmusia;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.magmamobile.mmusia.activities.MMUSIAActivity;
import com.magmamobile.mmusia.activities.MMUSIABeforeExitActivity;
import com.magmamobile.mmusia.adapters.MoreGamesListAdapterEx;
import com.magmamobile.mmusia.data.LanguageBase;
import com.magmamobile.mmusia.parser.JSonParser;
import com.magmamobile.mmusia.parser.data.ApiBase;
import com.magmamobile.mmusia.parser.parsers.JSonNews;
import com.magmamobile.mmusia.views.MoreGamesDialogView;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

public class MMUSIA {
    public static String LANGUAGE = null;
    public static int RES_DRAWABLE_ICONAPP = 0;
    public static int RES_ID_BEFOREEXIT_BTN_CLOSE = 10014;
    public static int RES_ID_BEFOREEXIT_CHK_DONTSHOW = 10015;
    public static int RES_ID_IMG_MOREGAMES_HEAD = 10011;
    public static int RES_ID_ITEM_DATE = 10007;
    public static int RES_ID_ITEM_DESC = 10008;
    public static int RES_ID_ITEM_IMG = 10009;
    public static int RES_ID_ITEM_LINEARITEM = 10005;
    public static int RES_ID_ITEM_TITLE = 10006;
    public static int RES_ID_LISTVIEW_APPUPDATE = 10000;
    public static int RES_ID_LISTVIEW_MAINTAB = 10002;
    public static int RES_ID_LISTVIEW_MOREGAMES = 10010;
    public static int RES_ID_LISTVIEW_NEWSLIST = 10001;
    public static int RES_ID_MOREGAMES_ITEM_FREE = 10013;
    public static int RES_ID_PREF_CHECK_ENABLE = 0;
    public static int RES_ID_TAB_NEWS = 10004;
    public static int RES_ID_TAB_UPDATE = 10003;
    public static int RES_ID_TITLE_MOREGAMES_HEAD = 10012;
    public static String RefererComplement = "";
    public static boolean _notifAllowed = false;
    public static ApiBase api = null;
    public static final String apiUrl = "http://api.magmamobile.com/api/mmusia.ashx";
    public static Handler handler;
    private static NotificationManager mNotificationManager;
    public static Typeface tf = null;

    public void Init(Context context, String Language, String refComplement) {
        Init(context, Language, refComplement, true);
    }

    public void Init(final Context context, String Language, String refComplement, boolean notifAllowed) {
        _notifAllowed = notifAllowed;
        MCommon.Log_i("MMUSIA INIT");
        handler = new Handler();
        RES_DRAWABLE_ICONAPP = context.getResources().getIdentifier("icon", "drawable", context.getPackageName());
        RefererComplement = refComplement;
        if (Language.equals("fr")) {
            LanguageBase.setLanguageFR();
            LANGUAGE = "fr";
        } else {
            LanguageBase.setLanguageEN();
            LANGUAGE = "en";
        }
        mNotificationManager = (NotificationManager) context.getSystemService("notification");
        new Thread() {
            public void run() {
                boolean z;
                Context context = context;
                if (MCommon.getPrefNotifStatus(context)) {
                    z = true;
                } else {
                    z = false;
                }
                MMUSIA.getLatestNews(context, z, !MCommon.getPrefNotifStatus(context));
                MMUSIA.ping(context);
            }
        }.start();
        MCommon.launchCountIncrement(context);
    }

    public static void setTypeFace(Typeface _tf) {
        tf = _tf;
    }

    public static Typeface getTypeFace() {
        if (tf != null) {
            return tf;
        }
        return Typeface.DEFAULT;
    }

    public static void getLatestNews(final Context context, boolean notify, boolean forceUpdate) {
        try {
            api = JSonNews.loadItems(context, apiUrl, MCommon.buildUrlParam(context, 0, forceUpdate));
            if (api != null) {
                if (notify) {
                    if (api.HasNewNews == 1 && _notifAllowed) {
                        Notification notification = new Notification(context.getResources().getIdentifier("icon", "drawable", context.getPackageName()), "Magma Mobile News", System.currentTimeMillis());
                        CharSequence contentTitle = LanguageBase.NOTIF_NEWS_TITLE;
                        CharSequence contentText = LanguageBase.NOTIF_NEWS_DESC;
                        Intent notificationIntent = new Intent(context, MMUSIAActivity.class);
                        notificationIntent.putExtra("tab", "news");
                        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
                        notification.flags = 16;
                        notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
                        mNotificationManager.notify(999652, notification);
                    }
                    if (api.HasNewUpdates == 1 && _notifAllowed) {
                        Notification notification2 = new Notification(context.getResources().getIdentifier("icon", "drawable", context.getPackageName()), "Update available", System.currentTimeMillis());
                        CharSequence contentTitle2 = LanguageBase.NOTIF_UPDATE_TITLE;
                        CharSequence contentText2 = LanguageBase.NOTIF_UPDATE_DESC;
                        Intent notificationIntent2 = new Intent(context, MMUSIAActivity.class);
                        notificationIntent2.putExtra("tab", "app");
                        PendingIntent contentIntent2 = PendingIntent.getActivity(context, 0, notificationIntent2, 0);
                        notification2.flags = 16;
                        notification2.setLatestEventInfo(context, contentTitle2, contentText2, contentIntent2);
                        mNotificationManager.notify(999652, notification2);
                    }
                }
                if (api.HasNewVersionAvailable == 1) {
                    handler.post(new Runnable() {
                        public void run() {
                            MMUSIA.notifNewVersionDialogThread(context);
                        }
                    });
                } else if (_notifAllowed && MCommon.getlaunchCount(context) > 2 && api.promoId > 0 && api.promoId > MCommon.getLatestPromoIDPref(context)) {
                    handler.post(new Runnable() {
                        public void run() {
                            MMUSIA.notifPromoDialogThread(context, MMUSIA.api);
                        }
                    });
                    MCommon.setLatestPromoIDPref(context, api.promoId);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void notifNewVersionDialogThread(final Context context) {
        try {
            AlertDialog.Builder ad = new AlertDialog.Builder(context);
            ad.setCancelable(true);
            if (RES_DRAWABLE_ICONAPP > 0) {
                ad.setIcon(RES_DRAWABLE_ICONAPP);
            }
            ad.setTitle(LanguageBase.DIALOG_UPDATE_TITLE);
            ad.setMessage(LanguageBase.DIALOG_UPDATE_MESSAGE);
            ad.setPositiveButton(LanguageBase.DIALOG_UPDATE_YES, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int arg1) {
                    MMUSIA.LUpdateDialog(context, MCommon.getAppPackageName(context));
                    MCommon.openMarket(context, String.valueOf(MCommon.getAppPackageName(context)) + "&referrer=utm_source%3DMMUSIA%26utm_medium%3DMMUSIADialogUpdate%26utm_campaign%3DMMUSIADialogUpdate");
                }
            });
            ad.setNegativeButton(LanguageBase.DIALOG_UPDATE_NO, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int arg1) {
                }
            });
            ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                }
            });
            ad.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void notifPromoDialogThread(final Context context, final ApiBase api2) {
        try {
            AlertDialog.Builder ad = new AlertDialog.Builder(context);
            ad.setCancelable(true);
            if (!api2.promoIconUrl.equals("")) {
                ad.setIcon(ImageOperations(api2.promoIconUrl));
            }
            ad.setTitle(api2.promoTitle);
            ad.setMessage(api2.promoDesc);
            ad.setPositiveButton(LanguageBase.DIALOG_UPDATE_YES, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int arg1) {
                    MMUSIA.LPromoDialog(context, MCommon.getAppPackageName(context), api2.promoId);
                    MCommon.openMarketLink(context, api2.promoUrl);
                }
            });
            ad.setNegativeButton(LanguageBase.DIALOG_UPDATE_NO, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int arg1) {
                }
            });
            ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                }
            });
            ad.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0039, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003a, code lost:
        com.magmamobile.mmusia.MCommon.Log_e("DialogImage ImageOperations Malformed :: " + r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0055, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0056, code lost:
        com.magmamobile.mmusia.MCommon.Log_e("DialogImage ImageOperations IO :: " + r5.getMessage());
        com.magmamobile.mmusia.MCommon.Log_e(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0072, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0073, code lost:
        com.magmamobile.mmusia.MCommon.Log_e("DialogImage ImageOperations Exception Image :: " + r5.getMessage());
        com.magmamobile.mmusia.MCommon.Log_e(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0039 A[ExcHandler: MalformedURLException (r5v10 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055 A[ExcHandler: IOException (r5v5 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.graphics.drawable.Drawable ImageOperations(java.lang.String r8) {
        /*
            r7 = 0
            java.net.URL r5 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0039, IOException -> 0x0055, Exception -> 0x0072 }
            r5.<init>(r8)     // Catch:{ MalformedURLException -> 0x0039, IOException -> 0x0055, Exception -> 0x0072 }
            java.net.URLConnection r0 = r5.openConnection()     // Catch:{ MalformedURLException -> 0x0039, IOException -> 0x0055, Exception -> 0x0072 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0039, IOException -> 0x0055, Exception -> 0x0072 }
            r5 = 1
            java.net.HttpURLConnection.setFollowRedirects(r5)     // Catch:{ MalformedURLException -> 0x0039, IOException -> 0x0055, Exception -> 0x0072 }
            r5 = 3000(0xbb8, float:4.204E-42)
            r0.setConnectTimeout(r5)     // Catch:{ MalformedURLException -> 0x0039, IOException -> 0x0055, Exception -> 0x0072 }
            r5 = 3000(0xbb8, float:4.204E-42)
            r0.setReadTimeout(r5)     // Catch:{ MalformedURLException -> 0x0039, IOException -> 0x0055, Exception -> 0x0072 }
            r0.connect()     // Catch:{ MalformedURLException -> 0x0039, IOException -> 0x0055, Exception -> 0x0072 }
            java.io.InputStream r3 = r0.getInputStream()     // Catch:{ MalformedURLException -> 0x0039, IOException -> 0x0055, Exception -> 0x0072 }
            byte[] r4 = readBytes(r3)     // Catch:{ MalformedURLException -> 0x0039, IOException -> 0x0055, Exception -> 0x0072 }
            android.graphics.drawable.Drawable r1 = loadResizedBitmap(r4)     // Catch:{ OutOfMemoryError -> 0x0033, Exception -> 0x004f, MalformedURLException -> 0x0039, IOException -> 0x0055 }
        L_0x0029:
            if (r3 == 0) goto L_0x002e
            r3.close()     // Catch:{ MalformedURLException -> 0x0039, IOException -> 0x0055, Exception -> 0x0072 }
        L_0x002e:
            r0.disconnect()     // Catch:{ MalformedURLException -> 0x0039, IOException -> 0x0055, Exception -> 0x0072 }
            r5 = r1
        L_0x0032:
            return r5
        L_0x0033:
            r2 = move-exception
            r1 = 0
            r2.printStackTrace()     // Catch:{ MalformedURLException -> 0x0039, IOException -> 0x0055, Exception -> 0x0072 }
            goto L_0x0029
        L_0x0039:
            r5 = move-exception
            r2 = r5
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "DialogImage ImageOperations Malformed :: "
            r5.<init>(r6)
            java.lang.StringBuilder r5 = r5.append(r8)
            java.lang.String r5 = r5.toString()
            com.magmamobile.mmusia.MCommon.Log_e(r5)
            r5 = r7
            goto L_0x0032
        L_0x004f:
            r2 = move-exception
            r1 = 0
            r2.printStackTrace()     // Catch:{ MalformedURLException -> 0x0039, IOException -> 0x0055, Exception -> 0x0072 }
            goto L_0x0029
        L_0x0055:
            r5 = move-exception
            r2 = r5
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "DialogImage ImageOperations IO :: "
            r5.<init>(r6)
            java.lang.String r6 = r2.getMessage()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            com.magmamobile.mmusia.MCommon.Log_e(r5)
            com.magmamobile.mmusia.MCommon.Log_e(r8)
            r5 = r7
            goto L_0x0032
        L_0x0072:
            r5 = move-exception
            r2 = r5
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "DialogImage ImageOperations Exception Image :: "
            r5.<init>(r6)
            java.lang.String r6 = r2.getMessage()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            com.magmamobile.mmusia.MCommon.Log_e(r5)
            com.magmamobile.mmusia.MCommon.Log_e(r8)
            r5 = r7
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.magmamobile.mmusia.MMUSIA.ImageOperations(java.lang.String):android.graphics.drawable.Drawable");
    }

    public static byte[] readBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        while (true) {
            int len = inputStream.read(buffer);
            if (len == -1) {
                return byteBuffer.toByteArray();
            }
            byteBuffer.write(buffer, 0, len);
        }
    }

    public static Drawable loadResizedBitmap(byte[] mybytes) {
        Bitmap bitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(mybytes, 0, mybytes.length, options);
        if (options.outHeight > 0 && options.outWidth > 0) {
            options.inJustDecodeBounds = false;
            options.inSampleSize = 1;
            while (options.outWidth / options.inSampleSize > 64 && options.outHeight / options.inSampleSize > 64) {
                options.inSampleSize++;
            }
            bitmap = BitmapFactory.decodeByteArray(mybytes, 0, mybytes.length, options);
        }
        return new BitmapDrawable(bitmap);
    }

    public static void activateNews(Context context, boolean activation) {
        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair("di", MCommon.getDeviceID(context)));
        if (activation) {
            nvps.add(new BasicNameValuePair("a", "activate"));
        } else {
            nvps.add(new BasicNameValuePair("a", "desactivate"));
        }
        try {
            api = JSonNews.loadItems(context, apiUrl, nvps);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void LNews(Context context, int newsid) {
        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair("di", MCommon.getDeviceID(context)));
        nvps.add(new BasicNameValuePair("nid", new StringBuilder(String.valueOf(newsid)).toString()));
        nvps.add(new BasicNameValuePair("a", "click"));
        try {
            api = JSonNews.loadItems(context, apiUrl, nvps);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void LUpdate(Context context, String pName) {
        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair("di", MCommon.getDeviceID(context)));
        nvps.add(new BasicNameValuePair("pn", pName));
        nvps.add(new BasicNameValuePair("a", "click"));
        try {
            api = JSonNews.loadItems(context, apiUrl, nvps);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void LUpdateDialog(Context context, String pName) {
        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair("di", MCommon.getDeviceID(context)));
        nvps.add(new BasicNameValuePair("pn", pName));
        nvps.add(new BasicNameValuePair("a", "click"));
        nvps.add(new BasicNameValuePair("dlg", "1"));
        try {
            api = JSonNews.loadItems(context, apiUrl, nvps);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void LPromoDialog(Context context, String pName, int id) {
        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair("di", MCommon.getDeviceID(context)));
        nvps.add(new BasicNameValuePair("prid", new StringBuilder(String.valueOf(id)).toString()));
        nvps.add(new BasicNameValuePair("pn", pName));
        nvps.add(new BasicNameValuePair("a", "clickpromo"));
        nvps.add(new BasicNameValuePair("dlg", "1"));
        try {
            api = JSonNews.loadItems(context, apiUrl, nvps);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void lClickMoreApp(Context context, String pname) {
        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair("di", MCommon.getDeviceID(context)));
        nvps.add(new BasicNameValuePair("pn", MCommon.getAppPackageName(context)));
        nvps.add(new BasicNameValuePair("pn2", pname));
        nvps.add(new BasicNameValuePair("a", "moregame"));
        try {
            api = JSonNews.loadItems(context, apiUrl, nvps);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void lClickBeforeExit(Context context, String pname) {
        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair("di", MCommon.getDeviceID(context)));
        nvps.add(new BasicNameValuePair("pn", MCommon.getAppPackageName(context)));
        nvps.add(new BasicNameValuePair("pn2", pname));
        nvps.add(new BasicNameValuePair("a", "exit"));
        try {
            api = JSonNews.loadItems(context, apiUrl, nvps);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static final void launch(Activity context, int param) {
        context.startActivityForResult(new Intent(context, MMUSIAActivity.class), param);
    }

    public static final void launchBeforeExit(Activity context, int param) {
        context.startActivityForResult(new Intent(context, MMUSIABeforeExitActivity.class), param);
    }

    public static final void ping(Context context) {
        try {
            String url = "http://api.magmamobile.com/app/" + MCommon.getAppPackageName(context) + ".aspx";
            MCommon.Log_d("ping [" + url + "]: " + JSonParser.sendJSonRequest(url));
        } catch (Exception e) {
            Exception e2 = e;
            MCommon.Log_w("Ping error : " + e2.getMessage());
            e2.printStackTrace();
        }
    }

    public static void showMoreGames(Activity context) {
        MCommon.useDpi(context);
        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        AlertDialog ad = adb.create();
        View v = new MoreGamesDialogView(context);
        v.setBackgroundColor(-1);
        v.setDrawingCacheBackgroundColor(-1);
        ListView mList = (ListView) v.findViewById(RES_ID_LISTVIEW_MOREGAMES);
        MoreGamesListAdapterEx arrAgg = new MoreGamesListAdapterEx(context);
        if (api != null) {
            arrAgg.setData(api.moregames);
            MCommon.Log_e("MMUSIA MORE GAMES LIST : " + api.moregames.length);
        } else {
            MCommon.Log_e("MMUSIA EMPTY MORE GAMES LIST");
        }
        try {
            mList.setAdapter((ListAdapter) arrAgg);
        } catch (IllegalStateException ie) {
            ie.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mList.setBackgroundColor(16777215);
        mList.setCacheColorHint(-1);
        ad.getWindow().setFlags(1024, 1024);
        ad.setView(v);
        ad.setIcon(context.getResources().getIdentifier("icon", "drawable", context.getPackageName()));
        ad.setCancelable(true);
        ad.setTitle(LanguageBase.DIALOG_MOREGAMES_TITLE);
        adb.setPositiveButton(LanguageBase.DIALOG_SETTINGS_CLOSE, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
            }
        });
        ad.show();
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
            }
        });
    }
}
