package com.magmamobile.mmusia.parser;

import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtils {
    private static final boolean verboseLog = false;

    public static String getJSString(JSONObject jo, String key) throws JSONException {
        return getJSString(jo, key, "");
    }

    public static String getJSString(JSONObject jo, String key, String defaultValue) throws JSONException {
        if (jo.has(key)) {
            return jo.getString(key);
        }
        return defaultValue;
    }

    public static int getJSInt(JSONObject jo, String key) throws JSONException {
        return getJSInt(jo, key, 0);
    }

    public static int getJSInt(JSONObject jo, String key, int defaultValue) throws JSONException {
        if (jo.has(key)) {
            return jo.getInt(key);
        }
        return defaultValue;
    }

    public static long getJSLong(JSONObject jo, String key) throws JSONException {
        return getJSLong(jo, key, 0);
    }

    public static long getJSLong(JSONObject jo, String key, long defaultValue) throws JSONException {
        if (jo.has(key)) {
            return jo.getLong(key);
        }
        return defaultValue;
    }

    public static double getJSDouble(JSONObject jo, String key) throws JSONException {
        return getJSDouble(jo, key, 0.0d);
    }

    public static double getJSDouble(JSONObject jo, String key, double defaultValue) throws JSONException {
        if (jo.has(key)) {
            return jo.getDouble(key);
        }
        return defaultValue;
    }

    public static Date getJSDate(JSONObject jo, String key) throws JSONException {
        return getJSDate(jo, key, new Date());
    }

    public static Date getJSDate(JSONObject jo, String key, Date defaultValue) throws JSONException {
        if (jo.has(key)) {
            return new Date(Date.parse(jo.getString(key)));
        }
        return defaultValue;
    }
}
