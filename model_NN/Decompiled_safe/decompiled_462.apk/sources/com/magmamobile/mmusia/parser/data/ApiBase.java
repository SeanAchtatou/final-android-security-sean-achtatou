package com.magmamobile.mmusia.parser.data;

public class ApiBase {
    public int HasNewNews;
    public int HasNewUpdates;
    public int HasNewVersionAvailable;
    public ItemMoreGames[] moregames;
    public ItemNews[] news;
    public String promoDesc;
    public String promoIconUrl;
    public int promoId;
    public String promoTitle;
    public String promoUrl;
    public ItemAppUpdate[] updates;
}
