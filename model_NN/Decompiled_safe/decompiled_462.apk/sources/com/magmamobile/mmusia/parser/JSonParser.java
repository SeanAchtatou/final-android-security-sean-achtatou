package com.magmamobile.mmusia.parser;

import android.os.SystemClock;
import com.magmamobile.mmusia.MCommon;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.json.JSONTokener;

public class JSonParser {
    protected static SimpleDateFormat formaterDate = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    public static DefaultHttpClient http;
    private static ArrayList<String> tmpApiLst = new ArrayList<>();
    private static boolean verbose = false;

    public static String sendJSonRequestPost(String uri, List<NameValuePair> nvps) throws Exception {
        return sendJSonRequestPost(uri, nvps, true);
    }

    public static String sendJSonRequest(String uri) throws Exception {
        return sendJSonRequest(uri, true);
    }

    public static String sendJSonRequest(String uri, boolean firstTry) throws Exception {
        float tm = (float) SystemClock.currentThreadTimeMillis();
        if (http == null) {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 20000);
            HttpConnectionParams.setSoTimeout(httpParams, 20000);
            http = new DefaultHttpClient(httpParams);
        }
        HttpGet request = new HttpGet(uri);
        request.addHeader("Accept-Encoding", "gzip, deflate");
        HttpResponse response = http.execute(request);
        InputStream is = response.getEntity().getContent();
        MCommon.Log_i("sendJSonRequest :: " + uri);
        if (response.getHeaders("Content-Encoding").length > 0 && response.getHeaders("Content-Encoding")[0].getValue().toLowerCase().equals("gzip")) {
            is = new GZIPInputStream(is);
        }
        MCommon.Log_w("JSON Feed load time : " + ((((float) SystemClock.currentThreadTimeMillis()) - tm) / 1000.0f) + " sec");
        return MCommon.generateString(is);
    }

    public static String sendJSonRequestPost(String uri, List<NameValuePair> nvps, boolean firstTry) throws Exception {
        float tm = (float) SystemClock.currentThreadTimeMillis();
        if (http == null) {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 20000);
            HttpConnectionParams.setSoTimeout(httpParams, 20000);
            http = new DefaultHttpClient(httpParams);
        }
        HttpPost methodpost = new HttpPost(uri);
        methodpost.addHeader("Accept-Charset", "UTF-8");
        methodpost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
        methodpost.addHeader("Accept-Encoding", "gzip, deflate");
        HttpResponse response = http.execute(methodpost);
        InputStream is = response.getEntity().getContent();
        MCommon.Log_i("sendJSonRequest :: " + uri);
        if (response.getHeaders("Content-Encoding").length > 0 && response.getHeaders("Content-Encoding")[0].getValue().toLowerCase().equals("gzip")) {
            is = new GZIPInputStream(is);
        }
        MCommon.Log_w("JSON Feed load time : " + ((((float) SystemClock.currentThreadTimeMillis()) - tm) / 1000.0f) + " sec");
        return MCommon.generateString(is);
    }

    public static void extractJsonNames(JSONObject ob, String root, boolean recursif) {
        if (verbose) {
            for (int i = 0; i < ob.names().length(); i++) {
                String newRoot = "";
                try {
                    newRoot = String.valueOf(root) + "/" + ob.names().getString(i);
                } catch (JSONException e) {
                }
                try {
                    Object o = ob.get(ob.names().getString(i));
                    if (o instanceof JSONObject) {
                        if (!tmpApiLst.contains(newRoot)) {
                            tmpApiLst.add(newRoot);
                            MCommon.Log_d(newRoot);
                        }
                        extractJsonNames((JSONObject) o, newRoot, true);
                    } else if (o instanceof JSONStringer) {
                        MCommon.Log_d(String.valueOf(newRoot) + "/[StringER]");
                    } else if (o instanceof JSONTokener) {
                        MCommon.Log_d(String.valueOf(newRoot) + "/[Token]");
                    } else if (o instanceof JSONArray) {
                        if (!tmpApiLst.contains(String.valueOf(newRoot) + "/[Array]")) {
                            tmpApiLst.add(String.valueOf(newRoot) + "/[Array]");
                            MCommon.Log_d(String.valueOf(newRoot) + "/[Array]");
                        }
                        extractJsonArrayNames((JSONArray) o, newRoot);
                    } else if (o instanceof String) {
                        if (!tmpApiLst.contains(String.valueOf(newRoot) + "/[String]")) {
                            tmpApiLst.add(String.valueOf(newRoot) + "/[String]");
                            MCommon.Log_d(String.valueOf(newRoot) + "/[String]");
                        }
                    } else if (o instanceof Integer) {
                        if (!tmpApiLst.contains(String.valueOf(newRoot) + "/[int]")) {
                            tmpApiLst.add(String.valueOf(newRoot) + "/[int]");
                            MCommon.Log_d(String.valueOf(newRoot) + "/[int]");
                        }
                    } else if (o instanceof Boolean) {
                        if (!tmpApiLst.contains(String.valueOf(newRoot) + "/[bool]")) {
                            tmpApiLst.add(String.valueOf(newRoot) + "/[bool]");
                            MCommon.Log_d(String.valueOf(newRoot) + "/[bool]");
                        }
                    } else if (o instanceof Long) {
                        if (!tmpApiLst.contains(String.valueOf(newRoot) + "/[long]")) {
                            tmpApiLst.add(String.valueOf(newRoot) + "/[long]");
                            MCommon.Log_d(String.valueOf(newRoot) + "/[long]");
                        }
                    } else if (o instanceof Double) {
                        if (!tmpApiLst.contains(String.valueOf(newRoot) + "/[double]")) {
                            tmpApiLst.add(String.valueOf(newRoot) + "/[double]");
                            MCommon.Log_d(String.valueOf(newRoot) + "/[double]");
                        }
                    } else if (o.toString() != "null") {
                        MCommon.Log_d("OULALA !!! C'est quoi ce truc ? : " + newRoot + " / " + o.toString());
                    }
                } catch (JSONException e2) {
                    MCommon.Log_e(String.valueOf(newRoot) + " ERROR");
                    e2.printStackTrace();
                }
            }
        }
    }

    public static void extractJsonArrayNames(JSONArray ob, String root) {
        for (int i = 0; i < ob.length(); i++) {
            try {
                extractJsonNames(ob.getJSONObject(i), root, true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
