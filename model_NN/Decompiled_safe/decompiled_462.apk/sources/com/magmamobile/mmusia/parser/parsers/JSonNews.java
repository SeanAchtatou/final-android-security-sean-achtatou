package com.magmamobile.mmusia.parser.parsers;

import android.content.Context;
import android.os.SystemClock;
import com.magmamobile.mmusia.MCommon;
import com.magmamobile.mmusia.MMUSIA;
import com.magmamobile.mmusia.parser.JSonParser;
import com.magmamobile.mmusia.parser.JsonUtils;
import com.magmamobile.mmusia.parser.data.ApiBase;
import com.magmamobile.mmusia.parser.data.ItemAppUpdate;
import com.magmamobile.mmusia.parser.data.ItemMoreGames;
import com.magmamobile.mmusia.parser.data.ItemNews;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSonNews extends JSonParser {
    /* JADX INFO: Multiple debug info for r3v8 org.json.JSONArray: [D('newsItems' org.json.JSONArray), D('updatesItems' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r3v13 org.json.JSONArray: [D('moreGamesItems' org.json.JSONArray), D('updatesItems' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r1v10 java.lang.String: [D('rawJSON' java.lang.String), D('fromCache' boolean)] */
    public static ApiBase loadItems(Context context, String url, List<NameValuePair> nvps) throws JSONException {
        String url2;
        boolean goodJson;
        String rawJSON;
        boolean fromCache;
        ItemNews[] newsArr;
        ItemAppUpdate[] updatesArr;
        ItemMoreGames[] moregamesArr;
        boolean goodJson2;
        boolean goodJson3;
        JSONObject ob;
        String rawJSON2;
        ItemMoreGames[] moregamesArr2;
        float tm = (float) SystemClock.currentThreadTimeMillis();
        ApiBase api = new ApiBase();
        ItemNews[] newsArr2 = new ItemNews[0];
        ItemAppUpdate[] updatesArr2 = new ItemAppUpdate[0];
        ItemMoreGames[] moregamesArr3 = new ItemMoreGames[0];
        try {
            url2 = sendJSonRequestPost(url, nvps);
        } catch (Exception e) {
            e.printStackTrace();
            url2 = "";
        }
        MCommon.Log_d("rawJSON from api : " + url2);
        if (url2.equals("")) {
            String rawJSON3 = readRawJson(context, "mmusia.json");
            MCommon.Log_d("rawJSON from cache : " + rawJSON3);
            goodJson = false;
            fromCache = true;
            rawJSON = rawJSON3;
        } else {
            goodJson = true;
            rawJSON = url2;
            fromCache = false;
        }
        if (!rawJSON.equals("")) {
            MCommon.Log_d(rawJSON);
            try {
                ob = new JSONObject(rawJSON);
                rawJSON2 = rawJSON;
                goodJson2 = goodJson;
                goodJson3 = fromCache;
            } catch (JSONException e1) {
                goodJson2 = false;
                goodJson3 = true;
                MCommon.Log_d("try to load from cache : " + rawJSON);
                String rawJSON4 = readRawJson(context, "mmusia.json");
                JSONObject ob2 = new JSONObject(rawJSON4);
                e1.printStackTrace();
                ob = ob2;
                rawJSON2 = rawJSON4;
            }
            if (!goodJson3) {
                api.HasNewNews = ob.getInt("HasNewNews");
                api.HasNewUpdates = ob.getInt("HasNewUpdates");
                api.HasNewVersionAvailable = ob.getInt("newVersionAvailable");
                api.promoId = ob.getInt("promoId");
                api.promoTitle = ob.getString("promoTitle");
                api.promoDesc = ob.getString("promoDesc");
                api.promoUrl = String.valueOf(ob.getString("promoUrl")) + "-" + MMUSIA.RefererComplement;
                api.promoIconUrl = ob.getString("promoIconUrl");
            } else {
                api.HasNewNews = 0;
                api.HasNewUpdates = 0;
                api.HasNewVersionAvailable = 0;
                api.promoId = 0;
                api.promoTitle = "";
                api.promoDesc = "";
                api.promoUrl = "";
                api.promoIconUrl = "";
            }
            JSONArray newsItems = ob.getJSONArray("news");
            ItemNews[] newsArr3 = new ItemNews[newsItems.length()];
            for (int l = 0; l < newsItems.length(); l++) {
                JSONObject lo = newsItems.getJSONObject(l);
                newsArr3[l] = new ItemNews();
                newsArr3[l].NewsID = lo.getInt("NewsID");
                newsArr3[l].NewsTitle = lo.getString("NewsTitle");
                newsArr3[l].NewsLanguage = lo.getString("NewsLanguage");
                newsArr3[l].NewsDesc = lo.getString("NewsDesc");
                newsArr3[l].NewsDate = JsonUtils.getJSDate(lo, "NewsDate");
                newsArr3[l].NewsUrlLink = lo.getString("NewsUrlLink");
                newsArr3[l].NewsUrlMarket = String.valueOf(lo.getString("NewsUrlMarket")) + "-" + MMUSIA.RefererComplement;
                newsArr3[l].imgUrl = lo.getString("NewsImgUrl");
                MCommon.Log_d(newsArr3[l].NewsUrlMarket);
            }
            JSONArray updatesItems = ob.getJSONArray("updates");
            ItemAppUpdate[] updatesArr3 = new ItemAppUpdate[updatesItems.length()];
            for (int l2 = 0; l2 < updatesItems.length(); l2++) {
                JSONObject lo2 = updatesItems.getJSONObject(l2);
                updatesArr3[l2] = new ItemAppUpdate();
                updatesArr3[l2].Name = lo2.getString("Name");
                updatesArr3[l2].ChangeLog = lo2.getString("ChangeLog");
                updatesArr3[l2].MarketLink = lo2.getString("MarketLink");
                updatesArr3[l2].PackageName = lo2.getString("PackageName");
                updatesArr3[l2].UpdateDate = JsonUtils.getJSDate(lo2, "UpdateDate");
                updatesArr3[l2].Version = lo2.getString("Version");
                updatesArr3[l2].VersionName = lo2.getString("VersionName");
            }
            try {
                JSONArray updatesItems2 = ob.getJSONArray("moregames");
                moregamesArr3 = new ItemMoreGames[updatesItems2.length()];
                for (int l3 = 0; l3 < updatesItems2.length(); l3++) {
                    JSONObject lo3 = updatesItems2.getJSONObject(l3);
                    moregamesArr3[l3] = new ItemMoreGames();
                    moregamesArr3[l3].title = lo3.getString("title");
                    moregamesArr3[l3].pname = lo3.getString("pname");
                    moregamesArr3[l3].urlImg = lo3.getString("urlImg");
                    moregamesArr3[l3].urlMarket = String.valueOf(lo3.getString("urlMarket")) + "-" + MMUSIA.RefererComplement;
                    moregamesArr3[l3].free = lo3.getInt("free");
                }
                moregamesArr2 = moregamesArr3;
            } catch (Exception e2) {
                MCommon.Log_e("MMUSIA MORE GAMES LIST Error : " + e2.getMessage());
                e2.printStackTrace();
                moregamesArr2 = moregamesArr3;
            }
            if (goodJson2) {
                saveRawJSon(context, "mmusia.json", rawJSON2);
            }
            updatesArr = updatesArr3;
            moregamesArr = moregamesArr2;
            newsArr = newsArr3;
        } else {
            newsArr = newsArr2;
            updatesArr = updatesArr2;
            moregamesArr = moregamesArr3;
        }
        MCommon.Log_w("JSON Parse time : " + ((((float) SystemClock.currentThreadTimeMillis()) - tm) / 1000.0f) + " sec");
        api.news = newsArr;
        api.updates = updatesArr;
        api.moregames = moregamesArr;
        return api;
    }

    private static void saveRawJSon(Context context, String filename, String data) {
        if (data != null && !data.equals("")) {
            synchronized (data) {
                File file = new File(String.valueOf(context.getFilesDir().getAbsolutePath()) + "/" + filename);
                try {
                    FileOutputStream fos = new FileOutputStream(file);
                    OutputStreamWriter oos = new OutputStreamWriter(fos);
                    oos.write(data);
                    oos.close();
                    fos.close();
                    MCommon.Log_d("Saved " + file.getAbsolutePath());
                } catch (Exception e) {
                    Exception e2 = e;
                    MCommon.Log_e("Cache not Saved !!  " + e2.getMessage());
                    e2.printStackTrace();
                }
            }
            return;
        }
        return;
    }

    public static String readRawJson(Context context, String filename) {
        File file = new File(String.valueOf(context.getFilesDir().getAbsolutePath()) + "/" + filename);
        try {
            if (!file.exists()) {
                MCommon.Log_d("file not exist in cache");
                return "";
            }
            StringBuilder text = new StringBuilder();
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                while (true) {
                    String line = br.readLine();
                    if (line == null) {
                        return text.toString();
                    }
                    text.append(line);
                    text.append(10);
                }
            } catch (IOException e) {
                return "";
            }
        } catch (Exception e2) {
            return "";
        }
    }
}
