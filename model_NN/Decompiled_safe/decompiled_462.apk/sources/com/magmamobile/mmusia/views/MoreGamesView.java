package com.magmamobile.mmusia.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.magmamobile.mmusia.MCommon;
import com.magmamobile.mmusia.MMUSIA;

public class MoreGamesView extends LinearLayout {
    private Context mContext;

    public MoreGamesView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        setBackgroundColor(-16777216);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        setOrientation(1);
        buildView(context);
    }

    public MoreGamesView(Context context) {
        super(context);
        setBackgroundColor(-16777216);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        setOrientation(1);
        buildView(context);
    }

    public void buildView(Context context) {
        LinearLayout linearMain = new LinearLayout(context);
        linearMain.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearMain.setOrientation(1);
        LinearLayout linearHead = new LinearLayout(context);
        linearHead.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        linearHead.setOrientation(0);
        linearHead.setGravity(16);
        linearHead.setMinimumHeight(MCommon.dpi(48));
        linearHead.setId(MMUSIA.RES_ID_ITEM_LINEARITEM);
        linearHead.setBackgroundColor(-16777216);
        ImageViewEx img = new ImageViewEx(context);
        img.setLayoutParams(new ViewGroup.LayoutParams(MCommon.dpi(64), MCommon.dpi(64)));
        img.setId(MMUSIA.RES_ID_IMG_MOREGAMES_HEAD);
        img.setPadding(MCommon.dpi(5), MCommon.dpi(5), MCommon.dpi(10), MCommon.dpi(5));
        LinearLayout linear = new LinearLayout(context);
        linear.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        linear.setOrientation(1);
        TextView textTitle = new TextView(context);
        textTitle.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        textTitle.setId(MMUSIA.RES_ID_TITLE_MOREGAMES_HEAD);
        textTitle.setTextColor(-1);
        textTitle.setTextSize((float) MCommon.dpi(14));
        textTitle.setTypeface(MMUSIA.getTypeFace(), 1);
        textTitle.setMaxLines(2);
        linearHead.addView(img);
        linearHead.addView(textTitle);
        addView(linearHead);
        ListView lst = new ListView(context);
        lst.setId(MMUSIA.RES_ID_LISTVIEW_MOREGAMES);
        lst.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        lst.setBackgroundColor(-1);
        lst.setCacheColorHint(-1);
        lst.setDividerHeight(0);
        lst.setClickable(true);
        linearMain.addView(lst);
        addView(linearMain);
    }
}
