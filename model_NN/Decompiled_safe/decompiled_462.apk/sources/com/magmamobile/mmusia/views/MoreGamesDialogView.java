package com.magmamobile.mmusia.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.magmamobile.mmusia.MMUSIA;

public class MoreGamesDialogView extends LinearLayout {
    private Context mContext;

    public MoreGamesDialogView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        setBackgroundColor(-16777216);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        setOrientation(1);
        buildView(context);
    }

    public MoreGamesDialogView(Context context) {
        super(context);
        setBackgroundColor(-16777216);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        setOrientation(1);
        buildView(context);
    }

    public void buildView(Context context) {
        LinearLayout linearMain = new LinearLayout(context);
        linearMain.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearMain.setOrientation(1);
        ListView lst = new ListView(context);
        lst.setId(MMUSIA.RES_ID_LISTVIEW_MOREGAMES);
        lst.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        lst.setBackgroundColor(-1);
        lst.setCacheColorHint(-1);
        lst.setDividerHeight(0);
        lst.setClickable(true);
        linearMain.addView(lst);
        addView(linearMain);
    }
}
