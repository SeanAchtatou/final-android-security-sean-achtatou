package com.magmamobile.mmusia.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.magmamobile.mmusia.MCommon;
import com.magmamobile.mmusia.image.ImageSetterSDK3;
import com.magmamobile.mmusia.image.ImageSetterSDK4;
import com.magmamobile.mmusia.image.cache.Images;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public final class ImageViewEx extends ImageView {
    private static final String TAG = "MMUSIA";
    final Handler handler;
    /* access modifiers changed from: private */
    public String mRemote;
    private boolean save2Disk;
    private boolean verboseLog;

    public ImageViewEx(Context context) {
        this(context, null, 0);
    }

    public ImageViewEx(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ImageViewEx(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.verboseLog = false;
        this.save2Disk = true;
        this.handler = new Handler() {
            public void handleMessage(Message message) {
                ImageViewEx.this.setFromLocal();
            }
        };
    }

    public void setRemoteURI(String uri) {
        if (uri != null && uri.startsWith("http")) {
            this.mRemote = uri;
        }
    }

    public void loadImage(Context context, boolean saveInCacheDisk) {
        this.save2Disk = saveInCacheDisk;
        loadImage(context);
    }

    public void loadImage(final Context context) {
        if (this.mRemote == null || this.mRemote.equals("")) {
            return;
        }
        if (!MCommon.drawableMap.containsKey(this.mRemote) && Images.isExist(context, MCommon.alphaNum(this.mRemote, "")) && this.save2Disk) {
            MCommon.drawableMap.put(this.mRemote, Images.loadImage(context, MCommon.alphaNum(this.mRemote, "")));
            if (this.verboseLog) {
                MCommon.Log_i("MMUSIA", "LoadImage : Exists in Cache, Added in memory cache");
            }
            new Thread() {
                public void run() {
                    ImageViewEx.this.handler.sendMessage(ImageViewEx.this.handler.obtainMessage(1));
                }
            }.start();
        } else if (MCommon.drawableMap.containsKey(this.mRemote)) {
            if (this.verboseLog) {
                MCommon.Log_i("MMUSIA", "LoadImage : Exists");
            }
            new Thread() {
                public void run() {
                    ImageViewEx.this.handler.sendMessage(ImageViewEx.this.handler.obtainMessage(1));
                }
            }.start();
        } else {
            if (this.verboseLog) {
                MCommon.Log_i("MMUSIA", "LoadImage : NOT Exists");
            }
            new Thread() {
                public void run() {
                    ImageViewEx.this.fetchDrawable(context, ImageViewEx.this.mRemote);
                    ImageViewEx.this.handler.sendMessage(ImageViewEx.this.handler.obtainMessage(1));
                }
            }.start();
        }
    }

    /* access modifiers changed from: private */
    public final void fetchDrawable(Context context, String urlString) {
        fetchDrawable(context, urlString, true);
    }

    private final void fetchDrawable(Context context, String urlString, boolean firstry) {
        Drawable drawable = ImageOperations(urlString);
        if (drawable != null) {
            if (this.save2Disk) {
                Images.saveImage(context, drawable, MCommon.alphaNum(urlString, ""));
            }
            MCommon.drawableMap.put(urlString, drawable);
        } else if (firstry) {
            MCommon.Log_e("MMUSIA", "ImageViewEx :: drawable == null !!!! Second Try ! :: " + urlString);
            fetchDrawable(context, urlString, false);
        } else {
            MCommon.Log_e("MMUSIA", "ImageViewEx :: drawable == null !!!! Last Try :( " + urlString);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002f, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0030, code lost:
        com.magmamobile.mmusia.MCommon.Log_e("MMUSIA", "ImageViewEx ImageOperations Malformed :: " + r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004d, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004e, code lost:
        com.magmamobile.mmusia.MCommon.Log_e("MMUSIA", "ImageViewEx ImageOperations IO :: " + r5.getMessage());
        com.magmamobile.mmusia.MCommon.Log_e("MMUSIA", r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006e, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x006f, code lost:
        com.magmamobile.mmusia.MCommon.Log_e("MMUSIA", "ImageViewEx ImageOperations Exception Image :: " + r5.getMessage());
        com.magmamobile.mmusia.MCommon.Log_e("MMUSIA", r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f A[ExcHandler: MalformedURLException (r5v8 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004d A[ExcHandler: IOException (r5v4 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.drawable.Drawable ImageOperations(java.lang.String r10) {
        /*
            r9 = this;
            r8 = 0
            java.net.URL r5 = new java.net.URL     // Catch:{ MalformedURLException -> 0x002f, IOException -> 0x004d, Exception -> 0x006e }
            r5.<init>(r10)     // Catch:{ MalformedURLException -> 0x002f, IOException -> 0x004d, Exception -> 0x006e }
            java.net.URLConnection r0 = r5.openConnection()     // Catch:{ MalformedURLException -> 0x002f, IOException -> 0x004d, Exception -> 0x006e }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x002f, IOException -> 0x004d, Exception -> 0x006e }
            r5 = 1
            java.net.HttpURLConnection.setFollowRedirects(r5)     // Catch:{ MalformedURLException -> 0x002f, IOException -> 0x004d, Exception -> 0x006e }
            r0.connect()     // Catch:{ MalformedURLException -> 0x002f, IOException -> 0x004d, Exception -> 0x006e }
            java.io.InputStream r3 = r0.getInputStream()     // Catch:{ MalformedURLException -> 0x002f, IOException -> 0x004d, Exception -> 0x006e }
            byte[] r4 = readBytes(r3)     // Catch:{ MalformedURLException -> 0x002f, IOException -> 0x004d, Exception -> 0x006e }
            android.graphics.drawable.Drawable r1 = loadResizedBitmap(r4)     // Catch:{ OutOfMemoryError -> 0x0029, Exception -> 0x0047, MalformedURLException -> 0x002f, IOException -> 0x004d }
        L_0x001f:
            if (r3 == 0) goto L_0x0024
            r3.close()     // Catch:{ MalformedURLException -> 0x002f, IOException -> 0x004d, Exception -> 0x006e }
        L_0x0024:
            r0.disconnect()     // Catch:{ MalformedURLException -> 0x002f, IOException -> 0x004d, Exception -> 0x006e }
            r5 = r1
        L_0x0028:
            return r5
        L_0x0029:
            r2 = move-exception
            r1 = 0
            r2.printStackTrace()     // Catch:{ MalformedURLException -> 0x002f, IOException -> 0x004d, Exception -> 0x006e }
            goto L_0x001f
        L_0x002f:
            r5 = move-exception
            r2 = r5
            java.lang.String r5 = "MMUSIA"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "ImageViewEx ImageOperations Malformed :: "
            r6.<init>(r7)
            java.lang.StringBuilder r6 = r6.append(r10)
            java.lang.String r6 = r6.toString()
            com.magmamobile.mmusia.MCommon.Log_e(r5, r6)
            r5 = r8
            goto L_0x0028
        L_0x0047:
            r2 = move-exception
            r1 = 0
            r2.printStackTrace()     // Catch:{ MalformedURLException -> 0x002f, IOException -> 0x004d, Exception -> 0x006e }
            goto L_0x001f
        L_0x004d:
            r5 = move-exception
            r2 = r5
            java.lang.String r5 = "MMUSIA"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "ImageViewEx ImageOperations IO :: "
            r6.<init>(r7)
            java.lang.String r7 = r2.getMessage()
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            com.magmamobile.mmusia.MCommon.Log_e(r5, r6)
            java.lang.String r5 = "MMUSIA"
            com.magmamobile.mmusia.MCommon.Log_e(r5, r10)
            r5 = r8
            goto L_0x0028
        L_0x006e:
            r5 = move-exception
            r2 = r5
            java.lang.String r5 = "MMUSIA"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "ImageViewEx ImageOperations Exception Image :: "
            r6.<init>(r7)
            java.lang.String r7 = r2.getMessage()
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            com.magmamobile.mmusia.MCommon.Log_e(r5, r6)
            java.lang.String r5 = "MMUSIA"
            com.magmamobile.mmusia.MCommon.Log_e(r5, r10)
            r5 = r8
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.magmamobile.mmusia.views.ImageViewEx.ImageOperations(java.lang.String):android.graphics.drawable.Drawable");
    }

    public static Drawable loadResizedBitmap(byte[] mybytes) {
        Bitmap bitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(mybytes, 0, mybytes.length, options);
        if (options.outHeight > 0 && options.outWidth > 0) {
            options.inJustDecodeBounds = false;
            options.inSampleSize = 1;
            while (options.outWidth / options.inSampleSize > 320 && options.outHeight / options.inSampleSize > 240) {
                options.inSampleSize++;
            }
            bitmap = BitmapFactory.decodeByteArray(mybytes, 0, mybytes.length, options);
        }
        return new BitmapDrawable(bitmap);
    }

    public static Drawable resizeDrawable(Drawable d, int newWidth, int newHeight) {
        if (d == null) {
            return null;
        }
        return new BitmapDrawable(Bitmap.createScaledBitmap(((BitmapDrawable) d).getBitmap(), newWidth, newHeight, false));
    }

    public static byte[] readBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        while (true) {
            int len = inputStream.read(buffer);
            if (len == -1) {
                return byteBuffer.toByteArray();
            }
            byteBuffer.write(buffer, 0, len);
        }
    }

    /* access modifiers changed from: private */
    public void setFromLocal() {
        Drawable d;
        if (MCommon.drawableMap.containsKey(this.mRemote) && (d = MCommon.drawableMap.get(this.mRemote)) != null) {
            if (MCommon.isSDKAPI4Mini()) {
                ImageSetterSDK4.setImage(d, this);
            } else {
                ImageSetterSDK3.setImage(d, this);
            }
        }
    }
}
