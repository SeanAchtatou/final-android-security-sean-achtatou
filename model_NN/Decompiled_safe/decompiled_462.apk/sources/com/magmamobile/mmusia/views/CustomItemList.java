package com.magmamobile.mmusia.views;

import android.graphics.Bitmap;

public class CustomItemList {
    public Bitmap bm = null;
    public String tag = "";
    public String text = "";

    public CustomItemList(String mText, Bitmap mBm, String mTag) {
        this.text = mText;
        this.bm = mBm;
        this.tag = mTag;
    }
}
