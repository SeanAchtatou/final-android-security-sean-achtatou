package com.magmamobile.mmusia.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.magmamobile.mmusia.MCommon;
import com.magmamobile.mmusia.MMUSIA;

public class ItemMoreGameView extends LinearLayout {
    private Context mContext;

    public ItemMoreGameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        buildView(context);
    }

    public ItemMoreGameView(Context context) {
        super(context);
        setOrientation(1);
        buildView(context);
    }

    public void buildView(Context context) {
        LinearLayout linearMain = new LinearLayout(context);
        linearMain.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        linearMain.setOrientation(0);
        linearMain.setGravity(16);
        linearMain.setMinimumHeight(MCommon.dpi(48));
        linearMain.setId(MMUSIA.RES_ID_ITEM_LINEARITEM);
        ImageViewEx img = new ImageViewEx(context);
        img.setLayoutParams(new ViewGroup.LayoutParams(MCommon.dpi(64), MCommon.dpi(64)));
        img.setId(MMUSIA.RES_ID_ITEM_IMG);
        img.setPadding(MCommon.dpi(5), MCommon.dpi(5), MCommon.dpi(10), MCommon.dpi(5));
        LinearLayout linear = new LinearLayout(context);
        linear.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        linear.setOrientation(1);
        TextView textTitle = new TextView(context);
        textTitle.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        textTitle.setId(MMUSIA.RES_ID_ITEM_TITLE);
        textTitle.setTextColor(-16777216);
        textTitle.setTypeface(MMUSIA.getTypeFace(), 1);
        textTitle.setMaxLines(2);
        TextView textFree = new TextView(context);
        textFree.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        textFree.setId(MMUSIA.RES_ID_MOREGAMES_ITEM_FREE);
        textFree.setTextColor(-7864320);
        textFree.setTextSize((float) MCommon.dpi(10));
        textFree.setTypeface(MMUSIA.getTypeFace(), 1);
        textFree.setMaxLines(2);
        textFree.setGravity(5);
        textFree.setPadding(0, 0, MCommon.dpi(10), 0);
        View divider = new View(context);
        divider.setBackgroundColor(-4144960);
        divider.setLayoutParams(new ViewGroup.LayoutParams(-1, 1));
        linear.addView(textTitle);
        linear.addView(textFree);
        linearMain.addView(img);
        linearMain.addView(linear);
        addView(linearMain);
        addView(divider);
    }
}
