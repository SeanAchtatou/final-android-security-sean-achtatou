package com.magmamobile.mmusia.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabWidget;
import com.magmamobile.mmusia.MMUSIA;

public class MainView extends LinearLayout {
    private Context mContext;

    public MainView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        setBackgroundColor(-16777216);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        setOrientation(1);
        buildView(context);
    }

    public MainView(Context context) {
        super(context);
        setBackgroundColor(-16777216);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        setOrientation(1);
        buildView(context);
    }

    public void buildView(Context context) {
        TabHost tabs = new TabHost(context, null);
        tabs.setId(MMUSIA.RES_ID_LISTVIEW_MAINTAB);
        tabs.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        LinearLayout linearMain = new LinearLayout(context);
        linearMain.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearMain.setOrientation(1);
        FrameLayout frame = new FrameLayout(context);
        frame.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        frame.setBackgroundColor(16777215);
        frame.setId(16908305);
        LinearLayout linearTabUpdate = new LinearLayout(context);
        linearTabUpdate.setId(MMUSIA.RES_ID_TAB_UPDATE);
        linearTabUpdate.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearTabUpdate.setOrientation(1);
        ListView listUpdate = new ListView(context);
        listUpdate.setId(MMUSIA.RES_ID_LISTVIEW_APPUPDATE);
        listUpdate.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        listUpdate.setBackgroundColor(-1);
        listUpdate.setCacheColorHint(-1);
        listUpdate.setDividerHeight(0);
        listUpdate.setClickable(true);
        LinearLayout linearTabNews = new LinearLayout(context);
        linearTabNews.setId(MMUSIA.RES_ID_TAB_NEWS);
        linearTabNews.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearTabNews.setOrientation(1);
        ListView listNews = new ListView(context);
        listNews.setId(MMUSIA.RES_ID_LISTVIEW_NEWSLIST);
        listNews.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        listNews.setBackgroundColor(-1);
        listNews.setCacheColorHint(-1);
        listNews.setDividerHeight(0);
        listNews.setClickable(true);
        TabWidget tabwid = new TabWidget(context);
        tabwid.setId(16908307);
        tabwid.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        tabwid.setBackgroundColor(0);
        tabwid.setBaselineAligned(false);
        tabwid.setClipToPadding(true);
        tabwid.setDrawingCacheQuality(1048576);
        linearTabUpdate.addView(listUpdate);
        linearTabNews.addView(listNews);
        frame.addView(linearTabUpdate);
        frame.addView(linearTabNews);
        linearMain.addView(tabwid);
        linearMain.addView(frame);
        tabs.addView(linearMain);
        addView(tabs);
    }
}
