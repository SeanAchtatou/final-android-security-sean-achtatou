package com.magmamobile.mmusia.views;

import android.content.Context;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import com.magmamobile.mmusia.MMUSIA;
import com.magmamobile.mmusia.data.LanguageBase;

public class PrefView extends LinearLayout {
    public PrefView(Context context) {
        super(context);
        setOrientation(1);
        buildView(context);
    }

    public void buildView(Context context) {
        CheckBox check = new CheckBox(context);
        check.setText(LanguageBase.DIALOG_SETTINGS_CHKTEXT);
        check.setId(MMUSIA.RES_ID_PREF_CHECK_ENABLE);
        addView(check);
    }
}
