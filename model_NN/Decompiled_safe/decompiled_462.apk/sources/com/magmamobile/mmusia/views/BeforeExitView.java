package com.magmamobile.mmusia.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.magmamobile.mmusia.MCommon;
import com.magmamobile.mmusia.MMUSIA;

public class BeforeExitView extends LinearLayout {
    private Context mContext;

    public BeforeExitView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        setBackgroundColor(-16777216);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        setOrientation(1);
        buildView(context);
    }

    public BeforeExitView(Context context) {
        super(context);
        setBackgroundColor(-16777216);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        setOrientation(1);
        buildView(context);
    }

    public void buildView(Context context) {
        LinearLayout linearMain = new LinearLayout(context);
        LinearLayout.LayoutParams lpLinearMain = new LinearLayout.LayoutParams(-1, -1);
        lpLinearMain.setMargins(0, 0, 0, MCommon.dpi(50));
        linearMain.setLayoutParams(lpLinearMain);
        linearMain.setOrientation(1);
        LinearLayout linearHead = new LinearLayout(context);
        linearHead.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        linearHead.setOrientation(0);
        linearHead.setGravity(16);
        linearHead.setMinimumHeight(MCommon.dpi(48));
        linearHead.setId(MMUSIA.RES_ID_ITEM_LINEARITEM);
        linearHead.setBackgroundColor(-16777216);
        ImageViewEx img = new ImageViewEx(context);
        img.setLayoutParams(new ViewGroup.LayoutParams(MCommon.dpi(64), MCommon.dpi(64)));
        img.setId(MMUSIA.RES_ID_IMG_MOREGAMES_HEAD);
        img.setPadding(MCommon.dpi(5), MCommon.dpi(5), MCommon.dpi(10), MCommon.dpi(5));
        TextView textTitle = new TextView(context);
        textTitle.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        textTitle.setId(MMUSIA.RES_ID_TITLE_MOREGAMES_HEAD);
        textTitle.setTextColor(-1);
        textTitle.setTextSize((float) MCommon.dpi(14));
        textTitle.setTypeface(MMUSIA.getTypeFace(), 1);
        textTitle.setMaxLines(2);
        linearHead.addView(img);
        linearHead.addView(textTitle);
        addView(linearHead);
        GridView lst = new GridView(context);
        lst.setId(MMUSIA.RES_ID_LISTVIEW_MOREGAMES);
        lst.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        lst.setBackgroundColor(-1);
        lst.setCacheColorHint(-1);
        lst.setClickable(true);
        lst.setNumColumns(3);
        LinearLayout linearBottom = new LinearLayout(context);
        LinearLayout.LayoutParams lpLinearBottom = new LinearLayout.LayoutParams(-1, -1);
        lpLinearBottom.setMargins(0, MCommon.dpi(-50), 0, 0);
        linearBottom.setLayoutParams(lpLinearBottom);
        linearBottom.setOrientation(0);
        linearBottom.setBackgroundColor(-3487030);
        Button btnClose = new Button(context);
        LinearLayout.LayoutParams lpbtnClose = new LinearLayout.LayoutParams(-1, -1);
        lpbtnClose.gravity = 5;
        btnClose.setLayoutParams(lpbtnClose);
        btnClose.setId(MMUSIA.RES_ID_BEFOREEXIT_BTN_CLOSE);
        btnClose.setText("Close");
        linearMain.addView(lst);
        linearBottom.addView(btnClose);
        addView(linearMain);
        addView(linearBottom);
    }
}
