package com.magmamobile.mmusia.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.magmamobile.mmusia.MCommon;
import com.magmamobile.mmusia.MMUSIA;

public class ItemView extends LinearLayout {
    private Context mContext;

    public ItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        buildView(context);
    }

    public ItemView(Context context) {
        super(context);
        setOrientation(1);
        buildView(context);
    }

    public void buildView(Context context) {
        LinearLayout linearMain = new LinearLayout(context);
        linearMain.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        linearMain.setOrientation(0);
        linearMain.setGravity(16);
        linearMain.setMinimumHeight(MCommon.dpi(48));
        linearMain.setId(MMUSIA.RES_ID_ITEM_LINEARITEM);
        ImageViewEx img = new ImageViewEx(context);
        img.setLayoutParams(new ViewGroup.LayoutParams(MCommon.dpi(48), MCommon.dpi(48)));
        img.setId(MMUSIA.RES_ID_ITEM_IMG);
        img.setPadding(MCommon.dpi(5), MCommon.dpi(5), MCommon.dpi(10), MCommon.dpi(5));
        LinearLayout linear = new LinearLayout(context);
        linear.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        linear.setOrientation(1);
        TextView textTitle = new TextView(context);
        textTitle.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        textTitle.setId(MMUSIA.RES_ID_ITEM_TITLE);
        textTitle.setTextColor(-16777088);
        textTitle.setTypeface(Typeface.DEFAULT, 1);
        textTitle.setMaxLines(2);
        TextView textDate = new TextView(context);
        textDate.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        textDate.setId(MMUSIA.RES_ID_ITEM_DATE);
        textDate.setTextColor(-6250336);
        textDate.setTypeface(Typeface.DEFAULT, 0);
        textDate.setMaxLines(1);
        TextView textDesc = new TextView(context);
        textDesc.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        textDesc.setId(MMUSIA.RES_ID_ITEM_DESC);
        textDesc.setTextColor(-4144960);
        textDesc.setTypeface(Typeface.DEFAULT, 0);
        textDesc.setMaxLines(3);
        View divider = new View(context);
        divider.setBackgroundColor(-4144960);
        divider.setLayoutParams(new ViewGroup.LayoutParams(-1, 1));
        linear.addView(textTitle);
        linear.addView(textDate);
        linear.addView(textDesc);
        linearMain.addView(img);
        linearMain.addView(linear);
        addView(linearMain);
        addView(divider);
    }
}
