package com.magmamobile.mmusia.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;
import com.magmamobile.mmusia.MCommon;
import com.magmamobile.mmusia.MMUSIA;
import com.magmamobile.mmusia.adapters.NewsListAdapterEx;
import com.magmamobile.mmusia.adapters.UpdatesListAdapterEx;
import com.magmamobile.mmusia.data.LanguageBase;
import com.magmamobile.mmusia.parser.data.ItemAppUpdate;
import com.magmamobile.mmusia.parser.data.ItemNews;
import com.magmamobile.mmusia.views.MainView;

public class MMUSIAActivity extends Activity {
    private final int MENU_MMUSIA_QUIT = 3;
    private final int MENU_MMUSIA_REFRESH = 2;
    private final int MENU_MMUSIA_SETTINGS = 1;
    private final int MSG_CHANGE_MESSAGE = 2;
    private final int MSG_CLOSE = 1;
    private final int MSG_LOADJSONFINISH = 5;
    private final int MSG_OPEN = 0;
    private final int MSG_TOAST = 4;
    /* access modifiers changed from: private */
    public ProgressDialog mDialog;
    /* access modifiers changed from: private */
    public ListView mListAppUpdate;
    /* access modifiers changed from: private */
    public ListView mListNewsList;
    private TabHost mTabs;
    Handler messageHandler = new Handler() {
        /* Debug info: failed to restart local var, previous not found, register: 6 */
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    MMUSIAActivity.this.mDialog = ProgressDialog.show(MMUSIAActivity.this, LanguageBase.DIALOG_LOADING, LanguageBase.DIALOG_PLEASEWAIT, true, true);
                    return;
                case 1:
                    try {
                        MMUSIAActivity.this.mDialog.dismiss();
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case 2:
                    MMUSIAActivity.this.mDialog.setMessage((String) msg.obj);
                    return;
                case 4:
                    Toast.makeText(MMUSIAActivity.this, (String) msg.obj, 1).show();
                    return;
                case 5:
                    MMUSIAActivity.this.displayNews();
                    return;
                case 999999:
                    MMUSIAActivity.this.finish();
                    return;
                default:
                    return;
            }
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguageBase.reloadIfNeeded();
        try {
            if (MMUSIA.RES_DRAWABLE_ICONAPP == 0) {
                MMUSIA.RES_DRAWABLE_ICONAPP = getResources().getIdentifier("icon", "drawable", getPackageName());
            }
        } catch (Exception e) {
            MCommon.Log_e("TRIED TO RELOAD ICON APP ID WITHOUT SUCCESS !!!");
            e.printStackTrace();
        }
        Bundle extras = getIntent().getExtras();
        MCommon.useDpi(this);
        MCommon.Log_d(new StringBuilder(String.valueOf(MCommon.getDensity(this))).toString());
        setContentView(new MainView(this));
        this.mListAppUpdate = (ListView) findViewById(MMUSIA.RES_ID_LISTVIEW_APPUPDATE);
        this.mListNewsList = (ListView) findViewById(MMUSIA.RES_ID_LISTVIEW_NEWSLIST);
        this.mTabs = (TabHost) findViewById(MMUSIA.RES_ID_LISTVIEW_MAINTAB);
        this.mTabs.setVisibility(0);
        this.mTabs.setup();
        try {
            MCommon.Log_e("MMUSIA.RES_DRAWABLE_ICONAPP : " + MMUSIA.RES_DRAWABLE_ICONAPP);
            if (MMUSIA.RES_DRAWABLE_ICONAPP == 0) {
                this.mTabs.addTab(this.mTabs.newTabSpec("tab_main_channel").setIndicator(LanguageBase.TAB_UPDATES).setContent(MMUSIA.RES_ID_TAB_UPDATE));
            } else {
                this.mTabs.addTab(this.mTabs.newTabSpec("tab_main_channel").setIndicator(LanguageBase.TAB_UPDATES, MCommon.getAssetDrawableResize(getResources().getDrawable(MMUSIA.RES_DRAWABLE_ICONAPP), MCommon.dpiImage(24), MCommon.dpiImage(24))).setContent(MMUSIA.RES_ID_TAB_UPDATE));
            }
        } catch (Resources.NotFoundException e2) {
            MCommon.Log_e("OULALA ICON INTROUVABLE !!!");
            MCommon.Log_e("MMUSIA.RES_DRAWABLE_ICONAPP : " + MMUSIA.RES_DRAWABLE_ICONAPP);
            MCommon.Log_e("LanguageBase.TAB_UPDATES : " + LanguageBase.TAB_UPDATES);
            MCommon.Log_e("MMUSIA.RES_ID_TAB_UPDATE : " + MMUSIA.RES_ID_TAB_UPDATE);
            this.mTabs.addTab(this.mTabs.newTabSpec("tab_main_channel").setIndicator(LanguageBase.TAB_UPDATES).setContent(MMUSIA.RES_ID_TAB_UPDATE));
            e2.printStackTrace();
        }
        this.mTabs.addTab(this.mTabs.newTabSpec("tab_main_last").setIndicator(LanguageBase.TAB_NEWS, MCommon.getAssetDrawableResize(MCommon.getAssetDrawable(this, "mmusiaicon.png"), MCommon.dpiImage(75), MCommon.dpiImage(24))).setContent(MMUSIA.RES_ID_TAB_NEWS));
        if (extras != null) {
            if (extras.getString("tab").equals("news")) {
                this.mTabs.setCurrentTab(1);
            } else {
                this.mTabs.setCurrentTab(0);
            }
        }
        if (MMUSIA.api == null) {
            loadNews(this);
        } else if (MMUSIA.api.news.length == 0) {
            loadNews(this);
        } else {
            displayNews();
        }
        this.mListNewsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                ItemNews item = ((NewsListAdapterEx) MMUSIAActivity.this.mListNewsList.getAdapter()).getItem(arg2);
                if (!item.NewsUrlMarket.equals("")) {
                    MCommon.openMarketLink(MMUSIAActivity.this, item.NewsUrlMarket);
                } else {
                    MCommon.openUrlPage(MMUSIAActivity.this, item.NewsUrlLink);
                }
                MMUSIAActivity.this.lNews(MMUSIAActivity.this, item.NewsID);
            }
        });
        this.mListAppUpdate.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                ItemAppUpdate item = ((UpdatesListAdapterEx) MMUSIAActivity.this.mListAppUpdate.getAdapter()).getItem(arg2);
                MCommon.openMarketLink(MMUSIAActivity.this, item.MarketLink);
                MMUSIAActivity.this.lUpdate(MMUSIAActivity.this, item.PackageName);
            }
        });
    }

    public void lNews(final Context context, final int nid) {
        new Thread() {
            public void run() {
                MMUSIA.LNews(context, nid);
            }
        }.start();
    }

    public void lUpdate(final Context context, final String pname) {
        new Thread() {
            public void run() {
                MMUSIA.LUpdate(context, pname);
            }
        }.start();
    }

    public void lUpdateDialog(final Context context, final String pname) {
        new Thread() {
            public void run() {
                MMUSIA.LUpdateDialog(context, pname);
            }
        }.start();
    }

    public void loadNews(final Context context) {
        new Thread() {
            public void run() {
                MMUSIAActivity.this.loadNewsThread(context);
            }
        }.start();
    }

    public void loadNewsThread(Context context) {
        this.messageHandler.sendMessage(Message.obtain(this.messageHandler, 0));
        MMUSIA.getLatestNews(context, false, true);
        this.messageHandler.sendMessage(Message.obtain(this.messageHandler, 1));
        this.messageHandler.sendMessage(Message.obtain(this.messageHandler, 5));
    }

    public void displayNews() {
        try {
            if (MMUSIA.api.news == null) {
                MMUSIA.api.news = new ItemNews[0];
            }
            NewsListAdapterEx newsAdp = new NewsListAdapterEx(this);
            newsAdp.setData(MMUSIA.api.news);
            this.mListNewsList.setAdapter((ListAdapter) newsAdp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (MMUSIA.api.updates == null) {
                MMUSIA.api.updates = new ItemAppUpdate[0];
            }
            UpdatesListAdapterEx updAdp = new UpdatesListAdapterEx(this);
            updAdp.setData(MMUSIA.api.updates);
            this.mListAppUpdate.setAdapter((ListAdapter) updAdp);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        try {
            if (MMUSIA.api.updates.length == 0) {
                this.mTabs.setCurrentTab(1);
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, LanguageBase.MENU_SETTINGS).setIcon(MCommon.getAssetDrawable(this, "mmusiasettings.png"));
        menu.add(0, 2, 0, LanguageBase.MENU_REFRESH).setIcon(MCommon.getAssetDrawable(this, "mmusiarefresh.png"));
        menu.add(0, 3, 0, LanguageBase.MENU_QUIT).setIcon(MCommon.getAssetDrawable(this, "mmusiaexit.png"));
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                MCommon.showPrefs(this);
                return true;
            case 2:
                loadNews(this);
                return true;
            case 3:
                finish();
                return true;
            default:
                return true;
        }
    }
}
