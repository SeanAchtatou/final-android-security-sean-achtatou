package com.magmamobile.mmusia.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.magmamobile.mmusia.MCommon;
import com.magmamobile.mmusia.MMUSIA;
import com.magmamobile.mmusia.adapters.MoreGamesListAdapterEx;
import com.magmamobile.mmusia.data.LanguageBase;
import com.magmamobile.mmusia.parser.data.ItemMoreGames;
import com.magmamobile.mmusia.views.MoreGamesView;

public class MMUSIAMoreGamesActivity extends Activity {
    private final int MENU_MMUSIA_QUIT = 3;
    private final int MENU_MMUSIA_REFRESH = 2;
    private final int MSG_CHANGE_MESSAGE = 2;
    private final int MSG_CLOSE = 1;
    private final int MSG_LOADJSONFINISH = 5;
    private final int MSG_OPEN = 0;
    private final int MSG_TOAST = 4;
    /* access modifiers changed from: private */
    public ProgressDialog mDialog;
    /* access modifiers changed from: private */
    public ListView mListMoreGamesList;
    Handler messageHandler = new Handler() {
        /* Debug info: failed to restart local var, previous not found, register: 5 */
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    MMUSIAMoreGamesActivity.this.mDialog = ProgressDialog.show(MMUSIAMoreGamesActivity.this, LanguageBase.DIALOG_LOADING, LanguageBase.DIALOG_PLEASEWAIT, true, true);
                    return;
                case 1:
                    MMUSIAMoreGamesActivity.this.mDialog.dismiss();
                    return;
                case 2:
                    MMUSIAMoreGamesActivity.this.mDialog.setMessage((String) msg.obj);
                    return;
                case 4:
                    Toast.makeText(MMUSIAMoreGamesActivity.this, (String) msg.obj, 1).show();
                    return;
                case 5:
                    MMUSIAMoreGamesActivity.this.displayMoreGames();
                    return;
                case 999999:
                    MMUSIAMoreGamesActivity.this.finish();
                    return;
                default:
                    return;
            }
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguageBase.reloadIfNeeded();
        try {
            if (MMUSIA.RES_DRAWABLE_ICONAPP == 0) {
                MMUSIA.RES_DRAWABLE_ICONAPP = getResources().getIdentifier("icon", "drawable", getPackageName());
            }
        } catch (Exception e) {
            MCommon.Log_e("TRIED TO RELOAD ICON APP ID WITHOUT SUCCESS !!!");
            e.printStackTrace();
        }
        MCommon.useDpi(this);
        MCommon.Log_d(new StringBuilder(String.valueOf(MCommon.getDensity(this))).toString());
        setContentView(new MoreGamesView(this));
        this.mListMoreGamesList = (ListView) findViewById(MMUSIA.RES_ID_LISTVIEW_MOREGAMES);
        try {
            MCommon.Log_e("MMUSIA.RES_DRAWABLE_ICONAPP : " + MMUSIA.RES_DRAWABLE_ICONAPP);
            if (MMUSIA.RES_DRAWABLE_ICONAPP != 0) {
                ((ImageView) findViewById(MMUSIA.RES_ID_IMG_MOREGAMES_HEAD)).setImageDrawable(MCommon.getAssetDrawableResize(getResources().getDrawable(MMUSIA.RES_DRAWABLE_ICONAPP), MCommon.dpiImage(48), MCommon.dpiImage(48)));
            }
        } catch (Resources.NotFoundException e2) {
            MCommon.Log_e("OULALA ICON INTROUVABLE !!!");
            MCommon.Log_e("MMUSIA.RES_DRAWABLE_ICONAPP : " + MMUSIA.RES_DRAWABLE_ICONAPP);
            MCommon.Log_e("LanguageBase.TAB_UPDATES : " + LanguageBase.TAB_UPDATES);
            MCommon.Log_e("MMUSIA.RES_ID_TAB_UPDATE : " + MMUSIA.RES_ID_TAB_UPDATE);
            e2.printStackTrace();
        }
        ((TextView) findViewById(MMUSIA.RES_ID_TITLE_MOREGAMES_HEAD)).setText(LanguageBase.DIALOG_MOREGAMES_TITLE);
        if (MMUSIA.api == null) {
            loadNews(this);
        } else if (MMUSIA.api.moregames.length == 0) {
            loadNews(this);
        } else {
            displayMoreGames();
        }
        this.mListMoreGamesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                ItemMoreGames item = ((MoreGamesListAdapterEx) MMUSIAMoreGamesActivity.this.mListMoreGamesList.getAdapter()).getItem(arg2);
                if (!item.urlMarket.equals("")) {
                    if (item.urlMarket.startsWith("http://")) {
                        MCommon.openUrlPage(MMUSIAMoreGamesActivity.this, item.urlMarket);
                    } else {
                        MCommon.openMarketLink(MMUSIAMoreGamesActivity.this, item.urlMarket);
                    }
                }
                MMUSIAMoreGamesActivity.this.lClickMoreApp(MMUSIAMoreGamesActivity.this, item.pname);
            }
        });
    }

    public void lClickMoreApp(final Context context, final String pname) {
        new Thread() {
            public void run() {
                MMUSIA.lClickMoreApp(context, pname);
            }
        }.start();
    }

    public void loadNews(final Context context) {
        new Thread() {
            public void run() {
                MMUSIAMoreGamesActivity.this.loadNewsThread(context);
            }
        }.start();
    }

    public void loadNewsThread(Context context) {
        this.messageHandler.sendMessage(Message.obtain(this.messageHandler, 0));
        MMUSIA.getLatestNews(context, false, true);
        this.messageHandler.sendMessage(Message.obtain(this.messageHandler, 1));
        this.messageHandler.sendMessage(Message.obtain(this.messageHandler, 5));
    }

    public void displayMoreGames() {
        try {
            if (MMUSIA.api.moregames == null) {
                MMUSIA.api.moregames = new ItemMoreGames[0];
            }
            MoreGamesListAdapterEx moregamesAdp = new MoreGamesListAdapterEx(this);
            moregamesAdp.setData(MMUSIA.api.moregames);
            this.mListMoreGamesList.setAdapter((ListAdapter) moregamesAdp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 2, 0, LanguageBase.MENU_REFRESH).setIcon(MCommon.getAssetDrawable(this, "mmusiarefresh.png"));
        menu.add(0, 3, 0, LanguageBase.MENU_QUIT).setIcon(MCommon.getAssetDrawable(this, "mmusiaexit.png"));
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2:
                loadNews(this);
                return true;
            case 3:
                finish();
                return true;
            default:
                return true;
        }
    }
}
