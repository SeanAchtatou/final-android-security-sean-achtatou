package com.magmamobile.mmusia.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import com.magmamobile.mmusia.parser.data.ItemMoreGames;
import java.util.ArrayList;
import java.util.List;

public class MMUtils {
    private static List<ResolveInfo> list;

    public static boolean isPackageNameInDevice(Context context, String pname) {
        if (list == null) {
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            intent.addCategory("android.intent.category.LAUNCHER");
            list = context.getPackageManager().queryIntentActivities(intent, 0);
        }
        for (int i = 0; i < list.size(); i++) {
            if (isSystemPackage(list.get(i))) {
                list.remove(i);
            } else if (list.get(i).activityInfo.packageName.equals(pname)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isSystemPackage(ResolveInfo ri) {
        return (ri.activityInfo.applicationInfo.flags & 1) != 0;
    }

    public static ItemMoreGames[] getMoreGamesForExit(Context context, ItemMoreGames[] lst) {
        ArrayList<ItemMoreGames> more = new ArrayList<>();
        for (int i = 0; i < lst.length; i++) {
            if (isPackageNameInDevice(context, lst[i].pname)) {
                lst[i].isLocal = true;
                lst[i].cname = getCname(context, lst[i].pname);
                more.add(lst[i]);
            } else {
                lst[i].isLocal = false;
                more.add(0, lst[i]);
            }
        }
        ItemMoreGames[] moreArr = new ItemMoreGames[more.size()];
        for (int i2 = 0; i2 < more.size(); i2++) {
            moreArr[i2] = (ItemMoreGames) more.get(i2);
        }
        return moreArr;
    }

    private static String getCname(Context context, String pname) {
        if (list == null) {
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            intent.addCategory("android.intent.category.LAUNCHER");
            list = context.getPackageManager().queryIntentActivities(intent, 0);
        }
        for (int i = 0; i < list.size(); i++) {
            if (isSystemPackage(list.get(i))) {
                list.remove(i);
            } else if (list.get(i).activityInfo.packageName.equals(pname)) {
                return list.get(i).activityInfo.name;
            }
        }
        return "";
    }
}
