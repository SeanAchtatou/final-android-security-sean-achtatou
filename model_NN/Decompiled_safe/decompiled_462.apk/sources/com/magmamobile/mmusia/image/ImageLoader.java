package com.magmamobile.mmusia.image;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.widget.ImageView;
import com.magmamobile.mmusia.MCommon;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class ImageLoader {
    private static ImageLoader _instance;
    private boolean _busy = false;
    private Bitmap _missing;
    private Queue<Group> _queue = new LinkedList();
    private DownloadThread _thread;
    private HashMap<String, Bitmap> _urlToBitmap = new HashMap<>();

    public static ImageLoader getInstance() {
        if (_instance == null) {
            _instance = new ImageLoader();
        }
        return _instance;
    }

    private ImageLoader() {
    }

    public Bitmap get(String url) {
        return this._urlToBitmap.get(url);
    }

    public void load(ImageView image, String url) {
        load(image, url, false);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public void load(ImageView image, String url, boolean cache) {
        if (this._urlToBitmap.get(url) == null) {
            queue(image, url, cache);
        } else if (image == null) {
        } else {
            if (MCommon.isSDKAPI4Mini()) {
                ImageSetterSDK4.setImage(new BitmapDrawable(this._urlToBitmap.get(url)), image);
            } else {
                ImageSetterSDK3.setImage(new BitmapDrawable(this._urlToBitmap.get(url)), image);
            }
        }
    }

    public void queue(ImageView image, String url, boolean cache) {
        Iterator<Group> it = this._queue.iterator();
        if (image != null) {
            while (true) {
                if (it.hasNext()) {
                    if (it.next().image.equals(image)) {
                        it.remove();
                        break;
                    }
                } else {
                    break;
                }
            }
        } else if (url != null) {
            while (true) {
                if (it.hasNext()) {
                    if (it.next().url.equals(url)) {
                        it.remove();
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        this._queue.add(new Group(image, url, null, cache));
        loadNext();
    }

    public void clearQueue() {
        this._queue = new LinkedList();
    }

    public void clearCache() {
        this._urlToBitmap = new HashMap<>();
    }

    public void cancel() {
        clearQueue();
        if (this._thread != null) {
            this._thread.disconnect();
            this._thread = null;
        }
    }

    public void setMissingBitmap(Bitmap bitmap) {
        this._missing = bitmap;
    }

    private void loadNext() {
        Iterator<Group> it = this._queue.iterator();
        if (!this._busy && it.hasNext()) {
            this._busy = true;
            Group group = it.next();
            it.remove();
            if (this._urlToBitmap.get(group.url) != null) {
                if (group.image != null) {
                    if (MCommon.isSDKAPI4Mini()) {
                        ImageSetterSDK4.setImage(new BitmapDrawable(this._urlToBitmap.get(group.url)), group.image);
                    } else {
                        ImageSetterSDK3.setImage(new BitmapDrawable(this._urlToBitmap.get(group.url)), group.image);
                    }
                }
                this._busy = false;
                loadNext();
                return;
            }
            this._thread = new DownloadThread(group);
            this._thread.start();
        }
    }

    /* access modifiers changed from: private */
    public void onLoad() {
        if (this._thread != null) {
            Group group = this._thread.group;
            if (group.bitmap != null) {
                if (group.cache) {
                    this._urlToBitmap.put(group.url, group.bitmap);
                }
                if (group.image != null) {
                    if (MCommon.isSDKAPI4Mini()) {
                        ImageSetterSDK4.setImage(new BitmapDrawable(group.bitmap), group.image);
                    } else {
                        ImageSetterSDK3.setImage(new BitmapDrawable(group.bitmap), group.image);
                    }
                }
            } else if (!(this._missing == null || group.image == null)) {
                if (MCommon.isSDKAPI4Mini()) {
                    ImageSetterSDK4.setImage(new BitmapDrawable(this._missing), group.image);
                } else {
                    ImageSetterSDK3.setImage(new BitmapDrawable(this._missing), group.image);
                }
            }
        }
        this._thread = null;
        this._busy = false;
        loadNext();
    }

    private class Group {
        public Bitmap bitmap;
        public boolean cache;
        public ImageView image;
        public String url;

        public Group(ImageView image2, String url2, Bitmap bitmap2, boolean cache2) {
            this.image = image2;
            this.url = url2;
            this.bitmap = bitmap2;
            this.cache = cache2;
        }
    }

    private class DownloadThread extends Thread {
        private HttpURLConnection _conn;
        public Group group;
        final Runnable threadCallback = new Runnable() {
            public void run() {
                ImageLoader.this.onLoad();
            }
        };
        final Handler threadHandler = new Handler();

        public DownloadThread(Group group2) {
            this.group = group2;
        }

        public void run() {
            InputStream inStream = null;
            this._conn = null;
            try {
                this._conn = (HttpURLConnection) new URL(this.group.url).openConnection();
                this._conn.setDoInput(true);
                this._conn.connect();
                InputStream inStream2 = this._conn.getInputStream();
                this.group.bitmap = BitmapFactory.decodeStream(inStream2);
                inStream2.close();
                this._conn.disconnect();
                inStream = null;
                this._conn = null;
            } catch (Exception e) {
            }
            if (inStream != null) {
                try {
                    inStream.close();
                } catch (Exception e2) {
                }
            }
            disconnect();
            this._conn = null;
            this.threadHandler.post(this.threadCallback);
        }

        public void disconnect() {
            if (this._conn != null) {
                this._conn.disconnect();
            }
        }
    }
}
