package com.magmamobile.mmusia.image.cache;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.magmamobile.mmusia.MCommon;
import java.io.File;
import java.io.FileOutputStream;

public class Images {
    private static final String TAG = "PodKast";

    public static boolean isExist(Context context, String key) {
        try {
            if (getCacheFile(context, key).exists() || getDataFile(context, key).exists()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void saveImage(Context context, Drawable data, String key) {
        MCommon.Log_d(TAG, "Save data to cache");
        if (data != null) {
            try {
                FileOutputStream fos = new FileOutputStream(getCacheFile(context, key));
                ((BitmapDrawable) data).getBitmap().compress(Bitmap.CompressFormat.PNG, 10, fos);
                fos.close();
                MCommon.Log_d(TAG, "Image Saved");
            } catch (Exception e) {
                Exception e2 = e;
                MCommon.Log_e(TAG, "Image Cache not Saved :: " + e2.getMessage());
                e2.printStackTrace();
            }
        }
    }

    public static Drawable loadImage(Context context, String key) {
        File file = getCacheFile(context, key);
        if (!file.exists()) {
            return null;
        }
        try {
            return BitmapDrawable.createFromPath(file.getPath());
        } catch (Exception e) {
            Exception e2 = e;
            MCommon.Log_e(TAG, "Image Cache not Loaded :: " + e2.getMessage());
            e2.printStackTrace();
            return null;
        }
    }

    private static File getCacheFile(Context context, String key) {
        try {
            return new File(String.valueOf(context.getCacheDir().getAbsolutePath()) + "/" + key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static File getDataFile(Context context, String key) {
        try {
            return new File(String.valueOf(context.getFilesDir().getAbsolutePath()) + "/" + key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
