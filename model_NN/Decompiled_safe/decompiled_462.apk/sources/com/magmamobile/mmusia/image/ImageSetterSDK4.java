package com.magmamobile.mmusia.image;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public class ImageSetterSDK4 {
    public static void setImage(Drawable d, ImageView img) {
        try {
            Bitmap bm = ((BitmapDrawable) d).getBitmap();
            bm.setDensity(160);
            img.setImageBitmap(bm);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
