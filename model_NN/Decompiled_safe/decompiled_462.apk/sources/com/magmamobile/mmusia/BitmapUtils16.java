package com.magmamobile.mmusia;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.DisplayMetrics;
import android.view.Display;
import java.io.IOException;

public final class BitmapUtils16 {
    public static BitmapDrawable loadDrawable(Activity context, String filename) throws IOException {
        Display display = context.getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        BitmapFactory.Options opts = new BitmapFactory.Options();
        display.getMetrics(metrics);
        opts.inScaled = true;
        opts.inDensity = 160;
        opts.inTargetDensity = metrics.densityDpi;
        return new BitmapDrawable(BitmapFactory.decodeStream(context.getAssets().open(filename), null, opts));
    }
}
