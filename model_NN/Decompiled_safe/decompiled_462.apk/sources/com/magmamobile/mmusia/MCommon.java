package com.magmamobile.mmusia;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.Toast;
import com.magmamobile.mmusia.data.LanguageBase;
import com.magmamobile.mmusia.views.PrefView;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class MCommon {
    public static String GUID = "";
    public static final String TAG = "MMUSIA";
    public static final boolean USE_DEBUG = false;
    public static Map<String, Drawable> drawableMap = new HashMap();
    private static float mDensity = 0.0f;

    public static void Log_d(String msg) {
    }

    public static void Log_i(String msg) {
    }

    public static void Log_w(String msg) {
    }

    public static void Log_e(String msg) {
    }

    public static void Log_v(String msg) {
    }

    public static void Log_d(String TAG2, String msg) {
    }

    public static void Log_i(String TAG2, String msg) {
    }

    public static void Log_w(String TAG2, String msg) {
    }

    public static void Log_e(String TAG2, String msg) {
    }

    public static void Log_v(String TAG2, String msg) {
    }

    public static String getMMUSIAVersion() {
        return "3";
    }

    public static String getLanguage() {
        if (MMUSIA.LANGUAGE == null) {
            MMUSIA.LANGUAGE = "en";
        }
        return MMUSIA.LANGUAGE;
    }

    public static String getSDK() {
        return Build.VERSION.SDK;
    }

    public static String getModelNumber() {
        return Build.MODEL;
    }

    public static String getOperatorName(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName();
    }

    public static String getScreenSize(Context context) {
        Display disp = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        return String.valueOf(disp.getWidth()) + "x" + disp.getHeight();
    }

    public static int getAppVersionCode(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String getAppPackageName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).packageName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getDeviceID(Context context) {
        try {
            String deviceId = Settings.System.getString(context.getContentResolver(), "android_id");
            if (deviceId == null) {
                String deviceId2 = getDIDPref(context);
                if (!deviceId2.equals("") && deviceId2 != null) {
                    return deviceId2;
                }
                String deviceId3 = "emulator/" + UUID.randomUUID().toString();
                setDIDPref(context, deviceId3);
                return deviceId3;
            } else if (!deviceId.toLowerCase().equals("9774d56d682e549c")) {
                return deviceId;
            } else {
                String deviceId4 = getDIDPref(context);
                if (!deviceId4.equals("") && deviceId4 != null) {
                    return deviceId4;
                }
                String deviceId5 = "9774d56d682e549c/" + UUID.randomUUID().toString();
                setDIDPref(context, deviceId5);
                return deviceId5;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private static String getDIDPref(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("DIDBIS", "");
    }

    private static void setDIDPref(Context context, String DeviceId) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString("DIDBIS", DeviceId);
        editor.commit();
    }

    public static boolean getBeforeExitDontShow(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("bExitDshow", false);
    }

    public static void setBeforeExitDontShow(Context context, boolean dontShow) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putBoolean("bExitDshow", dontShow);
        editor.commit();
    }

    public static void launchCountIncrement(Context context) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putInt("LaunchCount", getlaunchCount(context) + 1);
        editor.commit();
    }

    public static int getlaunchCount(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("LaunchCount", 0);
    }

    public static int getLatestPromoIDPref(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("PROMOID", 0);
    }

    public static void setLatestPromoIDPref(Context context, int id) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putInt("PROMOID", id);
        editor.commit();
    }

    public static List<NameValuePair> buildUrlParam(Context context, int LatestNewsId, boolean forceUpdate) {
        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair("di", getDeviceID(context)));
        nvps.add(new BasicNameValuePair("lng", getLanguage()));
        nvps.add(new BasicNameValuePair("lid", new StringBuilder(String.valueOf(LatestNewsId)).toString()));
        nvps.add(new BasicNameValuePair("ver", new StringBuilder(String.valueOf(getAppVersionCode(context))).toString()));
        nvps.add(new BasicNameValuePair("pn", getAppPackageName(context)));
        nvps.add(new BasicNameValuePair("pm", getModelNumber()));
        nvps.add(new BasicNameValuePair("sw", getScreenSize(context)));
        nvps.add(new BasicNameValuePair("sv", getSDK()));
        nvps.add(new BasicNameValuePair("mmver", getMMUSIAVersion()));
        if (forceUpdate) {
            nvps.add(new BasicNameValuePair("gu", "1"));
        }
        return nvps;
    }

    public static String generateString(InputStream stream) {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(stream));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String cur = buffer.readLine();
                if (cur != null) {
                    sb.append(cur);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                break;
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        stream.close();
        return sb.toString();
    }

    public static String generateString(InputStream stream, String charset) {
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader buffer = new BufferedReader(new InputStreamReader(stream, charset));
            while (true) {
                String cur = buffer.readLine();
                if (cur == null) {
                    break;
                }
                sb.append(cur).append("\n");
            }
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static void showPrefs(final Context context) {
        AlertDialog.Builder ad = new AlertDialog.Builder(context);
        View v = new PrefView(context);
        final CheckBox check = (CheckBox) v.findViewById(MMUSIA.RES_ID_PREF_CHECK_ENABLE);
        check.setChecked(getPrefNotifStatus(context));
        ad.setView(v);
        ad.setCancelable(true);
        ad.setTitle(LanguageBase.DIALOG_SETTINGS_TITLE);
        ad.setPositiveButton(LanguageBase.DIALOG_SETTINGS_CLOSE, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                MCommon.setPrefNotifStatus(context, check.isChecked());
                MMUSIA.activateNews(context, check.isChecked());
            }
        });
        ad.show();
    }

    public static boolean getPrefNotifStatus(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("mmusianotif", true);
    }

    public static void setPrefNotifStatus(Context context, boolean activate) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putBoolean("mmusianotif", activate);
        editor.commit();
    }

    public static Drawable getAssetDrawable(Activity context, String filename) {
        try {
            return BitmapUtils.loadDrawable(context, filename);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Drawable getAssetDrawableResize(Drawable d, int W, int H) {
        if (d == null) {
            return null;
        }
        try {
            return new BitmapDrawable(Bitmap.createScaledBitmap(((BitmapDrawable) d).getBitmap(), W, H, false));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static final void useDpi(Context context) {
        mDensity = getDensity(context);
    }

    public static final float getDensity(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.density;
    }

    public static final int dpi(int value) {
        if (mDensity == 1.0f) {
            return value;
        }
        Log_d("dpi : value " + value + " => " + ((int) (((float) value) * mDensity)));
        return (int) (((float) value) * mDensity);
    }

    public static final int dpiImage(int value) {
        if (mDensity == 1.0f) {
            return value;
        }
        if (((double) mDensity) == 1.5d) {
            Log_d("dpi : value " + value + " x2 => " + ((int) (((float) value) * 3.0f)));
            return (int) (((float) value) * 3.0f);
        }
        Log_d("dpi : value " + value + " => " + ((int) (((float) value) * mDensity)));
        return (int) (((float) value) * mDensity);
    }

    public static void openMarket(Context context, String pname) {
        try {
            ((Activity) context).startActivityForResult(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + pname)), 2000);
        } catch (Exception e) {
            Toast.makeText(context, LanguageBase.MARKET_NOT_FOUND, 2000).show();
        }
    }

    public static void openMarketLink(Context context, String link) {
        try {
            ((Activity) context).startActivityForResult(new Intent("android.intent.action.VIEW", Uri.parse(link)), 2000);
        } catch (Exception e) {
            Toast.makeText(context, LanguageBase.MARKET_NOT_FOUND, 2000).show();
        }
    }

    public static void openUrlPage(Context context, String url) {
        ((Activity) context).startActivityForResult(new Intent("android.intent.action.VIEW", Uri.parse(url)), 9998);
    }

    public static boolean isSDKAPI4Mini() {
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 4) {
                return true;
            }
            return false;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String alphaNum(String name, String replaceWith) {
        return name.replaceAll("[^a-zA-Z0-9]", replaceWith);
    }

    public static String alphaNumWithAccent(String name, String replaceWith) {
        return name.replaceAll("[^a-zA-ZÀ-ÿ0-9]", replaceWith);
    }
}
