package com.magmamobile.mmusia;

import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import java.io.IOException;

public final class BitmapUtils {
    public static BitmapDrawable loadDrawable(Activity context, String filename) throws IOException {
        if (Integer.valueOf(Build.VERSION.SDK).intValue() <= 3) {
            return new BitmapDrawable(context.getAssets().open(filename));
        }
        return BitmapUtils16.loadDrawable(context, filename);
    }
}
