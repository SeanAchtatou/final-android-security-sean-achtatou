package com.adwhirl.adapters;

import android.util.Log;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.obj.Extra2;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;

public class GenericAdapter extends AdWhirlAdapter {
    public GenericAdapter(AdWhirlLayout adWhirlLayout, Ration ration) {
        super(adWhirlLayout, ration);
    }

    public void handle() {
        if (Extra2.verboselog) {
            Log.d(AdWhirlUtil.ADWHIRL, "Generic notification request initiated");
        }
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            if (adWhirlLayout.adWhirlInterface != null) {
                adWhirlLayout.adWhirlInterface.adWhirlGeneric();
            } else if (Extra2.verboselog) {
                Log.w(AdWhirlUtil.ADWHIRL, "Generic notification sent, but no interface is listening");
            }
            adWhirlLayout.adWhirlManager.resetRollover();
            adWhirlLayout.rotateThreadedDelayed();
        }
    }
}
