package com.adwhirl.adapters;

import android.util.Log;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.obj.Extra2;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;

public class EventAdapter extends AdWhirlAdapter {
    public EventAdapter(AdWhirlLayout adWhirlLayout, Ration ration) {
        super(adWhirlLayout, ration);
    }

    public void handle() {
        if (Extra2.verboselog) {
            Log.d(AdWhirlUtil.ADWHIRL, "Event notification request initiated");
        }
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            if (adWhirlLayout.adWhirlInterface != null) {
                String key = this.ration.key;
                if (key == null) {
                    if (Extra2.verboselog) {
                        Log.w(AdWhirlUtil.ADWHIRL, "Event key is null");
                    }
                    adWhirlLayout.rollover();
                    return;
                }
                int methodIndex = key.indexOf("|;|");
                if (methodIndex < 0) {
                    if (Extra2.verboselog) {
                        Log.w(AdWhirlUtil.ADWHIRL, "Event key separator not found");
                    }
                    adWhirlLayout.rollover();
                    return;
                }
                try {
                    adWhirlLayout.adWhirlInterface.getClass().getMethod(key.substring(methodIndex + 3), null).invoke(adWhirlLayout.adWhirlInterface, null);
                } catch (Exception e) {
                    Exception e2 = e;
                    if (Extra2.verboselog) {
                        Log.e(AdWhirlUtil.ADWHIRL, "Caught exception in handle()", e2);
                    }
                    adWhirlLayout.rollover();
                }
            } else {
                if (Extra2.verboselog) {
                    Log.w(AdWhirlUtil.ADWHIRL, "Event notification would be sent, but no interface is listening");
                }
                adWhirlLayout.rollover();
            }
        }
    }
}
