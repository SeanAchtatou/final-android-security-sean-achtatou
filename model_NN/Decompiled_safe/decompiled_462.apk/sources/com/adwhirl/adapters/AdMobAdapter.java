package com.adwhirl.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import com.admob.android.ads.AdListener;
import com.admob.android.ads.AdManager;
import com.admob.android.ads.AdView;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlTargeting;
import com.adwhirl.obj.Extra;
import com.adwhirl.obj.Extra2;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;
import java.util.GregorianCalendar;

public class AdMobAdapter extends AdWhirlAdapter implements AdListener {
    public AdMobAdapter(AdWhirlLayout adWhirlLayout, Ration ration) {
        super(adWhirlLayout, ration);
    }

    public void handle() {
        String keywords;
        if (Extra2.verboselog) {
            Log.d(AdWhirlUtil.ADWHIRL, "AdMob 3.X Handle");
        }
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            try {
                AdManager.setPublisherId(this.ration.key);
                Activity activity = adWhirlLayout.activityReference.get();
                if (activity != null) {
                    AdView adMob = new AdView(activity);
                    adMob.setAdListener(this);
                    Extra extra = adWhirlLayout.extra;
                    int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
                    int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
                    adMob.setBackgroundColor(bgColor);
                    adMob.setPrimaryTextColor(fgColor);
                    AdWhirlTargeting.Gender gender = AdWhirlTargeting.getGender();
                    if (gender == AdWhirlTargeting.Gender.FEMALE) {
                        AdManager.setGender(AdManager.Gender.FEMALE);
                    } else if (gender == AdWhirlTargeting.Gender.MALE) {
                        AdManager.setGender(AdManager.Gender.MALE);
                    }
                    GregorianCalendar birthDate = AdWhirlTargeting.getBirthDate();
                    if (birthDate != null) {
                        AdManager.setBirthday(birthDate);
                    }
                    String postalCode = AdWhirlTargeting.getPostalCode();
                    if (!TextUtils.isEmpty(postalCode)) {
                        AdManager.setPostalCode(postalCode);
                    }
                    if (AdWhirlTargeting.getKeywordSet() != null) {
                        keywords = TextUtils.join(" ", AdWhirlTargeting.getKeywordSet());
                    } else {
                        keywords = AdWhirlTargeting.getKeywords();
                    }
                    if (!TextUtils.isEmpty(keywords)) {
                        adMob.setKeywords(keywords);
                    }
                    if (extra.locationOn == 1) {
                        AdManager.setAllowUseOfLocation(true);
                    }
                    adMob.setVisibility(4);
                    adWhirlLayout.addView(adMob, -2, -2);
                }
            } catch (IllegalArgumentException e) {
                adWhirlLayout.rollover();
            }
        }
    }

    public void onReceiveAd(AdView adView) {
        if (Extra2.verboselog) {
            Log.d(AdWhirlUtil.ADWHIRL, "AdMob success");
        }
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adWhirlLayout.removeView(adView);
            adView.setVisibility(0);
            adWhirlLayout.adWhirlManager.resetRollover();
            adWhirlLayout.handler.post(new AdWhirlLayout.ViewAdRunnable(adWhirlLayout, adView));
            adWhirlLayout.rotateThreadedDelayed();
        }
    }

    public void onFailedToReceiveAd(AdView adView) {
        if (Extra2.verboselog) {
            Log.d(AdWhirlUtil.ADWHIRL, "AdMob failure");
        }
        adView.setAdListener(null);
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adWhirlLayout.removeView(adView);
            adWhirlLayout.rollover();
        }
    }

    public void onFailedToReceiveRefreshedAd(AdView adView) {
    }

    public void onReceiveRefreshedAd(AdView adView) {
    }
}
