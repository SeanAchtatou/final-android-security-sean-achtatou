package com.adwhirl.adapters;

import android.app.Activity;
import android.util.Log;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.obj.Extra2;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;
import com.zestadz.android.AdManager;
import com.zestadz.android.ZestADZAdView;

public class ZestAdzAdapter extends AdWhirlAdapter implements ZestADZAdView.ZestADZListener {
    public ZestAdzAdapter(AdWhirlLayout adWhirlLayout, Ration ration) {
        super(adWhirlLayout, ration);
    }

    public void handle() {
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            try {
                AdManager.setadclientId(this.ration.key);
                try {
                    Activity activity = adWhirlLayout.activityReference.get();
                    if (activity != null) {
                        ZestADZAdView adView = new ZestADZAdView(activity);
                        adView.setListener(this);
                        adView.displayAd();
                    }
                } catch (Exception e) {
                    adWhirlLayout.rollover();
                }
            } catch (IllegalArgumentException e2) {
                adWhirlLayout.rollover();
            }
        }
    }

    public void AdReturned(ZestADZAdView adView) {
        if (Extra2.verboselog) {
            Log.d(AdWhirlUtil.ADWHIRL, "ZestADZ success");
        }
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adWhirlLayout.adWhirlManager.resetRollover();
            adWhirlLayout.handler.post(new AdWhirlLayout.ViewAdRunnable(adWhirlLayout, adView));
            adWhirlLayout.rotateThreadedDelayed();
        }
    }

    public void AdFailed(ZestADZAdView adView) {
        if (Extra2.verboselog) {
            Log.d(AdWhirlUtil.ADWHIRL, "ZestADZ failure");
        }
        adView.setListener(null);
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adWhirlLayout.rollover();
        }
    }
}
