package com.adwhirl;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import com.adwhirl.obj.Custom;
import com.adwhirl.obj.Extra;
import com.adwhirl.obj.Extra2;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class AdWhirlManager {
    private static final String PREFS_STRING_CONFIG = "config";
    private static final String PREFS_STRING_TIMESTAMP = "timestamp";
    private static long configExpireTimeout = 1800000;
    private WeakReference<Context> contextReference;
    public String deviceIDHash;
    private Extra extra;
    public String keyAdWhirl;
    public String localeString;
    public Location location;
    private List<Ration> rationsList;
    Iterator<Ration> rollovers;
    private double totalWeight = 0.0d;

    public AdWhirlManager(WeakReference<Context> contextReference2, String keyAdWhirl2) {
        if (Extra2.verboselog) {
            Log.i(AdWhirlUtil.ADWHIRL, "Creating adWhirlManager...");
        }
        this.contextReference = contextReference2;
        this.keyAdWhirl = keyAdWhirl2;
        this.localeString = Locale.getDefault().toString();
        if (Extra2.verboselog) {
            Log.d(AdWhirlUtil.ADWHIRL, "Locale is: " + this.localeString);
        }
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            StringBuffer deviceIDString = new StringBuffer("android_id");
            deviceIDString.append("AdWhirl");
            this.deviceIDHash = AdWhirlUtil.convertToHex(md.digest(deviceIDString.toString().getBytes()));
        } catch (NoSuchAlgorithmException e) {
            this.deviceIDHash = "00000000000000000000000000000000";
        }
        if (Extra2.verboselog) {
            Log.d(AdWhirlUtil.ADWHIRL, "Hashed device ID is: " + this.deviceIDHash);
        }
        if (Extra2.verboselog) {
            Log.i(AdWhirlUtil.ADWHIRL, "Finished creating adWhirlManager");
        }
    }

    public static void setConfigExpireTimeout(long configExpireTimeout2) {
        configExpireTimeout = configExpireTimeout2;
    }

    public Extra getExtra() {
        if (this.totalWeight > 0.0d) {
            return this.extra;
        }
        if (Extra2.verboselog) {
            Log.i(AdWhirlUtil.ADWHIRL, "Sum of ration weights is 0 - no ads to be shown");
        }
        return null;
    }

    public Ration getRation() {
        double r = new Random().nextDouble() * this.totalWeight;
        double s = 0.0d;
        if (Extra2.verboselog) {
            Log.d(AdWhirlUtil.ADWHIRL, "Dart is <" + r + "> of <" + this.totalWeight + ">");
        }
        Iterator<Ration> it = this.rationsList.iterator();
        Ration ration = null;
        while (it.hasNext()) {
            ration = it.next();
            s += ration.weight;
            if (s >= r) {
                break;
            }
        }
        return ration;
    }

    public Ration getRollover() {
        if (this.rollovers == null) {
            return null;
        }
        if (Extra2.verboselog) {
            Log.w(AdWhirlUtil.ADWHIRL, "GETROLLOVER");
        }
        Ration ration = null;
        if (this.rollovers.hasNext()) {
            ration = this.rollovers.next();
        }
        return ration;
    }

    public void resetRollover() {
        if (Extra2.verboselog) {
            Log.w(AdWhirlUtil.ADWHIRL, "RESET ROLLOVER");
        }
        this.rollovers = this.rationsList.iterator();
    }

    public Custom getKreactive(String nid) {
        String baseUrl;
        HttpClient httpClient = new DefaultHttpClient();
        if (Extra2.kreativeTest) {
            baseUrl = "http://dep-adserver.feedvalue.com/getads.php";
        } else {
            baseUrl = "http://adserver.feedvalue.com/getads.php";
        }
        String url = baseUrl.concat(String.format(AdWhirlUtil.urlKreative, Integer.valueOf(Extra2.kreativeCat), Integer.valueOf(Extra2.kreativeAppID), Integer.valueOf(Extra2.kreativeW), Integer.valueOf(Extra2.kreativeH), Integer.valueOf(ModCommon.getConnectionType(this.contextReference.get())), ModCommon.getLocale(this.contextReference.get())));
        if (Extra2.verboselog) {
            Log.d("URL Kreactive", url);
        }
        try {
            HttpResponse httpResponse = httpClient.execute(new HttpGet(url));
            if (Extra2.verboselog) {
                Log.d(AdWhirlUtil.ADWHIRL, httpResponse.getStatusLine().toString());
            }
            HttpEntity entity = httpResponse.getEntity();
            if (!(entity == null || httpResponse.getStatusLine().getStatusCode() == 403)) {
                return parseKreactiveXMLStream(entity.getContent());
            }
        } catch (ClientProtocolException e) {
            ClientProtocolException e2 = e;
            if (Extra2.verboselog) {
                Log.e(AdWhirlUtil.ADWHIRL, "Caught ClientProtocolException in getCustom()", e2);
            }
        } catch (IOException e3) {
            IOException e4 = e3;
            if (Extra2.verboselog) {
                Log.e(AdWhirlUtil.ADWHIRL, "Caught IOException in getCustom()", e4);
            }
        }
        return null;
    }

    public Custom getCustom(String nid) {
        String locationString;
        HttpClient httpClient = new DefaultHttpClient();
        if (this.extra.locationOn == 1) {
            this.location = getLocation();
            if (this.location != null) {
                locationString = String.format(AdWhirlUtil.locationString, Double.valueOf(this.location.getLatitude()), Double.valueOf(this.location.getLongitude()), Long.valueOf(this.location.getTime()));
            } else {
                locationString = "";
            }
        } else {
            this.location = null;
            locationString = "";
        }
        try {
            HttpResponse httpResponse = httpClient.execute(new HttpGet(String.format(AdWhirlUtil.urlCustom, this.keyAdWhirl, nid, this.deviceIDHash, this.localeString, locationString, Integer.valueOf((int) AdWhirlUtil.VERSION))));
            if (Extra2.verboselog) {
                Log.d(AdWhirlUtil.ADWHIRL, httpResponse.getStatusLine().toString());
            }
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                return parseCustomJsonString(convertStreamToString(entity.getContent()));
            }
        } catch (ClientProtocolException e) {
            ClientProtocolException e2 = e;
            if (Extra2.verboselog) {
                Log.e(AdWhirlUtil.ADWHIRL, "Caught ClientProtocolException in getCustom()", e2);
            }
        } catch (IOException e3) {
            IOException e4 = e3;
            if (Extra2.verboselog) {
                Log.e(AdWhirlUtil.ADWHIRL, "Caught IOException in getCustom()", e4);
            }
        }
        return null;
    }

    public void fetchConfig() {
        Context context = this.contextReference.get();
        if (context != null) {
            SharedPreferences adWhirlPrefs = context.getSharedPreferences(this.keyAdWhirl, 0);
            String jsonString = adWhirlPrefs.getString(PREFS_STRING_CONFIG, null);
            long timestamp = adWhirlPrefs.getLong(PREFS_STRING_TIMESTAMP, -1);
            if (Extra2.verboselog) {
                Log.d(AdWhirlUtil.ADWHIRL, "Prefs{" + this.keyAdWhirl + "}: {\"" + PREFS_STRING_CONFIG + "\": \"" + jsonString + "\", \"" + PREFS_STRING_TIMESTAMP + "\": " + timestamp + "}");
            }
            if (jsonString == null || configExpireTimeout == -1 || System.currentTimeMillis() >= configExpireTimeout + timestamp) {
                if (Extra2.verboselog) {
                    Log.i(AdWhirlUtil.ADWHIRL, "Stored config info not present or expired, fetching fresh data");
                }
                try {
                    HttpResponse httpResponse = new DefaultHttpClient().execute(new HttpGet(String.format(AdWhirlUtil.urlConfig, this.keyAdWhirl, Integer.valueOf((int) AdWhirlUtil.VERSION))));
                    if (Extra2.verboselog) {
                        Log.d(AdWhirlUtil.ADWHIRL, httpResponse.getStatusLine().toString());
                    }
                    HttpEntity entity = httpResponse.getEntity();
                    if (entity != null) {
                        jsonString = convertStreamToString(entity.getContent());
                        SharedPreferences.Editor editor = adWhirlPrefs.edit();
                        editor.putString(PREFS_STRING_CONFIG, jsonString);
                        editor.putLong(PREFS_STRING_TIMESTAMP, System.currentTimeMillis());
                        editor.commit();
                    }
                } catch (ClientProtocolException e) {
                    ClientProtocolException e2 = e;
                    if (Extra2.verboselog) {
                        Log.e(AdWhirlUtil.ADWHIRL, "Caught ClientProtocolException in fetchConfig()", e2);
                    }
                } catch (IOException e3) {
                    IOException e4 = e3;
                    if (Extra2.verboselog) {
                        Log.e(AdWhirlUtil.ADWHIRL, "Caught IOException in fetchConfig()", e4);
                    }
                }
            } else if (Extra2.verboselog) {
                Log.i(AdWhirlUtil.ADWHIRL, "Using stored config data");
            }
            parseConfigurationString(jsonString);
        }
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        is.close();
                        return sb.toString();
                    } catch (IOException e) {
                        if (Extra2.verboselog) {
                            Log.e(AdWhirlUtil.ADWHIRL, "Caught IOException in convertStreamToString()", e);
                        }
                        return null;
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                IOException e3 = e2;
                if (Extra2.verboselog) {
                    Log.e(AdWhirlUtil.ADWHIRL, "Caught IOException in convertStreamToString()", e3);
                }
                try {
                    is.close();
                    return null;
                } catch (IOException e4) {
                    if (Extra2.verboselog) {
                        Log.e(AdWhirlUtil.ADWHIRL, "Caught IOException in convertStreamToString()", e4);
                    }
                    return null;
                }
            } catch (Throwable th) {
                try {
                    is.close();
                    throw th;
                } catch (IOException e5) {
                    if (Extra2.verboselog) {
                        Log.e(AdWhirlUtil.ADWHIRL, "Caught IOException in convertStreamToString()", e5);
                    }
                    return null;
                }
            }
        }
    }

    private void parseConfigurationString(String jsonString) {
        if (Extra2.verboselog) {
            Log.d(AdWhirlUtil.ADWHIRL, "Received jsonString: " + jsonString);
        }
        try {
            JSONObject json = new JSONObject(jsonString);
            parseExtraJson(json.getJSONObject("extra"));
            parseRationsJson(json.getJSONArray("rations"));
        } catch (JSONException e) {
            JSONException e2 = e;
            if (Extra2.verboselog) {
                Log.e(AdWhirlUtil.ADWHIRL, "Unable to parse response from JSON. This may or may not be fatal.", e2);
            }
            this.extra = new Extra();
        } catch (NullPointerException e3) {
            NullPointerException e4 = e3;
            if (Extra2.verboselog) {
                Log.e(AdWhirlUtil.ADWHIRL, "Unable to parse response from JSON. This may or may not be fatal.", e4);
            }
            this.extra = new Extra();
        }
    }

    private void parseExtraJson(JSONObject json) {
        Extra extra2 = new Extra();
        try {
            extra2.cycleTime = json.getInt("cycle_time");
            extra2.locationOn = json.getInt("location_on");
            extra2.transition = json.getInt("transition");
            JSONObject backgroundColor = json.getJSONObject("background_color_rgb");
            extra2.bgRed = backgroundColor.getInt("red");
            extra2.bgGreen = backgroundColor.getInt("green");
            extra2.bgBlue = backgroundColor.getInt("blue");
            extra2.bgAlpha = backgroundColor.getInt("alpha") * 255;
            JSONObject textColor = json.getJSONObject("text_color_rgb");
            extra2.fgRed = textColor.getInt("red");
            extra2.fgGreen = textColor.getInt("green");
            extra2.fgBlue = textColor.getInt("blue");
            extra2.fgAlpha = textColor.getInt("alpha") * 255;
        } catch (JSONException e) {
            JSONException e2 = e;
            if (Extra2.verboselog) {
                Log.e(AdWhirlUtil.ADWHIRL, "Exception in parsing config.extra JSON. This may or may not be fatal.", e2);
            }
        }
        this.extra = extra2;
    }

    private void parseRationsJson(JSONArray json) {
        List<Ration> rationsList2 = new ArrayList<>();
        this.totalWeight = 0.0d;
        int i = 0;
        while (i < json.length()) {
            try {
                JSONObject jsonRation = json.getJSONObject(i);
                if (jsonRation != null) {
                    Ration ration = new Ration();
                    ration.nid = jsonRation.getString("nid");
                    ration.type = jsonRation.getInt("type");
                    ration.name = jsonRation.getString("nname");
                    ration.weight = (double) jsonRation.getInt("weight");
                    ration.priority = jsonRation.getInt("priority");
                    switch (ration.type) {
                        case 8:
                            JSONObject keyObj = jsonRation.getJSONObject("key");
                            ration.key = keyObj.getString("siteID");
                            ration.key2 = keyObj.getString("publisherID");
                            break;
                        default:
                            ration.key = jsonRation.getString("key");
                            break;
                    }
                    this.totalWeight += ration.weight;
                    rationsList2.add(ration);
                }
                i++;
            } catch (JSONException e) {
                JSONException e2 = e;
                if (Extra2.verboselog) {
                    Log.e(AdWhirlUtil.ADWHIRL, "JSONException in parsing config.rations JSON. This may or may not be fatal.", e2);
                }
            }
        }
        Collections.sort(rationsList2);
        this.rationsList = rationsList2;
        this.rollovers = this.rationsList.iterator();
    }

    private Custom parseKreactiveXMLStream(InputStream xmlStream) {
        Custom custom = new Custom();
        try {
            NodeList items = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlStream).getDocumentElement().getElementsByTagName("ad");
            for (int i = 0; i < items.getLength(); i++) {
                NodeList properties = items.item(i).getChildNodes();
                for (int j = 0; j < properties.getLength(); j++) {
                    Node property = properties.item(j);
                    String name = property.getNodeName();
                    if (name.equalsIgnoreCase("url")) {
                        custom.imageLink = property.getFirstChild().getNodeValue();
                    } else if (name.equalsIgnoreCase("destination")) {
                        custom.link = property.getFirstChild().getNodeValue();
                    } else if (name.equalsIgnoreCase("id")) {
                        String id = property.getFirstChild().getNodeValue();
                        Context context = this.contextReference.get();
                        custom.setTrackingClick(context, id);
                        custom.setTrackingDisplay(context, id);
                    }
                }
            }
            custom.type = 1;
            custom.description = "Kreactive";
            if (custom.link.startsWith("market://")) {
                custom.link = String.valueOf(custom.link) + Extra2.extraLinkMarketCustom;
            }
            custom.image = fetchImage(custom.imageLink);
            return custom;
        } catch (Exception e) {
            Exception e2 = e;
            if (Extra2.verboselog) {
                Log.e(AdWhirlUtil.ADWHIRL, "Caught Exception in parseKreactiveXMLString()", e2);
            }
            return null;
        }
    }

    private Custom parseCustomJsonString(String jsonString) {
        if (Extra2.verboselog) {
            Log.d(AdWhirlUtil.ADWHIRL, "Received custom jsonString: " + jsonString);
        }
        Custom custom = new Custom();
        JSONObject json = new JSONObject(jsonString);
        custom.type = json.getInt("ad_type");
        custom.imageLink = json.getString("img_url");
        custom.link = json.getString("redirect_url");
        custom.description = json.getString("ad_text");
        if (custom.link.startsWith("market://")) {
            custom.link = String.valueOf(custom.link) + Extra2.extraLinkMarketCustom;
        }
        try {
            custom.imageLink480x75 = json.getString("img_url_480x75");
        } catch (JSONException e) {
            JSONException jSONException = e;
            custom.imageLink480x75 = null;
        }
        try {
            DisplayMetrics metrics = new DisplayMetrics();
            ((WindowManager) this.contextReference.get().getSystemService("window")).getDefaultDisplay().getMetrics(metrics);
            if (((double) metrics.density) != 1.5d || custom.type != 1 || custom.imageLink480x75 == null || custom.imageLink480x75.length() == 0) {
                custom.image = fetchImage(custom.imageLink);
            } else {
                custom.image = fetchImage(custom.imageLink480x75);
            }
            return custom;
        } catch (JSONException e2) {
            JSONException e3 = e2;
            if (Extra2.verboselog) {
                Log.e(AdWhirlUtil.ADWHIRL, "Caught JSONException in parseCustomJsonString()", e3);
            }
            return null;
        }
    }

    private Drawable fetchImage(String urlString) {
        try {
            return Drawable.createFromStream((InputStream) new URL(urlString).getContent(), "src");
        } catch (Exception e) {
            Exception e2 = e;
            if (Extra2.verboselog) {
                Log.e(AdWhirlUtil.ADWHIRL, "Unable to fetchImage(): ", e2);
            }
            return null;
        }
    }

    public Location getLocation() {
        if (this.contextReference == null) {
            return null;
        }
        Context context = this.contextReference.get();
        if (context == null) {
            return null;
        }
        Location location2 = null;
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
            location2 = ((LocationManager) context.getSystemService("location")).getLastKnownLocation("gps");
        } else if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            location2 = ((LocationManager) context.getSystemService("location")).getLastKnownLocation("network");
        }
        return location2;
    }
}
