package com.adwhirl.obj;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.adwhirl.ModCommon;
import com.adwhirl.util.AdWhirlUtil;

public class Custom {
    public String description;
    public Drawable image;
    public String imageLink;
    public String imageLink480x75;
    public String link;
    public String trackingClick;
    public String trackingDisplay;
    public int type;

    public void setTrackingClick(Context context, String id) {
        String baseUrl;
        if (Extra2.kreativeTest) {
            baseUrl = "http://dep-adserver.feedvalue.com/tracking.html";
        } else {
            baseUrl = "http://adserver.feedvalue.com/tracking.html";
        }
        this.trackingClick = baseUrl.concat(String.format(AdWhirlUtil.urlKreativeTracking, 2, Integer.valueOf(Extra2.kreativeCat), ModCommon.getDeviceID(context), ModCommon.getModelNumber(), ModCommon.getSDK(), Integer.valueOf(Extra2.kreativeAppID), id));
        if (Extra2.verboselog) {
            Log.d("TRACKING CLICK", this.trackingClick);
        }
    }

    public void setTrackingDisplay(Context context, String id) {
        String baseUrl;
        if (Extra2.kreativeTest) {
            baseUrl = "http://dep-adserver.feedvalue.com/tracking.html";
        } else {
            baseUrl = "http://adserver.feedvalue.com/tracking.html";
        }
        this.trackingDisplay = baseUrl.concat(String.format(AdWhirlUtil.urlKreativeTracking, 1, Integer.valueOf(Extra2.kreativeCat), ModCommon.getDeviceID(context), ModCommon.getModelNumber(), ModCommon.getSDK(), Integer.valueOf(Extra2.kreativeAppID), id));
        if (Extra2.verboselog) {
            Log.i("TRACKING DISPLAY", this.trackingDisplay);
        }
    }
}
