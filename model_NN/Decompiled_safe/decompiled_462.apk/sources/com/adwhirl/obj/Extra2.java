package com.adwhirl.obj;

import com.inmobi.androidsdk.impl.Constants;

public class Extra2 {
    public static String adsenseColorBackground = "#FFFFFF";
    public static String adsenseColorBorder = "#FFFFFF";
    public static String adsenseColorLink = "#0000FF";
    public static String adsenseColorText = "#000000";
    public static String adsenseColorUrl = "#0000FF";
    public static String extraLinkMarketCustom = "";
    public static String googleAdSenseWebEquivalentUrl = "";
    public static boolean googleTestMode = false;
    public static int kreativeAppID = 181;
    public static int kreativeCat = 2;
    public static int kreativeH = 50;
    public static boolean kreativeTest = false;
    public static int kreativeW = Constants.INMOBI_ADVIEW_WIDTH;
    public static boolean verboselog = false;
}
