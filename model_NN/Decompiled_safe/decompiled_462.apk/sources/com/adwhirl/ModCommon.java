package com.adwhirl;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Process;
import android.provider.Settings;
import android.util.Log;
import com.admob.android.ads.AdManager;
import com.adwhirl.obj.Extra2;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class ModCommon {
    public static String getModelNumber() {
        return alphaNum(Build.MODEL, "_");
    }

    public static String getDeviceID(Context context) {
        String deviceId = Settings.System.getString(context.getContentResolver(), "android_id");
        if (deviceId == null) {
            return AdManager.TEST_EMULATOR;
        }
        return deviceId;
    }

    public static String getSDK() {
        return Build.VERSION.SDK;
    }

    public static int getConnectionType(Context context) {
        if (context.checkPermission("android.permission.ACCESS_NETWORK_STATE", Process.myPid(), Process.myUid()) == -1) {
            return 3;
        }
        NetworkInfo info = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        int netType = info.getType();
        int netSubtype = info.getSubtype();
        if (netType == 1) {
            return 1;
        }
        return (netType == 0 && netSubtype == 3) ? 2 : 3;
    }

    public static String getLocale(Context context) {
        return context.getResources().getConfiguration().locale.toString();
    }

    public static void callTracking(String _url) {
        final String url = _url;
        new Thread(new Runnable() {
            public void run() {
                try {
                    HttpParams httpParams = new BasicHttpParams();
                    HttpConnectionParams.setConnectionTimeout(httpParams, 3000);
                    HttpConnectionParams.setSoTimeout(httpParams, 5000);
                    new DefaultHttpClient(httpParams).execute(new HttpGet(url));
                    if (Extra2.verboselog) {
                        Log.i("Tracking Kreactive OK", url);
                    }
                } catch (Exception e) {
                    if (Extra2.verboselog) {
                        Log.d("Error tracking Kreactive", url);
                    }
                }
            }
        }).start();
    }

    public static String alphaNum(String name, String replaceWith) {
        return name.replaceAll("[^a-zA-Z0-9]", replaceWith);
    }
}
