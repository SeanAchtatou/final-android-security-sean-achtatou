package com.adwhirl;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import com.adwhirl.adapters.AdWhirlAdapter;
import com.adwhirl.obj.Custom;
import com.adwhirl.obj.Extra;
import com.adwhirl.obj.Extra2;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;
import com.google.ads.AdSenseSpec;
import com.google.ads.GoogleAdView;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class AdWhirlLayout extends RelativeLayout {
    public static final String ADWHIRL_KEY = "ADWHIRL_KEY";
    public Ration activeRation;
    public WeakReference<Activity> activityReference;
    public AdWhirlInterface adWhirlInterface;
    public AdWhirlManager adWhirlManager;
    public AdSenseSpec.AdType adsenseAdType = null;
    public Custom custom;
    public Extra extra;
    public final Handler handler = new Handler();
    /* access modifiers changed from: private */
    public boolean hasWindow;
    /* access modifiers changed from: private */
    public boolean isScheduled;
    private String keyAdWhirl;
    private int maxHeight;
    private int maxWidth;
    public Ration nextRation;
    public final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    public WeakReference<RelativeLayout> superViewReference;

    public interface AdWhirlInterface {
        void adWhirlGeneric();
    }

    public void setMaxWidth(int width) {
        this.maxWidth = width;
    }

    public void setMaxHeight(int height) {
        this.maxHeight = height;
    }

    public AdWhirlLayout(Activity context, String keyAdWhirl2) {
        super(context);
        init(context, keyAdWhirl2);
    }

    public AdWhirlLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init((Activity) context, getAdWhirlKey(context));
    }

    /* access modifiers changed from: protected */
    public String getAdWhirlKey(Context context) {
        String packageName = context.getPackageName();
        String activityName = context.getClass().getName();
        PackageManager pm = context.getPackageManager();
        try {
            Bundle bundle = pm.getActivityInfo(new ComponentName(packageName, activityName), 128).metaData;
            if (bundle != null) {
                return bundle.getString(ADWHIRL_KEY);
            }
            try {
                Bundle bundle2 = pm.getApplicationInfo(packageName, 128).metaData;
                if (bundle2 != null) {
                    return bundle2.getString(ADWHIRL_KEY);
                }
                return null;
            } catch (PackageManager.NameNotFoundException e) {
                return null;
            }
        } catch (PackageManager.NameNotFoundException e2) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void init(Activity context, String keyAdWhirl2) {
        this.activityReference = new WeakReference<>(context);
        this.superViewReference = new WeakReference<>(this);
        this.keyAdWhirl = keyAdWhirl2;
        this.hasWindow = true;
        this.isScheduled = true;
        this.scheduler.schedule(new InitRunnable(this, keyAdWhirl2), 0, TimeUnit.SECONDS);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        this.maxWidth = 0;
        this.maxHeight = 0;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = View.MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = View.MeasureSpec.getSize(heightMeasureSpec);
        if (this.maxWidth > 0 && widthSize > this.maxWidth) {
            widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.maxWidth, Integer.MIN_VALUE);
        }
        if (this.maxHeight > 0 && heightSize > this.maxHeight) {
            heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.maxHeight, Integer.MIN_VALUE);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        if (visibility == 0) {
            this.hasWindow = true;
            if (!this.isScheduled) {
                this.isScheduled = true;
                if (this.extra != null) {
                    rotateThreadedNow();
                } else {
                    this.scheduler.schedule(new InitRunnable(this, this.keyAdWhirl), 0, TimeUnit.SECONDS);
                }
            }
        } else {
            this.hasWindow = false;
        }
    }

    public void onWindowVisibilityChanged2(int visibility) {
        if (visibility != 0) {
            this.isScheduled = false;
        }
        onWindowVisibilityChanged(visibility);
    }

    /* access modifiers changed from: private */
    public void rotateAd() {
        if (!this.hasWindow) {
            this.isScheduled = false;
            return;
        }
        if (Extra2.verboselog) {
            Log.i(AdWhirlUtil.ADWHIRL, "Rotating Ad");
        }
        this.nextRation = this.adWhirlManager.getRation();
        this.handler.post(new HandleAdRunnable(this));
    }

    /* access modifiers changed from: private */
    public void handleAd() {
        if (this.nextRation == null) {
            if (Extra2.verboselog) {
                Log.e(AdWhirlUtil.ADWHIRL, "nextRation is null!");
            }
            rotateThreadedDelayed();
            return;
        }
        try {
            String rationInfo = String.format("Showing ad:\n\tnid: %s\n\tname: %s\n\ttype: %d\n\tkey: %s\n\tkey2: %s", this.nextRation.nid, this.nextRation.name, Integer.valueOf(this.nextRation.type), this.nextRation.key, this.nextRation.key2);
            if (Extra2.verboselog) {
                Log.d(AdWhirlUtil.ADWHIRL, rationInfo);
            }
            AdWhirlAdapter.handle(this, this.nextRation);
        } catch (Throwable th) {
            Throwable t = th;
            if (Extra2.verboselog) {
                Log.w(AdWhirlUtil.ADWHIRL, "Caught an exception in adapter:", t);
            }
            rollover();
        }
    }

    public void rotateThreadedNow() {
        this.scheduler.schedule(new RotateAdRunnable(this), 0, TimeUnit.SECONDS);
    }

    public void rotateThreadedDelayed() {
        if (Extra2.verboselog) {
            Log.d(AdWhirlUtil.ADWHIRL, "Will call rotateAd() in " + this.extra.cycleTime + " seconds");
        }
        this.scheduler.schedule(new RotateAdRunnable(this), (long) this.extra.cycleTime, TimeUnit.SECONDS);
    }

    public void killAdsenseWebViewOnDestroy() {
        try {
            if (getChildAt(0) instanceof GoogleAdView) {
                if (Extra2.verboselog) {
                    Log.d(AdWhirlUtil.ADWHIRL, "ADSENSE WEBVIEW TO KILL");
                }
                GoogleAdView adView = (GoogleAdView) getChildAt(0);
                if (adView != null) {
                    for (int i = 0; i < adView.getChildCount(); i++) {
                        if (adView.getChildAt(i) instanceof ViewGroup) {
                            if (Extra2.verboselog) {
                                Log.d(AdWhirlUtil.ADWHIRL, "ViewGroup Inside Adsense View !");
                            }
                            ViewGroup vg = (ViewGroup) adView.getChildAt(i);
                            for (int j = 0; j < vg.getChildCount(); j++) {
                                if (vg.getChildAt(j) instanceof WebView) {
                                    if (Extra2.verboselog) {
                                        Log.e(AdWhirlUtil.ADWHIRL, "Adsense WEBVIEW ! => ON DESTROY !!!");
                                    }
                                    ((WebView) vg.getChildAt(j)).destroy();
                                }
                            }
                        }
                        if (adView.getChildAt(i) instanceof WebView) {
                            ((WebView) adView.getChildAt(i)).destroy();
                        }
                    }
                }
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (Extra2.verboselog) {
                Log.e(AdWhirlUtil.ADWHIRL, "Adsense WEBVIEW onDestroy Failed !!!");
            }
            e2.printStackTrace();
        }
    }

    public void pushSubView(ViewGroup subView) {
        if (Extra2.verboselog) {
            Log.d(AdWhirlUtil.ADWHIRL, "pushSubView Start");
        }
        RelativeLayout superView = this.superViewReference.get();
        if (Extra2.verboselog) {
            Log.d(AdWhirlUtil.ADWHIRL, "pushSubView superView Getted");
        }
        if (superView != null) {
            try {
                if (superView.getChildAt(0) instanceof GoogleAdView) {
                    if (Extra2.verboselog) {
                        Log.d(AdWhirlUtil.ADWHIRL, "ADSENSE WEBVIEW TO KILL");
                    }
                    GoogleAdView adView = (GoogleAdView) superView.getChildAt(0);
                    if (adView != null) {
                        for (int i = 0; i < adView.getChildCount(); i++) {
                            if (adView.getChildAt(i) instanceof ViewGroup) {
                                if (Extra2.verboselog) {
                                    Log.d(AdWhirlUtil.ADWHIRL, "ViewGroup Inside Adsense View !");
                                }
                                ViewGroup vg = (ViewGroup) adView.getChildAt(i);
                                for (int j = 0; j < vg.getChildCount(); j++) {
                                    if (vg.getChildAt(j) instanceof WebView) {
                                        if (Extra2.verboselog) {
                                            Log.e(AdWhirlUtil.ADWHIRL, "Adsense WEBVIEW ! => ON DESTROY !!!");
                                        }
                                        ((WebView) vg.getChildAt(j)).destroy();
                                    }
                                }
                            }
                            if (adView.getChildAt(i) instanceof WebView) {
                                ((WebView) adView.getChildAt(i)).destroy();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Exception e2 = e;
                if (Extra2.verboselog) {
                    Log.e(AdWhirlUtil.ADWHIRL, "Adsense WEBVIEW onDestroy Failed !!!");
                }
                e2.printStackTrace();
            }
            superView.removeAllViews();
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13);
            superView.addView(subView, layoutParams);
            if (Extra2.verboselog) {
                Log.d(AdWhirlUtil.ADWHIRL, "Added subview");
            }
            this.activeRation = this.nextRation;
            countImpression();
        }
    }

    public void rollover() {
        if (Extra2.verboselog) {
            Log.w(AdWhirlUtil.ADWHIRL, "ROLLOVER !");
        }
        this.nextRation = this.adWhirlManager.getRollover();
        this.handler.post(new HandleAdRunnable(this));
    }

    private void countImpression() {
        if (this.activeRation != null) {
            this.scheduler.schedule(new PingUrlRunnable(String.format(AdWhirlUtil.urlImpression, this.adWhirlManager.keyAdWhirl, this.activeRation.nid, Integer.valueOf(this.activeRation.type), this.adWhirlManager.deviceIDHash, this.adWhirlManager.localeString, Integer.valueOf((int) AdWhirlUtil.VERSION))), 0, TimeUnit.SECONDS);
        }
    }

    private void countClick() {
        if (this.activeRation != null) {
            this.scheduler.schedule(new PingUrlRunnable(String.format(AdWhirlUtil.urlClick, this.adWhirlManager.keyAdWhirl, this.activeRation.nid, Integer.valueOf(this.activeRation.type), this.adWhirlManager.deviceIDHash, this.adWhirlManager.localeString, Integer.valueOf((int) AdWhirlUtil.VERSION))), 0, TimeUnit.SECONDS);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                if (Extra2.verboselog) {
                    Log.d(AdWhirlUtil.ADWHIRL, "Intercepted ACTION_DOWN event");
                }
                if (this.activeRation != null) {
                    countClick();
                    if (this.activeRation.type != 9) {
                        if (this.activeRation.type == 17 && this.activeRation.key.toLowerCase().equals(AdWhirlUtil.KREACTIVE_KEY)) {
                            if (this.custom == null || this.custom.link == null) {
                                if (Extra2.verboselog) {
                                    Log.w(AdWhirlUtil.ADWHIRL, "In onInterceptTouchEvent(), but custom or custom.link is null");
                                    break;
                                }
                            } else {
                                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.custom.link));
                                ModCommon.callTracking(this.custom.trackingClick);
                                intent.addFlags(268435456);
                                try {
                                    if (this.activityReference != null) {
                                        Activity activity = this.activityReference.get();
                                        if (activity != null) {
                                            activity.startActivity(intent);
                                            break;
                                        } else {
                                            return false;
                                        }
                                    } else {
                                        return false;
                                    }
                                } catch (Exception e) {
                                    Exception e2 = e;
                                    if (Extra2.verboselog) {
                                        Log.w(AdWhirlUtil.ADWHIRL, "Could not handle click to " + this.custom.link, e2);
                                        break;
                                    }
                                }
                            }
                        }
                    } else if (this.custom != null && this.custom.link != null) {
                        Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(this.custom.link));
                        intent2.addFlags(268435456);
                        try {
                            if (this.activityReference != null) {
                                Activity activity2 = this.activityReference.get();
                                if (activity2 != null) {
                                    activity2.startActivity(intent2);
                                    break;
                                } else {
                                    return false;
                                }
                            } else {
                                return false;
                            }
                        } catch (Exception e3) {
                            Exception e4 = e3;
                            if (Extra2.verboselog) {
                                Log.w(AdWhirlUtil.ADWHIRL, "Could not handle click to " + this.custom.link, e4);
                                break;
                            }
                        }
                    } else {
                        Log.w(AdWhirlUtil.ADWHIRL, "In onInterceptTouchEvent(), but custom or custom.link is null");
                        break;
                    }
                }
                break;
        }
        return false;
    }

    public void setAdWhirlInterface(AdWhirlInterface i) {
        this.adWhirlInterface = i;
    }

    private static class InitRunnable implements Runnable {
        private WeakReference<AdWhirlLayout> adWhirlLayoutReference;
        private String keyAdWhirl;

        public InitRunnable(AdWhirlLayout adWhirlLayout, String keyAdWhirl2) {
            this.adWhirlLayoutReference = new WeakReference<>(adWhirlLayout);
            this.keyAdWhirl = keyAdWhirl2;
        }

        public void run() {
            Activity activity;
            AdWhirlLayout adWhirlLayout = this.adWhirlLayoutReference.get();
            if (adWhirlLayout != null && (activity = adWhirlLayout.activityReference.get()) != null) {
                if (adWhirlLayout.adWhirlManager == null) {
                    adWhirlLayout.adWhirlManager = new AdWhirlManager(new WeakReference(activity.getApplicationContext()), this.keyAdWhirl);
                }
                if (!adWhirlLayout.hasWindow) {
                    adWhirlLayout.isScheduled = false;
                    return;
                }
                adWhirlLayout.adWhirlManager.fetchConfig();
                adWhirlLayout.extra = adWhirlLayout.adWhirlManager.getExtra();
                if (adWhirlLayout.extra == null) {
                    adWhirlLayout.scheduler.schedule(this, 30, TimeUnit.SECONDS);
                } else {
                    adWhirlLayout.rotateAd();
                }
            }
        }
    }

    private static class HandleAdRunnable implements Runnable {
        private WeakReference<AdWhirlLayout> adWhirlLayoutReference;

        public HandleAdRunnable(AdWhirlLayout adWhirlLayout) {
            this.adWhirlLayoutReference = new WeakReference<>(adWhirlLayout);
        }

        public void run() {
            AdWhirlLayout adWhirlLayout = this.adWhirlLayoutReference.get();
            if (adWhirlLayout != null) {
                adWhirlLayout.handleAd();
            }
        }
    }

    public static class ViewAdRunnable implements Runnable {
        private WeakReference<AdWhirlLayout> adWhirlLayoutReference;
        private ViewGroup nextView;

        public ViewAdRunnable(AdWhirlLayout adWhirlLayout, ViewGroup nextView2) {
            this.adWhirlLayoutReference = new WeakReference<>(adWhirlLayout);
            this.nextView = nextView2;
        }

        public void run() {
            AdWhirlLayout adWhirlLayout = this.adWhirlLayoutReference.get();
            if (adWhirlLayout != null) {
                adWhirlLayout.pushSubView(this.nextView);
            }
        }
    }

    private static class RotateAdRunnable implements Runnable {
        private WeakReference<AdWhirlLayout> adWhirlLayoutReference;

        public RotateAdRunnable(AdWhirlLayout adWhirlLayout) {
            this.adWhirlLayoutReference = new WeakReference<>(adWhirlLayout);
        }

        public void run() {
            AdWhirlLayout adWhirlLayout = this.adWhirlLayoutReference.get();
            if (adWhirlLayout != null) {
                adWhirlLayout.rotateAd();
            }
        }
    }

    private static class PingUrlRunnable implements Runnable {
        private String url;

        public PingUrlRunnable(String url2) {
            this.url = url2;
        }

        public void run() {
            if (Extra2.verboselog) {
                Log.d(AdWhirlUtil.ADWHIRL, "Pinging URL: " + this.url);
            }
            try {
                new DefaultHttpClient().execute(new HttpGet(this.url));
            } catch (ClientProtocolException e) {
                if (Extra2.verboselog) {
                    Log.e(AdWhirlUtil.ADWHIRL, "Caught ClientProtocolException in PingUrlRunnable", e);
                }
            } catch (IOException e2) {
                if (Extra2.verboselog) {
                    Log.e(AdWhirlUtil.ADWHIRL, "Caught IOException in PingUrlRunnable", e2);
                }
            } catch (Exception e3) {
                if (Extra2.verboselog) {
                    Log.e(AdWhirlUtil.ADWHIRL, "Caught Exception in PingUrlRunnable NIX", e3);
                }
            }
        }
    }
}
