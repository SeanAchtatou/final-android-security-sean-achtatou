package com.adwhirl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class PrivateAccessor {
    public static Object invokePrivateMethod(Object o, String methodName, Object[] params) {
        Method[] methods = o.getClass().getDeclaredMethods();
        int i = 0;
        int n = methods.length;
        while (i < n) {
            if (methodName.equals(methods[i].getName())) {
                try {
                    methods[i].setAccessible(true);
                    return methods[i].invoke(o, params);
                } catch (IllegalAccessException | InvocationTargetException e) {
                }
            } else {
                i++;
            }
        }
        return null;
    }
}
