package com.nix.game.pinball.free.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.magmamobile.mmusia.MMUSIA;
import com.nix.game.pinball.free.AskForRateManager;
import com.nix.game.pinball.free.R;
import com.nix.game.pinball.free.classes.Common;
import com.nix.game.pinball.free.controls.HelpDialog;
import com.nix.game.pinball.free.controls.WhatsNewDialog;
import com.nix.game.pinball.free.managers.AdWhirlAdsLayout;
import com.nix.game.pinball.free.managers.DataManager;
import com.nix.game.pinball.free.managers.GoogleAnalytics;

public final class menu extends Activity implements View.OnClickListener {
    private AdWhirlAdsLayout ggads;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new MMUSIA().Init(this, getString(R.string.res_mmusia), "-Pinball");
        setVolumeControlStream(3);
        AskForRateManager.start(this);
        AskForRateManager.increment(this);
        setContentView((int) R.layout.frm_menu);
        this.ggads = (AdWhirlAdsLayout) findViewById(R.id.ggads);
        findViewById(R.id.btnNew).setOnClickListener(this);
        findViewById(R.id.btnHelp).setOnClickListener(this);
        findViewById(R.id.btnOptions).setOnClickListener(this);
        findViewById(R.id.btnOtherApps).setOnClickListener(this);
        GoogleAnalytics.start(this, "/Pinball-");
        if (DataManager.firsttime) {
            new HelpDialog(this).show();
        }
        new WhatsNewDialog(this).show(false);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, (int) R.string.res_mmusia_menu).setIcon((int) R.drawable.mmusiaicon);
        menu.add(0, 0, 0, (int) R.string.cmnExit).setIcon(17301580);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                finish();
                return true;
            case 1:
                MMUSIA.launch(this, 0);
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.ggads.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.ggads.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Common.deleteCache(this);
        GoogleAnalytics.stop();
        super.onDestroy();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnNew:
                DataManager.playSound(DataManager.SND_SELECT);
                GoogleAnalytics.trackAndDispatch("Select");
                startActivity(new Intent(this, select.class));
                return;
            case R.id.btnOptions:
                DataManager.playSound(DataManager.SND_SELECT);
                GoogleAnalytics.trackAndDispatch("Options");
                startActivity(new Intent(this, options.class));
                return;
            case R.id.btnHelp:
                DataManager.playSound(DataManager.SND_SELECT);
                GoogleAnalytics.trackAndDispatch("Help");
                new HelpDialog(this).show();
                return;
            case R.id.btnOtherApps:
                DataManager.playSound(DataManager.SND_SELECT);
                GoogleAnalytics.trackAndDispatch("Other");
                Common.showMoreGames(this);
                return;
            default:
                return;
        }
    }
}
