package com.nix.game.pinball.free.objects;

import com.nix.game.pinball.free.managers.ScriptManager;
import com.nix.game.pinball.free.math.Vec2;
import java.io.DataInputStream;
import java.io.IOException;

public final class Trigger extends PinballObject {
    private int OnTouchEvent;
    private boolean on;
    private int px;
    private int py;
    private int pz;
    private int radius;
    private int uplayer;

    public Trigger(DataInputStream dis) throws IOException {
        super(dis);
        this.px = dis.readShort();
        this.py = dis.readShort();
        this.radius = dis.readUnsignedByte();
        this.visible = (dis.readInt() & 1) != 0;
        this.uplayer = dis.readShort();
        this.OnTouchEvent = dis.readInt();
        this.pz = this.uplayer;
    }

    public void Process(Ball b, Vec2 c) {
        if (!this.visible || b.z != this.pz) {
            return;
        }
        if (!Intersect(b)) {
            this.on = false;
        } else if (!this.on) {
            this.on = true;
            if (this.OnTouchEvent != 0) {
                ScriptManager.call(this.OnTouchEvent, b);
            }
        }
    }

    public boolean Intersect(Ball b) {
        float cx = b.p.x - ((float) this.px);
        float cy = b.p.y - ((float) this.py);
        return ((float) Math.sqrt((double) ((cx * cx) + (cy * cy)))) <= ((float) this.radius);
    }

    public void update() {
    }
}
