package com.nix.game.pinball.free.objects;

import android.graphics.Canvas;
import android.graphics.Paint;
import com.nix.game.pinball.free.classes.Point;
import com.nix.game.pinball.free.math.Intersect;
import com.nix.game.pinball.free.math.Vec2;
import java.io.DataInputStream;
import java.io.IOException;

public final class Wall extends PinballObject {
    static Vec2 z = new Vec2();
    private int bbbottom = -9999;
    private int bbleft = 9999;
    private int bbright = -9999;
    private int bbtop = 9999;
    private Point[] p;
    private Paint paint;
    private int pz;
    private int uplayer;

    public Wall(DataInputStream dis) throws IOException {
        super(dis);
        this.visible = (dis.readInt() & 1) != 0;
        this.uplayer = dis.readShort();
        this.pz = this.uplayer;
        int readShort = dis.readShort();
        this.p = new Point[readShort];
        for (int i = 0; i < readShort; i++) {
            this.p[i] = new Point(dis);
            if (this.p[i].p.x > ((float) this.bbright)) {
                this.bbright = (int) this.p[i].p.x;
            }
            if (this.p[i].p.x < ((float) this.bbleft)) {
                this.bbleft = (int) this.p[i].p.x;
            }
            if (this.p[i].p.y > ((float) this.bbbottom)) {
                this.bbbottom = (int) this.p[i].p.y;
            }
            if (this.p[i].p.y < ((float) this.bbtop)) {
                this.bbtop = (int) this.p[i].p.y;
            }
        }
        this.paint = new Paint();
        this.paint.setColor(-16777216);
    }

    public void draw(Canvas c) {
        if (this.visible) {
            int n = this.p.length;
            for (int i = 0; i < n; i++) {
                Point u = this.p[i];
                Point v = this.p[(i + 1) % n];
                c.drawLine(u.p.x, u.p.y, v.p.x, v.p.y, this.paint);
            }
        }
    }

    public void Process(Ball b, Vec2 c) {
        if (b.z == this.pz && ((float) this.bbleft) < b.p.x + ((float) b.r) && b.p.x - ((float) b.r) < ((float) this.bbright) && ((float) this.bbtop) < b.p.y + ((float) b.r) && b.p.y - ((float) b.r) < ((float) this.bbbottom)) {
            int n = this.p.length;
            for (int i = 0; i < n; i++) {
                processBall(b, this.p[i], this.p[(i + 1) % n]);
            }
        }
    }

    private void processBall(Ball e, Point a, Point b) {
        if (Intersect.SegmentSegmentIntersect(e.q, e.p, a.p, b.p, z)) {
            Intersect.processCollision2(a, b, e, z);
        } else if (Intersect.CircleSegmentIntersect(e.p, (float) e.r, a.p, b.p, z)) {
            Intersect.processCollision2(a, b, e, z);
        }
    }
}
