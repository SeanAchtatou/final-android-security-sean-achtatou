package com.nix.game.pinball.free;

import android.content.Context;
import android.os.Build;
import android.preference.PreferenceManager;
import android.view.MotionEvent;

public abstract class TouchManager {
    private Context context;

    public abstract boolean isMultitouch();

    public abstract void onTouch(MotionEvent motionEvent);

    public Context getContext() {
        return this.context;
    }

    public TouchManager(Context context2) {
        this.context = context2;
    }

    public void setViewSize(int width, int height) {
    }

    public static final TouchManager getTouchManager(Context context2) {
        boolean umt = PreferenceManager.getDefaultSharedPreferences(context2).getBoolean("multitouch", true);
        if (!isMultiTouchAvailable() || !umt) {
            return new SingleTouchManager(context2);
        }
        return new MultiTouchManager(context2);
    }

    public static final boolean isMultiTouchAvailable() {
        return sdkVersion() > 7;
    }

    public static final int sdkVersion() {
        return Integer.valueOf(Build.VERSION.SDK).intValue();
    }
}
