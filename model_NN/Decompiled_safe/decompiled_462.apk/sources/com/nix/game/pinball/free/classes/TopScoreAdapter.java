package com.nix.game.pinball.free.classes;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.nix.game.pinball.free.R;
import java.util.ArrayList;

public final class TopScoreAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<HighScore> mItems;

    private final class ViewHolder {
        LinearLayout layout;
        TextView text01;
        TextView text02;
        TextView text03;
        TextView text04;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(TopScoreAdapter topScoreAdapter, ViewHolder viewHolder) {
            this();
        }
    }

    public TopScoreAdapter(Context context, ArrayList<HighScore> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mContext = context;
        this.mItems = data;
    }

    public int getCount() {
        return this.mItems.size() + 1;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public Object getItem(int position) {
        if (position > 0) {
            return this.mItems.get(position - 1);
        }
        return null;
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate((int) R.layout.inc_scoreitem, (ViewGroup) null);
            holder = new ViewHolder(this, null);
            holder.layout = (LinearLayout) convertView.findViewById(R.id.primary);
            holder.text01 = (TextView) convertView.findViewById(R.id.text1);
            holder.text02 = (TextView) convertView.findViewById(R.id.text2);
            holder.text03 = (TextView) convertView.findViewById(R.id.text3);
            holder.text04 = (TextView) convertView.findViewById(R.id.text4);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (position == 0) {
            Resources res = this.mContext.getResources();
            holder.text01.setText(res.getString(R.string.lstPos));
            holder.text02.setText(res.getString(R.string.lstScore));
            holder.text03.setText(res.getString(R.string.lstTable));
            holder.text04.setText(res.getString(R.string.lstName));
            holder.layout.setBackgroundColor(0);
        } else {
            HighScore item = this.mItems.get(position - 1);
            holder.text01.setText(String.valueOf(item.pos));
            holder.text02.setText(String.valueOf(item.score));
            holder.text03.setText(item.table);
            holder.text04.setText(item.name);
            holder.layout.setBackgroundColor(item.selected ? -16777088 : 0);
        }
        return convertView;
    }
}
