package com.nix.game.pinball.free.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.google.ads.AdSenseSpec;
import com.millennialmedia.android.MMAdView;
import com.nix.game.pinball.free.R;
import com.nix.game.pinball.free.TouchManager;
import com.nix.game.pinball.free.classes.HighScore;
import com.nix.game.pinball.free.controls.GameView;
import com.nix.game.pinball.free.managers.AdWhirlAdsLayout;
import com.nix.game.pinball.free.managers.DataManager;
import com.nix.game.pinball.free.managers.ScriptManager;
import com.nix.game.pinball.free.objects.Table;

public final class game extends Activity implements SensorEventListener, Handler.Callback {
    /* access modifiers changed from: private */
    public static SurfaceHolder holder;
    /* access modifiers changed from: private */
    public static boolean runDraw;
    /* access modifiers changed from: private */
    public static boolean runGame;
    /* access modifiers changed from: private */
    public static boolean running;
    /* access modifiers changed from: private */
    public static GameThread thread1;
    /* access modifiers changed from: private */
    public static DrawThread thread2;
    private AdWhirlAdsLayout ggads;
    /* access modifiers changed from: private */
    public Handler handler;
    private boolean noAds;
    /* access modifiers changed from: private */
    public View overlay;
    private GameView view;

    private void checkAds() {
        this.noAds = getWindowManager().getDefaultDisplay().getHeight() < 480;
        if (!this.noAds) {
            this.ggads = (AdWhirlAdsLayout) findViewById(R.id.ggads);
            this.ggads.setAdsenseAdType(AdSenseSpec.AdType.IMAGE);
            return;
        }
        findViewById(R.id.primarycontainer).setPadding(0, 0, 0, 0);
    }

    public void onCreate(Bundle savedInstanceState) {
        SharedPreferences pref;
        int showctrl;
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        setContentView((int) R.layout.frm_game);
        this.view = (GameView) findViewById(R.id.primary);
        holder = this.view.getHolder();
        this.handler = new Handler(this);
        this.view.setTouchManager(TouchManager.getTouchManager(this));
        if (this.view.getTouchManager().isMultitouch() && (showctrl = (pref = PreferenceManager.getDefaultSharedPreferences(this)).getInt("showctrl", 5)) > 0) {
            SharedPreferences.Editor ed = pref.edit();
            ed.putInt("showctrl", showctrl - 1);
            ed.commit();
            this.overlay = findViewById(R.id.overlay);
            this.overlay.setVisibility(0);
            this.handler.postDelayed(new Runnable() {
                public void run() {
                    game.this.overlay.setVisibility(8);
                }
            }, 5000);
        }
        checkAds();
        new Thread() {
            public void run() {
                DataManager.init(game.this);
                Table.load(game.this);
                game.running = true;
                game.thread1 = new GameThread(null);
                game.thread1.setPriority(10);
                game.thread1.start();
                game.thread2 = new DrawThread(null);
                game.thread2.setPriority(10);
                game.thread2.start();
                game.this.handler.sendEmptyMessage(0);
            }
        }.start();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.noAds) {
            this.ggads.onResume();
        }
        if (this.view != null) {
            this.view.bringToFront();
            this.view.requestFocus();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.noAds) {
            this.ggads.onPause();
        }
        Table.pause = true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        running = false;
        super.onDestroy();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return onKeyboardEvent(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return onKeyboardEvent(keyCode, event);
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        switch (event.sensor.getType()) {
            case 1:
                Table.accx = event.values[0];
                Table.accy = event.values[1];
                Table.accz = event.values[2];
                return;
            default:
                return;
        }
    }

    private static class DrawThread extends Thread {
        private static final int FRAMETICK = 41;
        private static long currTime;
        private static long nextTime;

        private DrawThread() {
        }

        /* synthetic */ DrawThread(DrawThread drawThread) {
            this();
        }

        public void run() {
            game.runDraw = true;
            while (game.running) {
                currTime = SystemClock.elapsedRealtime();
                if (currTime < nextTime) {
                    try {
                        Thread.sleep(nextTime - currTime);
                    } catch (InterruptedException e) {
                    }
                }
                nextTime = SystemClock.elapsedRealtime() + 41;
                Canvas c = game.holder.lockCanvas();
                if (c != null) {
                    try {
                        Table.draw(c);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    game.holder.unlockCanvasAndPost(c);
                }
            }
            Log.i("--App--", "End of drawing loop");
            game.runDraw = false;
            game.tryRelease();
        }
    }

    private static class GameThread extends Thread {
        private static final int FRAMETICK = 11;
        private static long currTime;
        private static long nextTime;

        private GameThread() {
        }

        /* synthetic */ GameThread(GameThread gameThread) {
            this();
        }

        public void run() {
            game.runGame = true;
            while (game.running) {
                currTime = SystemClock.elapsedRealtime();
                if (currTime < nextTime) {
                    try {
                        Thread.sleep(nextTime - currTime);
                    } catch (InterruptedException e) {
                    }
                }
                nextTime = SystemClock.elapsedRealtime() + 11;
                try {
                    Table.cycle();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            Log.i("--App--", "End of game loop");
            game.runGame = false;
            game.tryRelease();
        }
    }

    /* access modifiers changed from: private */
    public static void tryRelease() {
        if (!runDraw && !runGame) {
            Table.release();
        }
    }

    public boolean handleMessage(Message arg0) {
        switch (arg0.what) {
            case 0:
                findViewById(R.id.loading).setVisibility(8);
                return true;
            default:
                return true;
        }
    }

    public void fnDialogExit() {
        Table.pause = true;
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case -2:
                        Table.pause = false;
                        return;
                    case MMAdView.REFRESH_INTERVAL_OFF:
                        game.this.terminate();
                        return;
                    default:
                        return;
                }
            }
        };
        AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setPositiveButton((int) R.string.cmnYes, listener);
        ad.setNegativeButton((int) R.string.cmnNo, listener);
        ad.setIcon(17301659);
        ad.setTitle((int) R.string.cmnExit);
        ad.setCancelable(false);
        ad.show();
    }

    /* access modifiers changed from: private */
    public void QuitAndHighScore(boolean submit) {
        HighScore.Helper.addInfo(this, DataManager.nickname, Table.tablename, ScriptManager.getScore(), Table.playTime);
        if (submit) {
            HighScore.Helper.addHighScore(this, DataManager.nickname, Table.tablename, ScriptManager.getScore());
            Intent intent = new Intent(this, score.class);
            intent.putExtra("table", Table.tablename);
            startActivity(intent);
        }
        terminate();
    }

    public void fnDialogSendScore() {
        if (ScriptManager.getScore() <= 0) {
            QuitAndHighScore(false);
            return;
        }
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case -2:
                        game.this.QuitAndHighScore(false);
                        return;
                    case MMAdView.REFRESH_INTERVAL_OFF:
                        if (DataManager.nickname.equals("")) {
                            game.this.fnDialogSetNickname();
                            return;
                        } else {
                            game.this.QuitAndHighScore(true);
                            return;
                        }
                    default:
                        return;
                }
            }
        };
        AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setPositiveButton((int) R.string.cmnYes, listener);
        ad.setNegativeButton((int) R.string.cmnNo, listener);
        ad.setIcon(17301659);
        ad.setTitle((int) R.string.optHighScores);
        ad.setMessage(getResources().getString(R.string.sumSubmit) + "\n\n" + ScriptManager.getScore());
        ad.setCancelable(false);
        ad.show();
    }

    /* access modifiers changed from: private */
    public void fnDialogSetNickname() {
        final EditText edit = new EditText(this);
        edit.setHint((int) R.string.optNick);
        edit.setId(R.id.text1);
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case -2:
                        game.this.QuitAndHighScore(false);
                        return;
                    case MMAdView.REFRESH_INTERVAL_OFF:
                        String nickname = edit.getText().toString();
                        if (nickname.equals("")) {
                            game.this.fnDialogSetNickname();
                            Toast.makeText(game.this, (int) R.string.errEmpty, 1).show();
                            return;
                        }
                        DataManager.nickname = nickname;
                        DataManager.writeString(game.this, "nickname", nickname);
                        game.this.QuitAndHighScore(true);
                        return;
                    default:
                        return;
                }
            }
        };
        AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setPositiveButton((int) R.string.cmnOk, listener);
        ad.setNegativeButton((int) R.string.cmnCancel, listener);
        ad.setTitle((int) R.string.optNick);
        ad.setCancelable(false);
        ad.setView(edit);
        ad.show();
    }

    public boolean onKeyboardEvent(int keyCode, KeyEvent event) {
        if (!Table.isLoaded()) {
            return true;
        }
        switch (event.getAction()) {
            case 0:
                switch (keyCode) {
                    case 24:
                        return false;
                    case 25:
                        return false;
                    default:
                        if (Table.loose) {
                            DataManager.playSound(DataManager.SND_SELECT);
                            fnDialogSendScore();
                        } else {
                            if (keyCode == DataManager.keya) {
                                Table.keyb[0] = true;
                            }
                            if (keyCode == DataManager.keyb) {
                                Table.keyb[1] = true;
                            }
                            if (keyCode == DataManager.keyc) {
                                Table.keyb[2] = true;
                            }
                            if (keyCode == DataManager.keyd) {
                                fnDialogExit();
                            }
                            if (keyCode == DataManager.keye) {
                                Table.pause = !Table.pause;
                            }
                        }
                        return true;
                }
            case 1:
                switch (keyCode) {
                    case 24:
                        return false;
                    case 25:
                        return false;
                    default:
                        if (keyCode == DataManager.keya) {
                            Table.keyb[0] = false;
                        }
                        if (keyCode == DataManager.keyb) {
                            Table.keyb[1] = false;
                        }
                        if (keyCode == DataManager.keyc) {
                            Table.keyb[2] = false;
                        }
                        return true;
                }
            default:
                return true;
        }
    }

    /* access modifiers changed from: private */
    public void terminate() {
        finish();
    }
}
