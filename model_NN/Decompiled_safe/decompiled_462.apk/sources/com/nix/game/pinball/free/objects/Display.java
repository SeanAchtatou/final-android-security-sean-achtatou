package com.nix.game.pinball.free.objects;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public final class Display extends PinballObject {
    private ByteBuffer bf = ByteBuffer.wrap(this.bff);
    private byte[] bff = new byte[((this.width * this.height) * 2)];
    private Canvas cnv;
    private Bitmap disp = Bitmap.createBitmap(this.width, this.height, Bitmap.Config.ARGB_4444);
    private int display;
    private int height;
    private Image img;
    private Rect rdst;
    private int ref1;
    private Rect rscr;
    private int width;
    private int x;
    private int y;

    public Display(DataInputStream dis) throws IOException {
        super(dis);
        this.x = dis.readShort();
        this.y = dis.readShort();
        this.width = dis.readShort();
        this.height = dis.readShort();
        this.display = dis.readUnsignedByte();
        this.ref1 = dis.readInt();
        this.disp.copyPixelsFromBuffer(this.bf);
        this.cnv = new Canvas(this.disp);
        this.rscr = new Rect();
        this.rdst = new Rect();
    }

    public void draw(Canvas c) {
        if (this.visible && this.img != null) {
            char[] base = null;
            switch (this.display) {
                case 0:
                    base = Table.bffscore;
                    break;
                case 1:
                    base = Table.bffbonus;
                    break;
                case 2:
                    base = Table.bfffactor;
                    break;
                case 3:
                    base = Table.bffball;
                    break;
            }
            this.disp.copyPixelsFromBuffer(this.bf);
            if (base != null) {
                int ix = this.width - ((8 + 1) * 12);
                for (int i = 0; i < 8; i++) {
                    char b = base[i];
                    ix += 12;
                    if (b > 0) {
                        char b2 = (char) (b - '0');
                        this.rscr.set(b2 * 12, 0, (b2 * 12) + 12, 20);
                        this.rdst.set(ix, 0, ix + 12, 20);
                        this.cnv.drawBitmap(this.img.image, this.rscr, this.rdst, (Paint) null);
                    }
                }
            }
            c.drawBitmap(this.disp, (float) this.x, (float) this.y, (Paint) null);
        }
    }

    public void update() {
        this.img = (Image) Table.omap.get(Integer.valueOf(this.ref1));
    }
}
