package com.nix.game.pinball.free.classes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public final class TableInfo {
    public static final int TABLE_MAGIC = -31672;
    private static final byte TABLE_ON_ASSET = 2;
    private static final byte TABLE_ON_FILE = 1;
    private static final byte TABLE_ON_RES = 0;
    public static final int TABLE_VERSION = 257;
    private static final String[] tablemap = {"tbl/sdbowling.pn1", "tbl/sdunderwater1.pn1", "tbl/sdsoccer1.pn1", "tbl/sdinvaders.pn1", "tbl/sdandroid.pn1", "tbl/sdsixties.pn1", "tbl/sdxmas.pn1"};
    private String mAsset;
    private Context mContext;
    private File mFile;
    private boolean mLoaded;
    private String mName;
    private Bitmap mPreview;
    private int mResID;
    private int mType = 0;

    public TableInfo(Context context, int resid) {
        this.mResID = resid;
        this.mContext = context;
    }

    public TableInfo(Context context, String asset) {
        this.mAsset = asset;
        this.mContext = context;
    }

    public TableInfo(Context context, File file) {
        this.mFile = file;
        this.mContext = context;
    }

    public boolean isOnSDCard() {
        return this.mType == 1;
    }

    public String getName() {
        if (!this.mLoaded) {
            load();
        }
        return this.mName;
    }

    public Bitmap getPreview() {
        if (!this.mLoaded) {
            load();
        }
        return this.mPreview;
    }

    public InputStream openStream() throws IOException {
        switch (this.mType) {
            case 0:
                return this.mContext.getResources().openRawResource(this.mResID);
            case 1:
                return new FileInputStream(this.mFile);
            case 2:
                return this.mContext.getAssets().open(this.mAsset);
            default:
                return null;
        }
    }

    private void load() {
        this.mName = "No name";
        this.mPreview = null;
        try {
            this.mLoaded = true;
            InputStream is = openStream();
            DataInputStream dis = new DataInputStream(is);
            short magic = dis.readShort();
            if (dis.readShort() == 257 && magic == -31672) {
                this.mName = Common.readString(dis);
                int size = dis.readInt();
                if (size > 0) {
                    byte[] b = new byte[size];
                    dis.read(b);
                    this.mPreview = BitmapFactory.decodeByteArray(b, 0, b.length);
                }
            }
            dis.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<TableInfo> getTableInfo(Context context) {
        File[] files;
        ArrayList<TableInfo> retval = new ArrayList<>();
        for (String tableInfo : tablemap) {
            retval.add(new TableInfo(context, tableInfo));
        }
        File dir = new File("/sdcard/pinball/tables");
        if (dir.exists() && (files = dir.listFiles()) != null) {
            for (File tableInfo2 : files) {
                retval.add(new TableInfo(context, tableInfo2));
            }
        }
        return retval;
    }
}
