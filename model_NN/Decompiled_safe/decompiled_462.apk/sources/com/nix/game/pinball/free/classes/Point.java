package com.nix.game.pinball.free.classes;

import com.nix.game.pinball.free.math.Vec2;
import java.io.DataInputStream;
import java.io.IOException;

public final class Point {
    public int OnTouchEvent;
    public float bounce;
    public boolean hasEvent;
    public Vec2 m = new Vec2();
    public Vec2 n = new Vec2();
    public Vec2 p = new Vec2();

    public Point(DataInputStream dis) throws IOException {
        this.p.x = (float) dis.readShort();
        this.p.y = (float) dis.readShort();
        this.m.x = (float) dis.readShort();
        this.m.y = (float) dis.readShort();
        this.n.x = dis.readFloat();
        this.n.y = dis.readFloat();
        this.bounce = dis.readFloat();
        this.OnTouchEvent = dis.readInt();
    }
}
