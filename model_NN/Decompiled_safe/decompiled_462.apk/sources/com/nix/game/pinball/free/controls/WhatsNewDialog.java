package com.nix.game.pinball.free.controls;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.millennialmedia.android.MMAdView;
import com.nix.game.pinball.free.R;
import com.nix.game.pinball.free.classes.Common;
import java.io.IOException;
import java.io.InputStream;

public final class WhatsNewDialog implements DialogInterface.OnClickListener {
    private Context mContext;

    public WhatsNewDialog(Context context) {
        this.mContext = context;
    }

    public void show(boolean force) {
        if (!Common.getAppVersion(this.mContext).equals(PreferenceManager.getDefaultSharedPreferences(this.mContext).getString("lastversion", "")) || force) {
            AlertDialog.Builder ad = new AlertDialog.Builder(this.mContext);
            View vw = LayoutInflater.from(this.mContext).inflate((int) R.layout.dlg_changelog, (ViewGroup) null);
            ((TextView) vw.findViewById(16908308)).setText(getText(this.mContext, R.raw.log));
            ad.setIcon(17301659);
            ad.setPositiveButton((int) R.string.cmnClose, this);
            ad.setTitle((int) R.string.sumWhatsNew);
            ad.setCancelable(false);
            ad.setView(vw);
            ad.show();
        }
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case MMAdView.REFRESH_INTERVAL_OFF:
                SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.mContext).edit();
                edit.putString("lastversion", Common.getAppVersion(this.mContext));
                edit.commit();
                return;
            default:
                return;
        }
    }

    private static String getText(Context context, int res) {
        try {
            InputStream is = context.getResources().openRawResource(res);
            byte[] dt = new byte[is.available()];
            is.read(dt);
            is.close();
            return new String(dt, "utf-8");
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }
}
