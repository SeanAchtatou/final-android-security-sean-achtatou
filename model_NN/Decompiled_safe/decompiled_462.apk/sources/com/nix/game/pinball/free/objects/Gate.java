package com.nix.game.pinball.free.objects;

import android.graphics.Canvas;
import android.graphics.Paint;
import com.nix.game.pinball.free.managers.ScriptManager;
import com.nix.game.pinball.free.math.Vec2;
import java.io.DataInputStream;
import java.io.IOException;

public final class Gate extends PinballObject {
    private int OnTouchEvent;
    private float ax = (this.p2x - this.p1x);
    private float ay = (this.p2y - this.p1y);
    private float d;
    private float lsqr;
    private float nx = this.ay;
    private float ny = (-this.ax);
    private boolean on;
    private float p1x;
    private float p1y;
    private float p2x;
    private float p2y;
    private Paint paint;
    private int uplayer;

    public Gate(DataInputStream dis) throws IOException {
        super(dis);
        this.p1x = (float) dis.readShort();
        this.p1y = (float) dis.readShort();
        this.p2x = (float) dis.readShort();
        this.p2y = (float) dis.readShort();
        float s = (float) Math.sqrt((double) ((this.nx * this.nx) + (this.ny * this.ny)));
        if (s != 0.0f) {
            this.nx /= s;
            this.ny /= s;
        } else {
            this.nx = 0.0f;
            this.ny = 0.0f;
        }
        this.d = (this.p1x * this.nx) + (this.p1y * this.ny);
        this.lsqr = (this.ax * this.ax) + (this.ay * this.ay);
        dis.readByte();
        this.visible = (dis.readInt() & 1) != 0;
        this.uplayer = dis.readShort();
        this.OnTouchEvent = dis.readInt();
        this.paint = new Paint();
        this.paint.setColor(-16777216);
    }

    public void draw(Canvas c) {
        if (this.visible) {
            c.drawLine(this.p1x, this.p1y, this.p2x, this.p2y, this.paint);
        }
    }

    public void Process(Ball b, Vec2 c) {
        if (this.uplayer > 0) {
            ProcessUplayer(b, c);
        } else if (b.z == 0) {
            ProcessNormal(b, c);
        }
    }

    private void ProcessUplayer(Ball b, Vec2 c) {
        float t = ((b.p.x * this.nx) + (b.p.y * this.ny)) - this.d;
        if (Math.abs(t) <= ((float) (b.r * 2))) {
            float r0 = ((this.ax * (b.p.x - this.p1x)) + (this.ay * (b.p.y - this.p1y))) / this.lsqr;
            if (t > 0.0f) {
                if (r0 > 0.0f && r0 < 1.0f) {
                    b.z = 0;
                }
            } else if (r0 > 0.0f && r0 < 1.0f) {
                b.z = this.uplayer;
            }
        }
    }

    private void ProcessNormal(Ball b, Vec2 c) {
        boolean r1;
        float dx = b.p.x - b.q.x;
        float dy = b.p.y - b.q.y;
        if ((this.ax * dy) - (this.ay * dx) != 0.0f) {
            float s1 = (((this.p1y - b.q.y) * dx) + ((b.q.x - this.p1x) * dy)) / ((this.ax * dy) - (this.ay * dx));
            float t1 = ((this.ax * (b.q.y - this.p1y)) + (this.ay * (this.p1x - b.q.x))) / ((this.ay * dx) - (this.ax * dy));
            if (s1 < 0.0f || s1 > 1.0f || t1 < 0.0f || t1 > 1.0f) {
                r1 = false;
            } else {
                r1 = true;
            }
            if (r1 && ((b.q.x * this.nx) + (b.q.y * this.ny)) - this.d > 0.0f) {
                c.x = this.p1x + (this.ax * s1);
                c.y = this.p1y + (this.ay * s1);
                b.p.x = (this.nx * ((float) b.r)) + c.x;
                b.p.y = (this.ny * ((float) b.r)) + c.y;
                b.v.x += this.nx;
                b.v.y += this.ny;
                b.bind();
                return;
            }
        }
        float t = ((b.p.x * this.nx) + (b.p.y * this.ny)) - this.d;
        if (Math.abs(t) > ((float) b.r)) {
            this.on = false;
        } else if (t <= 0.0f) {
            this.on = true;
        } else if (!this.on) {
            float r0 = ((this.ax * (b.p.x - this.p1x)) + (this.ay * (b.p.y - this.p1y))) / this.lsqr;
            if (r0 > 0.0f && r0 < 1.0f) {
                c.x = this.p1x + (this.ax * r0);
                c.y = this.p1y + (this.ay * r0);
                b.p.x = (this.nx * ((float) b.r)) + c.x;
                b.p.y = (this.ny * ((float) b.r)) + c.y;
                b.v.x += this.nx;
                b.v.y += this.ny;
                b.bind();
                if (this.OnTouchEvent != 0) {
                    ScriptManager.call(this.OnTouchEvent, b);
                }
            }
        }
    }

    public void update() {
    }
}
