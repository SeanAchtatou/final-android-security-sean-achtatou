package com.nix.game.pinball.free;

import android.content.Context;
import android.view.MotionEvent;
import com.nix.game.pinball.free.activities.game;
import com.nix.game.pinball.free.managers.DataManager;
import com.nix.game.pinball.free.objects.Table;

public final class MultiTouchManager extends TouchManager {
    private static final int MAX_POINT = 10;
    private int mheight;
    private int mwidth;
    private boolean[] tPtr = new boolean[10];
    private boolean touch1;
    private boolean touch2;
    private boolean touch3;
    private float[] xPtr = new float[10];
    private float[] yPtr = new float[10];

    public MultiTouchManager(Context context) {
        super(context);
    }

    public void setViewSize(int width, int height) {
        this.mwidth = (int) (((float) width) * 0.5f);
        this.mheight = (int) (((float) height) * 0.7f);
    }

    public void onTouch(MotionEvent event) {
        int ac = event.getAction() & 255;
        int id = event.getPointerId((event.getAction() & 65280) >>> 8);
        if (id <= event.getPointerCount() - 1 && id >= 0 && id >= 0 && id < 10) {
            switch (ac) {
                case 0:
                    this.tPtr[id] = true;
                    this.xPtr[id] = event.getX(id);
                    this.yPtr[id] = event.getY(id);
                    setTouch(id, true);
                    return;
                case 1:
                    this.tPtr[id] = false;
                    setTouch(id, false);
                    return;
                case 2:
                case 3:
                case 4:
                default:
                    return;
                case 5:
                    this.tPtr[id] = true;
                    this.xPtr[id] = event.getX(id);
                    this.yPtr[id] = event.getY(id);
                    setTouch(id, true);
                    return;
                case 6:
                    this.tPtr[id] = false;
                    setTouch(id, false);
                    return;
            }
        }
    }

    private int getTouchId(float x, float y) {
        if (y < ((float) this.mheight)) {
            return 3;
        }
        if (x < ((float) this.mwidth)) {
            return 1;
        }
        return 2;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    private void setTouch(int i, boolean v) {
        if (Table.loose) {
            DataManager.playSound(DataManager.SND_SELECT);
            ((game) getContext()).fnDialogSendScore();
            return;
        }
        switch (getTouchId(this.xPtr[i], this.yPtr[i])) {
            case 1:
                if (this.touch1 != v) {
                    Table.keyb[0] = v;
                    this.touch1 = v;
                    return;
                }
                return;
            case 2:
                if (this.touch2 != v) {
                    Table.keyb[1] = v;
                    this.touch2 = v;
                    return;
                }
                return;
            case 3:
                if (this.touch3 != v) {
                    Table.keyb[2] = v;
                    this.touch3 = v;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public boolean isMultitouch() {
        return true;
    }
}
