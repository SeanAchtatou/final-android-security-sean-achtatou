package com.nix.game.pinball.free.objects;

import android.graphics.Canvas;
import android.graphics.Paint;
import com.nix.game.pinball.free.managers.ScriptManager;
import com.nix.game.pinball.free.math.Vec2;
import java.io.DataInputStream;
import java.io.IOException;

public final class Kicker extends PinballObject {
    private int OnTouchEvent;
    private Image image;
    private boolean on;
    private float px;
    private float py;
    private int pz;
    private int radius;
    private int ref1;
    private int uplayer;

    public Kicker(DataInputStream dis) throws IOException {
        super(dis);
        this.px = (float) dis.readShort();
        this.py = (float) dis.readShort();
        this.radius = dis.readUnsignedByte();
        this.visible = (dis.readInt() & 1) != 0;
        this.uplayer = dis.readShort();
        this.ref1 = dis.readInt();
        this.OnTouchEvent = dis.readInt();
        this.pz = this.uplayer;
    }

    public void Process(Ball b, Vec2 c) {
        if (!this.visible || b.z != this.pz) {
            return;
        }
        if (!Intersect(b)) {
            this.on = false;
        } else if (!this.on) {
            this.on = true;
            b.StopAtXY(this.px, this.py);
            if (this.OnTouchEvent != 0) {
                ScriptManager.call(this.OnTouchEvent, b);
            }
        }
    }

    private boolean Intersect(Ball b) {
        float cx = b.p.x - this.px;
        float cy = b.p.y - this.py;
        return ((float) Math.sqrt((double) ((cx * cx) + (cy * cy)))) <= ((float) this.radius);
    }

    public void draw(Canvas c) {
        if (this.visible && this.image != null) {
            c.drawBitmap(this.image.image, this.px - ((float) this.image.x), this.py - ((float) this.image.y), (Paint) null);
        }
    }

    public void update() {
        this.image = (Image) Table.omap.get(Integer.valueOf(this.ref1));
    }
}
