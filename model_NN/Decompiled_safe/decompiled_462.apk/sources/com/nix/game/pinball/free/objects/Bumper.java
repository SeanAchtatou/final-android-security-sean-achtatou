package com.nix.game.pinball.free.objects;

import android.graphics.Canvas;
import android.graphics.Paint;
import com.nix.game.pinball.free.managers.ScriptManager;
import com.nix.game.pinball.free.math.Vec2;
import java.io.DataInputStream;
import java.io.IOException;

public class Bumper extends PinballObject {
    private int OnTouchEvent;
    private float bounce;
    private Image imgoff;
    private Image imgon;
    private short lite;
    public float px;
    public float py;
    public int pz;
    private int radius;
    private int ref1;
    private int ref2;
    private int uplayer;

    public Bumper(DataInputStream dis) throws IOException {
        super(dis);
        this.px = (float) dis.readShort();
        this.py = (float) dis.readShort();
        this.radius = dis.readByte();
        this.bounce = dis.readFloat();
        this.visible = (dis.readInt() & 1) != 0;
        this.uplayer = dis.readShort();
        this.ref1 = dis.readInt();
        this.ref2 = dis.readInt();
        this.OnTouchEvent = dis.readInt();
        this.pz = this.uplayer;
    }

    public void Process(Ball b, Vec2 c) {
        if (this.visible && b.z == this.pz) {
            float dx = b.p.x - this.px;
            float dy = b.p.y - this.py;
            float s0 = (float) Math.sqrt((double) ((dx * dx) + (dy * dy)));
            if (s0 <= ((float) (b.r + this.radius))) {
                float nx = 0.0f;
                float ny = 0.0f;
                if (s0 != 0.0f) {
                    nx = dx / s0;
                    ny = dy / s0;
                }
                b.v.x = this.bounce * nx;
                b.v.y = this.bounce * ny;
                b.p.x = this.px + (((float) (b.r + this.radius)) * nx);
                b.p.y = this.py + (((float) (b.r + this.radius)) * ny);
                b.q.x = b.p.x;
                b.q.y = b.p.y;
                this.lite = 20;
                if (this.OnTouchEvent != 0) {
                    ScriptManager.call(this.OnTouchEvent, b);
                }
            }
        }
    }

    public void draw(Canvas c) {
        if (this.visible) {
            Image image = this.lite > 0 ? this.imgon : this.imgoff;
            if (image != null) {
                c.drawBitmap(image.image, this.px - ((float) image.x), this.py - ((float) image.y), (Paint) null);
            }
        }
    }

    public void cycle() {
        if (this.lite > 0) {
            this.lite = (short) (this.lite - 1);
        }
    }

    public void update() {
        this.imgon = (Image) Table.omap.get(Integer.valueOf(this.ref1));
        this.imgoff = (Image) Table.omap.get(Integer.valueOf(this.ref2));
    }

    public void moveby(int bx, int by) {
        this.px += (float) bx;
        this.py += (float) by;
    }
}
