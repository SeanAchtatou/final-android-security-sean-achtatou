package com.nix.game.pinball.free.activities;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceActivity;
import com.nix.game.pinball.free.R;
import com.nix.game.pinball.free.TouchManager;
import com.nix.game.pinball.free.managers.DataManager;

public final class options extends PreferenceActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        addPreferencesFromResource(R.xml.options);
        if (!TouchManager.isMultiTouchAvailable()) {
            CheckBoxPreference cb = (CheckBoxPreference) findPreference("multitouch");
            cb.setEnabled(false);
            cb.setChecked(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        DataManager.writeBoolean(this, "firsttime", false);
        DataManager.update(this);
        super.onStop();
    }
}
