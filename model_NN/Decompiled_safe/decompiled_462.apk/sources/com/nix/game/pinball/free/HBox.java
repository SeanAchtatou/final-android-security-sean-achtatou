package com.nix.game.pinball.free;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public class HBox extends ViewGroup {
    public HBox(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HBox(Context context) {
        this(context, null);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = widthMeasureSpec;
        int height = 0;
        int n = getChildCount();
        for (int i = 0; i < n; i++) {
            View c = getChildAt(i);
            c.measure(width, -1);
            int h = c.getMeasuredHeight();
            if (h > height) {
                height = h;
            }
        }
        int width2 = Math.max(width, getSuggestedMinimumWidth());
        setMeasuredDimension(getDefaultSize(width2, widthMeasureSpec), getDefaultSize(Math.max(height, getSuggestedMinimumHeight()), heightMeasureSpec));
        int w = View.MeasureSpec.makeMeasureSpec(getDefaultSize(width2, widthMeasureSpec) / getChildCount(), 1073741824);
        int n2 = getChildCount();
        for (int i2 = 0; i2 < n2; i2++) {
            getChildAt(i2).measure(w, -1);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int l, int t, int r, int b) {
        int count = getChildCount();
        int width = getMeasuredWidth() / count;
        int height = getMeasuredHeight();
        int left = 0;
        for (int i = 0; i < count; i++) {
            int w = width;
            int h = height;
            int lf = (width - w) / 2;
            int lt = (height - h) / 2;
            getChildAt(i).layout(left + lf, lt, left + lf + w, lt + h);
            left += width;
        }
    }
}
