package com.nix.game.pinball.free.controls;

import android.app.Activity;
import android.content.Context;
import android.preference.Preference;
import android.util.AttributeSet;
import com.nix.game.pinball.free.R;
import com.nix.game.pinball.free.classes.Common;

public final class MarketPreference extends Preference {
    private boolean mShowUpdate;

    public MarketPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mShowUpdate = context.obtainStyledAttributes(attrs, R.styleable.MarketPreference).getBoolean(0, false);
    }

    public MarketPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    /* access modifiers changed from: protected */
    public void onClick() {
        if (this.mShowUpdate) {
            Common.launchMarket(getContext(), this.mShowUpdate);
        } else {
            Common.showMoreGames((Activity) getContext());
        }
    }
}
