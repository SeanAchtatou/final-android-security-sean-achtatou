package com.nix.game.pinball.free.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.SparseArray;
import com.nix.game.pinball.free.classes.Common;
import com.nix.game.pinball.free.classes.TableInfo;
import java.io.IOException;

public final class DataManager {
    public static final int SND_BONUS = 1454095241;
    public static final int SND_BOWLING = -1898744640;
    public static final int SND_BUMPER1 = 1902404289;
    public static final int SND_BUMPER2 = -395496581;
    public static final int SND_BUMPER3 = -1620442131;
    public static final int SND_EXTRA = -2079536746;
    public static final int SND_EXTRABALL = 872895449;
    public static final int SND_FLIPPER = -1530313114;
    public static final int SND_GOAL = -1019699619;
    public static final int SND_HOHOHO = -1488284931;
    public static final int SND_HOLE = 1462323426;
    public static final int SND_LOOSE = 1255636232;
    public static final int SND_PLUNGER = -1401421752;
    public static final int SND_SELECT = 1871674018;
    public static final int SND_SLINGSHOT = 826563933;
    public static String deviceid;
    public static boolean firsttime;
    public static int keya;
    public static int keyb;
    public static int keyc;
    public static int keyd;
    public static int keye;
    public static int language;
    public static String nickname;
    public static TableInfo selectedTable;
    private static final Object[] soundAssetsMap = {"sndbumper1.ogg", Integer.valueOf((int) SND_BUMPER1), "sndbumper2.ogg", Integer.valueOf((int) SND_BUMPER2), "sndbumper3.ogg", Integer.valueOf((int) SND_BUMPER3), "sndflipper.ogg", Integer.valueOf((int) SND_FLIPPER), "sndselect.ogg", Integer.valueOf((int) SND_SELECT), "sndplunger.ogg", Integer.valueOf((int) SND_PLUNGER), "sndslingshot.ogg", Integer.valueOf((int) SND_SLINGSHOT), "sndhole.ogg", Integer.valueOf((int) SND_HOLE), "sndbonus.ogg", Integer.valueOf((int) SND_BONUS), "sndextraball.ogg", Integer.valueOf((int) SND_EXTRABALL), "sndloose.ogg", Integer.valueOf((int) SND_LOOSE), "sndextra.ogg", Integer.valueOf((int) SND_EXTRA), "sndgoal.ogg", Integer.valueOf((int) SND_GOAL), "sndbowling.ogg", Integer.valueOf((int) SND_BOWLING), "sndhohoho.ogg", Integer.valueOf((int) SND_HOHOHO)};
    private static boolean soundEnabled;
    /* access modifiers changed from: private */
    public static AudioManager soundManager;
    private static SoundPool soundPool;
    private static int soundVolume;
    private static SparseArray<Integer> sounds;
    public static int tableindex;
    public static boolean vibrate;
    /* access modifiers changed from: private */
    public static Vibrator vibrator;

    public static final void init(final Context context) {
        new Thread() {
            public void run() {
                DataManager.deviceid = Common.getDeviceID(context);
                DataManager.vibrator = (Vibrator) context.getSystemService("vibrator");
                DataManager.soundManager = (AudioManager) context.getSystemService("audio");
                DataManager.loadSoundInfo(context);
                DataManager.language = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString("locale", "-1"));
                DataManager.update(context);
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public static final void loadSoundInfo(Context context) {
        soundPool = new SoundPool(4, 3, 100);
        sounds = new SparseArray<>();
        int n = soundAssetsMap.length;
        for (int i = 0; i < n; i += 2) {
            loadSound(context, (String) soundAssetsMap[i + 0], ((Integer) soundAssetsMap[i + 1]).intValue());
        }
    }

    private static final void loadSound(Context context, String filename, int key) {
        try {
            AssetFileDescriptor af = context.getAssets().openFd("sfx/".concat(filename));
            int handle = soundPool.load(af, 1);
            af.close();
            sounds.put(key, Integer.valueOf(handle));
        } catch (IOException e) {
            throw new RuntimeException("SFX file not found : ".concat(filename));
        }
    }

    public static final void playSound(int sound) {
        Integer sid;
        if (soundEnabled && sounds != null && (sid = sounds.get(sound)) != null) {
            soundPool.play(sid.intValue(), (float) soundVolume, (float) soundVolume, 1, 0, 1.0f);
        }
    }

    public static final void vibrate(long milliseconds) {
        if (vibrate && vibrator != null) {
            vibrator.vibrate(milliseconds);
        }
    }

    public static final void update(Context context) {
        SharedPreferences options = PreferenceManager.getDefaultSharedPreferences(context);
        firsttime = options.getBoolean("firsttime", true);
        soundEnabled = options.getBoolean("sound", true);
        soundManager = (AudioManager) context.getSystemService("audio");
        soundVolume = soundManager.getStreamVolume(3);
        vibrate = options.getBoolean("vibrate", false);
        nickname = options.getString("nickname", "");
        keya = options.getInt("keya", 0);
        keyb = options.getInt("keyb", 0);
        keyc = options.getInt("keyc", 0);
        keyd = options.getInt("keyd", 4);
        keye = options.getInt("keye", 82);
    }

    public static final int getVolume() {
        return soundVolume;
    }

    public static final void setVolume(int value) {
        if (soundManager != null) {
            if (value < 0) {
                value = 0;
            }
            if (value > 15) {
                value = 15;
            }
            soundVolume = value;
            soundManager.setStreamVolume(3, value, 0);
        }
    }

    public static final void writeString(Context context, String name, String value) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(name, value);
        editor.commit();
    }

    public static final void writeBoolean(Context context, String name, boolean value) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putBoolean(name, value);
        editor.commit();
    }
}
