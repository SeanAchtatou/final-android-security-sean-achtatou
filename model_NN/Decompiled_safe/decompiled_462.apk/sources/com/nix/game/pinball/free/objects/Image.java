package com.nix.game.pinball.free.objects;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.DataInputStream;
import java.io.IOException;

public final class Image extends PinballObject {
    public Bitmap image;
    public int x;
    public int y;

    public Image(DataInputStream dis, BitmapFactory.Options options) throws IOException {
        super(dis);
        this.x = dis.readShort();
        this.y = dis.readShort();
        int size = dis.readInt();
        byte[] b = new byte[size];
        dis.read(b, 0, b.length);
        this.image = BitmapFactory.decodeByteArray(b, 0, size, options);
    }
}
