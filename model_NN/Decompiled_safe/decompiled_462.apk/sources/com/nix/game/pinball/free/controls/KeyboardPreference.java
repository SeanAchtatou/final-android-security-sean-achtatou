package com.nix.game.pinball.free.controls;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import com.adwhirl.util.AdWhirlUtil;
import com.google.ads.util.Base64;
import com.nix.game.pinball.free.R;

public final class KeyboardPreference extends DialogPreference implements DialogInterface.OnKeyListener, View.OnTouchListener {
    private static String[] keynames = {"TOUCHSCREEN", "SOFT_LEFT", "SOFT_RIGHT", "HOME", "BACK", "CALL", "ENDCALL", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "STAR", "POUND", "DPAD_UP", "DPAD_DOWN", "DPAD_LEFT", "DPAD_RIGHT", "DPAD_CENTER", "VOLUME_UP", "VOLUME_DOWN", "POWER", "CAMERA", "CLEAR", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "COMMA", "PERIOD", "ALT_LEFT", "ALT_RIGHT", "SHIFT_LEFT", "SHIFT_RIGHT", "TAB", "SPACE", "SYM", "EXPLORER", "ENVELOPE", "ENTER", "DEL", "GRAVE", "MINUS", "EQUALS", "LEFT_BRACKET", "RIGHT_BRACKET", "BACKSLASH", "SEMICOLON", "APOSTROPHE", "SLASH", "AT", "NUM", "HEADSETHOOK", "FOCUS", "PLUS", "MENU", "NOTIFICATION", "SEARCH"};
    private int keycode;
    private TextView keyview;
    private int lastkeyCode;

    public KeyboardPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public KeyboardPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    /* access modifiers changed from: protected */
    public void onBindView(View view) {
        super.onBindView(view);
        TextView summaryView = (TextView) view.findViewById(16908304);
        if (summaryView != null) {
            String summary = (String) getSummary();
            if (summary != null) {
                summaryView.setText(summary.replace("%1", getkeyName(this.keycode)));
            } else {
                summaryView.setText(getkeyName(this.keycode));
            }
        }
    }

    /* access modifiers changed from: protected */
    public Object onGetDefaultValue(TypedArray a, int index) {
        return Integer.valueOf(a.getInt(index, 0));
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        int keyCode = restoreValue ? getPersistedInt(this.keycode) : ((Integer) defaultValue).intValue();
        this.lastkeyCode = keyCode;
        setKeyCode(keyCode);
    }

    public void setKeyCode(int keyCode) {
        this.keycode = keyCode;
        persistInt(keyCode);
        notifyDependencyChange(shouldDisableDependents());
        notifyChanged();
    }

    /* access modifiers changed from: protected */
    public void onBindDialogView(View view) {
        view.setOnTouchListener(this);
        this.keyview = (TextView) view.findViewById(R.id.primary);
        this.keyview.setText(getkeyName(this.keycode));
        super.onBindDialogView(view);
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialogBuilder(AlertDialog.Builder builder) {
        super.onPrepareDialogBuilder(builder);
        builder.setOnKeyListener(this);
    }

    public boolean onTouch(View v, MotionEvent event) {
        setKeyCode(0);
        this.keyview.setText(getkeyName(0));
        return true;
    }

    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        if (!isGoodKey(keyCode)) {
            return true;
        }
        setKeyCode(keyCode);
        this.keyview.setText(getkeyName(keyCode));
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        if (!positiveResult) {
            setKeyCode(this.lastkeyCode);
        }
    }

    private static String getkeyName(int keyCode) {
        if (keyCode < keynames.length) {
            return keynames[keyCode];
        }
        return "K" + keyCode;
    }

    private static boolean isGoodKey(int keyCode) {
        switch (keyCode) {
            case 3:
            case 6:
            case Base64.Encoder.LINE_GROUPS:
            case AdWhirlUtil.NETWORK_TYPE_ZESTADZ:
            case 21:
            case 22:
                return false;
            default:
                return true;
        }
    }
}
