package com.nix.game.pinball.free;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import com.nix.game.pinball.free.classes.Common;
import com.nix.game.pinball.free.managers.GoogleAnalytics;

public final class AskForRateManager {
    /* access modifiers changed from: private */
    public static boolean prefAsk4RateNeverAskAgain = false;
    private static int prefAskedMarketCount = 0;
    private static int prefGameCount = 0;

    public static void start(Context context) {
        loadPref(context);
        if (prefGameCount / 10 != prefAskedMarketCount && !prefAsk4RateNeverAskAgain && prefGameCount / 10 > 0) {
            prefAskedMarketCount = prefGameCount / 10;
            showAsk4Rate(context);
            savePref(context);
        }
    }

    private static void loadPref(Context context) {
        SharedPreferences se = PreferenceManager.getDefaultSharedPreferences(context);
        prefGameCount = se.getInt("prefGameCount", 0);
        prefAskedMarketCount = se.getInt("prefAskedMarketCount", 0);
        prefAsk4RateNeverAskAgain = se.getBoolean("prefAsk4RateNeverAskAgain", false);
    }

    /* access modifiers changed from: private */
    public static void savePref(Context context) {
        SharedPreferences.Editor ed = PreferenceManager.getDefaultSharedPreferences(context).edit();
        ed.putInt("prefGameCount", prefGameCount);
        ed.putInt("prefAskedMarketCount", prefAskedMarketCount);
        ed.putBoolean("prefAsk4RateNeverAskAgain", prefAsk4RateNeverAskAgain);
        ed.commit();
    }

    public static void increment(Context context) {
        prefGameCount++;
        savePref(context);
    }

    private static void showAsk4Rate(final Context context) {
        AlertDialog.Builder ad = new AlertDialog.Builder(context);
        View v = LayoutInflater.from(context).inflate((int) R.layout.ask4rate, (ViewGroup) null);
        final CheckBox check = (CheckBox) v.findViewById(R.id.chkNeverAskAgain);
        check.setChecked(false);
        ad.setView(v);
        ad.setIcon((int) R.drawable.icon);
        ad.setCancelable(true);
        ad.setTitle(context.getString(R.string.res_about));
        final AlertDialog ald = ad.show();
        ((Button) v.findViewById(R.id.btnAskYes)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ald.dismiss();
                Common.launchMarket(context, true);
                AskForRateManager.prefAsk4RateNeverAskAgain = true;
                AskForRateManager.savePref(context);
                GoogleAnalytics.trackAndDispatch("Ask4Rate/Yes");
            }
        });
        ((Button) v.findViewById(R.id.btnAskNo)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ald.dismiss();
                AskForRateManager.prefAsk4RateNeverAskAgain = check.isChecked();
                AskForRateManager.savePref(context);
                if (AskForRateManager.prefAsk4RateNeverAskAgain) {
                    GoogleAnalytics.trackAndDispatch("Ask4Rate/Later/NeverAgain");
                } else {
                    GoogleAnalytics.trackAndDispatch("Ask4Rate/Later");
                }
            }
        });
    }
}
