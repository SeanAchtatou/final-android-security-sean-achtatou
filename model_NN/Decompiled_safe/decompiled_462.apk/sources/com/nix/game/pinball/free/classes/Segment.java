package com.nix.game.pinball.free.classes;

import com.nix.game.pinball.free.math.Intersect;
import com.nix.game.pinball.free.math.Vec2;
import com.nix.game.pinball.free.objects.Ball;
import com.nix.game.pinball.free.objects.Table;

public final class Segment {
    static Vec2 p = new Vec2();
    public Vec2 a;
    public Vec2 b;
    public float d;
    public float k;
    public Vec2 m = new Vec2();
    public Vec2 n = new Vec2();

    public Segment(float x1, float y1, float x2, float y2, float bounce) {
        this.a = new Vec2(x1, y1);
        this.b = new Vec2(x2, y2);
        this.k = bounce;
        Vec2.normal(this.n, this.a, this.b);
        Vec2.lerp(this.m, this.a, this.b, 0.5f);
        this.d = Vec2.dot(this.a, this.n);
    }

    public void process() {
        int n2 = Table.balls.size();
        for (int i = 0; i < n2; i++) {
            processBall(Table.balls.get(i));
        }
    }

    public void processBall(Ball e) {
        if (Intersect.SegmentSegmentIntersect(e.q, e.p, this.a, this.b, p)) {
            Intersect.processCollision(this, e, p, 0.0f);
        } else if (Intersect.CircleSegmentIntersect(e.p, (float) e.r, this.a, this.b, p)) {
            Intersect.processCollision(this, e, p, 0.0f);
        }
    }
}
