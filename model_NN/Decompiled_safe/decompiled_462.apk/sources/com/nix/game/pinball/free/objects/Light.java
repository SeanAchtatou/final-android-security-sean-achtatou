package com.nix.game.pinball.free.objects;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import java.io.DataInputStream;
import java.io.IOException;

public final class Light extends PinballObject {
    private short angle;
    private Image imgoff;
    private Image imgon;
    private int initwait;
    private Matrix matrix;
    private int px;
    private int py;
    private int ref1;
    private int ref2;
    private int tick;
    private boolean uplayer;
    private int wait;

    public Light(DataInputStream dis) throws IOException {
        super(dis);
        boolean z;
        boolean z2;
        this.px = dis.readShort();
        this.py = dis.readShort();
        dis.readByte();
        this.angle = dis.readShort();
        byte b = dis.readByte();
        if ((b & 1) != 0) {
            z = true;
        } else {
            z = false;
        }
        this.visible = z;
        if ((b & 2) != 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        this.uplayer = z2;
        this.ref1 = dis.readInt();
        this.ref2 = dis.readInt();
    }

    public boolean isUpLayer() {
        return this.uplayer;
    }

    public void draw(Canvas c) {
        Image image;
        if (this.visible) {
            image = this.imgon;
        } else {
            image = this.imgoff;
        }
        if (image != null) {
            if (this.angle != 0) {
                c.drawBitmap(image.image, this.matrix, null);
            } else {
                c.drawBitmap(image.image, (float) (this.px - image.x), (float) (this.py - image.y), (Paint) null);
            }
        }
    }

    public void cycle() {
        boolean z;
        if (this.wait > 0) {
            this.visible = (this.wait / this.tick) % 2 != 0;
            this.wait--;
        } else if (this.wait < 0) {
            if ((this.wait / this.tick) % 2 != 0) {
                z = true;
            } else {
                z = false;
            }
            this.visible = z;
            this.wait++;
            if (this.wait >= 0) {
                this.wait = this.initwait;
            }
        }
    }

    public void update() {
        this.imgon = (Image) Table.omap.get(Integer.valueOf(this.ref1));
        this.imgoff = (Image) Table.omap.get(Integer.valueOf(this.ref2));
        if (this.angle != 0) {
            this.matrix = new Matrix();
            this.matrix.reset();
            this.matrix.preTranslate((float) (-this.imgoff.x), (float) (-this.imgoff.y));
            this.matrix.postRotate((float) this.angle);
            this.matrix.postTranslate((float) this.px, (float) this.py);
        }
    }

    public void flash(int wtime, int wtick) {
        this.initwait = wtime;
        this.wait = wtime;
        this.tick = wtick;
    }

    public void hide() {
        this.visible = false;
        this.wait = 0;
    }

    public int get(int index) {
        switch (index) {
            case PinballObject.PRP_VISIBLE /*2058414169*/:
                return (this.visible || this.wait != 0) ? 1 : 0;
            case PinballObject.PRP_CRC /*2086832125*/:
                return this.crc;
            default:
                return 0;
        }
    }
}
