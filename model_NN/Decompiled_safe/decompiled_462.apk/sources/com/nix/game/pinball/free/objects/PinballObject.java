package com.nix.game.pinball.free.objects;

import android.graphics.Canvas;
import java.io.DataInputStream;
import java.io.IOException;

public class PinballObject {
    public static final int PRP_CRC = 2086832125;
    public static final int PRP_VISIBLE = 2058414169;
    public int crc;
    public boolean visible = true;

    public PinballObject(DataInputStream dis) throws IOException {
        this.crc = dis.readInt();
    }

    public PinballObject(int Crc) {
        this.crc = Crc;
    }

    public void draw(Canvas c) {
    }

    public void cycle() {
    }

    public void update() {
    }

    public void show() {
        this.visible = true;
    }

    public void hide() {
        this.visible = false;
    }

    public void flash(int wtime, int wtick) {
    }

    public void moveby(int bx, int by) {
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void set(int index, int value) {
        switch (index) {
            case PRP_VISIBLE /*2058414169*/:
                this.visible = value != 0;
                break;
            case PRP_CRC /*2086832125*/:
                break;
            default:
                return;
        }
        this.crc = value;
    }

    public int get(int index) {
        switch (index) {
            case PRP_VISIBLE /*2058414169*/:
                return this.visible ? 1 : 0;
            case PRP_CRC /*2086832125*/:
                return this.crc;
            default:
                return 0;
        }
    }
}
