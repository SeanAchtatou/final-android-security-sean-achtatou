package com.nix.game.pinball.free.activities;

import android.os.Bundle;
import com.nix.game.pinball.free.managers.DataManager;
import magmamobile.app.SplashActivity;

public class main extends SplashActivity {
    public main() {
        super(menu.class);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataManager.init(this);
    }
}
