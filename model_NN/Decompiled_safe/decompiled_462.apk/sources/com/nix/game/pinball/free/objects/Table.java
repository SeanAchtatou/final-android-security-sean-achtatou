package com.nix.game.pinball.free.objects;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.SystemClock;
import android.util.Log;
import com.inmobi.androidsdk.impl.Constants;
import com.nix.game.pinball.free.R;
import com.nix.game.pinball.free.classes.Common;
import com.nix.game.pinball.free.managers.DataManager;
import com.nix.game.pinball.free.managers.ScriptManager;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public final class Table {
    private static final int ANIMSPEED = 20;
    public static final int KEY_L_FLIP = 0;
    public static final int KEY_MENU = 3;
    public static final int KEY_PAUSE = 4;
    public static final int KEY_PLUNG = 2;
    public static final int KEY_R_FLIP = 1;
    public static final int KEY_TILT = 5;
    private static int OnCycleEvent;
    private static int OnGameOverEvent;
    private static int OnLooseEvent;
    private static int OnStartEvent;
    private static boolean _loaded;
    public static float accx;
    public static float accy;
    public static float accz;
    private static boolean anim;
    private static short animtick;
    private static Ball ball;
    public static int ballradius;
    public static ArrayList<Ball> balls;
    public static char[] bffball;
    public static char[] bffbonus;
    public static char[] bfffactor;
    public static char[] bffscore;
    public static Bitmap bmpGameover;
    public static Bitmap bmpPause;
    public static Bitmap bmpSound;
    public static Bitmap bmpSoundBar;
    public static ArrayList<Bumper> bumpers;
    public static ArrayList<Display> displays;
    public static ArrayList<Flipper> flippers;
    public static ArrayList<Gate> gates;
    public static float gravityx;
    public static float gravityy;
    public static ArrayList<Image> images;
    public static Image imgBall;
    public static Image imgTable;
    public static boolean[] keyb;
    public static ArrayList<Kicker> kickers;
    public static float lacx;
    public static float lacy;
    public static float lacz;
    public static ArrayList<Light> lights;
    public static boolean loose;
    public static float maxspeed;
    public static int numballs;
    public static HashMap<Integer, PinballObject> omap;
    public static Paint paint;
    public static boolean pause;
    public static long playTime;
    public static ArrayList<Plunger> plungers;
    public static int ref1;
    public static int ref2;
    public static boolean surfaceReady;
    public static String tablename;
    public static ArrayList<Trigger> triggers;
    public static boolean upLeft;
    public static boolean upRight;
    public static boolean upSound;
    public static ArrayList<Light> uplights;
    public static ArrayList<Wall> walls;

    public static void load(Context context) {
        System.gc();
        _loaded = false;
        playTime = SystemClock.elapsedRealtime();
        Resources res = context.getResources();
        bmpGameover = BitmapFactory.decodeResource(res, R.drawable.imggameover);
        bmpSoundBar = BitmapFactory.decodeResource(res, R.drawable.soundbar);
        bmpSound = BitmapFactory.decodeResource(res, R.drawable.sound);
        bmpPause = BitmapFactory.decodeResource(res, R.drawable.imgpause);
        keyb = new boolean[6];
        bffscore = new char[8];
        bffbonus = new char[8];
        bfffactor = new char[8];
        bffball = new char[8];
        paint = new Paint();
        lacx = accx;
        lacy = accy;
        lacz = accz;
        paint.setColor(-4473925);
        if (!readTable()) {
            throw new RuntimeException("Can't load pinball table");
        }
        newBall();
        if (OnStartEvent != 0) {
            ScriptManager.call(OnStartEvent, null);
        }
    }

    public static boolean isLoaded() {
        return _loaded;
    }

    public static void release() {
        Log.d("--App--", "Release");
        _loaded = false;
        omap = null;
        walls = null;
        plungers = null;
        kickers = null;
        bumpers = null;
        gates = null;
        flippers = null;
        balls = null;
        keyb = null;
        displays = null;
        lights = null;
        triggers = null;
        paint = null;
        uplights = null;
    }

    public static void draw(Canvas c) {
        if (surfaceReady) {
            animtick = (short) (animtick + 1);
            if (animtick > 20) {
                animtick = 0;
                anim = !anim;
            }
            if (imgTable != null) {
                c.drawBitmap(imgTable.image, 0.0f, 0.0f, (Paint) null);
            } else {
                c.drawPaint(paint);
                int n = walls.size();
                for (int i = 0; i < n; i++) {
                    walls.get(i).draw(c);
                }
                int n2 = gates.size();
                for (int i2 = 0; i2 < n2; i2++) {
                    gates.get(i2).draw(c);
                }
            }
            int n3 = displays.size();
            for (int i3 = 0; i3 < n3; i3++) {
                displays.get(i3).draw(c);
            }
            int n4 = lights.size();
            for (int i4 = 0; i4 < n4; i4++) {
                lights.get(i4).draw(c);
            }
            int n5 = kickers.size();
            for (int i5 = 0; i5 < n5; i5++) {
                kickers.get(i5).draw(c);
            }
            int n6 = bumpers.size();
            for (int i6 = 0; i6 < n6; i6++) {
                bumpers.get(i6).draw(c);
            }
            int n7 = flippers.size();
            for (int i7 = 0; i7 < n7; i7++) {
                flippers.get(i7).draw(c);
            }
            int n8 = plungers.size();
            for (int i8 = 0; i8 < n8; i8++) {
                plungers.get(i8).draw(c);
            }
            int n9 = balls.size();
            for (int i9 = 0; i9 < n9; i9++) {
                ball = balls.get(i9);
                if (ball.z == 0) {
                    ball.draw(c);
                }
            }
            int n10 = uplights.size();
            for (int i10 = 0; i10 < n10; i10++) {
                uplights.get(i10).draw(c);
            }
            int n11 = balls.size();
            for (int i11 = 0; i11 < n11; i11++) {
                ball = balls.get(i11);
                if (ball.z > 0) {
                    ball.draw(c);
                }
            }
            if (pause) {
                if (!anim) {
                    c.drawBitmap(bmpPause, (float) ((Constants.INMOBI_ADVIEW_WIDTH - bmpPause.getWidth()) >> 1), (float) ((480 - bmpPause.getHeight()) >> 1), (Paint) null);
                }
            } else if (loose && !anim) {
                c.drawBitmap(bmpGameover, (float) ((Constants.INMOBI_ADVIEW_WIDTH - bmpGameover.getWidth()) >> 1), (float) ((480 - bmpGameover.getHeight()) >> 1), (Paint) null);
            }
        }
    }

    public static void cycle() {
        if (!pause) {
            if (OnCycleEvent != 0) {
                ScriptManager.call(OnCycleEvent, null);
            }
            int n = balls.size();
            for (int i = 0; i < n; i++) {
                balls.get(i).cycle();
            }
            int n2 = lights.size();
            for (int i2 = 0; i2 < n2; i2++) {
                lights.get(i2).cycle();
            }
            int n3 = uplights.size();
            for (int i3 = 0; i3 < n3; i3++) {
                uplights.get(i3).cycle();
            }
            upSound = false;
            if (keyb[0]) {
                if (!upLeft) {
                    upLeft = true;
                    upSound = true;
                }
            } else if (upLeft) {
                upLeft = false;
            }
            if (keyb[1]) {
                if (!upRight) {
                    upRight = true;
                    upSound = true;
                }
            } else if (upRight) {
                upRight = false;
            }
            if (upSound) {
                DataManager.playSound(DataManager.SND_FLIPPER);
            }
            int n4 = flippers.size();
            for (int i4 = 0; i4 < n4; i4++) {
                flippers.get(i4).cycle();
            }
            int n5 = bumpers.size();
            for (int i5 = 0; i5 < n5; i5++) {
                bumpers.get(i5).cycle();
            }
            int n6 = plungers.size();
            for (int i6 = 0; i6 < n6; i6++) {
                plungers.get(i6).cycle();
            }
            ScriptManager.updateVars();
            if (!loose) {
                int u = 0;
                while (u < balls.size()) {
                    if (!balls.get(u).enabled) {
                        balls.remove(u);
                    } else {
                        u++;
                    }
                }
                if (balls.size() == 0) {
                    ScriptManager.dataTbl[1] = 0;
                    ScriptManager.dataTbl[2] = 1;
                    if (!newBall()) {
                        loose = true;
                        if (OnGameOverEvent != 0) {
                            ScriptManager.call(OnGameOverEvent, null);
                        }
                    } else if (OnLooseEvent != 0) {
                        ScriptManager.call(OnLooseEvent, null);
                    }
                }
            }
        }
    }

    public static boolean addBall() {
        if (plungers.size() <= 0) {
            return false;
        }
        balls.add(plungers.get(0).newBall());
        return true;
    }

    private static boolean decBall() {
        if (plungers.size() <= 0 || ScriptManager.getLives() <= 0) {
            return false;
        }
        balls.add(plungers.get(0).newBall());
        return true;
    }

    public static boolean newBall() {
        boolean ok = decBall();
        if (ok) {
            int[] iArr = ScriptManager.dataTbl;
            iArr[3] = iArr[3] - 1;
        }
        return ok;
    }

    /* JADX INFO: Multiple debug info for r1v3 java.io.DataInputStream: [D('info' com.nix.game.pinball.free.classes.TableInfo), D('dis' java.io.DataInputStream)] */
    /* JADX INFO: Multiple debug info for r3v4 int: [D('n' int), D('i' int)] */
    /* JADX INFO: Multiple debug info for r2v29 com.nix.game.pinball.free.objects.PinballObject: [D('size' int), D('e' com.nix.game.pinball.free.objects.PinballObject)] */
    /* JADX INFO: Multiple debug info for r2v30 com.nix.game.pinball.free.objects.Image: [D('size' int), D('e' com.nix.game.pinball.free.objects.PinballObject)] */
    /* JADX INFO: Multiple debug info for r2v31 com.nix.game.pinball.free.objects.Flipper: [D('size' int), D('e' com.nix.game.pinball.free.objects.PinballObject)] */
    /* JADX INFO: Multiple debug info for r2v32 com.nix.game.pinball.free.objects.Gate: [D('size' int), D('e' com.nix.game.pinball.free.objects.PinballObject)] */
    /* JADX INFO: Multiple debug info for r2v33 com.nix.game.pinball.free.objects.Display: [D('size' int), D('e' com.nix.game.pinball.free.objects.PinballObject)] */
    /* JADX INFO: Multiple debug info for r2v34 com.nix.game.pinball.free.objects.Light: [D('size' int), D('e' com.nix.game.pinball.free.objects.PinballObject)] */
    /* JADX INFO: Multiple debug info for r2v35 com.nix.game.pinball.free.objects.Plunger: [D('size' int), D('e' com.nix.game.pinball.free.objects.PinballObject)] */
    /* JADX INFO: Multiple debug info for r2v36 com.nix.game.pinball.free.objects.Trigger: [D('size' int), D('e' com.nix.game.pinball.free.objects.PinballObject)] */
    /* JADX INFO: Multiple debug info for r2v37 com.nix.game.pinball.free.objects.Kicker: [D('size' int), D('e' com.nix.game.pinball.free.objects.PinballObject)] */
    /* JADX INFO: Multiple debug info for r2v38 com.nix.game.pinball.free.objects.Bumper: [D('size' int), D('e' com.nix.game.pinball.free.objects.PinballObject)] */
    /* JADX INFO: Multiple debug info for r2v39 com.nix.game.pinball.free.objects.Wall: [D('size' int), D('e' com.nix.game.pinball.free.objects.PinballObject)] */
    private static final boolean readTable() {
        try {
            InputStream is = DataManager.selectedTable.openStream();
            DataInputStream dis = new DataInputStream(is);
            omap = new HashMap<>();
            walls = new ArrayList<>();
            plungers = new ArrayList<>();
            kickers = new ArrayList<>();
            bumpers = new ArrayList<>();
            gates = new ArrayList<>();
            flippers = new ArrayList<>();
            balls = new ArrayList<>();
            displays = new ArrayList<>();
            images = new ArrayList<>();
            lights = new ArrayList<>();
            triggers = new ArrayList<>();
            uplights = new ArrayList<>();
            pause = false;
            loose = false;
            anim = false;
            upLeft = false;
            upRight = false;
            animtick = 0;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            short magic = dis.readShort();
            if (dis.readShort() == 257 && magic == -31672) {
                tablename = Common.readString(dis);
                dis.skip((long) dis.readInt());
                int total = dis.readShort();
                PinballObject[] imap = new PinballObject[total];
                for (int i = 0; i < total; i++) {
                    byte type = dis.readByte();
                    int size = dis.readInt();
                    switch (type) {
                        case 1:
                            Wall wall = new Wall(dis);
                            walls.add(wall);
                            omap.put(Integer.valueOf(wall.crc), wall);
                            imap[i] = wall;
                            break;
                        case 2:
                            Bumper bumper = new Bumper(dis);
                            bumpers.add(bumper);
                            omap.put(Integer.valueOf(bumper.crc), bumper);
                            imap[i] = bumper;
                            break;
                        case 3:
                            Kicker kicker = new Kicker(dis);
                            kickers.add(kicker);
                            omap.put(Integer.valueOf(kicker.crc), kicker);
                            imap[i] = kicker;
                            break;
                        case 4:
                            Trigger trigger = new Trigger(dis);
                            triggers.add(trigger);
                            omap.put(Integer.valueOf(trigger.crc), trigger);
                            imap[i] = trigger;
                            break;
                        case 5:
                            Plunger plunger = new Plunger(dis);
                            plungers.add(plunger);
                            omap.put(Integer.valueOf(plunger.crc), plunger);
                            imap[i] = plunger;
                            break;
                        case 6:
                            Light light = new Light(dis);
                            if (light.isUpLayer() != 0) {
                                uplights.add(light);
                            } else {
                                lights.add(light);
                            }
                            omap.put(Integer.valueOf(light.crc), light);
                            imap[i] = light;
                            break;
                        case 7:
                            Display display = new Display(dis);
                            displays.add(display);
                            omap.put(Integer.valueOf(display.crc), display);
                            imap[i] = display;
                            break;
                        case 8:
                            Gate gate = new Gate(dis);
                            gates.add(gate);
                            omap.put(Integer.valueOf(gate.crc), gate);
                            imap[i] = gate;
                            break;
                        case 9:
                            Flipper flipper = new Flipper(dis);
                            flippers.add(flipper);
                            omap.put(Integer.valueOf(flipper.crc), flipper);
                            imap[i] = flipper;
                            break;
                        case 10:
                        default:
                            if (size > 0) {
                                dis.skip((long) size);
                            }
                            imap[i] = new PinballObject(0);
                            break;
                        case 11:
                            Image image = new Image(dis, options);
                            images.add(image);
                            omap.put(Integer.valueOf(image.crc), image);
                            imap[i] = image;
                            break;
                        case 12:
                            PinballObject e = loadTable(dis);
                            omap.put(Integer.valueOf(e.crc), e);
                            imap[i] = e;
                            break;
                    }
                }
                ScriptManager.load(dis);
                update();
                for (PinballObject update : imap) {
                    update.update();
                }
            }
            dis.close();
            is.close();
            _loaded = true;
            return true;
        } catch (IOException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    private static PinballObject loadTable(DataInputStream dis) throws IOException {
        int crc = dis.readInt();
        gravityy = dis.readFloat();
        numballs = dis.readShort();
        maxspeed = dis.readFloat();
        ballradius = dis.readUnsignedByte();
        ref1 = dis.readInt();
        ref2 = dis.readInt();
        OnStartEvent = dis.readInt();
        OnLooseEvent = dis.readInt();
        OnGameOverEvent = dis.readInt();
        OnCycleEvent = dis.readInt();
        return new PinballObject(crc);
    }

    private static void update() {
        imgTable = (Image) omap.get(Integer.valueOf(ref1));
        imgBall = (Image) omap.get(Integer.valueOf(ref2));
    }
}
