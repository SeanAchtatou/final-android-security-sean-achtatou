package com.nix.game.pinball.free.managers;

import com.adwhirl.util.AdWhirlUtil;
import com.google.ads.util.Base64;
import com.inmobi.androidsdk.impl.Constants;
import com.nix.game.pinball.free.classes.Common;
import com.nix.game.pinball.free.objects.Ball;
import com.nix.game.pinball.free.objects.PinballObject;
import com.nix.game.pinball.free.objects.Table;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public final class ScriptManager {
    public static final int MAXBALL = 99;
    public static final int MAXBONUS = 99999999;
    public static final int MAXFACTOR = 4;
    public static final int MAXSCORE = 99999999;
    public static final int VAR_BONUS = 1;
    public static final int VAR_FACTOR = 2;
    public static final int VAR_LIVES = 3;
    public static final int VAR_SCORE = 0;
    private static ByteBuffer buffer;
    private static byte[] codeTbl;
    private static int codesize;
    public static int[] dataTbl;
    public static int datasize;

    public static int getScore() {
        return dataTbl[0];
    }

    public static int getBonus() {
        return dataTbl[1];
    }

    public static int getLives() {
        return dataTbl[3];
    }

    public static int getFactor() {
        return dataTbl[2];
    }

    public static void updateVars() {
        dataTbl[0] = Common.minmax(dataTbl[0], 0, 99999999);
        dataTbl[1] = Common.minmax(dataTbl[1], 0, 99999999);
        dataTbl[2] = Common.minmax(dataTbl[2], 0, 4);
        dataTbl[3] = Common.minmax(dataTbl[3], 0, 99);
        Common.fixed(dataTbl[0], Table.bffscore);
        Common.fixed(dataTbl[1], Table.bffbonus);
        Common.fixed(dataTbl[2], Table.bfffactor);
        Common.fixed(dataTbl[3], Table.bffball);
    }

    public static void load(DataInputStream dis) throws IOException {
        datasize = dis.readInt();
        dataTbl = new int[datasize];
        codesize = dis.readInt();
        codeTbl = new byte[codesize];
        dis.read(codeTbl);
        buffer = ByteBuffer.wrap(codeTbl);
        dataTbl[0] = 0;
        dataTbl[1] = 0;
        dataTbl[2] = 1;
        dataTbl[3] = Table.numballs;
    }

    /* JADX INFO: Multiple debug info for r7v12 byte: [D('opcode' byte), D('b' byte)] */
    /* JADX INFO: Multiple debug info for r7v14 short: [D('opcode' byte), D('j' int)] */
    /* JADX INFO: Multiple debug info for r7v15 int: [D('l' int), D('j' int)] */
    /* JADX INFO: Multiple debug info for r7v17 int: [D('opcode' byte), D('i' int)] */
    /* JADX INFO: Multiple debug info for r7v20 byte: [D('opcode' byte), D('b' byte)] */
    /* JADX INFO: Multiple debug info for r3v6 int: [D('l' int), D('k' int)] */
    /* JADX INFO: Multiple debug info for r7v32 int: [D('opcode' byte), D('i' int)] */
    /* JADX INFO: Multiple debug info for r7v36 int: [D('opcode' byte), D('i' int)] */
    /* JADX INFO: Multiple debug info for r7v40 int: [D('opcode' byte), D('i' int)] */
    /* JADX INFO: Multiple debug info for r7v41 int: [D('opcode' byte), D('i' int)] */
    /* JADX INFO: Multiple debug info for r7v42 int: [D('opcode' byte), D('i' int)] */
    /* JADX INFO: Multiple debug info for r7v43 int: [D('opcode' byte), D('i' int)] */
    /* JADX INFO: Multiple debug info for r7v46 int: [D('opcode' byte), D('i' int)] */
    /* JADX INFO: Multiple debug info for r7v50 int: [D('opcode' byte), D('i' int)] */
    /* JADX INFO: Multiple debug info for r7v55 byte: [D('opcode' byte), D('b' byte)] */
    /* JADX INFO: Multiple debug info for r7v57 byte: [D('opcode' byte), D('b' byte)] */
    /* JADX INFO: Multiple debug info for r7v59 byte: [D('opcode' byte), D('b' byte)] */
    /* JADX INFO: Multiple debug info for r7v61 byte: [D('opcode' byte), D('b' byte)] */
    /* JADX INFO: Multiple debug info for r7v63 byte: [D('opcode' byte), D('b' byte)] */
    /* JADX INFO: Multiple debug info for r7v65 byte: [D('opcode' byte), D('b' byte)] */
    public static void call(int ptr, Object param) {
        buffer.position(ptr);
        while (buffer.position() < codesize) {
            switch (buffer.get()) {
                case 1:
                    dataTbl[buffer.get()] = 0;
                    break;
                case 2:
                    dataTbl[buffer.get()] = execGetVar();
                    break;
                case 3:
                    byte b = buffer.get();
                    if (b != 0) {
                        int[] iArr = dataTbl;
                        iArr[b] = iArr[b] + execGetVar();
                        break;
                    } else {
                        int[] iArr2 = dataTbl;
                        iArr2[b] = iArr2[b] + (execGetVar() * getFactor());
                        break;
                    }
                case 4:
                    byte b2 = buffer.get();
                    if (b2 != 0) {
                        int[] iArr3 = dataTbl;
                        iArr3[b2] = iArr3[b2] - execGetVar();
                        break;
                    } else {
                        int[] iArr4 = dataTbl;
                        iArr4[b2] = iArr4[b2] - (execGetVar() * getFactor());
                        break;
                    }
                case 5:
                    byte b3 = buffer.get();
                    if (b3 != 0) {
                        int[] iArr5 = dataTbl;
                        iArr5[b3] = iArr5[b3] * execGetVar();
                        break;
                    } else {
                        int[] iArr6 = dataTbl;
                        iArr6[b3] = iArr6[b3] * execGetVar() * getFactor();
                        break;
                    }
                case 6:
                    byte b4 = buffer.get();
                    if (b4 != 0) {
                        int[] iArr7 = dataTbl;
                        iArr7[b4] = iArr7[b4] / execGetVar();
                        break;
                    } else {
                        int[] iArr8 = dataTbl;
                        iArr8[b4] = iArr8[b4] / (execGetVar() * getFactor());
                        break;
                    }
                case 16:
                    PinballObject e = Table.omap.get(Integer.valueOf(execGetVar()));
                    if (e == null) {
                        break;
                    } else {
                        e.show();
                        break;
                    }
                case 17:
                    PinballObject e2 = Table.omap.get(Integer.valueOf(execGetVar()));
                    if (e2 == null) {
                        break;
                    } else {
                        e2.hide();
                        break;
                    }
                case AdWhirlUtil.NETWORK_TYPE_INMOBI:
                    int i = execGetVar();
                    int j = execGetVar();
                    if (param != null && param.getClass() == Ball.class) {
                        Ball o = (Ball) param;
                        o.p.x = (float) i;
                        o.p.y = (float) j;
                        o.q.x = (float) i;
                        o.q.y = (float) j;
                        break;
                    }
                case Base64.Encoder.LINE_GROUPS:
                    int i2 = execGetVar();
                    if (i2 == 0) {
                        break;
                    } else {
                        DataManager.playSound(i2);
                        break;
                    }
                case AdWhirlUtil.NETWORK_TYPE_ZESTADZ:
                    int i3 = execGetVar();
                    if (i3 == 0) {
                        break;
                    } else {
                        DataManager.vibrate((long) i3);
                        break;
                    }
                case 21:
                    int i4 = execGetVar();
                    if (param != null && param.getClass() == Ball.class) {
                        ((Ball) param).wait = i4;
                        break;
                    }
                case 22:
                    PinballObject e3 = Table.omap.get(Integer.valueOf(execGetVar()));
                    if (e3 == null) {
                        break;
                    } else {
                        e3.flash(execGetVar(), execGetVar());
                        break;
                    }
                case AdWhirlUtil.NETWORK_TYPE_ONERIOT:
                    PinballObject e4 = Table.omap.get(Integer.valueOf(execGetVar()));
                    if (e4 == null) {
                        break;
                    } else {
                        e4.moveby(execGetVar(), execGetVar());
                        break;
                    }
                case 24:
                    if (param != null && param.getClass() == Ball.class) {
                        ((Ball) param).z = 1;
                        break;
                    }
                case 25:
                    if (param != null && param.getClass() == Ball.class) {
                        ((Ball) param).z = 0;
                        break;
                    }
                case 32:
                    byte b5 = buffer.get();
                    int k = buffer.getShort();
                    int i5 = execGetVar();
                    int j2 = execGetVar();
                    int k2 = k + buffer.position();
                    switch (b5) {
                        case 1:
                            if (i5 != j2) {
                                buffer.position(k2);
                                break;
                            } else {
                                continue;
                            }
                        case 2:
                            if (i5 == j2) {
                                buffer.position(k2);
                                break;
                            } else {
                                continue;
                            }
                        case 3:
                            if (i5 >= j2) {
                                buffer.position(k2);
                                break;
                            } else {
                                continue;
                            }
                        case 4:
                            if (i5 <= j2) {
                                buffer.position(k2);
                                break;
                            } else {
                                continue;
                            }
                        case 5:
                            if (i5 > j2) {
                                buffer.position(k2);
                                break;
                            } else {
                                continue;
                            }
                        case 6:
                            if (i5 < j2) {
                                buffer.position(k2);
                                break;
                            } else {
                                continue;
                            }
                        default:
                            buffer.position(k2);
                            continue;
                    }
                case 48:
                    return;
                case 49:
                    int i6 = buffer.getInt();
                    int j3 = buffer.position();
                    call(i6, param);
                    buffer.position(j3);
                    break;
                case Constants.INMOBI_ADVIEW_HEIGHT:
                    buffer.position(buffer.getShort() + buffer.position());
                    break;
                case CustomVariable.MAX_CUSTOM_VARIABLE_LENGTH:
                    Table.addBall();
                    break;
                case 65:
                    dataTbl[buffer.get()] = (int) (Math.random() * ((double) execGetVar()));
                    break;
                case 66:
                    if (param != null && param.getClass() == Ball.class) {
                        Ball o2 = (Ball) param;
                        o2.v.x = 0.0f;
                        o2.v.y = 0.0f;
                        break;
                    }
                case 80:
                    int[] iArr9 = dataTbl;
                    iArr9[0] = iArr9[0] + (dataTbl[1] * dataTbl[2]);
                    dataTbl[1] = 0;
                    dataTbl[2] = 1;
                    break;
            }
        }
    }

    private static int execGetVar() {
        switch (buffer.get()) {
            case 0:
                return dataTbl[buffer.get()];
            case 10:
                int crc = buffer.getInt();
                int prp = buffer.getInt();
                PinballObject obj = Table.omap.get(Integer.valueOf(crc));
                if (obj != null) {
                    return obj.get(prp);
                }
                return 0;
            case 11:
                return buffer.getInt();
            case 12:
                return buffer.get();
            case 13:
                return buffer.getShort();
            case 14:
                return buffer.getInt();
            default:
                return 0;
        }
    }
}
