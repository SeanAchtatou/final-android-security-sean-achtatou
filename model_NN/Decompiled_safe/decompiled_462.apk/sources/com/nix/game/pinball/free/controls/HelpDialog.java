package com.nix.game.pinball.free.controls;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import com.nix.game.pinball.free.R;
import com.nix.game.pinball.free.activities.options;
import com.nix.game.pinball.free.classes.Common;
import com.nix.game.pinball.free.managers.DataManager;

public final class HelpDialog extends WebViewClient implements View.OnClickListener, DialogInterface.OnDismissListener {
    private static final String HTMLURL = "http://www.androlib.com/pinball/howtoplay-";
    private static final String[] LNGS = {"fr", "en", "es", "de", "pl", "nl", "zh", "ru", "ja", "it", "gr", "pt", "br", "hu", "mk", "cz", "hr"};
    private Button mButton;
    private Context mContext;
    private float mDensity;
    private Dialog mDialog;
    private LinearLayout mLayout;
    private ProgressBar mProgress;
    private WebView mWebview;

    public HelpDialog(Activity context) {
        this.mDensity = Common.getDensity(context);
        this.mContext = context;
    }

    private int dpi(float px) {
        return (int) (this.mDensity * px);
    }

    public void show() {
        this.mDialog = new Dialog(this.mContext);
        this.mDialog.setOnDismissListener(this);
        this.mDialog.setTitle((int) R.string.sumHowToPlay);
        this.mLayout = new LinearLayout(this.mContext);
        this.mLayout.setLayoutParams(new LinearLayout.LayoutParams(dpi(300.0f), dpi(450.0f)));
        this.mLayout.setOrientation(1);
        this.mLayout.setGravity(1);
        this.mProgress = new ProgressBar(this.mContext);
        this.mProgress.setIndeterminate(true);
        this.mProgress.setPadding(dpi(64.0f), dpi(64.0f), dpi(64.0f), dpi(64.0f));
        this.mLayout.addView(this.mProgress);
        this.mButton = new Button(this.mContext);
        this.mButton.setLayoutParams(new LinearLayout.LayoutParams(dpi(160.0f), -2));
        this.mButton.setText((int) R.string.cmnClose);
        this.mButton.setOnClickListener(this);
        this.mWebview = new WebView(this.mContext);
        this.mWebview.setHorizontalScrollBarEnabled(true);
        this.mWebview.setLayoutParams(new LinearLayout.LayoutParams(dpi(300.0f), dpi(350.0f)));
        this.mWebview.loadUrl(getHtmlURL(this.mContext));
        this.mWebview.setWebViewClient(this);
        this.mDialog.setContentView(this.mLayout);
        this.mDialog.show();
    }

    private static String getHtmlURL(Context context) {
        StringBuilder s = new StringBuilder(HTMLURL);
        String ln = context.getResources().getConfiguration().locale.getLanguage();
        int n = LNGS.length;
        for (int i = 0; i < n; i++) {
            if (LNGS[i].equals(ln)) {
                s.append(LNGS[i].concat(".html"));
                return s.toString();
            }
        }
        s.append("en.html");
        return s.toString();
    }

    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        this.mLayout.removeAllViews();
        this.mLayout.addView(this.mWebview);
        this.mLayout.addView(this.mButton);
    }

    public void onClick(View view) {
        if (view == this.mButton) {
            this.mDialog.dismiss();
        }
    }

    public void onDismiss(DialogInterface arg0) {
        if (DataManager.firsttime) {
            this.mContext.startActivity(new Intent(this.mContext, options.class));
        }
    }
}
