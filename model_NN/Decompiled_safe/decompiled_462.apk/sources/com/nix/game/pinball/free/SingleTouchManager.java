package com.nix.game.pinball.free;

import android.content.Context;
import android.view.MotionEvent;
import com.nix.game.pinball.free.activities.game;
import com.nix.game.pinball.free.managers.DataManager;
import com.nix.game.pinball.free.objects.Table;

public final class SingleTouchManager extends TouchManager {
    public SingleTouchManager(Context context) {
        super(context);
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void onTouch(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                if (Table.loose) {
                    DataManager.playSound(DataManager.SND_SELECT);
                    ((game) getContext()).fnDialogSendScore();
                    return;
                }
                if (DataManager.keya == 0) {
                    Table.keyb[0] = true;
                }
                if (DataManager.keyb == 0) {
                    Table.keyb[1] = true;
                }
                if (DataManager.keyc == 0) {
                    Table.keyb[2] = true;
                }
                if (DataManager.keyd == 0) {
                    ((game) getContext()).fnDialogExit();
                }
                if (DataManager.keye == 0) {
                    Table.pause = !Table.pause;
                    return;
                }
                return;
            case 1:
                if (DataManager.keya == 0) {
                    Table.keyb[0] = false;
                }
                if (DataManager.keyb == 0) {
                    Table.keyb[1] = false;
                }
                if (DataManager.keyc == 0) {
                    Table.keyb[2] = false;
                    return;
                }
                return;
            case 2:
            default:
                return;
        }
    }

    public boolean isMultitouch() {
        return false;
    }
}
