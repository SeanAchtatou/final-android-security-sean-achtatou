package com.nix.game.pinball.free.objects;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import com.nix.game.pinball.free.classes.Common;
import com.nix.game.pinball.free.classes.Segment;
import com.nix.game.pinball.free.managers.DataManager;
import com.nix.game.pinball.free.math.Intersect;
import com.nix.game.pinball.free.math.Vec2;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class Flipper extends PinballObject {
    static Vec2 k = new Vec2();
    static Vec2 n1 = new Vec2();
    static Vec2 z = new Vec2();
    private float a;
    private float aa;
    private boolean ccw;
    private float da;
    private Image image;
    private boolean isup;
    private int length;
    private Matrix matrix;
    private Vec2 p = new Vec2();
    private Paint paint = new Paint();
    private float power;
    private Vec2 q1;
    private Vec2 q2;
    private int ref1;
    private int ref2;
    private Segment sg;
    private Segment sgd;
    private Segment sgu;
    private int vibrate;

    public Flipper(DataInputStream dis) throws IOException {
        super(dis);
        this.p.x = (float) dis.readShort();
        this.p.y = (float) dis.readShort();
        this.length = dis.readUnsignedByte();
        this.a = Common.radian(dis.readShort());
        this.da = Common.radian(dis.readShort());
        this.power = dis.readFloat();
        this.vibrate = dis.readShort();
        this.ref1 = dis.readInt();
        this.ref2 = dis.readInt();
        this.ccw = this.da > 0.0f;
        int len = this.length;
        this.paint.setFilterBitmap(true);
        this.paint.setAntiAlias(true);
        this.matrix = new Matrix();
        this.q1 = new Vec2(this.p, this.a, len);
        float fx1 = this.p.x + (((float) Math.cos((double) this.a)) * ((float) this.length));
        float fy1 = this.p.y + (((float) Math.sin((double) this.a)) * ((float) this.length));
        this.aa = this.a + this.da;
        this.q2 = new Vec2(this.p, this.aa, len);
        this.sgu = new Segment(this.p.x, this.p.y, this.p.x + (((float) Math.cos((double) this.aa)) * ((float) this.length)), this.p.y + (((float) Math.sin((double) this.aa)) * ((float) this.length)), 0.0f);
        this.sgd = new Segment(this.p.x, this.p.y, fx1, fy1, 0.0f);
        this.sg = this.sgd;
    }

    public void cycle() {
        if (this.visible) {
            boolean b = false;
            if (this.ccw ? Table.upRight : Table.upLeft) {
                if (!this.isup) {
                    b = true;
                    this.isup = true;
                    this.sg = this.sgu;
                    ComputeMatrix((int) (((double) (this.aa * 180.0f)) / 3.141592653589793d));
                }
            } else if (this.isup) {
                b = true;
                this.isup = false;
                this.sg = this.sgd;
                ComputeMatrix((int) (((double) (this.a * 180.0f)) / 3.141592653589793d));
            }
            ArrayList<Ball> balls = Table.balls;
            int n = balls.size();
            if (b) {
                for (int i = 0; i < n; i++) {
                    processFlipperDynamic(balls.get(i), this.isup);
                }
                return;
            }
            for (int i2 = 0; i2 < n; i2++) {
                processFlipperStatic(balls.get(i2));
            }
        }
    }

    private void ComputeMatrix(int degree) {
        if (this.image != null) {
            this.matrix.reset();
            this.matrix.preTranslate((float) (-this.image.x), (float) (-this.image.y));
            this.matrix.postRotate((float) degree);
            this.matrix.postTranslate(this.p.x, this.p.y);
        }
    }

    public void draw(Canvas c) {
        if (this.visible && this.image != null) {
            c.drawBitmap(this.image.image, this.matrix, this.paint);
        }
    }

    private void processFlipperDynamic(Ball e, boolean up) {
        boolean u;
        if (this.ccw) {
            u = Intersect.PointInPie(e.p, this.p, this.q1, this.q2, (float) this.length);
        } else {
            u = Intersect.PointInPie(e.p, this.p, this.q2, this.q1, (float) this.length);
        }
        if (u) {
            float dx = e.p.x - this.p.x;
            float dy = e.p.y - this.p.y;
            float l = ((float) Math.sqrt((double) ((dx * dx) + (dy * dy)))) / ((float) this.length);
            float pow = this.power * l;
            Vec2.normal(n1, e.p, this.p);
            if (up) {
                Vec2.lerp(e.p, this.p, this.q2, l);
            } else {
                Vec2.lerp(e.p, this.p, this.q1, l);
            }
            e.v.x *= 0.4f;
            e.v.y *= 0.4f;
            e.v.x = -e.v.x;
            e.v.y = -e.v.y;
            if (!up) {
                n1.x = -n1.x;
                n1.y = -n1.y;
            }
            if (this.ccw) {
                e.p.x += n1.x * ((float) e.r);
                e.p.y += n1.y * ((float) e.r);
                e.v.x += n1.x * pow;
                e.v.y += n1.y * pow;
            } else {
                e.p.x -= n1.x * ((float) e.r);
                e.p.y -= n1.y * ((float) e.r);
                e.v.x -= n1.x * pow;
                e.v.y -= n1.y * pow;
            }
            e.q.x = e.p.x;
            e.q.y = e.p.y;
            if (this.vibrate > 0) {
                DataManager.vibrate((long) ((int) (((float) this.vibrate) * l)));
            }
            if (this.ref2 > 0) {
                DataManager.playSound(this.ref2);
            }
        }
    }

    private void processFlipperStatic(Ball e) {
        if (e.wait <= 0) {
            if (Intersect.SegmentSegmentIntersect(e.q, e.p, this.sg.a, this.sg.b, k)) {
                if (k.x != this.sg.a.x || k.y != this.sg.a.y) {
                    if (k.x != this.sg.b.x || k.y != this.sg.b.y) {
                        float d1 = ((e.q.x * this.sg.n.x) + (e.q.y * this.sg.n.y)) - ((this.sg.a.x * this.sg.n.x) + (this.sg.a.y * this.sg.n.y));
                        if (d1 > 0.0f) {
                            processFlipperCollisionStatic(e, this.sg, k, true);
                        } else if (d1 < 0.0f) {
                            processFlipperCollisionStatic(e, this.sg, k, false);
                        }
                    }
                }
            } else if (!Intersect.CircleSegmentIntersect(e.p, (float) e.r, this.sg.a, this.sg.b, k)) {
            } else {
                if (k.x != this.sg.a.x || k.y != this.sg.a.y) {
                    if (k.x != this.sg.b.x || k.y != this.sg.b.y) {
                        float d12 = ((e.p.x * this.sg.n.x) + (e.p.y * this.sg.n.y)) - ((this.sg.a.x * this.sg.n.x) + (this.sg.a.y * this.sg.n.y));
                        if (d12 > 0.0f) {
                            processFlipperCollisionStatic(e, this.sg, k, true);
                        } else if (d12 < 0.0f) {
                            processFlipperCollisionStatic(e, this.sg, k, false);
                        }
                    }
                }
            }
        }
    }

    private void processFlipperCollisionStatic(Ball e, Segment l, Vec2 k2, boolean fob) {
        float s = e.v.length();
        if (fob) {
            e.p.x = k2.x + (l.n.x * ((float) e.r));
            e.p.y = k2.y + (l.n.y * ((float) e.r));
            e.v.x += l.n.x * s;
            e.v.y += l.n.y * s;
        } else {
            e.p.x = k2.x - (l.n.x * ((float) e.r));
            e.p.y = k2.y - (l.n.y * ((float) e.r));
            e.v.x -= l.n.x * s;
            e.v.y -= l.n.y * s;
        }
        e.bind();
    }

    public void update() {
        this.image = (Image) Table.omap.get(Integer.valueOf(this.ref1));
        ComputeMatrix(Common.degree(this.a));
    }
}
