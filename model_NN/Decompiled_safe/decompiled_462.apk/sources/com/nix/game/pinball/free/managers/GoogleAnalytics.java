package com.nix.game.pinball.free.managers;

import android.content.Context;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public final class GoogleAnalytics {
    private static final String CHANNEL = "UA-11900364-4";
    private static String mRoot;
    private static GoogleAnalyticsTracker mTracker;

    public static void start(Context context, String root) {
        if (mTracker == null) {
            try {
                mRoot = root;
                mTracker = GoogleAnalyticsTracker.getInstance();
                mTracker.start(CHANNEL, context);
                trackAndDispatch(null);
            } catch (Exception e) {
            }
        }
    }

    public static void trackAndDispatch(String page) {
        if (mTracker != null) {
            try {
                mTracker.trackPageView(getPageWithRoot(page));
                mTracker.dispatch();
            } catch (Exception e) {
            }
        }
    }

    public static void track(String page) {
        if (mTracker != null) {
            try {
                mTracker.trackPageView(getPageWithRoot(page));
            } catch (Exception e) {
            }
        }
    }

    public static void stop() {
        if (mTracker != null) {
            try {
                mTracker.stop();
            } catch (Exception e) {
            }
        }
    }

    private static String getPageWithRoot(String page) {
        return page != null ? mRoot.concat(page) : mRoot;
    }
}
