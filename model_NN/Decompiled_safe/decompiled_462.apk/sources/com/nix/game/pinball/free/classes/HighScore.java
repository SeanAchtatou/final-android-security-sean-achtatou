package com.nix.game.pinball.free.classes;

import android.content.Context;
import android.os.SystemClock;
import com.nix.game.pinball.free.managers.DataManager;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public final class HighScore {
    private static final String APIURL = "http://api.androlib.com/api/pinball.ashx";
    public String name;
    public int pos;
    public int score;
    public boolean selected;
    public String table;

    public HighScore(int Pos, int Score, String Name, String Table, boolean Selected) {
        this.pos = Pos;
        this.name = Name;
        this.table = Table;
        this.score = Score;
        this.selected = Selected;
    }

    public static class Helper {
        private static DocumentBuilder db;
        private static DocumentBuilderFactory dbf;

        /* access modifiers changed from: private */
        public static void sendXMLRequestNoReturn(String uri) throws Exception {
            new DefaultHttpClient().execute(new HttpGet(uri));
        }

        private static Element sendXMLRequest(String uri) throws Exception {
            DefaultHttpClient http = new DefaultHttpClient();
            HttpGet request = new HttpGet(uri);
            request.addHeader("Accept-Encoding", "gzip, deflate");
            HttpResponse response = http.execute(request);
            InputStream is = response.getEntity().getContent();
            if (response.getHeaders("Content-Encoding").length > 0 && response.getHeaders("Content-Encoding")[0].getValue().toLowerCase().equals("gzip")) {
                is = new GZIPInputStream(is);
            }
            if (dbf == null) {
                dbf = DocumentBuilderFactory.newInstance();
            }
            if (db == null) {
                db = dbf.newDocumentBuilder();
            }
            Element e = db.parse(is).getDocumentElement();
            e.normalize();
            return e;
        }

        public static void addInfo(Context context, String playerName, String tableName, int score, long playTime) {
            final Context context2 = context;
            final String str = tableName;
            final long j = playTime;
            final int i = score;
            new Thread() {
                public void run() {
                    try {
                        String ver = Common.getAppVersion(context2);
                        String uid = DataManager.deviceid;
                        String table = URLEncoder.encode(str);
                        Helper.sendXMLRequestNoReturn("http://api.androlib.com/api/pinball.ashx?endgame=1&t=" + table + "&di=" + uid + "&s=" + i + "&v=" + ver + "&tm=" + String.valueOf(SystemClock.elapsedRealtime() - j));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }

        public static void addHighScore(final Context context, final String playerName, final String tableName, final int score) {
            new Thread() {
                public void run() {
                    try {
                        Helper.sendXMLRequestNoReturn("http://api.androlib.com/api/pinball.ashx?sendscore=1&t=" + URLEncoder.encode(tableName) + "&n=" + URLEncoder.encode(playerName) + "&di=" + DataManager.deviceid + "&s=" + score + "&v=" + Common.getAppVersion(context));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }

        /* JADX INFO: Multiple debug info for r8v2 java.lang.String: [D('context' android.content.Context), D('level' java.lang.String)] */
        /* JADX INFO: Multiple debug info for r9v1 java.lang.String: [D('tableName' java.lang.String), D('uid' java.lang.String)] */
        /* JADX INFO: Multiple debug info for r8v7 org.w3c.dom.Element: [D('url' java.lang.String), D('api' org.w3c.dom.Element)] */
        public static ArrayList<HighScore> getListByTableAndDeviceID(Context context, String tableName) {
            ArrayList<HighScore> list = new ArrayList<>();
            try {
                NodeList nodes = sendXMLRequest("http://api.androlib.com/api/pinball.ashx?myscore=1&di=" + DataManager.deviceid + "&t=" + URLEncoder.encode(tableName) + "&v=" + Common.getAppVersion(context)).getElementsByTagName("item");
                int n = nodes.getLength();
                for (int i = 0; i < n; i++) {
                    Element node = (Element) nodes.item(i);
                    list.add(new HighScore(Integer.valueOf(node.getElementsByTagName("Position").item(0).getFirstChild().getNodeValue()).intValue(), Integer.valueOf(node.getElementsByTagName("Score").item(0).getFirstChild().getNodeValue()).intValue(), node.getElementsByTagName("NickName").item(0).getFirstChild().getNodeValue(), node.getElementsByTagName("TableName").item(0).getFirstChild().getNodeValue(), false));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return list;
        }

        /* JADX INFO: Multiple debug info for r8v2 java.lang.String: [D('context' android.content.Context), D('level' java.lang.String)] */
        /* JADX INFO: Multiple debug info for r8v7 org.w3c.dom.Element: [D('url' java.lang.String), D('api' org.w3c.dom.Element)] */
        public static ArrayList<HighScore> getListByTable(Context context, String tableName) {
            ArrayList<HighScore> list = new ArrayList<>();
            try {
                NodeList nodes = sendXMLRequest("http://api.androlib.com/api/pinball.ashx?scorelist=1&t=" + URLEncoder.encode(tableName) + "&v=" + Common.getAppVersion(context)).getElementsByTagName("item");
                int n = nodes.getLength();
                for (int i = 0; i < n; i++) {
                    Element node = (Element) nodes.item(i);
                    list.add(new HighScore(Integer.valueOf(node.getElementsByTagName("Position").item(0).getFirstChild().getNodeValue()).intValue(), Integer.valueOf(node.getElementsByTagName("Score").item(0).getFirstChild().getNodeValue()).intValue(), node.getElementsByTagName("NickName").item(0).getFirstChild().getNodeValue(), node.getElementsByTagName("TableName").item(0).getFirstChild().getNodeValue(), false));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return list;
        }
    }
}
