package com.nix.game.pinball.free.controls;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.inmobi.androidsdk.impl.Constants;
import com.nix.game.pinball.free.TouchManager;
import com.nix.game.pinball.free.classes.Common;
import com.nix.game.pinball.free.objects.Table;

public final class GameView extends SurfaceView implements SurfaceHolder.Callback {
    private TouchManager tmngr;

    public GameView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        getHolder().setFixedSize(Constants.INMOBI_ADVIEW_WIDTH, 430);
        getHolder().addCallback(this);
        setFocusableInTouchMode(true);
        setBackgroundColor(0);
        setFocusable(true);
        requestFocus();
    }

    public TouchManager getTouchManager() {
        return this.tmngr;
    }

    public void setTouchManager(TouchManager v) {
        this.tmngr = v;
    }

    public GameView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GameView(Context context) {
        this(context, null);
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Display display = ((Activity) getContext()).getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        this.tmngr.setViewSize(display.getWidth(), display.getHeight() - ((int) (metrics.density * 50.0f)));
    }

    public void surfaceCreated(SurfaceHolder holder) {
        Table.surfaceReady = true;
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        Table.surfaceReady = false;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!Table.isLoaded()) {
            return true;
        }
        this.tmngr.onTouch(event);
        Common.Pause(30);
        return true;
    }
}
