package com.nix.game.pinball.free.controls;

import android.content.Context;
import android.preference.Preference;
import android.util.AttributeSet;
import com.nix.game.pinball.free.classes.Common;

public final class VersionPreference extends Preference {
    public VersionPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setSummary(Common.getAppVersion(context));
    }

    public VersionPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    /* access modifiers changed from: protected */
    public void onClick() {
        new WhatsNewDialog(getContext()).show(true);
    }
}
