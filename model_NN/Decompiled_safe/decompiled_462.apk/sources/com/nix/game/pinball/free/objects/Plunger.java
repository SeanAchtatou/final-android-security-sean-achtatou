package com.nix.game.pinball.free.objects;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.nix.game.pinball.free.managers.DataManager;
import java.io.DataInputStream;
import java.io.IOException;

public final class Plunger extends PinballObject {
    private float current;
    private Rect dst = new Rect();
    private Image image;
    private int length;
    private float px;
    private float py;
    private int ref1;
    private int sound;
    private float speed;
    private Rect src = new Rect();
    private int strength;
    private int vibrate;

    public Plunger(DataInputStream dis) throws IOException {
        super(dis);
        this.px = (float) dis.readShort();
        this.py = (float) dis.readShort();
        this.length = dis.readUnsignedByte();
        this.strength = dis.readUnsignedByte();
        this.speed = dis.readFloat();
        this.vibrate = dis.readShort();
        this.ref1 = dis.readInt();
        this.sound = dis.readInt();
        this.current = (float) this.length;
    }

    public void cycle() {
        if (Table.keyb[2]) {
            this.current -= this.speed;
            if (this.current < 0.0f) {
                this.current = 0.0f;
            }
            if (this.current > ((float) this.length)) {
                this.current = (float) this.length;
            }
        } else if (this.current < ((float) this.length)) {
            int r = 0;
            float s = (((float) this.length) - this.current) / ((float) this.length);
            int n = Table.balls.size();
            for (int i = 0; i < n; i++) {
                r += process(Table.balls.get(i), s);
            }
            if (r > 0) {
                if (this.sound != 0) {
                    DataManager.playSound(this.sound);
                }
                if (this.vibrate > 0) {
                    DataManager.vibrate((long) ((int) (((float) this.vibrate) * s)));
                }
            }
            this.current = (float) this.length;
        }
    }

    private int process(Ball b, float s) {
        if (Math.abs(b.p.x - this.px) >= 12.0f || Math.abs(b.p.y - this.py) >= 12.0f) {
            return 0;
        }
        b.v.x = 0.0f;
        b.v.y = ((float) (-this.strength)) * s;
        return 1;
    }

    public Ball newBall() {
        return new Ball(this.px, this.py, Table.ballradius, Table.imgBall);
    }

    public void draw(Canvas c) {
        if (this.image != null) {
            Bitmap bmp = this.image.image;
            int left = (int) (this.px - ((float) this.image.x));
            int top = (int) (((this.py + ((float) this.length)) - this.current) - ((float) this.image.y));
            int width = bmp.getWidth();
            int height = (int) (((float) bmp.getHeight()) - (((float) this.length) - this.current));
            this.src.set(0, 0, width, height);
            this.dst.set(left, top, left + width, top + height);
            c.drawBitmap(bmp, this.src, this.dst, (Paint) null);
        }
    }

    public void update() {
        this.image = (Image) Table.omap.get(Integer.valueOf(this.ref1));
    }
}
