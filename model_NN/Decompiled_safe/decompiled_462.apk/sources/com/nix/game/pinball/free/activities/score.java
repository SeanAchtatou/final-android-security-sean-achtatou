package com.nix.game.pinball.free.activities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.nix.game.pinball.free.R;
import com.nix.game.pinball.free.classes.HighScore;
import com.nix.game.pinball.free.classes.TopScoreAdapter;
import com.nix.game.pinball.free.managers.AdWhirlAdsLayout;
import com.nix.game.pinball.free.managers.DataManager;
import java.util.ArrayList;

public final class score extends Activity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public TopScoreAdapter adapter;
    /* access modifiers changed from: private */
    public ArrayList<HighScore> data;
    /* access modifiers changed from: private */
    public ArrayList<HighScore> dataAll;
    /* access modifiers changed from: private */
    public ArrayList<HighScore> dataUser;
    private AdWhirlAdsLayout ggads;
    /* access modifiers changed from: private */
    public Handler handler;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public LinearLayout layout;
    /* access modifiers changed from: private */
    public ListView list;
    /* access modifiers changed from: private */
    public String tableName;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        setContentView((int) R.layout.frm_score);
        findViewById(R.id.btn_MyScores).setOnClickListener(this);
        findViewById(R.id.btn_AllScores).setOnClickListener(this);
        this.ggads = (AdWhirlAdsLayout) findViewById(R.id.ggads);
        this.layout = (LinearLayout) findViewById(R.id.primary);
        this.tableName = getIntent().getStringExtra("table");
        this.inflater = LayoutInflater.from(this);
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 0:
                        score.this.adapter = new TopScoreAdapter(score.this, score.this.data);
                        score.this.list = new ListView(score.this);
                        score.this.list.setItemsCanFocus(false);
                        score.this.list.setAdapter((ListAdapter) score.this.adapter);
                        score.this.layout.removeAllViews();
                        score.this.layout.addView(score.this.list, new ViewGroup.LayoutParams(-1, -1));
                        return;
                    default:
                        return;
                }
            }
        };
        getAllScores();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.ggads.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.ggads.onResume();
    }

    private void setLoading() {
        this.layout.removeAllViews();
        this.layout.addView(this.inflater.inflate((int) R.layout.inc_loading, (ViewGroup) null), new ViewGroup.LayoutParams(-1, -1));
    }

    private void getAllScores() {
        setLoading();
        new Thread() {
            public void run() {
                if (score.this.dataAll == null) {
                    score.this.dataAll = HighScore.Helper.getListByTable(score.this, score.this.tableName);
                }
                score.this.data = score.this.dataAll;
                score.this.handler.sendEmptyMessage(0);
            }
        }.start();
    }

    private void getMyScores() {
        setLoading();
        new Thread() {
            public void run() {
                if (score.this.dataUser == null) {
                    score.this.dataUser = HighScore.Helper.getListByTableAndDeviceID(score.this, score.this.tableName);
                }
                score.this.data = score.this.dataUser;
                score.this.handler.sendEmptyMessage(0);
            }
        }.start();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_AllScores:
                DataManager.playSound(DataManager.SND_SELECT);
                getAllScores();
                return;
            case R.id.btn_MyScores:
                DataManager.playSound(DataManager.SND_SELECT);
                getMyScores();
                return;
            default:
                return;
        }
    }
}
