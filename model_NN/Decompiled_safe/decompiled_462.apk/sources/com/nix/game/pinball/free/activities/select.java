package com.nix.game.pinball.free.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.nix.game.pinball.free.R;
import com.nix.game.pinball.free.classes.TableInfo;
import com.nix.game.pinball.free.managers.AdWhirlAdsLayout;
import com.nix.game.pinball.free.managers.DataManager;
import com.nix.game.pinball.free.managers.GoogleAnalytics;
import java.util.ArrayList;

public final class select extends Activity implements View.OnClickListener, GestureDetector.OnGestureListener {
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private AdWhirlAdsLayout ggads;
    private Button mBtnNext;
    private Button mBtnPrev;
    private ImageView mImgNext;
    private ImageView mImgPrev;
    private Animation slideLeftIn;
    private Animation slideLeftOut;
    private Animation slideRightIn;
    private Animation slideRightOut;
    private ArrayList<TableInfo> tables;
    private ViewFlipper viewFlipper;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        setContentView((int) R.layout.frm_select);
        this.ggads = (AdWhirlAdsLayout) findViewById(R.id.ggads);
        this.mImgNext = (ImageView) findViewById(R.id.img_Next);
        this.mImgPrev = (ImageView) findViewById(R.id.img_Prev);
        this.mBtnPrev = (Button) findViewById(R.id.btn_Prev);
        this.mBtnNext = (Button) findViewById(R.id.btn_Next);
        this.mBtnNext.setOnClickListener(this);
        this.mBtnPrev.setOnClickListener(this);
        findViewById(R.id.btn_Scores).setOnClickListener(this);
        findViewById(R.id.select1).findViewById(R.id.primary).setOnClickListener(this);
        findViewById(R.id.select2).findViewById(R.id.primary).setOnClickListener(this);
        this.viewFlipper = (ViewFlipper) findViewById(R.id.flipper);
        this.slideLeftIn = AnimationUtils.loadAnimation(this, R.anim.slide_left_in);
        this.slideLeftOut = AnimationUtils.loadAnimation(this, R.anim.slide_left_out);
        this.slideRightIn = AnimationUtils.loadAnimation(this, R.anim.slide_right_in);
        this.slideRightOut = AnimationUtils.loadAnimation(this, R.anim.slide_right_out);
        this.tables = TableInfo.getTableInfo(this);
        bindView(DataManager.tableindex);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.ggads.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.ggads.onResume();
    }

    private void prevTable() {
        if (DataManager.tableindex > 0) {
            this.viewFlipper.setInAnimation(this.slideRightIn);
            this.viewFlipper.setOutAnimation(this.slideRightOut);
            this.viewFlipper.showPrevious();
            bindView(DataManager.tableindex - 1);
        }
    }

    private void nextTable() {
        if (DataManager.tableindex < this.tables.size() - 1) {
            this.viewFlipper.setInAnimation(this.slideLeftIn);
            this.viewFlipper.setOutAnimation(this.slideLeftOut);
            this.viewFlipper.showNext();
            bindView(DataManager.tableindex + 1);
        }
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if (Math.abs(e1.getY() - e2.getY()) > 250.0f) {
            return false;
        }
        if (e1.getX() - e2.getX() > 120.0f && Math.abs(velocityX) > 200.0f) {
            nextTable();
        } else if (e2.getX() - e1.getX() > 120.0f && Math.abs(velocityX) > 200.0f) {
            prevTable();
        }
        return false;
    }

    public boolean onDown(MotionEvent arg0) {
        return false;
    }

    public void onLongPress(MotionEvent e) {
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    public void onShowPress(MotionEvent e) {
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    private void bindView(int value) {
        boolean z;
        boolean z2;
        int i;
        int i2;
        if (this.tables.size() > 0) {
            int max = this.tables.size() - 1;
            int n = value;
            if (n < 0) {
                n = 0;
            }
            if (n > max) {
                n = max;
            }
            Button button = this.mBtnPrev;
            if (n != 0) {
                z = true;
            } else {
                z = false;
            }
            button.setEnabled(z);
            Button button2 = this.mBtnNext;
            if (n != max) {
                z2 = true;
            } else {
                z2 = false;
            }
            button2.setEnabled(z2);
            ImageView imageView = this.mImgPrev;
            if (n == 0) {
                i = 4;
            } else {
                i = 0;
            }
            imageView.setVisibility(i);
            ImageView imageView2 = this.mImgNext;
            if (n == max) {
                i2 = 4;
            } else {
                i2 = 0;
            }
            imageView2.setVisibility(i2);
            View view = this.viewFlipper.getCurrentView();
            TableInfo info = this.tables.get(n);
            ImageView imgPrev = (ImageView) view.findViewById(R.id.primary);
            if (info.getPreview() != null) {
                imgPrev.setImageBitmap(info.getPreview());
            } else {
                imgPrev.setImageResource(R.drawable.imgpreview);
            }
            TextView txtSdcard = (TextView) view.findViewById(R.id.text4);
            if (info.isOnSDCard()) {
                txtSdcard.setVisibility(0);
            } else {
                txtSdcard.setVisibility(4);
            }
            ((TextView) view.findViewById(R.id.text3)).setText(info.getName());
            DataManager.tableindex = n;
            DataManager.selectedTable = info;
        }
    }

    private void startNewGame() {
        GoogleAnalytics.trackAndDispatch("New-" + DataManager.selectedTable.getName());
        startActivity(new Intent(this, game.class));
    }

    private void startHighScore() {
        String tableName = DataManager.selectedTable.getName();
        GoogleAnalytics.trackAndDispatch("Score-" + tableName);
        Intent intent = new Intent(this, score.class);
        intent.putExtra("table", tableName);
        startActivity(intent);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.primary:
                DataManager.playSound(DataManager.SND_SELECT);
                startNewGame();
                return;
            case R.id.btn_Prev:
                DataManager.playSound(DataManager.SND_SELECT);
                prevTable();
                return;
            case R.id.btn_Scores:
                DataManager.playSound(DataManager.SND_SELECT);
                startHighScore();
                return;
            case R.id.btn_Next:
                DataManager.playSound(DataManager.SND_SELECT);
                nextTable();
                return;
            default:
                return;
        }
    }
}
