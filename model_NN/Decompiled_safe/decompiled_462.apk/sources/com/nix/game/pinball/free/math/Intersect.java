package com.nix.game.pinball.free.math;

import com.nix.game.pinball.free.classes.Point;
import com.nix.game.pinball.free.classes.Segment;
import com.nix.game.pinball.free.managers.ScriptManager;
import com.nix.game.pinball.free.objects.Ball;

public final class Intersect {
    static Vec2 d = new Vec2();

    public static float isLeft2(Vec2 p, Vec2 a, Vec2 b) {
        return ((b.x - a.x) * (p.y - a.y)) - ((p.x - a.x) * (b.y - a.y));
    }

    public static final boolean PointInPie(Vec2 c, Vec2 p, Vec2 a, Vec2 b, float r) {
        float dx = c.x - p.x;
        float dy = c.y - p.y;
        if (((float) Math.sqrt((double) ((dx * dx) + (dy * dy)))) > r) {
            return false;
        }
        if (isLeft2(c, p, a) <= 0.0f || isLeft2(c, p, b) >= 0.0f) {
            return false;
        }
        return true;
    }

    public static final boolean SegmentSegmentIntersect(Vec2 a1, Vec2 a2, Vec2 b1, Vec2 b2, Vec2 p) {
        float dx = a2.x - a1.x;
        float dy = a2.y - a1.y;
        float da = b2.x - b1.x;
        float db = b2.y - b1.y;
        if ((da * dy) - (db * dx) != 0.0f) {
            float s = (((b1.y - a1.y) * dx) + ((a1.x - b1.x) * dy)) / ((da * dy) - (db * dx));
            float t = (((a1.y - b1.y) * da) + ((b1.x - a1.x) * db)) / ((db * dx) - (da * dy));
            if (s >= 0.0f && s <= 1.0f && t >= 0.0f && t <= 1.0f) {
                p.x = a1.x + ((a2.x - a1.x) * t);
                p.y = a1.y + ((a2.y - a1.y) * t);
                return true;
            }
        }
        return false;
    }

    public static final float DistToSegment(Vec2 p, Vec2 a, Vec2 b, Vec2 c) {
        float dx;
        float dy;
        float dx2 = b.x - a.x;
        float dy2 = b.y - a.y;
        if (dx2 == 0.0f && dy2 == 0.0f) {
            c.x = a.x;
            c.y = a.y;
            dx = p.x - a.x;
            dy = p.y - a.y;
        } else {
            float t = (((p.x - a.x) * dx2) + ((p.y - a.y) * dy2)) / ((dx2 * dx2) + (dy2 * dy2));
            if (t < 0.0f) {
                c.x = a.x;
                c.y = a.y;
                dx = p.x - a.x;
                dy = p.y - a.y;
            } else if (t > 1.0f) {
                c.x = b.x;
                c.y = b.y;
                dx = p.x - b.x;
                dy = p.y - b.y;
            } else {
                c.x = a.x + (dx2 * t);
                c.y = a.y + (dy2 * t);
                dx = p.x - c.x;
                dy = p.y - c.y;
            }
        }
        return (float) Math.sqrt((double) ((dx * dx) + (dy * dy)));
    }

    /* JADX INFO: Multiple debug info for r3v2 float: [D('acx' float), D('acab' float)] */
    /* JADX INFO: Multiple debug info for r0v6 float: [D('ab2' float), D('t' float)] */
    /* JADX INFO: Multiple debug info for r0v9 float: [D('t' float), D('hx' float)] */
    /* JADX INFO: Multiple debug info for r7v2 float: [D('c' com.nix.game.pinball.free.math.Vec2), D('hy' float)] */
    /* JADX INFO: Multiple debug info for r7v4 float: [D('h2' float), D('hy' float)] */
    /* JADX INFO: Multiple debug info for r8v1 float: [D('r' float), D('r2' float)] */
    /* JADX INFO: Multiple debug info for r10v6 float: [D('b' com.nix.game.pinball.free.math.Vec2), D('abx' float)] */
    /* JADX INFO: Multiple debug info for r9v6 float: [D('aby' float), D('a' com.nix.game.pinball.free.math.Vec2)] */
    public static boolean CircleSegmentIntersect(Vec2 c, float r, Vec2 a, Vec2 b, Vec2 p) {
        float aby;
        float abx;
        float acx = c.x - a.x;
        float acy = c.y - a.y;
        float abx2 = b.x - a.x;
        float aby2 = b.y - a.y;
        float t = ((acx * abx2) + (acy * aby2)) / ((abx2 * abx2) + (aby2 * aby2));
        if (t < 0.0f) {
            float abx3 = a.x;
            float aby3 = a.y;
            aby = abx3;
            abx = aby3;
        } else if (t > 1.0f) {
            aby = b.x;
            abx = b.y;
        } else {
            float abx4 = a.x + (abx2 * t);
            float aby4 = a.y + (t * aby2);
            aby = abx4;
            abx = aby4;
        }
        float hx = aby - c.x;
        float hy = abx - c.y;
        float hy2 = hy * hy;
        p.x = aby;
        p.y = abx;
        if (hy2 + (hx * hx) > r * r) {
            return false;
        }
        return true;
    }

    public static boolean processCollision(Segment l, Ball b, Vec2 p, float bounce) {
        if ((p.x == l.a.x && p.y == l.a.y) || (p.x == l.b.x && p.y == l.b.y)) {
            return false;
        }
        b.p.x = l.n.x * ((float) b.r);
        b.p.y = l.n.y * ((float) b.r);
        b.p.x += p.x;
        b.p.y += p.y;
        if (bounce != 0.0f) {
            float s = b.v.length() * bounce;
            d.x = l.n.x * s;
            d.y = l.n.y * s;
        } else {
            d.x = l.n.x * 1.0f;
            d.y = l.n.y * 1.0f;
        }
        b.v.x += d.x;
        b.v.y += d.y;
        b.bind();
        return true;
    }

    public static void processCollision2(Point l1, Point l2, Ball b, Vec2 p) {
        if (p.x != l1.p.x || p.y != l1.p.y) {
            if (p.x != l2.p.x || p.y != l2.p.y) {
                b.p.x = (l1.n.x * ((float) b.r)) + p.x;
                b.p.y = (l1.n.y * ((float) b.r)) + p.y;
                if (l1.bounce != 0.0f) {
                    float s = b.v.length() * l1.bounce;
                    d.x = (l1.n.x * s) - b.v.x;
                    d.y = (l1.n.y * s) - b.v.y;
                } else {
                    float s2 = b.v.length();
                    d.x = l1.n.x * s2 * 0.31830987f;
                    d.y = l1.n.y * s2 * 0.31830987f;
                }
                b.v.x += d.x;
                b.v.y += d.y;
                b.bind();
                if (l1.OnTouchEvent != 0) {
                    ScriptManager.call(l1.OnTouchEvent, b);
                }
            }
        }
    }
}
