package com.nix.game.pinball.free.managers;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlTargeting;
import com.adwhirl.adapters.AdWhirlAdapter;
import com.adwhirl.obj.Extra2;
import com.google.ads.AdSenseSpec;

public class AdWhirlAdsLayout extends LinearLayout {
    private static final String ADWHIRL_ID = "2dde0147200e41f680231c7d8be84cc8";
    private static final int DIP_HEIGHT = 50;
    private static final String GGADS_APP_NAME = "Pinball";
    private static final String GGADS_CHANNEL_ID = "9200833115";
    private static final String GGADS_COMPANY_NAME = "Magma Mobile";
    private static final String GGADS_EXPAND_DIRECTION = "BOTTOM";
    private static final String GGADS_KEYWORDS = "pinball+game";
    private static final int KREACTIVE_APP_ID = 183;
    private static final String LINK_MARKET_CUSTOM = "-Pinball";
    private AdWhirlLayout adWhirlLayout;
    private LinearLayout.LayoutParams mParams;

    static {
        Extra2.extraLinkMarketCustom = LINK_MARKET_CUSTOM;
        Extra2.kreativeAppID = KREACTIVE_APP_ID;
        Extra2.verboselog = false;
        Extra2.googleTestMode = false;
    }

    public AdWhirlAdsLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mParams = new LinearLayout.LayoutParams(-1, (int) (50.0f * getResources().getDisplayMetrics().density));
        AdWhirlAdapter.setGoogleAdSenseCompanyName(GGADS_COMPANY_NAME);
        AdWhirlAdapter.setGoogleAdSenseAppName(GGADS_APP_NAME);
        AdWhirlAdapter.setGoogleAdSenseChannel(GGADS_CHANNEL_ID);
        AdWhirlAdapter.setGoogleAdSenseExpandDirection(GGADS_EXPAND_DIRECTION);
        AdWhirlTargeting.setKeywords(GGADS_KEYWORDS);
        this.adWhirlLayout = new AdWhirlLayout((Activity) context, ADWHIRL_ID);
        addView(this.adWhirlLayout, this.mParams);
    }

    public AdWhirlAdsLayout(Context context) {
        this(context, null);
    }

    public void setAdsenseAdType(AdSenseSpec.AdType type) {
        this.adWhirlLayout.adsenseAdType = type;
    }

    public AdSenseSpec.AdType getAdsenseAdType() {
        return this.adWhirlLayout.adsenseAdType;
    }

    public void onResume() {
        if (this.adWhirlLayout != null) {
            removeAllViews();
            addView(this.adWhirlLayout, this.mParams);
        }
    }

    public void onPause() {
        removeAllViews();
    }
}
