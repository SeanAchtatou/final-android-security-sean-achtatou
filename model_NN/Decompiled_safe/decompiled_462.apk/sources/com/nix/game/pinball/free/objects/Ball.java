package com.nix.game.pinball.free.objects;

import android.graphics.Canvas;
import android.graphics.Paint;
import com.nix.game.pinball.free.math.Vec2;
import java.util.ArrayList;

public class Ball extends PinballObject {
    public Vec2 c;
    public boolean enabled;
    public Image image;
    public Vec2 p;
    public Vec2 q;
    public int r;
    public Vec2 v = new Vec2(0.0f, 0.0f);
    public int wait;
    public short waitflag;
    public int z;

    public Ball(float x, float y, int r2, Image img) {
        super(0);
        this.p = new Vec2(x, y);
        this.q = new Vec2(x, y);
        this.c = new Vec2();
        this.enabled = true;
        this.z = 0;
        this.r = r2;
        this.image = img;
    }

    public void draw(Canvas c2) {
        if (this.image != null) {
            c2.drawBitmap(this.image.image, this.p.x - ((float) this.image.x), this.p.y - ((float) this.image.y), (Paint) null);
        }
    }

    public void cycle() {
        if (this.wait > 0) {
            this.wait--;
            if (this.wait <= 0) {
                switch (this.waitflag) {
                    case 1:
                        this.enabled = false;
                        return;
                    default:
                        return;
                }
            }
        } else {
            this.q.x = this.p.x;
            this.q.y = this.p.y;
            this.v.x += Table.gravityx;
            this.v.y += Table.gravityy;
            if (this.v.length() > Table.maxspeed) {
                this.v.normalize();
                this.v.scale(Table.maxspeed);
            }
            this.p.x += this.v.x;
            this.p.y += this.v.y;
            if (this.p.x + ((float) this.r) < 0.0f || this.p.x - ((float) this.r) > 320.0f || this.p.y + ((float) this.r) < 0.0f || this.p.y - ((float) this.r) > 480.0f) {
                this.wait = 20;
                this.waitflag = 1;
            }
            ArrayList<Gate> arr4 = Table.gates;
            int n = arr4.size();
            for (int i = 0; i < n; i++) {
                Gate k = arr4.get(i);
                if (k.visible) {
                    k.Process(this, this.c);
                }
            }
            ArrayList<Wall> arr5 = Table.walls;
            int n2 = arr5.size();
            for (int i2 = 0; i2 < n2; i2++) {
                Wall k2 = arr5.get(i2);
                if (k2.visible) {
                    k2.Process(this, this.c);
                }
            }
            ArrayList<Bumper> arr1 = Table.bumpers;
            int n3 = arr1.size();
            for (int i3 = 0; i3 < n3; i3++) {
                arr1.get(i3).Process(this, this.c);
            }
            ArrayList<Kicker> arr2 = Table.kickers;
            int n4 = arr2.size();
            for (int i4 = 0; i4 < n4; i4++) {
                arr2.get(i4).Process(this, this.c);
            }
            ArrayList<Trigger> arr3 = Table.triggers;
            int n5 = arr3.size();
            for (int i5 = 0; i5 < n5; i5++) {
                arr3.get(i5).Process(this, this.c);
            }
        }
    }

    public void StopAtXY(float cx, float cy) {
        this.p.x = cx;
        this.p.y = cy;
        this.q.x = cx;
        this.q.y = cy;
        this.v.x = 0.0f;
        this.v.y = 0.0f;
    }

    public void bind() {
        if (this.v.length() > Table.maxspeed) {
            this.v.normalize();
            this.v.scale(Table.maxspeed);
        }
    }
}
