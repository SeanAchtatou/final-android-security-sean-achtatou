package com.nix.game.pinball.free.math;

public final class Vec2 {
    public static final float MIPI = 1.5707964f;
    public static final float PI = 3.1415927f;
    public float x;
    public float y;

    public Vec2() {
        this.x = 0.0f;
        this.y = 0.0f;
    }

    public Vec2(float x2, float y2) {
        this.x = x2;
        this.y = y2;
    }

    public Vec2(Vec2 v, float a, int l) {
        this.x = (float) (((double) v.x) + (Math.cos((double) a) * ((double) l)));
        this.y = (float) (((double) v.y) + (Math.sin((double) a) * ((double) l)));
    }

    public final void add(Vec2 v) {
        this.x += v.x;
        this.y += v.y;
    }

    public final void sub(Vec2 v) {
        this.x -= v.x;
        this.y -= v.y;
    }

    public final void mul(Vec2 v) {
        this.x *= v.x;
        this.y *= v.y;
    }

    public final void div(Vec2 v) {
        this.x /= v.x;
        this.y /= v.y;
    }

    public final void scale(float s) {
        this.x *= s;
        this.y *= s;
    }

    public final float dot(Vec2 v) {
        return (this.x * v.x) + (this.y * v.y);
    }

    public final float length() {
        return (float) Math.sqrt((double) ((this.x * this.x) + (this.y * this.y)));
    }

    public final void normalize() {
        float n = length();
        if (n != 0.0f) {
            float n2 = 1.0f / n;
            this.x *= n2;
            this.y *= n2;
            return;
        }
        this.x = 0.0f;
        this.y = 0.0f;
    }

    public final void rotatez(float a) {
        float cosa = (float) Math.cos((double) a);
        float sina = (float) Math.sin((double) a);
        float tx = this.x;
        float ty = this.y;
        this.x = (tx * cosa) - (ty * sina);
        this.y = (tx * sina) + (ty * cosa);
    }

    public final void cross(Vec2 v) {
        float tx = v.x;
        this.x = v.y - v.y;
        this.y = v.x - tx;
    }

    public final void lerp(Vec2 v, float s) {
        this.x += (v.x - this.x) * s;
        this.y += (v.y - this.y) * s;
    }

    public static final void scale(Vec2 vout, Vec2 v, float s) {
        vout.x = v.x * s;
        vout.y = v.y * s;
    }

    public static final void substract(Vec2 vout, Vec2 v1, Vec2 v2) {
        vout.x = v1.x - v2.x;
        vout.y = v1.y - v2.y;
    }

    public static final void add(Vec2 vout, Vec2 v1, Vec2 v2) {
        vout.x = v1.x + v2.x;
        vout.y = v1.y + v2.y;
    }

    public static final void normalize(Vec2 vout, Vec2 v) {
        float nx = v.x;
        float ny = v.y;
        float s = (float) Math.sqrt((double) ((nx * nx) + (ny * ny)));
        if (s != 0.0f) {
            float s2 = 1.0f / s;
            vout.x *= s2;
            vout.y *= s2;
            return;
        }
        vout.x = 0.0f;
        vout.y = 0.0f;
    }

    public static final void normal(Vec2 vout, Vec2 v1, Vec2 v2) {
        float ax = v2.x - v1.x;
        float nx = v2.y - v1.y;
        float ny = -ax;
        float s = (float) Math.sqrt((double) ((nx * nx) + (ny * ny)));
        if (s != 0.0f) {
            float s2 = 1.0f / s;
            vout.x = nx * s2;
            vout.y = ny * s2;
            return;
        }
        vout.x = 0.0f;
        vout.y = 0.0f;
    }

    public static final void lerp(Vec2 vout, Vec2 v1, Vec2 v2, float s) {
        vout.x = v1.x + ((v2.x - v1.x) * s);
        vout.y = v1.y + ((v2.y - v1.y) * s);
    }

    public static final float dot(Vec2 v1, Vec2 v2) {
        return (v1.x * v2.x) + (v1.y * v2.y);
    }

    public static final void cross(Vec2 vout, Vec2 v1, Vec2 v2) {
        vout.x = v1.y - v2.y;
        vout.y = v2.x - v1.x;
    }
}
