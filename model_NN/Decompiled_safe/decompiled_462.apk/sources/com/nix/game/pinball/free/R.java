package com.nix.game.pinball.free;

public final class R {

    public static final class anim {
        public static final int slide_left_in = 2130968576;
        public static final int slide_left_out = 2130968577;
        public static final int slide_right_in = 2130968578;
        public static final int slide_right_out = 2130968579;
    }

    public static final class array {
        public static final int prefLocal = 2131230720;
        public static final int prefLocalValues = 2131230721;
    }

    public static final class attr {
        public static final int showUpdate = 2130771968;
    }

    public static final class color {
        public static final int button = 2131361792;
    }

    public static final class drawable {
        public static final int btn = 2130837504;
        public static final int btn_normal = 2130837505;
        public static final int btn_pressed = 2130837506;
        public static final int btn_selected = 2130837507;
        public static final int chevronl = 2130837508;
        public static final int chevronr = 2130837509;
        public static final int gradient = 2130837510;
        public static final int icon = 2130837511;
        public static final int imggameover = 2130837512;
        public static final int imglogo = 2130837513;
        public static final int imgpause = 2130837514;
        public static final int imgpreview = 2130837515;
        public static final int mmusiaicon = 2130837516;
        public static final int slprofil = 2130837517;
        public static final int sound = 2130837518;
        public static final int soundbar = 2130837519;
        public static final int title_bar = 2130837520;
    }

    public static final class id {
        public static final int btnAskNo = 2131427330;
        public static final int btnAskYes = 2131427329;
        public static final int btnHelp = 2131427336;
        public static final int btnNew = 2131427334;
        public static final int btnOptions = 2131427335;
        public static final int btnOtherApps = 2131427337;
        public static final int btnUpdate = 2131427343;
        public static final int btn_AllScores = 2131427339;
        public static final int btn_MyScores = 2131427340;
        public static final int btn_Next = 2131427351;
        public static final int btn_Prev = 2131427349;
        public static final int btn_Scores = 2131427350;
        public static final int chkNeverAskAgain = 2131427331;
        public static final int flipper = 2131427344;
        public static final int frmMenu = 2131427333;
        public static final int ggads = 2131427352;
        public static final int img_Next = 2131427348;
        public static final int img_Prev = 2131427347;
        public static final int layout = 2131427332;
        public static final int loading = 2131427354;
        public static final int overlay = 2131427355;
        public static final int primary = 2131427338;
        public static final int primarycontainer = 2131427359;
        public static final int select1 = 2131427345;
        public static final int select2 = 2131427346;
        public static final int text1 = 2131427353;
        public static final int text2 = 2131427356;
        public static final int text3 = 2131427357;
        public static final int text4 = 2131427358;
        public static final int txtAskRating = 2131427328;
        public static final int txtEmail = 2131427342;
        public static final int txtName = 2131427341;
    }

    public static final class layout {
        public static final int ask4rate = 2130903040;
        public static final int dlg_changelog = 2130903041;
        public static final int frm_game = 2130903042;
        public static final int frm_menu = 2130903043;
        public static final int frm_score = 2130903044;
        public static final int frm_scoreloop = 2130903045;
        public static final int frm_select = 2130903046;
        public static final int inc_ggad = 2130903047;
        public static final int inc_keyb = 2130903048;
        public static final int inc_loading = 2130903049;
        public static final int inc_overlay = 2130903050;
        public static final int inc_scoreitem = 2130903051;
        public static final int inc_select = 2130903052;
        public static final int inc_surface = 2130903053;
    }

    public static final class raw {
        public static final int log = 2131099648;
    }

    public static final class string {
        public static final int app_name = 2131165184;
        public static final int ask4rate_checkbox = 2131165186;
        public static final int ask4rate_later = 2131165189;
        public static final int ask4rate_text = 2131165187;
        public static final int ask4rate_yes = 2131165188;
        public static final int cmnCancel = 2131165200;
        public static final int cmnClose = 2131165203;
        public static final int cmnDisabled = 2131165202;
        public static final int cmnEnabled = 2131165201;
        public static final int cmnExit = 2131165204;
        public static final int cmnHelp = 2131165208;
        public static final int cmnMrkErr = 2131165205;
        public static final int cmnNext = 2131165207;
        public static final int cmnNo = 2131165198;
        public static final int cmnOk = 2131165199;
        public static final int cmnOther = 2131165209;
        public static final int cmnPrev = 2131165206;
        public static final int cmnYes = 2131165197;
        public static final int errEmpty = 2131165253;
        public static final int lstName = 2131165252;
        public static final int lstPos = 2131165249;
        public static final int lstScore = 2131165250;
        public static final int lstTable = 2131165251;
        public static final int menunew = 2131165194;
        public static final int menuoption = 2131165195;
        public static final int menuquit = 2131165196;
        public static final int menusplash = 2131165193;
        public static final int optAccel = 2131165218;
        public static final int optHighScores = 2131165225;
        public static final int optKeyConfig = 2131165215;
        public static final int optKeyboard = 2131165214;
        public static final int optLanguage = 2131165213;
        public static final int optMisc = 2131165219;
        public static final int optMultitouch = 2131165192;
        public static final int optNick = 2131165223;
        public static final int optOther = 2131165220;
        public static final int optScore = 2131165224;
        public static final int optSound = 2131165212;
        public static final int optUpdate = 2131165221;
        public static final int optVersion = 2131165222;
        public static final int optVibrate = 2131165217;
        public static final int optVolume = 2131165216;
        public static final int res_about = 2131165185;
        public static final int res_mmusia = 2131165190;
        public static final int res_mmusia_menu = 2131165191;
        public static final int sumBetaVer = 2131165226;
        public static final int sumHighScores = 2131165237;
        public static final int sumHowToPlay = 2131165239;
        public static final int sumKeyA = 2131165244;
        public static final int sumKeyB = 2131165245;
        public static final int sumKeyC = 2131165246;
        public static final int sumKeyConf = 2131165229;
        public static final int sumKeyCurr = 2131165232;
        public static final int sumKeyD = 2131165247;
        public static final int sumKeyE = 2131165248;
        public static final int sumKeybOff = 2131165231;
        public static final int sumKeybOn = 2131165230;
        public static final int sumLanguage = 2131165233;
        public static final int sumNick = 2131165236;
        public static final int sumOther = 2131165234;
        public static final int sumSelTable = 2131165227;
        public static final int sumShowHelp = 2131165240;
        public static final int sumSubmit = 2131165242;
        public static final int sumTouchScrn = 2131165238;
        public static final int sumUpdate = 2131165235;
        public static final int sumVolume = 2131165228;
        public static final int sumWait = 2131165241;
        public static final int sumWhatsNew = 2131165243;
        public static final int txtAllScores = 2131165210;
        public static final int txtMyScores = 2131165211;
    }

    public static final class style {
        public static final int TopScore1 = 2131296256;
        public static final int TopScore2 = 2131296257;
        public static final int TopScore3 = 2131296258;
        public static final int TopScore4 = 2131296259;
    }

    public static final class styleable {
        public static final int[] MarketPreference = {R.attr.showUpdate};
        public static final int MarketPreference_showUpdate = 0;
    }

    public static final class xml {
        public static final int options = 2131034112;
    }
}
