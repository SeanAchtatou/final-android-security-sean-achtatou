package com.nix.game.pinball.free.classes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.widget.Toast;
import com.magmamobile.mmusia.activities.MMUSIAMoreGamesActivity;
import com.nix.game.pinball.free.R;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.UUID;

public final class Common {
    public static float getDensity(Activity context) {
        DisplayMetrics metrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.density;
    }

    public static final void showAlertDialog(Context context, String title, String message) {
        AlertDialog.Builder ad = new AlertDialog.Builder(context);
        ad.setMessage(message);
        ad.setTitle(title);
        ad.show();
    }

    public static final void showMoreGames(Activity context) {
        context.startActivityForResult(new Intent(context, MMUSIAMoreGamesActivity.class), 0);
    }

    public static final void launchMarket(Context context, boolean update) {
        try {
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(update ? "market://details?id=" + context.getPackageName() : "market://search?q=pub:\"Magma Mobile\"")));
        } catch (Exception e) {
            Toast.makeText(context, context.getResources().getString(R.string.cmnMrkErr), 1).show();
        }
    }

    public static final void Pause(long time) {
        try {
            Thread.sleep(time);
        } catch (Exception e) {
        }
    }

    public static final int min(int v1, int v2) {
        return v1 < v2 ? v1 : v2;
    }

    public static final int max(int v1, int v2) {
        return v1 > v2 ? v1 : v2;
    }

    public static final int minmax(int v0, int vmin, int vmax) {
        if (v0 < vmin) {
            return vmin;
        }
        return v0 > vmax ? vmax : v0;
    }

    public static final String getAppVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static final String getDeviceID(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        String deviceid = pref.getString("GUID", null);
        if (deviceid != null) {
            return deviceid;
        }
        String deviceid2 = Settings.System.getString(context.getContentResolver(), "android_id");
        if (deviceid2 != null) {
            return deviceid2;
        }
        String deviceid3 = UUID.randomUUID().toString();
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("GUID", deviceid3);
        editor.commit();
        return deviceid3;
    }

    public static final void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static final boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (String file : children) {
                if (!deleteDir(new File(dir, file))) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    public static final String readString(DataInputStream dis) throws IOException {
        int count = dis.readShort();
        byte[] bytes = new byte[count];
        dis.read(bytes, 0, count);
        return new String(bytes);
    }

    public static final String readString(ByteBuffer dis) throws IOException {
        int count = dis.getShort();
        byte[] bytes = new byte[count];
        dis.get(bytes, 0, count);
        return new String(bytes);
    }

    public static final void writeString(DataOutputStream dis, String string) throws IOException {
        byte[] bytes = string.getBytes();
        int count = bytes.length;
        dis.writeShort(count);
        dis.write(bytes, 0, count);
    }

    public static final float radian(int degree) {
        return ((float) degree) * 0.017453292f;
    }

    public static final int degree(float radian) {
        return (int) (57.29578f * radian);
    }

    public static final void fixed(int n, char[] bff) {
        int s = bff.length - 1;
        int next = 0;
        for (int j = 0; j <= s; j++) {
            bff[j] = 0;
        }
        if (n == 0) {
            int next2 = 0 + 1;
            bff[s - 0] = '0';
            return;
        }
        while (n > 0) {
            int r = n % 10;
            n /= 10;
            int i = s - next;
            next++;
            bff[i] = (char) (r + 48);
        }
    }
}
