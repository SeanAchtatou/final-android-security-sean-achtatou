package com.inmobi.androidsdk.impl;

import com.millennialmedia.android.MMAdView;
import java.io.IOException;
import java.io.Reader;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public final class XMLParser {
    private AdUnit ad;

    public AdUnit buildAdUnitFromResponseData(Reader reader) throws IOException, AdConstructionException {
        this.ad = new AdUnit();
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(reader);
            int eventType = xpp.getEventType();
            String startName = null;
            while (eventType != 1) {
                if (eventType != 0) {
                    if (eventType == 2) {
                        startName = xpp.getName();
                        if (startName != null && startName.equalsIgnoreCase("Ad")) {
                            this.ad.setWidth(Integer.parseInt(xpp.getAttributeValue(null, MMAdView.KEY_WIDTH)));
                            this.ad.setHeight(Integer.parseInt(xpp.getAttributeValue(null, MMAdView.KEY_HEIGHT)));
                            this.ad.setAdActionType(AdUnit.adActionTypefromString(xpp.getAttributeValue(null, "actionName")));
                            this.ad.setAdType(AdUnit.adTypefromString(xpp.getAttributeValue(null, "type")));
                        }
                    } else if (eventType == 3) {
                        startName = null;
                    } else if (eventType == 5) {
                        this.ad.setCDATABlock(xpp.getText());
                    } else if (eventType == 4 && startName != null) {
                        if (startName.equalsIgnoreCase("AdURL")) {
                            this.ad.setTargetUrl(xpp.getText());
                            this.ad.setDefaultTargetUrl(xpp.getText());
                        }
                    }
                }
                try {
                    eventType = xpp.nextToken();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return this.ad;
        } catch (XmlPullParserException e2) {
            throw new AdConstructionException("Exception constructing Ad", e2);
        }
    }
}
