package com.inmobi.androidsdk.impl;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import com.inmobi.androidsdk.impl.AdUnit;
import com.inmobi.androidsdk.impl.net.HttpRequestBuilder;
import com.inmobi.androidsdk.impl.net.RequestResponseManager;
import java.util.ArrayList;
import java.util.List;

class ClickProcessingTask extends AsyncTask<Void, Void, String> {
    private final InMobiAdView adView;
    private final AdUnit adunit;
    private final Context appCtx;
    private String encoding = "UTF-8";
    private final UserInfo uInfo;

    public ClickProcessingTask(InMobiAdView adView2, AdUnit adunit2, UserInfo uInfo2, Context appCtx2) {
        this.adView = adView2;
        this.adunit = adunit2;
        this.uInfo = uInfo2;
        this.appCtx = appCtx2;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(Void... params) {
        String targetRedirectUrl = null;
        try {
            RequestResponseManager requestResponseManager = new RequestResponseManager(this.appCtx);
            List<String> arrayData = new ArrayList<>();
            arrayData.add("x-mkhoj-adactiontype");
            arrayData.add(this.adunit.getAdActionType().toString());
            targetRedirectUrl = requestResponseManager.initiateClick(this.adunit.getTargetUrl(), this.uInfo, arrayData);
            String newAdActionType = requestResponseManager.getNewAdActionType();
            if (newAdActionType != null) {
                this.adunit.setAdActionType(AdUnit.adActionTypefromString(newAdActionType));
            }
            String newEncoding = requestResponseManager.getEncodingType();
            if (newEncoding != null) {
                this.encoding = newEncoding;
            }
        } catch (Exception e) {
            Log.w(Constants.LOGGING_TAG, "Encountered generic exception initiating click", e);
        }
        return targetRedirectUrl;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String targetRedirectUrl) {
        if (targetRedirectUrl != null) {
            finishAdClick(targetRedirectUrl);
            return;
        }
        InMobiAdView.setAdRedirectionInProgress(false);
        this.adView.setProcessingAdClick(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void finishAdClick(String targetUrl) {
        if (targetUrl != null) {
            String urlText = targetUrl;
            String extraData = null;
            String action = "android.intent.action.VIEW";
            try {
                AdUnit ad = this.adView.getCurrentAdUnit();
                if (ad.getAdActionType() == AdUnit.AdActionTypes.AdActionType_SMS) {
                    action = "android.intent.action.SENDTO";
                    int index = urlText.indexOf("&Body=", 0);
                    if (index > 0) {
                        extraData = urlText.substring("&Body=".length() + index);
                        urlText = urlText.substring(0, index);
                    }
                }
                Intent adActionIntent = new Intent(action, Uri.parse(urlText));
                adActionIntent.addFlags(268435456);
                if (ad.getAdActionType() == AdUnit.AdActionTypes.AdActionType_SMS) {
                    adActionIntent.putExtra("compose_mode", true);
                    if (extraData != null) {
                        adActionIntent.putExtra("sms_body", HttpRequestBuilder.getURLDecoded(extraData, this.encoding));
                    }
                }
                this.appCtx.startActivity(adActionIntent);
            } catch (ActivityNotFoundException e) {
                Log.e(Constants.LOGGING_TAG, "Operation could not be performed : " + targetUrl, e);
                InMobiAdView.setAdRedirectionInProgress(false);
                this.adView.setProcessingAdClick(false);
                return;
            } catch (Exception e2) {
                Log.e(Constants.LOGGING_TAG, "Error executing post click actions on URL : " + targetUrl, e2);
                InMobiAdView.setAdRedirectionInProgress(false);
                this.adView.setProcessingAdClick(false);
                return;
            } catch (Throwable th) {
                InMobiAdView.setAdRedirectionInProgress(false);
                this.adView.setProcessingAdClick(false);
                throw th;
            }
        }
        InMobiAdView.setAdRedirectionInProgress(false);
        this.adView.setProcessingAdClick(false);
    }
}
