package com.millennialmedia.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import com.millennialmedia.android.MMAdViewSDK;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class HandShake {
    static String apid = null;
    private static final String handShakeURL = "http://ads.mp.mydas.mobi/appConfigServlet?apid=";
    private static HandShake sharedInstance;
    LinkedHashMap<String, AdTypeHandShake> adTypeHandShakes = new LinkedHashMap<>();
    /* access modifiers changed from: private */
    public WeakReference<Context> contextRef;
    long deferredViewTimeout = 3600000;
    long handShakeCallback = 86400000;
    final Handler handler = new Handler();
    boolean kill = false;
    long lastHandShake;
    /* access modifiers changed from: private */
    public Runnable updateHandShakeRunnable = new Runnable() {
        public void run() {
            Context context = (Context) HandShake.this.contextRef.get();
            if (context != null) {
                HandShake.sharedHandShake(context);
            }
        }
    };

    static synchronized HandShake sharedHandShake(Context context) {
        HandShake handShake;
        synchronized (HandShake.class) {
            if (apid == null) {
                Log.e(MMAdViewSDK.SDKLOG, "No apid set for the handshake.");
                handShake = null;
            } else {
                if (sharedInstance == null) {
                    sharedInstance = new HandShake(context);
                } else if (System.currentTimeMillis() - sharedInstance.lastHandShake > sharedInstance.handShakeCallback) {
                    MMAdViewSDK.Log.d("Handshake expired, requesting new handshake from the server.");
                    sharedInstance = new HandShake(context);
                }
                handShake = sharedInstance;
            }
        }
        return handShake;
    }

    /* access modifiers changed from: package-private */
    public synchronized void didReceiveVideoXHeader(Context context, String adType) {
        AdTypeHandShake adTypeHandShake = this.adTypeHandShakes.get(adType);
        if (adTypeHandShake != null) {
            adTypeHandShake.didReceiveVideoXHeader(context, adType);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean canRequestVideo(Context context, String adType) {
        boolean z;
        AdTypeHandShake adTypeHandShake = this.adTypeHandShakes.get(adType);
        if (adTypeHandShake != null) {
            z = adTypeHandShake.canRequestVideo(context, adType);
        } else {
            z = true;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean canWatchVideoAd(Context context, String adType, String videoAd) {
        boolean z;
        AdTypeHandShake adTypeHandShake = this.adTypeHandShakes.get(adType);
        if (adTypeHandShake != null) {
            z = adTypeHandShake.canWatchVideoAd(context, adType, videoAd);
        } else {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public synchronized void updateLastVideoViewedTime(Context context, String adType) {
        AdTypeHandShake adTypeHandShake = this.adTypeHandShakes.get(adType);
        if (adTypeHandShake != null) {
            adTypeHandShake.updateLastVideoViewedTime(context, adType);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean isAdTypeDownloading(String adType) {
        boolean z;
        AdTypeHandShake adTypeHandShake = this.adTypeHandShakes.get(adType);
        if (adTypeHandShake != null) {
            z = adTypeHandShake.downloading;
        } else {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public synchronized void lockAdTypeDownload(String adType) {
        AdTypeHandShake adTypeHandShake = this.adTypeHandShakes.get(adType);
        if (adTypeHandShake != null) {
            adTypeHandShake.downloading = true;
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void unlockAdTypeDownload(String adType) {
        AdTypeHandShake adTypeHandShake = this.adTypeHandShakes.get(adType);
        if (adTypeHandShake != null) {
            adTypeHandShake.downloading = false;
        }
    }

    /* access modifiers changed from: package-private */
    public void overrideAdRefresh(MMAdView adView) {
        AdTypeHandShake adTypeHandShake;
        if (adView.adType != null && (adTypeHandShake = this.adTypeHandShakes.get(adView.adType)) != null && adTypeHandShake.refreshInterval != null) {
            adView.refreshInterval = Integer.parseInt(adTypeHandShake.refreshInterval);
        }
    }

    private HandShake(Context context) {
        this.contextRef = new WeakReference<>(context);
        if (!loadHandShake(context) || System.currentTimeMillis() - this.lastHandShake > this.handShakeCallback) {
            this.lastHandShake = System.currentTimeMillis();
            new Thread(new Runnable() {
                public void run() {
                    HttpGetRequest getRequest = new HttpGetRequest();
                    Context tempContext = (Context) HandShake.this.contextRef.get();
                    if (tempContext != null) {
                        try {
                            DisplayMetrics metrics = tempContext.getResources().getDisplayMetrics();
                            float scale = metrics.density;
                            int heightPixels = metrics.heightPixels;
                            int widthPixels = metrics.widthPixels;
                            StringBuilder handshakeUrlBuilder = new StringBuilder("&auid=" + URLEncoder.encode(HandShake.getAuid(tempContext), "UTF-8"));
                            if (Build.MODEL != null) {
                                handshakeUrlBuilder.append("&dm=" + URLEncoder.encode(Build.MODEL, "UTF-8"));
                            }
                            if (Build.VERSION.RELEASE != null) {
                                handshakeUrlBuilder.append("&dv=Android" + URLEncoder.encode(Build.VERSION.RELEASE, "UTF-8"));
                            }
                            handshakeUrlBuilder.append("&density=" + Float.toString(scale));
                            handshakeUrlBuilder.append("&hpx=" + heightPixels);
                            handshakeUrlBuilder.append("&wpx=" + widthPixels);
                            handshakeUrlBuilder.append("&mmisdk=" + URLEncoder.encode(MMAdViewSDK.SDKVER, "UTF-8"));
                            Locale locale = Locale.getDefault();
                            if (locale != null) {
                                handshakeUrlBuilder.append("&language=" + locale.getLanguage() + "&country=" + locale.getCountry());
                            }
                            HandShake.this.deserializeFromObj(HandShake.this.parseJson(HttpGetRequest.convertStreamToString(getRequest.get(HandShake.handShakeURL + HandShake.apid + handshakeUrlBuilder.toString()).getEntity().getContent())), tempContext);
                            HandShake.this.saveHandShake(tempContext);
                            HandShake.this.handler.postDelayed(HandShake.this.updateHandShakeRunnable, HandShake.this.handShakeCallback);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
        }
    }

    public static String getAuid(Context context) {
        String auid = "android_idandroid_id";
        TelephonyManager tm = (TelephonyManager) context.getSystemService("phone");
        if (tm != null) {
            try {
                auid = tm.getDeviceId();
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
        if (auid == null || auid.length() == 0) {
            auid = Settings.Secure.getString(context.getContentResolver(), "android_id");
        }
        if (auid == null || auid.length() == 0) {
            return "UNKNOWN";
        }
        return auid;
    }

    private class AdTypeHandShake {
        boolean downloading;
        long lastVideo = 0;
        String refreshInterval;
        long videoInterval = 0;

        AdTypeHandShake() {
        }

        /* access modifiers changed from: package-private */
        public void didReceiveVideoXHeader(Context context, String adType) {
            this.lastVideo = System.currentTimeMillis();
            save(context, adType);
        }

        /* access modifiers changed from: package-private */
        public boolean canRequestVideo(Context context, String adType) {
            MMAdViewSDK.Log.d("canRequestVideo() Current Time: " + System.currentTimeMillis() + " last video: " + (this.lastVideo / 1000) + " Diff: " + ((System.currentTimeMillis() - this.lastVideo) / 1000) + " Video interval: " + (this.videoInterval / 1000));
            if (System.currentTimeMillis() - this.lastVideo > this.videoInterval) {
                return true;
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean canWatchVideoAd(Context context, String adType, String videoAd) {
            AdDatabaseHelper db = new AdDatabaseHelper(context);
            long deferredViewStart = db.getDeferredViewStart(videoAd);
            db.close();
            if (System.currentTimeMillis() - deferredViewStart >= HandShake.this.deferredViewTimeout) {
                return false;
            }
            this.lastVideo = System.currentTimeMillis();
            save(context, adType);
            return true;
        }

        /* access modifiers changed from: package-private */
        public void updateLastVideoViewedTime(Context context, String adType) {
            this.lastVideo = System.currentTimeMillis();
            save(context, adType);
        }

        /* access modifiers changed from: package-private */
        public void deserializeFromObj(JSONObject adTypeObject) throws JSONException {
            if (adTypeObject != null) {
                try {
                    if (adTypeObject.has("videointerval")) {
                        this.videoInterval = adTypeObject.getLong("videointerval") * 1000;
                    }
                    if (adTypeObject.has("adrefresh")) {
                        this.refreshInterval = adTypeObject.getString("adrefresh");
                        if (this.refreshInterval.equalsIgnoreCase("sdk")) {
                            this.refreshInterval = null;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public boolean load(SharedPreferences settings, String adType) {
            boolean settingsFound = false | settings.contains("handshake_lastvideo_" + adType);
            if (settingsFound) {
                this.lastVideo = settings.getLong("handshake_lastvideo_" + adType, this.lastVideo);
            }
            boolean settingsFound2 = settingsFound | settings.contains("handshake_videointerval_" + adType);
            if (settingsFound2) {
                this.videoInterval = settings.getLong("handshake_videointerval_" + adType, this.videoInterval);
            }
            boolean settingsFound3 = settingsFound2 | settings.contains("handshake_adrefresh_" + adType);
            if (settingsFound3) {
                this.refreshInterval = settings.getString("handshake_adrefresh_" + adType, null);
            }
            return settingsFound3;
        }

        /* access modifiers changed from: package-private */
        public void loadLastVideo(Context context, String adType) {
            SharedPreferences settings = context.getSharedPreferences("MillennialMediaSettings", 0);
            if (settings != null && settings.contains("handshake_lastvideo_" + adType)) {
                this.lastVideo = settings.getLong("handshake_lastvideo_" + adType, this.lastVideo);
            }
        }

        /* access modifiers changed from: package-private */
        public void save(SharedPreferences.Editor editor, String adType) {
            editor.putLong("handshake_lastvideo_" + adType, this.lastVideo);
            editor.putLong("handshake_videointerval_" + adType, this.videoInterval);
            if (this.refreshInterval != null) {
                editor.putString("handshake_adrefresh_" + adType, this.refreshInterval);
            }
        }

        /* access modifiers changed from: package-private */
        public void save(Context context, String adType) {
            SharedPreferences.Editor editor = context.getSharedPreferences("MillennialMediaSettings", 0).edit();
            save(editor, adType);
            editor.commit();
        }
    }

    /* access modifiers changed from: private */
    public JSONObject parseJson(String jsonString) {
        MMAdViewSDK.Log.d("JSON String: " + jsonString);
        if (jsonString != null) {
            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                MMAdViewSDK.Log.v(jsonObject.toString());
                if (jsonObject.has("mmishake")) {
                    return jsonObject.getJSONObject("mmishake");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void deserializeFromObj(JSONObject handShakeObject, Context context) throws JSONException {
        JSONObject adTypesObject;
        JSONObject adTypeObject;
        JSONArray jsonArray;
        if (handShakeObject != null) {
            try {
                if (handShakeObject.has("errors") && (jsonArray = handShakeObject.getJSONArray("errors")) != null) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject errorObject = jsonArray.getJSONObject(i);
                        if (errorObject.has("type") && errorObject.has("message")) {
                            String message = errorObject.getString("message");
                            String type = errorObject.getString("type");
                            if (type.equalsIgnoreCase("log")) {
                                Log.e(MMAdViewSDK.SDKLOG, message);
                            } else if (type.equalsIgnoreCase("prompt")) {
                                final Context context2 = context;
                                final String str = message;
                                this.handler.post(new Runnable() {
                                    public void run() {
                                        try {
                                            final AlertDialog overlayDialog = new AlertDialog.Builder(context2).create();
                                            overlayDialog.setTitle("Error");
                                            overlayDialog.setMessage(str);
                                            overlayDialog.setButton(-3, "OK", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    overlayDialog.cancel();
                                                }
                                            });
                                            overlayDialog.show();
                                        } catch (WindowManager.BadTokenException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
                if (handShakeObject.has("adtypes") && (adTypesObject = handShakeObject.getJSONObject("adtypes")) != null) {
                    String[] adTypes = MMAdView.getAdTypes();
                    for (int i2 = 0; i2 < adTypes.length; i2++) {
                        if (adTypesObject.has(adTypes[i2]) && (adTypeObject = adTypesObject.getJSONObject(adTypes[i2])) != null) {
                            AdTypeHandShake adTypeHandShake = new AdTypeHandShake();
                            adTypeHandShake.deserializeFromObj(adTypeObject);
                            adTypeHandShake.loadLastVideo(context, adTypes[i2]);
                            this.adTypeHandShakes.put(adTypes[i2], adTypeHandShake);
                        }
                    }
                }
                if (handShakeObject.has("deferredviewtimeout")) {
                    this.deferredViewTimeout = handShakeObject.getLong("deferredviewtimeout") * 1000;
                }
                if (handShakeObject.has("kill")) {
                    this.kill = handShakeObject.getBoolean("kill");
                }
                if (handShakeObject.has("handshakecallback")) {
                    this.handShakeCallback = handShakeObject.getLong("handshakecallback") * 1000;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean loadHandShake(Context context) {
        SharedPreferences settings = context.getSharedPreferences("MillennialMediaSettings", 0);
        if (settings == null) {
            return false;
        }
        boolean settingsFound = false | settings.contains("handshake_deferredviewtimeout");
        if (settingsFound) {
            this.deferredViewTimeout = settings.getLong("handshake_deferredviewtimeout", this.deferredViewTimeout);
        }
        boolean settingsFound2 = settingsFound | settings.contains("handshake_kill");
        if (settingsFound2) {
            this.kill = settings.getBoolean("handshake_kill", this.kill);
        }
        boolean settingsFound3 = settingsFound2 | settings.contains("handshake_callback");
        if (settingsFound3) {
            this.handShakeCallback = settings.getLong("handshake_callback", this.handShakeCallback);
        }
        String[] adTypes = MMAdView.getAdTypes();
        for (int i = 0; i < adTypes.length; i++) {
            AdTypeHandShake adTypeHandShake = new AdTypeHandShake();
            if (adTypeHandShake.load(settings, adTypes[i])) {
                settingsFound3 = true;
                this.adTypeHandShakes.put(adTypes[i], adTypeHandShake);
            }
        }
        boolean settingsFound4 = settingsFound3 | settings.contains("handshake_lasthandshake");
        if (settingsFound4) {
            this.lastHandShake = settings.getLong("handshake_lasthandshake", this.lastHandShake);
        }
        if (settingsFound4 && System.currentTimeMillis() - this.lastHandShake < this.handShakeCallback) {
            this.handler.postDelayed(this.updateHandShakeRunnable, this.handShakeCallback - (System.currentTimeMillis() - this.lastHandShake));
        }
        if (settingsFound4) {
            MMAdViewSDK.Log.d("Handshake successfully loaded from shared preferences.");
        }
        return settingsFound4;
    }

    /* access modifiers changed from: private */
    public void saveHandShake(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences("MillennialMediaSettings", 0).edit();
        editor.putLong("handshake_deferredviewtimeout", this.deferredViewTimeout);
        editor.putBoolean("handshake_kill", this.kill);
        editor.putLong("handshake_callback", this.handShakeCallback);
        for (String adType : this.adTypeHandShakes.keySet()) {
            this.adTypeHandShakes.get(adType).save(editor, adType);
        }
        editor.putLong("handshake_lasthandshake", this.lastHandShake);
        editor.commit();
    }
}
