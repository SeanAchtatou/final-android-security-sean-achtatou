package com.millennialmedia.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.millennialmedia.android.MMAdViewSDK;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

class MMAdViewController {
    private static final HashMap<Long, MMAdViewController> controllers = new HashMap<>();
    /* access modifiers changed from: private */
    public WeakReference<MMAdView> adViewRef;
    private boolean appPaused;
    private Handler cacheHandler = new Handler();
    boolean canAccelerate;
    /* access modifiers changed from: private */
    public Handler handler;
    String nextUrl;
    String overlayTitle = "Advertisement";
    String overlayTransition = "bottomtotop";
    private boolean paused = true;
    private boolean refreshTimerOn;
    boolean requestInProgress;
    private Runnable runnable = new Runnable() {
        public void run() {
            MMAdViewController.this.chooseCachedAdOrAdCall();
            MMAdView adView = (MMAdView) MMAdViewController.this.adViewRef.get();
            if (adView == null) {
                Log.e(MMAdViewSDK.SDKLOG, "The reference to the ad view was broken.");
            } else {
                MMAdViewController.this.handler.postDelayed(this, (long) (adView.refreshInterval * 1000));
            }
        }
    };
    boolean shouldEnableBottomBar = true;
    boolean shouldLaunchToOverlay = false;
    boolean shouldMakeOverlayTransparent = false;
    int shouldResizeOverlay;
    boolean shouldShowBottomBar = true;
    boolean shouldShowTitlebar = false;
    private long timeRemaining;
    private long timeResumed;
    long transitionTime = 600;
    /* access modifiers changed from: private */
    public String urlString;
    /* access modifiers changed from: private */
    public WebView webView;

    private MMAdViewController(MMAdView adView) {
        this.adViewRef = new WeakReference<>(adView);
        this.webView = new WebView(adView.getContext());
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.getSettings().setCacheMode(2);
        this.webView.setBackgroundColor(0);
        this.webView.setWillNotDraw(false);
        this.webView.setClickable(true);
        this.webView.addJavascriptInterface(new MMJSInterface(), "interface");
        this.webView.setId(15063);
    }

    static synchronized void assignAdViewController(MMAdView adView) {
        synchronized (MMAdViewController.class) {
            boolean reassign = true;
            if (adView.controller == null) {
                if (adView.getId() == -1) {
                    Log.e(MMAdViewSDK.SDKLOG, "MMAdView found without a view id. Ad requests on this MMAdView are disabled.");
                } else {
                    MMAdViewController controller = controllers.get(adView.adViewId);
                    if (controller == null) {
                        controller = new MMAdViewController(adView);
                        controllers.put(adView.adViewId, controller);
                        reassign = false;
                    }
                    controller.adViewRef = new WeakReference<>(adView);
                    adView.controller = controller;
                    adView.addView(controller.webView, new ViewGroup.LayoutParams(-1, -1));
                    if (adView.refreshInterval >= 0 && adView.refreshInterval < 15) {
                        controller.refreshTimerOn = false;
                        MMAdViewSDK.Log.d("Refresh interval is " + adView.refreshInterval + ". Change to at least 15 to refresh ads.");
                    } else if (adView.refreshInterval < 0) {
                        controller.refreshTimerOn = false;
                        MMAdViewSDK.Log.d("Automatic ad fetching is off with " + adView.refreshInterval + ". You must manually call for ads.");
                    } else {
                        controller.refreshTimerOn = true;
                        controller.resumeTimer(false);
                    }
                    if (adView.refreshInterval >= 0 && !reassign) {
                        controller.chooseCachedAdOrAdCall();
                    }
                }
            }
        }
    }

    static synchronized void removeAdViewController(MMAdView adView, boolean isFinishing) {
        MMAdViewController controller;
        synchronized (MMAdViewController.class) {
            if (adView.controller != null) {
                if (isFinishing) {
                    controller = controllers.put(adView.adViewId, null);
                } else {
                    controller = controllers.get(adView.adViewId);
                }
                adView.controller = null;
                if (controller != null) {
                    controller.pauseTimer(false);
                    if (isFinishing) {
                        controller.handler = null;
                    }
                    adView.removeView(controller.webView);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void chooseCachedAdOrAdCall() {
        MMAdView adView = this.adViewRef.get();
        if (adView == null) {
            Log.e(MMAdViewSDK.SDKLOG, "The reference to the ad view was broken.");
        } else {
            Context context = adView.getContext();
            if (HandShake.sharedHandShake(context).kill) {
                Log.i(MMAdViewSDK.SDKLOG, "The server is no longer allowing ads.");
            } else if (this.requestInProgress) {
                Log.i(MMAdViewSDK.SDKLOG, "There is already an ad request in progress. Defering call for new ad");
            } else if (!HandShake.sharedHandShake(context).isAdTypeDownloading(adView.adType)) {
                MMAdViewSDK.Log.d("No download in progress.");
                if (checkForAdNotDownloaded(adView)) {
                    Log.i(MMAdViewSDK.SDKLOG, "Last ad wasn't fully downloaded. Download again.");
                    DownloadLastAd(adView);
                } else {
                    Log.i(MMAdViewSDK.SDKLOG, "No incomplete downloads.");
                    cleanUpExpiredAds(adView);
                    SharedPreferences settings = adView.getContext().getSharedPreferences("MillennialMediaSettings", 0);
                    String lastAdName = settings.getString("lastDownloadedAdName", null);
                    if (lastAdName == null) {
                        Log.i(MMAdViewSDK.SDKLOG, "Last ad name is null. Call for new ad.");
                        getNextAd();
                    } else if (checkIfAdExistsInDb(lastAdName, adView)) {
                        Log.i(MMAdViewSDK.SDKLOG, "Ad found in the database");
                        if (!checkIfAdExistsInFilesystem(lastAdName, adView)) {
                            Log.i(MMAdViewSDK.SDKLOG, "Last ad can't be found in the file system. Download again.");
                            DownloadLastAd(adView);
                        } else if (!checkIfExpired(lastAdName, adView)) {
                            boolean adViewed = settings.getBoolean("lastAdViewed", false);
                            Log.i(MMAdViewSDK.SDKLOG, "Last ad viewed?: " + adViewed);
                            if (adViewed) {
                                Log.i(MMAdViewSDK.SDKLOG, "Existing ad has been viewed. Call for a new ad");
                                getNextAd();
                            } else if (HandShake.sharedHandShake(context).canWatchVideoAd(context, adView.adType, lastAdName)) {
                                if (adView.listener != null) {
                                    try {
                                        adView.listener.MMAdReturned(adView);
                                        Log.i(MMAdViewSDK.SDKLOG, "Millennial ad return success");
                                    } catch (Exception e) {
                                        Log.w(MMAdViewSDK.SDKLOG, "Exception raised in your MMAdListener: ", e);
                                    }
                                }
                                playVideo(lastAdName, adView);
                            } else {
                                Log.i(MMAdViewSDK.SDKLOG, "Outside of the timeout window. Call for a new ad");
                                getNextAd();
                            }
                        } else {
                            Log.i(MMAdViewSDK.SDKLOG, "Existing ad is expired. Delete and call for a new ad");
                            deleteAd(lastAdName, adView);
                            SharedPreferences.Editor editor = adView.getContext().getSharedPreferences("MillennialMediaSettings", 0).edit();
                            editor.putString("lastDownloadedAdName", null);
                            editor.commit();
                            MMAdViewSDK.Log.v("Setting last ad name to NULL");
                            getNextAd();
                        }
                    } else {
                        Log.i(MMAdViewSDK.SDKLOG, "Last ad can't be found in the database. Remove any files from the filesystem and call for new ad.");
                        deleteAd(lastAdName, adView);
                        SharedPreferences.Editor editor2 = adView.getContext().getSharedPreferences("MillennialMediaSettings", 0).edit();
                        editor2.putString("lastDownloadedAdName", null);
                        editor2.commit();
                        MMAdViewSDK.Log.v("Setting last ad name to NULL");
                        getNextAd();
                    }
                }
            } else if (adView.listener != null) {
                try {
                    adView.listener.MMAdFailed(adView);
                    adView.listener.MMAdRequestIsCaching(adView);
                    Log.i(MMAdViewSDK.SDKLOG, "There is a download in progress. Defering call for new ad");
                } catch (Exception e2) {
                    Log.w(MMAdViewSDK.SDKLOG, "Exception raised in your MMAdListener: ", e2);
                }
            }
        }
    }

    private void getNextAd() {
        this.requestInProgress = true;
        new Thread() {
            public void run() {
                String content;
                IOException e1;
                IllegalStateException e12;
                MMAdView adView = (MMAdView) MMAdViewController.this.adViewRef.get();
                if (adView == null) {
                    Log.e(MMAdViewSDK.SDKLOG, "The reference to the ad view was broken.");
                    MMAdViewController.this.requestInProgress = false;
                } else if (adView.apid == null) {
                    try {
                        Log.e(MMAdViewSDK.SDKLOG, "MMAdView found with a null apid. New ad requests on this MMAdView are disabled until an apid has been assigned.");
                        adView.listener.MMAdFailed(adView);
                    } catch (Exception e) {
                        Log.w(MMAdViewSDK.SDKLOG, "Exception raised in your MMAdListener: ", e);
                    }
                    MMAdViewController.this.requestInProgress = false;
                } else {
                    if (MMAdViewSDK.connectivityManager.getActiveNetworkInfo() != null && MMAdViewSDK.connectivityManager.getActiveNetworkInfo().isConnected()) {
                        String adTypeString = null;
                        String adUrl = null;
                        try {
                            String metaValues = MMAdViewController.this.getURLMetaValues(adView);
                            if (adView.testMode) {
                                Log.w(MMAdViewSDK.SDKLOG, "*********** Advertising test mode is deprecated. Refer to wiki.millennialmedia.com for testing information. ");
                            }
                            adTypeString = MMAdViewController.this.getAdType(adView.adType);
                            String ua = MMAdViewController.this.webView.getSettings().getUserAgentString() + Build.MODEL;
                            DisplayMetrics metrics = adView.getContext().getResources().getDisplayMetrics();
                            float scale = metrics.density;
                            int heightPixels = metrics.heightPixels;
                            int widthPixels = metrics.widthPixels;
                            StringBuilder adUrlBuilder = new StringBuilder("http://androidsdk.ads.mp.mydas.mobi/getAd.php5?sdkapid=" + URLEncoder.encode(adView.apid, "UTF-8"));
                            if (adView.auid != null) {
                                adUrlBuilder.append("&auid=" + URLEncoder.encode(adView.auid, "UTF-8"));
                            } else {
                                adUrlBuilder.append("&auid=UNKNOWN");
                            }
                            if (ua != null) {
                                adUrlBuilder.append("&ua=" + URLEncoder.encode(ua, "UTF-8"));
                            } else {
                                adUrlBuilder.append("&ua=UNKNOWN");
                            }
                            if (Build.MODEL != null) {
                                adUrlBuilder.append("&dm=" + URLEncoder.encode(Build.MODEL, "UTF-8"));
                            }
                            if (Build.VERSION.RELEASE != null) {
                                adUrlBuilder.append("&dv=Android" + URLEncoder.encode(Build.VERSION.RELEASE, "UTF-8"));
                            }
                            adUrlBuilder.append("&density=" + Float.toString(scale));
                            adUrlBuilder.append("&hpx=" + heightPixels);
                            adUrlBuilder.append("&wpx=" + widthPixels);
                            adUrlBuilder.append("&mmisdk=" + URLEncoder.encode(MMAdViewSDK.SDKVER, "UTF-8") + metaValues);
                            Context context = adView.getContext();
                            if (HandShake.sharedHandShake(context).canRequestVideo(context, adView.adType)) {
                                adUrlBuilder.append("&video=true");
                            } else {
                                adUrlBuilder.append("&video=false");
                            }
                            if (!Build.VERSION.SDK.equalsIgnoreCase("8") || (Environment.getExternalStorageState().equals("mounted") && adView.getContext().checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") != -1)) {
                                adUrlBuilder.append("&cachedvideo=true");
                            } else {
                                adUrlBuilder.append("&cachedvideo=false");
                            }
                            adUrl = adUrlBuilder.toString();
                        } catch (UnsupportedEncodingException e2) {
                        }
                        if (adTypeString != null) {
                            adUrl = adUrl + adTypeString;
                        }
                        MMAdViewSDK.Log.d("Calling for an advertisement: " + adUrl);
                        try {
                            Log.i(MMAdViewSDK.SDKLOG, "Making ad request");
                            HttpResponse httpResponse = new HttpGetRequest().get(adUrl);
                            if (httpResponse == null) {
                                Log.e(MMAdViewSDK.SDKLOG, "HTTP response is null");
                                MMAdViewController.this.requestInProgress = false;
                                return;
                            }
                            HttpEntity httpEntity = httpResponse.getEntity();
                            if (httpEntity == null) {
                                Log.i(MMAdViewSDK.SDKLOG, "null entity");
                                MMAdViewController.this.requestInProgress = false;
                                return;
                            } else if (httpEntity.getContentLength() == 0) {
                                try {
                                    Log.i(MMAdViewSDK.SDKLOG, "Millennial ad return failed. Zero content length returned.");
                                    adView.listener.MMAdFailed(adView);
                                } catch (Exception e3) {
                                    Log.w(MMAdViewSDK.SDKLOG, "Exception raised in your MMAdListener: ", e3);
                                }
                                MMAdViewController.this.requestInProgress = false;
                                return;
                            } else {
                                Header httpHeader = httpEntity.getContentType();
                                if (!(httpHeader == null || httpHeader.getValue() == null)) {
                                    if (httpHeader.getValue().equalsIgnoreCase("application/json")) {
                                        try {
                                            VideoAd videoAd = new VideoAd(HttpGetRequest.convertStreamToString(httpEntity.getContent()));
                                            try {
                                                Log.i(MMAdViewSDK.SDKLOG, "Current environment: " + Environment.getExternalStorageState());
                                                if (Environment.getExternalStorageState().equals("mounted")) {
                                                    videoAd.storedOnSdCard = true;
                                                }
                                                if (videoAd != null) {
                                                    Log.i(MMAdViewSDK.SDKLOG, "Cached video ad JSON received: " + videoAd.id);
                                                    MMAdViewController.this.handleCachedAdResponse(videoAd);
                                                }
                                            } catch (IllegalStateException e4) {
                                                e12 = e4;
                                                e12.printStackTrace();
                                                Log.i(MMAdViewSDK.SDKLOG, "Millennial ad return failed. Invalid response data.");
                                                MMAdViewController.this.requestInProgress = false;
                                                return;
                                            } catch (IOException e5) {
                                                e1 = e5;
                                                e1.printStackTrace();
                                                Log.i(MMAdViewSDK.SDKLOG, "Millennial ad return failed. Invalid response data.");
                                                MMAdViewController.this.requestInProgress = false;
                                                return;
                                            }
                                        } catch (IllegalStateException e6) {
                                            e12 = e6;
                                            e12.printStackTrace();
                                            Log.i(MMAdViewSDK.SDKLOG, "Millennial ad return failed. Invalid response data.");
                                            MMAdViewController.this.requestInProgress = false;
                                            return;
                                        } catch (IOException e7) {
                                            e1 = e7;
                                            e1.printStackTrace();
                                            Log.i(MMAdViewSDK.SDKLOG, "Millennial ad return failed. Invalid response data.");
                                            MMAdViewController.this.requestInProgress = false;
                                            return;
                                        }
                                    } else if (httpHeader.getValue().equalsIgnoreCase("text/html")) {
                                        Header xHeader = httpResponse.getFirstHeader("X-MM-Video");
                                        if (xHeader != null && xHeader.getValue().equalsIgnoreCase("true")) {
                                            Context context2 = adView.getContext();
                                            HandShake.sharedHandShake(context2).didReceiveVideoXHeader(context2, adView.adType);
                                        }
                                        MMAdViewController.this.webView.setWebViewClient(new WebViewClient() {
                                            public void onPageFinished(WebView webView, String url) {
                                                webView.loadUrl("javascript:window.interface.setLoaded(true);");
                                                webView.loadUrl("javascript:window.interface.getUrl(document.links[0].href);");
                                                if (webView != null) {
                                                    webView.clearCache(true);
                                                }
                                            }

                                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                                return true;
                                            }

                                            public void onScaleChanged(WebView view, float oldScale, float newScale) {
                                                Log.e(MMAdViewSDK.SDKLOG, "Scale Changed");
                                            }
                                        });
                                        try {
                                            if (adView.ignoreDensityScaling) {
                                                content = "<head><meta name=\"viewport\" content=\"target-densitydpi=device-dpi\" /></head>" + HttpGetRequest.convertStreamToString(httpEntity.getContent());
                                            } else {
                                                content = HttpGetRequest.convertStreamToString(httpEntity.getContent());
                                            }
                                            MMAdViewController.this.webView.loadDataWithBaseURL(adUrl.substring(0, adUrl.lastIndexOf("/") + 1), content, "text/html", "UTF-8", null);
                                        } catch (IOException e8) {
                                            IOException ioe = e8;
                                            if (adView.listener != null) {
                                                try {
                                                    Log.i(MMAdViewSDK.SDKLOG, "Millennial ad webview failed.");
                                                    adView.listener.MMAdFailed(adView);
                                                } catch (Exception e9) {
                                                    Log.w(MMAdViewSDK.SDKLOG, "Exception raised in your MMAdListener: ", e9);
                                                }
                                                Log.e(MMAdViewSDK.SDKLOG, "Exception raised in your ad webview: ", ioe);
                                            }
                                        }
                                    } else if (adView.listener != null) {
                                        try {
                                            Log.i(MMAdViewSDK.SDKLOG, "Millennial ad return failed. Invalid mime type returned.");
                                            adView.listener.MMAdFailed(adView);
                                        } catch (Exception e10) {
                                            Log.w(MMAdViewSDK.SDKLOG, "Exception raised in your MMAdListener: ", e10);
                                        }
                                    }
                                }
                            }
                        } catch (Exception e11) {
                            Log.e(MMAdViewSDK.SDKLOG, "HTTP error: ", e11);
                            MMAdViewController.this.requestInProgress = false;
                            return;
                        }
                    } else if (adView.listener != null) {
                        try {
                            Log.i(MMAdViewSDK.SDKLOG, "No network available, can't call for ads.");
                            adView.listener.MMAdFailed(adView);
                        } catch (Exception e13) {
                            Log.w(MMAdViewSDK.SDKLOG, "Exception raised in your MMAdListener: ", e13);
                        }
                    }
                    MMAdViewController.this.requestInProgress = false;
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00f9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleCachedAdResponse(final com.millennialmedia.android.VideoAd r11) {
        /*
            r10 = this;
            java.lang.ref.WeakReference<com.millennialmedia.android.MMAdView> r7 = r10.adViewRef
            java.lang.Object r0 = r7.get()
            com.millennialmedia.android.MMAdView r0 = (com.millennialmedia.android.MMAdView) r0
            if (r0 != 0) goto L_0x0012
            java.lang.String r7 = "MillennialMediaAdSDK"
            java.lang.String r8 = "The reference to the ad view was broken."
            android.util.Log.e(r7, r8)
        L_0x0011:
            return
        L_0x0012:
            java.lang.String r7 = r11.id
            boolean r7 = r10.checkIfAdExistsInDb(r7, r0)
            if (r7 != 0) goto L_0x008a
            java.lang.String r7 = r11.id
            boolean r7 = r10.checkIfAdExistsInFilesystem(r7, r0)
            if (r7 != 0) goto L_0x0047
            java.util.Date r7 = r11.expiration
            boolean r7 = r10.isExpired(r7)
            if (r7 != 0) goto L_0x003f
            java.lang.String r7 = "MillennialMediaAdSDK"
            java.lang.String r8 = "Ad is valid. Saving new ad settings. Download TRUE."
            android.util.Log.i(r7, r8)
            r10.writeAdDataToSettings(r11, r0)
            android.os.Handler r7 = r10.cacheHandler
            com.millennialmedia.android.MMAdViewController$2 r8 = new com.millennialmedia.android.MMAdViewController$2
            r8.<init>(r11)
            r7.post(r8)
            goto L_0x0011
        L_0x003f:
            java.lang.String r7 = "MillennialMediaAdSDK"
            java.lang.String r8 = "New ad has expiration date in the past. Not downloading ad content."
            android.util.Log.i(r7, r8)
            goto L_0x0011
        L_0x0047:
            java.lang.String r7 = "MillennialMediaAdSDK"
            java.lang.String r8 = "Ad received exists in filesystem but not database. Running checks . . ."
            android.util.Log.i(r7, r8)
            android.content.Context r7 = r0.getContext()
            java.lang.String r8 = "MillennialMediaSettings"
            r9 = 0
            android.content.SharedPreferences r6 = r7.getSharedPreferences(r8, r9)
            java.lang.String r7 = "lastDownloadedAdName"
            r8 = 0
            java.lang.String r5 = r6.getString(r7, r8)
            java.lang.String r7 = "MillennialMediaAdSDK"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "Last ad name: "
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.StringBuilder r8 = r8.append(r5)
            java.lang.String r8 = r8.toString()
            android.util.Log.i(r7, r8)
            if (r5 == 0) goto L_0x0011
            java.lang.ref.WeakReference<com.millennialmedia.android.MMAdView> r7 = r10.adViewRef
            if (r7 == 0) goto L_0x0011
            java.lang.ref.WeakReference<com.millennialmedia.android.MMAdView> r7 = r10.adViewRef
            java.lang.Object r7 = r7.get()
            com.millennialmedia.android.MMAdView r7 = (com.millennialmedia.android.MMAdView) r7
            r10.deleteAd(r5, r7)
            goto L_0x0011
        L_0x008a:
            r1 = 0
            com.millennialmedia.android.AdDatabaseHelper r2 = new com.millennialmedia.android.AdDatabaseHelper     // Catch:{ SQLiteException -> 0x00d9 }
            android.content.Context r7 = r0.getContext()     // Catch:{ SQLiteException -> 0x00d9 }
            r2.<init>(r7)     // Catch:{ SQLiteException -> 0x00d9 }
            java.lang.String r7 = r11.id     // Catch:{ SQLiteException -> 0x014b, all -> 0x0148 }
            r2.updateDeferredViewStart(r7)     // Catch:{ SQLiteException -> 0x014b, all -> 0x0148 }
            r4 = 0
        L_0x009a:
            java.util.ArrayList<com.millennialmedia.android.VideoImage> r7 = r11.buttons     // Catch:{ SQLiteException -> 0x014b, all -> 0x0148 }
            int r7 = r7.size()     // Catch:{ SQLiteException -> 0x014b, all -> 0x0148 }
            if (r4 >= r7) goto L_0x00ad
            java.util.ArrayList<com.millennialmedia.android.VideoImage> r7 = r11.buttons     // Catch:{ SQLiteException -> 0x014b, all -> 0x0148 }
            java.lang.Object r7 = r7.get(r4)     // Catch:{ SQLiteException -> 0x014b, all -> 0x0148 }
            com.millennialmedia.android.VideoImage r7 = (com.millennialmedia.android.VideoImage) r7     // Catch:{ SQLiteException -> 0x014b, all -> 0x0148 }
            int r4 = r4 + 1
            goto L_0x009a
        L_0x00ad:
            r2.updateAdData(r11)     // Catch:{ SQLiteException -> 0x014b, all -> 0x0148 }
            if (r2 == 0) goto L_0x014f
            r2.close()
            r1 = r2
        L_0x00b6:
            java.lang.String r7 = r11.id
            boolean r7 = r10.checkIfAdExistsInFilesystem(r7, r0)
            if (r7 != 0) goto L_0x00f9
            java.util.Date r7 = r11.expiration
            boolean r7 = r10.isExpired(r7)
            if (r7 != 0) goto L_0x00eb
            java.lang.String r7 = "MillennialMediaAdSDK"
            java.lang.String r8 = "Ad is valid. Saving new ad settings."
            android.util.Log.i(r7, r8)
            android.os.Handler r7 = r10.cacheHandler
            com.millennialmedia.android.MMAdViewController$3 r8 = new com.millennialmedia.android.MMAdViewController$3
            r8.<init>(r11)
            r7.post(r8)
            goto L_0x0011
        L_0x00d9:
            r7 = move-exception
            r3 = r7
        L_0x00db:
            r3.printStackTrace()     // Catch:{ all -> 0x00e4 }
            if (r1 == 0) goto L_0x00b6
            r1.close()
            goto L_0x00b6
        L_0x00e4:
            r7 = move-exception
        L_0x00e5:
            if (r1 == 0) goto L_0x00ea
            r1.close()
        L_0x00ea:
            throw r7
        L_0x00eb:
            java.lang.String r7 = "MillennialMediaAdSDK"
            java.lang.String r8 = "New ad has expiration date in the past. Not downloading ad content. Remove any expired content from the filesystem."
            android.util.Log.i(r7, r8)
            java.lang.String r7 = r11.id
            r10.deleteAd(r7, r0)
            goto L_0x0011
        L_0x00f9:
            java.lang.String r7 = r11.id
            boolean r7 = r10.checkIfExpired(r7, r0)
            if (r7 != 0) goto L_0x013a
            java.lang.String r7 = "MillennialMediaAdSDK"
            java.lang.String r8 = "Cached ad is valid. Show."
            android.util.Log.i(r7, r8)
            android.content.Context r7 = r0.getContext()
            com.millennialmedia.android.HandShake r7 = com.millennialmedia.android.HandShake.sharedHandShake(r7)
            android.content.Context r8 = r0.getContext()
            java.lang.String r9 = r0.adType
            r7.updateLastVideoViewedTime(r8, r9)
            com.millennialmedia.android.MMAdView$MMAdListener r7 = r0.listener
            if (r7 == 0) goto L_0x0129
            com.millennialmedia.android.MMAdView$MMAdListener r7 = r0.listener     // Catch:{ Exception -> 0x0130 }
            r7.MMAdReturned(r0)     // Catch:{ Exception -> 0x0130 }
            java.lang.String r7 = "MillennialMediaAdSDK"
            java.lang.String r8 = "Millennial ad return success"
            android.util.Log.i(r7, r8)     // Catch:{ Exception -> 0x0130 }
        L_0x0129:
            java.lang.String r7 = r11.id
            r10.playVideo(r7, r0)
            goto L_0x0011
        L_0x0130:
            r7 = move-exception
            r3 = r7
            java.lang.String r7 = "MillennialMediaAdSDK"
            java.lang.String r8 = "Exception raised in your MMAdListener: "
            android.util.Log.w(r7, r8, r3)
            goto L_0x0129
        L_0x013a:
            java.lang.String r7 = "MillennialMediaAdSDK"
            java.lang.String r8 = "Ad returned exists in db & filesystem but is expired. Deleting."
            android.util.Log.i(r7, r8)
            java.lang.String r7 = r11.id
            r10.deleteAd(r7, r0)
            goto L_0x0011
        L_0x0148:
            r7 = move-exception
            r1 = r2
            goto L_0x00e5
        L_0x014b:
            r7 = move-exception
            r3 = r7
            r1 = r2
            goto L_0x00db
        L_0x014f:
            r1 = r2
            goto L_0x00b6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.android.MMAdViewController.handleCachedAdResponse(com.millennialmedia.android.VideoAd):void");
    }

    /* access modifiers changed from: package-private */
    public void handleClick(String url) {
        this.urlString = url;
        new Thread() {
            public void run() {
                int rc;
                String mimeTypeString = null;
                MMAdViewSDK.Log.d("Touch occured, opening ad...");
                if (MMAdViewController.this.urlString != null) {
                    String locationString = MMAdViewController.this.urlString;
                    MMAdView adView = (MMAdView) MMAdViewController.this.adViewRef.get();
                    if (adView == null) {
                        Log.e(MMAdViewSDK.SDKLOG, "The reference to the ad view was broken.");
                        return;
                    }
                    Activity activity = (Activity) adView.getContext();
                    if (activity == null) {
                        Log.e(MMAdViewSDK.SDKLOG, "The ad view does not have a parent activity.");
                        return;
                    }
                    do {
                        try {
                            URL connectURL = new URL(MMAdViewController.this.urlString);
                            HttpURLConnection.setFollowRedirects(false);
                            HttpURLConnection conn = (HttpURLConnection) connectURL.openConnection();
                            conn.setRequestMethod("GET");
                            conn.connect();
                            String unused = MMAdViewController.this.urlString = conn.getHeaderField("Location");
                            mimeTypeString = conn.getHeaderField("Content-Type");
                            rc = conn.getResponseCode();
                            MMAdViewSDK.Log.d("Response: " + conn.getResponseCode() + " " + conn.getResponseMessage());
                            MMAdViewSDK.Log.d("urlString: " + MMAdViewController.this.urlString);
                            if (rc < 300) {
                                break;
                            }
                        } catch (MalformedURLException e) {
                        } catch (IOException e2) {
                            e2.printStackTrace();
                        }
                    } while (rc < 400);
                    MMAdViewSDK.Log.d(locationString);
                    if (locationString != null) {
                        if (mimeTypeString == null) {
                            mimeTypeString = "";
                        }
                        Uri destinationURI = Uri.parse(locationString);
                        if (destinationURI != null && destinationURI.getScheme() != null && mimeTypeString != null) {
                            if ((destinationURI.getScheme().equalsIgnoreCase("http") || destinationURI.getScheme().equalsIgnoreCase("https")) && mimeTypeString.equalsIgnoreCase("text/html")) {
                                Intent intent = new Intent(activity, MMAdViewOverlayActivity.class);
                                intent.putExtra("canAccelerate", MMAdViewController.this.canAccelerate);
                                intent.putExtra("overlayTransition", MMAdViewController.this.overlayTransition);
                                intent.putExtra("transitionTime", MMAdViewController.this.transitionTime);
                                intent.putExtra("shouldResizeOverlay", MMAdViewController.this.shouldResizeOverlay);
                                intent.putExtra("shouldShowTitlebar", MMAdViewController.this.shouldShowTitlebar);
                                intent.putExtra("shouldShowBottomBar", MMAdViewController.this.shouldShowBottomBar);
                                intent.putExtra("shouldEnableBottomBar", MMAdViewController.this.shouldEnableBottomBar);
                                intent.putExtra("shouldMakeOverlayTransparent", MMAdViewController.this.shouldMakeOverlayTransparent);
                                intent.putExtra("overlayTitle", MMAdViewController.this.overlayTitle);
                                MMAdViewSDK.Log.v("Accelerometer on?: " + MMAdViewController.this.canAccelerate);
                                intent.setData(destinationURI);
                                activity.startActivityForResult(intent, 0);
                            } else if (destinationURI.getScheme().equalsIgnoreCase("market")) {
                                MMAdViewSDK.Log.v("Android Market URL, launch the Market Application");
                                activity.startActivity(new Intent("android.intent.action.VIEW", destinationURI));
                            } else if (destinationURI.getScheme().equalsIgnoreCase("rtsp") || (destinationURI.getScheme().equalsIgnoreCase("http") && (mimeTypeString.equalsIgnoreCase("video/mp4") || mimeTypeString.equalsIgnoreCase("video/3gpp")))) {
                                MMAdViewSDK.Log.v("Video, launch the video player for video at: " + destinationURI);
                                Intent intent2 = new Intent(activity, VideoPlayer.class);
                                intent2.setData(destinationURI);
                                activity.startActivityForResult(intent2, 0);
                            } else if (destinationURI.getScheme().equalsIgnoreCase("tel")) {
                                MMAdViewSDK.Log.v("Telephone Number, launch the phone");
                                activity.startActivity(new Intent("android.intent.action.DIAL", destinationURI));
                            } else if (destinationURI.getScheme().equalsIgnoreCase("geo")) {
                                MMAdViewSDK.Log.v("Google Maps");
                                activity.startActivity(new Intent("android.intent.action.VIEW", destinationURI));
                            } else if (!destinationURI.getScheme().equalsIgnoreCase("http") || destinationURI.getLastPathSegment() == null) {
                                if (destinationURI.getScheme().equalsIgnoreCase("http")) {
                                    Intent intent3 = new Intent(activity, MMAdViewOverlayActivity.class);
                                    intent3.putExtra("canAccelerate", MMAdViewController.this.canAccelerate);
                                    intent3.putExtra("overlayTransition", MMAdViewController.this.overlayTransition);
                                    intent3.putExtra("transitionTime", MMAdViewController.this.transitionTime);
                                    intent3.putExtra("shouldResizeOverlay", MMAdViewController.this.shouldResizeOverlay);
                                    intent3.putExtra("shouldShowTitlebar", MMAdViewController.this.shouldShowTitlebar);
                                    intent3.putExtra("shouldShowBottomBar", MMAdViewController.this.shouldShowBottomBar);
                                    intent3.putExtra("shouldEnableBottomBar", MMAdViewController.this.shouldEnableBottomBar);
                                    intent3.putExtra("shouldMakeOverlayTransparent", MMAdViewController.this.shouldMakeOverlayTransparent);
                                    intent3.putExtra("overlayTitle", MMAdViewController.this.overlayTitle);
                                    MMAdViewSDK.Log.v("Accelerometer on?: " + MMAdViewController.this.canAccelerate);
                                    intent3.setData(destinationURI);
                                    activity.startActivityForResult(intent3, 0);
                                    return;
                                }
                                activity.startActivity(new Intent("android.intent.action.VIEW", destinationURI));
                            } else if (destinationURI.getLastPathSegment().endsWith(".mp4") || destinationURI.getLastPathSegment().endsWith(".3gp")) {
                                MMAdViewSDK.Log.v("Video, launch the video player for video at: " + destinationURI);
                                Intent intent4 = new Intent(activity, VideoPlayer.class);
                                intent4.setData(destinationURI);
                                activity.startActivityForResult(intent4, 0);
                            } else {
                                Intent intent5 = new Intent(activity, MMAdViewOverlayActivity.class);
                                intent5.putExtra("canAccelerate", MMAdViewController.this.canAccelerate);
                                intent5.putExtra("overlayTransition", MMAdViewController.this.overlayTransition);
                                intent5.putExtra("transitionTime", MMAdViewController.this.transitionTime);
                                intent5.putExtra("shouldResizeOverlay", MMAdViewController.this.shouldResizeOverlay);
                                intent5.putExtra("shouldShowTitlebar", MMAdViewController.this.shouldShowTitlebar);
                                intent5.putExtra("shouldShowBottomBar", MMAdViewController.this.shouldShowBottomBar);
                                intent5.putExtra("shouldEnableBottomBar", MMAdViewController.this.shouldEnableBottomBar);
                                intent5.putExtra("shouldMakeOverlayTransparent", MMAdViewController.this.shouldMakeOverlayTransparent);
                                intent5.putExtra("overlayTitle", MMAdViewController.this.overlayTitle);
                                MMAdViewSDK.Log.v("Accelerometer on?: " + MMAdViewController.this.canAccelerate);
                                intent5.setData(destinationURI);
                                activity.startActivityForResult(intent5, 0);
                            }
                        }
                    }
                }
            }
        }.start();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void pauseTimer(boolean r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            boolean r0 = r4.refreshTimerOn     // Catch:{ all -> 0x0012 }
            if (r0 != 0) goto L_0x0007
            monitor-exit(r4)     // Catch:{ all -> 0x0012 }
        L_0x0006:
            return
        L_0x0007:
            boolean r0 = r4.paused     // Catch:{ all -> 0x0012 }
            if (r0 == 0) goto L_0x0015
            if (r5 == 0) goto L_0x0010
            r0 = 1
            r4.appPaused = r0     // Catch:{ all -> 0x0012 }
        L_0x0010:
            monitor-exit(r4)     // Catch:{ all -> 0x0012 }
            goto L_0x0006
        L_0x0012:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0012 }
            throw r0
        L_0x0015:
            android.os.Handler r0 = r4.handler     // Catch:{ all -> 0x0012 }
            java.lang.Runnable r1 = r4.runnable     // Catch:{ all -> 0x0012 }
            r0.removeCallbacks(r1)     // Catch:{ all -> 0x0012 }
            long r0 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x0012 }
            long r2 = r4.timeResumed     // Catch:{ all -> 0x0012 }
            long r0 = r0 - r2
            r4.timeRemaining = r0     // Catch:{ all -> 0x0012 }
            r0 = 1
            r4.paused = r0     // Catch:{ all -> 0x0012 }
            r4.appPaused = r5     // Catch:{ all -> 0x0012 }
            monitor-exit(r4)     // Catch:{ all -> 0x0012 }
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.android.MMAdViewController.pauseTimer(boolean):void");
    }

    /* access modifiers changed from: package-private */
    public void resumeTimer(boolean appRequested) {
        synchronized (this) {
            if (this.refreshTimerOn) {
                if (this.paused) {
                    if (!this.appPaused || appRequested) {
                        MMAdView adView = this.adViewRef.get();
                        if (adView == null) {
                            Log.e(MMAdViewSDK.SDKLOG, "The reference to the ad view was broken.");
                            return;
                        }
                        if (this.handler == null) {
                            this.handler = new Handler();
                        }
                        if (this.timeRemaining <= 0 || this.timeRemaining > ((long) (adView.refreshInterval * 1000))) {
                            this.timeRemaining = (long) (adView.refreshInterval * 1000);
                        }
                        this.handler.postDelayed(this.runnable, this.timeRemaining);
                        this.timeResumed = SystemClock.uptimeMillis();
                        this.appPaused = false;
                        this.paused = false;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public String getURLMetaValues(MMAdView adView) throws UnsupportedEncodingException {
        StringBuilder metaString = new StringBuilder();
        Locale locale = Locale.getDefault();
        if (locale != null) {
            metaString.append("&language=" + locale.getLanguage());
            metaString.append("&country=" + locale.getCountry());
        }
        if (adView.age != null) {
            metaString.append("&age=" + URLEncoder.encode(adView.age, "UTF-8"));
        }
        if (adView.gender != null) {
            metaString.append("&gender=" + URLEncoder.encode(adView.gender, "UTF-8"));
        }
        if (adView.zip != null) {
            metaString.append("&zip=" + URLEncoder.encode(adView.zip, "UTF-8"));
        }
        if (adView.marital != null && (adView.marital.equals("single") || adView.marital.equals("married") || adView.marital.equals("divorced") || adView.marital.equals("swinger") || adView.marital.equals("relationship") || adView.marital.equals("engaged"))) {
            metaString.append("&marital=" + adView.marital);
        }
        if (adView.income != null) {
            metaString.append("&income=" + URLEncoder.encode(adView.income, "UTF-8"));
        }
        if (adView.keywords != null) {
            metaString.append("&kw=" + URLEncoder.encode(adView.keywords, "UTF-8"));
        }
        if (adView.latitude != null) {
            metaString.append("&lat=" + URLEncoder.encode(adView.latitude, "UTF-8"));
        }
        if (adView.longitude != null) {
            metaString.append("&long=" + URLEncoder.encode(adView.longitude, "UTF-8"));
        }
        if (adView.acid != null) {
            metaString.append("&acid=" + URLEncoder.encode(adView.acid, "UTF-8"));
        }
        if (adView.mxsdk != null) {
            metaString.append("&mxsdk=" + URLEncoder.encode(adView.mxsdk, "UTF-8"));
        }
        if (adView.height != null) {
            metaString.append("&hsht=" + URLEncoder.encode(adView.height, "UTF-8"));
        }
        if (adView.width != null) {
            metaString.append("&hswd=" + URLEncoder.encode(adView.width, "UTF-8"));
        }
        if (adView.ethnicity != null) {
            metaString.append("&ethnicity=" + URLEncoder.encode(adView.ethnicity, "UTF-8"));
        }
        if (adView.orientation != null && (adView.orientation.equals("straight") || adView.orientation.equals("gay") || adView.orientation.equals("bisexual") || adView.orientation.equals("notsure"))) {
            metaString.append("&orientation=" + adView.orientation);
        }
        if (adView.education != null) {
            metaString.append("&edu=" + URLEncoder.encode(adView.education, "UTF-8"));
        }
        if (adView.children != null) {
            metaString.append("&children=" + URLEncoder.encode(adView.children, "UTF-8"));
        }
        if (adView.politics != null) {
            metaString.append("&politics=" + URLEncoder.encode(adView.politics, "UTF-8"));
        }
        if (metaString != null) {
            return metaString.toString();
        }
        return "";
    }

    /* access modifiers changed from: private */
    public String getAdType(String adtype) {
        if (adtype != null) {
            if (adtype.equals(MMAdView.BANNER_AD_TOP)) {
                return "&adtype=" + MMAdView.BANNER_AD_TOP;
            }
            if (adtype.equals(MMAdView.BANNER_AD_BOTTOM)) {
                return "&adtype=" + MMAdView.BANNER_AD_BOTTOM;
            }
            if (adtype.equals(MMAdView.BANNER_AD_RECTANGLE)) {
                return "&adtype=" + MMAdView.BANNER_AD_RECTANGLE;
            }
            if (adtype.equals(MMAdView.FULLSCREEN_AD_LAUNCH)) {
                return "&adtype=" + MMAdView.FULLSCREEN_AD_LAUNCH;
            }
            if (adtype.equals(MMAdView.FULLSCREEN_AD_TRANSITION)) {
                return "&adtype=" + MMAdView.FULLSCREEN_AD_TRANSITION;
            }
        }
        Log.e(MMAdViewSDK.SDKLOG, "******* ERROR: INCORRECT AD TYPE IN MMADVIEW OBJECT PARAMETERS (" + adtype + ") **********");
        Log.e(MMAdViewSDK.SDKLOG, "******* SDK DEFAULTED TO MMBannerAdTop. THIS MAY AFFECT THE ADS YOU RECIEVE!!! **********");
        return "&adtype=" + MMAdView.BANNER_AD_TOP;
    }

    private class DownloadAdTask extends AsyncTask<VideoAd, Void, String> {
        DownloadAdTask() {
            MMAdView adView = (MMAdView) MMAdViewController.this.adViewRef.get();
            if (adView == null) {
                Log.e(MMAdViewSDK.SDKLOG, "The reference to the ad view was broken.");
            } else {
                HandShake.sharedHandShake(adView.getContext()).lockAdTypeDownload(adView.adType);
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            MMAdView adView = (MMAdView) MMAdViewController.this.adViewRef.get();
            if (adView == null) {
                Log.e(MMAdViewSDK.SDKLOG, "The reference to the ad view was broken.");
                return;
            }
            MMAdViewSDK.Log.v("DownloadAdTask onPreExecute");
            SharedPreferences.Editor editor = adView.getContext().getSharedPreferences("MillennialMediaSettings", 0).edit();
            editor.putBoolean("pendingDownload", true);
            editor.commit();
            MMAdViewSDK.Log.v("Setting pendingDownload to TRUE");
        }

        /* access modifiers changed from: protected */
        public String doInBackground(VideoAd... ad) {
            File cacheDir;
            MMAdView adView = (MMAdView) MMAdViewController.this.adViewRef.get();
            if (adView == null) {
                Log.e(MMAdViewSDK.SDKLOG, "The reference to the ad view was broken.");
                return null;
            } else if (ad == null || ad.length == 0) {
                return null;
            } else {
                if (!ad[0].storedOnSdCard) {
                    cacheDir = adView.getContext().getCacheDir();
                } else if (Environment.getExternalStorageState().equals("mounted")) {
                    File file = new File(Environment.getExternalStorageDirectory(), "millennialmedia");
                    if (file.exists()) {
                        cacheDir = file;
                    } else if (file.mkdirs()) {
                        cacheDir = file;
                    } else {
                        cacheDir = adView.getContext().getCacheDir();
                        AdDatabaseHelper adDatabaseHelper = new AdDatabaseHelper(adView.getContext());
                        adDatabaseHelper.updateAdOnSDCard(ad[0].id, false);
                        adDatabaseHelper.close();
                    }
                } else {
                    cacheDir = adView.getContext().getCacheDir();
                    AdDatabaseHelper adDatabaseHelper2 = new AdDatabaseHelper(adView.getContext());
                    adDatabaseHelper2.updateAdOnSDCard(ad[0].id, false);
                    adDatabaseHelper2.close();
                }
                String newAdDirPath = cacheDir + "/" + ad[0].id + "/";
                new File(newAdDirPath).mkdir();
                MMAdViewSDK.Log.v("Downloading content to " + newAdDirPath.toString());
                if (!MMAdViewController.this.downloadComponent(ad[0].contentUrl, "video.dat", newAdDirPath)) {
                    SharedPreferences.Editor editor = adView.getContext().getSharedPreferences("MillennialMediaSettings", 0).edit();
                    editor.putBoolean("pendingDownload", true);
                    editor.commit();
                    return ad[0].id;
                }
                for (int i = 0; i < ad[0].buttons.size(); i++) {
                    String imageUrl = ad[0].buttons.get(i).imageUrl;
                    if (!MMAdViewController.this.downloadComponent(imageUrl, Uri.parse(imageUrl).getLastPathSegment(), newAdDirPath)) {
                        SharedPreferences.Editor editor2 = adView.getContext().getSharedPreferences("MillennialMediaSettings", 0).edit();
                        editor2.putBoolean("pendingDownload", true);
                        editor2.commit();
                        return ad[0].id;
                    }
                }
                SharedPreferences.Editor editor22 = adView.getContext().getSharedPreferences("MillennialMediaSettings", 0).edit();
                editor22.putBoolean("pendingDownload", false);
                editor22.commit();
                return ad[0].id;
            }
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String adName) {
            boolean success;
            MMAdView adView = (MMAdView) MMAdViewController.this.adViewRef.get();
            if (adView == null) {
                Log.e(MMAdViewSDK.SDKLOG, "The reference to the ad view was broken.");
                return;
            }
            SharedPreferences settings = adView.getContext().getSharedPreferences("MillennialMediaSettings", 0);
            SharedPreferences.Editor editor = settings.edit();
            if (adName != null) {
                editor.putString("lastDownloadedAdName", adName);
                MMAdViewSDK.Log.v("Download complete. LastDownloadedAdName: " + adName);
                if (settings.getBoolean("pendingDownload", true)) {
                    success = false;
                } else {
                    success = true;
                }
            } else {
                editor.putString("lastDownloadedAdName", null);
                success = false;
            }
            HandShake.sharedHandShake(adView.getContext()).unlockAdTypeDownload(adView.adType);
            if (success) {
                editor.putInt("downloadAttempts", 0);
                MMAdViewSDK.Log.d("Ad download completed successfully: TRUE");
                editor.putBoolean("lastAdViewed", false);
                MMAdViewSDK.Log.v("Last ad viewed: FALSE");
            } else {
                editor.putInt("downloadAttempts", settings.getInt("downloadAttempts", 0) + 1);
                MMAdViewSDK.Log.d("Ad download completed successfully: FALSE");
            }
            editor.commit();
            if (adView.listener != null) {
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0098, code lost:
        r17 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0099, code lost:
        r17.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00fc, code lost:
        r17 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00fd, code lost:
        r17.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0137, code lost:
        r17 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0138, code lost:
        r17.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x013f, code lost:
        r17 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0140, code lost:
        r17.printStackTrace();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0098 A[ExcHandler: MalformedURLException (r17v8 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:1:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00fc A[ExcHandler: FileNotFoundException (r17v6 'e' java.io.FileNotFoundException A[CUSTOM_DECLARE]), Splitter:B:1:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0137 A[ExcHandler: IllegalStateException (r17v5 'e' java.lang.IllegalStateException A[CUSTOM_DECLARE]), Splitter:B:1:0x0021] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean downloadComponent(java.lang.String r21, java.lang.String r22, java.lang.String r23) {
        /*
            r20 = this;
            java.io.File r10 = new java.io.File
            java.lang.StringBuilder r17 = new java.lang.StringBuilder
            r17.<init>()
            r0 = r17
            r1 = r23
            java.lang.StringBuilder r17 = r0.append(r1)
            r0 = r17
            r1 = r22
            java.lang.StringBuilder r17 = r0.append(r1)
            java.lang.String r17 = r17.toString()
            r0 = r10
            r1 = r17
            r0.<init>(r1)
            android.net.ConnectivityManager r17 = com.millennialmedia.android.MMAdViewSDK.connectivityManager     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            android.net.NetworkInfo r17 = r17.getActiveNetworkInfo()     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            if (r17 == 0) goto L_0x012e
            android.net.ConnectivityManager r17 = com.millennialmedia.android.MMAdViewSDK.connectivityManager     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            android.net.NetworkInfo r17 = r17.getActiveNetworkInfo()     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            boolean r17 = r17.isConnected()     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            r18 = 1
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x012e
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            r17.<init>()     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            java.lang.String r18 = "Downloading Component: "
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            r0 = r17
            r1 = r22
            java.lang.StringBuilder r17 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            java.lang.String r18 = " from "
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            r0 = r17
            r1 = r21
            java.lang.StringBuilder r17 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            java.lang.String r17 = r17.toString()     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            com.millennialmedia.android.MMAdViewSDK.Log.v(r17)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            java.net.URL r6 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            r0 = r6
            r1 = r21
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            r17 = 0
            java.net.HttpURLConnection.setFollowRedirects(r17)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            java.net.URLConnection r5 = r6.openConnection()     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            java.net.HttpURLConnection r5 = (java.net.HttpURLConnection) r5     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            java.lang.String r17 = "GET"
            r0 = r5
            r1 = r17
            r0.setRequestMethod(r1)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            r5.connect()     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            java.io.InputStream r16 = r5.getInputStream()     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            java.lang.String r17 = "Content-Length"
            r0 = r5
            r1 = r17
            java.lang.String r11 = r0.getHeaderField(r1)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            if (r16 != 0) goto L_0x00a4
            java.io.IOException r17 = new java.io.IOException     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            java.lang.String r18 = "Stream is null"
            r17.<init>(r18)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            throw r17     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
        L_0x0098:
            r17 = move-exception
            r8 = r17
            r8.printStackTrace()
        L_0x009e:
            r10.delete()
            r17 = 0
        L_0x00a3:
            return r17
        L_0x00a4:
            java.io.FileOutputStream r13 = new java.io.FileOutputStream     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            r17.<init>()     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            r0 = r17
            r1 = r23
            java.lang.StringBuilder r17 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            r0 = r17
            r1 = r22
            java.lang.StringBuilder r17 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            java.lang.String r17 = r17.toString()     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            r0 = r13
            r1 = r17
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            r17 = 1024(0x400, float:1.435E-42)
            r0 = r17
            byte[] r0 = new byte[r0]     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            r4 = r0
        L_0x00cc:
            r0 = r16
            r1 = r4
            int r12 = r0.read(r1)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            if (r12 > 0) goto L_0x00f1
            r16.close()     // Catch:{ IOException -> 0x0103, MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137 }
            r13.close()     // Catch:{ IOException -> 0x0103, MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137 }
            if (r10 == 0) goto L_0x009e
            long r14 = r10.length()     // Catch:{ Exception -> 0x0147 }
            java.lang.Long r7 = new java.lang.Long     // Catch:{ Exception -> 0x0147 }
            r7.<init>(r11)     // Catch:{ Exception -> 0x0147 }
            long r17 = r7.longValue()     // Catch:{ Exception -> 0x0147 }
            int r17 = (r14 > r17 ? 1 : (r14 == r17 ? 0 : -1))
            if (r17 != 0) goto L_0x009e
            r17 = 1
            goto L_0x00a3
        L_0x00f1:
            r17 = 0
            r0 = r13
            r1 = r4
            r2 = r17
            r3 = r12
            r0.write(r1, r2, r3)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            goto L_0x00cc
        L_0x00fc:
            r17 = move-exception
            r8 = r17
            r8.printStackTrace()
            goto L_0x009e
        L_0x0103:
            r17 = move-exception
            r9 = r17
            java.lang.String r17 = "MillennialMediaAdSDK"
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            r18.<init>()     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            java.lang.String r19 = "Content caching error: "
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            java.lang.String r19 = r9.getMessage()     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            java.lang.String r18 = r18.toString()     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            r0 = r17
            r1 = r18
            r2 = r9
            android.util.Log.e(r0, r1, r2)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            r10.delete()     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            r17 = 0
            goto L_0x00a3
        L_0x012e:
            java.lang.String r17 = "MillennialMediaAdSDK"
            java.lang.String r18 = "Not connected to the internet"
            android.util.Log.e(r17, r18)     // Catch:{ MalformedURLException -> 0x0098, FileNotFoundException -> 0x00fc, IllegalStateException -> 0x0137, IOException -> 0x013f }
            goto L_0x009e
        L_0x0137:
            r17 = move-exception
            r8 = r17
            r8.printStackTrace()
            goto L_0x009e
        L_0x013f:
            r17 = move-exception
            r8 = r17
            r8.printStackTrace()
            goto L_0x009e
        L_0x0147:
            r17 = move-exception
            goto L_0x009e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.android.MMAdViewController.downloadComponent(java.lang.String, java.lang.String, java.lang.String):boolean");
    }

    private void cleanUpExpiredAds(MMAdView adView) {
        AdDatabaseHelper db = new AdDatabaseHelper(adView.getContext());
        List<String> expiredAds = db.getAllExpiredAds();
        db.close();
        if (expiredAds != null && expiredAds.size() > 0) {
            MMAdViewSDK.Log.v("Some ads are expired");
            for (int i = 0; i < expiredAds.size(); i++) {
                deleteAd(expiredAds.get(i), adView);
            }
        }
    }

    public boolean freeMemoryOnDisk(MMAdView adView) {
        try {
            if (Environment.getExternalStorageState().equals("mounted")) {
                File sdCardFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/millennialmedia");
                if (sdCardFile.exists()) {
                    return sdCardFile.length() < 12582912;
                }
            }
            File cacheDir = adView.getContext().getCacheDir();
            if (cacheDir != null) {
                long usedMem = cacheDir.length();
                Log.i(MMAdViewSDK.SDKLOG, "Cache: " + usedMem);
                return usedMem < 12582912;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public boolean checkForAdNotDownloaded(MMAdView adView) {
        adView.getContext();
        SharedPreferences settings = adView.getContext().getSharedPreferences("MillennialMediaSettings", 0);
        boolean downloaded = settings.getBoolean("pendingDownload", false);
        MMAdViewSDK.Log.v("Pending download?: " + downloaded);
        if (settings.getInt("downloadAttempts", 0) < 3) {
            return downloaded;
        }
        MMAdViewSDK.Log.v("Cached ad download failed too many times. Purging it from the database.");
        deleteAd(settings.getString("lastDownloadedAdName", null), adView);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("downloadAttempts", 0);
        editor.commit();
        return false;
    }

    public boolean checkIfAdExistsInFilesystem(String name, MMAdView adView) {
        File adDir;
        String[] list;
        boolean existsInFilesystem = false;
        AdDatabaseHelper db = new AdDatabaseHelper(adView.getContext());
        int fileCount = db.getButtonCountForAd(name);
        boolean onSDCard = db.isAdOnSDCard(name);
        db.close();
        if (!onSDCard || !Environment.getExternalStorageState().equals("mounted")) {
            adDir = new File(adView.getContext().getCacheDir() + "/" + name);
        } else {
            adDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/millennialmedia/" + name);
        }
        if (adDir.exists() && (list = adDir.list()) != null && list.length >= fileCount) {
            existsInFilesystem = true;
        }
        MMAdViewSDK.Log.v("Last ad " + name + " in filesystem?: " + existsInFilesystem);
        return existsInFilesystem;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0044  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean checkIfAdExistsInDb(java.lang.String r7, com.millennialmedia.android.MMAdView r8) {
        /*
            r6 = this;
            r0 = 0
            r3 = 0
            com.millennialmedia.android.AdDatabaseHelper r1 = new com.millennialmedia.android.AdDatabaseHelper     // Catch:{ SQLiteException -> 0x0036 }
            android.content.Context r4 = r8.getContext()     // Catch:{ SQLiteException -> 0x0036 }
            r1.<init>(r4)     // Catch:{ SQLiteException -> 0x0036 }
            boolean r3 = r1.checkIfAdExists(r7)     // Catch:{ SQLiteException -> 0x004b, all -> 0x0048 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x004b, all -> 0x0048 }
            r4.<init>()     // Catch:{ SQLiteException -> 0x004b, all -> 0x0048 }
            java.lang.String r5 = "Last ad "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLiteException -> 0x004b, all -> 0x0048 }
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ SQLiteException -> 0x004b, all -> 0x0048 }
            java.lang.String r5 = " in database?: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLiteException -> 0x004b, all -> 0x0048 }
            java.lang.StringBuilder r4 = r4.append(r3)     // Catch:{ SQLiteException -> 0x004b, all -> 0x0048 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLiteException -> 0x004b, all -> 0x0048 }
            com.millennialmedia.android.MMAdViewSDK.Log.v(r4)     // Catch:{ SQLiteException -> 0x004b, all -> 0x0048 }
            if (r1 == 0) goto L_0x004f
            r1.close()
            r0 = r1
        L_0x0035:
            return r3
        L_0x0036:
            r4 = move-exception
            r2 = r4
        L_0x0038:
            r2.printStackTrace()     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x0035
            r0.close()
            goto L_0x0035
        L_0x0041:
            r4 = move-exception
        L_0x0042:
            if (r0 == 0) goto L_0x0047
            r0.close()
        L_0x0047:
            throw r4
        L_0x0048:
            r4 = move-exception
            r0 = r1
            goto L_0x0042
        L_0x004b:
            r4 = move-exception
            r2 = r4
            r0 = r1
            goto L_0x0038
        L_0x004f:
            r0 = r1
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.android.MMAdViewController.checkIfAdExistsInDb(java.lang.String, com.millennialmedia.android.MMAdView):boolean");
    }

    public void DownloadLastAd(MMAdView adView) {
        String lastAdName = adView.getContext().getSharedPreferences("MillennialMediaSettings", 0).getString("lastDownloadedAdName", null);
        MMAdViewSDK.Log.v("Downloading last ad: " + lastAdName);
        AdDatabaseHelper db = new AdDatabaseHelper(adView.getContext());
        final VideoAd ad = db.getVideoAd(lastAdName);
        db.close();
        if (ad != null) {
            if (adView.listener != null) {
                try {
                    adView.listener.MMAdFailed(adView);
                    adView.listener.MMAdRequestIsCaching(adView);
                    Log.i(MMAdViewSDK.SDKLOG, "Millennial restarting or finishing caching ad.");
                } catch (Exception e) {
                    Log.w(MMAdViewSDK.SDKLOG, "Exception raised in your MMAdListener: ", e);
                }
            }
            this.cacheHandler.post(new Runnable() {
                public void run() {
                    new DownloadAdTask().execute(ad);
                }
            });
            return;
        }
        MMAdViewSDK.Log.d("Can't find last ad in database, calling for new ad");
        getNextAd();
    }

    private boolean checkIfExpired(String adName, MMAdView adView) {
        AdDatabaseHelper db = new AdDatabaseHelper(adView.getContext());
        Date exp = db.getExpiration(adName);
        db.close();
        return isExpired(exp);
    }

    private boolean isExpired(Date date) {
        if (date == null || date.getTime() > System.currentTimeMillis()) {
            return false;
        }
        return true;
    }

    private void writeAdDataToSettings(VideoAd ad, MMAdView adView) {
        MMAdViewSDK.Log.d("Storing ad data in db");
        AdDatabaseHelper db = new AdDatabaseHelper(adView.getContext());
        db.storeAd(ad);
        db.close();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void playVideo(String name, MMAdView adView) {
        if (name != null) {
            SharedPreferences.Editor editor = adView.getContext().getSharedPreferences("MillennialMediaSettings", 0).edit();
            editor.putBoolean("lastAdViewed", true);
            editor.commit();
            if (adView.listener != null) {
                try {
                    adView.listener.MMAdOverlayLaunched(adView);
                    MMAdViewSDK.Log.d("Launch Video Player. Playing " + name);
                } catch (Exception e) {
                    Log.w(MMAdViewSDK.SDKLOG, "Exception raised in your MMAdListener: ", e);
                }
            }
            Intent intent = new Intent().setClass(adView.getContext(), VideoPlayer.class);
            intent.putExtra("cached", true);
            intent.putExtra("adName", name);
            AdDatabaseHelper db = new AdDatabaseHelper(adView.getContext());
            boolean onSDCard = db.isAdOnSDCard(name);
            db.close();
            if (!onSDCard || !Environment.getExternalStorageState().equals("mounted")) {
                intent.setData(Uri.parse(name));
            } else {
                intent.setData(Uri.parse(Environment.getExternalStorageDirectory().getAbsolutePath() + "/millennialmedia/" + name + "/video.dat"));
            }
            adView.getContext().startActivity(intent);
        }
    }

    private void deleteAd(String adName, MMAdView adView) {
        if (adName != null) {
            if (Environment.getExternalStorageState().equals("mounted")) {
                File sdCardAd = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/millennialmedia/" + adName);
                if (sdCardAd.exists()) {
                    File[] files = sdCardAd.listFiles();
                    MMAdViewSDK.Log.v("Ad directory size: " + files.length);
                    for (File delete : files) {
                        delete.delete();
                    }
                    sdCardAd.delete();
                    MMAdViewSDK.Log.v(adName + " directory deleted");
                }
            }
            File cacheDirAd = new File(adView.getContext().getCacheDir() + "/" + adName);
            if (cacheDirAd.exists()) {
                File[] files2 = cacheDirAd.listFiles();
                MMAdViewSDK.Log.v("Ad directory size: " + files2.length);
                for (File delete2 : files2) {
                    delete2.delete();
                }
                cacheDirAd.delete();
                MMAdViewSDK.Log.v(adName + " directory deleted");
            }
            AdDatabaseHelper db = new AdDatabaseHelper(adView.getContext());
            boolean purged = db.purgeAdFromDb(adName);
            db.close();
            MMAdViewSDK.Log.v("Ad deleted from database: " + adName + " with succuess? " + purged);
        }
    }

    private class MMJSInterface {
        private MMJSInterface() {
        }

        public void setLoaded(boolean success) {
            MMAdView adView = (MMAdView) MMAdViewController.this.adViewRef.get();
            if (adView == null) {
                Log.e(MMAdViewSDK.SDKLOG, "The reference to the ad view was broken.");
            } else if (adView.listener == null) {
            } else {
                if (success) {
                    try {
                        adView.listener.MMAdReturned(adView);
                        Log.i(MMAdViewSDK.SDKLOG, "Millennial ad return success");
                    } catch (Exception e) {
                        Log.w(MMAdViewSDK.SDKLOG, "Exception raised in your MMAdListener: ", e);
                    }
                } else {
                    try {
                        Log.i(MMAdViewSDK.SDKLOG, "Millennial ad return failed");
                        adView.listener.MMAdFailed(adView);
                    } catch (Exception e2) {
                        Log.w(MMAdViewSDK.SDKLOG, "Exception raised in your MMAdListener: ", e2);
                    }
                }
            }
        }

        public void countImages(String size) {
            int num;
            MMAdView adView = (MMAdView) MMAdViewController.this.adViewRef.get();
            if (adView == null) {
                Log.e(MMAdViewSDK.SDKLOG, "The reference to the ad view was broken.");
                return;
            }
            MMAdViewSDK.Log.d("size: " + size);
            if (size != null) {
                num = new Integer(size).intValue();
            } else {
                num = 0;
                Log.e(MMAdViewSDK.SDKLOG, "Image count is null");
            }
            MMAdViewSDK.Log.d("num: " + num);
            if (num > 0) {
                if (adView.listener != null) {
                    try {
                        adView.listener.MMAdReturned(adView);
                        Log.i(MMAdViewSDK.SDKLOG, "Millennial ad return success");
                        MMAdViewSDK.Log.v("View height: " + adView.getHeight());
                    } catch (Exception e) {
                        Log.w(MMAdViewSDK.SDKLOG, "Exception raised in your MMAdListener: ", e);
                    }
                }
            } else if (adView.listener != null) {
                try {
                    Log.i(MMAdViewSDK.SDKLOG, "Millennial ad return failed");
                    adView.listener.MMAdFailed(adView);
                } catch (Exception e2) {
                    Log.w(MMAdViewSDK.SDKLOG, "Exception raised in your MMAdListener: ", e2);
                }
            }
        }

        public void getUrl(String url) {
            MMAdViewController.this.nextUrl = url;
            MMAdViewSDK.Log.v("nextUrl: " + MMAdViewController.this.nextUrl);
        }

        public void shouldOpen(String url) {
            MMAdView adView = (MMAdView) MMAdViewController.this.adViewRef.get();
            if (adView == null) {
                Log.e(MMAdViewSDK.SDKLOG, "The reference to the ad view was broken.");
                return;
            }
            MMAdViewController.this.handleClick(url);
            if (adView.listener != null) {
                try {
                    adView.listener.MMAdOverlayLaunched(adView);
                } catch (Exception e) {
                    Log.w(MMAdViewSDK.SDKLOG, "Exception raised in your MMAdListener: ", e);
                }
            }
        }

        public void shouldOverlay(boolean shouldOverlay) {
            MMAdViewController.this.shouldLaunchToOverlay = shouldOverlay;
        }

        public void overlayTitle(String title) {
            MMAdViewController.this.overlayTitle = title;
        }

        public void overlayTransition(String transition, long time) {
            MMAdViewController.this.overlayTransition = transition;
            MMAdViewController.this.transitionTime = time;
        }

        public void shouldAccelerate(boolean shouldAccelerate) {
            MMAdView adView = (MMAdView) MMAdViewController.this.adViewRef.get();
            if (adView == null) {
                return;
            }
            if (adView.accelerate) {
                MMAdViewController.this.canAccelerate = shouldAccelerate;
            } else {
                MMAdViewController.this.canAccelerate = false;
            }
        }

        public void shouldResizeOverlay(int padding) {
            MMAdViewController.this.shouldResizeOverlay = padding;
        }

        public void shouldShowTitlebar(boolean showTitlebar) {
            MMAdViewController.this.shouldShowTitlebar = showTitlebar;
        }

        public void shouldShowBottomBar(boolean showBottomBar) {
            MMAdViewController.this.shouldShowBottomBar = showBottomBar;
        }

        public void shouldEnableBottomBar(boolean enableBottomBar) {
            MMAdViewController.this.shouldEnableBottomBar = enableBottomBar;
        }

        public void shouldMakeOverlayTransparent(boolean isTransparent) {
            MMAdViewController.this.shouldMakeOverlayTransparent = isTransparent;
        }

        public void log(String message) {
            MMAdViewSDK.Log.d(message);
        }

        public void vibrate(int time) {
            MMAdView adView = (MMAdView) MMAdViewController.this.adViewRef.get();
            if (adView != null && adView.vibrate) {
                Activity activity = (Activity) adView.getContext();
                if (activity.getPackageManager().checkPermission("android.permission.VIBRATE", activity.getPackageName()) == 0) {
                    ((Vibrator) activity.getSystemService("vibrator")).vibrate((long) time);
                } else {
                    Log.w(MMAdViewSDK.SDKLOG, "Advertisement is trying to use vibrator but permissions are missing.");
                }
            }
        }
    }
}
