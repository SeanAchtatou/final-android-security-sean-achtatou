package com.millennialmedia.android;

public final class R {

    public static final class attr {
        public static final int accelerate = 2130771972;
        public static final int acid = 2130771969;
        public static final int adType = 2130771970;
        public static final int age = 2130771974;
        public static final int apid = 2130771968;
        public static final int children = 2130771982;
        public static final int education = 2130771983;
        public static final int ethnicity = 2130771979;
        public static final int gender = 2130771975;
        public static final int goalId = 2130771985;
        public static final int ignoreDensityScaling = 2130771973;
        public static final int income = 2130771977;
        public static final int keywords = 2130771978;
        public static final int marital = 2130771981;
        public static final int orientation = 2130771980;
        public static final int politics = 2130771984;
        public static final int refreshInterval = 2130771971;
        public static final int zip = 2130771976;
    }

    public static final class styleable {
        public static final int[] MMAdView = {com.nix.game.pinball.free.R.attr.showUpdate, R.attr.acid, R.attr.adType, R.attr.refreshInterval, R.attr.accelerate, R.attr.ignoreDensityScaling, R.attr.age, R.attr.gender, R.attr.zip, R.attr.income, R.attr.keywords, R.attr.ethnicity, R.attr.orientation, R.attr.marital, R.attr.children, R.attr.education, R.attr.politics, R.attr.goalId};
        public static final int MMAdView_accelerate = 4;
        public static final int MMAdView_acid = 1;
        public static final int MMAdView_adType = 2;
        public static final int MMAdView_age = 6;
        public static final int MMAdView_apid = 0;
        public static final int MMAdView_children = 14;
        public static final int MMAdView_education = 15;
        public static final int MMAdView_ethnicity = 11;
        public static final int MMAdView_gender = 7;
        public static final int MMAdView_goalId = 17;
        public static final int MMAdView_ignoreDensityScaling = 5;
        public static final int MMAdView_income = 9;
        public static final int MMAdView_keywords = 10;
        public static final int MMAdView_marital = 13;
        public static final int MMAdView_orientation = 12;
        public static final int MMAdView_politics = 16;
        public static final int MMAdView_refreshInterval = 3;
        public static final int MMAdView_zip = 8;
    }
}
