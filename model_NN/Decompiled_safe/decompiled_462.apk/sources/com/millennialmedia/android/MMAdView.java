package com.millennialmedia.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import com.millennialmedia.android.MMAdViewSDK;
import java.util.Hashtable;

public class MMAdView extends FrameLayout {
    public static final String BANNER_AD_BOTTOM = "MMBannerAdBottom";
    public static final String BANNER_AD_RECTANGLE = "MMBannerAdRectangle";
    public static final String BANNER_AD_TOP = "MMBannerAdTop";
    public static final String FULLSCREEN_AD_LAUNCH = "MMFullScreenAdLaunch";
    public static final String FULLSCREEN_AD_TRANSITION = "MMFullScreenAdTransition";
    public static final String KEY_AGE = "age";
    public static final String KEY_CHILDREN = "children";
    public static final String KEY_EDUCATION = "education";
    public static final String KEY_ETHNICITY = "ethnicity";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_HEIGHT = "height";
    public static final String KEY_INCOME = "income";
    public static final String KEY_KEYWORDS = "keywords";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_MARITAL_STATUS = "marital";
    public static final String KEY_ORIENTATION = "orientation";
    public static final String KEY_POLITICS = "politics";
    public static final String KEY_WIDTH = "width";
    public static final String KEY_ZIP_CODE = "zip";
    public static final int REFRESH_INTERVAL_OFF = -1;
    private static long nextAdViewId = 1;
    String _goalId;
    boolean accelerate;
    String acid;
    String adType;
    Long adViewId;
    String age;
    String apid;
    String auid;
    String children;
    MMAdViewController controller;
    boolean deferedCallForAd;
    String education;
    String ethnicity;
    String gender;
    String height;
    boolean ignoreDensityScaling;
    String income;
    String keywords;
    String latitude;
    MMAdListener listener;
    String longitude;
    String marital;
    String mxsdk;
    String orientation;
    String politics;
    int refreshInterval;
    boolean testMode;
    boolean vibrate;
    String width;
    boolean xmlLayout;
    String zip;

    public interface MMAdListener {
        void MMAdClickedToNewBrowser(MMAdView mMAdView);

        void MMAdClickedToOverlay(MMAdView mMAdView);

        void MMAdFailed(MMAdView mMAdView);

        void MMAdOverlayLaunched(MMAdView mMAdView);

        void MMAdRequestIsCaching(MMAdView mMAdView);

        void MMAdReturned(MMAdView mMAdView);
    }

    public MMAdView(Activity context) {
        super(context);
        this.refreshInterval = 60;
        this.apid = MMAdViewSDK.DEFAULT_APID;
        this.age = null;
        this.gender = null;
        this.zip = null;
        this.marital = null;
        this.income = null;
        this.keywords = null;
        this.latitude = null;
        this.longitude = null;
        this.ethnicity = null;
        this.orientation = null;
        this.education = null;
        this.children = null;
        this.politics = null;
        this.acid = null;
        this.mxsdk = null;
        this.height = null;
        this.width = null;
        this.accelerate = true;
        this.vibrate = true;
        this.testMode = false;
        this.xmlLayout = false;
        this.ignoreDensityScaling = false;
        MMAdViewSDK.Log.d("Creating new MMAdView for conversion tracking.");
        checkPermissions(context);
        if (MMAdViewSDK.connectivityManager == null) {
            MMAdViewSDK.connectivityManager = (ConnectivityManager) context.getApplicationContext().getSystemService("connectivity");
        }
        this.auid = getAuid(context);
    }

    public MMAdView(Context context, AttributeSet attrs) {
        this((Activity) context, attrs, 0);
    }

    public MMAdView(Activity context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.refreshInterval = 60;
        this.apid = MMAdViewSDK.DEFAULT_APID;
        this.age = null;
        this.gender = null;
        this.zip = null;
        this.marital = null;
        this.income = null;
        this.keywords = null;
        this.latitude = null;
        this.longitude = null;
        this.ethnicity = null;
        this.orientation = null;
        this.education = null;
        this.children = null;
        this.politics = null;
        this.acid = null;
        this.mxsdk = null;
        this.height = null;
        this.width = null;
        this.accelerate = true;
        this.vibrate = true;
        this.testMode = false;
        this.xmlLayout = false;
        this.ignoreDensityScaling = false;
        MMAdViewSDK.Log.d("Creating MMAdView from XML layout.");
        if (attrs != null) {
            this.apid = attrs.getAttributeValue("http://millennialmedia.com/android/schema", "apid");
            this.acid = attrs.getAttributeValue("http://millennialmedia.com/android/schema", "acid");
            this.adType = attrs.getAttributeValue("http://millennialmedia.com/android/schema", "adType");
            this.refreshInterval = attrs.getAttributeIntValue("http://millennialmedia.com/android/schema", "refreshInterval", 60);
            this.accelerate = attrs.getAttributeBooleanValue("http://millennialmedia.com/android/schema", "accelerate", true);
            this.ignoreDensityScaling = attrs.getAttributeBooleanValue("http://millennialmedia.com/android/schema", "ignoreDensityScaling", false);
            this.height = attrs.getAttributeValue("http://millennialmedia.com/android/schema", KEY_HEIGHT);
            this.width = attrs.getAttributeValue("http://millennialmedia.com/android/schema", KEY_WIDTH);
            this.age = attrs.getAttributeValue("http://millennialmedia.com/android/schema", KEY_AGE);
            this.gender = attrs.getAttributeValue("http://millennialmedia.com/android/schema", KEY_GENDER);
            this.zip = attrs.getAttributeValue("http://millennialmedia.com/android/schema", KEY_ZIP_CODE);
            this.income = attrs.getAttributeValue("http://millennialmedia.com/android/schema", KEY_INCOME);
            this.keywords = attrs.getAttributeValue("http://millennialmedia.com/android/schema", KEY_KEYWORDS);
            this.ethnicity = attrs.getAttributeValue("http://millennialmedia.com/android/schema", KEY_ETHNICITY);
            this.orientation = attrs.getAttributeValue("http://millennialmedia.com/android/schema", KEY_ORIENTATION);
            this.marital = attrs.getAttributeValue("http://millennialmedia.com/android/schema", KEY_MARITAL_STATUS);
            this.children = attrs.getAttributeValue("http://millennialmedia.com/android/schema", KEY_CHILDREN);
            this.education = attrs.getAttributeValue("http://millennialmedia.com/android/schema", KEY_EDUCATION);
            this.politics = attrs.getAttributeValue("http://millennialmedia.com/android/schema", KEY_POLITICS);
            this._goalId = attrs.getAttributeValue("http://millennialmedia.com/android/schema", "goalId");
        }
        this.xmlLayout = true;
        init(context);
    }

    public MMAdView(Activity context, String apid2, String adType2, int refreshInterval2) {
        super(context);
        this.refreshInterval = 60;
        this.apid = MMAdViewSDK.DEFAULT_APID;
        this.age = null;
        this.gender = null;
        this.zip = null;
        this.marital = null;
        this.income = null;
        this.keywords = null;
        this.latitude = null;
        this.longitude = null;
        this.ethnicity = null;
        this.orientation = null;
        this.education = null;
        this.children = null;
        this.politics = null;
        this.acid = null;
        this.mxsdk = null;
        this.height = null;
        this.width = null;
        this.accelerate = true;
        this.vibrate = true;
        this.testMode = false;
        this.xmlLayout = false;
        this.ignoreDensityScaling = false;
        MMAdViewSDK.Log.d("Creating new MMAdView.");
        this.apid = apid2;
        this.adType = adType2;
        this.refreshInterval = refreshInterval2;
        init(context);
    }

    public MMAdView(Activity context, String apid2, String adType2, int refreshInterval2, boolean testMode2) {
        super(context);
        this.refreshInterval = 60;
        this.apid = MMAdViewSDK.DEFAULT_APID;
        this.age = null;
        this.gender = null;
        this.zip = null;
        this.marital = null;
        this.income = null;
        this.keywords = null;
        this.latitude = null;
        this.longitude = null;
        this.ethnicity = null;
        this.orientation = null;
        this.education = null;
        this.children = null;
        this.politics = null;
        this.acid = null;
        this.mxsdk = null;
        this.height = null;
        this.width = null;
        this.accelerate = true;
        this.vibrate = true;
        this.testMode = false;
        this.xmlLayout = false;
        this.ignoreDensityScaling = false;
        MMAdViewSDK.Log.d("Creating new MMAdView.");
        this.apid = apid2;
        this.adType = adType2;
        this.refreshInterval = refreshInterval2;
        this.testMode = testMode2;
        init(context);
    }

    public MMAdView(Activity context, String apid2, String adType2, int refreshInterval2, boolean testMode2, boolean accelerate2) {
        super(context);
        this.refreshInterval = 60;
        this.apid = MMAdViewSDK.DEFAULT_APID;
        this.age = null;
        this.gender = null;
        this.zip = null;
        this.marital = null;
        this.income = null;
        this.keywords = null;
        this.latitude = null;
        this.longitude = null;
        this.ethnicity = null;
        this.orientation = null;
        this.education = null;
        this.children = null;
        this.politics = null;
        this.acid = null;
        this.mxsdk = null;
        this.height = null;
        this.width = null;
        this.accelerate = true;
        this.vibrate = true;
        this.testMode = false;
        this.xmlLayout = false;
        this.ignoreDensityScaling = false;
        MMAdViewSDK.Log.d("Creating new MMAdView.");
        this.apid = apid2;
        this.adType = adType2;
        this.refreshInterval = refreshInterval2;
        this.testMode = testMode2;
        this.accelerate = accelerate2;
        init(context);
    }

    public MMAdView(Activity context, String apid2, String adType2, int refreshInterval2, Hashtable<String, String> metaMap) {
        super(context);
        this.refreshInterval = 60;
        this.apid = MMAdViewSDK.DEFAULT_APID;
        this.age = null;
        this.gender = null;
        this.zip = null;
        this.marital = null;
        this.income = null;
        this.keywords = null;
        this.latitude = null;
        this.longitude = null;
        this.ethnicity = null;
        this.orientation = null;
        this.education = null;
        this.children = null;
        this.politics = null;
        this.acid = null;
        this.mxsdk = null;
        this.height = null;
        this.width = null;
        this.accelerate = true;
        this.vibrate = true;
        this.testMode = false;
        this.xmlLayout = false;
        this.ignoreDensityScaling = false;
        MMAdViewSDK.Log.d("Creating new MMAdView.");
        this.apid = apid2;
        this.adType = adType2;
        this.refreshInterval = refreshInterval2;
        setMetaValues(metaMap);
        init(context);
    }

    public MMAdView(Activity context, String apid2, String adType2, int refreshInterval2, Hashtable<String, String> metaMap, boolean accelerate2) {
        super(context);
        this.refreshInterval = 60;
        this.apid = MMAdViewSDK.DEFAULT_APID;
        this.age = null;
        this.gender = null;
        this.zip = null;
        this.marital = null;
        this.income = null;
        this.keywords = null;
        this.latitude = null;
        this.longitude = null;
        this.ethnicity = null;
        this.orientation = null;
        this.education = null;
        this.children = null;
        this.politics = null;
        this.acid = null;
        this.mxsdk = null;
        this.height = null;
        this.width = null;
        this.accelerate = true;
        this.vibrate = true;
        this.testMode = false;
        this.xmlLayout = false;
        this.ignoreDensityScaling = false;
        MMAdViewSDK.Log.d("Creating new MMAdView.");
        this.apid = apid2;
        this.adType = adType2;
        this.refreshInterval = refreshInterval2;
        setMetaValues(metaMap);
        this.accelerate = accelerate2;
        init(context);
    }

    public MMAdView(Activity context, String apid2, String adType2, int refreshInterval2, boolean testMode2, Hashtable<String, String> metaMap) {
        super(context);
        this.refreshInterval = 60;
        this.apid = MMAdViewSDK.DEFAULT_APID;
        this.age = null;
        this.gender = null;
        this.zip = null;
        this.marital = null;
        this.income = null;
        this.keywords = null;
        this.latitude = null;
        this.longitude = null;
        this.ethnicity = null;
        this.orientation = null;
        this.education = null;
        this.children = null;
        this.politics = null;
        this.acid = null;
        this.mxsdk = null;
        this.height = null;
        this.width = null;
        this.accelerate = true;
        this.vibrate = true;
        this.testMode = false;
        this.xmlLayout = false;
        this.ignoreDensityScaling = false;
        MMAdViewSDK.Log.d("Creating new MMAdView.");
        this.apid = apid2;
        this.adType = adType2;
        this.testMode = testMode2;
        this.refreshInterval = refreshInterval2;
        setMetaValues(metaMap);
        init(context);
    }

    public MMAdView(Activity context, String apid2, String adType2, int refreshInterval2, boolean testMode2, boolean accelerate2, Hashtable<String, String> metaMap) {
        super(context);
        this.refreshInterval = 60;
        this.apid = MMAdViewSDK.DEFAULT_APID;
        this.age = null;
        this.gender = null;
        this.zip = null;
        this.marital = null;
        this.income = null;
        this.keywords = null;
        this.latitude = null;
        this.longitude = null;
        this.ethnicity = null;
        this.orientation = null;
        this.education = null;
        this.children = null;
        this.politics = null;
        this.acid = null;
        this.mxsdk = null;
        this.height = null;
        this.width = null;
        this.accelerate = true;
        this.vibrate = true;
        this.testMode = false;
        this.xmlLayout = false;
        this.ignoreDensityScaling = false;
        MMAdViewSDK.Log.d("Creating new MMAdView.");
        this.apid = apid2;
        this.adType = adType2;
        this.refreshInterval = refreshInterval2;
        this.testMode = testMode2;
        this.accelerate = accelerate2;
        setMetaValues(metaMap);
        init(context);
    }

    public MMAdView(Activity context, String apid2, String adType2, boolean accelerate2, Hashtable<String, String> metaMap) {
        super(context);
        this.refreshInterval = 60;
        this.apid = MMAdViewSDK.DEFAULT_APID;
        this.age = null;
        this.gender = null;
        this.zip = null;
        this.marital = null;
        this.income = null;
        this.keywords = null;
        this.latitude = null;
        this.longitude = null;
        this.ethnicity = null;
        this.orientation = null;
        this.education = null;
        this.children = null;
        this.politics = null;
        this.acid = null;
        this.mxsdk = null;
        this.height = null;
        this.width = null;
        this.accelerate = true;
        this.vibrate = true;
        this.testMode = false;
        this.xmlLayout = false;
        this.ignoreDensityScaling = false;
        MMAdViewSDK.Log.d("Creating new MMAdView.");
        this.apid = apid2;
        this.adType = adType2;
        this.refreshInterval = -1;
        this.accelerate = accelerate2;
        setMetaValues(metaMap);
        init(context);
    }

    private void init(Activity activity) {
        MMAdViewSDK.Log.d("Initializing MMAdView.");
        synchronized (MMAdView.class) {
            this.adViewId = new Long(nextAdViewId);
            nextAdViewId++;
            MMAdViewSDK.Log.v("Assigning MMAdView internal id: " + this.adViewId);
        }
        checkPermissions(activity);
        if (activity.checkCallingOrSelfPermission("android.permission.VIBRATE") == -1) {
            this.vibrate = false;
        }
        PackageManager pm = activity.getPackageManager();
        try {
            ActivityInfo activityInfo = pm.getActivityInfo(new ComponentName(activity, "com.millennialmedia.android.VideoPlayer"), 128);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(MMAdViewSDK.SDKLOG, "Activity VideoPlayer not declared in AndroidManifest.xml");
            e.printStackTrace();
            final AlertDialog videoDialog = new AlertDialog.Builder(activity).create();
            videoDialog.setTitle("Whoops!");
            videoDialog.setMessage("Looks like you forgot to declare the Millennial Media Video Player in your manifest file.");
            videoDialog.setButton(-3, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    videoDialog.cancel();
                }
            });
            videoDialog.show();
        }
        try {
            ActivityInfo activityInfo2 = pm.getActivityInfo(new ComponentName(activity, "com.millennialmedia.android.MMAdViewOverlayActivity"), 128);
        } catch (PackageManager.NameNotFoundException e2) {
            Log.e(MMAdViewSDK.SDKLOG, "Activity MMAdViewOverlayActivity not declared in AndroidManifest.xml");
            e2.printStackTrace();
            final AlertDialog overlayDialog = new AlertDialog.Builder(activity).create();
            overlayDialog.setTitle("Whoops!");
            overlayDialog.setMessage("Looks like you forgot to declare the Millennial Media MMAdViewOverlayActivity in your manifest file.");
            overlayDialog.setButton(-3, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    overlayDialog.cancel();
                }
            });
            overlayDialog.show();
        }
        setBackgroundColor(0);
        this.auid = getAuid(activity);
        if (MMAdViewSDK.connectivityManager == null) {
            MMAdViewSDK.connectivityManager = (ConnectivityManager) activity.getApplicationContext().getSystemService("connectivity");
        }
        if (this.apid == null) {
            Log.e(MMAdViewSDK.SDKLOG, "MMAdView initialized without an apid. New ad requests will be disabled.");
            HandShake.apid = MMAdViewSDK.DEFAULT_APID;
        } else {
            HandShake.apid = this.apid;
        }
        HandShake.sharedHandShake(activity).overrideAdRefresh(this);
        if ((this.adType == FULLSCREEN_AD_TRANSITION || this.adType == FULLSCREEN_AD_LAUNCH) && this.refreshInterval != -1) {
            Log.w(MMAdViewSDK.SDKLOG, "Turning off interstitial refresh interval.");
            this.refreshInterval = -1;
        }
    }

    static String[] getAdTypes() {
        return new String[]{BANNER_AD_TOP, BANNER_AD_BOTTOM, BANNER_AD_RECTANGLE, FULLSCREEN_AD_LAUNCH, FULLSCREEN_AD_TRANSITION};
    }

    public void onWindowFocusChanged(boolean windowInFocus) {
        super.onWindowFocusChanged(windowInFocus);
        if (this.controller != null) {
            if (windowInFocus) {
                this.controller.resumeTimer(false);
            } else {
                this.controller.pauseTimer(false);
            }
        }
        MMAdViewSDK.Log.d("Window Focus Changed. Window in focus?: " + windowInFocus);
        if (!windowInFocus) {
            Activity activity = (Activity) getContext();
            if (activity == null || activity.isFinishing()) {
                MMAdViewController.removeAdViewController(this, true);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View changedView, int visibility) {
        boolean z;
        if (this.controller != null) {
            if (visibility == 0) {
                this.controller.resumeTimer(false);
            } else {
                this.controller.pauseTimer(false);
            }
        }
        StringBuilder append = new StringBuilder().append("Window Visibility Changed. Window is visible?: ");
        if (visibility == 0) {
            z = true;
        } else {
            z = false;
        }
        MMAdViewSDK.Log.d(append.append(Boolean.toString(z)).toString());
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        MMAdViewSDK.Log.d("onAttachedToWindow");
        MMAdViewController.assignAdViewController(this);
        if (this.deferedCallForAd) {
            callForAd();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        MMAdViewSDK.Log.d("onDetachedFromWindow");
        MMAdViewController.removeAdViewController(this, false);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        MMAdViewSDK.Log.d("onSaveInstanceState");
        Bundle bundle = new Bundle();
        bundle.putParcelable("super", super.onSaveInstanceState());
        bundle.putLong("MMAdView", this.adViewId.longValue());
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable state) {
        Bundle bundle = (Bundle) state;
        MMAdViewSDK.Log.d("onRestoreInstanceState");
        this.adViewId = new Long(bundle.getLong("MMAdView"));
        super.onRestoreInstanceState(bundle.getParcelable("super"));
    }

    public boolean dispatchTouchEvent(MotionEvent e) {
        int action = e.getAction();
        if (this.controller == null) {
            return super.dispatchTouchEvent(e);
        }
        if (action == 1) {
            MMAdViewSDK.Log.v("Ad clicked: action=" + action + " x=" + e.getX() + " y=" + e.getY());
            if (this.controller.shouldLaunchToOverlay) {
                if (this.listener != null) {
                    try {
                        this.listener.MMAdClickedToOverlay(this);
                    } catch (Exception e2) {
                        Log.w(MMAdViewSDK.SDKLOG, "Exception raised in your MMAdListener: ", e2);
                    }
                }
                if (this.controller.nextUrl != null) {
                    this.controller.handleClick(this.controller.nextUrl);
                } else {
                    Log.e(MMAdViewSDK.SDKLOG, "No ad returned, no overlay launched");
                }
            } else if (this.controller.nextUrl != null) {
                MMAdViewSDK.Log.d("Ad clicked, launching new browser");
                Activity activity = (Activity) getContext();
                if (activity == null) {
                    Log.e(MMAdViewSDK.SDKLOG, "The reference to the ad view was broken.");
                    return true;
                }
                activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.controller.nextUrl)));
                if (this.listener != null) {
                    try {
                        this.listener.MMAdClickedToNewBrowser(this);
                    } catch (Exception e3) {
                        Log.w(MMAdViewSDK.SDKLOG, "Exception raised in your MMAdListener: ", e3);
                    }
                }
            } else {
                Log.e(MMAdViewSDK.SDKLOG, "No ad returned, no new browser launched");
            }
        }
        return super.dispatchTouchEvent(e);
    }

    public void callForAd() {
        MMAdViewSDK.Log.d("callForAd");
        if (getWindowToken() != null || !this.xmlLayout) {
            if (this.refreshInterval < 0) {
                MMAdViewController.assignAdViewController(this);
                if (this.controller != null) {
                    this.controller.chooseCachedAdOrAdCall();
                }
            }
            this.deferedCallForAd = false;
            return;
        }
        this.deferedCallForAd = true;
    }

    public void halt() {
    }

    public void pause() {
        if (this.controller != null) {
            this.controller.pauseTimer(true);
        }
    }

    public void resume() {
        if (this.controller != null) {
            this.controller.resumeTimer(true);
        }
    }

    public void updateUserLocation(Location userLocation) {
        if (userLocation != null) {
            if (userLocation.getLatitude() <= 90.0d && userLocation.getLatitude() >= -90.0d) {
                this.latitude = String.valueOf(userLocation.getLatitude());
            }
            if (userLocation.getLongitude() <= 180.0d && userLocation.getLongitude() >= -180.0d) {
                this.longitude = String.valueOf(userLocation.getLongitude());
            }
        }
    }

    public void setAdType(String adType2) {
        this.adType = adType2;
    }

    public void setAge(String age2) {
        this.age = age2;
    }

    public void setGender(String gender2) {
        this.gender = gender2;
    }

    public void setZip(String zip2) {
        this.zip = zip2;
    }

    public void setMarital(String marital2) {
        this.marital = marital2;
    }

    public void setIncome(String income2) {
        this.income = income2;
    }

    public void setLatitude(String latitude2) {
        this.latitude = latitude2;
    }

    public void setLongitude(String longitude2) {
        this.longitude = longitude2;
    }

    public void setEthnicity(String ethnicity2) {
        this.ethnicity = ethnicity2;
    }

    public void setOrientation(String orientation2) {
        this.orientation = orientation2;
    }

    public void setEducation(String education2) {
        this.education = education2;
    }

    public void setPolitics(String politics2) {
        this.politics = politics2;
    }

    public void setApid(String apid2) {
        this.apid = apid2;
    }

    public void setHeight(String height2) {
        this.height = height2;
    }

    public void setWidth(String width2) {
        this.width = width2;
    }

    public void setAcid(String acid2) {
        this.acid = acid2;
    }

    public void setMxsdk(String mxsdk2) {
        this.mxsdk = mxsdk2;
    }

    public void setMetaValues(Hashtable<String, String> metaHash) {
        if (metaHash.containsKey(KEY_AGE)) {
            this.age = metaHash.get(KEY_AGE);
        }
        if (metaHash.containsKey(KEY_GENDER)) {
            this.gender = metaHash.get(KEY_GENDER);
        }
        if (metaHash.containsKey(KEY_ZIP_CODE)) {
            this.zip = metaHash.get(KEY_ZIP_CODE);
        }
        if (metaHash.containsKey(KEY_MARITAL_STATUS)) {
            this.marital = metaHash.get(KEY_MARITAL_STATUS);
        }
        if (metaHash.containsKey(KEY_INCOME)) {
            this.income = metaHash.get(KEY_INCOME);
        }
        if (metaHash.containsKey(KEY_KEYWORDS)) {
            this.keywords = metaHash.get(KEY_KEYWORDS);
        }
        if (metaHash.containsKey("mmacid")) {
            this.acid = metaHash.get("mmacid");
        }
        if (metaHash.containsKey("mxsdk")) {
            this.mxsdk = metaHash.get("mxsdk");
        }
        if (metaHash.containsKey(KEY_HEIGHT)) {
            this.height = metaHash.get(KEY_HEIGHT);
        }
        if (metaHash.containsKey(KEY_WIDTH)) {
            this.width = metaHash.get(KEY_WIDTH);
        }
        if (metaHash.containsKey(KEY_ETHNICITY)) {
            this.ethnicity = metaHash.get(KEY_ETHNICITY);
        }
        if (metaHash.containsKey(KEY_ORIENTATION)) {
            this.orientation = metaHash.get(KEY_ORIENTATION);
        }
        if (metaHash.containsKey(KEY_EDUCATION)) {
            this.education = metaHash.get(KEY_EDUCATION);
        }
        if (metaHash.containsKey(KEY_CHILDREN)) {
            this.children = metaHash.get(KEY_CHILDREN);
        }
        if (metaHash.containsKey(KEY_POLITICS)) {
            this.politics = metaHash.get(KEY_POLITICS);
        }
    }

    public void setListener(MMAdListener listener2) {
        synchronized (this) {
            this.listener = listener2;
        }
    }

    public void setIgnoresDensityScaling(boolean ignoresDensityScaling) {
        synchronized (this) {
            this.ignoreDensityScaling = ignoresDensityScaling;
        }
    }

    public static String getAuid(Activity activity) {
        String auid2 = "android_idandroid_id";
        TelephonyManager tm = (TelephonyManager) activity.getSystemService("phone");
        if (tm != null) {
            try {
                auid2 = tm.getDeviceId();
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
        if (auid2 == null || auid2.length() == 0) {
            auid2 = Settings.Secure.getString(activity.getContentResolver(), "android_id");
        }
        if (auid2 == null || auid2.length() == 0) {
            return "UNKNOWN";
        }
        return auid2;
    }

    private static void checkPermissions(Activity activity) {
        if (activity.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == -1) {
            final AlertDialog phoneStateDialog = new AlertDialog.Builder(activity).create();
            phoneStateDialog.setTitle("Whoops!");
            phoneStateDialog.setMessage("Looks like you forgot to declare the READ_PHONE_STATE in your manifest file.");
            phoneStateDialog.setButton(-3, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    phoneStateDialog.cancel();
                }
            });
            phoneStateDialog.show();
        }
        if (activity.checkCallingOrSelfPermission("android.permission.INTERNET") == -1) {
            final AlertDialog internetDialog = new AlertDialog.Builder(activity).create();
            internetDialog.setTitle("Whoops!");
            internetDialog.setMessage("Looks like you forgot to declare the INTERNET in your manifest file.");
            internetDialog.setButton(-3, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    internetDialog.cancel();
                }
            });
            internetDialog.show();
        }
        if (activity.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == -1) {
            final AlertDialog networkStateDialog = new AlertDialog.Builder(activity).create();
            networkStateDialog.setTitle("Whoops!");
            networkStateDialog.setMessage("Looks like you forgot to declare the ACCESS_NETWORK_STATE in your manifest file.");
            networkStateDialog.setButton(-3, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    networkStateDialog.cancel();
                }
            });
            networkStateDialog.show();
        }
        int version = 3;
        try {
            version = Integer.parseInt(Build.VERSION.SDK);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (activity.checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") == -1 && version > 3) {
            final AlertDialog storageDialog = new AlertDialog.Builder(activity).create();
            storageDialog.setTitle("Sorry!");
            storageDialog.setMessage("Looks like you forgot to declare the WRITE_EXTERNAL_STORAGE in your manifest file.");
            storageDialog.setButton(-3, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    storageDialog.cancel();
                }
            });
            storageDialog.show();
        }
    }

    public void startConversionTrackerWithGoalId(String goalId) {
        this._goalId = goalId;
        SharedPreferences settings = getContext().getSharedPreferences("MillennialMediaSettings", 0);
        final boolean isFirstLaunch = settings.getBoolean("firstLaunch", true);
        if (isFirstLaunch) {
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("firstLaunch", false);
            editor.commit();
        }
        if (MMAdViewSDK.connectivityManager.getActiveNetworkInfo() == null || !MMAdViewSDK.connectivityManager.getActiveNetworkInfo().isConnected()) {
            Log.w(MMAdViewSDK.SDKLOG, "No network available for conversion tracking.");
        } else {
            new Thread() {
                public void run() {
                    try {
                        new HttpGetRequest().trackConversion(MMAdView.this._goalId, MMAdView.this.auid, isFirstLaunch);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    }

    public static void startConversionTrackerWithGoalId(Context context, final String goalId) {
        synchronized (context) {
            SharedPreferences settings = context.getSharedPreferences("MillennialMediaSettings", 0);
            final boolean isFirstLaunch = settings.getBoolean("firstLaunch", true);
            if (isFirstLaunch) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("firstLaunch", false);
                editor.commit();
            }
            checkPermissions((Activity) context);
            if (MMAdViewSDK.connectivityManager == null) {
                MMAdViewSDK.connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            }
            final String auid2 = getAuid((Activity) context);
            if (MMAdViewSDK.connectivityManager.getActiveNetworkInfo() == null || !MMAdViewSDK.connectivityManager.getActiveNetworkInfo().isConnected()) {
                Log.w(MMAdViewSDK.SDKLOG, "No network available for conversion tracking.");
            } else {
                new Thread() {
                    public void run() {
                        try {
                            new HttpGetRequest().trackConversion(goalId, auid2, isFirstLaunch);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            }
        }
    }
}
