package magmamobile.app;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.io.InputStream;

public class SplashActivity extends Activity {
    /* access modifiers changed from: private */
    public BitmapDrawable image;
    private ImageView imageview;
    private LinearLayout layout;
    /* access modifiers changed from: private */
    public Class<?> mActivity;
    /* access modifiers changed from: private */
    public boolean mCancel;
    /* access modifiers changed from: private */
    public long mDuration;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public String mLogoAsset;
    /* access modifiers changed from: private */
    public long mStartTime;

    public SplashActivity(Class<?> activity) {
        this(activity, "magmamobile.png");
    }

    public SplashActivity(Class<?> activity, String logoAsset) {
        this(activity, logoAsset, 100, 3000);
    }

    public SplashActivity(Class<?> activity, String logoAsset, long startTime, long duration) {
        this.mActivity = activity;
        this.mLogoAsset = logoAsset;
        this.mStartTime = startTime;
        this.mDuration = duration;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                SplashActivity.this.handleMessage(msg);
            }
        };
        this.imageview = new ImageView(this);
        this.layout = new LinearLayout(this);
        this.layout.setBackgroundColor(-1);
        this.layout.setGravity(17);
        this.layout.addView(this.imageview);
        setContentView(this.layout);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        new Thread() {
            public void run() {
                try {
                    InputStream stream = SplashActivity.this.getAssets().open(SplashActivity.this.mLogoAsset);
                    SplashActivity.this.image = new BitmapDrawable(stream);
                    stream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(SplashActivity.this.mStartTime);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                SplashActivity.this.mHandler.sendEmptyMessage(0);
                try {
                    Thread.sleep(SplashActivity.this.mDuration);
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                if (!SplashActivity.this.mCancel && SplashActivity.this.mActivity != null) {
                    SplashActivity.this.startActivity(new Intent(SplashActivity.this, SplashActivity.this.mActivity));
                }
                SplashActivity.this.finish();
            }
        }.start();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.mCancel = true;
    }

    public boolean handleMessage(Message msg) {
        float mx = (float) (this.image.getIntrinsicWidth() >> 1);
        float my = (float) (this.image.getIntrinsicHeight() >> 1);
        int width = getWindowManager().getDefaultDisplay().getWidth();
        AnimationSet anims = new AnimationSet(true);
        TranslateAnimation anim2 = new TranslateAnimation(0.0f, 0.0f, -500.0f, 0.0f);
        anim2.setDuration(1000);
        anims.addAnimation(anim2);
        ScaleAnimation anim3 = new ScaleAnimation(10.0f, 1.0f, 10.0f, 1.0f, mx, my);
        anim3.setDuration(1000);
        anims.addAnimation(anim3);
        RotateAnimation anim4 = new RotateAnimation(360.0f, 0.0f, mx, my);
        anim4.setDuration(1000);
        anims.addAnimation(anim4);
        this.imageview.setBackgroundDrawable(this.image);
        this.imageview.setLayoutParams(new LinearLayout.LayoutParams(width, (int) (((float) width) * (my / mx))));
        this.imageview.startAnimation(anims);
        return true;
    }
}
