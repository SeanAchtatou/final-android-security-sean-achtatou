package eu.reman.sudokusolver;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int fbg = 2130837504;
        public static final int icon = 2130837505;
    }

    public static final class id {
        public static final int adView = 2131034117;
        public static final int button0 = 2131034130;
        public static final int button1 = 2131034118;
        public static final int button2 = 2131034119;
        public static final int button3 = 2131034120;
        public static final int button4 = 2131034122;
        public static final int button5 = 2131034123;
        public static final int button6 = 2131034124;
        public static final int button7 = 2131034126;
        public static final int button8 = 2131034127;
        public static final int button9 = 2131034128;
        public static final int cancel = 2131034131;
        public static final int clear = 2131034115;
        public static final int clearCalc = 2131034114;
        public static final int imageView1 = 2131034112;
        public static final int linearLayout1 = 2131034113;
        public static final int linearLayout2 = 2131034121;
        public static final int linearLayout3 = 2131034125;
        public static final int linearLayout4 = 2131034129;
        public static final int solve = 2131034116;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int numbers = 2130903041;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
    }
}
