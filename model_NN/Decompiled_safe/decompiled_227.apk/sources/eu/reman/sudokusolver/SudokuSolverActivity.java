package eu.reman.sudokusolver;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class SudokuSolverActivity extends Activity {
    public static int DISPLAY_HEIGHT;
    public static int DISPLAY_WIDTH;
    final int MIN_NUMBERS = 11;
    AlertDialog.Builder _clearBox;
    SudokuField _field;
    AlertDialog.Builder _minNumbersBox;
    AlertDialog.Builder _noSolutionBox;
    AlertDialog.Builder _startNumbersBox;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        Display display = getWindowManager().getDefaultDisplay();
        DISPLAY_WIDTH = display.getWidth();
        DISPLAY_HEIGHT = display.getHeight();
        this._field = (SudokuField) findViewById(R.id.imageView1);
        this._clearBox = new AlertDialog.Builder(this);
        this._clearBox.setMessage("Clear field?");
        this._clearBox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SudokuSolverActivity.this._field.ClearField();
                SudokuSolverActivity.this._field.invalidate();
            }
        });
        this._clearBox.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        this._minNumbersBox = new AlertDialog.Builder(this);
        this._minNumbersBox.setMessage("Please enter at least 11 numbers!");
        this._minNumbersBox.setNeutralButton("Ok", (DialogInterface.OnClickListener) null);
        this._startNumbersBox = new AlertDialog.Builder(this);
        this._startNumbersBox.setMessage("Wrong numbers!");
        this._startNumbersBox.setNeutralButton("Ok", (DialogInterface.OnClickListener) null);
        this._noSolutionBox = new AlertDialog.Builder(this);
        this._noSolutionBox.setMessage("No solution!");
        this._noSolutionBox.setNeutralButton("Ok", (DialogInterface.OnClickListener) null);
        Button clearResult = (Button) findViewById(R.id.clearCalc);
        clearResult.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SudokuSolverActivity.this._field.ClearResult();
                SudokuSolverActivity.this._field.invalidate();
            }
        });
        Button clear = (Button) findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SudokuSolverActivity.this._clearBox.show();
            }
        });
        Button solve = (Button) findViewById(R.id.solve);
        solve.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SudokuSolverActivity.this._field.CountStartNumbers() < 11) {
                    SudokuSolverActivity.this._minNumbersBox.show();
                    return;
                }
                if (!SudokuSolverActivity.this._field.CheckStartNumbers()) {
                    SudokuSolverActivity.this._startNumbersBox.show();
                } else if (!SudokuSolverActivity.this._field.Solve()) {
                    SudokuSolverActivity.this._noSolutionBox.show();
                    SudokuSolverActivity.this._field.ClearResult();
                }
                SudokuSolverActivity.this._field.invalidate();
            }
        });
        AdView adView = (AdView) findViewById(R.id.adView);
        adView.loadAd(new AdRequest());
        LinearLayout lin = (LinearLayout) findViewById(R.id.linearLayout1);
        switch (DISPLAY_WIDTH) {
            case 320:
                this._field.getLayoutParams().width = 270;
                this._field.getLayoutParams().height = 351;
                clearResult.getLayoutParams().width = 100;
                clearResult.setTextSize(10.0f);
                clear.getLayoutParams().width = 100;
                clear.setTextSize(10.0f);
                solve.getLayoutParams().width = 100;
                solve.setTextSize(10.0f);
                lin.getLayoutParams().height = 30;
                adView.getLayoutParams().width = 320;
                adView.getLayoutParams().height = 50;
                return;
            default:
                return;
        }
    }
}
