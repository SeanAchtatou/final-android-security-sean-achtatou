package eu.reman.sudokusolver;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class SudokuField extends ImageView {
    Bitmap _background;
    List<Point> _errors;
    byte[][] _field;
    Point _lastNumber = null;
    Dialog _numbers = null;
    Point _selected = null;
    byte[][] _startField;

    public SudokuField(Context context) {
        super(context);
        Init(context);
    }

    public SudokuField(Context context, AttributeSet attrs) {
        super(context, attrs);
        Init(context);
    }

    /* access modifiers changed from: package-private */
    public int getBlockWidth() {
        switch (SudokuSolverActivity.DISPLAY_WIDTH) {
            case 320:
                return 30;
            default:
                return 50;
        }
    }

    /* access modifiers changed from: package-private */
    public int getBlockHeight() {
        switch (SudokuSolverActivity.DISPLAY_WIDTH) {
            case 320:
                return 39;
            default:
                return 65;
        }
    }

    /* access modifiers changed from: package-private */
    public void Init(Context context) {
        this._field = (byte[][]) Array.newInstance(Byte.TYPE, 9, 9);
        this._startField = (byte[][]) Array.newInstance(Byte.TYPE, 9, 9);
        this._errors = new ArrayList();
        this._background = BitmapFactory.decodeResource(getResources(), R.drawable.fbg);
        ClearField();
        SetupDialog(context);
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        int i;
        super.dispatchDraw(canvas);
        canvas.drawBitmap(this._background, 0.0f, 0.0f, (Paint) null);
        if (this._selected != null) {
            Paint rect = new Paint();
            rect.setColor(Color.argb(255, 100, 100, 100));
            canvas.drawRect(new Rect(this._selected.x * getBlockWidth(), this._selected.y * getBlockHeight(), (this._selected.x * getBlockWidth()) + getBlockWidth(), (this._selected.y * getBlockHeight()) + getBlockHeight()), rect);
        }
        Paint paint = new Paint();
        paint.setTextAlign(Paint.Align.CENTER);
        switch (SudokuSolverActivity.DISPLAY_WIDTH) {
            case 320:
                paint.setTextSize(28.0f);
                break;
            default:
                paint.setTextSize(40.0f);
                break;
        }
        for (int x = 0; x < this._field.length; x++) {
            for (int y = 0; y < this._field[x].length; y++) {
                if (this._field[x][y] > 0) {
                    if (!this._errors.contains(new Point(x, y))) {
                        if (this._startField[x][y] == 0) {
                            i = 0;
                        } else {
                            i = 255;
                        }
                        paint.setARGB(255, 0, 0, i);
                    } else {
                        paint.setARGB(255, 255, 0, 0);
                    }
                    switch (SudokuSolverActivity.DISPLAY_WIDTH) {
                        case 320:
                            canvas.drawText(String.valueOf((int) this._field[x][y]), (float) ((x * 30) + 15), (float) ((y * 39) + 30), paint);
                            break;
                        default:
                            canvas.drawText(String.valueOf((int) this._field[x][y]), (float) ((x * 50) + 25), (float) ((y * 65) + 50), paint);
                            break;
                    }
                }
            }
        }
        super.dispatchDraw(canvas);
    }

    /* access modifiers changed from: package-private */
    public boolean CanPlace(int px, int py, byte number) {
        for (int x = 0; x < this._field.length; x++) {
            if (px != x && this._field[x][py] == number) {
                return false;
            }
        }
        for (int y = 0; y < this._field[px].length; y++) {
            if (py != y && this._field[px][y] == number) {
                return false;
            }
        }
        for (int x2 = (px / 3) * 3; x2 < ((px / 3) * 3) + 3; x2++) {
            for (int y2 = (py / 3) * 3; y2 < ((py / 3) * 3) + 3; y2++) {
                if (py != x2 && py != y2 && this._field[x2][y2] == number) {
                    return false;
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean FillFixedNumbers() {
        for (int x = 0; x < this._field.length; x++) {
            for (int y = 0; y < this._field[x].length; y++) {
                if (this._field[x][y] == 0) {
                    int places = 0;
                    byte lastNum = 0;
                    for (byte i = 1; i <= 9; i = (byte) (i + 1)) {
                        if (CanPlace(x, y, i)) {
                            places++;
                            lastNum = i;
                        }
                    }
                    if (places == 1) {
                        this._field[x][y] = lastNum;
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean FillBacktrack() {
        for (int x = 0; x < this._field.length; x++) {
            for (int y = 0; y < this._field[x].length; y++) {
                if (this._field[x][y] == 0) {
                    List<Byte> numbers = new ArrayList<>();
                    for (byte i = 1; i <= 9; i = (byte) (i + 1)) {
                        if (CanPlace(x, y, i)) {
                            numbers.add(Byte.valueOf(i));
                        }
                    }
                    if (numbers.size() == 0) {
                        return false;
                    }
                    for (Byte byteValue : numbers) {
                        this._field[x][y] = byteValue.byteValue();
                        if (!IsFilled()) {
                            if (FillBacktrack()) {
                                return true;
                            }
                        } else if (IsSolved()) {
                            return true;
                        }
                    }
                    this._field[x][y] = 0;
                    return false;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean IsFilled() {
        for (int x = 0; x < this._field.length; x++) {
            for (byte b : this._field[x]) {
                if (b == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean IsSolved() {
        for (int x = 0; x < this._field.length; x++) {
            for (int y = 0; y < this._field[x].length; y++) {
                if (!CanPlace(x, y, this._field[x][y])) {
                    return false;
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void SetupDialog(Context context) {
        this._numbers = new Dialog(context);
        this._numbers.setTitle("Select number");
        this._numbers.setContentView((int) R.layout.numbers);
        ((Button) this._numbers.findViewById(R.id.button1)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SudokuField.this._numbers.hide();
                SudokuField.this.SetLastNumber(1);
            }
        });
        ((Button) this._numbers.findViewById(R.id.button2)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SudokuField.this._numbers.hide();
                SudokuField.this.SetLastNumber(2);
            }
        });
        ((Button) this._numbers.findViewById(R.id.button3)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SudokuField.this._numbers.hide();
                SudokuField.this.SetLastNumber(3);
            }
        });
        ((Button) this._numbers.findViewById(R.id.button4)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SudokuField.this._numbers.hide();
                SudokuField.this.SetLastNumber(4);
            }
        });
        ((Button) this._numbers.findViewById(R.id.button5)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SudokuField.this._numbers.hide();
                SudokuField.this.SetLastNumber(5);
            }
        });
        ((Button) this._numbers.findViewById(R.id.button6)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SudokuField.this._numbers.hide();
                SudokuField.this.SetLastNumber(6);
            }
        });
        ((Button) this._numbers.findViewById(R.id.button7)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SudokuField.this._numbers.hide();
                SudokuField.this.SetLastNumber(7);
            }
        });
        ((Button) this._numbers.findViewById(R.id.button8)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SudokuField.this._numbers.hide();
                SudokuField.this.SetLastNumber(8);
            }
        });
        ((Button) this._numbers.findViewById(R.id.button9)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SudokuField.this._numbers.hide();
                SudokuField.this.SetLastNumber(9);
            }
        });
        ((Button) this._numbers.findViewById(R.id.button0)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SudokuField.this._numbers.hide();
                SudokuField.this.SetLastNumber(0);
            }
        });
        ((Button) this._numbers.findViewById(R.id.cancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SudokuField.this._numbers.hide();
                SudokuField.this.invalidate();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void SetLastNumber(int number) {
        if (this._lastNumber != null) {
            byte[] bArr = this._field[this._lastNumber.x];
            int i = this._lastNumber.y;
            byte b = (byte) number;
            this._startField[this._lastNumber.x][this._lastNumber.y] = b;
            bArr[i] = b;
            if (this._errors.contains(new Point(this._lastNumber.x, this._lastNumber.y))) {
                this._errors.remove(new Point(this._lastNumber.x, this._lastNumber.y));
            }
            invalidate();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int fieldX;
        int fieldY;
        int fieldX2;
        int fieldY2;
        int fieldX3;
        int fieldY3;
        switch (event.getAction()) {
            case 0:
                switch (SudokuSolverActivity.DISPLAY_WIDTH) {
                    case 320:
                        fieldX3 = 30;
                        fieldY3 = 39;
                        break;
                    default:
                        fieldX3 = 50;
                        fieldY3 = 65;
                        break;
                }
                this._selected = new Point((int) (event.getX() / ((float) fieldX3)), (int) (event.getY() / ((float) fieldY3)));
                invalidate();
                return true;
            case 1:
                switch (SudokuSolverActivity.DISPLAY_WIDTH) {
                    case 320:
                        fieldX = 30;
                        fieldY = 39;
                        break;
                    default:
                        fieldX = 50;
                        fieldY = 65;
                        break;
                }
                int x = (int) (event.getX() / ((float) fieldX));
                int y = (int) (event.getY() / ((float) fieldY));
                if (x >= 0 && x < this._field.length && y >= 0 && y < this._field[0].length) {
                    this._lastNumber = new Point(x, y);
                    this._selected = null;
                    this._numbers.show();
                }
                invalidate();
                return true;
            case 2:
                switch (SudokuSolverActivity.DISPLAY_WIDTH) {
                    case 320:
                        fieldX2 = 30;
                        fieldY2 = 39;
                        break;
                    default:
                        fieldX2 = 50;
                        fieldY2 = 65;
                        break;
                }
                this._selected = new Point((int) (event.getX() / ((float) fieldX2)), (int) (event.getY() / ((float) fieldY2)));
                invalidate();
                return true;
            default:
                return true;
        }
    }

    public void ClearResult() {
        for (int x = 0; x < this._field.length; x++) {
            for (int y = 0; y < this._field[x].length; y++) {
                this._field[x][y] = this._startField[x][y];
            }
        }
    }

    public void ClearField() {
        for (int x = 0; x < this._field.length; x++) {
            for (int y = 0; y < this._field[x].length; y++) {
                this._field[x][y] = 0;
                this._startField[x][y] = 0;
            }
        }
        this._errors.clear();
    }

    public int CountStartNumbers() {
        int count = 0;
        for (int x = 0; x < this._field.length; x++) {
            for (int y = 0; y < this._field[x].length; y++) {
                if (this._startField[x][y] > 0) {
                    count++;
                }
            }
        }
        return count;
    }

    public boolean CheckStartNumbers() {
        this._errors.clear();
        for (int x = 0; x < this._field.length; x++) {
            for (int y = 0; y < this._field[x].length; y++) {
                if (this._startField[x][y] > 0 && !CanPlace(x, y, this._startField[x][y])) {
                    this._errors.add(new Point(x, y));
                }
            }
        }
        if (this._errors.size() == 0) {
            return true;
        }
        return false;
    }

    public boolean Solve() {
        ClearResult();
        do {
        } while (FillFixedNumbers());
        return FillBacktrack();
    }
}
