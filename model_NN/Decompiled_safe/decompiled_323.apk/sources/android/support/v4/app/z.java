package android.support.v4.app;

import android.support.v4.b.a;
import android.support.v4.b.c;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* compiled from: ProGuard */
class z extends x {

    /* renamed from: a  reason: collision with root package name */
    static boolean f66a = false;
    final c<aa> b = new c<>();
    final c<aa> c = new c<>();
    final String d;
    FragmentActivity e;
    boolean f;
    boolean g;

    z(String str, FragmentActivity fragmentActivity, boolean z) {
        this.d = str;
        this.e = fragmentActivity;
        this.f = z;
    }

    /* access modifiers changed from: package-private */
    public void a(FragmentActivity fragmentActivity) {
        this.e = fragmentActivity;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (f66a) {
            Log.v("LoaderManager", "Starting in " + this);
        }
        if (this.f) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStart when already started: " + this, runtimeException);
            return;
        }
        this.f = true;
        for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
            this.b.b(a2).a();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (f66a) {
            Log.v("LoaderManager", "Stopping in " + this);
        }
        if (!this.f) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStop when not started: " + this, runtimeException);
            return;
        }
        for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
            this.b.b(a2).e();
        }
        this.f = false;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (f66a) {
            Log.v("LoaderManager", "Retaining in " + this);
        }
        if (!this.f) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doRetain when not started: " + this, runtimeException);
            return;
        }
        this.g = true;
        this.f = false;
        for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
            this.b.b(a2).b();
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (this.g) {
            if (f66a) {
                Log.v("LoaderManager", "Finished Retaining in " + this);
            }
            this.g = false;
            for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
                this.b.b(a2).c();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
            this.b.b(a2).k = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
            this.b.b(a2).d();
        }
    }

    /* access modifiers changed from: package-private */
    public void h() {
        if (!this.g) {
            if (f66a) {
                Log.v("LoaderManager", "Destroying Active in " + this);
            }
            for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
                this.b.b(a2).f();
            }
        }
        if (f66a) {
            Log.v("LoaderManager", "Destroying Inactive in " + this);
        }
        for (int a3 = this.c.a() - 1; a3 >= 0; a3--) {
            this.c.b(a3).f();
        }
        this.c.b();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        a.a(this.e, sb);
        sb.append("}}");
        return sb.toString();
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        if (this.b.a() > 0) {
            printWriter.print(str);
            printWriter.println("Active Loaders:");
            String str2 = str + "    ";
            for (int i = 0; i < this.b.a(); i++) {
                aa b2 = this.b.b(i);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.b.a(i));
                printWriter.print(": ");
                printWriter.println(b2.toString());
                b2.a(str2, fileDescriptor, printWriter, strArr);
            }
        }
        if (this.c.a() > 0) {
            printWriter.print(str);
            printWriter.println("Inactive Loaders:");
            String str3 = str + "    ";
            for (int i2 = 0; i2 < this.c.a(); i2++) {
                aa b3 = this.c.b(i2);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.c.a(i2));
                printWriter.print(": ");
                printWriter.println(b3.toString());
                b3.a(str3, fileDescriptor, printWriter, strArr);
            }
        }
    }

    public boolean a() {
        boolean z;
        int a2 = this.b.a();
        boolean z2 = false;
        for (int i = 0; i < a2; i++) {
            aa b2 = this.b.b(i);
            if (!b2.h || b2.f) {
                z = false;
            } else {
                z = true;
            }
            z2 |= z;
        }
        return z2;
    }
}
