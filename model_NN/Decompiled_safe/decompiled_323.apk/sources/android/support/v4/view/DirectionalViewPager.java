package android.support.v4.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.a.a;
import android.support.v4.widget.d;
import android.util.AttributeSet;
import android.util.Log;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Interpolator;
import android.widget.Scroller;
import com.qq.ndk.NativeFileObject;
import com.tencent.android.qqdownloader.b;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.tmsecurelite.optimize.ISystemOptimize;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/* compiled from: ProGuard */
public class DirectionalViewPager extends ViewGroup {
    private static final int CLOSE_ENOUGH = 2;
    private static final Comparator<m> COMPARATOR = new i();
    private static final boolean DEBUG = false;
    private static final int DEFAULT_GUTTER_SIZE = 16;
    private static final int DEFAULT_OFFSCREEN_PAGES = 1;
    private static final int DRAW_ORDER_DEFAULT = 0;
    private static final int DRAW_ORDER_FORWARD = 1;
    private static final int DRAW_ORDER_REVERSE = 2;
    public static final int HORIZONTAL = 0;
    private static final int INVALID_POINTER = -1;
    /* access modifiers changed from: private */
    public static final int[] LAYOUT_ATTRS = {16842931};
    private static final int MAX_SETTLE_DURATION = 600;
    private static final int MIN_DISTANCE_FOR_FLING = 25;
    private static final int MIN_FLING_VELOCITY = 400;
    public static final int SCROLL_STATE_DRAGGING = 1;
    public static final int SCROLL_STATE_IDLE = 0;
    public static final int SCROLL_STATE_SETTLING = 2;
    private static final String TAG = "ViewPager";
    private static final boolean USE_CACHE = false;
    public static final int VERTICAL = 1;
    private static final Interpolator sInterpolator = new j();
    private static final r sPositionComparator = new r();
    private int mActivePointerId = -1;
    /* access modifiers changed from: private */
    public PagerAdapter mAdapter;
    private o mAdapterChangeListener;
    private int mBottomPageBounds;
    private boolean mCalledSuper;
    private int mChildHeightMeasureSpec;
    private int mChildWidthMeasureSpec;
    private int mCloseEnough;
    /* access modifiers changed from: private */
    public int mCurItem;
    private int mDecorChildCount;
    private int mDefaultGutterSize;
    private int mDrawingOrder;
    private ArrayList<View> mDrawingOrderedChildren;
    private d mEndEdge;
    private Matrix mEndEdgeMatrix;
    private final Runnable mEndScrollRunnable = new k(this);
    private int mExpectedAdapterCount;
    private long mFakeDragBeginTime;
    private boolean mFakeDragging;
    private boolean mFirstLayout = true;
    private float mFirstOffset = -3.4028235E38f;
    private int mFlingDistance;
    private int mGutterSize;
    private boolean mIgnoreGutter;
    private boolean mInLayout;
    private float mInitialMotionX;
    private float mInitialMotionY;
    private OnPageChangeListener mInternalPageChangeListener;
    private boolean mIsBeingDragged;
    private boolean mIsUnableToDrag;
    private final ArrayList<m> mItems = new ArrayList<>();
    private float mLastMotionX;
    private float mLastMotionY;
    private float mLastOffset = Float.MAX_VALUE;
    private int mLeftPageBounds;
    private Drawable mMarginDrawable;
    private int mMaximumVelocity;
    private int mMinimumVelocity;
    private boolean mNeedCalculatePageOffsets = false;
    private p mObserver;
    private int mOffscreenPageLimit = 1;
    private OnPageChangeListener mOnPageChangeListener;
    private int mOrientation = 0;
    private int mPageMargin;
    private PageTransformer mPageTransformer;
    private boolean mPopulatePending;
    private Parcelable mRestoredAdapterState = null;
    private ClassLoader mRestoredClassLoader = null;
    private int mRestoredCurItem = -1;
    private int mRightPageBounds;
    private int mScrollState = 0;
    private Scroller mScroller;
    private boolean mScrollingCacheEnabled;
    private Method mSetChildrenDrawingOrderEnabled;
    private d mStartEdge;
    private final m mTempItem = new m();
    private final Rect mTempRect = new Rect();
    private int mTopPageBounds;
    private int mTouchSlop;
    private VelocityTracker mVelocityTracker;

    /* compiled from: ProGuard */
    public interface OnPageChangeListener {
        void onPageScrollStateChanged(int i);

        void onPageScrolled(int i, float f, int i2);

        void onPageSelected(int i);
    }

    /* compiled from: ProGuard */
    public interface PageTransformer {
        void transformPage(View view, float f);
    }

    /* compiled from: ProGuard */
    public class SimpleOnPageChangeListener implements OnPageChangeListener {
        public void onPageScrolled(int i, float f, int i2) {
        }

        public void onPageSelected(int i) {
        }

        public void onPageScrollStateChanged(int i) {
        }
    }

    public DirectionalViewPager(Context context) {
        super(context);
        initViewPager();
    }

    public DirectionalViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, b.c);
        int i = obtainStyledAttributes.getInt(0, -1);
        if (i == 0 || i == 1) {
            setOrientation(i);
        }
        obtainStyledAttributes.recycle();
        initViewPager();
    }

    /* access modifiers changed from: package-private */
    public void initViewPager() {
        setWillNotDraw(false);
        setDescendantFocusability(NativeFileObject.S_IFDIR);
        setFocusable(true);
        Context context = getContext();
        this.mScroller = new Scroller(context, sInterpolator);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        float f = context.getResources().getDisplayMetrics().density;
        this.mTouchSlop = ViewConfigurationCompat.getScaledPagingTouchSlop(viewConfiguration);
        this.mMinimumVelocity = (int) (400.0f * f);
        this.mMaximumVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
        this.mStartEdge = new d(context);
        this.mEndEdge = new d(context);
        this.mFlingDistance = (int) (25.0f * f);
        this.mCloseEnough = (int) (2.0f * f);
        this.mDefaultGutterSize = (int) (16.0f * f);
        ViewCompat.setAccessibilityDelegate(this, new n(this));
        if (ViewCompat.getImportantForAccessibility(this) == 0) {
            ViewCompat.setImportantForAccessibility(this, 1);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.mEndScrollRunnable);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: private */
    public void setScrollState(int i) {
        if (this.mScrollState != i) {
            this.mScrollState = i;
            if (this.mPageTransformer != null) {
                enableLayers(i != 0);
            }
            if (this.mOnPageChangeListener != null) {
                this.mOnPageChangeListener.onPageScrollStateChanged(i);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.PagerAdapter.destroyItem(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.DirectionalViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.PagerAdapter.destroyItem(android.view.View, int, java.lang.Object):void
      android.support.v4.view.PagerAdapter.destroyItem(android.view.ViewGroup, int, java.lang.Object):void */
    public void setAdapter(PagerAdapter pagerAdapter) {
        if (this.mAdapter != null) {
            this.mAdapter.unregisterDataSetObserver(this.mObserver);
            this.mAdapter.startUpdate((ViewGroup) this);
            for (int i = 0; i < this.mItems.size(); i++) {
                m mVar = this.mItems.get(i);
                this.mAdapter.destroyItem((ViewGroup) this, mVar.b, mVar.f94a);
            }
            this.mAdapter.finishUpdate((ViewGroup) this);
            this.mItems.clear();
            removeNonDecorViews();
            this.mCurItem = 0;
            scrollTo(0, 0);
        }
        PagerAdapter pagerAdapter2 = this.mAdapter;
        this.mAdapter = pagerAdapter;
        this.mExpectedAdapterCount = 0;
        if (this.mAdapter != null) {
            if (this.mObserver == null) {
                this.mObserver = new p(this, null);
            }
            this.mAdapter.registerDataSetObserver(this.mObserver);
            this.mPopulatePending = false;
            boolean z = this.mFirstLayout;
            this.mFirstLayout = true;
            this.mExpectedAdapterCount = this.mAdapter.getCount();
            if (this.mRestoredCurItem >= 0) {
                this.mAdapter.restoreState(this.mRestoredAdapterState, this.mRestoredClassLoader);
                setCurrentItemInternal(this.mRestoredCurItem, false, true);
                this.mRestoredCurItem = -1;
                this.mRestoredAdapterState = null;
                this.mRestoredClassLoader = null;
            } else if (!z) {
                populate();
            } else {
                requestLayout();
            }
        }
        if (this.mAdapterChangeListener != null && pagerAdapter2 != pagerAdapter) {
            this.mAdapterChangeListener.a(pagerAdapter2, pagerAdapter);
        }
    }

    private void removeNonDecorViews() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < getChildCount()) {
                if (!((LayoutParams) getChildAt(i2).getLayoutParams()).isDecor) {
                    removeViewAt(i2);
                    i2--;
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public PagerAdapter getAdapter() {
        return this.mAdapter;
    }

    /* access modifiers changed from: package-private */
    public void setOnAdapterChangeListener(o oVar) {
        this.mAdapterChangeListener = oVar;
    }

    private int getClientDimension() {
        return this.mOrientation == 0 ? getClientWidth() : getClientHeight();
    }

    private int getClientHeight() {
        return (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom();
    }

    private int getClientWidth() {
        return (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
    }

    public void setCurrentItem(int i) {
        boolean z;
        this.mPopulatePending = false;
        if (!this.mFirstLayout) {
            z = true;
        } else {
            z = false;
        }
        setCurrentItemInternal(i, z, false);
    }

    public void setCurrentItem(int i, boolean z) {
        this.mPopulatePending = false;
        setCurrentItemInternal(i, z, false);
    }

    public int getCurrentItem() {
        return this.mCurItem;
    }

    /* access modifiers changed from: package-private */
    public void setCurrentItemInternal(int i, boolean z, boolean z2) {
        setCurrentItemInternal(i, z, z2, 0);
    }

    /* access modifiers changed from: package-private */
    public void setCurrentItemInternal(int i, boolean z, boolean z2, int i2) {
        boolean z3 = false;
        if (this.mAdapter == null || this.mAdapter.getCount() <= 0) {
            setScrollingCacheEnabled(false);
        } else if (z2 || this.mCurItem != i || this.mItems.size() == 0) {
            if (i < 0) {
                i = 0;
            } else if (i >= this.mAdapter.getCount()) {
                i = this.mAdapter.getCount() - 1;
            }
            int i3 = this.mOffscreenPageLimit;
            if (i > this.mCurItem + i3 || i < this.mCurItem - i3) {
                for (int i4 = 0; i4 < this.mItems.size(); i4++) {
                    this.mItems.get(i4).c = true;
                }
            }
            if (this.mCurItem != i) {
                z3 = true;
            }
            if (this.mFirstLayout) {
                this.mCurItem = i;
                if (z3 && this.mOnPageChangeListener != null) {
                    this.mOnPageChangeListener.onPageSelected(i);
                }
                if (z3 && this.mInternalPageChangeListener != null) {
                    this.mInternalPageChangeListener.onPageSelected(i);
                }
                requestLayout();
                return;
            }
            populate(i);
            scrollToItem(i, z, i2, z3);
        } else {
            setScrollingCacheEnabled(false);
        }
    }

    private void scrollToItem(int i, boolean z, int i2, boolean z2) {
        int i3;
        m infoForPosition = infoForPosition(i);
        if (infoForPosition != null) {
            i3 = (int) (Math.max(this.mFirstOffset, Math.min(infoForPosition.e, this.mLastOffset)) * ((float) getClientDimension()));
        } else {
            i3 = 0;
        }
        if (z) {
            if (this.mOrientation == 0) {
                smoothScrollTo(i3, 0, i2);
            } else {
                smoothScrollTo(0, i3, i2);
            }
            if (z2 && this.mOnPageChangeListener != null) {
                this.mOnPageChangeListener.onPageSelected(i);
            }
            if (z2 && this.mInternalPageChangeListener != null) {
                this.mInternalPageChangeListener.onPageSelected(i);
                return;
            }
            return;
        }
        if (z2 && this.mOnPageChangeListener != null) {
            this.mOnPageChangeListener.onPageSelected(i);
        }
        if (z2 && this.mInternalPageChangeListener != null) {
            this.mInternalPageChangeListener.onPageSelected(i);
        }
        completeScroll(false);
        if (this.mOrientation == 0) {
            scrollTo(i3, 0);
        } else {
            scrollTo(0, i3);
        }
        pageScrolled(i3);
    }

    public void setOnPageChangeListener(OnPageChangeListener onPageChangeListener) {
        this.mOnPageChangeListener = onPageChangeListener;
    }

    public void setOrientation(int i) {
        this.mOrientation = i;
    }

    public void setPageTransformer(boolean z, PageTransformer pageTransformer) {
        boolean z2;
        int i = 1;
        if (Build.VERSION.SDK_INT >= 11) {
            boolean z3 = pageTransformer != null;
            if (this.mPageTransformer != null) {
                z2 = true;
            } else {
                z2 = false;
            }
            boolean z4 = z3 != z2;
            this.mPageTransformer = pageTransformer;
            setChildrenDrawingOrderEnabledCompat(z3);
            if (z3) {
                if (z) {
                    i = 2;
                }
                this.mDrawingOrder = i;
            } else {
                this.mDrawingOrder = 0;
            }
            if (z4) {
                populate();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void setChildrenDrawingOrderEnabledCompat(boolean z) {
        if (Build.VERSION.SDK_INT >= 7) {
            if (this.mSetChildrenDrawingOrderEnabled == null) {
                Class<ViewGroup> cls = ViewGroup.class;
                try {
                    this.mSetChildrenDrawingOrderEnabled = cls.getDeclaredMethod("setChildrenDrawingOrderEnabled", Boolean.TYPE);
                } catch (NoSuchMethodException e) {
                    Log.e(TAG, "Can't find setChildrenDrawingOrderEnabled", e);
                }
            }
            try {
                this.mSetChildrenDrawingOrderEnabled.invoke(this, Boolean.valueOf(z));
            } catch (Exception e2) {
                Log.e(TAG, "Error changing children drawing order", e2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i, int i2) {
        if (this.mDrawingOrder == 2) {
            i2 = (i - 1) - i2;
        }
        return ((LayoutParams) this.mDrawingOrderedChildren.get(i2).getLayoutParams()).childIndex;
    }

    /* access modifiers changed from: package-private */
    public OnPageChangeListener setInternalPageChangeListener(OnPageChangeListener onPageChangeListener) {
        OnPageChangeListener onPageChangeListener2 = this.mInternalPageChangeListener;
        this.mInternalPageChangeListener = onPageChangeListener;
        return onPageChangeListener2;
    }

    private void setLastOffset(float f) {
        if (f != this.mLastOffset) {
            this.mEndEdgeMatrix = null;
            this.mLastOffset = f;
        }
    }

    public int getOffscreenPageLimit() {
        return this.mOffscreenPageLimit;
    }

    public void setOffscreenPageLimit(int i) {
        if (i < 1) {
            Log.w(TAG, "Requested offscreen page limit " + i + " too small; defaulting to " + 1);
            i = 1;
        }
        if (i != this.mOffscreenPageLimit) {
            this.mOffscreenPageLimit = i;
            populate();
        }
    }

    public void setPageMargin(int i) {
        int i2 = this.mPageMargin;
        this.mPageMargin = i;
        int width = getWidth();
        recomputeScrollPosition(width, width, i, i2);
        requestLayout();
    }

    public int getPageMargin() {
        return this.mPageMargin;
    }

    public int getScrollCoord() {
        return this.mOrientation == 0 ? getScrollX() : getScrollY();
    }

    public void setPageMarginDrawable(Drawable drawable) {
        this.mMarginDrawable = drawable;
        if (drawable != null) {
            refreshDrawableState();
        }
        setWillNotDraw(drawable == null);
        invalidate();
    }

    public void setPageMarginDrawable(int i) {
        setPageMarginDrawable(getContext().getResources().getDrawable(i));
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.mMarginDrawable;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.mMarginDrawable;
        if (drawable != null && drawable.isStateful()) {
            drawable.setState(getDrawableState());
        }
    }

    /* access modifiers changed from: package-private */
    public float distanceInfluenceForSnapDuration(float f) {
        return (float) Math.sin((double) ((float) (((double) (f - 0.5f)) * 0.4712389167638204d)));
    }

    /* access modifiers changed from: package-private */
    public void smoothScrollTo(int i, int i2) {
        smoothScrollTo(i, i2, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: package-private */
    public void smoothScrollTo(int i, int i2, int i3) {
        int abs;
        if (getChildCount() == 0) {
            setScrollingCacheEnabled(false);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int i4 = i - scrollX;
        int i5 = i2 - scrollY;
        if (i4 == 0 && i5 == 0) {
            completeScroll(false);
            populate();
            setScrollState(0);
            return;
        }
        setScrollingCacheEnabled(true);
        setScrollState(2);
        int clientWidth = getClientWidth();
        int i6 = clientWidth / 2;
        float distanceInfluenceForSnapDuration = (((float) i6) * distanceInfluenceForSnapDuration(Math.min(1.0f, (((float) Math.abs(i4)) * 1.0f) / ((float) clientWidth)))) + ((float) i6);
        int abs2 = Math.abs(i3);
        if (abs2 > 0) {
            abs = Math.round(1000.0f * Math.abs(distanceInfluenceForSnapDuration / ((float) abs2))) * 4;
        } else {
            abs = (int) (((((float) Math.abs(i4)) / ((((float) clientWidth) * this.mAdapter.getPageWidth(this.mCurItem)) + ((float) this.mPageMargin))) + 1.0f) * 100.0f);
        }
        this.mScroller.startScroll(scrollX, scrollY, i4, i5, Math.min(abs, (int) MAX_SETTLE_DURATION));
        ViewCompat.postInvalidateOnAnimation(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.PagerAdapter.instantiateItem(android.view.ViewGroup, int):java.lang.Object
     arg types: [android.support.v4.view.DirectionalViewPager, int]
     candidates:
      android.support.v4.view.PagerAdapter.instantiateItem(android.view.View, int):java.lang.Object
      android.support.v4.view.PagerAdapter.instantiateItem(android.view.ViewGroup, int):java.lang.Object */
    /* access modifiers changed from: package-private */
    public m addNewItem(int i, int i2) {
        m mVar = new m();
        mVar.b = i;
        mVar.f94a = this.mAdapter.instantiateItem((ViewGroup) this, i);
        mVar.d = this.mAdapter.getPageWidth(i);
        if (i2 < 0 || i2 >= this.mItems.size()) {
            this.mItems.add(mVar);
        } else {
            this.mItems.add(i2, mVar);
        }
        return mVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.PagerAdapter.destroyItem(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.DirectionalViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.PagerAdapter.destroyItem(android.view.View, int, java.lang.Object):void
      android.support.v4.view.PagerAdapter.destroyItem(android.view.ViewGroup, int, java.lang.Object):void */
    /* access modifiers changed from: package-private */
    public void dataSetChanged() {
        int i;
        boolean z;
        int i2;
        boolean z2;
        int count = this.mAdapter.getCount();
        this.mExpectedAdapterCount = count;
        boolean z3 = this.mItems.size() < (this.mOffscreenPageLimit * 2) + 1 && this.mItems.size() < count;
        boolean z4 = false;
        int i3 = this.mCurItem;
        boolean z5 = z3;
        int i4 = 0;
        while (i4 < this.mItems.size()) {
            m mVar = this.mItems.get(i4);
            int itemPosition = this.mAdapter.getItemPosition(mVar.f94a);
            if (itemPosition == -1) {
                i = i4;
                z = z4;
                i2 = i3;
                z2 = z5;
            } else if (itemPosition == -2) {
                this.mItems.remove(i4);
                int i5 = i4 - 1;
                if (!z4) {
                    this.mAdapter.startUpdate((ViewGroup) this);
                    z4 = true;
                }
                this.mAdapter.destroyItem((ViewGroup) this, mVar.b, mVar.f94a);
                if (this.mCurItem == mVar.b) {
                    i = i5;
                    z = z4;
                    i2 = Math.max(0, Math.min(this.mCurItem, count - 1));
                    z2 = true;
                } else {
                    i = i5;
                    z = z4;
                    i2 = i3;
                    z2 = true;
                }
            } else if (mVar.b != itemPosition) {
                if (mVar.b == this.mCurItem) {
                    i3 = itemPosition;
                }
                mVar.b = itemPosition;
                i = i4;
                z = z4;
                i2 = i3;
                z2 = true;
            } else {
                i = i4;
                z = z4;
                i2 = i3;
                z2 = z5;
            }
            z5 = z2;
            i3 = i2;
            z4 = z;
            i4 = i + 1;
        }
        if (z4) {
            this.mAdapter.finishUpdate((ViewGroup) this);
        }
        Collections.sort(this.mItems, COMPARATOR);
        if (z5) {
            int childCount = getChildCount();
            for (int i6 = 0; i6 < childCount; i6++) {
                LayoutParams layoutParams = (LayoutParams) getChildAt(i6).getLayoutParams();
                if (!layoutParams.isDecor) {
                    layoutParams.dimensionFactor = 0.0f;
                }
            }
            setCurrentItemInternal(i3, false, true);
            requestLayout();
        }
    }

    /* access modifiers changed from: package-private */
    public void populate() {
        populate(this.mCurItem);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.PagerAdapter.setPrimaryItem(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.DirectionalViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.PagerAdapter.setPrimaryItem(android.view.View, int, java.lang.Object):void
      android.support.v4.view.PagerAdapter.setPrimaryItem(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.PagerAdapter.destroyItem(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.DirectionalViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.PagerAdapter.destroyItem(android.view.View, int, java.lang.Object):void
      android.support.v4.view.PagerAdapter.destroyItem(android.view.ViewGroup, int, java.lang.Object):void */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00ff, code lost:
        if (r2.b == r0.mCurItem) goto L_0x0101;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void populate(int r19) {
        /*
            r18 = this;
            r3 = 0
            r2 = 2
            r0 = r18
            int r4 = r0.mCurItem
            r0 = r19
            if (r4 == r0) goto L_0x034c
            r0 = r18
            int r2 = r0.mCurItem
            r0 = r19
            if (r2 >= r0) goto L_0x0030
            r2 = 66
        L_0x0014:
            r0 = r18
            int r3 = r0.mCurItem
            r0 = r18
            android.support.v4.view.m r3 = r0.infoForPosition(r3)
            r0 = r19
            r1 = r18
            r1.mCurItem = r0
            r4 = r3
            r3 = r2
        L_0x0026:
            r0 = r18
            android.support.v4.view.PagerAdapter r2 = r0.mAdapter
            if (r2 != 0) goto L_0x0033
            r18.sortChildDrawingOrder()
        L_0x002f:
            return
        L_0x0030:
            r2 = 17
            goto L_0x0014
        L_0x0033:
            r0 = r18
            boolean r2 = r0.mPopulatePending
            if (r2 == 0) goto L_0x003d
            r18.sortChildDrawingOrder()
            goto L_0x002f
        L_0x003d:
            android.os.IBinder r2 = r18.getWindowToken()
            if (r2 == 0) goto L_0x002f
            r0 = r18
            android.support.v4.view.PagerAdapter r2 = r0.mAdapter
            r0 = r18
            r2.startUpdate(r0)
            r0 = r18
            int r2 = r0.mOffscreenPageLimit
            r5 = 0
            r0 = r18
            int r6 = r0.mCurItem
            int r6 = r6 - r2
            int r11 = java.lang.Math.max(r5, r6)
            r0 = r18
            android.support.v4.view.PagerAdapter r5 = r0.mAdapter
            int r12 = r5.getCount()
            int r5 = r12 + -1
            r0 = r18
            int r6 = r0.mCurItem
            int r2 = r2 + r6
            int r13 = java.lang.Math.min(r5, r2)
            r0 = r18
            int r2 = r0.mExpectedAdapterCount
            if (r12 == r2) goto L_0x00da
            android.content.res.Resources r2 = r18.getResources()     // Catch:{ NotFoundException -> 0x00d0 }
            int r3 = r18.getId()     // Catch:{ NotFoundException -> 0x00d0 }
            java.lang.String r2 = r2.getResourceName(r3)     // Catch:{ NotFoundException -> 0x00d0 }
        L_0x007f:
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "The application's PagerAdapter changed the adapter's contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: "
            java.lang.StringBuilder r4 = r4.append(r5)
            r0 = r18
            int r5 = r0.mExpectedAdapterCount
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = ", found: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r12)
            java.lang.String r5 = " Pager id: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = " Pager class: "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.Class r4 = r18.getClass()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = " Problematic adapter: "
            java.lang.StringBuilder r2 = r2.append(r4)
            r0 = r18
            android.support.v4.view.PagerAdapter r4 = r0.mAdapter
            java.lang.Class r4 = r4.getClass()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r3.<init>(r2)
            throw r3
        L_0x00d0:
            r2 = move-exception
            int r2 = r18.getId()
            java.lang.String r2 = java.lang.Integer.toHexString(r2)
            goto L_0x007f
        L_0x00da:
            r6 = 0
            r2 = 0
            r5 = r2
        L_0x00dd:
            r0 = r18
            java.util.ArrayList<android.support.v4.view.m> r2 = r0.mItems
            int r2 = r2.size()
            if (r5 >= r2) goto L_0x0349
            r0 = r18
            java.util.ArrayList<android.support.v4.view.m> r2 = r0.mItems
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.m r2 = (android.support.v4.view.m) r2
            int r7 = r2.b
            r0 = r18
            int r8 = r0.mCurItem
            if (r7 < r8) goto L_0x01da
            int r7 = r2.b
            r0 = r18
            int r8 = r0.mCurItem
            if (r7 != r8) goto L_0x0349
        L_0x0101:
            if (r2 != 0) goto L_0x0346
            if (r12 <= 0) goto L_0x0346
            r0 = r18
            int r2 = r0.mCurItem
            r0 = r18
            android.support.v4.view.m r2 = r0.addNewItem(r2, r5)
            r10 = r2
        L_0x0110:
            if (r10 == 0) goto L_0x018b
            r9 = 0
            int r8 = r5 + -1
            if (r8 < 0) goto L_0x01df
            r0 = r18
            java.util.ArrayList<android.support.v4.view.m> r2 = r0.mItems
            java.lang.Object r2 = r2.get(r8)
            android.support.v4.view.m r2 = (android.support.v4.view.m) r2
        L_0x0121:
            int r14 = r18.getClientDimension()
            r0 = r18
            int r6 = r0.mOrientation
            if (r6 != 0) goto L_0x01e2
            int r6 = r18.getPaddingLeft()
            float r6 = (float) r6
        L_0x0130:
            if (r14 > 0) goto L_0x01e9
            r6 = 0
        L_0x0133:
            r0 = r18
            int r7 = r0.mCurItem
            int r7 = r7 + -1
            r16 = r7
            r7 = r9
            r9 = r16
            r17 = r8
            r8 = r5
            r5 = r17
        L_0x0143:
            if (r9 < 0) goto L_0x014d
            int r15 = (r7 > r6 ? 1 : (r7 == r6 ? 0 : -1))
            if (r15 < 0) goto L_0x0223
            if (r9 >= r11) goto L_0x0223
            if (r2 != 0) goto L_0x01f3
        L_0x014d:
            float r6 = r10.d
            int r9 = r8 + 1
            r2 = 1073741824(0x40000000, float:2.0)
            int r2 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r2 >= 0) goto L_0x0186
            r0 = r18
            java.util.ArrayList<android.support.v4.view.m> r2 = r0.mItems
            int r2 = r2.size()
            if (r9 >= r2) goto L_0x0259
            r0 = r18
            java.util.ArrayList<android.support.v4.view.m> r2 = r0.mItems
            java.lang.Object r2 = r2.get(r9)
            android.support.v4.view.m r2 = (android.support.v4.view.m) r2
            r7 = r2
        L_0x016c:
            if (r14 > 0) goto L_0x025c
            r2 = 0
            r5 = r2
        L_0x0170:
            r0 = r18
            int r2 = r0.mCurItem
            int r2 = r2 + 1
            r16 = r2
            r2 = r7
            r7 = r9
            r9 = r16
        L_0x017c:
            if (r9 >= r12) goto L_0x0186
            int r11 = (r6 > r5 ? 1 : (r6 == r5 ? 0 : -1))
            if (r11 < 0) goto L_0x02a7
            if (r9 <= r13) goto L_0x02a7
            if (r2 != 0) goto L_0x0269
        L_0x0186:
            r0 = r18
            r0.calculatePageOffsets(r10, r8, r4)
        L_0x018b:
            r0 = r18
            android.support.v4.view.PagerAdapter r4 = r0.mAdapter
            r0 = r18
            int r5 = r0.mCurItem
            if (r10 == 0) goto L_0x02f5
            java.lang.Object r2 = r10.f94a
        L_0x0197:
            r0 = r18
            r4.setPrimaryItem(r0, r5, r2)
            r0 = r18
            android.support.v4.view.PagerAdapter r2 = r0.mAdapter
            r0 = r18
            r2.finishUpdate(r0)
            int r5 = r18.getChildCount()
            r2 = 0
            r4 = r2
        L_0x01ab:
            if (r4 >= r5) goto L_0x02f8
            r0 = r18
            android.view.View r6 = r0.getChildAt(r4)
            android.view.ViewGroup$LayoutParams r2 = r6.getLayoutParams()
            android.support.v4.view.DirectionalViewPager$LayoutParams r2 = (android.support.v4.view.DirectionalViewPager.LayoutParams) r2
            r2.childIndex = r4
            boolean r7 = r2.isDecor
            if (r7 != 0) goto L_0x01d6
            float r7 = r2.dimensionFactor
            r8 = 0
            int r7 = (r7 > r8 ? 1 : (r7 == r8 ? 0 : -1))
            if (r7 != 0) goto L_0x01d6
            r0 = r18
            android.support.v4.view.m r6 = r0.infoForChild(r6)
            if (r6 == 0) goto L_0x01d6
            float r7 = r6.d
            r2.dimensionFactor = r7
            int r6 = r6.b
            r2.position = r6
        L_0x01d6:
            int r2 = r4 + 1
            r4 = r2
            goto L_0x01ab
        L_0x01da:
            int r2 = r5 + 1
            r5 = r2
            goto L_0x00dd
        L_0x01df:
            r2 = 0
            goto L_0x0121
        L_0x01e2:
            int r6 = r18.getPaddingTop()
            float r6 = (float) r6
            goto L_0x0130
        L_0x01e9:
            r7 = 1073741824(0x40000000, float:2.0)
            float r15 = r10.d
            float r7 = r7 - r15
            float r15 = (float) r14
            float r6 = r6 / r15
            float r6 = r6 + r7
            goto L_0x0133
        L_0x01f3:
            int r15 = r2.b
            if (r9 != r15) goto L_0x021d
            boolean r15 = r2.c
            if (r15 != 0) goto L_0x021d
            r0 = r18
            java.util.ArrayList<android.support.v4.view.m> r15 = r0.mItems
            r15.remove(r5)
            r0 = r18
            android.support.v4.view.PagerAdapter r15 = r0.mAdapter
            java.lang.Object r2 = r2.f94a
            r0 = r18
            r15.destroyItem(r0, r9, r2)
            int r5 = r5 + -1
            int r8 = r8 + -1
            if (r5 < 0) goto L_0x0221
            r0 = r18
            java.util.ArrayList<android.support.v4.view.m> r2 = r0.mItems
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.m r2 = (android.support.v4.view.m) r2
        L_0x021d:
            int r9 = r9 + -1
            goto L_0x0143
        L_0x0221:
            r2 = 0
            goto L_0x021d
        L_0x0223:
            if (r2 == 0) goto L_0x023d
            int r15 = r2.b
            if (r9 != r15) goto L_0x023d
            float r2 = r2.d
            float r7 = r7 + r2
            int r5 = r5 + -1
            if (r5 < 0) goto L_0x023b
            r0 = r18
            java.util.ArrayList<android.support.v4.view.m> r2 = r0.mItems
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.m r2 = (android.support.v4.view.m) r2
            goto L_0x021d
        L_0x023b:
            r2 = 0
            goto L_0x021d
        L_0x023d:
            int r2 = r5 + 1
            r0 = r18
            android.support.v4.view.m r2 = r0.addNewItem(r9, r2)
            float r2 = r2.d
            float r7 = r7 + r2
            int r8 = r8 + 1
            if (r5 < 0) goto L_0x0257
            r0 = r18
            java.util.ArrayList<android.support.v4.view.m> r2 = r0.mItems
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.m r2 = (android.support.v4.view.m) r2
            goto L_0x021d
        L_0x0257:
            r2 = 0
            goto L_0x021d
        L_0x0259:
            r7 = 0
            goto L_0x016c
        L_0x025c:
            int r2 = r18.getPaddingRight()
            float r2 = (float) r2
            float r5 = (float) r14
            float r2 = r2 / r5
            r5 = 1073741824(0x40000000, float:2.0)
            float r2 = r2 + r5
            r5 = r2
            goto L_0x0170
        L_0x0269:
            int r11 = r2.b
            if (r9 != r11) goto L_0x033f
            boolean r11 = r2.c
            if (r11 != 0) goto L_0x033f
            r0 = r18
            java.util.ArrayList<android.support.v4.view.m> r11 = r0.mItems
            r11.remove(r7)
            r0 = r18
            android.support.v4.view.PagerAdapter r11 = r0.mAdapter
            java.lang.Object r2 = r2.f94a
            r0 = r18
            r11.destroyItem(r0, r9, r2)
            r0 = r18
            java.util.ArrayList<android.support.v4.view.m> r2 = r0.mItems
            int r2 = r2.size()
            if (r7 >= r2) goto L_0x02a5
            r0 = r18
            java.util.ArrayList<android.support.v4.view.m> r2 = r0.mItems
            java.lang.Object r2 = r2.get(r7)
            android.support.v4.view.m r2 = (android.support.v4.view.m) r2
        L_0x0297:
            r16 = r6
            r6 = r2
            r2 = r16
        L_0x029c:
            int r9 = r9 + 1
            r16 = r2
            r2 = r6
            r6 = r16
            goto L_0x017c
        L_0x02a5:
            r2 = 0
            goto L_0x0297
        L_0x02a7:
            if (r2 == 0) goto L_0x02ce
            int r11 = r2.b
            if (r9 != r11) goto L_0x02ce
            float r2 = r2.d
            float r6 = r6 + r2
            int r7 = r7 + 1
            r0 = r18
            java.util.ArrayList<android.support.v4.view.m> r2 = r0.mItems
            int r2 = r2.size()
            if (r7 >= r2) goto L_0x02cc
            r0 = r18
            java.util.ArrayList<android.support.v4.view.m> r2 = r0.mItems
            java.lang.Object r2 = r2.get(r7)
            android.support.v4.view.m r2 = (android.support.v4.view.m) r2
        L_0x02c6:
            r16 = r6
            r6 = r2
            r2 = r16
            goto L_0x029c
        L_0x02cc:
            r2 = 0
            goto L_0x02c6
        L_0x02ce:
            r0 = r18
            android.support.v4.view.m r2 = r0.addNewItem(r9, r7)
            int r7 = r7 + 1
            float r2 = r2.d
            float r6 = r6 + r2
            r0 = r18
            java.util.ArrayList<android.support.v4.view.m> r2 = r0.mItems
            int r2 = r2.size()
            if (r7 >= r2) goto L_0x02f3
            r0 = r18
            java.util.ArrayList<android.support.v4.view.m> r2 = r0.mItems
            java.lang.Object r2 = r2.get(r7)
            android.support.v4.view.m r2 = (android.support.v4.view.m) r2
        L_0x02ed:
            r16 = r6
            r6 = r2
            r2 = r16
            goto L_0x029c
        L_0x02f3:
            r2 = 0
            goto L_0x02ed
        L_0x02f5:
            r2 = 0
            goto L_0x0197
        L_0x02f8:
            r18.sortChildDrawingOrder()
            boolean r2 = r18.hasFocus()
            if (r2 == 0) goto L_0x002f
            android.view.View r2 = r18.findFocus()
            if (r2 == 0) goto L_0x033d
            r0 = r18
            android.support.v4.view.m r2 = r0.infoForAnyChild(r2)
        L_0x030d:
            if (r2 == 0) goto L_0x0317
            int r2 = r2.b
            r0 = r18
            int r4 = r0.mCurItem
            if (r2 == r4) goto L_0x002f
        L_0x0317:
            r2 = 0
        L_0x0318:
            int r4 = r18.getChildCount()
            if (r2 >= r4) goto L_0x002f
            r0 = r18
            android.view.View r4 = r0.getChildAt(r2)
            r0 = r18
            android.support.v4.view.m r5 = r0.infoForChild(r4)
            if (r5 == 0) goto L_0x033a
            int r5 = r5.b
            r0 = r18
            int r6 = r0.mCurItem
            if (r5 != r6) goto L_0x033a
            boolean r4 = r4.requestFocus(r3)
            if (r4 != 0) goto L_0x002f
        L_0x033a:
            int r2 = r2 + 1
            goto L_0x0318
        L_0x033d:
            r2 = 0
            goto L_0x030d
        L_0x033f:
            r16 = r6
            r6 = r2
            r2 = r16
            goto L_0x029c
        L_0x0346:
            r10 = r2
            goto L_0x0110
        L_0x0349:
            r2 = r6
            goto L_0x0101
        L_0x034c:
            r4 = r3
            r3 = r2
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.DirectionalViewPager.populate(int):void");
    }

    private void sortChildDrawingOrder() {
        if (this.mDrawingOrder != 0) {
            if (this.mDrawingOrderedChildren == null) {
                this.mDrawingOrderedChildren = new ArrayList<>();
            } else {
                this.mDrawingOrderedChildren.clear();
            }
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                this.mDrawingOrderedChildren.add(getChildAt(i));
            }
            Collections.sort(this.mDrawingOrderedChildren, sPositionComparator);
        }
    }

    private void calculatePageOffsets(m mVar, int i, m mVar2) {
        float f;
        m mVar3;
        m mVar4;
        int count = this.mAdapter.getCount();
        int clientDimension = getClientDimension();
        if (clientDimension > 0) {
            f = ((float) this.mPageMargin) / ((float) clientDimension);
        } else {
            f = 0.0f;
        }
        if (mVar2 != null) {
            int i2 = mVar2.b;
            if (i2 < mVar.b) {
                float f2 = mVar2.e + mVar2.d + f;
                int i3 = i2 + 1;
                int i4 = 0;
                while (i3 <= mVar.b && i4 < this.mItems.size()) {
                    m mVar5 = this.mItems.get(i4);
                    while (true) {
                        mVar4 = mVar5;
                        if (i3 > mVar4.b && i4 < this.mItems.size() - 1) {
                            i4++;
                            mVar5 = this.mItems.get(i4);
                        }
                    }
                    while (i3 < mVar4.b) {
                        f2 += this.mAdapter.getPageWidth(i3) + f;
                        i3++;
                    }
                    mVar4.e = f2;
                    f2 += mVar4.d + f;
                    i3++;
                }
            } else if (i2 > mVar.b) {
                int size = this.mItems.size() - 1;
                float f3 = mVar2.e;
                int i5 = i2 - 1;
                while (i5 >= mVar.b && size >= 0) {
                    m mVar6 = this.mItems.get(size);
                    while (true) {
                        mVar3 = mVar6;
                        if (i5 < mVar3.b && size > 0) {
                            size--;
                            mVar6 = this.mItems.get(size);
                        }
                    }
                    while (i5 > mVar3.b) {
                        f3 -= this.mAdapter.getPageWidth(i5) + f;
                        i5--;
                    }
                    f3 -= mVar3.d + f;
                    mVar3.e = f3;
                    i5--;
                }
            }
        }
        int size2 = this.mItems.size();
        float f4 = mVar.e;
        int i6 = mVar.b - 1;
        this.mFirstOffset = mVar.b == 0 ? mVar.e : -3.4028235E38f;
        setLastOffset(mVar.b == count + -1 ? (mVar.e + mVar.d) - 1.0f : Float.MAX_VALUE);
        for (int i7 = i - 1; i7 >= 0; i7--) {
            m mVar7 = this.mItems.get(i7);
            float f5 = f4;
            while (i6 > mVar7.b) {
                f5 -= this.mAdapter.getPageWidth(i6) + f;
                i6--;
            }
            f4 = f5 - (mVar7.d + f);
            mVar7.e = f4;
            if (mVar7.b == 0) {
                this.mFirstOffset = f4;
            }
            i6--;
        }
        float f6 = mVar.e + mVar.d + f;
        int i8 = mVar.b + 1;
        for (int i9 = i + 1; i9 < size2; i9++) {
            m mVar8 = this.mItems.get(i9);
            float f7 = f6;
            while (i8 < mVar8.b) {
                f7 = this.mAdapter.getPageWidth(i8) + f + f7;
                i8++;
            }
            if (mVar8.b == count - 1) {
                setLastOffset((mVar8.d + f7) - 1.0f);
            }
            mVar8.e = f7;
            f6 = f7 + mVar8.d + f;
            i8++;
        }
        this.mNeedCalculatePageOffsets = false;
    }

    /* compiled from: ProGuard */
    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = a.a(new q());
        Parcelable adapterState;
        ClassLoader loader;
        int position;

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.position);
            parcel.writeParcelable(this.adapterState, i);
        }

        public String toString() {
            return "FragmentPager.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " position=" + this.position + "}";
        }

        SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel);
            classLoader = classLoader == null ? getClass().getClassLoader() : classLoader;
            this.position = parcel.readInt();
            this.adapterState = parcel.readParcelable(classLoader);
            this.loader = classLoader;
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.position = this.mCurItem;
        if (this.mAdapter != null) {
            savedState.adapterState = this.mAdapter.saveState();
        }
        return savedState;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (this.mAdapter != null) {
            this.mAdapter.restoreState(savedState.adapterState, savedState.loader);
            setCurrentItemInternal(savedState.position, false, true);
            return;
        }
        this.mRestoredCurItem = savedState.position;
        this.mRestoredAdapterState = savedState.adapterState;
        this.mRestoredClassLoader = savedState.loader;
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        ViewGroup.LayoutParams layoutParams2;
        if (!checkLayoutParams(layoutParams)) {
            layoutParams2 = generateLayoutParams(layoutParams);
        } else {
            layoutParams2 = layoutParams;
        }
        LayoutParams layoutParams3 = (LayoutParams) layoutParams2;
        layoutParams3.isDecor |= view instanceof l;
        if (!this.mInLayout) {
            super.addView(view, i, layoutParams2);
        } else if (layoutParams3 == null || !layoutParams3.isDecor) {
            layoutParams3.needsMeasure = true;
            addViewInLayout(view, i, layoutParams2);
        } else {
            throw new IllegalStateException("Cannot add pager decor view during layout");
        }
    }

    public void removeView(View view) {
        if (this.mInLayout) {
            removeViewInLayout(view);
        } else {
            super.removeView(view);
        }
    }

    /* access modifiers changed from: package-private */
    public m infoForChild(View view) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.mItems.size()) {
                return null;
            }
            m mVar = this.mItems.get(i2);
            if (this.mAdapter.isViewFromObject(view, mVar.f94a)) {
                return mVar;
            }
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public m infoForAnyChild(View view) {
        while (true) {
            ViewParent parent = view.getParent();
            if (parent == this) {
                return infoForChild(view);
            }
            if (parent == null || !(parent instanceof View)) {
                return null;
            }
            view = (View) parent;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public m infoForPosition(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.mItems.size()) {
                return null;
            }
            m mVar = this.mItems.get(i3);
            if (mVar.b == i) {
                return mVar;
            }
            i2 = i3 + 1;
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mFirstLayout = true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a6  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00be  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r14, int r15) {
        /*
            r13 = this;
            r0 = 0
            int r0 = getDefaultSize(r0, r14)
            r1 = 0
            int r1 = getDefaultSize(r1, r15)
            r13.setMeasuredDimension(r0, r1)
            int r1 = r13.getMeasuredWidth()
            int r2 = r13.getMeasuredHeight()
            int r0 = r13.mOrientation
            if (r0 != 0) goto L_0x00af
            int r0 = r1 / 10
        L_0x001b:
            int r3 = r13.mDefaultGutterSize
            int r0 = java.lang.Math.min(r0, r3)
            r13.mGutterSize = r0
            int r0 = r13.getPaddingLeft()
            int r0 = r1 - r0
            int r1 = r13.getPaddingRight()
            int r3 = r0 - r1
            int r0 = r13.getPaddingTop()
            int r0 = r2 - r0
            int r1 = r13.getPaddingBottom()
            int r5 = r0 - r1
            int r9 = r13.getChildCount()
            r0 = 0
            r8 = r0
        L_0x0041:
            if (r8 >= r9) goto L_0x00c6
            android.view.View r10 = r13.getChildAt(r8)
            int r0 = r10.getVisibility()
            r1 = 8
            if (r0 == r1) goto L_0x00ab
            android.view.ViewGroup$LayoutParams r0 = r10.getLayoutParams()
            android.support.v4.view.DirectionalViewPager$LayoutParams r0 = (android.support.v4.view.DirectionalViewPager.LayoutParams) r0
            if (r0 == 0) goto L_0x00ab
            boolean r1 = r0.isDecor
            if (r1 == 0) goto L_0x00ab
            int r1 = r0.gravity
            r6 = r1 & 7
            int r1 = r0.gravity
            r4 = r1 & 112(0x70, float:1.57E-43)
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = 48
            if (r4 == r7) goto L_0x006f
            r7 = 80
            if (r4 != r7) goto L_0x00b3
        L_0x006f:
            r4 = 1
            r7 = r4
        L_0x0071:
            r4 = 3
            if (r6 == r4) goto L_0x0077
            r4 = 5
            if (r6 != r4) goto L_0x00b6
        L_0x0077:
            r4 = 1
            r6 = r4
        L_0x0079:
            if (r7 == 0) goto L_0x00b9
            r2 = 1073741824(0x40000000, float:2.0)
        L_0x007d:
            int r4 = r0.width
            r11 = -2
            if (r4 == r11) goto L_0x012c
            r4 = 1073741824(0x40000000, float:2.0)
            int r2 = r0.width
            r11 = -1
            if (r2 == r11) goto L_0x0129
            int r2 = r0.width
        L_0x008b:
            int r11 = r0.height
            r12 = -2
            if (r11 == r12) goto L_0x0126
            r1 = 1073741824(0x40000000, float:2.0)
            int r11 = r0.height
            r12 = -1
            if (r11 == r12) goto L_0x0126
            int r0 = r0.height
        L_0x0099:
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r4)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r1)
            r10.measure(r2, r0)
            if (r7 == 0) goto L_0x00be
            int r0 = r10.getMeasuredHeight()
            int r5 = r5 - r0
        L_0x00ab:
            int r0 = r8 + 1
            r8 = r0
            goto L_0x0041
        L_0x00af:
            int r0 = r2 / 10
            goto L_0x001b
        L_0x00b3:
            r4 = 0
            r7 = r4
            goto L_0x0071
        L_0x00b6:
            r4 = 0
            r6 = r4
            goto L_0x0079
        L_0x00b9:
            if (r6 == 0) goto L_0x007d
            r1 = 1073741824(0x40000000, float:2.0)
            goto L_0x007d
        L_0x00be:
            if (r6 == 0) goto L_0x00ab
            int r0 = r10.getMeasuredWidth()
            int r3 = r3 - r0
            goto L_0x00ab
        L_0x00c6:
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r0)
            r13.mChildWidthMeasureSpec = r0
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r5, r0)
            r13.mChildHeightMeasureSpec = r0
            r0 = 1
            r13.mInLayout = r0
            r13.populate()
            r0 = 0
            r13.mInLayout = r0
            int r4 = r13.getChildCount()
            r0 = 0
            r2 = r0
        L_0x00e5:
            if (r2 >= r4) goto L_0x0125
            android.view.View r6 = r13.getChildAt(r2)
            int r0 = r6.getVisibility()
            r1 = 8
            if (r0 == r1) goto L_0x0113
            android.view.ViewGroup$LayoutParams r0 = r6.getLayoutParams()
            android.support.v4.view.DirectionalViewPager$LayoutParams r0 = (android.support.v4.view.DirectionalViewPager.LayoutParams) r0
            if (r0 == 0) goto L_0x00ff
            boolean r1 = r0.isDecor
            if (r1 != 0) goto L_0x0113
        L_0x00ff:
            int r1 = r13.mOrientation
            if (r1 != 0) goto L_0x0117
            float r1 = (float) r3
            float r0 = r0.dimensionFactor
            float r0 = r0 * r1
            int r0 = (int) r0
            r1 = 1073741824(0x40000000, float:2.0)
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r1)
            int r0 = r13.mChildHeightMeasureSpec
        L_0x0110:
            r6.measure(r1, r0)
        L_0x0113:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x00e5
        L_0x0117:
            int r1 = r13.mChildWidthMeasureSpec
            float r7 = (float) r5
            float r0 = r0.dimensionFactor
            float r0 = r0 * r7
            int r0 = (int) r0
            r7 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r7)
            goto L_0x0110
        L_0x0125:
            return
        L_0x0126:
            r0 = r5
            goto L_0x0099
        L_0x0129:
            r2 = r3
            goto L_0x008b
        L_0x012c:
            r4 = r2
            r2 = r3
            goto L_0x008b
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.DirectionalViewPager.onMeasure(int, int):void");
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i != i3) {
            recomputeScrollPosition(i, i3, this.mPageMargin, this.mPageMargin);
        }
    }

    private void recomputeScrollPosition(int i, int i2, int i3, int i4) {
        if (i2 <= 0 || this.mItems.isEmpty()) {
            m infoForPosition = infoForPosition(this.mCurItem);
            int min = (int) ((infoForPosition != null ? Math.min(infoForPosition.e, this.mLastOffset) : 0.0f) * ((float) ((i - getPaddingLeft()) - getPaddingRight())));
            if (min != getScrollX()) {
                completeScroll(false);
                scrollTo(min, getScrollY());
                return;
            }
            return;
        }
        int paddingLeft = (int) (((float) (((i - getPaddingLeft()) - getPaddingRight()) + i3)) * (((float) getScrollX()) / ((float) (((i2 - getPaddingLeft()) - getPaddingRight()) + i4))));
        scrollTo(paddingLeft, getScrollY());
        if (!this.mScroller.isFinished()) {
            this.mScroller.startScroll(paddingLeft, 0, (int) (infoForPosition(this.mCurItem).e * ((float) i)), 0, this.mScroller.getDuration() - this.mScroller.timePassed());
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        m infoForChild;
        int i5;
        int i6;
        int makeMeasureSpec;
        int makeMeasureSpec2;
        int i7;
        int i8;
        int i9;
        int measuredHeight;
        int i10;
        int i11;
        int childCount = getChildCount();
        int i12 = i3 - i;
        int i13 = i4 - i2;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int i14 = 0;
        int i15 = 0;
        while (i15 < childCount) {
            View childAt = getChildAt(i15);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.isDecor) {
                    int i16 = layoutParams.gravity & 7;
                    int i17 = layoutParams.gravity & 112;
                    switch (i16) {
                        case 1:
                            i9 = Math.max((i12 - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            break;
                        case 2:
                        case 4:
                        default:
                            i9 = paddingLeft;
                            break;
                        case 3:
                            i9 = paddingLeft;
                            paddingLeft = childAt.getMeasuredWidth() + paddingLeft;
                            break;
                        case 5:
                            int measuredWidth = (i12 - paddingRight) - childAt.getMeasuredWidth();
                            paddingRight += childAt.getMeasuredWidth();
                            i9 = measuredWidth;
                            break;
                    }
                    switch (i17) {
                        case 16:
                            measuredHeight = Math.max((i13 - childAt.getMeasuredHeight()) / 2, paddingTop);
                            int i18 = paddingBottom;
                            i10 = paddingTop;
                            i11 = i18;
                            break;
                        case 48:
                            int measuredHeight2 = childAt.getMeasuredHeight() + paddingTop;
                            int i19 = paddingTop;
                            i11 = paddingBottom;
                            i10 = measuredHeight2;
                            measuredHeight = i19;
                            break;
                        case NormalErrorRecommendPage.ERROR_TYPE_DETAIL_EMPTY_TO_NO /*80*/:
                            measuredHeight = (i13 - paddingBottom) - childAt.getMeasuredHeight();
                            int measuredHeight3 = paddingBottom + childAt.getMeasuredHeight();
                            i10 = paddingTop;
                            i11 = measuredHeight3;
                            break;
                        default:
                            measuredHeight = paddingTop;
                            int i20 = paddingBottom;
                            i10 = paddingTop;
                            i11 = i20;
                            break;
                    }
                    int i21 = i9 + scrollX;
                    int i22 = measuredHeight + scrollY;
                    childAt.layout(i21, i22, childAt.getMeasuredWidth() + i21, childAt.getMeasuredHeight() + i22);
                    i7 = i14 + 1;
                    i8 = i10;
                    paddingBottom = i11;
                    i15++;
                    paddingLeft = paddingLeft;
                    paddingRight = paddingRight;
                    paddingTop = i8;
                    i14 = i7;
                }
            }
            i7 = i14;
            i8 = paddingTop;
            i15++;
            paddingLeft = paddingLeft;
            paddingRight = paddingRight;
            paddingTop = i8;
            i14 = i7;
        }
        int i23 = (i12 - paddingLeft) - paddingRight;
        int i24 = (i13 - paddingTop) - paddingBottom;
        for (int i25 = 0; i25 < childCount; i25++) {
            View childAt2 = getChildAt(i25);
            if (childAt2.getVisibility() != 8) {
                LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
                if (!layoutParams2.isDecor && (infoForChild = infoForChild(childAt2)) != null) {
                    if (this.mOrientation == 0) {
                        i6 = ((int) (infoForChild.e * ((float) i23))) + paddingLeft;
                        i5 = paddingTop;
                    } else {
                        i5 = ((int) (infoForChild.e * ((float) i24))) + paddingTop;
                        i6 = paddingLeft;
                    }
                    if (layoutParams2.needsMeasure) {
                        layoutParams2.needsMeasure = false;
                        if (this.mOrientation == 0) {
                            makeMeasureSpec = View.MeasureSpec.makeMeasureSpec((int) (layoutParams2.dimensionFactor * ((float) i23)), 1073741824);
                            makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec((i13 - paddingTop) - paddingBottom, 1073741824);
                        } else {
                            makeMeasureSpec = View.MeasureSpec.makeMeasureSpec((i12 - paddingLeft) - paddingRight, 1073741824);
                            makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec((int) (layoutParams2.dimensionFactor * ((float) i24)), 1073741824);
                        }
                        childAt2.measure(makeMeasureSpec, makeMeasureSpec2);
                    }
                    childAt2.layout(i6, i5, childAt2.getMeasuredWidth() + i6, childAt2.getMeasuredHeight() + i5);
                }
            }
        }
        this.mLeftPageBounds = paddingLeft;
        this.mTopPageBounds = paddingTop;
        this.mRightPageBounds = paddingRight;
        this.mBottomPageBounds = i13 - paddingBottom;
        this.mDecorChildCount = i14;
        if (this.mFirstLayout) {
            scrollToItem(this.mCurItem, false, 0, false);
        }
        this.mFirstLayout = false;
    }

    public void computeScroll() {
        if (this.mScroller.isFinished() || !this.mScroller.computeScrollOffset()) {
            completeScroll(true);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int currX = this.mScroller.getCurrX();
        int currY = this.mScroller.getCurrY();
        if (!(scrollX == currX && scrollY == currY)) {
            scrollTo(currX, currY);
            if (this.mOrientation == 0) {
                if (!pageScrolled(currX)) {
                    this.mScroller.abortAnimation();
                    scrollTo(0, currY);
                }
            } else if (!pageScrolled(currY)) {
                this.mScroller.abortAnimation();
                scrollTo(currX, 0);
            }
        }
        ViewCompat.postInvalidateOnAnimation(this);
    }

    private boolean pageScrolled(int i) {
        if (this.mItems.size() == 0) {
            this.mCalledSuper = false;
            onPageScrolled(0, 0.0f, 0);
            if (this.mCalledSuper) {
                return false;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        }
        m infoForCurrentScrollPosition = infoForCurrentScrollPosition();
        int clientDimension = getClientDimension();
        int i2 = this.mPageMargin + clientDimension;
        float f = ((float) this.mPageMargin) / ((float) clientDimension);
        int i3 = infoForCurrentScrollPosition.b;
        float f2 = ((((float) i) / ((float) clientDimension)) - infoForCurrentScrollPosition.e) / (infoForCurrentScrollPosition.d + f);
        this.mCalledSuper = false;
        onPageScrolled(i3, f2, (int) (((float) i2) * f2));
        if (this.mCalledSuper) {
            return true;
        }
        throw new IllegalStateException("onPageScrolled did not call superclass implementation");
    }

    /* access modifiers changed from: protected */
    public void onPageScrolled(int i, float f, int i2) {
        int measuredWidth;
        int i3;
        int i4;
        if (this.mDecorChildCount > 0) {
            int scrollX = getScrollX();
            int paddingLeft = getPaddingLeft();
            int paddingRight = getPaddingRight();
            int width = getWidth();
            int childCount = getChildCount();
            int i5 = 0;
            while (i5 < childCount) {
                View childAt = getChildAt(i5);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (!layoutParams.isDecor) {
                    int i6 = paddingRight;
                    i3 = paddingLeft;
                    i4 = i6;
                } else {
                    switch (layoutParams.gravity & 7) {
                        case 1:
                            measuredWidth = Math.max((width - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            int i7 = paddingRight;
                            i3 = paddingLeft;
                            i4 = i7;
                            break;
                        case 2:
                        case 4:
                        default:
                            measuredWidth = paddingLeft;
                            int i8 = paddingRight;
                            i3 = paddingLeft;
                            i4 = i8;
                            break;
                        case 3:
                            int width2 = childAt.getWidth() + paddingLeft;
                            int i9 = paddingLeft;
                            i4 = paddingRight;
                            i3 = width2;
                            measuredWidth = i9;
                            break;
                        case 5:
                            measuredWidth = (width - paddingRight) - childAt.getMeasuredWidth();
                            int measuredWidth2 = paddingRight + childAt.getMeasuredWidth();
                            i3 = paddingLeft;
                            i4 = measuredWidth2;
                            break;
                    }
                    int left = (measuredWidth + scrollX) - childAt.getLeft();
                    if (left != 0) {
                        childAt.offsetLeftAndRight(left);
                    }
                }
                i5++;
                int i10 = i4;
                paddingLeft = i3;
                paddingRight = i10;
            }
        }
        if (this.mOnPageChangeListener != null) {
            this.mOnPageChangeListener.onPageScrolled(i, f, i2);
        }
        if (this.mInternalPageChangeListener != null) {
            this.mInternalPageChangeListener.onPageScrolled(i, f, i2);
        }
        if (this.mPageTransformer != null) {
            int scrollX2 = getScrollX();
            int childCount2 = getChildCount();
            for (int i11 = 0; i11 < childCount2; i11++) {
                View childAt2 = getChildAt(i11);
                if (!((LayoutParams) childAt2.getLayoutParams()).isDecor) {
                    this.mPageTransformer.transformPage(childAt2, ((float) (childAt2.getLeft() - scrollX2)) / ((float) getClientWidth()));
                }
            }
        }
        this.mCalledSuper = true;
    }

    private void completeScroll(boolean z) {
        boolean z2 = this.mScrollState == 2;
        if (z2) {
            setScrollingCacheEnabled(false);
            this.mScroller.abortAnimation();
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            int currX = this.mScroller.getCurrX();
            int currY = this.mScroller.getCurrY();
            if (!(scrollX == currX && scrollY == currY)) {
                scrollTo(currX, currY);
            }
        }
        this.mPopulatePending = false;
        boolean z3 = z2;
        for (int i = 0; i < this.mItems.size(); i++) {
            m mVar = this.mItems.get(i);
            if (mVar.c) {
                mVar.c = false;
                z3 = true;
            }
        }
        if (!z3) {
            return;
        }
        if (z) {
            ViewCompat.postOnAnimation(this, this.mEndScrollRunnable);
        } else {
            this.mEndScrollRunnable.run();
        }
    }

    private boolean isGutterDrag(float f, float f2) {
        return (f < ((float) this.mGutterSize) && f2 > 0.0f) || (f > ((float) (getWidth() - this.mGutterSize)) && f2 < 0.0f);
    }

    private void enableLayers(boolean z) {
        int i;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            if (z) {
                i = 2;
            } else {
                i = 0;
            }
            ViewCompat.setLayerType(getChildAt(i2), i, null);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int finalY;
        int action = motionEvent.getAction() & 255;
        if (action == 3 || action == 1) {
            this.mIsBeingDragged = false;
            this.mIsUnableToDrag = false;
            this.mActivePointerId = -1;
            if (this.mVelocityTracker != null) {
                this.mVelocityTracker.recycle();
                this.mVelocityTracker = null;
            }
            return false;
        }
        if (action != 0) {
            if (this.mIsBeingDragged) {
                return true;
            }
            if (this.mIsUnableToDrag) {
                return false;
            }
        }
        switch (action) {
            case 0:
                float x = motionEvent.getX();
                this.mInitialMotionX = x;
                this.mLastMotionX = x;
                float y = motionEvent.getY();
                this.mInitialMotionY = y;
                this.mLastMotionY = y;
                this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent, 0);
                this.mIsUnableToDrag = false;
                this.mScroller.computeScrollOffset();
                if (this.mOrientation == 0) {
                    finalY = this.mScroller.getFinalX() - this.mScroller.getCurrX();
                } else {
                    finalY = this.mScroller.getFinalY() - this.mScroller.getCurrY();
                }
                if (this.mScrollState == 2 && Math.abs(finalY) > this.mCloseEnough) {
                    this.mScroller.abortAnimation();
                    this.mPopulatePending = false;
                    populate();
                    this.mIsBeingDragged = true;
                    setScrollState(1);
                    break;
                } else {
                    completeScroll(false);
                    this.mIsBeingDragged = false;
                    break;
                }
            case 2:
                int i = this.mActivePointerId;
                if (i != -1) {
                    int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent, i);
                    float x2 = MotionEventCompat.getX(motionEvent, findPointerIndex);
                    float y2 = MotionEventCompat.getY(motionEvent, findPointerIndex);
                    if (this.mOrientation == 0) {
                        float f = x2 - this.mLastMotionX;
                        float abs = Math.abs(f);
                        float abs2 = Math.abs(y2 - this.mInitialMotionY);
                        if (f == 0.0f || isGutterDrag(this.mLastMotionX, f) || !canScroll(this, false, (int) f, 0, (int) x2, (int) y2)) {
                            if (abs > ((float) this.mTouchSlop) && 0.5f * abs > abs2) {
                                this.mIsBeingDragged = true;
                                setScrollState(1);
                                this.mLastMotionX = f > 0.0f ? this.mInitialMotionX + ((float) this.mTouchSlop) : this.mInitialMotionX - ((float) this.mTouchSlop);
                                this.mLastMotionY = y2;
                                setScrollingCacheEnabled(true);
                            } else if (abs2 > ((float) this.mTouchSlop)) {
                                this.mIsUnableToDrag = true;
                            }
                            if (this.mIsBeingDragged && performDrag(x2)) {
                                ViewCompat.postInvalidateOnAnimation(this);
                                break;
                            }
                        } else {
                            this.mLastMotionX = x2;
                            this.mLastMotionY = y2;
                            this.mIsUnableToDrag = true;
                            return false;
                        }
                    } else {
                        float f2 = y2 - this.mLastMotionY;
                        float abs3 = Math.abs(f2);
                        float abs4 = Math.abs(x2 - this.mInitialMotionX);
                        if (f2 == 0.0f || isGutterDrag(this.mLastMotionY, f2) || !canScroll(this, false, 0, (int) f2, (int) x2, (int) y2)) {
                            if (abs3 > ((float) this.mTouchSlop) && 0.5f * abs3 > abs4) {
                                this.mIsBeingDragged = true;
                                setScrollState(1);
                                this.mLastMotionX = x2;
                                this.mLastMotionY = f2 > 0.0f ? this.mInitialMotionY + ((float) this.mTouchSlop) : this.mInitialMotionY - ((float) this.mTouchSlop);
                                setScrollingCacheEnabled(true);
                            } else if (abs4 > ((float) this.mTouchSlop)) {
                                this.mIsUnableToDrag = true;
                            }
                            if (this.mIsBeingDragged && performDrag(y2)) {
                                ViewCompat.postInvalidateOnAnimation(this);
                                break;
                            }
                        } else {
                            this.mLastMotionX = x2;
                            this.mLastMotionY = y2;
                            this.mIsUnableToDrag = true;
                            return false;
                        }
                    }
                }
                break;
            case 6:
                onSecondaryPointerUp(motionEvent);
                break;
        }
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(motionEvent);
        return this.mIsBeingDragged;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int yVelocity;
        int i;
        float f;
        boolean z = false;
        if (this.mFakeDragging) {
            return true;
        }
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        if (this.mAdapter == null || this.mAdapter.getCount() == 0) {
            return false;
        }
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                this.mScroller.abortAnimation();
                this.mPopulatePending = false;
                populate();
                this.mIsBeingDragged = true;
                setScrollState(1);
                float x = motionEvent.getX();
                this.mInitialMotionX = x;
                this.mLastMotionX = x;
                float y = motionEvent.getY();
                this.mInitialMotionY = y;
                this.mLastMotionY = y;
                this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent, 0);
                break;
            case 1:
                if (this.mIsBeingDragged) {
                    VelocityTracker velocityTracker = this.mVelocityTracker;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.mMaximumVelocity);
                    if (this.mOrientation == 0) {
                        yVelocity = (int) VelocityTrackerCompat.getXVelocity(velocityTracker, this.mActivePointerId);
                    } else {
                        yVelocity = (int) VelocityTrackerCompat.getYVelocity(velocityTracker, this.mActivePointerId);
                    }
                    this.mPopulatePending = true;
                    int clientDimension = getClientDimension();
                    int scrollCoord = getScrollCoord();
                    m infoForCurrentScrollPosition = infoForCurrentScrollPosition();
                    int i2 = infoForCurrentScrollPosition.b;
                    float f2 = ((((float) scrollCoord) / ((float) clientDimension)) - infoForCurrentScrollPosition.e) / infoForCurrentScrollPosition.d;
                    int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent, this.mActivePointerId);
                    float x2 = this.mOrientation == 0 ? MotionEventCompat.getX(motionEvent, findPointerIndex) : MotionEventCompat.getY(motionEvent, findPointerIndex);
                    if (this.mOrientation == 0) {
                        i = (int) (x2 - this.mInitialMotionX);
                    } else {
                        i = (int) (x2 - this.mInitialMotionY);
                    }
                    setCurrentItemInternal(determineTargetPage(i2, f2, yVelocity, i), true, true, yVelocity);
                    this.mActivePointerId = -1;
                    endDrag();
                    z = this.mEndEdge.c() | this.mStartEdge.c();
                    break;
                }
                break;
            case 2:
                if (!this.mIsBeingDragged) {
                    int findPointerIndex2 = MotionEventCompat.findPointerIndex(motionEvent, this.mActivePointerId);
                    float x3 = MotionEventCompat.getX(motionEvent, findPointerIndex2);
                    float abs = Math.abs(x3 - this.mLastMotionX);
                    float y2 = MotionEventCompat.getY(motionEvent, findPointerIndex2);
                    float abs2 = Math.abs(y2 - this.mLastMotionY);
                    if (this.mOrientation == 0) {
                        if (abs > ((float) this.mTouchSlop) && abs > abs2) {
                            this.mIsBeingDragged = true;
                            this.mLastMotionX = x3 - this.mInitialMotionX > 0.0f ? this.mInitialMotionX + ((float) this.mTouchSlop) : this.mInitialMotionX - ((float) this.mTouchSlop);
                            this.mLastMotionY = y2;
                            setScrollState(1);
                            setScrollingCacheEnabled(true);
                        }
                    } else if (abs2 > ((float) this.mTouchSlop) && abs2 > abs) {
                        this.mIsBeingDragged = true;
                        this.mLastMotionX = x3;
                        if (y2 - this.mInitialMotionY > 0.0f) {
                            f = this.mInitialMotionY + ((float) this.mTouchSlop);
                        } else {
                            f = this.mInitialMotionY - ((float) this.mTouchSlop);
                        }
                        this.mLastMotionY = f;
                        setScrollState(1);
                        setScrollingCacheEnabled(true);
                    }
                }
                if (this.mIsBeingDragged) {
                    int findPointerIndex3 = MotionEventCompat.findPointerIndex(motionEvent, this.mActivePointerId);
                    z = false | performDrag(this.mOrientation == 0 ? MotionEventCompat.getX(motionEvent, findPointerIndex3) : MotionEventCompat.getY(motionEvent, findPointerIndex3));
                    break;
                }
                break;
            case 3:
                if (this.mIsBeingDragged) {
                    scrollToItem(this.mCurItem, true, 0, false);
                    this.mActivePointerId = -1;
                    endDrag();
                    z = this.mEndEdge.c() | this.mStartEdge.c();
                    break;
                }
                break;
            case 5:
                int actionIndex = MotionEventCompat.getActionIndex(motionEvent);
                if (this.mOrientation == 0) {
                    this.mLastMotionX = MotionEventCompat.getX(motionEvent, actionIndex);
                } else {
                    this.mLastMotionY = MotionEventCompat.getY(motionEvent, actionIndex);
                }
                this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent, actionIndex);
                break;
            case 6:
                onSecondaryPointerUp(motionEvent);
                if (this.mOrientation != 0) {
                    this.mLastMotionY = MotionEventCompat.getY(motionEvent, MotionEventCompat.findPointerIndex(motionEvent, this.mActivePointerId));
                    break;
                } else {
                    this.mLastMotionX = MotionEventCompat.getX(motionEvent, MotionEventCompat.findPointerIndex(motionEvent, this.mActivePointerId));
                    break;
                }
        }
        if (z) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
        return true;
    }

    private boolean performDrag(float f) {
        float f2;
        boolean z;
        float f3;
        boolean z2 = true;
        boolean z3 = false;
        if (this.mOrientation == 0) {
            f2 = this.mLastMotionX - f;
            this.mLastMotionX = f;
        } else {
            f2 = this.mLastMotionY - f;
            this.mLastMotionY = f;
        }
        float scrollCoord = ((float) getScrollCoord()) + f2;
        int clientDimension = getClientDimension();
        float f4 = ((float) clientDimension) * this.mFirstOffset;
        float f5 = ((float) clientDimension) * this.mLastOffset;
        m mVar = this.mItems.get(0);
        m mVar2 = this.mItems.get(this.mItems.size() - 1);
        if (mVar.b != 0) {
            f4 = mVar.e * ((float) clientDimension);
            z = false;
        } else {
            z = true;
        }
        if (mVar2.b != this.mAdapter.getCount() - 1) {
            f3 = mVar2.e * ((float) clientDimension);
            z2 = false;
        } else {
            f3 = f5;
        }
        if (scrollCoord < f4) {
            if (z) {
                z3 = this.mStartEdge.a(Math.abs(f4 - scrollCoord) / ((float) clientDimension));
            }
        } else if (scrollCoord > f3) {
            if (z2) {
                z3 = this.mEndEdge.a(Math.abs(scrollCoord - f3) / ((float) clientDimension));
            }
            f4 = f3;
        } else {
            f4 = scrollCoord;
        }
        if (this.mOrientation == 0) {
            this.mLastMotionX += f4 - ((float) ((int) f4));
            scrollTo((int) f4, getScrollY());
        } else {
            this.mLastMotionY += f4 - ((float) ((int) f4));
            scrollTo(getScrollX(), (int) f4);
        }
        pageScrolled((int) f4);
        return z3;
    }

    private m infoForCurrentScrollPosition() {
        float f;
        int i;
        m mVar;
        int clientDimension = getClientDimension();
        float scrollCoord = clientDimension > 0 ? ((float) getScrollCoord()) / ((float) clientDimension) : 0.0f;
        if (clientDimension > 0) {
            f = ((float) this.mPageMargin) / ((float) clientDimension);
        } else {
            f = 0.0f;
        }
        float f2 = 0.0f;
        float f3 = 0.0f;
        int i2 = -1;
        int i3 = 0;
        boolean z = true;
        m mVar2 = null;
        while (i3 < this.mItems.size()) {
            m mVar3 = this.mItems.get(i3);
            if (z || mVar3.b == i2 + 1) {
                m mVar4 = mVar3;
                i = i3;
                mVar = mVar4;
            } else {
                m mVar5 = this.mTempItem;
                mVar5.e = f2 + f3 + f;
                mVar5.b = i2 + 1;
                mVar5.d = this.mAdapter.getPageWidth(mVar5.b);
                m mVar6 = mVar5;
                i = i3 - 1;
                mVar = mVar6;
            }
            float f4 = mVar.e;
            float f5 = mVar.d + f4 + f;
            if (!z && scrollCoord < f4) {
                return mVar2;
            }
            if (scrollCoord < f5 || i == this.mItems.size() - 1) {
                return mVar;
            }
            f3 = f4;
            i2 = mVar.b;
            z = false;
            f2 = mVar.d;
            mVar2 = mVar;
            i3 = i + 1;
        }
        return mVar2;
    }

    private int determineTargetPage(int i, float f, int i2, int i3) {
        if (Math.abs(i3) <= this.mFlingDistance || Math.abs(i2) <= this.mMinimumVelocity) {
            i = (int) ((i >= this.mCurItem ? 0.4f : 0.6f) + ((float) i) + f);
        } else if (i2 <= 0) {
            i++;
        }
        if (this.mItems.size() > 0) {
            return Math.max(this.mItems.get(0).b, Math.min(i, this.mItems.get(this.mItems.size() - 1).b));
        }
        return i;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        boolean z = false;
        int overScrollMode = ViewCompat.getOverScrollMode(this);
        if (overScrollMode == 0 || (overScrollMode == 1 && this.mAdapter != null && this.mAdapter.getCount() > 1)) {
            if (!this.mStartEdge.a()) {
                int save = canvas.save();
                if (this.mOrientation == 0) {
                    int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
                    int width = getWidth();
                    canvas.rotate(270.0f);
                    canvas.translate((float) ((-height) + getPaddingTop()), this.mFirstOffset * ((float) width));
                    this.mStartEdge.a(height, width);
                } else {
                    int height2 = getHeight();
                    int width2 = (getWidth() - getPaddingLeft()) - getPaddingRight();
                    canvas.translate((float) getPaddingLeft(), this.mFirstOffset * ((float) height2));
                    this.mStartEdge.a(width2, height2);
                }
                z = false | this.mStartEdge.a(canvas);
                canvas.restoreToCount(save);
            }
            if (!this.mEndEdge.a()) {
                int save2 = canvas.save();
                if (this.mOrientation == 0) {
                    int width3 = getWidth();
                    int height3 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                    canvas.rotate(90.0f);
                    canvas.translate((float) (-getPaddingTop()), (-(this.mLastOffset + 1.0f)) * ((float) width3));
                    this.mEndEdge.a(height3, width3);
                } else {
                    int width4 = (getWidth() - getPaddingLeft()) - getPaddingRight();
                    int height4 = getHeight();
                    if (this.mEndEdgeMatrix == null) {
                        this.mEndEdgeMatrix = new Matrix();
                        this.mEndEdgeMatrix.setScale(1.0f, -1.0f);
                        this.mEndEdgeMatrix.postTranslate(0.0f, (this.mLastOffset + 1.0f) * ((float) height4));
                    }
                    canvas.concat(this.mEndEdgeMatrix);
                    this.mEndEdge.a(width4, height4);
                }
                z |= this.mEndEdge.a(canvas);
                canvas.restoreToCount(save2);
            }
        } else {
            this.mStartEdge.b();
            this.mEndEdge.b();
        }
        if (z) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float f;
        super.onDraw(canvas);
        if (this.mPageMargin > 0 && this.mMarginDrawable != null && this.mItems.size() > 0 && this.mAdapter != null) {
            int scrollCoord = getScrollCoord();
            int width = this.mOrientation == 0 ? getWidth() : getHeight();
            float f2 = ((float) this.mPageMargin) / ((float) width);
            m mVar = this.mItems.get(0);
            float f3 = mVar.e;
            int size = this.mItems.size();
            int i = mVar.b;
            int i2 = this.mItems.get(size - 1).b;
            int i3 = 0;
            int i4 = i;
            while (i4 < i2) {
                while (i4 > mVar.b && i3 < size) {
                    i3++;
                    mVar = this.mItems.get(i3);
                }
                if (i4 == mVar.b) {
                    f = (mVar.e + mVar.d) * ((float) width);
                    f3 = mVar.e + mVar.d + f2;
                } else {
                    float pageWidth = this.mAdapter.getPageWidth(i4);
                    f = (f3 + pageWidth) * ((float) width);
                    f3 += pageWidth + f2;
                }
                if (((float) this.mPageMargin) + f > ((float) scrollCoord)) {
                    if (this.mOrientation == 0) {
                        this.mMarginDrawable.setBounds((int) f, this.mTopPageBounds, (int) (((float) this.mPageMargin) + f + 0.5f), this.mBottomPageBounds);
                    } else {
                        this.mMarginDrawable.setBounds(this.mLeftPageBounds, (int) f, this.mRightPageBounds, (int) (((float) this.mPageMargin) + f + 0.5f));
                    }
                    this.mMarginDrawable.draw(canvas);
                }
                if (f <= ((float) (scrollCoord + width))) {
                    i4++;
                } else {
                    return;
                }
            }
        }
    }

    public boolean beginFakeDrag() {
        if (this.mIsBeingDragged) {
            return false;
        }
        this.mFakeDragging = true;
        setScrollState(1);
        this.mLastMotionX = 0.0f;
        this.mInitialMotionX = 0.0f;
        this.mLastMotionY = 0.0f;
        this.mInitialMotionY = 0.0f;
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        } else {
            this.mVelocityTracker.clear();
        }
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 0, 0.0f, 0.0f, 0);
        this.mVelocityTracker.addMovement(obtain);
        obtain.recycle();
        this.mFakeDragBeginTime = uptimeMillis;
        return true;
    }

    public void endFakeDrag() {
        if (!this.mFakeDragging) {
            throw new IllegalStateException("No fake drag in progress. Call beginFakeDrag first.");
        }
        VelocityTracker velocityTracker = this.mVelocityTracker;
        velocityTracker.computeCurrentVelocity(1000, (float) this.mMaximumVelocity);
        int xVelocity = (int) VelocityTrackerCompat.getXVelocity(velocityTracker, this.mActivePointerId);
        this.mPopulatePending = true;
        int clientDimension = getClientDimension();
        int scrollCoord = getScrollCoord();
        m infoForCurrentScrollPosition = infoForCurrentScrollPosition();
        setCurrentItemInternal(determineTargetPage(infoForCurrentScrollPosition.b, ((((float) scrollCoord) / ((float) clientDimension)) - infoForCurrentScrollPosition.e) / infoForCurrentScrollPosition.d, xVelocity, this.mOrientation == 0 ? (int) (this.mLastMotionX - this.mInitialMotionX) : (int) (this.mLastMotionY - this.mInitialMotionY)), true, true, xVelocity);
        endDrag();
        this.mFakeDragging = false;
    }

    public void fakeDragBy(float f) {
        float f2;
        float f3;
        if (!this.mFakeDragging) {
            throw new IllegalStateException("No fake drag in progress. Call beginFakeDrag first.");
        }
        if (this.mOrientation == 0) {
            this.mLastMotionX += f;
        } else {
            this.mLastMotionY += f;
        }
        float scrollCoord = ((float) getScrollCoord()) - f;
        int clientDimension = getClientDimension();
        float f4 = ((float) clientDimension) * this.mFirstOffset;
        float f5 = ((float) clientDimension) * this.mLastOffset;
        m mVar = this.mItems.get(0);
        m mVar2 = this.mItems.get(this.mItems.size() - 1);
        if (mVar.b != 0) {
            f2 = mVar.e * ((float) clientDimension);
        } else {
            f2 = f4;
        }
        if (mVar2.b != this.mAdapter.getCount() - 1) {
            f3 = mVar2.e * ((float) clientDimension);
        } else {
            f3 = f5;
        }
        if (scrollCoord >= f2) {
            if (scrollCoord > f3) {
                f2 = f3;
            } else {
                f2 = scrollCoord;
            }
        }
        if (this.mOrientation == 0) {
            this.mLastMotionX += f2 - ((float) ((int) f2));
            scrollTo((int) f2, getScrollY());
        } else {
            this.mLastMotionY += f2 - ((float) ((int) f2));
            scrollTo(getScrollX(), (int) f2);
        }
        pageScrolled((int) f2);
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = this.mOrientation == 0 ? MotionEvent.obtain(this.mFakeDragBeginTime, uptimeMillis, 2, this.mLastMotionX, 0.0f, 0) : MotionEvent.obtain(this.mFakeDragBeginTime, uptimeMillis, 2, 0.0f, this.mLastMotionY, 0);
        this.mVelocityTracker.addMovement(obtain);
        obtain.recycle();
    }

    public boolean isFakeDragging() {
        return this.mFakeDragging;
    }

    private void onSecondaryPointerUp(MotionEvent motionEvent) {
        int actionIndex = MotionEventCompat.getActionIndex(motionEvent);
        if (MotionEventCompat.getPointerId(motionEvent, actionIndex) == this.mActivePointerId) {
            int i = actionIndex == 0 ? 1 : 0;
            this.mLastMotionX = MotionEventCompat.getX(motionEvent, i);
            this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent, i);
            if (this.mVelocityTracker != null) {
                this.mVelocityTracker.clear();
            }
        }
    }

    private void endDrag() {
        this.mIsBeingDragged = false;
        this.mIsUnableToDrag = false;
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
    }

    private void setScrollingCacheEnabled(boolean z) {
        if (this.mScrollingCacheEnabled != z) {
            this.mScrollingCacheEnabled = z;
        }
    }

    public boolean canScrollHorizontally(int i) {
        boolean z = true;
        if (this.mAdapter == null) {
            return false;
        }
        int clientWidth = getClientWidth();
        int scrollX = getScrollX();
        if (i < 0) {
            if (scrollX <= ((int) (((float) clientWidth) * this.mFirstOffset))) {
                z = false;
            }
            return z;
        } else if (i <= 0) {
            return false;
        } else {
            if (scrollX >= ((int) (((float) clientWidth) * this.mLastOffset))) {
                z = false;
            }
            return z;
        }
    }

    public boolean canScrollVertically(int i) {
        boolean z = true;
        if (this.mAdapter == null) {
            return false;
        }
        int clientHeight = getClientHeight();
        int scrollY = getScrollY();
        if (i < 0) {
            if (scrollY <= ((int) (((float) clientHeight) * this.mFirstOffset))) {
                z = false;
            }
            return z;
        } else if (i <= 0) {
            return false;
        } else {
            if (scrollY >= ((int) (((float) clientHeight) * this.mLastOffset))) {
                z = false;
            }
            return z;
        }
    }

    /* access modifiers changed from: protected */
    public boolean canScroll(View view, boolean z, int i, int i2, int i3, int i4) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (i3 + scrollX >= childAt.getLeft() && i3 + scrollX < childAt.getRight() && i4 + scrollY >= childAt.getTop() && i4 + scrollY < childAt.getBottom()) {
                    if (canScroll(childAt, true, i, i2, (i3 + scrollX) - childAt.getLeft(), (i4 + scrollY) - childAt.getTop())) {
                        return true;
                    }
                }
            }
        }
        return z && ViewCompat.canScrollHorizontally(view, -i);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || executeKeyEvent(keyEvent);
    }

    public boolean executeKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0) {
            return false;
        }
        switch (keyEvent.getKeyCode()) {
            case 21:
                return arrowScroll(17);
            case ISystemOptimize.T_killTaskAsync /*22*/:
                return arrowScroll(66);
            case 61:
                if (Build.VERSION.SDK_INT < 11) {
                    return false;
                }
                if (KeyEventCompat.hasNoModifiers(keyEvent)) {
                    return arrowScroll(2);
                }
                if (KeyEventCompat.hasModifiers(keyEvent, 1)) {
                    return arrowScroll(1);
                }
                return false;
            default:
                return false;
        }
    }

    public boolean arrowScroll(int i) {
        View view;
        boolean z;
        boolean z2;
        View findFocus = findFocus();
        if (findFocus == this) {
            view = null;
        } else {
            if (findFocus != null) {
                ViewParent parent = findFocus.getParent();
                while (true) {
                    if (!(parent instanceof ViewGroup)) {
                        z = false;
                        break;
                    } else if (parent == this) {
                        z = true;
                        break;
                    } else {
                        parent = parent.getParent();
                    }
                }
                if (!z) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(findFocus.getClass().getSimpleName());
                    for (ViewParent parent2 = findFocus.getParent(); parent2 instanceof ViewGroup; parent2 = parent2.getParent()) {
                        sb.append(" => ").append(parent2.getClass().getSimpleName());
                    }
                    Log.e(TAG, "arrowScroll tried to find focus based on non-child current focused view " + sb.toString());
                    view = null;
                }
            }
            view = findFocus;
        }
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, view, i);
        if (findNextFocus == null || findNextFocus == view) {
            if (i == 17 || i == 1) {
                z2 = pageLeft();
            } else {
                if (i == 66 || i == 2) {
                    z2 = pageRight();
                }
                z2 = false;
            }
        } else if (i == 17) {
            z2 = (view == null || getChildRectInPagerCoordinates(this.mTempRect, findNextFocus).left < getChildRectInPagerCoordinates(this.mTempRect, view).left) ? findNextFocus.requestFocus() : pageLeft();
        } else {
            if (i == 66) {
                z2 = (view == null || getChildRectInPagerCoordinates(this.mTempRect, findNextFocus).left > getChildRectInPagerCoordinates(this.mTempRect, view).left) ? findNextFocus.requestFocus() : pageRight();
            }
            z2 = false;
        }
        if (z2) {
            playSoundEffect(SoundEffectConstants.getContantForFocusDirection(i));
        }
        return z2;
    }

    private Rect getChildRectInPagerCoordinates(Rect rect, View view) {
        Rect rect2;
        if (rect == null) {
            rect2 = new Rect();
        } else {
            rect2 = rect;
        }
        if (view == null) {
            rect2.set(0, 0, 0, 0);
            return rect2;
        }
        rect2.left = view.getLeft();
        rect2.right = view.getRight();
        rect2.top = view.getTop();
        rect2.bottom = view.getBottom();
        ViewParent parent = view.getParent();
        while ((parent instanceof ViewGroup) && parent != this) {
            ViewGroup viewGroup = (ViewGroup) parent;
            rect2.left += viewGroup.getLeft();
            rect2.right += viewGroup.getRight();
            rect2.top += viewGroup.getTop();
            rect2.bottom += viewGroup.getBottom();
            parent = viewGroup.getParent();
        }
        return rect2;
    }

    /* access modifiers changed from: package-private */
    public boolean pageLeft() {
        if (this.mCurItem <= 0) {
            return false;
        }
        setCurrentItem(this.mCurItem - 1, true);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean pageRight() {
        if (this.mAdapter == null || this.mCurItem >= this.mAdapter.getCount() - 1) {
            return false;
        }
        setCurrentItem(this.mCurItem + 1, true);
        return true;
    }

    public void addFocusables(ArrayList<View> arrayList, int i, int i2) {
        m infoForChild;
        int size = arrayList.size();
        int descendantFocusability = getDescendantFocusability();
        if (descendantFocusability != 393216) {
            for (int i3 = 0; i3 < getChildCount(); i3++) {
                View childAt = getChildAt(i3);
                if (childAt.getVisibility() == 0 && (infoForChild = infoForChild(childAt)) != null && infoForChild.b == this.mCurItem) {
                    childAt.addFocusables(arrayList, i, i2);
                }
            }
        }
        if ((descendantFocusability == 262144 && size != arrayList.size()) || !isFocusable()) {
            return;
        }
        if (((i2 & 1) != 1 || !isInTouchMode() || isFocusableInTouchMode()) && arrayList != null) {
            arrayList.add(this);
        }
    }

    public void addTouchables(ArrayList<View> arrayList) {
        m infoForChild;
        for (int i = 0; i < getChildCount(); i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 0 && (infoForChild = infoForChild(childAt)) != null && infoForChild.b == this.mCurItem) {
                childAt.addTouchables(arrayList);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i, Rect rect) {
        int i2;
        m infoForChild;
        int i3 = -1;
        int childCount = getChildCount();
        if ((i & 2) != 0) {
            i3 = 1;
            i2 = 0;
        } else {
            i2 = childCount - 1;
            childCount = -1;
        }
        while (i2 != childCount) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (infoForChild = infoForChild(childAt)) != null && infoForChild.b == this.mCurItem && childAt.requestFocus(i, rect)) {
                return true;
            }
            i2 += i3;
        }
        return false;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        m infoForChild;
        if (accessibilityEvent.getEventType() == 4096) {
            return super.dispatchPopulateAccessibilityEvent(accessibilityEvent);
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 0 && (infoForChild = infoForChild(childAt)) != null && infoForChild.b == this.mCurItem && childAt.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams();
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return generateDefaultLayoutParams();
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && super.checkLayoutParams(layoutParams);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* compiled from: ProGuard */
    public class LayoutParams extends ViewGroup.LayoutParams {
        int childIndex;
        float dimensionFactor = 0.0f;
        public int gravity;
        public boolean isDecor;
        boolean needsMeasure;
        int position;

        public LayoutParams() {
            super(-1, -1);
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, DirectionalViewPager.LAYOUT_ATTRS);
            this.gravity = obtainStyledAttributes.getInteger(0, 48);
            obtainStyledAttributes.recycle();
        }
    }
}
