package android.support.v4.view.a;

import android.os.Bundle;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import java.util.List;

/* compiled from: ProGuard */
final class m extends AccessibilityNodeProvider {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ n f79a;

    m(n nVar) {
        this.f79a = nVar;
    }

    public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
        return (AccessibilityNodeInfo) this.f79a.a(i);
    }

    public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
        return this.f79a.a(str, i);
    }

    public boolean performAction(int i, int i2, Bundle bundle) {
        return this.f79a.a(i, i2, bundle);
    }
}
