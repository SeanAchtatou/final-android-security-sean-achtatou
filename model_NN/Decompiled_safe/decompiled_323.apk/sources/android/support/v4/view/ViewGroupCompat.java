package android.support.v4.view;

import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: ProGuard */
public class ViewGroupCompat {
    static final bn IMPL;

    static {
        if (Build.VERSION.SDK_INT >= 14) {
            IMPL = new bm();
        } else {
            IMPL = new bo();
        }
    }

    private ViewGroupCompat() {
    }

    public static boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return IMPL.a(viewGroup, view, accessibilityEvent);
    }
}
