package android.support.v4.view;

import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: ProGuard */
class a extends f {
    a() {
    }

    public Object a() {
        return AccessibilityDelegateCompatIcs.a();
    }

    public Object a(AccessibilityDelegateCompat accessibilityDelegateCompat) {
        return AccessibilityDelegateCompatIcs.a(new b(this, accessibilityDelegateCompat));
    }

    public boolean a(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        return AccessibilityDelegateCompatIcs.a(obj, view, accessibilityEvent);
    }

    public void b(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        AccessibilityDelegateCompatIcs.b(obj, view, accessibilityEvent);
    }

    public void a(Object obj, View view, android.support.v4.view.a.a aVar) {
        AccessibilityDelegateCompatIcs.a(obj, view, aVar.a());
    }

    public void c(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        AccessibilityDelegateCompatIcs.c(obj, view, accessibilityEvent);
    }

    public boolean a(Object obj, ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return AccessibilityDelegateCompatIcs.a(obj, viewGroup, view, accessibilityEvent);
    }

    public void a(Object obj, View view, int i) {
        AccessibilityDelegateCompatIcs.a(obj, view, i);
    }

    public void d(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        AccessibilityDelegateCompatIcs.d(obj, view, accessibilityEvent);
    }
}
