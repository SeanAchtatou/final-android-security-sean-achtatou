package com.tencent.downloadsdk.protocol;

import android.content.Context;
import android.net.Proxy;
import android.text.TextUtils;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.downloadsdk.b;
import com.tencent.downloadsdk.protocol.jce.Net;
import com.tencent.downloadsdk.protocol.jce.ReqHead;
import com.tencent.downloadsdk.protocol.jce.Request;
import com.tencent.downloadsdk.protocol.jce.Response;
import com.tencent.downloadsdk.protocol.jce.Terminal;
import com.tencent.downloadsdk.utils.e;
import com.tencent.downloadsdk.utils.g;
import java.util.HashMap;
import java.util.Map;

public class a {
    public static int a(String str) {
        if (str == null) {
            return 0;
        }
        HashMap hashMap = new HashMap();
        hashMap.put(1, "ReportLog");
        hashMap.put(2, "GetSettings");
        hashMap.put(3, "GetAppUpdate");
        hashMap.put(4, "GetAuthorized");
        for (Map.Entry entry : hashMap.entrySet()) {
            if (entry != null) {
                Integer num = (Integer) entry.getKey();
                String str2 = (String) entry.getValue();
                if (str2 != null && str2.equals(str)) {
                    return num.intValue();
                }
            }
        }
        return 0;
    }

    public static JceStruct a(JceStruct jceStruct, byte[] bArr) {
        JceStruct d;
        if (jceStruct == null || bArr == null || (d = d(jceStruct)) == null) {
            return null;
        }
        try {
            JceInputStream jceInputStream = new JceInputStream(bArr);
            jceInputStream.setServerEncoding("utf-8");
            d.readFrom(jceInputStream);
            return d;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static <T extends com.qq.taf.jce.JceStruct> T a(byte[] r3, java.lang.Class r4) {
        /*
            r1 = 0
            if (r3 != 0) goto L_0x0005
            r0 = r1
        L_0x0004:
            return r0
        L_0x0005:
            com.qq.taf.jce.JceInputStream r2 = new com.qq.taf.jce.JceInputStream     // Catch:{ Exception -> 0x0019 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0019 }
            java.lang.String r0 = "utf-8"
            r2.setServerEncoding(r0)     // Catch:{ Exception -> 0x0019 }
            java.lang.Object r0 = r4.newInstance()     // Catch:{ Exception -> 0x0019 }
            com.qq.taf.jce.JceStruct r0 = (com.qq.taf.jce.JceStruct) r0     // Catch:{ Exception -> 0x0019 }
            r0.readFrom(r2)     // Catch:{ Exception -> 0x0019 }
            goto L_0x0004
        L_0x0019:
            r0 = move-exception
            r0 = r1
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.protocol.a.a(byte[], java.lang.Class):com.qq.taf.jce.JceStruct");
    }

    public static ReqHead a(JceStruct jceStruct) {
        int i = 0;
        if (jceStruct == null) {
            return null;
        }
        Context b = DownloadManager.a().b();
        ReqHead reqHead = new ReqHead();
        reqHead.f2495a = b.a();
        String simpleName = jceStruct.getClass().getSimpleName();
        reqHead.b = a(simpleName.substring(0, simpleName.length() - "Request".length()));
        reqHead.d = b.g;
        reqHead.c = b.b(b);
        Terminal terminal = new Terminal();
        terminal.c = b.i;
        terminal.d = reqHead.c;
        terminal.f2501a = g.a(b);
        terminal.e = g.b(b);
        terminal.b = g.c(b);
        reqHead.f = terminal;
        reqHead.g = b.f;
        reqHead.h = b.d;
        reqHead.j = b.b;
        reqHead.j = b.c;
        Net net = new Net();
        net.f2494a = (byte) g.h(b);
        net.b = g.g(b);
        net.c = g.h(b);
        if (!TextUtils.isEmpty(Proxy.getDefaultHost())) {
            i = 1;
        }
        net.d = (byte) i;
        reqHead.i = net;
        return reqHead;
    }

    public static Response a(byte[] bArr) {
        if (bArr == null || bArr.length < 4) {
            return null;
        }
        Response response = new Response();
        try {
            JceInputStream jceInputStream = new JceInputStream(bArr);
            jceInputStream.setServerEncoding("utf-8");
            response.readFrom(jceInputStream);
            if (response.f2497a.c != 0) {
                return response;
            }
            if ((response.f2497a.d & 2) == 2) {
                response.b = b(response.b, "ji*9^&43U0X-~./(".getBytes());
            }
            if ((response.f2497a.d & 1) == 1) {
                response.b = b.b(response.b);
            }
            b.a(DownloadManager.a().b(), response.f2497a.e);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] a(Request request) {
        if (request == null) {
            return null;
        }
        request.f2496a.e = 0;
        if (request.b.length > 256) {
            request.b = b.a(request.b);
            request.f2496a.e = (byte) (request.f2496a.e | 1);
        }
        request.b = a(request.b, "ji*9^&43U0X-~./(".getBytes());
        request.f2496a.e = (byte) (request.f2496a.e | 2);
        return c(request);
    }

    public static byte[] a(byte[] bArr, byte[] bArr2) {
        return new e().b(bArr, bArr2);
    }

    public static Request b(JceStruct jceStruct) {
        if (jceStruct == null) {
            return null;
        }
        Request request = new Request();
        request.f2496a = a(jceStruct);
        request.b = c(jceStruct);
        return request;
    }

    public static byte[] b(byte[] bArr, byte[] bArr2) {
        return new e().a(bArr, bArr2);
    }

    public static byte[] c(JceStruct jceStruct) {
        if (jceStruct == null) {
            return null;
        }
        JceOutputStream jceOutputStream = new JceOutputStream();
        jceOutputStream.setServerEncoding("utf-8");
        jceStruct.writeTo(jceOutputStream);
        return jceOutputStream.toByteArray();
    }

    private static JceStruct d(JceStruct jceStruct) {
        JceStruct jceStruct2;
        if (jceStruct == null) {
            return null;
        }
        String name = jceStruct.getClass().getName();
        try {
            jceStruct2 = (JceStruct) Class.forName(name.substring(0, name.length() - "Request".length()) + "Response").newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            jceStruct2 = null;
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
            jceStruct2 = null;
        } catch (InstantiationException e3) {
            e3.printStackTrace();
            jceStruct2 = null;
        }
        return jceStruct2;
    }
}
