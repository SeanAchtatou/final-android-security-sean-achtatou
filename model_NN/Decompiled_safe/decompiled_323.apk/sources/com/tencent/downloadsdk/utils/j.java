package com.tencent.downloadsdk.utils;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.text.TextUtils;
import java.util.concurrent.atomic.AtomicInteger;

public class j {

    /* renamed from: a  reason: collision with root package name */
    private static Handler f2517a;
    private static Object b = new Object();
    private static Handler[] c = new Handler[3];
    private static final AtomicInteger d = new AtomicInteger(0);

    static {
        for (int i = 0; i < 3; i++) {
            HandlerThread handlerThread = new HandlerThread("download-handler-thread-" + i);
            handlerThread.start();
            try {
                c[i] = new Handler(handlerThread.getLooper());
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    c[i] = new Handler(handlerThread.getLooper());
                } catch (Exception e2) {
                }
            }
        }
    }

    public static Handler a() {
        if (f2517a == null) {
            synchronized (b) {
                if (f2517a == null) {
                    f2517a = new Handler(Looper.getMainLooper());
                }
            }
        }
        return f2517a;
    }

    public static Handler a(String str) {
        if (TextUtils.isEmpty(str)) {
            str = "default-thread";
        }
        HandlerThread handlerThread = new HandlerThread(str);
        handlerThread.start();
        Looper looper = handlerThread.getLooper();
        if (looper != null) {
            return new Handler(looper);
        }
        return null;
    }

    public static Handler b() {
        Handler handler = c[d.getAndIncrement() % 3];
        return handler == null ? c[0] : handler;
    }
}
