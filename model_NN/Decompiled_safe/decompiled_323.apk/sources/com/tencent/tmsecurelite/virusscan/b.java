package com.tencent.tmsecurelite.virusscan;

import android.os.IBinder;
import android.os.Parcel;
import java.util.List;

/* compiled from: ProGuard */
public final class b implements IVirusScan {

    /* renamed from: a  reason: collision with root package name */
    private IBinder f4041a;

    public b(IBinder iBinder) {
        this.f4041a = iBinder;
    }

    public IBinder asBinder() {
        return this.f4041a;
    }

    public void scanInstalledPackages(IScanListener iScanListener, boolean z) {
        int i = 1;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            obtain.writeStrongBinder((IBinder) iScanListener);
            if (!z) {
                i = 0;
            }
            obtain.writeByte((byte) i);
            this.f4041a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void scanSdcardApks(IScanListener iScanListener, boolean z) {
        int i = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            obtain.writeStrongBinder((IBinder) iScanListener);
            if (z) {
                i = 1;
            }
            obtain.writeByte((byte) i);
            this.f4041a.transact(2, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void scanGlobal(IScanListener iScanListener, boolean z) {
        int i = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            obtain.writeStrongBinder((IBinder) iScanListener);
            if (z) {
                i = 1;
            }
            obtain.writeByte((byte) i);
            this.f4041a.transact(3, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void scanPackages(List<String> list, IScanListener iScanListener, boolean z) {
        int i = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            obtain.writeStringList(list);
            obtain.writeStrongBinder((IBinder) iScanListener);
            if (z) {
                i = 1;
            }
            obtain.writeByte((byte) i);
            this.f4041a.transact(4, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void scanApks(List<String> list, IScanListener iScanListener, boolean z) {
        int i = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            obtain.writeStringList(list);
            obtain.writeStrongBinder((IBinder) iScanListener);
            if (z) {
                i = 1;
            }
            obtain.writeByte((byte) i);
            this.f4041a.transact(5, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void pauseScan() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            this.f4041a.transact(6, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void continueScan() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            this.f4041a.transact(7, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void cancelScan() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            this.f4041a.transact(8, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void freeScanner() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            this.f4041a.transact(9, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public boolean checkVersion(int i) {
        boolean z = true;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            obtain.writeInt(i);
            this.f4041a.transact(10, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readByte() != 1) {
                z = false;
            }
            return z;
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int scanInstalledPackagesAsync(IScanListener iScanListener, boolean z) {
        int i = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            obtain.writeStrongBinder((IBinder) iScanListener);
            if (z) {
                i = 1;
            }
            obtain.writeByte((byte) i);
            this.f4041a.transact(11, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int scanSdcardApksAsync(IScanListener iScanListener, boolean z) {
        int i = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            obtain.writeStrongBinder((IBinder) iScanListener);
            if (z) {
                i = 1;
            }
            obtain.writeByte((byte) i);
            this.f4041a.transact(12, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int scanGlobalAsync(IScanListener iScanListener, boolean z) {
        int i = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            obtain.writeStrongBinder((IBinder) iScanListener);
            if (z) {
                i = 1;
            }
            obtain.writeByte((byte) i);
            this.f4041a.transact(13, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int scanPackagesAsync(List<String> list, IScanListener iScanListener, boolean z) {
        int i = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            obtain.writeStringList(list);
            obtain.writeStrongBinder((IBinder) iScanListener);
            if (z) {
                i = 1;
            }
            obtain.writeByte((byte) i);
            this.f4041a.transact(14, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int scanApksAsync(List<String> list, IScanListener iScanListener, boolean z) {
        int i = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            obtain.writeStringList(list);
            obtain.writeStrongBinder((IBinder) iScanListener);
            if (z) {
                i = 1;
            }
            obtain.writeByte((byte) i);
            this.f4041a.transact(15, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int pauseScanAsync() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            this.f4041a.transact(16, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int continueScanAsync() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            this.f4041a.transact(17, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int cancelScanAsync() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            this.f4041a.transact(18, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int freeScannerAsync() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            this.f4041a.transact(19, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void updateTmsConfigAsync(com.tencent.tmsecurelite.commom.b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IVirusScan.INTERFACE);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4041a.transact(21, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }
}
