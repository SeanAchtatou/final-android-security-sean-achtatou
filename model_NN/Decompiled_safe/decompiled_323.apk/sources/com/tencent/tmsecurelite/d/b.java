package com.tencent.tmsecurelite.d;

import android.os.IBinder;
import android.os.Parcel;

/* compiled from: ProGuard */
public final class b implements a {

    /* renamed from: a  reason: collision with root package name */
    private IBinder f4035a;

    public b(IBinder iBinder) {
        this.f4035a = iBinder;
    }

    public IBinder asBinder() {
        return this.f4035a;
    }

    public void a(boolean z, com.tencent.tmsecurelite.commom.b bVar) {
        int i = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IPaySecure");
            if (!z) {
                i = 1;
            }
            obtain.writeInt(i);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4035a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }
}
