package com.tencent.tmsecurelite.a;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* compiled from: ProGuard */
public abstract class d extends Binder implements n {
    public d() {
        attachInterface(this, "com.tencent.tmsecurelite.IDeleteListener");
    }

    public static n a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.tencent.tmsecurelite.IDeleteListener");
        if (queryLocalInterface == null || !(queryLocalInterface instanceof n)) {
            return new c(iBinder);
        }
        return (n) queryLocalInterface;
    }

    /* access modifiers changed from: protected */
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                a(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 2:
                b(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 3:
                c(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 4:
                a(parcel.readInt(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 5:
                a(parcel.readInt(), parcel.readInt(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 6:
                a(parcel.readInt(), parcel.readInt(), parcel.readInt());
                parcel2.writeNoException();
                return true;
            default:
                return true;
        }
    }
}
