package com.tencent.feedback.proguard;

import com.tencent.feedback.common.c;
import com.tencent.feedback.common.g;
import java.util.Date;

/* compiled from: ProGuard */
final class ax implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aw f2598a;

    ax(aw awVar) {
        this.f2598a = awVar;
    }

    public final void run() {
        long e = this.f2598a.e();
        long time = new Date().getTime();
        long j = e - time;
        c f = this.f2598a.f();
        if (f == null) {
            return;
        }
        if (j > 0) {
            f.a(this, j + 1000);
            return;
        }
        g.b("rqdp{  next day to upload}", new Object[0]);
        this.f2598a.c();
        long d = this.f2598a.d();
        this.f2598a.b(d);
        f.a(this, d - time);
        g.b("rqdp{ next day %d}", Long.valueOf(d - time));
    }
}
