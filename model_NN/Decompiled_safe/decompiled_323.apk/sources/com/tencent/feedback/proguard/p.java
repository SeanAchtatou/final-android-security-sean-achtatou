package com.tencent.feedback.proguard;

import com.tencent.feedback.common.g;
import java.io.Serializable;
import java.util.Locale;

/* compiled from: ProGuard */
public class p implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public final int f2604a;
    public final long b;
    public final long c;
    public final long d;
    public final long e;
    public final long f;
    public final long g;
    private long h = -1;

    public p(int i, long j, long j2, long j3, long j4, long j5, long j6) {
        this.f2604a = i;
        this.b = j;
        this.c = j2;
        this.d = j3;
        this.e = j4;
        this.f = j5;
        this.g = j6;
    }

    public String toString() {
        try {
            return String.format(Locale.US, "[tp:%d , st:%d ,counts:%d, wifi:%d , notWifi:%d , up:%d , dn:%d]", Integer.valueOf(this.f2604a), Long.valueOf(this.b), Long.valueOf(this.c), Long.valueOf(this.d), Long.valueOf(this.e), Long.valueOf(this.f), Long.valueOf(this.g));
        } catch (Throwable th) {
            if (!g.a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    public final synchronized long a() {
        return this.h;
    }

    public final synchronized void a(long j) {
        this.h = j;
    }
}
