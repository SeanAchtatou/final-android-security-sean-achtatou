package com.tencent.feedback.proguard;

import android.content.Context;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.feedback.common.b;
import com.tencent.feedback.common.e;
import com.tencent.feedback.common.g;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: ProGuard */
public final class a extends com.tencent.feedback.b.a {
    private aw d = null;
    private byte e = -1;

    public a(Context context, aw awVar, byte b) {
        super(context, EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLIST_FAIL, NormalErrorRecommendPage.ERROR_TYPE_SEARCH_RESULT_EMPTY);
        this.d = awVar;
        this.e = b;
    }

    public final synchronized void a(boolean z) {
    }

    public final synchronized C a() {
        return e();
    }

    private synchronized C e() {
        C c = null;
        synchronized (this) {
            try {
                Y a2 = a(this.c, this.d, this.e);
                if (a2 != null) {
                    g.b("rqdp{   guid =} %s rqdp{ procName=} %s rqdp{ imei=}%s rqdp{ mac=}%s", a2.f2581a, a2.b, a2.c, a2.d);
                    c = a(this.c, this.f2535a, a2.a());
                }
            } catch (Throwable th) {
                if (!g.a(th)) {
                    th.printStackTrace();
                }
            }
        }
        return c;
    }

    public static Y a(Context context, aw awVar, byte b) {
        String i;
        q[] a2;
        byte b2;
        if (awVar == null || context == null || (a2 = awVar.a((i = b.i(context)))) == null || a2.length <= 0) {
            return null;
        }
        awVar.a(a2);
        e a3 = e.a(context);
        String p = a3.p();
        String c = ac.c((a3.f() + p + a3.s()).getBytes());
        String q = a3.q();
        ArrayList<X> arrayList = new ArrayList<>(a2.length);
        for (q qVar : a2) {
            switch (qVar.b()) {
                case 1:
                    b2 = 1;
                    break;
                case 2:
                    b2 = 2;
                    break;
                case 3:
                    b2 = 3;
                    break;
                case 4:
                    b2 = 4;
                    break;
                default:
                    g.c("rqdp{  unknown app state :%d ,drop it}", Integer.valueOf(qVar.b()));
                    continue;
            }
            X x = new X(qVar.a(), b2, qVar.e(), qVar.c());
            g.b("rqdp{  loc st :%s , uid:%s ,  tm:%d , st:%d }", qVar.c(), qVar.e(), Long.valueOf(qVar.a()), Byte.valueOf(b2));
            arrayList.add(x);
        }
        if (arrayList.size() <= 0) {
            return null;
        }
        Y y = new Y();
        y.f2581a = c;
        y.c = p;
        y.d = q;
        y.b = i;
        y.f = arrayList;
        y.e = b;
        J j = new J();
        j.f2573a = new StringBuilder().append(a3).toString() != null ? a3.p() : "null";
        j.d = a3.f();
        j.i = a3.x();
        j.h = a3.w();
        j.c = a3.i();
        j.f = a3.t();
        j.g = a3.u();
        j.e = a3.g();
        j.j = new HashMap(5);
        j.j.put("totalSD", new StringBuilder().append(a3.v()).toString());
        j.j.put("country", a3.z());
        j.j.put("imei", a3.p());
        j.j.put("imsi", a3.r());
        j.j.put("androidId", a3.s());
        j.j.put("mac", a3.q());
        y.g = j;
        g.b("symbol %s", j.d);
        g.b("brand %s", j.d);
        g.b("cpuName %s", j.i);
        g.b("cpuType %s", j.h);
        g.b("deviceId %s", j.c);
        g.b("diskSize %s", Long.valueOf(j.f));
        g.b("memSize %s", Long.valueOf(j.g));
        g.b("osver %s", j.e);
        g.b("totalSD %s", j.j.get("totalSD"));
        g.b("country %s", j.j.get("country"));
        g.b("imei %s", j.j.get("imei"));
        g.b("imsi %s", j.j.get("imsi"));
        g.b("androidId %s", j.j.get("androidId"));
        g.b("mac %s", j.j.get("mac"));
        return y;
    }
}
