package com.tencent.feedback.proguard;

/* compiled from: ProGuard */
public final class ba {

    /* renamed from: a  reason: collision with root package name */
    private long f2601a = -1;
    private int b = -1;
    private byte[] c = null;
    private long d = -1;

    public final long a() {
        return this.f2601a;
    }

    public final void a(long j) {
        this.f2601a = j;
    }

    public final int b() {
        return this.b;
    }

    public final void a(int i) {
        this.b = i;
    }

    public final byte[] c() {
        return this.c;
    }

    public final void a(byte[] bArr) {
        this.c = bArr;
    }

    public final long d() {
        return this.d;
    }

    public final void b(long j) {
        this.d = j;
    }
}
