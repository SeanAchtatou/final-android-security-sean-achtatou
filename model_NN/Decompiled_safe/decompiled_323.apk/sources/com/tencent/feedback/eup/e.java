package com.tencent.feedback.eup;

import com.tencent.assistant.st.STConst;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.common.PlugInInfo;
import com.tencent.feedback.common.g;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/* compiled from: ProGuard */
public class e implements Serializable {
    private long A = -1;
    private long B = -1;
    private String C;
    private byte[] D;
    private Map<String, PlugInInfo> E;
    private String F;
    private boolean G;
    private String H;
    private String I = Constants.STR_EMPTY;
    private String J = Constants.STR_EMPTY;
    private String K = Constants.STR_EMPTY;
    private String L = "unknwon";
    private String M = Constants.STR_EMPTY;
    private String N = Constants.STR_EMPTY;
    private String O = Constants.STR_EMPTY;
    private final Map<String, String> P = new HashMap();
    private long Q = -1;

    /* renamed from: a  reason: collision with root package name */
    private long f2555a = -1;
    private byte b = 0;
    private boolean c = false;
    private boolean d = false;
    private int e = 0;
    private String f = Constants.STR_EMPTY;
    private String g;
    private String h;
    private String i;
    private String j;
    private long k;
    private String l;
    private int m;
    private byte[] n;
    private String o;
    private String p;
    private String q;
    private String r;
    private float s = -1.0f;
    private float t = -1.0f;
    private long u = -1;
    private long v = -1;
    private long w = -1;
    private long x = -1;
    private long y = -1;
    private long z = -1;

    public e() {
        try {
            this.F = UUID.randomUUID().toString();
        } catch (Throwable th) {
            if (!g.a(th) && !g.a(th)) {
                th.printStackTrace();
            }
            this.F = STConst.ST_INSTALL_FAIL_STR_UNKNOWN;
        }
    }

    public final long a() {
        return this.f2555a;
    }

    public final void a(long j2) {
        this.f2555a = j2;
    }

    public final boolean b() {
        return this.b == 0;
    }

    public final boolean c() {
        return this.c;
    }

    public final void a(boolean z2) {
        this.c = true;
    }

    public final boolean d() {
        return this.b == 2;
    }

    public final String e() {
        return this.g;
    }

    public final void a(String str) {
        this.g = str;
    }

    public final String f() {
        return this.h;
    }

    public final void b(String str) {
        this.h = str;
    }

    public final String g() {
        return this.i;
    }

    public final void c(String str) {
        this.i = str;
    }

    public final String h() {
        return this.j;
    }

    public final void d(String str) {
        this.j = str;
    }

    public final long i() {
        return this.k;
    }

    public final void b(long j2) {
        this.k = j2;
    }

    public final float j() {
        return this.s;
    }

    public final void a(float f2) {
        this.s = -1.0f;
    }

    public final float k() {
        return this.t;
    }

    public final int l() {
        return this.e;
    }

    public final void a(int i2) {
        this.e = i2;
    }

    public final String m() {
        return this.f;
    }

    public final void e(String str) {
        this.f = str;
    }

    public final String n() {
        return this.l;
    }

    public final void f(String str) {
        this.l = str;
    }

    public final int o() {
        return this.m;
    }

    public final void b(int i2) {
        this.m = i2;
    }

    public final byte[] p() {
        return this.n;
    }

    public final void a(byte[] bArr) {
        this.n = bArr;
    }

    public final String q() {
        return this.o;
    }

    public final void g(String str) {
        this.o = str;
    }

    public final String r() {
        return this.p;
    }

    public final void h(String str) {
        this.p = str;
    }

    public final String s() {
        return this.q;
    }

    public final void i(String str) {
        this.q = str;
    }

    public final String t() {
        return this.r;
    }

    public final void j(String str) {
        this.r = str;
    }

    public final String u() {
        return this.C;
    }

    public final void k(String str) {
        this.C = str;
    }

    public final byte[] v() {
        return this.D;
    }

    public final void b(byte[] bArr) {
        this.D = bArr;
    }

    public final Map<String, PlugInInfo> w() {
        return this.E;
    }

    public final void a(Map<String, PlugInInfo> map) {
        this.E = map;
    }

    public final String x() {
        return this.F;
    }

    public final void l(String str) {
        this.F = str;
    }

    public final boolean y() {
        return this.G;
    }

    public final void b(boolean z2) {
        this.G = true;
    }

    public final boolean z() {
        return this.b == 3;
    }

    public final String A() {
        return this.H;
    }

    public final void m(String str) {
        this.H = str;
    }

    public final String B() {
        return this.I;
    }

    public final String C() {
        return this.J;
    }

    public final void n(String str) {
        this.J = str;
    }

    public final String D() {
        return this.K;
    }

    public final void o(String str) {
        this.K = str;
    }

    public final String E() {
        return this.L;
    }

    public final void p(String str) {
        this.L = str;
    }

    public final Map<String, String> F() {
        return this.P;
    }

    public final String G() {
        return this.M;
    }

    public final void q(String str) {
        this.M = str;
    }

    public final boolean H() {
        return this.d;
    }

    public final void c(boolean z2) {
        this.d = true;
    }

    public final long I() {
        return this.u;
    }

    public final void c(long j2) {
        this.u = j2;
    }

    public final long J() {
        return this.v;
    }

    public final void d(long j2) {
        this.v = j2;
    }

    public final long K() {
        return this.w;
    }

    public final void e(long j2) {
        this.w = j2;
    }

    public final long L() {
        return this.x;
    }

    public final void f(long j2) {
        this.x = j2;
    }

    public final long M() {
        return this.y;
    }

    public final void g(long j2) {
        this.y = j2;
    }

    public final long N() {
        return this.z;
    }

    public final void h(long j2) {
        this.z = j2;
    }

    public final long O() {
        return this.A;
    }

    public final void i(long j2) {
        this.A = j2;
    }

    public final long P() {
        return this.B;
    }

    public final void j(long j2) {
        this.B = j2;
    }

    public final String Q() {
        return this.N;
    }

    public final void r(String str) {
        this.N = str;
    }

    public final String R() {
        return this.O;
    }

    public final void s(String str) {
        this.O = str;
    }

    public final byte S() {
        return this.b;
    }

    public final void a(byte b2) {
        this.b = b2;
    }

    public final long T() {
        return this.Q;
    }

    public final void k(long j2) {
        this.Q = j2;
    }
}
