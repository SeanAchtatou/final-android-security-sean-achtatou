package com.tencent.nucleus.socialcontact.comment;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.protocol.jce.ReplyDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.Random;

/* compiled from: ProGuard */
class am extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ReplyDetail f3095a;
    final /* synthetic */ int b;
    final /* synthetic */ ap c;
    final /* synthetic */ CommentReplyListAdapter d;

    am(CommentReplyListAdapter commentReplyListAdapter, ReplyDetail replyDetail, int i, ap apVar) {
        this.d = commentReplyListAdapter;
        this.f3095a = replyDetail;
        this.b = i;
        this.c = apVar;
    }

    public STInfoV2 getStInfo(View view) {
        return null;
    }

    public void onTMAClick(View view) {
        boolean z;
        byte b2;
        if (this.d.e == 0) {
            ah.a().removeCallbacks(this.d.l);
            ah.a().postDelayed(this.d.l, 2000);
        } else if (this.d.e >= 3) {
            Toast.makeText(this.d.f3079a, this.d.f3079a.getString(R.string.comment_praise_tofast), 0).show();
            ah.a().removeCallbacks(this.d.l);
            ah.a().postDelayed(this.d.l, 2000);
        }
        CommentReplyListAdapter.d(this.d);
        if (this.f3095a.k == 1) {
            z = true;
        } else {
            z = false;
        }
        if (z) {
            this.f3095a.k = 2;
            this.f3095a.j--;
            if (this.f3095a.j < 0) {
                this.f3095a.j = 0;
            }
            l.a(new STInfoV2(STConst.ST_PAGE_COMMENT_REPLY, this.d.j + bm.a(this.b + 2), 0, STConst.ST_DEFAULT_SLOT, 200));
            b2 = 2;
        } else {
            this.f3095a.k = 1;
            this.c.h.setText(this.d.g[new Random().nextInt(this.d.g.length)]);
            this.c.h.setVisibility(0);
            Animation loadAnimation = AnimationUtils.loadAnimation(this.d.f3079a, R.anim.comment_praise_animation);
            loadAnimation.setAnimationListener(new ao(this.d, this.c.h));
            this.c.h.startAnimation(loadAnimation);
            this.f3095a.j++;
            if (this.f3095a.j < 0) {
                this.f3095a.j = 0;
            }
            l.a(new STInfoV2(STConst.ST_PAGE_COMMENT_REPLY, this.d.i + bm.a(this.b + 2), 0, STConst.ST_DEFAULT_SLOT, 200));
            b2 = 1;
        }
        this.d.notifyDataSetChanged();
        this.d.h.a(this.d.f, this.f3095a.f1459a, b2);
    }
}
