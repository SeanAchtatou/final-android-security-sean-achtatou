package com.tencent.nucleus.socialcontact.login;

import android.os.Message;
import android.util.Log;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.module.callback.i;
import com.tencent.assistant.protocol.jce.GetUserInfoRequest;
import com.tencent.assistant.protocol.jce.GetUserInfoResponse;

/* compiled from: ProGuard */
class d implements i {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f3158a;

    d(a aVar) {
        this.f3158a = aVar;
    }

    public void a(int i, GetUserInfoRequest getUserInfoRequest, GetUserInfoResponse getUserInfoResponse) {
        if (getUserInfoRequest != null) {
            m mVar = new m(getUserInfoResponse.b, getUserInfoResponse.c, getUserInfoResponse.d);
            l.a(mVar);
            Message obtainMessage = AstApp.i().j().obtainMessage();
            obtainMessage.obj = mVar;
            obtainMessage.arg1 = i;
            Log.d("Donaldxu", "UI_EVENT_GET_USERINFO_SUCCESS---profile = " + mVar.f3165a + " nickName = " + mVar.b + " bitmap= " + mVar.c);
            obtainMessage.what = EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS;
            AstApp.i().j().sendMessage(obtainMessage);
        }
    }

    public void a(int i, int i2, GetUserInfoRequest getUserInfoRequest, GetUserInfoResponse getUserInfoResponse) {
        l.a((m) null);
        Message obtainMessage = AstApp.i().j().obtainMessage();
        obtainMessage.what = EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL;
        obtainMessage.arg1 = i;
        Log.d("Donaldxu", "UI_EVENT_GET_USERINFO_FAIL errorCode=" + i2);
        AstApp.i().j().sendMessage(obtainMessage);
    }
}
