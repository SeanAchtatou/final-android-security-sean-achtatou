package com.tencent.nucleus.socialcontact.comment;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.protocol.jce.PraiseReplyRequest;
import com.tencent.assistant.protocol.jce.PraiseReplyResponse;

/* compiled from: ProGuard */
class ba implements CallbackHelper.Caller<y> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3108a;
    final /* synthetic */ PraiseReplyResponse b;
    final /* synthetic */ PraiseReplyRequest c;
    final /* synthetic */ az d;

    ba(az azVar, int i, PraiseReplyResponse praiseReplyResponse, PraiseReplyRequest praiseReplyRequest) {
        this.d = azVar;
        this.f3108a = i;
        this.b = praiseReplyResponse;
        this.c = praiseReplyRequest;
    }

    /* renamed from: a */
    public void call(y yVar) {
        yVar.a(this.f3108a, this.b.f1444a, this.c.f1443a, this.b.b, this.b.c);
    }
}
