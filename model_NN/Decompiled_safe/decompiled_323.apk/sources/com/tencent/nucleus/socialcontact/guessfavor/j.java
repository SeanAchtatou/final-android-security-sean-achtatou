package com.tencent.nucleus.socialcontact.guessfavor;

import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.GuessYouLikeRequest;
import com.tencent.assistant.protocol.jce.GuessYouLikeResponse;
import com.tencent.assistant.protocol.jce.RecommendAppInfoEx;
import com.tencent.assistant.protocol.jce.SimpleAppInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.by;
import com.tencent.assistant.utils.t;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class j extends BaseEngine<i> {

    /* renamed from: a  reason: collision with root package name */
    protected int f3151a;
    protected byte[] b = new byte[0];
    protected int c;
    protected List<RecommendAppInfoEx> d = new ArrayList();
    protected List<SimpleAppModel> e = new ArrayList();

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        if (jceStruct2 instanceof GuessYouLikeResponse) {
            GuessYouLikeResponse guessYouLikeResponse = (GuessYouLikeResponse) jceStruct2;
            this.b = guessYouLikeResponse.c;
            int i2 = guessYouLikeResponse.f1383a;
            if (i2 != 0) {
                XLog.i("Jie", "GuessYouLike success>>seq=" + i + " ret=" + i2 + " response=" + jceStruct2);
            }
            a(guessYouLikeResponse.b);
            notifyDataChangedInMainThread(new k(this, i, i2));
        }
    }

    private void a(List<RecommendAppInfoEx> list) {
        this.d.clear();
        this.e.clear();
        if (list != null && !list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (RecommendAppInfoEx next : list) {
                if (ApkResourceManager.getInstance().getInstalledApkInfo(next.f1455a != null ? next.f1455a.f : Constants.STR_EMPTY) != null) {
                    arrayList.add(next);
                    arrayList2.add(a(next.f1455a, next.d));
                } else {
                    this.d.add(next);
                    this.e.add(a(next.f1455a, next.d));
                }
            }
            this.d.addAll(arrayList);
            this.e.addAll(arrayList2);
        }
    }

    public static SimpleAppModel a(SimpleAppInfo simpleAppInfo, byte[] bArr) {
        if (simpleAppInfo == null) {
            return null;
        }
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        if (simpleAppInfo == null) {
            return simpleAppModel;
        }
        simpleAppModel.f938a = simpleAppInfo.f1495a;
        simpleAppModel.b = simpleAppInfo.p;
        simpleAppModel.d = simpleAppInfo.b;
        simpleAppModel.c = simpleAppInfo.f;
        simpleAppModel.g = simpleAppInfo.h;
        simpleAppModel.f = simpleAppInfo.g;
        simpleAppModel.h = simpleAppInfo.o;
        simpleAppModel.e = simpleAppInfo.c;
        simpleAppModel.k = simpleAppInfo.d;
        simpleAppModel.i = simpleAppInfo.e;
        simpleAppModel.j = k.a((byte) 1, simpleAppInfo.q);
        simpleAppModel.p = simpleAppInfo.i;
        simpleAppModel.r = simpleAppInfo.j;
        if (simpleAppInfo.k != null) {
            simpleAppModel.q = simpleAppInfo.k.b;
        }
        if (simpleAppInfo.l != null) {
            simpleAppModel.s = simpleAppInfo.l.b;
        }
        simpleAppModel.B = simpleAppInfo.m;
        simpleAppModel.P = simpleAppInfo.r;
        simpleAppModel.ac = simpleAppInfo.s;
        simpleAppModel.W = simpleAppInfo.n;
        simpleAppModel.ad = simpleAppInfo.t;
        simpleAppModel.R = (long) simpleAppInfo.v;
        simpleAppModel.y = bArr;
        return simpleAppModel;
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.i("Jie", "GuessYouLike fail>>seq=" + i + " errorCode=" + i2);
        notifyDataChangedInMainThread(new l(this, i, i2));
    }

    private void a(byte[] bArr) {
        if (this.f3151a > 0) {
            cancel(this.f3151a);
        }
        GuessYouLikeRequest guessYouLikeRequest = new GuessYouLikeRequest();
        if (bArr == null) {
            bArr = new byte[0];
        }
        guessYouLikeRequest.b = c();
        guessYouLikeRequest.f1382a = bArr;
        XLog.i("Jie", "request guess you like");
        this.f3151a = send(guessYouLikeRequest);
    }

    public void a() {
        a(this.b);
    }

    private int c() {
        if (this.c != 0) {
            return this.c;
        }
        int i = t.c;
        float f = t.d;
        this.c = (int) (((double) (((float) by.b(AstApp.i().getApplicationContext(), (float) i)) - 150.0f)) * 0.0119d);
        if (f == 0.0f) {
            this.c = 0;
        }
        return this.c;
    }

    public void b() {
        cancel(this.f3151a);
    }
}
