package com.tencent.nucleus.socialcontact.comment;

import android.app.Activity;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.manager.ak;
import java.util.Date;

/* compiled from: ProGuard */
class bn extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopViewDialog f3120a;

    bn(PopViewDialog popViewDialog) {
        this.f3120a = popViewDialog;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareAppModel, boolean):void
     arg types: [android.app.Activity, com.tencent.pangu.model.ShareAppModel, int]
     candidates:
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareAppModel, boolean):void */
    public void onTMAClick(View view) {
        String str;
        int i;
        switch (view.getId()) {
            case R.id.cancel /*2131165503*/:
                this.f3120a.f();
                if (this.f3120a.z.j()) {
                    l.a(new STInfoV2(STConst.ST_PAGE_COMMENT_DIALOG_LOGIN, "03_001", STConst.ST_PAGE_APP_DETAIL_COMMENT, STConst.ST_DEFAULT_SLOT, 200));
                } else {
                    l.a(new STInfoV2(STConst.ST_PAGE_COMMENT_DIALOG_NOT_LOGIN, "03_001", STConst.ST_PAGE_APP_DETAIL_COMMENT, STConst.ST_DEFAULT_SLOT, 200));
                }
                this.f3120a.dismiss();
                return;
            case R.id.submit /*2131165869*/:
                this.f3120a.f();
                int round = Math.round(this.f3120a.c.getRating());
                if (round <= 0) {
                    this.f3120a.f.setText((int) R.string.comment_pop_star_text);
                    this.f3120a.f.setTextColor(-65536);
                    return;
                } else if (!this.f3120a.K || round != this.f3120a.F || !this.f3120a.c().getText().toString().equals(this.f3120a.H)) {
                    String obj = this.f3120a.c().getText().toString();
                    if (!TextUtils.isEmpty(obj)) {
                        if (obj.length() > 100) {
                            this.f3120a.g.setVisibility(0);
                            this.f3120a.g.setText((int) R.string.comment_expendcount_error);
                            return;
                        }
                        String str2 = null;
                        CharSequence text = this.f3120a.g.getText();
                        if (text != null) {
                            str2 = text.toString();
                        }
                        String string = this.f3120a.b.getString(R.string.comment_expendcount_error);
                        if (this.f3120a.g.getVisibility() == 0 && !TextUtils.isEmpty(str2) && str2.equals(string)) {
                            this.f3120a.g.setVisibility(8);
                        }
                    }
                    String str3 = "03_002";
                    if (!this.f3120a.z.j()) {
                        this.f3120a.j();
                        l.a(new STInfoV2(STConst.ST_PAGE_COMMENT_DIALOG_NOT_LOGIN, str3, STConst.ST_PAGE_APP_DETAIL_COMMENT, STConst.ST_DEFAULT_SLOT, 200));
                        return;
                    }
                    if (TextUtils.isEmpty(this.f3120a.c().getText().toString())) {
                        this.f3120a.l.b(Constants.STR_EMPTY);
                        this.f3120a.c().setText(Constants.STR_EMPTY);
                    } else {
                        this.f3120a.l.b(this.f3120a.c().getText().toString());
                    }
                    this.f3120a.l.a(new Date().getTime() / 1000);
                    this.f3120a.l.a(this.f3120a.z.q());
                    this.f3120a.l.a(round);
                    if (!ak.a().c(this.f3120a.I) || !ak.a().d(this.f3120a.I)) {
                        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(this.f3120a.I);
                        if (localApkInfo != null) {
                            int i2 = localApkInfo.mVersionCode;
                            str = localApkInfo.mVersionName;
                            i = i2;
                        } else {
                            return;
                        }
                    } else if (ak.a().a(this.f3120a.I) != null) {
                        int s = this.f3120a.s;
                        str = this.f3120a.t;
                        i = s;
                    } else {
                        return;
                    }
                    if (i > 0) {
                        this.f3120a.l.b(i);
                        this.f3120a.l.c(com.tencent.nucleus.socialcontact.login.l.f().f3165a);
                        if (this.f3120a.k != null) {
                            this.f3120a.k.a(this.f3120a.d.getText().toString(), round, i, this.f3120a.A || this.f3120a.B);
                        }
                        if (this.f3120a.s != this.f3120a.G) {
                            if (i == this.f3120a.s) {
                                this.f3120a.m.a(this.f3120a.l, this.f3120a.p, this.f3120a.q, str, this.f3120a.r, this.f3120a.I);
                            } else {
                                this.f3120a.m.a(this.f3120a.l, this.f3120a.p, -1, str, this.f3120a.r, this.f3120a.I);
                            }
                        }
                        if ((this.f3120a.A || this.f3120a.B) && this.f3120a.C != null) {
                            if (this.f3120a.A) {
                                this.f3120a.C.b = this.f3120a.c().getText().toString();
                                str3 = "04_001";
                                this.f3120a.D.a(false);
                                this.f3120a.D.a((Activity) this.f3120a.b, this.f3120a.C, false);
                            } else if (this.f3120a.B && this.f3120a.z.v()) {
                                this.f3120a.D.a(false);
                                this.f3120a.C.m = this.f3120a.c().getText().toString();
                                str3 = "04_002";
                                this.f3120a.D.a(this.f3120a.b, this.f3120a.C, true, true);
                            }
                        }
                        l.a(new STInfoV2(STConst.ST_PAGE_COMMENT_DIALOG_LOGIN, str3, STConst.ST_PAGE_APP_DETAIL_COMMENT, STConst.ST_DEFAULT_SLOT, 200));
                        this.f3120a.dismiss();
                        return;
                    }
                    return;
                } else {
                    this.f3120a.dismiss();
                    return;
                }
            default:
                view.setSelected(true);
                String str4 = ((Object) ((TextView) view).getText()) + "。";
                int selectionEnd = this.f3120a.c().getSelectionEnd();
                String obj2 = this.f3120a.c().getText().subSequence(0, selectionEnd).toString();
                if (obj2.length() < 100) {
                    this.f3120a.c().setText(obj2 + str4 + this.f3120a.c().getText().subSequence(selectionEnd, this.f3120a.c().getText().length()).toString());
                    int length = (obj2 + str4).length();
                    Editable text2 = this.f3120a.d.getText();
                    if (length >= text2.length()) {
                        length = text2.length();
                    }
                    Selection.setSelection(text2, length);
                    return;
                }
                return;
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 c = this.f3120a.i();
        if (c != null) {
            if (this.clickViewId == R.id.cancel) {
                c.slotId = a.a(this.f3120a.h(), "003");
            } else if (this.clickViewId == R.id.submit) {
                c.slotId = a.a(this.f3120a.h(), "004");
            }
            c.actionId = 200;
        }
        return c;
    }
}
