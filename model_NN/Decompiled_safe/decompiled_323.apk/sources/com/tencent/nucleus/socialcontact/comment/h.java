package com.tencent.nucleus.socialcontact.comment;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.protocol.jce.CommentAppResponse;
import com.tencent.assistant.protocol.jce.CommentDetail;

/* compiled from: ProGuard */
class h implements CallbackHelper.Caller<j> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3125a;
    final /* synthetic */ CommentAppResponse b;
    final /* synthetic */ CommentDetail c;
    final /* synthetic */ g d;

    h(g gVar, int i, CommentAppResponse commentAppResponse, CommentDetail commentDetail) {
        this.d = gVar;
        this.f3125a = i;
        this.b = commentAppResponse;
        this.c = commentDetail;
    }

    /* renamed from: a */
    public void call(j jVar) {
        jVar.a(this.f3125a, this.b.f1201a, this.c, this.b.c);
    }
}
