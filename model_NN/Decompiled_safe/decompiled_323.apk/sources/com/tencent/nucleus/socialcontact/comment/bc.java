package com.tencent.nucleus.socialcontact.comment;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.ModifyAppCommentRequest;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class bc extends BaseEngine<j> {
    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        ModifyAppCommentRequest modifyAppCommentRequest = (ModifyAppCommentRequest) jceStruct;
        XLog.d("comment", "ModifyAppCommentEngine.onRequestSuccessed, ModifyAppCommentRequest=" + modifyAppCommentRequest.toString());
        notifyDataChangedInMainThread(new bd(this, i, modifyAppCommentRequest));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.d("comment", "ModifyAppCommentEngine.onRequestFailed, errorCode=" + i2);
        notifyDataChangedInMainThread(new be(this, i, i2, (ModifyAppCommentRequest) jceStruct));
    }
}
