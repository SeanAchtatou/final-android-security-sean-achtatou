package com.tencent.nucleus.socialcontact.login;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.login.model.MoblieQIdentityInfo;
import com.tencent.assistant.plugin.a.b;
import com.tencent.assistant.protocol.jce.Ticket;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.appbackup.p;
import java.util.HashMap;

/* compiled from: ProGuard */
public class j {

    /* renamed from: a  reason: collision with root package name */
    private static j f3162a;
    private e b;
    private e c;
    private Object d = new Object();
    private f e;

    public static synchronized j a() {
        j jVar;
        synchronized (j.class) {
            if (f3162a == null) {
                f3162a = new j();
                f3162a.h();
            }
            jVar = f3162a;
        }
        return jVar;
    }

    public j() {
        String a2 = l.a();
        String d2 = l.d();
        XLog.d("LoginProxy", "create--userId = " + a2 + " sig = " + d2);
        if (!TextUtils.isEmpty(a2)) {
            if (!TextUtils.isEmpty(d2)) {
                this.e = c(AppConst.IdentityType.WX);
            } else {
                this.e = c(AppConst.IdentityType.MOBILEQ);
            }
        }
        p.a().b();
    }

    public void b() {
    }

    public void a(AppConst.IdentityType identityType) {
        this.e = c(identityType);
    }

    public e c() {
        e eVar;
        synchronized (this.d) {
            if (this.b == null) {
                this.b = n.b();
            }
            eVar = this.b;
        }
        return eVar;
    }

    public AppConst.IdentityType d() {
        return c().getType();
    }

    public synchronized void a(e eVar) {
        boolean c2 = c(eVar);
        synchronized (this.d) {
            this.c = null;
            if (eVar == null) {
                eVar = n.b();
            }
            this.b = eVar;
        }
        y();
        if (c2) {
            a.a().b();
        }
    }

    public void b(e eVar) {
        synchronized (this.d) {
            this.c = this.b;
            if (eVar == null) {
                eVar = n.b();
            }
            this.b = eVar;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:95:?, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean c(com.tencent.nucleus.socialcontact.login.e r7) {
        /*
            r6 = this;
            r2 = 0
            r1 = 1
            java.lang.Object r3 = r6.d     // Catch:{ Exception -> 0x006d }
            monitor-enter(r3)     // Catch:{ Exception -> 0x006d }
            java.lang.String r0 = "LoginProxy"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x006a }
            r4.<init>()     // Catch:{ all -> 0x006a }
            java.lang.String r5 = "id = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x006a }
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ all -> 0x006a }
            java.lang.String r5 = " mId = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x006a }
            com.tencent.nucleus.socialcontact.login.e r5 = r6.b     // Catch:{ all -> 0x006a }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x006a }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x006a }
            com.tencent.assistant.utils.XLog.d(r0, r4)     // Catch:{ all -> 0x006a }
            java.lang.String r0 = "LoginProxy"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x006a }
            r4.<init>()     // Catch:{ all -> 0x006a }
            java.lang.String r5 = "id.type = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x006a }
            com.tencent.assistant.AppConst$IdentityType r5 = r7.getType()     // Catch:{ all -> 0x006a }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x006a }
            java.lang.String r5 = " mid.type = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x006a }
            com.tencent.nucleus.socialcontact.login.e r5 = r6.b     // Catch:{ all -> 0x006a }
            com.tencent.assistant.AppConst$IdentityType r5 = r5.getType()     // Catch:{ all -> 0x006a }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x006a }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x006a }
            com.tencent.assistant.utils.XLog.d(r0, r4)     // Catch:{ all -> 0x006a }
            monitor-exit(r3)     // Catch:{ all -> 0x006a }
        L_0x0056:
            if (r7 == 0) goto L_0x0068
            com.tencent.assistant.AppConst$IdentityType r0 = r7.getType()
            com.tencent.assistant.AppConst$IdentityType r3 = com.tencent.assistant.AppConst.IdentityType.NONE
            if (r0 == r3) goto L_0x0068
            com.tencent.assistant.AppConst$IdentityType r0 = r7.getType()
            com.tencent.assistant.AppConst$IdentityType r3 = com.tencent.assistant.AppConst.IdentityType.WXCODE
            if (r0 != r3) goto L_0x0072
        L_0x0068:
            r0 = r2
        L_0x0069:
            return r0
        L_0x006a:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x006a }
            throw r0     // Catch:{ Exception -> 0x006d }
        L_0x006d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0056
        L_0x0072:
            java.lang.Object r3 = r6.d
            monitor-enter(r3)
            com.tencent.nucleus.socialcontact.login.e r0 = r6.b     // Catch:{ all -> 0x0122 }
            if (r0 == 0) goto L_0x0083
            com.tencent.nucleus.socialcontact.login.e r0 = r6.b     // Catch:{ all -> 0x0122 }
            com.tencent.assistant.AppConst$IdentityType r0 = r0.getType()     // Catch:{ all -> 0x0122 }
            com.tencent.assistant.AppConst$IdentityType r4 = com.tencent.assistant.AppConst.IdentityType.NONE     // Catch:{ all -> 0x0122 }
            if (r0 != r4) goto L_0x0091
        L_0x0083:
            com.tencent.assistant.AppConst$IdentityType r0 = r7.getType()     // Catch:{ all -> 0x0122 }
            com.tencent.assistant.AppConst$IdentityType r4 = com.tencent.assistant.AppConst.IdentityType.WXCODE     // Catch:{ all -> 0x0122 }
            if (r0 != r4) goto L_0x008e
            monitor-exit(r3)     // Catch:{ all -> 0x0122 }
            r0 = r2
            goto L_0x0069
        L_0x008e:
            monitor-exit(r3)     // Catch:{ all -> 0x0122 }
            r0 = r1
            goto L_0x0069
        L_0x0091:
            com.tencent.nucleus.socialcontact.login.e r0 = r6.b     // Catch:{ all -> 0x0122 }
            com.tencent.assistant.AppConst$IdentityType r0 = r0.getType()     // Catch:{ all -> 0x0122 }
            com.tencent.assistant.AppConst$IdentityType r4 = com.tencent.assistant.AppConst.IdentityType.WXCODE     // Catch:{ all -> 0x0122 }
            if (r0 != r4) goto L_0x00bc
            com.tencent.assistant.AppConst$IdentityType r0 = r7.getType()     // Catch:{ all -> 0x0122 }
            com.tencent.assistant.AppConst$IdentityType r4 = com.tencent.assistant.AppConst.IdentityType.WX     // Catch:{ all -> 0x0122 }
            if (r0 != r4) goto L_0x00b1
            com.tencent.nucleus.socialcontact.login.e r0 = r6.b     // Catch:{ all -> 0x0122 }
            com.tencent.nucleus.socialcontact.login.q r0 = (com.tencent.nucleus.socialcontact.login.q) r0     // Catch:{ all -> 0x0122 }
            boolean r0 = r0.d     // Catch:{ all -> 0x0122 }
            if (r0 == 0) goto L_0x00ae
            monitor-exit(r3)     // Catch:{ all -> 0x0122 }
            r0 = r1
            goto L_0x0069
        L_0x00ae:
            monitor-exit(r3)     // Catch:{ all -> 0x0122 }
            r0 = r2
            goto L_0x0069
        L_0x00b1:
            com.tencent.assistant.AppConst$IdentityType r0 = r7.getType()     // Catch:{ all -> 0x0122 }
            com.tencent.assistant.AppConst$IdentityType r4 = com.tencent.assistant.AppConst.IdentityType.MOBILEQ     // Catch:{ all -> 0x0122 }
            if (r0 != r4) goto L_0x011e
            monitor-exit(r3)     // Catch:{ all -> 0x0122 }
            r0 = r1
            goto L_0x0069
        L_0x00bc:
            com.tencent.nucleus.socialcontact.login.e r0 = r6.b     // Catch:{ all -> 0x0122 }
            com.tencent.assistant.AppConst$IdentityType r0 = r0.getType()     // Catch:{ all -> 0x0122 }
            com.tencent.assistant.AppConst$IdentityType r4 = com.tencent.assistant.AppConst.IdentityType.WX     // Catch:{ all -> 0x0122 }
            if (r0 != r4) goto L_0x00ec
            com.tencent.assistant.AppConst$IdentityType r0 = r7.getType()     // Catch:{ all -> 0x0122 }
            com.tencent.assistant.AppConst$IdentityType r4 = com.tencent.assistant.AppConst.IdentityType.WX     // Catch:{ all -> 0x0122 }
            if (r0 != r4) goto L_0x00e0
            com.tencent.nucleus.socialcontact.login.e r0 = r6.b     // Catch:{ all -> 0x0122 }
            com.tencent.nucleus.socialcontact.login.r r0 = (com.tencent.nucleus.socialcontact.login.r) r0     // Catch:{ all -> 0x0122 }
            com.tencent.nucleus.socialcontact.login.r r7 = (com.tencent.nucleus.socialcontact.login.r) r7     // Catch:{ all -> 0x0122 }
            boolean r0 = r0.equals(r7)     // Catch:{ all -> 0x0122 }
            if (r0 == 0) goto L_0x00dd
            monitor-exit(r3)     // Catch:{ all -> 0x0122 }
            r0 = r2
            goto L_0x0069
        L_0x00dd:
            monitor-exit(r3)     // Catch:{ all -> 0x0122 }
            r0 = r1
            goto L_0x0069
        L_0x00e0:
            com.tencent.assistant.AppConst$IdentityType r0 = r7.getType()     // Catch:{ all -> 0x0122 }
            com.tencent.assistant.AppConst$IdentityType r4 = com.tencent.assistant.AppConst.IdentityType.MOBILEQ     // Catch:{ all -> 0x0122 }
            if (r0 != r4) goto L_0x011e
            monitor-exit(r3)     // Catch:{ all -> 0x0122 }
            r0 = r1
            goto L_0x0069
        L_0x00ec:
            com.tencent.nucleus.socialcontact.login.e r0 = r6.b     // Catch:{ all -> 0x0122 }
            com.tencent.assistant.AppConst$IdentityType r0 = r0.getType()     // Catch:{ all -> 0x0122 }
            com.tencent.assistant.AppConst$IdentityType r4 = com.tencent.assistant.AppConst.IdentityType.MOBILEQ     // Catch:{ all -> 0x0122 }
            if (r0 != r4) goto L_0x011e
            com.tencent.assistant.AppConst$IdentityType r0 = r7.getType()     // Catch:{ all -> 0x0122 }
            com.tencent.assistant.AppConst$IdentityType r4 = com.tencent.assistant.AppConst.IdentityType.WX     // Catch:{ all -> 0x0122 }
            if (r0 != r4) goto L_0x0102
            monitor-exit(r3)     // Catch:{ all -> 0x0122 }
            r0 = r1
            goto L_0x0069
        L_0x0102:
            com.tencent.assistant.AppConst$IdentityType r0 = r7.getType()     // Catch:{ all -> 0x0122 }
            com.tencent.assistant.AppConst$IdentityType r4 = com.tencent.assistant.AppConst.IdentityType.MOBILEQ     // Catch:{ all -> 0x0122 }
            if (r0 != r4) goto L_0x011e
            com.tencent.nucleus.socialcontact.login.e r0 = r6.b     // Catch:{ all -> 0x0122 }
            com.tencent.assistant.login.model.MoblieQIdentityInfo r0 = (com.tencent.assistant.login.model.MoblieQIdentityInfo) r0     // Catch:{ all -> 0x0122 }
            com.tencent.assistant.login.model.MoblieQIdentityInfo r7 = (com.tencent.assistant.login.model.MoblieQIdentityInfo) r7     // Catch:{ all -> 0x0122 }
            boolean r0 = r0.equals(r7)     // Catch:{ all -> 0x0122 }
            if (r0 == 0) goto L_0x011a
            monitor-exit(r3)     // Catch:{ all -> 0x0122 }
            r0 = r2
            goto L_0x0069
        L_0x011a:
            monitor-exit(r3)     // Catch:{ all -> 0x0122 }
            r0 = r1
            goto L_0x0069
        L_0x011e:
            monitor-exit(r3)     // Catch:{ all -> 0x0122 }
            r0 = r2
            goto L_0x0069
        L_0x0122:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0122 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.nucleus.socialcontact.login.j.c(com.tencent.nucleus.socialcontact.login.e):boolean");
    }

    public void e() {
        synchronized (this.d) {
            this.b = n.b();
            z();
        }
    }

    public void f() {
        synchronized (this.d) {
            if (this.c == null) {
                this.b = n.b();
            } else {
                this.b = this.c;
                this.c = null;
            }
        }
    }

    public Ticket g() {
        Ticket ticket;
        synchronized (this.d) {
            if (this.b != null) {
                ticket = this.b.getTicket();
            } else {
                n b2 = n.b();
                this.b = b2;
                ticket = b2.getTicket();
            }
        }
        return ticket;
    }

    public void h() {
        XLog.d("LoginProxy", "loadIdentityCache--mLoginEngine = " + this.e);
        if (this.e != null) {
            this.e.g();
        }
    }

    private boolean y() {
        synchronized (this.d) {
            if (this.b.getType() == AppConst.IdentityType.WX) {
                r rVar = (r) this.b;
                l.a(rVar.b(), rVar.c(), rVar.d());
            } else if (this.b.getType() == AppConst.IdentityType.MOBILEQ) {
                MoblieQIdentityInfo moblieQIdentityInfo = (MoblieQIdentityInfo) this.b;
                l.a(moblieQIdentityInfo.getAccount(), moblieQIdentityInfo.getUin(), null, null);
            }
        }
        return true;
    }

    private boolean z() {
        if (this.e != null) {
            this.e.f();
        }
        l.a(null, null, null);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.login.j.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.tencent.nucleus.socialcontact.login.j.a(com.tencent.assistant.AppConst$IdentityType, android.os.Bundle):void
      com.tencent.nucleus.socialcontact.login.j.a(boolean, java.lang.String):void */
    public void a(Ticket ticket) {
        boolean z;
        e eVar;
        int i;
        int i2 = 2000;
        if (ticket != null && ticket.f1584a == AppConst.IdentityType.WX.ordinal() && ticket.b != null) {
            e a2 = r.a(ticket.b);
            synchronized (this.d) {
                if (this.b.getType() == AppConst.IdentityType.WXCODE) {
                    z = !((q) this.b).d;
                } else {
                    z = false;
                }
            }
            if (a2 != null) {
                a(a2);
                if (AstApp.m() != null) {
                    i = AstApp.m().f();
                    i2 = AstApp.m().m();
                } else {
                    i = 2000;
                }
                l.a(new STInfoV2(i, STConst.ST_DEFAULT_SLOT, i2, STConst.ST_DEFAULT_SLOT, STConstAction.ACTION_HIT_LOGIN_SUCCESS_WX));
                eVar = a2;
            } else {
                e b2 = n.b();
                a(b2);
                eVar = b2;
            }
            if (z) {
                Message obtainMessage = AstApp.i().j().obtainMessage(eVar != null ? EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS : EventDispatcherEnum.UI_EVENT_LOGIN_FAIL);
                obtainMessage.arg1 = AppConst.LoginEgnineType.ENGINE_WX.ordinal();
                if (this.e != null) {
                    obtainMessage.obj = this.e.a();
                }
                AstApp.i().j().sendMessage(obtainMessage);
                if (eVar != null) {
                    a(true, "success");
                } else {
                    a(false, "fail");
                }
            }
        }
    }

    private void a(boolean z, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", str);
        hashMap.put("B2", Global.getPhoneGuidAndGen());
        hashMap.put("B3", Global.getQUAForBeacon());
        hashMap.put("B4", t.g());
        a.a("Login_WX", z, -1, -1, hashMap, true);
        XLog.d("beacon", "beacon report >> event: Login_WX, params : " + hashMap.toString());
    }

    private void b(AppConst.IdentityType identityType, Bundle bundle) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", identityType.name());
        hashMap.put("B2", bundle != null ? bundle.getInt(AppConst.KEY_FROM_TYPE) + Constants.STR_EMPTY : "0");
        hashMap.put("B3", a(bundle));
        hashMap.put("B4", Global.getPhoneGuidAndGen());
        hashMap.put("B5", Global.getQUAForBeacon());
        hashMap.put("B6", t.g());
        a.a("LoginTriggerAll", true, -1, -1, hashMap, true);
        XLog.d("beacon", "beacon report >> event : LoginTriggerAll. paras : " + hashMap.toString());
    }

    private String a(Bundle bundle) {
        if (bundle == null) {
            return "默认";
        }
        switch (bundle.getInt(AppConst.KEY_FROM_TYPE)) {
            case 0:
                return "默认";
            case 1:
                return "分享";
            case 2:
                return "收藏";
            case 3:
                return "抢号";
            case 4:
                return "内测";
            case 5:
                return "手Q透传票据";
            case 6:
                return "应用吧";
            case 7:
                return "无缝连接";
            case 8:
                return "账号栏";
            case 9:
                return "猜你喜欢";
            case 10:
                return "应用备份";
            case 11:
                return "应用详情页发话题";
            case 12:
                return "应用详情页发话题-带提示";
            case 13:
                return "好友榜";
            case 14:
                return "QQ阅读插件";
            case 15:
                return "隐私设置";
            case 16:
                return "引导页";
            case 17:
                return "免费wifi";
            case 18:
                return "第一次启动后恢复历史应用";
            default:
                return "默认";
        }
    }

    public Bundle i() {
        if (this.e != null) {
            return this.e.a();
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.login.j.a(com.tencent.assistant.AppConst$IdentityType, android.os.Bundle, boolean):void
     arg types: [com.tencent.assistant.AppConst$IdentityType, android.os.Bundle, int]
     candidates:
      com.tencent.nucleus.socialcontact.login.j.a(java.lang.String, java.lang.String, java.lang.String):void
      com.tencent.nucleus.socialcontact.login.j.a(com.tencent.assistant.AppConst$IdentityType, android.os.Bundle, boolean):void */
    public void a(AppConst.IdentityType identityType, Bundle bundle) {
        a(identityType, bundle, true);
    }

    public void a(AppConst.IdentityType identityType, Bundle bundle, boolean z) {
        synchronized (this.d) {
            this.c = this.b;
        }
        this.e = c(identityType);
        if (this.e != null) {
            this.e.a(bundle);
            this.e.d();
        }
        if (z) {
            b(identityType, bundle);
        }
    }

    public void b(AppConst.IdentityType identityType) {
        a(identityType, (Bundle) null);
    }

    private f c(AppConst.IdentityType identityType) {
        return identityType == AppConst.IdentityType.WX ? s.h() : h.h();
    }

    public boolean j() {
        boolean z;
        synchronized (this.d) {
            z = (this.b == null || this.b.getType() == AppConst.IdentityType.NONE || this.b.getType() == AppConst.IdentityType.WXCODE) ? false : true;
        }
        return z;
    }

    public boolean k() {
        boolean z;
        synchronized (this.d) {
            z = this.b != null && this.b.getType() == AppConst.IdentityType.MOBILEQ;
        }
        return z;
    }

    public boolean l() {
        boolean z;
        synchronized (this.d) {
            z = this.b != null && this.b.getType() == AppConst.IdentityType.WX;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String m() {
        /*
            r3 = this;
            java.lang.Object r1 = r3.d
            monitor-enter(r1)
            com.tencent.nucleus.socialcontact.login.e r0 = r3.b     // Catch:{ all -> 0x001e }
            if (r0 == 0) goto L_0x001b
            com.tencent.nucleus.socialcontact.login.e r0 = r3.b     // Catch:{ all -> 0x001e }
            com.tencent.assistant.AppConst$IdentityType r0 = r0.getType()     // Catch:{ all -> 0x001e }
            com.tencent.assistant.AppConst$IdentityType r2 = com.tencent.assistant.AppConst.IdentityType.MOBILEQ     // Catch:{ all -> 0x001e }
            if (r0 != r2) goto L_0x001b
            com.tencent.nucleus.socialcontact.login.e r0 = r3.b     // Catch:{ all -> 0x001e }
            com.tencent.assistant.login.model.MoblieQIdentityInfo r0 = (com.tencent.assistant.login.model.MoblieQIdentityInfo) r0     // Catch:{ all -> 0x001e }
            java.lang.String r0 = r0.getSKey()     // Catch:{ all -> 0x001e }
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
        L_0x001a:
            return r0
        L_0x001b:
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
            r0 = 0
            goto L_0x001a
        L_0x001e:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.nucleus.socialcontact.login.j.m():java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001c, code lost:
        return com.tencent.connect.common.Constants.STR_EMPTY;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String n() {
        /*
            r3 = this;
            java.lang.Object r1 = r3.d
            monitor-enter(r1)
            com.tencent.nucleus.socialcontact.login.e r0 = r3.b     // Catch:{ all -> 0x001f }
            if (r0 == 0) goto L_0x001b
            com.tencent.nucleus.socialcontact.login.e r0 = r3.b     // Catch:{ all -> 0x001f }
            com.tencent.assistant.AppConst$IdentityType r0 = r0.getType()     // Catch:{ all -> 0x001f }
            com.tencent.assistant.AppConst$IdentityType r2 = com.tencent.assistant.AppConst.IdentityType.MOBILEQ     // Catch:{ all -> 0x001f }
            if (r0 != r2) goto L_0x001b
            com.tencent.nucleus.socialcontact.login.e r0 = r3.b     // Catch:{ all -> 0x001f }
            com.tencent.assistant.login.model.MoblieQIdentityInfo r0 = (com.tencent.assistant.login.model.MoblieQIdentityInfo) r0     // Catch:{ all -> 0x001f }
            java.lang.String r0 = r0.getSid()     // Catch:{ all -> 0x001f }
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
        L_0x001a:
            return r0
        L_0x001b:
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
            java.lang.String r0 = ""
            goto L_0x001a
        L_0x001f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.nucleus.socialcontact.login.j.n():java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001c, code lost:
        return com.tencent.connect.common.Constants.STR_EMPTY;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String o() {
        /*
            r3 = this;
            java.lang.Object r1 = r3.d
            monitor-enter(r1)
            com.tencent.nucleus.socialcontact.login.e r0 = r3.b     // Catch:{ all -> 0x001f }
            if (r0 == 0) goto L_0x001b
            com.tencent.nucleus.socialcontact.login.e r0 = r3.b     // Catch:{ all -> 0x001f }
            com.tencent.assistant.AppConst$IdentityType r0 = r0.getType()     // Catch:{ all -> 0x001f }
            com.tencent.assistant.AppConst$IdentityType r2 = com.tencent.assistant.AppConst.IdentityType.MOBILEQ     // Catch:{ all -> 0x001f }
            if (r0 != r2) goto L_0x001b
            com.tencent.nucleus.socialcontact.login.e r0 = r3.b     // Catch:{ all -> 0x001f }
            com.tencent.assistant.login.model.MoblieQIdentityInfo r0 = (com.tencent.assistant.login.model.MoblieQIdentityInfo) r0     // Catch:{ all -> 0x001f }
            java.lang.String r0 = r0.getVkey()     // Catch:{ all -> 0x001f }
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
        L_0x001a:
            return r0
        L_0x001b:
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
            java.lang.String r0 = ""
            goto L_0x001a
        L_0x001f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.nucleus.socialcontact.login.j.o():java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return 0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long p() {
        /*
            r3 = this;
            java.lang.Object r2 = r3.d
            monitor-enter(r2)
            com.tencent.nucleus.socialcontact.login.e r0 = r3.b     // Catch:{ all -> 0x001f }
            if (r0 == 0) goto L_0x001b
            com.tencent.nucleus.socialcontact.login.e r0 = r3.b     // Catch:{ all -> 0x001f }
            com.tencent.assistant.AppConst$IdentityType r0 = r0.getType()     // Catch:{ all -> 0x001f }
            com.tencent.assistant.AppConst$IdentityType r1 = com.tencent.assistant.AppConst.IdentityType.MOBILEQ     // Catch:{ all -> 0x001f }
            if (r0 != r1) goto L_0x001b
            com.tencent.nucleus.socialcontact.login.e r0 = r3.b     // Catch:{ all -> 0x001f }
            com.tencent.assistant.login.model.MoblieQIdentityInfo r0 = (com.tencent.assistant.login.model.MoblieQIdentityInfo) r0     // Catch:{ all -> 0x001f }
            long r0 = r0.getUin()     // Catch:{ all -> 0x001f }
            monitor-exit(r2)     // Catch:{ all -> 0x001f }
        L_0x001a:
            return r0
        L_0x001b:
            monitor-exit(r2)     // Catch:{ all -> 0x001f }
            r0 = 0
            goto L_0x001a
        L_0x001f:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x001f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.nucleus.socialcontact.login.j.p():long");
    }

    public String q() {
        m f = l.f();
        if (f == null || TextUtils.isEmpty(f.b)) {
            return null;
        }
        return f.b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String r() {
        /*
            r3 = this;
            java.lang.Object r1 = r3.d
            monitor-enter(r1)
            com.tencent.nucleus.socialcontact.login.e r0 = r3.b     // Catch:{ all -> 0x001e }
            if (r0 == 0) goto L_0x001b
            com.tencent.nucleus.socialcontact.login.e r0 = r3.b     // Catch:{ all -> 0x001e }
            com.tencent.assistant.AppConst$IdentityType r0 = r0.getType()     // Catch:{ all -> 0x001e }
            com.tencent.assistant.AppConst$IdentityType r2 = com.tencent.assistant.AppConst.IdentityType.WX     // Catch:{ all -> 0x001e }
            if (r0 != r2) goto L_0x001b
            com.tencent.nucleus.socialcontact.login.e r0 = r3.b     // Catch:{ all -> 0x001e }
            com.tencent.nucleus.socialcontact.login.r r0 = (com.tencent.nucleus.socialcontact.login.r) r0     // Catch:{ all -> 0x001e }
            java.lang.String r0 = r0.c()     // Catch:{ all -> 0x001e }
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
        L_0x001a:
            return r0
        L_0x001b:
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
            r0 = 0
            goto L_0x001a
        L_0x001e:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.nucleus.socialcontact.login.j.r():java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String s() {
        /*
            r3 = this;
            java.lang.Object r1 = r3.d
            monitor-enter(r1)
            com.tencent.nucleus.socialcontact.login.e r0 = r3.b     // Catch:{ all -> 0x001e }
            if (r0 == 0) goto L_0x001b
            com.tencent.nucleus.socialcontact.login.e r0 = r3.b     // Catch:{ all -> 0x001e }
            com.tencent.assistant.AppConst$IdentityType r0 = r0.getType()     // Catch:{ all -> 0x001e }
            com.tencent.assistant.AppConst$IdentityType r2 = com.tencent.assistant.AppConst.IdentityType.WX     // Catch:{ all -> 0x001e }
            if (r0 != r2) goto L_0x001b
            com.tencent.nucleus.socialcontact.login.e r0 = r3.b     // Catch:{ all -> 0x001e }
            com.tencent.nucleus.socialcontact.login.r r0 = (com.tencent.nucleus.socialcontact.login.r) r0     // Catch:{ all -> 0x001e }
            java.lang.String r0 = r0.b()     // Catch:{ all -> 0x001e }
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
        L_0x001a:
            return r0
        L_0x001b:
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
            r0 = 0
            goto L_0x001a
        L_0x001e:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.nucleus.socialcontact.login.j.s():java.lang.String");
    }

    public boolean t() {
        return ApkResourceManager.getInstance().getInstalledApkInfo("com.tencent.mobileqq") != null;
    }

    public boolean u() {
        boolean t = t();
        if (t) {
            try {
                PackageInfo packageInfo = AstApp.i().getPackageManager().getPackageInfo("com.tencent.mobileqq", 0);
                if (packageInfo != null && packageInfo.versionCode < 66) {
                    return false;
                }
            } catch (PackageManager.NameNotFoundException e2) {
                return false;
            }
        }
        return t;
    }

    public boolean v() {
        return ApkResourceManager.getInstance().getInstalledApkInfo("com.tencent.mm") != null;
    }

    public int w() {
        LocalApkInfo installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo("com.tencent.mobileqq");
        if (installedApkInfo != null) {
            return installedApkInfo.mVersionCode;
        }
        return 0;
    }

    public void x() {
        e();
        l.a((m) null);
        synchronized (this.d) {
            this.c = null;
        }
        ah.a().post(new k(this));
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_LOGOUT));
        b.a();
    }

    public void a(String str, String str2, String str3) {
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            synchronized (this.d) {
                this.b = new r(str, str2, str3);
                AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.UI_EVENT_INIT_IDENTITY_SUCCESS);
            }
        }
    }

    public void a(MoblieQIdentityInfo moblieQIdentityInfo) {
        synchronized (this.d) {
            if (moblieQIdentityInfo != null) {
                this.b = moblieQIdentityInfo;
                AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.UI_EVENT_INIT_IDENTITY_SUCCESS);
            }
        }
    }
}
