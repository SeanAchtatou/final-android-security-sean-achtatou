package com.tencent.nucleus.socialcontact.comment;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.PraiseCommentRequest;
import com.tencent.assistant.protocol.jce.PraiseCommentResponse;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class d extends BaseEngine<j> {
    public int a(long j, long j2, byte b, long j3, long j4, String str, int i, String str2, String str3, String str4) {
        PraiseCommentRequest praiseCommentRequest = new PraiseCommentRequest();
        praiseCommentRequest.f1441a = j;
        praiseCommentRequest.b = j2;
        praiseCommentRequest.c = b;
        praiseCommentRequest.d = j3;
        praiseCommentRequest.e = j4;
        praiseCommentRequest.f = str;
        praiseCommentRequest.g = i;
        praiseCommentRequest.h = str2;
        praiseCommentRequest.i = str3;
        praiseCommentRequest.j = str4;
        XLog.d("comment", "AppCommentPraiseEngine.sendRequest, PraiseCommentRequest=" + praiseCommentRequest.toString());
        return send(praiseCommentRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        PraiseCommentResponse praiseCommentResponse = (PraiseCommentResponse) jceStruct2;
        PraiseCommentRequest praiseCommentRequest = (PraiseCommentRequest) jceStruct;
        XLog.d("comment", "AppCommentPraiseEngine.onRequestSuccessed, PraiseCommentResponse=" + (praiseCommentResponse != null ? praiseCommentResponse.toString() : "null"));
        if (praiseCommentResponse != null) {
            notifyDataChangedInMainThread(new e(this, i, praiseCommentResponse, praiseCommentRequest));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        PraiseCommentResponse praiseCommentResponse = (PraiseCommentResponse) jceStruct2;
        XLog.d("comment", "AppCommentPraiseEngine.onRequestFailed, errorCode=" + i2);
        notifyDataChangedInMainThread(new f(this, i, i2, (PraiseCommentRequest) jceStruct));
    }
}
