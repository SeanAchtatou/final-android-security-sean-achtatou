package com.tencent.nucleus.socialcontact.login;

import android.app.Dialog;
import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.e;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.modelmsg.SendAuth;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

/* compiled from: ProGuard */
public class s extends f implements UIEventListener {
    private static s h;
    private static int i = 1;
    private static Object j = new Object();
    private final int b = 350;
    private boolean c = false;
    private int d = -1;
    private int e = -1;
    private Dialog f = null;
    private final String g = "WxLoginEngine";

    public static synchronized s h() {
        s sVar;
        synchronized (s.class) {
            if (h == null) {
                h = new s();
            }
            sVar = h;
        }
        return sVar;
    }

    public s() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL, this);
    }

    public boolean i() {
        return this.c;
    }

    public void j() {
        this.c = false;
    }

    public void d() {
        l();
        XLog.d("WxLoginEngine", "WXEntryActivity--login--start");
        this.c = false;
        this.d = -1;
        try {
            IWXAPI createWXAPI = WXAPIFactory.createWXAPI(AstApp.i(), "wx3909f6add1206543", false);
            if (!e.b("com.tencent.mm", 350) || !createWXAPI.isWXAppInstalled()) {
                Message obtainMessage = AstApp.i().j().obtainMessage();
                obtainMessage.what = EventDispatcherEnum.UI_EVENT_WX_UNINSTALLED;
                AstApp.i().j().sendMessage(obtainMessage);
                return;
            }
            createWXAPI.registerApp("wx3909f6add1206543");
            SendAuth.Req req = new SendAuth.Req();
            req.scope = "snsapi_userinfo,snsapi_snsevent";
            req.state = "none";
            XLog.d("WxLoginEngine", "WXEntryActivity--login--send--request");
            if (createWXAPI.sendReq(req)) {
                this.c = true;
                this.d = k();
                return;
            }
            b();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void l() {
        if (this.f3160a == null) {
            l.a(new STInfoV2(STConst.ST_PAGE_LOGIN_VIRTUAL_WX, "03_001", 2000, STConst.ST_DEFAULT_SLOT, 200));
        }
    }

    /* access modifiers changed from: protected */
    public int k() {
        int i2;
        synchronized (j) {
            i2 = i;
            i = i2 + 1;
        }
        return i2;
    }

    private void m() {
        try {
            if (this.f != null && this.f.isShowing()) {
                this.f.dismiss();
            }
        } catch (Exception e2) {
        }
        this.f = null;
    }

    private void n() {
        XLog.d("WxLoginEngine", "WXEntryActivity--requireUserInfo");
        AppConst.LoadingDialogInfo loadingDialogInfo = new AppConst.LoadingDialogInfo();
        loadingDialogInfo.loadingText = "登录中";
        this.f = DialogUtils.showLoadingDialog(loadingDialogInfo);
        this.e = a.a().b();
    }

    public void a(BaseResp baseResp) {
        XLog.d("WxLoginEngine", "WXEntryActivity--onResp rsp.errCode = " + baseResp.errCode);
        this.d = -1;
        if (baseResp.errCode == 0) {
            if (baseResp instanceof SendAuth.Resp) {
                SendAuth.Resp resp = (SendAuth.Resp) baseResp;
                XLog.d(" " + resp.code);
                a(resp.code);
                if (!TextUtils.isEmpty(resp.code)) {
                    n();
                } else {
                    b();
                }
            }
        } else if (baseResp.errCode == -2 || baseResp.errCode == -4) {
            c();
        } else {
            b();
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS:
                XLog.d("WxLoginEngine", "WXEntryActivity--getUserInfo Success");
                if (this.e == message.arg1) {
                    m();
                    break;
                }
                break;
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL:
                if (this.e == message.arg1) {
                    m();
                    b();
                    break;
                }
                break;
        }
        this.e = -1;
    }

    public AppConst.LoginEgnineType e() {
        return AppConst.LoginEgnineType.ENGINE_WX;
    }

    public void g() {
        String a2 = l.a();
        String d2 = l.d();
        String c2 = l.c();
        if (!TextUtils.isEmpty(a2) && !TextUtils.isEmpty(d2)) {
            j.a().a(a2, d2, c2);
        }
    }
}
