package com.tencent.nucleus.manager.backgroundscan;

import com.tencent.assistant.db.table.f;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.nucleus.manager.backgroundscan.BackgroundScanManager;
import com.tencent.nucleus.manager.spaceclean.SpaceScanManager;
import com.tencent.nucleus.manager.spaceclean.SubRubbishInfo;
import com.tencent.nucleus.manager.spaceclean.ae;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
class l extends ae {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BackgroundScanManager f2833a;

    l(BackgroundScanManager backgroundScanManager) {
        this.f2833a = backgroundScanManager;
    }

    public void a(long j, SubRubbishInfo subRubbishInfo) {
    }

    public void a(boolean z) {
        XLog.i("BackgroundScan", "<scan> bigfile scan cleaned !");
        a(0);
    }

    public void a(ArrayList<SubRubbishInfo> arrayList) {
        long j = 0;
        Iterator<SubRubbishInfo> it = arrayList.iterator();
        while (true) {
            long j2 = j;
            if (it.hasNext()) {
                SubRubbishInfo next = it.next();
                if (next.f3011a == SubRubbishInfo.RubbishType.BIG_FILE) {
                    j = j2 + next.c;
                } else {
                    j = j2;
                }
            } else {
                a(j2);
                XLog.i("BackgroundScan", "<scan> bigfile scan finish, bigfile size = " + at.c(j2));
                return;
            }
        }
    }

    private void a(long j) {
        BackgroundScan a2 = this.f2833a.a((byte) 5);
        a2.e = j;
        f.a().b(a2);
        this.f2833a.i.put((byte) 5, BackgroundScanManager.SStatus.finish);
        SpaceScanManager.a().b(this.f2833a.l);
    }
}
