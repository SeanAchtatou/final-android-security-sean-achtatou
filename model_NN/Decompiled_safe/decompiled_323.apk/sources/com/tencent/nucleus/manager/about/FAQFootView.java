package com.tencent.nucleus.manager.about;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class FAQFootView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f2730a;
    private TextView b;

    public FAQFootView(Context context) {
        super(context);
        this.f2730a = context;
        a();
    }

    public FAQFootView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f2730a = context;
        a();
    }

    private void a() {
        this.b = (TextView) LayoutInflater.from(this.f2730a).inflate((int) R.layout.footer_layout, this).findViewById(R.id.text);
        this.b.setText(this.f2730a.getString(R.string.help_feedback));
    }

    public void a(int i) {
        if (i > 0) {
            this.b.setText(String.format(this.f2730a.getString(R.string.help_feedback_has_reply), Integer.valueOf(i)));
            return;
        }
        this.b.setText(this.f2730a.getString(R.string.help_feedback));
    }
}
