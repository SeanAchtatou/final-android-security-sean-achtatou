package com.tencent.nucleus.manager.apkuninstall;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class f extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ i f2788a;
    final /* synthetic */ LocalApkInfo b;
    final /* synthetic */ STInfoV2 c;
    final /* synthetic */ PreInstalledAppListAdapter d;

    f(PreInstalledAppListAdapter preInstalledAppListAdapter, i iVar, LocalApkInfo localApkInfo, STInfoV2 sTInfoV2) {
        this.d = preInstalledAppListAdapter;
        this.f2788a = iVar;
        this.b = localApkInfo;
        this.c = sTInfoV2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter, boolean):boolean
     arg types: [com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter, int]
     candidates:
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(int, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(android.widget.TextView, long):void
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(com.tencent.nucleus.manager.apkuninstall.i, com.tencent.assistant.localres.model.LocalApkInfo):void
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, int):void
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(boolean, boolean):void
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter, boolean):boolean */
    public void onTMAClick(View view) {
        if (this.d.i) {
            boolean unused = this.d.i = false;
            this.d.a(this.f2788a, this.b);
            return;
        }
        this.d.b(this.f2788a, this.b);
    }

    public STInfoV2 getStInfo() {
        this.c.actionId = 200;
        this.c.status = "02";
        this.c.extraData = this.b.mAppName;
        return this.c;
    }
}
