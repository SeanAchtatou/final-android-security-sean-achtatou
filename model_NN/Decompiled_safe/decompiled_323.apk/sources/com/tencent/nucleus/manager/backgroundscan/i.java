package com.tencent.nucleus.manager.backgroundscan;

import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.manager.backgroundscan.BackgroundScanManager;
import com.tencent.nucleus.manager.spaceclean.SpaceScanManager;
import com.tencent.securemodule.impl.SecureModuleService;
import java.util.Map;

/* compiled from: ProGuard */
class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BackgroundScanManager f2830a;

    i(BackgroundScanManager backgroundScanManager) {
        this.f2830a = backgroundScanManager;
    }

    public void run() {
        for (Map.Entry entry : this.f2830a.i.entrySet()) {
            BackgroundScanManager.SStatus sStatus = (BackgroundScanManager.SStatus) entry.getValue();
            if (sStatus == BackgroundScanManager.SStatus.prepare || sStatus == BackgroundScanManager.SStatus.running) {
                switch (((Byte) entry.getKey()).byteValue()) {
                    case 1:
                        XLog.d("BackgroundScan", "try to cancel health scan");
                        this.f2830a.i.put((byte) 1, BackgroundScanManager.SStatus.none);
                        continue;
                    case 2:
                        XLog.d("BackgroundScan", "try to cancel mem scan");
                        this.f2830a.i.put((byte) 2, BackgroundScanManager.SStatus.none);
                        continue;
                    case 3:
                        XLog.d("BackgroundScan", "try to cancel pkg scan");
                        this.f2830a.f.c();
                        this.f2830a.f.b(this.f2830a.j);
                        this.f2830a.i.put((byte) 3, BackgroundScanManager.SStatus.none);
                        continue;
                    case 4:
                        XLog.d("BackgroundScan", "try to cancel rubbish scan");
                        SpaceScanManager.a().h();
                        this.f2830a.i.put((byte) 4, BackgroundScanManager.SStatus.none);
                        continue;
                    case 5:
                        XLog.d("BackgroundScan", "try to cancel bigfile scan");
                        this.f2830a.i.put((byte) 5, BackgroundScanManager.SStatus.none);
                        continue;
                    case 6:
                        XLog.d("BackgroundScan", "try to cancel virus scan");
                        if (this.f2830a.f2819a != null) {
                            SecureModuleService.getInstance(this.f2830a.e).unregisterCloudScanListener(this.f2830a.e, this.f2830a.f2819a);
                        }
                        this.f2830a.i.put((byte) 6, BackgroundScanManager.SStatus.none);
                        continue;
                }
            }
        }
    }
}
