package com.tencent.nucleus.manager.component;

import android.view.View;
import android.view.animation.Animation;

/* compiled from: ProGuard */
class c implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f2863a;
    final /* synthetic */ e b;
    final /* synthetic */ AnimationExpandableListView c;

    c(AnimationExpandableListView animationExpandableListView, View view, e eVar) {
        this.c = animationExpandableListView;
        this.f2863a = view;
        this.b = eVar;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f2863a.clearAnimation();
        this.f2863a.setVisibility(0);
        this.b.a();
    }
}
