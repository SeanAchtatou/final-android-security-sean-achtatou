package com.tencent.nucleus.manager.about;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BrowserActivity;
import com.tencent.assistant.manager.webview.component.WebViewFooter;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;

/* compiled from: ProGuard */
public class HelperFAQActivity extends BrowserActivity {
    /* access modifiers changed from: private */
    public Context C;
    /* access modifiers changed from: private */
    public FAQFootView D;
    private Handler E = new s(this);
    private l F = new t(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.C = this;
        D();
        n.a().register(this.F);
        this.E.sendEmptyMessageDelayed(0, 50);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        n.a().unregister(this.F);
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(getString(R.string.help_title));
    }

    public void u() {
        this.w = (WebViewFooter) findViewById(R.id.browser_footer_view);
        this.w.setVisibility(8);
    }

    public void c(boolean z) {
    }

    private void D() {
        this.D = new FAQFootView(this);
        ((RelativeLayout) findViewById(R.id.browser_footer_layout)).addView(this.D, new RelativeLayout.LayoutParams(-1, -2));
        this.D.setOnClickListener(new u(this));
        SecondNavigationTitleViewV5 secondNavigationTitleViewV5 = (SecondNavigationTitleViewV5) findViewById(R.id.browser_header_view);
        secondNavigationTitleViewV5.d();
        secondNavigationTitleViewV5.j();
    }
}
