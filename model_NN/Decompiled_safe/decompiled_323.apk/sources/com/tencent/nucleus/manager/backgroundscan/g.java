package com.tencent.nucleus.manager.backgroundscan;

import com.tencent.nucleus.manager.backgroundscan.BackgroundScanManager;
import java.util.Map;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BackgroundScanManager f2828a;

    g(BackgroundScanManager backgroundScanManager) {
        this.f2828a = backgroundScanManager;
    }

    public void run() {
        if (this.f2828a.k()) {
            o.a().a("b_new_scan_push_begin2", (byte) 0);
        }
        for (Map.Entry entry : this.f2828a.i.entrySet()) {
            if (((BackgroundScanManager.SStatus) entry.getValue()) == BackgroundScanManager.SStatus.prepare) {
                switch (((Byte) entry.getKey()).byteValue()) {
                    case 1:
                        this.f2828a.l();
                        break;
                    case 2:
                        this.f2828a.m();
                        break;
                    case 3:
                        this.f2828a.o();
                        break;
                    case 4:
                        this.f2828a.p();
                        break;
                    case 5:
                        this.f2828a.n();
                        break;
                    case 6:
                        if (this.f2828a.g()) {
                            this.f2828a.i();
                            break;
                        }
                        break;
                }
                if (this.f2828a.g() || ((Byte) entry.getKey()).byteValue() != 6) {
                    o.a().a("b_new_scan_push_start", ((Byte) entry.getKey()).byteValue());
                } else {
                    return;
                }
            }
        }
    }
}
