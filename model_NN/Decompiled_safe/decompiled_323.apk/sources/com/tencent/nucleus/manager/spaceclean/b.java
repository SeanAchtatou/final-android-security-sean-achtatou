package com.tencent.nucleus.manager.spaceclean;

import android.os.Message;
import com.tencent.assistant.Global;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.t;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/* compiled from: ProGuard */
class b extends ae {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BigFileCleanActivity f3025a;

    b(BigFileCleanActivity bigFileCleanActivity) {
        this.f3025a = bigFileCleanActivity;
    }

    public void a(long j, SubRubbishInfo subRubbishInfo) {
        XLog.d("miles", "BigFileCleanActivity >> onRubbishFound. " + subRubbishInfo.b);
        BigFileCleanActivity.a(this.f3025a);
        if (this.f3025a.E % 1 == 0) {
            int unused = this.f3025a.E = 0;
            if (this.f3025a.P.hasMessages(24)) {
                this.f3025a.P.removeMessages(24);
            }
            Message obtain = Message.obtain(this.f3025a.P, 24);
            obtain.obj = Long.valueOf(j);
            obtain.sendToTarget();
        }
    }

    public void a(ArrayList<SubRubbishInfo> arrayList) {
        XLog.d("miles", "BigFileCleanActivity >> onScanFinished.");
        n nVar = null;
        n nVar2 = null;
        n nVar3 = null;
        n nVar4 = null;
        n nVar5 = null;
        Iterator<SubRubbishInfo> it = arrayList.iterator();
        while (it.hasNext()) {
            SubRubbishInfo next = it.next();
            next.d = false;
            switch (d.f3027a[next.f3011a.ordinal()]) {
                case 1:
                    if (nVar == null) {
                        nVar = new n();
                        nVar.b = "视频";
                        nVar.h = 0;
                        this.f3025a.D.add(nVar);
                    }
                    nVar.d += next.c;
                    nVar.a(next);
                    break;
                case 2:
                    if (nVar2 == null) {
                        nVar2 = new n();
                        nVar2.b = "音乐";
                        nVar2.h = 1;
                        this.f3025a.D.add(nVar2);
                    }
                    nVar2.d += next.c;
                    nVar2.a(next);
                    break;
                case 3:
                    if (nVar3 == null) {
                        nVar3 = new n();
                        nVar3.b = "文档";
                        nVar3.h = 2;
                        this.f3025a.D.add(nVar3);
                    }
                    nVar3.d += next.c;
                    nVar3.a(next);
                    break;
                case 4:
                    if (nVar4 == null) {
                        nVar4 = new n();
                        nVar4.b = "压缩文件";
                        nVar4.h = 3;
                        this.f3025a.D.add(nVar4);
                    }
                    nVar4.d += next.c;
                    nVar4.a(next);
                    break;
                default:
                    if (nVar5 == null) {
                        nVar5 = new n();
                        nVar5.b = "其他文件";
                        nVar5.h = 4;
                        this.f3025a.D.add(nVar5);
                    }
                    nVar5.d += next.c;
                    nVar5.a(next);
                    break;
            }
            n nVar6 = nVar5;
            n nVar7 = nVar4;
            nVar = nVar;
            nVar2 = nVar2;
            nVar3 = nVar3;
            nVar4 = nVar7;
            nVar5 = nVar6;
        }
        long j = 0;
        Iterator it2 = this.f3025a.D.iterator();
        while (true) {
            long j2 = j;
            if (it2.hasNext()) {
                j = j2 + ((n) it2.next()).d;
            } else {
                if (this.f3025a.P.hasMessages(25)) {
                    this.f3025a.P.removeMessages(25);
                }
                Message obtain = Message.obtain(this.f3025a.P, 25);
                obtain.obj = Long.valueOf(j2);
                obtain.sendToTarget();
                long unused = this.f3025a.L = System.currentTimeMillis();
                HashMap hashMap = new HashMap();
                hashMap.put("B1", ((j2 / 1024) / 1024) + Constants.STR_EMPTY);
                hashMap.put("B2", j2 + Constants.STR_EMPTY);
                hashMap.put("B3", Global.getPhoneGuidAndGen());
                hashMap.put("B4", Global.getQUAForBeacon());
                hashMap.put("B5", t.g());
                a.a("BigFileScan", true, this.f3025a.L - this.f3025a.K, -1, hashMap, true);
                XLog.d("beacon", "beacon report >> event: BigFileScan, totalTime : " + (this.f3025a.L - this.f3025a.K) + ", params : " + hashMap.toString());
                return;
            }
        }
    }

    public void a(boolean z) {
        Message.obtain(this.f3025a.P, 27).sendToTarget();
        long unused = this.f3025a.N = System.currentTimeMillis();
        HashMap hashMap = new HashMap();
        hashMap.put("B1", ((this.f3025a.F / 1024) / 1024) + Constants.STR_EMPTY);
        hashMap.put("B2", this.f3025a.F + Constants.STR_EMPTY);
        hashMap.put("B3", Global.getPhoneGuidAndGen());
        hashMap.put("B4", Global.getQUAForBeacon());
        hashMap.put("B5", t.g());
        a.a("BigFileClean", true, this.f3025a.N - this.f3025a.M, -1, hashMap, true);
        XLog.d("beacon", "beacon report >> event: BigFileClean, totalTime : " + (this.f3025a.N - this.f3025a.M) + ", params : " + hashMap.toString());
    }
}
