package com.tencent.nucleus.manager.appbackup;

import android.view.View;
import android.widget.TextView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;

/* compiled from: ProGuard */
class c extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TextView f2802a;
    final /* synthetic */ int b;
    final /* synthetic */ BackupAppListAdapter c;

    c(BackupAppListAdapter backupAppListAdapter, TextView textView, int i) {
        this.c = backupAppListAdapter;
        this.f2802a = textView;
        this.b = i;
    }

    public void onTMAClick(View view) {
        this.f2802a.setSelected(!this.f2802a.isSelected());
        this.c.e[this.b] = this.f2802a.isSelected();
        f a2 = this.c.a();
        if (a2 != null) {
            this.c.c.a(a2.f2805a, a2.b, a2.c);
        } else {
            this.c.c.a(0, false, "0M");
        }
    }
}
