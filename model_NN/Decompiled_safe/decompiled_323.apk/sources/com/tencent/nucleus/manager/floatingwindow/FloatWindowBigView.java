package com.tencent.nucleus.manager.floatingwindow;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.SpaceCleanActivity;
import com.tencent.assistant.activity.SplashActivity;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.module.k;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ar;
import com.tencent.cloud.activity.UpdateListActivity;
import com.tencent.nucleus.manager.component.RollTextView;
import com.tencent.nucleus.manager.main.AssistantTabActivity;
import com.tencent.nucleus.manager.setting.SettingActivity;
import com.tencent.nucleus.manager.spaceclean.SpaceScanManager;
import java.text.DecimalFormat;

/* compiled from: ProGuard */
public class FloatWindowBigView extends LinearLayout implements View.OnClickListener, View.OnTouchListener, UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    public static int f2884a;
    public static int b;
    /* access modifiers changed from: private */
    public long A;
    /* access modifiers changed from: private */
    public View B;
    /* access modifiers changed from: private */
    public Animation C;
    private long D = 0;
    private long E = 0;
    private View c;
    private View d;
    private View e;
    private View f;
    private View g;
    private View h;
    private View i;
    private View j;
    private View k;
    private View l;
    private TextView m;
    private View n;
    private View o;
    private View p;
    /* access modifiers changed from: private */
    public View q;
    /* access modifiers changed from: private */
    public View r;
    /* access modifiers changed from: private */
    public View s;
    /* access modifiers changed from: private */
    public RollTextView t;
    /* access modifiers changed from: private */
    public TextView u;
    /* access modifiers changed from: private */
    public ImageView v;
    /* access modifiers changed from: private */
    public RollTextView w;
    /* access modifiers changed from: private */
    public int x = 75;
    private int y;
    /* access modifiers changed from: private */
    public boolean z = true;

    public FloatWindowBigView(Context context) {
        super(context);
        try {
            LayoutInflater.from(context).inflate((int) R.layout.float_window_big, this);
            setGravity(17);
            setOnTouchListener(this);
            this.c = findViewById(R.id.big_window_layout);
            f2884a = this.c.getLayoutParams().width;
            b = this.c.getLayoutParams().height;
            this.d = this.c.findViewById(R.id.ll_yyb_entrance);
            this.e = this.c.findViewById(R.id.ll_setting_entrance);
            this.p = this.c.findViewById(R.id.rl_accelerate_circle);
            this.q = this.c.findViewById(R.id.rl_circle_content_normal);
            this.r = this.c.findViewById(R.id.rl_circle_content_result);
            this.s = this.c.findViewById(R.id.rl_circle_content_finish);
            this.t = (RollTextView) this.c.findViewById(R.id.mem_free_size);
            this.u = (TextView) this.c.findViewById(R.id.header_size_unit_result);
            this.t.a(this.u);
            this.t.a(false);
            this.v = (ImageView) this.c.findViewById(R.id.iv_rocket);
            this.w = (RollTextView) this.c.findViewById(R.id.mem_percent);
            this.f = this.c.findViewById(R.id.update_number);
            this.g = this.c.findViewById(R.id.rubbish_circle);
            this.h = this.c.findViewById(R.id.more_manage_circle);
            this.i = this.c.findViewById(R.id.tv_app_update);
            this.j = this.c.findViewById(R.id.tv_rubbish_clean);
            this.k = this.c.findViewById(R.id.tv_more_manage);
            this.l = this.c.findViewById(R.id.rl_app_update_entrance);
            this.m = (TextView) this.c.findViewById(R.id.update_number);
            this.n = this.c.findViewById(R.id.rl_rubbish_clean_entrance);
            this.o = this.c.findViewById(R.id.rl_more_manage_entrance);
            this.B = this.c.findViewById(R.id.pb_accelerate);
            this.C = AnimationUtils.loadAnimation(getContext(), R.anim.speedcircle);
            this.p.setOnClickListener(this);
            this.d.setOnClickListener(this);
            this.e.setOnClickListener(this);
            this.l.setOnClickListener(this);
            this.n.setOnClickListener(this);
            this.o.setOnClickListener(this);
            this.w.a(new DecimalFormat("0"));
            this.x = (int) (ar.d() * 100.0f);
            this.w.b((double) this.x);
            this.y = (int) getResources().getDimension(R.dimen.floating_big_window_circle_size);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_SUCCESS, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL, this);
            e();
            setBackgroundColor(1711276032);
            a();
        } catch (Throwable th) {
            t.a().b();
        }
    }

    private void a() {
        AnimationSet animationSet = new AnimationSet(true);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(500);
        alphaAnimation.setFillAfter(true);
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.9f, 1.0f, 0.9f, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(500);
        scaleAnimation.setFillAfter(true);
        animationSet.addAnimation(alphaAnimation);
        animationSet.addAnimation(scaleAnimation);
        this.c.startAnimation(animationSet);
        AnimationSet animationSet2 = new AnimationSet(true);
        AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation2.setStartOffset(400);
        alphaAnimation2.setDuration(400);
        alphaAnimation2.setFillAfter(true);
        ScaleAnimation scaleAnimation2 = new ScaleAnimation(0.8f, 1.0f, 0.8f, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation2.setStartOffset(400);
        scaleAnimation2.setDuration(400);
        scaleAnimation2.setFillAfter(true);
        animationSet2.addAnimation(alphaAnimation2);
        animationSet2.addAnimation(scaleAnimation2);
        this.f.startAnimation(animationSet2);
        this.g.startAnimation(animationSet2);
        this.h.startAnimation(animationSet2);
        this.i.startAnimation(alphaAnimation2);
        this.j.startAnimation(alphaAnimation2);
        this.k.startAnimation(alphaAnimation2);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        Rect rect = new Rect();
        this.c.getGlobalVisibleRect(rect);
        if (rect.contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
            return false;
        }
        n.a().d(AstApp.i());
        n.a().f();
        n.a().a(AstApp.i());
        return false;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() != 4) {
            return super.dispatchKeyEvent(keyEvent);
        }
        n.a().d(AstApp.i());
        n.a().f();
        n.a().a(AstApp.i());
        return true;
    }

    public void onClick(View view) {
        Intent intent = new Intent();
        intent.addFlags(268435456);
        switch (view.getId()) {
            case R.id.ll_yyb_entrance /*2131166089*/:
                try {
                    intent.setPackage(AstApp.i().getPackageName());
                    intent.setAction("android.intent.action.MAIN");
                    intent.addCategory("android.intent.category.LAUNCHER");
                    AstApp.i().startActivity(intent);
                } catch (Exception e2) {
                    try {
                        intent.setClass(AstApp.i(), SplashActivity.class);
                        AstApp.i().startActivity(intent);
                    } catch (Exception e3) {
                    }
                }
                n.a().d(AstApp.i());
                n.a().a(STConst.ST_PAGE_FLOAT_BIG_WINDOW_PAGEID, "05_001", 200);
                return;
            case R.id.ll_setting_entrance /*2131166091*/:
                intent.setClass(AstApp.i(), SettingActivity.class);
                intent.addFlags(67108864);
                try {
                    AstApp.i().startActivity(intent);
                    n.a().d(AstApp.i());
                    n.a().a(STConst.ST_PAGE_FLOAT_BIG_WINDOW_PAGEID, "05_002", 200);
                    return;
                } catch (Throwable th) {
                    th.printStackTrace();
                    return;
                }
            case R.id.rl_app_update_entrance /*2131166094*/:
                intent.setClass(AstApp.i(), UpdateListActivity.class);
                intent.addFlags(67108864);
                try {
                    AstApp.i().startActivity(intent);
                    n.a().d(AstApp.i());
                    n.a().a(STConst.ST_PAGE_FLOAT_BIG_WINDOW_PAGEID, "04_001", 200);
                    return;
                } catch (Throwable th2) {
                    th2.printStackTrace();
                    return;
                }
            case R.id.rl_rubbish_clean_entrance /*2131166097*/:
                intent.setClass(AstApp.i(), SpaceCleanActivity.class);
                intent.addFlags(67108864);
                try {
                    AstApp.i().startActivity(intent);
                    n.a().d(AstApp.i());
                    n.a().a(STConst.ST_PAGE_FLOAT_BIG_WINDOW_PAGEID, "04_002", 200);
                    return;
                } catch (Throwable th3) {
                    th3.printStackTrace();
                    return;
                }
            case R.id.rl_more_manage_entrance /*2131166100*/:
                intent.setClass(AstApp.i(), AssistantTabActivity.class);
                intent.addFlags(67108864);
                try {
                    AstApp.i().startActivity(intent);
                    n.a().d(AstApp.i());
                    n.a().a(STConst.ST_PAGE_FLOAT_BIG_WINDOW_PAGEID, "04_003", 200);
                    return;
                } catch (Throwable th4) {
                    th4.printStackTrace();
                    return;
                }
            case R.id.rl_accelerate_circle /*2131166103*/:
                if (this.z) {
                    this.z = false;
                    this.A = 0;
                    n.a().a(STConst.ST_PAGE_FLOAT_BIG_WINDOW_PAGEID, "03_001", 200);
                    n.a().a("FloatWindowAccelerate", "BigWindow");
                    SpaceScanManager.a().p();
                    b();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void b() {
        this.D = System.currentTimeMillis();
        AnimationSet animationSet = new AnimationSet(true);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(300);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.8f, 1.0f, 0.8f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(300);
        animationSet.addAnimation(alphaAnimation);
        animationSet.addAnimation(scaleAnimation);
        animationSet.setFillAfter(true);
        this.q.startAnimation(animationSet);
        AnimationSet animationSet2 = new AnimationSet(true);
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) this.y, -10.0f);
        translateAnimation.setDuration(400);
        TranslateAnimation translateAnimation2 = new TranslateAnimation(0.0f, 0.0f, -10.0f, 0.0f);
        translateAnimation2.setStartOffset(400);
        translateAnimation2.setDuration(100);
        animationSet2.addAnimation(translateAnimation);
        animationSet2.addAnimation(translateAnimation2);
        animationSet2.setStartOffset(300);
        animationSet2.setAnimationListener(new a(this));
        this.v.setVisibility(4);
        try {
            this.v.setImageResource(R.drawable.admin_launch_rocket_line);
        } catch (Throwable th) {
            t.a().b();
        }
        this.v.startAnimation(animationSet2);
    }

    /* access modifiers changed from: private */
    public void c() {
        AnimationSet animationSet = new AnimationSet(true);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.8f, 1.0f, 0.8f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setDuration(200);
        scaleAnimation.setAnimationListener(new b(this));
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (-this.y));
        translateAnimation.setStartOffset(300);
        translateAnimation.setDuration(200);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(translateAnimation);
        animationSet.setFillAfter(true);
        this.v.startAnimation(animationSet);
        postDelayed(new c(this), 700);
    }

    /* access modifiers changed from: private */
    public void d() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(400);
        alphaAnimation.setFillAfter(true);
        this.s.setVisibility(4);
        this.s.startAnimation(alphaAnimation);
        postDelayed(new f(this), 2500);
    }

    private void e() {
        int i2 = k.i();
        this.m.setVisibility(0);
        this.m.setText(String.valueOf(i2 > 99 ? "99+" : Integer.valueOf(i2)));
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_SUCCESS:
                XLog.d("floatingwindow", "FloatingWindowBigView >> 加速成功！");
                this.A = ((Long) message.obj).longValue();
                this.E = System.currentTimeMillis();
                if (this.E - this.D < 2300) {
                    postDelayed(new h(this), 2300 - (this.E - this.D));
                    return;
                } else {
                    c();
                    return;
                }
            case EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL:
                XLog.d("floatingwindow", "FloatingWindowBigView >> 加速失败");
                this.E = System.currentTimeMillis();
                if (this.E - this.D < 2300) {
                    postDelayed(new i(this), 2300 - (this.E - this.D));
                    return;
                } else {
                    c();
                    return;
                }
            default:
                return;
        }
    }
}
