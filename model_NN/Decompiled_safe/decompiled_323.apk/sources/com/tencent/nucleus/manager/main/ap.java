package com.tencent.nucleus.manager.main;

import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ar;
import com.tencent.securemodule.impl.AppInfo;
import com.tencent.securemodule.service.CloudScanListener;
import java.util.List;

/* compiled from: ProGuard */
class ap implements CloudScanListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ aj f2944a;

    private ap(aj ajVar) {
        this.f2944a = ajVar;
    }

    /* synthetic */ ap(aj ajVar, ak akVar) {
        this(ajVar);
    }

    public void onFinish(int i) {
        XLog.i(this.f2944a.b, ">>Safe scan finish>>");
        Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_MGR_SAFE_SCAN_SUCCESS);
        obtainMessage.obj = 0;
        AstApp.i().j().dispatchMessage(obtainMessage);
    }

    public void onRiskFoud(List<AppInfo> list) {
        int i = 0;
        XLog.i(this.f2944a.b, "onRiskFoun:" + (list == null ? 0 : list.size()));
        Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_MGR_SAFE_SCAN_SUCCESS);
        if (list != null) {
            i = list.size();
        }
        obtainMessage.obj = Integer.valueOf(i);
        AstApp.i().j().dispatchMessage(obtainMessage);
        StringBuilder sb = new StringBuilder();
        if (list != null) {
            for (AppInfo next : list) {
                if (next != null) {
                    sb.append(next.pkgName + "|");
                }
            }
        }
        ar.a(sb.toString());
    }

    @Deprecated
    public void onRiskFound() {
    }
}
