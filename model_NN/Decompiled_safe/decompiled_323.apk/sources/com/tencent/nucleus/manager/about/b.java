package com.tencent.nucleus.manager.about;

import android.view.View;
import com.tencent.assistant.utils.t;

/* compiled from: ProGuard */
class b implements View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AboutActivity f2741a;

    b(AboutActivity aboutActivity) {
        this.f2741a = aboutActivity;
    }

    public boolean onLongClick(View view) {
        if (this.f2741a.A == null) {
            af unused = this.f2741a.A = new af();
            this.f2741a.A.register(new c(this));
        }
        String unused2 = this.f2741a.B = "RANDOM:" + this.f2741a.b(6) + "\n" + "IMEI:" + t.g();
        this.f2741a.A.a(this.f2741a.B);
        return true;
    }
}
