package com.tencent.nucleus.manager.about;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.model.ItemElement;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.manager.SelfUpdateManager;
import java.util.ArrayList;
import java.util.Random;

/* compiled from: ProGuard */
public class AboutActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public af A = null;
    /* access modifiers changed from: private */
    public String B = null;
    private Context n;
    private SecondNavigationTitleViewV5 u;
    private ImageView v;
    private TextView w;
    private TextView x;
    private ListView y;
    /* access modifiers changed from: private */
    public AboutAdapter z;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int i = displayMetrics.densityDpi;
        int i2 = displayMetrics.heightPixels;
        int i3 = displayMetrics.widthPixels;
        if (i < 320 || (i2 == 960 && i3 == 640)) {
            setContentView((int) R.layout.activity_about);
        } else {
            setContentView((int) R.layout.activity_about_hdpi);
        }
        this.n = this;
        this.z = new AboutAdapter(this.n);
        u();
        t();
    }

    private void t() {
        this.v = (ImageView) findViewById(R.id.about_logo);
        this.v.setOnClickListener(new a(this));
        this.v.setOnLongClickListener(new b(this));
        this.u = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.u.a(this);
        this.u.b(getString(R.string.about_title));
        this.u.d();
        this.u.c(new d(this));
        this.w = (TextView) findViewById(R.id.app_name);
        this.x = (TextView) findViewById(R.id.about_version);
        this.x.setText(Global.getAppVersionName());
        this.y = (ListView) findViewById(R.id.content_list);
        this.y.setAdapter((ListAdapter) this.z);
        this.y.setDivider(null);
        this.y.setOnItemClickListener(new e(this));
    }

    private void u() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new ItemElement(this.n.getString(R.string.about_check_version), null, 1, 0, 0));
        arrayList.add(new ItemElement(this.n.getString(R.string.about_help_feedback), null, 2, 1, 0));
        arrayList.add(new ItemElement(this.n.getString(R.string.about_welcome_page), null, 3, 2, 0));
        arrayList.add(new ItemElement(this.n.getString(R.string.about_declare), null, 4, 3, 0));
        this.z.a(arrayList);
    }

    /* access modifiers changed from: private */
    public void v() {
        f fVar = new f(this);
        fVar.titleRes = getString(R.string.ver_low_tips_title);
        fVar.contentRes = this.B;
        fVar.btnTxtRes = getString(R.string.ver_low_tips_ok);
        DialogUtils.show1BtnDialog(fVar);
    }

    /* access modifiers changed from: private */
    public void a(ItemElement itemElement) {
        switch (itemElement.c) {
            case 1:
                SelfUpdateManager.a().a(true);
                SelfUpdateManager.a().o().a(true);
                return;
            case 2:
                Intent intent = new Intent(this.n, HelperFAQActivity.class);
                intent.putExtra("com.tencent.assistant.BROWSER_URL", "http://maweb.3g.qq.com/help/help.html");
                this.n.startActivity(intent);
                b("04_001");
                return;
            case 3:
                this.n.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("tmast://guide")));
                return;
            case 4:
                this.n.startActivity(new Intent(this.n, AboutDeclareActivity.class));
                return;
            default:
                return;
        }
    }

    private void b(String str) {
        l.a(new STInfoV2(f(), str, m(), STConst.ST_DEFAULT_SLOT, 200));
    }

    public int f() {
        return STConst.ST_PAGE_ABOUT;
    }

    /* access modifiers changed from: private */
    public String b(int i) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i2 = 0; i2 < i; i2++) {
            sb.append(random.nextInt(10));
        }
        return sb.toString();
    }

    public void onResume() {
        super.onResume();
        if (this.z != null) {
            this.z.a();
        }
    }

    public void onPause() {
        super.onPause();
        if (this.z != null) {
            this.z.b();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.z != null) {
            this.z.c();
        }
    }
}
