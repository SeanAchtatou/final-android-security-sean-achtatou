package com.tencent.nucleus.manager.floatingwindow;

/* compiled from: ProGuard */
class ac implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Wave f2897a;

    private ac(Wave wave) {
        this.f2897a = wave;
    }

    public void run() {
        long j = 0;
        synchronized (this.f2897a) {
            long currentTimeMillis = System.currentTimeMillis();
            this.f2897a.d();
            this.f2897a.invalidate();
            long currentTimeMillis2 = 16 - (System.currentTimeMillis() - currentTimeMillis);
            Wave wave = this.f2897a;
            if (currentTimeMillis2 >= 0) {
                j = currentTimeMillis2;
            }
            wave.postDelayed(this, j);
        }
    }
}
