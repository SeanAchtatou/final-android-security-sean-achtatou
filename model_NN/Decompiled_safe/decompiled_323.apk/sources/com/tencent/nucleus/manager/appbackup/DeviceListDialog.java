package com.tencent.nucleus.manager.appbackup;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class DeviceListDialog extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    private ListView f2799a;
    private BackupDeviceAdapter b;
    private ImageView c;

    public DeviceListDialog(Context context) {
        super(context, R.style.dialog);
        setCancelable(true);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.device_list_layout);
        Window window = getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.gravity = 17;
        Display defaultDisplay = getWindow().getWindowManager().getDefaultDisplay();
        attributes.width = (int) (((double) defaultDisplay.getWidth()) * 0.919d);
        attributes.height = (int) (((double) defaultDisplay.getHeight()) * 0.8d);
        window.setAttributes(attributes);
        a();
    }

    private void a() {
        this.f2799a = (ListView) findViewById(R.id.listview);
        this.f2799a.setDivider(null);
        if (this.b != null) {
            this.f2799a.setAdapter((ListAdapter) this.b);
        }
        this.c = (ImageView) findViewById(R.id.close);
        this.c.setOnClickListener(new r(this));
    }

    public void a(BackupDeviceAdapter backupDeviceAdapter) {
        this.b = backupDeviceAdapter;
        if (this.f2799a != null) {
            this.f2799a.setAdapter((ListAdapter) this.b);
        }
        this.b.a(this);
    }

    public void a(int i) {
        switch (i) {
            case -1:
                this.b.a();
                return;
            case 0:
                this.b.a();
                return;
            case 1:
                dismiss();
                return;
            default:
                return;
        }
    }
}
