package com.tencent.nucleus.manager.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.activity.SpaceCleanActivity;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.module.update.AppUpdateConst;
import com.tencent.assistant.module.update.b;
import com.tencent.assistant.module.update.j;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.plugin.PluginProxyUtils;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ar;
import com.tencent.assistant.utils.by;
import com.tencent.assistant.utils.e;
import com.tencent.assistantv2.component.SimpleScrollView;
import com.tencent.assistantv2.component.aq;
import com.tencent.assistantv2.component.bd;
import com.tencent.assistantv2.model.a;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.cloud.activity.UpdateListActivity;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.about.l;
import com.tencent.nucleus.manager.about.n;
import com.tencent.nucleus.manager.component.AssistantUpdateOverTurnView;
import com.tencent.nucleus.manager.component.ScaleRelativeLayout;
import com.tencent.nucleus.manager.component.t;
import com.tencent.nucleus.manager.spaceclean.SpaceScanManager;
import com.tencent.pangu.activity.ShareBaseActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: ProGuard */
public class AssistantTabActivity extends ShareBaseActivity implements Handler.Callback, UIEventListener {
    private FrameLayout A;
    private FrameLayout B;
    private RelativeLayout C;
    private RelativeLayout D;
    private RelativeLayout E;
    /* access modifiers changed from: private */
    public ScaleRelativeLayout F;
    /* access modifiers changed from: private */
    public RelativeLayout G;
    private RelativeLayout H;
    private ImageView I;
    /* access modifiers changed from: private */
    public ImageView J;
    /* access modifiers changed from: private */
    public ImageView K;
    private ImageView L;
    private ImageView M;
    private Animation N;
    private Animation O;
    private Animation P;
    /* access modifiers changed from: private */
    public ViewSwitcher Q;
    private TextView R;
    private TextView S;
    private TextView T;
    /* access modifiers changed from: private */
    public TextView U;
    private TextView V;
    private TextView W;
    private TextView X;
    private AssistantUpdateOverTurnView Y;
    private List<AppUpdateInfo> Z;
    private final String aA = "04_002";
    private final String aB = "05_001";
    private final String aC = "05_002";
    private final String aD = "05_003";
    /* access modifiers changed from: private */
    public int aE = 80;
    /* access modifiers changed from: private */
    public int aF = 99;
    private String aG = Constants.STR_EMPTY;
    private boolean aH = true;
    /* access modifiers changed from: private */
    public boolean aI = false;
    /* access modifiers changed from: private */
    public boolean aJ = false;
    private Animation aK;
    private Animation aL;
    private Animation aM;
    private Animation aN;
    private aj aO;
    /* access modifiers changed from: private */
    public a aP;
    private a aQ;
    /* access modifiers changed from: private */
    public float aR = 0.0f;
    /* access modifiers changed from: private */
    public float aS = 0.0f;
    private float aT;
    private int aU = 0;
    private int aV = 0;
    /* access modifiers changed from: private */
    public volatile boolean aW = true;
    private final int aX = 100;
    private final int aY = 90;
    private final int aZ = 75;
    private List<AppUpdateInfo> aa;
    private int ab = 4;
    private long ac = 5000;
    private final int ad = 300;
    private final int ae = EventDispatcherEnum.CACHE_EVENT_END;
    private AtomicInteger af = new AtomicInteger(2);
    /* access modifiers changed from: private */
    public Class<?> ag;
    /* access modifiers changed from: private */
    public List<a> ah = new ArrayList();
    /* access modifiers changed from: private */
    public Object ai = new Object();
    /* access modifiers changed from: private */
    public int aj = -1;
    /* access modifiers changed from: private */
    public long ak = 0;
    /* access modifiers changed from: private */
    public long al = 0;
    /* access modifiers changed from: private */
    public int am = 0;
    /* access modifiers changed from: private */
    public int an = 0;
    private boolean ao = false;
    private int ap = 0;
    private TextView aq;
    private final String ar = "03_001";
    private final String as = "06_001";
    private final String at = "08_001";
    private final String au = "08_002";
    private final String av = "08_003";
    private final String aw = "05_001";
    private final String ax = "05_002";
    private final String ay = "05_003";
    private final String az = "04_001";
    private final int ba = 0;
    /* access modifiers changed from: private */
    public boolean bb = false;
    private boolean bc = false;
    private boolean bd = true;
    private aq be;
    private volatile boolean bf = false;
    /* access modifiers changed from: private */
    public volatile boolean bg = false;
    /* access modifiers changed from: private */
    public int bh = 2;
    /* access modifiers changed from: private */
    public Handler bi;
    private boolean bj = false;
    private boolean bk = false;
    private final int bl = 1;
    private final int bm = 2;
    private Typeface bn;
    private boolean bo = false;
    private bd bp = new y(this);
    private t bq = new z(this);
    private OnTMAParamExClickListener br = new g(this);
    private com.tencent.nucleus.manager.a bs = new s(this);
    l n = new c(this);
    private String u = "Assistant";
    /* access modifiers changed from: private */
    public Context v;
    /* access modifiers changed from: private */
    public SimpleScrollView w;
    private GridView x;
    /* access modifiers changed from: private */
    public AssistantTabAdapter y;
    private LinearLayout z;

    static /* synthetic */ int n(AssistantTabActivity assistantTabActivity) {
        int i = assistantTabActivity.bh;
        assistantTabActivity.bh = i - 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.activity_asistant_layout_v5);
            this.v = this;
            x();
            C();
            I();
            D();
            n.a().register(this.n);
            if (m.a().L()) {
                R();
            }
            com.tencent.nucleus.manager.floatingwindow.n.a().c();
            w();
        } catch (Throwable th) {
            this.bo = true;
            finish();
            com.tencent.assistant.manager.t.a().b();
        }
    }

    private void w() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", com.tencent.assistant.utils.t.g());
        XLog.d("beacon", "beacon report >> expose_managehome. " + hashMap.toString());
        com.tencent.beacon.event.a.a("expose_managehome", true, -1, -1, hashMap, true);
    }

    private void x() {
        this.bi = new Handler(this);
        this.aO = y();
        this.aE = ar.a();
        this.aG = d(this.aE);
        b("03_001", String.valueOf(this.aE));
        b("06_001", String.valueOf(k.i()));
        this.aT = getResources().getDimension(R.dimen.manager_layout_score_height);
        if (this.aT == 0.0f) {
            this.aT = (float) by.a(getApplicationContext(), 127.0f);
        }
        this.aR = 0.0f;
        this.aS = this.aT;
        TemporaryThreadManager.get().start(new n(this));
    }

    /* access modifiers changed from: private */
    public aj y() {
        if (this.aO == null) {
            this.aO = new aj();
        }
        return this.aO;
    }

    /* access modifiers changed from: private */
    public void c(int i) {
        boolean z2 = true;
        int i2 = 0;
        boolean z3 = m.a().a("update_newest_versioncode", 0) > com.tencent.assistant.utils.t.o();
        if (i <= 0 && !z3) {
            z2 = false;
        }
        if (this.I != null) {
            ImageView imageView = this.I;
            if (!z2) {
                i2 = 8;
            }
            imageView.setVisibility(i2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.bo) {
            this.bd = true;
            if (this.y != null) {
                this.y.c();
            }
            b(d(this.aE));
            z();
            B();
            L();
            this.bi.sendEmptyMessageDelayed(3, 300);
            if (this.af.get() == 2 && !this.aI && !this.bg) {
                this.bi.sendEmptyMessageDelayed(6, 300);
            }
            if (System.currentTimeMillis() - m.a().a("app_update_response_succ_time", -1L) >= 600000) {
                b.a().a(AppUpdateConst.RequestLaunchType.TYPE_ASSISTANT_RETRY, (Map<String, String>) null);
            }
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
            AstApp.i().k().addUIEventListener(1016, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED, this);
            AstApp.i().k().addUIEventListener(1019, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_START, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_APK_DEL_SUCCESS, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_APK_DEL_FAIL, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_SUCCESS, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_SPACE_CLEAN_SUCCESS, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_SPACE_CLEAN_FAIL, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_SAFE_SCAN_SUCCESS, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_SAFE_SCAN_FAIL, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0041, code lost:
        if (r0 != null) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0043, code lost:
        r0 = com.tencent.connect.common.Constants.STR_EMPTY;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0049, code lost:
        if (android.text.TextUtils.isEmpty(r0) != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0055, code lost:
        if (r0.equals(com.tencent.assistant.activity.SpaceCleanActivity.class.getName()) == false) goto L_0x006d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x005b, code lost:
        if (com.tencent.assistant.utils.ar.c() == false) goto L_0x0064;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005f, code lost:
        if (r8.aJ != false) goto L_0x0064;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0061, code lost:
        v();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0066, code lost:
        if (r8.aJ == false) goto L_0x006d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0068, code lost:
        com.tencent.assistant.utils.ar.b(0, com.tencent.assistant.activity.SpaceCleanActivity.class);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0077, code lost:
        if (r0.equals(com.tencent.assistant.activity.StartScanActivity.class.getName()) == false) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x007d, code lost:
        if (com.tencent.assistant.utils.ar.b() != 0) goto L_0x0084;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x007f, code lost:
        com.tencent.assistant.utils.ar.a(0, com.tencent.assistant.activity.StartScanActivity.class);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0088, code lost:
        if (com.tencent.assistant.utils.ar.i() != 0) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x008a, code lost:
        r8.an = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0096, code lost:
        if (r0.equals(com.tencent.assistant.activity.ApkMgrActivity.class.getName()) == false) goto L_0x00a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x009c, code lost:
        if (com.tencent.assistant.utils.ar.i() != 0) goto L_0x00a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x009e, code lost:
        r8.am = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00a4, code lost:
        if (com.tencent.assistant.utils.ar.i() != r2) goto L_0x00e0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00a6, code lost:
        r1 = r8.ai;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00a8, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00ab, code lost:
        if (r8.bc == false) goto L_0x00cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00b5, code lost:
        if (r8.ah.size() <= r8.aj) goto L_0x00cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00b7, code lost:
        r8.bi.postDelayed(new com.tencent.nucleus.manager.main.v(r8, r8.ah.get(r8.aj).b), 350);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00cf, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00e0, code lost:
        r8.aE += r2 - com.tencent.assistant.utils.ar.i();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00ed, code lost:
        if (r8.aE < 100) goto L_0x00f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00ef, code lost:
        r8.aE = 100;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00f1, code lost:
        D();
        r0 = getString(com.tencent.android.qqdownloader.R.string.manage_need_more);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x00fd, code lost:
        if (r8.aE != 100) goto L_0x0106;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00ff, code lost:
        r0 = getString(com.tencent.android.qqdownloader.R.string.manage_great);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0106, code lost:
        b(r0);
        A();
        r8.bi.postDelayed(new com.tencent.nucleus.manager.main.w(r8), 350);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void z() {
        /*
            r8 = this;
            r6 = 350(0x15e, double:1.73E-321)
            r5 = 100
            r4 = 0
            boolean r0 = r8.aH
            if (r0 != 0) goto L_0x011d
            int r0 = r8.aE
            if (r0 == r5) goto L_0x0118
            java.lang.Object r1 = r8.ai
            monitor-enter(r1)
            int r0 = r8.aj     // Catch:{ all -> 0x00dd }
            if (r0 < 0) goto L_0x001e
            int r0 = r8.aj     // Catch:{ all -> 0x00dd }
            java.util.List<com.tencent.assistantv2.model.a> r2 = r8.ah     // Catch:{ all -> 0x00dd }
            int r2 = r2.size()     // Catch:{ all -> 0x00dd }
            if (r0 < r2) goto L_0x0020
        L_0x001e:
            monitor-exit(r1)     // Catch:{ all -> 0x00dd }
        L_0x001f:
            return
        L_0x0020:
            java.util.List<com.tencent.assistantv2.model.a> r0 = r8.ah     // Catch:{ all -> 0x00dd }
            int r2 = r8.aj     // Catch:{ all -> 0x00dd }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x00dd }
            com.tencent.assistantv2.model.a r0 = (com.tencent.assistantv2.model.a) r0     // Catch:{ all -> 0x00dd }
            int r2 = r0.c     // Catch:{ all -> 0x00dd }
            java.lang.Class<?> r0 = r8.ag     // Catch:{ all -> 0x00dd }
            if (r0 != 0) goto L_0x00d5
            java.util.List<com.tencent.assistantv2.model.a> r0 = r8.ah     // Catch:{ all -> 0x00dd }
            int r3 = r8.aj     // Catch:{ all -> 0x00dd }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ all -> 0x00dd }
            com.tencent.assistantv2.model.a r0 = (com.tencent.assistantv2.model.a) r0     // Catch:{ all -> 0x00dd }
            java.lang.Class<?> r0 = r0.f2019a     // Catch:{ all -> 0x00dd }
            java.lang.String r0 = r0.getName()     // Catch:{ all -> 0x00dd }
        L_0x0040:
            monitor-exit(r1)     // Catch:{ all -> 0x00dd }
            if (r0 != 0) goto L_0x0045
            java.lang.String r0 = ""
        L_0x0045:
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 != 0) goto L_0x001f
            java.lang.Class<com.tencent.assistant.activity.SpaceCleanActivity> r1 = com.tencent.assistant.activity.SpaceCleanActivity.class
            java.lang.String r1 = r1.getName()
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x006d
            boolean r1 = com.tencent.assistant.utils.ar.c()
            if (r1 == 0) goto L_0x0064
            boolean r1 = r8.aJ
            if (r1 != 0) goto L_0x0064
            r8.v()
        L_0x0064:
            boolean r1 = r8.aJ
            if (r1 == 0) goto L_0x006d
            java.lang.Class<com.tencent.assistant.activity.SpaceCleanActivity> r1 = com.tencent.assistant.activity.SpaceCleanActivity.class
            com.tencent.assistant.utils.ar.b(r4, r1)
        L_0x006d:
            java.lang.Class<com.tencent.assistant.activity.StartScanActivity> r1 = com.tencent.assistant.activity.StartScanActivity.class
            java.lang.String r1 = r1.getName()
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x008c
            int r1 = com.tencent.assistant.utils.ar.b()
            if (r1 != 0) goto L_0x0084
            java.lang.Class<com.tencent.assistant.activity.StartScanActivity> r1 = com.tencent.assistant.activity.StartScanActivity.class
            com.tencent.assistant.utils.ar.a(r4, r1)
        L_0x0084:
            int r1 = com.tencent.assistant.utils.ar.i()
            if (r1 != 0) goto L_0x008c
            r8.an = r4
        L_0x008c:
            java.lang.Class<com.tencent.assistant.activity.ApkMgrActivity> r1 = com.tencent.assistant.activity.ApkMgrActivity.class
            java.lang.String r1 = r1.getName()
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x00a0
            int r0 = com.tencent.assistant.utils.ar.i()
            if (r0 != 0) goto L_0x00a0
            r8.am = r4
        L_0x00a0:
            int r0 = com.tencent.assistant.utils.ar.i()
            if (r0 != r2) goto L_0x00e0
            java.lang.Object r1 = r8.ai
            monitor-enter(r1)
            boolean r0 = r8.bc     // Catch:{ all -> 0x00d2 }
            if (r0 == 0) goto L_0x00cf
            java.util.List<com.tencent.assistantv2.model.a> r0 = r8.ah     // Catch:{ all -> 0x00d2 }
            int r0 = r0.size()     // Catch:{ all -> 0x00d2 }
            int r2 = r8.aj     // Catch:{ all -> 0x00d2 }
            if (r0 <= r2) goto L_0x00cf
            java.util.List<com.tencent.assistantv2.model.a> r0 = r8.ah     // Catch:{ all -> 0x00d2 }
            int r2 = r8.aj     // Catch:{ all -> 0x00d2 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x00d2 }
            com.tencent.assistantv2.model.a r0 = (com.tencent.assistantv2.model.a) r0     // Catch:{ all -> 0x00d2 }
            java.lang.String r0 = r0.b     // Catch:{ all -> 0x00d2 }
            android.os.Handler r2 = r8.bi     // Catch:{ all -> 0x00d2 }
            com.tencent.nucleus.manager.main.v r3 = new com.tencent.nucleus.manager.main.v     // Catch:{ all -> 0x00d2 }
            r3.<init>(r8, r0)     // Catch:{ all -> 0x00d2 }
            r4 = 350(0x15e, double:1.73E-321)
            r2.postDelayed(r3, r4)     // Catch:{ all -> 0x00d2 }
        L_0x00cf:
            monitor-exit(r1)     // Catch:{ all -> 0x00d2 }
            goto L_0x001f
        L_0x00d2:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00d2 }
            throw r0
        L_0x00d5:
            java.lang.Class<?> r0 = r8.ag     // Catch:{ all -> 0x00dd }
            java.lang.String r0 = r0.getName()     // Catch:{ all -> 0x00dd }
            goto L_0x0040
        L_0x00dd:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00dd }
            throw r0
        L_0x00e0:
            int r0 = r8.aE
            int r1 = com.tencent.assistant.utils.ar.i()
            int r1 = r2 - r1
            int r0 = r0 + r1
            r8.aE = r0
            int r0 = r8.aE
            if (r0 < r5) goto L_0x00f1
            r8.aE = r5
        L_0x00f1:
            r8.D()
            r0 = 2131362656(0x7f0a0360, float:1.8345099E38)
            java.lang.String r0 = r8.getString(r0)
            int r1 = r8.aE
            if (r1 != r5) goto L_0x0106
            r0 = 2131362655(0x7f0a035f, float:1.8345097E38)
            java.lang.String r0 = r8.getString(r0)
        L_0x0106:
            r8.b(r0)
            r8.A()
            android.os.Handler r0 = r8.bi
            com.tencent.nucleus.manager.main.w r1 = new com.tencent.nucleus.manager.main.w
            r1.<init>(r8)
            r0.postDelayed(r1, r6)
            goto L_0x001f
        L_0x0118:
            r8.M()
            goto L_0x001f
        L_0x011d:
            r8.aH = r4
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.nucleus.manager.main.AssistantTabActivity.z():void");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.bo) {
            if (this.y != null) {
                this.y.b();
            }
            this.bd = false;
            this.bi.removeMessages(5);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
            AstApp.i().k().removeUIEventListener(1016, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED, this);
            AstApp.i().k().removeUIEventListener(1019, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_START, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        }
    }

    /* access modifiers changed from: private */
    public String d(int i) {
        if (i >= 90 && i < 100) {
            return getString(R.string.manage_opt_90_99);
        }
        if (i >= 75 && i < 90) {
            return getString(R.string.manage_opt_75_90);
        }
        if (i > 0 && i < 75) {
            return getString(R.string.manage_opt_0_75);
        }
        if (i == 100) {
            return getString(R.string.manage_opt_100);
        }
        return Constants.STR_EMPTY;
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        this.R.setText(str);
        this.V.setText(str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A() {
        /*
            r3 = this;
            java.lang.Object r1 = r3.ai
            monitor-enter(r1)
            int r0 = r3.aj     // Catch:{ all -> 0x001c }
            if (r0 < 0) goto L_0x0011
            int r0 = r3.aj     // Catch:{ all -> 0x001c }
            java.util.List<com.tencent.assistantv2.model.a> r2 = r3.ah     // Catch:{ all -> 0x001c }
            int r2 = r2.size()     // Catch:{ all -> 0x001c }
            if (r0 < r2) goto L_0x0013
        L_0x0011:
            monitor-exit(r1)     // Catch:{ all -> 0x001c }
        L_0x0012:
            return
        L_0x0013:
            java.util.List<com.tencent.assistantv2.model.a> r0 = r3.ah     // Catch:{ all -> 0x001c }
            int r2 = r3.aj     // Catch:{ all -> 0x001c }
            r0.remove(r2)     // Catch:{ all -> 0x001c }
            monitor-exit(r1)     // Catch:{ all -> 0x001c }
            goto L_0x0012
        L_0x001c:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001c }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.nucleus.manager.main.AssistantTabActivity.A():void");
    }

    private void B() {
        if (this.aa == null) {
            this.aa = new ArrayList();
        }
        this.aa.clear();
        long currentTimeMillis = System.currentTimeMillis();
        this.Z = a(k.g());
        XLog.i("Jie", ">updateAppinfo cost=" + (System.currentTimeMillis() - currentTimeMillis));
        int size = this.Z.size();
        if (size >= 1) {
            if (size > this.ab) {
                size = this.ab;
            }
            int nextInt = new Random().nextInt(size);
            if (this.Z != null) {
                for (int i = nextInt; i < size; i++) {
                    this.aa.add(this.Z.get(i));
                }
                for (int i2 = 0; i2 < nextInt; i2++) {
                    this.aa.add(this.Z.get(i2));
                }
            }
        }
    }

    private void C() {
        int f = ((BaseActivity) this.v).f();
        this.z = (LinearLayout) findViewById(R.id.layout_title);
        this.w = (SimpleScrollView) findViewById(R.id.scroll_layout);
        this.x = (GridView) findViewById(R.id.sliding_frame_layout);
        this.x.setFocusable(false);
        this.y = new AssistantTabAdapter(this.v);
        this.x.setAdapter((ListAdapter) this.y);
        this.x.setOnItemClickListener(new x(this, f));
        this.H = (RelativeLayout) findViewById(R.id.layout_update);
        this.H.setTag(R.id.tma_st_slot_tag, "06_001");
        this.Y = (AssistantUpdateOverTurnView) findViewById(R.id.app_overturn);
        this.aq = (TextView) findViewById(R.id.tv_update_number);
        this.bn = PluginProxyUtils.getTypeFace();
        this.W = (TextView) findViewById(R.id.tv_score);
        this.X = (TextView) findViewById(R.id.tv_point);
        this.W.setTypeface(this.bn);
        this.X.setTypeface(this.bn);
        this.A = (FrameLayout) findViewById(R.id.layout_score);
        this.A.setTag(R.id.tma_st_slot_tag, "03_001");
        this.B = (FrameLayout) findViewById(R.id.layout_circle);
        this.J = (ImageView) findViewById(R.id.iv_outer_circle);
        this.K = (ImageView) findViewById(R.id.iv_inner_circle);
        this.L = (ImageView) findViewById(R.id.iv_admin_circle);
        this.M = (ImageView) findViewById(R.id.pb_scanning);
        this.C = (RelativeLayout) findViewById(R.id.layout_setting);
        this.C.setTag(R.id.tma_st_slot_tag, "08_001");
        this.D = (RelativeLayout) findViewById(R.id.layout_share);
        this.D.setTag(R.id.tma_st_slot_tag, "08_002");
        this.E = (RelativeLayout) findViewById(R.id.layout_about);
        this.E.setTag(R.id.tma_st_slot_tag, "08_003");
        this.F = (ScaleRelativeLayout) findViewById(R.id.layout_toast);
        this.G = (RelativeLayout) findViewById(R.id.layout_toast_info);
        this.R = (TextView) findViewById(R.id.tv_tips);
        this.V = (TextView) findViewById(R.id.tv_tips2);
        b(this.aG);
        this.S = (TextView) findViewById(R.id.tv_toast);
        this.T = (TextView) findViewById(R.id.tv_no_update);
        this.U = (TextView) findViewById(R.id.tv_tips_score);
        this.Q = (ViewSwitcher) findViewById(R.id.switcher_tips);
        this.Q.setTag(R.id.tma_st_slot_tag, "03_001");
        this.C.setOnClickListener(this.br);
        this.D.setOnClickListener(this.br);
        this.E.setOnClickListener(this.br);
        this.A.setOnClickListener(this.br);
        this.F.setOnClickListener(this.br);
        this.F.a(this.bq);
        this.H.setOnClickListener(this.br);
        this.z.setOnClickListener(this.br);
        this.Q.setOnClickListener(this.br);
        this.I = (ImageView) findViewById(R.id.about_promot);
        this.w.a(this.bp);
        this.w.a(this.aS);
    }

    /* access modifiers changed from: private */
    public void a(float f) {
        try {
            this.W.setText(String.valueOf(Math.round(f)));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void D() {
        if (this.aP == null || !this.aP.a()) {
            if (this.be == null) {
                this.be = new aq((float) this.aF, (float) this.aE, new aa(this));
            }
            this.be.a((float) this.aF, (float) this.aE);
            this.be.setDuration((long) Math.min(Math.abs(this.aF - this.aE) * 100, (int) EventDispatcherEnum.CACHE_EVENT_START));
            this.z.startAnimation(this.be);
            this.U.setText(getString(R.string.manage_score, new Object[]{Integer.valueOf(this.aE)}));
            return;
        }
        a((float) this.aE);
    }

    private void F() {
        this.z.clearAnimation();
    }

    private void b(boolean z2) {
        a((float) this.aE);
        if (this.aP == null || this.aP.a() || z2) {
            if (this.bi != null) {
                this.bi.removeMessages(6);
            }
            this.J.clearAnimation();
            this.K.clearAnimation();
            this.J.setVisibility(8);
            this.K.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void G() {
        if (this.bg) {
            return;
        }
        if (this.aP == null || !this.aP.a()) {
            this.J.setVisibility(0);
            this.K.setVisibility(0);
            this.J.startAnimation(this.N);
            this.K.startAnimation(this.O);
            if (this.bi != null && !isFinishing()) {
                this.bi.removeMessages(6);
                this.bi.sendEmptyMessageDelayed(6, 4000);
            }
        }
    }

    private boolean H() {
        if ((this.aP != null && this.aP.a()) || this.M.getVisibility() == 0) {
            return false;
        }
        this.J.startAnimation(this.P);
        this.L.setVisibility(8);
        this.M.setVisibility(0);
        try {
            this.M.setBackgroundResource(R.drawable.admin_loadcircle);
        } catch (Throwable th) {
            com.tencent.assistant.manager.t.a().b();
        }
        this.M.startAnimation(this.aL);
        b(getString(R.string.manage_opt));
        return true;
    }

    /* access modifiers changed from: private */
    public void c(boolean z2) {
        if (this.aP == null || this.aP.a() || z2) {
            if (this.J != null) {
                this.J.clearAnimation();
            }
            if (this.M != null) {
                this.M.clearAnimation();
                this.M.setVisibility(8);
            }
            if (this.L != null) {
                this.L.setVisibility(0);
            }
        }
    }

    private void I() {
        this.aL = AnimationUtils.loadAnimation(this, R.anim.speedcircle);
        this.N = AnimationUtils.loadAnimation(this, R.anim.mgr_outter_circle_scale);
        this.N.setAnimationListener(new ab(this));
        this.O = AnimationUtils.loadAnimation(this, R.anim.mgr_inner_circle_scale);
        this.O.setAnimationListener(new d(this));
        this.P = AnimationUtils.loadAnimation(this, R.anim.mgr_click_circle_scale);
        this.aK = AnimationUtils.loadAnimation(this, R.anim.mgr_fade_in);
        this.aK.setFillAfter(true);
        this.P.setAnimationListener(new e(this));
        this.aM = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.mgr_switcher_in);
        this.aN = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.mgr_switcher_out);
        this.aM.setAnimationListener(new f(this));
        O();
        P();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.main.AssistantTabActivity.a(boolean, long):void
     arg types: [int, long]
     candidates:
      com.tencent.nucleus.manager.main.AssistantTabActivity.a(com.tencent.nucleus.manager.main.AssistantTabActivity, java.lang.Class):java.lang.Class
      com.tencent.nucleus.manager.main.AssistantTabActivity.a(com.tencent.nucleus.manager.main.AssistantTabActivity, float):void
      com.tencent.nucleus.manager.main.AssistantTabActivity.a(com.tencent.nucleus.manager.main.AssistantTabActivity, int):void
      com.tencent.nucleus.manager.main.AssistantTabActivity.a(com.tencent.nucleus.manager.main.AssistantTabActivity, java.lang.String):void
      com.tencent.nucleus.manager.main.AssistantTabActivity.a(com.tencent.nucleus.manager.main.AssistantTabActivity, boolean):boolean
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.nucleus.manager.main.AssistantTabActivity.a(boolean, long):void */
    public void t() {
        J();
        b(true);
        if (this.aP != null && this.aP.a()) {
            a(true, (long) ((1.0f - this.aP.b()) * 500.0f));
            this.bb = true;
        } else if (H() && !this.bg) {
            if (this.aI) {
                this.bi.sendEmptyMessageDelayed(7, 1000);
                return;
            }
            this.bg = true;
            this.bi.sendEmptyMessageDelayed(7, 35000);
            TemporaryThreadManager.get().start(new h(this));
            TemporaryThreadManager.get().start(new i(this));
            TemporaryThreadManager.get().start(new j(this));
            TemporaryThreadManager.get().start(new k(this));
        }
    }

    private void J() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", com.tencent.assistant.utils.t.g());
        XLog.d("beacon", "beacon report >> click_manage_circle. " + hashMap.toString());
        com.tencent.beacon.event.a.a("click_manage_circle", true, -1, -1, hashMap, true);
    }

    /* access modifiers changed from: private */
    public void K() {
        Intent intent = new Intent(this.v, UpdateListActivity.class);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        this.v.startActivity(intent);
    }

    public int l() {
        return 0;
    }

    public int f() {
        return STConst.ST_PAGE_ASSISTANT;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.bo) {
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_APK_DEL_SUCCESS, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_APK_DEL_FAIL, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_SUCCESS, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_SPACE_CLEAN_SUCCESS, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_SPACE_CLEAN_FAIL, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_SAFE_SCAN_SUCCESS, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_SAFE_SCAN_FAIL, this);
            b(true);
            this.bi.removeCallbacksAndMessages(null);
            this.w.a();
            y().g();
            n.a().unregister(this.n);
            SpaceScanManager.a().b(this.bs);
        }
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 3:
                c(n.a().b());
                break;
            case 5:
                if (this.ao) {
                    this.Y.a();
                    B();
                    this.ao = false;
                }
                L();
                break;
            case 6:
                this.bh = 2;
                G();
                break;
            case 7:
                this.af.set(0);
                N();
                break;
        }
        return true;
    }

    private void L() {
        int i = k.i();
        this.ap = i;
        if (i > 0) {
            this.aq.setVisibility(0);
            this.Y.setVisibility(0);
            this.T.setVisibility(8);
            if (i > 99) {
                this.aq.setText("99+");
            } else {
                this.aq.setText(k.i() + Constants.STR_EMPTY);
            }
        } else {
            this.aq.setVisibility(8);
            this.Y.setVisibility(8);
            this.T.setVisibility(0);
        }
        if (this.aa.size() != 0) {
            AppUpdateInfo remove = this.aa.remove(0);
            this.aa.add(remove);
            this.Y.a(remove);
            if (this.aa.size() > 1 && this.bi != null) {
                this.bi.sendEmptyMessageDelayed(5, this.ac);
            }
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD:
            case 1016:
            case EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED:
            case 1019:
            case EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_START:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE:
                this.ao = true;
                if (this.aq.getVisibility() != 0 || this.ap < 2) {
                    if (this.bi != null) {
                        this.bi.removeMessages(5);
                    }
                    B();
                    L();
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_INSTALL:
                if ((message.obj instanceof String) && AppConst.TENCENT_MOBILE_MANAGER_PKGNAME.equals((String) message.obj)) {
                    v();
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_SUCCESS:
                this.af.decrementAndGet();
                long longValue = ((Long) message.obj).longValue();
                XLog.i("Jie", ">>memory clean success>>memory free size=" + longValue + " opcount=" + this.af.get());
                this.ak = longValue * 1024;
                N();
                return;
            case EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL:
                this.af.decrementAndGet();
                N();
                return;
            case EventDispatcherEnum.UI_EVENT_MGR_SPACE_CLEAN_SUCCESS:
                this.af.decrementAndGet();
                this.al = ((Long) message.obj).longValue();
                N();
                return;
            case EventDispatcherEnum.UI_EVENT_MGR_SPACE_CLEAN_FAIL:
                this.af.decrementAndGet();
                N();
                return;
            case EventDispatcherEnum.UI_EVENT_MGR_APK_DEL_SUCCESS:
                this.am = ((Integer) message.obj).intValue();
                return;
            case EventDispatcherEnum.UI_EVENT_MGR_APK_DEL_FAIL:
            case EventDispatcherEnum.UI_EVENT_MGR_SAFE_SCAN_FAIL:
            default:
                return;
            case EventDispatcherEnum.UI_EVENT_MGR_SAFE_SCAN_SUCCESS:
                this.an = ((Integer) message.obj).intValue();
                return;
        }
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        if (!this.bd) {
            this.bc = true;
            return;
        }
        this.bc = false;
        if (this.F != null) {
            this.F.a(500);
            this.F.setVisibility(0);
            this.S.setText(str);
        }
        this.bi.postDelayed(new l(this), 4000);
    }

    /* access modifiers changed from: private */
    public boolean M() {
        if (this.F == null || this.F.b() == -1) {
            return false;
        }
        this.F.a(400);
        this.F.setVisibility(-1);
        return true;
    }

    private void N() {
        if (this.af.get() == 0) {
            synchronized (this.ai) {
                this.ah.clear();
            }
            ar.e();
            ar.f();
            runOnUiThread(new m(this));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: private */
    public void a(float f, float f2, float f3) {
        boolean z2 = true;
        if (f3 != 0.0f && this.aT != 0.0f) {
            F();
            b(f2 > 0.0f);
            if (f2 <= 0.0f) {
                z2 = false;
            }
            c(z2);
            O();
            P();
            float min = Math.min(Math.max(1.0f - ((f2 - f) / this.aT), this.aR), 1.0f);
            float min2 = Math.min(Math.max(1.0f - (f2 / this.aT), this.aR), 1.0f);
            this.aP.a(min, min2);
            this.aQ.a(min, min2);
            this.aP.setDuration(0);
            this.aQ.setDuration(0);
            this.A.startAnimation(this.aP);
            this.B.startAnimation(this.aQ);
        }
    }

    /* access modifiers changed from: private */
    public void d(boolean z2) {
        if (!z2) {
            this.Q.getChildAt(1).setVisibility(4);
            this.Q.getChildAt(1).clearAnimation();
            this.Q.getChildAt(0).clearAnimation();
            this.Q.setInAnimation(null);
            this.Q.setOutAnimation(null);
            this.Q.setDisplayedChild(0);
        } else if (this.aW) {
            this.U.setText(getString(R.string.manage_score, new Object[]{Integer.valueOf(this.aE)}));
            this.Q.getChildAt(1).setVisibility(0);
            this.Q.setInAnimation(this.aM);
            this.Q.setOutAnimation(this.aN);
            this.Q.setDisplayedChild(1);
        }
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        if (!this.bj) {
            u();
        }
    }

    public void u() {
        boolean z2 = false;
        int[] iArr = new int[2];
        this.w.getLocationInWindow(iArr);
        if (this.aU == 0) {
            this.aU = iArr[1];
        }
        if (this.aV == 0) {
            this.aV = this.aU + this.w.getHeight();
        }
        if (((int) ((((float) ((int) ((((float) iArr[1]) + this.aT) - ((float) this.A.getHeight())))) - this.aT) + ((float) this.w.getChildAt(0).getMeasuredHeight()))) > this.aV) {
            z2 = true;
        }
        this.bk = z2;
        this.bj = true;
        this.w.a(this.bk);
    }

    private a O() {
        if (this.aP == null) {
            this.aP = new a(this.A, 1.0f, 1.0f, true);
            this.aP.setFillAfter(true);
            this.aP.a(new o(this));
        }
        return this.aP;
    }

    private a P() {
        if (this.aQ == null) {
            this.aQ = new a(this.B, 1.0f, 1.0f, false);
            this.aQ.setFillAfter(true);
        }
        return this.aQ;
    }

    /* access modifiers changed from: private */
    public void a(boolean z2, long j) {
        float f = 1.0f;
        if (z2) {
            d(!z2);
        }
        F();
        b(false);
        c(false);
        O();
        P();
        this.aP.a(z2 ? 1.0f : this.aR);
        a aVar = this.aQ;
        if (!z2) {
            f = this.aR;
        }
        aVar.a(f);
        this.aP.setDuration(j);
        this.aQ.setDuration(j);
        this.A.startAnimation(this.aP);
        this.B.startAnimation(this.aQ);
    }

    public void v() {
        if (SpaceScanManager.a().e()) {
            this.aJ = true;
            ar.b(0, SpaceCleanActivity.class);
            z();
            return;
        }
        SpaceScanManager.a().a(this.bs);
        SpaceScanManager.a().c();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 != i) {
            return super.onKeyDown(i, keyEvent);
        }
        finish();
        return true;
    }

    public STPageInfo n() {
        this.p.f2060a = STConst.ST_PAGE_ASSISTANT;
        return this.p;
    }

    /* access modifiers changed from: private */
    public STInfoV2 a(String str, String str2, int i) {
        STInfoV2 sTInfoV2 = new STInfoV2(this.p.f2060a, str, this.p.c, this.p.d, i);
        sTInfoV2.status = str2;
        sTInfoV2.slotId = str;
        sTInfoV2.actionId = i;
        return sTInfoV2;
    }

    /* access modifiers changed from: private */
    public void b(String str, String str2) {
        STInfoV2 a2 = a(str, STConst.ST_STATUS_DEFAULT, 100);
        a2.extraData = str2;
        com.tencent.assistantv2.st.l.a(a2);
    }

    /* access modifiers changed from: private */
    public void Q() {
        if (!this.bf && this.C.getTop() < com.tencent.assistant.utils.t.c) {
            this.bf = true;
            b("08_003", (String) null);
            b("08_001", (String) null);
            b("08_002", (String) null);
        }
    }

    public List<AppUpdateInfo> a(List<AppUpdateInfo> list) {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        ArrayList arrayList5 = new ArrayList();
        ArrayList arrayList6 = new ArrayList();
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        for (AppUpdateInfo next : list) {
            if (d(next.f1162a) == 1) {
                arrayList2.add(next);
            } else {
                arrayList3.add(next);
            }
        }
        Iterator it = arrayList2.iterator();
        while (it.hasNext()) {
            AppUpdateInfo appUpdateInfo = (AppUpdateInfo) it.next();
            k.a(appUpdateInfo, simpleAppModel);
            AppConst.AppState d = k.d(simpleAppModel);
            if (AppConst.AppState.DOWNLOADED == d) {
                z4 = true;
            } else {
                z4 = false;
            }
            if (AppConst.AppState.INSTALLING == d) {
                z5 = true;
            } else {
                z5 = false;
            }
            if (AppConst.AppState.INSTALLED == d) {
                z6 = true;
            } else {
                z6 = false;
            }
            if (z4 || z5 || z6) {
                arrayList4.add(appUpdateInfo);
            } else {
                arrayList5.add(appUpdateInfo);
            }
        }
        arrayList.addAll(arrayList4);
        arrayList.addAll(arrayList5);
        if (arrayList.size() >= this.ab) {
            return arrayList;
        }
        Iterator it2 = arrayList3.iterator();
        while (it2.hasNext()) {
            AppUpdateInfo appUpdateInfo2 = (AppUpdateInfo) it2.next();
            k.a(appUpdateInfo2, simpleAppModel);
            AppConst.AppState d2 = k.d(simpleAppModel);
            boolean z7 = AppConst.AppState.DOWNLOADED == d2;
            if (AppConst.AppState.INSTALLING == d2) {
                z2 = true;
            } else {
                z2 = false;
            }
            if (AppConst.AppState.INSTALLED == d2) {
                z3 = true;
            } else {
                z3 = false;
            }
            if (z7 || z2 || z3) {
                arrayList.add(appUpdateInfo2);
            } else {
                arrayList6.add(appUpdateInfo2);
            }
            if (arrayList.size() >= this.ab) {
                return arrayList;
            }
        }
        arrayList.addAll(arrayList6);
        return arrayList;
    }

    private int d(String str) {
        int i = 0;
        ArrayList<Integer> c = j.b().c(str);
        if (c != null && c.size() > 0) {
            i = c.get(0).intValue();
        }
        return e(i);
    }

    private int e(int i) {
        switch (i) {
            case 1:
            case 2:
                return 1;
            default:
                return 2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void R() {
        if (!m.a().M()) {
            try {
                Bitmap bitmap = ((BitmapDrawable) getResources().getDrawable(R.drawable.icon_shortcut_setting)).getBitmap();
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("tmast://mobilemanage"));
                intent.setFlags(67108864);
                intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, S());
                intent.putExtra(com.tencent.assistant.a.a.G, true);
                e.a(this, getString(R.string.mobile_manage_short_cut), intent);
                e.a(this, bitmap, getString(R.string.mobile_manage_short_cut), intent);
                TemporaryThreadManager.get().start(new u(this));
                com.tencent.assistantv2.st.l.a(new STInfoV2(204005, "03_001", 2000, STConst.ST_DEFAULT_SLOT, 100));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    private int S() {
        return STConst.ST_PAGE_SPACECLEAN_DESK_PAGEID;
    }
}
