package com.tencent.nucleus.manager;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.ad;
import com.tencent.assistant.module.callback.h;
import com.tencent.assistant.module.k;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.m;
import com.tencent.assistantv2.component.AppStateButtonV5;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.spaceclean.SpaceScanManager;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean;

/* compiled from: ProGuard */
public class MobileManagerInstallActivity extends BaseActivity implements UIEventListener, NetworkMonitor.ConnectivityChangeListener, h {
    private final int A = -2;
    private final int B = 10;
    private final int C = -10;
    private final int D = 20;
    private final int E = 21;
    private final String F = "000116083137393932323936";
    /* access modifiers changed from: private */
    public int G = -1;
    /* access modifiers changed from: private */
    public SimpleAppModel H;
    /* access modifiers changed from: private */
    public ad I = new ad();
    /* access modifiers changed from: private */
    public Handler J = new f(this);
    private a K = new i(this);
    private SecondNavigationTitleViewV5 n;
    private RelativeLayout u;
    private ImageView v;
    /* access modifiers changed from: private */
    public AppStateButtonV5 w;
    /* access modifiers changed from: private */
    public Button x;
    private LoadingView y;
    private final int z = -1;

    public int f() {
        return STConst.ST_PAGE_GARBAGE_UNINSTALL_STEWARD;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_mobile_manager_install);
        w();
        t();
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        AstApp.i().k().addUIEventListener(1013, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this);
        SpaceScanManager.a().a(this.K);
        t.a().a(this);
        u();
    }

    private void t() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String string = extras.getString("toast_message");
            if (!TextUtils.isEmpty(string)) {
                Toast.makeText(this, string, 0).show();
            }
        }
    }

    public void handleUIEvent(Message message) {
        XLog.d("miles", "MobileMnagerInstallActivity.  handleUIEvent. msg.what=" + message.what);
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_INSTALL:
            case 1013:
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC:
                String str = Constants.STR_EMPTY;
                String str2 = Constants.STR_EMPTY;
                if (message.obj instanceof String) {
                    str = (String) message.obj;
                    str2 = (String) message.obj;
                } else if (message.obj instanceof InstallUninstallTaskBean) {
                    InstallUninstallTaskBean installUninstallTaskBean = (InstallUninstallTaskBean) message.obj;
                    String str3 = installUninstallTaskBean.downloadTicket;
                    str2 = installUninstallTaskBean.packageName;
                    str = str3;
                }
                if ((!TextUtils.isEmpty(str) && str.equals(this.H.q())) || (!TextUtils.isEmpty(str2) && str2.equals(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME))) {
                    SpaceScanManager.a().c();
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_UNINSTALL:
                if ((message.obj instanceof String) && ((String) message.obj).equals(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME)) {
                    this.J.sendEmptyMessage(-1);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.n != null) {
            this.n.l();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.n != null) {
            this.n.m();
        }
    }

    private void u() {
        if (this.H == null) {
            this.H = new SimpleAppModel();
            this.H.c = AppConst.TENCENT_MOBILE_MANAGER_PKGNAME;
            this.H.ac = "000116083137393932323936";
        }
        this.w.a(this.H);
        TemporaryThreadManager.get().start(new e(this));
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        int i = 0;
        if (this.u != null) {
            this.u.setVisibility(z2 ? 8 : 0);
        }
        if (this.y != null) {
            LoadingView loadingView = this.y;
            if (!z2) {
                i = 8;
            }
            loadingView.setVisibility(i);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.pangu.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.plugin.PluginDownloadInfo, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.k.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.m, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    /* access modifiers changed from: private */
    public void v() {
        DownloadInfo downloadInfo = null;
        DownloadInfo a2 = DownloadProxy.a().a(this.H);
        StatInfo a3 = a.a(STInfoBuilder.buildSTInfo(this, this.H, STConst.ST_DEFAULT_SLOT, 200, null));
        if (a2 == null || !a2.needReCreateInfo(this.H)) {
            downloadInfo = a2;
        } else {
            DownloadProxy.a().b(a2.downloadTicket);
        }
        if (downloadInfo == null) {
            downloadInfo = DownloadInfo.createDownloadInfo(this.H, a3, this.w);
        } else {
            downloadInfo.updateDownloadInfoStatInfo(a3);
        }
        switch (j.f2926a[k.a(downloadInfo, false, false).ordinal()]) {
            case 1:
            case 2:
                com.tencent.pangu.download.a.a().a(downloadInfo);
                return;
            case 3:
            case 4:
                com.tencent.pangu.download.a.a().b(downloadInfo.downloadTicket);
                return;
            case 5:
                com.tencent.pangu.download.a.a().b(downloadInfo);
                return;
            case 6:
                if (!downloadInfo.isDownloadFileExist()) {
                    AppConst.TwoBtnDialogInfo b = b(downloadInfo);
                    if (b != null) {
                        DialogUtils.show2BtnDialog(b);
                        return;
                    }
                    return;
                }
                com.tencent.pangu.download.a.a().d(downloadInfo);
                return;
            case 7:
                com.tencent.pangu.download.a.a().c(downloadInfo);
                return;
            case 8:
            case 9:
                com.tencent.pangu.download.a.a().a(downloadInfo);
                return;
            case 10:
                Toast.makeText(this, (int) R.string.tips_slicent_install, 0).show();
                return;
            case 11:
                Toast.makeText(this, (int) R.string.tips_slicent_uninstall, 0).show();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public CharSequence a(DownloadInfo downloadInfo) {
        String format;
        boolean z2 = ApkResourceManager.getInstance().getInstalledApkInfo(downloadInfo.packageName) != null;
        if (z2 && downloadInfo.isSslUpdate()) {
            format = String.format(getResources().getString(R.string.mobile_rubbish_clear_btn_update), at.a(downloadInfo.sllFileSize));
        } else if (!z2 || downloadInfo.isUpdate != 1) {
            format = String.format(getResources().getString(R.string.mobile_rubbish_clear_btn_download), at.a(downloadInfo.fileSize));
        } else {
            format = String.format(getResources().getString(R.string.mobile_rubbish_clear_btn_update), at.a(downloadInfo.fileSize));
        }
        SpannableString spannableString = new SpannableString(format);
        spannableString.setSpan(new AbsoluteSizeSpan(14, true), 4, format.length(), 33);
        return spannableString;
    }

    private AppConst.TwoBtnDialogInfo b(DownloadInfo downloadInfo) {
        h hVar = new h(this, downloadInfo);
        hVar.hasTitle = true;
        hVar.titleRes = getResources().getString(R.string.down_page_dialog_title);
        hVar.contentRes = getResources().getString(R.string.down_page_dialog_content);
        hVar.lBtnTxtRes = getResources().getString(R.string.down_page_dialog_left_del);
        hVar.rBtnTxtRes = getResources().getString(R.string.down_page_dialog_right_down);
        return hVar;
    }

    private void w() {
        this.n = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.n.a(this);
        this.n.b(getString(R.string.rubbish_clear_title));
        this.n.d();
        this.u = (RelativeLayout) findViewById(R.id.layout_main);
        this.v = (ImageView) findViewById(R.id.image_banner);
        try {
            Bitmap a2 = m.a(R.drawable.rubbish_clean_banner);
            if (a2 != null && !a2.isRecycled()) {
                this.v.setImageBitmap(a2);
            }
        } catch (Throwable th) {
            t.a().b();
        }
        this.w = (AppStateButtonV5) findViewById(R.id.btn_download);
        this.x = (Button) findViewById(R.id.btn_no_net);
        this.w.a((int) getResources().getDimension(R.dimen.app_detail_float_bar_btn_height));
        this.w.b(16);
        this.y = (LoadingView) findViewById(R.id.loading);
        b(true);
    }

    public void onGetAppInfoSuccess(int i, int i2, AppSimpleDetail appSimpleDetail) {
        if (appSimpleDetail == null) {
            this.J.sendEmptyMessage(-10);
            return;
        }
        this.H = k.a(appSimpleDetail);
        this.J.sendEmptyMessage(10);
    }

    public void onGetAppInfoFail(int i, int i2) {
        this.J.sendEmptyMessage(-10);
    }

    public void onConnected(APN apn) {
        if (!SpaceScanManager.a().e()) {
            SpaceScanManager.a().c();
        }
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        AstApp.i().k().removeUIEventListener(1013, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this);
        SpaceScanManager.a().b(this.K);
        t.a().b(this);
        super.onDestroy();
    }
}
