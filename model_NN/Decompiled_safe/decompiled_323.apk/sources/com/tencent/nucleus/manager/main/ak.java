package com.tencent.nucleus.manager.main;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: ProGuard */
class ak implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Method f2939a;
    final /* synthetic */ Object b;
    final /* synthetic */ aj c;

    ak(aj ajVar, Method method, Object obj) {
        this.c = ajVar;
        this.f2939a = method;
        this.b = obj;
    }

    public void run() {
        try {
            this.f2939a.invoke(this.b, false);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        } catch (InvocationTargetException e3) {
            e3.printStackTrace();
        }
    }
}
