package com.tencent.nucleus.manager.floatingwindow;

import com.tencent.assistant.m;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.t;
import com.tencent.nucleus.manager.spaceclean.SpaceScanManager;

/* compiled from: ProGuard */
class p implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ n f2912a;

    p(n nVar) {
        this.f2912a = nVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public void run() {
        boolean z = true;
        if (t.z()) {
            XLog.e("floatingwindow", "is xiaomi rom. don't show floating window.");
        } else if (m.a().o() || (m.a().a("key_float_window_manual_state", true) && m.a().a("key_float_window_should_auto_show", false) && !SpaceScanManager.a().k())) {
            XLog.d("floatingwindow", "满足悬浮窗开启条件，启动悬浮窗服务...");
            m.a().f(true);
            this.f2912a.o();
        } else {
            StringBuilder append = new StringBuilder().append("不满足悬浮窗开启条件 >> \n设置中开关状态：").append(m.a().o()).append("\n用户是否主动关闭了悬浮窗：");
            if (m.a().a("key_float_window_manual_state", true)) {
                z = false;
            }
            XLog.e("floatingwindow", append.append(z).append("\n后台配置的开关状态：").append(m.a().a("key_float_window_should_auto_show", false)).append("\n手管是否已安装：").append(SpaceScanManager.a().k()).toString());
        }
    }
}
