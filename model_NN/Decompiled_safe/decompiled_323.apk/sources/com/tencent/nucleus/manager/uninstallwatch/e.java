package com.tencent.nucleus.manager.uninstallwatch;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Process;
import android.os.SystemClock;
import com.qq.AppService.AstApp;
import com.qq.util.p;
import com.tencent.assistant.Global;
import com.tencent.assistant.m;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.t;
import com.tencent.beacon.event.a;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

/* compiled from: ProGuard */
public class e {
    private static e b = new e();
    private static boolean c = false;
    private static Object d = new Object();

    /* renamed from: a  reason: collision with root package name */
    private ScheduledExecutorService f3052a = null;
    /* access modifiers changed from: private */
    public volatile boolean e = false;

    private e() {
    }

    public static synchronized e a() {
        e eVar;
        synchronized (e.class) {
            eVar = b;
        }
        return eVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
        r2 = r5.f3052a;
        r3 = new com.tencent.nucleus.manager.uninstallwatch.f(r5, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
        if (r7 == false) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0027, code lost:
        r0 = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0029, code lost:
        r2.schedule(r3, r0, java.util.concurrent.TimeUnit.SECONDS);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0032, code lost:
        r0 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(android.content.Context r6, boolean r7) {
        /*
            r5 = this;
            java.util.concurrent.ScheduledExecutorService r0 = r5.f3052a
            if (r0 != 0) goto L_0x0011
            com.tencent.assistant.utils.q r0 = new com.tencent.assistant.utils.q
            java.lang.String r1 = "watchManager"
            r0.<init>(r1)
            java.util.concurrent.ScheduledExecutorService r0 = java.util.concurrent.Executors.newSingleThreadScheduledExecutor(r0)
            r5.f3052a = r0
        L_0x0011:
            java.lang.Object r1 = com.tencent.nucleus.manager.uninstallwatch.e.d
            monitor-enter(r1)
            boolean r0 = com.tencent.nucleus.manager.uninstallwatch.e.c     // Catch:{ all -> 0x002f }
            if (r0 == 0) goto L_0x001a
            monitor-exit(r1)     // Catch:{ all -> 0x002f }
        L_0x0019:
            return
        L_0x001a:
            r0 = 1
            com.tencent.nucleus.manager.uninstallwatch.e.c = r0     // Catch:{ all -> 0x002f }
            monitor-exit(r1)     // Catch:{ all -> 0x002f }
            java.util.concurrent.ScheduledExecutorService r2 = r5.f3052a
            com.tencent.nucleus.manager.uninstallwatch.f r3 = new com.tencent.nucleus.manager.uninstallwatch.f
            r3.<init>(r5, r6)
            if (r7 == 0) goto L_0x0032
            r0 = 3
        L_0x0029:
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.SECONDS
            r2.schedule(r3, r0, r4)
            goto L_0x0019
        L_0x002f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x002f }
            throw r0
        L_0x0032:
            r0 = 0
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.nucleus.manager.uninstallwatch.e.a(android.content.Context, boolean):void");
    }

    /* access modifiers changed from: private */
    public void b(Context context) {
        XLog.i("uninstall", "killOldWatchProcess");
        int i = 0;
        try {
            i = Integer.parseInt(FileUtil.readFromAppData("uninstall_process"));
        } catch (Exception e2) {
        }
        XLog.i("uninstall", "killOldWatchProcess pid=" + i);
        if (i != 0) {
            String a2 = p.a(i);
            if (a2 == null) {
                a2 = p.a(context, i);
            }
            if (a2 == null || (!a2.equals("yyb_uninstall") && !a2.equals("com.tencent.android.qqdownloader:uninstall"))) {
                XLog.d("uninstall", "old watch process doesn't exist !!");
                return;
            }
            Process.killProcess(i);
            XLog.d("uninstall", "kill old watch process ...");
        }
    }

    private static void b() {
        List<Integer> a2 = p.a("libwatch.so");
        if (a2 != null && !a2.isEmpty()) {
            for (Integer next : a2) {
                Process.killProcess(next.intValue());
                XLog.i("uninstall", "<java> kill pid: " + next);
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x008a, code lost:
        if (r4.endsWith("libwatch.so") != false) goto L_0x008c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void c(android.content.Context r8) {
        /*
            r0 = 1
            r7 = 2
            r1 = 0
            com.tencent.nucleus.manager.uninstallwatch.h r3 = new com.tencent.nucleus.manager.uninstallwatch.h
            r3.<init>()
            boolean r2 = r3.c()
            if (r2 == 0) goto L_0x0017
            boolean r2 = com.tencent.nucleus.manager.uninstallwatch.a.b(r8)
            if (r2 == 0) goto L_0x0017
            b()
        L_0x0017:
            boolean r2 = r3.c()
            if (r2 == 0) goto L_0x00e2
            boolean r2 = com.tencent.nucleus.manager.uninstallwatch.a.b(r8)
            if (r2 == 0) goto L_0x00d9
            java.lang.String r2 = "uninstall"
            java.lang.String r4 = "<java> update watch params ..."
            com.tencent.assistant.utils.XLog.d(r2, r4)
            java.lang.String r2 = com.tencent.nucleus.manager.uninstallwatch.a.e(r8)
            java.lang.String r4 = com.tencent.nucleus.manager.uninstallwatch.a.b()
            java.lang.String r5 = r8.getPackageName()
            r3.b(r2, r4, r5)
            com.tencent.nucleus.manager.uninstallwatch.a.c(r8)
        L_0x003c:
            com.tencent.nucleus.manager.uninstallwatch.b r2 = com.tencent.nucleus.manager.uninstallwatch.a.d(r8)
            if (r2 == 0) goto L_0x0048
            boolean r4 = c()
            if (r4 != 0) goto L_0x0052
        L_0x0048:
            if (r2 == 0) goto L_0x0199
            java.lang.String r4 = r2.f3051a
            boolean r4 = com.tencent.nucleus.manager.uninstallwatch.a.b(r4)
            if (r4 == 0) goto L_0x0199
        L_0x0052:
            r3.a(r2)
            java.lang.String r4 = r2.f3051a
            com.tencent.nucleus.manager.uninstallwatch.a.a(r4)
            java.lang.String r4 = "uninstall"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "<java> setCallBrowser = "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r2 = r2.b
            java.lang.StringBuilder r2 = r5.append(r2)
            java.lang.String r2 = r2.toString()
            com.tencent.assistant.utils.XLog.d(r4, r2)
        L_0x0074:
            int r2 = f()
            if (r2 <= 0) goto L_0x01a5
            java.lang.String r4 = com.qq.util.p.c(r2)
            boolean r5 = android.text.TextUtils.isEmpty(r4)
            if (r5 != 0) goto L_0x01a5
            java.lang.String r5 = "libwatch.so"
            boolean r4 = r4.endsWith(r5)
            if (r4 == 0) goto L_0x01a5
        L_0x008c:
            java.lang.String r1 = "uninstall"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "<java> wake process alive: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r5 = " wakePid: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r2 = r2.toString()
            com.tencent.assistant.utils.XLog.d(r1, r2)
            if (r0 != 0) goto L_0x00d1
            int r0 = r3.a()
            java.lang.String r1 = "uninstall"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "<java> wake new process pid: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            com.tencent.assistant.utils.XLog.d(r1, r2)
            if (r0 <= 0) goto L_0x00d1
            a(r0)
        L_0x00d1:
            java.lang.Object r1 = com.tencent.nucleus.manager.uninstallwatch.e.d
            monitor-enter(r1)
            r0 = 0
            com.tencent.nucleus.manager.uninstallwatch.e.c = r0     // Catch:{ all -> 0x01a2 }
            monitor-exit(r1)     // Catch:{ all -> 0x01a2 }
        L_0x00d8:
            return
        L_0x00d9:
            java.lang.String r2 = "uninstall"
            java.lang.String r4 = "<java> dosen't need to do anything !!"
            com.tencent.assistant.utils.XLog.d(r2, r4)
            goto L_0x003c
        L_0x00e2:
            boolean r2 = r3.b()
            if (r2 != 0) goto L_0x0141
            r2 = r1
        L_0x00e9:
            if (r2 >= r7) goto L_0x01aa
            java.lang.Process r4 = com.tencent.nucleus.manager.uninstallwatch.a.a(r8)
            if (r4 == 0) goto L_0x012b
            java.lang.String r2 = "uninstall"
            java.lang.String r5 = "<java> wait for child process(watch process) to die ..."
            com.tencent.assistant.utils.XLog.d(r2, r5)
            int r2 = r4.waitFor()     // Catch:{ InterruptedException -> 0x0122 }
            if (r2 != 0) goto L_0x011a
            java.lang.String r2 = "uninstall"
            java.lang.String r4 = "<java> watch process died normal"
            com.tencent.assistant.utils.XLog.d(r2, r4)     // Catch:{ InterruptedException -> 0x0122 }
        L_0x0105:
            java.lang.String r2 = "uninstall"
            java.lang.String r4 = "<java> watch socket server start success !!!"
            com.tencent.assistant.utils.XLog.d(r2, r4)
            r2 = r0
        L_0x010d:
            if (r2 != 0) goto L_0x0148
            java.lang.Object r1 = com.tencent.nucleus.manager.uninstallwatch.e.d
            monitor-enter(r1)
            r0 = 0
            com.tencent.nucleus.manager.uninstallwatch.e.c = r0     // Catch:{ all -> 0x0117 }
            monitor-exit(r1)     // Catch:{ all -> 0x0117 }
            goto L_0x00d8
        L_0x0117:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0117 }
            throw r0
        L_0x011a:
            java.lang.String r2 = "uninstall"
            java.lang.String r4 = "<java> watch process died illegal"
            com.tencent.assistant.utils.XLog.e(r2, r4)     // Catch:{ InterruptedException -> 0x0122 }
            goto L_0x0105
        L_0x0122:
            r2 = move-exception
            java.lang.String r4 = "uninstall"
            java.lang.String r5 = "initUninstallTask -> waitFor"
            com.tencent.assistant.utils.XLog.e(r4, r5, r2)
            goto L_0x0105
        L_0x012b:
            int r4 = r2 + 1
            if (r4 != r7) goto L_0x0139
            java.lang.String r4 = "uninstall"
            java.lang.String r5 = "<java> watch socket server start failed !!!"
            com.tencent.assistant.utils.XLog.e(r4, r5)
        L_0x0136:
            int r2 = r2 + 1
            goto L_0x00e9
        L_0x0139:
            java.lang.String r4 = "uninstall"
            java.lang.String r5 = "<java> watch socket server start failed\ntry to start watch socket server again ..."
            com.tencent.assistant.utils.XLog.e(r4, r5)
            goto L_0x0136
        L_0x0141:
            java.lang.String r2 = "uninstall"
            java.lang.String r4 = "<java> watch socket server already exists ..."
            com.tencent.assistant.utils.XLog.d(r2, r4)
        L_0x0148:
            r2 = r1
        L_0x0149:
            if (r2 >= r7) goto L_0x01a8
            java.lang.String r4 = "uninstall"
            java.lang.String r5 = "<java> start to watch ..."
            com.tencent.assistant.utils.XLog.d(r4, r5)
            java.lang.String r4 = com.tencent.nucleus.manager.uninstallwatch.a.e(r8)
            java.lang.String r5 = com.tencent.nucleus.manager.uninstallwatch.a.b()
            java.lang.String r6 = r8.getPackageName()
            boolean r4 = r3.a(r4, r5, r6)
            if (r4 == 0) goto L_0x0183
            java.lang.String r2 = "uninstall"
            java.lang.String r4 = "<java> add watch success !!!"
            com.tencent.assistant.utils.XLog.d(r2, r4)
            boolean r2 = com.tencent.nucleus.manager.uninstallwatch.a.b(r8)
            if (r2 == 0) goto L_0x0174
            com.tencent.nucleus.manager.uninstallwatch.a.c(r8)
        L_0x0174:
            r2 = r0
        L_0x0175:
            if (r2 != 0) goto L_0x003c
            java.lang.Object r1 = com.tencent.nucleus.manager.uninstallwatch.e.d
            monitor-enter(r1)
            r0 = 0
            com.tencent.nucleus.manager.uninstallwatch.e.c = r0     // Catch:{ all -> 0x0180 }
            monitor-exit(r1)     // Catch:{ all -> 0x0180 }
            goto L_0x00d8
        L_0x0180:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0180 }
            throw r0
        L_0x0183:
            int r4 = r2 + 1
            if (r4 != r7) goto L_0x0191
            java.lang.String r4 = "uninstall"
            java.lang.String r5 = "<java> add watch failed !!!"
            com.tencent.assistant.utils.XLog.e(r4, r5)
        L_0x018e:
            int r2 = r2 + 1
            goto L_0x0149
        L_0x0191:
            java.lang.String r4 = "uninstall"
            java.lang.String r5 = "<java> add watch failed \n try to add watch again ..."
            com.tencent.assistant.utils.XLog.e(r4, r5)
            goto L_0x018e
        L_0x0199:
            java.lang.String r2 = "uninstall"
            java.lang.String r4 = "<java> didn't update browser info"
            com.tencent.assistant.utils.XLog.d(r2, r4)
            goto L_0x0074
        L_0x01a2:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x01a2 }
            throw r0
        L_0x01a5:
            r0 = r1
            goto L_0x008c
        L_0x01a8:
            r2 = r1
            goto L_0x0175
        L_0x01aa:
            r2 = r1
            goto L_0x010d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.nucleus.manager.uninstallwatch.e.c(android.content.Context):void");
    }

    private static boolean c() {
        long d2 = d();
        return d2 != 0 && Math.abs(e() - d2) > 10000;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    private static long d() {
        return m.a().a("key_system_boot_time", 0L);
    }

    private static long e() {
        return System.currentTimeMillis() - SystemClock.elapsedRealtime();
    }

    private static void a(int i) {
        SharedPreferences.Editor edit = AstApp.i().getSharedPreferences("wake.sharepref", 0).edit();
        edit.putInt("wake_pid", i);
        if (Build.VERSION.SDK_INT >= 9) {
            edit.apply();
        } else {
            edit.commit();
        }
    }

    private static int f() {
        return AstApp.i().getSharedPreferences("wake.sharepref", 0).getInt("wake_pid", 0);
    }

    public static void a(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getQUAForBeacon());
        hashMap.put("B2", Global.getPhoneGuidAndGen());
        hashMap.put("B3", t.g());
        hashMap.put("B4", str);
        XLog.d("uninstall", "doWakeUpReport para = " + hashMap.toString());
        a.a("watch_wake", true, -1, -1, hashMap, true);
    }

    public void a(Intent intent) {
        TemporaryThreadManager.get().start(new g(this, intent));
    }
}
