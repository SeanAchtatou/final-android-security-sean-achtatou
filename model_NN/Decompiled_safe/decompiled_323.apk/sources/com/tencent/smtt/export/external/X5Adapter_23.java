package com.tencent.smtt.export.external;

import android.annotation.TargetApi;
import android.content.Context;

public class X5Adapter_23 {
    @TargetApi(9)
    public static String getNativeLibraryDirGB(Context context) {
        return context.getApplicationInfo().nativeLibraryDir;
    }
}
