package com.tencent.smtt.export.external;

import android.annotation.TargetApi;
import android.content.Context;

public class X5Adapter_16 {
    @TargetApi(4)
    public static String getNativeLibraryDirDonut(Context context) {
        return context.getApplicationInfo().dataDir + "/lib";
    }
}
