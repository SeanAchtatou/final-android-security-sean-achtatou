package com.tencent.cloud.component;

import android.view.View;
import com.tencent.assistant.component.categorydetail.SmoothShrinkListener;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
class e implements SmoothShrinkListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CategoryDetailListView f2280a;

    e(CategoryDetailListView categoryDetailListView) {
        this.f2280a = categoryDetailListView;
    }

    public void onStop() {
        int f;
        int a2 = by.a(this.f2280a.getContext(), 4.0f);
        int i = 0;
        for (int i2 = 0; i2 < this.f2280a.G; i2++) {
            View childAt = this.f2280a.getListView().getChildAt(i2);
            if (childAt != null) {
                i += childAt.getHeight();
            }
        }
        if (this.f2280a.G <= 1) {
            f = a2 + 0;
        } else {
            f = this.f2280a.H - i;
        }
        if (this.f2280a.y != null) {
            this.f2280a.y.hideEmptyHeader();
        }
        this.f2280a.getListView().setSelectionFromTop(this.f2280a.G, f);
        this.f2280a.getListView().postInvalidate();
    }

    public void onStart() {
    }

    public void onShrink(int i) {
    }
}
