package com.tencent.cloud.component;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;

/* compiled from: ProGuard */
public class CategoryListPage extends RelativeLayout implements j {

    /* renamed from: a  reason: collision with root package name */
    private Context f2264a;
    private CategoryListView b;
    private LoadingView c;
    private NormalErrorRecommendPage d;
    private View.OnClickListener e = new g(this);

    public CategoryListPage(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context);
        this.f2264a = context;
        this.b.a(this);
    }

    public CategoryListPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
        this.f2264a = context;
        this.b.a(this);
    }

    public CategoryListPage(Context context) {
        super(context);
        a(context);
        this.f2264a = context;
        this.b.a(this);
    }

    private void a(Context context) {
        LayoutInflater.from(context).inflate((int) R.layout.applist_component_category_view, this);
        this.b = (CategoryListView) findViewById(R.id.applist);
        this.b.setVisibility(8);
        this.b.setDivider(null);
        this.b.setSelector(new ColorDrawable(0));
        this.b.setCacheColorHint(17170445);
        this.c = (LoadingView) findViewById(R.id.loading_view);
        this.c.setVisibility(0);
        this.d = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.d.setButtonClickListener(this.e);
    }

    public void a(ViewPageScrollListener viewPageScrollListener) {
        this.b.a(viewPageScrollListener);
    }

    public void a(BaseAdapter baseAdapter) {
        this.b.setAdapter(baseAdapter);
    }

    public void a(int i) {
        this.b.setVisibility(8);
        this.d.setVisibility(0);
        this.c.setVisibility(8);
        this.d.setErrorType(i);
    }

    public void a() {
        this.b.setVisibility(0);
        this.d.setVisibility(8);
        this.c.setVisibility(8);
    }

    public void b() {
        this.c.setVisibility(0);
        this.b.setVisibility(8);
        this.d.setVisibility(8);
    }

    public void c() {
        this.b.r();
    }

    public void d() {
        this.b.q();
    }

    public void a(long j) {
        if (this.b != null) {
            this.b.a(j);
        }
    }
}
