package com.tencent.cloud.component;

import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.m;
import com.tencent.assistantv2.manager.a;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.nucleus.manager.component.SwitchButton;

/* compiled from: ProGuard */
class o extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CftAppRankListView f2288a;

    o(CftAppRankListView cftAppRankListView) {
        this.f2288a = cftAppRankListView;
    }

    public void onTMAClick(View view) {
        boolean z = true;
        this.f2288a.D++;
        if (this.f2288a.D < 10) {
            int i = this.f2288a.D - 1;
            this.f2288a.C[i] = true;
            if (i > 0) {
                this.f2288a.C[i - 1] = false;
            }
            this.f2288a.z.postDelayed(new p(this, i), 2000);
            if (this.f2288a.D == 5) {
                this.f2288a.y.setBackgroundResource(R.color.apk_list_bg);
            }
            if (this.f2288a.D > 5) {
                this.f2288a.A.setText(CftAppRankListView.K[this.f2288a.D % 5]);
                this.f2288a.A.setVisibility(0);
                return;
            }
            SwitchButton switchButton = this.f2288a.z;
            if (this.f2288a.z.b()) {
                z = false;
            }
            switchButton.b(z);
            m.a().t(this.f2288a.z.b());
            a.a().b().a(this.f2288a.z.b());
            if (this.f2288a.z.b()) {
                Toast.makeText(this.f2288a.getContext(), (int) R.string.hide_installed_apps_in_list, 4).show();
            } else {
                Toast.makeText(this.f2288a.getContext(), (int) R.string.hide_installed_apps_in_list_cancel_rank, 4).show();
            }
            this.f2288a.f.c();
            if (this.f2288a.f.getCount() > 0 && this.f2288a.e != null && !this.f2288a.e.g()) {
                this.f2288a.b_();
            }
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 sTInfoV2 = new STInfoV2(((BaseActivity) this.f2288a.getContext()).f(), "08", ((BaseActivity) this.f2288a.getContext()).f(), "08", 200);
        sTInfoV2.slotId = com.tencent.assistantv2.st.page.a.a("08", 0);
        if (!a.a().b().c()) {
            sTInfoV2.status = "01";
        } else {
            sTInfoV2.status = "02";
        }
        sTInfoV2.actionId = 200;
        return sTInfoV2;
    }
}
