package com.tencent.cloud.component;

import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.module.k;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class ab extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UpdateListView f2274a;

    ab(UpdateListView updateListView) {
        this.f2274a = updateListView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.cloud.adapter.g.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, boolean):void
     arg types: [java.util.List<com.tencent.assistant.model.SimpleAppModel>, int]
     candidates:
      com.tencent.cloud.adapter.g.a(com.tencent.cloud.adapter.g, com.tencent.assistant.protocol.jce.LbsData):com.tencent.assistant.protocol.jce.LbsData
      com.tencent.cloud.adapter.g.a(int, int):java.lang.String
      com.tencent.cloud.adapter.g.a(com.tencent.cloud.adapter.g, java.lang.String):java.lang.String
      com.tencent.cloud.adapter.g.a(com.tencent.assistant.model.SimpleAppModel, android.view.View):void
      com.tencent.cloud.adapter.g.a(com.tencent.cloud.adapter.g, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.cloud.adapter.g.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.cloud.adapter.b):void
      com.tencent.cloud.adapter.g.a(com.tencent.assistantv2.st.page.STExternalInfo, java.lang.String):void
      com.tencent.cloud.adapter.g.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, boolean):void */
    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.f2274a.f2269a.a(k.a(this.f2274a.f2269a.b), true);
                this.f2274a.l();
                this.f2274a.k();
                this.f2274a.o();
                int unused = this.f2274a.s = this.f2274a.f2269a.getGroupCount();
                if (this.f2274a.B == 1) {
                    if (this.f2274a.A == 1 || this.f2274a.A == 2) {
                        TemporaryThreadManager.get().start(new ac(this));
                        return;
                    }
                    return;
                } else if (this.f2274a.F != null && this.f2274a.F.size() > 0) {
                    this.f2274a.a(k.h(this.f2274a.F));
                    return;
                } else {
                    return;
                }
            case 2:
                this.f2274a.f2269a.a(k.a(this.f2274a.f2269a.b), false);
                this.f2274a.l();
                this.f2274a.k();
                this.f2274a.o();
                int unused2 = this.f2274a.s = this.f2274a.f2269a.getGroupCount();
                return;
            default:
                return;
        }
    }
}
