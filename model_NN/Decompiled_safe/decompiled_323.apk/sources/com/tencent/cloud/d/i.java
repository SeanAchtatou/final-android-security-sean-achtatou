package com.tencent.cloud.d;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.TemporaryThreadManager;
import java.util.ArrayList;

/* compiled from: ProGuard */
class i implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f2307a;
    private int b = 0;
    /* access modifiers changed from: private */
    public byte[] c;
    private long d;
    /* access modifiers changed from: private */
    public boolean e = true;
    private ArrayList<SimpleAppModel> f;
    private byte[] g;
    /* access modifiers changed from: private */
    public int h = -1;

    public i(b bVar) {
        this.f2307a = bVar;
    }

    public void a(byte[] bArr) {
        if (bArr != null && bArr.length != 0) {
            b();
            this.b = 1;
            this.c = bArr;
            this.h = this.f2307a.getUniqueId();
            TemporaryThreadManager.get().start(new j(this));
        }
    }

    public int a() {
        return this.h;
    }

    public void b() {
        this.h = -1;
        this.c = null;
        this.e = true;
        this.g = null;
        this.f = null;
        this.b = 0;
    }

    public boolean c() {
        return this.b == 1;
    }

    public byte[] d() {
        return this.c;
    }

    public ArrayList<SimpleAppModel> e() {
        return this.f;
    }

    public boolean f() {
        return this.e;
    }

    public byte[] g() {
        return this.g;
    }

    public long h() {
        return this.d;
    }

    public void a(long j, ArrayList<SimpleAppModel> arrayList, boolean z, byte[] bArr) {
        this.d = j;
        this.f = arrayList;
        this.e = z;
        this.g = bArr;
        this.b = 2;
    }

    public void i() {
        this.b = 2;
    }

    /* renamed from: j */
    public i clone() {
        try {
            i iVar = (i) super.clone();
            if (this.f == null) {
                return iVar;
            }
            iVar.f = (ArrayList) this.f.clone();
            return iVar;
        } catch (CloneNotSupportedException e2) {
            return this;
        }
    }
}
