package com.tencent.cloud.activity;

import android.app.Activity;
import android.webkit.WebView;
import com.tencent.assistant.activity.BrowserActivity;

/* compiled from: ProGuard */
class ae implements BrowserActivity.WebChromeClientListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ VideoActivityV2 f2171a;

    ae(VideoActivityV2 videoActivityV2) {
        this.f2171a = videoActivityV2;
    }

    public Activity getActivity() {
        return this.f2171a;
    }

    public void onProgressChanged(WebView webView, int i) {
        this.f2171a.d.setProgress(i);
    }

    public void onReceivedTitle(WebView webView, String str) {
        this.f2171a.setTitle(str);
    }
}
