package com.tencent.cloud.smartcard.c;

import com.tencent.assistant.protocol.jce.SmartCardTagInfo;
import com.tencent.assistant.smartcard.c.z;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.cloud.smartcard.b.c;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class a extends z {
    public boolean a(n nVar, List<Long> list) {
        ArrayList<SmartCardTagInfo> arrayList;
        if (!((Boolean) b(nVar).first).booleanValue()) {
            return false;
        }
        if (nVar instanceof c) {
            if (((c) nVar).h() == null || ((c) nVar).h().b() == null) {
                arrayList = null;
            } else {
                arrayList = ((c) nVar).h().b();
            }
            if (arrayList == null || arrayList.size() < 6) {
                return false;
            }
        }
        return true;
    }
}
