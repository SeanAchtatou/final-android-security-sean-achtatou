package com.tencent.cloud.adapter;

import android.widget.TextView;
import com.tencent.assistant.adapter.a.c;
import com.tencent.assistant.adapter.a.d;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class s implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpecailTopicDetailAdapter f2228a;

    s(SpecailTopicDetailAdapter specailTopicDetailAdapter) {
        this.f2228a = specailTopicDetailAdapter;
    }

    public c a(Object obj) {
        if (obj != null && (obj instanceof t)) {
            t tVar = (t) obj;
            if (tVar.f2229a != null) {
                return tVar.f2229a.b;
            }
        }
        return null;
    }

    public c b(Object obj) {
        if (obj == null || !(obj instanceof u)) {
            return null;
        }
        return ((u) obj).b;
    }

    public TextView c(Object obj) {
        if (obj != null && (obj instanceof t)) {
            t tVar = (t) obj;
            if (tVar.f2229a != null) {
                return tVar.f2229a.f2230a;
            }
        }
        return null;
    }

    public TextView d(Object obj) {
        if (obj == null || !(obj instanceof u)) {
            return null;
        }
        return ((u) obj).f2230a;
    }

    public String a() {
        return a.a(STCommonInfo.ContentIdType.SPECIAL, String.valueOf(this.f2228a.i));
    }
}
