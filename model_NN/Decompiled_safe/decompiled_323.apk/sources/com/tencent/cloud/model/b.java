package com.tencent.cloud.model;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
final class b implements Parcelable.Creator<SimpleVideoModel> {
    b() {
    }

    /* renamed from: a */
    public SimpleVideoModel createFromParcel(Parcel parcel) {
        SimpleVideoModel simpleVideoModel = new SimpleVideoModel();
        simpleVideoModel.f2320a = parcel.readString();
        simpleVideoModel.b = parcel.readString();
        simpleVideoModel.c = parcel.readString();
        simpleVideoModel.d = parcel.readString();
        simpleVideoModel.e = parcel.readString();
        simpleVideoModel.f = parcel.readString();
        simpleVideoModel.g = parcel.readString();
        simpleVideoModel.h = parcel.readString();
        simpleVideoModel.i = parcel.readString();
        simpleVideoModel.k = parcel.readString();
        simpleVideoModel.l = parcel.readString();
        return simpleVideoModel;
    }

    /* renamed from: a */
    public SimpleVideoModel[] newArray(int i) {
        return new SimpleVideoModel[i];
    }
}
