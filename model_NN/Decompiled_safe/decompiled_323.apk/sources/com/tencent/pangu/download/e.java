package com.tencent.pangu.download;

import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.manager.ak;

/* compiled from: ProGuard */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f3758a;
    final /* synthetic */ SimpleDownloadInfo.UIType b;
    final /* synthetic */ a c;

    e(a aVar, DownloadInfo downloadInfo, SimpleDownloadInfo.UIType uIType) {
        this.c = aVar;
        this.f3758a = downloadInfo;
        this.b = uIType;
    }

    public void run() {
        if (this.f3758a.uiType != this.b) {
            this.f3758a.uiType = this.b;
            DownloadProxy.a().d(this.f3758a);
        }
        if (ak.a().b(this.f3758a)) {
            ak.a().d(this.f3758a);
            return;
        }
        if (this.f3758a.downloadState == SimpleDownloadInfo.DownloadState.WAITTING_FOR_WIFI) {
            this.f3758a.downloadState = SimpleDownloadInfo.DownloadState.INIT;
        }
        DownloadProxy.a().c(this.f3758a);
    }
}
