package com.tencent.pangu.utils.installuninstall;

import android.text.TextUtils;
import android.util.Pair;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.event.EventDispatcher;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.m;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.e;
import com.tencent.assistantv2.st.business.o;
import com.tencent.assistantv2.st.l;
import com.tencent.cloud.a.f;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager;
import java.io.File;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class ac {

    /* renamed from: a  reason: collision with root package name */
    private static ac f3990a = null;
    private static final Object i = new Object();
    /* access modifiers changed from: private */
    public EventDispatcher b = AstApp.i().j();
    private af c = new af(this, null);
    /* access modifiers changed from: private */
    public final Object d = new Object();
    private final Object e = new Object();
    private final Object f = new Object();
    private ArrayList<InstallUninstallTaskBean> g = new ArrayList<>();
    private ah h = new ah(this);
    private final Object j = new Object();
    private ArrayList<InstallUninstallTaskBean> k = new ArrayList<>();
    private ag l = new ag(this);

    public static synchronized ac a() {
        ac acVar;
        synchronized (ac.class) {
            if (f3990a == null) {
                f3990a = new ac();
            }
            acVar = f3990a;
        }
        return acVar;
    }

    private ac() {
        d();
    }

    private void d() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_ATFRONT, this.c);
    }

    public boolean b() {
        return (this.l.c == null || this.h.b == null) ? false : true;
    }

    public boolean c() {
        return i() && e();
    }

    /* access modifiers changed from: private */
    public Pair<Boolean, Boolean> a(InstallUninstallDialogManager installUninstallDialogManager, InstallUninstallTaskBean installUninstallTaskBean) {
        Pair<Boolean, String> e2 = e.e(AstApp.i(), installUninstallTaskBean.filePath);
        if (((Boolean) e2.first).booleanValue()) {
            String str = "broken[" + ((String) e2.second) + "]";
            l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, (byte) -10, (byte) 1, str);
            l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, o.a().a(installUninstallTaskBean.style), (byte) 1, str, false);
            if (installUninstallTaskBean.promptForInstallUninstall) {
                installUninstallDialogManager.a(InstallUninstallDialogManager.DIALOG_DEALWITH_TYPE.BROKEN, installUninstallTaskBean);
            }
            return Pair.create(false, true);
        }
        l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, (byte) -10, (byte) 0, STConst.ST_INSTALL_CHECK_STR_BROKEN);
        Pair<Boolean, String> b2 = InstallUninstallUtil.b(installUninstallTaskBean.packageName, installUninstallTaskBean.signatrue);
        if (!((Boolean) b2.first).booleanValue()) {
            if (installUninstallTaskBean.promptForInstallUninstall) {
                installUninstallDialogManager.a(true);
                boolean a2 = installUninstallDialogManager.a(InstallUninstallDialogManager.DIALOG_DEALWITH_TYPE.DIFF_SIGNATURE, installUninstallTaskBean);
                installUninstallDialogManager.a(false);
                if (a2) {
                    c(installUninstallTaskBean);
                    return Pair.create(false, false);
                }
            }
            String str2 = "diff signature[" + ((String) b2.second) + "]";
            l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, (byte) -10, (byte) 1, str2);
            l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, o.a().a(installUninstallTaskBean.style), (byte) 1, str2, false);
            return Pair.create(false, true);
        }
        l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, (byte) -10, (byte) 0, STConst.ST_INSTALL_CHECK_STR_SIGNATURE);
        Pair<Boolean, String> a3 = InstallUninstallUtil.a(installUninstallTaskBean.fileSize);
        if (!((Boolean) a3.first).booleanValue()) {
            if (installUninstallTaskBean.promptForInstallUninstall) {
                installUninstallDialogManager.a(true);
                InstallUninstallDialogManager.DIALOG_DEALWITH_RESULT b3 = installUninstallDialogManager.b(InstallUninstallDialogManager.DIALOG_DEALWITH_TYPE.INSTALL_SPACE_NOT_ENOUGH, installUninstallTaskBean);
                installUninstallDialogManager.a(false);
                if (b3 == InstallUninstallDialogManager.DIALOG_DEALWITH_RESULT.RESULT_RIGHT_BUTTON) {
                    if (a.a().a(installUninstallTaskBean) == InstallUninstallDialogManager.DIALOG_DEALWITH_RESULT.RESULT_CANCEL) {
                        String str3 = "space not enough[" + ((String) a3.second) + "], cancel after app uninstall";
                        l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, (byte) -10, (byte) 1, str3);
                        l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, o.a().a(installUninstallTaskBean.style), (byte) 1, str3, false);
                        return Pair.create(false, true);
                    }
                } else if (b3 == InstallUninstallDialogManager.DIALOG_DEALWITH_RESULT.RESULT_CANCEL) {
                    String str4 = "space not enough[" + ((String) a3.second) + "], cancel before app uninstall";
                    l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, (byte) -10, (byte) 1, str4);
                    l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, o.a().a(installUninstallTaskBean.style), (byte) 1, str4, false);
                    return Pair.create(false, true);
                }
            } else {
                String str5 = "space not enough[" + ((String) a3.second) + "], cancel no prompt";
                l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, (byte) -10, (byte) 1, str5);
                l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, o.a().a(installUninstallTaskBean.style), (byte) 1, str5, false);
                return Pair.create(false, true);
            }
        }
        l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, (byte) -10, (byte) 0, STConst.ST_INSTALL_CHECK_STR_SPACE);
        Pair<Boolean, String> b4 = InstallUninstallUtil.b(installUninstallTaskBean.filePath);
        if (!((Boolean) b4.first).booleanValue()) {
            String str6 = "rom unsupport[" + ((String) b4.second) + "]";
            l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, (byte) -10, (byte) 1, str6);
            l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, o.a().a(installUninstallTaskBean.style), (byte) 1, str6, false);
            if (installUninstallTaskBean.promptForInstallUninstall) {
                installUninstallDialogManager.a(InstallUninstallDialogManager.DIALOG_DEALWITH_TYPE.ROM_UNSUPPORT, installUninstallTaskBean);
            }
            return Pair.create(false, true);
        }
        l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, (byte) -10, (byte) 0, STConst.ST_INSTALL_CHECK_STR_ROM);
        return Pair.create(true, true);
    }

    private void c(InstallUninstallTaskBean installUninstallTaskBean) {
        byte b2;
        byte b3;
        if (installUninstallTaskBean != null) {
            if (installUninstallTaskBean.style == 0) {
                InstallUninstallUtil.a(installUninstallTaskBean.packageName);
                try {
                    synchronized (this.d) {
                        this.d.wait();
                    }
                } catch (InterruptedException e2) {
                }
                if (ApkResourceManager.getInstance().getLocalApkInfo(installUninstallTaskBean.packageName) == null) {
                    DownloadProxy.a().c(installUninstallTaskBean.downloadTicket).installType = 0;
                    InstallUninstallUtil.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, installUninstallTaskBean.filePath);
                    return;
                }
                return;
            }
            this.b.sendMessage(this.b.obtainMessage(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START, installUninstallTaskBean));
            boolean a2 = InstallUninstallUtil.a(installUninstallTaskBean.style, installUninstallTaskBean.packageName);
            this.b.sendMessage(this.b.obtainMessage(a2 ? EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC : EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL, installUninstallTaskBean));
            a(-1, a2, null, installUninstallTaskBean);
            if (!a2) {
                InstallUninstallUtil.a(installUninstallTaskBean.packageName);
                try {
                    synchronized (this.d) {
                        this.d.wait();
                    }
                } catch (InterruptedException e3) {
                }
            }
            if (ApkResourceManager.getInstance().getLocalApkInfo(installUninstallTaskBean.packageName) == null) {
                this.b.sendMessage(this.b.obtainMessage(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, installUninstallTaskBean));
                DownloadInfo c2 = DownloadProxy.a().c(installUninstallTaskBean.downloadTicket);
                c2.installType = 2;
                ak a3 = InstallUninstallUtil.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, installUninstallTaskBean.style, installUninstallTaskBean.filePath);
                boolean z = a3.f3997a;
                installUninstallTaskBean.failDesc = a3.b;
                l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, o.a().a(installUninstallTaskBean.style), z ? (byte) 0 : 1, installUninstallTaskBean.failDesc);
                String str = installUninstallTaskBean.packageName;
                int i2 = installUninstallTaskBean.versionCode;
                byte a4 = o.a().a(installUninstallTaskBean.style);
                if (z) {
                    b2 = 0;
                } else {
                    b2 = 1;
                }
                l.b(str, i2, a4, b2, installUninstallTaskBean.failDesc);
                if (installUninstallTaskBean.style == 2 && !z && a3.c && (m.a().i() || !m.a().h())) {
                    installUninstallTaskBean.style = 1;
                    c2.installType = 1;
                    a3 = InstallUninstallUtil.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, installUninstallTaskBean.style, installUninstallTaskBean.filePath);
                    z = a3.f3997a;
                    installUninstallTaskBean.failDesc = a3.b;
                    l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, o.a().a(installUninstallTaskBean.style), z ? (byte) 0 : 1, installUninstallTaskBean.failDesc);
                    String str2 = installUninstallTaskBean.packageName;
                    int i3 = installUninstallTaskBean.versionCode;
                    byte a5 = o.a().a(installUninstallTaskBean.style);
                    if (z) {
                        b3 = 0;
                    } else {
                        b3 = 1;
                    }
                    l.b(str2, i3, a5, b3, installUninstallTaskBean.failDesc);
                }
                boolean z2 = z;
                ak akVar = a3;
                this.b.sendMessage(this.b.obtainMessage(z2 ? EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC : 1027, installUninstallTaskBean));
                if (!z2) {
                    if (akVar.b.toUpperCase().contains("INSTALL_FAILED_INSUFFICIENT_STORAGE")) {
                        a(1, z2, AstApp.i().getResources().getString(R.string.toast_root_install_fail_space_no_enough, installUninstallTaskBean.appName), installUninstallTaskBean);
                    } else {
                        a(1, z2, null, installUninstallTaskBean);
                    }
                    if (installUninstallTaskBean == null || TextUtils.isEmpty(installUninstallTaskBean.filePath) || !installUninstallTaskBean.trySystemAfterSilentFail) {
                        l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, o.a().a(installUninstallTaskBean.style), (byte) 1, installUninstallTaskBean.failDesc, true);
                        return;
                    }
                    c2.installType = 0;
                    InstallUninstallUtil.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, installUninstallTaskBean.filePath);
                    return;
                }
                a(1, z2, null, installUninstallTaskBean);
                l.a(installUninstallTaskBean.packageName, installUninstallTaskBean.versionCode, o.a().a(installUninstallTaskBean.style), (byte) 0, installUninstallTaskBean.failDesc, true);
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean e() {
        boolean isEmpty;
        synchronized (this.f) {
            isEmpty = this.g.isEmpty();
        }
        return isEmpty;
    }

    /* access modifiers changed from: private */
    public InstallUninstallTaskBean f() {
        InstallUninstallTaskBean installUninstallTaskBean;
        synchronized (this.f) {
            if (!this.g.isEmpty()) {
                installUninstallTaskBean = this.g.remove(0);
            } else {
                installUninstallTaskBean = null;
            }
        }
        return installUninstallTaskBean;
    }

    public void a(String str, String str2, int i2, String str3, String str4, String str5, long j2, boolean z) {
        if (!TextUtils.isEmpty(str)) {
            InstallUninstallTaskBean installUninstallTaskBean = new InstallUninstallTaskBean(0, 1, str, str2, i2, str3, str4, str5, j2);
            installUninstallTaskBean.isAutoInstall = z;
            a(installUninstallTaskBean);
        }
    }

    private boolean d(InstallUninstallTaskBean installUninstallTaskBean) {
        if (TextUtils.isEmpty(installUninstallTaskBean.packageName)) {
            return true;
        }
        InstallUninstallTaskBean installUninstallTaskBean2 = this.h.b;
        if (installUninstallTaskBean2 != null && installUninstallTaskBean.packageName.equalsIgnoreCase(installUninstallTaskBean2.packageName) && installUninstallTaskBean2.versionCode == installUninstallTaskBean.versionCode) {
            return true;
        }
        int size = this.g.size();
        for (int i2 = 0; i2 < size; i2++) {
            InstallUninstallTaskBean installUninstallTaskBean3 = this.g.get(i2);
            if (installUninstallTaskBean3 != null && !TextUtils.isEmpty(installUninstallTaskBean3.packageName) && installUninstallTaskBean.packageName.equalsIgnoreCase(installUninstallTaskBean3.packageName) && installUninstallTaskBean3.versionCode == installUninstallTaskBean.versionCode) {
                return true;
            }
        }
        return false;
    }

    public void a(InstallUninstallTaskBean installUninstallTaskBean) {
        synchronized (this.f) {
            if (!d(installUninstallTaskBean)) {
                DownloadInfo d2 = DownloadProxy.a().d(installUninstallTaskBean.downloadTicket);
                if (d2 != null) {
                    d2.installType = 0;
                }
                this.g.add(installUninstallTaskBean);
                this.b.sendMessage(this.b.obtainMessage(1023, installUninstallTaskBean));
            }
        }
        h();
    }

    /* access modifiers changed from: private */
    public void g() {
        synchronized (this.e) {
            try {
                this.e.wait();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        if (!this.h.isAlive()) {
            try {
                this.h.start();
            } catch (IllegalThreadStateException e2) {
            }
        }
        synchronized (this.e) {
            this.e.notifyAll();
        }
    }

    /* access modifiers changed from: private */
    public boolean i() {
        boolean isEmpty;
        synchronized (this.j) {
            isEmpty = this.k.isEmpty();
        }
        return isEmpty;
    }

    /* access modifiers changed from: private */
    public InstallUninstallTaskBean j() {
        InstallUninstallTaskBean installUninstallTaskBean;
        synchronized (this.j) {
            if (!this.k.isEmpty()) {
                installUninstallTaskBean = this.k.remove(0);
            } else {
                installUninstallTaskBean = null;
            }
        }
        return installUninstallTaskBean;
    }

    public void b(InstallUninstallTaskBean installUninstallTaskBean) {
        synchronized (this.j) {
            this.k.add(installUninstallTaskBean);
            DownloadInfo c2 = DownloadProxy.a().c(installUninstallTaskBean.downloadTicket);
            if (c2 != null) {
                c2.installType = 2;
            }
            if (installUninstallTaskBean.action == 1) {
                this.b.sendMessage(this.b.obtainMessage(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, installUninstallTaskBean));
            }
        }
        l();
    }

    private boolean a(String str, long j2) {
        if (TextUtils.isEmpty(str)) {
            return true;
        }
        InstallUninstallTaskBean installUninstallTaskBean = this.l.c;
        if (installUninstallTaskBean != null && str.equalsIgnoreCase(installUninstallTaskBean.packageName) && j2 == ((long) installUninstallTaskBean.versionCode)) {
            return true;
        }
        synchronized (this.j) {
            int size = this.k.size();
            for (int i2 = 0; i2 < size; i2++) {
                InstallUninstallTaskBean installUninstallTaskBean2 = this.k.get(i2);
                if (installUninstallTaskBean2 != null && !TextUtils.isEmpty(installUninstallTaskBean2.packageName) && str.equalsIgnoreCase(installUninstallTaskBean2.packageName) && j2 == ((long) installUninstallTaskBean2.versionCode)) {
                    return true;
                }
            }
            return false;
        }
    }

    public void a(int i2, String str, String str2, String str3, int i3, String str4, String str5, String str6, long j2, boolean z, boolean z2, boolean z3, boolean z4, f fVar) {
        if (!TextUtils.isEmpty(str2)) {
            if (!a(str3, (long) i3) && new File(str2).exists()) {
                InstallUninstallTaskBean installUninstallTaskBean = new InstallUninstallTaskBean(i2, 1, str2, str3, i3, str4, str5, str6, j2, z, z2, z3);
                installUninstallTaskBean.isAutoInstall = z4;
                installUninstallTaskBean.applinkInfo = fVar;
                b(installUninstallTaskBean);
            }
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        synchronized (i) {
            try {
                i.wait();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        if (!this.l.isAlive()) {
            try {
                this.l.start();
            } catch (IllegalThreadStateException e2) {
            }
        }
        synchronized (i) {
            i.notifyAll();
        }
    }

    public void a(int i2, boolean z, String str, InstallUninstallTaskBean installUninstallTaskBean) {
        if (installUninstallTaskBean != null && installUninstallTaskBean.promptForInstallUninstall && AstApp.i().l()) {
            if (i2 == 1) {
                if (TextUtils.isEmpty(str)) {
                    str = AstApp.i().getResources().getString(z ? R.string.toast_root_install_succ : R.string.toast_root_install_fail, installUninstallTaskBean.appName);
                }
                ah.a().post(new ad(this, str));
            } else if (i2 == -1 && !TextUtils.isEmpty(installUninstallTaskBean.appName)) {
                ah.a().post(new ae(this, AstApp.i().getResources().getString(z ? R.string.toast_root_uninstall_succ : R.string.toast_root_uninstall_fail, installUninstallTaskBean.appName)));
            }
        }
    }
}
