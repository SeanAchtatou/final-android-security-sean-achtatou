package com.tencent.pangu.utils.installuninstall;

import com.tencent.cloud.a.f;
import java.io.Serializable;

/* compiled from: ProGuard */
public class InstallUninstallTaskBean implements Serializable {
    public static final int ACTION_CANCEL = -2;
    public static final int ACTION_INSTALL = 1;
    public static final int ACTION_UNINSTALL = -1;
    public int action;
    public String appName;
    public f applinkInfo = null;
    public String downloadTicket;
    public String failDesc;
    public String filePath;
    public long fileSize;
    public boolean isAutoInstall = true;
    public boolean isImportantVersion;
    public boolean isSystemApp = false;
    public String packageName;
    public boolean promptForInstallUninstall = true;
    public String signatrue;
    public int style;
    public boolean trySystemAfterSilentFail = true;
    public int versionCode = -1;

    public InstallUninstallTaskBean() {
    }

    public InstallUninstallTaskBean(int i, int i2, String str, String str2, int i3, String str3, String str4, String str5, long j) {
        this.style = i;
        this.action = i2;
        this.filePath = str;
        this.packageName = str2;
        this.versionCode = i3;
        this.appName = str3;
        this.signatrue = str4;
        this.downloadTicket = str5;
        this.fileSize = j;
    }

    public InstallUninstallTaskBean(int i, int i2, String str, String str2, int i3, String str3, String str4, String str5, long j, boolean z, boolean z2, boolean z3) {
        this.style = i;
        this.action = i2;
        this.filePath = str;
        this.packageName = str2;
        this.versionCode = i3;
        this.appName = str3;
        this.signatrue = str4;
        this.downloadTicket = str5;
        this.fileSize = j;
        this.trySystemAfterSilentFail = z;
        this.promptForInstallUninstall = z2;
        this.isImportantVersion = z3;
    }

    public InstallUninstallTaskBean(int i, int i2, String str, String str2) {
        this.style = i;
        this.action = i2;
        this.packageName = str;
        this.appName = str2;
    }

    public InstallUninstallTaskBean(int i, String str, int i2, String str2) {
        this.action = i;
        this.packageName = str;
        this.appName = str2;
        this.versionCode = i2;
    }
}
