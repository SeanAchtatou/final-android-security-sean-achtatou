package com.tencent.pangu.utils.installuninstall;

/* compiled from: ProGuard */
class e extends o {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstallUninstallDialogManager f4000a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    e(InstallUninstallDialogManager installUninstallDialogManager) {
        super(installUninstallDialogManager);
        this.f4000a = installUninstallDialogManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onLeftBtnClick() {
        boolean unused = this.f4000a.c = false;
        this.f4000a.c();
        this.f4000a.a(this.b.downloadTicket);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onRightBtnClick() {
        boolean unused = this.f4000a.c = true;
        this.f4000a.c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onCancell() {
        boolean unused = this.f4000a.c = false;
        this.f4000a.c();
        this.f4000a.a(this.b.downloadTicket);
    }
}
