package com.tencent.pangu.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.module.k;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.component.appdetail.process.s;
import com.tencent.pangu.download.DownloadInfo;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class SearchMatchAdapter extends BaseAdapter implements UIEventListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static int f3406a = 0;
    /* access modifiers changed from: private */
    public static int b = 1;
    /* access modifiers changed from: private */
    public Context c;
    private LayoutInflater d = null;
    private ArrayList<SimpleAppModel> e = null;
    private ArrayList<String> f = null;
    /* access modifiers changed from: private */
    public View.OnClickListener g = null;
    /* access modifiers changed from: private */
    public View h = null;
    /* access modifiers changed from: private */
    public String i = null;
    private b j = null;
    private int k = STConst.ST_PAGE_SEARCH_SUGGEST;

    public SearchMatchAdapter(Context context, View view, View.OnClickListener onClickListener) {
        this.c = context;
        this.d = LayoutInflater.from(context);
        this.g = onClickListener;
        this.h = view;
    }

    public void a(int i2) {
        this.k = i2;
    }

    public void a(ArrayList<SimpleAppModel> arrayList, ArrayList<String> arrayList2, String str) {
        this.i = str;
        if (arrayList != null) {
            if (this.e == null) {
                this.e = new ArrayList<>();
                this.e.addAll(arrayList);
            } else {
                this.e.clear();
                this.e.addAll(arrayList);
            }
        }
        if (arrayList2 != null) {
            if (this.f == null) {
                this.f = new ArrayList<>();
                this.f.addAll(arrayList2);
            } else {
                this.f.clear();
                this.f.addAll(arrayList2);
            }
        }
        notifyDataSetChanged();
    }

    public Object getItem(int i2) {
        if (this.e != null) {
            if (i2 < this.e.size()) {
                return this.e.get(i2);
            }
            if (!(this.f == null || this.f.size() == 0 || i2 - this.e.size() >= this.f.size())) {
                return this.f.get(i2 - this.e.size());
            }
        } else if (!(this.f == null || this.f.size() == 0 || i2 >= this.f.size())) {
            return this.f.get(i2);
        }
        return null;
    }

    public int getViewTypeCount() {
        return 2;
    }

    public int getItemViewType(int i2) {
        if (this.e == null || i2 >= this.e.size()) {
            return b;
        }
        return f3406a;
    }

    public int getCount() {
        int i2 = 0;
        if (this.e != null) {
            i2 = 0 + this.e.size();
        }
        if (this.f != null) {
            return i2 + this.f.size();
        }
        return i2;
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        bi biVar;
        bg bgVar;
        if (f3406a == getItemViewType(i2)) {
            if (view == null || ((bh) view.getTag()).f3437a == null) {
                bgVar = new bg(this, null);
                view = this.d.inflate((int) R.layout.search_app_item, (ViewGroup) null);
                bgVar.c = (TXAppIconView) view.findViewById(R.id.app_icon_img);
                bgVar.d = (TextView) view.findViewById(R.id.app_name_txt);
                bgVar.e = (ListItemInfoView) view.findViewById(R.id.download_info);
                bgVar.f = (DownloadButton) view.findViewById(R.id.state_app_btn);
                bgVar.f3436a = (ImageView) view.findViewById(R.id.sort_num_image);
                bgVar.b = (TextView) view.findViewById(R.id.text_sort);
                bgVar.g = (ImageView) view.findViewById(R.id.line);
                bh bhVar = new bh(this, null);
                bhVar.f3437a = bgVar;
                view.setTag(bhVar);
            } else {
                bgVar = ((bh) view.getTag()).f3437a;
            }
            bgVar.b.setVisibility(8);
            SimpleAppModel simpleAppModel = (SimpleAppModel) getItem(i2);
            a(bgVar, simpleAppModel, i2);
            if (i2 < getCount() - 1) {
                bgVar.g.setVisibility(0);
            } else {
                bgVar.g.setVisibility(8);
            }
            view.setTag(R.id.list_item_pos, Integer.valueOf(i2));
            view.setOnClickListener(new bc(this, simpleAppModel));
        } else {
            if (view == null || ((bh) view.getTag()).b == null) {
                view = this.d.inflate((int) R.layout.search_match_word, (ViewGroup) null);
                biVar = new bi(this, null);
                biVar.f3438a = (TextView) view.findViewById(R.id.content);
                biVar.b = (ImageView) view.findViewById(R.id.line);
                bh bhVar2 = new bh(this, null);
                bhVar2.b = biVar;
                view.setTag(bhVar2);
            } else {
                biVar = ((bh) view.getTag()).b;
            }
            view.setId(i2);
            a(biVar, (String) getItem(i2), i2);
            if (i2 < getCount() - 1) {
                biVar.b.setVisibility(0);
            } else {
                biVar.b.setVisibility(8);
            }
            view.setTag(R.id.search_key_word, getItem(i2));
            view.setOnClickListener(new bd(this));
        }
        return view;
    }

    private void a(bg bgVar, SimpleAppModel simpleAppModel, int i2) {
        if (simpleAppModel != null && bgVar != null) {
            bgVar.d.setText(simpleAppModel.d);
            if (1 == (((int) (simpleAppModel.B >> 2)) & 3)) {
                try {
                    Drawable drawable = this.c.getResources().getDrawable(R.drawable.appdownload_icon_original);
                    drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                    bgVar.d.setCompoundDrawablePadding(by.b(6.0f));
                    bgVar.d.setCompoundDrawables(null, null, drawable, null);
                } catch (Throwable th) {
                    t.a().b();
                }
            } else {
                bgVar.d.setCompoundDrawables(null, null, null, null);
            }
            bgVar.e.a(simpleAppModel);
            bgVar.c.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            bgVar.f.a(simpleAppModel);
            bgVar.f.setTag(R.id.search_app_info, simpleAppModel);
            bgVar.f.setTag(R.id.search_key_word, simpleAppModel.d);
            if (s.a(simpleAppModel)) {
                bgVar.f.setClickable(false);
            } else {
                bgVar.f.setClickable(true);
                STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c, 200);
                if (buildSTInfo != null) {
                    buildSTInfo.scene = this.k;
                    buildSTInfo.slotId = a.a("03", i2);
                    if (simpleAppModel != null) {
                        buildSTInfo.extraData = this.i + ";" + simpleAppModel.f938a;
                    } else {
                        buildSTInfo.extraData = this.i;
                    }
                    buildSTInfo.updateStatus(simpleAppModel);
                    buildSTInfo.updateWithSimpleAppModel(simpleAppModel);
                }
                bgVar.f.a(buildSTInfo, new be(this), (d) null, bgVar.f, bgVar.e);
            }
            com.tencent.assistant.adapter.a.b(this.c, simpleAppModel, bgVar.d, true);
            a(simpleAppModel, i2, null, 100, f3406a);
        }
    }

    private void a(bi biVar, String str, int i2) {
        biVar.f3438a.setText(str);
        a(null, i2, null, 100, b);
    }

    public void a() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        AstApp.i().k().addUIEventListener(1016, this);
    }

    public void b() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        AstApp.i().k().removeUIEventListener(1016, this);
    }

    public void handleUIEvent(Message message) {
        DownloadInfo downloadInfo;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
                if (message.obj instanceof DownloadInfo) {
                    DownloadInfo downloadInfo2 = (DownloadInfo) message.obj;
                    if (downloadInfo2 != null && !TextUtils.isEmpty(downloadInfo2.downloadTicket)) {
                        downloadInfo = downloadInfo2;
                    } else {
                        return;
                    }
                } else {
                    downloadInfo = null;
                }
                if (this.e != null) {
                    Iterator<SimpleAppModel> it = this.e.iterator();
                    while (it.hasNext()) {
                        SimpleAppModel next = it.next();
                        if (downloadInfo != null && next.q().equals(downloadInfo.downloadTicket)) {
                            f();
                        }
                    }
                    return;
                }
                return;
            case 1016:
                k.g(this.e);
                f();
                return;
            default:
                return;
        }
    }

    private void f() {
        ah.a().post(new bf(this));
    }

    public void c() {
        if (this.f != null) {
            this.f.clear();
        }
        if (this.e != null) {
            this.e.clear();
        }
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel, int i2, String str, int i3, int i4) {
        STInfoV2 buildSTInfo;
        if ((this.c instanceof BaseActivity) && (buildSTInfo = STInfoBuilder.buildSTInfo(this.c, i3)) != null) {
            buildSTInfo.scene = this.k;
            if (i4 == f3406a) {
                buildSTInfo.slotId = a.a("03", i2);
                if (simpleAppModel != null) {
                    buildSTInfo.extraData = this.i + ";" + simpleAppModel.f938a;
                } else {
                    buildSTInfo.extraData = this.i;
                }
                buildSTInfo.updateWithSimpleAppModel(simpleAppModel);
            } else {
                buildSTInfo.slotId = a.a("04", i2);
                buildSTInfo.extraData = this.i;
            }
            if (str != null) {
                buildSTInfo.status = str;
            }
            if (i3 == 100) {
                if (this.j == null) {
                    this.j = new b();
                }
                this.j.exposure(buildSTInfo);
                return;
            }
            l.a(buildSTInfo);
        }
    }
}
