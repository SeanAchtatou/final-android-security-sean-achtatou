package com.tencent.pangu.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.AppExCfg;
import com.tencent.assistant.utils.by;
import com.tencent.pangu.component.ba;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class AppDetailAppCfgAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<AppExCfg> f3400a;
    private LayoutInflater b;
    private Context c;

    public int getCount() {
        if (this.f3400a == null) {
            return 0;
        }
        return this.f3400a.size();
    }

    public Object getItem(int i) {
        if (this.f3400a == null || this.f3400a.size() == 0) {
            return null;
        }
        return this.f3400a.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        b bVar;
        AppExCfg appExCfg;
        if (view == null || view.getTag() == null) {
            bVar = new b(this);
            view = this.b.inflate((int) R.layout.appdetail_cfg_item, (ViewGroup) null);
            bVar.f3429a = (TXImageView) view.findViewById(R.id.cfg_icon_img);
            bVar.b = (TextView) view.findViewById(R.id.cfg_desc);
            view.setTag(bVar);
        } else {
            bVar = (b) view.getTag();
        }
        if (this.f3400a == null || i < 0 || i >= this.f3400a.size()) {
            appExCfg = null;
        } else {
            appExCfg = this.f3400a.get(i);
        }
        a(bVar, appExCfg);
        return view;
    }

    private void a(b bVar, AppExCfg appExCfg) {
        if (bVar != null && appExCfg != null) {
            switch (appExCfg.f1150a) {
                case 1:
                    bVar.f3429a.updateImageView(appExCfg.b, R.drawable.icon_libao, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                    a(bVar.f3429a, appExCfg.e, R.color.app_detail_cfg_libao_icon_background);
                    break;
                case 2:
                    bVar.f3429a.updateImageView(appExCfg.b, R.drawable.icon_gonglue, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                    a(bVar.f3429a, appExCfg.e, R.color.app_detail_cfg_gonglue_icon_background);
                    break;
                case 3:
                    bVar.f3429a.updateImageView(appExCfg.b, R.drawable.icon_hua, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                    a(bVar.f3429a, appExCfg.e, R.color.app_detail_cfg_hua_icon_background);
                    break;
                case 4:
                    bVar.f3429a.updateImageView(appExCfg.b, R.drawable.icon_peizhi, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                    a(bVar.f3429a, appExCfg.e, R.color.app_detail_cfg_peizhi_icon_background);
                    break;
                default:
                    bVar.f3429a.updateImageView(appExCfg.b, R.drawable.icon_libao, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                    a(bVar.f3429a, appExCfg.e, R.color.app_detail_cfg_libao_icon_background);
                    break;
            }
            bVar.b.setText(appExCfg.c);
        }
    }

    private void a(ImageView imageView, String str, int i) {
        ba baVar = new ba(this.c);
        baVar.a((float) by.a(this.c, 44.0f));
        if (TextUtils.isEmpty(str)) {
            baVar.a(i);
        } else {
            try {
                baVar.a(str);
            } catch (IllegalArgumentException e) {
                baVar.a(i);
            }
        }
        imageView.setBackgroundDrawable(baVar);
    }
}
