package com.tencent.pangu.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

/* compiled from: ProGuard */
public class bp extends PagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private List<View> f3443a = null;

    public bp(List<View> list) {
        this.f3443a = list;
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        viewGroup.removeView(this.f3443a.get(i));
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        viewGroup.addView(this.f3443a.get(i));
        return this.f3443a.get(i);
    }

    public int getCount() {
        return this.f3443a.size();
    }

    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }
}
