package com.tencent.pangu.adapter;

import android.content.Intent;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class bc implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f3432a;
    final /* synthetic */ SearchMatchAdapter b;

    bc(SearchMatchAdapter searchMatchAdapter, SimpleAppModel simpleAppModel) {
        this.b = searchMatchAdapter;
        this.f3432a = simpleAppModel;
    }

    public void onClick(View view) {
        int i = 0;
        try {
            i = ((Integer) view.getTag(R.id.list_item_pos)).intValue();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.b.a(this.f3432a, i, "01", 200, SearchMatchAdapter.f3406a);
        Intent intent = new Intent(this.b.c, AppDetailActivityV5.class);
        intent.putExtra("simpleModeInfo", this.f3432a);
        StatInfo statInfo = new StatInfo();
        statInfo.sourceScene = STConst.ST_PAGE_SEARCH_SUGGEST;
        statInfo.f2058a = 1;
        statInfo.extraData = this.b.i;
        intent.putExtra("statInfo", statInfo);
        if (this.b.c instanceof BaseActivity) {
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.b.c).f());
        }
        this.b.c.startActivity(intent);
    }
}
