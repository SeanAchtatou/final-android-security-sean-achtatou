package com.tencent.pangu.c;

import android.content.SharedPreferences;
import android.text.format.Time;
import com.qq.AppService.AstApp;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
public class o {

    /* renamed from: a  reason: collision with root package name */
    private static String f3484a;
    private static int b = 0;

    private static SharedPreferences i() {
        return AstApp.i().getApplicationContext().getSharedPreferences("share_pref", 0);
    }

    public static void a(int i) {
        SharedPreferences.Editor edit = i().edit();
        edit.putInt(SocialConstants.PARAM_TYPE, i);
        edit.commit();
    }

    public static int a() {
        return i().getInt(SocialConstants.PARAM_TYPE, -1);
    }

    public static void b(int i) {
        b = i;
    }

    public static int b() {
        return b;
    }

    public static void a(long j) {
        i().edit().putLong("yyb_save_time", j).commit();
    }

    public static long c() {
        return i().getLong("yyb_save_time", 0);
    }

    public static void c(int i) {
        SharedPreferences.Editor edit = i().edit();
        edit.putInt("yybshare_download_num", i);
        edit.commit();
    }

    public static int d() {
        return i().getInt("yybshare_download_num", 0);
    }

    public static void e() {
        SharedPreferences.Editor edit = i().edit();
        edit.putBoolean("yyb_need_show", false);
        edit.commit();
    }

    public static void f() {
        long currentTimeMillis = System.currentTimeMillis();
        SharedPreferences.Editor edit = i().edit();
        edit.putLong("app_share_time", currentTimeMillis);
        edit.commit();
    }

    public static boolean g() {
        long j = i().getLong("app_share_time", 0);
        if (j != 0 && Time.getJulianDay(System.currentTimeMillis(), 0) <= Time.getJulianDay(j, 0)) {
            return false;
        }
        return true;
    }

    public static void a(String str) {
        f3484a = str;
    }

    public static String h() {
        String str = f3484a;
        f3484a = Constants.STR_EMPTY;
        return str;
    }
}
