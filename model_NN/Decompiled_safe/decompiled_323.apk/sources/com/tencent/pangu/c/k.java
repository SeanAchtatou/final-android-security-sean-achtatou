package com.tencent.pangu.c;

import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.model.ShareAppModel;

/* compiled from: ProGuard */
class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f3480a;

    k(e eVar) {
        this.f3480a = eVar;
    }

    public void run() {
        if (this.f3480a.e == null) {
            return;
        }
        if (!this.f3480a.m) {
            DialogUtils.showShareQzDialog(this.f3480a.e.get(), this.f3480a.d, this.f3480a.o);
        } else if (this.f3480a.d instanceof ShareAppModel) {
            ShareAppModel shareAppModel = (ShareAppModel) this.f3480a.d;
            if (shareAppModel.b == null) {
                shareAppModel.b = Constants.STR_EMPTY;
            }
            this.f3480a.a(shareAppModel.e, this.f3480a.c(shareAppModel), this.f3480a.b(shareAppModel), shareAppModel.b, shareAppModel.f, null);
        }
    }
}
