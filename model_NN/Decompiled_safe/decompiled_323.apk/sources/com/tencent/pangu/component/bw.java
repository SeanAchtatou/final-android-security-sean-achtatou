package com.tencent.pangu.component;

import android.content.Context;
import android.text.TextUtils;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.pangu.mediadownload.a;
import com.tencent.pangu.mediadownload.q;

/* compiled from: ProGuard */
class bw extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    private q f3663a;
    private bx b;

    public bw(Context context, q qVar, int i, bx bxVar) {
        this.f3663a = qVar;
        this.b = bxVar;
        this.titleRes = context.getResources().getString(R.string.video_down_tips);
        String string = i == -3 ? context.getResources().getString(R.string.video_down_tips_older) : context.getResources().getString(R.string.video_down_tips_content);
        Object[] objArr = new Object[1];
        objArr[0] = TextUtils.isEmpty(qVar.i) ? context.getResources().getString(R.string.video_down_tips_unknow_player) : qVar.i;
        this.contentRes = String.format(string, objArr);
        this.lBtnTxtRes = context.getResources().getString(R.string.video_down_cancel);
        this.rBtnTxtRes = context.getResources().getString(R.string.video_down_install);
    }

    public void onLeftBtnClick() {
    }

    public void onRightBtnClick() {
        a.a().a(this.f3663a.e, this.f3663a.h, true);
        if (this.b != null) {
            this.b.a();
        }
    }

    public void onCancell() {
    }
}
