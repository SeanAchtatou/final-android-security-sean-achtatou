package com.tencent.pangu.component.hotwords;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/* compiled from: ProGuard */
public class HotwordsItem extends RelativeLayout {
    public HotwordsItem(Context context) {
        super(context);
    }

    public HotwordsItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }
}
