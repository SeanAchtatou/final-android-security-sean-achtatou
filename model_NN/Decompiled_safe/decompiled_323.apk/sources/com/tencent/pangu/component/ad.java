package com.tencent.pangu.component;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.m;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.l;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ProGuard */
class ad extends ViewInvalidateMessageHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankCustomizeListView f3513a;

    ad(RankCustomizeListView rankCustomizeListView) {
        this.f3513a = rankCustomizeListView;
    }

    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        if (viewInvalidateMessage.what == 1) {
            int i = viewInvalidateMessage.arg1;
            int i2 = viewInvalidateMessage.arg2;
            Map map = (Map) viewInvalidateMessage.params;
            boolean booleanValue = ((Boolean) map.get("isFirstPage")).booleanValue();
            boolean booleanValue2 = ((Boolean) map.get("hasNext")).booleanValue();
            Map map2 = (Map) map.get("key_data");
            if (!(map2 == null || map2.size() <= 0 || this.f3513a.z == null)) {
                long j = -1;
                if (this.f3513a.y != null) {
                    j = this.f3513a.y.c();
                }
                if (booleanValue) {
                    this.f3513a.z.a(map2, j);
                    l.a((int) STConst.ST_PAGE_RANK_RECOMMEND, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                    int i3 = 0;
                    for (AppGroupInfo appGroupInfo : map2.keySet()) {
                        Iterator it = ((ArrayList) map2.get(appGroupInfo)).iterator();
                        while (it.hasNext()) {
                            if (((SimpleAppModel) it.next()).u() == AppConst.AppState.INSTALLED) {
                                i3++;
                            }
                        }
                    }
                    if ((i3 < 5 || !m.a().aj()) && ((!this.f3513a.v && !m.a().al()) || (this.f3513a.v && !m.a().am()))) {
                        this.f3513a.j = false;
                        this.f3513a.g.setVisibility(8);
                        this.f3513a.g.postDelayed(new ae(this), 200);
                    } else {
                        this.f3513a.j = true;
                        this.f3513a.l();
                        m.a().x(false);
                        if (this.f3513a.v) {
                            m.a().A(false);
                        } else {
                            m.a().z(false);
                        }
                    }
                } else {
                    this.f3513a.z.b(map2, j);
                }
                for (int i4 = 0; i4 < this.f3513a.z.getGroupCount(); i4++) {
                    this.f3513a.expandGroup(i4);
                }
            }
            this.f3513a.a(i2, i, booleanValue, booleanValue2);
            return;
        }
        this.f3513a.A.b();
        if (this.f3513a.z != null) {
            this.f3513a.z.notifyDataSetChanged();
        }
    }
}
