package com.tencent.pangu.component;

import android.view.View;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistantv2.component.t;

/* compiled from: ProGuard */
class e implements t {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentDetailView f3667a;

    e(CommentDetailView commentDetailView) {
        this.f3667a = commentDetailView;
    }

    public void a(int i) {
        if (i <= 2) {
            this.f3667a.j.b(8);
            return;
        }
        this.f3667a.j.b(0);
        if (i > 3) {
            this.f3667a.j.c(3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.CommentDetailView.a(com.tencent.pangu.component.CommentDetailView, boolean):boolean
     arg types: [com.tencent.pangu.component.CommentDetailView, int]
     candidates:
      com.tencent.pangu.component.CommentDetailView.a(com.tencent.pangu.component.CommentDetailView, com.tencent.assistant.protocol.jce.CommentTagInfo):com.tencent.assistant.protocol.jce.CommentTagInfo
      com.tencent.pangu.component.CommentDetailView.a(com.tencent.pangu.component.CommentDetailView, int):void
      com.tencent.pangu.component.CommentDetailView.a(com.tencent.pangu.component.CommentDetailView, boolean):boolean */
    public void a(View view, CommentTagInfo commentTagInfo) {
        boolean unused = this.f3667a.H = true;
        CommentTagInfo unused2 = this.f3667a.N = commentTagInfo;
        this.f3667a.a(commentTagInfo);
    }
}
