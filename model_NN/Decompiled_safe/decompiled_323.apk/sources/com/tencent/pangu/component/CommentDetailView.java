package com.tencent.pangu.component;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.RatingView;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.protocol.jce.RatingInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.bo;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.t;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.socialcontact.comment.CommentFooterView;
import com.tencent.nucleus.socialcontact.comment.CommentHeaderTagView;
import com.tencent.nucleus.socialcontact.comment.a;
import com.tencent.nucleus.socialcontact.comment.bc;
import com.tencent.nucleus.socialcontact.comment.d;
import com.tencent.nucleus.socialcontact.comment.g;
import com.tencent.nucleus.socialcontact.comment.j;
import com.tencent.nucleus.socialcontact.comment.k;
import com.tencent.nucleus.socialcontact.login.l;
import com.tencent.pangu.component.appdetail.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/* compiled from: ProGuard */
public class CommentDetailView extends LinearLayout implements ITXRefreshListViewListener {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f3490a = false;
    private long A;
    private boolean B = false;
    private f C;
    private bc D = new bc();
    private a E = a.a();
    /* access modifiers changed from: private */
    public d F = new d();
    private int G = 0;
    /* access modifiers changed from: private */
    public boolean H = false;
    /* access modifiers changed from: private */
    public String[] I = {"+1", "好", "顶", "赞", "朕觉OK", "不明觉厉", "吊炸天"};
    private int[] J = {R.drawable.pinglun_icon_jinghua1, R.drawable.pinglun_icon_jinghua2};
    /* access modifiers changed from: private */
    public String K = "08_";
    /* access modifiers changed from: private */
    public String L = "09_";
    /* access modifiers changed from: private */
    public String M = "13_";
    /* access modifiers changed from: private */
    public CommentTagInfo N;
    private RelativeLayout O;
    private ArrayList<CommentTagInfo> P = null;
    private View.OnClickListener Q = new f(this);
    private final int R = 1;
    private final int S = 2;
    private final String T = "isFirstPage";
    private final String U = "data";
    private final String V = "selectedData";
    private final String W = "hasNext";
    private final String Z = "curVerComment";
    private final String aa = "comment";
    private final String ab = "oldCommentId";
    private final String ac = "taglist";
    private final String ad = "reqTagInfo";
    /* access modifiers changed from: private */
    public ViewPageScrollListener ae;
    /* access modifiers changed from: private */
    public ViewInvalidateMessageHandler af = new h(this);
    /* access modifiers changed from: private */
    public int ag = 0;
    /* access modifiers changed from: private */
    public Runnable ah = new i(this);
    public t b = new e(this);
    public j c = new g(this);
    /* access modifiers changed from: private */
    public Context d;
    private LayoutInflater e;
    /* access modifiers changed from: private */
    public LinearLayout f;
    /* access modifiers changed from: private */
    public ArrayList<CommentDetail> g = new ArrayList<>();
    /* access modifiers changed from: private */
    public LinearLayout h;
    /* access modifiers changed from: private */
    public ArrayList<CommentDetail> i;
    /* access modifiers changed from: private */
    public CommentHeaderTagView j;
    private TextView k;
    private TextView l;
    private View m;
    /* access modifiers changed from: private */
    public CommentFooterView n;
    /* access modifiers changed from: private */
    public k o = null;
    /* access modifiers changed from: private */
    public l p;
    /* access modifiers changed from: private */
    public boolean q = false;
    /* access modifiers changed from: private */
    public SimpleAppModel r = null;
    private RatingInfo s = null;
    /* access modifiers changed from: private */
    public long t = 0;
    private long u = 0;
    /* access modifiers changed from: private */
    public int v;
    private NormalErrorRecommendPage w;
    private LoadingView x = null;
    private int y = 3;
    private g z = new g();

    static /* synthetic */ int q(CommentDetailView commentDetailView) {
        int i2 = commentDetailView.ag;
        commentDetailView.ag = i2 + 1;
        return i2;
    }

    public CommentDetailView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.d = context;
        k();
    }

    public CommentDetailView(Context context) {
        super(context);
        this.d = context;
        k();
    }

    public void a(Map<String, Object> map) {
        if (map != null) {
            b(map);
            l();
        }
    }

    public g a() {
        if (this.z == null) {
            this.z = new g();
        }
        return this.z;
    }

    private void k() {
        this.e = LayoutInflater.from(this.d);
        this.e.inflate((int) R.layout.comment_detail_activity, this);
        this.k = (TextView) findViewById(R.id.select_comment_numbers);
        this.h = (LinearLayout) findViewById(R.id.select_comment_layout);
        this.l = (TextView) findViewById(R.id.select_comment_numbers_txt);
        this.f = (LinearLayout) findViewById(R.id.comment_Layout);
        this.O = (RelativeLayout) findViewById(R.id.comment_empty_layout);
        this.w = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.w.setButtonClickListener(new d(this));
        this.x = (LoadingView) findViewById(R.id.loading_view);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.x.getLayoutParams();
        layoutParams.topMargin = (int) (-80.0f * com.tencent.assistant.utils.t.d);
        this.x.setLayoutParams(layoutParams);
    }

    private void l() {
        m();
        n();
        o();
    }

    private void b(Map<String, Object> map) {
        this.r = (SimpleAppModel) map.get("simpleModeInfo");
        this.s = (RatingInfo) map.get("ratingInfo");
        this.t = ((Long) map.get("apkId")).longValue();
        this.u = ((Long) map.get("count")).longValue();
        this.v = ((Integer) map.get("versionCode")).intValue();
        this.A = ((Long) map.get("replyId")).longValue();
        this.P = (ArrayList) map.get("alltaglist");
    }

    public void a(l lVar) {
        this.p = lVar;
    }

    public void a(CommentTagInfo commentTagInfo) {
        q();
        this.q = true;
        this.o.a(this.r.f938a, this.t, this.r.c, this.r.g, commentTagInfo, this.P);
    }

    public void b() {
        if (this.n != null) {
            this.n.a(2);
            this.o.b();
        }
    }

    private void m() {
        this.j = (CommentHeaderTagView) findViewById(R.id.comment_header);
        this.j.a(String.valueOf(((double) Math.round(this.s.b * 10.0d)) / 10.0d));
        long j2 = this.s.f1453a;
        this.j.b(String.format(getResources().getString(R.string.comment_detail_average_users), bm.a(j2)));
        this.j.a(this.b);
        this.m = findViewById(R.id.comment_txt_area);
    }

    private void n() {
        this.n = new CommentFooterView(this.d, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.NONE);
        this.n.setOnClickListener(this.Q);
        this.n.a(1);
    }

    public int c() {
        if (this.n == null) {
            return 1;
        }
        return this.n.a();
    }

    private void o() {
        this.j.setVisibility(8);
        this.k.setVisibility(8);
        this.h.setVisibility(8);
        this.l.setVisibility(8);
        this.f.setVisibility(8);
    }

    public void d() {
        if (this.o == null) {
            this.o = new k();
        }
        this.o.register(this.c);
        if (this.z == null) {
            this.z = new g();
        }
        this.z.register(this.c);
        this.E.register(this.c);
        this.D.register(this.c);
        this.F.register(this.c);
    }

    public boolean e() {
        if (this.o != null) {
            return this.o.c();
        }
        return false;
    }

    public void f() {
        if (this.o == null) {
        }
    }

    private void c(CommentDetail commentDetail) {
        HashMap hashMap = (HashMap) this.s.c;
        hashMap.put(Integer.valueOf(commentDetail.b), Long.valueOf(((Long) hashMap.get(Integer.valueOf(commentDetail.b))).longValue() + 1));
        RatingInfo ratingInfo = this.s;
        long j2 = ratingInfo.f1453a + 1;
        ratingInfo.f1453a = j2;
        this.j.b(String.format(getResources().getString(R.string.comment_detail_average_users), bm.a(j2)));
    }

    /* access modifiers changed from: private */
    public void p() {
        this.x.setVisibility(8);
        findViewById(R.id.bottom_view).setVisibility(0);
        this.f.setVisibility(0);
        this.j.setVisibility(0);
        this.w.setVisibility(8);
        this.m.setVisibility(0);
        this.O.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        this.h.setVisibility(i2);
        if (!(this.k == null || this.l == null)) {
            this.k.setVisibility(i2);
            this.l.setVisibility(i2);
        }
        if (i2 == 0 && this.m != null) {
            this.m.setVisibility(0);
        }
    }

    private void q() {
        this.x.setVisibility(0);
        if (this.f != null) {
            this.f.setVisibility(8);
        }
        if (!this.H && this.j != null) {
            this.j.setVisibility(8);
        }
        if (this.h != null) {
            b(8);
        }
        if (this.w != null) {
            this.w.setVisibility(8);
        }
        this.O.setVisibility(8);
    }

    public void g() {
        this.x.setVisibility(8);
    }

    private void c(int i2) {
        this.x.setVisibility(8);
        this.f.setVisibility(8);
        b(8);
        this.O.setVisibility(8);
        this.w.setVisibility(0);
        this.w.setErrorType(i2);
        findViewById(R.id.bottom_view).setVisibility(8);
    }

    public void a(CommentDetail commentDetail) {
        c(commentDetail);
        if (this.g.size() > 0 && this.g.get(0).l) {
            ((TextView) this.f.getChildAt(0).findViewById(R.id.nick_name)).setText(this.g.get(0).c);
        }
        this.g.add(0, commentDetail);
        this.f.addView(a(commentDetail, 0, 0), 0);
    }

    public void a(ViewPageScrollListener viewPageScrollListener) {
        this.ae = viewPageScrollListener;
    }

    /* access modifiers changed from: private */
    public void a(int i2, boolean z2, CommentTagInfo commentTagInfo, List<CommentDetail> list, List<CommentDetail> list2, List<CommentTagInfo> list3, boolean z3, CommentDetail commentDetail) {
        if (i2 == 0) {
            p();
            if (z2) {
                b(8);
                if (list3 != null && list3.size() > 0) {
                    this.j.a(commentTagInfo, list3);
                    this.j.a(0);
                }
                if (this.H) {
                    this.g.clear();
                    this.f.removeAllViews();
                    requestLayout();
                    this.H = false;
                }
                if (list == null || list.size() == 0) {
                    this.O.setVisibility(0);
                    this.n.setVisibility(8);
                    this.m.setVisibility(8);
                    findViewById(R.id.bottom_view).setVisibility(8);
                } else {
                    this.O.setVisibility(8);
                    a(list);
                    this.g.addAll(list);
                    if (this.o.a()) {
                        this.n.a(1);
                    } else {
                        this.n.setVisibility(8);
                    }
                }
                if (commentDetail == null || commentDetail.h <= 0) {
                    this.p.a(-1, Constants.STR_EMPTY, -1, -1);
                } else {
                    this.p.a(commentDetail.h, commentDetail.g, commentDetail.b, commentDetail.d);
                }
            } else {
                this.O.setVisibility(8);
                if (list != null && list.size() > 0) {
                    a(list);
                    if (this.g != null) {
                        this.g.addAll(list);
                    }
                }
                if (this.o.a()) {
                    this.n.a(1);
                } else {
                    this.n.setVisibility(8);
                }
            }
            if (!z3) {
                this.n.setVisibility(8);
                this.n.a(3);
                return;
            }
            return;
        }
        this.p.a(-2, Constants.STR_EMPTY, -1, -1);
        if (!z2) {
            this.n.a(4);
        } else if (-800 == i2) {
            c(30);
        } else if (this.y <= 0) {
            c(20);
        } else {
            this.o.a(this.r.f938a, this.t, this.r.c, this.r.g, null, this.P);
            this.y--;
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, CommentDetail commentDetail, long j2) {
        boolean z2 = false;
        if (l.b(i2)) {
            if (i2 == 0) {
                p();
                if (commentDetail != null) {
                    b(commentDetail);
                    a(commentDetail);
                }
            } else if (i2 != 1) {
                i();
            } else if (commentDetail != null) {
                b(j2);
                this.g.add(0, commentDetail);
                this.f.addView(a(commentDetail, 0, 0), 0);
                Toast.makeText(this.d, this.d.getString(R.string.comment_modify_succ), 0).show();
            }
            if (this.p != null) {
                long j3 = -1;
                if (commentDetail != null) {
                    j3 = commentDetail.h;
                }
                l lVar = this.p;
                if (i2 == 0 || i2 == 1) {
                    z2 = true;
                }
                lVar.a(z2, j3);
            }
        }
    }

    public void b(CommentTagInfo commentTagInfo) {
        if (this.j != null) {
            this.j.a(commentTagInfo);
        }
    }

    public void h() {
        if (this.j != null) {
            this.j.a(this.N);
        }
    }

    private void b(long j2) {
        if (this.i != null) {
            for (int i2 = 0; i2 < this.i.size(); i2++) {
                if (this.i.get(i2).h == j2) {
                    this.i.remove(i2);
                    this.h.removeViewAt(i2);
                    return;
                }
            }
        }
        if (this.g != null) {
            for (int i3 = 0; i3 < this.g.size(); i3++) {
                if (this.g.get(i3).h == j2) {
                    this.g.remove(i3);
                    this.f.removeViewAt(i3);
                    return;
                }
            }
        }
    }

    public void i() {
        Toast.makeText(this.d, this.d.getString(R.string.comment_failed), 0).show();
    }

    public void b(CommentDetail commentDetail) {
        if (commentDetail.a() < this.v) {
            Toast.makeText(this.d, this.d.getString(R.string.comment_succ_old), 0).show();
        } else {
            Toast.makeText(this.d, this.d.getString(R.string.comment_succ), 0).show();
        }
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        b();
    }

    private void a(List<CommentDetail> list) {
        View a2;
        this.f.removeView(this.n);
        this.G = this.f.getChildCount();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < list.size()) {
                if (!d(list.get(i3)) && (a2 = a(list.get(i3), i3, this.G)) != null) {
                    this.f.addView(a2);
                }
                i2 = i3 + 1;
            } else {
                this.f.addView(this.n);
                requestLayout();
                return;
            }
        }
    }

    private boolean d(CommentDetail commentDetail) {
        if (this.i != null) {
            for (int i2 = 0; i2 < this.i.size(); i2++) {
                if (this.i.get(i2).h == commentDetail.h) {
                    return true;
                }
            }
        }
        if (this.g == null) {
            return false;
        }
        for (int i3 = 0; i3 < this.g.size(); i3++) {
            if (this.g.get(i3).h == commentDetail.h) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        super.onLayout(z2, i2, i3, i4, i5);
        if (this.B) {
            this.B = false;
            if (this.C != null) {
                this.C.b(this.f.getTop());
            }
        }
    }

    public void a(int i2) {
        this.x.getLayoutParams().height = i2;
        this.w.getLayoutParams().height = i2;
    }

    public void a(long j2, long j3, int i2, long j4) {
        View childAt;
        int i3 = 0;
        while (i3 < this.g.size()) {
            if (this.g.get(i3).h != j2) {
                i3++;
            } else if (this.f != null && (childAt = this.f.getChildAt(i3)) != null) {
                CommentDetail commentDetail = (CommentDetail) childAt.getTag(R.id.comment_page_detail);
                if (commentDetail != null && commentDetail.h == j2) {
                    commentDetail.v = j3;
                    commentDetail.s = (byte) i2;
                    commentDetail.r = j4;
                    childAt.setTag(R.id.comment_page_detail, commentDetail);
                }
                TextView textView = (TextView) childAt.findViewById(R.id.replycount);
                if (textView != null) {
                    textView.setText(j3 + Constants.STR_EMPTY);
                }
                TextView textView2 = (TextView) childAt.findViewById(R.id.likecount);
                if (textView2 != null) {
                    textView2.setText(bm.a(j4) + Constants.STR_EMPTY);
                    textView2.setTag(R.id.comment_praise_count, Long.valueOf(j4));
                    Drawable drawable = this.d.getResources().getDrawable(R.drawable.pinglun_icon_zan);
                    drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                    Drawable drawable2 = this.d.getResources().getDrawable(R.drawable.pinglun_icon_yizan);
                    drawable2.setBounds(0, 0, drawable2.getMinimumWidth(), drawable2.getMinimumHeight());
                    textView2.setCompoundDrawablePadding(by.a(this.d, 6.0f));
                    if (i2 == 1) {
                        textView2.setCompoundDrawables(drawable2, null, null, null);
                        textView2.setTextColor(Color.parseColor("#b68a46"));
                        textView2.setTag(true);
                        return;
                    }
                    textView2.setCompoundDrawables(drawable, null, null, null);
                    textView2.setTextColor(Color.parseColor("#6e6e6e"));
                    textView2.setTag(false);
                    return;
                }
                return;
            } else {
                return;
            }
        }
    }

    private View a(CommentDetail commentDetail, int i2, int i3) {
        long j2;
        m mVar = new m(this, null);
        View inflate = this.e.inflate((int) R.layout.comment_view, (ViewGroup) null);
        inflate.setTag(R.id.comment_page_detail, commentDetail);
        ((TXAppIconView) inflate.findViewById(R.id.picture)).updateImageView(commentDetail.i, R.drawable.common_icon_useravarta_default, TXImageView.TXImageViewType.ROUND_IMAGE);
        TextView textView = (TextView) inflate.findViewById(R.id.nick_name);
        TextView textView2 = (TextView) inflate.findViewById(R.id.likeanimation);
        TextView textView3 = (TextView) inflate.findViewById(R.id.likecount);
        TextView textView4 = (TextView) inflate.findViewById(R.id.replycount);
        TextView textView5 = (TextView) inflate.findViewById(R.id.myorfriends);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.colorselect);
        if (commentDetail.l && i2 == 0) {
            textView.setText((int) R.string.comment_nick_my_comment);
            textView.setTextColor(Color.parseColor("#ff7a0c"));
            textView5.setVisibility(8);
        } else if (TextUtils.isEmpty(commentDetail.c)) {
            textView.setText((int) R.string.comment_default_nick_name);
            textView5.setVisibility(8);
        } else if (!commentDetail.c.startsWith("(好友)")) {
            textView.setText(commentDetail.c);
            textView5.setVisibility(8);
        } else {
            String substring = commentDetail.c.substring(4);
            if (substring.length() > 8) {
                substring = substring.substring(0, 8) + "...";
            }
            if (TextUtils.isEmpty(substring)) {
                textView.setText((int) R.string.comment_default_nick_name);
                textView5.setVisibility(8);
            } else {
                textView.setText(substring);
                textView.setTextColor(Color.parseColor("#ff7a0c"));
                textView5.setVisibility(0);
            }
        }
        mVar.b = textView;
        mVar.c = textView5;
        RatingView ratingView = (RatingView) inflate.findViewById(R.id.comment_score);
        mVar.f3683a = ratingView;
        ratingView.setRating((float) commentDetail.b);
        TextView textView6 = (TextView) inflate.findViewById(R.id.time);
        mVar.d = textView6;
        if (commentDetail.m <= 0) {
            j2 = commentDetail.f1202a;
        } else {
            j2 = commentDetail.m;
        }
        textView6.setText(a(j2 * 1000));
        TextView textView7 = (TextView) inflate.findViewById(R.id.content);
        mVar.e = textView7;
        textView7.setText(commentDetail.g);
        if (commentDetail.t) {
            imageView.setImageResource(this.J[new Random().nextInt(this.J.length)]);
            imageView.setVisibility(0);
        }
        textView3.setText(bm.a(commentDetail.b()) + Constants.STR_EMPTY);
        textView3.setTag(R.id.comment_praise_count, Long.valueOf(commentDetail.b()));
        Drawable drawable = getResources().getDrawable(R.drawable.pinglun_icon_zan);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        Drawable drawable2 = getResources().getDrawable(R.drawable.pinglun_icon_yizan);
        drawable2.setBounds(0, 0, drawable2.getMinimumWidth(), drawable2.getMinimumHeight());
        textView3.setCompoundDrawablePadding(by.a(this.d, 6.0f));
        if (commentDetail.c() == 0) {
            textView3.setCompoundDrawables(drawable, null, null, null);
            textView3.setTextColor(Color.parseColor("#a4a4a4"));
            textView3.setTag(false);
        } else if (commentDetail.c() == 1) {
            textView3.setCompoundDrawables(drawable2, null, null, null);
            textView3.setTextColor(Color.parseColor("#b68a46"));
            textView3.setTag(true);
        } else {
            textView3.setCompoundDrawables(drawable, null, null, null);
            textView3.setTextColor(Color.parseColor("#a4a4a4"));
            textView3.setTag(false);
        }
        com.tencent.assistantv2.st.l.a(new STInfoV2(STConst.ST_PAGE_APP_DETAIL_COMMENT, this.K + bm.a(i3 + i2 + 1), 0, STConst.ST_DEFAULT_SLOT, 100));
        textView3.setTag(R.id.tma_st_slot_tag, bm.a(i3 + i2 + 1));
        textView3.setOnClickListener(new j(this, drawable, commentDetail, i3, i2, inflate, drawable2, textView2));
        mVar.g = textView3;
        textView4.setText(commentDetail.v + Constants.STR_EMPTY);
        textView4.setTextColor(Color.parseColor("#a4a4a4"));
        textView4.setTag(R.id.tma_st_slot_tag, bm.a(i3 + i2 + 1));
        textView4.setOnClickListener(new k(this, inflate));
        TextView textView8 = (TextView) inflate.findViewById(R.id.version);
        if (!TextUtils.isEmpty(com.tencent.assistant.utils.t.v()) && com.tencent.assistant.utils.t.v().equals("LGE-Nexus 5") && Build.VERSION.SDK_INT >= 20) {
            textView8.setPadding(textView8.getPaddingLeft(), getResources().getDimensionPixelSize(R.dimen.app_detail_float_share_yyb_dialog_y), textView8.getPaddingRight(), textView8.getBottom());
            ratingView.setPadding(ratingView.getPaddingLeft(), getResources().getDimensionPixelSize(R.dimen.app_detail_float_share_yyb_dialog_y), ratingView.getPaddingRight(), ratingView.getBottom());
        }
        if (commentDetail.d > 0) {
            textView8.setVisibility(0);
            if (this.v <= commentDetail.d) {
                textView8.setText(this.d.getString(R.string.comment_detail_current_version));
            } else {
                textView8.setText(commentDetail.u);
            }
        }
        inflate.setTag(mVar);
        return inflate;
    }

    /* access modifiers changed from: private */
    public void a(m mVar, String str, int i2, long j2, String str2, boolean z2) {
        if (i2 > 0) {
            mVar.f3683a.setRating((float) i2);
        }
        if (!TextUtils.isEmpty(str)) {
            mVar.e.setText(str);
        }
        if (!TextUtils.isEmpty(str2)) {
            mVar.b.setText(str2);
        }
        if (j2 > 0) {
            mVar.d.setText(a(1000 * j2));
        }
    }

    public static String a(long j2) {
        return bo.g(j2);
    }

    public bc j() {
        return this.D;
    }
}
