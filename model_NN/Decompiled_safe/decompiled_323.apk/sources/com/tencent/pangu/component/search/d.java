package com.tencent.pangu.component.search;

import android.content.Intent;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.module.k;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchExplicitHotWordView f3701a;

    d(SearchExplicitHotWordView searchExplicitHotWordView) {
        this.f3701a = searchExplicitHotWordView;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.container /*2131165301*/:
                if (this.f3701a.k != null) {
                    this.f3701a.k.onClick(view);
                }
                this.f3701a.a(this.f3701a.o, "02", 200);
                return;
            case R.id.state_app_btn /*2131165445*/:
                this.f3701a.a(this.f3701a.o, view, "001");
                AppConst.AppState d = k.d(this.f3701a.o);
                int a2 = a.a(d);
                this.f3701a.a(this.f3701a.o, a.a(d, this.f3701a.o), a2);
                return;
            case R.id.app_view /*2131166418*/:
                Intent intent = new Intent(this.f3701a.p, AppDetailActivityV5.class);
                intent.putExtra("simpleModeInfo", this.f3701a.o);
                StatInfo statInfo = new StatInfo();
                statInfo.sourceScene = STConst.ST_PAGE_SEARCH;
                statInfo.extraData = this.f3701a.n;
                intent.putExtra("statInfo", statInfo);
                if (this.f3701a.p instanceof BaseActivity) {
                    intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.f3701a.p).f());
                }
                this.f3701a.p.startActivity(intent);
                this.f3701a.a(this.f3701a.o, "01", 200);
                return;
            default:
                return;
        }
    }
}
