package com.tencent.pangu.component.appdetail.process;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.manager.u;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.h;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.nucleus.socialcontact.comment.PopViewDialog;
import com.tencent.pangu.activity.AppDetailActivityV5;
import com.tencent.pangu.model.ShareAppModel;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public Context f3605a = null;
    public boolean b = false;
    private e c;
    private AppdetailActionUIListener d;
    private i e = null;
    private n f = null;
    private m g = null;
    private SimpleAppModel h;
    private ShareAppModel i;
    private long j;
    private boolean k;
    private PopViewDialog l;
    private STInfoV2 m = null;
    private Bundle n;
    private boolean o;
    private byte p;

    public void a(SimpleAppModel simpleAppModel, long j2, boolean z, PopViewDialog popViewDialog, STInfoV2 sTInfoV2, Bundle bundle, boolean z2, byte b2, Context context, boolean z3, m mVar) {
        this.f3605a = context;
        this.h = simpleAppModel;
        this.j = j2;
        this.k = z;
        this.l = popViewDialog;
        this.m = sTInfoV2;
        this.n = bundle;
        this.o = z2;
        this.b = z3;
        this.g = mVar;
        u.a(j2, b2);
        this.p = b2;
        b();
    }

    public void a(ShareAppModel shareAppModel) {
        this.i = shareAppModel;
    }

    public SimpleAppModel a() {
        return this.h;
    }

    public void b() {
        boolean z;
        byte b2;
        boolean z2;
        boolean z3 = false;
        String string = this.n.getString(com.tencent.assistant.a.a.g);
        String string2 = this.n.getString(com.tencent.assistant.a.a.s);
        if (!TextUtils.isEmpty(string) && String.valueOf(1).equals(string) && string2 == null) {
            string2 = String.valueOf(1);
        }
        boolean z4 = s.a(this.p, (byte) 4) || s.a(this.p, (byte) 5);
        if (TextUtils.isEmpty(string) || TextUtils.isEmpty(string2) || (!this.h.d() && !this.h.e())) {
            z = false;
        } else {
            z = true;
        }
        if (!z4 && (z || this.h.d() || this.h.e())) {
            if (!TextUtils.isEmpty(string)) {
                try {
                    b2 = Byte.valueOf(string).byteValue();
                } catch (NumberFormatException e2) {
                    b2 = 0;
                }
            } else {
                b2 = this.h.P;
            }
            if ((b2 & 1) != 0) {
                z2 = true;
            } else {
                z2 = false;
            }
            if ((b2 & 2) != 0) {
                z3 = true;
            }
            if (z2 && z3) {
                this.f = new n(this);
                this.c = this.f;
                this.c.a(true);
            } else if (z2) {
                this.c = new n(this);
                this.c.a(true);
            } else if (z3) {
                this.c = new i(this);
                this.c.a(true);
            } else {
                this.c = new f(this);
            }
        } else if (this.o) {
            this.c = new b(this);
        } else {
            this.c = new f(this);
        }
        if (this.c != null) {
            this.c.a(this.d);
            this.c.a(this.h, this.j, this.k, this.l, null, this.n, this.o, this.f3605a);
            this.c.a(this.g);
        }
    }

    public void a(AppdetailActionUIListener appdetailActionUIListener) {
        this.d = appdetailActionUIListener;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0093  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(android.view.View r12) {
        /*
            r11 = this;
            r11.k()
            int r0 = r11.j()
            android.os.Bundle r1 = r11.n
            java.lang.String r2 = com.tencent.assistant.a.a.s
            java.lang.String r1 = r1.getString(r2)
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x0057
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ NumberFormatException -> 0x0053 }
            int r0 = r1.intValue()     // Catch:{ NumberFormatException -> 0x0053 }
            r10 = r0
        L_0x001e:
            int r0 = r12.getId()
            switch(r0) {
                case 2131165677: goto L_0x0059;
                case 2131165678: goto L_0x0025;
                case 2131165679: goto L_0x0025;
                case 2131165680: goto L_0x0093;
                default: goto L_0x0025;
            }
        L_0x0025:
            com.tencent.pangu.component.appdetail.process.e r0 = r11.c
            if (r0 == 0) goto L_0x00ce
            int r0 = r12.getId()
            r1 = 2131165664(0x7f0701e0, float:1.7945551E38)
            if (r0 == r1) goto L_0x00ce
            int r0 = r12.getId()
            r1 = 2131165667(0x7f0701e3, float:1.7945558E38)
            if (r0 == r1) goto L_0x00ce
            int r0 = r12.getId()
            r1 = 2131165684(0x7f0701f4, float:1.7945592E38)
            if (r0 == r1) goto L_0x00ce
            int r0 = r12.getId()
            r1 = 2131165686(0x7f0701f6, float:1.7945596E38)
            if (r0 == r1) goto L_0x00ce
            com.tencent.pangu.component.appdetail.process.e r0 = r11.c
            r0.a()
        L_0x0052:
            return
        L_0x0053:
            r1 = move-exception
            r1.printStackTrace()
        L_0x0057:
            r10 = r0
            goto L_0x001e
        L_0x0059:
            com.tencent.pangu.component.appdetail.process.n r0 = r11.f
            if (r0 != 0) goto L_0x0086
            com.tencent.pangu.component.appdetail.process.n r0 = new com.tencent.pangu.component.appdetail.process.n
            r0.<init>(r11)
            r11.f = r0
            com.tencent.pangu.component.appdetail.process.n r0 = r11.f
            com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener r1 = r11.d
            r0.a(r1)
            com.tencent.pangu.component.appdetail.process.n r0 = r11.f
            com.tencent.pangu.component.appdetail.process.m r1 = r11.g
            r0.a(r1)
            com.tencent.pangu.component.appdetail.process.n r0 = r11.f
            com.tencent.assistant.model.SimpleAppModel r1 = r11.h
            long r2 = r11.j
            boolean r4 = r11.k
            com.tencent.nucleus.socialcontact.comment.PopViewDialog r5 = r11.l
            r6 = 0
            android.os.Bundle r7 = r11.n
            boolean r8 = r11.o
            android.content.Context r9 = r11.f3605a
            r0.a(r1, r2, r4, r5, r6, r7, r8, r9)
        L_0x0086:
            com.tencent.pangu.component.appdetail.process.n r0 = r11.f
            r0.c()
            com.tencent.pangu.component.appdetail.process.n r0 = r11.f
            r11.c = r0
            r0 = 1
            if (r10 != r0) goto L_0x0025
            goto L_0x0025
        L_0x0093:
            com.tencent.pangu.component.appdetail.process.i r0 = r11.e
            if (r0 != 0) goto L_0x00c0
            com.tencent.pangu.component.appdetail.process.i r0 = new com.tencent.pangu.component.appdetail.process.i
            r0.<init>(r11)
            r11.e = r0
            com.tencent.pangu.component.appdetail.process.i r0 = r11.e
            com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener r1 = r11.d
            r0.a(r1)
            com.tencent.pangu.component.appdetail.process.i r0 = r11.e
            com.tencent.pangu.component.appdetail.process.m r1 = r11.g
            r0.a(r1)
            com.tencent.pangu.component.appdetail.process.i r0 = r11.e
            com.tencent.assistant.model.SimpleAppModel r1 = r11.h
            long r2 = r11.j
            boolean r4 = r11.k
            com.tencent.nucleus.socialcontact.comment.PopViewDialog r5 = r11.l
            r6 = 0
            android.os.Bundle r7 = r11.n
            boolean r8 = r11.o
            android.content.Context r9 = r11.f3605a
            r0.a(r1, r2, r4, r5, r6, r7, r8, r9)
        L_0x00c0:
            com.tencent.pangu.component.appdetail.process.i r0 = r11.e
            r0.c()
            com.tencent.pangu.component.appdetail.process.i r0 = r11.e
            r11.c = r0
            r0 = 1
            if (r10 != r0) goto L_0x0025
            goto L_0x0025
        L_0x00ce:
            com.tencent.pangu.component.appdetail.process.e r0 = r11.c
            if (r0 == 0) goto L_0x00db
            int r0 = r12.getId()
            r1 = 2131165664(0x7f0701e0, float:1.7945551E38)
            if (r0 == r1) goto L_0x00f6
        L_0x00db:
            int r0 = r12.getId()
            r1 = 2131165667(0x7f0701e3, float:1.7945558E38)
            if (r0 == r1) goto L_0x00f6
            int r0 = r12.getId()
            r1 = 2131165684(0x7f0701f4, float:1.7945592E38)
            if (r0 == r1) goto L_0x00f6
            int r0 = r12.getId()
            r1 = 2131165686(0x7f0701f6, float:1.7945596E38)
            if (r0 != r1) goto L_0x0052
        L_0x00f6:
            com.tencent.pangu.component.appdetail.process.e r0 = r11.c
            r0.a(r12)
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.pangu.component.appdetail.process.a.a(android.view.View):void");
    }

    public void c() {
        b(900);
    }

    public STInfoV2 d() {
        return this.m;
    }

    private void b(int i2) {
        if (this.m == null) {
            this.m = ((AppDetailActivityV5) this.f3605a).t();
        }
        if (this.m != null) {
            this.m.actionId = i2;
            this.m.status = STConst.ST_STATUS_DEFAULT;
            this.m.actionFlag = e();
            if (i2 == 900 || i2 == 905) {
                this.m.isImmediately = true;
            }
            if (this.h != null && h.a(this.h.au)) {
                this.m.isImmediately = true;
            }
            l.a(this.m);
        }
    }

    private int j() {
        String string = this.n.getString(com.tencent.assistant.a.a.s);
        if (TextUtils.isEmpty(string)) {
            return 0;
        }
        try {
            return Integer.valueOf(string).intValue();
        } catch (NumberFormatException e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    private int k() {
        if (this.h != null) {
            return com.tencent.assistantv2.st.page.a.a(k.d(this.h));
        }
        return -1;
    }

    public void a(boolean z, AppConst.AppState appState, long j2, String str, int i2, int i3, boolean z2) {
        if (this.c != null) {
            this.c.a(z, appState, j2, str, i2, i3, z2);
        }
    }

    public void a(int i2) {
        if (this.c != null) {
            this.c.a(i2);
        }
    }

    public int e() {
        if (this.c != null) {
            return this.c.g();
        }
        return 0;
    }

    public void f() {
        if (this.c != null) {
            this.c.b();
        }
    }

    public void g() {
        if (this.c != null) {
            this.c.c();
        }
    }

    public void h() {
        if (this.c != null) {
            this.c.d();
        }
    }

    public void i() {
        if (this.c != null) {
            this.c.e();
        }
    }
}
