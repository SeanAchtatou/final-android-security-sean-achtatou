package com.tencent.pangu.component.homeEntry;

import android.graphics.Bitmap;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Bitmap f3675a;
    final /* synthetic */ HomeEntryCellBase b;

    c(HomeEntryCellBase homeEntryCellBase, Bitmap bitmap) {
        this.b = homeEntryCellBase;
        this.f3675a = bitmap;
    }

    public void run() {
        XLog.v("home_entry", "cell---requestImg--cell name:" + getClass().getSimpleName() + "--bitmap refresh2222222---------------local");
        this.b.a(this.f3675a);
    }
}
