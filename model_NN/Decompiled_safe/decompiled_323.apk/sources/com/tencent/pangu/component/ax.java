package com.tencent.pangu.component;

import android.app.Dialog;
import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.e;
import com.tencent.pangu.download.a;

/* compiled from: ProGuard */
class ax implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Dialog f3632a;
    final /* synthetic */ RecommandApplinkDialog b;

    ax(RecommandApplinkDialog recommandApplinkDialog, Dialog dialog) {
        this.b = recommandApplinkDialog;
        this.f3632a = dialog;
    }

    public void onClick(View view) {
        try {
            boolean a2 = e.a(this.b.b.f1132a);
            XLog.d("RecommendDownloadManager", "<install> <GuideOpen> 浮层打开应用 = " + this.b.b.f1132a + ", action = " + this.b.b.c + ", isInstalled = " + a2);
            if (!a2) {
                ah.a().post(new ay(this));
            } else {
                a.a().a(this.b.b.f1132a, this.b.b.c);
            }
            this.b.a("09_001" + this.b.o, 200, this.b.b.c);
            this.f3632a.dismiss();
        } catch (Throwable th) {
            XLog.d("RecommendDownloadManager", "<install> <GuideOpen> 浮层打开应用异常 = " + th.toString());
            az azVar = new az(this);
            azVar.titleRes = AstApp.i().getResources().getString(R.string.down_uninstall_title);
            azVar.contentRes = AstApp.i().getResources().getString(R.string.down_cannot_open_tips);
            azVar.btnTxtRes = AstApp.i().getResources().getString(R.string.down_uninstall_tips_close);
            DialogUtils.show1BtnDialog(azVar);
        }
    }
}
