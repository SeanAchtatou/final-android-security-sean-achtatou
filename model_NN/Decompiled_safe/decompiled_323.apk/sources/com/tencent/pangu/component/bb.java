package com.tencent.pangu.component;

import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.SelfUpdateActivity;

/* compiled from: ProGuard */
class bb extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SelfForceUpdateView f3647a;

    bb(SelfForceUpdateView selfForceUpdateView) {
        this.f3647a = selfForceUpdateView;
    }

    public void onTMAClick(View view) {
        AstApp.i().f();
    }

    public STInfoV2 getStInfo() {
        if (!(this.f3647a.getContext() instanceof SelfUpdateActivity)) {
            return null;
        }
        STInfoV2 t = ((SelfUpdateActivity) this.f3647a.getContext()).t();
        t.slotId = "01_002";
        t.actionId = 200;
        t.extraData = "02";
        return t;
    }
}
