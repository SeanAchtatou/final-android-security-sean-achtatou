package com.tencent.pangu.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.protocol.jce.HotWordItem;
import com.tencent.pangu.component.appdetail.RecommendAppViewV5;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class DownloadListFooterView extends TXLoadingLayoutBase {
    private Context c;
    private LinearLayout d;
    private TextView e;
    private RecommendAppViewV5 f;
    private HotwordsCardView g;
    private RelativeLayout h;

    public DownloadListFooterView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public DownloadListFooterView(Context context, TXScrollViewBase.ScrollDirection scrollDirection, TXScrollViewBase.ScrollMode scrollMode) {
        super(context, scrollDirection, scrollMode);
        this.c = context;
        a(context);
    }

    private void a(Context context) {
        View inflate;
        LayoutInflater from = LayoutInflater.from(context);
        try {
            inflate = from.inflate((int) R.layout.downloadlist_footer_layout, this);
        } catch (Throwable th) {
            t.a().b();
            inflate = from.inflate((int) R.layout.downloadlist_footer_layout, this);
        }
        this.d = (LinearLayout) inflate.findViewById(R.id.layout_id);
        this.f = (RecommendAppViewV5) inflate.findViewById(R.id.recommendAppView);
        this.f.a("03");
        this.f.b(2);
        this.e = (TextView) findViewById(R.id.hot_title);
        this.g = (HotwordsCardView) inflate.findViewById(R.id.recommendHotwordsView);
        this.h = (RelativeLayout) inflate.findViewById(R.id.recommendHotWordsLayout);
        reset();
    }

    public RecommendAppViewV5 a() {
        return this.f;
    }

    public void reset() {
    }

    public void pullToRefresh() {
    }

    public void releaseToRefresh() {
    }

    public void refreshing() {
    }

    public void loadFinish(String str) {
    }

    public void onPull(int i) {
    }

    public void hideAllSubViews() {
    }

    public void showAllSubViews() {
    }

    public void setWidth(int i) {
        getLayoutParams().width = i;
        requestLayout();
    }

    public void setHeight(int i) {
        getLayoutParams().height = i;
        requestLayout();
    }

    public int getContentSize() {
        return this.d.getHeight();
    }

    public int getTriggerSize() {
        return getResources().getDimensionPixelSize(R.dimen.tx_pull_to_refresh_trigger_size);
    }

    public void refreshSuc() {
    }

    public void refreshFail(String str) {
    }

    public void loadSuc() {
    }

    public void loadFail() {
    }

    public void a(String str, String str2, String str3, ArrayList<HotWordItem> arrayList) {
        if (this.g != null) {
            this.e.setText(str);
            this.g.a(1, str2, str3, arrayList);
        }
    }

    public void a(int i) {
        if (this.h != null) {
            this.h.setVisibility(i);
        }
    }
}
