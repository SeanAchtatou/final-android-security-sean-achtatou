package com.tencent.pangu.component.search;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;

/* compiled from: ProGuard */
class p extends ViewPageScrollListener {

    /* renamed from: a  reason: collision with root package name */
    boolean f3711a = false;
    final /* synthetic */ SearchResultTabPagesBase b;

    p(SearchResultTabPagesBase searchResultTabPagesBase) {
        this.b = searchResultTabPagesBase;
    }

    public void onPageScrollStateChanged(int i) {
        super.onPageScrollStateChanged(i);
        if (i == 1) {
            this.f3711a = true;
        }
        if (i == 0) {
        }
    }

    public void onPageSelected(int i) {
        super.onPageSelected(i);
        if (this.f3711a) {
            this.b.a("04", "005", 200);
        }
        this.b.d = i;
        this.f3711a = false;
        this.b.a(i);
    }

    public void onPageScrolled(int i, float f, int i2) {
        super.onPageScrolled(i, f, i2);
        if (this.b.g != null) {
            this.b.g.a(i, f);
        }
    }

    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        super.handleMessage(viewInvalidateMessage);
        int i = viewInvalidateMessage.what;
    }
}
