package com.tencent.pangu.component.topbanner;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class g extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TopBannerView f3723a;

    g(TopBannerView topBannerView) {
        this.f3723a = topBannerView;
    }

    public void onTMAClick(View view) {
        if (this.f3723a.e != null && this.f3723a.e.i != null) {
            b.a(this.f3723a.getContext(), this.f3723a.e.i);
            b.a().f(this.f3723a.e);
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3723a.getContext(), 200);
        buildSTInfo.slotId = this.f3723a.c();
        if (this.f3723a.e != null) {
            switch (this.f3723a.e.f) {
                case 1:
                    buildSTInfo.status = "01";
                    break;
                case 2:
                    buildSTInfo.status = "02";
                    break;
                case 3:
                    buildSTInfo.status = "03";
                    break;
                case 4:
                    buildSTInfo.status = "04";
                    break;
                case 5:
                    buildSTInfo.status = "05";
                    break;
            }
        }
        return buildSTInfo;
    }
}
