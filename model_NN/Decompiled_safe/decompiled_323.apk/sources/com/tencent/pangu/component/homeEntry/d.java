package com.tencent.pangu.component.homeEntry;

import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.aj;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class d extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HomeEntryTemplateView f3676a;

    d(HomeEntryTemplateView homeEntryTemplateView) {
        this.f3676a = homeEntryTemplateView;
    }

    public void onTMAClick(View view) {
        XLog.v("home_entry", "HomeEntryTemplateView-----OnClickListener---onTMAClick--");
        if (this.f3676a.f3672a != null && this.f3676a.f3672a.d != null && !TextUtils.isEmpty(this.f3676a.f3672a.d.f1125a)) {
            XLog.v("home_entry", "HomeEntryTemplateView------OnClickListener---onClick actionUrl is ok");
            b.a(this.f3676a.d, this.f3676a.f3672a.d);
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3676a.d, 200);
        buildSTInfo.scene = STConst.ST_PAGE_COMPETITIVE;
        if (this.f3676a.f3672a != null) {
            aj.a(this.f3676a.f3672a.e, buildSTInfo);
        }
        XLog.v("home_entry", "HomeEntryTemplateView----OnClickListener---getStInfo--stInfo.slotId= " + buildSTInfo.slotId + "--stInfo.pushInfo= " + buildSTInfo.pushInfo);
        return buildSTInfo;
    }
}
