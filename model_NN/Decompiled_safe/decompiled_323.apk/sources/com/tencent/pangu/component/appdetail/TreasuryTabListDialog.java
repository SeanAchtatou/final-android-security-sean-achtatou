package com.tencent.pangu.component.appdetail;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
public class TreasuryTabListDialog extends Dialog implements View.OnFocusChangeListener {

    /* renamed from: a  reason: collision with root package name */
    LinearLayout f3559a;
    private bo b;
    private int c = 0;
    private ImageView d;
    private Context e;

    public TreasuryTabListDialog(Context context, int i) {
        super(context, i);
        this.e = context;
        a();
    }

    public TreasuryTabListDialog(Context context) {
        super(context, R.style.notDimdialog);
        this.e = context;
        a();
    }

    private void a() {
        Window window = getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.gravity = 53;
        window.setWindowAnimations(R.style.dialogWindowAnim);
        Display defaultDisplay = getWindow().getWindowManager().getDefaultDisplay();
        attributes.height = defaultDisplay.getHeight();
        attributes.width = defaultDisplay.getWidth();
        attributes.y = this.e.getResources().getDimensionPixelSize(R.dimen.navigation_extra_list_y);
        window.setAttributes(attributes);
        setCanceledOnTouchOutside(true);
        this.f3559a = new LinearLayout(this.e);
        this.f3559a.setOrientation(1);
        this.f3559a.setBackgroundResource(R.drawable.bg_card_n);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(this.f3559a);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.e.getResources().getDimensionPixelSize(R.dimen.navigation_extra_list_lenth), -2);
        layoutParams.leftMargin = (getWindow().getWindowManager().getDefaultDisplay().getWidth() - layoutParams.width) - this.e.getResources().getDimensionPixelSize(R.dimen.normal_list_margin_lfet);
        layoutParams.rightMargin = this.e.getResources().getDimensionPixelSize(R.dimen.normal_list_margin_lfet) >> 1;
        this.f3559a.setLayoutParams(layoutParams);
    }

    public void a(int i) {
        if (i >= 0 && i < this.f3559a.getChildCount()) {
            this.f3559a.getChildAt(this.c).setSelected(false);
            this.c = i;
            this.f3559a.getChildAt(this.c).setSelected(true);
        }
    }

    public void onFocusChange(View view, boolean z) {
        if (this.f3559a.getChildAt(this.c) != null) {
            if (view == this.f3559a && z) {
                this.f3559a.getChildAt(this.c).requestFocus();
            } else if (z) {
                for (int i = 0; i < this.f3559a.getChildCount(); i++) {
                    if (this.f3559a.getChildAt(i) == view) {
                        a(i);
                        this.b.a(i, false);
                        return;
                    }
                }
            }
        }
    }

    private void a(boolean z) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.e, 200);
        buildSTInfo.slotId = a.a("04", "006");
        buildSTInfo.status = z ? "01" : "02";
        l.a(buildSTInfo);
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.d != null) {
            this.d.setImageResource(R.drawable.common_navigation_down);
        }
        a(false);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.d != null) {
            this.d.setImageResource(R.drawable.common_navigation_up);
        }
    }
}
