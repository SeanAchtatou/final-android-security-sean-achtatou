package com.tencent.pangu.component.banner;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.notification.a.a.c;
import com.tencent.assistant.protocol.jce.Banner;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.b.d;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.ba;

/* compiled from: ProGuard */
public abstract class f {

    /* renamed from: a  reason: collision with root package name */
    public Banner f3641a;
    /* access modifiers changed from: protected */
    public String b = Constants.STR_EMPTY;
    public Bitmap c = null;
    View d;
    private int e = -1;
    private d f = new d();

    public abstract int a();

    /* access modifiers changed from: protected */
    public abstract View a(Context context, ViewGroup viewGroup, int i, long j, int i2);

    public f(Banner banner) {
        this.f3641a = banner;
    }

    public void a(String str) {
        this.b = str;
    }

    public View b(Context context, ViewGroup viewGroup, int i, long j, int i2) {
        this.f.a(context, this.d, a.a(this.b, i2));
        if (this.d != null) {
            if (this.d.getParent() != null && (this.d.getParent() instanceof ViewGroup)) {
                ((ViewGroup) this.d.getParent()).removeView(this.d);
            }
            return this.d;
        }
        this.d = a(context, viewGroup, i, j, i2);
        return this.d;
    }

    public void a(Context context, View view) {
        ba baVar = new ba(context);
        baVar.a(by.a(0.5f));
        baVar.b(c());
        view.setBackgroundDrawable(baVar);
    }

    public c b() {
        this.c = null;
        XLog.d("banner", "**** loadBackgroundImage bgImgUrl=" + this.f3641a.g);
        if (this.f3641a == null || TextUtils.isEmpty(this.f3641a.g)) {
            return null;
        }
        c cVar = new c(this.f3641a.g, 1);
        cVar.a(new g(this));
        return cVar;
    }

    public void a(Context context) {
        if (context != null && this.c != null && !this.c.isRecycled()) {
            this.d.setBackgroundDrawable(new BitmapDrawable(context.getResources(), this.c));
        }
    }

    public int c() {
        if (this.f3641a == null || TextUtils.isEmpty(this.f3641a.d)) {
            return -1;
        }
        return b(this.f3641a.d);
    }

    /* access modifiers changed from: protected */
    public int b(String str) {
        try {
            return Color.parseColor(str);
        } catch (Exception e2) {
            e2.printStackTrace();
            return -1;
        }
    }

    public int d() {
        return this.e;
    }

    public void a(int i) {
        this.e = i;
    }

    /* access modifiers changed from: protected */
    public int a(int i, long j, int i2) {
        String[] b2;
        String a2 = m.a().a("key_b_r_i_" + i + "_" + i2, (String) null);
        if (!TextUtils.isEmpty(a2) && (b2 = bm.b(a2, "|")) != null && b2.length == 2 && ((long) bm.a(b2[0], 0)) == j) {
            return bm.a(b2[1], 0);
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public void a(int i, long j, int i2, int i3) {
        m.a().b("key_b_r_i_" + i + "_" + i2, j + "|" + i3);
    }

    public Banner e() {
        return this.f3641a;
    }
}
