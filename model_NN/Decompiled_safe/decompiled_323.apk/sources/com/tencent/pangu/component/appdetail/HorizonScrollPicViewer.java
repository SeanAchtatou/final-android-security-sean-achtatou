package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.ApkInfo;
import com.tencent.assistant.protocol.jce.SnapshotsPic;
import com.tencent.assistant.protocol.jce.Video;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.activity.AppDetailActivityV5;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class HorizonScrollPicViewer extends LinearLayout {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f3554a;
    private LayoutInflater b;
    /* access modifiers changed from: private */
    public boolean c;
    private int d;
    /* access modifiers changed from: private */
    public HorizonImageListView e;
    /* access modifiers changed from: private */
    public List<SnapshotsPic> f = new ArrayList();
    /* access modifiers changed from: private */
    public List<Video> g = new ArrayList();
    private ba h;
    /* access modifiers changed from: private */
    public bb i;
    private aw j = new ax(this);
    private float k;
    private ac l = new az(this);

    public HorizonScrollPicViewer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3554a = context;
        this.b = LayoutInflater.from(context);
        e();
    }

    public HorizonScrollPicViewer(Context context) {
        super(context);
        this.f3554a = context;
        this.b = LayoutInflater.from(context);
        e();
    }

    public void a(ApkInfo apkInfo) {
        if (apkInfo != null && this.f.size() <= 0) {
            ArrayList<SnapshotsPic> arrayList = apkInfo.g;
            ArrayList<Video> arrayList2 = apkInfo.t;
            if (arrayList != null || arrayList2 != null) {
                if (arrayList2 != null) {
                    for (Video next : arrayList2) {
                        next.b += "&url=" + next.b;
                    }
                    this.g.clear();
                    this.g.addAll(arrayList2);
                }
                if (arrayList != null) {
                    this.f.clear();
                    this.f.addAll(arrayList);
                }
                d();
                requestLayout();
            }
        }
    }

    private void d() {
        ArrayList arrayList = new ArrayList();
        for (Video next : this.g) {
            if (!TextUtils.isEmpty(next.f1608a)) {
                arrayList.add(next.f1608a);
            }
        }
        for (SnapshotsPic next2 : this.f) {
            if (this.c) {
                arrayList.add(next2.b);
            } else {
                arrayList.add(next2.c);
            }
        }
        if (arrayList == null || arrayList.size() == 0) {
            this.e.setVisibility(8);
            return;
        }
        try {
            this.e.a(this.g.size(), arrayList, (int) getResources().getDimension(R.dimen.app_detail_pic_gap), (int) R.drawable.pic_default_big);
        } catch (Throwable th) {
            t.a().b();
        }
        if (this.g.size() > 0 && (this.f3554a instanceof AppDetailActivityV5)) {
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3554a, 100);
            buildSTInfo.slotId = a.a(Constants.VIA_REPORT_TYPE_START_WAP, 0);
            l.a(buildSTInfo);
        }
    }

    private void e() {
        if (c.e() || c.g()) {
            this.c = true;
            this.d = (int) this.f3554a.getResources().getDimension(R.dimen.app_detail_pic_height_wifi3g);
        } else {
            this.c = false;
            this.d = (int) this.f3554a.getResources().getDimension(R.dimen.app_detail_pic_height_wifi3g);
        }
        this.e = (HorizonImageListView) this.b.inflate((int) R.layout.horizon_image_scroll_listview, this).findViewById(R.id.imageListView);
        ((RelativeLayout.LayoutParams) this.e.getLayoutParams()).height = this.d;
        this.e.a(this.d);
        this.e.a(this.j);
    }

    public void a() {
        if (this.e != null) {
            this.e.c();
        }
    }

    public void b() {
        if (this.e != null) {
            this.e.d();
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                if (this.h != null && this.e.b()) {
                    this.h.a();
                }
                this.k = motionEvent.getX();
                break;
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return super.onTouchEvent(motionEvent);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return super.dispatchTouchEvent(motionEvent);
    }

    public void a(ba baVar) {
        this.h = baVar;
        this.e.a(baVar);
    }

    public void a(bb bbVar) {
        this.i = bbVar;
    }

    public ac c() {
        return this.l;
    }
}
