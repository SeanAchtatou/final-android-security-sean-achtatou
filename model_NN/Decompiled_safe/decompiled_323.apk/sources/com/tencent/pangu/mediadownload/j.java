package com.tencent.pangu.mediadownload;

import android.text.TextUtils;
import com.tencent.assistant.Global;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.bm;
import com.tencent.downloadsdk.ae;
import com.tencent.pangu.model.AbstractDownloadInfo;
import com.tencent.pangu.model.a;
import com.tencent.pangu.model.d;
import java.io.File;

/* compiled from: ProGuard */
class j implements ae {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f3861a;

    j(e eVar) {
        this.f3861a = eVar;
    }

    public void a(int i, String str, String str2) {
        d dVar = (d) this.f3861a.f3856a.get(str);
        if (dVar != null) {
            dVar.q = str2;
            dVar.r = str2;
            dVar.s = AbstractDownloadInfo.DownState.SUCC;
            dVar.t = 0;
            dVar.p = System.currentTimeMillis();
            this.f3861a.c.sendMessage(this.f3861a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_SUCC, dVar));
            this.f3861a.b.a(dVar);
        }
    }

    public void a(int i, String str) {
        d dVar = (d) this.f3861a.f3856a.get(str);
        if (dVar != null) {
            dVar.s = AbstractDownloadInfo.DownState.DOWNLOADING;
            dVar.t = 0;
            this.f3861a.c.sendMessage(this.f3861a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_START, dVar));
            this.f3861a.b.a(dVar);
        }
    }

    public void a(int i, String str, long j, String str2, String str3) {
        d dVar = (d) this.f3861a.f3856a.get(str);
        if (dVar != null) {
            if (dVar.u == null) {
                dVar.u = new a();
            }
            dVar.u.f3881a = j;
            dVar.n = j;
            if (!TextUtils.isEmpty(str2)) {
                dVar.r = str2;
                dVar.k = new File(str2).getName();
            }
            dVar.w = str3;
            this.f3861a.c.sendMessage(this.f3861a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FILENAME, dVar));
            this.f3861a.b.a(dVar);
        }
    }

    public void a(int i, String str, long j, long j2, double d) {
        d dVar = (d) this.f3861a.f3856a.get(str);
        if (dVar != null) {
            if (dVar.s != AbstractDownloadInfo.DownState.DOWNLOADING) {
                this.f3861a.c.sendMessage(this.f3861a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_START, dVar));
            }
            dVar.s = AbstractDownloadInfo.DownState.DOWNLOADING;
            dVar.t = 0;
            dVar.u.b = j2;
            dVar.u.f3881a = j;
            dVar.u.c = bm.a(d);
            this.f3861a.c.sendMessage(this.f3861a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING, dVar));
        }
    }

    public void a(int i, String str, String str2, String str3) {
        d dVar = (d) this.f3861a.f3856a.get(str);
        if (dVar != null) {
            dVar.r = str2;
            if (!TextUtils.isEmpty(str2)) {
                dVar.k = new File(str2).getName();
            }
            dVar.w = str3;
            dVar.s = AbstractDownloadInfo.DownState.SUCC;
            dVar.t = 0;
            dVar.p = System.currentTimeMillis();
            dVar.u.b = dVar.u.f3881a;
            this.f3861a.c.sendMessage(this.f3861a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_SUCC, dVar));
            this.f3861a.b.a(dVar);
        }
    }

    public void a(int i, String str, int i2, byte[] bArr, String str2) {
        d dVar = (d) this.f3861a.f3856a.get(str);
        if (dVar != null) {
            dVar.s = AbstractDownloadInfo.DownState.FAIL;
            dVar.t = i2;
            this.f3861a.c.sendMessage(this.f3861a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FAIL, dVar));
            this.f3861a.b.a(dVar);
        }
        if (i2 == -12) {
            TemporaryThreadManager.get().start(new k(this));
        } else if (i2 == -11) {
            ah.a().post(new l(this));
        }
        FileUtil.tryRefreshPath(i2);
    }

    public void b(int i, String str) {
        if (Global.isDev()) {
            XLog.d("FileDownTag", "onTaskPaused.type:" + i + ",ticketid:" + str);
        }
        d dVar = (d) this.f3861a.f3856a.get(str);
        if (dVar == null) {
            return;
        }
        if (dVar.s == AbstractDownloadInfo.DownState.QUEUING || dVar.s == AbstractDownloadInfo.DownState.DOWNLOADING) {
            dVar.s = AbstractDownloadInfo.DownState.PAUSED;
            dVar.t = 0;
            this.f3861a.c.sendMessage(this.f3861a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_PAUSE, dVar));
            this.f3861a.b.a(dVar);
        }
    }

    public void b(int i, String str, String str2) {
    }
}
