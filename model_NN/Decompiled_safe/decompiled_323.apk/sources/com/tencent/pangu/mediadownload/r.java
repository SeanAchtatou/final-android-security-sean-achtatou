package com.tencent.pangu.mediadownload;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.table.ai;
import com.tencent.assistant.event.EventDispatcher;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.st.l;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.downloadsdk.DownloadTask;
import com.tencent.downloadsdk.ac;
import com.tencent.downloadsdk.ae;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.download.k;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.model.AbstractDownloadInfo;
import com.tencent.pangu.model.a;
import com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class r {
    private static final Object e = new Object();
    private static r f;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Map<String, q> f3869a = new ConcurrentHashMap(5);
    /* access modifiers changed from: private */
    public ai b = new ai();
    /* access modifiers changed from: private */
    public EventDispatcher c = AstApp.i().j();
    /* access modifiers changed from: private */
    public InstallUninstallDialogManager d = new InstallUninstallDialogManager();
    private Handler g = k.a();
    private ae h = new w(this);

    private r() {
        this.d.a(true);
        try {
            List<q> a2 = this.b.a();
            ArrayList<ac> a3 = DownloadManager.a().a(8);
            for (q next : a2) {
                next.e();
                this.f3869a.put(next.m, next);
            }
            if (a3 != null) {
                Iterator<ac> it = a3.iterator();
                while (it.hasNext()) {
                    ac next2 = it.next();
                    q qVar = this.f3869a.get(next2.b);
                    if (qVar != null) {
                        if (qVar.u == null) {
                            qVar.u = new a();
                        }
                        qVar.u.b = next2.c;
                        qVar.u.f3881a = next2.d;
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean a(q qVar) {
        this.g.post(new s(this, qVar));
        return true;
    }

    public boolean b(q qVar) {
        if (qVar == null || TextUtils.isEmpty(qVar.m)) {
            return false;
        }
        if (qVar.f() || qVar.d()) {
            this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC, qVar));
            e(qVar);
            return true;
        } else if (!this.f3869a.containsKey(qVar.m) && TextUtils.isEmpty(qVar.k)) {
            return false;
        } else {
            String b2 = qVar.b();
            if (TextUtils.isEmpty(b2) || !b2.startsWith("/data/data/")) {
                ArrayList arrayList = new ArrayList(1);
                arrayList.add(qVar.l);
                DownloadTask downloadTask = new DownloadTask(qVar.a(), qVar.m, 0, 0, qVar.b(), qVar.k, arrayList, null);
                ae a2 = l.a(qVar.m, 0, 0, (byte) qVar.a(), qVar.v, SimpleDownloadInfo.UIType.NORMAL, SimpleDownloadInfo.DownloadType.VIDEO);
                downloadTask.b = DownloadInfo.getPriority(SimpleDownloadInfo.DownloadType.VIDEO, SimpleDownloadInfo.UIType.NORMAL);
                qVar.q = downloadTask.d();
                downloadTask.a(this.h);
                downloadTask.a(a2);
                DownloadManager.a().a(downloadTask);
                boolean d2 = d(qVar);
                if (DownloadManager.a().c(8, qVar.m) || DownloadProxy.a().h() < 2) {
                    if (qVar.s != AbstractDownloadInfo.DownState.DOWNLOADING) {
                        qVar.s = AbstractDownloadInfo.DownState.DOWNLOADING;
                        this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START, qVar));
                    }
                    this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING, qVar));
                } else {
                    qVar.s = AbstractDownloadInfo.DownState.QUEUING;
                    this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_QUEUING, qVar));
                }
                if (d2) {
                    this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_ADD, qVar));
                }
                this.b.a(qVar);
                return true;
            }
            TemporaryThreadManager.get().start(new t(this));
            return false;
        }
    }

    private boolean d(q qVar) {
        try {
            if (!this.f3869a.containsKey(qVar.m)) {
                this.f3869a.put(qVar.m, qVar);
                qVar.o = System.currentTimeMillis();
                return true;
            }
            if (this.f3869a.get(qVar.m) != qVar) {
                this.f3869a.put(qVar.m, qVar);
            }
            return false;
        } catch (NullPointerException e2) {
            return false;
        }
    }

    private void e(q qVar) {
        if (qVar != null && !TextUtils.isEmpty(qVar.m)) {
            this.f3869a.put(qVar.m, qVar);
            this.b.a(qVar);
        }
    }

    public void a(String str) {
        TemporaryThreadManager.get().start(new u(this, str));
    }

    /* access modifiers changed from: private */
    public void d(String str) {
        q qVar;
        if (!TextUtils.isEmpty(str) && (qVar = this.f3869a.get(str)) != null) {
            DownloadManager.a().a(8, str);
            if (qVar.s == AbstractDownloadInfo.DownState.DOWNLOADING || qVar.s == AbstractDownloadInfo.DownState.QUEUING) {
                qVar.s = AbstractDownloadInfo.DownState.PAUSED;
                this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_PAUSE, qVar));
                this.b.a(qVar);
            }
        }
    }

    public boolean b(String str) {
        this.g.post(new v(this, str));
        return true;
    }

    public boolean a(String str, boolean z) {
        q remove;
        if (TextUtils.isEmpty(str) || (remove = this.f3869a.remove(str)) == null) {
            return false;
        }
        DownloadManager.a().b(8, str);
        this.b.a(str);
        if (z) {
            FileUtil.deleteFile(remove.r);
        }
        remove.s = AbstractDownloadInfo.DownState.DELETE;
        this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE, remove));
        return true;
    }

    public List<q> a() {
        return a(0);
    }

    private List<q> a(int i) {
        ArrayList arrayList = new ArrayList(this.f3869a.size());
        for (Map.Entry next : this.f3869a.entrySet()) {
            if (i == 0 || (i == 1 && (((q) next.getValue()).s == AbstractDownloadInfo.DownState.DOWNLOADING || ((q) next.getValue()).s == AbstractDownloadInfo.DownState.QUEUING))) {
                arrayList.add(next.getValue());
            }
        }
        return arrayList;
    }

    public List<q> b() {
        return a(1);
    }

    public q c(String str) {
        return this.f3869a.get(str);
    }

    public static synchronized r c() {
        r rVar;
        synchronized (r.class) {
            if (f == null) {
                f = new r();
            }
            rVar = f;
        }
        return rVar;
    }

    public void d() {
        TemporaryThreadManager.get().start(new z(this));
    }

    public void e() {
        TemporaryThreadManager.get().start(new aa(this));
    }

    public int a(Context context, q qVar) {
        String[] b2;
        if (qVar == null) {
            return 0;
        }
        if (!qVar.f()) {
            return -4;
        }
        LocalApkInfo installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(qVar.e);
        if (installedApkInfo == null) {
            return -2;
        }
        if (installedApkInfo.mVersionCode < qVar.h) {
            return -3;
        }
        if (TextUtils.isEmpty(qVar.g)) {
            return -1;
        }
        try {
            Uri parse = Uri.parse(a(qVar, qVar.g));
            if (parse.getScheme().equals("intent")) {
                String host = parse.getHost();
                String query = parse.getQuery();
                Intent intent = new Intent(host);
                if (!TextUtils.isEmpty(query) && (b2 = bm.b(query, "&")) != null && b2.length > 0) {
                    for (String str : b2) {
                        String[] b3 = bm.b(str, "=");
                        if (str != null && str.length() > 1 && b3 != null && b3.length > 0) {
                            intent.putExtra(b3[0], b3[1]);
                        }
                    }
                }
                intent.setFlags(268435456);
                context.startActivity(intent);
                return 0;
            }
            Intent intent2 = new Intent("android.intent.action.VIEW", parse);
            intent2.setFlags(268435456);
            context.startActivity(intent2);
            return 0;
        } catch (Exception e2) {
            e2.printStackTrace();
            return -2;
        }
    }

    public int c(q qVar) {
        LocalApkInfo installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(qVar.e);
        if (installedApkInfo == null) {
            return -2;
        }
        if (installedApkInfo.mVersionCode < qVar.h) {
            return -3;
        }
        return 0;
    }

    private String a(q qVar, String str) {
        if (TextUtils.isEmpty(str) || qVar == null || TextUtils.isEmpty(qVar.r)) {
            return str;
        }
        return str.replace("${video_path}", qVar.r);
    }

    public void f() {
        if (this.f3869a != null && this.f3869a.size() > 0) {
            for (Map.Entry<String, q> key : this.f3869a.entrySet()) {
                d((String) key.getKey());
            }
        }
    }

    public int g() {
        int i = 0;
        if (this.f3869a == null || this.f3869a.size() <= 0) {
            return 0;
        }
        Iterator<Map.Entry<String, q>> it = this.f3869a.entrySet().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            Map.Entry next = it.next();
            if (next.getValue() != null && (((q) next.getValue()).s == AbstractDownloadInfo.DownState.DOWNLOADING || ((q) next.getValue()).s == AbstractDownloadInfo.DownState.QUEUING)) {
                i2++;
            }
            i = i2;
        }
    }

    public int h() {
        int i = 0;
        if (this.f3869a == null || this.f3869a.size() <= 0) {
            return 0;
        }
        Iterator<Map.Entry<String, q>> it = this.f3869a.entrySet().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            Map.Entry next = it.next();
            if (next.getValue() != null && ((q) next.getValue()).s == AbstractDownloadInfo.DownState.FAIL) {
                i2++;
            }
            i = i2;
        }
    }
}
