package com.tencent.pangu.module.wisedownload.condition;

import com.qq.ndk.NativeFileObject;
import com.tencent.assistant.protocol.jce.AutoDownloadCfg;
import com.tencent.assistant.utils.bo;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.module.wisedownload.b;
import com.tencent.pangu.module.wisedownload.condition.ThresholdCondition;
import com.tencent.pangu.module.wisedownload.s;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class o extends ThresholdCondition {
    private int c;
    private int d;
    private int e;

    public o(b bVar) {
        a(bVar);
    }

    public void a(b bVar) {
        AutoDownloadCfg j;
        if (bVar != null && (j = bVar.j()) != null) {
            this.c = j.c;
            this.d = j.d;
            this.e = j.e;
        }
    }

    public boolean a() {
        a(ThresholdCondition.CONDITION_RESULT_CODE.OK);
        return m() && n();
    }

    private boolean m() {
        long b = s.b();
        if (b == 0) {
            a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_OTHER_NO_APP);
            return false;
        }
        boolean a2 = a(b + ((long) (this.c * NativeFileObject.S_IFREG)));
        if (a2) {
            return a2;
        }
        a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_OTHER_SPACE);
        return a2;
    }

    private boolean n() {
        List<DownloadInfo> c2 = s.c();
        ArrayList<DownloadInfo> arrayList = new ArrayList<>();
        ArrayList arrayList2 = new ArrayList();
        if (c2 == null || c2.isEmpty()) {
            boolean z = this.d > 0 || this.e > 0;
            if (z) {
                return z;
            }
            a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_OTHER_MAX_COUNT_NOT_SPECIFIED);
            return z;
        }
        for (DownloadInfo next : c2) {
            if (next != null && bo.d(next.downloadEndTime)) {
                arrayList.add(next);
            }
        }
        if (arrayList.size() >= this.e) {
            a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_OTHER_WEEK);
            return false;
        }
        for (DownloadInfo downloadInfo : arrayList) {
            if (downloadInfo != null && bo.b(downloadInfo.downloadEndTime)) {
                arrayList2.add(downloadInfo);
            }
        }
        if (arrayList2.size() < this.d) {
            return true;
        }
        a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_OTHER_DAY);
        return false;
    }

    public boolean b() {
        long b = s.b();
        if (b == 0) {
            return true;
        }
        return a(b + ((long) (this.c * NativeFileObject.S_IFREG)));
    }

    public boolean h() {
        return s.b() != 0;
    }

    public int i() {
        return this.e;
    }

    public int j() {
        List<DownloadInfo> c2 = s.c();
        ArrayList arrayList = new ArrayList();
        if (c2 == null || c2.isEmpty()) {
            return 0;
        }
        for (DownloadInfo next : c2) {
            if (next != null && bo.d(next.downloadEndTime)) {
                arrayList.add(next);
            }
        }
        return arrayList.size();
    }

    public int k() {
        return this.d;
    }

    public int l() {
        List<DownloadInfo> c2 = s.c();
        ArrayList arrayList = new ArrayList();
        if (c2 == null || c2.isEmpty()) {
            return 0;
        }
        for (DownloadInfo next : c2) {
            if (next != null && bo.b(next.downloadEndTime)) {
                arrayList.add(next);
            }
        }
        return arrayList.size();
    }
}
