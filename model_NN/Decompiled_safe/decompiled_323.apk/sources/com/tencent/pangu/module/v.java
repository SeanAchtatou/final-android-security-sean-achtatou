package com.tencent.pangu.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.GetAuthorAppsRequest;
import com.tencent.assistant.protocol.jce.GetAuthorAppsResponse;
import com.tencent.pangu.module.a.c;

/* compiled from: ProGuard */
public class v extends BaseEngine<c> {

    /* renamed from: a  reason: collision with root package name */
    public int f3953a = 4;
    /* access modifiers changed from: private */
    public byte[] b = new byte[0];

    public int a(String str) {
        GetAuthorAppsRequest getAuthorAppsRequest = new GetAuthorAppsRequest();
        getAuthorAppsRequest.b = this.b;
        getAuthorAppsRequest.c = this.f3953a;
        getAuthorAppsRequest.e = str;
        return send(getAuthorAppsRequest);
    }

    public int a(String str, byte[] bArr) {
        GetAuthorAppsRequest getAuthorAppsRequest = new GetAuthorAppsRequest();
        getAuthorAppsRequest.b = bArr;
        getAuthorAppsRequest.c = this.f3953a;
        getAuthorAppsRequest.e = str;
        return send(getAuthorAppsRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z;
        if (jceStruct2 != null) {
            GetAuthorAppsRequest getAuthorAppsRequest = (GetAuthorAppsRequest) jceStruct;
            GetAuthorAppsResponse getAuthorAppsResponse = (GetAuthorAppsResponse) jceStruct2;
            this.b = getAuthorAppsResponse.b();
            boolean z2 = getAuthorAppsRequest.b == null || getAuthorAppsRequest.b.length == 0;
            if (getAuthorAppsResponse.d == 1) {
                z = true;
            } else {
                z = false;
            }
            notifyDataChangedInMainThread(new w(this, i, z2, k.b(getAuthorAppsResponse.a()), z));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        GetAuthorAppsRequest getAuthorAppsRequest = (GetAuthorAppsRequest) jceStruct;
        notifyDataChangedInMainThread(new x(this, i, i2, getAuthorAppsRequest.b == null || getAuthorAppsRequest.b.length == 0));
    }
}
