package com.tencent.pangu.module;

import android.text.TextUtils;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.b;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.SearchRequest;
import com.tencent.assistant.protocol.jce.SearchResponse;
import com.tencent.pangu.module.a.j;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class AppSearchResultEngine extends BaseEngine<j> {
    private static int d = 10;

    /* renamed from: a  reason: collision with root package name */
    public String f3890a;
    private byte[] b;
    private String c;
    private int e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public long g;
    private int h;
    private int i;
    private b j;
    /* access modifiers changed from: private */
    public ArrayList<com.tencent.pangu.model.b> k;

    /* compiled from: ProGuard */
    public enum DataType {
        APP_MODEL,
        HIT_TAG,
        TOPIC,
        VIDEOCARD,
        FENGYUNCARD,
        VIDEO,
        EBOOK,
        VIDEO_RELATE_CARD,
        EBOOK_RELATE_CARD,
        YUYI_TAG_CARD
    }

    public AppSearchResultEngine() {
        this.b = null;
        this.c = null;
        this.f3890a = null;
        this.e = 0;
        this.f = false;
        this.g = 0;
        this.h = 0;
        this.i = 0;
        this.j = new b();
        this.k = new ArrayList<>();
        this.b = new byte[0];
    }

    public void a(int i2) {
        this.i = i2;
    }

    public int a(String str, int i2) {
        if (str == null || str.length() <= 0 || str.equals(this.c)) {
            return -1;
        }
        this.c = str;
        this.h = i2;
        if (this.e > 0) {
            cancel(this.e);
        }
        this.b = new byte[0];
        this.e = d();
        return this.e;
    }

    public int b(String str, int i2) {
        this.c = str;
        this.h = i2;
        if (this.e > 0) {
            cancel(this.e);
        }
        this.b = new byte[0];
        this.e = d();
        return this.e;
    }

    public int a() {
        if (this.c == null || this.c.length() <= 0) {
            return -1;
        }
        if (this.e > 0) {
            cancel(this.e);
        }
        this.e = d();
        return this.e;
    }

    public void a(String str) {
        if (this.e <= 0) {
            return;
        }
        if (TextUtils.isEmpty(str) || !str.equals(this.c)) {
            cancel(this.e);
        }
    }

    private int d() {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.f1481a = this.c;
        searchRequest.b = this.b;
        searchRequest.c = d;
        searchRequest.d = this.h;
        searchRequest.e = this.g;
        searchRequest.f = this.i;
        return send(searchRequest);
    }

    public b b() {
        this.j.c(this.k);
        this.j.b(0);
        this.j.a(this.f3890a + "|" + this.c);
        this.j.a(this.g);
        return this.j;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z;
        if (jceStruct2 != null && this.e == i2) {
            SearchRequest searchRequest = (SearchRequest) jceStruct;
            if (this.c.equals(searchRequest.f1481a)) {
                SearchResponse searchResponse = (SearchResponse) jceStruct2;
                this.b = searchResponse.b;
                this.g = searchResponse.g;
                this.j.a(this.g);
                if (searchRequest.b == null || searchRequest.b.length == 0) {
                }
                ArrayList arrayList = new ArrayList(a(searchResponse));
                boolean z2 = searchRequest.b == null || searchRequest.b.length == 0;
                if (searchResponse.f == 1) {
                    z = true;
                } else {
                    z = false;
                }
                int i3 = searchResponse.l;
                String str = searchResponse.m;
                this.f = z;
                runOnUiThread(new m(this, z2, arrayList, i2, searchResponse, z, str, i3));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
        SearchRequest searchRequest = (SearchRequest) jceStruct;
        notifyDataChangedInMainThread(new o(this, i2, i3, searchRequest.b == null || searchRequest.b.length == 0));
    }

    public void c() {
        this.c = null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:115:0x0049 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List<com.tencent.pangu.model.b> a(com.tencent.assistant.protocol.jce.SearchResponse r10) {
        /*
            r9 = this;
            r8 = 4
            r7 = 3
            r6 = 2
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            if (r10 == 0) goto L_0x0016
            java.util.ArrayList<com.tencent.assistant.protocol.jce.SearchCard> r0 = r10.j
            if (r0 == 0) goto L_0x0016
            java.util.ArrayList<com.tencent.assistant.protocol.jce.SearchCard> r0 = r10.j
            int r0 = r0.size()
            if (r0 != 0) goto L_0x0018
        L_0x0016:
            r0 = r2
        L_0x0017:
            return r0
        L_0x0018:
            r0 = 0
            r1 = r0
        L_0x001a:
            java.util.ArrayList<com.tencent.assistant.protocol.jce.SearchCard> r0 = r10.j
            int r0 = r0.size()
            if (r1 >= r0) goto L_0x02f9
            java.util.ArrayList<com.tencent.assistant.protocol.jce.SearchCard> r0 = r10.j
            java.lang.Object r0 = r0.get(r1)
            com.tencent.assistant.protocol.jce.SearchCard r0 = (com.tencent.assistant.protocol.jce.SearchCard) r0
            com.tencent.pangu.model.b r3 = new com.tencent.pangu.model.b
            r3.<init>()
            java.lang.String r4 = r0.c
            r3.h = r4
            byte r4 = r0.f1478a
            com.tencent.pangu.module.AppSearchResultEngine$DataType r5 = com.tencent.pangu.module.AppSearchResultEngine.DataType.APP_MODEL
            int r5 = r5.ordinal()
            if (r4 != r5) goto L_0x0061
            byte[] r0 = r0.b
            java.lang.Class<com.tencent.assistant.protocol.jce.CardItem> r4 = com.tencent.assistant.protocol.jce.CardItem.class
            com.qq.taf.jce.JceStruct r0 = com.tencent.assistant.utils.an.b(r0, r4)
            com.tencent.assistant.protocol.jce.CardItem r0 = (com.tencent.assistant.protocol.jce.CardItem) r0
            if (r0 != 0) goto L_0x004d
        L_0x0049:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x001a
        L_0x004d:
            com.tencent.assistant.model.SimpleAppModel r0 = com.tencent.assistant.module.k.a(r0)
            r3.c = r0
            com.tencent.assistant.model.SimpleAppModel r0 = r3.c
            com.tencent.assistant.module.k.a(r0)
            r0 = 1
            r3.b = r0
        L_0x005b:
            if (r3 == 0) goto L_0x0049
            r2.add(r3)
            goto L_0x0049
        L_0x0061:
            byte r4 = r0.f1478a
            com.tencent.pangu.module.AppSearchResultEngine$DataType r5 = com.tencent.pangu.module.AppSearchResultEngine.DataType.HIT_TAG
            int r5 = r5.ordinal()
            if (r4 != r5) goto L_0x00af
            byte[] r0 = r0.b
            java.lang.Class<com.tencent.assistant.protocol.jce.HitTagInSearch> r4 = com.tencent.assistant.protocol.jce.HitTagInSearch.class
            com.qq.taf.jce.JceStruct r0 = com.tencent.assistant.utils.an.b(r0, r4)
            com.tencent.assistant.protocol.jce.HitTagInSearch r0 = (com.tencent.assistant.protocol.jce.HitTagInSearch) r0
            if (r0 == 0) goto L_0x0049
            com.tencent.assistant.smartcard.d.b r4 = new com.tencent.assistant.smartcard.d.b
            r4.<init>()
            java.lang.String r5 = r0.f1384a
            r4.l = r5
            java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem> r5 = r0.b
            java.util.ArrayList r5 = com.tencent.assistant.module.k.b(r5)
            java.util.ArrayList r5 = r9.a(r5)
            r4.c = r5
            int r5 = r0.c
            r4.f1749a = r5
            java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel> r5 = r4.c
            if (r5 == 0) goto L_0x0049
            java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel> r5 = r4.c
            int r5 = r5.size()
            if (r5 == 0) goto L_0x0049
            java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel> r5 = r4.c
            int r5 = r5.size()
            int r0 = r0.c
            if (r5 < r0) goto L_0x0049
            r0 = 2002(0x7d2, float:2.805E-42)
            r4.j = r0
            r3.g = r4
            r3.b = r6
            goto L_0x005b
        L_0x00af:
            byte r4 = r0.f1478a
            com.tencent.pangu.module.AppSearchResultEngine$DataType r5 = com.tencent.pangu.module.AppSearchResultEngine.DataType.TOPIC
            int r5 = r5.ordinal()
            if (r4 != r5) goto L_0x00ea
            byte[] r0 = r0.b
            java.lang.Class<com.tencent.assistant.protocol.jce.HitTopicInSearch> r4 = com.tencent.assistant.protocol.jce.HitTopicInSearch.class
            com.qq.taf.jce.JceStruct r0 = com.tencent.assistant.utils.an.b(r0, r4)
            com.tencent.assistant.protocol.jce.HitTopicInSearch r0 = (com.tencent.assistant.protocol.jce.HitTopicInSearch) r0
            if (r0 == 0) goto L_0x0049
            com.tencent.assistant.smartcard.d.aa r4 = new com.tencent.assistant.smartcard.d.aa
            r4.<init>()
            java.lang.String r5 = r0.b
            r4.p = r5
            com.tencent.assistant.protocol.jce.ActionUrl r5 = r0.d
            if (r5 == 0) goto L_0x00d8
            com.tencent.assistant.protocol.jce.ActionUrl r5 = r0.d
            java.lang.String r5 = r5.f1125a
            r4.o = r5
        L_0x00d8:
            java.lang.String r5 = r0.c
            r4.f1745a = r5
            r5 = 2001(0x7d1, float:2.804E-42)
            r4.j = r5
            int r0 = r0.f1385a
            r4.k = r0
            r3.g = r4
            r3.b = r6
            goto L_0x005b
        L_0x00ea:
            byte r4 = r0.f1478a
            com.tencent.pangu.module.AppSearchResultEngine$DataType r5 = com.tencent.pangu.module.AppSearchResultEngine.DataType.VIDEOCARD
            int r5 = r5.ordinal()
            if (r4 != r5) goto L_0x0135
            byte[] r0 = r0.b
            java.lang.Class<com.tencent.assistant.protocol.jce.CommonCardInSearch> r4 = com.tencent.assistant.protocol.jce.CommonCardInSearch.class
            com.qq.taf.jce.JceStruct r0 = com.tencent.assistant.utils.an.b(r0, r4)
            com.tencent.assistant.protocol.jce.CommonCardInSearch r0 = (com.tencent.assistant.protocol.jce.CommonCardInSearch) r0
            if (r0 == 0) goto L_0x0049
            com.tencent.assistant.smartcard.d.ab r4 = new com.tencent.assistant.smartcard.d.ab
            r4.<init>()
            java.lang.String r5 = r0.f1205a
            r4.l = r5
            java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem> r5 = r0.b
            java.util.ArrayList r5 = com.tencent.assistant.module.k.b(r5)
            r4.b = r5
            int r5 = r0.c
            r4.f1746a = r5
            java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel> r5 = r4.b
            if (r5 == 0) goto L_0x0049
            java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel> r5 = r4.b
            int r5 = r5.size()
            if (r5 == 0) goto L_0x0049
            java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel> r5 = r4.b
            int r5 = r5.size()
            int r0 = r0.c
            if (r5 < r0) goto L_0x0049
            r0 = 2003(0x7d3, float:2.807E-42)
            r4.j = r0
            r3.g = r4
            r3.b = r6
            goto L_0x005b
        L_0x0135:
            byte r4 = r0.f1478a
            com.tencent.pangu.module.AppSearchResultEngine$DataType r5 = com.tencent.pangu.module.AppSearchResultEngine.DataType.FENGYUNCARD
            int r5 = r5.ordinal()
            if (r4 != r5) goto L_0x018c
            byte[] r0 = r0.b
            java.lang.Class<com.tencent.assistant.protocol.jce.CommonCardInSearch> r4 = com.tencent.assistant.protocol.jce.CommonCardInSearch.class
            com.qq.taf.jce.JceStruct r0 = com.tencent.assistant.utils.an.b(r0, r4)
            com.tencent.assistant.protocol.jce.CommonCardInSearch r0 = (com.tencent.assistant.protocol.jce.CommonCardInSearch) r0
            if (r0 == 0) goto L_0x0049
            com.tencent.assistant.smartcard.d.f r4 = new com.tencent.assistant.smartcard.d.f
            r4.<init>()
            java.lang.String r5 = r0.f1205a
            r4.l = r5
            java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem> r5 = r0.b
            java.util.ArrayList r5 = com.tencent.assistant.module.k.b(r5)
            java.util.ArrayList r5 = r9.b(r5)
            r4.b = r5
            int r5 = r0.c
            r4.f1753a = r5
            java.lang.String r5 = r0.d
            r4.p = r5
            java.lang.String r5 = r0.e
            r4.o = r5
            java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel> r5 = r4.b
            if (r5 == 0) goto L_0x0049
            java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel> r5 = r4.b
            int r5 = r5.size()
            if (r5 == 0) goto L_0x0049
            java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel> r5 = r4.b
            int r5 = r5.size()
            int r0 = r0.c
            if (r5 < r0) goto L_0x0049
            r0 = 2004(0x7d4, float:2.808E-42)
            r4.j = r0
            r3.g = r4
            r3.b = r6
            goto L_0x005b
        L_0x018c:
            byte r4 = r0.f1478a
            com.tencent.pangu.module.AppSearchResultEngine$DataType r5 = com.tencent.pangu.module.AppSearchResultEngine.DataType.VIDEO
            int r5 = r5.ordinal()
            if (r4 != r5) goto L_0x01ac
            byte[] r0 = r0.b
            java.lang.Class<com.tencent.assistant.protocol.jce.VideoInfo> r4 = com.tencent.assistant.protocol.jce.VideoInfo.class
            com.qq.taf.jce.JceStruct r0 = com.tencent.assistant.utils.an.b(r0, r4)
            com.tencent.assistant.protocol.jce.VideoInfo r0 = (com.tencent.assistant.protocol.jce.VideoInfo) r0
            if (r0 == 0) goto L_0x0049
            com.tencent.cloud.model.SimpleVideoModel r0 = com.tencent.cloud.d.u.a(r0)
            r3.e = r0
            r3.b = r7
            goto L_0x005b
        L_0x01ac:
            byte r4 = r0.f1478a
            com.tencent.pangu.module.AppSearchResultEngine$DataType r5 = com.tencent.pangu.module.AppSearchResultEngine.DataType.VIDEO
            int r5 = r5.ordinal()
            if (r4 != r5) goto L_0x01cc
            byte[] r0 = r0.b
            java.lang.Class<com.tencent.assistant.protocol.jce.VideoInfo> r4 = com.tencent.assistant.protocol.jce.VideoInfo.class
            com.qq.taf.jce.JceStruct r0 = com.tencent.assistant.utils.an.b(r0, r4)
            com.tencent.assistant.protocol.jce.VideoInfo r0 = (com.tencent.assistant.protocol.jce.VideoInfo) r0
            if (r0 == 0) goto L_0x0049
            com.tencent.cloud.model.SimpleVideoModel r0 = com.tencent.cloud.d.u.a(r0)
            r3.e = r0
            r3.b = r7
            goto L_0x005b
        L_0x01cc:
            byte r4 = r0.f1478a
            com.tencent.pangu.module.AppSearchResultEngine$DataType r5 = com.tencent.pangu.module.AppSearchResultEngine.DataType.VIDEO_RELATE_CARD
            int r5 = r5.ordinal()
            if (r4 != r5) goto L_0x0217
            byte[] r0 = r0.b
            java.lang.Class<com.tencent.assistant.protocol.jce.VideoCard> r4 = com.tencent.assistant.protocol.jce.VideoCard.class
            com.qq.taf.jce.JceStruct r0 = com.tencent.assistant.utils.an.b(r0, r4)
            com.tencent.assistant.protocol.jce.VideoCard r0 = (com.tencent.assistant.protocol.jce.VideoCard) r0
            if (r0 == 0) goto L_0x0049
            com.tencent.assistant.smartcard.d.ac r4 = new com.tencent.assistant.smartcard.d.ac
            r4.<init>()
            java.lang.String r5 = r0.f1609a
            r4.l = r5
            java.util.ArrayList<com.tencent.assistant.protocol.jce.VideoInfo> r5 = r0.b
            java.util.List r5 = com.tencent.cloud.d.u.a(r5)
            r4.f1747a = r5
            int r0 = r0.c
            r4.b = r0
            java.util.List<com.tencent.cloud.model.SimpleVideoModel> r0 = r4.f1747a
            if (r0 == 0) goto L_0x0049
            java.util.List<com.tencent.cloud.model.SimpleVideoModel> r0 = r4.f1747a
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x0049
            java.util.List<com.tencent.cloud.model.SimpleVideoModel> r0 = r4.f1747a
            int r0 = r0.size()
            int r5 = r4.b
            if (r0 < r5) goto L_0x0049
            r0 = 2005(0x7d5, float:2.81E-42)
            r4.j = r0
            r3.g = r4
            r3.b = r6
            goto L_0x005b
        L_0x0217:
            byte r4 = r0.f1478a
            com.tencent.pangu.module.AppSearchResultEngine$DataType r5 = com.tencent.pangu.module.AppSearchResultEngine.DataType.EBOOK
            int r5 = r5.ordinal()
            if (r4 != r5) goto L_0x0237
            byte[] r0 = r0.b
            java.lang.Class<com.tencent.assistant.protocol.jce.EBookInfo> r4 = com.tencent.assistant.protocol.jce.EBookInfo.class
            com.qq.taf.jce.JceStruct r0 = com.tencent.assistant.utils.an.b(r0, r4)
            com.tencent.assistant.protocol.jce.EBookInfo r0 = (com.tencent.assistant.protocol.jce.EBookInfo) r0
            if (r0 == 0) goto L_0x0049
            com.tencent.cloud.model.SimpleEbookModel r0 = com.tencent.cloud.d.q.a(r0)
            r3.f = r0
            r3.b = r8
            goto L_0x005b
        L_0x0237:
            byte r4 = r0.f1478a
            com.tencent.pangu.module.AppSearchResultEngine$DataType r5 = com.tencent.pangu.module.AppSearchResultEngine.DataType.EBOOK
            int r5 = r5.ordinal()
            if (r4 != r5) goto L_0x0257
            byte[] r0 = r0.b
            java.lang.Class<com.tencent.assistant.protocol.jce.EBookInfo> r4 = com.tencent.assistant.protocol.jce.EBookInfo.class
            com.qq.taf.jce.JceStruct r0 = com.tencent.assistant.utils.an.b(r0, r4)
            com.tencent.assistant.protocol.jce.EBookInfo r0 = (com.tencent.assistant.protocol.jce.EBookInfo) r0
            if (r0 == 0) goto L_0x0049
            com.tencent.cloud.model.SimpleEbookModel r0 = com.tencent.cloud.d.q.a(r0)
            r3.f = r0
            r3.b = r8
            goto L_0x005b
        L_0x0257:
            byte r4 = r0.f1478a
            com.tencent.pangu.module.AppSearchResultEngine$DataType r5 = com.tencent.pangu.module.AppSearchResultEngine.DataType.EBOOK_RELATE_CARD
            int r5 = r5.ordinal()
            if (r4 != r5) goto L_0x02a2
            byte[] r0 = r0.b
            java.lang.Class<com.tencent.assistant.protocol.jce.EBookCard> r4 = com.tencent.assistant.protocol.jce.EBookCard.class
            com.qq.taf.jce.JceStruct r0 = com.tencent.assistant.utils.an.b(r0, r4)
            com.tencent.assistant.protocol.jce.EBookCard r0 = (com.tencent.assistant.protocol.jce.EBookCard) r0
            if (r0 == 0) goto L_0x0049
            com.tencent.assistant.smartcard.d.e r4 = new com.tencent.assistant.smartcard.d.e
            r4.<init>()
            java.lang.String r5 = r0.f1224a
            r4.l = r5
            java.util.ArrayList<com.tencent.assistant.protocol.jce.EBookInfo> r5 = r0.b
            java.util.List r5 = com.tencent.cloud.d.q.a(r5)
            r4.f1752a = r5
            int r0 = r0.c
            r4.b = r0
            java.util.List<com.tencent.cloud.model.SimpleEbookModel> r0 = r4.f1752a
            if (r0 == 0) goto L_0x0049
            java.util.List<com.tencent.cloud.model.SimpleEbookModel> r0 = r4.f1752a
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x0049
            java.util.List<com.tencent.cloud.model.SimpleEbookModel> r0 = r4.f1752a
            int r0 = r0.size()
            int r5 = r4.b
            if (r0 < r5) goto L_0x0049
            r0 = 2006(0x7d6, float:2.811E-42)
            r4.j = r0
            r3.g = r4
            r3.b = r6
            goto L_0x005b
        L_0x02a2:
            byte r4 = r0.f1478a
            com.tencent.pangu.module.AppSearchResultEngine$DataType r5 = com.tencent.pangu.module.AppSearchResultEngine.DataType.YUYI_TAG_CARD
            int r5 = r5.ordinal()
            if (r4 != r5) goto L_0x005b
            byte[] r0 = r0.b
            java.lang.Class<com.tencent.assistant.protocol.jce.CommonCardInSearch> r4 = com.tencent.assistant.protocol.jce.CommonCardInSearch.class
            com.qq.taf.jce.JceStruct r0 = com.tencent.assistant.utils.an.b(r0, r4)
            com.tencent.assistant.protocol.jce.CommonCardInSearch r0 = (com.tencent.assistant.protocol.jce.CommonCardInSearch) r0
            if (r0 == 0) goto L_0x0049
            com.tencent.assistant.smartcard.d.ad r4 = new com.tencent.assistant.smartcard.d.ad
            r4.<init>()
            java.lang.String r5 = r0.f1205a
            r4.l = r5
            java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem> r5 = r0.b
            java.util.ArrayList r5 = com.tencent.assistant.module.k.b(r5)
            java.util.ArrayList r5 = r9.b(r5)
            r4.b = r5
            int r5 = r0.c
            r4.f1748a = r5
            java.lang.String r5 = r0.d
            r4.p = r5
            java.lang.String r5 = r0.e
            r4.o = r5
            java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel> r5 = r4.b
            if (r5 == 0) goto L_0x0049
            java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel> r5 = r4.b
            int r5 = r5.size()
            if (r5 == 0) goto L_0x0049
            java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel> r5 = r4.b
            int r5 = r5.size()
            int r0 = r0.c
            if (r5 < r0) goto L_0x0049
            r0 = 2007(0x7d7, float:2.812E-42)
            r4.j = r0
            r3.g = r4
            r3.b = r6
            goto L_0x005b
        L_0x02f9:
            r0 = r2
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.pangu.module.AppSearchResultEngine.a(com.tencent.assistant.protocol.jce.SearchResponse):java.util.List");
    }

    private ArrayList<SimpleAppModel> a(List<SimpleAppModel> list) {
        LocalApkInfo installedApkInfo;
        if (list == null || list.size() == 0) {
            return null;
        }
        ArrayList<SimpleAppModel> arrayList = new ArrayList<>(list);
        ArrayList arrayList2 = new ArrayList();
        for (SimpleAppModel next : list) {
            if (next != null && (installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(next.c)) != null && installedApkInfo.mVersionCode == next.g && installedApkInfo.mGrayVersionCode == next.ad) {
                arrayList2.add(next);
            }
        }
        arrayList.removeAll(arrayList2);
        return arrayList;
    }

    private ArrayList<SimpleAppModel> b(List<SimpleAppModel> list) {
        LocalApkInfo installedApkInfo;
        if (list == null || list.size() == 0) {
            return null;
        }
        ArrayList<SimpleAppModel> arrayList = new ArrayList<>(list);
        ArrayList arrayList2 = new ArrayList();
        for (SimpleAppModel next : list) {
            if (next != null && (installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(next.c)) != null && installedApkInfo.mVersionCode == next.g && installedApkInfo.mGrayVersionCode == next.ad) {
                arrayList2.add(next);
            }
        }
        arrayList.removeAll(arrayList2);
        arrayList.addAll(arrayList2);
        return arrayList;
    }
}
