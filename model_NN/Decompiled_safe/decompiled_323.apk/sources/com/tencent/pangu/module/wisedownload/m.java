package com.tencent.pangu.module.wisedownload;

import android.util.Log;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.module.wisedownload.condition.ThresholdCondition;
import com.tencent.securemodule.service.Constants;

/* compiled from: ProGuard */
public class m {
    public static void a(int i, ThresholdCondition.CONDITION_RESULT_CODE condition_result_code, int i2) {
        p pVar = new p();
        pVar.a(i2);
        pVar.b(condition_result_code.ordinal());
        if (condition_result_code != ThresholdCondition.CONDITION_RESULT_CODE.OK) {
            i.a().a(pVar);
        }
        a(i, STConstAction.ACTION_HIT_WISE_DOWNLOAD, condition_result_code.ordinal(), pVar.f());
    }

    public static void b(int i, ThresholdCondition.CONDITION_RESULT_CODE condition_result_code, int i2) {
        int i3;
        if (condition_result_code == ThresholdCondition.CONDITION_RESULT_CODE.OK_BEGIN_DOWNLOAD) {
            i3 = 200;
        } else if (condition_result_code == ThresholdCondition.CONDITION_RESULT_CODE.OK_CONTINUE_DOWNLOAD) {
            i3 = Constants.PLATFROM_ANDROID_PHONE;
        } else {
            i3 = 100;
        }
        a(i, 900, i3, String.valueOf(i2));
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static void a(int i, int i2, int i3, String str) {
        Log.v("AAA", "group= " + i + " actionId = " + i2 + " eventId = " + i3 + " statUserInfoStr  = " + str);
        switch (i) {
            case 1:
                STInfoV2 sTInfoV2 = new STInfoV2(STConst.ST_PAGE_WIFI_WISE_SELF_UPDATE, a(3, i3), STConst.ST_PAGE_WIFI_WISE_SELF_UPDATE, STConst.ST_DEFAULT_SLOT, i2);
                sTInfoV2.extraData = str;
                l.a(sTInfoV2);
                return;
            case 2:
                STInfoV2 sTInfoV22 = new STInfoV2(STConst.ST_PAGE_WIFI_WISE_UPDATE, a(3, i3), STConst.ST_PAGE_WIFI_WISE_UPDATE, STConst.ST_DEFAULT_SLOT, i2);
                sTInfoV22.extraData = str;
                l.a(sTInfoV22);
                return;
            case 3:
                STInfoV2 sTInfoV23 = new STInfoV2(STConst.ST_PAGE_APP_TREASURE_BOX, a(5, i3), STConst.ST_PAGE_APP_TREASURE_BOX, STConst.ST_DEFAULT_SLOT, i2);
                sTInfoV23.extraData = str;
                l.a(sTInfoV23);
                return;
            case 4:
            default:
                return;
            case 5:
                STInfoV2 sTInfoV24 = new STInfoV2(STConst.ST_PAGE_WIFI_WISE_BOOKING, a(3, i3), STConst.ST_PAGE_WIFI_WISE_BOOKING, STConst.ST_DEFAULT_SLOT, i2);
                sTInfoV24.extraData = str;
                l.a(sTInfoV24);
                break;
            case 6:
                break;
        }
        STInfoV2 sTInfoV25 = new STInfoV2(STConst.ST_PAGE_WIFI_WISE_SUBSCRIPTION, a(3, i3), STConst.ST_PAGE_WIFI_WISE_SUBSCRIPTION, STConst.ST_DEFAULT_SLOT, i2);
        sTInfoV25.extraData = str;
        l.a(sTInfoV25);
    }

    private static String a(int i, int i2) {
        return '0' + String.valueOf(i) + "_" + bm.a(i2);
    }

    public static int a(SimpleDownloadInfo.UIType uIType) {
        switch (n.f3968a[uIType.ordinal()]) {
            case 1:
                return STConst.ST_PAGE_WIFI_WISE_UPDATE;
            case 2:
                return STConst.ST_PAGE_APP_TREASURE_BOX;
            case 3:
                return STConst.ST_PAGE_WIFI_WISE_SELF_UPDATE;
            case 4:
                return STConst.ST_PAGE_WIFI_WISE_BOOKING;
            case 5:
                return STConst.ST_PAGE_WIFI_WISE_SUBSCRIPTION;
            default:
                return -1;
        }
    }
}
