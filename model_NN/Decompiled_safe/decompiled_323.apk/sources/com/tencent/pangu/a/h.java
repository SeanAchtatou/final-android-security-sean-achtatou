package com.tencent.pangu.a;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.view.ViewStub;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.r;
import com.tencent.pangu.activity.AppTreasureBoxActivity;
import com.tencent.pangu.component.treasurebox.AppTreasureEntryBlinkEyesView;

/* compiled from: ProGuard */
public class h extends a {
    UIEventListener c = new k(this);
    private View d;
    /* access modifiers changed from: private */
    public AppTreasureEntryBlinkEyesView e;
    /* access modifiers changed from: private */
    public TextView f;
    /* access modifiers changed from: private */
    public boolean g = false;
    private final int h = 1;
    private final int i = 1;
    private final int j = 0;
    /* access modifiers changed from: private */
    public Handler k = new m(this);

    public h(Context context, ViewStub viewStub) {
        super(context, viewStub);
        a();
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_SHOW_TREASURE_BOX_ENTRY, this.c);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_DISMISS_TREASURE_BOX_ENTRY, this.c);
    }

    public void a(boolean z) {
        b(z);
    }

    /* access modifiers changed from: protected */
    public void a() {
        b();
    }

    private void b() {
        this.b.setLayoutResource(R.layout.competitive_stub_treasureboxentry);
        this.d = this.b.inflate();
        BaseActivity baseActivity = (BaseActivity) this.f3289a;
        this.e = (AppTreasureEntryBlinkEyesView) this.d.findViewById(R.id.treasurebox_entry);
        this.f = (TextView) this.d.findViewById(R.id.treasurebox_entry_beybey_bubble);
        if (this.e != null) {
            this.e.setOnClickListener(new i(this, baseActivity));
        }
    }

    private void b(boolean z) {
        if (z) {
            this.e.setVisibility(8);
            this.f.setVisibility(8);
            c();
            return;
        }
        TemporaryThreadManager.get().start(new j(this));
    }

    /* access modifiers changed from: private */
    public void c() {
        if (r.e <= 0 && this.e != null) {
            this.e.measure(0, 0);
            r.e = this.e.getMeasuredHeight();
        }
        int i2 = 2000;
        if (this.f3289a != null && (this.f3289a instanceof BaseActivity)) {
            i2 = ((BaseActivity) this.f3289a).f();
        }
        AstApp.i().j().removeMessages(EventDispatcherEnum.UI_EVENT_DISMISS_TREASURE_BOX_ENTRY);
        Intent intent = new Intent(this.f3289a, AppTreasureBoxActivity.class);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
        this.f3289a.startActivity(intent);
    }
}
