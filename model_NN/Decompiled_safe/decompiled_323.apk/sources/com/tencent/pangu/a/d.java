package com.tencent.pangu.a;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewStub;
import android.widget.ImageView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.NpcCfg;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ah;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.graphic.g;
import com.tencent.pangu.graphic.h;
import com.tencent.pangu.graphic.i;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
public class d extends a {
    /* access modifiers changed from: private */
    public NpcCfg c = null;
    /* access modifiers changed from: private */
    public ImageView d;
    private boolean e = false;
    private View f;
    private h g = new g(this);

    public d(Context context, ViewStub viewStub) {
        super(context, viewStub);
        a();
    }

    /* access modifiers changed from: protected */
    public void a() {
        b();
    }

    public void a(boolean z) {
        if (z) {
            if (this.c != null) {
                e();
            }
        } else if (c.a().a(this.c)) {
            this.d.setVisibility(0);
            if (!(this.d.getBackground() instanceof AnimationDrawable)) {
                c();
            }
        } else {
            this.d.setVisibility(8);
        }
    }

    public void a(NpcCfg npcCfg) {
        this.c = npcCfg;
    }

    private void b() {
        this.b.setLayoutResource(R.layout.competitive_stub_operationentry);
        this.f = this.b.inflate();
        this.d = (ImageView) this.f.findViewById(R.id.operation_entry);
        this.d.setOnClickListener(new e(this, (BaseActivity) this.f3289a));
    }

    private void c() {
        g gVar = new g(d());
        i.a().a(this.g);
        i.a().a(gVar);
    }

    /* access modifiers changed from: private */
    public String d() {
        if (this.c == null || this.c.f == null || this.c.f.size() <= 0) {
            return Constants.STR_EMPTY;
        }
        return this.c.f.get(0);
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        int i;
        int i2 = 2000;
        if (z) {
            c.a().a(this.c, z);
        } else if (!this.e) {
            c.a().a(this.c, z);
            this.e = true;
            if (this.f3289a == null || !(this.f3289a instanceof BaseActivity)) {
                i = 2000;
            } else {
                i = ((BaseActivity) this.f3289a).f();
                i2 = ((BaseActivity) this.f3289a).m();
            }
            l.a(new STInfoV2(i, "15_001", i2, STConst.ST_DEFAULT_SLOT, 100));
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        Bundle bundle = new Bundle();
        if (this.f3289a instanceof BaseActivity) {
            bundle.putSerializable(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, Integer.valueOf(((BaseActivity) this.f3289a).f()));
        }
        b.b(this.f3289a, this.c.e, bundle);
    }

    /* access modifiers changed from: private */
    public void a(AnimationDrawable animationDrawable) {
        ah.a().post(new f(this, animationDrawable));
    }
}
