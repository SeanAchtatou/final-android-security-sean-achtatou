package com.tencent.pangu.manager;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.m;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class bb extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f3811a;
    final /* synthetic */ au b;

    bb(au auVar, STInfoV2 sTInfoV2) {
        this.b = auVar;
        this.f3811a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        if (this.b.b != null) {
            this.b.b.setVisibility(8);
        }
        m.a().b("succ_float_cancel", Long.valueOf(System.currentTimeMillis()));
    }

    public STInfoV2 getStInfo() {
        this.f3811a.slotId = "03_002_200";
        this.f3811a.actionId = 200;
        return this.f3811a;
    }
}
