package com.tencent.pangu.manager;

import com.tencent.assistant.protocol.jce.SearchWebCfg;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class f {

    /* renamed from: a  reason: collision with root package name */
    private static f f3829a;
    private SearchWebCfg b;

    public static synchronized f a() {
        f fVar;
        synchronized (f.class) {
            if (f3829a == null) {
                f3829a = new f();
            }
            fVar = f3829a;
        }
        return fVar;
    }

    public void a(SearchWebCfg searchWebCfg) {
        XLog.d("SearchWebCfg", "cfg = " + searchWebCfg);
        this.b = searchWebCfg;
    }

    public SearchWebCfg b() {
        return this.b;
    }
}
