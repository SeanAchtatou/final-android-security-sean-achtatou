package com.tencent.pangu.manager;

import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.h;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;

/* compiled from: ProGuard */
class bf implements h {

    /* renamed from: a  reason: collision with root package name */
    bj f3815a;
    final /* synthetic */ au b;

    bf(au auVar) {
        this.b = auVar;
    }

    public void a(bj bjVar) {
        this.f3815a = bjVar;
    }

    public void onGetAppInfoSuccess(int i, int i2, AppSimpleDetail appSimpleDetail) {
        if (appSimpleDetail == null || this.f3815a == null || this.b.G == null || this.b.E == null || this.b.b == null) {
            XLog.e("RecommendDownloadManager", "<install> simpleDetail = " + appSimpleDetail + ", matcher = " + this.f3815a);
            return;
        }
        SimpleAppModel a2 = k.a(appSimpleDetail);
        DownloadInfo a3 = DownloadProxy.a().a(a2);
        if (a3 != null && a3.needReCreateInfo(a2)) {
            DownloadProxy.a().b(a3.downloadTicket);
            a3 = null;
        }
        if (a3 == null) {
            StatInfo statInfo = new StatInfo(a2.b, STConst.ST_PAGE_INSTALL_RECOMMEND, 0, null, 0);
            statInfo.scene = STConst.ST_PAGE_INSTALL_RECOMMEND;
            a3 = DownloadInfo.createDownloadInfo(a2, statInfo);
            a3.downloadState = SimpleDownloadInfo.DownloadState.PAUSED;
        }
        DownloadInfo downloadInfo = a3;
        downloadInfo.autoInstall = true;
        this.b.E.updateImageView(downloadInfo.iconUrl, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        if (!this.b.f()) {
            XLog.e("RecommendDownloadManager", "<install> 判断<不会>弹小黄条");
            return;
        }
        try {
            if (this.b.b.isShown()) {
                XLog.d("RecommendDownloadManager", "<install> 已经有小黄条正在显示  !");
                return;
            }
            XLog.d("RecommendDownloadManager", "<install> 弹出小黄条");
            this.b.b.setVisibility(0);
            this.b.f3803a.addView(this.b.b, this.b.g());
            this.f3815a.c();
            this.b.a(STConst.ST_PAGE_INSTALL_RECOMMEND, AstApp.m().f(), STConst.ST_DEFAULT_SLOT, 100, null);
            au.a("rec_pop_tips_exp", this.f3815a.e);
            this.b.b.postDelayed(new bg(this), 5000);
            this.b.G.setOnClickListener(new bh(this, downloadInfo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onGetAppInfoFail(int i, int i2) {
        XLog.e("RecommendDownloadManager", "<install> 服务器拉取推荐下载的应用信息<失败>");
    }
}
