package com.tencent.pangu.manager;

import com.tencent.assistant.AppConst;
import com.tencent.pangu.download.DownloadInfo;
import java.util.concurrent.CountDownLatch;

/* compiled from: ProGuard */
class am extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3795a;
    final /* synthetic */ DownloadInfo b;
    final /* synthetic */ boolean c;
    final /* synthetic */ ak d;

    am(ak akVar, int i, DownloadInfo downloadInfo, boolean z) {
        this.d = akVar;
        this.f3795a = i;
        this.b = downloadInfo;
        this.c = z;
    }

    public void onRightBtnClick() {
        if (this.f3795a == 1) {
            this.d.j(this.b);
            if (this.c) {
                this.d.a(20430101, ak.b, "03_001", 200);
            } else {
                this.d.a(20430102, ak.b, "03_001", 200);
            }
        } else {
            this.d.a((CountDownLatch) null);
            if (this.c) {
                this.d.a(20430103, ak.b, "03_001", 200);
            } else {
                this.d.a(20430104, ak.b, "03_001", 200);
            }
        }
    }

    public void onLeftBtnClick() {
        onCancell();
    }

    public void onCancell() {
        if (this.f3795a != 2) {
            this.d.c(this.b);
        }
        if (this.f3795a == 1) {
            if (this.c) {
                this.d.a(20430101, ak.b, "03_002", 200);
            } else {
                this.d.a(20430102, ak.b, "03_002", 200);
            }
        } else if (this.c) {
            this.d.a(20430103, ak.b, "03_002", 200);
        } else {
            this.d.a(20430104, ak.b, "03_002", 200);
        }
    }
}
