package com.tencent.pangu.activity;

import android.widget.AbsListView;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;

/* compiled from: ProGuard */
class aj extends ListViewScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadActivity f3311a;

    aj(DownloadActivity downloadActivity) {
        this.f3311a = downloadActivity;
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.f3311a.A != null && this.f3311a.w != null) {
            if (i == 0) {
                this.f3311a.A.setVisibility(8);
            }
            try {
                this.f3311a.B();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
