package com.tencent.pangu.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;

/* compiled from: ProGuard */
class cy extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartPopWindowActivity f3376a;

    private cy(StartPopWindowActivity startPopWindowActivity) {
        this.f3376a = startPopWindowActivity;
    }

    /* synthetic */ cy(StartPopWindowActivity startPopWindowActivity, cn cnVar) {
        this(startPopWindowActivity);
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.net.wifi.STATE_CHANGE")) {
            NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra("networkInfo");
            this.f3376a.v.postDelayed(new cz(this), 500);
        }
    }
}
