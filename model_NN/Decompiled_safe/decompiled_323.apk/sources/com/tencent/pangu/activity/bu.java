package com.tencent.pangu.activity;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class bu extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f3345a;

    bu(SearchActivity searchActivity) {
        this.f3345a = searchActivity;
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        this.f3345a.runOnUiThread(new bv(this));
    }
}
