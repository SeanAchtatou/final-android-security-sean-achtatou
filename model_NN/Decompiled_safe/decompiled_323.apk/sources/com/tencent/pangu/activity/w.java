package com.tencent.pangu.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class w extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppTreasureBoxActivity f3396a;

    w(AppTreasureBoxActivity appTreasureBoxActivity) {
        this.f3396a = appTreasureBoxActivity;
    }

    public void onTMAClick(View view) {
        this.f3396a.t();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3396a.n, 200);
        buildSTInfo.slotId = a.a("06", "001");
        return buildSTInfo;
    }
}
