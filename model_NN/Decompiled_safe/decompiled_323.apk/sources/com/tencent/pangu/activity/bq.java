package com.tencent.pangu.activity;

import android.os.Handler;
import android.view.ViewTreeObserver;

/* compiled from: ProGuard */
class bq implements ViewTreeObserver.OnGlobalLayoutListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ReportActivity f3341a;

    bq(ReportActivity reportActivity) {
        this.f3341a = reportActivity;
    }

    public void onGlobalLayout() {
        this.f3341a.G.scrollTo(0, this.f3341a.G.getHeight());
        if (this.f3341a.G.getRootView().getHeight() - this.f3341a.G.getHeight() > 100) {
            new Handler().postDelayed(new br(this), 300);
        }
    }
}
