package com.tencent.pangu.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class r extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f3392a;

    r(AppDetailActivityV5 appDetailActivityV5) {
        this.f3392a = appDetailActivityV5;
    }

    public void onTMAClick(View view) {
        int unused = this.f3392a.R = view.getId() - 136;
        this.f3392a.B.setCurrentItem(this.f3392a.R);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3392a.A, 200);
        if (buildSTInfo != null) {
            buildSTInfo.slotId = a.a("07", this.clickViewId - 136);
            if (this.f3392a.aq != null) {
                buildSTInfo.appId = this.f3392a.aq.appId;
                buildSTInfo.contentId = this.f3392a.aq.contentId;
                buildSTInfo.updateWithExternalPara(this.f3392a.q);
            }
        }
        return buildSTInfo;
    }
}
