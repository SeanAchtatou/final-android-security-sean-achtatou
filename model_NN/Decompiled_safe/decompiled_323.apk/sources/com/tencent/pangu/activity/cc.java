package com.tencent.pangu.activity;

import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.event.EventDispatcherEnum;

/* compiled from: ProGuard */
class cc extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f3354a;

    cc(SearchActivity searchActivity) {
        this.f3354a = searchActivity;
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 0:
                String obj = message.obj.toString();
                this.f3354a.x.setVisibility(8);
                this.f3354a.w.setVisibility(8);
                int unused = this.f3354a.I = this.f3354a.G.a(obj.toString().trim());
                return;
            case EventDispatcherEnum.PLUGIN_EVENT_LOGIN_START:
                this.f3354a.B();
                return;
            default:
                return;
        }
    }
}
