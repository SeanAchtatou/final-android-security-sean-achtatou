package com.tencent.pangu.activity;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.a.a;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.activity.BrowserActivity;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.m;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.c;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.by;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class ExternalCallActivity extends BrowserActivity {
    private static final String D = ExternalCallActivity.class.getSimpleName();
    public View.OnClickListener C = new av(this);
    /* access modifiers changed from: private */
    public LinearLayout E;
    private RelativeLayout F;
    /* access modifiers changed from: private */
    public boolean G = true;
    private String H = null;
    private String I = null;
    private int J = STConst.ST_PAGE_EXTERNAL_CALL;
    private b K = null;
    private long L = 0;
    private long M = 0;
    private View.OnClickListener N = new au(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.L = System.currentTimeMillis();
        XLog.d(D, "Activity onCreate time = " + this.L);
        F();
        G();
        a((String) null, 100, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(boolean, int):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(com.tencent.assistantv2.component.SecondNavigationTitleViewV5, java.lang.String):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(com.tencent.assistantv2.component.SecondNavigationTitleViewV5, boolean):boolean
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(java.lang.String, int):void
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(java.lang.String, android.text.TextUtils$TruncateAt):void
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(boolean, int):void */
    private void F() {
        super.c(true);
        if (this.v != null) {
            this.v.a(this);
            this.v.setVisibility(0);
            this.v.i();
            this.v.a(this.n.getResources().getString(R.string.down_page_title));
            this.v.a(false, 0);
            this.v.b((int) R.drawable.common_icon_myapplogo_color);
            this.v.b(this.N);
            this.v.e(this.C);
        }
        this.F = (RelativeLayout) findViewById(R.id.browser_root);
        this.E = new LinearLayout(this.n);
        this.E.setId(R.id.external_call_topbar);
        this.E.setPadding(by.a(this.n, 16.0f), by.a(this.n, 10.0f), by.a(this.n, 16.0f), by.a(this.n, 10.0f));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(3, R.id.browser_header_view);
        this.E.setOrientation(0);
        this.E.setGravity(16);
        this.E.setBackgroundColor(getResources().getColor(R.color.white));
        this.E.setOnClickListener(this.N);
        TXImageView tXImageView = new TXImageView(this.n);
        tXImageView.setImageResource(R.drawable.icon_about_logo);
        tXImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(by.a(this.n, 24.0f), by.a(this.n, 24.0f));
        layoutParams2.rightMargin = by.a(this.n, 10.0f);
        this.E.addView(tXImageView, layoutParams2);
        TextView textView = new TextView(this.n);
        textView.setText((int) R.string.external_call_see_more_apps);
        textView.setTextSize(2, 14.0f);
        this.E.addView(textView);
        if (this.F != null) {
            this.F.addView(this.E, layoutParams);
        }
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams3.addRule(3, this.E.getId());
        if (this.u != null) {
            this.u.setLayoutParams(layoutParams3);
            this.u.a(new at(this));
        }
    }

    private void G() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.I = extras.getString(a.j);
            this.H = extras.getString(a.y);
            Set<String> keySet = extras.keySet();
            JSONObject jSONObject = new JSONObject();
            try {
                for (String next : keySet) {
                    jSONObject.put(next, extras.get(next));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String a2 = m.a().a("key_external_call_yyb_url", (String) null);
            if (!TextUtils.isEmpty(a2)) {
                StringBuilder sb = new StringBuilder();
                try {
                    this.M = System.currentTimeMillis();
                    sb.append(a2).append("downloadinfo").append("=").append(URLEncoder.encode(jSONObject.toString(), "utf-8")).append("&").append("mobileinfo").append("=").append(URLEncoder.encode(H(), "utf-8"));
                    this.u.a(sb.toString());
                    XLog.d(D, "url:" + a2 + " ,webview loadUrl time = " + this.M);
                } catch (UnsupportedEncodingException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    private String H() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("androidId", t.l());
            jSONObject.put("qua", Global.getQUA());
            jSONObject.put("osVer", Build.VERSION.SDK_INT);
            com.tencent.assistant.net.b i = c.i();
            if (i.f1059a == APN.UN_DETECT) {
                c.k();
            }
            jSONObject.put("apn", i.f1059a);
            jSONObject.put("isWap", i.d ? 1 : 0);
            jSONObject.put("wifiBssid", i.e);
            jSONObject.put("resolution", t.b + "x" + t.c);
            jSONObject.put("macAdress", t.k());
            jSONObject.put("imsi", t.h());
            jSONObject.put("imei", t.g());
            jSONObject.put("versionCode", Global.getAppVersionCode());
            jSONObject.put("versionName", Global.getAppVersionName());
            jSONObject.put("phoneGuid", Global.getPhoneGuid());
            jSONObject.put("model", t.u());
            jSONObject.put("deviceName", t.v());
            jSONObject.put("freeMemory", t.r());
            jSONObject.put("totalMemory", t.q());
            jSONObject.put("activityCreateTime", this.L);
            jSONObject.put("webviewLoadTime", this.M);
        } catch (JSONException e) {
            XLog.e(D, "JSONException>>>", e);
        }
        return jSONObject.toString();
    }

    public String D() {
        return this.H;
    }

    /* access modifiers changed from: private */
    public void a(String str, int i, int i2) {
        STInfoV2 buildSTInfo;
        if ((this.n instanceof BaseActivity) && (buildSTInfo = STInfoBuilder.buildSTInfo(this.n, i)) != null) {
            buildSTInfo.scene = this.J;
            if (!TextUtils.isEmpty(this.I)) {
                buildSTInfo.callerVia = this.I;
            }
            switch (i2) {
                case 0:
                    buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a(STConst.ST_DEFAULT_SLOT, Constants.STR_EMPTY);
                    break;
                case 1:
                    buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a("05", "001");
                    break;
                case 2:
                    buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a("05", "002");
                    break;
                case 3:
                    buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a(STConst.ST_STATUS_DEFAULT, "001");
                    break;
            }
            if (str != null) {
                buildSTInfo.status = str;
            }
            if (i == 100) {
                if (this.K == null) {
                    this.K = new b();
                }
                this.K.exposure(buildSTInfo);
                return;
            }
            l.a(buildSTInfo);
        }
    }

    public int f() {
        return this.J;
    }
}
