package com.tencent.pangu.activity;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.mediadownload.e;
import com.tencent.pangu.model.d;

/* compiled from: ProGuard */
class ar extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f3317a;
    final /* synthetic */ DownloadActivity b;

    ar(DownloadActivity downloadActivity, String str) {
        this.b = downloadActivity;
        this.f3317a = str;
    }

    public void onLeftBtnClick() {
        l.a(new STInfoV2(STConst.ST_PAGE_DOWNLOAD_URL_UNSAFE, a.a("03", 1), this.b.f(), STConst.ST_DEFAULT_SLOT, 200));
    }

    public void onRightBtnClick() {
        d a2 = d.a(this.b.P.c(), this.b.P.b(), this.f3317a);
        a2.v = a.a(STInfoBuilder.buildSTInfo(this.b, 900));
        e.c().a(a2);
        l.a(new STInfoV2(STConst.ST_PAGE_DOWNLOAD_URL_UNSAFE, a.a("03", 0), this.b.f(), STConst.ST_DEFAULT_SLOT, 200));
    }

    public void onCancell() {
    }
}
