package com.tencent.pangu.activity;

import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
class f extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f3380a;

    f(AppDetailActivityV5 appDetailActivityV5) {
        this.f3380a = appDetailActivityV5;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.activity.AppDetailActivityV5.a(com.tencent.pangu.activity.AppDetailActivityV5, boolean):boolean
     arg types: [com.tencent.pangu.activity.AppDetailActivityV5, int]
     candidates:
      com.tencent.pangu.activity.AppDetailActivityV5.a(com.tencent.pangu.activity.AppDetailActivityV5, int):int
      com.tencent.pangu.activity.AppDetailActivityV5.a(com.tencent.pangu.activity.AppDetailActivityV5, com.tencent.assistant.localres.model.LocalApkInfo):com.tencent.assistant.localres.model.LocalApkInfo
      com.tencent.pangu.activity.AppDetailActivityV5.a(com.tencent.pangu.activity.AppDetailActivityV5, com.tencent.assistant.model.c):com.tencent.assistant.model.c
      com.tencent.pangu.activity.AppDetailActivityV5.a(com.tencent.assistant.model.c, com.tencent.assistant.model.SimpleAppModel):com.tencent.pangu.model.ShareAppModel
      com.tencent.pangu.activity.AppDetailActivityV5.a(int, com.tencent.assistant.protocol.jce.GetRecommendAppListResponse):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.activity.AppDetailActivityV5.a(com.tencent.pangu.activity.AppDetailActivityV5, boolean):boolean */
    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                if (this.f3380a.I != null) {
                    this.f3380a.I.f();
                    boolean unused = this.f3380a.ar = true;
                    return;
                }
                return;
            case 2:
                if (this.f3380a.Q()) {
                    this.f3380a.B.c().a(-message.arg1);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
