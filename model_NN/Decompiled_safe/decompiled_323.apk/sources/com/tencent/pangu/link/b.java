package com.tencent.pangu.link;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.tencent.assistant.a.a;
import com.tencent.assistant.activity.PhotoBackupNewActivity;
import com.tencent.assistant.protocol.jce.ActionUrl;
import com.tencent.connector.ConnectionActivity;
import com.tencent.connector.UsbDebugModeAlertActivity;
import java.util.HashMap;

/* compiled from: ProGuard */
public class b {
    public static final void a(Context context, String str, Bundle bundle) {
        if (!TextUtils.isEmpty(str)) {
            a(context, Uri.parse(str), bundle);
        }
    }

    public static final void a(Context context, ActionUrl actionUrl) {
        a(context, (Bundle) null, actionUrl);
    }

    public static final void a(Context context, Bundle bundle, ActionUrl actionUrl) {
        if (actionUrl != null && !TextUtils.isEmpty(actionUrl.f1125a)) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putSerializable("com.tencent.assistant.ACTION_URL", actionUrl);
            b(context, actionUrl.f1125a, bundle);
        }
    }

    public static final void b(Context context, String str, Bundle bundle) {
        String str2;
        if (!TextUtils.isEmpty(str)) {
            if (!str.contains("?")) {
                str2 = str + a.b;
            } else {
                str2 = str + a.f3777a;
            }
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putString(a.D, "1");
            a(context, Uri.parse(str2), bundle);
        }
    }

    public static final void a(Context context, String str) {
        a(context, str, (Bundle) null);
    }

    public static final void b(Context context, String str) {
        b(context, str, (Bundle) null);
    }

    public static final void c(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            str = str + a.c;
        }
        Bundle bundle = new Bundle();
        bundle.putString(a.E, "1");
        a(context, str, bundle);
    }

    public static final void a(Context context, Uri uri, Bundle bundle) {
        String str = null;
        try {
            str = uri.getScheme();
        } catch (Throwable th) {
        }
        if (str != null) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean(a.t, true);
            if (str.equals("tmast")) {
                c(context, uri, bundle);
            } else if (str.equals("tpmast")) {
                f(context, uri, bundle);
            } else if (str.equals("market")) {
                e(context, uri, bundle);
            } else if (str.equals("http") || str.equals("https")) {
                d(context, uri, bundle);
            }
        }
    }

    public static final void a(Context context, Uri uri) {
        if (uri != null) {
            a(context, uri, (Bundle) null);
        }
    }

    protected static boolean b(Context context, Uri uri, Bundle bundle) {
        try {
            String host = uri.getHost();
            if (host.equals("usbdebugmode")) {
                b(context, bundle);
                return true;
            } else if (host.equals("picbackup")) {
                c(context, bundle);
                return true;
            } else if (!host.equals("connect")) {
                return false;
            } else {
                a(context, bundle);
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean c(Context context, Uri uri, Bundle bundle) {
        return a.a(context, uri, bundle);
    }

    private static boolean d(Context context, Uri uri, Bundle bundle) {
        return a.b(context, uri, bundle);
    }

    private static boolean e(Context context, Uri uri, Bundle bundle) {
        return a.c(context, uri, bundle);
    }

    private static boolean f(Context context, Uri uri, Bundle bundle) {
        return a.d(context, uri, bundle);
    }

    public static Uri a(String str, String str2, HashMap<String, String> hashMap) {
        return a.a(str, str2, hashMap);
    }

    private static void a(Context context, Bundle bundle) {
        Intent intent = new Intent(context, ConnectionActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        context.startActivity(intent);
    }

    private static void b(Context context, Bundle bundle) {
        Intent intent = new Intent(context, UsbDebugModeAlertActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.setFlags(268435456);
        context.startActivity(intent);
    }

    private static void c(Context context, Bundle bundle) {
        Intent intent = new Intent(context, PhotoBackupNewActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        context.startActivity(intent);
    }

    public static boolean a(Context context, Intent intent) {
        String scheme;
        Uri data = intent.getData();
        if (data == null || (scheme = data.getScheme()) == null || !scheme.equals("tmast")) {
            return a.a(context, intent);
        }
        String host = data.getHost();
        return host.equals("usbdebugmode") || host.equals("picbackup") || host.equals("connect") || a.a(context, intent);
    }
}
