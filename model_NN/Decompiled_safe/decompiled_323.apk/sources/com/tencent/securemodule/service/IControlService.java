package com.tencent.securemodule.service;

public interface IControlService {
    void doRemoteTask(String str, ICallback iCallback);

    void setIsShowingTips(boolean z, boolean z2);
}
