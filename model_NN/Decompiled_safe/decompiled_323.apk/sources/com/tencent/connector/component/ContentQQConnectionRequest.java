package com.tencent.connector.component;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.qq.AppService.s;
import com.qq.l.p;
import com.qq.util.QQUtils;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.st.a;
import com.tencent.connect.common.Constants;
import com.tencent.connector.ConnectionActivity;

/* compiled from: ProGuard */
public class ContentQQConnectionRequest extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f2392a;
    /* access modifiers changed from: private */
    public ConnectionActivity b;
    private BannerQQStatus c;
    private ImageView d;
    private TextView e;
    private TextView f;
    private Button g;
    private Button h;
    private View i;
    private TextView j;
    private ProgressBar k;
    private View l;

    public ContentQQConnectionRequest(Context context) {
        super(context);
        this.f2392a = context;
    }

    public ContentQQConnectionRequest(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f2392a = context;
    }

    public void setHostActivity(ConnectionActivity connectionActivity) {
        this.b = connectionActivity;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.c = (BannerQQStatus) findViewById(R.id.qq_status);
        this.d = (ImageView) findViewById(R.id.logo_qrcode);
        this.e = (TextView) findViewById(R.id.pc_name);
        this.f = (TextView) findViewById(R.id.connection_media);
        this.i = findViewById(R.id.space2);
        this.g = (Button) findViewById(R.id.allow);
        this.h = (Button) findViewById(R.id.cancel);
        this.j = (TextView) findViewById(R.id.connecting_txt);
        this.k = (ProgressBar) findViewById(R.id.connecting_circle);
        this.l = findViewById(R.id.qq_status_zone);
    }

    public void displayQQRequest(String str, String str2, String str3, String str4, String str5) {
        if (this.j != null) {
            this.j.setVisibility(8);
        }
        if (this.k != null) {
            this.k.setVisibility(8);
        }
        if (this.d != null) {
            this.d.setVisibility(8);
        }
        if (this.i != null) {
            this.i.setVisibility(0);
        }
        if (this.e != null) {
            if (TextUtils.isEmpty(str)) {
                str = getResources().getString(R.string.tip_no_pc_name);
            }
            this.e.setText(getResources().getString(R.string.label_connection_with_pc_ongoing, str));
        }
        if (this.f != null) {
            if (TextUtils.isEmpty(str4)) {
                str4 = Constants.SOURCE_QQ;
            }
            this.f.setText(getResources().getString(R.string.label_connection_by, str4));
        }
        if (this.l != null) {
            this.l.setVisibility(0);
        }
        if (this.c != null) {
            String a2 = QQUtils.a(str5, QQUtils.Quality.Qua_40);
            this.c.updateWraper(R.drawable.banner_qq_status);
            this.c.updateSmallQQImage(a2);
            this.c.updateBigQQImage(a2);
        }
        if (this.g != null) {
            this.g.setVisibility(0);
            this.g.setOnClickListener(new i(this));
        }
        if (this.h != null) {
            this.h.setVisibility(0);
            this.h.setOnClickListener(new j(this));
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        if (this.j != null) {
            this.j.setVisibility(0);
        }
        if (this.k != null) {
            this.k.setVisibility(0);
        }
        if (this.i != null) {
            this.i.setVisibility(8);
        }
        if (this.g != null) {
            this.g.setVisibility(8);
        }
        if (this.h != null) {
            this.h.setVisibility(8);
        }
    }

    public void displayQrcodeRequest(String str, String str2, String str3) {
        if (this.j != null) {
            this.j.setVisibility(8);
        }
        if (this.k != null) {
            this.k.setVisibility(8);
        }
        if (this.l != null) {
            this.l.setVisibility(8);
        }
        if (this.d != null) {
            this.d.setVisibility(0);
        }
        if (this.i != null) {
            this.i.setVisibility(0);
        }
        if (this.e != null) {
            if (str == null) {
                str = getResources().getString(R.string.tip_no_pc_name);
            }
            this.e.setText(getResources().getString(R.string.label_connection_with_pc_ongoing, str));
        }
        if (this.f != null) {
            String string = getResources().getString(R.string.label_qrcode);
            this.f.setText(getResources().getString(R.string.label_connection_by, string));
        }
        if (this.g != null) {
            this.g.setVisibility(0);
            this.g.setOnClickListener(new k(this));
        }
        if (this.h != null) {
            this.h.setVisibility(0);
            this.h.setOnClickListener(new l(this));
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.j != null) {
            this.j.setVisibility(0);
        }
        if (this.k != null) {
            this.k.setVisibility(0);
        }
        if (this.i != null) {
            this.i.setVisibility(8);
        }
        if (this.g != null) {
            this.g.setVisibility(8);
        }
        if (this.h != null) {
            this.h.setVisibility(8);
        }
    }

    public void displayQQConnecting(String str, String str2, String str3) {
        if (this.j != null) {
            this.j.setVisibility(0);
        }
        if (this.k != null) {
            this.k.setVisibility(0);
        }
        if (this.d != null) {
            this.d.setVisibility(8);
        }
        if (this.i != null) {
            this.i.setVisibility(8);
        }
        if (this.g != null) {
            this.g.setVisibility(8);
        }
        if (this.h != null) {
            this.h.setVisibility(8);
        }
        if (this.e != null) {
            if (TextUtils.isEmpty(str)) {
                str = getResources().getString(R.string.tip_no_pc_name);
            }
            this.e.setText(getResources().getString(R.string.label_connection_with_pc_ongoing, str));
        }
        if (this.f != null) {
            if (TextUtils.isEmpty(str2)) {
                str2 = Constants.SOURCE_QQ;
            }
            this.f.setText(getResources().getString(R.string.label_connection_by, str2));
        }
        if (this.l != null) {
            this.l.setVisibility(0);
        }
        if (this.c != null) {
            this.c.updateWraper(R.drawable.banner_qq_status);
            this.c.updateSmallQQImage(str3);
            this.c.updateBigQQImage(str3);
        }
    }

    public void denyRequest() {
        s.a(1);
        p.m().c(704, 3, -1);
        a.a().b((byte) 8);
    }
}
