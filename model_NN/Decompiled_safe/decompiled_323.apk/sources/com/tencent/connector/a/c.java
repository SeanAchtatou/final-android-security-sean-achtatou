package com.tencent.connector.a;

import android.content.Context;
import android.os.Handler;
import com.qq.AppService.AppService;

/* compiled from: ProGuard */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private Context f2382a;
    /* access modifiers changed from: private */
    public Handler b;
    /* access modifiers changed from: private */
    public AppService.BusinessConnectionType c;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public Thread e;

    public c(Context context, Handler handler, AppService.BusinessConnectionType businessConnectionType) {
        this.f2382a = context;
        this.b = handler;
        this.c = businessConnectionType;
        if (businessConnectionType == AppService.BusinessConnectionType.QRCODE) {
            this.d = 30000;
        } else {
            this.d = 30000;
        }
    }

    public void a() {
        this.e = new d(this);
        this.e.start();
    }

    public void b() {
        if (this.e != null) {
            this.e.interrupt();
            this.e = null;
        }
        this.f2382a = null;
        this.b = null;
    }
}
