package com.tencent.f.a.a;

import com.tencent.d.a.f;
import java.io.ByteArrayInputStream;
import java.net.ProtocolException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Properties;

/* compiled from: ProGuard */
class b {

    /* renamed from: a  reason: collision with root package name */
    Properties f2531a;
    byte[] b;

    private b() {
        this.f2531a = new Properties();
    }

    /* synthetic */ b(b bVar) {
        this();
    }

    /* access modifiers changed from: package-private */
    public void a(byte[] bArr) {
        f.a("ApkExternalInfoTool", "enter");
        if (bArr == null) {
            f.a("ApkExternalInfoTool", "null == data");
            f.a("ApkExternalInfoTool", "exit");
            return;
        }
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        int length = a.b.a().length;
        byte[] bArr2 = new byte[length];
        wrap.get(bArr2);
        if (!a.b.equals(new d(bArr2))) {
            f.c("ApkExternalInfoTool", "unknow protocol");
            throw new ProtocolException("unknow protocl [" + Arrays.toString(bArr) + "]");
        } else if (bArr.length - length <= 2) {
            f.a("ApkExternalInfoTool", "data.length - headLength <= 2");
            f.a("ApkExternalInfoTool", "exit");
        } else {
            byte[] bArr3 = new byte[2];
            wrap.get(bArr3);
            int b2 = new d(bArr3).b();
            if ((bArr.length - length) - 2 < b2) {
                f.a("ApkExternalInfoTool", "data.length - headLength - 2 < len");
                f.a("ApkExternalInfoTool", "exit");
                return;
            }
            byte[] bArr4 = new byte[b2];
            wrap.get(bArr4);
            this.f2531a.load(new ByteArrayInputStream(bArr4));
            int length2 = ((bArr.length - length) - b2) - 2;
            if (length2 > 0) {
                this.b = new byte[length2];
                wrap.get(this.b);
            }
        }
    }

    public String toString() {
        return "ApkExternalInfo [p=" + this.f2531a + ", otherData=" + Arrays.toString(this.b) + "]";
    }
}
