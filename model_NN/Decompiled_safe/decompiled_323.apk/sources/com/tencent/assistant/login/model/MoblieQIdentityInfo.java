package com.tencent.assistant.login.model;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.protocol.jce.TicketWtLogin;
import com.tencent.nucleus.socialcontact.login.e;

/* compiled from: ProGuard */
public class MoblieQIdentityInfo extends e {
    private String d;
    private byte[] e;
    private byte[] f;
    private byte[] g;
    private String h;
    private String i;
    private String j;
    private long k;

    public MoblieQIdentityInfo(String str, byte[] bArr, byte[] bArr2, byte[] bArr3, String str2, long j2) {
        super(AppConst.IdentityType.MOBILEQ);
        this.d = str;
        this.e = bArr;
        this.f = bArr2;
        this.g = bArr3;
        this.h = str2;
        this.k = j2;
        getTicket();
    }

    /* access modifiers changed from: protected */
    public JceStruct a() {
        return new TicketWtLogin(this.f, this.g, this.k);
    }

    public byte[] getKey() {
        return this.e;
    }

    public long getUin() {
        return this.k;
    }

    public String getAccount() {
        return this.d;
    }

    public String getSKey() {
        return this.h;
    }

    public byte[] getGTKey_ST_A2() {
        return this.g;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof MoblieQIdentityInfo) || this.k != ((MoblieQIdentityInfo) obj).k) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (int) this.k;
    }

    public String getSid() {
        return this.i;
    }

    public void setSid(String str) {
        this.i = str;
    }

    public String getVkey() {
        return this.j;
    }

    public void setVkey(String str) {
        this.j = str;
    }
}
