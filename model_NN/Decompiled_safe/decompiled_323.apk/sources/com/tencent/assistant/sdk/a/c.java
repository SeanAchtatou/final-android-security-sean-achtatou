package com.tencent.assistant.sdk.a;

import android.os.IBinder;
import android.os.Parcel;

/* compiled from: ProGuard */
class c implements a {

    /* renamed from: a  reason: collision with root package name */
    private IBinder f1646a;

    c(IBinder iBinder) {
        this.f1646a = iBinder;
    }

    public IBinder asBinder() {
        return this.f1646a;
    }

    public int a(String str, String str2, d dVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.assistant.sdk.remote.BaseService");
            obtain.writeString(str);
            obtain.writeString(str2);
            obtain.writeStrongBinder(dVar != null ? dVar.asBinder() : null);
            this.f1646a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }

    public int a(d dVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.assistant.sdk.remote.BaseService");
            obtain.writeStrongBinder(dVar != null ? dVar.asBinder() : null);
            this.f1646a.transact(2, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }

    public byte[] a(String str, byte[] bArr) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.assistant.sdk.remote.BaseService");
            obtain.writeString(str);
            obtain.writeByteArray(bArr);
            this.f1646a.transact(3, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.createByteArray();
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }

    public void b(String str, byte[] bArr) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.assistant.sdk.remote.BaseService");
            obtain.writeString(str);
            obtain.writeByteArray(bArr);
            this.f1646a.transact(4, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }
}
