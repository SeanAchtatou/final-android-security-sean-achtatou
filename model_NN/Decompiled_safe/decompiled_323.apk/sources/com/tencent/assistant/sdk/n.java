package com.tencent.assistant.sdk;

/* compiled from: ProGuard */
public class n {
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00c3, code lost:
        r8.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00cc, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00d0, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00db, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00dc, code lost:
        r8 = r1;
        r1 = r0;
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        return r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00d0 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:17:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:56:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:58:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.tencent.pangu.download.DownloadInfo a(com.tencent.assistant.sdk.param.jce.IPCBaseParam r11) {
        /*
            r3 = 0
            r8 = 0
            if (r11 != 0) goto L_0x0006
            r0 = r8
        L_0x0005:
            return r0
        L_0x0006:
            com.tencent.pangu.manager.DownloadProxy r0 = com.tencent.pangu.manager.DownloadProxy.a()
            java.lang.String r1 = r11.d
            java.util.List r1 = r0.e(r1)
            if (r1 == 0) goto L_0x0047
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x0047
            java.lang.String r0 = r11.c
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0047
            java.lang.String r0 = r11.c
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            int r0 = r0.intValue()
            if (r0 <= 0) goto L_0x0047
            java.lang.Object r0 = r1.get(r3)
            com.tencent.pangu.download.DownloadInfo r0 = (com.tencent.pangu.download.DownloadInfo) r0
            int r0 = r0.versionCode
            java.lang.String r2 = r11.c
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            int r2 = r2.intValue()
            if (r0 != r2) goto L_0x0047
            java.lang.Object r0 = r1.get(r3)
            com.tencent.pangu.download.DownloadInfo r0 = (com.tencent.pangu.download.DownloadInfo) r0
            goto L_0x0005
        L_0x0047:
            com.tencent.assistant.db.table.l r9 = new com.tencent.assistant.db.table.l     // Catch:{ Exception -> 0x00bb, all -> 0x00c8 }
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()     // Catch:{ Exception -> 0x00bb, all -> 0x00c8 }
            r9.<init>(r0)     // Catch:{ Exception -> 0x00bb, all -> 0x00c8 }
            com.tencent.downloadsdk.storage.helper.SqliteHelper r0 = r9.e()     // Catch:{ Exception -> 0x00bb, all -> 0x00c8 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x00bb, all -> 0x00c8 }
            java.lang.String r1 = "downloadsinfo"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x00bb, all -> 0x00c8 }
            r3 = 0
            java.lang.String r4 = "downloadTicket"
            r2[r3] = r4     // Catch:{ Exception -> 0x00bb, all -> 0x00c8 }
            java.lang.String r3 = "packageName = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x00bb, all -> 0x00c8 }
            r5 = 0
            java.lang.String r6 = r11.d     // Catch:{ Exception -> 0x00bb, all -> 0x00c8 }
            r4[r5] = r6     // Catch:{ Exception -> 0x00bb, all -> 0x00c8 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x00bb, all -> 0x00c8 }
            if (r1 == 0) goto L_0x00e2
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            if (r0 == 0) goto L_0x00e2
            java.lang.String r0 = "downloadTicket"
            int r0 = r1.getColumnIndexOrThrow(r0)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            if (r2 != 0) goto L_0x00e2
            com.tencent.pangu.download.DownloadInfo r2 = r9.a(r0)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            java.lang.String r0 = r11.c     // Catch:{ Exception -> 0x00db, all -> 0x00d0 }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x00db, all -> 0x00d0 }
            if (r0 != 0) goto L_0x00b3
            java.lang.String r0 = r11.c     // Catch:{ Exception -> 0x00db, all -> 0x00d0 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x00db, all -> 0x00d0 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x00db, all -> 0x00d0 }
            if (r0 <= 0) goto L_0x00b3
            if (r2 == 0) goto L_0x00b3
            java.lang.String r0 = r11.c     // Catch:{ Exception -> 0x00db, all -> 0x00d0 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x00db, all -> 0x00d0 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x00db, all -> 0x00d0 }
            int r3 = r2.versionCode     // Catch:{ Exception -> 0x00db, all -> 0x00d0 }
            if (r0 == r3) goto L_0x00e0
        L_0x00b3:
            r0 = r8
        L_0x00b4:
            if (r1 == 0) goto L_0x0005
            r1.close()
            goto L_0x0005
        L_0x00bb:
            r0 = move-exception
            r1 = r0
            r0 = r8
        L_0x00be:
            r1.printStackTrace()     // Catch:{ all -> 0x00d2 }
            if (r8 == 0) goto L_0x0005
            r8.close()
            goto L_0x0005
        L_0x00c8:
            r0 = move-exception
            r1 = r8
        L_0x00ca:
            if (r1 == 0) goto L_0x00cf
            r1.close()
        L_0x00cf:
            throw r0
        L_0x00d0:
            r0 = move-exception
            goto L_0x00ca
        L_0x00d2:
            r0 = move-exception
            r1 = r8
            goto L_0x00ca
        L_0x00d5:
            r0 = move-exception
            r10 = r0
            r0 = r8
            r8 = r1
            r1 = r10
            goto L_0x00be
        L_0x00db:
            r0 = move-exception
            r8 = r1
            r1 = r0
            r0 = r2
            goto L_0x00be
        L_0x00e0:
            r0 = r2
            goto L_0x00b4
        L_0x00e2:
            r0 = r8
            goto L_0x00b4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.sdk.n.a(com.tencent.assistant.sdk.param.jce.IPCBaseParam):com.tencent.pangu.download.DownloadInfo");
    }
}
