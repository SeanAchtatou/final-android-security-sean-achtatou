package com.tencent.assistant.sdk;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.text.TextUtils;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.a.a;
import com.tencent.assistant.sdk.param.entity.BatchDownloadParam;
import com.tencent.assistant.sdk.param.jce.BatchDownloadActionRequest;
import com.tencent.assistant.sdk.param.jce.IPCDownloadParam;
import com.tencent.assistant.sdk.param.jce.IPCRequest;
import com.tencent.assistant.utils.e;
import com.tencent.pangu.link.b;
import com.tencent.pangu.manager.DownloadProxy;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class f extends r {
    private final String n = "SDKBatchOperateResolver";

    public f(Context context, IPCRequest iPCRequest, String str) {
        super(context, iPCRequest, str);
    }

    /* access modifiers changed from: protected */
    public void a(JceStruct jceStruct) {
        super.a(jceStruct);
        if (this.k == 1 || this.k == 2 || this.k == 4) {
            g();
        } else {
            h();
        }
    }

    private void g() {
        b.a(this.c, i(), a(this.l));
    }

    private void h() {
        if (this.l != null && this.l.b != null) {
            switch (this.k) {
                case 5:
                    DownloadProxy.a().n();
                    return;
                case 6:
                    DownloadProxy.a().m();
                    return;
                case 7:
                    ArrayList arrayList = new ArrayList();
                    Iterator<IPCDownloadParam> it = this.l.b.iterator();
                    while (it.hasNext()) {
                        IPCDownloadParam next = it.next();
                        if (next != null) {
                            arrayList.add(n.a(next.a()));
                        }
                    }
                    DownloadProxy.a().b(arrayList);
                    return;
                case 8:
                    ArrayList arrayList2 = new ArrayList();
                    Iterator<IPCDownloadParam> it2 = this.l.b.iterator();
                    while (it2.hasNext()) {
                        IPCDownloadParam next2 = it2.next();
                        if (next2 != null) {
                            arrayList2.add(n.a(next2.a()));
                        }
                    }
                    DownloadProxy.a().a(arrayList2);
                    return;
                default:
                    return;
            }
        }
    }

    private String i() {
        String j = j();
        return j + a(this.k);
    }

    private String j() {
        return "tpmast://" + "update" + "?";
    }

    private String a(int i) {
        String str;
        String str2 = a.n + "=" + this.f1655a.c + "&" + a.o + "=" + this.f1655a.d + "&" + a.ae + "=" + this.d;
        if (!TextUtils.isEmpty(this.h)) {
            str2 = str2 + "&" + a.y + "=" + this.h;
        }
        if (!TextUtils.isEmpty(this.i)) {
            str2 = str2 + "&" + a.z + "=" + this.i;
        }
        if (!TextUtils.isEmpty(this.j)) {
            str2 = str2 + "&" + a.j + "=" + this.j;
        }
        if (!TextUtils.isEmpty(this.h)) {
            str2 = str2 + "&" + a.y + "=" + this.h;
        }
        String str3 = str2 + "&" + a.T + "=" + 1;
        switch (i) {
            case 1:
                str = str3 + "&" + a.S + "=" + 1;
                break;
            case 2:
                str = str3 + "&" + a.S + "=" + 0;
                break;
            case 3:
            default:
                str = str3 + "&" + a.S + "=" + 0;
                break;
            case 4:
                str = str3 + "&" + a.S + "=" + 2;
                break;
        }
        return str + "&" + a.u + "=" + true;
    }

    private Bundle a(BatchDownloadActionRequest batchDownloadActionRequest) {
        Bundle bundle = new Bundle();
        if (!(this.k != 4 || batchDownloadActionRequest == null || batchDownloadActionRequest.b == null)) {
            ArrayList arrayList = new ArrayList();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= batchDownloadActionRequest.b.size()) {
                    break;
                }
                BatchDownloadParam batchDownloadParam = new BatchDownloadParam();
                if (batchDownloadActionRequest.b.get(i2).f1664a != null) {
                    batchDownloadParam.f1657a = batchDownloadActionRequest.b.get(i2).f1664a.f1662a;
                    batchDownloadParam.b = batchDownloadActionRequest.b.get(i2).f1664a.b;
                    batchDownloadParam.c = batchDownloadActionRequest.b.get(i2).f1664a.c;
                    batchDownloadParam.d = batchDownloadActionRequest.b.get(i2).f1664a.d;
                    batchDownloadParam.e = com.tencent.pangu.b.a.a.a(batchDownloadActionRequest.b.get(i2).f1664a.e);
                    batchDownloadParam.f = batchDownloadActionRequest.b.get(i2).f1664a.f;
                    batchDownloadParam.g = batchDownloadActionRequest.b.get(i2).f1664a.g;
                    batchDownloadParam.h = batchDownloadActionRequest.b.get(i2).f1664a.h;
                }
                batchDownloadParam.i = batchDownloadActionRequest.b.get(i2).b;
                batchDownloadParam.j = batchDownloadActionRequest.b.get(i2).c;
                batchDownloadParam.k = batchDownloadActionRequest.b.get(i2).d;
                batchDownloadParam.l = batchDownloadActionRequest.b.get(i2).e;
                arrayList.add(batchDownloadParam);
                i = i2 + 1;
            }
            bundle.putParcelableArrayList(a.Q, a(b(arrayList)));
        }
        return bundle;
    }

    private ArrayList<BatchDownloadParam> a(ArrayList<BatchDownloadParam> arrayList) {
        ArrayList<BatchDownloadParam> arrayList2 = new ArrayList<>();
        if (arrayList == null || arrayList.size() <= 0) {
            return arrayList2;
        }
        for (int i = 0; i < arrayList.size(); i++) {
            BatchDownloadParam batchDownloadParam = arrayList.get(i);
            if (batchDownloadParam != null && !TextUtils.isEmpty(batchDownloadParam.d)) {
                int i2 = 0;
                while (i2 < arrayList2.size() && (arrayList2.get(i2) == null || TextUtils.isEmpty(arrayList2.get(i2).d) || !batchDownloadParam.d.equals(arrayList2.get(i2).d))) {
                    i2++;
                }
                if (i2 >= arrayList2.size()) {
                    arrayList2.add(batchDownloadParam);
                }
            }
        }
        return arrayList2;
    }

    private ArrayList<BatchDownloadParam> b(ArrayList<BatchDownloadParam> arrayList) {
        ArrayList<BatchDownloadParam> arrayList2 = new ArrayList<>();
        if (arrayList == null || arrayList.size() <= 0) {
            return arrayList2;
        }
        List<PackageInfo> c = e.c(this.c);
        for (int i = 0; i < arrayList.size(); i++) {
            BatchDownloadParam batchDownloadParam = arrayList.get(i);
            if (batchDownloadParam != null && !TextUtils.isEmpty(batchDownloadParam.d)) {
                int i2 = 0;
                while (true) {
                    if (i2 < (c == null ? 0 : c.size())) {
                        if (c.get(i2) != null && !TextUtils.isEmpty(c.get(i2).packageName) && batchDownloadParam.d.equals(c.get(i2).packageName)) {
                            batchDownloadParam.c = String.valueOf(c.get(i2).versionCode);
                            break;
                        }
                        i2++;
                    } else {
                        break;
                    }
                }
                if (c != null && i2 >= 0 && i2 < c.size()) {
                    arrayList2.add(batchDownloadParam);
                }
            }
        }
        return arrayList2;
    }
}
