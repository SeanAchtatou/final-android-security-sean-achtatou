package com.tencent.assistant.plugin;

import com.tencent.assistant.module.callback.CallbackHelper;

/* compiled from: ProGuard */
class b implements CallbackHelper.Caller<GetPluginListCallback> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GetPluginListEngine f1086a;

    b(GetPluginListEngine getPluginListEngine) {
        this.f1086a = getPluginListEngine;
    }

    /* renamed from: a */
    public void call(GetPluginListCallback getPluginListCallback) {
        getPluginListCallback.success(this.f1086a.e);
    }
}
