package com.tencent.assistant.plugin.mgr;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.tencent.assistant.plugin.PluginApplication;

/* compiled from: ProGuard */
public class PluginConnectNotificationService extends Service {
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        switch (intent.getIntExtra("notification_type", 0)) {
            case 1:
                i.b().a();
                PluginApplication.getInstance().initConnectApplication();
                break;
        }
        stopSelf();
    }
}
