package com.tencent.assistant.plugin.mgr;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.plugin.GetPluginListCallback;
import com.tencent.assistant.plugin.GetPluginListEngine;
import com.tencent.assistant.plugin.PluginDownloadInfo;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.download.a;
import com.tencent.pangu.manager.DownloadProxy;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: ProGuard */
public class c implements UIEventListener, NetworkMonitor.ConnectivityChangeListener, GetPluginListCallback {

    /* renamed from: a  reason: collision with root package name */
    private static c f1097a;
    private static HashSet<String> e = new HashSet<>(1);
    private static Handler f = null;
    private AstApp b;
    private Set<String> c = null;
    private Object d = new Object();

    static {
        e.add("com.tencent.assistant.freewifi");
    }

    public static synchronized c a() {
        c cVar;
        synchronized (c.class) {
            if (f1097a == null) {
                f1097a = new c();
            }
            cVar = f1097a;
        }
        return cVar;
    }

    private c() {
        b();
    }

    private void b() {
        this.b = AstApp.i();
        c();
        GetPluginListEngine.getInstance().register(this);
        t.a().a(this);
    }

    private void c() {
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_QUEUING, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_DOWNLOADING, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_PAUSE, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_SUCC, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_FAIL, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL_AND_RETRY, this);
    }

    public DownloadInfo a(PluginDownloadInfo pluginDownloadInfo) {
        return a(pluginDownloadInfo, SimpleDownloadInfo.UIType.NORMAL);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, android.app.Dialog):android.app.Dialog
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.assistant.net.APN):com.tencent.assistant.net.APN
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$DownloadState):void
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, int):com.tencent.pangu.download.DownloadInfo
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, boolean):void */
    public DownloadInfo b(PluginDownloadInfo pluginDownloadInfo) {
        DownloadInfo c2 = DownloadProxy.a().c(pluginDownloadInfo.getDownloadTicket());
        if (c2 == null) {
            return null;
        }
        if (c2.versionCode == pluginDownloadInfo.version) {
            return c2;
        }
        DownloadProxy.a().a(pluginDownloadInfo.getDownloadTicket(), false);
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, android.app.Dialog):android.app.Dialog
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.assistant.net.APN):com.tencent.assistant.net.APN
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$DownloadState):void
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, int):com.tencent.pangu.download.DownloadInfo
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, boolean):void */
    public DownloadInfo a(PluginDownloadInfo pluginDownloadInfo, SimpleDownloadInfo.UIType uIType) {
        DownloadInfo d2 = DownloadProxy.a().d(pluginDownloadInfo.getDownloadTicket());
        if (d2 == null) {
            d2 = DownloadInfo.createDownloadInfo(pluginDownloadInfo);
        } else if (d2.versionCode != pluginDownloadInfo.version) {
            DownloadProxy.a().a(pluginDownloadInfo.getDownloadTicket(), false);
            d2 = DownloadInfo.createDownloadInfo(pluginDownloadInfo);
        }
        d2.uiType = uIType;
        DownloadProxy.a().d(d2);
        return d2;
    }

    public void c(PluginDownloadInfo pluginDownloadInfo) {
        DownloadInfo a2 = a(pluginDownloadInfo);
        if (a2.downloadState != SimpleDownloadInfo.DownloadState.DOWNLOADING) {
            DownloadProxy.a().c(a2);
        }
    }

    public void d(PluginDownloadInfo pluginDownloadInfo) {
        if (pluginDownloadInfo != null && !TextUtils.isEmpty(pluginDownloadInfo.downUrl)) {
            STInfoV2 sTInfoV2 = new STInfoV2(STConst.ST_PAGE_PLUGIN, STConst.ST_DEFAULT_SLOT, STConst.ST_PAGE_PLUGIN, STConst.ST_DEFAULT_SLOT, 200);
            sTInfoV2.extraData = pluginDownloadInfo.pluginId + "|" + pluginDownloadInfo.pluginPackageName + "|" + pluginDownloadInfo.startActivity + "|" + 3;
            l.a(sTInfoV2);
            DownloadProxy.a().c(a(pluginDownloadInfo, SimpleDownloadInfo.UIType.PLUGIN_PREDOWNLOAD));
        }
    }

    public void e(PluginDownloadInfo pluginDownloadInfo) {
        DownloadInfo d2 = DownloadProxy.a().d(pluginDownloadInfo.getDownloadTicket());
        if (d2 != null) {
            a.a().b(d2.downloadTicket);
        }
    }

    public void handleUIEvent(Message message) {
        PluginDownloadInfo plugin;
        String str = Constants.STR_EMPTY;
        if (message.obj instanceof String) {
            str = (String) message.obj;
        }
        if (!TextUtils.isEmpty(str)) {
            switch (message.what) {
                case EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_DOWNLOADING:
                case EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_PAUSE:
                case EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_QUEUING:
                default:
                    return;
                case EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_SUCC:
                    DownloadInfo c2 = DownloadProxy.a().c(str);
                    if (c2 != null && (plugin = GetPluginListEngine.getInstance().getPlugin(str)) != null) {
                        TemporaryThreadManager.get().start(new d(this, c2, plugin));
                        return;
                    }
                    return;
                case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC:
                case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL:
                    DownloadProxy.a().b(str);
                    return;
                case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL_AND_RETRY:
                    if (!a.a().a(str)) {
                        DownloadProxy.a().b(str);
                        this.b.j().sendMessage(this.b.j().obtainMessage(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FINAL_FAIL, str));
                        return;
                    }
                    return;
            }
        }
    }

    public void start() {
    }

    public void success(List<PluginDownloadInfo> list) {
        a(list);
        d().postDelayed(new e(this, list), 10000);
    }

    private void c(String str) {
        synchronized (this.d) {
            if (this.c == null) {
                this.c = new HashSet(1);
            }
            this.c.add(str);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.util.List<com.tencent.assistant.plugin.PluginDownloadInfo> r6) {
        /*
            r5 = this;
            java.lang.Object r1 = r5.d
            monitor-enter(r1)
            java.util.Set<java.lang.String> r0 = r5.c     // Catch:{ all -> 0x002f }
            if (r0 == 0) goto L_0x000f
            if (r6 == 0) goto L_0x000f
            boolean r0 = r6.isEmpty()     // Catch:{ all -> 0x002f }
            if (r0 == 0) goto L_0x0011
        L_0x000f:
            monitor-exit(r1)     // Catch:{ all -> 0x002f }
        L_0x0010:
            return
        L_0x0011:
            java.util.Iterator r2 = r6.iterator()     // Catch:{ all -> 0x002f }
        L_0x0015:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x002f }
            if (r0 == 0) goto L_0x0032
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x002f }
            com.tencent.assistant.plugin.PluginDownloadInfo r0 = (com.tencent.assistant.plugin.PluginDownloadInfo) r0     // Catch:{ all -> 0x002f }
            java.util.Set<java.lang.String> r3 = r5.c     // Catch:{ all -> 0x002f }
            java.lang.String r4 = r0.pluginPackageName     // Catch:{ all -> 0x002f }
            boolean r3 = r3.contains(r4)     // Catch:{ all -> 0x002f }
            if (r3 == 0) goto L_0x0015
            r5.c(r0)     // Catch:{ all -> 0x002f }
            goto L_0x0015
        L_0x002f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x002f }
            throw r0
        L_0x0032:
            r0 = 0
            r5.c = r0     // Catch:{ all -> 0x002f }
            monitor-exit(r1)     // Catch:{ all -> 0x002f }
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.plugin.mgr.c.a(java.util.List):void");
    }

    public PluginDownloadInfo a(String str) {
        return GetPluginListEngine.getInstance().getPluginByPackageName(str);
    }

    public PluginDownloadInfo a(int i) {
        return GetPluginListEngine.getInstance().getPlugin(i);
    }

    public void b(String str) {
        c(str);
        GetPluginListEngine.getInstance().refreshData(0);
    }

    /* access modifiers changed from: private */
    public synchronized void b(List<PluginDownloadInfo> list) {
        if (list != null) {
            for (PluginDownloadInfo next : list) {
                if (f(next)) {
                    d(next);
                }
            }
        }
    }

    private boolean f(PluginDownloadInfo pluginDownloadInfo) {
        if (pluginDownloadInfo.needPreDownload == 0) {
            return false;
        }
        PluginInfo a2 = i.b().a(pluginDownloadInfo.pluginPackageName);
        if (e != null && e.contains("com.tencent.assistant.freewifi") && a2 != null && a2.getVersion() < pluginDownloadInfo.version) {
            XLog.i("PluginDownloadManager", "[PluginDownloadmanager] ---> canPreDownloadPlugin pluginPackageName = com.tencent.assistant.freewifi");
            return true;
        } else if ((a2 != null && (a2.getVersion() >= pluginDownloadInfo.version || h.a(pluginDownloadInfo.pluginPackageName) >= 0)) || !a.a(pluginDownloadInfo.pluginPackageName, pluginDownloadInfo.version)) {
            return false;
        } else {
            DownloadInfo c2 = DownloadProxy.a().c(pluginDownloadInfo.getDownloadTicket());
            if (c2 != null && (c2.uiType == SimpleDownloadInfo.UIType.NORMAL || c2.downloadState == SimpleDownloadInfo.DownloadState.DOWNLOADING || c2.downloadState == SimpleDownloadInfo.DownloadState.SUCC || c2.downloadState == SimpleDownloadInfo.DownloadState.QUEUING)) {
                return false;
            }
            int i = pluginDownloadInfo.needPreDownload;
            if (i >= 1 && com.tencent.assistant.net.c.e() && !com.tencent.assistant.net.c.l()) {
                return true;
            }
            if (i >= 2 && com.tencent.assistant.net.c.g()) {
                return true;
            }
            if (i < 3 || !com.tencent.assistant.net.c.f()) {
                return false;
            }
            return true;
        }
    }

    public void failed(int i) {
        this.b.j().sendMessage(this.b.j().obtainMessage(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLIST_FAIL));
    }

    public void onConnected(APN apn) {
        d().postDelayed(new f(this), 1000);
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        d().postDelayed(new g(this), 1000);
    }

    private static synchronized Handler d() {
        Handler handler;
        synchronized (c.class) {
            if (f == null) {
                f = ah.a("plugin_down_delay");
            }
            handler = f;
        }
        return handler;
    }
}
