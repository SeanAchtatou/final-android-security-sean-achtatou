package com.tencent.assistant.plugin;

import android.graphics.drawable.Drawable;
import java.io.Serializable;

/* compiled from: ProGuard */
public class PluginStartEntry implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private int f1072a;
    private String b;
    private String c;
    private int d;
    private String e;
    private String f;
    private transient Drawable g;

    public PluginStartEntry(int i, String str, String str2, int i2, String str3, String str4) {
        this.f1072a = i;
        this.b = str;
        this.c = str2;
        this.d = i2;
        this.e = str3;
        this.f = str4;
    }

    public PluginStartEntry(int i, String str, String str2, int i2, String str3, Drawable drawable) {
        this.f1072a = i;
        this.b = str;
        this.c = str2;
        this.d = i2;
        this.e = str3;
        this.g = drawable;
    }

    public String getName() {
        return this.b;
    }

    public void setName(String str) {
        this.b = str;
    }

    public String getPackageName() {
        return this.c;
    }

    public void setPackageName(String str) {
        this.c = str;
    }

    public int getVersionCode() {
        return this.d;
    }

    public void setVersionCode(int i) {
        this.d = i;
    }

    public String getStartActivity() {
        return this.e;
    }

    public void setStartActivity(String str) {
        this.e = str;
    }

    public String getIconUrl() {
        return this.f;
    }

    public void setIconUrl(String str) {
        this.f = str;
    }

    public int getPluginId() {
        return this.f1072a;
    }

    public void setPluginId(int i) {
        this.f1072a = i;
    }

    public Drawable getIconDrawable() {
        return this.g;
    }

    public void setIconDrawable(Drawable drawable) {
        this.g = drawable;
    }
}
