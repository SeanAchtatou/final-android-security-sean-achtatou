package com.tencent.assistant.localres;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import java.lang.ref.WeakReference;
import java.util.Iterator;

/* compiled from: ProGuard */
class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f828a;
    final /* synthetic */ boolean b;
    final /* synthetic */ ApkResourceManager c;

    i(ApkResourceManager apkResourceManager, String str, boolean z) {
        this.c = apkResourceManager;
        this.f828a = str;
        this.b = z;
    }

    public void run() {
        LocalApkInfo localApkInfo = (LocalApkInfo) this.c.h.get(this.f828a);
        localApkInfo.mIsEnabled = this.b;
        Iterator it = this.c.e.iterator();
        while (it.hasNext()) {
            ApkResCallback apkResCallback = (ApkResCallback) ((WeakReference) it.next()).get();
            if (apkResCallback != null) {
                apkResCallback.onInstalledApkDataChanged(localApkInfo, 5);
            }
        }
    }
}
