package com.tencent.assistant.localres;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import com.tencent.assistant.localres.model.LocalVideo;
import com.tencent.assistant.utils.av;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class LocalVideoLoader extends LocalMediaLoader<LocalVideo> {
    public LocalVideoLoader(Context context) {
        super(context);
        this.c = 2;
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b.addAll(a(MediaStore.Video.Media.EXTERNAL_CONTENT_URI));
        this.b.addAll(a(MediaStore.Video.Media.INTERNAL_CONTENT_URI));
        a(this, this.b, true);
    }

    private ArrayList<LocalVideo> a(Uri uri) {
        ArrayList<LocalVideo> arrayList = new ArrayList<>();
        Cursor query = this.f804a.getContentResolver().query(uri, null, "duration> 0", null, "date_modified DESC");
        if (query == null || !query.moveToFirst()) {
            if (query != null) {
                query.close();
            }
            return arrayList;
        }
        int columnIndexOrThrow = query.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = query.getColumnIndexOrThrow("_data");
        int columnIndexOrThrow3 = query.getColumnIndexOrThrow("title");
        int columnIndexOrThrow4 = query.getColumnIndexOrThrow("_display_name");
        int columnIndexOrThrow5 = query.getColumnIndexOrThrow("_size");
        int columnIndexOrThrow6 = query.getColumnIndexOrThrow("date_added");
        int columnIndexOrThrow7 = query.getColumnIndexOrThrow("date_modified");
        int columnIndexOrThrow8 = query.getColumnIndexOrThrow("mime_type");
        int columnIndexOrThrow9 = query.getColumnIndexOrThrow("duration");
        int columnIndexOrThrow10 = query.getColumnIndexOrThrow("artist");
        int columnIndexOrThrow11 = query.getColumnIndexOrThrow("album");
        int columnIndexOrThrow12 = query.getColumnIndexOrThrow("resolution");
        int columnIndexOrThrow13 = query.getColumnIndexOrThrow(SocialConstants.PARAM_COMMENT);
        int columnIndexOrThrow14 = query.getColumnIndexOrThrow("isprivate");
        int columnIndexOrThrow15 = query.getColumnIndexOrThrow("tags");
        int columnIndexOrThrow16 = query.getColumnIndexOrThrow("category");
        int columnIndexOrThrow17 = query.getColumnIndexOrThrow("language");
        int columnIndexOrThrow18 = query.getColumnIndexOrThrow("latitude");
        int columnIndexOrThrow19 = query.getColumnIndexOrThrow("longitude");
        int columnIndexOrThrow20 = query.getColumnIndexOrThrow("datetaken");
        do {
            LocalVideo localVideo = new LocalVideo();
            localVideo.id = query.getInt(columnIndexOrThrow);
            localVideo.path = query.getString(columnIndexOrThrow2);
            localVideo.name = query.getString(columnIndexOrThrow3);
            localVideo.description = query.getString(columnIndexOrThrow4);
            localVideo.size = query.getLong(columnIndexOrThrow5);
            localVideo.softKey = av.a(localVideo.name);
            localVideo.createDate = query.getLong(columnIndexOrThrow6);
            localVideo.updateDate = query.getLong(columnIndexOrThrow7);
            localVideo.mimeType = query.getString(columnIndexOrThrow8);
            localVideo.duration = query.getInt(columnIndexOrThrow9);
            localVideo.artist = query.getString(columnIndexOrThrow10);
            localVideo.album = query.getString(columnIndexOrThrow11);
            localVideo.resolution = query.getString(columnIndexOrThrow12);
            localVideo.description = query.getString(columnIndexOrThrow13);
            localVideo.privateFlag = query.getInt(columnIndexOrThrow14) == 1;
            localVideo.tags = query.getString(columnIndexOrThrow15);
            localVideo.category = query.getString(columnIndexOrThrow16);
            localVideo.language = query.getString(columnIndexOrThrow17);
            localVideo.latitude = query.getDouble(columnIndexOrThrow18);
            localVideo.longitude = query.getDouble(columnIndexOrThrow19);
            localVideo.datetaken = query.getInt(columnIndexOrThrow20);
            arrayList.add(localVideo);
        } while (query.moveToNext());
        query.close();
        return arrayList;
    }
}
