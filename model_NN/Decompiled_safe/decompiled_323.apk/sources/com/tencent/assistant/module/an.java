package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.j;
import com.tencent.assistant.protocol.jce.GetUserTagInfoListRequest;
import com.tencent.assistant.protocol.jce.GetUserTagInfoListResponse;

/* compiled from: ProGuard */
public class an extends BaseEngine<j> {
    public int a(int i, String str) {
        return send(new GetUserTagInfoListRequest(i, str));
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new ao(this, i, (GetUserTagInfoListResponse) jceStruct2));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        GetUserTagInfoListResponse getUserTagInfoListResponse = (GetUserTagInfoListResponse) jceStruct2;
        notifyDataChangedInMainThread(new ap(this, i, i2));
    }
}
