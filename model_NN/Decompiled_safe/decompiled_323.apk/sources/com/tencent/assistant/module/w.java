package com.tencent.assistant.module;

import com.tencent.assistant.manager.webview.js.j;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.f;
import com.tencent.assistant.protocol.jce.GetDomainCapabilityResponse;

/* compiled from: ProGuard */
class w implements CallbackHelper.Caller<f> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GetDomainCapabilityResponse f1052a;
    final /* synthetic */ int b;
    final /* synthetic */ v c;

    w(v vVar, GetDomainCapabilityResponse getDomainCapabilityResponse, int i) {
        this.c = vVar;
        this.f1052a = getDomainCapabilityResponse;
        this.b = i;
    }

    /* renamed from: a */
    public void call(f fVar) {
        j jVar = null;
        if (this.f1052a != null && this.f1052a.f1281a == 0) {
            jVar = new j(this.f1052a.c, this.f1052a.b);
        }
        if (jVar != null) {
            fVar.onGetDomainCapabilitySuccess(this.b, 0, jVar);
        }
    }
}
