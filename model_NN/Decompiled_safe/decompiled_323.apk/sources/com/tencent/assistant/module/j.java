package com.tencent.assistant.module;

import com.tencent.assistant.manager.i;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.GetAppListResponse;
import java.util.ArrayList;

/* compiled from: ProGuard */
class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ i f996a;

    j(i iVar) {
        this.f996a = iVar;
    }

    public void run() {
        boolean z = true;
        GetAppListResponse a2 = i.y().a(this.f996a.f995a.f975a, this.f996a.f995a.b, this.f996a.c);
        if (a2 == null || a2.e != this.f996a.f995a.f || a2.b == null || a2.b.size() <= 0) {
            int unused = this.f996a.h = this.f996a.f995a.a(this.f996a.h, this.f996a.c);
            return;
        }
        i iVar = this.f996a;
        long j = a2.e;
        ArrayList<SimpleAppModel> b = k.b(a2.b);
        if (a2.d != 1) {
            z = false;
        }
        iVar.a(j, b, z, a2.c);
    }
}
