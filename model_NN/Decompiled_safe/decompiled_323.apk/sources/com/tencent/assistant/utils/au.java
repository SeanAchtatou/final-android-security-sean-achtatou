package com.tencent.assistant.utils;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import java.io.File;

/* compiled from: ProGuard */
public class au {
    public static Intent a(String str) {
        File file = new File(str);
        if (!file.exists()) {
            return null;
        }
        String lowerCase = file.getName().substring(file.getName().lastIndexOf(".") + 1, file.getName().length()).toLowerCase();
        if (lowerCase.equals("m4a") || lowerCase.equals("mp3") || lowerCase.equals("mid") || lowerCase.equals("xmf") || lowerCase.equals("ogg") || lowerCase.equals("wav")) {
            return d(str);
        }
        if (lowerCase.equals("3gp") || lowerCase.equals("mp4")) {
            return d(str);
        }
        if (lowerCase.equals("jpg") || lowerCase.equals("gif") || lowerCase.equals("png") || lowerCase.equals("jpeg") || lowerCase.equals("bmp")) {
            return e(str);
        }
        if (lowerCase.equals("apk")) {
            return c(str);
        }
        if (lowerCase.equals("ppt")) {
            return f(str);
        }
        if (lowerCase.equals("xls")) {
            return g(str);
        }
        if (lowerCase.equals("doc")) {
            return h(str);
        }
        if (lowerCase.equals("pdf")) {
            return j(str);
        }
        if (lowerCase.equals("chm")) {
            return i(str);
        }
        if (lowerCase.equals("txt")) {
            return a(str, false);
        }
        return b(str);
    }

    public static Intent b(String str) {
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(str)), "*/*");
        return intent;
    }

    public static Intent c(String str) {
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(str)), "application/vnd.android.package-archive");
        return intent;
    }

    public static Intent d(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(67108864);
        intent.putExtra("oneshot", 0);
        intent.putExtra("configchange", 0);
        intent.setDataAndType(Uri.fromFile(new File(str)), "audio/*");
        return intent;
    }

    public static Intent e(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.fromFile(new File(str)), "image/*");
        return intent;
    }

    public static Intent f(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.fromFile(new File(str)), "application/vnd.ms-powerpoint");
        return intent;
    }

    public static Intent g(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.fromFile(new File(str)), "application/vnd.ms-excel");
        return intent;
    }

    public static Intent h(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.fromFile(new File(str)), "application/msword");
        return intent;
    }

    public static Intent i(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.fromFile(new File(str)), "application/x-chm");
        return intent;
    }

    public static Intent a(String str, boolean z) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(268435456);
        if (z) {
            intent.setDataAndType(Uri.parse(str), "text/plain");
        } else {
            intent.setDataAndType(Uri.fromFile(new File(str)), "text/plain");
        }
        return intent;
    }

    public static Intent j(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.fromFile(new File(str)), "application/pdf");
        return intent;
    }

    public static Uri a(ContentResolver contentResolver, String str, String str2, String str3) {
        ContentValues contentValues = new ContentValues();
        if (str2 != null) {
            contentValues.put("title", str2);
        } else {
            contentValues.put("title", str);
        }
        long currentTimeMillis = System.currentTimeMillis();
        contentValues.put("date_added", Long.valueOf(currentTimeMillis));
        contentValues.put("datetaken", Long.valueOf(currentTimeMillis));
        contentValues.put("mime_type", str3);
        File file = new File(str);
        contentValues.put("_size", Long.valueOf(file.length()));
        contentValues.put("date_modified", Long.valueOf(file.lastModified()));
        contentValues.put("_data", str);
        try {
            return contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }
}
