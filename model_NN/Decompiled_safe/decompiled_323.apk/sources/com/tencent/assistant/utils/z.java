package com.tencent.assistant.utils;

import android.app.Dialog;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.dialog.DialogUtils;

/* compiled from: ProGuard */
final class z implements Runnable {
    z() {
    }

    public void run() {
        BaseActivity m = AstApp.m();
        if (m != null && AstApp.i().l()) {
            if (FunctionUtils.f1802a == null || !FunctionUtils.f1802a.isShowing()) {
                aa aaVar = new aa(this);
                aaVar.titleRes = m.getString(R.string.down_url_error_dialog_title);
                aaVar.contentRes = m.getString(R.string.down_url_error_dialog_message);
                aaVar.btnTxtRes = m.getString(R.string.ver_low_tips_ok);
                Dialog unused = FunctionUtils.f1802a = DialogUtils.show1BtnDialog(aaVar);
            }
        }
    }
}
