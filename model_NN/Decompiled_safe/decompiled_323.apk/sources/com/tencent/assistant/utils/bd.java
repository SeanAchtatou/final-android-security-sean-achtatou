package com.tencent.assistant.utils;

import android.content.Context;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class bd {
    public static int a(Context context, int i) {
        return by.a(context, 100.0f) * i;
    }

    public static String b(Context context, int i) {
        if (context == null || i < 10) {
            return null;
        }
        return context.getResources().getString(R.string.refresh_list_loading_loadfinish_format, Integer.valueOf(a(context, i)), Integer.valueOf(i));
    }
}
