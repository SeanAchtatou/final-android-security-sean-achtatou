package com.tencent.assistant.utils;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.text.TextUtils;
import com.tencent.assistant.Global;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: ProGuard */
public class ah {

    /* renamed from: a  reason: collision with root package name */
    private static Handler f1818a;
    private static Object b = new Object();
    private static final AtomicInteger c = new AtomicInteger(0);
    private static Map<String, Handler> d = Collections.synchronizedMap(new HashMap());

    public static Handler a() {
        if (f1818a == null) {
            synchronized (b) {
                if (f1818a == null) {
                    f1818a = new Handler(Looper.getMainLooper());
                }
            }
        }
        return f1818a;
    }

    public static Handler a(String str) {
        if (TextUtils.isEmpty(str)) {
            str = "default-thread";
        }
        XLog.d("HandlerUtils", "getHandler(" + str + ") exists at cache:" + d.containsKey(str));
        if (d.containsKey(str)) {
            Handler handler = d.get(str);
            if (!Global.isDev()) {
                return handler;
            }
            handler.dump(new ap(3, "HandlerUtils"), "getHandler(" + str + ")");
            return handler;
        }
        try {
            HandlerThread handlerThread = new HandlerThread(str);
            handlerThread.start();
            Looper looper = handlerThread.getLooper();
            if (looper != null) {
                Handler handler2 = new Handler(looper);
                try {
                    d.put(str, handler2);
                    return handler2;
                } catch (StackOverflowError e) {
                    return handler2;
                }
            } else {
                handlerThread.quit();
                return null;
            }
        } catch (StackOverflowError e2) {
            return null;
        }
    }
}
