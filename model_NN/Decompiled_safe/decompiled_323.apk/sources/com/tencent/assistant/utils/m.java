package com.tencent.assistant.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.qq.AppService.AstApp;
import com.tencent.assistant.manager.t;

/* compiled from: ProGuard */
public class m {
    public static BitmapFactory.Options a() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inSampleSize = 1;
        try {
            BitmapFactory.Options.class.getField("inNativeAlloc").setBoolean(options, true);
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException e) {
        }
        return options;
    }

    public static Bitmap a(byte[] bArr, int i, int i2) {
        Bitmap decodeByteArray;
        Bitmap createScaledBitmap;
        if (bArr == null) {
            return null;
        }
        BitmapFactory.Options a2 = a();
        BitmapFactory.decodeByteArray(bArr, 0, bArr.length + 0, a2);
        int i3 = a2.outWidth;
        int i4 = a2.outHeight;
        if (i == 0) {
            i = a2.outWidth;
        }
        if (i2 == 0) {
            i2 = a2.outHeight;
        }
        if (a2.outHeight <= 0 || a2.outWidth <= 0) {
            return null;
        }
        BitmapFactory.Options a3 = a(a2, i, i2);
        try {
            decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length + 0, a3);
        } catch (OutOfMemoryError e) {
            t.a().b();
            a3.inSampleSize++;
            decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length + 0, a3);
        }
        if (decodeByteArray == null) {
            return null;
        }
        float f = t.d;
        if (f <= 0.0f || f - 2.0f >= 0.0f) {
            return decodeByteArray;
        }
        int i5 = (int) ((((float) i4) * f) / 2.0f);
        int i6 = (int) ((f * ((float) i3)) / 2.0f);
        try {
            createScaledBitmap = Bitmap.createScaledBitmap(decodeByteArray, i6, i5, true);
        } catch (Throwable th) {
            t.a().b();
            createScaledBitmap = Bitmap.createScaledBitmap(decodeByteArray, i6, i5, true);
        }
        if (createScaledBitmap == decodeByteArray) {
            return createScaledBitmap;
        }
        decodeByteArray.recycle();
        return createScaledBitmap;
    }

    public static Bitmap a(int i) {
        Bitmap decodeResource;
        Bitmap createScaledBitmap;
        if (i <= 0) {
            return null;
        }
        BitmapFactory.Options a2 = a();
        BitmapFactory.decodeResource(AstApp.i().getResources(), i, a2);
        int i2 = a2.outWidth;
        int i3 = a2.outHeight;
        if (a2.outHeight <= 0 || a2.outWidth <= 0) {
            return null;
        }
        BitmapFactory.Options a3 = a(a2, a2.outWidth, a2.outHeight);
        try {
            decodeResource = BitmapFactory.decodeResource(AstApp.i().getResources(), i, a3);
        } catch (OutOfMemoryError e) {
            t.a().b();
            a3.inSampleSize++;
            decodeResource = BitmapFactory.decodeResource(AstApp.i().getResources(), i, a3);
        }
        if (decodeResource == null) {
            return null;
        }
        float f = t.d;
        if (f <= 0.0f || f >= 2.0f) {
            return decodeResource;
        }
        int i4 = (int) ((((float) i3) * f) / 2.0f);
        int i5 = (int) ((f * ((float) i2)) / 2.0f);
        try {
            createScaledBitmap = Bitmap.createScaledBitmap(decodeResource, i5, i4, true);
        } catch (Throwable th) {
            t.a().b();
            createScaledBitmap = Bitmap.createScaledBitmap(decodeResource, i5, i4, true);
        }
        if (createScaledBitmap == decodeResource) {
            return createScaledBitmap;
        }
        decodeResource.recycle();
        return createScaledBitmap;
    }

    public static BitmapFactory.Options a(BitmapFactory.Options options, int i, int i2) {
        return a(options, i, i2, false);
    }

    private static BitmapFactory.Options a(BitmapFactory.Options options, int i, int i2, boolean z) {
        options.inJustDecodeBounds = false;
        options.inSampleSize = 2;
        if (z) {
            while (true) {
                if (options.outWidth / options.inSampleSize < i && options.outHeight / options.inSampleSize < i2) {
                    break;
                }
                options.inSampleSize++;
            }
        } else {
            while (options.outWidth / options.inSampleSize >= i && options.outHeight / options.inSampleSize >= i2) {
                options.inSampleSize++;
            }
        }
        options.inSampleSize--;
        return options;
    }
}
