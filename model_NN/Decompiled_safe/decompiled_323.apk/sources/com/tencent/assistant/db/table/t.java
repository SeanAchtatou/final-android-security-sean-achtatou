package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.PluginDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.plugin.PluginInfo;
import java.util.List;

/* compiled from: ProGuard */
public class t implements IBaseTable {
    public int a(String str) {
        SQLiteDatabaseWrapper sQLiteDatabaseWrapper = null;
        try {
            sQLiteDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
            int delete = sQLiteDatabaseWrapper.delete("plugin_installed_info", "packagename=?", new String[]{str});
            if (sQLiteDatabaseWrapper == null) {
                return delete;
            }
            try {
                sQLiteDatabaseWrapper.close();
                return delete;
            } catch (Exception e) {
                e.printStackTrace();
                return delete;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            if (sQLiteDatabaseWrapper != null) {
                try {
                    sQLiteDatabaseWrapper.close();
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
            return -1;
        } catch (Throwable th) {
            if (sQLiteDatabaseWrapper != null) {
                try {
                    sQLiteDatabaseWrapper.close();
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
            }
            throw th;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: com.tencent.assistant.db.helper.SQLiteDatabaseWrapper} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: com.tencent.assistant.db.helper.SQLiteDatabaseWrapper} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: com.tencent.assistant.db.helper.SQLiteDatabaseWrapper} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: com.tencent.assistant.db.helper.SQLiteDatabaseWrapper} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v5, resolved type: com.tencent.assistant.db.helper.SQLiteDatabaseWrapper} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: com.tencent.assistant.db.helper.SQLiteDatabaseWrapper} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002c A[SYNTHETIC, Splitter:B:12:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0031 A[SYNTHETIC, Splitter:B:15:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x003c A[SYNTHETIC, Splitter:B:23:0x003c] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0041 A[Catch:{ Exception -> 0x0045 }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x004e A[SYNTHETIC, Splitter:B:32:0x004e] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0053 A[Catch:{ Exception -> 0x0057 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.tencent.assistant.plugin.PluginInfo> a() {
        /*
            r5 = this;
            r1 = 0
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            com.tencent.assistant.db.helper.SqliteHelper r0 = r5.getHelper()     // Catch:{ Exception -> 0x0035, all -> 0x004a }
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r2 = r0.getReadableDatabaseWrapper()     // Catch:{ Exception -> 0x0035, all -> 0x004a }
            java.lang.String r0 = "select * from plugin_installed_info order by version desc"
            r4 = 0
            android.database.Cursor r1 = r2.rawQuery(r0, r4)     // Catch:{ Exception -> 0x0062 }
            if (r1 == 0) goto L_0x002a
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0062 }
            if (r0 == 0) goto L_0x002a
        L_0x001d:
            com.tencent.assistant.plugin.PluginInfo r0 = r5.a(r1)     // Catch:{ Exception -> 0x0062 }
            r3.add(r0)     // Catch:{ Exception -> 0x0062 }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x0062 }
            if (r0 != 0) goto L_0x001d
        L_0x002a:
            if (r1 == 0) goto L_0x002f
            r1.close()     // Catch:{ Exception -> 0x005c }
        L_0x002f:
            if (r2 == 0) goto L_0x0034
            r2.close()     // Catch:{ Exception -> 0x005e }
        L_0x0034:
            return r3
        L_0x0035:
            r0 = move-exception
            r2 = r1
        L_0x0037:
            r0.printStackTrace()     // Catch:{ all -> 0x0060 }
            if (r1 == 0) goto L_0x003f
            r1.close()     // Catch:{ Exception -> 0x0045 }
        L_0x003f:
            if (r2 == 0) goto L_0x0034
            r2.close()     // Catch:{ Exception -> 0x0045 }
            goto L_0x0034
        L_0x0045:
            r0 = move-exception
        L_0x0046:
            r0.printStackTrace()
            goto L_0x0034
        L_0x004a:
            r0 = move-exception
            r2 = r1
        L_0x004c:
            if (r1 == 0) goto L_0x0051
            r1.close()     // Catch:{ Exception -> 0x0057 }
        L_0x0051:
            if (r2 == 0) goto L_0x0056
            r2.close()     // Catch:{ Exception -> 0x0057 }
        L_0x0056:
            throw r0
        L_0x0057:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0056
        L_0x005c:
            r0 = move-exception
            goto L_0x0046
        L_0x005e:
            r0 = move-exception
            goto L_0x0046
        L_0x0060:
            r0 = move-exception
            goto L_0x004c
        L_0x0062:
            r0 = move-exception
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.t.a():java.util.List");
    }

    private PluginInfo a(Cursor cursor) {
        String[] strArr;
        String[] strArr2;
        PluginInfo pluginInfo = new PluginInfo();
        pluginInfo.setVersion(cursor.getInt(cursor.getColumnIndexOrThrow("version")));
        pluginInfo.setPackageName(cursor.getString(cursor.getColumnIndexOrThrow("packagename")));
        pluginInfo.setPkgid(cursor.getInt(cursor.getColumnIndexOrThrow("pkgid")));
        pluginInfo.setMinBaoVersion(cursor.getInt(cursor.getColumnIndexOrThrow("min_bao_version")));
        pluginInfo.setMinApiLevel(cursor.getInt(cursor.getColumnIndexOrThrow("min_api_level")));
        pluginInfo.setMinFLevel(cursor.getInt(cursor.getColumnIndexOrThrow("min_f_level")));
        String string = cursor.getString(cursor.getColumnIndexOrThrow("name"));
        String string2 = cursor.getString(cursor.getColumnIndexOrThrow("start_activity"));
        if (!TextUtils.isEmpty(string)) {
            strArr = TextUtils.split(string, "\\^\\$\\^");
        } else {
            strArr = null;
        }
        if (!TextUtils.isEmpty(string2)) {
            strArr2 = TextUtils.split(string2, "\\^\\$\\^");
        } else {
            strArr2 = null;
        }
        if (!(string == null || strArr == null || string2 == null || strArr2 == null || strArr.length != strArr2.length)) {
            for (int i = 0; i < strArr.length; i++) {
                pluginInfo.addEntry(strArr[i], strArr2[i]);
            }
        }
        pluginInfo.setInProcess(cursor.getInt(cursor.getColumnIndexOrThrow("inprocess")));
        pluginInfo.setLaunchApplication(cursor.getString(cursor.getColumnIndexOrThrow("launch_application")));
        pluginInfo.setSmsReceiverImpl(cursor.getString(cursor.getColumnIndexOrThrow("sms_receiver_impl")));
        pluginInfo.setMainReceiverImpl(cursor.getString(cursor.getColumnIndexOrThrow("main_receiver_impl")));
        pluginInfo.setApkRecieverImpl(cursor.getString(cursor.getColumnIndexOrThrow("apk_receiver_impl")));
        pluginInfo.setSmsSentReceiverImpl(cursor.getString(cursor.getColumnIndexOrThrow("sms_sent_receiver_impl")));
        pluginInfo.setAuthorReceiverImpl(cursor.getString(cursor.getColumnIndexOrThrow("author_receiver_impl")));
        pluginInfo.setAppServiceImpl(cursor.getString(cursor.getColumnIndexOrThrow("app_service_impl")));
        pluginInfo.setIpcServiceImpl(cursor.getString(cursor.getColumnIndexOrThrow("ipc_service_impl")));
        pluginInfo.setDockReceiverImpl(cursor.getString(cursor.getColumnIndexOrThrow("dock_receiver_impl")));
        pluginInfo.setAccelerationServiceImpl(cursor.getString(cursor.getColumnIndexOrThrow("acc_service_impl")));
        pluginInfo.setExtendReceiverImpl(cursor.getString(cursor.getColumnIndexOrThrow("extend_receiver")));
        pluginInfo.setExtendServiceImpl(cursor.getString(cursor.getColumnIndexOrThrow("extend_service")));
        pluginInfo.setFileMd5(cursor.getString(cursor.getColumnIndexOrThrow("filemd5")));
        return pluginInfo;
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "plugin_installed_info";
    }

    public String createTableSQL() {
        return "Create TABLE if not exists plugin_installed_info (\n[_id] integer PRIMARY KEY AUTOINCREMENT\n,[version] integer\n,[packagename] text\n,[pkgid] integer\n,[min_bao_version] integer\n,[min_api_level] integer\n,[min_f_level] integer\n,[inprocess] integer DEFAULT 0\n,[name] text\n,[start_activity] text\n,[sms_receiver_impl] text\n,[launch_application] text\n,[main_receiver_impl] text\n,[apk_receiver_impl] text\n,[sms_sent_receiver_impl] text\n,[author_receiver_impl] text\n,[app_service_impl] text\n,[ipc_service_impl] text\n,[dock_receiver_impl] text\n,[acc_service_impl] text\n,[extend_service] text\n,[extend_receiver] text\n,[filemd5] text\n);";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i2 == 1) {
            return new String[]{"Create TABLE if not exists plugin_installed_info (\n[_id] integer PRIMARY KEY AUTOINCREMENT\n,[version] integer\n,[packagename] text\n,[pkgid] integer\n,[min_bao_version] integer\n,[min_api_level] integer\n,[min_f_level] integer\n,[inprocess] integer DEFAULT 0\n,[name] text\n,[start_activity] text\n,[sms_receiver_impl] text\n,[launch_application] text\n,[main_receiver_impl] text\n,[apk_receiver_impl] text\n,[sms_sent_receiver_impl] text\n,[author_receiver_impl] text\n,[app_service_impl] text\n,[ipc_service_impl] text\n,[dock_receiver_impl] text\n,[acc_service_impl] text\n,[extend_service] text\n,[extend_receiver] text\n,[filemd5] text\n);"};
        } else if (i == 1 && i2 == 2) {
            return new String[]{"alter table plugin_installed_info add column dock_receiver_impl TEXT;", "alter table plugin_installed_info add column acc_service_impl TEXT;", "alter table plugin_installed_info add column extend_receiver TEXT;", "alter table plugin_installed_info add column extend_service TEXT;"};
        } else if (i != 2 || i2 != 3) {
            return null;
        } else {
            return new String[]{"alter table plugin_installed_info add column filemd5 TEXT;"};
        }
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return PluginDbHelper.get(AstApp.i());
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0043 A[SYNTHETIC, Splitter:B:27:0x0043] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long a(com.tencent.assistant.plugin.PluginInfo r5) {
        /*
            r4 = this;
            r1 = 0
            if (r5 == 0) goto L_0x003c
            com.tencent.assistant.db.helper.SqliteHelper r0 = r4.getHelper()     // Catch:{ Exception -> 0x0033, all -> 0x003f }
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r2 = r0.getWritableDatabaseWrapper()     // Catch:{ Exception -> 0x0033, all -> 0x003f }
            int r0 = r4.a(r5, r2)     // Catch:{ Exception -> 0x0058, all -> 0x0053 }
            if (r0 > 0) goto L_0x0026
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ Exception -> 0x0058, all -> 0x0053 }
            r0.<init>()     // Catch:{ Exception -> 0x0058, all -> 0x0053 }
            r4.a(r0, r5)     // Catch:{ Exception -> 0x0058, all -> 0x0053 }
            java.lang.String r1 = "plugin_installed_info"
            r3 = 0
            long r0 = r2.insert(r1, r3, r0)     // Catch:{ Exception -> 0x0058, all -> 0x0053 }
            if (r2 == 0) goto L_0x0025
            r2.close()     // Catch:{ Exception -> 0x0051 }
        L_0x0025:
            return r0
        L_0x0026:
            r0 = 0
            if (r2 == 0) goto L_0x0025
            r2.close()     // Catch:{ Exception -> 0x002e }
            goto L_0x0025
        L_0x002e:
            r2 = move-exception
        L_0x002f:
            r2.printStackTrace()
            goto L_0x0025
        L_0x0033:
            r0 = move-exception
        L_0x0034:
            r0.printStackTrace()     // Catch:{ all -> 0x0055 }
            if (r1 == 0) goto L_0x003c
            r1.close()     // Catch:{ Exception -> 0x004c }
        L_0x003c:
            r0 = -1
            goto L_0x0025
        L_0x003f:
            r0 = move-exception
            r2 = r1
        L_0x0041:
            if (r2 == 0) goto L_0x0046
            r2.close()     // Catch:{ Exception -> 0x0047 }
        L_0x0046:
            throw r0
        L_0x0047:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0046
        L_0x004c:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x003c
        L_0x0051:
            r2 = move-exception
            goto L_0x002f
        L_0x0053:
            r0 = move-exception
            goto L_0x0041
        L_0x0055:
            r0 = move-exception
            r2 = r1
            goto L_0x0041
        L_0x0058:
            r0 = move-exception
            r1 = r2
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.t.a(com.tencent.assistant.plugin.PluginInfo):long");
    }

    public int b(String str) {
        int i = 0;
        SQLiteDatabaseWrapper sQLiteDatabaseWrapper = null;
        try {
            sQLiteDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
            i = sQLiteDatabaseWrapper.delete("plugin_installed_info", "packagename= ?", new String[]{str});
            if (sQLiteDatabaseWrapper != null) {
                try {
                    sQLiteDatabaseWrapper.close();
                } catch (Exception e) {
                    e = e;
                    e.printStackTrace();
                    return i;
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            if (sQLiteDatabaseWrapper != null) {
                try {
                    sQLiteDatabaseWrapper.close();
                } catch (Exception e3) {
                    e = e3;
                }
            }
        } catch (Throwable th) {
            if (sQLiteDatabaseWrapper != null) {
                try {
                    sQLiteDatabaseWrapper.close();
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
            }
            throw th;
        }
        return i;
    }

    private int a(PluginInfo pluginInfo, SQLiteDatabaseWrapper sQLiteDatabaseWrapper) {
        if (pluginInfo == null) {
            return -1;
        }
        try {
            ContentValues contentValues = new ContentValues();
            a(contentValues, pluginInfo);
            int update = sQLiteDatabaseWrapper.update("plugin_installed_info", contentValues, "packagename = ? and version= ?", new String[]{pluginInfo.getPackageName(), String.valueOf(pluginInfo.getVersion())});
            if (update <= 0) {
                return 0;
            }
            return update;
        } catch (Exception e) {
            e.printStackTrace();
            return -2;
        }
    }

    private void a(ContentValues contentValues, PluginInfo pluginInfo) {
        if (pluginInfo != null) {
            contentValues.put("version", Integer.valueOf(pluginInfo.getVersion()));
            contentValues.put("packagename", pluginInfo.getPackageName());
            contentValues.put("pkgid", Integer.valueOf(pluginInfo.getPkgid()));
            contentValues.put("inprocess", Integer.valueOf(pluginInfo.getInProcess()));
            contentValues.put("min_api_level", Integer.valueOf(pluginInfo.getMinApiLevel()));
            contentValues.put("min_bao_version", Integer.valueOf(pluginInfo.getMinBaoVersion()));
            contentValues.put("min_f_level", Integer.valueOf(pluginInfo.getMinFLevel()));
            List<PluginInfo.PluginEntry> pluginEntryList = pluginInfo.getPluginEntryList();
            if (pluginEntryList != null && pluginEntryList.size() > 0) {
                StringBuilder sb = new StringBuilder();
                StringBuilder sb2 = new StringBuilder();
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= pluginEntryList.size()) {
                        break;
                    }
                    if (i2 > 0) {
                        sb.append("^$^");
                        sb2.append("^$^");
                    }
                    sb.append(pluginEntryList.get(i2).getName());
                    sb2.append(pluginEntryList.get(i2).getStartActivity());
                    i = i2 + 1;
                }
                contentValues.put("name", sb.toString());
                contentValues.put("start_activity", sb2.toString());
            }
            contentValues.put("launch_application", pluginInfo.getLaunchApplication());
            contentValues.put("sms_receiver_impl", pluginInfo.getSmsReceiverImpl());
            contentValues.put("main_receiver_impl", pluginInfo.getMainReceiverImpl());
            contentValues.put("apk_receiver_impl", pluginInfo.getApkRecieverImpl());
            contentValues.put("sms_sent_receiver_impl", pluginInfo.getSmsSentReceiverImpl());
            contentValues.put("author_receiver_impl", pluginInfo.getAuthorReceiverImpl());
            contentValues.put("app_service_impl", pluginInfo.getAppServiceImpl());
            contentValues.put("ipc_service_impl", pluginInfo.getIpcServiceImpl());
            contentValues.put("dock_receiver_impl", pluginInfo.getDockReceiverImpl());
            contentValues.put("acc_service_impl", pluginInfo.getAccelerationServiceImpl());
            contentValues.put("extend_receiver", pluginInfo.getExtendReceiverImpl());
            contentValues.put("extend_service", pluginInfo.getExtendServiceImpl());
            contentValues.put("filemd5", pluginInfo.getFileMd5());
        }
    }
}
