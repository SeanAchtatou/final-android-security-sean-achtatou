package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.module.update.r;

/* compiled from: ProGuard */
public class c implements IBaseTable {
    public c() {
    }

    public c(Context context) {
    }

    /* access modifiers changed from: protected */
    public r a(Cursor cursor) {
        r rVar = new r();
        rVar.f1044a = cursor.getString(cursor.getColumnIndex("packagename"));
        rVar.c = cursor.getInt(cursor.getColumnIndex("versionCode"));
        rVar.b = cursor.getString(cursor.getColumnIndex("versionName"));
        return rVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0044 A[Catch:{ Exception -> 0x0038, all -> 0x0041 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.ArrayList<com.tencent.assistant.module.update.r> a() {
        /*
            r10 = this;
            r9 = 0
            monitor-enter(r10)
            java.util.ArrayList r8 = new java.util.ArrayList     // Catch:{ all -> 0x0048 }
            r8.<init>()     // Catch:{ all -> 0x0048 }
            com.tencent.assistant.db.helper.SqliteHelper r0 = r10.getHelper()     // Catch:{ all -> 0x0048 }
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()     // Catch:{ all -> 0x0048 }
            java.lang.String r1 = "appupdate_ignore"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0038, all -> 0x0041 }
            if (r0 == 0) goto L_0x0030
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x004f, all -> 0x004b }
            if (r1 == 0) goto L_0x0030
        L_0x0023:
            com.tencent.assistant.module.update.r r1 = r10.a(r0)     // Catch:{ Exception -> 0x004f, all -> 0x004b }
            r8.add(r1)     // Catch:{ Exception -> 0x004f, all -> 0x004b }
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x004f, all -> 0x004b }
            if (r1 != 0) goto L_0x0023
        L_0x0030:
            if (r0 == 0) goto L_0x0035
            r0.close()     // Catch:{ all -> 0x0048 }
        L_0x0035:
            r0 = r8
        L_0x0036:
            monitor-exit(r10)
            return r0
        L_0x0038:
            r0 = move-exception
            r0 = r9
        L_0x003a:
            if (r0 == 0) goto L_0x003f
            r0.close()     // Catch:{ all -> 0x0048 }
        L_0x003f:
            r0 = r8
            goto L_0x0036
        L_0x0041:
            r0 = move-exception
        L_0x0042:
            if (r9 == 0) goto L_0x0047
            r9.close()     // Catch:{ all -> 0x0048 }
        L_0x0047:
            throw r0     // Catch:{ all -> 0x0048 }
        L_0x0048:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x004b:
            r1 = move-exception
            r9 = r0
            r0 = r1
            goto L_0x0042
        L_0x004f:
            r1 = move-exception
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.c.a():java.util.ArrayList");
    }

    public synchronized boolean a(r rVar) {
        ContentValues contentValues;
        contentValues = new ContentValues();
        contentValues.put("packagename", rVar.f1044a);
        contentValues.put("versionCode", Integer.valueOf(rVar.c));
        contentValues.put("versionName", rVar.b);
        return getHelper().getWritableDatabaseWrapper().insert("appupdate_ignore", null, contentValues) != -1;
    }

    public synchronized int b(r rVar) {
        return getHelper().getWritableDatabaseWrapper().delete("appupdate_ignore", "packagename = ?", new String[]{rVar.f1044a});
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "appupdate_ignore";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists appupdate_ignore (_id INTEGER PRIMARY KEY AUTOINCREMENT,packagename TEXT,versionCode INTEGER,versionName TEXT);";
    }

    public String[] getAlterSQL(int i, int i2) {
        return null;
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }
}
