package com.tencent.assistant.db.table;

import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.db.helper.StatDbHelper;

/* compiled from: ProGuard */
public class g implements IBaseTable {
    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "st_connection_data";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists st_connection_data (_id INTEGER PRIMARY KEY AUTOINCREMENT,aid TEXT,content BLOB);";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i != 1 || i2 != 2) {
            return null;
        }
        return new String[]{"CREATE TABLE if not exists st_connection_data (_id INTEGER PRIMARY KEY AUTOINCREMENT,aid TEXT,content BLOB);"};
    }

    public SqliteHelper getHelper() {
        return StatDbHelper.get(AstApp.i());
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }
}
