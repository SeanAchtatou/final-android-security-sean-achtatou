package com.tencent.assistant.thumbnailCache;

import android.graphics.Bitmap;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.aq;
import com.tencent.assistant.utils.n;
import com.tencent.assistant.utils.t;
import java.io.File;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.LinkedHashMap;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    protected LinkedHashMap<String, Bitmap> f1789a;
    protected LinkedHashMap<String, a> b;
    protected ReferenceQueue<Bitmap> c;
    protected d d;
    protected int e;
    protected int f;
    String g;
    protected int h;
    private Object i;
    private boolean j;
    private String k;
    private String l;
    /* access modifiers changed from: private */
    public long m;
    /* access modifiers changed from: private */
    public long n;
    private float o;
    private n p;

    static /* synthetic */ long a(b bVar, long j2) {
        long j3 = bVar.m - j2;
        bVar.m = j3;
        return j3;
    }

    public b(String str, String str2, int i2, int i3, int i4, boolean z, n nVar) {
        this.e = 0;
        this.f = 0;
        this.i = new Object();
        this.j = false;
        this.n = 4096;
        this.o = 0.05f;
        this.n = (long) (((float) (t.a() * 1024)) * this.o);
        this.p = nVar;
        this.h = i4;
        this.f1789a = new ThumbnailCache$1(this, i2, 0.75f, true);
        this.b = new ThumbnailCache$2(this, i2 * 2, 0.75f, true);
        this.c = new ReferenceQueue<>();
        this.e = i2;
        this.f = i3;
        this.j = z;
        this.g = str;
        if (this.j) {
            this.l = FileUtil.getPath(FileUtil.getInternalCachePath() + "ThumbnailCache" + File.separator + str2, true);
        } else {
            this.k = FileUtil.getPath(FileUtil.getCommonRootDir() + File.separator + "ThumbnailCache" + File.separator + str2, true);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0019, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x003e, code lost:
        r0 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0061, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0073, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0074, code lost:
        r4 = r0;
        r0 = null;
        r1 = r4;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0061 A[ExcHandler: all (th java.lang.Throwable), PHI: r1 
      PHI: (r1v5 android.graphics.Bitmap) = (r1v0 android.graphics.Bitmap), (r1v14 android.graphics.Bitmap), (r1v17 android.graphics.Bitmap), (r1v17 android.graphics.Bitmap), (r1v24 android.graphics.Bitmap), (r1v24 android.graphics.Bitmap) binds: [B:4:0x0009, B:15:0x001c, B:50:0x0060, B:51:?, B:37:0x004d, B:38:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:4:0x0009] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap a(java.lang.String r6) {
        /*
            r5 = this;
            r1 = 0
            if (r6 != 0) goto L_0x0005
            r0 = r1
        L_0x0004:
            return r0
        L_0x0005:
            java.lang.String r2 = r5.b(r6)
            java.util.LinkedHashMap<java.lang.String, android.graphics.Bitmap> r0 = r5.f1789a     // Catch:{ Exception -> 0x0073, all -> 0x0061 }
            if (r0 == 0) goto L_0x001a
            java.util.LinkedHashMap<java.lang.String, android.graphics.Bitmap> r3 = r5.f1789a     // Catch:{ Exception -> 0x0073, all -> 0x0061 }
            monitor-enter(r3)     // Catch:{ Exception -> 0x0073, all -> 0x0061 }
            java.util.LinkedHashMap<java.lang.String, android.graphics.Bitmap> r0 = r5.f1789a     // Catch:{ all -> 0x004b }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x004b }
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0     // Catch:{ all -> 0x004b }
            monitor-exit(r3)     // Catch:{ all -> 0x007d }
            r1 = r0
        L_0x001a:
            if (r1 != 0) goto L_0x0082
            java.util.LinkedHashMap<java.lang.String, com.tencent.assistant.thumbnailCache.a> r3 = r5.b     // Catch:{ Exception -> 0x004e, all -> 0x0061 }
            monitor-enter(r3)     // Catch:{ Exception -> 0x004e, all -> 0x0061 }
            java.util.LinkedHashMap<java.lang.String, com.tencent.assistant.thumbnailCache.a> r0 = r5.b     // Catch:{ all -> 0x005e }
            boolean r0 = r0.containsKey(r2)     // Catch:{ all -> 0x005e }
            if (r0 == 0) goto L_0x003d
            java.util.LinkedHashMap<java.lang.String, com.tencent.assistant.thumbnailCache.a> r0 = r5.b     // Catch:{ all -> 0x005e }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x005e }
            com.tencent.assistant.thumbnailCache.a r0 = (com.tencent.assistant.thumbnailCache.a) r0     // Catch:{ all -> 0x005e }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x005e }
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0     // Catch:{ all -> 0x005e }
            if (r0 != 0) goto L_0x003c
            java.util.LinkedHashMap<java.lang.String, com.tencent.assistant.thumbnailCache.a> r1 = r5.b     // Catch:{ all -> 0x0078 }
            r1.remove(r2)     // Catch:{ all -> 0x0078 }
        L_0x003c:
            r1 = r0
        L_0x003d:
            monitor-exit(r3)     // Catch:{ all -> 0x005e }
            r0 = r1
        L_0x003f:
            if (r0 == 0) goto L_0x0004
            com.tencent.assistant.thumbnailCache.d r1 = r5.d
            if (r1 == 0) goto L_0x0004
            com.tencent.assistant.thumbnailCache.d r1 = r5.d
        L_0x0047:
            r1.a(r2)
            goto L_0x0004
        L_0x004b:
            r0 = move-exception
        L_0x004c:
            monitor-exit(r3)     // Catch:{ all -> 0x004b }
            throw r0     // Catch:{ Exception -> 0x004e, all -> 0x0061 }
        L_0x004e:
            r0 = move-exception
            r4 = r0
            r0 = r1
            r1 = r4
        L_0x0052:
            r1.printStackTrace()     // Catch:{ all -> 0x006e }
            if (r0 == 0) goto L_0x0004
            com.tencent.assistant.thumbnailCache.d r1 = r5.d
            if (r1 == 0) goto L_0x0004
            com.tencent.assistant.thumbnailCache.d r1 = r5.d
            goto L_0x0047
        L_0x005e:
            r0 = move-exception
        L_0x005f:
            monitor-exit(r3)     // Catch:{ all -> 0x005e }
            throw r0     // Catch:{ Exception -> 0x004e, all -> 0x0061 }
        L_0x0061:
            r0 = move-exception
        L_0x0062:
            if (r1 == 0) goto L_0x006d
            com.tencent.assistant.thumbnailCache.d r1 = r5.d
            if (r1 == 0) goto L_0x006d
            com.tencent.assistant.thumbnailCache.d r1 = r5.d
            r1.a(r2)
        L_0x006d:
            throw r0
        L_0x006e:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0062
        L_0x0073:
            r0 = move-exception
            r4 = r0
            r0 = r1
            r1 = r4
            goto L_0x0052
        L_0x0078:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x005f
        L_0x007d:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x004c
        L_0x0082:
            r0 = r1
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.thumbnailCache.b.a(java.lang.String):android.graphics.Bitmap");
    }

    public void a(String str, Bitmap bitmap) {
        if (bitmap != null) {
            String b2 = b(str);
            if (this.d != null) {
                this.d.a(b2);
            }
            try {
                d();
                if (this.f1789a != null) {
                    synchronized (this.f1789a) {
                        if (!this.f1789a.containsKey(b2)) {
                            this.f1789a.put(b2, bitmap);
                            this.m += (long) (bitmap.getRowBytes() * bitmap.getHeight());
                        }
                    }
                    return;
                }
                synchronized (this.b) {
                    if (!this.b.containsKey(b2)) {
                        this.b.put(b2, new a(b2, bitmap, this.c));
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    private void d() {
        synchronized (this.b) {
            while (true) {
                Reference<? extends Bitmap> poll = this.c.poll();
                if (poll != null) {
                    this.b.remove(((a) poll).f1788a);
                }
            }
        }
    }

    public void a() {
        synchronized (this.b) {
            this.f1789a.clear();
            this.b.clear();
            this.m = 0;
            this.o = (float) (((double) this.o) - 2.0E-4d);
        }
    }

    public void b() {
        if (this.f <= 0) {
            return;
        }
        if (this.d == null) {
            this.d = new d(new File(this.j ? this.l : this.k), this.f);
        } else {
            this.d.a();
        }
    }

    public void c() {
        if (this.d != null) {
            this.d.b();
        }
    }

    public String b(String str) {
        String str2;
        if (this.j) {
            str2 = this.l + File.separator + g(str.toString());
            File file = new File(this.l);
            if (!file.exists()) {
                file.mkdirs();
            }
        } else {
            str2 = this.k + File.separator + g(str.toString());
            File file2 = new File(this.k);
            if (!file2.exists()) {
                file2.mkdirs();
            }
        }
        return str2;
    }

    private String g(String str) {
        return aq.b(str);
    }

    public long c(String str) {
        long j2 = 0;
        if (this.f > 0) {
            String b2 = b(str);
            synchronized (this.i) {
                j2 = FileUtil.getFileSize(b2);
            }
        }
        return j2;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:12:0x0024=Splitter:B:12:0x0024, B:31:0x003e=Splitter:B:31:0x003e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] d(java.lang.String r5) {
        /*
            r4 = this;
            r0 = 0
            int r1 = r4.f
            if (r1 <= 0) goto L_0x0025
            java.lang.String r1 = r4.b(r5)
            com.tencent.assistant.utils.aw r2 = new com.tencent.assistant.utils.aw
            com.tencent.assistant.utils.n r3 = r4.p
            r2.<init>(r3)
            java.lang.Object r3 = r4.i
            monitor-enter(r3)
            if (r2 == 0) goto L_0x001f
            boolean r1 = com.tencent.assistant.utils.FileUtil.readFile(r1, r2)     // Catch:{ Exception -> 0x0026 }
            if (r1 == 0) goto L_0x001f
            byte[] r0 = r2.a()     // Catch:{ Exception -> 0x0026 }
        L_0x001f:
            if (r2 == 0) goto L_0x0024
            r2.close()     // Catch:{ IOException -> 0x0044 }
        L_0x0024:
            monitor-exit(r3)     // Catch:{ all -> 0x0035 }
        L_0x0025:
            return r0
        L_0x0026:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x0038 }
            if (r2 == 0) goto L_0x0024
            r2.close()     // Catch:{ IOException -> 0x0030 }
            goto L_0x0024
        L_0x0030:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x0035 }
            goto L_0x0024
        L_0x0035:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0035 }
            throw r0
        L_0x0038:
            r0 = move-exception
            if (r2 == 0) goto L_0x003e
            r2.close()     // Catch:{ IOException -> 0x003f }
        L_0x003e:
            throw r0     // Catch:{ all -> 0x0035 }
        L_0x003f:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x0035 }
            goto L_0x003e
        L_0x0044:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x0035 }
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.thumbnailCache.b.d(java.lang.String):byte[]");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001a, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001b, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] e(java.lang.String r5) {
        /*
            r4 = this;
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            r0 = 0
            java.lang.Object r2 = r4.i
            monitor-enter(r2)
            if (r1 == 0) goto L_0x0015
            boolean r3 = com.tencent.assistant.utils.FileUtil.readFile(r5, r1)     // Catch:{ all -> 0x001f }
            if (r3 == 0) goto L_0x0015
            byte[] r0 = r1.toByteArray()     // Catch:{ all -> 0x001f }
        L_0x0015:
            r1.close()     // Catch:{ IOException -> 0x001a }
        L_0x0018:
            monitor-exit(r2)     // Catch:{ all -> 0x001f }
            return r0
        L_0x001a:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x001f }
            goto L_0x0018
        L_0x001f:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x001f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.thumbnailCache.b.e(java.lang.String):byte[]");
    }

    public boolean a(String str, byte[] bArr) {
        boolean z = false;
        if (this.f > 0) {
            String b2 = b(str);
            if (FileUtil.freeSpaceOnSd() < 30) {
            }
            if (bArr != null) {
                synchronized (this.i) {
                    z = FileUtil.write2File(bArr, b2);
                }
            }
        }
        return z;
    }

    public boolean f(String str) {
        boolean z = false;
        if (this.f > 0) {
            String b2 = b(str);
            synchronized (this.i) {
                z = FileUtil.deleteFile(b2);
            }
        }
        return z;
    }
}
