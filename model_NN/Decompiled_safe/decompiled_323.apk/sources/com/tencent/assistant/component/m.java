package com.tencent.assistant.component;

import android.view.View;
import com.tencent.assistant.component.HorizonScrollLayout;

/* compiled from: ProGuard */
class m implements HorizonScrollLayout.OnTouchScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HomePageBanner f681a;

    m(HomePageBanner homePageBanner) {
        this.f681a = homePageBanner;
    }

    public void onScrollStateChanged(int i, int i2) {
        if (1 == i) {
            this.f681a.stopPlay();
        } else if (i == 0) {
            this.f681a.startPlay();
        }
    }

    public void onScroll(View view, float f, float f2) {
    }

    public void onScreenChange(int i) {
        if (this.f681a.cards.size() > 0) {
            this.f681a.updateDots(i);
        }
    }
}
