package com.tencent.assistant.component;

import com.tencent.assistant.AppConst;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f641a;
    final /* synthetic */ AppConst.AppState b;
    final /* synthetic */ AppIconView c;

    c(AppIconView appIconView, String str, AppConst.AppState appState) {
        this.c = appIconView;
        this.f641a = str;
        this.b = appState;
    }

    public void run() {
        if (this.c.mAppModel != null && this.c.mAppModel.q().equals(this.f641a)) {
            this.c.updateProgressView(DownloadProxy.a().d(this.f641a), this.b);
            this.c.updateAppIcon(this.f641a, this.b);
        }
    }
}
