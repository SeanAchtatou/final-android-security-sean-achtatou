package com.tencent.assistant.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.List;

/* compiled from: ProGuard */
public class ColorCardView extends LinearLayout {
    public static final String CATEGORY_COLOR_CARD_COLUMN_ID = "03";
    private final int MAX_COUNT;
    private final String TAG;
    private b exposureStrategy;
    private int[] mColorArray;
    private LinearLayout mContainer;
    private LinearLayout.LayoutParams mLineLayoutParams;
    /* access modifiers changed from: private */
    public View.OnClickListener mOutListener;
    private View.OnClickListener mTmaListener;

    public ColorCardView(Context context) {
        this(context, null);
    }

    public ColorCardView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, null);
    }

    public ColorCardView(Context context, AttributeSet attributeSet, View.OnClickListener onClickListener) {
        super(context, attributeSet);
        this.TAG = "ColorCardView";
        this.mOutListener = null;
        this.MAX_COUNT = 6;
        this.exposureStrategy = null;
        this.mTmaListener = new g(this);
        initViews(context, attributeSet);
        this.mOutListener = onClickListener;
    }

    private void initViews(Context context, AttributeSet attributeSet) {
        setOrientation(1);
        LayoutInflater.from(getContext()).inflate((int) R.layout.color_card_view, this);
        this.mContainer = (LinearLayout) findViewById(R.id.item_container);
        this.mLineLayoutParams = new LinearLayout.LayoutParams(-1, -2);
        this.mLineLayoutParams.topMargin = by.a(getContext(), 7.0f);
        TypedArray obtainTypedArray = context.getResources().obtainTypedArray(R.array.app_category_color_card_txt_array);
        this.mColorArray = new int[obtainTypedArray.length()];
        for (int i = 0; i < obtainTypedArray.length(); i++) {
            this.mColorArray[i] = obtainTypedArray.getColor(i, 0);
        }
        obtainTypedArray.recycle();
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    public void refreshData(List<ColorCardItem> list) {
        int i;
        int i2 = 6;
        XLog.d("ColorCardView", "refresh");
        if (list != null) {
            if (6 >= list.size()) {
                i2 = list.size();
            }
            if (i2 % 2 != 0) {
                i = i2 - 1;
            } else {
                i = i2;
            }
            this.mContainer.removeAllViews();
            for (int i3 = 0; i3 < i; i3 += 2) {
                View inflate = LayoutInflater.from(getContext()).inflate((int) R.layout.color_card_list_item_view, (ViewGroup) null);
                View findViewById = inflate.findViewById(R.id.item_left);
                findViewById.setOnClickListener(this.mTmaListener);
                ColorCardItem colorCardItem = list.get(i3);
                findViewById.setTag(R.id.category_pos, Integer.valueOf(i3));
                findViewById.setTag(R.id.category_data, colorCardItem);
                if (colorCardItem != null) {
                    TextView textView = (TextView) findViewById.findViewById(R.id.title);
                    textView.setText(colorCardItem.c);
                    if (this.mColorArray != null && this.mColorArray.length > 0) {
                        textView.setTextColor(this.mColorArray[i3 % this.mColorArray.length]);
                    }
                }
                itemActionReport(i3, 100);
                View findViewById2 = inflate.findViewById(R.id.item_right);
                findViewById2.setOnClickListener(this.mTmaListener);
                ColorCardItem colorCardItem2 = list.get(i3 + 1);
                findViewById2.setTag(R.id.category_pos, Integer.valueOf(i3 + 1));
                findViewById2.setTag(R.id.category_data, colorCardItem2);
                if (colorCardItem2 != null) {
                    TextView textView2 = (TextView) findViewById2.findViewById(R.id.title);
                    textView2.setText(colorCardItem2.c);
                    if (this.mColorArray != null && this.mColorArray.length > 0) {
                        textView2.setTextColor(this.mColorArray[(i3 + 1) % this.mColorArray.length]);
                    }
                }
                if (i3 > 1) {
                    inflate.setLayoutParams(this.mLineLayoutParams);
                }
                this.mContainer.addView(inflate);
                itemActionReport(i3 + 1, 100);
            }
            invalidate();
        }
    }

    /* access modifiers changed from: private */
    public void itemActionReport(int i, int i2) {
        if (getContext() instanceof BaseActivity) {
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(getContext(), i2);
            buildSTInfo.slotId = a.a("03", i);
            if (i2 == 100) {
                if (this.exposureStrategy == null) {
                    this.exposureStrategy = new b();
                }
                this.exposureStrategy.exposure(buildSTInfo);
                return;
            }
            l.a(buildSTInfo);
        }
    }
}
