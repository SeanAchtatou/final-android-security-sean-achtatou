package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class LoadingDialogView extends RelativeLayout {
    private ProgressBar bar;
    private LayoutInflater mInflater;
    private TextView textView;

    public LoadingDialogView(Context context) {
        super(context);
        this.mInflater = LayoutInflater.from(context);
        initView();
    }

    public LoadingDialogView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mInflater = LayoutInflater.from(context);
        initView();
    }

    private void initView() {
        this.mInflater.inflate((int) R.layout.dialog_loading, this);
        this.bar = (ProgressBar) findViewById(R.id.loading_view);
        this.textView = (TextView) findViewById(R.id.loading_txt);
    }

    public void setLoadingText(String str) {
        if (this.textView != null && str != null) {
            this.textView.setText(str);
        }
    }

    public void setVisibility(int i) {
        if (i == 0) {
            this.bar.setIndeterminateDrawable(getResources().getDrawable(R.anim.page_loading_animation));
        } else {
            this.bar.setIndeterminateDrawable(null);
        }
        super.setVisibility(i);
    }
}
