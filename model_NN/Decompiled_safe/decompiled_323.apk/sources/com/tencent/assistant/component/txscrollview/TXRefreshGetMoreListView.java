package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.bd;

/* compiled from: ProGuard */
public class TXRefreshGetMoreListView extends TXRefreshScrollViewBase<ListView> implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    protected TXLoadingLayoutBase f694a;
    protected TXLoadingLayoutBase b;
    protected TXRefreshScrollViewBase.RefreshState c = TXRefreshScrollViewBase.RefreshState.RESET;
    private ListAdapter d;
    private String e;
    private int f = 0;
    private IScrollListener u;
    private Bitmap v = null;
    private boolean w = false;
    private OnDispatchTouchEventListener x;
    private boolean y = false;

    public TXRefreshGetMoreListView(Context context, TXScrollViewBase.ScrollMode scrollMode) {
        super(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.PULL_FROM_START);
        if (this.s != null) {
            ((ListView) this.s).setCacheColorHint(0);
        }
    }

    public TXRefreshGetMoreListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (this.s != null) {
            ((ListView) this.s).setCacheColorHint(0);
        }
    }

    public void setRefreshTimeKey(String str) {
        this.e = str;
        if (this.g != null) {
            ((RefreshListLoading) this.g).setRefreshTimeKey(str);
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        LinearLayout.LayoutParams k = k();
        if (this.g != null) {
            if (this.g.getParent() == this) {
                removeView(this.g);
            }
            a(this.g, 0, k);
        }
        this.h = null;
        l();
    }

    public void resetLoadLoadingLayoutBg() {
        if (this.w) {
            if (this.f694a != null) {
                this.f694a.setBackgroundDrawable(null);
                this.f694a.setDrawingCacheEnabled(false);
                ((RefreshListLoading) this.f694a).resetLoadingResource();
            }
            if (this.g != null) {
                this.g.setBackgroundDrawable(null);
                this.g.setDrawingCacheEnabled(false);
                ((RefreshListLoading) this.g).resetLoadingResource();
            }
            this.w = false;
            if (this.v != null && !this.v.isRecycled()) {
                this.v.recycle();
                this.v = null;
            }
        }
    }

    public void updateBackground(Context context, Bitmap bitmap) {
        Bitmap bitmap2;
        if (context != null && bitmap != null && !bitmap.isRecycled()) {
            if (this.v != null) {
                this.v = null;
            }
            this.v = bitmap;
            if (this.f694a != null && this.g != null) {
                try {
                    bitmap2 = Bitmap.createBitmap(this.v, 0, this.v.getHeight() - l, this.v.getWidth(), l);
                } catch (OutOfMemoryError e2) {
                    e2.printStackTrace();
                    bitmap2 = null;
                }
                if (bitmap2 != null) {
                    this.f694a.setBackgroundDrawable(new BitmapDrawable(bitmap2));
                    this.f694a.setDrawingCacheEnabled(true);
                    this.g.setBackgroundDrawable(new BitmapDrawable(this.v));
                    this.g.setDrawingCacheEnabled(true);
                    this.w = true;
                    ((RefreshListLoading) this.g).refreshLoadingResource();
                    ((RefreshListLoading) this.f694a).refreshLoadingResource();
                }
            }
        }
    }

    public void onRefreshComplete(boolean z, boolean z2, String str) {
        if (this.f694a != null) {
            if (z) {
                this.f694a.refreshSuc();
                this.g.refreshSuc();
            } else {
                this.f694a.refreshFail(str);
                this.g.refreshFail(str);
            }
        }
        this.g.hideAllSubViews();
        ah.a().postDelayed(new l(this, z2), 900);
    }

    public void onRefreshComplete(boolean z) {
        onRefreshComplete(z, true);
    }

    public void onRefreshComplete(boolean z, boolean z2) {
        super.onRefreshComplete(z);
        if (this.c == TXRefreshScrollViewBase.RefreshState.REFRESHING) {
            if (z) {
                this.c = TXRefreshScrollViewBase.RefreshState.RESET;
            } else {
                this.c = TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH;
            }
        } else if (this.c == TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH) {
            this.c = TXRefreshScrollViewBase.RefreshState.RESET;
        } else if (this.c == TXRefreshScrollViewBase.RefreshState.RESET && !z) {
            this.c = TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH;
        }
        a(z2);
    }

    /* access modifiers changed from: protected */
    public void b() {
        int i;
        TXLoadingLayoutBase tXLoadingLayoutBase;
        TXLoadingLayoutBase tXLoadingLayoutBase2;
        boolean z;
        if (this.n != TXScrollViewBase.ScrollState.ScrollState_FromStart || this.g == null || this.f694a == null) {
            i = 0;
            tXLoadingLayoutBase = null;
            tXLoadingLayoutBase2 = null;
            z = false;
        } else {
            tXLoadingLayoutBase2 = this.g;
            tXLoadingLayoutBase = this.f694a;
            i = 0 - this.g.getContentSize();
            z = Math.abs(((ListView) this.s).getFirstVisiblePosition() - 0) <= 1;
        }
        if (tXLoadingLayoutBase != null && tXLoadingLayoutBase.getVisibility() == 0) {
            tXLoadingLayoutBase.setVisibility(8);
            if (Build.VERSION.SDK_INT >= 11) {
                tXLoadingLayoutBase2.setTranslationY(0.0f);
            }
            tXLoadingLayoutBase2.showAllSubViews();
            if (z) {
                ((ListView) this.s).setSelection(0);
                a(i);
            }
        }
        this.m = false;
        this.i = true;
        smoothScrollTo(0);
        postDelayed(new m(this), 200);
    }

    /* access modifiers changed from: protected */
    public void c() {
        int i;
        TXLoadingLayoutBase tXLoadingLayoutBase;
        TXLoadingLayoutBase tXLoadingLayoutBase2 = null;
        ListAdapter adapter = ((ListView) this.s).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            super.c();
            return;
        }
        if (this.n != TXScrollViewBase.ScrollState.ScrollState_FromStart || this.g == null || this.f694a == null) {
            i = 0;
            tXLoadingLayoutBase = null;
        } else {
            tXLoadingLayoutBase = this.g;
            tXLoadingLayoutBase2 = this.f694a;
            i = getScrollY() + this.g.getContentSize();
        }
        if (tXLoadingLayoutBase2 != null) {
            tXLoadingLayoutBase2.setVisibility(0);
            tXLoadingLayoutBase2.refreshing();
        }
        if (tXLoadingLayoutBase != null) {
            tXLoadingLayoutBase.refreshing();
            if (Build.VERSION.SDK_INT >= 11 && this.w) {
                tXLoadingLayoutBase.setTranslationY((float) l);
            }
            tXLoadingLayoutBase.hideAllSubViews();
        }
        this.i = false;
        a(i);
        ((ListView) this.s).setSelection(0);
        smoothScrollTo(0, new n(this));
    }

    /* access modifiers changed from: protected */
    public void d() {
        if (this.n == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.g != null && this.f694a != null) {
            TXLoadingLayoutBase tXLoadingLayoutBase = this.g;
            TXLoadingLayoutBase tXLoadingLayoutBase2 = this.f694a;
            int contentSize = 0 - this.g.getContentSize();
            boolean z = Math.abs(((ListView) this.s).getFirstVisiblePosition() - 0) <= 1;
            if (tXLoadingLayoutBase2 != null && tXLoadingLayoutBase2.getVisibility() == 0) {
                tXLoadingLayoutBase2.setVisibility(8);
                if (Build.VERSION.SDK_INT >= 11) {
                    tXLoadingLayoutBase.setTranslationY(0.0f);
                }
                tXLoadingLayoutBase.showAllSubViews();
                if (z) {
                    ((ListView) this.s).setSelection(0);
                    a(contentSize);
                }
            }
            this.m = false;
            this.i = true;
            if (this.g != null) {
                this.g.reset();
            }
            smoothScrollTo(0);
        }
    }

    /* access modifiers changed from: protected */
    public void g() {
        a(true);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        if (this.b != null) {
            switch (o.f717a[this.c.ordinal()]) {
                case 1:
                    if (z) {
                        this.b.loadSuc();
                        return;
                    } else {
                        this.b.loadFail();
                        return;
                    }
                case 2:
                    int i = 0;
                    if (getRawAdapter() != null) {
                        i = getRawAdapter().getCount();
                    }
                    this.b.loadFinish(bd.b(getContext(), i));
                    return;
                case 3:
                    this.b.refreshing();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ListView b(Context context) {
        ListView listView = new ListView(context);
        if (this.o != TXScrollViewBase.ScrollMode.NONE) {
            this.f694a = a(context, listView, TXScrollViewBase.ScrollMode.PULL_FROM_START);
            this.b = a(context, TXScrollViewBase.ScrollMode.PULL_FROM_END);
            this.b.setVisibility(0);
            listView.addFooterView(this.b);
            listView.setOnScrollListener(this);
        }
        return listView;
    }

    /* access modifiers changed from: protected */
    public TXLoadingLayoutBase a(Context context, ListView listView, TXScrollViewBase.ScrollMode scrollMode) {
        FrameLayout frameLayout = new FrameLayout(getContext());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -2, 1);
        TXLoadingLayoutBase a2 = a(context, scrollMode);
        a2.setVisibility(8);
        frameLayout.addView(a2, layoutParams);
        listView.addHeaderView(frameLayout, null, false);
        return a2;
    }

    public void addFooterView(View view) {
        if (view != null) {
            if (this.b != null) {
                ((ListView) this.s).removeFooterView(this.b);
            }
            this.b = (TXLoadingLayoutBase) view;
            ((ListView) this.s).addFooterView(this.b);
            this.b.setVisibility(0);
            g();
        }
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        this.f = i;
        if (this.u != null) {
            this.u.onScrollStateChanged(absListView, i);
        }
        if (i == 0 && this.y && this.c == TXRefreshScrollViewBase.RefreshState.RESET) {
            if (this.k != null) {
                this.k.onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState.ScrollState_FromEnd);
            }
            this.c = TXRefreshScrollViewBase.RefreshState.REFRESHING;
            g();
        }
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.s != null) {
            this.y = f();
            if (this.u != null) {
                this.u.onScroll(absListView, i, i2, i3);
            }
        }
    }

    /* access modifiers changed from: protected */
    public TXLoadingLayoutBase a(Context context, TXScrollViewBase.ScrollMode scrollMode) {
        return new RefreshListLoading(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, scrollMode);
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        View childAt;
        ListAdapter adapter = ((ListView) this.s).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        if ((this.w && this.j == TXRefreshScrollViewBase.RefreshState.REFRESHING) || ((ListView) this.s).getFirstVisiblePosition() > 1 || (childAt = ((ListView) this.s).getChildAt(0)) == null) {
            return false;
        }
        return childAt.getTop() >= ((ListView) this.s).getTop();
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        View childAt;
        ListAdapter adapter = ((ListView) this.s).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        int count = ((ListView) this.s).getCount() - 1;
        int lastVisiblePosition = ((ListView) this.s).getLastVisiblePosition();
        if (lastVisiblePosition < count - 1 || (childAt = ((ListView) this.s).getChildAt(lastVisiblePosition - ((ListView) this.s).getFirstVisiblePosition())) == null) {
            return false;
        }
        return childAt.getBottom() <= ((ListView) this.s).getBottom();
    }

    public void setDivider(Drawable drawable) {
        ((ListView) this.s).setDivider(drawable);
    }

    public void setListSelector(int i) {
        ((ListView) this.s).setSelector(i);
    }

    public void setSelection(int i) {
        ((ListView) this.s).setSelection(i);
    }

    public void setAdapter(ListAdapter listAdapter) {
        if (listAdapter != null) {
            this.d = listAdapter;
            ((ListView) this.s).setAdapter(listAdapter);
        }
    }

    public ListAdapter getRawAdapter() {
        return this.d;
    }

    public void setSelector(Drawable drawable) {
        if (this.s != null && drawable != null) {
            ((ListView) this.s).setSelector(drawable);
        }
    }

    public int getFirstVisiblePosition() {
        if (this.s != null) {
            return ((ListView) this.s).getFirstVisiblePosition();
        }
        return -1;
    }

    public ListView getListView() {
        return (ListView) this.s;
    }

    public boolean isScrollStateIdle() {
        return this.f == 0;
    }

    public void setIScrollerListener(IScrollListener iScrollListener) {
        this.u = iScrollListener;
    }

    public int pointToPosition(int i, int i2) {
        if (this.s != null) {
            return ((ListView) this.s).pointToPosition(i, i2);
        }
        return -1;
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (this.x != null) {
            this.x.dispatchTouchEvent(motionEvent);
        }
        try {
            return super.dispatchTouchEvent(motionEvent);
        } catch (OutOfMemoryError e2) {
            e2.printStackTrace();
            return false;
        }
    }

    /* compiled from: ProGuard */
    public abstract class OnDispatchTouchEventListener {
        public abstract boolean dispatchTouchEvent(MotionEvent motionEvent);

        public OnDispatchTouchEventListener() {
        }
    }

    public void setOnTouchEventListener(OnDispatchTouchEventListener onDispatchTouchEventListener) {
        this.x = onDispatchTouchEventListener;
    }
}
