package com.tencent.assistant.component;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.localres.LocalMediaLoader;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: ProGuard */
public abstract class WifiTransferMediaAdapter<T> extends BaseAdapter implements LocalMediaLoader.ILocalMediaLoaderListener<T> {
    protected Context mContext = null;
    protected boolean mIsNeedNotifyDataSetChanged = true;
    private IWifiTransferMediaAdapterListener mListener = null;
    protected ArrayList<T> mMediaList = null;
    protected LocalMediaLoader<T> mMediaLoader;
    private HashMap<Integer, WifiTransferMediaAdapter<T>.SendFileState> mSendFileStateMap;

    /* compiled from: ProGuard */
    public interface IWifiTransferMediaAdapterListener {
        void onWifiTransferMediaAdapterLoadDataFinish(WifiTransferMediaAdapter<?> wifiTransferMediaAdapter, int i);

        void onWifiTransferMediaAdapterLoaderDataOver();
    }

    /* access modifiers changed from: protected */
    public abstract View createConvertView();

    /* access modifiers changed from: protected */
    public abstract void fillConvertView(View view, Object obj, int i);

    public WifiTransferMediaAdapter(Context context) {
        this.mContext = context;
        this.mMediaList = new ArrayList<>();
        this.mSendFileStateMap = new HashMap<>();
    }

    public void loadData() {
        if (this.mMediaLoader != null) {
            this.mMediaLoader.getLocalMediaList();
        }
    }

    public void loadData(boolean z) {
        this.mIsNeedNotifyDataSetChanged = z;
        loadData();
    }

    public boolean getMediaItemIsSending(int i) {
        SendFileState sendFileState = this.mSendFileStateMap.get(Integer.valueOf(i));
        if (sendFileState == null) {
            return false;
        }
        if (sendFileState.mSendState == 0) {
            return false;
        }
        return true;
    }

    public void addMediaState(String str, int i) {
        SendFileState sendFileState = new SendFileState();
        sendFileState.mFileId = str;
        sendFileState.mSendState = 1;
        sendFileState.mPosition = i;
        this.mSendFileStateMap.put(Integer.valueOf(i), sendFileState);
        notifyDataSetChanged();
    }

    public void modifyMediaState(String str, int i) {
        for (Integer num : this.mSendFileStateMap.keySet()) {
            SendFileState sendFileState = this.mSendFileStateMap.get(num);
            if (sendFileState.mFileId.equals(str)) {
                if (i != 0) {
                    sendFileState.mSendState = i;
                } else if (sendFileState.mSendState != 3) {
                    sendFileState.mSendState = 0;
                } else {
                    return;
                }
                notifyDataSetChanged();
                return;
            }
        }
    }

    public void clearSendState() {
        if (this.mSendFileStateMap != null && this.mSendFileStateMap.size() > 0) {
            this.mSendFileStateMap.clear();
            notifyDataSetChanged();
        }
    }

    public void removeItem2(int i) {
        if (i >= 0 && i < this.mMediaList.size()) {
            this.mMediaList.remove(this.mMediaList.get(i));
            notifyDataSetChanged();
        }
    }

    public void removeItem(int i) {
        if (i >= 0 && i < this.mMediaList.size()) {
            T t = this.mMediaList.get(i);
            this.mSendFileStateMap.remove(Integer.valueOf(i));
            this.mMediaList.remove(t);
            notifyDataSetChanged();
        }
    }

    public int getCount() {
        return this.mMediaList.size();
    }

    public Object getItem(int i) {
        return this.mMediaList.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = createConvertView();
        }
        T t = this.mMediaList.get(i);
        BaseViewHolder baseViewHolder = (BaseViewHolder) view.getTag();
        if (baseViewHolder != null) {
            if (getMediaItemIsSending(i)) {
                baseViewHolder.mCheckBox.setVisibility(0);
            } else {
                baseViewHolder.mCheckBox.setVisibility(8);
            }
        }
        fillConvertView(view, t, i);
        return view;
    }

    public void onLocalMediaLoaderFinish(LocalMediaLoader<T> localMediaLoader, ArrayList<T> arrayList, boolean z) {
        updateMediaList(arrayList, z);
        if (this.mListener != null && this.mIsNeedNotifyDataSetChanged) {
            this.mListener.onWifiTransferMediaAdapterLoadDataFinish(this, getCount());
        }
    }

    public void onLocalMediaLoaderOver() {
        if (this.mListener != null) {
            this.mListener.onWifiTransferMediaAdapterLoaderDataOver();
        }
    }

    public synchronized void updateMediaList(ArrayList<T> arrayList, boolean z) {
        if (arrayList != null) {
            if (arrayList.size() > 0) {
                if (z) {
                    this.mMediaList.clear();
                }
                this.mMediaList.addAll(arrayList);
                if (this.mIsNeedNotifyDataSetChanged) {
                    notifyDataSetChanged();
                }
            }
        }
    }

    /* compiled from: ProGuard */
    public class BaseViewHolder {
        public ImageView mCheckBox;
        public TXImageView mImageView;
        public TextView mInfoLabel;
        public TextView mTvCheckBox;

        public BaseViewHolder() {
        }
    }

    /* compiled from: ProGuard */
    public class SendFileState {
        public static final int SendFileState_ASK = 1;
        public static final int SendFileState_NO = 0;
        public static final int SendFileState_Sending = 2;
        public static final int SendFileState_finish = 3;
        public String mFileId;
        public int mPosition;
        public int mSendState = 0;

        public SendFileState() {
        }
    }

    public void setIsNeedNotifyDataSetChanged(boolean z) {
        this.mIsNeedNotifyDataSetChanged = z;
        if (z && this.mListener != null) {
            this.mListener.onWifiTransferMediaAdapterLoadDataFinish(this, getCount());
        }
    }

    public void setListener(IWifiTransferMediaAdapterListener iWifiTransferMediaAdapterListener) {
        this.mListener = iWifiTransferMediaAdapterListener;
    }
}
