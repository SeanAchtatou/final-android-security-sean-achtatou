package com.tencent.assistant.component.listener;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.Map;

/* compiled from: ProGuard */
public abstract class OnTMAItemClickListener implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private int f678a = 0;

    public abstract void onTMAItemClick(AdapterView<?> adapterView, View view, int i, long j);

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        onTMAItemClick(adapterView, view, i, j);
        this.f678a = i;
        a(view);
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
        Context context = view.getContext();
        if (context instanceof BaseActivity) {
            BaseActivity baseActivity = (BaseActivity) context;
            int f = baseActivity.f();
            int m = baseActivity.m();
            String sTSlotId = getSTSlotId();
            if (TextUtils.isEmpty(sTSlotId) && (view.getTag(R.id.tma_st_slot_tag) instanceof String)) {
                sTSlotId = (String) view.getTag(R.id.tma_st_slot_tag);
            }
            STInfoV2 sTInfoV2 = new STInfoV2(f, sTSlotId, m, STConst.ST_DEFAULT_SLOT, 200);
            sTInfoV2.recommendId = getOtherData();
            l.a(sTInfoV2, getSTParameter());
        }
    }

    public int getPosition() {
        return this.f678a;
    }

    public String getSTSlotId() {
        return Constants.STR_EMPTY;
    }

    public Map<String, String> getSTParameter() {
        return null;
    }

    public byte[] getOtherData() {
        return null;
    }
}
