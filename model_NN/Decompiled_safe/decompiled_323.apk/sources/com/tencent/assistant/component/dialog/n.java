package com.tencent.assistant.component.dialog;

import android.content.DialogInterface;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class n implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f660a;

    n(AppConst.TwoBtnDialogInfo twoBtnDialogInfo) {
        this.f660a = twoBtnDialogInfo;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f660a.onCancell();
    }
}
