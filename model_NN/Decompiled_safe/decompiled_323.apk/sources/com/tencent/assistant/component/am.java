package com.tencent.assistant.component;

import android.content.Intent;
import android.view.View;
import com.tencent.nucleus.manager.about.HelperFeedbackActivity;

/* compiled from: ProGuard */
class am implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleView f634a;

    am(SecondNavigationTitleView secondNavigationTitleView) {
        this.f634a = secondNavigationTitleView;
    }

    public void onClick(View view) {
        this.f634a.context.startActivity(new Intent(this.f634a.context, HelperFeedbackActivity.class));
    }
}
