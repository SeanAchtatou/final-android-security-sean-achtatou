package com.tencent.assistant.activity.debug;

import android.view.View;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class am implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f486a;

    am(DActivity dActivity) {
        this.f486a = dActivity;
    }

    public void onClick(View view) {
        TemporaryThreadManager.get().start(new an(this));
    }
}
