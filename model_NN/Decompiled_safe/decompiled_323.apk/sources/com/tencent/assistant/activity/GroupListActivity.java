package com.tencent.assistant.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.k;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.g;
import com.tencent.assistant.module.y;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: ProGuard */
public class GroupListActivity extends BaseActivity implements ITXRefreshListViewListener, g {
    /* access modifiers changed from: private */
    public TextView A = null;
    private int B = 3;
    /* access modifiers changed from: private */
    public int C = 0;
    /* access modifiers changed from: private */
    public int D;
    /* access modifiers changed from: private */
    public int E = 0;
    /* access modifiers changed from: private */
    public int F = 0;
    private ApkResCallback G = new av(this);
    private boolean H = false;
    private View.OnClickListener I = new az(this);
    private AbsListView.OnScrollListener J = new ba(this);
    /* access modifiers changed from: private */
    public TXExpandableListView n;
    private SecondNavigationTitleViewV5 u;
    private NormalErrorRecommendPage v;
    private LoadingView w;
    /* access modifiers changed from: private */
    public k x;
    /* access modifiers changed from: private */
    public y y;
    /* access modifiers changed from: private */
    public RelativeLayout z = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.group_applist_layout);
            t();
            v();
            u();
            q();
        } catch (Throwable th) {
            th.printStackTrace();
            this.H = true;
            finish();
        }
    }

    public int f() {
        if (this.F == 1) {
            return STConst.ST_PAGE_NECESSARY;
        }
        if (this.F == 2) {
            return STConst.ST_PAGE_HOT;
        }
        return 2000;
    }

    public boolean g() {
        return false;
    }

    private void t() {
        this.F = getIntent().getIntExtra("com.tencent.assistant.APP_GROUP_TYPE", 1);
    }

    private void u() {
        this.v = (NormalErrorRecommendPage) findViewById(R.id.network_error);
        this.v.setButtonClickListener(this.I);
        this.w = (LoadingView) findViewById(R.id.loading_view);
        this.w.setVisibility(0);
        this.n = (TXExpandableListView) findViewById(R.id.group_list);
        this.n.setGroupIndicator(null);
        this.n.setDivider(null);
        this.n.setChildDivider(null);
        this.n.setSelector(R.drawable.transparent_selector);
        this.n.setRefreshListViewListener(this);
        this.n.setOnScrollListener(this.J);
        this.n.setOnGroupClickListener(new ax(this));
        this.x = new k(this, this.n, f());
        this.y = new y(true);
        TemporaryThreadManager.get().start(new ay(this));
        this.n.setAdapter(this.x);
        this.E = this.x.getGroupCount();
        this.A = (TextView) findViewById(R.id.group_title);
    }

    private void v() {
        String string;
        this.u = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.u.a(this);
        if (2 == this.F) {
            string = getResources().getString(R.string.hot);
        } else {
            string = getResources().getString(R.string.necessary);
        }
        this.u.b(string);
        this.u.i();
        this.u.bringToFront();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.H) {
            this.n = null;
            this.y.a();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.H) {
            this.x.b();
            this.y.unregister(this);
            ApkResourceManager.getInstance().unRegisterApkResCallback(this.G);
            this.u.m();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.H) {
            this.x.a();
            this.y.register(this);
            ApkResourceManager.getInstance().registerApkResCallback(this.G);
            this.u.l();
        }
    }

    private void b(int i) {
        this.n.setVisibility(8);
        this.v.setVisibility(0);
        this.w.setVisibility(8);
        this.v.setErrorType(i);
    }

    private void w() {
        this.n.setVisibility(0);
        this.w.setVisibility(8);
        this.v.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void x() {
        this.n.setVisibility(8);
        this.w.setVisibility(0);
        this.v.setVisibility(8);
    }

    public void a(int i, int i2, boolean z2, Map<AppGroupInfo, ArrayList<SimpleAppModel>> map, ArrayList<Long> arrayList, boolean z3) {
        if (i2 == 0) {
            if (map.size() > 0) {
                w();
                this.x.a(z2, map, this.y.c(), z3);
                for (int i3 = 0; i3 < this.x.getGroupCount(); i3++) {
                    this.n.expandGroup(i3);
                }
                this.E = this.x.getGroupCount();
            } else if (z2 && this.x != null && this.x.getGroupCount() == 0) {
                b(10);
            }
            this.n.onRefreshComplete(z3);
        } else if (!z2) {
            this.n.onRefreshComplete(z3);
        } else if (-800 == i2) {
            b(30);
        } else if (this.B <= 0) {
            b(20);
        } else {
            this.y.a(this.F);
            this.B--;
        }
    }

    /* access modifiers changed from: private */
    public int y() {
        int i = this.D;
        int pointToPosition = this.n.pointToPosition(0, this.D);
        if (pointToPosition == -1 || ExpandableListView.getPackedPositionGroup(this.n.getExpandableListPosition(pointToPosition)) == this.C) {
            return i;
        }
        View expandChildAt = this.n.getExpandChildAt(pointToPosition - this.n.getFirstVisiblePosition());
        if (expandChildAt != null) {
            return expandChildAt.getTop();
        }
        return 100;
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (TXRefreshScrollViewBase.RefreshState.RESET == this.n.getMoreRefreshState() && TXScrollViewBase.ScrollState.ScrollState_FromEnd == scrollState) {
            this.y.d();
        }
    }
}
