package com.tencent.assistant.activity;

import android.os.IBinder;
import com.tencent.assistant.utils.XLog;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.optimize.f;
import org.json.JSONException;

/* compiled from: ProGuard */
class i extends f {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkMgrActivity f537a;

    i(ApkMgrActivity apkMgrActivity) {
        this.f537a = apkMgrActivity;
    }

    public IBinder asBinder() {
        return null;
    }

    public void c() {
        XLog.d("ApkMgrActivity", "onScanStarted.");
    }

    public void a(int i) {
        XLog.d("ApkMgrActivity", "onScanProgressChanged. progress = " + i);
    }

    public void b() {
        if (!this.f537a.L) {
            if (this.f537a.S.hasMessages(110011)) {
                this.f537a.S.removeMessages(110011);
            }
            this.f537a.S.sendMessage(this.f537a.S.obtainMessage(110011));
            XLog.i("ApkMgrActivity", "onScanFinished in ApkMgrActivity");
        }
    }

    public void a() {
        XLog.d("ApkMgrActivity", "onScanCanceled in ApkMgrActivity");
        if (!this.f537a.L) {
            if (this.f537a.S.hasMessages(110011)) {
                this.f537a.S.removeMessages(110011);
            }
            this.f537a.S.sendMessage(this.f537a.S.obtainMessage(110011));
        }
    }

    public void a(int i, DataEntity dataEntity) {
        XLog.i("ig_rubbish", dataEntity.toString());
        try {
            if (!dataEntity.getBoolean("rubbish.suggest")) {
                return;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (!this.f537a.L) {
            boolean unused = this.f537a.L = true;
            if (this.f537a.S.hasMessages(110011)) {
                this.f537a.S.removeMessages(110011);
            }
            this.f537a.S.sendMessage(this.f537a.S.obtainMessage(110011));
        }
        XLog.i("ApkMgrActivity", "onRubbishFound in ApkMgrActivity");
    }
}
