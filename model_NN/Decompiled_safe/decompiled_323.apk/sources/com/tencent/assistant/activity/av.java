package com.tencent.assistant.activity;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import java.util.List;

/* compiled from: ProGuard */
class av extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GroupListActivity f406a;

    av(GroupListActivity groupListActivity) {
        this.f406a = groupListActivity;
    }

    public void onLoadInstalledApkSuccess(List<LocalApkInfo> list) {
        this.f406a.runOnUiThread(new aw(this));
    }
}
