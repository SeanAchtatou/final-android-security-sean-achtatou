package com.tencent.assistant.activity.pictureprocessor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.tencent.assistant.component.Shieldable;
import java.lang.reflect.Field;

/* compiled from: ProGuard */
public class MyViewPager extends ViewPager implements Shieldable {

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f547a;
    private Paint b = new Paint(1);
    private boolean c = true;

    public MyViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MyViewPager(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        if (this.f547a != null) {
            int width = this.f547a.getWidth();
            int height = this.f547a.getHeight();
            int count = getAdapter().getCount();
            int scrollX = getScrollX();
            int width2 = (getWidth() * height) / getHeight();
            int width3 = (((width - width2) / (count - 1)) * scrollX) / getWidth();
            canvas.drawBitmap(this.f547a, new Rect(width3, 0, width2 + width3, height), new Rect(scrollX, 0, getWidth() + scrollX, getHeight()), this.b);
        }
        super.dispatchDraw(canvas);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0 || this.c) {
            try {
                return super.onInterceptTouchEvent(motionEvent);
            } catch (Exception e) {
                return false;
            }
        } else {
            Class<ViewPager> cls = ViewPager.class;
            try {
                Field declaredField = cls.getDeclaredField("mLastMotionX");
                declaredField.setAccessible(true);
                declaredField.set(this, Float.valueOf(motionEvent.getX()));
                cls.getDeclaredField("mInitialMotionX");
                declaredField.setAccessible(true);
                declaredField.set(this, Float.valueOf(motionEvent.getX()));
                return false;
            } catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException e2) {
                return false;
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        try {
            return super.onTouchEvent(motionEvent);
        } catch (Exception e) {
            return false;
        }
    }

    public void setShielded(boolean z) {
        this.c = !z;
    }
}
