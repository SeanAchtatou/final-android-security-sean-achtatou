package com.tencent.assistant.activity.pictureprocessor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.thumbnailCache.k;
import com.tencent.assistant.thumbnailCache.o;
import com.tencent.assistant.thumbnailCache.p;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class PicView extends LinearLayout implements c, p {

    /* renamed from: a  reason: collision with root package name */
    private Context f548a;
    private LayoutInflater b;
    private String c;
    private AnimationImageView d;
    /* access modifiers changed from: private */
    public TextView e;
    /* access modifiers changed from: private */
    public DrawCallbackProgressBar f;
    private Bitmap g;
    private boolean h;
    /* access modifiers changed from: private */
    public g i;
    private ArrayList<String> j = new ArrayList<>();
    /* access modifiers changed from: private */
    public Handler k = null;
    private String l;
    /* access modifiers changed from: private */
    public o m = null;
    /* access modifiers changed from: private */
    public boolean n = false;
    /* access modifiers changed from: private */
    public boolean o = false;
    private float p = 90.0f;
    private float q = 50.0f;
    private Animation.AnimationListener r = new f(this);

    public PicView(Context context) {
        super(context);
        this.f548a = context;
        g();
    }

    public PicView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f548a = context;
        g();
    }

    private void g() {
        this.b = LayoutInflater.from(getContext());
        i();
        h();
    }

    private void h() {
        this.k = new d(this);
    }

    /* access modifiers changed from: private */
    public void a(o oVar) {
        if (oVar != null && oVar.c().equals(this.c)) {
            Bitmap bitmap = oVar.f;
            this.g = bitmap;
            if (bitmap != null && !bitmap.isRecycled()) {
                this.h = true;
                this.d.setVisibility(0);
                this.d.a(bitmap);
                this.f.setVisibility(8);
                this.e.setVisibility(8);
            }
        }
    }

    private void i() {
        View inflate = this.b.inflate((int) R.layout.show_pic_view_item, this);
        this.d = (AnimationImageView) inflate.findViewById(R.id.img);
        try {
            this.d.b(getResources().getDrawable(R.drawable.wb_scrollbar));
            this.d.a(getResources().getDrawable(R.drawable.wb_scrollbar_wide));
        } catch (Throwable th) {
            t.a().b();
        }
        this.f = (DrawCallbackProgressBar) inflate.findViewById(R.id.img_progress);
        this.f.a(this);
        this.e = (TextView) inflate.findViewById(R.id.txt_progress);
        this.d.setOnClickListener(new e(this));
        this.d.b(this.r);
        this.e.setText(getResources().getString(R.string.is_connecting));
    }

    public void b() {
    }

    public void a(String str, g gVar) {
        if (gVar != null) {
            this.i = gVar;
        }
        if (str != null) {
            this.c = str;
            a(str, (String) null, 2);
        }
    }

    public void a(String str, String str2, g gVar) {
        if (gVar != null) {
            this.i = gVar;
        }
        if (str != null) {
            this.c = str;
            this.l = str2;
            a(str, str2, 2);
        }
    }

    public String c() {
        return this.c;
    }

    public void d() {
    }

    public void e() {
        d();
    }

    private void a(String str, String str2, int i2) {
        Bitmap a2;
        this.g = k.b().a(str, i2, this);
        if (this.g != null) {
            this.h = true;
            this.d.a(this.g);
            this.f.setVisibility(8);
            this.e.setVisibility(8);
            return;
        }
        this.j.add(str);
        this.f.setVisibility(0);
        this.e.setVisibility(0);
        if (str2 == null || (a2 = k.b().a(str2, i2, null)) == null) {
            try {
                this.d.a(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.pic_defaule));
            } catch (Throwable th) {
                t.a().b();
            }
        } else {
            this.d.a(a2);
        }
    }

    public void f() {
        this.d.a(a(this.d.a(), this.d.b(), (int) R.drawable.appdetail_picbreak));
        this.f.setVisibility(8);
        this.e.setVisibility(8);
    }

    public void a() {
        float f2 = 99.99f;
        if (this.q >= 0.0f && this.p >= 0.0f) {
            float random = (float) (Math.random() * 0.57d);
            if (this.q + random < this.p + 10.0f && this.q < 100.0f) {
                this.q = random + this.q;
            }
            if (this.q < 99.99f) {
                f2 = this.q;
            }
            this.q = f2;
            this.e.setText(String.format("%.1f", Float.valueOf(this.q)) + "%");
        }
    }

    public void a(int[] iArr) {
        this.o = true;
        this.d.a(iArr);
    }

    public void b(int[] iArr) {
        this.d.b(iArr);
    }

    public boolean a(Animation.AnimationListener animationListener) {
        if (this.d != null) {
            return this.d.a(animationListener);
        }
        return false;
    }

    public void thumbnailRequestCompleted(o oVar) {
        if (this.j.contains(oVar.c())) {
            if (this.o) {
                this.m = oVar;
            } else {
                this.k.obtainMessage(0, oVar).sendToTarget();
            }
        }
    }

    public void thumbnailRequestFailed(o oVar) {
        if (this.j.contains(oVar.c())) {
            if (this.o) {
                this.n = true;
            } else {
                this.k.sendEmptyMessageDelayed(1, 0);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.Bitmap a(int r11, int r12, int r13) {
        /*
            r10 = this;
            r8 = 1
            r2 = 0
            r0 = 0
            if (r11 <= 0) goto L_0x0007
            if (r12 > 0) goto L_0x0008
        L_0x0007:
            return r0
        L_0x0008:
            android.content.Context r1 = r10.getContext()     // Catch:{ Throwable -> 0x0093 }
            android.content.res.Resources r1 = r1.getResources()     // Catch:{ Throwable -> 0x0093 }
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeResource(r1, r13)     // Catch:{ Throwable -> 0x0093 }
            if (r4 == 0) goto L_0x0007
            int r3 = r4.getWidth()     // Catch:{ Throwable -> 0x00a3 }
            int r1 = r4.getHeight()     // Catch:{ Throwable -> 0x00a7 }
            int r5 = r11 / r3
            int r5 = r5 * r1
            int r12 = r5 + 100
            android.graphics.Bitmap$Config r5 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ Throwable -> 0x00aa }
            android.graphics.Bitmap r5 = android.graphics.Bitmap.createBitmap(r11, r12, r5)     // Catch:{ Throwable -> 0x00aa }
            r9 = r1
            r1 = r5
            r5 = r4
            r4 = r3
            r3 = r9
        L_0x002e:
            if (r1 == 0) goto L_0x0007
            android.graphics.Canvas r0 = new android.graphics.Canvas
            r0.<init>(r1)
            android.graphics.Paint r6 = new android.graphics.Paint
            r6.<init>(r8)
            int r4 = r11 - r4
            int r4 = r4 / 2
            float r4 = (float) r4
            int r7 = r12 - r3
            int r7 = r7 + -100
            int r7 = r7 / 2
            float r7 = (float) r7
            r0.drawBitmap(r5, r4, r7, r6)
            android.graphics.Paint r4 = new android.graphics.Paint
            r4.<init>(r8)
            android.graphics.Paint$Align r5 = android.graphics.Paint.Align.CENTER
            r4.setTextAlign(r5)
            r5 = 1108082688(0x420c0000, float:35.0)
            r4.setTextSize(r5)
            r5 = -1
            r4.setColor(r5)
            android.content.res.Resources r5 = r10.getResources()
            r6 = 2131362323(0x7f0a0213, float:1.8344423E38)
            java.lang.String r5 = r5.getString(r6)
            android.graphics.Rect r6 = new android.graphics.Rect
            r6.<init>()
            int r7 = r5.length()
            r4.getTextBounds(r5, r2, r7, r6)
            int r2 = r6.bottom
            int r6 = r6.top
            int r2 = r2 - r6
            int r6 = r11 / 2
            float r6 = (float) r6
            int r3 = r3 + r12
            int r3 = r3 + 100
            int r3 = r3 / 2
            int r2 = r2 / 2
            int r2 = r3 - r2
            float r2 = (float) r2
            r0.drawText(r5, r6, r2, r4)
            r2 = 31
            r0.save(r2)
            r0.restore()
            r0 = r1
            goto L_0x0007
        L_0x0093:
            r1 = move-exception
            r1 = r2
            r3 = r2
            r4 = r0
        L_0x0097:
            com.tencent.assistant.manager.t r5 = com.tencent.assistant.manager.t.a()
            r5.b()
            r5 = r4
            r4 = r3
            r3 = r1
            r1 = r0
            goto L_0x002e
        L_0x00a3:
            r1 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x0097
        L_0x00a7:
            r1 = move-exception
            r1 = r2
            goto L_0x0097
        L_0x00aa:
            r5 = move-exception
            goto L_0x0097
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.activity.pictureprocessor.PicView.a(int, int, int):android.graphics.Bitmap");
    }
}
