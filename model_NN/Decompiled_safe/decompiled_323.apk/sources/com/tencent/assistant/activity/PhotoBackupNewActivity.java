package com.tencent.assistant.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import com.qq.AppService.AppService;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.component.ConnectSuccessTopView;
import com.tencent.assistant.component.CustomBackupRelativeLayout;
import com.tencent.assistant.component.PCListLinearLayout;
import com.tencent.assistant.component.PhotoBackupBottomView;
import com.tencent.assistant.component.PhotoBackupMiddleGridView;
import com.tencent.assistant.component.WaitingPCConnectView;
import com.tencent.assistant.component.WaitingPCConnectViewNew;
import com.tencent.assistant.d.a;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.event.listener.b;
import com.tencent.assistant.model.OnlinePCListItemModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.business.t;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connector.ConnectionActivity;
import com.tencent.connector.ipc.ConnectionType;
import com.tencent.nucleus.manager.main.AssistantTabActivity;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/* compiled from: ProGuard */
public class PhotoBackupNewActivity extends BaseActivity implements UIEventListener, b {
    private TextView A;
    private boolean B = false;
    private TextView C;
    private TextView D;
    private TextView E;
    private TextView F;
    private int G = 0;
    private CustomBackupRelativeLayout H;
    private View I;
    private View J;
    private WaitingPCConnectViewNew K;
    private WaitingPCConnectView L;
    private View M;
    private t N = new t();
    private Timer O = null;
    private TimerTask P = null;
    private View.OnClickListener Q = new bv(this);
    /* access modifiers changed from: private */
    public boolean R = false;
    private a S = new bx(this);
    private PCListLinearLayout.PCListClickCallback T = new by(this);
    public boolean n = true;
    private SecondNavigationTitleViewV5 u;
    private ConnectSuccessTopView v;
    private PhotoBackupMiddleGridView w;
    private PhotoBackupBottomView x;
    private TextView y;
    /* access modifiers changed from: private */
    public PCListLinearLayout z;

    public int f() {
        return STConst.ST_PAGE_PHOTO_BACKUP;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_photo_backup_newlayout);
        E();
        B();
        A();
        overridePendingTransition(R.anim.activity_right_in, R.anim.activity_left_out);
        b(getIntent().getIntExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, 2000));
        z();
    }

    private void z() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", com.tencent.assistant.utils.t.g());
        XLog.d("beacon", "beacon report >> expose_photobackup. " + hashMap.toString());
        com.tencent.beacon.event.a.a("expose_photobackup", true, -1, -1, hashMap, true);
    }

    public void b(int i) {
        l.a(new STInfoV2(f(), STConst.ST_DEFAULT_SLOT, i, STConst.ST_DEFAULT_SLOT, 100));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void A() {
        if (!com.tencent.connector.ipc.a.a().c()) {
            Context applicationContext = getApplicationContext();
            Intent intent = new Intent();
            intent.setClass(applicationContext, AppService.class);
            intent.putExtra("isWifi", true);
            applicationContext.startService(intent);
        }
    }

    private void B() {
        if (this.A != null) {
            this.A.setOnClickListener(this.Q);
        }
        if (this.C != null) {
            this.C.setOnClickListener(this.Q);
        }
        if (this.D != null) {
            this.D.setOnClickListener(this.Q);
        }
        if (this.E != null) {
            this.E.setOnClickListener(this.Q);
        }
        if (this.F != null) {
            this.F.setOnClickListener(this.Q);
        }
    }

    public void c(int i) {
        switch (i) {
            case 1:
                this.I.setVisibility(0);
                this.J.setVisibility(8);
                this.K.setVisibility(8);
                this.L.setVisibility(8);
                this.M.setVisibility(8);
                break;
            case 2:
                this.I.setVisibility(8);
                this.J.setVisibility(0);
                this.K.setVisibility(8);
                this.L.setVisibility(8);
                this.M.setVisibility(8);
                break;
            case 3:
                this.I.setVisibility(8);
                this.J.setVisibility(8);
                this.K.setVisibility(0);
                this.L.setVisibility(8);
                this.M.setVisibility(8);
                break;
            case 4:
                this.I.setVisibility(8);
                this.J.setVisibility(8);
                this.K.setVisibility(8);
                this.L.setVisibility(0);
                this.M.setVisibility(8);
                break;
            case 5:
                this.I.setVisibility(8);
                this.J.setVisibility(8);
                this.K.setVisibility(8);
                this.L.setVisibility(8);
                this.M.setVisibility(0);
                break;
        }
        this.G = i;
    }

    public void d(int i) {
        switch (i) {
            case 1:
                this.I.setVisibility(8);
                this.z.stopTimer();
                break;
            case 2:
                this.J.setVisibility(8);
                break;
            case 3:
                this.K.setVisibility(8);
                u();
                break;
            case 4:
                this.L.setVisibility(8);
                break;
            case 5:
                this.M.setVisibility(8);
                break;
        }
        this.G = 0;
    }

    public void t() {
        if (this.n) {
            this.A.setText((int) R.string.photo_backup_cancel);
        } else {
            this.A.setText((int) R.string.photo_select_cancel);
        }
        c(4);
        this.L.getPcListStart();
        com.tencent.assistant.d.b.a().c();
    }

    public void b(String str, String str2) {
        if (str != null && !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            c(3);
            this.K.updatePc(str);
            com.tencent.assistant.d.b.a().a(str2);
            C();
        }
    }

    private void C() {
        this.P = new bt(this);
        this.O = new Timer(true);
        this.O.schedule(this.P, 15000);
    }

    /* access modifiers changed from: private */
    public void D() {
        com.tencent.connector.ipc.a.a().h();
        d(3);
        e(0);
        t();
    }

    public void u() {
        if (this.P != null) {
            this.P.cancel();
            this.P = null;
        }
        if (this.O != null) {
            this.O.cancel();
            this.O = null;
        }
    }

    private void E() {
        H();
        K();
        J();
        I();
        F();
    }

    private void F() {
        this.E = (TextView) findViewById(R.id.ok);
        this.F = (TextView) findViewById(R.id.cancel);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        boolean z2 = false;
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        switch (this.G) {
            case 1:
                d(1);
                if (com.tencent.connector.ipc.a.a().d()) {
                    z2 = true;
                    break;
                }
                break;
            case 2:
                d(2);
                break;
            case 3:
                d(3);
                break;
            case 4:
                d(4);
                if (com.tencent.connector.ipc.a.a().d()) {
                    z2 = true;
                    break;
                }
                break;
            case 5:
                d(5);
                if (com.tencent.connector.ipc.a.a().d()) {
                    z2 = true;
                    break;
                }
                break;
        }
        if (z2) {
            return true;
        }
        G();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void G() {
        if (!this.s) {
            finish();
            return;
        }
        Intent intent = new Intent(this, AssistantTabActivity.class);
        intent.setFlags(67108864);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        intent.putExtra(com.tencent.assistant.a.a.G, true);
        startActivity(intent);
        this.s = false;
        finish();
        XLog.i("PhotoBackupNewActivity", "PhotoBackupNewActivity >> key back finish");
    }

    /* access modifiers changed from: protected */
    public void v() {
        Intent intent = new Intent();
        intent.setClass(this, ConnectionActivity.class);
        startActivity(intent);
    }

    private void H() {
        this.H = (CustomBackupRelativeLayout) findViewById(R.id.backup_layout);
        this.I = findViewById(R.id.backupPcListLayout);
        this.J = findViewById(R.id.wifiDisbaleLayout);
        this.K = (WaitingPCConnectViewNew) findViewById(R.id.waitingPCLayout);
        this.L = (WaitingPCConnectView) findViewById(R.id.getPCLoading);
        this.M = findViewById(R.id.tipsDisconnectLayout);
    }

    private void I() {
        this.C = (TextView) findViewById(R.id.no_backup);
        this.D = (TextView) findViewById(R.id.connect_pc);
    }

    private void J() {
        this.y = (TextView) findViewById(R.id.title);
        this.z = (PCListLinearLayout) findViewById(R.id.pcList);
        this.z.setPCListClickCallback(this.T);
        this.A = (TextView) findViewById(R.id.cancel_backup);
    }

    private void K() {
        this.u = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.v = (ConnectSuccessTopView) findViewById(R.id.topRightView);
        this.w = (PhotoBackupMiddleGridView) findViewById(R.id.middleView);
        this.x = (PhotoBackupBottomView) findViewById(R.id.bottom_view);
        this.u.a(this);
        this.u.b(getString(R.string.photo_backup_manager));
        this.u.d();
        this.u.c(new bw(this));
        this.v.setActivity(this);
        this.w.setActivity(this);
        this.x.setActivity(this);
        this.x.setBacupEnable(false);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AstApp.i().k().addConnectionEventListener(5002, this);
        AstApp.i().k().addConnectionEventListener(5001, this);
        AstApp.i().k().addConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_PC_PING, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FAILED, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FINISH, this);
        if (!com.tencent.connector.ipc.a.f2417a && this.w.getCount() > 0) {
            Q();
        }
        L();
        N();
        com.tencent.assistant.d.b.a().b();
        if (this.w != null) {
            this.w.onResume();
        }
        if (this.u != null) {
            this.u.l();
        }
    }

    private void L() {
        if (this.w != null) {
            this.w.checkIDs(com.tencent.connector.ipc.a.b);
        }
        if (com.tencent.connector.ipc.a.f2417a) {
            if (this.H != null) {
                this.H.setClickEnable(false);
            }
            if (this.x != null) {
                this.x.setBackupStart();
            }
        }
    }

    private void M() {
        com.tencent.assistant.d.b.a().b(this.S);
    }

    private void N() {
        com.tencent.assistant.d.b.a().a(this.S);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.G != 0) {
            d(this.G);
        }
        T();
        AstApp.i().k().removeConnectionEventListener(5002, this);
        AstApp.i().k().removeConnectionEventListener(5001, this);
        AstApp.i().k().removeConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_PC_PING, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_START, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FAILED, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FINISH, this);
        M();
        if (this.w != null) {
            this.w.onPause();
        }
        if (this.u != null) {
            this.u.m();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        byte b;
        super.onDestroy();
        if (this.w != null) {
            this.w.onDestroy();
        }
        if (!this.B) {
            ConnectionType e = com.tencent.connector.ipc.a.a().e();
            if (e == ConnectionType.USB) {
                b = 1;
            } else {
                b = e == ConnectionType.WIFI ? (byte) 2 : 0;
            }
            if (this.N != null) {
                this.N.a(b, false, 0);
            }
        }
    }

    public void e(int i) {
        if (i == 1) {
            this.y.setBackgroundResource(R.drawable.apptoast_bg_top);
            this.y.setText((int) R.string.photo_backup_please_select_backup_to_pc);
            this.y.setTextColor(getResources().getColor(R.color.report_reason));
            return;
        }
        this.y.setBackgroundResource(R.drawable.apptoast_bg_top_warn);
        this.y.setText((int) R.string.photo_backup_connect_failed);
        this.y.setTextColor(getResources().getColor(R.color.white));
    }

    public void w() {
        int[] checkedItemIds;
        if (this.w != null && com.tencent.connector.ipc.a.a().d() && (checkedItemIds = this.w.getCheckedItemIds()) != null && checkedItemIds.length > 0) {
            this.B = true;
            com.tencent.connector.ipc.a.b = checkedItemIds;
            com.tencent.connector.ipc.a.a().a(checkedItemIds);
            ConnectionType e = com.tencent.connector.ipc.a.a().e();
            byte b = 0;
            if (e == ConnectionType.USB) {
                b = 1;
            } else if (e == ConnectionType.WIFI) {
                b = 2;
            }
            if (this.N != null) {
                this.N.a(b, true, (short) checkedItemIds.length);
            }
        }
    }

    private void O() {
        if (this.x != null) {
            this.x.setBackupStart();
        }
        if (this.H != null) {
            this.H.setClickEnable(false);
        }
    }

    private void P() {
        if (this.x != null) {
            this.x.setBackupFailed();
        }
        if (this.w != null) {
            this.w.clearChoices();
        }
        if (this.H != null) {
            this.H.setClickEnable(true);
        }
    }

    private void Q() {
        if (this.x != null) {
            this.x.setBackupFinish();
        }
        if (this.w != null) {
            this.w.clearChoices();
        }
        if (this.H != null) {
            this.H.setClickEnable(true);
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_START /*1035*/:
                Log.d("PhotoBackupNewActivity", "backup start");
                O();
                return;
            case EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FAILED /*1036*/:
                Log.d("PhotoBackupNewActivity", "backup failed");
                P();
                return;
            case EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FINISH /*1037*/:
                Log.d("PhotoBackupNewActivity", "backup finish");
                Q();
                return;
            default:
                return;
        }
    }

    public void a(Message message) {
        switch (message.what) {
            case 5001:
                Log.d("PhotoBackupNewActivity", "event_disconnect");
                R();
                return;
            case 5002:
                Log.d("PhotoBackupNewActivity", "event_connect");
                if (this.G != 0) {
                    d(this.G);
                }
                ConnectionType connectionType = (ConnectionType) message.obj;
                if (connectionType == ConnectionType.USB) {
                    x();
                    return;
                } else if (connectionType == ConnectionType.WIFI) {
                    y();
                    return;
                } else {
                    R();
                    return;
                }
            case EventDispatcherEnum.CONNECTION_EVENT_PC_PING /*5008*/:
                Log.d("PhotoBackupNewActivity", "event_pc_ping");
                OnlinePCListItemModel onlinePCListItemModel = (OnlinePCListItemModel) message.obj;
                if (this.z != null) {
                    this.z.updateItem(onlinePCListItemModel);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void R() {
        if (this.G != 0) {
            Log.d("PhotoBackupNewActivity", "hide: " + this.G);
            d(this.G);
        }
        S();
        T();
        com.tencent.assistant.d.b.a().b();
    }

    private void S() {
        if (this.H != null) {
            this.H.setClickEnable(true);
        }
        if (this.x != null) {
            this.x.reset();
        }
    }

    private void T() {
        if (this.v != null && this.v.getVisibility() == 0) {
            this.v.setVisibility(8);
        }
        if (this.w != null) {
            this.w.clearChoices();
            f(0);
        }
    }

    public void f(int i) {
        if (this.x != null) {
            this.x.updateBackupCount(i);
        }
    }

    /* access modifiers changed from: protected */
    public void x() {
        if (this.v != null) {
            this.v.setVisibility(0);
            String f = com.tencent.connector.ipc.a.a().f();
            if (TextUtils.isEmpty(f)) {
                f = "usb";
            }
            this.v.switchConnectModel((byte) 0, f);
        }
        com.qq.k.b.a(AstApp.i(), System.currentTimeMillis());
    }

    /* access modifiers changed from: protected */
    public void y() {
        if (this.v != null) {
            this.v.setVisibility(0);
            String f = com.tencent.connector.ipc.a.a().f();
            if (TextUtils.isEmpty(f)) {
                f = "wifi";
            }
            this.v.switchConnectModel((byte) 1, f);
        }
        com.qq.k.b.a(AstApp.i(), System.currentTimeMillis());
    }
}
