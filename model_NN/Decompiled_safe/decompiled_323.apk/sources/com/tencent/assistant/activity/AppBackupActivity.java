package com.tencent.assistant.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.component.TouchAnalizer;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.c;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.BackupApp;
import com.tencent.assistant.protocol.jce.BackupDevice;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bo;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.beacon.event.a;
import com.tencent.nucleus.manager.appbackup.BackupAppListAdapter;
import com.tencent.nucleus.manager.appbackup.BackupApplistDialog;
import com.tencent.nucleus.manager.appbackup.BackupDeviceAdapter;
import com.tencent.nucleus.manager.appbackup.DeviceListDialog;
import com.tencent.nucleus.manager.appbackup.m;
import com.tencent.nucleus.manager.appbackup.o;
import com.tencent.nucleus.manager.appbackup.s;
import com.tencent.nucleus.manager.appbackup.t;
import com.tencent.nucleus.manager.main.AssistantTabActivity;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.pangu.utils.d;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: ProGuard */
public class AppBackupActivity extends BaseActivity implements View.OnClickListener, UIEventListener, NetworkMonitor.ConnectivityChangeListener {
    private static int C = 280;
    private static int[] ag = {EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS, EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL, EventDispatcherEnum.UI_EVENT_BACKUP_APPLIST_SUCCESS, EventDispatcherEnum.UI_EVENT_BACKUP_APPLIST_FAIL, EventDispatcherEnum.UI_EVENT_GET_BACKUP_DEVICELIST_SUCCESS, EventDispatcherEnum.UI_EVENT_GET_BACKUP_DEVICELIST_FAIL, EventDispatcherEnum.UI_EVENT_GET_BACKUP_APPS_SUCCESS, EventDispatcherEnum.UI_EVENT_GET_BACKUP_APPS_FAIL};
    /* access modifiers changed from: private */
    public int A = -1;
    private int B = -1;
    private Dialog D;
    /* access modifiers changed from: private */
    public DeviceListDialog E;
    /* access modifiers changed from: private */
    public BackupApplistDialog F;
    private Dialog G;
    private m H;
    private int I = -1;
    private int J = -1;
    private boolean K = false;
    private int L = -1;
    private int M = -1;
    /* access modifiers changed from: private */
    public TXImageView N;
    private ImageView O;
    /* access modifiers changed from: private */
    public TextView P;
    private TextView Q;
    /* access modifiers changed from: private */
    public TextView R;
    /* access modifiers changed from: private */
    public ImageView S;
    /* access modifiers changed from: private */
    public ImageView T;
    private ImageView U;
    private ImageView V;
    /* access modifiers changed from: private */
    public Animation W;
    /* access modifiers changed from: private */
    public Animation X;
    private Animation Y;
    private Animation Z;
    /* access modifiers changed from: private */
    public RelativeLayout aa;
    /* access modifiers changed from: private */
    public ImageView ab;
    /* access modifiers changed from: private */
    public ImageView ac;
    /* access modifiers changed from: private */
    public boolean ad = true;
    private boolean ae;
    /* access modifiers changed from: private */
    public Context af;
    private boolean ah = false;
    private int ai = 0;
    private ApkResCallback.Stub aj = new x(this);
    private Animation.AnimationListener ak = new aa(this);
    private Animation.AnimationListener al = new ab(this);
    /* access modifiers changed from: private */
    public Animation.AnimationListener am = new ad(this);
    private boolean an = true;
    /* access modifiers changed from: private */
    public Handler ao = new w(this);
    /* access modifiers changed from: private */
    public Button n;
    private Button u;
    private SecondNavigationTitleViewV5 v;
    /* access modifiers changed from: private */
    public t w;
    /* access modifiers changed from: private */
    public s x;
    /* access modifiers changed from: private */
    public int y = -1;
    private int z = -1;

    public int f() {
        if (j.a().j()) {
            return STConst.ST_PAGE_APP_BACKUP_LOGIN;
        }
        return STConst.ST_PAGE_APP_BACKUP_NO_LOGIN;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.activity_app_backup_layout);
            x();
            u();
            com.tencent.assistant.manager.t.a().a(this);
            ApkResourceManager.getInstance().registerApkResCallback(this.aj);
            this.w = new t();
            this.x = new s();
            this.ai = d.a(getIntent(), "notification_id", 0);
            this.af = this;
            new Handler().postDelayed(new t(this), 500);
            t();
        } catch (Throwable th) {
            th.printStackTrace();
            this.ah = true;
            finish();
            com.tencent.assistant.manager.t.a().b();
        }
    }

    private void t() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", com.tencent.assistant.utils.t.g());
        XLog.d("beacon", "beacon report >> expose_appbackup. " + hashMap.toString());
        a.a("expose_appbackup", true, -1, -1, hashMap, true);
    }

    private void u() {
        for (int addUIEventListener : ag) {
            AstApp.i().k().addUIEventListener(addUIEventListener, this);
        }
    }

    private void v() {
        for (int removeUIEventListener : ag) {
            AstApp.i().k().removeUIEventListener(removeUIEventListener, this);
        }
    }

    private void w() {
        C = 200;
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.top_layout);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) relativeLayout.getLayoutParams();
        layoutParams.width = TouchAnalizer.CLICK_AREA;
        layoutParams.height = TouchAnalizer.CLICK_AREA;
        relativeLayout.setLayoutParams(layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) ((RelativeLayout) findViewById(R.id.cloud_mask)).getLayoutParams();
        layoutParams2.width = TouchAnalizer.CLICK_AREA;
        layoutParams2.height = TouchAnalizer.CLICK_AREA;
        relativeLayout.setLayoutParams(layoutParams2);
        RelativeLayout relativeLayout2 = (RelativeLayout) findViewById(R.id.auto_backup_layout);
        RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) relativeLayout2.getLayoutParams();
        layoutParams3.topMargin = 20;
        relativeLayout2.setLayoutParams(layoutParams3);
        Button button = (Button) findViewById(R.id.backup_button);
        RelativeLayout.LayoutParams layoutParams4 = (RelativeLayout.LayoutParams) button.getLayoutParams();
        layoutParams4.topMargin = 10;
        button.setLayoutParams(layoutParams4);
        Button button2 = (Button) findViewById(R.id.restore_button);
        RelativeLayout.LayoutParams layoutParams5 = (RelativeLayout.LayoutParams) button2.getLayoutParams();
        layoutParams5.topMargin = 20;
        button2.setLayoutParams(layoutParams5);
        View findViewById = findViewById(R.id.cloud1);
        View findViewById2 = findViewById(R.id.cloud2);
        View findViewById3 = findViewById(R.id.cloud3);
        View findViewById4 = findViewById(R.id.cloud4);
        RelativeLayout.LayoutParams layoutParams6 = (RelativeLayout.LayoutParams) findViewById.getLayoutParams();
        layoutParams6.topMargin -= 100;
        findViewById.setLayoutParams(layoutParams6);
        RelativeLayout.LayoutParams layoutParams7 = (RelativeLayout.LayoutParams) findViewById2.getLayoutParams();
        layoutParams7.topMargin -= 100;
        findViewById2.setLayoutParams(layoutParams7);
        RelativeLayout.LayoutParams layoutParams8 = (RelativeLayout.LayoutParams) findViewById3.getLayoutParams();
        layoutParams8.topMargin -= 100;
        findViewById3.setLayoutParams(layoutParams8);
        RelativeLayout.LayoutParams layoutParams9 = (RelativeLayout.LayoutParams) findViewById4.getLayoutParams();
        layoutParams9.topMargin -= 100;
        findViewById4.setLayoutParams(layoutParams9);
    }

    private void x() {
        if (by.d()) {
            w();
        }
        this.v = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.v.a((Activity) this);
        this.v.b(getResources().getString(R.string.app_backup_title));
        this.v.d();
        this.v.i();
        this.v.c(new z(this));
        this.n = (Button) findViewById(R.id.backup_button);
        this.u = (Button) findViewById(R.id.restore_button);
        this.P = (TextView) findViewById(R.id.nick_name);
        this.P.setClickable(true);
        this.P.setOnClickListener(this);
        this.aa = (RelativeLayout) findViewById(R.id.profile_icon_container);
        this.N = (TXImageView) findViewById(R.id.profile_icon);
        this.O = (ImageView) findViewById(R.id.profile_icon_no_login);
        this.ab = (ImageView) findViewById(R.id.backup_success);
        this.ac = (ImageView) findViewById(R.id.backup_failed);
        this.R = (TextView) findViewById(R.id.checkbox);
        this.Q = (TextView) findViewById(R.id.last_backup_time);
        this.S = (ImageView) findViewById(R.id.cloud1);
        this.T = (ImageView) findViewById(R.id.cloud2);
        this.U = (ImageView) findViewById(R.id.cloud3);
        this.V = (ImageView) findViewById(R.id.cloud4);
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, (float) by.a(this, (float) (C + 14)), 0.0f, 0.0f);
        TranslateAnimation translateAnimation2 = new TranslateAnimation(0.0f, (float) by.a(this, (float) (C + 27)), 0.0f, 0.0f);
        TranslateAnimation translateAnimation3 = new TranslateAnimation(0.0f, (float) by.a(this, 90.0f), 0.0f, 0.0f);
        TranslateAnimation translateAnimation4 = new TranslateAnimation(0.0f, (float) by.a(this, 85.0f), 0.0f, 0.0f);
        translateAnimation.setInterpolator(this, 17432587);
        translateAnimation2.setInterpolator(this, 17432587);
        translateAnimation3.setInterpolator(this, 17432587);
        translateAnimation4.setInterpolator(this, 17432587);
        translateAnimation.setFillAfter(true);
        translateAnimation2.setFillAfter(true);
        translateAnimation3.setFillAfter(true);
        translateAnimation4.setFillAfter(true);
        translateAnimation.setDuration(27800);
        translateAnimation2.setDuration(17000);
        translateAnimation3.setDuration(4500);
        translateAnimation4.setDuration(2125);
        translateAnimation.setAnimationListener(this.ak);
        this.S.startAnimation(translateAnimation);
        this.T.startAnimation(translateAnimation2);
        this.U.startAnimation(translateAnimation3);
        this.V.startAnimation(translateAnimation4);
        this.R.setOnClickListener(this);
        this.n.setOnClickListener(this);
        this.u.setOnClickListener(this);
        this.O.setOnClickListener(this);
        K();
        J();
    }

    /* access modifiers changed from: private */
    public void y() {
        if (this.an) {
            if (!this.ae) {
                int i = -by.a(this, 114.0f);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams.rightMargin = 0;
                layoutParams.leftMargin = i;
                layoutParams.topMargin = by.a(this, 130.0f);
                layoutParams.addRule(9);
                layoutParams.addRule(10);
                this.S.setLayoutParams(layoutParams);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams2.rightMargin = 0;
                layoutParams2.leftMargin = i;
                layoutParams2.topMargin = by.a(this, 90.0f);
                layoutParams2.addRule(9);
                layoutParams2.addRule(10);
                this.T.setLayoutParams(layoutParams2);
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams3.rightMargin = 0;
                layoutParams3.leftMargin = i;
                layoutParams3.topMargin = by.a(this, 95.0f);
                layoutParams3.addRule(9);
                layoutParams3.addRule(10);
                this.U.setLayoutParams(layoutParams3);
                RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams4.rightMargin = 0;
                layoutParams4.leftMargin = i;
                layoutParams4.topMargin = by.a(this, 65.0f);
                layoutParams4.addRule(9);
                layoutParams4.addRule(10);
                this.V.setLayoutParams(layoutParams4);
                this.ae = true;
            }
            if (this.W == null) {
                XLog.d("AppBackupActivity", "startRepeatAnimation. cloudAnimation1 == null. create...");
                int a2 = by.a(this, (float) (C + 114));
                this.W = new TranslateAnimation(0.0f, (float) a2, 0.0f, 0.0f);
                this.X = new TranslateAnimation(0.0f, (float) a2, 0.0f, 0.0f);
                this.Y = new TranslateAnimation(0.0f, (float) a2, 0.0f, 0.0f);
                this.Z = new TranslateAnimation(0.0f, (float) a2, 0.0f, 0.0f);
                this.W.setInterpolator(this, 17432587);
                this.X.setInterpolator(this, 17432587);
                this.Y.setInterpolator(this, 17432587);
                this.Z.setInterpolator(this, 17432587);
                this.W.setDuration(34000);
                this.X.setDuration(20000);
                this.Y.setDuration(18000);
                this.Z.setDuration(9000);
                this.W.setAnimationListener(this.ak);
            }
            this.U.requestLayout();
            this.V.requestLayout();
            this.U.setVisibility(0);
            this.V.setVisibility(0);
            this.U.startAnimation(this.Y);
            this.V.startAnimation(this.Z);
            if (this.ao.hasMessages(11902)) {
                this.ao.removeMessages(11902);
            }
            this.ao.sendEmptyMessageDelayed(11902, 9000);
        }
    }

    /* access modifiers changed from: private */
    public void a(View view, float f, float f2, boolean z2, Animation.AnimationListener animationListener) {
        al alVar = new al(this, f, f2, ((float) view.getWidth()) / 2.0f, ((float) view.getHeight()) / 2.0f, 0.0f, false);
        alVar.setDuration(200);
        alVar.setAnimationListener(animationListener);
        view.startAnimation(alVar);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.v != null) {
            this.v.l();
        }
        if (this.ah) {
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.v != null) {
            this.v.m();
        }
        if (this.ah) {
        }
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        if (z2 && !this.an) {
            this.an = true;
            y();
        } else if (!z2) {
            this.an = false;
            z();
        }
    }

    private void z() {
        if (this.ao.hasMessages(11902)) {
            this.ao.removeMessages(11902);
        }
        this.S.clearAnimation();
        this.T.clearAnimation();
        this.U.clearAnimation();
        this.V.clearAnimation();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.ah) {
            super.onDestroy();
            return;
        }
        v();
        com.tencent.assistant.manager.t.a().b(this);
        ApkResourceManager.getInstance().unRegisterApkResCallback(this.aj);
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 != i) {
            return super.onKeyDown(i, keyEvent);
        }
        A();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void A() {
        if (!this.s) {
            finish();
            return;
        }
        Intent intent = new Intent(this, AssistantTabActivity.class);
        intent.setFlags(67108864);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        intent.putExtra(com.tencent.assistant.a.a.G, true);
        startActivity(intent);
        this.s = false;
        finish();
        XLog.i("AppBackupActivity", "AppBackupActivity >> key back finish");
    }

    /* access modifiers changed from: private */
    public void b(int i) {
        if (this.G != null && this.G.isShowing()) {
            this.G.dismiss();
        }
        switch (i) {
            case 3:
                this.E = new DeviceListDialog(this);
                BackupDeviceAdapter backupDeviceAdapter = new BackupDeviceAdapter(this);
                this.H = new ak(this, null);
                backupDeviceAdapter.a(this.H);
                backupDeviceAdapter.a(this.w.b());
                this.E.a(backupDeviceAdapter);
                this.E.setOnDismissListener(new af(this));
                if (!isFinishing()) {
                    try {
                        this.E.show();
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                } else {
                    return;
                }
            case 4:
                this.F = new BackupApplistDialog(this);
                BackupAppListAdapter backupAppListAdapter = new BackupAppListAdapter(this);
                backupAppListAdapter.a(this.x.a());
                this.F.a(new ai(this, null));
                this.F.a(backupAppListAdapter);
                this.F.a(this.x.b());
                this.F.setOnDismissListener(new ag(this));
                if (!isFinishing()) {
                    try {
                        this.F.show();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                l.a(new STInfoV2(STConst.ST_PAGE_APP_BACKUP_APPLIST, "001", m(), STConst.ST_DEFAULT_SLOT, 100));
                return;
            default:
                return;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.profile_icon_no_login /*2131165335*/:
            case R.id.nick_name /*2131165336*/:
                if (j.a().j()) {
                    return;
                }
                if (!c.a()) {
                    B();
                    this.M = 3;
                    return;
                }
                this.M = -1;
                Bundle bundle = new Bundle();
                bundle.putInt(AppConst.KEY_LOGIN_TYPE, 2);
                bundle.putInt(AppConst.KEY_FROM_TYPE, 10);
                j.a().a(AppConst.IdentityType.MOBILEQ, bundle);
                this.L = 1;
                l.a(new STInfoV2(f(), "03_001", m(), STConst.ST_DEFAULT_SLOT, 200));
                return;
            case R.id.cloud_mask /*2131165337*/:
            case R.id.auto_backup_layout /*2131165338*/:
            case R.id.last_backup_time /*2131165339*/:
            default:
                return;
            case R.id.checkbox /*2131165340*/:
                o.e();
                this.R.setSelected(o.a());
                if (!this.K && this.R.isSelected()) {
                    this.J = C();
                }
                l.a(new STInfoV2(f(), "03_001", m(), STConst.ST_DEFAULT_SLOT, this.R.isSelected() ? STConstAction.ACTION_HIT_APK_CHECK : STConstAction.ACTION_HIT_APK_UNCHECK));
                return;
            case R.id.backup_button /*2131165341*/:
                if (!c.a()) {
                    B();
                    this.M = 1;
                    return;
                }
                this.M = -1;
                if (!j.a().j()) {
                    Bundle bundle2 = new Bundle();
                    bundle2.putInt(AppConst.KEY_LOGIN_TYPE, 2);
                    bundle2.putInt(AppConst.KEY_FROM_TYPE, 10);
                    j.a().a(AppConst.IdentityType.MOBILEQ, bundle2);
                    this.L = 2;
                } else if (this.w.d() || o.g()) {
                    H();
                } else {
                    G();
                }
                l.a(new STInfoV2(f(), "04_001", m(), STConst.ST_DEFAULT_SLOT, 200));
                return;
            case R.id.restore_button /*2131165342*/:
                if (!c.a()) {
                    B();
                    this.M = 2;
                    return;
                }
                this.M = -1;
                if (!j.a().j()) {
                    Bundle bundle3 = new Bundle();
                    bundle3.putInt(AppConst.KEY_LOGIN_TYPE, 2);
                    bundle3.putInt(AppConst.KEY_FROM_TYPE, 10);
                    j.a().a(AppConst.IdentityType.MOBILEQ, bundle3);
                    this.L = 3;
                } else if (D()) {
                    this.u.setEnabled(false);
                }
                l.a(new STInfoV2(f(), "05_001", m(), STConst.ST_DEFAULT_SLOT, 200));
                return;
        }
    }

    private void B() {
        AppConst.TwoBtnDialogInfo noWIFIDialogInfo = DialogUtils.getNoWIFIDialogInfo(this);
        noWIFIDialogInfo.titleRes = getResources().getString(R.string.backup_tips);
        this.G = DialogUtils.get2BtnDialog(noWIFIDialogInfo);
        if (this.G != null && !isFinishing()) {
            try {
                this.G.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private int C() {
        if (j.a().j()) {
            return com.tencent.assistant.module.update.j.b().c();
        }
        return -1;
    }

    private boolean D() {
        if (!j.a().j()) {
            return false;
        }
        this.B = 2;
        this.z = this.w.a();
        return true;
    }

    /* access modifiers changed from: private */
    public void E() {
        a(this.w.e());
    }

    private void a(BackupDevice backupDevice) {
        this.A = this.x.a(backupDevice);
        this.y = 2;
    }

    private void b(boolean z2) {
        this.u.setEnabled(z2);
    }

    private void c(boolean z2) {
        if (this.B == 1) {
            I();
            if (this.w.c()) {
                if (this.w.d()) {
                    if (o.g()) {
                        if (this.L == 2 && this.w.d()) {
                            H();
                        }
                        this.L = -1;
                    } else if (!o.h()) {
                        this.A = this.x.a(this.w.e());
                        this.y = 1;
                    }
                    BackupDevice e = this.w.e();
                    if (e != null) {
                        long c = e.c();
                        o.a(c);
                        a(c);
                    }
                } else {
                    if (this.ai != 1) {
                        c(1);
                    } else {
                        b(3);
                    }
                    this.L = -1;
                }
                b(true);
                return;
            }
            if (z2) {
                b(false);
                if (this.L == 2) {
                    G();
                } else {
                    try {
                        Toast.makeText(this, getString(R.string.have_no_backup_yet), 0).show();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
            this.L = -1;
        } else if (this.w.c()) {
            if (this.w.e() == null || this.w.b().size() != 1) {
                b(3);
                b(true);
            } else {
                this.A = this.x.a(this.w.e());
                this.y = 2;
            }
            BackupDevice e3 = this.w.e();
            if (e3 != null) {
                long c2 = e3.c();
                o.a(c2);
                a(c2);
            }
        } else if (z2) {
            try {
                b(false);
                Toast.makeText(this, getString(R.string.have_no_backup_yet), 0).show();
            } catch (Exception e4) {
                e4.printStackTrace();
            }
        } else {
            Toast.makeText(this, getString(R.string.get_backup_device_list_failed), 0).show();
            b(true);
        }
    }

    private void c(int i) {
        if (this.G != null && this.G.isShowing()) {
            this.G.dismiss();
        }
        ah ahVar = new ah(this, i, this.L);
        ahVar.blockCaller = true;
        ahVar.titleRes = getString(R.string.restore_backup_apps);
        ahVar.rBtnTxtRes = getString(R.string.restore_backup_apps);
        if (this.L == 2) {
            ahVar.lBtnTxtRes = getString(R.string.continue_backup);
        } else {
            ahVar.lBtnTxtRes = getString(R.string.cancel);
        }
        if (i == 1) {
            ahVar.contentRes = getString(R.string.switch_device_restore_tips);
        } else if (i == 2) {
            ahVar.contentRes = getString(R.string.refresh_device_restore_tips);
        }
        DialogUtils.show2BtnDialog(ahVar);
        if (i == 1) {
            l.a(new STInfoV2(STConst.ST_PAGE_APP_BACKUP_SWITCHDEVICE, "001", 0, STConst.ST_DEFAULT_SLOT, 100));
        } else if (i == 2) {
            l.a(new STInfoV2(STConst.ST_PAGE_APP_BACKUP_REFRESHDEVICE, "001", 0, STConst.ST_DEFAULT_SLOT, 100));
        }
    }

    private void d(int i) {
        if (this.G != null && this.G.isShowing()) {
            this.G.dismiss();
        }
        u uVar = new u(this);
        this.n.setEnabled(true);
        this.n.setText(getResources().getString(R.string.app_backup_list));
        uVar.blockCaller = true;
        uVar.lBtnTxtRes = getString(R.string.do_not_open);
        uVar.rBtnTxtRes = getString(R.string.open_auto_backup);
        uVar.titleRes = getString(R.string.backup_success);
        uVar.contentRes = String.format(getString(R.string.auto_backup_message), com.tencent.assistant.utils.t.v(), Integer.valueOf(i));
        DialogUtils.show2BtnDialog(uVar);
    }

    /* access modifiers changed from: private */
    public void F() {
        AppConst.LoadingDialogInfo loadingDialogInfo = new AppConst.LoadingDialogInfo();
        loadingDialogInfo.loadingText = "加载中";
        loadingDialogInfo.blockCaller = true;
        this.D = DialogUtils.showLoadingDialog(loadingDialogInfo);
        if (this.D != null) {
            this.D.setCancelable(true);
        }
    }

    /* access modifiers changed from: private */
    public void G() {
        this.I = C();
        if (this.I != -1) {
            this.n.setEnabled(false);
            this.n.setText(getString(R.string.backuping));
        }
    }

    /* access modifiers changed from: private */
    public void H() {
        if (this.G != null && this.G.isShowing()) {
            this.G.dismiss();
        }
        v vVar = new v(this);
        vVar.blockCaller = true;
        vVar.rBtnTxtRes = getString(R.string.continue_backup);
        vVar.lBtnTxtRes = getString(R.string.cancel);
        vVar.titleRes = getString(R.string.app_backup_list);
        vVar.contentRes = getString(R.string.continue_backup_content);
        DialogUtils.show2BtnDialog(vVar);
    }

    private void d(boolean z2) {
        I();
        ArrayList<BackupApp> a2 = this.x.a();
        if (this.y == 1) {
            if (a2 == null || a2.size() < 10) {
                if (this.L == 2 && this.w.d()) {
                    H();
                }
            } else if (this.ai != 2) {
                c(2);
            } else {
                b(4);
            }
            this.L = -1;
        } else if (z2) {
            try {
                if (this.x.c()) {
                    if (this.E != null && this.E.isShowing()) {
                        this.E.a(0);
                    }
                    Toast.makeText(this, "备份的应用本地已全部安装", 0).show();
                } else {
                    if (this.E != null && this.E.isShowing()) {
                        this.E.a(1);
                    }
                    b(4);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (this.E != null && this.E.isShowing()) {
                this.E.a(-1);
            }
            Toast.makeText(this, getString(R.string.get_backup_applist_failed), 0).show();
        }
        b(true);
    }

    private void I() {
        if (this.D != null && this.D.isShowing()) {
            this.D.dismiss();
        }
    }

    private void J() {
        if (j.a().j()) {
            this.R.setVisibility(0);
            this.R.setSelected(o.a());
        } else {
            this.R.setVisibility(4);
        }
        long j = 0;
        if (j.a().j()) {
            j = o.i();
        }
        a(j);
    }

    private void a(long j) {
        if (j == 0) {
            this.Q.setVisibility(4);
            return;
        }
        int f = bo.f(j);
        if (f != 0) {
            String valueOf = String.valueOf(f);
            SpannableString spannableString = new SpannableString(getString(R.string.last_backup_time, new Object[]{valueOf}));
            if (f >= 20) {
                spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.app_backup_day_text_color)), 0, valueOf.length(), 33);
            }
            this.Q.setVisibility(0);
            this.Q.setText(spannableString);
            return;
        }
        this.Q.setVisibility(0);
        this.Q.setText(getString(R.string.just_backuped));
    }

    /* access modifiers changed from: private */
    public void K() {
        com.tencent.nucleus.socialcontact.login.m f = com.tencent.nucleus.socialcontact.login.l.f();
        if (this.N != null) {
            if (j.a().j()) {
                this.N.updateImageView(f.f3165a, R.drawable.common_owner_icon_01, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                this.N.setVisibility(0);
                this.O.setVisibility(8);
            } else {
                this.N.setVisibility(8);
                this.O.setVisibility(0);
            }
        }
        if (this.P == null) {
            return;
        }
        if (j.a().j()) {
            this.P.setText(f.b);
        } else {
            this.P.setText(getString(R.string.hint_login));
        }
    }

    /* access modifiers changed from: private */
    public void L() {
        this.B = 1;
        this.z = this.w.a();
        F();
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS /*1077*/:
                K();
                return;
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL /*1078*/:
                K();
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS /*1081*/:
                XLog.d("AppBackupActivity", "LOGIN_SUCCESS");
                if (this.L == 3) {
                    if (D()) {
                        this.u.setEnabled(false);
                    }
                    this.L = -1;
                } else if (this.L == 2 || this.L == 1) {
                    L();
                }
                J();
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_FAIL /*1082*/:
                XLog.d("AppBackupActivity", "LOGIN_FAIL");
                J();
                this.L = -1;
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL /*1083*/:
                this.L = -1;
                return;
            case EventDispatcherEnum.UI_EVENT_BACKUP_APPLIST_SUCCESS /*1122*/:
                int i = message.arg1;
                int i2 = message.arg2;
                if (i == this.I) {
                    this.ad = true;
                    this.K = true;
                    if (o.g() || o.a()) {
                        this.N.setVisibility(0);
                        this.ab.setVisibility(8);
                        this.ac.setVisibility(8);
                        a(this.aa, 0.0f, 90.0f, false, this.al);
                    } else {
                        d(i2);
                    }
                    this.I = -1;
                } else if (i == this.J) {
                    this.K = true;
                    this.J = -1;
                }
                o.f();
                b(true);
                return;
            case EventDispatcherEnum.UI_EVENT_BACKUP_APPLIST_FAIL /*1123*/:
                int i3 = message.arg1;
                if (i3 == this.I) {
                    this.ad = false;
                    this.N.setVisibility(0);
                    this.ab.setVisibility(8);
                    this.ac.setVisibility(8);
                    a(this.aa, 0.0f, 90.0f, false, this.al);
                    this.I = -1;
                    com.tencent.nucleus.socialcontact.login.l.c(message.arg2);
                    return;
                } else if (i3 == this.J) {
                    this.J = -1;
                    return;
                } else {
                    return;
                }
            case EventDispatcherEnum.UI_EVENT_GET_BACKUP_DEVICELIST_SUCCESS /*1124*/:
                Log.d("AppBackupActivity", "UI_EVENT_GET_BACKUP_DEVICELIST_SUCCESS msg.arg1 = " + message.arg1 + " deviceRequestId = " + this.z);
                if (message.arg1 == this.z) {
                    c(true);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_GET_BACKUP_DEVICELIST_FAIL /*1125*/:
                Log.d("AppBackupActivity", "UI_EVENT_GET_BACKUP_DEVICELIST_FAIL msg.arg1 = " + message.arg1 + " deviceRequestId = " + this.z);
                if (message.arg1 == this.z) {
                    c(false);
                    if (this.B == 2) {
                        com.tencent.nucleus.socialcontact.login.l.b(message.arg2);
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_GET_BACKUP_APPS_SUCCESS /*1126*/:
                if (message.arg1 == this.A) {
                    I();
                    d(true);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_GET_BACKUP_APPS_FAIL /*1127*/:
                if (message.arg1 == this.A) {
                    I();
                    d(false);
                    if (this.y == 2) {
                        com.tencent.nucleus.socialcontact.login.l.b(message.arg2);
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onConnected(APN apn) {
        if (this.M == 1) {
            this.M = -1;
            onClick(this.n);
        } else if (this.M == 2) {
            this.M = -1;
            onClick(this.u);
        } else if (this.M == 3) {
            this.M = -1;
            onClick(this.O);
        }
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
    }
}
