package com.tencent.assistant.activity;

import android.view.animation.Animation;

/* compiled from: ProGuard */
class dj implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartScanActivity f530a;

    private dj(StartScanActivity startScanActivity) {
        this.f530a = startScanActivity;
    }

    /* synthetic */ dj(StartScanActivity startScanActivity, da daVar) {
        this(startScanActivity);
    }

    public void onAnimationEnd(Animation animation) {
        switch (this.f530a.u) {
            case 10:
                this.f530a.c(20);
                break;
            case 11:
                this.f530a.u();
                break;
            case 12:
                this.f530a.B.setVisibility(8);
                this.f530a.C();
                break;
        }
        this.f530a.I.c();
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
