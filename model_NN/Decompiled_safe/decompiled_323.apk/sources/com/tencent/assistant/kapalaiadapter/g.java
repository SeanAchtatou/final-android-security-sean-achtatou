package com.tencent.assistant.kapalaiadapter;

import java.lang.reflect.Method;

/* compiled from: ProGuard */
public final class g {
    public static Object a(Object obj, String str, Object[] objArr) {
        return a(obj, str, a(objArr), objArr);
    }

    public static Object a(Object obj, String str) {
        return a(obj, str, (Object[]) null);
    }

    public static Object a(Object obj, String str, Class<?>[] clsArr, Object[] objArr) {
        Class<?> cls = obj.getClass();
        Method method = null;
        try {
            method = cls.getDeclaredMethod(str, clsArr);
        } catch (Exception e) {
            if (0 == 0 && cls.getSuperclass() != null) {
                method = cls.getSuperclass().getDeclaredMethod(str, clsArr);
            }
            if (method == null) {
                throw e;
            }
        }
        method.setAccessible(true);
        return method.invoke(obj, objArr);
    }

    public static Object a(String str, String str2, Object[] objArr, Class<?>[] clsArr) {
        Class<?> cls = Class.forName(str);
        Method method = null;
        try {
            method = cls.getDeclaredMethod(str2, clsArr);
        } catch (Exception e) {
            if (0 == 0 && cls.getSuperclass() != null) {
                method = cls.getSuperclass().getDeclaredMethod(str2, clsArr);
            }
            if (method == null) {
                throw e;
            }
        }
        method.setAccessible(true);
        return method.invoke(cls, objArr);
    }

    public static Object a(String str, String str2, Object[] objArr) {
        return a(str, str2, objArr, a(objArr));
    }

    public static Object a(String str, String str2) {
        return a(str, str2, (Object[]) null);
    }

    private static Class<?>[] a(Object[] objArr) {
        Class<Long>[] clsArr = null;
        if (objArr != null) {
            clsArr = new Class[objArr.length];
            int length = objArr.length;
            for (int i = 0; i < length; i++) {
                if (objArr[i] != null) {
                    clsArr[i] = objArr[i].getClass();
                } else {
                    clsArr[i] = String.class;
                }
                if (clsArr[i] == Integer.class) {
                    clsArr[i] = Integer.TYPE;
                } else if (clsArr[i] == Boolean.class) {
                    clsArr[i] = Boolean.TYPE;
                } else if (clsArr[i] == Long.class) {
                    clsArr[i] = Long.TYPE;
                }
            }
        }
        return clsArr;
    }
}
