package com.tencent.assistant.kapalaiadapter;

/* compiled from: ProGuard */
public interface IMoblieModelConfig {

    /* compiled from: ProGuard */
    public enum ALPS {
        _R811,
        _R801,
        _U701
    }

    /* compiled from: ProGuard */
    public enum BBK {
        _VIVO_Y11,
        _VIVO_Y17T,
        _VIVO_Y11I_T,
        _VIVO_Y15T,
        _VIVO_S7T,
        _VIVO_X1ST,
        _VIVO_Y13,
        _VIVO_S7,
        _VIVO_Y19T,
        _VIVO_S11T,
        _VIVO_Y3T,
        _VIVO_Y20T,
        _VIVO_S9
    }

    /* compiled from: ProGuard */
    public enum COOLPAD {
        _7260,
        _7230_B,
        _COOLPAD_7235,
        _COOLPAD_8297,
        _COOLPAD_8079,
        _COOLPAD_7296,
        _COOLPAD_7295C,
        _COOLPAD_7295A,
        _COOLPAD_8076D,
        _COOLPAD7295,
        _8720,
        _COOLPAD_7270
    }

    /* compiled from: ProGuard */
    public enum GIONEE {
        _V182,
        _GN137
    }

    /* compiled from: ProGuard */
    public enum HISENSE {
    }

    /* compiled from: ProGuard */
    public enum HTC {
        _HTC_T528T,
        _HTC_T528W,
        _HTC_T328T,
        _HTC_T328D,
        _HTC_T328W,
        _HTC_INCREDIBLE_S,
        _HTC_EVO_3D_X515m,
        _HTC_T528D,
        _HTC_D816W
    }

    /* compiled from: ProGuard */
    public enum HUAWEI {
        _HUAWEI_Y511_T00,
        _H30_T00,
        _HUAWEI_G520_0000,
        _HUAWEI_G610_U00,
        _HUAWEI_G610_T11,
        _H30_U10,
        _HUAWEI_P6_C00,
        _H30_T10,
        _HUAWEI_A199,
        _HUAWEI_Y321_C00,
        _HUAWEI_C8813D,
        _HUAWEI_U8825D,
        _HUAWEI_Y325_T00,
        _HUAWEI_G610_C00,
        _HUAWEI_Y320_T00,
        _HUAWEI_G750_T00
    }

    /* compiled from: ProGuard */
    public enum K_TOUCH {
    }

    /* compiled from: ProGuard */
    public enum LENOVO {
        _LENOVO_A390T,
        _LENOVO_A630T,
        _LENOVO_A308T,
        _LENOVO_A820T,
        _LENOVO_A850,
        _LENOVO_A670T,
        _LENOVO_A278T,
        _LENOVO_S720,
        _LENOVO_A789,
        _S890,
        _LENOVO_S890,
        _LENOVO_A820
    }

    /* compiled from: ProGuard */
    public enum MEIZU {
    }

    /* compiled from: ProGuard */
    public enum MOTOROLA {
    }

    /* compiled from: ProGuard */
    public enum OPPO {
        _R821T,
        _R819T,
        _R831T,
        _R827T,
        _R829T,
        _R815T,
        _R823T,
        _U707T,
        _R833T,
        _R813T
    }

    /* compiled from: ProGuard */
    public enum OTHERPHONE {
    }

    /* compiled from: ProGuard */
    public enum SAMSUNG {
        _GT_I8552,
        _GT_S7572,
        _GT_I8262D,
        _GT_N7102,
        _SM_N9009,
        _GT_I9152,
        _GT_S7562I,
        _SCH_I959,
        _GT_I8558,
        _SM_N9002,
        _SCH_I869,
        _SCH_N719,
        _GT_I9502,
        _GT_I9082,
        _SCH_I829,
        _GT_I9158,
        _SCH_I879,
        _SM_G3812,
        _SM_G7108,
        _SM_G3502U,
        _SM_G3502,
        _GT_S6352,
        _SCH_W2013,
        _SM_G3819D,
        _SCH_I939,
        _SM_G7106,
        _GT_I9082I,
        _GT_I9300
    }

    /* compiled from: ProGuard */
    public enum SONY {
        _S39H,
        _MT15I,
        _LT26I,
        _LT26II
    }

    /* compiled from: ProGuard */
    public enum XIAOMI {
        _MI_1S,
        _2013022,
        _HM_1SC,
        _HM_NOTE_1TD
    }

    /* compiled from: ProGuard */
    public enum YULONG {
        _COOLPAD_5950,
        _8020,
        _8022,
        _8060,
        _5860A
    }

    /* compiled from: ProGuard */
    public enum ZTE {
        _ZTE_V889D
    }
}
