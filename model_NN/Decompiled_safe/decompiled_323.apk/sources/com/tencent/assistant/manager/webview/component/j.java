package com.tencent.assistant.manager.webview.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class j implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WebViewFooter f916a;

    j(WebViewFooter webViewFooter) {
        this.f916a = webViewFooter;
    }

    public void onClick(View view) {
        if (this.f916a.e != null) {
            switch (view.getId()) {
                case R.id.webview_back_img /*2131166682*/:
                    this.f916a.e.onBack();
                    return;
                case R.id.webview_fresh_img /*2131166683*/:
                    this.f916a.e.onFresh();
                    return;
                case R.id.webview_forward_img /*2131166684*/:
                    this.f916a.e.onForward();
                    return;
                default:
                    return;
            }
        }
    }
}
