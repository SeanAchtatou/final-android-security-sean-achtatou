package com.tencent.assistant.manager.notification.a;

import android.graphics.Bitmap;
import android.widget.RemoteViews;
import com.tencent.assistant.manager.notification.a.a.e;

/* compiled from: ProGuard */
class o implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RemoteViews f874a;
    final /* synthetic */ int[] b;
    final /* synthetic */ int c;
    final /* synthetic */ n d;

    o(n nVar, RemoteViews remoteViews, int[] iArr, int i) {
        this.d = nVar;
        this.f874a = remoteViews;
        this.b = iArr;
        this.c = i;
    }

    public void a(Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            this.f874a.setViewVisibility(this.b[this.c], 0);
            this.f874a.setImageViewBitmap(this.b[this.c], bitmap);
        }
    }
}
