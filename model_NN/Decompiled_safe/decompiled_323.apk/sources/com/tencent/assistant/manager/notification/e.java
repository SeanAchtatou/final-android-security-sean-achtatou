package com.tencent.assistant.manager.notification;

import android.graphics.Bitmap;
import com.tencent.assistant.thumbnailCache.k;

/* compiled from: ProGuard */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f881a;

    e(d dVar) {
        this.f881a = dVar;
    }

    public void run() {
        Bitmap a2 = k.b().a(this.f881a.f880a, this.f881a.b, this.f881a.e);
        if (a2 != null && !a2.isRecycled()) {
            this.f881a.a(0, a2);
        }
    }
}
