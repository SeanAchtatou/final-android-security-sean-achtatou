package com.tencent.assistant.manager.notification;

import com.tencent.pangu.download.DownloadInfoWrapper;
import java.util.Comparator;

/* compiled from: ProGuard */
class w implements Comparator<DownloadInfoWrapper> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ v f898a;

    w(v vVar) {
        this.f898a = vVar;
    }

    /* renamed from: a */
    public int compare(DownloadInfoWrapper downloadInfoWrapper, DownloadInfoWrapper downloadInfoWrapper2) {
        return (int) (downloadInfoWrapper2.a() - downloadInfoWrapper.a());
    }
}
