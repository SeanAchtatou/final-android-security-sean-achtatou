package com.tencent.assistant.manager.notification.a;

import android.text.TextUtils;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.notification.a.a.f;
import com.tencent.assistant.protocol.jce.PushInfo;

/* compiled from: ProGuard */
public class i extends e {
    /* access modifiers changed from: private */
    public static final String p = i.class.getSimpleName();

    public i(int i, PushInfo pushInfo, byte[] bArr) {
        super(i, pushInfo, bArr);
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        if (super.c() && this.c.f != null && !TextUtils.isEmpty(this.c.f.b)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        this.i = c(R.layout.notification_card_12);
        if (this.i == null) {
            return false;
        }
        f fVar = new f(this.c.f);
        fVar.a(new j(this));
        a(fVar);
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        if (this.c == null) {
            return false;
        }
        if (this.c.f.f1445a != 6) {
            return true;
        }
        this.k = c(R.layout.notification_card_12);
        if (this.k != null) {
            return a(this.b, this.k);
        }
        return false;
    }
}
