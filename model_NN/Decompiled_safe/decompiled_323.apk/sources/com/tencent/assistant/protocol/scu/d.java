package com.tencent.assistant.protocol.scu;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.protocol.a.e;
import com.tencent.assistant.protocol.b;
import com.tencent.assistant.protocol.jce.AuthRequest;
import com.tencent.assistant.protocol.jce.AuthResponse;
import com.tencent.assistant.protocol.jce.RspHead;
import com.tencent.assistant.protocol.jce.StatCSChannelData;
import com.tencent.assistant.protocol.scu.cscomm.CsCommManager;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: ProGuard */
public class d implements e, f, g {

    /* renamed from: a  reason: collision with root package name */
    private static d f1628a;
    private AppCertificateState b = new AppCertificateState();
    private AppAuthState c;
    private ReferenceQueue<h> d = new ReferenceQueue<>();
    private ConcurrentLinkedQueue<WeakReference<h>> e = new ConcurrentLinkedQueue<>();

    private d() {
        this.b.a(this);
        this.c = new AppAuthState();
        this.c.a(this);
    }

    public static synchronized d a() {
        d dVar;
        synchronized (d.class) {
            if (f1628a == null) {
                f1628a = new d();
            }
            dVar = f1628a;
        }
        return dVar;
    }

    public void a(h hVar) {
        if (hVar != null) {
            while (true) {
                WeakReference weakReference = (WeakReference) this.d.poll();
                if (weakReference == null) {
                    break;
                }
                this.e.remove(weakReference);
            }
            Iterator<WeakReference<h>> it = this.e.iterator();
            while (it.hasNext()) {
                if (hVar == ((h) it.next().get())) {
                    return;
                }
            }
            this.e.add(new WeakReference(hVar));
        }
    }

    private void i() {
        Iterator<WeakReference<h>> it = this.e.iterator();
        while (it.hasNext()) {
            h hVar = (h) it.next().get();
            if (hVar != null) {
                hVar.a();
            }
        }
    }

    private void j() {
        Iterator<WeakReference<h>> it = this.e.iterator();
        while (it.hasNext()) {
            h hVar = (h) it.next().get();
            if (hVar != null) {
                hVar.b();
            }
        }
    }

    private void b(int i) {
        Iterator<WeakReference<h>> it = this.e.iterator();
        while (it.hasNext()) {
            h hVar = (h) it.next().get();
            if (hVar != null) {
                hVar.a(i);
            }
        }
    }

    public boolean b() {
        return this.b.a();
    }

    public boolean c() {
        boolean a2;
        synchronized (this.c) {
            a2 = this.c.a();
        }
        return a2;
    }

    public List<JceStruct> a(List<JceStruct> list) {
        if (list != null) {
            AuthRequest authRequest = new AuthRequest();
            authRequest.f1163a = new byte[1];
            list.add(authRequest);
            synchronized (this.c) {
                d();
            }
        }
        return list;
    }

    public void a(byte b2) {
        synchronized (this.c) {
            if (!this.c.a()) {
                if (b2 == 2 && this.b.a()) {
                    return;
                }
                if (this.c.c()) {
                    i.c().a(100L);
                } else if (b2 != 7 || !this.b.a() || !this.c.b()) {
                    if (e.a().n()) {
                        a(b2, this.c.a());
                    }
                    if (b2 == 9) {
                        CsCommManager.a();
                        CsCommManager.clearAuthTicket();
                    }
                    ArrayList arrayList = new ArrayList(1);
                    AuthRequest authRequest = new AuthRequest();
                    authRequest.f1163a = new byte[1];
                    arrayList.add(authRequest);
                    i.c().a(e.a().h(), arrayList, this, b.a(), (byte) 4);
                    d();
                }
            }
        }
    }

    public void a(int i, int i2) {
        switch (i) {
            case -5:
                n();
                break;
            case 0:
                k();
                break;
            case 1:
                l();
                break;
            default:
                m();
                break;
        }
        if (i2 > 0) {
            c(i2);
        }
    }

    public void d() {
        synchronized (this.c) {
            this.c.d();
        }
    }

    private void k() {
        synchronized (this.b) {
            this.b.b();
        }
    }

    private void l() {
        k();
        j();
    }

    private void m() {
        synchronized (this.b) {
            this.b.c();
        }
    }

    private void n() {
        synchronized (this.b) {
            this.b.d();
        }
    }

    private void c(int i) {
        synchronized (this.b) {
            this.b.a(i);
        }
    }

    public void a(RspHead rspHead) {
    }

    public void onProtocoRequestFinish(int i, int i2, List<RequestResponePair> list) {
        int i3 = -9999;
        if (i2 == 0 && list != null) {
            RequestResponePair requestResponePair = list.get(0);
            if (requestResponePair.response != null) {
                AuthResponse authResponse = (AuthResponse) requestResponePair.response;
                i3 = authResponse.f1164a;
                if (authResponse.f1164a == 0) {
                    synchronized (this.c) {
                        this.c.a(i2, i3);
                    }
                    if (e.a().n()) {
                        a(i, i2, i3);
                        return;
                    }
                    return;
                }
            }
        }
        int i4 = i3;
        synchronized (this.c) {
            this.c.b(i2, i4);
        }
        if (e.a().n()) {
            a(i, i2, i4);
        }
    }

    public void e() {
        a((byte) 6);
    }

    public void f() {
        i();
    }

    public void g() {
        a((byte) 3);
    }

    public void a(int i) {
        a((byte) 5);
    }

    public void a(int i, boolean z, int i2) {
        if (i >= 2 || z) {
            b(i2);
        } else {
            a((byte) 4);
        }
    }

    public void h() {
    }

    private void a(byte b2, boolean z) {
        int i = 1;
        StatCSChannelData statCSChannelData = new StatCSChannelData();
        statCSChannelData.b = 2;
        statCSChannelData.c = 4;
        statCSChannelData.d = 1;
        StringBuilder sb = new StringBuilder();
        sb.append("sendType:").append((int) b2).append(";");
        StringBuilder append = sb.append("isAuthing:");
        if (!z) {
            i = 0;
        }
        append.append(i).append(";");
        statCSChannelData.g = sb.toString();
        e.a().a(statCSChannelData);
    }

    private void a(int i, int i2, int i3) {
        StatCSChannelData statCSChannelData = new StatCSChannelData();
        statCSChannelData.b = 2;
        statCSChannelData.c = 4;
        statCSChannelData.d = 2;
        statCSChannelData.f = i;
        StringBuilder sb = new StringBuilder();
        sb.append("rspErrorCode:").append(i2).append(";");
        sb.append("authRet:").append(i3).append(";");
        statCSChannelData.g = sb.toString();
        e.a().a(statCSChannelData);
    }
}
