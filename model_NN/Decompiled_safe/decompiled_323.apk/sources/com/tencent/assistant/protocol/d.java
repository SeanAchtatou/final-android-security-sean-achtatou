package com.tencent.assistant.protocol;

import java.io.IOException;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.protocol.HttpContext;

/* compiled from: ProGuard */
final class d implements HttpRequestRetryHandler {
    d() {
    }

    public boolean retryRequest(IOException iOException, int i, HttpContext httpContext) {
        if (i >= c.b) {
            return false;
        }
        if (iOException instanceof NoHttpResponseException) {
            return true;
        }
        if (iOException instanceof ClientProtocolException) {
            return true;
        }
        i.a("Rhett", "HttpClient Excpetion:" + iOException.toString());
        if (!(((HttpRequest) httpContext.getAttribute("http.request")) instanceof HttpEntityEnclosingRequest)) {
            return true;
        }
        return false;
    }
}
