package com.tencent.assistant.protocol.a;

import com.tencent.assistant.protocol.a.a.a;
import com.tencent.assistant.protocol.a.a.b;
import com.tencent.assistant.protocol.jce.RspHead;
import com.tencent.assistant.protocol.jce.StatCSChannelData;
import org.apache.http.impl.client.DefaultHttpClient;

/* compiled from: ProGuard */
public abstract class c {
    public abstract DefaultHttpClient a();

    public abstract void a(RspHead rspHead);

    public abstract void a(StatCSChannelData statCSChannelData);

    public abstract void a(boolean z, long j);

    public abstract b b();

    public abstract com.tencent.assistant.protocol.a.a.c c();

    public abstract String d();

    public abstract String e();

    public abstract int f();

    public void a(a aVar) {
    }
}
