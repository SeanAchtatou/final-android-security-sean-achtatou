package com.tencent.assistant.protocol.scu;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.protocol.ProtocolDecoder;
import com.tencent.assistant.protocol.a.e;
import com.tencent.assistant.protocol.g;
import com.tencent.assistant.protocol.j;
import com.tencent.assistant.protocol.jce.Request;
import com.tencent.assistant.protocol.jce.Response;
import com.tencent.assistant.protocol.jce.RspHead;
import com.tencent.assistant.protocol.jce.StatCSChannelData;
import com.tencent.assistant.protocol.l;
import com.tencent.assistantv2.st.business.LaunchSpeedSTManager;
import java.lang.reflect.Field;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class i implements g, h, p {

    /* renamed from: a  reason: collision with root package name */
    private static i f1629a;
    private ConcurrentHashMap<Integer, m> b = new ConcurrentHashMap<>();
    private n c = new n();
    private SecurityRequestHoldQueue d = new SecurityRequestHoldQueue();
    private HandlerThread e = null;
    private Handler f = null;

    private i() {
        this.d.a(this);
        d.a().a(this);
    }

    public static synchronized i c() {
        i iVar;
        synchronized (i.class) {
            if (f1629a == null) {
                f1629a = new i();
            }
            iVar = f1629a;
        }
        return iVar;
    }

    public int a(int i, List<JceStruct> list, g gVar, ProtocolDecoder protocolDecoder) {
        if (d.a().b()) {
            return a(i, list, gVar, protocolDecoder, (byte) 1);
        }
        if (this.c.a(list)) {
            return b(i, list, gVar, protocolDecoder);
        }
        this.d.a(a(i, list, gVar, (l) null, protocolDecoder), (byte) 1);
        d.a().a((byte) 2);
        return 0;
    }

    public int a(int i, List<JceStruct> list, g gVar, ProtocolDecoder protocolDecoder, byte b2) {
        if (LaunchSpeedSTManager.d().b(i)) {
            LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.SecurityPackage_packageRequest_Begin);
        }
        l a2 = k.a(i, list);
        if (LaunchSpeedSTManager.d().b(i)) {
            LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.SecurityPackage_packageRequest_End);
        }
        m a3 = a(i, list, gVar, a2, protocolDecoder);
        this.b.put(Integer.valueOf(i), a3);
        return a(a3, a2.f1631a, b2);
    }

    private int b(int i, List<JceStruct> list, g gVar, ProtocolDecoder protocolDecoder) {
        return a(i, d.a().a(list), gVar, protocolDecoder, (byte) 2);
    }

    private int a(m mVar, Request request, int i) {
        if (mVar == null || request == null) {
            return -1;
        }
        int a2 = j.a().a(mVar.f, request, mVar.h, this);
        if (LaunchSpeedSTManager.d().b(mVar.f)) {
            LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.Protocol_sendRequest_End);
        }
        if (e.a().n()) {
            a(mVar.f, i, mVar.f1632a);
        }
        if (a2 != -1) {
            return 1;
        }
        this.b.remove(Integer.valueOf(mVar.f));
        return -1;
    }

    private m a(int i, List<JceStruct> list, g gVar, l lVar, ProtocolDecoder protocolDecoder) {
        m mVar = new m();
        mVar.b = gVar;
        mVar.f1632a = list;
        mVar.c = protocolDecoder;
        mVar.f = i;
        mVar.e = System.currentTimeMillis();
        if (lVar != null) {
            mVar.h = lVar.b;
            Request request = lVar.f1631a;
            if (!(request == null || request.head == null || request.head.t != 1)) {
                mVar.g = true;
            }
        }
        return mVar;
    }

    public void a(RspHead rspHead) {
        if (rspHead != null) {
            if (rspHead.e == 0) {
                e.a().a(rspHead);
            }
            d.a().a(rspHead.l, rspHead.m);
        }
    }

    public void a(int i, int i2, Request request, Response response) {
        m remove = this.b.remove(Integer.valueOf(i));
        if (LaunchSpeedSTManager.d().b(i)) {
            LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.Protocol_onProtocoRequestFinish_Begin);
        }
        if (remove != null) {
            RspHead rspHead = null;
            if (response != null) {
                rspHead = response.head;
            }
            boolean z = rspHead != null && rspHead.l == -5;
            boolean z2 = i2 == -1018;
            if ((z || z2) && !remove.g) {
                int i3 = remove.d;
                int currentTimeMillis = (int) ((System.currentTimeMillis() - remove.e) / 1000);
                if (i3 < 1 && currentTimeMillis < e.a().q()) {
                    if (!this.c.a(remove.f1632a)) {
                        this.d.a(remove, (byte) 2);
                        if (e.a().n()) {
                            a(i, remove, rspHead, i2, null, true);
                            return;
                        }
                        return;
                    } else if (z2) {
                        this.d.a(remove, (byte) 2);
                        if (e.a().n()) {
                            a(i, remove, rspHead, i2, null, true);
                            return;
                        }
                        return;
                    }
                }
            }
            if (i2 == -1019 && !remove.g) {
                if (d.a().c()) {
                    a(remove, request, 5);
                } else {
                    this.d.a(remove, (byte) 2);
                }
            }
            a(i2, remove, response);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x00af  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r10, com.tencent.assistant.protocol.scu.m r11, com.tencent.assistant.protocol.jce.Response r12) {
        /*
            r9 = this;
            r7 = 1
            r6 = 0
            if (r11 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            com.tencent.assistantv2.st.business.LaunchSpeedSTManager r1 = com.tencent.assistantv2.st.business.LaunchSpeedSTManager.d()
            if (r11 == 0) goto L_0x00d1
            int r0 = r11.f
        L_0x000d:
            boolean r0 = r1.b(r0)
            if (r0 == 0) goto L_0x001c
            com.tencent.assistantv2.st.business.LaunchSpeedSTManager r0 = com.tencent.assistantv2.st.business.LaunchSpeedSTManager.d()
            com.tencent.assistantv2.st.business.LaunchSpeedSTManager$TYPE_TIME_POINT r1 = com.tencent.assistantv2.st.business.LaunchSpeedSTManager.TYPE_TIME_POINT.Protocol_Ticket_Cert_Handle_End
            r0.a(r1)
        L_0x001c:
            com.tencent.assistant.protocol.scu.g r8 = r11.b
            java.util.List<com.qq.taf.jce.JceStruct> r0 = r11.f1632a
            com.tencent.assistant.protocol.ProtocolDecoder r1 = r11.c
            java.util.List r5 = com.tencent.assistant.protocol.scu.k.a(r10, r0, r12, r1)
            r3 = 0
            if (r12 == 0) goto L_0x002b
            com.tencent.assistant.protocol.jce.RspHead r3 = r12.head
        L_0x002b:
            com.tencent.assistant.protocol.a.e r0 = com.tencent.assistant.protocol.a.e.a()
            boolean r0 = r0.n()
            if (r0 == 0) goto L_0x003d
            int r1 = r11.f
            r0 = r9
            r2 = r11
            r4 = r10
            r0.a(r1, r2, r3, r4, r5, r6)
        L_0x003d:
            com.tencent.assistantv2.st.business.LaunchSpeedSTManager r1 = com.tencent.assistantv2.st.business.LaunchSpeedSTManager.d()
            if (r11 == 0) goto L_0x00d4
            int r0 = r11.f
        L_0x0045:
            boolean r0 = r1.b(r0)
            if (r0 == 0) goto L_0x0054
            com.tencent.assistantv2.st.business.LaunchSpeedSTManager r0 = com.tencent.assistantv2.st.business.LaunchSpeedSTManager.d()
            com.tencent.assistantv2.st.business.LaunchSpeedSTManager$TYPE_TIME_POINT r1 = com.tencent.assistantv2.st.business.LaunchSpeedSTManager.TYPE_TIME_POINT.Protocol_unpackageResponse_End
            r0.a(r1)
        L_0x0054:
            boolean r0 = r11.g
            if (r0 == 0) goto L_0x0091
            if (r5 == 0) goto L_0x0091
            int r0 = r5.size()
            if (r0 <= r7) goto L_0x0091
            java.util.Iterator r1 = r5.iterator()
        L_0x0064:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0091
            java.lang.Object r0 = r1.next()
            com.tencent.assistant.protocol.scu.RequestResponePair r0 = (com.tencent.assistant.protocol.scu.RequestResponePair) r0
            com.qq.taf.jce.JceStruct r2 = r0.request
            boolean r2 = r2 instanceof com.tencent.assistant.protocol.jce.AuthRequest
            if (r2 == 0) goto L_0x0064
            r5.remove(r0)
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>(r7)
            r1.add(r0)
            com.tencent.assistant.protocol.scu.d r0 = com.tencent.assistant.protocol.scu.d.a()
            r0.a(r3)
            com.tencent.assistant.protocol.scu.d r0 = com.tencent.assistant.protocol.scu.d.a()
            int r2 = r11.f
            r0.onProtocoRequestFinish(r2, r10, r1)
        L_0x0091:
            if (r10 != 0) goto L_0x00b1
            if (r5 == 0) goto L_0x00db
            int r0 = r5.size()
            if (r0 <= 0) goto L_0x00db
            r1 = r6
        L_0x009c:
            int r0 = r5.size()
            if (r1 >= r0) goto L_0x00db
            java.lang.Object r0 = r5.get(r1)
            com.tencent.assistant.protocol.scu.RequestResponePair r0 = (com.tencent.assistant.protocol.scu.RequestResponePair) r0
            int r0 = r0.errorCode
            if (r0 != 0) goto L_0x00d7
            r0 = r6
        L_0x00ad:
            if (r0 == 0) goto L_0x00b1
            r10 = -841(0xfffffffffffffcb7, float:NaN)
        L_0x00b1:
            com.tencent.assistantv2.st.business.LaunchSpeedSTManager r0 = com.tencent.assistantv2.st.business.LaunchSpeedSTManager.d()
            if (r11 == 0) goto L_0x00b9
            int r6 = r11.f
        L_0x00b9:
            boolean r0 = r0.b(r6)
            if (r0 == 0) goto L_0x00c8
            com.tencent.assistantv2.st.business.LaunchSpeedSTManager r0 = com.tencent.assistantv2.st.business.LaunchSpeedSTManager.d()
            com.tencent.assistantv2.st.business.LaunchSpeedSTManager$TYPE_TIME_POINT r1 = com.tencent.assistantv2.st.business.LaunchSpeedSTManager.TYPE_TIME_POINT.Protocol_Merge_End
            r0.a(r1)
        L_0x00c8:
            if (r8 == 0) goto L_0x0004
            int r0 = r11.f
            r8.onProtocoRequestFinish(r0, r10, r5)
            goto L_0x0004
        L_0x00d1:
            r0 = r6
            goto L_0x000d
        L_0x00d4:
            r0 = r6
            goto L_0x0045
        L_0x00d7:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x009c
        L_0x00db:
            r0 = r7
            goto L_0x00ad
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.protocol.scu.i.a(int, com.tencent.assistant.protocol.scu.m, com.tencent.assistant.protocol.jce.Response):void");
    }

    public void b(int i) {
        c(i);
    }

    private void c(int i) {
        m remove = this.b.remove(Integer.valueOf(i));
        if (remove == null || !remove.g) {
            j.a().a(i);
            return;
        }
        remove.b = null;
        if (e.a().n()) {
            a(remove.f, remove.g);
        }
    }

    public void a(int i) {
        synchronized (this.d) {
            while (this.d.size() > 0) {
                m mVar = (m) this.d.remove();
                if (mVar != null) {
                    a(i, mVar, (Response) null);
                }
            }
        }
    }

    public void a() {
        synchronized (this.d) {
            while (this.d.size() > 0) {
                m mVar = (m) this.d.remove();
                if (mVar != null) {
                    a(mVar.f, mVar.f1632a, mVar.b, mVar.c, (byte) 3);
                }
            }
        }
    }

    public void b() {
    }

    public void a(List<m> list) {
        if (list != null && list.size() > 0) {
            for (m a2 : list) {
                a(-875, a2, (Response) null);
            }
        }
    }

    public void a(long j) {
        if (this.e == null) {
            this.e = new HandlerThread("ProtocolSecurityManager");
            this.e.start();
            Looper looper = this.e.getLooper();
            if (looper != null) {
                this.f = new Handler(looper);
            }
            if (this.f == null) {
                this.e.stop();
                this.e = null;
            }
        }
        if (this.f != null) {
            this.f.postDelayed(new j(this), j);
        }
    }

    /* access modifiers changed from: private */
    public void d(int i) {
        synchronized (this.d) {
            m mVar = (m) this.d.poll();
            if (mVar != null) {
                a(i, mVar, (Response) null);
            }
        }
    }

    public void a(int i, int i2, List<JceStruct> list) {
        if (!l.a(list) && list != null && list.size() != 0) {
            StatCSChannelData statCSChannelData = new StatCSChannelData();
            statCSChannelData.b = 2;
            statCSChannelData.c = 1;
            statCSChannelData.d = 1;
            statCSChannelData.f = i;
            StringBuilder sb = new StringBuilder();
            sb.append("cmd:");
            for (JceStruct a2 : list) {
                sb.append(l.a(a2)).append(";");
            }
            sb.append("type: " + i2);
            statCSChannelData.g = sb.toString();
            e.a().a(statCSChannelData);
        }
    }

    public void a(int i, m mVar, RspHead rspHead, int i2, List<RequestResponePair> list, boolean z) {
        int i3 = 1;
        if (mVar != null && mVar.f1632a != null && mVar.f1632a.size() != 0) {
            StatCSChannelData statCSChannelData = new StatCSChannelData();
            statCSChannelData.b = 2;
            statCSChannelData.c = 1;
            statCSChannelData.d = 2;
            statCSChannelData.f = i;
            StringBuilder sb = new StringBuilder();
            sb.append("errorCode:").append(i2).append(";");
            StringBuilder append = sb.append("isRehold:");
            if (!z) {
                i3 = 0;
            }
            append.append(i3).append(";");
            if (!z && list != null && list.size() > 0) {
                sb.append("RspErrorCode:");
                for (RequestResponePair next : list) {
                    sb.append("cmdId:").append(k.a(next.request)).append(" rsp:").append(next.errorCode).append(";");
                    if (next.response != null) {
                        try {
                            Field declaredField = next.response.getClass().getDeclaredField("ret");
                            declaredField.setAccessible(true);
                            sb.append(" ret:").append(((Integer) declaredField.get(next.response)).intValue()).append(";");
                        } catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException e2) {
                        }
                    }
                }
            }
            if (rspHead != null) {
                sb.append(" csTicketState:").append(rspHead.l).append(";");
            }
            statCSChannelData.g = sb.toString();
            e.a().a(statCSChannelData);
        }
    }

    public void a(int i, boolean z) {
        StringBuilder append;
        int i2 = 1;
        StatCSChannelData statCSChannelData = new StatCSChannelData();
        statCSChannelData.b = 2;
        statCSChannelData.c = 1;
        statCSChannelData.d = 3;
        statCSChannelData.f = i;
        if (!z) {
            i2 = 0;
        }
        append.append(i2).append(";");
        statCSChannelData.g = "addAuthFlag:";
        e.a().a(statCSChannelData);
    }
}
