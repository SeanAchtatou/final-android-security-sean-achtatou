package com.tencent.assistant.protocol.scu;

import com.tencent.assistant.protocol.a.e;
import com.tencent.assistant.protocol.jce.StatCSChannelData;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: ProGuard */
public class AppAuthState {

    /* renamed from: a  reason: collision with root package name */
    private AuthState f1619a = AuthState.STATE_NONE;
    private int b = 0;
    private ReferenceQueue<e> c = new ReferenceQueue<>();
    private ConcurrentLinkedQueue<WeakReference<e>> d = new ConcurrentLinkedQueue<>();
    private long e = -1;
    private long f = -1;
    private int g = -9999;
    private int h = -9999;

    /* compiled from: ProGuard */
    enum AuthState {
        STATE_NONE,
        STATE_AUTHING,
        STATE_SUCCESS,
        STATE_FAIL
    }

    public void a(e eVar) {
        if (eVar != null) {
            while (true) {
                WeakReference weakReference = (WeakReference) this.c.poll();
                if (weakReference == null) {
                    break;
                }
                this.d.remove(weakReference);
            }
            Iterator<WeakReference<e>> it = this.d.iterator();
            while (it.hasNext()) {
                if (eVar == ((e) it.next().get())) {
                    return;
                }
            }
            this.d.add(new WeakReference(eVar));
        }
    }

    public void a(AuthState authState, b bVar) {
        synchronized (this.f1619a) {
            switch (a.f1624a[authState.ordinal()]) {
                case 1:
                    e();
                    break;
                case 2:
                    f();
                    break;
                case 3:
                    g();
                    break;
                case 4:
                    a(bVar);
                    break;
            }
        }
    }

    private void e() {
        if (this.f1619a != AuthState.STATE_NONE) {
            AuthState authState = this.f1619a;
            this.f1619a = AuthState.STATE_NONE;
            if (e.a().n()) {
                a(authState, this.f1619a);
            }
        }
    }

    private void f() {
        if (this.f1619a != AuthState.STATE_AUTHING) {
            AuthState authState = this.f1619a;
            this.f1619a = AuthState.STATE_AUTHING;
            if (e.a().n()) {
                a(authState, this.f1619a);
            }
        }
    }

    private void g() {
        if (this.f1619a != AuthState.STATE_SUCCESS) {
            AuthState authState = this.f1619a;
            this.f1619a = AuthState.STATE_SUCCESS;
            if (e.a().n()) {
                a(authState, this.f1619a);
            }
            this.b = 0;
            h();
        }
        this.e = System.currentTimeMillis();
    }

    private void a(b bVar) {
        if (this.f1619a != AuthState.STATE_FAIL) {
            AuthState authState = this.f1619a;
            this.f1619a = AuthState.STATE_FAIL;
            if (e.a().n()) {
                a(authState, this.f1619a);
            }
            this.b++;
            a(this.b, bVar);
        }
        this.f = System.currentTimeMillis();
    }

    public boolean a() {
        return this.f1619a == AuthState.STATE_AUTHING;
    }

    public boolean b() {
        return System.currentTimeMillis() - this.e < e.a().m();
    }

    public boolean c() {
        return this.f1619a == AuthState.STATE_FAIL && System.currentTimeMillis() - this.f < e.a().o() && this.h == -8;
    }

    public void d() {
        a(AuthState.STATE_AUTHING, (b) null);
    }

    public void a(int i, int i2) {
        this.h = i;
        this.g = i2;
        a(AuthState.STATE_SUCCESS, (b) null);
    }

    public void b(int i, int i2) {
        this.h = i;
        this.g = i2;
        a(AuthState.STATE_FAIL, new b(this, i, i2));
    }

    private void h() {
        if (e.a().n()) {
            a((byte) 1);
        }
        Iterator<WeakReference<e>> it = this.d.iterator();
        while (it.hasNext()) {
            e eVar = (e) it.next().get();
            if (eVar != null) {
                eVar.h();
            }
        }
    }

    private void a(int i, b bVar) {
        if (e.a().n()) {
            a((byte) 2);
        }
        Iterator<WeakReference<e>> it = this.d.iterator();
        while (it.hasNext()) {
            e eVar = (e) it.next().get();
            if (!(eVar == null || bVar == null)) {
                eVar.a(i, c(), bVar.f1625a);
            }
        }
    }

    private void a(AuthState authState, AuthState authState2) {
        StatCSChannelData statCSChannelData = new StatCSChannelData();
        statCSChannelData.b = 2;
        statCSChannelData.c = 6;
        statCSChannelData.d = 5;
        StringBuilder sb = new StringBuilder();
        sb.append("oldState:").append(authState).append(";");
        sb.append("newState:").append(authState2).append(";");
        statCSChannelData.g = sb.toString();
        e.a().a(statCSChannelData);
    }

    private void a(byte b2) {
        StatCSChannelData statCSChannelData = new StatCSChannelData();
        statCSChannelData.b = 2;
        statCSChannelData.c = 6;
        statCSChannelData.d = 6;
        StringBuilder sb = new StringBuilder();
        sb.append("NotifyType:").append((int) b2).append(";");
        statCSChannelData.g = sb.toString();
        e.a().a(statCSChannelData);
    }
}
