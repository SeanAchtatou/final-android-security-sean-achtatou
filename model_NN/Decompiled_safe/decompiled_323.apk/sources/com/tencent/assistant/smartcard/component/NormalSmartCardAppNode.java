package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.text.Spanned;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class NormalSmartCardAppNode extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    RelativeLayout f1695a;
    TXAppIconView b;
    TextView c;
    DownloadButton d;
    ListItemInfoView e;
    TextView f;
    ImageView g;

    public NormalSmartCardAppNode(Context context) {
        this(context, (AttributeSet) null);
    }

    public NormalSmartCardAppNode(Context context, int i) {
        super(context);
        a(i);
    }

    public NormalSmartCardAppNode(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(R.layout.smartcard_applist_item);
    }

    private void a(int i) {
        try {
            this.f1695a = (RelativeLayout) LayoutInflater.from(getContext()).inflate(i, this);
            setBackgroundResource(R.drawable.smartcard_vertical_item_selector);
            this.b = (TXAppIconView) this.f1695a.findViewById(R.id.app_icon_img);
            this.c = (TextView) this.f1695a.findViewById(R.id.title);
            this.d = (DownloadButton) this.f1695a.findViewById(R.id.state_app_btn);
            this.e = (ListItemInfoView) this.f1695a.findViewById(R.id.download_info);
            this.f = (TextView) this.f1695a.findViewById(R.id.desc);
            this.g = (ImageView) this.f1695a.findViewById(R.id.cut_line);
        } catch (Throwable th) {
            t.a().b();
        }
    }

    public void a(SimpleAppModel simpleAppModel, Spanned spanned, STInfoV2 sTInfoV2, int i, boolean z, ListItemInfoView.InfoType infoType) {
        if (simpleAppModel != null && this.b != null) {
            if (this.b != null) {
                this.b.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            }
            this.c.setText(simpleAppModel.d);
            this.d.a(simpleAppModel);
            this.d.a(sTInfoV2);
            this.e.a(infoType);
            this.e.a(simpleAppModel);
            if (this.f != null) {
                this.f.setText(spanned);
            }
            if (this.g != null) {
                if (!z) {
                    this.g.setVisibility(8);
                } else {
                    this.g.setVisibility(0);
                }
            }
            setOnClickListener(new b(this, i, simpleAppModel, sTInfoV2));
        }
    }
}
