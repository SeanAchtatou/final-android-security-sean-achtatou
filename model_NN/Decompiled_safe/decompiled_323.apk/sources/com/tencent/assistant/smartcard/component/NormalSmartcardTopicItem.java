package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.smartcard.d.aa;
import com.tencent.assistant.smartcard.d.n;

/* compiled from: ProGuard */
public class NormalSmartcardTopicItem extends NormalSmartcardBaseItem {
    protected ImageView i;
    protected TXImageView l;
    protected TextView m;
    protected RelativeLayout n;
    private boolean o;

    public NormalSmartcardTopicItem(Context context) {
        super(context);
    }

    public NormalSmartcardTopicItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartcardTopicItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            this.c = this.b.inflate((int) R.layout.smartcard_topic, this);
            this.l = (TXImageView) this.c.findViewById(R.id.topic_pic);
            this.o = false;
            setOnClickListener(this.k);
            this.n = (RelativeLayout) this.c.findViewById(R.id.topic_more_layout);
            this.m = (TextView) this.c.findViewById(R.id.topic_title);
            this.i = (ImageView) this.c.findViewById(R.id.close);
            this.i.setOnClickListener(this.j);
            f();
        } catch (Exception e) {
            this.o = true;
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.o) {
            a();
        } else {
            f();
        }
    }

    /* access modifiers changed from: protected */
    public void f() {
        if (!this.o) {
            aa aaVar = null;
            if (this.d instanceof aa) {
                aaVar = (aa) this.d;
            }
            if (aaVar != null) {
                this.l.setInvalidater(this.g);
                this.l.updateImageView(aaVar.f1745a, -1, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
                this.n.setVisibility(8);
                aaVar.a(false);
                if (aaVar.a()) {
                    this.i.setVisibility(0);
                } else {
                    this.i.setVisibility(8);
                }
            }
        }
    }
}
