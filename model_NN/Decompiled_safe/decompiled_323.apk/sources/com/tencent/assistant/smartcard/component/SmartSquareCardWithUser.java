package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.y;
import com.tencent.assistant.smartcard.d.z;
import com.tencent.assistantv2.st.b.e;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.List;

/* compiled from: ProGuard */
public class SmartSquareCardWithUser extends NormalSmartcardBaseItem {
    private TextView i;
    private View l;
    private View m;
    private ImageView n;
    private ImageView o;
    private ImageView p;
    private ImageView q;
    private SmartSquareAppWithUserItem r;
    private SmartSquareAppWithUserItem s;
    private SmartSquareAppWithUserItem t;
    private SmartSquareAppWithUserItem u;
    private TextView v;
    private ImageView w;
    private View x;

    public SmartSquareCardWithUser(Context context) {
        this(context, null);
    }

    public SmartSquareCardWithUser(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SmartSquareCardWithUser(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
    }

    public void a(List<y> list) {
        if (list == null || list.size() < 2) {
            setVisibility(8);
            setMinimumHeight(0);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setVisibility(0);
        for (int i2 = 0; i2 < 2; i2++) {
            y yVar = list.get(i2);
            STInfoV2 a2 = a(yVar, i2);
            if (!(yVar == null || yVar.f1768a == null)) {
                if (i2 == 0) {
                    this.r.a(yVar, a2);
                } else {
                    this.s.a(yVar, a2);
                }
            }
        }
        if (list.size() >= 4) {
            this.m.setVisibility(0);
            this.o.setVisibility(0);
            this.p.setVisibility(0);
            this.q.setVisibility(8);
            this.n.setVisibility(0);
            for (int i3 = 2; i3 < 4; i3++) {
                y yVar2 = list.get(i3);
                STInfoV2 a3 = a(yVar2, i3);
                if (!(yVar2 == null || yVar2.f1768a == null)) {
                    new SmartSquareAppItem(this.f1693a).a(yVar2, a3);
                    if (i3 == 2) {
                        this.t.a(yVar2, a3);
                    } else {
                        this.u.a(yVar2, a3);
                    }
                }
            }
            return;
        }
        this.m.setVisibility(8);
        this.p.setVisibility(8);
        this.n.setVisibility(8);
        this.o.setVisibility(8);
        this.q.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            this.b.inflate((int) R.layout.smartcard_square_with_user, this);
            this.i = (TextView) findViewById(R.id.title);
            this.l = findViewById(R.id.topPart);
            this.m = findViewById(R.id.bottomPart);
            this.n = (ImageView) findViewById(R.id.line1_split);
            this.o = (ImageView) findViewById(R.id.line2_split);
            this.p = (ImageView) findViewById(R.id.line_split);
            this.q = (ImageView) findViewById(R.id.line1_single_split);
            this.r = (SmartSquareAppWithUserItem) findViewById(R.id.topleft_anchor);
            this.s = (SmartSquareAppWithUserItem) findViewById(R.id.topright_anchor);
            this.t = (SmartSquareAppWithUserItem) findViewById(R.id.bottomleft_anchor);
            this.u = (SmartSquareAppWithUserItem) findViewById(R.id.bottomright_anchor);
            this.v = (TextView) findViewById(R.id.show_all_titlte);
            this.w = (ImageView) findViewById(R.id.show_all_img);
            this.x = findViewById(R.id.show_all_layout);
            f();
        } catch (Exception e) {
            t.a().b();
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    private void f() {
        if (this.i != null) {
            z zVar = null;
            if (this.d instanceof z) {
                zVar = (z) this.d;
            }
            if (zVar != null) {
                this.i.setText(zVar.l);
                if (zVar.b()) {
                    this.i.setBackgroundResource(R.drawable.common_index_tag);
                    this.i.setTextColor(getResources().getColor(R.color.smart_card_applist_title_font_color));
                } else {
                    this.i.setBackgroundResource(0);
                    this.i.setTextColor(getResources().getColor(R.color.apk_name_v5));
                }
                String str = zVar.o;
                if (TextUtils.isEmpty(str) || TextUtils.isEmpty(zVar.p)) {
                    this.x.setVisibility(8);
                } else {
                    this.x.setVisibility(0);
                    this.v.setText(zVar.p);
                    this.x.setOnClickListener(new ao(this, str));
                }
                a(zVar.a());
            }
        }
    }

    private STInfoV2 a(y yVar, int i2) {
        STInfoV2 a2 = a(a.a("03", i2), 100);
        if (!(a2 == null || yVar == null)) {
            a2.updateWithSimpleAppModel(yVar.f1768a);
        }
        if (this.h == null) {
            this.h = new e();
        }
        this.h.a(a2);
        return a2;
    }
}
