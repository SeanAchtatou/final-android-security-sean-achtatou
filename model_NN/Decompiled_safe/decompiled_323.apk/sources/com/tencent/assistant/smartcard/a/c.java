package com.tencent.assistant.smartcard.a;

import android.content.Context;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.protocol.jce.SmartCardPicTemplate;
import com.tencent.assistant.smartcard.c.z;
import com.tencent.assistant.smartcard.component.NormalSmartcardBaseItem;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.a;
import com.tencent.assistant.smartcard.d.m;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.view.NormalSmartCardInterestItem;

/* compiled from: ProGuard */
public class c extends com.tencent.assistant.smartcard.c.c {
    public Class<? extends JceStruct> a() {
        return SmartCardPicTemplate.class;
    }

    public a b() {
        return new m();
    }

    /* access modifiers changed from: protected */
    public z c() {
        return new com.tencent.assistant.smartcard.e.c();
    }

    public NormalSmartcardBaseItem a(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        return new NormalSmartCardInterestItem(context, nVar, asVar, iViewInvalidater);
    }
}
