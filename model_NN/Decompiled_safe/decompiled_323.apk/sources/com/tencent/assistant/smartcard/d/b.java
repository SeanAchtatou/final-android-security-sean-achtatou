package com.tencent.assistant.smartcard.d;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.module.update.j;
import com.tencent.assistant.module.update.r;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.assistant.protocol.jce.SmartCardAppList;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.cb;
import com.tencent.assistant.utils.e;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.module.wisedownload.s;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* compiled from: ProGuard */
public class b extends n {

    /* renamed from: a  reason: collision with root package name */
    public int f1749a;
    public int b;
    public ArrayList<SimpleAppModel> c = new ArrayList<>();
    private int d = 0;

    public void a(int i, List<u> list) {
        this.d = 0;
        List<SimpleAppModel> b2 = b(i, list);
        this.c.clear();
        this.c.addAll(b2);
        this.b = this.d;
    }

    public void a(SmartCardAppList smartCardAppList, int i) {
        this.j = i;
        if (smartCardAppList != null) {
            this.l = smartCardAppList.f1499a;
            this.n = smartCardAppList.b;
            this.o = smartCardAppList.c;
            this.p = smartCardAppList.d;
            this.r = smartCardAppList.e;
        }
    }

    private List<SimpleAppModel> b(int i, List<u> list) {
        if (i == 2) {
            return b(list);
        }
        if (i == 919) {
            return c(list);
        }
        if (i == 4) {
            return d(list);
        }
        if (i == 3) {
            return e(list);
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>
     arg types: [com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, android.app.Dialog):android.app.Dialog
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.assistant.net.APN):com.tencent.assistant.net.APN
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$DownloadState):void
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, int):com.tencent.pangu.download.DownloadInfo
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, boolean):void
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.pangu.download.DownloadInfo> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.pangu.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.plugin.PluginDownloadInfo, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.k.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.m, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    private List<SimpleAppModel> b(List<u> list) {
        SimpleAppModel a2;
        ArrayList arrayList = new ArrayList();
        ArrayList<DownloadInfo> a3 = DownloadProxy.a().a(SimpleDownloadInfo.DownloadType.APK, true);
        if (a3 != null) {
            for (int i = 0; i < a3.size(); i++) {
                DownloadInfo downloadInfo = a3.get(i);
                if (k.a(downloadInfo, true, false) == AppConst.AppState.DOWNLOADED && (a2 = k.a(downloadInfo)) != null) {
                    AppConst.AppState d2 = k.d(a2);
                    if (e.c(a2.c, a2.g) && d2 == AppConst.AppState.DOWNLOADED) {
                        this.d++;
                        if (!a(a2, list)) {
                            arrayList.add(a2);
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    private List<SimpleAppModel> c(List<u> list) {
        ArrayList arrayList = new ArrayList();
        List<SimpleAppModel> a2 = k.a(false);
        if (a2 != null && a2.size() > 0) {
            for (SimpleAppModel next : a2) {
                if (k.d(next) == AppConst.AppState.DOWNLOADED) {
                    this.d++;
                    arrayList.add(next);
                }
            }
        }
        return arrayList;
    }

    private List<SimpleAppModel> d(List<u> list) {
        ArrayList arrayList = new ArrayList();
        List<AppUpdateInfo> a2 = k.a(j.b().a(2));
        if (a2 != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= a2.size()) {
                    break;
                }
                AppUpdateInfo appUpdateInfo = a2.get(i2);
                SimpleAppModel simpleAppModel = new SimpleAppModel();
                simpleAppModel.c = appUpdateInfo.f1162a;
                simpleAppModel.f938a = appUpdateInfo.o;
                k.c(simpleAppModel);
                AppConst.AppState d2 = k.d(simpleAppModel);
                if (e.c(simpleAppModel.c, simpleAppModel.g) && d2 == AppConst.AppState.UPDATE) {
                    this.d++;
                    if (!a(simpleAppModel, list)) {
                        arrayList.add(simpleAppModel);
                    }
                }
                i = i2 + 1;
            }
        }
        return arrayList;
    }

    private List<SimpleAppModel> e(List<u> list) {
        SimpleAppModel a2;
        ArrayList arrayList = new ArrayList();
        List<DownloadInfo> c2 = s.c();
        Collections.sort(c2, new cb());
        if (c2 != null) {
            r rVar = new r();
            Set<r> e = j.b().e();
            for (DownloadInfo next : c2) {
                rVar.a(next.packageName, next.versionName, next.versionCode, false);
                if ((e == null || !e.contains(rVar)) && (a2 = k.a(next)) != null) {
                    AppConst.AppState d2 = k.d(a2);
                    if (e.c(a2.c, a2.g) && d2 == AppConst.AppState.DOWNLOADED) {
                        this.d++;
                        if (!a(a2, list)) {
                            arrayList.add(a2);
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    private boolean a(SimpleAppModel simpleAppModel, List<u> list) {
        if (simpleAppModel != null) {
            if (list == null || list.size() == 0) {
                return false;
            }
            for (u next : list) {
                if (next != null && next.c.equalsIgnoreCase(simpleAppModel.c) && next.d == simpleAppModel.g && next.e == simpleAppModel.ad) {
                    return true;
                }
            }
        }
        return false;
    }

    public List<Long> d() {
        ArrayList arrayList = new ArrayList();
        if (this.c != null && this.c.size() > 0) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.f1749a || i2 >= this.c.size()) {
                    break;
                }
                arrayList.add(Long.valueOf(this.c.get(i2).f938a));
                i = i2 + 1;
            }
        }
        return arrayList;
    }

    public void a(List<Long> list) {
        if (list != null && list.size() != 0 && this.c != null && this.c.size() != 0) {
            ArrayList arrayList = new ArrayList();
            Iterator<SimpleAppModel> it = this.c.iterator();
            while (it.hasNext()) {
                SimpleAppModel next = it.next();
                if (list.contains(Long.valueOf(next.f938a))) {
                    arrayList.add(next);
                    XLog.d("jimluo", " remove the app : name " + next.d);
                }
            }
            if (arrayList.size() > 0) {
                this.c = new ArrayList<>(this.c);
            }
            this.c.removeAll(arrayList);
        }
    }
}
