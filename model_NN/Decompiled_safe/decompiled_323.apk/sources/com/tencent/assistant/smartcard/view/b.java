package com.tencent.assistant.smartcard.view;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class b extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1774a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ SimpleAppModel c;
    final /* synthetic */ NormalSmartCardGameRegisterNode d;

    b(NormalSmartCardGameRegisterNode normalSmartCardGameRegisterNode, int i, STInfoV2 sTInfoV2, SimpleAppModel simpleAppModel) {
        this.d = normalSmartCardGameRegisterNode;
        this.f1774a = i;
        this.b = sTInfoV2;
        this.c = simpleAppModel;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.d.getContext(), AppDetailActivityV5.class);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f1774a);
        intent.putExtra("simpleModeInfo", this.d.g);
        this.d.getContext().startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.actionId = 200;
            this.b.updateStatusToDetail(this.c);
            this.b.recommendId = this.c.y;
        }
        return this.b;
    }
}
