package com.tencent.assistant.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.manager.spaceclean.SpaceScanManager;

/* compiled from: ProGuard */
public class StorageLowReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        XLog.d("miles", "StorageLowReceiver >> broacast received.  action = " + action);
        if (action.equals("android.intent.action.DEVICE_STORAGE_LOW") && SpaceScanManager.a().g()) {
            SpaceScanManager.a().f();
            XLog.d("miles", "StorageLowReceiver >> deivce low memory broacast received.");
        }
    }
}
