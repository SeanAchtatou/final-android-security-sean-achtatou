package com.tencent.assistant.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.manager.DockRubbishRelateService;

/* compiled from: ProGuard */
public class DockRubbishRelateReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        XLog.i("DockRubbish", "[onReceive] ---> action : " + action);
        if (action != null && "com.tencent.android.qqdownloader.action.START_RUBBISH_SCAN".equals(action)) {
            Intent intent2 = new Intent(intent);
            intent2.setClass(context, DockRubbishRelateService.class);
            context.startService(intent2);
        }
    }
}
