package com.tencent.assistant.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.tencent.assistant.manager.t;

/* compiled from: ProGuard */
public class SdCardEventReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        t.a().a(a(intent));
    }

    private boolean a(Intent intent) {
        return intent.getAction().equals("android.intent.action.MEDIA_MOUNTED");
    }
}
