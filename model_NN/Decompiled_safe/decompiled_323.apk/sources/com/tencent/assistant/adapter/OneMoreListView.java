package com.tencent.assistant.adapter;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;
import com.tencent.assistant.manager.t;

/* compiled from: ProGuard */
public class OneMoreListView extends ListView {
    public OneMoreListView(Context context) {
        super(context);
    }

    public OneMoreListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public OneMoreListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void onMeasure(int i, int i2) {
        try {
            super.onMeasure(i, View.MeasureSpec.makeMeasureSpec(536870911, Integer.MIN_VALUE));
        } catch (Throwable th) {
            t.a().b();
        }
    }
}
