package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.k;
import com.tencent.assistantv2.model.ItemElement;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class h extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f592a;
    final /* synthetic */ ChildSettingAdapter b;

    h(ChildSettingAdapter childSettingAdapter, int i) {
        this.b = childSettingAdapter;
        this.f592a = i;
    }

    public void onTMAClick(View view) {
        ItemElement itemElement = (ItemElement) this.b.getItem(this.f592a);
        if (itemElement != null) {
            j jVar = (j) view.getTag();
            jVar.e.setSelected(!jVar.e.isSelected());
            k.a(itemElement.d, jVar.e.isSelected());
        }
    }

    public STInfoV2 getStInfo(View view) {
        if (!(view.getTag() instanceof j)) {
            return null;
        }
        return this.b.a(this.b.a(this.f592a), this.b.a(((j) view.getTag()).e.isSelected()), 200);
    }
}
