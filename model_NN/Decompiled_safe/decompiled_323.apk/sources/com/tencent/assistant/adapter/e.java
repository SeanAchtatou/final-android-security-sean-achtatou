package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppCategoryListAdapter f589a;

    e(AppCategoryListAdapter appCategoryListAdapter) {
        this.f589a = appCategoryListAdapter;
    }

    public void onClick(View view) {
        int i;
        try {
            i = ((Integer) view.getTag(R.id.category_pos)).intValue();
        } catch (Exception e) {
            e.printStackTrace();
            i = 0;
        }
        this.f589a.a((f) view.getTag(R.id.category_data), i / 20, i % 20, 200);
        if (this.f589a.z != null) {
            this.f589a.z.onClick(view);
        }
    }
}
