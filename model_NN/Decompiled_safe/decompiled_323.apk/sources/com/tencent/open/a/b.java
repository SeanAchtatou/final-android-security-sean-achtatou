package com.tencent.open.a;

import java.io.File;
import java.io.FileFilter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static SimpleDateFormat f3239a = l.a("yyyy-MM-dd");
    private static FileFilter b = new c();
    private String c = "Tracer.File";
    private int d = Integer.MAX_VALUE;
    private int e = Integer.MAX_VALUE;
    private int f = 4096;
    private long g = 10000;
    private File h;
    private int i = 10;
    private String j = ".log";
    private long k = Long.MAX_VALUE;
    private FileFilter l = new d(this);
    private Comparator<? super File> m = new e(this);

    public static long a(File file) {
        try {
            return f3239a.parse(file.getName()).getTime();
        } catch (Exception e2) {
            return -1;
        }
    }

    public b(File file, int i2, int i3, int i4, String str, long j2, int i5, String str2, long j3) {
        c(file);
        b(i2);
        a(i3);
        c(i4);
        a(str);
        b(j2);
        d(i5);
        b(str2);
        c(j3);
    }

    public File a() {
        return d(System.currentTimeMillis());
    }

    private File d(long j2) {
        return e(a(j2));
    }

    public File a(long j2) {
        File file = new File(g(), f3239a.format(Long.valueOf(j2)));
        file.mkdirs();
        return file;
    }

    private File e(File file) {
        File[] b2 = b(file);
        if (b2 == null || b2.length == 0) {
            return new File(file, "1" + i());
        }
        a(b2);
        File file2 = b2[b2.length - 1];
        int length = b2.length - e();
        if (((int) file2.length()) > d()) {
            file2 = new File(file, (f(file2) + 1) + i());
            length++;
        }
        for (int i2 = 0; i2 < length; i2++) {
            b2[i2].delete();
        }
        return file2;
    }

    public File[] b(File file) {
        return file.listFiles(this.l);
    }

    public void b() {
        File[] listFiles;
        if (g() != null && (listFiles = g().listFiles(b)) != null) {
            for (File file : listFiles) {
                if (System.currentTimeMillis() - a(file) > j()) {
                    i.a(file);
                }
            }
        }
    }

    public File[] a(File[] fileArr) {
        Arrays.sort(fileArr, this.m);
        return fileArr;
    }

    /* access modifiers changed from: private */
    public static int f(File file) {
        try {
            String name = file.getName();
            return Integer.parseInt(name.substring(0, name.indexOf(46)));
        } catch (Exception e2) {
            return -1;
        }
    }

    public String c() {
        return this.c;
    }

    public void a(String str) {
        this.c = str;
    }

    public int d() {
        return this.d;
    }

    public void a(int i2) {
        this.d = i2;
    }

    public int e() {
        return this.e;
    }

    public void b(int i2) {
        this.e = i2;
    }

    public int f() {
        return this.f;
    }

    public void c(int i2) {
        this.f = i2;
    }

    public void b(long j2) {
        this.g = j2;
    }

    public File g() {
        return this.h;
    }

    public void c(File file) {
        this.h = file;
    }

    public int h() {
        return this.i;
    }

    public void d(int i2) {
        this.i = i2;
    }

    public String i() {
        return this.j;
    }

    public void b(String str) {
        this.j = str;
    }

    public long j() {
        return this.k;
    }

    public void c(long j2) {
        this.k = j2;
    }
}
