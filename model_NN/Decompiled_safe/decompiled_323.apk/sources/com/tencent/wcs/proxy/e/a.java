package com.tencent.wcs.proxy.e;

import com.tencent.assistant.utils.q;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private ScheduledExecutorService f4132a;

    private a() {
        this.f4132a = Executors.newScheduledThreadPool(7, new q("AsyncService"));
    }

    public static a a() {
        return c.f4133a;
    }

    public void a(Runnable runnable, long j, TimeUnit timeUnit) {
        if (this.f4132a != null) {
            this.f4132a.schedule(runnable, j, timeUnit);
        }
    }

    public void a(Runnable runnable) {
        if (this.f4132a != null) {
            this.f4132a.submit(runnable);
        }
    }
}
