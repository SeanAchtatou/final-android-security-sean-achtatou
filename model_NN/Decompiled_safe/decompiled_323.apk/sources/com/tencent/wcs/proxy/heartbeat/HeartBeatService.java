package com.tencent.wcs.proxy.heartbeat;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import com.qq.taf.jce.JceInputStream;
import com.tencent.wcs.b.c;
import com.tencent.wcs.jce.MMHeartBeat;
import com.tencent.wcs.proxy.b;
import com.tencent.wcs.proxy.c.f;
import com.tencent.wcs.proxy.l;

/* compiled from: ProGuard */
public class HeartBeatService implements b {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static l f4137a;
    private Context b;
    private AlarmManager c;
    private PendingIntent d;
    private PendingIntent e;
    private int f = c.k;

    public HeartBeatService(Context context, l lVar) {
        this.b = context;
        f4137a = lVar;
        this.c = (AlarmManager) this.b.getSystemService("alarm");
        Intent intent = new Intent(this.b, HeartBeatTask.class);
        intent.setAction("com.tencent.wcs.proxy.heartbeat.HeartBeatService.ON_PULSE");
        this.d = PendingIntent.getBroadcast(this.b, 0, intent, 134217728);
        this.e = PendingIntent.getBroadcast(this.b, 0, intent, 536870912);
    }

    public void a() {
        if (!(this.c == null || this.d == null || this.e == null)) {
            this.c.cancel(this.e);
            this.c.setRepeating(2, SystemClock.elapsedRealtime() + ((long) (this.f * 1000)), (long) (this.f * 1000), this.d);
        }
        d();
    }

    public void b() {
        if (this.c != null && this.e != null) {
            this.c.cancel(this.e);
        }
    }

    public void a(int i) {
        if (i != 0 && this.f != i && i > 0 && i <= 1000) {
            c.k = i;
            this.f = i;
            if (!(this.c == null || this.d == null || this.e == null)) {
                this.c.cancel(this.e);
                this.c.setRepeating(2, SystemClock.elapsedRealtime() + ((long) (this.f * 1000)), (long) (this.f * 1000), this.d);
            }
            d();
        }
    }

    /* compiled from: ProGuard */
    public class HeartBeatTask extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            if ("com.tencent.wcs.proxy.heartbeat.HeartBeatService.ON_PULSE".equals(intent.getAction()) && HeartBeatService.f4137a != null) {
                HeartBeatService.f4137a.g();
            }
        }
    }

    public void a(f fVar) {
        JceInputStream jceInputStream = new JceInputStream(fVar.c());
        MMHeartBeat mMHeartBeat = new MMHeartBeat();
        mMHeartBeat.readFrom(jceInputStream);
        a(mMHeartBeat.b);
        if (c.f4064a) {
            com.tencent.wcs.c.b.a("HeartBeatService get pulse " + fVar.d() + " " + mMHeartBeat.b + " " + mMHeartBeat.f4090a);
        }
    }

    private void d() {
        if (f4137a != null) {
            f4137a.g();
        }
    }
}
