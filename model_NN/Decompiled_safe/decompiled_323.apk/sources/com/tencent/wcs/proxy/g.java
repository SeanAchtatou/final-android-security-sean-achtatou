package com.tencent.wcs.proxy;

import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.ah;
import com.tencent.wcs.c.b;
import com.tencent.wcs.proxy.b.e;
import com.tencent.wcs.proxy.b.h;
import com.tencent.wcs.proxy.b.k;
import com.tencent.wcs.proxy.b.n;
import com.tencent.wcs.proxy.b.q;
import com.tencent.wcs.proxy.b.t;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f4135a;

    g(c cVar) {
        this.f4135a = cVar;
    }

    public void run() {
        synchronized (this.f4135a.l) {
            if (this.f4135a.b != null) {
                this.f4135a.b.d();
                this.f4135a.g();
                this.f4135a.b.a(100);
                this.f4135a.b.a(1997);
                this.f4135a.b.a((int) STConstAction.ACTION_HIT_PAUSE);
                this.f4135a.b.a((int) STConst.ST_PAGE_NECESSARY);
                this.f4135a.b.a(1996);
                this.f4135a.b.a(1994);
                this.f4135a.b.a((int) STConstAction.ACTION_HIT_EXPAND);
            }
            if (this.f4135a.e != null) {
                this.f4135a.e.d();
            }
            if (this.f4135a.f != null) {
                this.f4135a.f.b();
            }
            e.b().a();
            h.b().a();
            n.b().a();
            q.b().a();
            t.b().a();
            k.b().a();
            ah.a().post(new h(this));
        }
        b.a("WanServiceManager stop wireless service over...");
    }
}
