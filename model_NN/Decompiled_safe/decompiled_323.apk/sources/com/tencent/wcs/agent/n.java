package com.tencent.wcs.agent;

import com.tencent.wcs.agent.config.RemoteType;
import com.tencent.wcs.b.c;
import com.tencent.wcs.c.b;
import java.io.IOException;

/* compiled from: ProGuard */
class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f4059a;

    n(l lVar) {
        this.f4059a = lVar;
    }

    public void run() {
        try {
            if (this.f4059a.k.b() == RemoteType.WEB) {
                this.f4059a.g();
            } else if (this.f4059a.k.b() == RemoteType.PC) {
                this.f4059a.f();
            }
        } catch (IOException e) {
            e.printStackTrace();
            if (!this.f4059a.g) {
                if (c.f4064a) {
                    b.a("AgentSessionOriginal read input exception channelId " + this.f4059a.m + " messageId " + this.f4059a.n + " port " + this.f4059a.q + " remote " + this.f4059a.k.a() + ", close it now");
                }
                synchronized (this.f4059a) {
                    this.f4059a.a(null, l.h(this.f4059a), 2);
                    if (this.f4059a.u != null) {
                        this.f4059a.u.a(this.f4059a.n);
                    }
                }
            }
        }
    }
}
