package com.tencent.assistantv2.component.banner.floatheader;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.banner.e;
import com.tencent.pangu.component.banner.f;
import java.util.List;

/* compiled from: ProGuard */
public class FloatNewBannerView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private List<f> f1975a;
    private int b;

    public FloatNewBannerView(Context context) {
        this(context, null);
    }

    public FloatNewBannerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public FloatNewBannerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        this.b = i;
        d();
    }

    private void d() {
        setOrientation(0);
        int a2 = by.a(getContext(), 8.0f);
        setPadding(a2, a2, a2, a2);
    }

    public void a(List<ColorCardItem> list) {
        this.f1975a = e.b(list, this.b == SmartListAdapter.SmartListType.GamePage.ordinal() ? 10 : 3, 2);
        e();
    }

    private void e() {
        removeAllViews();
        if (this.f1975a == null) {
            setVisibility(8);
            return;
        }
        setVisibility(0);
        int i = 0;
        for (f next : this.f1975a) {
            next.a(b());
            View b2 = next.b(getContext(), this, 0, 0, i);
            if (b2 != null) {
                try {
                    b2.setBackgroundResource(R.drawable.bg_card_selector);
                } catch (Throwable th) {
                }
                ViewGroup.LayoutParams layoutParams = b2.getLayoutParams();
                if (layoutParams == null) {
                    layoutParams = new LinearLayout.LayoutParams(-1, -1);
                }
                layoutParams.height = a();
                if (layoutParams instanceof LinearLayout.LayoutParams) {
                    ((LinearLayout.LayoutParams) layoutParams).weight = (float) next.a();
                    if (i > 0) {
                        ((LinearLayout.LayoutParams) layoutParams).leftMargin = by.a(getContext(), 8.0f);
                    }
                }
                addView(b2, layoutParams);
                i++;
            }
        }
    }

    public int a() {
        return by.a(getContext(), 43.0f);
    }

    public String b() {
        return Constants.VIA_REPORT_TYPE_SET_AVATAR;
    }

    public int c() {
        return by.a(getContext(), 8.0f) + a();
    }
}
