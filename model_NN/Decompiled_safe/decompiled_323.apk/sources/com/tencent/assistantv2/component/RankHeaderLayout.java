package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistantv2.manager.a;
import com.tencent.nucleus.manager.component.SwitchButton;

/* compiled from: ProGuard */
public class RankHeaderLayout extends TXLoadingLayoutBase {
    LinearLayout c;
    View d;
    private Context e;
    private SwitchButton f;
    private TextView g;
    private boolean h = true;

    public RankHeaderLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(TXScrollViewBase.ScrollMode.PULL_FROM_END);
    }

    public RankHeaderLayout(Context context, TXScrollViewBase.ScrollDirection scrollDirection, TXScrollViewBase.ScrollMode scrollMode) {
        super(context, scrollDirection, scrollMode);
        this.e = context;
        a(scrollMode);
    }

    private void a(TXScrollViewBase.ScrollMode scrollMode) {
        LayoutInflater.from(this.e).inflate((int) R.layout.v5_hide_installed_apps_area, this);
        this.c = (LinearLayout) findViewById(R.id.header_layout);
        this.d = findViewById(R.id.header_container);
        this.f = (SwitchButton) findViewById(R.id.hide_btn);
        this.g = (TextView) findViewById(R.id.text);
        this.f.a(a.a().b().c());
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.c.getLayoutParams();
        if (scrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_START) {
            layoutParams.gravity = 80;
        } else {
            layoutParams.gravity = 48;
        }
        reset();
    }

    public void a(boolean z) {
        this.f.a(z);
    }

    public void a(int i) {
        this.d.setPadding(this.d.getPaddingLeft(), this.d.getPaddingTop(), this.d.getPaddingRight(), this.d.getPaddingBottom() + i);
    }

    public void reset() {
        if (this.b == TXScrollViewBase.ScrollMode.PULL_FROM_START) {
            a();
        } else {
            this.c.setVisibility(0);
        }
    }

    public void pullToRefresh() {
        a();
    }

    public void releaseToRefresh() {
        this.c.setVisibility(0);
    }

    public void refreshing() {
        a();
    }

    public void loadFinish(String str) {
        this.c.setVisibility(0);
    }

    public void refreshSuc() {
        this.c.setVisibility(0);
    }

    public void refreshFail(String str) {
        this.c.setVisibility(0);
    }

    public void onPull(int i) {
        a();
    }

    public void hideAllSubViews() {
        if (getVisibility() == 0) {
        }
    }

    public void showAllSubViews() {
        this.c.setVisibility(0);
    }

    public void setWidth(int i) {
        getLayoutParams().width = i;
        requestLayout();
    }

    public void setHeight(int i) {
        getLayoutParams().height = i;
        requestLayout();
    }

    public int getContentSize() {
        return this.c.getHeight();
    }

    public int getTriggerSize() {
        return getResources().getDimensionPixelSize(R.dimen.tx_pull_to_refresh_trigger_size);
    }

    private void a() {
        this.c.setVisibility(0);
    }

    public void b(boolean z) {
        this.h = z;
    }

    public void loadSuc() {
    }

    public void loadFail() {
    }
}
