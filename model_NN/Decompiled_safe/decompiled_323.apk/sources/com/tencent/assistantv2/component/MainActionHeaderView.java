package com.tencent.assistantv2.component;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.a.a;
import com.tencent.assistant.component.DownloadProgressButton;
import com.tencent.assistant.component.SecondNavigationTitleView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.ExplicitHotWord;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.t;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.main.AssistantTabActivity;
import com.tencent.nucleus.socialcontact.login.l;
import com.tencent.nucleus.socialcontact.login.m;
import com.tencent.nucleus.socialcontact.usercenter.j;
import com.tencent.pangu.component.search.SearchTxtCycleShowView;
import com.tencent.pangu.link.b;
import com.tencent.pangu.model.a.c;
import com.tencent.pangu.module.h;
import java.util.HashMap;

/* compiled from: ProGuard */
public class MainActionHeaderView extends RelativeLayout implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    View.OnClickListener f1936a = new ag(this);
    /* access modifiers changed from: private */
    public Context b;
    private ImageButton c;
    private DownloadCenterButton d;
    /* access modifiers changed from: private */
    public TXImageView e;
    /* access modifiers changed from: private */
    public View f;
    /* access modifiers changed from: private */
    public SearchTxtCycleShowView g;
    /* access modifiers changed from: private */
    public ImageView h;
    /* access modifiers changed from: private */
    public TextView i;
    /* access modifiers changed from: private */
    public h j = h.a();
    private int k = 0;
    private int l = 1;
    private ImageView[] m = new ImageView[6];
    private int n = 0;
    private Animation.AnimationListener o = new ah(this);
    /* access modifiers changed from: private */
    public an p = new an(this, null);

    public MainActionHeaderView(Context context) {
        super(context);
        a(context);
    }

    public MainActionHeaderView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public MainActionHeaderView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context);
    }

    private void a(Context context) {
        this.b = context;
        System.currentTimeMillis();
        ah.a().post(new ae(this));
    }

    public void a() {
        AstApp.i().k().addUIEventListener(1019, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE, this);
        AstApp.i().k().addUIEventListener(1016, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        ah.a().postDelayed(new af(this), 2000);
    }

    public void b() {
        m();
        l();
        n();
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_INIT_IDENTITY_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USER_ACTIVITY_SUCCESS, this);
        g();
        if (this.d != null) {
            this.d.a();
        }
        if (this.g != null) {
            this.g.f();
        }
    }

    public void c() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USER_ACTIVITY_SUCCESS, this);
        if (this.g != null) {
            this.g.g();
        }
        if (this.d != null) {
            this.d.b();
        }
    }

    public void d() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_INIT_IDENTITY_SUCCESS, this);
        AstApp.i().k().removeUIEventListener(1019, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE, this);
        AstApp.i().k().removeUIEventListener(1016, this);
    }

    public void e() {
        if (this.g != null) {
            this.g.h();
        }
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
    }

    /* access modifiers changed from: private */
    public String b(View view) {
        if (view.getId() == this.f.getId()) {
            return SecondNavigationTitleView.TMA_ST_NAVBAR_SEARCH_TAG;
        }
        if (view.getId() == this.c.getId()) {
            return "03_001";
        }
        if (view.getId() == this.e.getId()) {
            return "00_001";
        }
        return STConst.ST_DEFAULT_SLOT;
    }

    /* access modifiers changed from: private */
    public void h() {
        this.d = (DownloadCenterButton) findViewById(R.id.mybtndownload);
        if (this.d != null) {
            this.d.a(DownloadProgressButton.TMA_ST_NAVBAR_DOWNLOAD_TAG);
        }
        this.c = (ImageButton) findViewById(R.id.mybtnpackage);
        this.c.setOnClickListener(this.f1936a);
        this.f = findViewById(R.id.mysearchenter);
        this.f.setOnClickListener(this.f1936a);
        this.g = (SearchTxtCycleShowView) findViewById(R.id.cycle_search_txt);
        this.i = (TextView) findViewById(R.id.update_number);
        this.g.a(2000L);
        this.e = (TXImageView) findViewById(R.id.mypic);
        this.e.setOnClickListener(this.f1936a);
        this.h = (ImageView) findViewById(R.id.iv_promot);
        i();
    }

    private void i() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < 6) {
                this.m[i3] = (ImageView) findViewById(R.id.head_icon_circle1 + i3);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        for (int i2 = 0; i2 < 6; i2++) {
            if (this.m[i2] != null) {
                this.m[i2].setVisibility(0);
            }
        }
        aw a2 = aw.a(this.m);
        if (a2 != null) {
            a2.setDuration(1000);
            a2.setInterpolator(new LinearInterpolator());
            a2.setRepeatCount(4);
            a2.setAnimationListener(this.o);
            if (this.m[0] != null) {
                this.m[0].startAnimation(a2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        if (this.m[0] != null) {
            this.m[0].clearAnimation();
        }
        for (int i2 = 0; i2 < 6; i2++) {
            if (this.m[i2] != null) {
                this.m[i2].setVisibility(8);
            }
        }
        j.d();
        l();
    }

    private void l() {
        TemporaryThreadManager.get().start(new ai(this));
    }

    public void a(View view) {
        ExplicitHotWord explicitHotWord;
        if (view.getId() == this.f.getId()) {
            String str = "tpmast://search?" + "&" + a.aa + "=" + c.a(f());
            if (this.g != null && (explicitHotWord = (ExplicitHotWord) this.g.e()) != null && !TextUtils.isEmpty(explicitHotWord.b) && !TextUtils.isEmpty(explicitHotWord.f1230a)) {
                str = (str + "&" + a.Y + "=" + explicitHotWord.b) + "&" + a.Z + "=" + explicitHotWord.f1230a;
                h.a().a(explicitHotWord);
            }
            b.b(getContext(), str, new Bundle());
        } else if (view.getId() == this.c.getId()) {
            getContext().startActivity(new Intent(getContext(), AssistantTabActivity.class));
            HashMap hashMap = new HashMap();
            hashMap.put("B1", Global.getPhoneGuidAndGen());
            hashMap.put("B2", Global.getQUAForBeacon());
            hashMap.put("B3", t.g());
            hashMap.put("B4", k.i() + Constants.STR_EMPTY);
            com.tencent.beacon.event.a.a("ManageIconClick", true, -1, -1, hashMap, true);
            XLog.d("beacon", "beacon report >> event : ManageIconClick, paras : " + hashMap.toString());
        } else if (view.getId() != this.e.getId()) {
        } else {
            if (com.tencent.nucleus.socialcontact.login.j.a().j()) {
                p();
            } else {
                o();
            }
        }
    }

    private void m() {
        m f2 = l.f();
        if (this.e == null) {
            return;
        }
        if (com.tencent.nucleus.socialcontact.login.j.a().j()) {
            this.e.updateImageView(f2.f3165a, R.drawable.common_owner_icon_01, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.e.setPadding(getResources().getDimensionPixelSize(R.dimen.main_action_head_pic_padding_left_right), getResources().getDimensionPixelSize(R.dimen.main_action_head_pic_padding_top), getResources().getDimensionPixelSize(R.dimen.main_action_head_pic_padding_left_right), getResources().getDimensionPixelSize(R.dimen.main_action_head_pic_padding_bottom));
            this.e.setBackgroundResource(R.drawable.common_owner_icon_02);
            findViewById(R.id.mypicup).setVisibility(0);
            return;
        }
        this.e.updateImageView(Constants.STR_EMPTY, R.drawable.common_owner_icon_01, TXImageView.TXImageViewType.LOCAL_IMAGE);
        this.e.setPadding(0, 0, 0, 0);
        this.e.setBackgroundDrawable(null);
        findViewById(R.id.mypicup).setVisibility(8);
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case 1016:
            case 1019:
            case EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE:
                n();
                return;
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS:
                m();
                return;
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL:
                m();
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                if (message.obj instanceof Bundle) {
                    Bundle bundle = (Bundle) message.obj;
                    if (bundle.containsKey(AppConst.KEY_FROM_TYPE) && bundle.getInt(AppConst.KEY_FROM_TYPE) == 8) {
                        XLog.i("xjp", "Login Secceeded");
                        p();
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_INIT_IDENTITY_SUCCESS:
                m();
                return;
            case EventDispatcherEnum.UI_EVENT_GET_USER_ACTIVITY_SUCCESS:
                l();
                return;
            default:
                return;
        }
    }

    private void n() {
        TemporaryThreadManager.get().start(new al(this));
    }

    private void o() {
        b.b(getContext(), "tmast://loginrichdialog", new Bundle());
    }

    private void p() {
        b.b(getContext(), "tmast://usercenter", new Bundle());
    }

    public void a(int i2) {
        this.k = i2;
    }

    public void b(int i2) {
        this.l = i2;
        com.tencent.pangu.model.a.a a2 = h.a().a(i2);
        String c2 = h.a().c();
        if (a2 != null && this.g != null) {
            this.g.b(a2.a(), c2);
        }
    }

    public int f() {
        return this.l;
    }

    public void g() {
        com.tencent.pangu.model.a.a a2 = h.a().a(this.l);
        String c2 = h.a().c();
        if (a2 != null && this.g != null) {
            this.g.b(a2.a(), c2);
        }
    }
}
