package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.pangu.download.DownloadInfo;

/* compiled from: ProGuard */
public interface j {
    int a();

    void a(View view);

    void a(View view, AppConst.AppState appState);

    void a(SimpleAppModel simpleAppModel, View view);

    void a(DownloadInfo downloadInfo);
}
