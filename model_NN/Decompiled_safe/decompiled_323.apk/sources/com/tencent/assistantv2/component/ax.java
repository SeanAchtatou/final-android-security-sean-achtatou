package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.utils.be;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class ax extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleViewV5 f1969a;

    ax(SecondNavigationTitleViewV5 secondNavigationTitleViewV5) {
        this.f1969a = secondNavigationTitleViewV5;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(com.tencent.assistantv2.component.SecondNavigationTitleViewV5, boolean):boolean
     arg types: [com.tencent.assistantv2.component.SecondNavigationTitleViewV5, int]
     candidates:
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(com.tencent.assistantv2.component.SecondNavigationTitleViewV5, java.lang.String):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(java.lang.String, int):void
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(java.lang.String, android.text.TextUtils$TruncateAt):void
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(boolean, int):void
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(com.tencent.assistantv2.component.SecondNavigationTitleViewV5, boolean):boolean */
    public void onTMAClick(View view) {
        if (this.f1969a.s != null) {
            this.f1969a.s.b(!be.b());
            be.a(this.f1969a.s.a());
            if (this.f1969a.r != null) {
                this.f1969a.r.setVisibility(8);
            }
            if (this.f1969a.z) {
                boolean unused = this.f1969a.z = false;
            }
            try {
                if (this.f1969a.k != null && !this.f1969a.k.isFinishing()) {
                    this.f1969a.s.show();
                }
            } catch (Throwable th) {
            }
            if (this.f1969a.y != null && this.f1969a.w != null) {
                this.f1969a.s.a(this.f1969a.y.f941a.f, this.f1969a.w.f938a);
            }
        }
    }

    public STInfoV2 getStInfo() {
        return this.f1969a.e(a.a("05", "001"));
    }
}
