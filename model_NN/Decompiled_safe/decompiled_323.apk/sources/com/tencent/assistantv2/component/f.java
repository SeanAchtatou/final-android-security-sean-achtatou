package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.manager.b;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class f extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f1988a;
    final /* synthetic */ STCommonInfo b;
    final /* synthetic */ j c;
    final /* synthetic */ b[] d;
    final /* synthetic */ DownloadButton e;

    f(DownloadButton downloadButton, SimpleAppModel simpleAppModel, STCommonInfo sTCommonInfo, j jVar, b[] bVarArr) {
        this.e = downloadButton;
        this.f1988a = simpleAppModel;
        this.b = sTCommonInfo;
        this.c = jVar;
        this.d = bVarArr;
    }

    public void onTMAClick(View view) {
        this.e.a(this.f1988a, this.b, this.c, this.d);
    }

    public STInfoV2 getStInfo() {
        if (this.b == null || !(this.b instanceof STInfoV2)) {
            return null;
        }
        this.b.updateStatus(this.f1988a);
        return (STInfoV2) this.b;
    }
}
