package com.tencent.assistantv2.component.banner.floatheader;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class b extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f1976a;
    final /* synthetic */ int b;
    final /* synthetic */ a c;

    b(a aVar, Context context, int i) {
        this.c = aVar;
        this.f1976a = context;
        this.b = i;
    }

    public void onTMAClick(View view) {
        com.tencent.pangu.link.b.a(this.f1976a, this.c.f3641a.e);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1976a, 200);
        buildSTInfo.slotId = a.a(this.c.b, this.b);
        return buildSTInfo;
    }
}
