package com.tencent.assistantv2.component.banner.floatheader;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.assistant.component.OverTurnView;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: ProGuard */
public class FloatBannerViewSwitcher extends OverTurnView {

    /* renamed from: a  reason: collision with root package name */
    private List<ColorCardItem> f1974a;
    private FloatOriginalBannerView b;
    private FloatNewBannerView c;
    private AbsListView d;
    private int e;
    private int f;
    private int g;

    public FloatBannerViewSwitcher(Context context) {
        this(context, null);
    }

    public FloatBannerViewSwitcher(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.e = -4;
        this.f = -1;
        this.g = -1;
    }

    public void a(AbsListView absListView, int i) {
        this.d = absListView;
        removeAllViews();
        this.b = new FloatOriginalBannerView(getContext(), null, i);
        addView(this.b);
        this.c = new FloatNewBannerView(getContext(), null, i);
        addView(this.c);
    }

    public void a(ArrayList<ColorCardItem> arrayList) {
        this.f1974a = Collections.synchronizedList(new ArrayList(arrayList));
        this.e = 3;
    }

    public void a() {
        if ((this.e & 1) > 0) {
            this.b.a(0, null, this.f1974a);
            this.e &= -2;
        }
        if (getCurrentView() instanceof FloatNewBannerView) {
            previous();
        }
        if (getVisibility() != 0) {
            setVisibility(0);
        }
    }

    public void b() {
        if ((this.e & 2) > 0) {
            this.c.a(this.f1974a);
            this.e &= -3;
        }
        if (getCurrentView() instanceof FloatOriginalBannerView) {
            a(d() - e(), d() - e());
            next();
        }
        if (getVisibility() != 0) {
            setVisibility(0);
        }
    }

    public void c() {
        if (getVisibility() != 8) {
            setVisibility(8);
        }
        if (getCurrentView() instanceof FloatNewBannerView) {
            previous();
        }
    }

    /* access modifiers changed from: protected */
    public void init() {
    }

    public View makeView() {
        return null;
    }

    public void doBeforeInit(Context context) {
    }

    public void a(int i, int i2) {
        if (this.b != null) {
            this.b.a(i, i2);
        }
    }

    public int d() {
        if (this.b != null) {
            return this.b.p();
        }
        return 0;
    }

    public int e() {
        if (this.c != null) {
            return this.c.c();
        }
        return 0;
    }

    public void a(AbsListView absListView, int i, int i2, int i3) {
        this.f = i;
        if (this.d != null) {
            if (i == 1) {
                View childAt = this.d.getChildAt(i);
                if (childAt != null) {
                    this.g = childAt.getTop();
                }
                if (this.g > d()) {
                    c();
                } else if (this.g > d() || this.g <= e()) {
                    b();
                } else {
                    a();
                    a(d() - this.g, d() - e());
                }
            } else if (i < 1) {
                c();
            } else {
                b();
            }
        }
    }

    public void b(AbsListView absListView, int i) {
        if (this.d != null && i == 0 && this.f == 1) {
            View childAt = this.d.getChildAt(this.f);
            if (childAt != null) {
                this.g = childAt.getTop();
            }
            if (this.g <= d() && this.g > e()) {
                a(d() - e(), d() - e());
                if ((this.d instanceof ListView) && this.d.getAdapter() != null && ((ListAdapter) this.d.getAdapter()).getCount() > 2) {
                    ((ListView) this.d).setSelectionFromTop(2, d() - e());
                }
            }
        }
    }

    public Animation setInDownAnim(Context context) {
        return null;
    }

    public Animation setOutDownAnim(Context context) {
        return null;
    }

    public Animation setInUpAnim(Context context) {
        return null;
    }

    public Animation setOutUpAnim(Context context) {
        return null;
    }

    public void f() {
        if (getVisibility() != 0 || this.d == null) {
            return;
        }
        if (this.g > d()) {
            c();
        } else if (this.g > d() || this.g <= e()) {
            b();
        } else {
            a(d() - e(), d() - e());
            if ((this.d instanceof ListView) && this.d.getAdapter() != null && ((ListAdapter) this.d.getAdapter()).getCount() > 2) {
                ((ListView) this.d).setSelectionFromTop(2, d() - e());
            }
        }
    }

    public void g() {
    }
}
