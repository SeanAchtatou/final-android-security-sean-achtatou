package com.tencent.assistantv2.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.activity.DownloadActivity;

/* compiled from: ProGuard */
class m extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadCenterButton f2003a;

    m(DownloadCenterButton downloadCenterButton) {
        this.f2003a = downloadCenterButton;
    }

    public void onTMAClick(View view) {
        boolean z;
        Intent intent = new Intent(this.f2003a.e, DownloadActivity.class);
        if (this.f2003a.d.getVisibility() == 0) {
            z = true;
        } else {
            z = false;
        }
        intent.putExtra("has_new_app_to_install", z);
        this.f2003a.e.startActivity(intent);
        com.tencent.assistant.m.a().b(Constants.STR_EMPTY, "key_download_center_red_dot", false);
    }

    public STInfoV2 getStInfo(View view) {
        if (view == null) {
            return null;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f2003a.getContext(), 200);
        buildSTInfo.slotId = this.f2003a.k;
        return buildSTInfo;
    }
}
