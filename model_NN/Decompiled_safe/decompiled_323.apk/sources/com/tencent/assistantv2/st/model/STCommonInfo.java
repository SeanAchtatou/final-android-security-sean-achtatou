package com.tencent.assistantv2.st.model;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STExternalInfo;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.component.appdetail.process.s;
import java.io.Serializable;

/* compiled from: ProGuard */
public class STCommonInfo implements Serializable, Cloneable {
    public int actionFlag;
    public int actionId;
    public String callerPackageName;
    public String callerUin;
    public String callerVersionCode;
    public String callerVia;
    public String contentId;
    public String expatiation;
    public String extraData;
    public long pushId;
    public String pushInfo;
    public int rankGroupId = 0;
    public byte[] recommendId = null;
    public int scene = 2000;
    public long searchId = 0;
    public String searchPreId;
    public String slotId = STConst.ST_DEFAULT_SLOT;
    public int sourceScene = 2000;
    public String sourceSceneSlotId = STConst.ST_DEFAULT_SLOT;
    public String status = STConst.ST_STATUS_DEFAULT;
    public String traceId;

    /* compiled from: ProGuard */
    public enum ContentIdType {
        CATEGORY,
        SPECIAL,
        DETAILAPPTAG
    }

    public void updateStatus(SimpleAppModel simpleAppModel) {
        AppConst.AppState d = k.d(simpleAppModel);
        if (!s.a(simpleAppModel, d)) {
            this.actionId = a.a(d);
        } else {
            this.actionId = 200;
        }
        this.status = a.a(d, simpleAppModel);
    }

    public void updateStatusToDetail(SimpleAppModel simpleAppModel) {
        this.status = "01";
    }

    public void updateWithExternalPara(String str, String str2, String str3, String str4, String str5) {
        this.callerVia = str;
        this.callerUin = str2;
        this.callerPackageName = str3;
        this.callerVersionCode = str4;
        this.traceId = str5;
    }

    public void updateWithExternalPara(STExternalInfo sTExternalInfo) {
        if (sTExternalInfo != null) {
            this.callerVia = sTExternalInfo.f2059a;
            this.callerUin = sTExternalInfo.b;
            this.callerPackageName = sTExternalInfo.c;
            this.callerVersionCode = sTExternalInfo.d;
            this.traceId = sTExternalInfo.e;
        }
    }

    public void setCategoryId(long j) {
        if (j > 0) {
            updateContentId(ContentIdType.CATEGORY, String.valueOf(j));
        }
    }

    public void updateContentId(ContentIdType contentIdType, String str) {
        this.contentId = a.a(contentIdType, str);
    }

    public String getFinalSlotId() {
        return a.b(this.slotId, this.status);
    }
}
