package com.tencent.assistantv2.st;

import android.text.TextUtils;
import com.tencent.assistant.protocol.jce.StatDiffMerge;
import com.tencent.assistant.protocol.jce.StatStdReport;
import com.tencent.assistant.protocol.jce.StatTraffic;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.st.business.BaseSTManagerV2;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.business.c;
import com.tencent.assistantv2.st.business.d;
import com.tencent.assistantv2.st.business.k;
import com.tencent.assistantv2.st.business.n;
import com.tencent.assistantv2.st.business.o;
import com.tencent.assistantv2.st.business.v;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.downloadsdk.ae;
import com.tencent.nucleus.manager.usagestats.UsagestatsSTManager;
import com.tencent.nucleus.manager.usagestats.a;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class l {
    public static void a(STInfoV2 sTInfoV2, Map<String, String> map) {
        if (sTInfoV2 != null) {
            if (map != null) {
                String str = map.get(STConst.KEY_TMA_ST_APPID);
                if (!TextUtils.isEmpty(str)) {
                    sTInfoV2.appId = (long) bm.d(str);
                }
                String str2 = map.get(STConst.KEY_TMA_ST_EXTRADATA);
                if (!TextUtils.isEmpty(str2)) {
                    sTInfoV2.extraData = str2;
                }
                String str3 = map.get(STConst.KEY_TMA_ST_PACKAGE_NAME);
                if (!TextUtils.isEmpty(str3)) {
                    sTInfoV2.packageName = str3;
                }
                String str4 = map.get(STConst.KEY_TMA_ST_VIA);
                if (!TextUtils.isEmpty(str4)) {
                    sTInfoV2.callerVia = str4;
                }
                String str5 = map.get(STConst.KEY_TMA_ST_UIN);
                if (!TextUtils.isEmpty(str5)) {
                    sTInfoV2.callerUin = str5;
                }
                String str6 = map.get(STConst.KEY_TMA_ST_HOST_VERSIONCODE);
                if (!TextUtils.isEmpty(str6)) {
                    sTInfoV2.callerVersionCode = str6;
                }
                String str7 = map.get(STConst.KEY_TMA_ST_SEARCH_PRE_ID);
                if (!TextUtils.isEmpty(str7)) {
                    sTInfoV2.searchPreId = str7;
                }
                String str8 = map.get(STConst.KEY_TMA_ST_EXPATIATION);
                if (!TextUtils.isEmpty(str8)) {
                    sTInfoV2.expatiation = str8;
                }
            }
            a(sTInfoV2);
        }
    }

    public static void a(STInfoV2 sTInfoV2) {
        if (sTInfoV2 != null) {
            BaseSTManagerV2 a2 = b.a().a(6);
            if (a2 == null) {
                a2 = new v();
                b.a().a(6, a2);
            }
            if (sTInfoV2.isImmediately || sTInfoV2.actionId == 900 || sTInfoV2.actionId == 905) {
                a2.reportRealTime(sTInfoV2);
            } else {
                a2.report(sTInfoV2);
            }
        }
    }

    public static ae a(String str, long j, long j2, byte b, StatInfo statInfo, SimpleDownloadInfo.UIType uIType, SimpleDownloadInfo.DownloadType downloadType) {
        BaseSTManagerV2 a2 = b.a().a(14);
        if (a2 == null) {
            a2 = c.a();
            b.a().a(14, a2);
        }
        if (!(a2 instanceof c)) {
            return null;
        }
        c cVar = (c) a2;
        cVar.a(str, j, j2, b, statInfo, uIType, downloadType);
        return cVar;
    }

    private static BaseSTManagerV2 a() {
        BaseSTManagerV2 a2 = b.a().a(5);
        if (a2 != null) {
            return a2;
        }
        o a3 = o.a();
        b.a().a(5, a3);
        return a3;
    }

    public static void a(DownloadInfo downloadInfo, byte b, boolean z) {
        BaseSTManagerV2 a2 = a();
        if (a2 instanceof o) {
            ((o) a2).a(downloadInfo, b, z);
        }
    }

    public static void a(String str, int i, byte b, byte b2, String str2) {
        BaseSTManagerV2 a2 = a();
        if (a2 != null && (a2 instanceof o)) {
            ((o) a2).a(str, i, b, b2, str2);
        }
    }

    public static void b(String str, int i, byte b, byte b2, String str2) {
        BaseSTManagerV2 a2 = a();
        if (a2 != null && (a2 instanceof o)) {
            ((o) a2).b(str, i, b, b2, str2);
        }
    }

    public static void a(String str, int i, byte b, byte b2, String str2, boolean z) {
        BaseSTManagerV2 a2 = a();
        if (a2 != null && (a2 instanceof o)) {
            ((o) a2).a(str, i, b, b2, str2, z);
        }
    }

    public static void a(boolean z) {
        k.a(z);
    }

    public static void a(List<StatTraffic> list) {
        d.a(list);
    }

    public static void a(boolean z, long j, long j2, int i, String str, StatInfo statInfo, String str2) {
        b.a().b((byte) 16, new StatDiffMerge(z ? (byte) 1 : 0, j, j2, (byte) i, str, str2));
    }

    public static void a(UsagestatsSTManager.ReportScene reportScene) {
        BaseSTManagerV2 a2 = b.a().a(17);
        if (a2 == null) {
            a2 = new a();
            b.a().a(17, a2);
        }
        if (a2 instanceof a) {
            ((a) a2).a(reportScene);
        }
    }

    public static void a(int i, String str) {
        BaseSTManagerV2 a2 = b.a().a(19);
        if (a2 == null) {
            a2 = new n();
            b.a().a(19, a2);
        }
        if (a2 instanceof n) {
            ((n) a2).a(i, str);
        }
    }

    public static void a(int i, CostTimeSTManager.TIMETYPE timetype, long j) {
        BaseSTManagerV2 a2 = b.a().a(20);
        if (a2 == null) {
            a2 = CostTimeSTManager.a();
            b.a().a(20, a2);
        }
        if (a2 instanceof CostTimeSTManager) {
            ((CostTimeSTManager) a2).a(i, timetype, j);
        }
    }

    public static void a(String str, String str2, String str3) {
        XLog.d("NewLogStd", "***** statName=" + str + " traceId=" + str2 + " content=" + str3);
        StatStdReport statStdReport = new StatStdReport();
        statStdReport.f1565a = str;
        statStdReport.f = str2;
        statStdReport.c = str3;
        b.a().a((byte) 23, statStdReport);
    }
}
