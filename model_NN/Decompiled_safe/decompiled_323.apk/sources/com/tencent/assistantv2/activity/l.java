package com.tencent.assistantv2.activity;

import com.qq.AppService.AstApp;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.m;
import com.tencent.assistant.module.ag;
import com.tencent.assistant.module.timer.job.GetSettingTimerJob;
import com.tencent.assistant.module.timer.job.GetUnionUpdateInfoTimerJob;
import com.tencent.assistant.module.update.t;
import com.tencent.assistant.utils.bh;
import com.tencent.assistant.utils.e;
import com.tencent.nucleus.manager.spaceclean.an;

/* compiled from: ProGuard */
class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MainActivity f1878a;

    l(MainActivity mainActivity) {
        this.f1878a = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    public void run() {
        try {
            bh.a().b();
            GetSettingTimerJob.e().d();
            ag.b().a();
            GetUnionUpdateInfoTimerJob.e().d();
            com.tencent.nucleus.manager.main.ag.a().b();
            t.a().b();
            ApkResourceManager.loadTraffic();
            new an().run();
            if (m.a().a("first_use", true)) {
                e.d(this.f1878a, "应用宝");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                e.d(this.f1878a, "QQ应用宝");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
                e.a(this.f1878a, AstApp.i().getPackageName(), "应用助手");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                }
                if (!e.b(this.f1878a, AstApp.i().getPackageName())) {
                    e.c(this.f1878a, AstApp.i().getPackageName());
                    e.a();
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e4) {
                        e4.printStackTrace();
                    }
                    e.a(this.f1878a, AstApp.i().getPackageName());
                }
                m.a().b("first_use", (Object) false);
            }
        } catch (Exception e5) {
        }
    }
}
