package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import com.qq.AppService.AstApp;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXRefreshGetMoreListView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.module.callback.l;
import com.tencent.assistant.module.k;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.smartcard.component.ISmartcard;
import com.tencent.assistant.smartcard.component.aq;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.component.GameBannerView;
import com.tencent.assistantv2.component.QuickBannerView;
import com.tencent.assistantv2.component.SoftwareBannerView;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.cloud.model.SimpleEbookModel;
import com.tencent.cloud.model.SimpleVideoModel;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.pangu.c.p;
import com.tencent.pangu.component.banner.e;
import com.tencent.pangu.component.banner.f;
import com.tencent.pangu.component.topbanner.TopBannerView;
import com.tencent.pangu.component.topbanner.a;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.model.b;
import com.tencent.pangu.model.g;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public abstract class SmartListAdapter extends BaseAdapter implements UIEventListener {
    private static int u = SmartItemType.values().length;
    private boolean A = false;
    private a B;
    private TopBannerView C;
    private g D;
    private boolean E = true;
    private boolean F = false;
    private GameBannerView G;
    private as H = new af(this);

    /* renamed from: a  reason: collision with root package name */
    private boolean f1897a = false;
    private boolean b = false;
    private boolean c = false;
    private AstApp d = AstApp.i();
    private Context e;
    private List<f> f = new ArrayList();
    private List<ColorCardItem> g = new ArrayList();
    /* access modifiers changed from: private */
    public List<b> h = new ArrayList();
    private QuickBannerView i;
    private List<com.tencent.pangu.c.a> j;
    private int k = 1;
    private ListView l;
    private int m = 2000;
    private long n = -100;
    private long o = 0;
    private com.tencent.assistant.model.b p = new com.tencent.assistant.model.b();
    private int q = 0;
    private l r;
    private boolean s = false;
    private Map<String, Integer> t = new HashMap(30);
    private IViewInvalidater v;
    private boolean w = false;
    private b x = null;
    private com.tencent.assistantv2.st.b.b y = null;
    private String z = null;

    /* compiled from: ProGuard */
    public enum BannerType {
        None,
        HomePage,
        QuickEntrance,
        All
    }

    /* compiled from: ProGuard */
    public enum SmartListType {
        DiscoverPage,
        AppPage,
        SoftWare,
        SearchPage,
        GamePage,
        CategoryDetailPage
    }

    /* access modifiers changed from: protected */
    public abstract String b(int i2);

    /* access modifiers changed from: protected */
    public abstract SmartListType o();

    public SmartListAdapter(Context context, View view, com.tencent.assistant.model.b bVar) {
        this.e = context;
        this.p = bVar;
        if (view != null && (view instanceof TXRefreshGetMoreListView)) {
            this.l = ((TXRefreshGetMoreListView) view).getListView();
        } else if (view != null && (view instanceof TXGetMoreListView)) {
            this.l = ((TXGetMoreListView) view).getListView();
        } else if (view instanceof ListView) {
            this.l = (ListView) view;
        }
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.d.k().addUIEventListener(1016, this);
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ENTRANCE_DOWNLOAD_PAGE, this);
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ENTRANCE_UPDATE_PAGE, this);
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGOUT, this);
        this.j = new ArrayList();
        this.y = new com.tencent.assistantv2.st.b.b();
    }

    public void a(a aVar) {
        this.B = aVar;
    }

    public void a(boolean z2, List<b> list, List<f> list2, List<ColorCardItem> list3) {
        a(z2, list, list2, list3, 0);
    }

    public void a(boolean z2, List<b> list, List<f> list2, List<ColorCardItem> list3, long j2) {
        if (list != null) {
            if (z2) {
                this.h.clear();
                this.f.clear();
                this.g.clear();
                if (!(list2 == null && list3 == null)) {
                    if (list2 != null) {
                        this.f.addAll(list2);
                    }
                    if (list3 != null) {
                        this.g.addAll(list3);
                    }
                    if (this.i != null) {
                        this.i.a(j2, this.f, this.g);
                    }
                }
                if (o() == SmartListType.AppPage) {
                    com.tencent.assistantv2.st.l.a(200501, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                } else if (o() == SmartListType.GamePage) {
                    com.tencent.assistantv2.st.l.a((int) STConst.ST_PAGE_COMPETITIVE, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                } else if (o() == SmartListType.DiscoverPage) {
                    com.tencent.assistantv2.st.l.a((int) STConst.ST_PAGE_COMPETITIVE, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                }
                this.q++;
            }
            this.h.addAll(list);
            this.s = true;
            notifyDataSetChanged();
        }
    }

    public void a(boolean z2, List<b> list, List<f> list2, g gVar, List<ColorCardItem> list3, long j2) {
        if (list != null) {
            if (z2) {
                this.F = true;
                this.h.clear();
                this.f.clear();
                this.g.clear();
                if (!(list2 == null && list3 == null && gVar == null)) {
                    if (list2 != null) {
                        this.f.addAll(list2);
                    }
                    if (list3 != null) {
                        this.g.addAll(list3);
                    }
                    if (this.i != null) {
                        this.i.a(j2, this.f, this.g);
                    }
                    if (gVar != null) {
                        if ((this.D == null && gVar.f3888a != 0) || !(this.D == null || (gVar.f3888a == this.D.f3888a && this.D.m == gVar.m))) {
                            this.D = gVar;
                            XLog.d("jiaxinwu", "replace banner");
                        }
                        if (this.D != null) {
                            this.D.k = com.tencent.pangu.component.topbanner.b.a().e(this.D);
                            if (com.tencent.pangu.component.topbanner.b.a().d(this.D)) {
                                c(true);
                                if (this.C != null) {
                                    this.C.a(this.D);
                                }
                            } else {
                                c(false);
                                if (this.C != null) {
                                    this.C.a();
                                    this.B.c();
                                }
                            }
                        }
                    }
                }
                if (o() == SmartListType.AppPage) {
                    com.tencent.assistantv2.st.l.a(200501, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                } else if (o() == SmartListType.GamePage) {
                    com.tencent.assistantv2.st.l.a((int) STConst.ST_PAGE_COMPETITIVE, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                } else if (o() == SmartListType.DiscoverPage) {
                    com.tencent.assistantv2.st.l.a((int) STConst.ST_PAGE_COMPETITIVE, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                }
                this.q++;
            }
            this.h.addAll(list);
            this.s = true;
            notifyDataSetChanged();
        }
    }

    public void a(int i2, long j2) {
        this.m = i2;
        this.n = j2;
    }

    public void a(int i2, long j2, long j3) {
        this.m = i2;
        this.n = j2;
        this.o = j3;
    }

    public void a() {
        this.h.clear();
        this.f.clear();
        this.g.clear();
        if (this.l != null) {
            for (int i2 = 0; i2 < this.l.getChildCount(); i2++) {
                View childAt = this.l.getChildAt(i2);
                if (childAt != null && (childAt.getTag() instanceof v)) {
                    ((v) childAt.getTag()).j.k = 0;
                }
            }
        }
        notifyDataSetChanged();
    }

    public void a(IViewInvalidater iViewInvalidater) {
        this.v = iViewInvalidater;
    }

    public void a(SimpleAppModel simpleAppModel) {
        if (this.h != null && simpleAppModel != null && !TextUtils.isEmpty(simpleAppModel.c)) {
            for (b next : this.h) {
                if (next != null && next.c() != null && simpleAppModel.c.equals(next.c().c)) {
                    next.c().ao = null;
                    this.w = true;
                    this.x = next;
                    notifyDataSetChanged();
                    return;
                }
            }
        }
    }

    public void a(String str) {
        this.z = str;
    }

    public String b() {
        return this.z;
    }

    public long c() {
        if (this.p == null) {
            return -1;
        }
        return this.p.a();
    }

    public void a(com.tencent.assistant.model.b bVar) {
        this.p = bVar;
    }

    public Context d() {
        return this.e;
    }

    public int e() {
        if (this.h != null) {
            return this.h.size();
        }
        return 0;
    }

    public int getCount() {
        int size = this.h != null ? this.h.size() : 0;
        if (this.c) {
            size++;
        }
        if (this.A) {
            return size + 1;
        }
        return size;
    }

    public boolean f() {
        return this.h != null && this.h.size() > 0;
    }

    public boolean g() {
        return this.s;
    }

    public Object getItem(int i2) {
        int c2 = c(i2);
        if (this.h == null || c2 < 0) {
            return null;
        }
        return this.h.get(c2);
    }

    public void h() {
        this.d.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.d.k().removeUIEventListener(1016, this);
        this.d.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, this);
        com.tencent.assistant.module.as.a().unregister(this.r);
        p.a().c();
        for (com.tencent.pangu.c.a f2 : this.j) {
            f2.f();
        }
        if (this.i != null && (this.i instanceof SoftwareBannerView)) {
            ((SoftwareBannerView) this.i).h();
        }
    }

    public void i() {
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.d.k().addUIEventListener(1016, this);
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, this);
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ENTRANCE_UPDATE_PAGE, this);
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ENTRANCE_DOWNLOAD_PAGE, this);
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGOUT, this);
        for (com.tencent.pangu.c.a e2 : this.j) {
            e2.e();
        }
        com.tencent.assistant.module.as.a().register(this.r);
        if (this.i != null && (this.i instanceof SoftwareBannerView)) {
            ((SoftwareBannerView) this.i).i();
        }
    }

    public void j() {
        this.d.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ENTRANCE_DOWNLOAD_PAGE, this);
        this.d.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ENTRANCE_UPDATE_PAGE, this);
        this.d.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        this.d.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ENTRANCE_UPDATE_PAGE, this);
        this.d.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ENTRANCE_DOWNLOAD_PAGE, this);
        this.d.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGOUT, this);
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        b bVar = null;
        int c2 = c(i2);
        if (this.h != null && c2 >= 0 && c2 < this.h.size()) {
            bVar = this.h.get(c2);
        }
        int itemViewType = getItemViewType(i2);
        if (itemViewType >= SmartItemType.values().length && bVar != null) {
            n f2 = bVar.f();
            z a2 = new z().b(this.m).a(this.z).a(c()).a(i2);
            if (view == null || !(view instanceof ISmartcard)) {
                return aq.a().a(this.e, f2, this.H, this.q, a2, this.v);
            }
            return aq.a().a((ISmartcard) view, this.e, f2, this.H, this.q, a2, this.v);
        } else if (SmartItemType.BANNER.ordinal() == itemViewType) {
            if (o() == SmartListType.GamePage) {
                if (this.G == null) {
                    this.G = new GameBannerView(this.e, null, SmartListType.GamePage.ordinal());
                }
                this.i = this.G.a();
                GameBannerView gameBannerView = this.G;
                this.G.a((long) this.k, this.f, this.g);
                return gameBannerView;
            }
            if (this.i == null) {
                if (o() == SmartListType.AppPage) {
                    this.i = new SoftwareBannerView(this.e, null, SmartListType.AppPage.ordinal());
                } else {
                    this.i = new QuickBannerView(this.e, null, o().ordinal());
                }
                this.i.a((long) this.k, this.f, this.g);
            }
            return this.i;
        } else if (SmartItemType.TOPBANNER.ordinal() == itemViewType) {
            if (this.C == null) {
                this.C = new TopBannerView(this.e, null, SmartListType.DiscoverPage.ordinal());
                this.C.a(this.B);
                if (this.D != null) {
                    this.C.a(this.D);
                }
            }
            this.C.a(this.E, this.F);
            XLog.d("jiaxinwu", "refreshView(topBanner)");
            this.E = false;
            this.F = false;
            return this.C;
        } else {
            aa a3 = new aa().a(this.p).a(this.m).b(this.f1897a).c(this.b).a(this.n).a(this.l);
            if (this.w && this.x != null && this.x == bVar && this.x.c != null) {
                a3.a(true);
            }
            if (bVar == null) {
                return null;
            }
            d e2 = k.e(bVar.c());
            STInfoV2 a4 = a(i2, bVar, 100, e2);
            if (!l() || ((this.e instanceof MainActivity) && ((MainActivity) this.e).a(o()))) {
                boolean z2 = true;
                if (m()) {
                    if (a4 == null || a4.scene != this.m) {
                        z2 = false;
                    } else {
                        z2 = true;
                    }
                }
                if (z2) {
                    if (a4 != null) {
                        a3.a(a4.scene);
                    }
                    if (this.y != null) {
                        this.y.exposure(a4);
                    }
                }
            }
            a3.a(a4);
            a3.a(e2);
            a3.a(this.y);
            a3.a(o());
            return ab.a(this.e, a3, view, SmartItemType.values()[itemViewType], i2, bVar, this.v);
        }
    }

    public int getItemViewType(int i2) {
        if (i2 == 0) {
            if (this.A) {
                return SmartItemType.TOPBANNER.ordinal();
            }
            if (this.c) {
                return SmartItemType.BANNER.ordinal();
            }
        }
        if (i2 == 1 && this.A && this.c) {
            return SmartItemType.BANNER.ordinal();
        }
        b bVar = this.h.get(c(i2));
        if (bVar.b == 2 && bVar.g != null) {
            String d_ = bVar.g.d_();
            Integer num = this.t.get(d_);
            if (num == null) {
                int i3 = u;
                u = i3 + 1;
                num = Integer.valueOf(i3);
                if (num.intValue() >= getViewTypeCount()) {
                    num = Integer.valueOf(getViewTypeCount() - 1);
                }
                this.t.put(d_, num);
            }
            return num.intValue();
        } else if (s() || bVar.b == 5) {
            if (bVar.b == 1) {
                SimpleAppModel c2 = bVar.c();
                if (c2 != null) {
                    SimpleAppModel.CARD_TYPE card_type = c2.U;
                    if (SimpleAppModel.CARD_TYPE.NORMAL == card_type) {
                        if (bVar.g()) {
                            return SmartItemType.NORMAL.ordinal();
                        }
                        return SmartItemType.NORMAL_NO_REASON.ordinal();
                    } else if (SimpleAppModel.CARD_TYPE.QUALITY == card_type) {
                        return SmartItemType.COMPETITIVE.ordinal();
                    }
                }
            } else if (bVar.b == 3) {
                SimpleVideoModel d2 = bVar.d();
                if (d2 != null) {
                    SimpleVideoModel.CARD_TYPE card_type2 = d2.j;
                    if (card_type2 == SimpleVideoModel.CARD_TYPE.NORMAL) {
                        return SmartItemType.VIDEO_NORMAL.ordinal();
                    }
                    if (card_type2 == SimpleVideoModel.CARD_TYPE.RICH) {
                        return SmartItemType.VIDEO_RICH.ordinal();
                    }
                }
                return SmartItemType.VIDEO_NORMAL.ordinal();
            } else if (bVar.b == 4) {
                SimpleEbookModel e2 = bVar.e();
                if (e2 != null) {
                    SimpleEbookModel.CARD_TYPE card_type3 = e2.j;
                    if (card_type3 == SimpleEbookModel.CARD_TYPE.NORMAL) {
                        return SmartItemType.EBOOK_NORMAL.ordinal();
                    }
                    if (card_type3 == SimpleEbookModel.CARD_TYPE.RICH) {
                        return SmartItemType.EBOOK_RICH.ordinal();
                    }
                }
                return SmartItemType.EBOOK_NORMAL.ordinal();
            } else if (bVar.b == 5) {
                return SmartItemType.NORMAL_LIST.ordinal();
            }
            return SmartItemType.NORMAL.ordinal();
        } else if (bVar.g()) {
            return SmartItemType.NORMAL.ordinal();
        } else {
            return SmartItemType.NORMAL_NO_REASON.ordinal();
        }
    }

    private int c(int i2) {
        if (!this.A || !this.c) {
            return (this.A || this.c) ? i2 - 1 : i2;
        }
        return i2 - 2;
    }

    public int getViewTypeCount() {
        return SmartItemType.values().length + 30;
    }

    public void handleUIEvent(Message message) {
        DownloadInfo downloadInfo;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
                if (message.obj instanceof DownloadInfo) {
                    downloadInfo = (DownloadInfo) message.obj;
                } else {
                    downloadInfo = null;
                }
                if (downloadInfo != null && !TextUtils.isEmpty(downloadInfo.downloadTicket)) {
                    for (SimpleAppModel next : q()) {
                        if (next != null) {
                            String q2 = next.q();
                            if (!TextUtils.isEmpty(q2) && q2.equals(downloadInfo.downloadTicket)) {
                                r();
                                return;
                            }
                        }
                    }
                    return;
                }
                return;
            case 1016:
                k.g(q());
                r();
                return;
            case EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY:
                r();
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
            case EventDispatcherEnum.UI_EVENT_LOGOUT:
                t();
                return;
            case EventDispatcherEnum.UI_EVENT_ENTRANCE_DOWNLOAD_PAGE:
                a(2, 0);
                return;
            case EventDispatcherEnum.UI_EVENT_ENTRANCE_UPDATE_PAGE:
                a(3, 0);
                a(4, 0);
                return;
            default:
                return;
        }
    }

    private List<SimpleAppModel> q() {
        ArrayList arrayList = new ArrayList();
        for (b next : this.h) {
            if (next != null && next.b == 1) {
                arrayList.add(next.c());
            }
        }
        return arrayList;
    }

    private void r() {
        ah.a().post(new ae(this));
    }

    public void a(boolean z2) {
        this.c = z2;
    }

    public void b(boolean z2) {
        this.E = z2;
    }

    public boolean k() {
        if (this.D != null) {
            this.D.k = com.tencent.pangu.component.topbanner.b.a().e(this.D);
            if (com.tencent.pangu.component.topbanner.b.a().d(this.D)) {
                c(true);
            } else {
                c(false);
                if (this.C != null) {
                    this.C.a();
                    this.B.c();
                }
            }
        }
        return this.A;
    }

    public void c(boolean z2) {
        this.A = z2;
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    private boolean s() {
        if ((!c.e() || c.l()) && m.a().p()) {
            return false;
        }
        return true;
    }

    private void a(int i2, int i3) {
        n f2;
        ArrayList arrayList = new ArrayList();
        for (b next : this.h) {
            if (next.b == 2 && (f2 = next.f()) != null && f2.j == i2 && f2.k == i3) {
                arrayList.add(next);
                STInfoV2 sTInfoV2 = new STInfoV2(f2.j + 202900, STConst.ST_DEFAULT_SLOT, this.m, STConst.ST_DEFAULT_SLOT, 100);
                sTInfoV2.extraData = f2.k + "||" + f2.j + "|" + 1;
                com.tencent.assistantv2.st.l.a(sTInfoV2);
            }
        }
        if (arrayList.size() > 0) {
            this.h.removeAll(arrayList);
            notifyDataSetChanged();
        }
    }

    private void t() {
        n f2;
        ArrayList arrayList = new ArrayList();
        if (j.a().k()) {
            long p2 = j.a().p();
            for (b next : this.h) {
                if (next.b == 2 && (f2 = next.f()) != null && a(f2, p2)) {
                    arrayList.add(next);
                    STInfoV2 sTInfoV2 = new STInfoV2(f2.j + 202900, STConst.ST_DEFAULT_SLOT, this.m, STConst.ST_DEFAULT_SLOT, 100);
                    sTInfoV2.extraData = f2.k + "||" + f2.j + "|" + 1;
                    com.tencent.assistantv2.st.l.a(sTInfoV2);
                }
            }
            if (arrayList.size() > 0) {
                this.h.removeAll(arrayList);
                notifyDataSetChanged();
            }
        }
    }

    private boolean a(n nVar, long j2) {
        if (!(nVar instanceof com.tencent.assistant.smartcard.d.p) || nVar.j != 7 || ((com.tencent.assistant.smartcard.d.p) nVar).f1759a == j2) {
            return false;
        }
        return true;
    }

    private String d(int i2) {
        return b(i2) + "_" + bm.a(i2) + "|" + (i2 % 10);
    }

    public STInfoV2 a(int i2, b bVar, int i3, d dVar) {
        SimpleAppModel simpleAppModel;
        if (bVar != null) {
            simpleAppModel = bVar.c();
        } else {
            simpleAppModel = null;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.e, simpleAppModel, d(i2), i3, null, dVar);
        if (!(buildSTInfo == null || this.n == -100)) {
            buildSTInfo.updateContentId(STCommonInfo.ContentIdType.CATEGORY, this.n + "_" + this.o);
        }
        return buildSTInfo;
    }

    public boolean l() {
        return true;
    }

    public boolean m() {
        return false;
    }

    public void a(int i2) {
        if (this.C != null && i2 == 8) {
            this.A = false;
            notifyDataSetChanged();
        }
    }

    public boolean n() {
        if (this.A && this.C != null) {
            return true;
        }
        return false;
    }

    public BannerType p() {
        boolean z2;
        boolean z3 = false;
        if (!(this.i instanceof SoftwareBannerView)) {
            return BannerType.None;
        }
        if (this.i.e() == null || this.i.e().size() <= 0) {
            z2 = false;
        } else {
            z2 = true;
        }
        if (this.i.f() != null && this.i.f().size() > 0 && e.a(this.i.f())) {
            z3 = true;
        }
        if (z2 && z3) {
            return BannerType.All;
        }
        if (z2) {
            return BannerType.HomePage;
        }
        if (z3) {
            return BannerType.QuickEntrance;
        }
        return BannerType.None;
    }

    public void d(boolean z2) {
        if (this.G != null) {
            this.G.a(z2);
        }
    }
}
