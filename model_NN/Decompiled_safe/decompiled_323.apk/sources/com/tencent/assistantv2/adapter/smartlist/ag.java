package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.by;
import com.tencent.cloud.model.SimpleVideoModel;
import com.tencent.pangu.model.b;

/* compiled from: ProGuard */
public class ag extends a {
    private final String c = "VideoNormalItemView";
    private IViewInvalidater d;
    private aa e;

    public ag(Context context, aa aaVar, IViewInvalidater iViewInvalidater) {
        super(context, aaVar);
        this.d = iViewInvalidater;
        this.e = aaVar;
    }

    public Pair<View, Object> a() {
        View inflate = LayoutInflater.from(this.f1900a).inflate((int) R.layout.video_normal_universal_item, (ViewGroup) null);
        aj ajVar = new aj();
        ajVar.f1906a = (TXAppIconView) inflate.findViewById(R.id.video_icon);
        ajVar.f1906a.setInvalidater(this.d);
        ajVar.b = (TextView) inflate.findViewById(R.id.video_title);
        ajVar.c = (TextView) inflate.findViewById(R.id.video_update_info);
        ajVar.d = (TextView) inflate.findViewById(R.id.video_view_count);
        ajVar.e = (TextView) inflate.findViewById(R.id.video_desc);
        return Pair.create(inflate, ajVar);
    }

    public void a(View view, Object obj, int i, b bVar) {
        SimpleVideoModel simpleVideoModel;
        aj ajVar = (aj) obj;
        if (bVar != null) {
            simpleVideoModel = bVar.d();
        } else {
            simpleVideoModel = null;
        }
        view.setOnClickListener(new ai(this, this.f1900a, i, simpleVideoModel, this.b));
        a(ajVar, simpleVideoModel, i);
    }

    private void a(aj ajVar, SimpleVideoModel simpleVideoModel, int i) {
        if (simpleVideoModel != null && ajVar != null) {
            ajVar.f1906a.updateImageView(simpleVideoModel.c, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            ajVar.b.setText(simpleVideoModel.b);
            if (!TextUtils.isEmpty(bm.f(simpleVideoModel.i))) {
                ajVar.c.setPadding(0, 0, by.a(this.f1900a, 10.0f), 0);
                ajVar.c.setText(bm.f(simpleVideoModel.i));
            } else {
                ajVar.c.setPadding(0, 0, 0, 0);
            }
            ajVar.d.setText(simpleVideoModel.h);
            ajVar.e.setText(bm.e(simpleVideoModel.f));
        }
    }
}
