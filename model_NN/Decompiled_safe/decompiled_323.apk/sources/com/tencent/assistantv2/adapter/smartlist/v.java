package com.tencent.assistantv2.adapter.smartlist;

import android.view.View;
import android.widget.TextView;
import com.tencent.assistant.adapter.a.c;
import com.tencent.assistant.component.HorizonMultiImageView;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.component.ListItemRelateNewsView;
import com.tencent.assistantv2.component.ListRecommendReasonView;

/* compiled from: ProGuard */
public class v {

    /* renamed from: a  reason: collision with root package name */
    public TextView f1920a;
    public TXAppIconView b;
    public TextView c;
    public DownloadButton d;
    public ListItemInfoView e;
    public TextView f;
    public ListRecommendReasonView g;
    public HorizonMultiImageView h;
    public View i;
    public c j;
    public TextView k;
    public TextView l;
    public ListItemRelateNewsView m;
}
