package com.tencent.assistantv2.adapter.smartlist;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BrowserActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class d extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f1909a;
    final /* synthetic */ b b;

    d(b bVar, SimpleAppModel simpleAppModel) {
        this.b = bVar;
        this.f1909a = simpleAppModel;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.b.f1900a, BrowserActivity.class);
        intent.putExtra("com.tencent.assistant.BROWSER_URL", this.f1909a.Z);
        this.b.f1900a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.b.b.e() != null) {
            this.b.b.e().updateStatus(this.f1909a);
        }
        return this.b.b.e();
    }
}
