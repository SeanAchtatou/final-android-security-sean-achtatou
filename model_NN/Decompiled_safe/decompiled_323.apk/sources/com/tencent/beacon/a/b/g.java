package com.tencent.beacon.a.b;

import android.util.SparseArray;
import com.tencent.beacon.d.c;
import java.util.Calendar;
import java.util.Map;

/* compiled from: ProGuard */
public class g {

    /* renamed from: a  reason: collision with root package name */
    private static g f2094a = null;
    private String b;
    private int c;
    private SparseArray<h> d;
    private Map<String, String> e;
    private byte f;
    private byte g;
    private String h;

    private g() {
        this.b = "http://strategy.beacon.qq.com/analytics/upload?mType=beacon";
        this.c = 360;
        this.d = null;
        this.e = null;
        this.f = 1;
        this.g = 2;
        this.h = "*^@K#K@!";
        this.d = new SparseArray<>(3);
        this.d.put(1, new h(1));
        this.d.put(2, new h(2));
        this.d.put(3, new h(3));
    }

    public static g a() {
        if (f2094a == null) {
            synchronized (g.class) {
                if (f2094a == null) {
                    f2094a = new g();
                }
            }
        }
        return f2094a;
    }

    public final String b() {
        return this.b;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final int c() {
        return this.c;
    }

    public final void a(int i) {
        this.c = i;
    }

    public final Map<String, String> d() {
        return this.e;
    }

    public final void a(Map<String, String> map) {
        this.e = map;
    }

    public final synchronized SparseArray<h> e() {
        SparseArray<h> sparseArray;
        if (this.d != null) {
            new c();
            sparseArray = c.a(this.d);
        } else {
            sparseArray = null;
        }
        return sparseArray;
    }

    public final synchronized h b(int i) {
        h hVar;
        if (this.d != null) {
            hVar = this.d.get(i);
        } else {
            hVar = null;
        }
        return hVar;
    }

    public final synchronized boolean c(int i) {
        boolean z;
        z = false;
        if (this.d != null) {
            z = this.d.get(2).b;
        }
        return z;
    }

    public final boolean f() {
        if (this.e != null) {
            String str = this.e.get("updateQimei");
            if (str != null && "n".equalsIgnoreCase(str)) {
                return false;
            }
            if (str != null && "y".equalsIgnoreCase(str)) {
                return true;
            }
        }
        return true;
    }

    public final synchronized boolean g() {
        boolean z;
        String str;
        if (this.e == null || (str = this.e.get("zeroPeak")) == null || !"y".equalsIgnoreCase(str) || Calendar.getInstance().get(11) != 0) {
            z = false;
        } else {
            z = true;
        }
        return z;
    }

    public final synchronized boolean h() {
        boolean z;
        String str;
        if (this.e == null || (str = this.e.get("qimeiZeroPeak")) == null || !"y".equalsIgnoreCase(str) || Calendar.getInstance().get(11) != 0) {
            z = false;
        } else {
            z = true;
        }
        return z;
    }

    public final synchronized byte i() {
        return this.f;
    }

    public final synchronized void a(byte b2) {
        this.f = b2;
    }

    public final synchronized byte j() {
        return this.g;
    }

    public final synchronized void b(byte b2) {
        this.g = b2;
    }

    public final synchronized String k() {
        return this.h;
    }

    public final synchronized void b(String str) {
        this.h = str;
    }
}
