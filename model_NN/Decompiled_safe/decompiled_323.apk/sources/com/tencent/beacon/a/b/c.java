package com.tencent.beacon.a.b;

import android.content.Context;
import android.util.SparseArray;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.beacon.a.d;
import com.tencent.beacon.d.a;
import com.tencent.beacon.f.i;
import com.tencent.beacon.f.j;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static c f2090a = null;
    private g b;
    private boolean c;
    private int d;
    private i e;
    private Runnable f;
    private List<b> g;
    private SparseArray<j> h;
    private List<i> i;
    private SparseArray<j> j;

    public static synchronized c a(Context context) {
        c cVar;
        synchronized (c.class) {
            if (f2090a == null && context != null) {
                f2090a = new c(context);
            }
            cVar = f2090a;
        }
        return cVar;
    }

    public static synchronized j a() {
        j jVar;
        synchronized (c.class) {
            if (f2090a != null) {
                jVar = f2090a.g();
            } else {
                jVar = null;
            }
        }
        return jVar;
    }

    private c(Context context) {
        this.b = null;
        this.c = false;
        this.d = 0;
        this.e = null;
        this.f = null;
        this.g = new ArrayList(5);
        this.h = new SparseArray<>(5);
        this.i = new ArrayList(5);
        this.j = new SparseArray<>(2);
        this.b = g.a();
        this.e = new a(context);
        this.f = new f(context);
        d.a().a(this.f);
    }

    public final synchronized g b() {
        return this.b;
    }

    private synchronized j g() {
        j jVar;
        if (this.h == null || this.h.size() <= 0) {
            jVar = null;
        } else {
            jVar = this.h.get(0);
        }
        return jVar;
    }

    public final synchronized void a(int i2, j jVar) {
        if (this.h != null) {
            if (jVar == null) {
                this.h.remove(i2);
            } else {
                this.h.put(i2, jVar);
                jVar.a(TXTabBarLayout.TABITEM_TIPS_TEXT_ID, c());
            }
        }
    }

    public final synchronized i c() {
        return this.e;
    }

    public final synchronized boolean d() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public final synchronized void a(boolean z) {
        this.c = true;
        a.f("isFirst }%b", true);
    }

    public final synchronized b[] e() {
        b[] bVarArr;
        if (this.g == null || this.g.size() <= 0) {
            bVarArr = null;
        } else {
            bVarArr = (b[]) this.g.toArray(new b[0]);
        }
        return bVarArr;
    }

    private synchronized i[] h() {
        i[] iVarArr;
        if (this.i == null || this.i.size() <= 0) {
            iVarArr = null;
        } else {
            iVarArr = (i[]) this.i.toArray(new i[0]);
        }
        return iVarArr;
    }

    public final synchronized int f() {
        return this.d;
    }

    public final synchronized void a(int i2) {
        this.d = i2;
        a.f("step:%d", Integer.valueOf(i2));
    }

    public final synchronized void a(b bVar) {
        if (bVar != null) {
            if (this.g == null) {
                this.g = new ArrayList();
            }
            if (!this.g.contains(bVar)) {
                this.g.add(bVar);
                int f2 = f();
                if (d()) {
                    a.e("add listener should notify app first run! %s", bVar.toString());
                    d.a().a(new d(this, bVar));
                }
                if (f2 >= 2) {
                    a.e("add listener should notify app start query! %s", bVar.toString());
                    d.a().a(new e(this, bVar, f2));
                }
            }
        }
    }

    public final synchronized void a(i iVar) {
        if (iVar != null) {
            if (this.i != null && !this.i.contains(iVar)) {
                this.i.add(iVar);
            }
        }
    }

    public final synchronized void a(int i2, j jVar) {
        if (jVar != null) {
            if (this.j != null) {
                this.j.put(1, jVar);
            }
        }
    }

    private synchronized SparseArray<j> i() {
        return this.j;
    }

    public final void a(g gVar) {
        i[] h2 = h();
        if (h2 != null) {
            for (i a2 : h2) {
                try {
                    a2.a(gVar);
                } catch (Throwable th) {
                    th.printStackTrace();
                    a.d("com strategy changed error %s", th.toString());
                }
            }
        }
    }

    public final void a(int i2, Map<String, String> map) {
        j jVar;
        SparseArray<j> i3 = i();
        if (i3 != null && (jVar = i3.get(i2)) != null) {
            jVar.a(map);
        }
    }
}
