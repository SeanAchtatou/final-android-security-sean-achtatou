package com.tencent.beacon.event;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.media.ThumbnailUtils;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.tencent.beacon.a.h;
import com.tencent.beacon.d.a;
import com.tencent.connect.common.Constants;
import java.io.File;
import java.io.IOException;
import java.util.List;

/* compiled from: ProGuard */
public final class q {

    /* renamed from: a  reason: collision with root package name */
    private static q f2143a = null;
    private String b = Constants.STR_EMPTY;
    private String c = Constants.STR_EMPTY;
    private String d = Constants.STR_EMPTY;
    private String e = Constants.STR_EMPTY;
    private String f = Constants.STR_EMPTY;
    private String g = Constants.STR_EMPTY;
    private String h = Constants.STR_EMPTY;
    private String i = Constants.STR_EMPTY;
    private String j = Constants.STR_EMPTY;
    private String k = Constants.STR_EMPTY;
    private String l = Constants.STR_EMPTY;
    private String m = Constants.STR_EMPTY;
    private String n = Constants.STR_EMPTY;
    private String o = Constants.STR_EMPTY;
    private String p = Constants.STR_EMPTY;
    private String q = Constants.STR_EMPTY;
    private String r = Constants.STR_EMPTY;

    public static synchronized q a(Context context) {
        q qVar;
        synchronized (q.class) {
            if (f2143a == null && context != null) {
                f2143a = new q(context);
            }
            qVar = f2143a;
        }
        return qVar;
    }

    public final synchronized String a() {
        return this.b;
    }

    public final synchronized String b() {
        return this.c;
    }

    public final synchronized String c() {
        return this.d;
    }

    public final synchronized String d() {
        return this.e;
    }

    public final synchronized String e() {
        return this.f;
    }

    public final synchronized String f() {
        return this.g;
    }

    public final synchronized String g() {
        return this.h;
    }

    public final synchronized String h() {
        return this.i;
    }

    public final synchronized String i() {
        return this.j;
    }

    public final synchronized String j() {
        return this.k;
    }

    public final String k() {
        return this.l;
    }

    public final String l() {
        return this.m;
    }

    public final String m() {
        return this.n;
    }

    public final String n() {
        return this.o;
    }

    public final String o() {
        return this.p;
    }

    public final String p() {
        return this.q;
    }

    public final String q() {
        return this.r;
    }

    private q(Context context) {
        String str;
        boolean z;
        if (context == null) {
            a.d(" UADeviceInfo context == null? pls check!", new Object[0]);
        }
        a.b(" start to create UADeviceInfo", new Object[0]);
        long currentTimeMillis = System.currentTimeMillis();
        h.a(context);
        this.b = h.a();
        h.b();
        DisplayMetrics h2 = h.h(context);
        if (h2 == null) {
            str = Constants.STR_EMPTY;
        } else {
            str = h2.widthPixels + "*" + h2.heightPixels;
        }
        this.c = str;
        this.d = h.d();
        this.e = Constants.STR_EMPTY;
        this.f = h.e() + "m";
        this.g = h.h() + "m";
        this.h = h.j();
        this.i = h.k();
        this.j = h.j(context);
        this.k = h.l();
        this.l = new StringBuilder().append(c(context)).toString();
        this.m = s() + "m";
        this.n = new StringBuilder().append(v()).toString();
        this.o = r();
        this.p = t() ? "Y" : "N";
        this.q = e(context);
        if (!d(context) || !u()) {
            z = false;
        } else {
            z = true;
        }
        this.r = z ? "Y" : "N";
        a.b(" detail create cost} %d  values:\n} %s", Long.valueOf(System.currentTimeMillis() - currentTimeMillis), toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0048, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0049, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0054 A[SYNTHETIC, Splitter:B:19:0x0054] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0064 A[SYNTHETIC, Splitter:B:28:0x0064] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0071 A[SYNTHETIC, Splitter:B:35:0x0071] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:16:0x004f=Splitter:B:16:0x004f, B:25:0x005f=Splitter:B:25:0x005f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String r() {
        /*
            java.lang.String r0 = ""
            r3 = 0
            r1 = 2
            java.lang.String[] r1 = new java.lang.String[r1]
            r2 = 0
            java.lang.String r4 = "/system/bin/cat"
            r1[r2] = r4
            r2 = 1
            java.lang.String r4 = "/proc/cpuinfo"
            r1[r2] = r4
            java.lang.Runtime r2 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x004d, Throwable -> 0x005d, all -> 0x006d }
            java.lang.Process r1 = r2.exec(r1)     // Catch:{ IOException -> 0x004d, Throwable -> 0x005d, all -> 0x006d }
            java.io.InputStream r1 = r1.getInputStream()     // Catch:{ IOException -> 0x004d, Throwable -> 0x005d, all -> 0x006d }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x004d, Throwable -> 0x005d, all -> 0x006d }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x004d, Throwable -> 0x005d, all -> 0x006d }
            r4.<init>(r1)     // Catch:{ IOException -> 0x004d, Throwable -> 0x005d, all -> 0x006d }
            r2.<init>(r4)     // Catch:{ IOException -> 0x004d, Throwable -> 0x005d, all -> 0x006d }
        L_0x0026:
            java.lang.String r1 = r2.readLine()     // Catch:{ IOException -> 0x007e, Throwable -> 0x007c }
            if (r1 == 0) goto L_0x0044
            java.lang.String r3 = "Processor"
            boolean r3 = r1.contains(r3)     // Catch:{ IOException -> 0x007e, Throwable -> 0x007c }
            if (r3 == 0) goto L_0x0026
            java.lang.String r3 = ":"
            int r3 = r1.indexOf(r3)     // Catch:{ IOException -> 0x007e, Throwable -> 0x007c }
            int r3 = r3 + 1
            java.lang.String r1 = r1.substring(r3)     // Catch:{ IOException -> 0x007e, Throwable -> 0x007c }
            java.lang.String r0 = r1.trim()     // Catch:{ IOException -> 0x007e, Throwable -> 0x007c }
        L_0x0044:
            r2.close()     // Catch:{ IOException -> 0x0048 }
        L_0x0047:
            return r0
        L_0x0048:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0047
        L_0x004d:
            r1 = move-exception
            r2 = r3
        L_0x004f:
            r1.printStackTrace()     // Catch:{ all -> 0x007a }
            if (r2 == 0) goto L_0x0047
            r2.close()     // Catch:{ IOException -> 0x0058 }
            goto L_0x0047
        L_0x0058:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0047
        L_0x005d:
            r1 = move-exception
            r2 = r3
        L_0x005f:
            r1.printStackTrace()     // Catch:{ all -> 0x007a }
            if (r2 == 0) goto L_0x0047
            r2.close()     // Catch:{ IOException -> 0x0068 }
            goto L_0x0047
        L_0x0068:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0047
        L_0x006d:
            r0 = move-exception
            r2 = r3
        L_0x006f:
            if (r2 == 0) goto L_0x0074
            r2.close()     // Catch:{ IOException -> 0x0075 }
        L_0x0074:
            throw r0
        L_0x0075:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0074
        L_0x007a:
            r0 = move-exception
            goto L_0x006f
        L_0x007c:
            r1 = move-exception
            goto L_0x005f
        L_0x007e:
            r1 = move-exception
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.event.q.r():java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0044 A[SYNTHETIC, Splitter:B:17:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0053 A[SYNTHETIC, Splitter:B:25:0x0053] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x005f A[SYNTHETIC, Splitter:B:31:0x005f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int s() {
        /*
            r0 = 0
            r3 = 0
            r1 = 2
            java.lang.String[] r1 = new java.lang.String[r1]
            java.lang.String r2 = "/system/bin/cat"
            r1[r0] = r2
            r2 = 1
            java.lang.String r4 = "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq"
            r1[r2] = r4
            java.lang.Runtime r2 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x003d, Throwable -> 0x004d }
            java.lang.Process r1 = r2.exec(r1)     // Catch:{ IOException -> 0x003d, Throwable -> 0x004d }
            java.io.InputStream r1 = r1.getInputStream()     // Catch:{ IOException -> 0x003d, Throwable -> 0x004d }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x003d, Throwable -> 0x004d }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x003d, Throwable -> 0x004d }
            r4.<init>(r1)     // Catch:{ IOException -> 0x003d, Throwable -> 0x004d }
            r2.<init>(r4)     // Catch:{ IOException -> 0x003d, Throwable -> 0x004d }
            java.lang.String r1 = r2.readLine()     // Catch:{ IOException -> 0x006e, Throwable -> 0x006b }
            if (r1 == 0) goto L_0x0034
            java.lang.String r1 = r1.trim()     // Catch:{ IOException -> 0x006e, Throwable -> 0x006b }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ IOException -> 0x006e, Throwable -> 0x006b }
            int r0 = r1 / 1000
        L_0x0034:
            r2.close()     // Catch:{ IOException -> 0x0038 }
        L_0x0037:
            return r0
        L_0x0038:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0037
        L_0x003d:
            r1 = move-exception
            r2 = r3
        L_0x003f:
            r1.printStackTrace()     // Catch:{ all -> 0x0068 }
            if (r2 == 0) goto L_0x0037
            r2.close()     // Catch:{ IOException -> 0x0048 }
            goto L_0x0037
        L_0x0048:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0037
        L_0x004d:
            r1 = move-exception
        L_0x004e:
            r1.printStackTrace()     // Catch:{ all -> 0x005c }
            if (r3 == 0) goto L_0x0037
            r3.close()     // Catch:{ IOException -> 0x0057 }
            goto L_0x0037
        L_0x0057:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0037
        L_0x005c:
            r0 = move-exception
        L_0x005d:
            if (r3 == 0) goto L_0x0062
            r3.close()     // Catch:{ IOException -> 0x0063 }
        L_0x0062:
            throw r0
        L_0x0063:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0062
        L_0x0068:
            r0 = move-exception
            r3 = r2
            goto L_0x005d
        L_0x006b:
            r1 = move-exception
            r3 = r2
            goto L_0x004e
        L_0x006e:
            r1 = move-exception
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.event.q.s():int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003d, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0048 A[SYNTHETIC, Splitter:B:19:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0057 A[SYNTHETIC, Splitter:B:27:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0063 A[SYNTHETIC, Splitter:B:33:0x0063] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean t() {
        /*
            r1 = 1
            r0 = 0
            r3 = 0
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]
            java.lang.String r4 = "/system/bin/cat"
            r2[r0] = r4
            java.lang.String r4 = "/proc/cpuinfo"
            r2[r1] = r4
            java.lang.Runtime r4 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x0041, Throwable -> 0x0051 }
            java.lang.Process r2 = r4.exec(r2)     // Catch:{ IOException -> 0x0041, Throwable -> 0x0051 }
            java.io.InputStream r4 = r2.getInputStream()     // Catch:{ IOException -> 0x0041, Throwable -> 0x0051 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0041, Throwable -> 0x0051 }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0041, Throwable -> 0x0051 }
            r5.<init>(r4)     // Catch:{ IOException -> 0x0041, Throwable -> 0x0051 }
            r2.<init>(r5)     // Catch:{ IOException -> 0x0041, Throwable -> 0x0051 }
        L_0x0024:
            java.lang.String r3 = r2.readLine()     // Catch:{ IOException -> 0x0072, Throwable -> 0x006f }
            if (r3 == 0) goto L_0x0038
            java.lang.String r3 = r3.toLowerCase()     // Catch:{ IOException -> 0x0072, Throwable -> 0x006f }
            r4 = -1
            java.lang.String r5 = "armv7"
            int r3 = r3.indexOf(r5)     // Catch:{ IOException -> 0x0072, Throwable -> 0x006f }
            if (r4 == r3) goto L_0x0024
            r0 = r1
        L_0x0038:
            r2.close()     // Catch:{ IOException -> 0x003c }
        L_0x003b:
            return r0
        L_0x003c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003b
        L_0x0041:
            r1 = move-exception
            r2 = r3
        L_0x0043:
            r1.printStackTrace()     // Catch:{ all -> 0x006c }
            if (r2 == 0) goto L_0x003b
            r2.close()     // Catch:{ IOException -> 0x004c }
            goto L_0x003b
        L_0x004c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003b
        L_0x0051:
            r1 = move-exception
        L_0x0052:
            r1.printStackTrace()     // Catch:{ all -> 0x0060 }
            if (r3 == 0) goto L_0x003b
            r3.close()     // Catch:{ IOException -> 0x005b }
            goto L_0x003b
        L_0x005b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003b
        L_0x0060:
            r0 = move-exception
        L_0x0061:
            if (r3 == 0) goto L_0x0066
            r3.close()     // Catch:{ IOException -> 0x0067 }
        L_0x0066:
            throw r0
        L_0x0067:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0066
        L_0x006c:
            r0 = move-exception
            r3 = r2
            goto L_0x0061
        L_0x006f:
            r1 = move-exception
            r3 = r2
            goto L_0x0052
        L_0x0072:
            r1 = move-exception
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.event.q.t():boolean");
    }

    private static int c(Context context) {
        int i2;
        if (context == null) {
            return 160;
        }
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            if (displayMetrics.density == 1.0f) {
                return 160;
            }
            if (((double) displayMetrics.density) <= 0.75d) {
                return 120;
            }
            if (((double) displayMetrics.density) == 1.5d) {
                return 240;
            }
            if (((double) displayMetrics.density) == 2.0d) {
                return ThumbnailUtils.TARGET_SIZE_MINI_THUMBNAIL;
            }
            if (((double) displayMetrics.density) == 3.0d) {
                i2 = 480;
            } else {
                i2 = 160;
            }
            return i2;
        } catch (Throwable th) {
            th.printStackTrace();
            return 160;
        }
    }

    public static String b(Context context) {
        List<String> allProviders;
        LocationManager locationManager = (LocationManager) context.getSystemService("location");
        return (locationManager == null || (allProviders = locationManager.getAllProviders()) == null || !allProviders.contains("gps")) ? "N" : "Y";
    }

    private static boolean u() {
        a.a("getIsRootByFile", new Object[0]);
        String[] strArr = {"/system/bin/", "/system/xbin/", "/system/sbin/", "/sbin/", "/vendor/bin/"};
        int i2 = 0;
        while (i2 < strArr.length) {
            try {
                if (new File(strArr[i2] + "su").exists()) {
                    return true;
                }
                i2++;
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return false;
    }

    private boolean d(Context context) {
        boolean z = true;
        if (context == null) {
            return false;
        }
        File file = new File("/data/data/root");
        try {
            file.createNewFile();
            if (file.exists()) {
                file.delete();
            }
        } catch (IOException e2) {
            String[] strArr = {"com.noshufou.android.su", "com.miui.uac", "eu.chainfire.supersu", "com.lbe.security.miui"};
            int length = strArr.length;
            int i2 = 0;
            while (true) {
                if (i2 < length) {
                    if (a(strArr[i2], context)) {
                        break;
                    }
                    i2++;
                } else {
                    z = false;
                    break;
                }
            }
        }
        return z;
    }

    private static boolean a(String str, Context context) {
        if (str == null || Constants.STR_EMPTY.equals(str)) {
            return false;
        }
        try {
            context.getPackageManager().getApplicationInfo(str, 8192);
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    private int v() {
        try {
            File[] listFiles = new File("/sys/devices/system/cpu/").listFiles(new r(this));
            if (listFiles == null) {
                return 1;
            }
            return listFiles.length;
        } catch (Exception e2) {
            a.d("CPU Count: Failed.", new Object[0]);
            e2.printStackTrace();
            return 1;
        }
    }

    private String e(Context context) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        String str8;
        if (context == null) {
            a.d("getSensor2 but context == null!", new Object[0]);
            return null;
        }
        a.a("getSensor2 start", new Object[0]);
        if (((WifiManager) context.getSystemService("wifi")) != null) {
            str = "Y";
        } else {
            str = "N";
        }
        if (Integer.parseInt(Build.VERSION.SDK) >= 10) {
            try {
                SensorManager sensorManager = (SensorManager) context.getSystemService("sensor");
                if (sensorManager.getDefaultSensor(1) != null) {
                    str6 = "Y";
                } else {
                    str6 = "N";
                }
                if (sensorManager.getDefaultSensor(5) != null) {
                    str7 = "Y";
                } else {
                    str7 = "N";
                }
                if (BluetoothAdapter.getDefaultAdapter() == null) {
                    str8 = "N";
                } else {
                    str8 = "Y";
                }
                if (context.getPackageManager().hasSystemFeature("android.hardware.nfc")) {
                    str2 = "Y";
                    str3 = str8;
                    str4 = str7;
                    str5 = str6;
                } else {
                    str2 = "N";
                    str3 = str8;
                    str4 = str7;
                    str5 = str6;
                }
            } catch (Throwable th) {
                str4 = "X";
                str5 = "X";
                a.d("getSensor2 error!", new Object[0]);
                str3 = "X";
                str2 = "X";
            }
        } else {
            str2 = "X";
            str3 = "X";
            str4 = "X";
            str5 = "X";
        }
        return str + str5 + str4 + str3 + str2;
    }
}
