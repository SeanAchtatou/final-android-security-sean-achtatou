package com.tencent.game.smartcard.view;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.module.k;
import com.tencent.assistant.smartcard.component.NormalSmartcardBaseItem;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.y;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.b.e;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.game.smartcard.b.b;
import com.tencent.game.smartcard.component.NormalSmartCardAppNodeWithRank;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class NormalSmartCardRankAggregationThreeItem extends NormalSmartcardBaseItem {
    private RelativeLayout i;
    private TextView l;
    private TextView m;
    private LinearLayout n;
    private List<NormalSmartCardAppNodeWithRank> o;
    private View.OnClickListener p;

    public NormalSmartCardRankAggregationThreeItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = inflate(this.f1693a, R.layout.smartcard_rank_aggregation_santiao_layout, this);
        this.l = (TextView) this.c.findViewById(R.id.card_title);
        this.m = (TextView) this.c.findViewById(R.id.more_txt);
        this.i = (RelativeLayout) this.c.findViewById(R.id.card_title_layout);
        this.n = (LinearLayout) this.c.findViewById(R.id.card_app_list_layout);
        f();
    }

    private void f() {
        int i2 = 0;
        b bVar = (b) this.d;
        if (!TextUtils.isEmpty(bVar.l)) {
            this.l.setText(bVar.l);
            this.l.setCompoundDrawables(null, null, null, null);
            this.l.setPadding(by.a(this.f1693a, 7.0f), 0, 0, 0);
            this.i.setVisibility(0);
            if (!TextUtils.isEmpty(bVar.p)) {
                this.m.setText(bVar.p);
                if (this.p == null) {
                    this.p = new b(this);
                }
                this.m.setOnClickListener(this.p);
                this.m.setVisibility(0);
            } else {
                this.m.setVisibility(8);
            }
        } else {
            this.i.setVisibility(8);
        }
        List<y> list = bVar.e;
        if (list == null || list.size() == 0) {
            this.n.setVisibility(8);
            return;
        }
        this.n.setVisibility(0);
        if (this.o == null) {
            this.n.setOrientation(1);
            g();
            int size = list.size();
            this.o = new ArrayList(size);
            while (i2 < size) {
                NormalSmartCardAppNodeWithRank normalSmartCardAppNodeWithRank = new NormalSmartCardAppNodeWithRank(this.f1693a);
                this.o.add(normalSmartCardAppNodeWithRank);
                normalSmartCardAppNodeWithRank.setMinimumHeight(by.a(getContext(), 67.0f));
                this.n.addView(normalSmartCardAppNodeWithRank);
                normalSmartCardAppNodeWithRank.a(list.get(i2).f1768a, a(list.get(i2), i2), a(this.f1693a), ListItemInfoView.InfoType.DOWNTIMES_SIZE, i2, size);
                i2++;
            }
            return;
        }
        int size2 = this.o.size() >= list.size() ? list.size() : this.o.size();
        while (i2 < size2) {
            this.o.get(i2).a(list.get(i2).f1768a, a(list.get(i2), i2), a(this.f1693a), ListItemInfoView.InfoType.DOWNTIMES_SIZE, i2, size2);
            i2++;
        }
    }

    private void g() {
        this.o = null;
        this.n.removeAllViews();
    }

    private STInfoV2 a(y yVar, int i2) {
        if (yVar == null) {
            return null;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1693a, yVar.f1768a, a.a(((b) this.d).f + Constants.STR_EMPTY, i2), 100, a.a(k.d(yVar.f1768a), yVar.f1768a));
        if (buildSTInfo != null) {
            buildSTInfo.updateWithSimpleAppModel(yVar.f1768a);
        }
        if (this.h == null) {
            this.h = new e();
        }
        this.h.a(buildSTInfo);
        return buildSTInfo;
    }

    public void b() {
        f();
    }
}
