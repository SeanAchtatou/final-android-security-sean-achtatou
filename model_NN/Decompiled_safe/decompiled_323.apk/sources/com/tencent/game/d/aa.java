package com.tencent.game.d;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.model.b;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.ag;
import com.tencent.assistant.module.ak;
import com.tencent.assistant.protocol.jce.CardItemWrapper;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.protocol.jce.GetSmartCardsRequest;
import com.tencent.assistant.protocol.jce.GetSmartCardsResponse;
import com.tencent.assistant.protocol.jce.GftGetGameGiftFlagRequest;
import com.tencent.assistant.protocol.jce.GftGetGameGiftFlagResponse;
import com.tencent.assistant.protocol.jce.GftGetRecommendTabPageRequest;
import com.tencent.assistant.protocol.jce.GftGetRecommendTabPageResponse;
import com.tencent.assistant.protocol.jce.GftGetTreasureBoxSettingRequest;
import com.tencent.assistant.protocol.jce.GftGetTreasureBoxSettingResponse;
import com.tencent.assistant.protocol.jce.SmartCardWrapper;
import com.tencent.assistant.protocol.scu.RequestResponePair;
import com.tencent.assistant.smartcard.b.a;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ah;
import com.tencent.connect.common.Constants;
import com.tencent.game.d.a.e;
import com.tencent.nucleus.socialcontact.login.j;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class aa extends BaseEngine<e> implements ak {

    /* renamed from: a  reason: collision with root package name */
    private static aa f2677a = null;
    private long b = -1;
    /* access modifiers changed from: private */
    public List<CardItemWrapper> c;
    private int d = -1;
    private int e = -1;
    private int f = 0;
    private List<CardItemWrapper> g = new ArrayList();
    /* access modifiers changed from: private */
    public ArrayList<ColorCardItem> h = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean i = true;
    private ak j;
    private b k = new b();
    /* access modifiers changed from: private */
    public a l = new a();
    private aj m;
    /* access modifiers changed from: private */
    public aj n;

    public static synchronized aa a() {
        aa aaVar;
        synchronized (aa.class) {
            if (f2677a == null) {
                f2677a = new aa();
            }
            aaVar = f2677a;
        }
        return aaVar;
    }

    private aa() {
        ag.b().a(this);
        this.j = new ak(this);
        this.m = new aj(this);
        this.n = new aj(this);
    }

    public b b() {
        this.k.b(this.b);
        this.k.a(this.h);
        return this.k;
    }

    public GftGetRecommendTabPageResponse c() {
        return i.y().c((byte[]) null);
    }

    public void d() {
        TemporaryThreadManager.get().start(new ab(this));
    }

    /* access modifiers changed from: private */
    public boolean g() {
        boolean z;
        GftGetRecommendTabPageResponse c2 = c();
        if (c2 == null) {
            return false;
        }
        long a2 = m.a().a((byte) 5);
        if (c2.e != a2) {
            return false;
        }
        if (a2 != -1 && c2.e != a2) {
            return false;
        }
        ArrayList<ColorCardItem> a3 = c2.a();
        this.c = c2.g;
        if (this.c == null || this.c.size() == 0 || a3 == null || a3.size() == 0) {
            return false;
        }
        boolean z2 = 1 == c2.f;
        byte[] bArr = c2.d;
        this.m.f2686a = bArr;
        aj ajVar = this.m;
        if (1 == c2.f) {
            z = true;
        } else {
            z = false;
        }
        ajVar.c = z;
        this.m.b = 0;
        this.n.a(this.m);
        if (this.h == null) {
            this.h = new ArrayList<>();
        }
        this.h.clear();
        this.h.addAll(a3);
        if (this.g == null) {
            this.g = new ArrayList();
        }
        this.g.clear();
        this.g.addAll(this.c);
        this.b = c2.c();
        this.k.b(this.b);
        notifyDataChangedInMainThread(new ac(this, z2, bArr, a3));
        return true;
    }

    public void onLocalDataHasUpdate() {
        if (this.b != m.a().a((byte) 5) && this.b != -1) {
            e();
        }
    }

    public void onPromptHasNewNotify(ArrayList<QuickEntranceNotify> arrayList) {
    }

    public int e() {
        if (this.d > 0) {
            cancel(this.d);
        }
        this.m.b();
        this.d = b(this.m);
        return this.d;
    }

    private int a(aj ajVar) {
        if (this.e > 0) {
            cancel(this.e);
        }
        this.e = b(ajVar);
        return this.e;
    }

    public int f() {
        if (this.n == null || this.n.f2686a == null || this.n.f2686a.length == 0) {
            return -1;
        }
        if (this.j.c()) {
            int a2 = this.j.a();
            this.j.b();
            return a2;
        } else if (!this.n.equals(this.j.d()) || this.j.e() == null) {
            return a(this.n);
        } else {
            boolean z = this.b != this.j.h();
            this.b = this.j.h();
            this.k.b(this.b);
            int a3 = this.j.a();
            ArrayList arrayList = new ArrayList(this.j.e());
            this.g.addAll(arrayList);
            this.i = this.j.g();
            this.n.a(this.j.d());
            this.m.a(this.n);
            this.l.b(this.j.f());
            notifyDataChangedInMainThread(new ad(this, a3, z, arrayList));
            if (this.j.g()) {
                this.j.a(this.n);
            }
            return this.j.a();
        }
    }

    private int b(aj ajVar) {
        return a(-1, ajVar);
    }

    /* access modifiers changed from: private */
    public int a(int i2, aj ajVar) {
        this.f = 0;
        GftGetRecommendTabPageRequest gftGetRecommendTabPageRequest = new GftGetRecommendTabPageRequest();
        String n2 = j.a().n();
        gftGetRecommendTabPageRequest.c = -2;
        gftGetRecommendTabPageRequest.f1371a = 30;
        gftGetRecommendTabPageRequest.d = n2;
        if (ajVar == null) {
            ajVar = new aj(this);
        }
        gftGetRecommendTabPageRequest.b = ajVar.f2686a;
        GetSmartCardsRequest getSmartCardsRequest = new GetSmartCardsRequest();
        getSmartCardsRequest.f1334a = 4;
        getSmartCardsRequest.e = 60;
        getSmartCardsRequest.d = ajVar.b;
        GftGetTreasureBoxSettingRequest gftGetTreasureBoxSettingRequest = new GftGetTreasureBoxSettingRequest();
        GftGetGameGiftFlagRequest gftGetGameGiftFlagRequest = new GftGetGameGiftFlagRequest();
        ArrayList arrayList = new ArrayList();
        arrayList.add(gftGetRecommendTabPageRequest);
        arrayList.add(getSmartCardsRequest);
        arrayList.add(gftGetTreasureBoxSettingRequest);
        arrayList.add(gftGetGameGiftFlagRequest);
        return send(i2, arrayList);
    }

    public int a(JceStruct jceStruct) {
        return super.send(jceStruct);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, List<RequestResponePair> list) {
        ArrayList<SmartCardWrapper> arrayList;
        boolean z;
        int i3;
        int i4;
        GetSmartCardsResponse getSmartCardsResponse;
        GftGetTreasureBoxSettingResponse gftGetTreasureBoxSettingResponse;
        GftGetGameGiftFlagResponse gftGetGameGiftFlagResponse;
        GetSmartCardsResponse getSmartCardsResponse2;
        GftGetRecommendTabPageResponse gftGetRecommendTabPageResponse;
        GftGetGameGiftFlagResponse gftGetGameGiftFlagResponse2 = null;
        GftGetTreasureBoxSettingResponse gftGetTreasureBoxSettingResponse2 = null;
        GftGetRecommendTabPageResponse gftGetRecommendTabPageResponse2 = null;
        GftGetRecommendTabPageRequest gftGetRecommendTabPageRequest = null;
        int i5 = -9999;
        GetSmartCardsResponse getSmartCardsResponse3 = null;
        int i6 = -9999;
        for (RequestResponePair next : list) {
            if (next.request instanceof GftGetRecommendTabPageRequest) {
                GftGetRecommendTabPageRequest gftGetRecommendTabPageRequest2 = (GftGetRecommendTabPageRequest) next.request;
                if (next.response instanceof GftGetRecommendTabPageResponse) {
                    gftGetRecommendTabPageResponse = (GftGetRecommendTabPageResponse) next.response;
                } else {
                    gftGetRecommendTabPageResponse = gftGetRecommendTabPageResponse2;
                }
                i3 = next.errorCode;
                gftGetRecommendTabPageResponse2 = gftGetRecommendTabPageResponse;
                gftGetRecommendTabPageRequest = gftGetRecommendTabPageRequest2;
            } else {
                i3 = i6;
            }
            if (next.request instanceof GetSmartCardsRequest) {
                if (next.response instanceof GetSmartCardsResponse) {
                    getSmartCardsResponse2 = (GetSmartCardsResponse) next.response;
                } else {
                    getSmartCardsResponse2 = getSmartCardsResponse3;
                }
                i4 = next.errorCode;
                getSmartCardsResponse = getSmartCardsResponse2;
            } else {
                i4 = i5;
                getSmartCardsResponse = getSmartCardsResponse3;
            }
            if (!(next.request instanceof GftGetTreasureBoxSettingRequest) || !(next.response instanceof GftGetTreasureBoxSettingResponse)) {
                gftGetTreasureBoxSettingResponse = gftGetTreasureBoxSettingResponse2;
            } else {
                gftGetTreasureBoxSettingResponse = (GftGetTreasureBoxSettingResponse) next.response;
            }
            if (!(next.request instanceof GftGetGameGiftFlagRequest) || !(next.response instanceof GftGetGameGiftFlagResponse)) {
                gftGetGameGiftFlagResponse = gftGetGameGiftFlagResponse2;
            } else {
                gftGetGameGiftFlagResponse = (GftGetGameGiftFlagResponse) next.response;
            }
            getSmartCardsResponse3 = getSmartCardsResponse;
            gftGetGameGiftFlagResponse2 = gftGetGameGiftFlagResponse;
            gftGetTreasureBoxSettingResponse2 = gftGetTreasureBoxSettingResponse;
            i5 = i4;
            i6 = i3;
        }
        if (gftGetRecommendTabPageRequest == null || gftGetRecommendTabPageResponse2 == null || i6 != 0) {
            onRequestFailed(i2, i6, list);
            return;
        }
        if (gftGetGameGiftFlagResponse2 == null) {
            gftGetGameGiftFlagResponse2 = new GftGetGameGiftFlagResponse();
        }
        i.y().a(gftGetGameGiftFlagResponse2);
        if (gftGetTreasureBoxSettingResponse2 != null) {
            ah.a(Constants.STR_EMPTY).post(new ae(this, gftGetTreasureBoxSettingResponse2));
        }
        if (gftGetRecommendTabPageResponse2 == null || gftGetRecommendTabPageResponse2.d() == null || gftGetRecommendTabPageResponse2.d().size() <= 0) {
            onRequestFailed(i2, -841, list);
            return;
        }
        ArrayList<CardItemWrapper> d2 = gftGetRecommendTabPageResponse2.d();
        ArrayList<CardItemWrapper> d3 = gftGetRecommendTabPageResponse2.d();
        new CardItemWrapper().c = d3.get(0).a();
        if (i5 != 0 || getSmartCardsResponse3 == null) {
            arrayList = null;
        } else {
            arrayList = getSmartCardsResponse3.a();
        }
        if (i2 == this.j.a()) {
            this.n.f2686a = gftGetRecommendTabPageResponse2.b();
            this.n.c = gftGetRecommendTabPageResponse2.f == 1;
            this.n.a();
            this.j.a(gftGetRecommendTabPageResponse2.e, d2, this.n, arrayList);
        } else {
            this.l.b(arrayList);
            this.m.a();
            boolean z2 = gftGetRecommendTabPageResponse2.e != this.b || gftGetRecommendTabPageRequest.b == null || gftGetRecommendTabPageRequest.b.length == 0;
            if (gftGetRecommendTabPageResponse2.f == 1) {
                z = true;
            } else {
                z = false;
            }
            byte[] b2 = gftGetRecommendTabPageResponse2.b();
            this.b = gftGetRecommendTabPageResponse2.e;
            this.k.b(this.b);
            if (z2) {
                if (this.h == null) {
                    this.h = new ArrayList<>();
                }
                this.h.clear();
                this.h.addAll(gftGetRecommendTabPageResponse2.a());
                this.g.clear();
                this.m.b = 60;
            }
            if (!(d2 == null || d2.size() == 0)) {
                this.g.addAll(d2);
            }
            this.m.f2686a = b2;
            this.i = z;
            this.n.a(this.m);
            if (z) {
                this.j.a(this.n);
            }
            notifyDataChangedInMainThread(new af(this, i2, z, b2, z2, d3));
        }
        i.y().a(gftGetRecommendTabPageRequest.b, gftGetRecommendTabPageResponse2);
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, List<RequestResponePair> list) {
        GftGetRecommendTabPageRequest gftGetRecommendTabPageRequest;
        Iterator<RequestResponePair> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                gftGetRecommendTabPageRequest = null;
                break;
            }
            RequestResponePair next = it.next();
            if (next.request instanceof GftGetRecommendTabPageRequest) {
                gftGetRecommendTabPageRequest = (GftGetRecommendTabPageRequest) next.request;
                break;
            }
        }
        if (i2 != this.j.a()) {
            if (gftGetRecommendTabPageRequest == null || gftGetRecommendTabPageRequest.b == null || gftGetRecommendTabPageRequest.b.length == 0) {
                TemporaryThreadManager.get().start(new ag(this, i2, i3));
            } else {
                notifyDataChangedInMainThread(new ai(this, i2, i3));
            }
        } else {
            this.j.i();
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
    }

    public String toString() {
        return getClass().getSimpleName().toString();
    }
}
