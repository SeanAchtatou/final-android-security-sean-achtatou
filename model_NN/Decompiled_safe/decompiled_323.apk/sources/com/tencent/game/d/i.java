package com.tencent.game.d;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.GftGetAppListResponse;
import java.util.ArrayList;

/* compiled from: ProGuard */
class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f2701a;

    i(h hVar) {
        this.f2701a = hVar;
    }

    public void run() {
        boolean z = true;
        GftGetAppListResponse c = com.tencent.assistant.manager.i.y().c(this.f2701a.f2700a.f2676a, this.f2701a.f2700a.b, this.f2701a.c);
        if (c == null || c.e != this.f2701a.f2700a.f || c.b == null || c.b.size() <= 0) {
            int unused = this.f2701a.h = this.f2701a.f2700a.a(this.f2701a.h, this.f2701a.c);
            return;
        }
        h hVar = this.f2701a;
        long j = c.e;
        ArrayList<SimpleAppModel> b = k.b(c.b);
        if (c.d != 1) {
            z = false;
        }
        hVar.a(j, b, z, c.c);
    }
}
