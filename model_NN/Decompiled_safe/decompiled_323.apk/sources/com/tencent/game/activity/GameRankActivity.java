package com.tencent.game.activity;

import android.os.Bundle;
import android.text.TextUtils;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ah;
import com.tencent.assistantv2.activity.a;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.component.TXViewPager;
import com.tencent.assistantv2.component.TabBarView;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.game.d.v;
import com.tencent.game.d.y;
import com.tencent.game.d.z;
import com.tencent.pangu.component.search.c;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class GameRankActivity extends BaseActivity {
    private c A = new j(this);
    private SecondNavigationTitleViewV5 n;
    /* access modifiers changed from: private */
    public TXViewPager u;
    /* access modifiers changed from: private */
    public k v;
    /* access modifiers changed from: private */
    public int w = 0;
    /* access modifiers changed from: private */
    public TabBarView x;
    private y y;
    private List<z> z;

    public int f() {
        if (this.v == null || this.v.a(this.w) == null) {
            return STConst.ST_PAGE_GAME_RANKING;
        }
        return ((a) this.v.a(this.w)).G();
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return false;
    }

    public void onCreate(Bundle bundle) {
        z zVar = null;
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_game_band);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            long j = extras.getLong("categoryid", -999);
            int i = extras.getInt("sortid", -999);
            String string = extras.getString("title");
            if (!(j == -999 || i == -999 || TextUtils.isEmpty(string))) {
                zVar = new z(string, GameRankTabType.NEWGAME.ordinal(), 0, null, j, i, 20, (byte) 0);
            }
        }
        this.n = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.n.d(false);
        this.n.a(this);
        this.n.i();
        this.y = v.a().b();
        if (zVar == null) {
            this.n.b(getString(R.string.gamerank_title));
            this.z = this.y.b;
            t();
        } else {
            this.z = new ArrayList();
            this.z.add(zVar);
            if (this.z == null || this.z.size() <= 0) {
                finish();
            }
            this.n.b(this.z.get(0).f2717a);
        }
        c(this.w);
        q();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.n != null) {
            this.n.l();
        }
        if (this.w < this.v.getCount()) {
            ah.a().post(new i(this, (a) this.v.a(this.w)));
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.n.m();
    }

    private void t() {
        String[] strArr = new String[this.y.b.size()];
        for (int i = 0; i < this.y.b.size(); i++) {
            strArr[i] = this.z.get(i).f2717a;
        }
        this.x = (TabBarView) findViewById(R.id.tab_view);
        this.x.setVisibility(0);
        this.x.a(strArr);
        this.x.a(this.w);
        this.x.a(this.A);
    }

    /* access modifiers changed from: private */
    public void b(int i) {
        STInfoV2 p = p();
        if (p != null) {
            p.actionId = 200;
            p.slotId = com.tencent.assistantv2.st.page.a.a("03", i);
        }
        l.a(p);
    }

    private void c(int i) {
        this.u = (TXViewPager) findViewById(R.id.vPager);
        if (this.v == null) {
            this.v = new k(this, e(), this, this.z);
        }
        this.u.setAdapter(this.v);
        this.u.setCurrentItem(i);
        d(i);
        this.u.setOnPageChangeListener(new l(this));
    }

    /* access modifiers changed from: private */
    public void d(int i) {
        if (this.v.a(i) != null) {
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
    }
}
