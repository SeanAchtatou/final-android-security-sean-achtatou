package com.tencent.game.component;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
class l extends ViewInvalidateMessageHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameRankAggregationListView f2666a;

    l(GameRankAggregationListView gameRankAggregationListView) {
        this.f2666a = gameRankAggregationListView;
    }

    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        int i;
        if (viewInvalidateMessage.what == 1) {
            int i2 = viewInvalidateMessage.arg2;
            int i3 = viewInvalidateMessage.arg1;
            List list = null;
            if (viewInvalidateMessage.params != null) {
                Map map = (Map) viewInvalidateMessage.params;
                if (map.containsKey(0)) {
                    list = (List) map.get(0);
                }
                if (map.containsKey(1)) {
                    i = ((Integer) map.get(1)).intValue();
                    this.f2666a.a(i2, i3, list, i);
                    return;
                }
            }
            i = 0;
            this.f2666a.a(i2, i3, list, i);
            return;
        }
        this.f2666a.v.u();
        if (this.f2666a.x != null) {
            this.f2666a.x.notifyDataSetChanged();
        }
    }
}
