package com.tencent.game.component;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.protocol.jce.SimpleAppInfo;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class u extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppInfo f2673a;
    final /* synthetic */ as b;
    final /* synthetic */ STInfoV2 c;
    final /* synthetic */ GameSquareAppItem d;

    u(GameSquareAppItem gameSquareAppItem, SimpleAppInfo simpleAppInfo, as asVar, STInfoV2 sTInfoV2) {
        this.d = gameSquareAppItem;
        this.f2673a = simpleAppInfo;
        this.b = asVar;
        this.c = sTInfoV2;
    }

    public void onTMAClick(View view) {
        PackageManager packageManager = this.d.b.getPackageManager();
        Intent intent = new Intent();
        try {
            intent = packageManager.getLaunchIntentForPackage(this.f2673a.f);
        } catch (Exception e) {
            XLog.e("GameSquareAppItem", " Launch " + this.f2673a.f + " error!");
        }
        this.d.b.startActivity(intent);
        if (this.b != null) {
            this.b.b(0, 0);
        }
    }

    public STInfoV2 getStInfo() {
        if (this.c != null) {
            this.c.actionId = 200;
        }
        return this.c;
    }
}
