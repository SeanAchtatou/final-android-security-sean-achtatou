package com.google.zxing;

public final class FormatException extends ReaderException {

    /* renamed from: a  reason: collision with root package name */
    private static final FormatException f112a = new FormatException();

    private FormatException() {
    }

    public static FormatException a() {
        return f112a;
    }
}
