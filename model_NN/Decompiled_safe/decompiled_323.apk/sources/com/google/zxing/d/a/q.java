package com.google.zxing.d.a;

public final class q {

    /* renamed from: a  reason: collision with root package name */
    public static final q f181a = new q(new int[]{0, 0, 0}, 0, "TERMINATOR");
    public static final q b = new q(new int[]{10, 12, 14}, 1, "NUMERIC");
    public static final q c = new q(new int[]{9, 11, 13}, 2, "ALPHANUMERIC");
    public static final q d = new q(new int[]{0, 0, 0}, 3, "STRUCTURED_APPEND");
    public static final q e = new q(new int[]{8, 16, 16}, 4, "BYTE");
    public static final q f = new q(null, 7, "ECI");
    public static final q g = new q(new int[]{8, 10, 12}, 8, "KANJI");
    public static final q h = new q(null, 5, "FNC1_FIRST_POSITION");
    public static final q i = new q(null, 9, "FNC1_SECOND_POSITION");
    private final int[] j;
    private final int k;
    private final String l;

    private q(int[] iArr, int i2, String str) {
        this.j = iArr;
        this.k = i2;
        this.l = str;
    }

    public static q a(int i2) {
        switch (i2) {
            case 0:
                return f181a;
            case 1:
                return b;
            case 2:
                return c;
            case 3:
                return d;
            case 4:
                return e;
            case 5:
                return h;
            case 6:
            default:
                throw new IllegalArgumentException();
            case 7:
                return f;
            case 8:
                return g;
            case 9:
                return i;
        }
    }

    public int a(r rVar) {
        if (this.j == null) {
            throw new IllegalArgumentException("Character count doesn't apply to this mode");
        }
        int a2 = rVar.a();
        return this.j[a2 <= 9 ? 0 : a2 <= 26 ? (char) 1 : 2];
    }

    public String toString() {
        return this.l;
    }
}
