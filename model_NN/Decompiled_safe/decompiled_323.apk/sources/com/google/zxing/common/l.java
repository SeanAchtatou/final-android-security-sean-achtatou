package com.google.zxing.common;

import com.google.zxing.NotFoundException;

public abstract class l {

    /* renamed from: a  reason: collision with root package name */
    private static l f165a = new h();

    public static l a() {
        return f165a;
    }

    protected static void a(b bVar, float[] fArr) {
        int b = bVar.b();
        int c = bVar.c();
        boolean z = true;
        for (int i = 0; i < fArr.length && z; i += 2) {
            int i2 = (int) fArr[i];
            int i3 = (int) fArr[i + 1];
            if (i2 < -1 || i2 > b || i3 < -1 || i3 > c) {
                throw NotFoundException.a();
            }
            if (i2 == -1) {
                fArr[i] = 0.0f;
                z = true;
            } else if (i2 == b) {
                fArr[i] = (float) (b - 1);
                z = true;
            } else {
                z = false;
            }
            if (i3 == -1) {
                fArr[i + 1] = 0.0f;
                z = true;
            } else if (i3 == c) {
                fArr[i + 1] = (float) (c - 1);
                z = true;
            }
        }
        boolean z2 = true;
        for (int length = fArr.length - 2; length >= 0 && z2; length -= 2) {
            int i4 = (int) fArr[length];
            int i5 = (int) fArr[length + 1];
            if (i4 < -1 || i4 > b || i5 < -1 || i5 > c) {
                throw NotFoundException.a();
            }
            if (i4 == -1) {
                fArr[length] = 0.0f;
                z2 = true;
            } else if (i4 == b) {
                fArr[length] = (float) (b - 1);
                z2 = true;
            } else {
                z2 = false;
            }
            if (i5 == -1) {
                fArr[length + 1] = 0.0f;
                z2 = true;
            } else if (i5 == c) {
                fArr[length + 1] = (float) (c - 1);
                z2 = true;
            }
        }
    }

    public abstract b a(b bVar, int i, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16);

    public b a(b bVar, int i, n nVar) {
        throw new IllegalStateException();
    }
}
