package com.google.zxing.common;

import com.google.zxing.NotFoundException;

public final class h extends l {
    public b a(b bVar, int i, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16) {
        return a(bVar, i, n.a(f, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15, f16));
    }

    public b a(b bVar, int i, n nVar) {
        b bVar2 = new b(i);
        float[] fArr = new float[(i << 1)];
        for (int i2 = 0; i2 < i; i2++) {
            int length = fArr.length;
            float f = ((float) i2) + 0.5f;
            for (int i3 = 0; i3 < length; i3 += 2) {
                fArr[i3] = ((float) (i3 >> 1)) + 0.5f;
                fArr[i3 + 1] = f;
            }
            nVar.a(fArr);
            a(bVar, fArr);
            int i4 = 0;
            while (i4 < length) {
                try {
                    if (bVar.a((int) fArr[i4], (int) fArr[i4 + 1])) {
                        bVar2.b(i4 >> 1, i2);
                    }
                    i4 += 2;
                } catch (ArrayIndexOutOfBoundsException e) {
                    throw NotFoundException.a();
                }
            }
        }
        return bVar2;
    }
}
