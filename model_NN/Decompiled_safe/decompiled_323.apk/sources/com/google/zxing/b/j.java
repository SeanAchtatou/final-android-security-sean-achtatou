package com.google.zxing.b;

import com.google.zxing.NotFoundException;
import com.google.zxing.ReaderException;
import com.google.zxing.a;
import com.google.zxing.d;
import com.google.zxing.g;
import com.google.zxing.h;
import java.util.Hashtable;
import java.util.Vector;

public final class j extends k {

    /* renamed from: a  reason: collision with root package name */
    private final Vector f146a;

    public j(Hashtable hashtable) {
        Vector vector = hashtable == null ? null : (Vector) hashtable.get(d.c);
        this.f146a = new Vector();
        if (vector != null) {
            if (vector.contains(a.f)) {
                this.f146a.addElement(new e());
            } else if (vector.contains(a.d)) {
                this.f146a.addElement(new l());
            }
            if (vector.contains(a.e)) {
                this.f146a.addElement(new f());
            }
            if (vector.contains(a.c)) {
                this.f146a.addElement(new o());
            }
        }
        if (this.f146a.isEmpty()) {
            this.f146a.addElement(new e());
            this.f146a.addElement(new f());
            this.f146a.addElement(new o());
        }
    }

    public h a(int i, com.google.zxing.common.a aVar, Hashtable hashtable) {
        int[] a2 = n.a(aVar);
        int size = this.f146a.size();
        int i2 = 0;
        while (i2 < size) {
            try {
                h a3 = ((n) this.f146a.elementAt(i2)).a(i, aVar, a2, hashtable);
                boolean z = a.f.equals(a3.c()) && a3.a().charAt(0) == '0';
                Vector vector = hashtable == null ? null : (Vector) hashtable.get(d.c);
                return (!z || !(vector == null || vector.contains(a.d))) ? a3 : new h(a3.a().substring(1), null, a3.b(), a.d);
            } catch (ReaderException e) {
                i2++;
            }
        }
        throw NotFoundException.a();
    }

    public void a() {
        int size = this.f146a.size();
        for (int i = 0; i < size; i++) {
            ((g) this.f146a.elementAt(i)).a();
        }
    }
}
