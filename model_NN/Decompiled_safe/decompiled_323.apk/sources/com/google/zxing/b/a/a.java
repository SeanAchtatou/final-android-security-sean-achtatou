package com.google.zxing.b.a;

import com.google.zxing.NotFoundException;
import com.google.zxing.b.k;

public abstract class a extends k {

    /* renamed from: a  reason: collision with root package name */
    protected final int[] f127a = new int[4];
    protected final int[] b = new int[8];
    protected final float[] c = new float[4];
    protected final float[] d = new float[4];
    protected final int[] e = new int[(this.b.length / 2)];
    protected final int[] f = new int[(this.b.length / 2)];

    protected a() {
    }

    protected static int a(int[] iArr) {
        int i = 0;
        for (int i2 : iArr) {
            i += i2;
        }
        return i;
    }

    protected static int a(int[] iArr, int[][] iArr2) {
        for (int i = 0; i < iArr2.length; i++) {
            if (a(iArr, iArr2[i], 102) < 51) {
                return i;
            }
        }
        throw NotFoundException.a();
    }

    protected static void a(int[] iArr, float[] fArr) {
        int i = 0;
        float f2 = fArr[0];
        for (int i2 = 1; i2 < iArr.length; i2++) {
            if (fArr[i2] > f2) {
                f2 = fArr[i2];
                i = i2;
            }
        }
        iArr[i] = iArr[i] + 1;
    }

    protected static void b(int[] iArr, float[] fArr) {
        int i = 0;
        float f2 = fArr[0];
        for (int i2 = 1; i2 < iArr.length; i2++) {
            if (fArr[i2] < f2) {
                f2 = fArr[i2];
                i = i2;
            }
        }
        iArr[i] = iArr[i] - 1;
    }

    protected static boolean b(int[] iArr) {
        int i = iArr[0] + iArr[1];
        float f2 = ((float) i) / ((float) ((iArr[2] + i) + iArr[3]));
        if (f2 < 0.7916667f || f2 > 0.89285713f) {
            return false;
        }
        int i2 = Integer.MAX_VALUE;
        int i3 = Integer.MIN_VALUE;
        for (int i4 : iArr) {
            if (i4 > i3) {
                i3 = i4;
            }
            if (i4 < i2) {
                i2 = i4;
            }
        }
        return i3 < i2 * 10;
    }
}
