package com.google.zxing.b.a.a;

import com.google.zxing.b.a.c;

final class b {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f134a;
    private final com.google.zxing.b.a.b b;
    private final com.google.zxing.b.a.b c;
    private final c d;

    b(com.google.zxing.b.a.b bVar, com.google.zxing.b.a.b bVar2, c cVar, boolean z) {
        this.b = bVar;
        this.c = bVar2;
        this.d = cVar;
        this.f134a = z;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.f134a;
    }

    /* access modifiers changed from: package-private */
    public com.google.zxing.b.a.b b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public com.google.zxing.b.a.b c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public c d() {
        return this.d;
    }

    public boolean e() {
        return this.c == null;
    }
}
