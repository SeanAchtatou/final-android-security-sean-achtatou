package com.google.zxing.b.a.a.a;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;

abstract class f extends i {
    f(a aVar) {
        super(aVar);
    }

    public String a() {
        if (this.f128a.b != 60) {
            throw NotFoundException.a();
        }
        StringBuffer stringBuffer = new StringBuffer();
        b(stringBuffer, 5);
        b(stringBuffer, 45, 15);
        return stringBuffer.toString();
    }
}
