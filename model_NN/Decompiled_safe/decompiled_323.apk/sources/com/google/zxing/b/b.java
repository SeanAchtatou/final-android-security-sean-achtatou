package com.google.zxing.b;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;

public final class b extends k {

    /* renamed from: a  reason: collision with root package name */
    static final int[][] f138a = {new int[]{2, 1, 2, 2, 2, 2}, new int[]{2, 2, 2, 1, 2, 2}, new int[]{2, 2, 2, 2, 2, 1}, new int[]{1, 2, 1, 2, 2, 3}, new int[]{1, 2, 1, 3, 2, 2}, new int[]{1, 3, 1, 2, 2, 2}, new int[]{1, 2, 2, 2, 1, 3}, new int[]{1, 2, 2, 3, 1, 2}, new int[]{1, 3, 2, 2, 1, 2}, new int[]{2, 2, 1, 2, 1, 3}, new int[]{2, 2, 1, 3, 1, 2}, new int[]{2, 3, 1, 2, 1, 2}, new int[]{1, 1, 2, 2, 3, 2}, new int[]{1, 2, 2, 1, 3, 2}, new int[]{1, 2, 2, 2, 3, 1}, new int[]{1, 1, 3, 2, 2, 2}, new int[]{1, 2, 3, 1, 2, 2}, new int[]{1, 2, 3, 2, 2, 1}, new int[]{2, 2, 3, 2, 1, 1}, new int[]{2, 2, 1, 1, 3, 2}, new int[]{2, 2, 1, 2, 3, 1}, new int[]{2, 1, 3, 2, 1, 2}, new int[]{2, 2, 3, 1, 1, 2}, new int[]{3, 1, 2, 1, 3, 1}, new int[]{3, 1, 1, 2, 2, 2}, new int[]{3, 2, 1, 1, 2, 2}, new int[]{3, 2, 1, 2, 2, 1}, new int[]{3, 1, 2, 2, 1, 2}, new int[]{3, 2, 2, 1, 1, 2}, new int[]{3, 2, 2, 2, 1, 1}, new int[]{2, 1, 2, 1, 2, 3}, new int[]{2, 1, 2, 3, 2, 1}, new int[]{2, 3, 2, 1, 2, 1}, new int[]{1, 1, 1, 3, 2, 3}, new int[]{1, 3, 1, 1, 2, 3}, new int[]{1, 3, 1, 3, 2, 1}, new int[]{1, 1, 2, 3, 1, 3}, new int[]{1, 3, 2, 1, 1, 3}, new int[]{1, 3, 2, 3, 1, 1}, new int[]{2, 1, 1, 3, 1, 3}, new int[]{2, 3, 1, 1, 1, 3}, new int[]{2, 3, 1, 3, 1, 1}, new int[]{1, 1, 2, 1, 3, 3}, new int[]{1, 1, 2, 3, 3, 1}, new int[]{1, 3, 2, 1, 3, 1}, new int[]{1, 1, 3, 1, 2, 3}, new int[]{1, 1, 3, 3, 2, 1}, new int[]{1, 3, 3, 1, 2, 1}, new int[]{3, 1, 3, 1, 2, 1}, new int[]{2, 1, 1, 3, 3, 1}, new int[]{2, 3, 1, 1, 3, 1}, new int[]{2, 1, 3, 1, 1, 3}, new int[]{2, 1, 3, 3, 1, 1}, new int[]{2, 1, 3, 1, 3, 1}, new int[]{3, 1, 1, 1, 2, 3}, new int[]{3, 1, 1, 3, 2, 1}, new int[]{3, 3, 1, 1, 2, 1}, new int[]{3, 1, 2, 1, 1, 3}, new int[]{3, 1, 2, 3, 1, 1}, new int[]{3, 3, 2, 1, 1, 1}, new int[]{3, 1, 4, 1, 1, 1}, new int[]{2, 2, 1, 4, 1, 1}, new int[]{4, 3, 1, 1, 1, 1}, new int[]{1, 1, 1, 2, 2, 4}, new int[]{1, 1, 1, 4, 2, 2}, new int[]{1, 2, 1, 1, 2, 4}, new int[]{1, 2, 1, 4, 2, 1}, new int[]{1, 4, 1, 1, 2, 2}, new int[]{1, 4, 1, 2, 2, 1}, new int[]{1, 1, 2, 2, 1, 4}, new int[]{1, 1, 2, 4, 1, 2}, new int[]{1, 2, 2, 1, 1, 4}, new int[]{1, 2, 2, 4, 1, 1}, new int[]{1, 4, 2, 1, 1, 2}, new int[]{1, 4, 2, 2, 1, 1}, new int[]{2, 4, 1, 2, 1, 1}, new int[]{2, 2, 1, 1, 1, 4}, new int[]{4, 1, 3, 1, 1, 1}, new int[]{2, 4, 1, 1, 1, 2}, new int[]{1, 3, 4, 1, 1, 1}, new int[]{1, 1, 1, 2, 4, 2}, new int[]{1, 2, 1, 1, 4, 2}, new int[]{1, 2, 1, 2, 4, 1}, new int[]{1, 1, 4, 2, 1, 2}, new int[]{1, 2, 4, 1, 1, 2}, new int[]{1, 2, 4, 2, 1, 1}, new int[]{4, 1, 1, 2, 1, 2}, new int[]{4, 2, 1, 1, 1, 2}, new int[]{4, 2, 1, 2, 1, 1}, new int[]{2, 1, 2, 1, 4, 1}, new int[]{2, 1, 4, 1, 2, 1}, new int[]{4, 1, 2, 1, 2, 1}, new int[]{1, 1, 1, 1, 4, 3}, new int[]{1, 1, 1, 3, 4, 1}, new int[]{1, 3, 1, 1, 4, 1}, new int[]{1, 1, 4, 1, 1, 3}, new int[]{1, 1, 4, 3, 1, 1}, new int[]{4, 1, 1, 1, 1, 3}, new int[]{4, 1, 1, 3, 1, 1}, new int[]{1, 1, 3, 1, 4, 1}, new int[]{1, 1, 4, 1, 3, 1}, new int[]{3, 1, 1, 1, 4, 1}, new int[]{4, 1, 1, 1, 3, 1}, new int[]{2, 1, 1, 4, 1, 2}, new int[]{2, 1, 1, 2, 1, 4}, new int[]{2, 1, 1, 2, 3, 2}, new int[]{2, 3, 3, 1, 1, 1, 2}};

    private static int a(a aVar, int[] iArr, int i) {
        a(aVar, i, iArr);
        int i2 = 64;
        int i3 = -1;
        for (int i4 = 0; i4 < f138a.length; i4++) {
            int a2 = a(iArr, f138a[i4], 179);
            if (a2 < i2) {
                i3 = i4;
                i2 = a2;
            }
        }
        if (i3 >= 0) {
            return i3;
        }
        throw NotFoundException.a();
    }

    private static int[] a(a aVar) {
        int i;
        int i2;
        boolean z;
        int a2 = aVar.a();
        int i3 = 0;
        while (i3 < a2 && !aVar.a(i3)) {
            i3++;
        }
        int[] iArr = new int[6];
        int length = iArr.length;
        int i4 = i3;
        boolean z2 = false;
        int i5 = i3;
        int i6 = 0;
        while (i4 < a2) {
            if (aVar.a(i4) ^ z2) {
                iArr[i6] = iArr[i6] + 1;
                z = z2;
                i = i6;
            } else {
                if (i6 == length - 1) {
                    int i7 = 64;
                    int i8 = -1;
                    int i9 = 103;
                    while (i9 <= 105) {
                        int a3 = a(iArr, f138a[i9], 179);
                        if (a3 < i7) {
                            i8 = i9;
                        } else {
                            a3 = i7;
                        }
                        i9++;
                        i7 = a3;
                    }
                    if (i8 < 0 || !aVar.a(Math.max(0, i5 - ((i4 - i5) / 2)), i5, false)) {
                        i2 = iArr[0] + iArr[1] + i5;
                        for (int i10 = 2; i10 < length; i10++) {
                            iArr[i10 - 2] = iArr[i10];
                        }
                        iArr[length - 2] = 0;
                        iArr[length - 1] = 0;
                        i = i6 - 1;
                    } else {
                        return new int[]{i5, i4, i8};
                    }
                } else {
                    i = i6 + 1;
                    i2 = i5;
                }
                iArr[i] = 1;
                z = !z2;
                i5 = i2;
            }
            i4++;
            z2 = z;
            i6 = i;
        }
        throw NotFoundException.a();
    }

    /* JADX WARNING: Removed duplicated region for block: B:7:0x0031  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.zxing.h a(int r19, com.google.zxing.common.a r20, java.util.Hashtable r21) {
        /*
            r18 = this;
            int[] r13 = a(r20)
            r1 = 2
            r3 = r13[r1]
            switch(r3) {
                case 103: goto L_0x000f;
                case 104: goto L_0x0056;
                case 105: goto L_0x0059;
                default: goto L_0x000a;
            }
        L_0x000a:
            com.google.zxing.FormatException r1 = com.google.zxing.FormatException.a()
            throw r1
        L_0x000f:
            r1 = 101(0x65, float:1.42E-43)
        L_0x0011:
            r8 = 0
            r7 = 0
            java.lang.StringBuffer r14 = new java.lang.StringBuffer
            r2 = 20
            r14.<init>(r2)
            r2 = 0
            r6 = r13[r2]
            r2 = 1
            r5 = r13[r2]
            r2 = 6
            int[] r15 = new int[r2]
            r10 = 0
            r4 = 0
            r2 = 0
            r9 = 1
            r12 = r7
            r7 = r8
            r8 = r1
            r1 = r2
            r2 = r3
            r3 = r10
            r10 = r4
            r4 = r6
        L_0x002f:
            if (r7 != 0) goto L_0x015c
            r6 = 0
            r0 = r20
            int r11 = a(r0, r15, r5)
            r3 = 106(0x6a, float:1.49E-43)
            if (r11 == r3) goto L_0x003d
            r9 = 1
        L_0x003d:
            r3 = 106(0x6a, float:1.49E-43)
            if (r11 == r3) goto L_0x0046
            int r1 = r1 + 1
            int r3 = r1 * r11
            int r2 = r2 + r3
        L_0x0046:
            r3 = 0
            r4 = r5
        L_0x0048:
            int r0 = r15.length
            r16 = r0
            r0 = r16
            if (r3 >= r0) goto L_0x005c
            r16 = r15[r3]
            int r4 = r4 + r16
            int r3 = r3 + 1
            goto L_0x0048
        L_0x0056:
            r1 = 100
            goto L_0x0011
        L_0x0059:
            r1 = 99
            goto L_0x0011
        L_0x005c:
            switch(r11) {
                case 103: goto L_0x0077;
                case 104: goto L_0x0077;
                case 105: goto L_0x0077;
                default: goto L_0x005f;
            }
        L_0x005f:
            switch(r8) {
                case 99: goto L_0x0100;
                case 100: goto L_0x00d4;
                case 101: goto L_0x007c;
                default: goto L_0x0062;
            }
        L_0x0062:
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r9
        L_0x0066:
            if (r12 == 0) goto L_0x006b
            switch(r3) {
                case 99: goto L_0x0158;
                case 100: goto L_0x0154;
                case 101: goto L_0x0150;
                default: goto L_0x006b;
            }
        L_0x006b:
            r9 = r6
            r12 = r7
            r7 = r8
            r8 = r3
            r3 = r10
            r10 = r11
            r17 = r5
            r5 = r4
            r4 = r17
            goto L_0x002f
        L_0x0077:
            com.google.zxing.FormatException r1 = com.google.zxing.FormatException.a()
            throw r1
        L_0x007c:
            r3 = 64
            if (r11 >= r3) goto L_0x008b
            int r3 = r11 + 32
            char r3 = (char) r3
            r14.append(r3)
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r9
            goto L_0x0066
        L_0x008b:
            r3 = 96
            if (r11 >= r3) goto L_0x009a
            int r3 = r11 + -64
            char r3 = (char) r3
            r14.append(r3)
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r9
            goto L_0x0066
        L_0x009a:
            r3 = 106(0x6a, float:1.49E-43)
            if (r11 == r3) goto L_0x009f
            r9 = 0
        L_0x009f:
            switch(r11) {
                case 96: goto L_0x00ad;
                case 97: goto L_0x00ad;
                case 98: goto L_0x00b1;
                case 99: goto L_0x00c3;
                case 100: goto L_0x00ba;
                case 101: goto L_0x00ad;
                case 102: goto L_0x00ad;
                case 103: goto L_0x00a2;
                case 104: goto L_0x00a2;
                case 105: goto L_0x00a2;
                case 106: goto L_0x00cc;
                default: goto L_0x00a2;
            }
        L_0x00a2:
            r3 = r6
            r6 = r7
            r7 = r8
        L_0x00a5:
            r8 = r6
            r6 = r9
            r17 = r3
            r3 = r7
            r7 = r17
            goto L_0x0066
        L_0x00ad:
            r3 = r6
            r6 = r7
            r7 = r8
            goto L_0x00a5
        L_0x00b1:
            r3 = 1
            r6 = 100
            r17 = r7
            r7 = r6
            r6 = r17
            goto L_0x00a5
        L_0x00ba:
            r3 = 100
            r17 = r6
            r6 = r7
            r7 = r3
            r3 = r17
            goto L_0x00a5
        L_0x00c3:
            r3 = 99
            r17 = r6
            r6 = r7
            r7 = r3
            r3 = r17
            goto L_0x00a5
        L_0x00cc:
            r3 = 1
            r7 = r8
            r17 = r3
            r3 = r6
            r6 = r17
            goto L_0x00a5
        L_0x00d4:
            r3 = 96
            if (r11 >= r3) goto L_0x00e3
            int r3 = r11 + 32
            char r3 = (char) r3
            r14.append(r3)
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r9
            goto L_0x0066
        L_0x00e3:
            r3 = 106(0x6a, float:1.49E-43)
            if (r11 == r3) goto L_0x01ec
            r3 = 0
        L_0x00e8:
            switch(r11) {
                case 96: goto L_0x00eb;
                case 97: goto L_0x00eb;
                case 98: goto L_0x00f4;
                case 99: goto L_0x00fb;
                case 100: goto L_0x00eb;
                case 101: goto L_0x00f8;
                case 102: goto L_0x00eb;
                case 103: goto L_0x00eb;
                case 104: goto L_0x00eb;
                case 105: goto L_0x00eb;
                case 106: goto L_0x00fe;
                default: goto L_0x00eb;
            }
        L_0x00eb:
            r17 = r3
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r17
            goto L_0x0066
        L_0x00f4:
            r6 = 1
            r8 = 99
            goto L_0x00eb
        L_0x00f8:
            r8 = 101(0x65, float:1.42E-43)
            goto L_0x00eb
        L_0x00fb:
            r8 = 99
            goto L_0x00eb
        L_0x00fe:
            r7 = 1
            goto L_0x00eb
        L_0x0100:
            r3 = 100
            if (r11 >= r3) goto L_0x0116
            r3 = 10
            if (r11 >= r3) goto L_0x010d
            r3 = 48
            r14.append(r3)
        L_0x010d:
            r14.append(r11)
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r9
            goto L_0x0066
        L_0x0116:
            r3 = 106(0x6a, float:1.49E-43)
            if (r11 == r3) goto L_0x01e9
            r3 = 0
        L_0x011b:
            switch(r11) {
                case 100: goto L_0x013b;
                case 101: goto L_0x0130;
                case 102: goto L_0x0127;
                case 103: goto L_0x011e;
                case 104: goto L_0x011e;
                case 105: goto L_0x011e;
                case 106: goto L_0x0146;
                default: goto L_0x011e;
            }
        L_0x011e:
            r17 = r3
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r17
            goto L_0x0066
        L_0x0127:
            r17 = r3
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r17
            goto L_0x0066
        L_0x0130:
            r8 = 101(0x65, float:1.42E-43)
            r17 = r3
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r17
            goto L_0x0066
        L_0x013b:
            r8 = 100
            r17 = r3
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r17
            goto L_0x0066
        L_0x0146:
            r7 = 1
            r17 = r3
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r17
            goto L_0x0066
        L_0x0150:
            r3 = 99
            goto L_0x006b
        L_0x0154:
            r3 = 101(0x65, float:1.42E-43)
            goto L_0x006b
        L_0x0158:
            r3 = 100
            goto L_0x006b
        L_0x015c:
            int r6 = r20.a()
        L_0x0160:
            if (r5 >= r6) goto L_0x016d
            r0 = r20
            boolean r7 = r0.a(r5)
            if (r7 == 0) goto L_0x016d
            int r5 = r5 + 1
            goto L_0x0160
        L_0x016d:
            int r7 = r5 - r4
            int r7 = r7 / 2
            int r7 = r7 + r5
            int r6 = java.lang.Math.min(r6, r7)
            r7 = 0
            r0 = r20
            boolean r6 = r0.a(r5, r6, r7)
            if (r6 != 0) goto L_0x0184
            com.google.zxing.NotFoundException r1 = com.google.zxing.NotFoundException.a()
            throw r1
        L_0x0184:
            int r1 = r1 * r3
            int r1 = r2 - r1
            int r1 = r1 % 103
            if (r1 == r3) goto L_0x0190
            com.google.zxing.ChecksumException r1 = com.google.zxing.ChecksumException.a()
            throw r1
        L_0x0190:
            int r1 = r14.length()
            if (r1 <= 0) goto L_0x01a1
            if (r9 == 0) goto L_0x01a1
            r2 = 99
            if (r8 != r2) goto L_0x01b0
            int r2 = r1 + -2
            r14.delete(r2, r1)
        L_0x01a1:
            java.lang.String r1 = r14.toString()
            int r2 = r1.length()
            if (r2 != 0) goto L_0x01b6
            com.google.zxing.FormatException r1 = com.google.zxing.FormatException.a()
            throw r1
        L_0x01b0:
            int r2 = r1 + -1
            r14.delete(r2, r1)
            goto L_0x01a1
        L_0x01b6:
            r2 = 1
            r2 = r13[r2]
            r3 = 0
            r3 = r13[r3]
            int r2 = r2 + r3
            float r2 = (float) r2
            r3 = 1073741824(0x40000000, float:2.0)
            float r2 = r2 / r3
            int r3 = r5 + r4
            float r3 = (float) r3
            r4 = 1073741824(0x40000000, float:2.0)
            float r3 = r3 / r4
            com.google.zxing.h r4 = new com.google.zxing.h
            r5 = 0
            r6 = 2
            com.google.zxing.j[] r6 = new com.google.zxing.j[r6]
            r7 = 0
            com.google.zxing.j r8 = new com.google.zxing.j
            r0 = r19
            float r9 = (float) r0
            r8.<init>(r2, r9)
            r6[r7] = r8
            r2 = 1
            com.google.zxing.j r7 = new com.google.zxing.j
            r0 = r19
            float r8 = (float) r0
            r7.<init>(r3, r8)
            r6[r2] = r7
            com.google.zxing.a r2 = com.google.zxing.a.h
            r4.<init>(r1, r5, r6, r2)
            return r4
        L_0x01e9:
            r3 = r9
            goto L_0x011b
        L_0x01ec:
            r3 = r9
            goto L_0x00e8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.b.b.a(int, com.google.zxing.common.a, java.util.Hashtable):com.google.zxing.h");
    }
}
