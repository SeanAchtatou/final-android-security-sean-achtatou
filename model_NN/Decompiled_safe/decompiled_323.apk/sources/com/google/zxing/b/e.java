package com.google.zxing.b;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;

public final class e extends n {

    /* renamed from: a  reason: collision with root package name */
    static final int[] f141a = {0, 11, 13, 14, 19, 25, 28, 21, 22, 26};
    private final int[] f = new int[4];

    private static void a(StringBuffer stringBuffer, int i) {
        for (int i2 = 0; i2 < 10; i2++) {
            if (i == f141a[i2]) {
                stringBuffer.insert(0, (char) (i2 + 48));
                return;
            }
        }
        throw NotFoundException.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.b.n.a(com.google.zxing.common.a, int, boolean, int[]):int[]
     arg types: [com.google.zxing.common.a, int, int, int[]]
     candidates:
      com.google.zxing.b.n.a(com.google.zxing.common.a, int[], int, int[][]):int
      com.google.zxing.b.n.a(int, com.google.zxing.common.a, int[], java.util.Hashtable):com.google.zxing.h
      com.google.zxing.b.n.a(com.google.zxing.common.a, int, boolean, int[]):int[] */
    /* access modifiers changed from: protected */
    public int a(a aVar, int[] iArr, StringBuffer stringBuffer) {
        int[] iArr2 = this.f;
        iArr2[0] = 0;
        iArr2[1] = 0;
        iArr2[2] = 0;
        iArr2[3] = 0;
        int a2 = aVar.a();
        int i = iArr[1];
        int i2 = 0;
        int i3 = 0;
        while (i2 < 6 && i < a2) {
            int a3 = a(aVar, iArr2, i, e);
            stringBuffer.append((char) ((a3 % 10) + 48));
            int i4 = i;
            for (int i5 : iArr2) {
                i4 += i5;
            }
            int i6 = a3 >= 10 ? (1 << (5 - i2)) | i3 : i3;
            i2++;
            i3 = i6;
            i = i4;
        }
        a(stringBuffer, i3);
        int i7 = a(aVar, i, true, c)[1];
        int i8 = 0;
        while (i8 < 6 && i7 < a2) {
            stringBuffer.append((char) (a(aVar, iArr2, i7, d) + 48));
            int i9 = i7;
            for (int i10 : iArr2) {
                i9 += i10;
            }
            i8++;
            i7 = i9;
        }
        return i7;
    }

    /* access modifiers changed from: package-private */
    public com.google.zxing.a b() {
        return com.google.zxing.a.f;
    }
}
