package com.google.zxing.b;

import com.google.zxing.common.a;

public final class f extends n {

    /* renamed from: a  reason: collision with root package name */
    private final int[] f142a = new int[4];

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.b.n.a(com.google.zxing.common.a, int, boolean, int[]):int[]
     arg types: [com.google.zxing.common.a, int, int, int[]]
     candidates:
      com.google.zxing.b.n.a(com.google.zxing.common.a, int[], int, int[][]):int
      com.google.zxing.b.n.a(int, com.google.zxing.common.a, int[], java.util.Hashtable):com.google.zxing.h
      com.google.zxing.b.n.a(com.google.zxing.common.a, int, boolean, int[]):int[] */
    /* access modifiers changed from: protected */
    public int a(a aVar, int[] iArr, StringBuffer stringBuffer) {
        int[] iArr2 = this.f142a;
        iArr2[0] = 0;
        iArr2[1] = 0;
        iArr2[2] = 0;
        iArr2[3] = 0;
        int a2 = aVar.a();
        int i = iArr[1];
        int i2 = 0;
        while (i2 < 4 && i < a2) {
            stringBuffer.append((char) (a(aVar, iArr2, i, d) + 48));
            int i3 = i;
            for (int i4 : iArr2) {
                i3 += i4;
            }
            i2++;
            i = i3;
        }
        int i5 = a(aVar, i, true, c)[1];
        int i6 = 0;
        while (i6 < 4 && i5 < a2) {
            stringBuffer.append((char) (a(aVar, iArr2, i5, d) + 48));
            int i7 = i5;
            for (int i8 : iArr2) {
                i7 += i8;
            }
            i6++;
            i5 = i7;
        }
        return i5;
    }

    /* access modifiers changed from: package-private */
    public com.google.zxing.a b() {
        return com.google.zxing.a.e;
    }
}
