package com.google.zxing;

import java.util.Enumeration;
import java.util.Hashtable;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private final String f195a;
    private final byte[] b;
    private j[] c;
    private final a d;
    private Hashtable e;
    private final long f;

    public h(String str, byte[] bArr, j[] jVarArr, a aVar) {
        this(str, bArr, jVarArr, aVar, System.currentTimeMillis());
    }

    public h(String str, byte[] bArr, j[] jVarArr, a aVar, long j) {
        if (str == null && bArr == null) {
            throw new IllegalArgumentException("Text and bytes are null");
        }
        this.f195a = str;
        this.b = bArr;
        this.c = jVarArr;
        this.d = aVar;
        this.e = null;
        this.f = j;
    }

    public String a() {
        return this.f195a;
    }

    public void a(i iVar, Object obj) {
        if (this.e == null) {
            this.e = new Hashtable(3);
        }
        this.e.put(iVar, obj);
    }

    public void a(Hashtable hashtable) {
        if (hashtable == null) {
            return;
        }
        if (this.e == null) {
            this.e = hashtable;
            return;
        }
        Enumeration keys = hashtable.keys();
        while (keys.hasMoreElements()) {
            i iVar = (i) keys.nextElement();
            this.e.put(iVar, hashtable.get(iVar));
        }
    }

    public void a(j[] jVarArr) {
        if (this.c == null) {
            this.c = jVarArr;
        } else if (jVarArr != null && jVarArr.length > 0) {
            j[] jVarArr2 = new j[(this.c.length + jVarArr.length)];
            System.arraycopy(this.c, 0, jVarArr2, 0, this.c.length);
            System.arraycopy(jVarArr, 0, jVarArr2, this.c.length, jVarArr.length);
            this.c = jVarArr2;
        }
    }

    public j[] b() {
        return this.c;
    }

    public a c() {
        return this.d;
    }

    public Hashtable d() {
        return this.e;
    }

    public String toString() {
        return this.f195a == null ? new StringBuffer().append("[").append(this.b.length).append(" bytes]").toString() : this.f195a;
    }
}
