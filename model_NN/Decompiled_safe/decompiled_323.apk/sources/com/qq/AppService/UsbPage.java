package com.qq.AppService;

import android.app.Activity;
import android.widget.ImageView;

/* compiled from: ProGuard */
public class UsbPage extends Activity {

    /* renamed from: a  reason: collision with root package name */
    public static UsbPage f210a = null;
    private ImageView b = null;

    public static void a() {
        if (f210a != null) {
            f210a.finish();
        }
        f210a = null;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        f210a = null;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }
}
