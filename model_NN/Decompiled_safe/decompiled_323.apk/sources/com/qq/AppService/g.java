package com.qq.AppService;

import com.tencent.assistant.utils.XLog;
import com.tencent.wcs.jce.ReportQQLoginStatusResponse;

/* compiled from: ProGuard */
class g implements com.qq.m.g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppService f240a;

    g(AppService appService) {
        this.f240a = appService;
    }

    public void a(int i, int i2, ReportQQLoginStatusResponse reportQQLoginStatusResponse) {
        XLog.d("Longer", "AppService QQLoginCallback onResult response: " + i + " errcode: " + i2);
        if (this.f240a.y != null) {
            synchronized (this.f240a.y.d) {
                if (this.f240a.y != null) {
                    this.f240a.y.c = false;
                }
                this.f240a.y.d.notifyAll();
            }
        }
    }
}
