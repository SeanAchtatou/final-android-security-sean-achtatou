package com.qq.AppService;

import com.tencent.assistant.b.f;
import com.tencent.assistant.module.a.a.c;
import com.tencent.assistant.module.a.a.d;
import com.tencent.assistant.module.a.a.e;
import com.tencent.assistant.module.a.i;
import com.tencent.assistant.module.a.j;
import com.tencent.assistant.module.a.l;
import com.tencent.assistant.module.a.n;
import com.tencent.assistant.module.update.AppUpdateConst;
import com.tencent.assistant.module.update.b;
import com.tencent.assistant.plugin.PluginHelper;
import com.tencent.assistantv2.st.business.LaunchSpeedSTManager;
import com.tencent.beacon.event.a;
import com.tencent.nucleus.manager.root.h;
import com.tencent.pangu.manager.ac;

/* compiled from: ProGuard */
final class o implements Runnable {
    o() {
    }

    public void run() {
        if (!AstApp.r()) {
            synchronized (AstApp.f) {
                if (!AstApp.r()) {
                    boolean unused = AstApp.o = true;
                    a.a(AstApp.i());
                    LaunchSpeedSTManager.d().c();
                    new com.tencent.assistant.module.a.a.a().run();
                    b.a().a(AppUpdateConst.RequestLaunchType.TYPE_STARTUP);
                    AstApp.i().E();
                    new l().run();
                    new com.tencent.assistant.module.a.a.b().run();
                    new j().run();
                    new d().run();
                    new c().run();
                    new com.tencent.pangu.module.b.a.c().run();
                    new h().run();
                    new i().run();
                    new n().run();
                    f.a().b();
                    com.tencent.pangu.module.wisedownload.l.a().b();
                    new e().run();
                    AstApp.G();
                    AstApp.H();
                    com.tencent.assistant.plugin.mgr.c.a();
                    PluginHelper.execFreeWifiInit();
                    ac.a();
                }
            }
        }
    }
}
