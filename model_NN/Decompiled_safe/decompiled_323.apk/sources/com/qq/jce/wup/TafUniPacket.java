package com.qq.jce.wup;

import com.tencent.connect.common.Constants;
import java.util.HashMap;
import java.util.Map;

public class TafUniPacket extends UniPacket {
    private static final long serialVersionUID = 1;

    public TafUniPacket() {
        this._package.iVersion = 2;
        this._package.cPacketType = 0;
        this._package.iMessageType = 0;
        this._package.iTimeout = 0;
        this._package.sBuffer = new byte[0];
        this._package.context = new HashMap();
        this._package.status = new HashMap();
    }

    public void setTafVersion(short version) {
        this._package.iVersion = version;
        if (version == 3) {
            useVersion3();
        }
    }

    public void setTafPacketType(byte packetType) {
        this._package.cPacketType = packetType;
    }

    public void setTafMessageType(int messageType) {
        this._package.iMessageType = messageType;
    }

    public void setTafTimeout(int timeout) {
        this._package.iTimeout = timeout;
    }

    public void setTafBuffer(byte[] buffer) {
        this._package.sBuffer = buffer;
    }

    public void setTafContext(Map<String, String> context) {
        this._package.context = context;
    }

    public void setTafStatus(Map<String, String> status) {
        this._package.status = status;
    }

    public short getTafVersion() {
        return this._package.iVersion;
    }

    public byte getTafPacketType() {
        return this._package.cPacketType;
    }

    public int getTafMessageType() {
        return this._package.iMessageType;
    }

    public int getTafTimeout() {
        return this._package.iTimeout;
    }

    public byte[] getTafBuffer() {
        return this._package.sBuffer;
    }

    public Map<String, String> getTafContext() {
        return this._package.context;
    }

    public Map<String, String> getTafStatus() {
        return this._package.status;
    }

    public int getTafResultCode() {
        String rcode = this._package.status.get("STATUS_RESULT_CODE");
        if (rcode != null) {
            return Integer.parseInt(rcode);
        }
        return 0;
    }

    public String getTafResultDesc() {
        String rdesc = this._package.status.get("STATUS_RESULT_DESC");
        return rdesc != null ? rdesc : Constants.STR_EMPTY;
    }
}
