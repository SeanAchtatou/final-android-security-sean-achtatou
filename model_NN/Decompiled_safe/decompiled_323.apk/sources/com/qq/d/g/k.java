package com.qq.d.g;

import android.os.SystemProperties;
import android.text.TextUtils;
import java.util.Map;

/* compiled from: ProGuard */
public class k extends r {
    public boolean a(Map<String, String> map, boolean z, s sVar) {
        boolean z2;
        boolean z3;
        boolean z4 = false;
        String c = sVar.c();
        String str = SystemProperties.get("ro.build.host");
        String str2 = SystemProperties.get("ro.mk.version");
        if (c.toLowerCase().contains("mokeeos") || (!TextUtils.isEmpty(str) && str.toLowerCase().contains("mokee"))) {
            if (!z) {
                a(map, "rombrand", "mokee");
                if (!TextUtils.isEmpty(str2)) {
                    c = str2;
                }
                a(map, "romversion", c);
            }
            z2 = true;
        } else {
            z2 = false;
        }
        if (this.b == null) {
            return z2;
        }
        r rVar = this.b;
        if (z || z2) {
            z3 = true;
        } else {
            z3 = false;
        }
        boolean a2 = rVar.a(map, z3, sVar);
        if (z2 || a2) {
            z4 = true;
        }
        return z4;
    }
}
