package com.qq.d.g;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import com.tencent.assistant.st.STConst;
import com.tencent.connect.common.Constants;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class o {

    /* renamed from: a  reason: collision with root package name */
    private Map<String, String> f282a;

    private o() {
        this.f282a = new HashMap();
    }

    public static o a() {
        return q.f283a;
    }

    public Map<String, String> b() {
        return this.f282a;
    }

    public String c() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry next : this.f282a.entrySet()) {
            sb.append("[" + ((String) next.getKey()) + "]: [" + ((String) next.getValue()) + "]\n");
        }
        String sb2 = sb.toString();
        Log.d(Constants.STR_EMPTY, "generatePropString" + sb2);
        return sb2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.d.g.a.a(java.util.Map<java.lang.String, java.lang.String>, boolean, com.qq.d.g.s):boolean
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, com.qq.d.g.s]
     candidates:
      com.qq.d.g.r.a(java.util.Map<java.lang.String, java.lang.String>, java.lang.String, java.lang.String):void
      com.qq.d.g.a.a(java.util.Map<java.lang.String, java.lang.String>, boolean, com.qq.d.g.s):boolean */
    public String a(Context context) {
        if (!this.f282a.isEmpty()) {
            return c();
        }
        s sVar = new s(context);
        a aVar = new a();
        d dVar = new d();
        g gVar = new g();
        h hVar = new h();
        n nVar = new n();
        i iVar = new i();
        k kVar = new k();
        u uVar = new u();
        w wVar = new w();
        l lVar = new l();
        v vVar = new v();
        m mVar = new m();
        c cVar = new c();
        e eVar = new e();
        aVar.a(dVar).a(lVar).a(gVar).a(hVar).a(nVar).a(iVar).a(kVar).a(uVar).a(wVar).a(eVar).a(new f()).a(vVar);
        vVar.a(mVar);
        mVar.a(cVar);
        if (!aVar.a(this.f282a, false, sVar)) {
            this.f282a.put("rombrand", STConst.ST_INSTALL_FAIL_STR_UNKNOWN);
            this.f282a.put("romversion", STConst.ST_INSTALL_FAIL_STR_UNKNOWN);
        }
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        String a2 = t.a(context);
        if (TextUtils.isEmpty(a2)) {
            a2 = Constants.STR_EMPTY;
        }
        this.f282a.put("imei", deviceId);
        this.f282a.put("mac", a2);
        return c();
    }

    public String b(Context context) {
        if ("miui".equals(this.f282a.get("rombrand"))) {
            return c(context);
        }
        if ("smartisan".equals(this.f282a.get("rombrand"))) {
            return d(context);
        }
        return Constants.STR_EMPTY;
    }

    private String c(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("g", j.a(context));
            jSONObject.put("v", j.a());
            jSONObject.put("c", j.c());
            jSONObject.put("d", j.d());
            jSONObject.put("i", j.d(context));
            jSONObject.put("b", j.b(context));
            jSONObject.put("l", "zh_CN");
            return jSONObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return Constants.STR_EMPTY;
        }
    }

    private String d(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("builddate", this.f282a.get("builddate"));
            return jSONObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return Constants.STR_EMPTY;
        }
    }
}
