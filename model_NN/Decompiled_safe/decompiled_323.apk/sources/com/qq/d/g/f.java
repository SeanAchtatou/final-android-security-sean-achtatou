package com.qq.d.g;

import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Log;
import com.tencent.connect.common.Constants;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: ProGuard */
public class f extends r {
    public boolean a(Map<String, String> map, boolean z, s sVar) {
        boolean z2;
        boolean z3;
        boolean z4 = false;
        String str = SystemProperties.get("ro.letv.release.version");
        String str2 = SystemProperties.get("ro.letv.ext.brand");
        String str3 = SystemProperties.get("ro.letv.ui");
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2) || TextUtils.isEmpty(str3)) {
            z2 = false;
        } else {
            a(map, "rombrand", "letv");
            a(map, "romversion", str);
            a(map, "rombranch", a(str));
            Log.d("LeTVHandler", "letv" + str);
            z2 = true;
        }
        if (this.b == null) {
            return z2;
        }
        r rVar = this.b;
        if (z || z2) {
            z3 = true;
        } else {
            z3 = false;
        }
        boolean a2 = rVar.a(map, z3, sVar);
        if (z2 || a2) {
            z4 = true;
        }
        return z4;
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        String lowerCase = str.toLowerCase();
        Matcher matcher = Pattern.compile("_[a-z]\\d{3}([a-z])").matcher(lowerCase);
        if (matcher.find()) {
            return TextUtils.equals(matcher.group(1), "s") ? "stable" : "develop";
        }
        Matcher matcher2 = Pattern.compile("_\\d{8}_([a-z])\\d{3}").matcher(lowerCase);
        if (matcher2.find()) {
            return TextUtils.equals(matcher2.group(1), "s") ? "stable" : "develop";
        }
        return Constants.STR_EMPTY;
    }
}
