package com.qq.provider;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;
import com.qq.AppService.AstApp;
import com.qq.AppService.r;
import com.qq.c.e;
import com.qq.c.h;
import com.qq.g.c;
import com.qq.ndk.Native;
import com.qq.ndk.NativeFileObject;
import com.qq.util.ab;
import com.qq.util.l;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Vector;

/* compiled from: ProGuard */
public final class n {

    /* renamed from: a  reason: collision with root package name */
    public static final String[] f345a = {"/sdcard/external_sd", "/emmc", "/sdcard/_ExternalSD", "/sdcard/ext-sd", "/sdcard/extsd", "/tflash", "/sdcard/tflash", "/mnt/sdcard-ext", "/mnt/ext_sdcard", "/mnt/extSdCard", "/mnt/sdcard/ext_sd", "/mnt/sdcard2", "/mnt/external1", "/mnt/emmc", "/mnt/sdcard/external_SD", "/storage/extSdCard", "/udisk", "/mnt/ext-card", "/mnt/extSdCard", "/storage/sdcard1", "/storage/sdcard0"};
    private static n b;
    private Vector<String> c = new Vector<>();

    public static n a() {
        if (b == null) {
            b = new n();
        }
        return b;
    }

    public void a(c cVar) {
        File[] listRoots = File.listRoots();
        if (listRoots != null) {
            String path = listRoots[0].getPath();
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(path));
            cVar.a(0);
            cVar.a(arrayList);
        }
    }

    public void a(Context context, c cVar) {
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        int h = cVar.h();
        if (r.b(j) || r.b(j2)) {
            cVar.a(1);
            return;
        }
        File file = new File(j);
        if (!file.exists() || !file.isDirectory()) {
            cVar.a(5);
            return;
        }
        if (j.equals("/")) {
            Log.e("com.qq.connect.ndk", " scan root may be crash for memory");
        } else if (j.endsWith("/")) {
            j = j.substring(0, j.length() - 1);
        }
        Native nativeR = new Native();
        long currentTimeMillis = System.currentTimeMillis();
        NativeFileObject[] findFileList = nativeR.findFileList(j, j2, h);
        Log.e("com.qq.connect.ndk", "time spent:" + (System.currentTimeMillis() - currentTimeMillis));
        if (findFileList == null) {
            cVar.a(6);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(findFileList.length));
        for (int i = 0; i < findFileList.length; i++) {
            arrayList.add(r.a(findFileList[i].fileName));
            arrayList.add(r.a(findFileList[i].fileType));
            arrayList.add(r.a(findFileList[i].uid));
            arrayList.add(r.a(findFileList[i].gid));
            arrayList.add(r.a(findFileList[i].fileSize));
            arrayList.add(r.a(findFileList[i].llAccessTime * 1000));
            arrayList.add(r.a(findFileList[i].llCreateTime * 1000));
            arrayList.add(r.a(findFileList[i].llModifyTime * 1000));
        }
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void b(Context context, c cVar) {
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        int h = cVar.h();
        if (r.b(j) || r.b(j2)) {
            cVar.a(1);
            return;
        }
        File file = new File(j);
        if (!file.exists() || !file.isDirectory()) {
            cVar.a(5);
        } else if (h > 3 || h < 0) {
            cVar.a(1);
        } else {
            if (j.equals("/")) {
                Log.e("com.qq.connect.ndk", " scan root may be crash for memory");
            } else if (j.endsWith("/")) {
                j = j.substring(0, j.length() - 1);
            }
            Native nativeR = new Native();
            long currentTimeMillis = System.currentTimeMillis();
            String[] findFiles = nativeR.findFiles(j, j2, h);
            Log.e("com.qq.connect.ndk", "time spent:" + (System.currentTimeMillis() - currentTimeMillis));
            if (findFiles == null) {
                cVar.a(6);
                return;
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(findFiles.length));
            for (String a2 : findFiles) {
                arrayList.add(r.a(a2));
            }
            cVar.a(0);
            cVar.a(arrayList);
        }
    }

    public void c(Context context, c cVar) {
        File cacheDir = context.getCacheDir();
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(cacheDir.getAbsolutePath()));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void d(Context context, c cVar) {
        try {
            b(context.getCacheDir());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static File b() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return Environment.getExternalStorageDirectory();
        }
        return null;
    }

    public void b(c cVar) {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            cVar.a(9);
            return;
        }
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        if (externalStorageDirectory == null) {
            cVar.a(9);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(externalStorageDirectory.getAbsolutePath()));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void e(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        String externalStorageState = Environment.getExternalStorageState();
        if (externalStorageState.equals("mounted")) {
            arrayList.add(r.a(0));
        } else if (externalStorageState.equals("mounted_ro")) {
            arrayList.add(r.a(1));
        } else if (externalStorageState.equals("shared")) {
            arrayList.add(r.a(2));
        } else if (externalStorageState.equals("nofs")) {
            arrayList.add(r.a(3));
        } else if (externalStorageState.equals("removed")) {
            arrayList.add(r.a(4));
        } else if (externalStorageState.equals("unmounted")) {
            arrayList.add(r.a(5));
        } else if (externalStorageState.equals("unmountable")) {
            arrayList.add(r.a(6));
        } else if (externalStorageState.equals("checking")) {
            arrayList.add(r.a(7));
        } else if (externalStorageState.equals("bad_removal")) {
            arrayList.add(r.a(8));
        } else {
            arrayList.add(r.a(-1));
        }
        cVar.a(arrayList);
        cVar.a(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.util.l.a(java.lang.String, boolean):int
     arg types: [java.lang.String, int]
     candidates:
      com.qq.util.l.a(java.io.BufferedOutputStream, java.lang.String):int
      com.qq.util.l.a(java.io.BufferedOutputStream, byte[]):void
      com.qq.util.l.a(java.lang.String, boolean):int */
    public void c(c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        cVar.g();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        try {
            cVar.a(l.a(j, true));
        } catch (Exception e) {
            cVar.a(4);
        }
    }

    public void d(c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        cVar.g();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        File file = new File(j);
        if (e(file)) {
            try {
                if (file.isDirectory()) {
                    cVar.a(0);
                } else {
                    cVar.a(5);
                }
            } catch (Exception e) {
                e.printStackTrace();
                cVar.a(4);
            }
        } else {
            try {
                if (file.mkdirs()) {
                    cVar.a(0);
                } else {
                    cVar.a(6);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                cVar.a(4);
            }
        }
    }

    public void f(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        File file = new File(j);
        ArrayList arrayList = new ArrayList();
        if (e(file)) {
            arrayList.add(r.a(1));
        } else {
            arrayList.add(r.a(0));
        }
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void g(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        File file = new File(j);
        if (!file.exists()) {
            cVar.a(3);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(f(file)));
        String b2 = r.b(file.lastModified());
        arrayList.add(r.a(b2));
        arrayList.add(r.a(b2));
        arrayList.add(r.a(0));
        arrayList.add(r.a(file.length()));
        arrayList.add(r.a(file.getName()));
        cVar.a(0);
        cVar.a(arrayList);
    }

    private static int f(File file) {
        if (!file.exists()) {
            return -1;
        }
        int i = 0;
        if (file.isDirectory()) {
            i = 8;
        }
        if (file.canWrite()) {
            i += 2;
        }
        if (file.canRead()) {
            return i + 1;
        }
        return i;
    }

    public void e(c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        File file = new File(j);
        if (!e(file)) {
            cVar.a(3);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(f(file)));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void f(c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
        } else if (!e(new File(j))) {
            cVar.a(3);
        } else {
            ArrayList arrayList = new ArrayList();
            cVar.a(0);
            cVar.a(arrayList);
        }
    }

    public void g(c cVar) {
        boolean z = true;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        if (r.b(j) || r.b(j2)) {
            cVar.a(1);
            return;
        }
        File file = new File(j);
        File file2 = new File(j2);
        if (!e(file)) {
            cVar.a(1);
            return;
        }
        try {
            z = file2.exists();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (z) {
            cVar.a(6);
            return;
        }
        try {
            if (!a(file, file2)) {
                cVar.a(4);
            } else {
                cVar.a(0);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            cVar.a(4);
        }
    }

    public void h(c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        if (r.b(j) || r.b(j2)) {
            cVar.a(1);
        } else if (j.equals(j2)) {
            cVar.a(0);
        } else if (j2.startsWith(j + "/")) {
            cVar.a(6);
        } else {
            File file = new File(j);
            File file2 = new File(j2);
            if (!e(file)) {
                cVar.a(1);
                return;
            }
            try {
                if (!d(file, file2)) {
                    cVar.a(4);
                } else {
                    cVar.a(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
                cVar.a(4);
            }
        }
    }

    public void i(c cVar) {
        boolean z;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        if (r.b(j) || r.b(j2)) {
            cVar.a(1);
            return;
        }
        File file = new File(j);
        File file2 = new File(j2);
        if (!e(file)) {
            cVar.a(1);
            return;
        }
        try {
            z = file2.exists();
        } catch (Exception e) {
            e.printStackTrace();
            z = true;
        }
        if (z) {
            cVar.a(6);
            return;
        }
        try {
            if (!a(file, file2)) {
                cVar.a(4);
            } else if (!file2.exists()) {
                cVar.a(1);
            } else {
                cVar.a(0);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            cVar.a(4);
        }
    }

    public void j(c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        File file = new File(cVar.j());
        if (!e(file)) {
            cVar.a(1);
            return;
        }
        try {
            if (file.isFile()) {
                cVar.a(3);
                return;
            }
            int length = file.list().length;
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(length));
            cVar.a(0);
            cVar.a(arrayList);
        } catch (Exception e) {
            e.printStackTrace();
            cVar.a(4);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0080 A[SYNTHETIC, Splitter:B:34:0x0080] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0085 A[SYNTHETIC, Splitter:B:37:0x0085] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00a2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void k(com.qq.g.c r13) {
        /*
            r12 = this;
            r11 = 8
            r4 = 0
            r3 = 1
            r1 = 0
            int r0 = r13.e()
            if (r0 >= r3) goto L_0x000f
            r13.a(r3)
        L_0x000e:
            return
        L_0x000f:
            java.lang.String r0 = r13.j()
            boolean r2 = com.qq.AppService.r.b(r0)
            if (r2 == 0) goto L_0x001d
            r13.a(r3)
            goto L_0x000e
        L_0x001d:
            java.lang.String r2 = com.qq.c.b.a()
            java.lang.Runtime r3 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x006d }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006d }
            r5.<init>()     // Catch:{ Exception -> 0x006d }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ Exception -> 0x006d }
            java.lang.String r5 = " -l -a "
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x006d }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x006d }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x006d }
            java.lang.Process r7 = r3.exec(r0)     // Catch:{ Exception -> 0x006d }
            if (r7 == 0) goto L_0x0045
            r7.waitFor()     // Catch:{ InterruptedException -> 0x0075 }
        L_0x0045:
            java.io.InputStream r3 = r7.getInputStream()     // Catch:{ Exception -> 0x0092 }
            java.io.DataInputStream r5 = new java.io.DataInputStream     // Catch:{ Exception -> 0x00b2 }
            r5.<init>(r3)     // Catch:{ Exception -> 0x00b2 }
            int r8 = r5.available()     // Catch:{ Exception -> 0x00b6 }
            int r0 = r8 + 1
            byte[] r6 = new byte[r0]     // Catch:{ Exception -> 0x00b6 }
            r0 = r1
            r2 = r1
        L_0x0058:
            r9 = 5
            if (r0 >= r9) goto L_0x007a
            if (r2 >= r8) goto L_0x007a
            r9 = 0
            int r10 = r8 - r2
            int r9 = r5.read(r6, r9, r10)     // Catch:{ Exception -> 0x00b6 }
            if (r9 <= 0) goto L_0x006a
            int r0 = r2 + r9
            r2 = r0
            r0 = r1
        L_0x006a:
            int r0 = r0 + 1
            goto L_0x0058
        L_0x006d:
            r0 = move-exception
            r0.printStackTrace()
            r13.a(r11)
            goto L_0x000e
        L_0x0075:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0045
        L_0x007a:
            r0 = 0
            r6[r8] = r0     // Catch:{ Exception -> 0x00b6 }
            r4 = r6
        L_0x007e:
            if (r3 == 0) goto L_0x0083
            r3.close()     // Catch:{ IOException -> 0x0098 }
        L_0x0083:
            if (r5 == 0) goto L_0x0088
            r5.close()     // Catch:{ IOException -> 0x009d }
        L_0x0088:
            r7.destroy()
            if (r4 != 0) goto L_0x00a2
            r13.a(r11)
            goto L_0x000e
        L_0x0092:
            r0 = move-exception
            r0 = r4
            r2 = r4
        L_0x0095:
            r3 = r0
            r5 = r2
            goto L_0x007e
        L_0x0098:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0083
        L_0x009d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0088
        L_0x00a2:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r0.add(r4)
            r13.a(r0)
            r13.a(r1)
            goto L_0x000e
        L_0x00b2:
            r0 = move-exception
            r0 = r3
            r2 = r4
            goto L_0x0095
        L_0x00b6:
            r0 = move-exception
            r0 = r3
            r2 = r5
            goto L_0x0095
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.n.k(com.qq.g.c):void");
    }

    public void l(c cVar) {
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        File file = new File(j);
        if (!e(file)) {
            cVar.a(3);
            return;
        }
        try {
            File[] listFiles = file.listFiles();
            if (listFiles == null) {
                cVar.a(8);
                return;
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(listFiles.length));
            for (int i = 0; i < listFiles.length; i++) {
                if (listFiles[i].isDirectory()) {
                    arrayList.add(r.a(8));
                } else if (listFiles[i].isFile()) {
                    arrayList.add(r.a(0));
                } else {
                    arrayList.add(r.a(16));
                }
                arrayList.add(r.a(listFiles[i].lastModified()));
                if (listFiles[i].isFile()) {
                    arrayList.add(r.a(listFiles[i].length()));
                } else {
                    arrayList.add(r.a(0L));
                }
                arrayList.add(r.a(listFiles[i].getName()));
            }
            cVar.a(arrayList);
            cVar.a(0);
        } catch (Exception e) {
            e.printStackTrace();
            cVar.a(8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.provider.n.a(java.io.File, boolean, java.io.FilenameFilter):void
     arg types: [java.io.File, int, ?[OBJECT, ARRAY]]
     candidates:
      com.qq.provider.n.a(java.io.File, byte[], int):int
      com.qq.provider.n.a(java.io.InputStream, java.io.File, byte[]):boolean
      com.qq.provider.n.a(java.io.File, boolean, java.io.FilenameFilter):void */
    public void m(c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        File file = new File(j);
        if (!e(file)) {
            cVar.a(3);
            return;
        }
        ArrayList arrayList = new ArrayList();
        this.c.clear();
        try {
            a(file, false, (FilenameFilter) null);
            arrayList.add(r.a(this.c.size()));
            for (int i = 0; i < this.c.size(); i++) {
                arrayList.add(r.a(this.c.get(i)));
            }
            this.c.clear();
            cVar.a(arrayList);
            cVar.a(0);
        } catch (Exception e) {
            cVar.a(4);
        }
    }

    public void n(c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
        } else if (!new File(j).exists()) {
            cVar.a(3);
        } else {
            String[] listFileDic = new Native().listFileDic(j);
            if (listFileDic == null || listFileDic.length == 0) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(r.a(0));
                cVar.a(arrayList);
                cVar.a(0);
                return;
            }
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(r.a(listFileDic.length));
            for (String a2 : listFileDic) {
                arrayList2.add(r.a(a2));
            }
            cVar.a(arrayList2);
            cVar.a(0);
        }
    }

    public void h(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        byte[] a2 = e.a(j);
        if (a2 != null) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(a2);
            cVar.a(arrayList);
            cVar.a(0);
            return;
        }
        cVar.a(4);
    }

    public void i(Context context, c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        int h = cVar.h();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        boolean b2 = e.b(j, h);
        if (!b2) {
            e.a(j, h);
            b2 = e.a(new File(j), h);
        }
        if (b2) {
            cVar.a(0);
        } else {
            cVar.a(4);
        }
    }

    public void o(c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
        } else if (!new File(j).exists()) {
            cVar.a(0);
        } else {
            try {
                if (!b(new File(j))) {
                    cVar.a(4);
                } else {
                    cVar.a(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
                cVar.a(4);
            }
        }
    }

    public void a(BufferedInputStream bufferedInputStream, BufferedOutputStream bufferedOutputStream) {
        try {
            byte[] bArr = new byte[4];
            if (bufferedInputStream.read(bArr) < bArr.length) {
                l.a(bufferedOutputStream, r.a(10));
                return;
            }
            byte[] bArr2 = new byte[r.a(bArr)];
            if (bufferedInputStream.read(bArr2) < bArr.length) {
                l.a(bufferedOutputStream, r.a(10));
                return;
            }
            String b2 = r.b(bArr2);
            File file = new File(b2);
            boolean z = true;
            if (!file.exists()) {
                z = c(file);
            }
            if (!z) {
                l.a(bufferedOutputStream, r.a(3));
                return;
            }
            int a2 = l.a(bufferedInputStream, b2, bufferedOutputStream);
            try {
                l.a(bufferedOutputStream, r.a(a2));
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (a2 != 0) {
                file.delete();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            try {
                l.a(bufferedOutputStream, r.a(8));
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    public void b(BufferedInputStream bufferedInputStream, BufferedOutputStream bufferedOutputStream) {
        try {
            byte[] bArr = new byte[4];
            for (int i = 0; i < bArr.length; i++) {
                bArr[i] = (byte) bufferedInputStream.read();
            }
            byte[] bArr2 = new byte[r.a(bArr)];
            for (int i2 = 0; i2 < bArr2.length; i2++) {
                bArr2[i2] = (byte) bufferedInputStream.read();
            }
            l.a(bufferedOutputStream, r.a(l.b(bufferedOutputStream, r.b(bArr2))));
        } catch (Exception e) {
            Log.e("com.qq.connect.io", e.getMessage(), e);
        }
    }

    public void c(BufferedInputStream bufferedInputStream, BufferedOutputStream bufferedOutputStream) {
        try {
            byte[] bArr = new byte[4];
            for (int i = 0; i < bArr.length; i++) {
                bArr[i] = (byte) bufferedInputStream.read();
            }
            byte[] bArr2 = new byte[r.a(bArr)];
            for (int i2 = 0; i2 < bArr2.length; i2++) {
                bArr2[i2] = (byte) bufferedInputStream.read();
            }
            l.a(bufferedOutputStream, r.a(l.a(bufferedOutputStream, r.b(bArr2))));
        } catch (Exception e) {
            Log.e("com.qq.connect.io", e.getMessage(), e);
        }
    }

    public static void a(File file) {
        File[] listFiles;
        if (file != null && file.exists() && file.isDirectory() && (listFiles = file.listFiles()) != null) {
            for (File delete : listFiles) {
                delete.delete();
            }
        }
    }

    public static boolean b(File file) {
        if (file.isDirectory()) {
            String[] list = file.list();
            for (String file2 : list) {
                boolean b2 = b(new File(file, file2));
                if (!b2) {
                    return b2;
                }
            }
            try {
                file.delete();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        } else {
            try {
                boolean delete = file.delete();
                if (delete) {
                    return delete;
                }
                Log.e("com.qq.connect", "Read Only. Cannot be deleted");
                return delete;
            } catch (Exception e2) {
                e2.printStackTrace();
                return false;
            }
        }
    }

    public void a(File file, boolean z, FilenameFilter filenameFilter) {
        File[] listFiles;
        int i = 0;
        if (file.exists() && file.isDirectory()) {
            if (filenameFilter != null) {
                listFiles = file.listFiles(filenameFilter);
            } else {
                listFiles = file.listFiles();
            }
            if (this.c == null) {
                this.c = new Vector<>();
            }
            if (listFiles == null) {
                return;
            }
            if (!z) {
                while (i < listFiles.length) {
                    this.c.add(listFiles[i].getAbsolutePath());
                    i++;
                }
                return;
            }
            while (i < listFiles.length) {
                this.c.add(listFiles[i].getAbsolutePath());
                a(listFiles[i], z, filenameFilter);
                i++;
            }
        }
    }

    public void j(Context context, c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        String j = cVar.j();
        if (h < 0 || r.b(j)) {
            cVar.a(1);
            return;
        }
        Log.d("com.qq.connect", "mode:" + h);
        if (h == 755) {
            h = 775;
        }
        if (!h.c(j, h)) {
            cVar.a(4);
        } else {
            cVar.a(0);
        }
    }

    public void k(Context context, c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        String j = cVar.j();
        if (h < 0 || r.b(j)) {
            cVar.a(1);
        } else if (!h.b(j, h)) {
            cVar.a(4);
        } else {
            cVar.a(0);
        }
    }

    public void l(Context context, c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        String j = cVar.j();
        if (h < 0 || r.b(j)) {
            cVar.a(1);
        } else if (!h.d(j, h)) {
            cVar.a(4);
        } else {
            cVar.a(0);
        }
    }

    public void m(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        int lastIndexOf = j.lastIndexOf(File.separatorChar);
        if (lastIndexOf > 0) {
            if (lastIndexOf < j.length() - 1) {
                j = j.substring(lastIndexOf + 1);
            } else {
                j = "tmp.apk";
            }
        }
        Native nativeR = new Native();
        File file = new File("/data/data/" + AstApp.i().getPackageName() + File.separator + j);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            nativeR.setFilePermission(j, 511);
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(file.getAbsolutePath()));
        cVar.a(arrayList);
    }

    public void n(Context context, c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        File dir = context.getDir(j, h);
        if (dir == null) {
            cVar.a(8);
            return;
        }
        cVar.a(0);
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(dir.getAbsolutePath()));
        cVar.a(arrayList);
    }

    public void o(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
        } else {
            new Native().uncryptFile(j);
        }
    }

    public void p(Context context, c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        if (r.b(j) || r.b(j2)) {
            cVar.a(1);
        } else if (!ab.a(j, j2)) {
            cVar.a(8);
        }
    }

    public void q(Context context, c cVar) {
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        String j3 = cVar.j();
        if (r.b(j) || r.b(j2) || r.b(j3)) {
            cVar.a(1);
        } else if (!ab.a(j, j2, j3)) {
            cVar.a(8);
        }
    }

    public void r(Context context, c cVar) {
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        int h = cVar.h();
        if (r.b(j) || r.b(j2)) {
            cVar.a(1);
        } else if (h <= 0 || h + 3 > cVar.e()) {
            cVar.a(1);
        } else {
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < h; i++) {
                String j3 = cVar.j();
                if (!r.b(j3)) {
                    arrayList.add(j3);
                }
            }
            if (!ab.a(j, j2, arrayList)) {
                cVar.a(8);
            }
        }
    }

    public void s(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h <= 0 || h + 1 > cVar.e()) {
            cVar.a(1);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(h));
        for (int i = 0; i < h; i++) {
            String j = cVar.j();
            if (r.b(j)) {
                arrayList.add(r.a(-1));
            } else {
                arrayList.add(r.a(f(new File(j))));
            }
        }
        cVar.a(arrayList);
    }

    public static boolean a(File file, File file2) {
        if (!file.exists()) {
            return false;
        }
        try {
            return file.renameTo(file2);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean b(File file, File file2) {
        if (!file.exists() || file.isDirectory()) {
            Log.d("com.qq.connect", "src is not illegal");
            return false;
        }
        if (!file2.isFile()) {
            b(file2);
        }
        if (!file2.exists()) {
            file2.createNewFile();
        }
        if (!file2.exists()) {
            Log.d("com.qq.connect", "dst nonesists");
            return false;
        }
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            FileOutputStream fileOutputStream = null;
            try {
                fileOutputStream = new FileOutputStream(file2);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            if (fileOutputStream == null) {
                try {
                    fileInputStream.close();
                    return false;
                } catch (IOException e2) {
                    e2.printStackTrace();
                    return false;
                }
            } else {
                byte[] bArr = new byte[4096];
                int i = 1;
                while (i > 0) {
                    try {
                        i = fileInputStream.read(bArr);
                        if (i > 0) {
                            fileOutputStream.write(bArr, 0, i);
                            fileOutputStream.flush();
                        }
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        if (fileInputStream != null) {
                            try {
                                fileInputStream.close();
                            } catch (IOException e4) {
                                e4.printStackTrace();
                            }
                        }
                        if (fileOutputStream == null) {
                            return false;
                        }
                        try {
                            fileOutputStream.close();
                            return false;
                        } catch (IOException e5) {
                            e5.printStackTrace();
                            return false;
                        }
                    } catch (Throwable th) {
                        if (fileInputStream != null) {
                            try {
                                fileInputStream.close();
                            } catch (IOException e6) {
                                e6.printStackTrace();
                            }
                        }
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                            } catch (IOException e7) {
                                e7.printStackTrace();
                            }
                        }
                        throw th;
                    }
                }
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e8) {
                        e8.printStackTrace();
                    }
                }
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e9) {
                        e9.printStackTrace();
                    }
                }
                return true;
            }
        } catch (FileNotFoundException e10) {
            e10.printStackTrace();
            return false;
        }
    }

    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Type inference failed for: r4v0 */
    /* JADX WARN: Type inference failed for: r4v1, types: [java.io.FileInputStream] */
    /* JADX WARN: Type inference failed for: r4v2 */
    /* JADX WARN: Type inference failed for: r4v3, types: [java.io.FileInputStream] */
    /* JADX WARN: Type inference failed for: r4v4 */
    /* JADX WARN: Type inference failed for: r4v5 */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0065 A[SYNTHETIC, Splitter:B:34:0x0065] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x006a A[SYNTHETIC, Splitter:B:37:0x006a] */
    public static boolean c(java.io.File r6, java.io.File r7) {
        /*
            r3 = 0
            r1 = 1
            r0 = 0
            boolean r2 = r6.exists()
            if (r2 == 0) goto L_0x000f
            boolean r2 = r6.isDirectory()
            if (r2 == 0) goto L_0x0017
        L_0x000f:
            java.lang.String r1 = "com.qq.connect"
            java.lang.String r2 = "src is not illegal"
            android.util.Log.d(r1, r2)
        L_0x0016:
            return r0
        L_0x0017:
            boolean r2 = r7.exists()
            if (r2 == 0) goto L_0x0026
            boolean r2 = r7.isFile()
            if (r2 != 0) goto L_0x0026
            b(r7)
        L_0x0026:
            boolean r2 = r7.exists()
            if (r2 != 0) goto L_0x002f
            r7.createNewFile()
        L_0x002f:
            boolean r2 = r7.exists()
            if (r2 != 0) goto L_0x003d
            java.lang.String r1 = "com.qq.connect"
            java.lang.String r2 = "dst nonesists"
            android.util.Log.d(r1, r2)
            goto L_0x0016
        L_0x003d:
            r0 = 4096(0x1000, float:5.74E-42)
            byte[] r5 = new byte[r0]
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0096, all -> 0x008e }
            r4.<init>(r6)     // Catch:{ Exception -> 0x0096, all -> 0x008e }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0099, all -> 0x0091 }
            r2.<init>(r7)     // Catch:{ Exception -> 0x0099, all -> 0x0091 }
            r0 = r1
        L_0x004c:
            if (r0 <= 0) goto L_0x006e
            int r0 = r4.read(r5)     // Catch:{ Exception -> 0x005c, all -> 0x0093 }
            if (r0 <= 0) goto L_0x004c
            r3 = 0
            r2.write(r5, r3, r0)     // Catch:{ Exception -> 0x005c, all -> 0x0093 }
            r2.flush()     // Catch:{ Exception -> 0x005c, all -> 0x0093 }
            goto L_0x004c
        L_0x005c:
            r0 = move-exception
            r1 = r2
            r3 = r4
        L_0x005f:
            throw r0     // Catch:{ all -> 0x0060 }
        L_0x0060:
            r0 = move-exception
            r4 = r3
            r3 = r1
        L_0x0063:
            if (r4 == 0) goto L_0x0068
            r4.close()     // Catch:{ Exception -> 0x007a }
        L_0x0068:
            if (r3 == 0) goto L_0x006d
            r3.close()     // Catch:{ Exception -> 0x007f }
        L_0x006d:
            throw r0
        L_0x006e:
            if (r4 == 0) goto L_0x0073
            r4.close()     // Catch:{ Exception -> 0x0084 }
        L_0x0073:
            if (r2 == 0) goto L_0x0078
            r2.close()     // Catch:{ Exception -> 0x0089 }
        L_0x0078:
            r0 = r1
            goto L_0x0016
        L_0x007a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0068
        L_0x007f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006d
        L_0x0084:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0073
        L_0x0089:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0078
        L_0x008e:
            r0 = move-exception
            r4 = r3
            goto L_0x0063
        L_0x0091:
            r0 = move-exception
            goto L_0x0063
        L_0x0093:
            r0 = move-exception
            r3 = r2
            goto L_0x0063
        L_0x0096:
            r0 = move-exception
            r1 = r3
            goto L_0x005f
        L_0x0099:
            r0 = move-exception
            r1 = r3
            r3 = r4
            goto L_0x005f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.n.c(java.io.File, java.io.File):boolean");
    }

    public static boolean d(File file, File file2) {
        boolean z;
        boolean z2;
        int read;
        if (!file.exists()) {
            return false;
        }
        if (file.isFile()) {
            if (!file2.exists()) {
                z2 = c(file2);
            } else {
                z2 = true;
            }
            if (!z2) {
                return z2;
            }
            FileInputStream fileInputStream = new FileInputStream(file);
            FileOutputStream fileOutputStream = new FileOutputStream(file2);
            byte[] bArr = new byte[4096];
            while (fileInputStream.available() > 0) {
                if (fileInputStream.available() > bArr.length) {
                    read = fileInputStream.read(bArr, 0, bArr.length);
                } else {
                    read = fileInputStream.read(bArr, 0, fileInputStream.available());
                }
                if (read > 0) {
                    fileOutputStream.write(bArr, 0, read);
                    fileOutputStream.flush();
                }
            }
            fileInputStream.close();
            fileOutputStream.close();
        } else {
            if (!file2.exists()) {
                z = d(file2);
            } else {
                z = true;
            }
            if (!z) {
                return z;
            }
            File[] listFiles = file.listFiles();
            if (e(file, file2) && listFiles != null) {
                for (int i = 0; i < listFiles.length; i++) {
                    if (listFiles[i].getAbsolutePath().equals(file2.getAbsolutePath())) {
                        File file3 = listFiles[0];
                        listFiles[0] = listFiles[i];
                        listFiles[i] = file3;
                    } else if (e(listFiles[i], file2)) {
                        File file4 = listFiles[0];
                        listFiles[0] = listFiles[i];
                        listFiles[i] = file4;
                    }
                }
            }
            if (listFiles != null) {
                for (int i2 = 0; i2 < listFiles.length; i2++) {
                    if (!listFiles[i2].getAbsolutePath().equals(file2.getAbsolutePath())) {
                        boolean d = d(listFiles[i2], new File(file2 + File.separator + listFiles[i2].getName()));
                        if (!d) {
                            return d;
                        }
                    }
                }
            }
        }
        return true;
    }

    public static boolean c(File file) {
        if (file.exists()) {
            return true;
        }
        File parentFile = file.getParentFile();
        if (parentFile.exists()) {
            return file.createNewFile();
        }
        boolean mkdirs = parentFile.mkdirs();
        if (mkdirs) {
            return file.createNewFile();
        }
        return mkdirs;
    }

    public static boolean d(File file) {
        if (file.exists()) {
            return true;
        }
        return file.mkdirs();
    }

    public static boolean e(File file, File file2) {
        if (!file.exists() || !file2.exists() || !file2.getAbsolutePath().startsWith(file.getAbsolutePath())) {
            return false;
        }
        return true;
    }

    public static boolean e(File file) {
        return file.exists();
    }

    public static String c() {
        long j;
        String str;
        String str2;
        long j2 = 0;
        Native nativeR = new Native();
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        if (externalStorageDirectory != null) {
            str2 = externalStorageDirectory.getAbsolutePath();
            str = nativeR.getRealLinkFile(str2);
            j = nativeR.getInode(str2);
            if (str != null) {
                j2 = nativeR.getInode(str);
            }
        } else {
            j = 0;
            str = null;
            str2 = null;
        }
        for (int i = 0; i < f345a.length; i++) {
            File file = new File(f345a[i]);
            if (file.exists() && file.isDirectory() && ((str2 == null || !file.getAbsolutePath().equals(str2)) && (str == null || !str.equals(file.getAbsolutePath())))) {
                String realLinkFile = nativeR.getRealLinkFile(f345a[i]);
                Log.d("com.qq.connect", "getLinkedFile:" + f345a[i] + "-->" + realLinkFile);
                long inode = nativeR.getInode(f345a[i]);
                if (inode == j) {
                    if (!a(str2, f345a[i])) {
                        inode = -1;
                    } else {
                        continue;
                    }
                }
                if (realLinkFile == null) {
                    if (!(nativeR.findInMount(f345a[i]) < 0 || inode == j || inode == j2)) {
                        return f345a[i];
                    }
                } else if ((nativeR.getInode(realLinkFile) != j2 || !a(str2, f345a[i])) && ((str2 == null || !realLinkFile.equals(str2)) && ((str == null || !str.equals(realLinkFile)) && ((realLinkFile == null || !realLinkFile.endsWith("legacy") || !a(str2, realLinkFile)) && nativeR.findInMount(realLinkFile) >= 0)))) {
                    return realLinkFile;
                }
            }
        }
        return null;
    }

    public static long a(String str) {
        StatFs statFs = new StatFs(str);
        return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
    }

    public static boolean a(String str, String str2) {
        if (str == null || str2 == null) {
            return false;
        }
        StatFs statFs = new StatFs(str);
        StatFs statFs2 = new StatFs(str2);
        long availableBlocks = (long) statFs.getAvailableBlocks();
        long blockCount = (long) statFs.getBlockCount();
        long blockSize = (long) statFs2.getBlockSize();
        long availableBlocks2 = (long) statFs2.getAvailableBlocks();
        long blockCount2 = (long) statFs2.getBlockCount();
        boolean z = true;
        if (((long) statFs.getBlockSize()) != blockSize) {
            z = false;
        }
        if (availableBlocks != availableBlocks2) {
            z = false;
        }
        if (blockCount != blockCount2) {
            return false;
        }
        return z;
    }

    public static boolean a(File file, byte[] bArr) {
        boolean z = false;
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            try {
                fileOutputStream.write(bArr);
                fileOutputStream.flush();
                z = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                fileOutputStream.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        } catch (FileNotFoundException e3) {
            e3.printStackTrace();
        }
        return z;
    }

    public static boolean b(File file, byte[] bArr) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
            try {
                l.a(bufferedOutputStream, bArr);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                bufferedOutputStream.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            try {
                fileOutputStream.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            return true;
        } catch (FileNotFoundException e4) {
            e4.printStackTrace();
            return false;
        }
    }

    public static boolean a(InputStream inputStream, File file, byte[] bArr) {
        boolean z = false;
        if (!(inputStream == null || file == null || bArr == null)) {
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                while (true) {
                    try {
                        int read = inputStream.read(bArr, 0, bArr.length);
                        if (read == -1) {
                            break;
                        }
                        fileOutputStream.write(bArr, 0, read);
                        fileOutputStream.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                z = true;
                try {
                    fileOutputStream.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            } catch (FileNotFoundException e3) {
                e3.printStackTrace();
            }
        }
        return z;
    }

    public static int a(File file, byte[] bArr, int i) {
        FileInputStream fileInputStream;
        int i2 = -1;
        if (!(file == null || bArr == null || i <= 0)) {
            try {
                fileInputStream = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                fileInputStream = null;
            }
            if (fileInputStream != null) {
                try {
                    i2 = fileInputStream.read(bArr, 0, i);
                } catch (IOException e2) {
                    e2.printStackTrace();
                    i2 = 0;
                }
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                }
            }
        }
        return i2;
    }
}
