package com.qq.provider.extern;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.qq.ndk.Native;

/* compiled from: ProGuard */
public class ExternDBHelper extends SQLiteOpenHelper {
    public static final String EXTERN_TABLE = "ex_s";
    public static final int version = 3;

    public ExternDBHelper(Context context) {
        super(context, "extern.db", (SQLiteDatabase.CursorFactory) null, 3);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS ex_s");
        onCreate(sQLiteDatabase);
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS ex_s");
    }

    private void createEXT(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE ex_s (_id integer primary key autoincrement,audio_time  long,image_time  long,sync_time   long,last_connect long);");
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        String path = sQLiteDatabase.getPath();
        Native nativeR = new Native();
        if (nativeR.getFilePermission(path) % 10 < 4) {
            Log.d("com.qq.connect", "ndk setDB FilePermission" + path + nativeR.setFilePermission(path, 438));
        }
        createEXT(sQLiteDatabase);
    }

    public synchronized void close() {
        super.close();
    }
}
