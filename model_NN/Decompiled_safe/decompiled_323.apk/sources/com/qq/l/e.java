package com.qq.l;

import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private static e f304a = null;
    private List<a> b;

    private e() {
        this.b = null;
        this.b = new ArrayList();
        this.b.add(p.m());
        this.b.add(q.m());
    }

    public static synchronized e a() {
        e eVar;
        synchronized (e.class) {
            if (f304a == null) {
                f304a = new e();
            }
            eVar = f304a;
        }
        return eVar;
    }

    public void b() {
        for (a l : this.b) {
            l.l();
        }
    }
}
