package com.qq.util;

import android.text.TextUtils;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: ProGuard */
public class p {
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0014  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(android.content.Context r6, int r7) {
        /*
            r1 = 0
            java.lang.String r0 = "activity"
            java.lang.Object r0 = r6.getSystemService(r0)
            if (r0 == 0) goto L_0x0012
            android.app.ActivityManager r0 = (android.app.ActivityManager) r0     // Catch:{ Throwable -> 0x000e }
        L_0x000b:
            if (r0 != 0) goto L_0x0014
        L_0x000d:
            return r1
        L_0x000e:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0012:
            r0 = r1
            goto L_0x000b
        L_0x0014:
            java.util.List r3 = r0.getRunningAppProcesses()
            if (r3 == 0) goto L_0x003c
            int r0 = r3.size()
            if (r0 <= 0) goto L_0x003c
            int r4 = r3.size()
            r0 = 0
            r2 = r0
        L_0x0026:
            if (r2 >= r4) goto L_0x003c
            java.lang.Object r0 = r3.get(r2)
            android.app.ActivityManager$RunningAppProcessInfo r0 = (android.app.ActivityManager.RunningAppProcessInfo) r0
            if (r0 == 0) goto L_0x0038
            int r5 = r0.pid
            if (r5 != r7) goto L_0x0038
            java.lang.String r0 = r0.processName
        L_0x0036:
            r1 = r0
            goto L_0x000d
        L_0x0038:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0026
        L_0x003c:
            r0 = r1
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.util.p.a(android.content.Context, int):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0049 A[SYNTHETIC, Splitter:B:21:0x0049] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(int r4) {
        /*
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "/proc/"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r2 = "/cmdline"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0034, all -> 0x0044 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0034, all -> 0x0044 }
            int r1 = r2.available()     // Catch:{ Exception -> 0x0056 }
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x0056 }
            r2.read(r1)     // Catch:{ Exception -> 0x0056 }
            java.lang.String r3 = "UTF-8"
            java.lang.String r0 = org.apache.http.util.EncodingUtils.getString(r1, r3)     // Catch:{ Exception -> 0x0056 }
            if (r2 == 0) goto L_0x0033
            r2.close()     // Catch:{ IOException -> 0x0052 }
        L_0x0033:
            return r0
        L_0x0034:
            r1 = move-exception
            r2 = r0
        L_0x0036:
            r1.printStackTrace()     // Catch:{ all -> 0x0054 }
            if (r2 == 0) goto L_0x0033
            r2.close()     // Catch:{ IOException -> 0x003f }
            goto L_0x0033
        L_0x003f:
            r1 = move-exception
        L_0x0040:
            r1.printStackTrace()
            goto L_0x0033
        L_0x0044:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0047:
            if (r2 == 0) goto L_0x004c
            r2.close()     // Catch:{ IOException -> 0x004d }
        L_0x004c:
            throw r0
        L_0x004d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004c
        L_0x0052:
            r1 = move-exception
            goto L_0x0040
        L_0x0054:
            r0 = move-exception
            goto L_0x0047
        L_0x0056:
            r1 = move-exception
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.util.p.a(int):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x002c A[SYNTHETIC, Splitter:B:6:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0031  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int b(int r6) {
        /*
            r4 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "/proc/"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r6)
            java.lang.String r1 = "/oom_adj"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r0.toString()
            r3 = 0
            r0 = 100
            r2 = 128(0x80, float:1.794E-43)
            byte[] r5 = new byte[r2]
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x003f }
            r2.<init>(r1)     // Catch:{ Exception -> 0x003f }
            int r1 = r2.read(r5)     // Catch:{ Exception -> 0x004b }
        L_0x002a:
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ IOException -> 0x0046 }
        L_0x002f:
            if (r1 <= 0) goto L_0x003e
            java.lang.String r0 = new java.lang.String
            r0.<init>(r5, r4, r1)
            java.lang.String r0 = r0.trim()
            int r0 = java.lang.Integer.parseInt(r0)
        L_0x003e:
            return r0
        L_0x003f:
            r1 = move-exception
            r2 = r3
        L_0x0041:
            r1.printStackTrace()
            r1 = r4
            goto L_0x002a
        L_0x0046:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x002f
        L_0x004b:
            r1 = move-exception
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.util.p.b(int):int");
    }

    public static List<Integer> a(String str) {
        InputStream inputStream;
        ByteArrayOutputStream byteArrayOutputStream;
        ByteArrayOutputStream byteArrayOutputStream2;
        InputStream inputStream2;
        BufferedReader bufferedReader = null;
        ArrayList arrayList = new ArrayList();
        if (!TextUtils.isEmpty(str)) {
            try {
                ProcessBuilder processBuilder = new ProcessBuilder("ps");
                processBuilder.redirectErrorStream(false);
                Process start = processBuilder.start();
                byteArrayOutputStream2 = new ByteArrayOutputStream();
                try {
                    inputStream2 = start.getInputStream();
                } catch (Exception e) {
                    inputStream = null;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    com.tencent.assistant.utils.p.a(bufferedReader);
                    com.tencent.assistant.utils.p.a(inputStream);
                    com.tencent.assistant.utils.p.a(byteArrayOutputStream);
                    return arrayList;
                } catch (Throwable th) {
                    th = th;
                    inputStream2 = null;
                    com.tencent.assistant.utils.p.a(bufferedReader);
                    com.tencent.assistant.utils.p.a(inputStream2);
                    com.tencent.assistant.utils.p.a(byteArrayOutputStream2);
                    throw th;
                }
                try {
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = inputStream2.read(bArr);
                        if (read == -1) {
                            break;
                        }
                        byteArrayOutputStream2.write(bArr, 0, read);
                    }
                    BufferedReader bufferedReader2 = new BufferedReader(new StringReader(byteArrayOutputStream2.toString()));
                    while (true) {
                        try {
                            String readLine = bufferedReader2.readLine();
                            if (readLine == null) {
                                break;
                            }
                            int a2 = a(readLine, str);
                            if (a2 > 0) {
                                arrayList.add(Integer.valueOf(a2));
                            }
                        } catch (Exception e2) {
                            bufferedReader = bufferedReader2;
                            inputStream = inputStream2;
                            byteArrayOutputStream = byteArrayOutputStream2;
                            com.tencent.assistant.utils.p.a(bufferedReader);
                            com.tencent.assistant.utils.p.a(inputStream);
                            com.tencent.assistant.utils.p.a(byteArrayOutputStream);
                            return arrayList;
                        } catch (Throwable th2) {
                            th = th2;
                            bufferedReader = bufferedReader2;
                            com.tencent.assistant.utils.p.a(bufferedReader);
                            com.tencent.assistant.utils.p.a(inputStream2);
                            com.tencent.assistant.utils.p.a(byteArrayOutputStream2);
                            throw th;
                        }
                    }
                    com.tencent.assistant.utils.p.a(bufferedReader2);
                    com.tencent.assistant.utils.p.a(inputStream2);
                    com.tencent.assistant.utils.p.a(byteArrayOutputStream2);
                } catch (Exception e3) {
                    inputStream = inputStream2;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    com.tencent.assistant.utils.p.a(bufferedReader);
                    com.tencent.assistant.utils.p.a(inputStream);
                    com.tencent.assistant.utils.p.a(byteArrayOutputStream);
                    return arrayList;
                } catch (Throwable th3) {
                    th = th3;
                    com.tencent.assistant.utils.p.a(bufferedReader);
                    com.tencent.assistant.utils.p.a(inputStream2);
                    com.tencent.assistant.utils.p.a(byteArrayOutputStream2);
                    throw th;
                }
            } catch (Exception e4) {
                inputStream = null;
                byteArrayOutputStream = null;
                com.tencent.assistant.utils.p.a(bufferedReader);
                com.tencent.assistant.utils.p.a(inputStream);
                com.tencent.assistant.utils.p.a(byteArrayOutputStream);
                return arrayList;
            } catch (Throwable th4) {
                th = th4;
                inputStream2 = null;
                byteArrayOutputStream2 = null;
                com.tencent.assistant.utils.p.a(bufferedReader);
                com.tencent.assistant.utils.p.a(inputStream2);
                com.tencent.assistant.utils.p.a(byteArrayOutputStream2);
                throw th;
            }
        }
        return arrayList;
    }

    private static int a(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        Matcher matcher = Pattern.compile("\\s+").matcher(str);
        if (matcher.find()) {
            str = matcher.replaceAll(",");
        }
        String[] split = str.split(",");
        if (split == null || split.length == 0 || split.length <= 8) {
            return 0;
        }
        String str3 = split[8];
        if (TextUtils.isEmpty(str3) || !str3.endsWith(str2)) {
            return 0;
        }
        try {
            return Integer.parseInt(split[1]);
        } catch (Exception e) {
            return 0;
        }
    }

    public static String c(int i) {
        ByteArrayOutputStream byteArrayOutputStream;
        FileInputStream fileInputStream;
        Throwable th;
        String str = null;
        try {
            fileInputStream = new FileInputStream("/proc/" + i + "/cmdline");
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
            } catch (Exception e) {
                e = e;
                byteArrayOutputStream = null;
                try {
                    e.printStackTrace();
                    com.tencent.assistant.utils.p.a(byteArrayOutputStream);
                    com.tencent.assistant.utils.p.a(fileInputStream);
                    return str;
                } catch (Throwable th2) {
                    th = th2;
                    com.tencent.assistant.utils.p.a(byteArrayOutputStream);
                    com.tencent.assistant.utils.p.a(fileInputStream);
                    throw th;
                }
            } catch (Throwable th3) {
                byteArrayOutputStream = null;
                th = th3;
                com.tencent.assistant.utils.p.a(byteArrayOutputStream);
                com.tencent.assistant.utils.p.a(fileInputStream);
                throw th;
            }
            try {
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = fileInputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                str = byteArrayOutputStream.toString();
                com.tencent.assistant.utils.p.a(byteArrayOutputStream);
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                com.tencent.assistant.utils.p.a(byteArrayOutputStream);
                com.tencent.assistant.utils.p.a(fileInputStream);
                return str;
            }
        } catch (Exception e3) {
            e = e3;
            byteArrayOutputStream = null;
            fileInputStream = null;
            e.printStackTrace();
            com.tencent.assistant.utils.p.a(byteArrayOutputStream);
            com.tencent.assistant.utils.p.a(fileInputStream);
            return str;
        } catch (Throwable th4) {
            byteArrayOutputStream = null;
            fileInputStream = null;
            th = th4;
            com.tencent.assistant.utils.p.a(byteArrayOutputStream);
            com.tencent.assistant.utils.p.a(fileInputStream);
            throw th;
        }
        com.tencent.assistant.utils.p.a(fileInputStream);
        return str;
    }
}
