package defpackage;

import android.util.Log;

/* renamed from: aw  reason: default package */
public final class aw {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f108a = true;
    private static av b = new bb();

    public static void a(String str, Object obj) {
        if (obj == null) {
            Log.e(str, "log message is null");
        } else {
            b.a(str, obj.toString());
        }
    }
}
