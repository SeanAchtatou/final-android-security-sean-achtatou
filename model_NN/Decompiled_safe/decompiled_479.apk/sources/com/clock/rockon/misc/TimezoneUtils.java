package com.clock.rockon.misc;

import android.text.format.Time;
import java.util.Locale;
import java.util.TimeZone;

public class TimezoneUtils {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static String parseCity(String s) {
        String[] raw = s.split("/");
        return raw[raw.length - 1].replace('_', ' ');
    }

    public static String getContinent(String s) {
        return s.split("/")[0];
    }

    public static String getTime(String s) {
        Time time = new Time(s);
        time.setToNow();
        return time.format("%H:%M");
    }

    public static String getTimeDifference(String s) {
        Time local = new Time();
        Time foreign = new Time(s);
        local.setToNow();
        foreign.setToNow();
        int foreignComplete = (foreign.year * 31449600) + (foreign.yearDay * 86400) + (foreign.hour * 3600);
        int localComplete = (local.year * 31449600) + (local.yearDay * 86400) + (local.hour * 3600);
        int difference = (foreignComplete - localComplete) / 3600;
        if (difference > 12 || difference < -12) {
            difference = (localComplete - foreignComplete) / 3600;
        }
        String res = Integer.toString(difference);
        if (difference >= 0) {
            return "+" + res;
        }
        return res;
    }

    public static String getTzString(String s) {
        String res = TimeZone.getTimeZone(s).getDisplayName(false, 0, new Locale("ENGLISH", "UK"));
        int dim = res.length();
        if (res.substring(dim - 2, dim).equals("00")) {
            res = res.substring(0, dim - 3);
        }
        return "(" + res + ")";
    }

    public static String getDistance(String s) {
        return String.valueOf(getTimeDifference(s)) + "\n" + getTzString(s);
    }

    public static String convertTo12h(String h24) {
        String[] time = h24.split(":");
        String hour = time[0];
        String mins = time[1];
        String suffix = "AM";
        int h = Integer.parseInt(hour);
        if (h > 12) {
            h -= 12;
            suffix = "PM";
        }
        return String.valueOf(Integer.toString(h)) + ":" + mins + " " + suffix;
    }
}
