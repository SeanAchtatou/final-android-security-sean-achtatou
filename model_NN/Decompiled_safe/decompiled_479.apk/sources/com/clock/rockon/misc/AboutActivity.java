package com.clock.rockon.misc;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.clock.rockon.R;
import java.io.IOException;

public class AboutActivity extends PreferenceActivity {
    private static final String ASSET_CHANGELOG = "CHANGELOG";
    private static final String ASSET_EULA = "EULA";
    private static final String ASSET_FAQ = "FAQ";
    private static final String ASSET_PRIVACY = "PRIVACY";
    private static final String ASSET_README = "README";
    private static final String ASSET_TODO = "TODO";
    private static final String EXTRA_AUTHOR_EMAIL = "EXTRA_AUTHOR_EMAIL";
    private static final String EXTRA_AUTHOR_MARKET_NAME = "EXTRA_AUTHOR_MARKET_NAME";
    private static final String EXTRA_AUTHOR_NAME = "EXTRA_AUTHOR_NAME";
    private static final String EXTRA_AUTHOR_SITE = "EXTRA_AUTHOR_SITE";
    private static final String EXTRA_DONATE_URL = "EXTRA_DONATE_URL";
    private static final String EXTRA_REPORT_BUG_URL = "EXTRA_REPORT_BUG_URL";
    private static final String EXTRA_SHOW_IN_MARKET = "EXTRA_SHOW_IN_MARKET";
    private static final String PREFS_ABOUT_ACTIVITY = "PREFS_ABOUT_ACTIVITY";
    private static final String PREF_EULA_KEY = "PREF_EULA_KEY";
    private static final String PREF_LAST_VERSION_KEY = "PREF_LAST_VERSION_KEY";

    public interface OnStartupChangeLogListener {
        void onVersionChanged(String str, String str2);
    }

    public interface OnStartupEulaListener {
        void onEulaAction(boolean z);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(1);
        ListView listView = new ListView(this);
        listView.setId(16908298);
        listView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        linearLayout.addView(listView);
        TextView textView = new TextView(this);
        textView.setText("Powered by Android Tools");
        textView.setGravity(1);
        textView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AboutActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://android.marcoduff.com/")));
            }
        });
        linearLayout.addView(textView);
        setContentView(linearLayout);
        Preference preferenceVersion = getPreferenceVersion();
        Preference preferenceAuthorName = getPreferenceFromStringExtra(EXTRA_AUTHOR_NAME, R.string.aboutactivity_author);
        Preference preferenceAuthorSite = getPreferenceFromUrlExtra(EXTRA_AUTHOR_SITE, R.string.aboutactivity_authorsite, R.string.aboutactivity_authorsite_summary);
        Preference preferenceAuthorMail = getPreferenceFromMailExtra(EXTRA_AUTHOR_EMAIL, R.string.aboutactivity_authoremail, R.string.aboutactivity_authoremail_summary);
        Preference preferenceReadme = getPreferenceFromAssetExtra(ASSET_README, R.string.aboutactivity_readme, R.string.aboutactivity_readme_summary);
        Preference preferenceFaq = getPreferenceFromAssetExtra(ASSET_FAQ, R.string.aboutactivity_faq, R.string.aboutactivity_faq_summary);
        Preference preferenceChangeLog = getPreferenceFromAssetExtra(ASSET_CHANGELOG, R.string.aboutactivity_changelog, R.string.aboutactivity_changelog_summary);
        Preference preferenceLicense = getPreferenceFromAssetExtra(ASSET_EULA, R.string.aboutactivity_license, R.string.aboutactivity_license_summary);
        Preference preferencePrivacy = getPreferenceFromAssetExtra(ASSET_PRIVACY, R.string.aboutactivity_privacy, R.string.aboutactivity_privacy_summary);
        Preference preferenceTodo = getPreferenceFromAssetExtra(ASSET_TODO, R.string.aboutactivity_todo, R.string.aboutactivity_todo_summary);
        Preference preferenceDonate = getPreferenceFromUrlExtra(EXTRA_DONATE_URL, R.string.aboutactivity_donate, R.string.aboutactivity_donate_summary);
        Preference preferenceMarket = getPreferenceMarket(EXTRA_SHOW_IN_MARKET, R.string.aboutactivity_market, R.string.aboutactivity_market_summary);
        Preference preferenceAuthorMarket = getPreferenceAuthorMarket(EXTRA_AUTHOR_MARKET_NAME, R.string.aboutactivity_authormarket, R.string.aboutactivity_authormarket_summary);
        Preference preferenceReportBug = getPreferenceFromUrlExtra(EXTRA_REPORT_BUG_URL, R.string.aboutactivity_reportbug, R.string.aboutactivity_reportbug_summary);
        PreferenceScreen preferenceScreen = getPreferenceManager().createPreferenceScreen(this);
        addPreferenceCategory(preferenceScreen, R.string.aboutactivity_info_category, preferenceVersion, preferenceAuthorName, preferenceAuthorSite, preferenceAuthorMail);
        addPreferenceCategory(preferenceScreen, R.string.aboutactivity_app_category, preferenceReadme, preferenceFaq, preferenceChangeLog, preferenceLicense, preferencePrivacy, preferenceTodo);
        addPreferenceCategory(preferenceScreen, R.string.aboutactivity_miscellaneous_category, preferenceDonate, preferenceMarket, preferenceAuthorMarket, preferenceReportBug);
        setPreferenceScreen(preferenceScreen);
    }

    public static boolean showStartupEula(Context context) {
        return showStartupEula(context, false);
    }

    public static boolean showStartupEula(final Context context, boolean forceShow) {
        final SharedPreferences preferences = context.getSharedPreferences(PREFS_ABOUT_ACTIVITY, 0);
        if (preferences.getBoolean(PREF_EULA_KEY, false) && !forceShow) {
            return true;
        }
        View dialogView = createDialogView(context, ASSET_EULA);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle((int) R.string.aboutactivity_license);
        builder.setView(dialogView);
        builder.setPositiveButton((int) R.string.aboutactivity_license_accept, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                preferences.edit().putBoolean(AboutActivity.PREF_EULA_KEY, true).commit();
                if (context instanceof OnStartupEulaListener) {
                    ((OnStartupEulaListener) context).onEulaAction(true);
                }
            }
        });
        builder.setNegativeButton((int) R.string.aboutactivity_license_refuse, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                preferences.edit().putBoolean(AboutActivity.PREF_EULA_KEY, false).commit();
                if (context instanceof OnStartupEulaListener) {
                    ((OnStartupEulaListener) context).onEulaAction(false);
                }
                if (context instanceof Activity) {
                    ((Activity) context).finish();
                }
            }
        });
        builder.create().show();
        return false;
    }

    public static boolean showStartupChangeLog(Context context) {
        return showStartupChangeLog(context, false);
    }

    public static boolean showStartupChangeLog(Context context, boolean forceShow) {
        String currentVersion = getCurrentVersion(context);
        SharedPreferences preferences = context.getSharedPreferences(PREFS_ABOUT_ACTIVITY, 0);
        String lastVersion = preferences.getString(PREF_LAST_VERSION_KEY, currentVersion);
        preferences.edit().putString(PREF_LAST_VERSION_KEY, currentVersion).commit();
        if (lastVersion.equals(currentVersion) && !forceShow) {
            return true;
        }
        View dialogView = createDialogView(context, ASSET_CHANGELOG);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle((int) R.string.aboutactivity_changelog);
        builder.setView(dialogView);
        builder.setNeutralButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
        if (context instanceof OnStartupChangeLogListener) {
            ((OnStartupChangeLogListener) context).onVersionChanged(lastVersion, currentVersion);
        }
        return false;
    }

    public static void showReadmeDialog(Context context) {
        showGenericDialog(context, R.string.aboutactivity_readme, ASSET_README);
    }

    public static void showFaqDialog(Context context) {
        showGenericDialog(context, R.string.aboutactivity_faq, ASSET_FAQ);
    }

    public static void showChangeLogDialog(Context context) {
        showGenericDialog(context, R.string.aboutactivity_changelog, ASSET_CHANGELOG);
    }

    public static void showEulaDialog(Context context) {
        showGenericDialog(context, R.string.aboutactivity_license, ASSET_EULA);
    }

    public static void showPrivacyDialog(Context context) {
        showGenericDialog(context, R.string.aboutactivity_privacy, ASSET_PRIVACY);
    }

    public static void showTodoDialog(Context context) {
        showGenericDialog(context, R.string.aboutactivity_todo, ASSET_TODO);
    }

    public static void showGenericDialog(Context context, int titleResId, String filename) {
        View dialogView = createDialogView(context, filename);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titleResId);
        builder.setView(dialogView);
        builder.create().show();
    }

    public static Intent getAboutActivityIntent(Context context, String authorName, String authorSite, String authorMail, String donateUrl, boolean showInAndroidMarketUrl, String authorAndroidMarketName, String reportBugUrl) {
        Intent intent = new Intent(context, AboutActivity.class);
        if (authorName != null) {
            intent.putExtra(EXTRA_AUTHOR_NAME, authorName);
        }
        if (authorSite != null) {
            intent.putExtra(EXTRA_AUTHOR_SITE, authorSite);
        }
        if (authorMail != null) {
            intent.putExtra(EXTRA_AUTHOR_EMAIL, authorMail);
        }
        if (donateUrl != null) {
            intent.putExtra(EXTRA_DONATE_URL, donateUrl);
        }
        if (showInAndroidMarketUrl) {
            intent.putExtra(EXTRA_SHOW_IN_MARKET, "true");
        }
        if (authorAndroidMarketName != null) {
            intent.putExtra(EXTRA_AUTHOR_MARKET_NAME, authorAndroidMarketName);
        }
        if (reportBugUrl != null) {
            intent.putExtra(EXTRA_REPORT_BUG_URL, reportBugUrl);
        }
        return intent;
    }

    private static View createDialogView(final Context context, String filename) {
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(1);
        WebView webView = new WebView(context);
        webView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        webView.loadUrl(String.format("file:///android_asset/%1$s", filename));
        layout.addView(webView);
        TextView poweredView = new TextView(context);
        poweredView.setText("Powered by Android Tools");
        poweredView.setGravity(1);
        poweredView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://android.marcoduff.com/")));
            }
        });
        layout.addView(poweredView);
        return layout;
    }

    private boolean addPreferenceCategory(PreferenceScreen preferenceScreen, int titleResId, Preference... preferences) {
        boolean addPreference = false;
        for (Preference preference : preferences) {
            if (preference != null) {
                addPreference = true;
            }
        }
        if (!addPreference) {
            return false;
        }
        PreferenceCategory preferenceCategory = new PreferenceCategory(this);
        preferenceCategory.setTitle(titleResId);
        preferenceScreen.addPreference(preferenceCategory);
        for (Preference preference2 : preferences) {
            if (preference2 != null) {
                preferenceCategory.addPreference(preference2);
            }
        }
        return true;
    }

    private Preference getPreferenceVersion() {
        String currentVersion = getCurrentVersion(this);
        Preference versionPreference = new Preference(this);
        versionPreference.setTitle((int) R.string.app_name);
        versionPreference.setSummary(String.format(getString(R.string.aboutactivity_version), currentVersion));
        return versionPreference;
    }

    private static String getCurrentVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "";
        }
    }

    private Preference getPreferenceFromStringExtra(String extra, int titleResId) {
        String extraValue = getIntent().getStringExtra(extra);
        if (extraValue == null) {
            return null;
        }
        Preference preference = new Preference(this);
        preference.setTitle(titleResId);
        preference.setSummary(extraValue);
        return preference;
    }

    private Preference getPreferenceFromUrlExtra(String extra, int titleResId, int summaryResId) {
        String url = getIntent().getStringExtra(extra);
        if (url == null) {
            return null;
        }
        Preference preference = new Preference(this);
        preference.setTitle(titleResId);
        preference.setSummary(summaryResId);
        preference.setIntent(new Intent("android.intent.action.VIEW", Uri.parse(url)));
        return preference;
    }

    private Preference getPreferenceMarket(String extra, int titleResId, int summaryResId) {
        String flag = getIntent().getStringExtra(extra);
        if (flag == null || !flag.equalsIgnoreCase("TRUE")) {
            return null;
        }
        Preference preference = new Preference(this);
        preference.setTitle(titleResId);
        preference.setSummary(summaryResId);
        preference.setIntent(new Intent("android.intent.action.VIEW", Uri.parse(String.format("market://search?q=pname:com.clock.rockon", getPackageName()))));
        return preference;
    }

    private Preference getPreferenceAuthorMarket(String extra, int titleResId, int summaryResId) {
        String authorMarket = getIntent().getStringExtra(extra);
        if (authorMarket == null) {
            return null;
        }
        Preference preference = new Preference(this);
        preference.setTitle(titleResId);
        preference.setSummary(summaryResId);
        preference.setIntent(new Intent("android.intent.action.VIEW", Uri.parse(String.format("market://search?q=pub:Android Tools", authorMarket))));
        return preference;
    }

    private Preference getPreferenceFromMailExtra(String extra, int titleResId, int summaryResId) {
        String email = getIntent().getStringExtra(extra);
        if (email == null) {
            return null;
        }
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("plain/text");
        intent.putExtra("android.intent.extra.EMAIL", new String[]{email});
        intent.putExtra("android.intent.extra.SUBJECT", String.format("New mail from %1$s", getString(R.string.app_name)));
        Preference preference = new Preference(this);
        preference.setTitle(titleResId);
        preference.setSummary(summaryResId);
        preference.setIntent(intent);
        return preference;
    }

    private Preference getPreferenceFromAssetExtra(String fileName, int titleResId, int summaryResId) {
        if (!assetExists(fileName)) {
            return null;
        }
        DialogPreference dialogPreference = new AssetDialogPreference(this, titleResId, fileName);
        dialogPreference.setSummary(summaryResId);
        dialogPreference.setNegativeButtonText((CharSequence) null);
        return dialogPreference;
    }

    private boolean assetExists(String fileName) {
        try {
            getAssets().open(fileName);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private class AssetDialogPreference extends DialogPreference {
        private String fileName;

        public AssetDialogPreference(Context context, int titleResId, String fileName2) {
            super(context, null);
            setTitle(titleResId);
            setDialogTitle(titleResId);
            this.fileName = fileName2;
        }

        /* access modifiers changed from: protected */
        public void onPrepareDialogBuilder(AlertDialog.Builder builder) {
            super.onPrepareDialogBuilder(builder);
            WebView webView = new WebView(getContext());
            webView.loadUrl(String.format("file:///android_asset/%1$s", this.fileName));
            builder.setView(webView);
        }
    }
}
