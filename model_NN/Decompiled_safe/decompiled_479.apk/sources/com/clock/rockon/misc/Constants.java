package com.clock.rockon.misc;

public class Constants {
    public static final int ABOUT_ID = 0;
    public static final int DELETE_ID = 2;
    public static final int DOWN_ID = 1;
    public static final int MAPS_ID = 3;
    public static final int SETTINGS_ID = 1;
    public static final int UP_ID = 0;
}
