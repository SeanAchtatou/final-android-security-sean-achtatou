package com.clock.rockon.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ClocksDatabase {
    private static final String CLOCKS_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS clocks (_id integer primary key autoincrement, timezone text not null);";
    private static final String DB_NAME = "clocksdb";
    private static final int DB_VERSION = 1;
    public static final int READ = 0;
    public static final int WRITE = 1;
    Context mContext;
    SQLiteDatabase mDb;
    DbHelper mDbHelper;

    public static class ClocksMetaData {
        public static final String CLOCKS_TABLE = "clocks";
        public static final String CLOCK_TIMEZONE_KEY = "timezone";
        public static final String ID = "_id";
    }

    public ClocksDatabase(Context ctx) {
        this.mContext = ctx;
        this.mDbHelper = new DbHelper(ctx, DB_NAME, null, 1);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void open(int mode) {
        switch (mode) {
            case 0:
                this.mDb = this.mDbHelper.getReadableDatabase();
                break;
            case 1:
                break;
            default:
                return;
        }
        this.mDb = this.mDbHelper.getWritableDatabase();
    }

    public void close() {
        this.mDb.close();
    }

    public void insertClock(String timezone) {
        ContentValues cv = new ContentValues();
        cv.put(ClocksMetaData.CLOCK_TIMEZONE_KEY, timezone);
        this.mDb.insert(ClocksMetaData.CLOCKS_TABLE, null, cv);
    }

    public int updateClock(long rowId, String timezone) {
        ContentValues cv = new ContentValues();
        cv.put(ClocksMetaData.CLOCK_TIMEZONE_KEY, timezone);
        return this.mDb.update(ClocksMetaData.CLOCKS_TABLE, cv, "_id=" + rowId, null);
    }

    public void delete(long rowId) {
        this.mDb.delete(ClocksMetaData.CLOCKS_TABLE, "_id=" + rowId, null);
    }

    public Cursor fetchClocks() {
        return this.mDb.query(ClocksMetaData.CLOCKS_TABLE, null, null, null, null, null, null);
    }

    private class DbHelper extends SQLiteOpenHelper {
        public DbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        public void onCreate(SQLiteDatabase _db) {
            _db.execSQL(ClocksDatabase.CLOCKS_TABLE_CREATE);
        }

        public void onUpgrade(SQLiteDatabase _db, int oldVersion, int newVersion) {
        }
    }
}
