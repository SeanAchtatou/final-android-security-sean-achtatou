package com.clock.rockon.gui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.clock.rockon.R;
import com.clock.rockon.misc.TimezoneUtils;

public class Details extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details);
        String pkg = getPackageName();
        String timezone = getIntent().getStringExtra(String.valueOf(pkg) + ".myString");
        TextView detailsCityName = (TextView) findViewById(R.id.details_city_name);
        TextView detailsContinentName = (TextView) findViewById(R.id.details_continent_name);
        TextView detailsHour = (TextView) findViewById(R.id.details_hour);
        TextView detailsInfo = (TextView) findViewById(R.id.details_info);
        boolean h12clock = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(getString(R.string.KEY_12H), false);
        String time = TimezoneUtils.getTime(timezone);
        if (h12clock) {
            time = TimezoneUtils.convertTo12h(time);
        }
        detailsCityName.setText(TimezoneUtils.parseCity(timezone));
        detailsContinentName.setText(TimezoneUtils.getContinent(timezone));
        detailsHour.setText(time);
        detailsInfo.setText(String.valueOf(TimezoneUtils.getTzString(timezone)) + "\n" + TimezoneUtils.getTimeDifference(timezone) + " " + getString(R.string.timezone_difference));
        if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean(getString(R.string.KEY_STANDARD_FONTS), false)) {
            Typeface textTypeface = Typeface.createFromAsset(getAssets(), "jmacscrl.ttf");
            Typeface clockTypeface = Typeface.createFromAsset(getAssets(), "bravenewera.ttf");
            detailsCityName.setTypeface(textTypeface);
            detailsCityName.refreshDrawableState();
            detailsContinentName.setTypeface(textTypeface);
            detailsContinentName.refreshDrawableState();
            detailsHour.setTypeface(clockTypeface);
            detailsHour.refreshDrawableState();
            detailsInfo.setTypeface(textTypeface);
            detailsInfo.refreshDrawableState();
        }
        final TextView textView = detailsCityName;
        ((Button) findViewById(R.id.map_btn)).setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
             arg types: [int, int]
             candidates:
              ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
              ClspMth{java.lang.String.replace(char, char):java.lang.String} */
            public void onClick(View v) {
                Details.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("geo:0,0?q=" + textView.getText().toString().replace(' ', '+'))));
            }
        });
    }
}
