package com.clock.rockon.gui;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.clock.rockon.R;
import com.clock.rockon.data.ClocksDatabase;
import java.util.TimeZone;

public class TimeZonePicker extends Activity {
    ArrayAdapter<String> adapter;
    ListView alltzLV;
    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            TimeZonePicker.this.adapter.getFilter().filter(s);
        }
    };
    EditText tzFilter;
    String[] tzIds;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.time_zone_picker);
        this.tzFilter = (EditText) findViewById(R.id.tzFilter);
        this.alltzLV = (ListView) findViewById(R.id.alltzLV);
        this.tzIds = TimeZone.getAvailableIDs();
        int dim = this.tzIds.length;
        for (int i = 0; i < dim; i++) {
            this.tzIds[i] = this.tzIds[i].replaceAll("/", " > ").replace('_', ' ');
        }
        this.adapter = new ArrayAdapter<>(this, 17367043, this.tzIds);
        this.alltzLV.setAdapter((ListAdapter) this.adapter);
        this.alltzLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
             arg types: [int, int]
             candidates:
              ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
              ClspMth{java.lang.String.replace(char, char):java.lang.String} */
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                TimeZonePicker.this.saveTimeZone(((TextView) v).getText().toString().replaceAll(" > ", "/").replace(' ', '_'));
                Toast.makeText(TimeZonePicker.this, TimeZonePicker.this.getString(R.string.new_tz_added), 0).show();
                TimeZonePicker.this.finish();
            }
        });
        this.tzFilter.addTextChangedListener(this.filterTextWatcher);
    }

    /* access modifiers changed from: package-private */
    public void saveTimeZone(String timezone) {
        ClocksDatabase db = new ClocksDatabase(getApplicationContext());
        db.open(1);
        db.insertClock(timezone);
        db.close();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.tzFilter.removeTextChangedListener(this.filterTextWatcher);
    }
}
