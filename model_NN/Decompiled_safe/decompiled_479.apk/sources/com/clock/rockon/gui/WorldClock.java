package com.clock.rockon.gui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.admob.android.ads.AdView;
import com.clock.rockon.R;
import com.clock.rockon.data.ClocksDatabase;
import com.clock.rockon.misc.AboutActivity;
import com.clock.rockon.misc.TimezoneUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WorldClock extends Activity implements View.OnClickListener {
    List<String> cities = new ArrayList();
    ListView clocksList;
    ClocksDatabase db;
    List<Long> dbIds = new ArrayList();
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            WorldClock.this.updateList();
        }
    };
    private Runnable mTask = new Runnable() {
        public void run() {
            while (true) {
                try {
                    WorldClock.this.handler.sendEmptyMessage(0);
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    return;
                } catch (Exception e2) {
                }
            }
        }
    };
    List<String> rawTimezonesList = new ArrayList();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        AboutActivity.showStartupChangeLog(this);
        AboutActivity.showStartupEula(this);
        this.clocksList = (ListView) findViewById(R.id.clocksList);
        this.db = new ClocksDatabase(getApplicationContext());
        registerForContextMenu(this.clocksList);
        ((Button) findViewById(R.id.add)).setOnClickListener(this);
        new Thread(null, this.mTask, "NotifyingService").start();
        if (getPackageName().equals("com.commabit.wockd")) {
            ((AdView) findViewById(R.id.ad)).setVisibility(8);
        }
        firstLaunch();
    }

    public void firstLaunch() {
        SharedPreferences pref = getSharedPreferences("firstlaunch", 0);
        if (pref.getBoolean("launch", true)) {
            pref.edit().putBoolean("launch", false).commit();
            this.db.open(1);
            this.db.insertClock("Europe/Rome");
            this.db.insertClock("America/New_York");
            this.db.insertClock("Asia/Hong_Kong");
            this.db.close();
            updateList();
        }
    }

    /* access modifiers changed from: private */
    public void updateList() {
        this.db.open(0);
        Cursor c = this.db.fetchClocks();
        startManagingCursor(c);
        ArrayList<HashMap<String, Object>> data = new ArrayList<>();
        boolean h12time = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(getString(R.string.KEY_12H), false);
        int rows = c.getCount();
        c.moveToFirst();
        for (int i = 0; i < rows; i++) {
            c.moveToPosition(i);
            String rawTimezone = c.getString(c.getColumnIndex(ClocksDatabase.ClocksMetaData.CLOCK_TIMEZONE_KEY));
            this.rawTimezonesList.add(i, rawTimezone);
            this.dbIds.add(i, Long.valueOf(c.getLong(c.getColumnIndex(ClocksDatabase.ClocksMetaData.ID))));
            String city = TimezoneUtils.parseCity(rawTimezone);
            String state = TimezoneUtils.getContinent(rawTimezone);
            String time = TimezoneUtils.getTime(rawTimezone);
            if (h12time) {
                time = TimezoneUtils.convertTo12h(time);
            }
            this.cities.add(i, city);
            HashMap<String, Object> matchMap = new HashMap<>();
            matchMap.put("city", city);
            matchMap.put("state", state);
            matchMap.put("display", time);
            matchMap.put("distance", TimezoneUtils.getDistance(rawTimezone));
            data.add(matchMap);
        }
        SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), data, R.layout.clock_item, new String[]{"city", "state", "display", "distance"}, new int[]{R.id.clock_city, R.id.clock_state, R.id.clock_display, R.id.distance});
        this.db.close();
        adapter.setViewBinder(new SimpleAdapter.ViewBinder() {
            public boolean setViewValue(View view, Object data, String textRepresentation) {
                if (PreferenceManager.getDefaultSharedPreferences(WorldClock.this).getBoolean(WorldClock.this.getString(R.string.KEY_STANDARD_FONTS), false)) {
                    switch (view.getId()) {
                        case R.id.clock_city:
                            ((TextView) view).setTextSize(20.0f);
                            return false;
                        case R.id.clock_state:
                        default:
                            return false;
                        case R.id.clock_display:
                            ((TextView) view).setTextSize(18.0f);
                            return false;
                    }
                } else {
                    Typeface textTypeface = Typeface.createFromAsset(WorldClock.this.getAssets(), "jmacscrl.ttf");
                    Typeface clockTypeface = Typeface.createFromAsset(WorldClock.this.getAssets(), "bravenewera.ttf");
                    switch (view.getId()) {
                        case R.id.clock_city:
                            ((TextView) view).setTypeface(textTypeface);
                            ((TextView) view).refreshDrawableState();
                            return false;
                        case R.id.clock_state:
                        default:
                            return false;
                        case R.id.clock_display:
                            ((TextView) view).setTypeface(clockTypeface);
                            ((TextView) view).refreshDrawableState();
                            return false;
                    }
                }
            }
        });
        this.clocksList.setAdapter((ListAdapter) adapter);
        this.clocksList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                String pkg = WorldClock.this.getPackageName();
                Intent intent = new Intent(WorldClock.this.getApplicationContext(), Details.class);
                intent.putExtra(String.valueOf(pkg) + ".myString", WorldClock.this.rawTimezonesList.get(position));
                WorldClock.this.startActivity(intent);
            }
        });
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, 0, 0, getString(R.string.up));
        menu.add(0, 1, 0, getString(R.string.down));
        menu.add(0, 2, 0, getString(R.string.delete));
        menu.add(0, 3, 0, getString(R.string.maps_view));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case 0:
                switchClocks(info.position, info.position - 1);
                break;
            case 1:
                switchClocks(info.position, info.position + 1);
                break;
            case 2:
                this.db.open(1);
                this.db.delete(this.dbIds.get(info.position).longValue());
                this.db.close();
                updateList();
                return true;
            case 3:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("geo:0,0?q=" + this.cities.get(info.position).replace(' ', '+'))));
                return true;
        }
        return super.onContextItemSelected(item);
    }

    private void switchClocks(int pos1, int pos2) {
        this.db.open(1);
        Cursor c = this.db.fetchClocks();
        startManagingCursor(c);
        c.moveToFirst();
        if (pos2 != -1 && pos2 <= c.getCount() - 1) {
            c.moveToPosition(pos1);
            String timezone1 = c.getString(c.getColumnIndex(ClocksDatabase.ClocksMetaData.CLOCK_TIMEZONE_KEY));
            c.moveToPosition(pos2);
            this.db.updateClock(this.dbIds.get(pos1).longValue(), c.getString(c.getColumnIndex(ClocksDatabase.ClocksMetaData.CLOCK_TIMEZONE_KEY)));
            this.db.updateClock(this.dbIds.get(pos2).longValue(), timezone1);
            this.db.close();
            updateList();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        updateList();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, getString(R.string.about)).setIcon(17301569);
        menu.add(0, 1, 0, getString(R.string.settings)).setIcon(17301577);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                startActivity(AboutActivity.getAboutActivityIntent(this, "Android Tools", "market://search?q=pname:com.clock.rockon", "androidgame4u@gmail.com", null, true, "CommaBit", null));
                break;
            case 1:
                startActivity(new Intent(this, Settings.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.up_btn:
            default:
                return;
            case R.id.add:
                startActivity(new Intent(this, TimeZonePicker.class));
                return;
        }
    }
}
