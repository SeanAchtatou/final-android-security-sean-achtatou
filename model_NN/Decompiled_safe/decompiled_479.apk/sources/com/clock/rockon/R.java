package com.clock.rockon;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int item_bg_indent = 2130837505;
        public static final int item_bg_normal = 2130837506;
    }

    public static final class id {
        public static final int ad = 2131099659;
        public static final int add = 2131099662;
        public static final int alltzLV = 2131099664;
        public static final int buttonBar = 2131099661;
        public static final int clock_city = 2131099648;
        public static final int clock_display = 2131099650;
        public static final int clock_state = 2131099649;
        public static final int clocksList = 2131099660;
        public static final int details_city_name = 2131099654;
        public static final int details_continent_name = 2131099655;
        public static final int details_hour = 2131099656;
        public static final int details_info = 2131099657;
        public static final int distance = 2131099653;
        public static final int down_btn = 2131099652;
        public static final int map_btn = 2131099658;
        public static final int tzFilter = 2131099663;
        public static final int up_btn = 2131099651;
    }

    public static final class layout {
        public static final int clock_item = 2130903040;
        public static final int details = 2130903041;
        public static final int main = 2130903042;
        public static final int time_zone_picker = 2130903043;
    }

    public static final class string {
        public static final int KEY_12H = 2131034163;
        public static final int KEY_STANDARD_FONTS = 2131034164;
        public static final int about = 2131034143;
        public static final int about_activity = 2131034144;
        public static final int aboutactivity_app_category = 2131034113;
        public static final int aboutactivity_author = 2131034116;
        public static final int aboutactivity_authoremail = 2131034119;
        public static final int aboutactivity_authoremail_summary = 2131034120;
        public static final int aboutactivity_authormarket = 2131034139;
        public static final int aboutactivity_authormarket_summary = 2131034140;
        public static final int aboutactivity_authorsite = 2131034117;
        public static final int aboutactivity_authorsite_summary = 2131034118;
        public static final int aboutactivity_changelog = 2131034125;
        public static final int aboutactivity_changelog_summary = 2131034126;
        public static final int aboutactivity_donate = 2131034135;
        public static final int aboutactivity_donate_summary = 2131034136;
        public static final int aboutactivity_faq = 2131034123;
        public static final int aboutactivity_faq_summary = 2131034124;
        public static final int aboutactivity_info_category = 2131034112;
        public static final int aboutactivity_license = 2131034127;
        public static final int aboutactivity_license_accept = 2131034129;
        public static final int aboutactivity_license_refuse = 2131034130;
        public static final int aboutactivity_license_summary = 2131034128;
        public static final int aboutactivity_market = 2131034137;
        public static final int aboutactivity_market_summary = 2131034138;
        public static final int aboutactivity_miscellaneous_category = 2131034114;
        public static final int aboutactivity_privacy = 2131034131;
        public static final int aboutactivity_privacy_summary = 2131034132;
        public static final int aboutactivity_readme = 2131034121;
        public static final int aboutactivity_readme_summary = 2131034122;
        public static final int aboutactivity_reportbug = 2131034141;
        public static final int aboutactivity_reportbug_summary = 2131034142;
        public static final int aboutactivity_todo = 2131034133;
        public static final int aboutactivity_todo_summary = 2131034134;
        public static final int aboutactivity_version = 2131034115;
        public static final int active_12h = 2131034165;
        public static final int add = 2131034145;
        public static final int app_name = 2131034146;
        public static final int delete = 2131034147;
        public static final int details = 2131034149;
        public static final int down = 2131034148;
        public static final int maps_view = 2131034150;
        public static final int new_tz_added = 2131034151;
        public static final int off_12h = 2131034154;
        public static final int off_standard_fonts = 2131034155;
        public static final int on_12h = 2131034152;
        public static final int on_standard_fonts = 2131034153;
        public static final int settings = 2131034156;
        public static final int timezone_difference = 2131034157;
        public static final int timezone_picker = 2131034158;
        public static final int title_12h = 2131034159;
        public static final int title_standard_fonts = 2131034160;
        public static final int tz_et_hint = 2131034161;
        public static final int up = 2131034162;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }

    public static final class xml {
        public static final int settings = 2130968576;
    }
}
