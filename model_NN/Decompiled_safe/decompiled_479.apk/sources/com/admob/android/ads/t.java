package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import org.json.JSONObject;
import org.json.JSONTokener;

/* compiled from: DeveloperNotice */
final class t {
    private static boolean a = false;

    t() {
    }

    public static void a(Context context) {
        byte[] d;
        String string;
        if (!a) {
            a = true;
            if (AdManager.getUserId(context) == null) {
                try {
                    String a2 = u.a(context, null, null, 0);
                    r a3 = e.a("http://api.admob.com/v1/pubcode/android_sdk_emulator_notice" + "?" + a2, "developer_message", AdManager.getUserId(context));
                    if (a3.a() && (d = a3.d()) != null && (string = new JSONObject(new JSONTokener(new String(d))).getString("data")) != null && !string.equals("")) {
                        Log.w(AdManager.LOG, string);
                    }
                } catch (Exception e) {
                    if (Log.isLoggable(AdManager.LOG, 2)) {
                        Log.v(AdManager.LOG, "Unhandled exception retrieving developer message.", e);
                    }
                }
            }
        }
    }
}
