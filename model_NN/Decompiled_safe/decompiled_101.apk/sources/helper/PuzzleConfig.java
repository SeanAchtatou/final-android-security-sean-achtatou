package helper;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Environment;
import android.text.TextUtils;
import main.Config;
import main.PuzzleTypeConfig;

public final class PuzzleConfig {
    private static final String MARKET_DISPLAY_NAME_AMAZON_APPSTORE = "Amazon Appstore";
    private static final String MARKET_DISPLAY_NAME_ANDROID_MARKET = "Android Market";
    private static final String MARKET_DISPLAY_NAME_SAM = "SAM";
    public static final String MARKET_FILE_NAME_SD = "web_data.dat";
    private static final String MARKET_FILE_NAME_WEB_AMAZON_APPSTORE = "web_data_amazon_appstore.txt";
    private static final String MARKET_FILE_NAME_WEB_ANDROID_MARKET = "web_data_android_market.txt";
    private static final String MARKET_FILE_NAME_WEB_SAM = "web_data_sam.txt";
    public static final int MARKET_ID_AMAZON_APPSTORE = 2;
    public static final int MARKET_ID_ANDROID_MARKET = 0;
    public static final int MARKET_ID_SAM = 1;
    public static String ROOT_DIR_HIDDEN = (Environment.getExternalStorageDirectory() + "/." + PuzzleTypeConfig.FOLDER_NAME + "/");
    public static String WEB_BASE_URL = ("http://dl.dropbox.com/u/17815975/" + PuzzleTypeConfig.FOLDER_NAME + "/");

    public static String getMarketDisplayName(int iMarketId) {
        switch (iMarketId) {
            case 0:
                return MARKET_DISPLAY_NAME_ANDROID_MARKET;
            case 1:
                return MARKET_DISPLAY_NAME_SAM;
            case 2:
                return MARKET_DISPLAY_NAME_AMAZON_APPSTORE;
            default:
                throw new RuntimeException("Invalid market id");
        }
    }

    public static String getMarketFileNameWeb(int iMarketId) {
        switch (iMarketId) {
            case 0:
                return MARKET_FILE_NAME_WEB_ANDROID_MARKET;
            case 1:
                return MARKET_FILE_NAME_WEB_SAM;
            case 2:
                return MARKET_FILE_NAME_WEB_AMAZON_APPSTORE;
            default:
                throw new RuntimeException("Invalid market id");
        }
    }

    public static synchronized String getDeveloperName(Context hPuzzleContext) {
        String string;
        synchronized (PuzzleConfig.class) {
            string = getString(hPuzzleContext, "dev_name", true);
        }
        return string;
    }

    public static synchronized String getPuzzleName(Context hPuzzleContext) {
        String string;
        synchronized (PuzzleConfig.class) {
            string = getString(hPuzzleContext, "app_name", true);
        }
        return string;
    }

    public static synchronized String getInfo(Context hPuzzleContext) {
        String string;
        synchronized (PuzzleConfig.class) {
            string = getString(hPuzzleContext, "info", false);
        }
        return string;
    }

    public static synchronized String getImageInfo(Context hPuzzleContext, int iImageIndex) {
        String string;
        synchronized (PuzzleConfig.class) {
            string = getString(hPuzzleContext, "image_info_" + iImageIndex, false);
        }
        return string;
    }

    public static synchronized int getGridSizeMin(Context hPuzzleContext) {
        int intValue;
        synchronized (PuzzleConfig.class) {
            intValue = getInt(hPuzzleContext, "grid_size_min", true).intValue();
        }
        return intValue;
    }

    public static synchronized int getGridSizeMax(Context hPuzzleContext) {
        int intValue;
        synchronized (PuzzleConfig.class) {
            intValue = getInt(hPuzzleContext, "grid_size_max", true).intValue();
        }
        return intValue;
    }

    public static synchronized Integer getMarketId(Context hPuzzleContext) {
        Integer num;
        synchronized (PuzzleConfig.class) {
            num = getInt(hPuzzleContext, "market_id", false);
        }
        return num;
    }

    public static synchronized String getAdWhirlKey(Context hPuzzleContext) {
        String string;
        synchronized (PuzzleConfig.class) {
            String sAdWhirlKey = new Config().getAdWhirlKey();
            if (!TextUtils.isEmpty(sAdWhirlKey)) {
                string = sAdWhirlKey;
            } else {
                string = getString(hPuzzleContext, "ad_whirl_key", false);
            }
        }
        return string;
    }

    public static synchronized int getVersionCode(Context hPuzzleContext) {
        int i;
        synchronized (PuzzleConfig.class) {
            try {
                i = hPuzzleContext.getPackageManager().getPackageInfo(hPuzzleContext.getPackageName(), 0).versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                i = -1;
            }
        }
        return i;
    }

    private static synchronized String getString(Context hPuzzleContext, String sStringResName, boolean bThrowException) {
        String string;
        synchronized (PuzzleConfig.class) {
            int iStringResId = hPuzzleContext.getResources().getIdentifier(sStringResName, "string", hPuzzleContext.getPackageName());
            if (iStringResId != 0) {
                string = hPuzzleContext.getString(iStringResId);
            } else if (bThrowException) {
                throw new RuntimeException("Invalid string res id");
            } else {
                string = null;
            }
        }
        return string;
    }

    private static synchronized Integer getInt(Context hPuzzleContext, String sIntResName, boolean bThrowException) {
        Integer valueOf;
        synchronized (PuzzleConfig.class) {
            Resources hRes = hPuzzleContext.getResources();
            int iIntResId = hRes.getIdentifier(sIntResName, "integer", hPuzzleContext.getPackageName());
            if (iIntResId != 0) {
                valueOf = Integer.valueOf(hRes.getInteger(iIntResId));
            } else if (bThrowException) {
                throw new RuntimeException("Invalid int res id");
            } else {
                valueOf = null;
            }
        }
        return valueOf;
    }
}
