package helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import data.PuzzleHelper;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import main.PuzzleTypeConfig;

public final class ImageSaver {
    private static final String IMAGE_EXT = ".jpg";

    public static synchronized String save(Context hContext, String sPuzzleId, int iImageIndex) {
        Exception e;
        String str;
        synchronized (ImageSaver.class) {
            if (!Global.isSDCardAvailable()) {
                str = null;
            } else {
                Bitmap hImage = BitmapHelper.createBitmapFromUri(hContext, PuzzleHelper.getImageUri(sPuzzleId, iImageIndex));
                if (hImage == null) {
                    throw new RuntimeException("Invalid image");
                }
                File hFileImage = new File(getImageDir(sPuzzleId), String.valueOf(iImageIndex) + IMAGE_EXT);
                BufferedOutputStream hBuffOut = null;
                try {
                    hFileImage.getParentFile().mkdirs();
                    hFileImage.createNewFile();
                    BufferedOutputStream hBuffOut2 = new BufferedOutputStream(new FileOutputStream(hFileImage), 8192);
                    try {
                        if (hImage.compress(Bitmap.CompressFormat.JPEG, 100, hBuffOut2)) {
                            str = hFileImage.getAbsolutePath();
                            Global.closeOutputStream(hBuffOut2);
                        } else {
                            Global.closeOutputStream(hBuffOut2);
                            str = null;
                        }
                    } catch (Exception e2) {
                        e = e2;
                        hBuffOut = hBuffOut2;
                        try {
                            throw new RuntimeException(hFileImage.getAbsolutePath(), e);
                        } catch (Throwable th) {
                            th = th;
                            Global.closeOutputStream(hBuffOut);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        hBuffOut = hBuffOut2;
                        Global.closeOutputStream(hBuffOut);
                        throw th;
                    }
                } catch (Exception e3) {
                    e = e3;
                    throw new RuntimeException(hFileImage.getAbsolutePath(), e);
                }
            }
        }
        return str;
    }

    private static synchronized File getImageDir(String sPuzzleId) {
        File hFileDir;
        synchronized (ImageSaver.class) {
            hFileDir = new File(Environment.getExternalStorageDirectory() + "/" + PuzzleTypeConfig.FOLDER_NAME + "/" + sPuzzleId);
            hFileDir.mkdirs();
        }
        return hFileDir;
    }
}
