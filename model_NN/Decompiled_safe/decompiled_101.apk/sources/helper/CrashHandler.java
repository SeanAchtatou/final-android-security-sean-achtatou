package helper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;
import java.lang.Thread;
import res.Res;

public final class CrashHandler {
    private static final String PREF_KEY_CRASH_REPORT = "PREF_KEY_CRASH_REPORT";
    private static final String PREF_NAME = "crash_report";
    /* access modifiers changed from: private */
    public final Context m_hContext;
    private final Thread.UncaughtExceptionHandler m_hExceptionHandler = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread hThread, Throwable hThrowable) {
            StringBuilder sbCrashLog = new StringBuilder();
            sbCrashLog.append(String.valueOf(hThrowable.toString()) + "\n");
            for (StackTraceElement hElementLoop : hThrowable.getStackTrace()) {
                sbCrashLog.append("\n").append(hElementLoop.toString());
            }
            Throwable hThrowableCause = hThrowable.getCause();
            if (hThrowableCause != null) {
                sbCrashLog.append("\n\nCaused by: " + hThrowableCause.toString() + "\n");
                for (StackTraceElement hElementLoop2 : hThrowableCause.getStackTrace()) {
                    sbCrashLog.append("\n").append(hElementLoop2.toString());
                }
            }
            CrashHandler.this.m_hContext.getSharedPreferences(CrashHandler.PREF_NAME, 0).edit().putString(CrashHandler.PREF_KEY_CRASH_REPORT, sbCrashLog.toString()).commit();
            hThrowable.printStackTrace();
            System.exit(-1);
        }
    };
    /* access modifiers changed from: private */
    public ILogCollectListener m_hListener;

    public interface ILogCollectListener {
        void onLogCollectFinished();
    }

    public CrashHandler(Context hContext) {
        this.m_hContext = hContext;
        init();
    }

    public CrashHandler(Context hContext, ILogCollectListener hListener) {
        this.m_hContext = hContext;
        this.m_hListener = hListener;
        init();
    }

    public CrashHandler(Context hContext, String sCrashLog) {
        this.m_hContext = hContext;
        if (sCrashLog != null) {
            handleCrash(sCrashLog);
        }
    }

    private void init() {
        Thread.setDefaultUncaughtExceptionHandler(this.m_hExceptionHandler);
        SharedPreferences hPref = this.m_hContext.getSharedPreferences(PREF_NAME, 0);
        String sCrashLog = hPref.getString(PREF_KEY_CRASH_REPORT, null);
        hPref.edit().putString(PREF_KEY_CRASH_REPORT, null).commit();
        handleCrash(sCrashLog);
    }

    private void handleCrash(final String sCrashLog) {
        if (sCrashLog != null) {
            final String sAppName = this.m_hContext.getString(Res.string(this.m_hContext, "app_name"));
            new AlertDialog.Builder(this.m_hContext).setTitle(sAppName).setIcon(17301659).setMessage("The app crashed. Please send the crashreport to crashreport@simboly.com.\nYou will have an opportunity to review and modify the data being sent.").setNegativeButton(17039360, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog2, int whichButton) {
                    if (CrashHandler.this.m_hListener != null) {
                        CrashHandler.this.m_hListener.onLogCollectFinished();
                    }
                }
            }).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog2, int whichButton) {
                    if (CrashHandler.this.m_hListener != null) {
                        CrashHandler.this.m_hListener.onLogCollectFinished();
                    }
                    PackageInfo hInfo = null;
                    try {
                        hInfo = CrashHandler.this.m_hContext.getPackageManager().getPackageInfo(CrashHandler.this.m_hContext.getPackageName(), 0);
                    } catch (PackageManager.NameNotFoundException e) {
                        PackageManager.NameNotFoundException e2 = e;
                        if (Constants.debug()) {
                            e2.printStackTrace();
                        }
                    }
                    StringBuilder sbSubject = new StringBuilder("Crash Report: ");
                    sbSubject.append("[").append(sAppName).append(" - ").append(Constants.LIB_VERSION).append(" - ").append(Build.MODEL).append("]");
                    StringBuilder sbText = new StringBuilder();
                    sbText.append("Application: ").append(sAppName).append("\n");
                    if (hInfo != null) {
                        sbText.append("Package name: ").append(hInfo.packageName).append("\n");
                        sbText.append("Version code: ").append(hInfo.versionCode).append("\n");
                        sbText.append("Version name: ").append(hInfo.versionName).append("\n");
                    }
                    Display hDisplay = ((WindowManager) CrashHandler.this.m_hContext.getSystemService("window")).getDefaultDisplay();
                    sbText.append("Model: ").append(Build.MODEL).append("\n");
                    sbText.append("Version: ").append(Build.VERSION.RELEASE).append("\n");
                    sbText.append("Build Fingerprint: ").append(Build.FINGERPRINT).append("\n");
                    sbText.append("Display: ").append(Build.DISPLAY).append("\n");
                    sbText.append("Orientation: ").append(hDisplay.getOrientation()).append("\n");
                    sbText.append("Width: ").append(hDisplay.getWidth()).append("\n");
                    sbText.append("Height: ").append(hDisplay.getHeight()).append("\n").append("\n");
                    sbText.append(sCrashLog);
                    Intent hIntentEmail = new Intent("android.intent.action.SEND");
                    hIntentEmail.addFlags(268435456);
                    hIntentEmail.putExtra("android.intent.extra.SUBJECT", sbSubject.toString());
                    hIntentEmail.putExtra("android.intent.extra.TEXT", sbText.toString());
                    hIntentEmail.putExtra("android.intent.extra.EMAIL", new String[]{Constants.EMAIL_CRASH});
                    hIntentEmail.setType("message/rfc822");
                    CrashHandler.this.m_hContext.startActivity(Intent.createChooser(hIntentEmail, "Send by ..."));
                }
            }).show();
        } else if (this.m_hListener != null) {
            this.m_hListener.onLogCollectFinished();
        }
    }
}
