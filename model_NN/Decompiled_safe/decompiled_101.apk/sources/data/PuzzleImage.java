package data;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import helper.BitmapHelper;
import helper.PuzzleConfig;

public final class PuzzleImage {
    private boolean m_bIsNextUnsolved = false;
    private boolean m_bSolved = false;
    private Bitmap m_hImage;
    private Bitmap m_hThumbnail;
    private int m_iBestMoves = 0;
    private final int m_iIndex;
    private long m_lBestTimeInMs = 0;
    private String m_sBestMovesName;
    private String m_sBestTimeInMsName;
    private String m_sInfo;
    private final String m_sPuzzleId;

    public PuzzleImage(String sPuzzleId, int iIndex) {
        this.m_sPuzzleId = sPuzzleId;
        this.m_iIndex = iIndex;
    }

    public int getIndex() {
        return this.m_iIndex;
    }

    public void setSolved(boolean bSolved) {
        this.m_bSolved = bSolved;
    }

    public boolean isSolved() {
        return this.m_bSolved;
    }

    public boolean isPlayable() {
        return this.m_bSolved || this.m_bIsNextUnsolved;
    }

    public void setIsNextUnsolved(boolean bIsNextUnsolved) {
        this.m_bIsNextUnsolved = bIsNextUnsolved;
    }

    public boolean isNextUnsolved() {
        return this.m_bIsNextUnsolved;
    }

    public void setBestTimeInMs(long lBestTimeInMs) {
        this.m_lBestTimeInMs = lBestTimeInMs;
    }

    public long getBestTimeInMs() {
        return this.m_lBestTimeInMs;
    }

    public void setBestTimeInMsName(String sBestTimeInMsName) {
        this.m_sBestTimeInMsName = sBestTimeInMsName;
    }

    public String getBestTimeInMsName() {
        return this.m_sBestTimeInMsName;
    }

    public void setBestMoves(int iBestMoves) {
        this.m_iBestMoves = iBestMoves;
    }

    public int getBestMoves() {
        return this.m_iBestMoves;
    }

    public void setBestMovesName(String sBestMovesName) {
        this.m_sBestMovesName = sBestMovesName;
    }

    public String getBestMovesName() {
        return this.m_sBestMovesName;
    }

    public void setThumbnail(Bitmap hThumbnail) {
        this.m_hThumbnail = hThumbnail;
    }

    public Bitmap getThumbnail(Context hContext, Puzzle hPuzzle, int iThumbnailSize) {
        if (this.m_hThumbnail != null) {
            return this.m_hThumbnail;
        }
        Uri hUri = PuzzleHelper.getImageUri(hPuzzle.getId(), this.m_iIndex);
        BitmapFactory.Options hOptions = new BitmapFactory.Options();
        hOptions.inSampleSize = 4;
        this.m_hThumbnail = BitmapHelper.createBitmapFromUri(hContext, hUri, Integer.valueOf(iThumbnailSize), hOptions);
        return this.m_hThumbnail;
    }

    public void setImage(Bitmap hImage, String sInfo) {
        this.m_sInfo = sInfo;
        this.m_hImage = hImage;
    }

    public Bitmap getImage(Context hContext) {
        if (this.m_hImage == null) {
            this.m_hImage = BitmapHelper.createBitmapFromUri(hContext, PuzzleHelper.getImageUri(this.m_sPuzzleId, this.m_iIndex), Integer.valueOf(hContext.getResources().getDisplayMetrics().widthPixels), null);
        }
        return this.m_hImage;
    }

    public String getInfo(Context hContext) {
        if (this.m_sInfo == null) {
            this.m_sInfo = PuzzleConfig.getImageInfo(hContext, this.m_iIndex);
        }
        return this.m_sInfo;
    }
}
