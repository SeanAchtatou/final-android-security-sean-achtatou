package data;

import android.content.Context;
import android.os.Process;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.text.format.Time;
import helper.Constants;
import helper.PuzzleConfig;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashSet;
import main.PuzzleTypeConfig;

public final class PuzzleVerifier {
    private static final String PREF_KEY_VERIFY_WEEK_NUMBER = "PREF_KEY_VERIFY_WEEK_NUMBER";
    private static final String WEB_DATA_COMMENT_TAG = "//";
    private static final String WEB_DATA_SEPARATOR = ":";
    private static final String WEB_DEV_BASE_URL = (String.valueOf(PuzzleConfig.WEB_BASE_URL) + WEB_DEV_DATA_DIR + "/");
    private static final String WEB_DEV_CHECK_FILE = "check.txt";
    private static final String WEB_DEV_DATA_DIR = "Developer";
    private static final String WEB_DEV_FILE_EXT = ".txt";
    private static final String WEB_DEV_FILE_PREF = (String.valueOf(PuzzleTypeConfig.FOLDER_NAME) + "_");

    public static synchronized boolean verifiy(Context hContext, Puzzle hPuzzle) {
        boolean z;
        synchronized (PuzzleVerifier.class) {
            if (hPuzzle == null) {
                z = false;
            } else {
                String sDeveloperName = hPuzzle.getDeveloperName();
                if (TextUtils.isEmpty(sDeveloperName)) {
                    z = false;
                } else {
                    String sPuzzleId = hPuzzle.getId();
                    HashSet<String> arrPuzzleId = loadRegisteredPuzzleIdFromSD(sDeveloperName);
                    if (arrPuzzleId == null || !arrPuzzleId.contains(sPuzzleId)) {
                        HashSet<String> arrPuzzleId2 = loadRegisteredPuzzleIdFromWeb(hContext, sDeveloperName);
                        if (arrPuzzleId2 == null) {
                            z = true;
                        } else if (!arrPuzzleId2.isEmpty() || (arrPuzzleId2 = loadRegisteredPuzzleIdFromWeb(hContext, sDeveloperName)) != null) {
                            saveRegisteredPuzzleIdToSD(sDeveloperName, arrPuzzleId2);
                            if (arrPuzzleId2.contains(sPuzzleId)) {
                                z = true;
                            } else {
                                z = false;
                            }
                        } else {
                            z = true;
                        }
                    } else {
                        updatePuzzleIdInBackground(hContext, sDeveloperName);
                        z = true;
                    }
                }
            }
        }
        return z;
    }

    private static synchronized void updatePuzzleIdInBackground(final Context hContext, final String sDeveloperName) {
        synchronized (PuzzleVerifier.class) {
            if (isTimeToUpdate(hContext)) {
                new Thread(new Runnable() {
                    public void run() {
                        Process.setThreadPriority(10);
                        HashSet<String> arrPuzzleId = PuzzleVerifier.loadRegisteredPuzzleIdFromWeb(hContext, sDeveloperName);
                        if (arrPuzzleId != null && PuzzleVerifier.saveRegisteredPuzzleIdToSD(sDeveloperName, arrPuzzleId)) {
                            Time hTime = new Time();
                            hTime.setToNow();
                            PreferenceManager.getDefaultSharedPreferences(hContext).edit().putInt(PuzzleVerifier.PREF_KEY_VERIFY_WEEK_NUMBER, hTime.getWeekNumber()).commit();
                        }
                    }
                }).start();
            }
        }
    }

    private static synchronized boolean isTimeToUpdate(Context hContext) {
        boolean z;
        synchronized (PuzzleVerifier.class) {
            Time hTime = new Time();
            hTime.setToNow();
            if (hTime.getWeekNumber() == PreferenceManager.getDefaultSharedPreferences(hContext).getInt(PREF_KEY_VERIFY_WEEK_NUMBER, -1)) {
                z = false;
            } else {
                z = true;
            }
        }
        return z;
    }

    /* JADX INFO: Multiple debug info for r0v17 java.lang.String: [D('hReader' java.io.BufferedReader), D('sLine' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v19 java.lang.String[]: [D('arrSource' java.lang.String[]), D('sLine' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v21 java.lang.String: [D('arrSource' java.lang.String[]), D('sPuzzleId' java.lang.String)] */
    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00c7 A[Catch:{ all -> 0x010f }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00d6 A[Catch:{ all -> 0x010f }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00e8 A[Catch:{ all -> 0x00ee }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0104 A[Catch:{ all -> 0x00ee }] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x010a A[SYNTHETIC, Splitter:B:79:0x010a] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:51:0x00d0=Splitter:B:51:0x00d0, B:43:0x00c1=Splitter:B:43:0x00c1, B:33:0x00aa=Splitter:B:33:0x00aa} */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:60:0x00e2=Splitter:B:60:0x00e2, B:74:0x00fe=Splitter:B:74:0x00fe} */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:67:0x00f1=Splitter:B:67:0x00f1, B:37:0x00b3=Splitter:B:37:0x00b3, B:55:0x00d9=Splitter:B:55:0x00d9, B:47:0x00ca=Splitter:B:47:0x00ca} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized java.util.HashSet<java.lang.String> loadRegisteredPuzzleIdFromWeb(android.content.Context r8, java.lang.String r9) {
        /*
            java.lang.Class<data.PuzzleVerifier> r4 = data.PuzzleVerifier.class
            monitor-enter(r4)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0078 }
            java.lang.String r1 = "Load registered addon id from web: "
            r0.<init>(r1)     // Catch:{ all -> 0x0078 }
            java.lang.StringBuilder r0 = r0.append(r9)     // Catch:{ all -> 0x0078 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0078 }
            helper.L.v(r0)     // Catch:{ all -> 0x0078 }
            boolean r8 = helper.Global.hasInternetConnection(r8)     // Catch:{ all -> 0x0078 }
            if (r8 != 0) goto L_0x001e
            r8 = 0
        L_0x001c:
            monitor-exit(r4)
            return r8
        L_0x001e:
            java.util.HashSet r8 = new java.util.HashSet     // Catch:{ all -> 0x0078 }
            r8.<init>()     // Catch:{ all -> 0x0078 }
            r0 = 0
            java.net.URI r2 = new java.net.URI     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            java.lang.String r1 = data.PuzzleVerifier.WEB_DEV_BASE_URL     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            r2.<init>(r1)     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            java.lang.String r3 = r2.getPath()     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            r1.<init>(r3)     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            java.lang.String r3 = data.PuzzleVerifier.WEB_DEV_FILE_PREF     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            java.lang.StringBuilder r1 = r1.append(r9)     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            java.lang.String r3 = ".txt"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            java.lang.String r3 = r1.toString()     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            java.net.URI r1 = new java.net.URI     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            java.lang.String r5 = r2.getScheme()     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            java.lang.String r2 = r2.getHost()     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            r6 = 0
            r1.<init>(r5, r2, r3, r6)     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            java.net.URL r1 = r1.toURL()     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            java.io.InputStream r1 = r1.openStream()     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            r2.<init>(r1)     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
            r3 = 1024(0x400, float:1.435E-42)
            r1.<init>(r2, r3)     // Catch:{ SocketException -> 0x011d, SocketTimeoutException -> 0x00c0, UnknownHostException -> 0x00cf, FileNotFoundException -> 0x00de, Exception -> 0x00fa }
        L_0x006e:
            java.lang.String r0 = r1.readLine()     // Catch:{ SocketException -> 0x00a8, SocketTimeoutException -> 0x011a, UnknownHostException -> 0x0117, FileNotFoundException -> 0x0115, Exception -> 0x0113 }
            if (r0 != 0) goto L_0x007b
            helper.Global.closeReader(r1)     // Catch:{ all -> 0x0078 }
            goto L_0x001c
        L_0x0078:
            r8 = move-exception
            monitor-exit(r4)
            throw r8
        L_0x007b:
            java.lang.String r0 = r0.trim()     // Catch:{ SocketException -> 0x00a8, SocketTimeoutException -> 0x011a, UnknownHostException -> 0x0117, FileNotFoundException -> 0x0115, Exception -> 0x0113 }
            boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ SocketException -> 0x00a8, SocketTimeoutException -> 0x011a, UnknownHostException -> 0x0117, FileNotFoundException -> 0x0115, Exception -> 0x0113 }
            if (r2 != 0) goto L_0x006e
            java.lang.String r2 = "//"
            boolean r2 = r0.startsWith(r2)     // Catch:{ SocketException -> 0x00a8, SocketTimeoutException -> 0x011a, UnknownHostException -> 0x0117, FileNotFoundException -> 0x0115, Exception -> 0x0113 }
            if (r2 != 0) goto L_0x006e
            java.lang.String r2 = ":"
            java.lang.String[] r0 = r0.split(r2)     // Catch:{ SocketException -> 0x00a8, SocketTimeoutException -> 0x011a, UnknownHostException -> 0x0117, FileNotFoundException -> 0x0115, Exception -> 0x0113 }
            int r2 = r0.length     // Catch:{ SocketException -> 0x00a8, SocketTimeoutException -> 0x011a, UnknownHostException -> 0x0117, FileNotFoundException -> 0x0115, Exception -> 0x0113 }
            r3 = 1
            if (r2 < r3) goto L_0x006e
            r2 = 0
            r0 = r0[r2]     // Catch:{ SocketException -> 0x00a8, SocketTimeoutException -> 0x011a, UnknownHostException -> 0x0117, FileNotFoundException -> 0x0115, Exception -> 0x0113 }
            java.lang.String r0 = r0.trim()     // Catch:{ SocketException -> 0x00a8, SocketTimeoutException -> 0x011a, UnknownHostException -> 0x0117, FileNotFoundException -> 0x0115, Exception -> 0x0113 }
            boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ SocketException -> 0x00a8, SocketTimeoutException -> 0x011a, UnknownHostException -> 0x0117, FileNotFoundException -> 0x0115, Exception -> 0x0113 }
            if (r2 != 0) goto L_0x006e
            r8.add(r0)     // Catch:{ SocketException -> 0x00a8, SocketTimeoutException -> 0x011a, UnknownHostException -> 0x0117, FileNotFoundException -> 0x0115, Exception -> 0x0113 }
            goto L_0x006e
        L_0x00a8:
            r9 = move-exception
            r0 = r1
        L_0x00aa:
            boolean r1 = helper.Constants.debug()     // Catch:{ all -> 0x010f }
            if (r1 == 0) goto L_0x00b3
            r9.printStackTrace()     // Catch:{ all -> 0x010f }
        L_0x00b3:
            helper.Global.closeReader(r0)     // Catch:{ all -> 0x0078 }
            r9 = r0
        L_0x00b7:
            boolean r9 = isServerRunning()     // Catch:{ all -> 0x0078 }
            if (r9 != 0) goto L_0x001c
            r8 = 0
            goto L_0x001c
        L_0x00c0:
            r9 = move-exception
        L_0x00c1:
            boolean r1 = helper.Constants.debug()     // Catch:{ all -> 0x010f }
            if (r1 == 0) goto L_0x00ca
            r9.printStackTrace()     // Catch:{ all -> 0x010f }
        L_0x00ca:
            helper.Global.closeReader(r0)     // Catch:{ all -> 0x0078 }
            r9 = r0
            goto L_0x00b7
        L_0x00cf:
            r9 = move-exception
        L_0x00d0:
            boolean r1 = helper.Constants.debug()     // Catch:{ all -> 0x010f }
            if (r1 == 0) goto L_0x00d9
            r9.printStackTrace()     // Catch:{ all -> 0x010f }
        L_0x00d9:
            helper.Global.closeReader(r0)     // Catch:{ all -> 0x0078 }
            r9 = r0
            goto L_0x00b7
        L_0x00de:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x00e2:
            boolean r2 = helper.Constants.debug()     // Catch:{ all -> 0x00ee }
            if (r2 == 0) goto L_0x00f5
            java.lang.RuntimeException r8 = new java.lang.RuntimeException     // Catch:{ all -> 0x00ee }
            r8.<init>(r9, r0)     // Catch:{ all -> 0x00ee }
            throw r8     // Catch:{ all -> 0x00ee }
        L_0x00ee:
            r8 = move-exception
            r9 = r8
            r8 = r1
        L_0x00f1:
            helper.Global.closeReader(r8)     // Catch:{ all -> 0x0078 }
            throw r9     // Catch:{ all -> 0x0078 }
        L_0x00f5:
            helper.Global.closeReader(r1)     // Catch:{ all -> 0x0078 }
            r9 = r1
            goto L_0x00b7
        L_0x00fa:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x00fe:
            boolean r2 = helper.Constants.debug()     // Catch:{ all -> 0x00ee }
            if (r2 == 0) goto L_0x010a
            java.lang.RuntimeException r8 = new java.lang.RuntimeException     // Catch:{ all -> 0x00ee }
            r8.<init>(r9, r0)     // Catch:{ all -> 0x00ee }
            throw r8     // Catch:{ all -> 0x00ee }
        L_0x010a:
            helper.Global.closeReader(r1)     // Catch:{ all -> 0x0078 }
            r9 = r1
            goto L_0x00b7
        L_0x010f:
            r8 = move-exception
            r9 = r8
            r8 = r0
            goto L_0x00f1
        L_0x0113:
            r0 = move-exception
            goto L_0x00fe
        L_0x0115:
            r0 = move-exception
            goto L_0x00e2
        L_0x0117:
            r9 = move-exception
            r0 = r1
            goto L_0x00d0
        L_0x011a:
            r9 = move-exception
            r0 = r1
            goto L_0x00c1
        L_0x011d:
            r9 = move-exception
            goto L_0x00aa
        */
        throw new UnsupportedOperationException("Method not decompiled: data.PuzzleVerifier.loadRegisteredPuzzleIdFromWeb(android.content.Context, java.lang.String):java.util.HashSet");
    }

    private static synchronized boolean isServerRunning() {
        boolean z;
        synchronized (PuzzleVerifier.class) {
            try {
                new URL(String.valueOf(WEB_DEV_BASE_URL) + WEB_DEV_CHECK_FILE).openStream();
                z = true;
            } catch (UnknownHostException e) {
                UnknownHostException e2 = e;
                if (Constants.debug()) {
                    e2.printStackTrace();
                }
                z = false;
                return z;
            } catch (ConnectException e3) {
                ConnectException e4 = e3;
                if (Constants.debug()) {
                    e4.printStackTrace();
                }
                z = false;
                return z;
            } catch (SocketTimeoutException e5) {
                SocketTimeoutException e6 = e5;
                if (Constants.debug()) {
                    e6.printStackTrace();
                }
                z = false;
                return z;
            } catch (FileNotFoundException e7) {
                FileNotFoundException e8 = e7;
                if (Constants.debug()) {
                    throw new RuntimeException(e8);
                }
                z = false;
                return z;
            } catch (Exception e9) {
                Exception e10 = e9;
                if (Constants.debug()) {
                    throw new RuntimeException(e10);
                }
                z = false;
                return z;
            }
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x007a A[Catch:{ all -> 0x00ac }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x008d A[Catch:{ all -> 0x00ac }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00a2 A[Catch:{ all -> 0x00ac }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00bd A[Catch:{ all -> 0x00ac }] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00c7 A[SYNTHETIC, Splitter:B:65:0x00c7] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:26:0x0074=Splitter:B:26:0x0074, B:36:0x0087=Splitter:B:36:0x0087, B:60:0x00b7=Splitter:B:60:0x00b7, B:48:0x009c=Splitter:B:48:0x009c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static synchronized java.util.HashSet<java.lang.String> loadRegisteredPuzzleIdFromSD(java.lang.String r11) {
        /*
            r10 = 0
            java.lang.Class<data.PuzzleVerifier> r7 = data.PuzzleVerifier.class
            monitor-enter(r7)
            java.io.File r3 = getDeveloperDir()     // Catch:{ all -> 0x0097 }
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x0097 }
            r2.<init>(r3, r11)     // Catch:{ all -> 0x0097 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0097 }
            java.lang.String r9 = "Load registered puzzle id from SD: "
            r8.<init>(r9)     // Catch:{ all -> 0x0097 }
            java.lang.String r9 = r2.getAbsolutePath()     // Catch:{ all -> 0x0097 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0097 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0097 }
            helper.L.v(r8)     // Catch:{ all -> 0x0097 }
            boolean r8 = helper.Global.isSDCardAvailable()     // Catch:{ all -> 0x0097 }
            if (r8 != 0) goto L_0x0037
            boolean r8 = helper.Constants.debug()     // Catch:{ all -> 0x0097 }
            if (r8 == 0) goto L_0x0034
            java.lang.String r8 = "SD Card unmounted or not present"
            helper.L.v(r8)     // Catch:{ all -> 0x0097 }
        L_0x0034:
            r8 = r10
        L_0x0035:
            monitor-exit(r7)
            return r8
        L_0x0037:
            boolean r8 = r2.exists()     // Catch:{ all -> 0x0097 }
            if (r8 != 0) goto L_0x0055
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0097 }
            java.lang.String r9 = "File does not exsits: "
            r8.<init>(r9)     // Catch:{ all -> 0x0097 }
            java.lang.String r9 = r2.getAbsolutePath()     // Catch:{ all -> 0x0097 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0097 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0097 }
            helper.L.v(r8)     // Catch:{ all -> 0x0097 }
            r8 = r10
            goto L_0x0035
        L_0x0055:
            r5 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ ClassCastException -> 0x0072, ClassNotFoundException -> 0x0085, IOException -> 0x009a, Exception -> 0x00b5 }
            r4.<init>(r2)     // Catch:{ ClassCastException -> 0x0072, ClassNotFoundException -> 0x0085, IOException -> 0x009a, Exception -> 0x00b5 }
            java.io.ObjectInputStream r6 = new java.io.ObjectInputStream     // Catch:{ ClassCastException -> 0x0072, ClassNotFoundException -> 0x0085, IOException -> 0x009a, Exception -> 0x00b5 }
            java.io.BufferedInputStream r8 = new java.io.BufferedInputStream     // Catch:{ ClassCastException -> 0x0072, ClassNotFoundException -> 0x0085, IOException -> 0x009a, Exception -> 0x00b5 }
            r9 = 8192(0x2000, float:1.14794E-41)
            r8.<init>(r4, r9)     // Catch:{ ClassCastException -> 0x0072, ClassNotFoundException -> 0x0085, IOException -> 0x009a, Exception -> 0x00b5 }
            r6.<init>(r8)     // Catch:{ ClassCastException -> 0x0072, ClassNotFoundException -> 0x0085, IOException -> 0x009a, Exception -> 0x00b5 }
            java.lang.Object r0 = r6.readObject()     // Catch:{ ClassCastException -> 0x00da, ClassNotFoundException -> 0x00d6, IOException -> 0x00d2, Exception -> 0x00ce, all -> 0x00cb }
            java.util.HashSet r0 = (java.util.HashSet) r0     // Catch:{ ClassCastException -> 0x00da, ClassNotFoundException -> 0x00d6, IOException -> 0x00d2, Exception -> 0x00ce, all -> 0x00cb }
            helper.Global.closeInputStream(r6)     // Catch:{ all -> 0x0097 }
            r8 = r0
            goto L_0x0035
        L_0x0072:
            r8 = move-exception
            r1 = r8
        L_0x0074:
            boolean r8 = helper.Constants.debug()     // Catch:{ all -> 0x00ac }
            if (r8 == 0) goto L_0x007d
            r1.printStackTrace()     // Catch:{ all -> 0x00ac }
        L_0x007d:
            r2.delete()     // Catch:{ all -> 0x00ac }
            helper.Global.closeInputStream(r5)     // Catch:{ all -> 0x0097 }
        L_0x0083:
            r8 = r10
            goto L_0x0035
        L_0x0085:
            r8 = move-exception
            r1 = r8
        L_0x0087:
            boolean r8 = helper.Constants.debug()     // Catch:{ all -> 0x00ac }
            if (r8 == 0) goto L_0x0090
            r1.printStackTrace()     // Catch:{ all -> 0x00ac }
        L_0x0090:
            r2.delete()     // Catch:{ all -> 0x00ac }
            helper.Global.closeInputStream(r5)     // Catch:{ all -> 0x0097 }
            goto L_0x0083
        L_0x0097:
            r8 = move-exception
            monitor-exit(r7)
            throw r8
        L_0x009a:
            r8 = move-exception
            r1 = r8
        L_0x009c:
            boolean r8 = helper.Constants.debug()     // Catch:{ all -> 0x00ac }
            if (r8 == 0) goto L_0x00b1
            java.lang.RuntimeException r8 = new java.lang.RuntimeException     // Catch:{ all -> 0x00ac }
            java.lang.String r9 = r2.getAbsolutePath()     // Catch:{ all -> 0x00ac }
            r8.<init>(r9, r1)     // Catch:{ all -> 0x00ac }
            throw r8     // Catch:{ all -> 0x00ac }
        L_0x00ac:
            r8 = move-exception
        L_0x00ad:
            helper.Global.closeInputStream(r5)     // Catch:{ all -> 0x0097 }
            throw r8     // Catch:{ all -> 0x0097 }
        L_0x00b1:
            helper.Global.closeInputStream(r5)     // Catch:{ all -> 0x0097 }
            goto L_0x0083
        L_0x00b5:
            r8 = move-exception
            r1 = r8
        L_0x00b7:
            boolean r8 = helper.Constants.debug()     // Catch:{ all -> 0x00ac }
            if (r8 == 0) goto L_0x00c7
            java.lang.RuntimeException r8 = new java.lang.RuntimeException     // Catch:{ all -> 0x00ac }
            java.lang.String r9 = r2.getAbsolutePath()     // Catch:{ all -> 0x00ac }
            r8.<init>(r9, r1)     // Catch:{ all -> 0x00ac }
            throw r8     // Catch:{ all -> 0x00ac }
        L_0x00c7:
            helper.Global.closeInputStream(r5)     // Catch:{ all -> 0x0097 }
            goto L_0x0083
        L_0x00cb:
            r8 = move-exception
            r5 = r6
            goto L_0x00ad
        L_0x00ce:
            r8 = move-exception
            r1 = r8
            r5 = r6
            goto L_0x00b7
        L_0x00d2:
            r8 = move-exception
            r1 = r8
            r5 = r6
            goto L_0x009c
        L_0x00d6:
            r8 = move-exception
            r1 = r8
            r5 = r6
            goto L_0x0087
        L_0x00da:
            r8 = move-exception
            r1 = r8
            r5 = r6
            goto L_0x0074
        */
        throw new UnsupportedOperationException("Method not decompiled: data.PuzzleVerifier.loadRegisteredPuzzleIdFromSD(java.lang.String):java.util.HashSet");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00ae A[Catch:{ all -> 0x0099 }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00b8 A[SYNTHETIC, Splitter:B:52:0x00b8] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:47:0x00a8=Splitter:B:47:0x00a8, B:30:0x0089=Splitter:B:30:0x0089} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean saveRegisteredPuzzleIdToSD(java.lang.String r9, java.util.HashSet<java.lang.String> r10) {
        /*
            r8 = 0
            java.lang.Class<data.PuzzleVerifier> r5 = data.PuzzleVerifier.class
            monitor-enter(r5)
            java.io.File r1 = getDeveloperDir()     // Catch:{ all -> 0x009e }
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x009e }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x009e }
            java.lang.String r7 = java.lang.String.valueOf(r9)     // Catch:{ all -> 0x009e }
            r6.<init>(r7)     // Catch:{ all -> 0x009e }
            java.lang.String r7 = ".tmp"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x009e }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x009e }
            r2.<init>(r1, r6)     // Catch:{ all -> 0x009e }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x009e }
            java.lang.String r7 = "Save registered puzzle id to SD: "
            r6.<init>(r7)     // Catch:{ all -> 0x009e }
            java.lang.String r7 = r2.getAbsolutePath()     // Catch:{ all -> 0x009e }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x009e }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x009e }
            helper.L.v(r6)     // Catch:{ all -> 0x009e }
            boolean r6 = helper.Global.isSDCardAvailable()     // Catch:{ all -> 0x009e }
            if (r6 != 0) goto L_0x004a
            boolean r6 = helper.Constants.debug()     // Catch:{ all -> 0x009e }
            if (r6 == 0) goto L_0x0047
            java.lang.String r6 = "SD Card unmounted or not present"
            helper.L.v(r6)     // Catch:{ all -> 0x009e }
        L_0x0047:
            r6 = r8
        L_0x0048:
            monitor-exit(r5)
            return r6
        L_0x004a:
            if (r10 == 0) goto L_0x0052
            boolean r6 = r10.isEmpty()     // Catch:{ all -> 0x009e }
            if (r6 == 0) goto L_0x005f
        L_0x0052:
            boolean r6 = helper.Constants.debug()     // Catch:{ all -> 0x009e }
            if (r6 == 0) goto L_0x005d
            java.lang.String r6 = "No registred puzzle id data"
            helper.L.v(r6)     // Catch:{ all -> 0x009e }
        L_0x005d:
            r6 = r8
            goto L_0x0048
        L_0x005f:
            r3 = 0
            java.io.File r6 = r2.getParentFile()     // Catch:{ IOException -> 0x0087, Exception -> 0x00a6 }
            r6.mkdirs()     // Catch:{ IOException -> 0x0087, Exception -> 0x00a6 }
            r2.createNewFile()     // Catch:{ IOException -> 0x0087, Exception -> 0x00a6 }
            java.io.ObjectOutputStream r4 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x0087, Exception -> 0x00a6 }
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0087, Exception -> 0x00a6 }
            r6.<init>(r2)     // Catch:{ IOException -> 0x0087, Exception -> 0x00a6 }
            r4.<init>(r6)     // Catch:{ IOException -> 0x0087, Exception -> 0x00a6 }
            r4.writeObject(r10)     // Catch:{ IOException -> 0x00c8, Exception -> 0x00c4, all -> 0x00c1 }
            java.io.File r6 = new java.io.File     // Catch:{ IOException -> 0x00c8, Exception -> 0x00c4, all -> 0x00c1 }
            r6.<init>(r1, r9)     // Catch:{ IOException -> 0x00c8, Exception -> 0x00c4, all -> 0x00c1 }
            boolean r6 = r2.renameTo(r6)     // Catch:{ IOException -> 0x00c8, Exception -> 0x00c4, all -> 0x00c1 }
            if (r6 == 0) goto L_0x00bc
            helper.Global.closeOutputStream(r4)     // Catch:{ all -> 0x009e }
            r6 = 1
            goto L_0x0048
        L_0x0087:
            r6 = move-exception
            r0 = r6
        L_0x0089:
            boolean r6 = helper.Constants.debug()     // Catch:{ all -> 0x0099 }
            if (r6 == 0) goto L_0x00a1
            java.lang.RuntimeException r6 = new java.lang.RuntimeException     // Catch:{ all -> 0x0099 }
            java.lang.String r7 = r2.getAbsolutePath()     // Catch:{ all -> 0x0099 }
            r6.<init>(r7, r0)     // Catch:{ all -> 0x0099 }
            throw r6     // Catch:{ all -> 0x0099 }
        L_0x0099:
            r6 = move-exception
        L_0x009a:
            helper.Global.closeOutputStream(r3)     // Catch:{ all -> 0x009e }
            throw r6     // Catch:{ all -> 0x009e }
        L_0x009e:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        L_0x00a1:
            helper.Global.closeOutputStream(r3)     // Catch:{ all -> 0x009e }
        L_0x00a4:
            r6 = r8
            goto L_0x0048
        L_0x00a6:
            r6 = move-exception
            r0 = r6
        L_0x00a8:
            boolean r6 = helper.Constants.debug()     // Catch:{ all -> 0x0099 }
            if (r6 == 0) goto L_0x00b8
            java.lang.RuntimeException r6 = new java.lang.RuntimeException     // Catch:{ all -> 0x0099 }
            java.lang.String r7 = r2.getAbsolutePath()     // Catch:{ all -> 0x0099 }
            r6.<init>(r7, r0)     // Catch:{ all -> 0x0099 }
            throw r6     // Catch:{ all -> 0x0099 }
        L_0x00b8:
            helper.Global.closeOutputStream(r3)     // Catch:{ all -> 0x009e }
            goto L_0x00a4
        L_0x00bc:
            helper.Global.closeOutputStream(r4)     // Catch:{ all -> 0x009e }
            r3 = r4
            goto L_0x00a4
        L_0x00c1:
            r6 = move-exception
            r3 = r4
            goto L_0x009a
        L_0x00c4:
            r6 = move-exception
            r0 = r6
            r3 = r4
            goto L_0x00a8
        L_0x00c8:
            r6 = move-exception
            r0 = r6
            r3 = r4
            goto L_0x0089
        */
        throw new UnsupportedOperationException("Method not decompiled: data.PuzzleVerifier.saveRegisteredPuzzleIdToSD(java.lang.String, java.util.HashSet):boolean");
    }

    private static synchronized File getDeveloperDir() {
        File hDir;
        synchronized (PuzzleVerifier.class) {
            hDir = new File(String.valueOf(PuzzleConfig.ROOT_DIR_HIDDEN) + WEB_DEV_DATA_DIR);
            hDir.mkdirs();
        }
        return hDir;
    }
}
