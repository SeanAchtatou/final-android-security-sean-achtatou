package data;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.text.format.Time;
import helper.Constants;
import helper.Global;
import helper.L;
import helper.PuzzleConfig;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import main.PuzzleTypeConfig;
import res.ResString;

public final class MorePuzzleList {
    private static final int BUFFER_SIZE = 8192;
    private static final String MORE_PUZZLE_DIR = "MorePuzzle";
    private static final String MORE_PUZZLE_INSTALLED_FILE_NAME = "more_puzzle_installed.dat";
    public static final int NUMBER_TYPE = 2;
    private static final String PREF_KEY_SYNCH_MORE_PUZZLE_MARKET_YEAR_DAY = "PREF_KEY_SYNCH_MORE_PUZZLE_MARKET_YEAR_DAY";
    public static final int TYPE_PUZZLE = 1;
    public static final int TYPE_SEPARATOR = 0;
    private static final String WEB_DATA_COMMENT_TAG = "//";
    private static final String WEB_DATA_SEPARATOR = "|";
    private static final String WEB_MORE_PUZZLE_MARKET_BASE_URL = (String.valueOf(PuzzleConfig.WEB_BASE_URL) + MORE_PUZZLE_DIR + "/");
    private final List<MorePuzzle> m_arrMorePuzzleInstalledExtern = new ArrayList();
    private final List<MorePuzzle> m_arrMorePuzzleInstalledIntern = new ArrayList();
    private final List<MorePuzzle> m_arrMorePuzzleMarketExtern = new ArrayList();
    private final List<MorePuzzle> m_arrMorePuzzleMarketIntern = new ArrayList();
    private final int m_iMarketId;
    private final String m_sSeparatorInstalled;
    private final String m_sSeparatorMarket;

    public MorePuzzleList(Context hContext) {
        this.m_iMarketId = PuzzleConfig.getMarketId(hContext).intValue();
        this.m_sSeparatorInstalled = ResString.separator_installed;
        this.m_sSeparatorMarket = PuzzleConfig.getMarketDisplayName(this.m_iMarketId);
    }

    public boolean isTimeToSynchWithWeb(Context hContext) {
        Time hTime = new Time();
        hTime.setToNow();
        if (hTime.yearDay != PreferenceManager.getDefaultSharedPreferences(hContext).getInt(PREF_KEY_SYNCH_MORE_PUZZLE_MARKET_YEAR_DAY, -1)) {
            return true;
        }
        return false;
    }

    public void synchWithWebSucceeded(Context hContext) {
        Time hTime = new Time();
        hTime.setToNow();
        PreferenceManager.getDefaultSharedPreferences(hContext).edit().putInt(PREF_KEY_SYNCH_MORE_PUZZLE_MARKET_YEAR_DAY, hTime.yearDay).commit();
        if (Constants.debug()) {
            L.v("Sync with web succeeded");
        }
    }

    public void resetSyncWithWebDate(Context hContext) {
        PreferenceManager.getDefaultSharedPreferences(hContext).edit().putInt(PREF_KEY_SYNCH_MORE_PUZZLE_MARKET_YEAR_DAY, -1).commit();
    }

    public void initMorePuzzleInstalled(Context hContext) {
        Integer iMorePuzzleInstalledMarketId;
        List<ResolveInfo> arrResolveInfoMorePuzzleInstalled = hContext.getPackageManager().queryIntentActivities(new Intent(PuzzleTypeConfig.START_PUZZLE_ACTION), 32);
        HashMap hashMap = new HashMap();
        boolean bSaveMorePuzzleProgess = false;
        for (ResolveInfo hResolveInfoLoop : arrResolveInfoMorePuzzleInstalled) {
            String sMorePuzzleInstalledId = hResolveInfoLoop.activityInfo.packageName;
            Context hMorePuzzleInstalledContext = null;
            try {
                hMorePuzzleInstalledContext = hContext.createPackageContext(sMorePuzzleInstalledId, 0);
            } catch (PackageManager.NameNotFoundException e) {
                if (Constants.debug()) {
                    e.printStackTrace();
                }
            }
            if (!(hMorePuzzleInstalledContext == null || (iMorePuzzleInstalledMarketId = PuzzleConfig.getMarketId(hMorePuzzleInstalledContext)) == null || this.m_iMarketId != iMorePuzzleInstalledMarketId.intValue())) {
                if (Constants.debug()) {
                    L.v("Installed: " + sMorePuzzleInstalledId);
                }
                String sPuzzleId = hMorePuzzleInstalledContext.getPackageName();
                if (!TextUtils.isEmpty(sPuzzleId)) {
                    String sDeveloperName = PuzzleConfig.getDeveloperName(hMorePuzzleInstalledContext);
                    if (!TextUtils.isEmpty(sDeveloperName)) {
                        String sPuzzleName = PuzzleConfig.getPuzzleName(hMorePuzzleInstalledContext);
                        if (!TextUtils.isEmpty(sPuzzleName)) {
                            MorePuzzle morePuzzle = new MorePuzzle(sPuzzleId, sDeveloperName, sPuzzleName);
                            if (Global.isSDCardAvailable() && MorePuzzleProgress.getImageCount(hContext, sPuzzleId) == 0) {
                                MorePuzzleProgress.setImageCount(hContext, sPuzzleId, Puzzle.getImageCount(hMorePuzzleInstalledContext, sPuzzleId));
                                bSaveMorePuzzleProgess = true;
                            }
                            hashMap.put(sPuzzleId, morePuzzle);
                        }
                    }
                }
            }
        }
        if (bSaveMorePuzzleProgess) {
            MorePuzzleProgress.saveProgressData(hContext);
        }
        List<MorePuzzle> arrMorePuzzleInstalledSD = loadMorePuzzleInstalledFromSD(hContext);
        if (!(arrMorePuzzleInstalledSD == null || hashMap.size() == arrMorePuzzleInstalledSD.size())) {
            resetSyncWithWebDate(hContext);
        }
        synchCurrentDataWithOrder(hashMap, arrMorePuzzleInstalledSD, this.m_arrMorePuzzleInstalledIntern);
        saveMorePuzzleInstalledToSD(hContext);
        if (Constants.debug() && hashMap.size() != this.m_arrMorePuzzleInstalledIntern.size()) {
            throw new RuntimeException("Invalid size");
        } else if (Global.isSDCardAvailable() && MorePuzzleProgress.removeNotInstalledData(hashMap.keySet())) {
            MorePuzzleProgress.saveProgressData(hContext);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    private void synchCurrentDataWithOrder(Map<String, MorePuzzle> mapCurrentDataWithoutOrder, List<MorePuzzle> arrOldDataWithOrder, List<MorePuzzle> arrDes) {
        ArrayList<String> arrMorePuzzleInstalled = new ArrayList<>();
        if (arrOldDataWithOrder != null) {
            for (MorePuzzle hMorePuzzleInstalledFromSDLoop : arrOldDataWithOrder) {
                String sMorePuzzleInstalledIdFromSD = hMorePuzzleInstalledFromSDLoop.getId();
                if (!arrMorePuzzleInstalled.contains(sMorePuzzleInstalledIdFromSD) && mapCurrentDataWithoutOrder.containsKey(sMorePuzzleInstalledIdFromSD)) {
                    arrMorePuzzleInstalled.add(sMorePuzzleInstalledIdFromSD);
                }
            }
        }
        for (String sPuzzleIdLoop : mapCurrentDataWithoutOrder.keySet()) {
            if (!arrMorePuzzleInstalled.contains(sPuzzleIdLoop)) {
                arrMorePuzzleInstalled.add(sPuzzleIdLoop);
            }
        }
        arrDes.clear();
        Iterator it = arrMorePuzzleInstalled.iterator();
        while (it.hasNext()) {
            arrDes.add(mapCurrentDataWithoutOrder.get((String) it.next()));
        }
    }

    public int moveInstalledUp(Context hContext, MorePuzzle hMorePuzzleInstalled) {
        String sId = hMorePuzzleInstalled.getId();
        int iSize = this.m_arrMorePuzzleInstalledIntern.size();
        int i = 0;
        while (i < iSize) {
            MorePuzzle hMorePuzzleInstalledLoop = this.m_arrMorePuzzleInstalledIntern.get(i);
            if (!hMorePuzzleInstalledLoop.getId().equals(sId) || i <= 0) {
                i++;
            } else {
                this.m_arrMorePuzzleInstalledIntern.remove(i);
                int iNewPos = i - 1;
                this.m_arrMorePuzzleInstalledIntern.add(iNewPos, hMorePuzzleInstalledLoop);
                saveMorePuzzleInstalledToSD(hContext);
                return iNewPos;
            }
        }
        return -1;
    }

    public int moveInstalledDown(Context hContext, MorePuzzle hMorePuzzleInstalled) {
        String sId = hMorePuzzleInstalled.getId();
        int iSize = this.m_arrMorePuzzleInstalledIntern.size() - 1;
        for (int i = 0; i < iSize; i++) {
            MorePuzzle hMorePuzzleInstalledLoop = this.m_arrMorePuzzleInstalledIntern.get(i);
            if (hMorePuzzleInstalledLoop.getId().equals(sId)) {
                this.m_arrMorePuzzleInstalledIntern.remove(i);
                int iNewPos = i + 1;
                this.m_arrMorePuzzleInstalledIntern.add(iNewPos, hMorePuzzleInstalledLoop);
                saveMorePuzzleInstalledToSD(hContext);
                return iNewPos;
            }
        }
        return -1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x006c A[Catch:{ all -> 0x0076 }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0087 A[Catch:{ all -> 0x0076 }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x008d  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:27:0x0081=Splitter:B:27:0x0081, B:16:0x0066=Splitter:B:16:0x0066} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void saveMorePuzzleInstalledToSD(android.content.Context r8) {
        /*
            r7 = this;
            int r5 = r7.m_iMarketId
            java.io.File r1 = getMorePuzzleDir(r8, r5)
            java.io.File r2 = new java.io.File
            java.lang.String r5 = "more_puzzle_installed.dat.tmp"
            r2.<init>(r1, r5)
            boolean r5 = helper.Constants.debug()
            if (r5 == 0) goto L_0x0029
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "Save more puzzle installed list to SD: "
            r5.<init>(r6)
            java.lang.String r6 = r2.getAbsolutePath()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            helper.L.v(r5)
        L_0x0029:
            boolean r5 = helper.Global.isSDCardAvailable()
            if (r5 != 0) goto L_0x003b
            boolean r5 = helper.Constants.debug()
            if (r5 == 0) goto L_0x003a
            java.lang.String r5 = "SD Card unmounted or not present"
            helper.L.v(r5)
        L_0x003a:
            return
        L_0x003b:
            r3 = 0
            java.io.File r5 = r2.getParentFile()     // Catch:{ IOException -> 0x0064, Exception -> 0x007f }
            r5.mkdirs()     // Catch:{ IOException -> 0x0064, Exception -> 0x007f }
            r2.createNewFile()     // Catch:{ IOException -> 0x0064, Exception -> 0x007f }
            java.io.ObjectOutputStream r4 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x0064, Exception -> 0x007f }
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0064, Exception -> 0x007f }
            r5.<init>(r2)     // Catch:{ IOException -> 0x0064, Exception -> 0x007f }
            r4.<init>(r5)     // Catch:{ IOException -> 0x0064, Exception -> 0x007f }
            java.util.List<data.MorePuzzle> r5 = r7.m_arrMorePuzzleInstalledIntern     // Catch:{ IOException -> 0x0098, Exception -> 0x0094, all -> 0x0091 }
            r4.writeObject(r5)     // Catch:{ IOException -> 0x0098, Exception -> 0x0094, all -> 0x0091 }
            java.io.File r5 = new java.io.File     // Catch:{ IOException -> 0x0098, Exception -> 0x0094, all -> 0x0091 }
            java.lang.String r6 = "more_puzzle_installed.dat"
            r5.<init>(r1, r6)     // Catch:{ IOException -> 0x0098, Exception -> 0x0094, all -> 0x0091 }
            r2.renameTo(r5)     // Catch:{ IOException -> 0x0098, Exception -> 0x0094, all -> 0x0091 }
            helper.Global.closeOutputStream(r4)
            r3 = r4
            goto L_0x003a
        L_0x0064:
            r5 = move-exception
            r0 = r5
        L_0x0066:
            boolean r5 = helper.Constants.debug()     // Catch:{ all -> 0x0076 }
            if (r5 == 0) goto L_0x007b
            java.lang.RuntimeException r5 = new java.lang.RuntimeException     // Catch:{ all -> 0x0076 }
            java.lang.String r6 = r2.getAbsolutePath()     // Catch:{ all -> 0x0076 }
            r5.<init>(r6, r0)     // Catch:{ all -> 0x0076 }
            throw r5     // Catch:{ all -> 0x0076 }
        L_0x0076:
            r5 = move-exception
        L_0x0077:
            helper.Global.closeOutputStream(r3)
            throw r5
        L_0x007b:
            helper.Global.closeOutputStream(r3)
            goto L_0x003a
        L_0x007f:
            r5 = move-exception
            r0 = r5
        L_0x0081:
            boolean r5 = helper.Constants.debug()     // Catch:{ all -> 0x0076 }
            if (r5 == 0) goto L_0x008d
            java.lang.RuntimeException r5 = new java.lang.RuntimeException     // Catch:{ all -> 0x0076 }
            r5.<init>(r0)     // Catch:{ all -> 0x0076 }
            throw r5     // Catch:{ all -> 0x0076 }
        L_0x008d:
            helper.Global.closeOutputStream(r3)
            goto L_0x003a
        L_0x0091:
            r5 = move-exception
            r3 = r4
            goto L_0x0077
        L_0x0094:
            r5 = move-exception
            r0 = r5
            r3 = r4
            goto L_0x0081
        L_0x0098:
            r5 = move-exception
            r0 = r5
            r3 = r4
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: data.MorePuzzleList.saveMorePuzzleInstalledToSD(android.content.Context):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0086 A[Catch:{ all -> 0x00ac }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0099 A[Catch:{ all -> 0x00ac }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x0080=Splitter:B:23:0x0080, B:31:0x0093=Splitter:B:31:0x0093, B:39:0x00a6=Splitter:B:39:0x00a6} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List<data.MorePuzzle> loadMorePuzzleInstalledFromSD(android.content.Context r11) {
        /*
            r10 = this;
            r9 = 0
            int r7 = r10.m_iMarketId
            java.io.File r2 = getMorePuzzleDir(r11, r7)
            java.io.File r3 = new java.io.File
            java.lang.String r7 = "more_puzzle_installed.dat"
            r3.<init>(r2, r7)
            boolean r7 = helper.Constants.debug()
            if (r7 == 0) goto L_0x002a
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "Load more puzzle installed list from SD: "
            r7.<init>(r8)
            java.lang.String r8 = r3.getAbsolutePath()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            helper.L.v(r7)
        L_0x002a:
            boolean r7 = helper.Global.isSDCardAvailable()
            if (r7 != 0) goto L_0x003d
            boolean r7 = helper.Constants.debug()
            if (r7 == 0) goto L_0x003b
            java.lang.String r7 = "SD Card unmounted or not present"
            helper.L.v(r7)
        L_0x003b:
            r7 = r9
        L_0x003c:
            return r7
        L_0x003d:
            boolean r7 = r3.exists()
            if (r7 != 0) goto L_0x0061
            boolean r7 = helper.Constants.debug()
            if (r7 == 0) goto L_0x005f
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "File does not exists: "
            r7.<init>(r8)
            java.lang.String r8 = r3.getAbsolutePath()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            helper.L.v(r7)
        L_0x005f:
            r7 = r9
            goto L_0x003c
        L_0x0061:
            r5 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ ClassCastException -> 0x007e, ClassNotFoundException -> 0x0091, Exception -> 0x00a4 }
            r4.<init>(r3)     // Catch:{ ClassCastException -> 0x007e, ClassNotFoundException -> 0x0091, Exception -> 0x00a4 }
            java.io.ObjectInputStream r6 = new java.io.ObjectInputStream     // Catch:{ ClassCastException -> 0x007e, ClassNotFoundException -> 0x0091, Exception -> 0x00a4 }
            java.io.BufferedInputStream r7 = new java.io.BufferedInputStream     // Catch:{ ClassCastException -> 0x007e, ClassNotFoundException -> 0x0091, Exception -> 0x00a4 }
            r8 = 8192(0x2000, float:1.14794E-41)
            r7.<init>(r4, r8)     // Catch:{ ClassCastException -> 0x007e, ClassNotFoundException -> 0x0091, Exception -> 0x00a4 }
            r6.<init>(r7)     // Catch:{ ClassCastException -> 0x007e, ClassNotFoundException -> 0x0091, Exception -> 0x00a4 }
            java.lang.Object r0 = r6.readObject()     // Catch:{ ClassCastException -> 0x00bc, ClassNotFoundException -> 0x00b8, Exception -> 0x00b4, all -> 0x00b1 }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ ClassCastException -> 0x00bc, ClassNotFoundException -> 0x00b8, Exception -> 0x00b4, all -> 0x00b1 }
            helper.Global.closeInputStream(r6)
            r7 = r0
            goto L_0x003c
        L_0x007e:
            r7 = move-exception
            r1 = r7
        L_0x0080:
            boolean r7 = helper.Constants.debug()     // Catch:{ all -> 0x00ac }
            if (r7 == 0) goto L_0x0089
            r1.printStackTrace()     // Catch:{ all -> 0x00ac }
        L_0x0089:
            r3.delete()     // Catch:{ all -> 0x00ac }
            helper.Global.closeInputStream(r5)
            r7 = r9
            goto L_0x003c
        L_0x0091:
            r7 = move-exception
            r1 = r7
        L_0x0093:
            boolean r7 = helper.Constants.debug()     // Catch:{ all -> 0x00ac }
            if (r7 == 0) goto L_0x009c
            r1.printStackTrace()     // Catch:{ all -> 0x00ac }
        L_0x009c:
            r3.delete()     // Catch:{ all -> 0x00ac }
            helper.Global.closeInputStream(r5)
            r7 = r9
            goto L_0x003c
        L_0x00a4:
            r7 = move-exception
            r1 = r7
        L_0x00a6:
            java.lang.RuntimeException r7 = new java.lang.RuntimeException     // Catch:{ all -> 0x00ac }
            r7.<init>(r1)     // Catch:{ all -> 0x00ac }
            throw r7     // Catch:{ all -> 0x00ac }
        L_0x00ac:
            r7 = move-exception
        L_0x00ad:
            helper.Global.closeInputStream(r5)
            throw r7
        L_0x00b1:
            r7 = move-exception
            r5 = r6
            goto L_0x00ad
        L_0x00b4:
            r7 = move-exception
            r1 = r7
            r5 = r6
            goto L_0x00a6
        L_0x00b8:
            r7 = move-exception
            r1 = r7
            r5 = r6
            goto L_0x0093
        L_0x00bc:
            r7 = move-exception
            r1 = r7
            r5 = r6
            goto L_0x0080
        */
        throw new UnsupportedOperationException("Method not decompiled: data.MorePuzzleList.loadMorePuzzleInstalledFromSD(android.content.Context):java.util.List");
    }

    public void initMorePuzzleMarketFromSD(Context hContext) {
        this.m_arrMorePuzzleMarketIntern.clear();
        List<MorePuzzle> arrMorePuzzleMarket = loadMorePuzzleMarketFromSD(hContext);
        if (arrMorePuzzleMarket == null) {
            resetSyncWithWebDate(hContext);
            return;
        }
        Random hRandom = new Random();
        boolean bResetSyncWithWebDate = true;
        for (MorePuzzle hMorePuzzleMarketLoop : arrMorePuzzleMarket) {
            if (!hMorePuzzleMarketLoop.isInstalled(hContext)) {
                String sIconPath = hMorePuzzleMarketLoop.getIconPath();
                if (TextUtils.isEmpty(sIconPath) || !new File(sIconPath).exists()) {
                    if (Constants.debug()) {
                        L.v("No icon on SD: " + sIconPath);
                    }
                    if (bResetSyncWithWebDate) {
                        resetSyncWithWebDate(hContext);
                        bResetSyncWithWebDate = false;
                    }
                } else if (this.m_arrMorePuzzleMarketIntern.isEmpty()) {
                    this.m_arrMorePuzzleMarketIntern.add(hMorePuzzleMarketLoop);
                } else {
                    this.m_arrMorePuzzleMarketIntern.add(hRandom.nextInt(this.m_arrMorePuzzleMarketIntern.size()), hMorePuzzleMarketLoop);
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:44:0x008d A[Catch:{ all -> 0x0079 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0093 A[SYNTHETIC, Splitter:B:46:0x0093] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:41:0x0087=Splitter:B:41:0x0087, B:25:0x006d=Splitter:B:25:0x006d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean saveMorePuzzleMarketToSD(android.content.Context r8, java.util.List<data.MorePuzzle> r9) {
        /*
            r7 = this;
            monitor-enter(r7)
            int r5 = r7.m_iMarketId     // Catch:{ all -> 0x007e }
            java.io.File r1 = getMorePuzzleDir(r8, r5)     // Catch:{ all -> 0x007e }
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x007e }
            java.lang.String r5 = "web_data.dat.tmp"
            r2.<init>(r1, r5)     // Catch:{ all -> 0x007e }
            boolean r5 = helper.Constants.debug()     // Catch:{ all -> 0x007e }
            if (r5 == 0) goto L_0x002a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x007e }
            java.lang.String r6 = "Save more puzzle market list to SD: "
            r5.<init>(r6)     // Catch:{ all -> 0x007e }
            java.lang.String r6 = r2.getAbsolutePath()     // Catch:{ all -> 0x007e }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x007e }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x007e }
            helper.L.v(r5)     // Catch:{ all -> 0x007e }
        L_0x002a:
            boolean r5 = helper.Global.isSDCardAvailable()     // Catch:{ all -> 0x007e }
            if (r5 != 0) goto L_0x0041
            boolean r5 = helper.Constants.debug()     // Catch:{ all -> 0x007e }
            if (r5 == 0) goto L_0x003b
            java.lang.String r5 = "SD Card unmounted or not present"
            helper.L.v(r5)     // Catch:{ all -> 0x007e }
        L_0x003b:
            r7.resetSyncWithWebDate(r8)     // Catch:{ all -> 0x007e }
            r5 = 0
        L_0x003f:
            monitor-exit(r7)
            return r5
        L_0x0041:
            r3 = 0
            java.io.File r5 = r2.getParentFile()     // Catch:{ IOException -> 0x006b, Exception -> 0x0085 }
            r5.mkdirs()     // Catch:{ IOException -> 0x006b, Exception -> 0x0085 }
            r2.createNewFile()     // Catch:{ IOException -> 0x006b, Exception -> 0x0085 }
            java.io.ObjectOutputStream r4 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x006b, Exception -> 0x0085 }
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x006b, Exception -> 0x0085 }
            r5.<init>(r2)     // Catch:{ IOException -> 0x006b, Exception -> 0x0085 }
            r4.<init>(r5)     // Catch:{ IOException -> 0x006b, Exception -> 0x0085 }
            r4.writeObject(r9)     // Catch:{ IOException -> 0x00a2, Exception -> 0x009e, all -> 0x009b }
            java.io.File r5 = new java.io.File     // Catch:{ IOException -> 0x00a2, Exception -> 0x009e, all -> 0x009b }
            java.lang.String r6 = "web_data.dat"
            r5.<init>(r1, r6)     // Catch:{ IOException -> 0x00a2, Exception -> 0x009e, all -> 0x009b }
            boolean r5 = r2.renameTo(r5)     // Catch:{ IOException -> 0x00a2, Exception -> 0x009e, all -> 0x009b }
            if (r5 == 0) goto L_0x0097
            helper.Global.closeOutputStream(r4)     // Catch:{ all -> 0x007e }
            r5 = 1
            goto L_0x003f
        L_0x006b:
            r5 = move-exception
            r0 = r5
        L_0x006d:
            boolean r5 = helper.Constants.debug()     // Catch:{ all -> 0x0079 }
            if (r5 == 0) goto L_0x0081
            java.lang.RuntimeException r5 = new java.lang.RuntimeException     // Catch:{ all -> 0x0079 }
            r5.<init>(r0)     // Catch:{ all -> 0x0079 }
            throw r5     // Catch:{ all -> 0x0079 }
        L_0x0079:
            r5 = move-exception
        L_0x007a:
            helper.Global.closeOutputStream(r3)     // Catch:{ all -> 0x007e }
            throw r5     // Catch:{ all -> 0x007e }
        L_0x007e:
            r5 = move-exception
            monitor-exit(r7)
            throw r5
        L_0x0081:
            helper.Global.closeOutputStream(r3)     // Catch:{ all -> 0x007e }
            goto L_0x003b
        L_0x0085:
            r5 = move-exception
            r0 = r5
        L_0x0087:
            boolean r5 = helper.Constants.debug()     // Catch:{ all -> 0x0079 }
            if (r5 == 0) goto L_0x0093
            java.lang.RuntimeException r5 = new java.lang.RuntimeException     // Catch:{ all -> 0x0079 }
            r5.<init>(r0)     // Catch:{ all -> 0x0079 }
            throw r5     // Catch:{ all -> 0x0079 }
        L_0x0093:
            helper.Global.closeOutputStream(r3)     // Catch:{ all -> 0x007e }
            goto L_0x003b
        L_0x0097:
            helper.Global.closeOutputStream(r4)     // Catch:{ all -> 0x007e }
            goto L_0x003b
        L_0x009b:
            r5 = move-exception
            r3 = r4
            goto L_0x007a
        L_0x009e:
            r5 = move-exception
            r0 = r5
            r3 = r4
            goto L_0x0087
        L_0x00a2:
            r5 = move-exception
            r0 = r5
            r3 = r4
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: data.MorePuzzleList.saveMorePuzzleMarketToSD(android.content.Context, java.util.List):boolean");
    }

    public synchronized void syncMorePuzzleMarketWithWebData(Context hContext, List<MorePuzzle> arrMorePuzzleMarketWeb) {
        if (Constants.debug()) {
            L.v("Sync more puzzle market data");
        }
        boolean bResyncWithWeb = false;
        Map<String, MorePuzzle> mapCurrentDataWithoutOrder = new HashMap<>();
        for (MorePuzzle hMorePuzzleMarketWebLoop : arrMorePuzzleMarketWeb) {
            if (!hMorePuzzleMarketWebLoop.isInstalled(hContext)) {
                String sIconPath = hMorePuzzleMarketWebLoop.getIconPath();
                if (TextUtils.isEmpty(sIconPath)) {
                    bResyncWithWeb = true;
                } else if (!new File(sIconPath).exists()) {
                    bResyncWithWeb = true;
                } else {
                    mapCurrentDataWithoutOrder.put(hMorePuzzleMarketWebLoop.getId(), hMorePuzzleMarketWebLoop);
                }
            }
        }
        if (bResyncWithWeb) {
            resetSyncWithWebDate(hContext);
        }
        synchCurrentDataWithOrder(mapCurrentDataWithoutOrder, new ArrayList<>(this.m_arrMorePuzzleMarketIntern), this.m_arrMorePuzzleMarketIntern);
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0077 A[Catch:{ all -> 0x00a9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x008a A[Catch:{ all -> 0x00a9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x009f A[Catch:{ all -> 0x00a9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00ba A[Catch:{ all -> 0x00a9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00c4 A[SYNTHETIC, Splitter:B:70:0x00c4] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:31:0x0071=Splitter:B:31:0x0071, B:41:0x0084=Splitter:B:41:0x0084, B:65:0x00b4=Splitter:B:65:0x00b4, B:53:0x0099=Splitter:B:53:0x0099} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized java.util.List<data.MorePuzzle> loadMorePuzzleMarketFromSD(android.content.Context r11) {
        /*
            r10 = this;
            r9 = 0
            monitor-enter(r10)
            int r7 = r10.m_iMarketId     // Catch:{ all -> 0x0094 }
            java.io.File r2 = getMorePuzzleDir(r11, r7)     // Catch:{ all -> 0x0094 }
            java.io.File r3 = new java.io.File     // Catch:{ all -> 0x0094 }
            java.lang.String r7 = "web_data.dat"
            r3.<init>(r2, r7)     // Catch:{ all -> 0x0094 }
            boolean r7 = helper.Constants.debug()     // Catch:{ all -> 0x0094 }
            if (r7 == 0) goto L_0x002b
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0094 }
            java.lang.String r8 = "Load more puzzle market list from SD: "
            r7.<init>(r8)     // Catch:{ all -> 0x0094 }
            java.lang.String r8 = r3.getAbsolutePath()     // Catch:{ all -> 0x0094 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0094 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0094 }
            helper.L.v(r7)     // Catch:{ all -> 0x0094 }
        L_0x002b:
            boolean r7 = helper.Global.isSDCardAvailable()     // Catch:{ all -> 0x0094 }
            if (r7 != 0) goto L_0x003f
            boolean r7 = helper.Constants.debug()     // Catch:{ all -> 0x0094 }
            if (r7 == 0) goto L_0x003c
            java.lang.String r7 = "SD Card unmounted or not present"
            helper.L.v(r7)     // Catch:{ all -> 0x0094 }
        L_0x003c:
            r7 = r9
        L_0x003d:
            monitor-exit(r10)
            return r7
        L_0x003f:
            boolean r7 = r3.exists()     // Catch:{ all -> 0x0094 }
            if (r7 != 0) goto L_0x0052
            boolean r7 = helper.Constants.debug()     // Catch:{ all -> 0x0094 }
            if (r7 == 0) goto L_0x0050
            java.lang.String r7 = "File does not exists"
            helper.L.v(r7)     // Catch:{ all -> 0x0094 }
        L_0x0050:
            r7 = r9
            goto L_0x003d
        L_0x0052:
            r5 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ ClassCastException -> 0x006f, ClassNotFoundException -> 0x0082, IOException -> 0x0097, Exception -> 0x00b2 }
            r4.<init>(r3)     // Catch:{ ClassCastException -> 0x006f, ClassNotFoundException -> 0x0082, IOException -> 0x0097, Exception -> 0x00b2 }
            java.io.ObjectInputStream r6 = new java.io.ObjectInputStream     // Catch:{ ClassCastException -> 0x006f, ClassNotFoundException -> 0x0082, IOException -> 0x0097, Exception -> 0x00b2 }
            java.io.BufferedInputStream r7 = new java.io.BufferedInputStream     // Catch:{ ClassCastException -> 0x006f, ClassNotFoundException -> 0x0082, IOException -> 0x0097, Exception -> 0x00b2 }
            r8 = 8192(0x2000, float:1.14794E-41)
            r7.<init>(r4, r8)     // Catch:{ ClassCastException -> 0x006f, ClassNotFoundException -> 0x0082, IOException -> 0x0097, Exception -> 0x00b2 }
            r6.<init>(r7)     // Catch:{ ClassCastException -> 0x006f, ClassNotFoundException -> 0x0082, IOException -> 0x0097, Exception -> 0x00b2 }
            java.lang.Object r0 = r6.readObject()     // Catch:{ ClassCastException -> 0x00d7, ClassNotFoundException -> 0x00d3, IOException -> 0x00cf, Exception -> 0x00cb, all -> 0x00c8 }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ ClassCastException -> 0x00d7, ClassNotFoundException -> 0x00d3, IOException -> 0x00cf, Exception -> 0x00cb, all -> 0x00c8 }
            helper.Global.closeInputStream(r6)     // Catch:{ all -> 0x0094 }
            r7 = r0
            goto L_0x003d
        L_0x006f:
            r7 = move-exception
            r1 = r7
        L_0x0071:
            boolean r7 = helper.Constants.debug()     // Catch:{ all -> 0x00a9 }
            if (r7 == 0) goto L_0x007a
            r1.printStackTrace()     // Catch:{ all -> 0x00a9 }
        L_0x007a:
            r3.delete()     // Catch:{ all -> 0x00a9 }
            helper.Global.closeInputStream(r5)     // Catch:{ all -> 0x0094 }
        L_0x0080:
            r7 = r9
            goto L_0x003d
        L_0x0082:
            r7 = move-exception
            r1 = r7
        L_0x0084:
            boolean r7 = helper.Constants.debug()     // Catch:{ all -> 0x00a9 }
            if (r7 == 0) goto L_0x008d
            r1.printStackTrace()     // Catch:{ all -> 0x00a9 }
        L_0x008d:
            r3.delete()     // Catch:{ all -> 0x00a9 }
            helper.Global.closeInputStream(r5)     // Catch:{ all -> 0x0094 }
            goto L_0x0080
        L_0x0094:
            r7 = move-exception
            monitor-exit(r10)
            throw r7
        L_0x0097:
            r7 = move-exception
            r1 = r7
        L_0x0099:
            boolean r7 = helper.Constants.debug()     // Catch:{ all -> 0x00a9 }
            if (r7 == 0) goto L_0x00ae
            java.lang.RuntimeException r7 = new java.lang.RuntimeException     // Catch:{ all -> 0x00a9 }
            java.lang.String r8 = r3.getAbsolutePath()     // Catch:{ all -> 0x00a9 }
            r7.<init>(r8, r1)     // Catch:{ all -> 0x00a9 }
            throw r7     // Catch:{ all -> 0x00a9 }
        L_0x00a9:
            r7 = move-exception
        L_0x00aa:
            helper.Global.closeInputStream(r5)     // Catch:{ all -> 0x0094 }
            throw r7     // Catch:{ all -> 0x0094 }
        L_0x00ae:
            helper.Global.closeInputStream(r5)     // Catch:{ all -> 0x0094 }
            goto L_0x0080
        L_0x00b2:
            r7 = move-exception
            r1 = r7
        L_0x00b4:
            boolean r7 = helper.Constants.debug()     // Catch:{ all -> 0x00a9 }
            if (r7 == 0) goto L_0x00c4
            java.lang.RuntimeException r7 = new java.lang.RuntimeException     // Catch:{ all -> 0x00a9 }
            java.lang.String r8 = r3.getAbsolutePath()     // Catch:{ all -> 0x00a9 }
            r7.<init>(r8, r1)     // Catch:{ all -> 0x00a9 }
            throw r7     // Catch:{ all -> 0x00a9 }
        L_0x00c4:
            helper.Global.closeInputStream(r5)     // Catch:{ all -> 0x0094 }
            goto L_0x0080
        L_0x00c8:
            r7 = move-exception
            r5 = r6
            goto L_0x00aa
        L_0x00cb:
            r7 = move-exception
            r1 = r7
            r5 = r6
            goto L_0x00b4
        L_0x00cf:
            r7 = move-exception
            r1 = r7
            r5 = r6
            goto L_0x0099
        L_0x00d3:
            r7 = move-exception
            r1 = r7
            r5 = r6
            goto L_0x0084
        L_0x00d7:
            r7 = move-exception
            r1 = r7
            r5 = r6
            goto L_0x0071
        */
        throw new UnsupportedOperationException("Method not decompiled: data.MorePuzzleList.loadMorePuzzleMarketFromSD(android.content.Context):java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:50:0x016a A[Catch:{ all -> 0x01a9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x017d A[Catch:{ all -> 0x01a9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0190 A[Catch:{ all -> 0x01a9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x01a0 A[Catch:{ all -> 0x01a9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x01ae  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x01bb A[Catch:{ all -> 0x01a9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x01c4 A[SYNTHETIC, Splitter:B:92:0x01c4] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:47:0x0164=Splitter:B:47:0x0164, B:87:0x01b5=Splitter:B:87:0x01b5, B:56:0x0177=Splitter:B:56:0x0177, B:67:0x018a=Splitter:B:67:0x018a, B:75:0x019a=Splitter:B:75:0x019a} */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:60:0x0180=Splitter:B:60:0x0180, B:71:0x0193=Splitter:B:71:0x0193, B:81:0x01aa=Splitter:B:81:0x01aa, B:51:0x016d=Splitter:B:51:0x016d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<data.MorePuzzle> loadMorePuzzleMarketFromWeb(android.content.Context r25) {
        /*
            r24 = this;
            monitor-enter(r24)
            boolean r21 = helper.Constants.debug()     // Catch:{ all -> 0x0184 }
            if (r21 == 0) goto L_0x000c
            java.lang.String r21 = "Load list from web"
            helper.L.v(r21)     // Catch:{ all -> 0x0184 }
        L_0x000c:
            java.util.HashMap r19 = new java.util.HashMap     // Catch:{ all -> 0x0184 }
            r19.<init>()     // Catch:{ all -> 0x0184 }
            java.util.ArrayList r11 = new java.util.ArrayList     // Catch:{ all -> 0x0184 }
            r11.<init>()     // Catch:{ all -> 0x0184 }
            r15 = 0
            java.net.URL r18 = new java.net.URL     // Catch:{ SocketTimeoutException -> 0x01e4, UnknownHostException -> 0x0174, ConnectException -> 0x0187, FileNotFoundException -> 0x0197, Exception -> 0x01b2 }
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x01e4, UnknownHostException -> 0x0174, ConnectException -> 0x0187, FileNotFoundException -> 0x0197, Exception -> 0x01b2 }
            java.lang.String r22 = data.MorePuzzleList.WEB_MORE_PUZZLE_MARKET_BASE_URL     // Catch:{ SocketTimeoutException -> 0x01e4, UnknownHostException -> 0x0174, ConnectException -> 0x0187, FileNotFoundException -> 0x0197, Exception -> 0x01b2 }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ SocketTimeoutException -> 0x01e4, UnknownHostException -> 0x0174, ConnectException -> 0x0187, FileNotFoundException -> 0x0197, Exception -> 0x01b2 }
            r21.<init>(r22)     // Catch:{ SocketTimeoutException -> 0x01e4, UnknownHostException -> 0x0174, ConnectException -> 0x0187, FileNotFoundException -> 0x0197, Exception -> 0x01b2 }
            r0 = r24
            int r0 = r0.m_iMarketId     // Catch:{ SocketTimeoutException -> 0x01e4, UnknownHostException -> 0x0174, ConnectException -> 0x0187, FileNotFoundException -> 0x0197, Exception -> 0x01b2 }
            r22 = r0
            java.lang.String r22 = helper.PuzzleConfig.getMarketFileNameWeb(r22)     // Catch:{ SocketTimeoutException -> 0x01e4, UnknownHostException -> 0x0174, ConnectException -> 0x0187, FileNotFoundException -> 0x0197, Exception -> 0x01b2 }
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ SocketTimeoutException -> 0x01e4, UnknownHostException -> 0x0174, ConnectException -> 0x0187, FileNotFoundException -> 0x0197, Exception -> 0x01b2 }
            java.lang.String r21 = r21.toString()     // Catch:{ SocketTimeoutException -> 0x01e4, UnknownHostException -> 0x0174, ConnectException -> 0x0187, FileNotFoundException -> 0x0197, Exception -> 0x01b2 }
            r0 = r18
            r1 = r21
            r0.<init>(r1)     // Catch:{ SocketTimeoutException -> 0x01e4, UnknownHostException -> 0x0174, ConnectException -> 0x0187, FileNotFoundException -> 0x0197, Exception -> 0x01b2 }
            java.io.InputStreamReader r17 = new java.io.InputStreamReader     // Catch:{ SocketTimeoutException -> 0x01e4, UnknownHostException -> 0x0174, ConnectException -> 0x0187, FileNotFoundException -> 0x0197, Exception -> 0x01b2 }
            java.io.InputStream r21 = r18.openStream()     // Catch:{ SocketTimeoutException -> 0x01e4, UnknownHostException -> 0x0174, ConnectException -> 0x0187, FileNotFoundException -> 0x0197, Exception -> 0x01b2 }
            r0 = r17
            r1 = r21
            r0.<init>(r1)     // Catch:{ SocketTimeoutException -> 0x01e4, UnknownHostException -> 0x0174, ConnectException -> 0x0187, FileNotFoundException -> 0x0197, Exception -> 0x01b2 }
            java.io.BufferedReader r16 = new java.io.BufferedReader     // Catch:{ SocketTimeoutException -> 0x01e4, UnknownHostException -> 0x0174, ConnectException -> 0x0187, FileNotFoundException -> 0x0197, Exception -> 0x01b2 }
            r21 = 8192(0x2000, float:1.14794E-41)
            r0 = r16
            r1 = r17
            r2 = r21
            r0.<init>(r1, r2)     // Catch:{ SocketTimeoutException -> 0x01e4, UnknownHostException -> 0x0174, ConnectException -> 0x0187, FileNotFoundException -> 0x0197, Exception -> 0x01b2 }
        L_0x0057:
            java.lang.String r20 = r16.readLine()     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            if (r20 != 0) goto L_0x0085
            boolean r21 = helper.Constants.debug()     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            if (r21 == 0) goto L_0x0068
            java.lang.String r21 = "Load list from web done"
            helper.L.v(r21)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
        L_0x0068:
            r0 = r24
            r1 = r25
            r2 = r11
            r0.cleanupIcons(r1, r2)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            java.util.ArrayList r10 = new java.util.ArrayList     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            java.util.Collection r21 = r19.values()     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            r0 = r10
            r1 = r21
            r0.<init>(r1)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            helper.Global.closeReader(r16)     // Catch:{ all -> 0x0184 }
            r15 = r16
            r21 = r10
        L_0x0083:
            monitor-exit(r24)
            return r21
        L_0x0085:
            java.lang.String r20 = r20.trim()     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            boolean r21 = android.text.TextUtils.isEmpty(r20)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            if (r21 != 0) goto L_0x0057
            java.lang.String r21 = "//"
            boolean r21 = r20.startsWith(r21)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            if (r21 != 0) goto L_0x0057
            java.lang.String r21 = "[|]"
            java.lang.String[] r12 = r20.split(r21)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            r0 = r12
            int r0 = r0.length     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            r21 = r0
            r22 = 5
            r0 = r21
            r1 = r22
            if (r0 < r1) goto L_0x0057
            r21 = 0
            r21 = r12[r21]     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            java.lang.String r5 = r21.trim()     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            boolean r21 = android.text.TextUtils.isEmpty(r5)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            if (r21 != 0) goto L_0x0057
            r21 = 1
            r21 = r12[r21]     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            java.lang.String r8 = r21.trim()     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            boolean r21 = android.text.TextUtils.isEmpty(r8)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            if (r21 != 0) goto L_0x0057
            r21 = 2
            r21 = r12[r21]     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            java.lang.String r6 = r21.trim()     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            boolean r21 = android.text.TextUtils.isEmpty(r6)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            if (r21 != 0) goto L_0x0057
            r21 = 3
            r21 = r12[r21]     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            java.lang.String r4 = r21.trim()     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            boolean r21 = android.text.TextUtils.isEmpty(r4)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            if (r21 != 0) goto L_0x0057
            r21 = 4
            r21 = r12[r21]     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            java.lang.String r7 = r21.trim()     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            boolean r21 = android.text.TextUtils.isEmpty(r7)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            if (r21 != 0) goto L_0x0057
            r0 = r19
            r1 = r7
            boolean r21 = r0.containsKey(r1)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            if (r21 != 0) goto L_0x0057
            java.io.File r14 = new java.io.File     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            r0 = r24
            int r0 = r0.m_iMarketId     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            r21 = r0
            r0 = r24
            r1 = r25
            r2 = r21
            java.io.File r21 = r0.getMorePuzzleIconDir(r1, r2)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            java.lang.String r23 = java.lang.String.valueOf(r4)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            r22.<init>(r23)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            r0 = r22
            r1 = r8
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            java.lang.String r22 = r22.toString()     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            r0 = r14
            r1 = r21
            r2 = r22
            r0.<init>(r1, r2)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            java.lang.String r9 = r14.getAbsolutePath()     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            data.MorePuzzle r3 = new data.MorePuzzle     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            r3.<init>(r4, r5, r6, r7, r8, r9)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            r0 = r3
            r1 = r25
            boolean r21 = r0.isInstalled(r1)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            if (r21 != 0) goto L_0x0057
            r11.add(r9)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            r0 = r19
            r1 = r7
            r2 = r3
            r0.put(r1, r2)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            boolean r21 = helper.Constants.debug()     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            if (r21 == 0) goto L_0x0057
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            java.lang.String r22 = "Parse: "
            r21.<init>(r22)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            r0 = r21
            r1 = r4
            java.lang.StringBuilder r21 = r0.append(r1)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            java.lang.String r21 = r21.toString()     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            helper.L.v(r21)     // Catch:{ SocketTimeoutException -> 0x015f, UnknownHostException -> 0x01de, ConnectException -> 0x01d8, FileNotFoundException -> 0x01d2, Exception -> 0x01cc, all -> 0x01c8 }
            goto L_0x0057
        L_0x015f:
            r21 = move-exception
            r13 = r21
            r15 = r16
        L_0x0164:
            boolean r21 = helper.Constants.debug()     // Catch:{ all -> 0x01a9 }
            if (r21 == 0) goto L_0x016d
            r13.printStackTrace()     // Catch:{ all -> 0x01a9 }
        L_0x016d:
            helper.Global.closeReader(r15)     // Catch:{ all -> 0x0184 }
        L_0x0170:
            r21 = 0
            goto L_0x0083
        L_0x0174:
            r21 = move-exception
            r13 = r21
        L_0x0177:
            boolean r21 = helper.Constants.debug()     // Catch:{ all -> 0x01a9 }
            if (r21 == 0) goto L_0x0180
            r13.printStackTrace()     // Catch:{ all -> 0x01a9 }
        L_0x0180:
            helper.Global.closeReader(r15)     // Catch:{ all -> 0x0184 }
            goto L_0x0170
        L_0x0184:
            r21 = move-exception
            monitor-exit(r24)
            throw r21
        L_0x0187:
            r21 = move-exception
            r13 = r21
        L_0x018a:
            boolean r21 = helper.Constants.debug()     // Catch:{ all -> 0x01a9 }
            if (r21 == 0) goto L_0x0193
            r13.printStackTrace()     // Catch:{ all -> 0x01a9 }
        L_0x0193:
            helper.Global.closeReader(r15)     // Catch:{ all -> 0x0184 }
            goto L_0x0170
        L_0x0197:
            r21 = move-exception
            r13 = r21
        L_0x019a:
            boolean r21 = helper.Constants.debug()     // Catch:{ all -> 0x01a9 }
            if (r21 == 0) goto L_0x01ae
            java.lang.RuntimeException r21 = new java.lang.RuntimeException     // Catch:{ all -> 0x01a9 }
            r0 = r21
            r1 = r13
            r0.<init>(r1)     // Catch:{ all -> 0x01a9 }
            throw r21     // Catch:{ all -> 0x01a9 }
        L_0x01a9:
            r21 = move-exception
        L_0x01aa:
            helper.Global.closeReader(r15)     // Catch:{ all -> 0x0184 }
            throw r21     // Catch:{ all -> 0x0184 }
        L_0x01ae:
            helper.Global.closeReader(r15)     // Catch:{ all -> 0x0184 }
            goto L_0x0170
        L_0x01b2:
            r21 = move-exception
            r13 = r21
        L_0x01b5:
            boolean r21 = helper.Constants.debug()     // Catch:{ all -> 0x01a9 }
            if (r21 == 0) goto L_0x01c4
            java.lang.RuntimeException r21 = new java.lang.RuntimeException     // Catch:{ all -> 0x01a9 }
            r0 = r21
            r1 = r13
            r0.<init>(r1)     // Catch:{ all -> 0x01a9 }
            throw r21     // Catch:{ all -> 0x01a9 }
        L_0x01c4:
            helper.Global.closeReader(r15)     // Catch:{ all -> 0x0184 }
            goto L_0x0170
        L_0x01c8:
            r21 = move-exception
            r15 = r16
            goto L_0x01aa
        L_0x01cc:
            r21 = move-exception
            r13 = r21
            r15 = r16
            goto L_0x01b5
        L_0x01d2:
            r21 = move-exception
            r13 = r21
            r15 = r16
            goto L_0x019a
        L_0x01d8:
            r21 = move-exception
            r13 = r21
            r15 = r16
            goto L_0x018a
        L_0x01de:
            r21 = move-exception
            r13 = r21
            r15 = r16
            goto L_0x0177
        L_0x01e4:
            r21 = move-exception
            r13 = r21
            goto L_0x0164
        */
        throw new UnsupportedOperationException("Method not decompiled: data.MorePuzzleList.loadMorePuzzleMarketFromWeb(android.content.Context):java.util.List");
    }

    private synchronized void cleanupIcons(Context hContext, List<String> arrMorePuzzleMarketIconFilePath) {
        if (Constants.debug()) {
            L.v("Clean up icon files");
        }
        if (Global.isSDCardAvailable()) {
            File[] arrFile = getMorePuzzleIconDir(hContext, this.m_iMarketId).listFiles();
            if (arrFile != null) {
                for (File hFileLoop : arrFile) {
                    if (!hFileLoop.isDirectory()) {
                        String sIconFilePath = hFileLoop.getName();
                        if (!arrMorePuzzleMarketIconFilePath.contains(hFileLoop.getAbsolutePath())) {
                            if (Constants.debug()) {
                                L.v("Delete icon: " + sIconFilePath);
                            }
                            hFileLoop.delete();
                        }
                    }
                }
            }
        } else if (Constants.debug()) {
            L.v(ResString.sd_card_unmounted);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x00fa A[Catch:{ all -> 0x0104 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x010f A[SYNTHETIC, Splitter:B:55:0x010f] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0120 A[Catch:{ all -> 0x0104 }] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x012a A[SYNTHETIC, Splitter:B:65:0x012a] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:35:0x00e2=Splitter:B:35:0x00e2, B:49:0x0105=Splitter:B:49:0x0105} */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:43:0x00f4=Splitter:B:43:0x00f4, B:60:0x011a=Splitter:B:60:0x011a} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean downloadIcon(android.content.Context r14, data.MorePuzzle r15) {
        /*
            r13 = this;
            monitor-enter(r13)
            java.lang.String r10 = r15.getIconPath()     // Catch:{ all -> 0x010c }
            java.io.File r6 = new java.io.File     // Catch:{ all -> 0x010c }
            r6.<init>(r10)     // Catch:{ all -> 0x010c }
            java.io.File r7 = new java.io.File     // Catch:{ all -> 0x010c }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x010c }
            java.lang.String r12 = java.lang.String.valueOf(r10)     // Catch:{ all -> 0x010c }
            r11.<init>(r12)     // Catch:{ all -> 0x010c }
            java.lang.String r12 = ".tmp"
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x010c }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x010c }
            r7.<init>(r11)     // Catch:{ all -> 0x010c }
            boolean r11 = r6.exists()     // Catch:{ all -> 0x010c }
            if (r11 == 0) goto L_0x0047
            boolean r11 = helper.Constants.debug()     // Catch:{ all -> 0x010c }
            if (r11 == 0) goto L_0x0044
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x010c }
            java.lang.String r12 = "Don't donwload, icon already exists: "
            r11.<init>(r12)     // Catch:{ all -> 0x010c }
            java.lang.String r12 = r15.getId()     // Catch:{ all -> 0x010c }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x010c }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x010c }
            helper.L.v(r11)     // Catch:{ all -> 0x010c }
        L_0x0044:
            r11 = 1
        L_0x0045:
            monitor-exit(r13)
            return r11
        L_0x0047:
            boolean r11 = helper.Constants.debug()     // Catch:{ all -> 0x010c }
            if (r11 == 0) goto L_0x0063
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x010c }
            java.lang.String r12 = "Download icon: "
            r11.<init>(r12)     // Catch:{ all -> 0x010c }
            java.lang.String r12 = r15.getId()     // Catch:{ all -> 0x010c }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x010c }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x010c }
            helper.L.v(r11)     // Catch:{ all -> 0x010c }
        L_0x0063:
            boolean r11 = helper.Global.isSDCardAvailable()     // Catch:{ all -> 0x010c }
            if (r11 != 0) goto L_0x0076
            boolean r11 = helper.Constants.debug()     // Catch:{ all -> 0x010c }
            if (r11 == 0) goto L_0x0074
            java.lang.String r11 = "SD Card unmounted or not present"
            helper.L.v(r11)     // Catch:{ all -> 0x010c }
        L_0x0074:
            r11 = 0
            goto L_0x0045
        L_0x0076:
            r2 = 0
            r4 = 0
            java.io.File r11 = r7.getParentFile()     // Catch:{ IOException -> 0x014a, Exception -> 0x0118 }
            r11.mkdirs()     // Catch:{ IOException -> 0x014a, Exception -> 0x0118 }
            r7.createNewFile()     // Catch:{ IOException -> 0x014a, Exception -> 0x0118 }
            java.net.URL r8 = new java.net.URL     // Catch:{ IOException -> 0x014a, Exception -> 0x0118 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x014a, Exception -> 0x0118 }
            java.lang.String r12 = data.MorePuzzleList.WEB_MORE_PUZZLE_MARKET_BASE_URL     // Catch:{ IOException -> 0x014a, Exception -> 0x0118 }
            java.lang.String r12 = java.lang.String.valueOf(r12)     // Catch:{ IOException -> 0x014a, Exception -> 0x0118 }
            r11.<init>(r12)     // Catch:{ IOException -> 0x014a, Exception -> 0x0118 }
            java.lang.String r12 = r15.getId()     // Catch:{ IOException -> 0x014a, Exception -> 0x0118 }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ IOException -> 0x014a, Exception -> 0x0118 }
            java.lang.String r11 = r11.toString()     // Catch:{ IOException -> 0x014a, Exception -> 0x0118 }
            r8.<init>(r11)     // Catch:{ IOException -> 0x014a, Exception -> 0x0118 }
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x014a, Exception -> 0x0118 }
            java.io.InputStream r11 = r8.openStream()     // Catch:{ IOException -> 0x014a, Exception -> 0x0118 }
            r12 = 8192(0x2000, float:1.14794E-41)
            r3.<init>(r11, r12)     // Catch:{ IOException -> 0x014a, Exception -> 0x0118 }
            java.io.BufferedOutputStream r5 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x014d, Exception -> 0x0141, all -> 0x013a }
            java.io.FileOutputStream r11 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x014d, Exception -> 0x0141, all -> 0x013a }
            r11.<init>(r7)     // Catch:{ IOException -> 0x014d, Exception -> 0x0141, all -> 0x013a }
            r12 = 8192(0x2000, float:1.14794E-41)
            r5.<init>(r11, r12)     // Catch:{ IOException -> 0x014d, Exception -> 0x0141, all -> 0x013a }
            r11 = 8192(0x2000, float:1.14794E-41)
            byte[] r0 = new byte[r11]     // Catch:{ IOException -> 0x00f0, Exception -> 0x0145, all -> 0x013d }
        L_0x00b9:
            int r9 = r3.read(r0)     // Catch:{ IOException -> 0x00f0, Exception -> 0x0145, all -> 0x013d }
            r11 = -1
            if (r9 != r11) goto L_0x00eb
            boolean r11 = r7.renameTo(r6)     // Catch:{ IOException -> 0x00f0, Exception -> 0x0145, all -> 0x013d }
            if (r11 == 0) goto L_0x0131
            boolean r11 = helper.Constants.debug()     // Catch:{ IOException -> 0x00f0, Exception -> 0x0145, all -> 0x013d }
            if (r11 == 0) goto L_0x00e2
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00f0, Exception -> 0x0145, all -> 0x013d }
            java.lang.String r12 = "Download icon succeeded: "
            r11.<init>(r12)     // Catch:{ IOException -> 0x00f0, Exception -> 0x0145, all -> 0x013d }
            java.lang.String r12 = r6.getAbsolutePath()     // Catch:{ IOException -> 0x00f0, Exception -> 0x0145, all -> 0x013d }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ IOException -> 0x00f0, Exception -> 0x0145, all -> 0x013d }
            java.lang.String r11 = r11.toString()     // Catch:{ IOException -> 0x00f0, Exception -> 0x0145, all -> 0x013d }
            helper.L.v(r11)     // Catch:{ IOException -> 0x00f0, Exception -> 0x0145, all -> 0x013d }
        L_0x00e2:
            helper.Global.closeInputStream(r3)     // Catch:{ all -> 0x010c }
            helper.Global.closeOutputStream(r5)     // Catch:{ all -> 0x010c }
            r11 = 1
            goto L_0x0045
        L_0x00eb:
            r11 = 0
            r5.write(r0, r11, r9)     // Catch:{ IOException -> 0x00f0, Exception -> 0x0145, all -> 0x013d }
            goto L_0x00b9
        L_0x00f0:
            r11 = move-exception
            r1 = r11
            r4 = r5
            r2 = r3
        L_0x00f4:
            boolean r11 = helper.Constants.debug()     // Catch:{ all -> 0x0104 }
            if (r11 == 0) goto L_0x010f
            java.lang.RuntimeException r11 = new java.lang.RuntimeException     // Catch:{ all -> 0x0104 }
            java.lang.String r12 = r7.getAbsolutePath()     // Catch:{ all -> 0x0104 }
            r11.<init>(r12, r1)     // Catch:{ all -> 0x0104 }
            throw r11     // Catch:{ all -> 0x0104 }
        L_0x0104:
            r11 = move-exception
        L_0x0105:
            helper.Global.closeInputStream(r2)     // Catch:{ all -> 0x010c }
            helper.Global.closeOutputStream(r4)     // Catch:{ all -> 0x010c }
            throw r11     // Catch:{ all -> 0x010c }
        L_0x010c:
            r11 = move-exception
            monitor-exit(r13)
            throw r11
        L_0x010f:
            helper.Global.closeInputStream(r2)     // Catch:{ all -> 0x010c }
            helper.Global.closeOutputStream(r4)     // Catch:{ all -> 0x010c }
        L_0x0115:
            r11 = 0
            goto L_0x0045
        L_0x0118:
            r11 = move-exception
            r1 = r11
        L_0x011a:
            boolean r11 = helper.Constants.debug()     // Catch:{ all -> 0x0104 }
            if (r11 == 0) goto L_0x012a
            java.lang.RuntimeException r11 = new java.lang.RuntimeException     // Catch:{ all -> 0x0104 }
            java.lang.String r12 = r7.getAbsolutePath()     // Catch:{ all -> 0x0104 }
            r11.<init>(r12, r1)     // Catch:{ all -> 0x0104 }
            throw r11     // Catch:{ all -> 0x0104 }
        L_0x012a:
            helper.Global.closeInputStream(r2)     // Catch:{ all -> 0x010c }
            helper.Global.closeOutputStream(r4)     // Catch:{ all -> 0x010c }
            goto L_0x0115
        L_0x0131:
            helper.Global.closeInputStream(r3)     // Catch:{ all -> 0x010c }
            helper.Global.closeOutputStream(r5)     // Catch:{ all -> 0x010c }
            r4 = r5
            r2 = r3
            goto L_0x0115
        L_0x013a:
            r11 = move-exception
            r2 = r3
            goto L_0x0105
        L_0x013d:
            r11 = move-exception
            r4 = r5
            r2 = r3
            goto L_0x0105
        L_0x0141:
            r11 = move-exception
            r1 = r11
            r2 = r3
            goto L_0x011a
        L_0x0145:
            r11 = move-exception
            r1 = r11
            r4 = r5
            r2 = r3
            goto L_0x011a
        L_0x014a:
            r11 = move-exception
            r1 = r11
            goto L_0x00f4
        L_0x014d:
            r11 = move-exception
            r1 = r11
            r2 = r3
            goto L_0x00f4
        */
        throw new UnsupportedOperationException("Method not decompiled: data.MorePuzzleList.downloadIcon(android.content.Context, data.MorePuzzle):boolean");
    }

    public synchronized boolean isEveryIconDownloaded(Context hContext, List<MorePuzzle> arrMorePuzzleMarketWeb) {
        boolean z;
        if (Constants.debug()) {
            L.v("Check if every icon is downloaded");
        }
        if (Global.isSDCardAvailable()) {
            File hDir = getMorePuzzleIconDir(hContext, this.m_iMarketId);
            Iterator<MorePuzzle> it = arrMorePuzzleMarketWeb.iterator();
            while (true) {
                if (it.hasNext()) {
                    MorePuzzle hMorePuzzleMarketWebLoop = it.next();
                    if (!new File(hDir, String.valueOf(hMorePuzzleMarketWebLoop.getId()) + hMorePuzzleMarketWebLoop.getIconVersion()).exists()) {
                        z = false;
                        break;
                    }
                } else {
                    z = true;
                    break;
                }
            }
        } else {
            if (Constants.debug()) {
                L.v(ResString.sd_card_unmounted);
            }
            z = false;
        }
        return z;
    }

    public boolean addMorePuzzleMarketFromWeb(Context hContext, MorePuzzle hMorePuzzleMarket) {
        if (hMorePuzzleMarket.isInstalled(hContext)) {
            return false;
        }
        String sMorePuzzleMarketId = hMorePuzzleMarket.getId();
        for (MorePuzzle hMorePuzzleMarketLoop : this.m_arrMorePuzzleMarketIntern) {
            if (sMorePuzzleMarketId.equals(hMorePuzzleMarketLoop.getId())) {
                return false;
            }
        }
        this.m_arrMorePuzzleMarketIntern.add(hMorePuzzleMarket);
        return true;
    }

    public int size() {
        return this.m_arrMorePuzzleInstalledExtern.size() + 2 + this.m_arrMorePuzzleMarketExtern.size();
    }

    public String getSeparatorTitle(int iIndex) {
        if (iIndex == 0) {
            return this.m_sSeparatorInstalled;
        }
        return this.m_sSeparatorMarket;
    }

    public int getItemViewType(int iIndex) {
        if (iIndex == 0) {
            return 0;
        }
        if (iIndex == this.m_arrMorePuzzleInstalledExtern.size() + 1) {
            return 0;
        }
        return 1;
    }

    public boolean isEnabled(int iIndex) {
        switch (getItemViewType(iIndex)) {
            case 0:
                return false;
            case 1:
                return true;
            default:
                throw new RuntimeException("Invalid index");
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public MorePuzzle get(int iIndex) {
        int iIndex2 = iIndex - 1;
        if (iIndex2 < 0) {
            return null;
        }
        int iSizeInstalled = this.m_arrMorePuzzleInstalledExtern.size();
        if (iIndex2 < iSizeInstalled) {
            return this.m_arrMorePuzzleInstalledExtern.get(iIndex2);
        }
        int iIndex3 = (iIndex2 - 1) - iSizeInstalled;
        if (iIndex3 < 0) {
            return null;
        }
        if (iIndex3 < this.m_arrMorePuzzleMarketExtern.size()) {
            return this.m_arrMorePuzzleMarketExtern.get(iIndex3);
        }
        return null;
    }

    public void update() {
        this.m_arrMorePuzzleInstalledExtern.clear();
        for (MorePuzzle hMorePuzzleInstalledLoop : this.m_arrMorePuzzleInstalledIntern) {
            this.m_arrMorePuzzleInstalledExtern.add(hMorePuzzleInstalledLoop);
        }
        this.m_arrMorePuzzleMarketExtern.clear();
        for (MorePuzzle hMorePuzzleMarketLoop : this.m_arrMorePuzzleMarketIntern) {
            this.m_arrMorePuzzleMarketExtern.add(hMorePuzzleMarketLoop);
        }
    }

    private synchronized File getMorePuzzleIconDir(Context hContext, int iMarketId) {
        File hDir;
        hDir = new File(getMorePuzzleDir(hContext, iMarketId), "icon");
        hDir.mkdirs();
        return hDir;
    }

    /* access modifiers changed from: private */
    public static synchronized File getMorePuzzleDir(Context hContext, int iMarketId) {
        File hDir;
        synchronized (MorePuzzleList.class) {
            hDir = new File(String.valueOf(PuzzleConfig.ROOT_DIR_HIDDEN) + MORE_PUZZLE_DIR + "/" + PuzzleConfig.getMarketDisplayName(iMarketId));
            hDir.mkdirs();
        }
        return hDir;
    }

    public static final class MorePuzzleProgress {
        private static final int BUFFER_SIZE = 8192;
        private static final String PROGRESS_DATA_FILE_NAME = "progress_data.dat";
        private static ProgressData s_hProgressData;

        private static final class ProgressData implements Serializable {
            private static final long serialVersionUID = -6165992871652814258L;
            public transient int iMarketId;
            public final ConcurrentMap<String, Integer> mapImageCount;
            public final ConcurrentMap<String, Integer> mapImageIndexNextUnsolved;

            private ProgressData() {
                this.mapImageCount = new ConcurrentHashMap();
                this.mapImageIndexNextUnsolved = new ConcurrentHashMap();
            }

            /* synthetic */ ProgressData(ProgressData progressData) {
                this();
            }
        }

        private static synchronized void init(Context hContext) {
            synchronized (MorePuzzleProgress.class) {
                if (s_hProgressData == null) {
                    int iMarketId = PuzzleConfig.getMarketId(hContext).intValue();
                    s_hProgressData = loadProgressData(hContext, iMarketId);
                    s_hProgressData.iMarketId = iMarketId;
                }
            }
        }

        public static synchronized int getImageCount(Context hContext, String sPuzzleId) {
            int intValue;
            synchronized (MorePuzzleProgress.class) {
                init(hContext);
                Integer iCount = s_hProgressData.mapImageCount.get(sPuzzleId);
                if (iCount == null) {
                    intValue = 0;
                } else {
                    intValue = iCount.intValue();
                }
            }
            return intValue;
        }

        public static synchronized void setImageCount(Context hContext, String sPuzzleId, int iImageCount) {
            synchronized (MorePuzzleProgress.class) {
                init(hContext);
                s_hProgressData.mapImageCount.put(sPuzzleId, Integer.valueOf(iImageCount));
                if (Constants.debug()) {
                    L.v("Set image count and save progress data to disc");
                }
            }
        }

        public static synchronized int getImageIndexNextUnsolved(Context hContext, String sPuzzleId) {
            int intValue;
            synchronized (MorePuzzleProgress.class) {
                init(hContext);
                Integer iImageIndexNextUnsovled = s_hProgressData.mapImageIndexNextUnsolved.get(sPuzzleId);
                if (iImageIndexNextUnsovled == null) {
                    intValue = 0;
                } else {
                    intValue = iImageIndexNextUnsovled.intValue();
                }
            }
            return intValue;
        }

        public static synchronized void setImageIndexNextUnsolved(Context hContext, String sPuzzleId, int iImageIndexNextUnsolved) {
            synchronized (MorePuzzleProgress.class) {
                init(hContext);
                s_hProgressData.mapImageIndexNextUnsolved.put(sPuzzleId, Integer.valueOf(iImageIndexNextUnsolved));
                if (Constants.debug()) {
                    L.v("Set image index next unsolved and save progress data to disc");
                }
            }
        }

        public static synchronized boolean removeNotInstalledData(Set<String> setPuzzleIdInstalled) {
            boolean bSave;
            synchronized (MorePuzzleProgress.class) {
                bSave = false;
                for (String sPuzzleIdLoop : s_hProgressData.mapImageCount.keySet()) {
                    if (!setPuzzleIdInstalled.contains(sPuzzleIdLoop)) {
                        s_hProgressData.mapImageCount.remove(sPuzzleIdLoop);
                        s_hProgressData.mapImageIndexNextUnsolved.remove(sPuzzleIdLoop);
                        bSave = true;
                    }
                }
            }
            return bSave;
        }

        /* JADX WARNING: Removed duplicated region for block: B:39:0x008b A[Catch:{ all -> 0x00b5 }] */
        /* JADX WARNING: Removed duplicated region for block: B:49:0x00a3 A[Catch:{ all -> 0x00b5 }] */
        /* JADX WARNING: Unknown top exception splitter block from list: {B:36:0x0085=Splitter:B:36:0x0085, B:46:0x009d=Splitter:B:46:0x009d, B:55:0x00af=Splitter:B:55:0x00af} */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private static synchronized data.MorePuzzleList.MorePuzzleProgress.ProgressData loadProgressData(android.content.Context r10, int r11) {
            /*
                java.lang.Class<data.MorePuzzleList$MorePuzzleProgress> r7 = data.MorePuzzleList.MorePuzzleProgress.class
                monitor-enter(r7)
                java.io.File r1 = data.MorePuzzleList.getMorePuzzleDir(r10, r11)     // Catch:{ all -> 0x0043 }
                java.io.File r2 = new java.io.File     // Catch:{ all -> 0x0043 }
                java.lang.String r8 = "progress_data.dat"
                r2.<init>(r1, r8)     // Catch:{ all -> 0x0043 }
                boolean r8 = helper.Constants.debug()     // Catch:{ all -> 0x0043 }
                if (r8 == 0) goto L_0x002a
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0043 }
                java.lang.String r9 = "Load puzzle progress data from SD: "
                r8.<init>(r9)     // Catch:{ all -> 0x0043 }
                java.lang.String r9 = r2.getAbsolutePath()     // Catch:{ all -> 0x0043 }
                java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0043 }
                java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0043 }
                helper.L.v(r8)     // Catch:{ all -> 0x0043 }
            L_0x002a:
                boolean r8 = helper.Global.isSDCardAvailable()     // Catch:{ all -> 0x0043 }
                if (r8 != 0) goto L_0x004e
                boolean r8 = helper.Constants.debug()     // Catch:{ all -> 0x0043 }
                if (r8 == 0) goto L_0x0046
                java.lang.String r8 = "SD Card unmounted or not present"
                helper.L.v(r8)     // Catch:{ all -> 0x0043 }
                java.lang.RuntimeException r8 = new java.lang.RuntimeException     // Catch:{ all -> 0x0043 }
                java.lang.String r9 = "Shouldn't reach here!"
                r8.<init>(r9)     // Catch:{ all -> 0x0043 }
                throw r8     // Catch:{ all -> 0x0043 }
            L_0x0043:
                r8 = move-exception
                monitor-exit(r7)
                throw r8
            L_0x0046:
                data.MorePuzzleList$MorePuzzleProgress$ProgressData r8 = new data.MorePuzzleList$MorePuzzleProgress$ProgressData     // Catch:{ all -> 0x0043 }
                r9 = 0
                r8.<init>(r9)     // Catch:{ all -> 0x0043 }
            L_0x004c:
                monitor-exit(r7)
                return r8
            L_0x004e:
                boolean r8 = r2.exists()     // Catch:{ all -> 0x0043 }
                if (r8 != 0) goto L_0x0066
                boolean r8 = helper.Constants.debug()     // Catch:{ all -> 0x0043 }
                if (r8 == 0) goto L_0x005f
                java.lang.String r8 = "Puzzle progress data file not exists, create new one"
                helper.L.v(r8)     // Catch:{ all -> 0x0043 }
            L_0x005f:
                data.MorePuzzleList$MorePuzzleProgress$ProgressData r8 = new data.MorePuzzleList$MorePuzzleProgress$ProgressData     // Catch:{ all -> 0x0043 }
                r9 = 0
                r8.<init>(r9)     // Catch:{ all -> 0x0043 }
                goto L_0x004c
            L_0x0066:
                r4 = 0
                java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ ClassCastException -> 0x0083, ClassNotFoundException -> 0x009b, Exception -> 0x00ad }
                r3.<init>(r2)     // Catch:{ ClassCastException -> 0x0083, ClassNotFoundException -> 0x009b, Exception -> 0x00ad }
                java.io.ObjectInputStream r5 = new java.io.ObjectInputStream     // Catch:{ ClassCastException -> 0x0083, ClassNotFoundException -> 0x009b, Exception -> 0x00ad }
                java.io.BufferedInputStream r8 = new java.io.BufferedInputStream     // Catch:{ ClassCastException -> 0x0083, ClassNotFoundException -> 0x009b, Exception -> 0x00ad }
                r9 = 8192(0x2000, float:1.14794E-41)
                r8.<init>(r3, r9)     // Catch:{ ClassCastException -> 0x0083, ClassNotFoundException -> 0x009b, Exception -> 0x00ad }
                r5.<init>(r8)     // Catch:{ ClassCastException -> 0x0083, ClassNotFoundException -> 0x009b, Exception -> 0x00ad }
                java.lang.Object r6 = r5.readObject()     // Catch:{ ClassCastException -> 0x00c5, ClassNotFoundException -> 0x00c1, Exception -> 0x00bd, all -> 0x00ba }
                data.MorePuzzleList$MorePuzzleProgress$ProgressData r6 = (data.MorePuzzleList.MorePuzzleProgress.ProgressData) r6     // Catch:{ ClassCastException -> 0x00c5, ClassNotFoundException -> 0x00c1, Exception -> 0x00bd, all -> 0x00ba }
                helper.Global.closeInputStream(r5)     // Catch:{ all -> 0x0043 }
                r8 = r6
                goto L_0x004c
            L_0x0083:
                r8 = move-exception
                r0 = r8
            L_0x0085:
                boolean r8 = helper.Constants.debug()     // Catch:{ all -> 0x00b5 }
                if (r8 == 0) goto L_0x008e
                r0.printStackTrace()     // Catch:{ all -> 0x00b5 }
            L_0x008e:
                r2.delete()     // Catch:{ all -> 0x00b5 }
                helper.Global.closeInputStream(r4)     // Catch:{ all -> 0x0043 }
            L_0x0094:
                data.MorePuzzleList$MorePuzzleProgress$ProgressData r8 = new data.MorePuzzleList$MorePuzzleProgress$ProgressData     // Catch:{ all -> 0x0043 }
                r9 = 0
                r8.<init>(r9)     // Catch:{ all -> 0x0043 }
                goto L_0x004c
            L_0x009b:
                r8 = move-exception
                r0 = r8
            L_0x009d:
                boolean r8 = helper.Constants.debug()     // Catch:{ all -> 0x00b5 }
                if (r8 == 0) goto L_0x00a6
                r0.printStackTrace()     // Catch:{ all -> 0x00b5 }
            L_0x00a6:
                r2.delete()     // Catch:{ all -> 0x00b5 }
                helper.Global.closeInputStream(r4)     // Catch:{ all -> 0x0043 }
                goto L_0x0094
            L_0x00ad:
                r8 = move-exception
                r0 = r8
            L_0x00af:
                java.lang.RuntimeException r8 = new java.lang.RuntimeException     // Catch:{ all -> 0x00b5 }
                r8.<init>(r0)     // Catch:{ all -> 0x00b5 }
                throw r8     // Catch:{ all -> 0x00b5 }
            L_0x00b5:
                r8 = move-exception
            L_0x00b6:
                helper.Global.closeInputStream(r4)     // Catch:{ all -> 0x0043 }
                throw r8     // Catch:{ all -> 0x0043 }
            L_0x00ba:
                r8 = move-exception
                r4 = r5
                goto L_0x00b6
            L_0x00bd:
                r8 = move-exception
                r0 = r8
                r4 = r5
                goto L_0x00af
            L_0x00c1:
                r8 = move-exception
                r0 = r8
                r4 = r5
                goto L_0x009d
            L_0x00c5:
                r8 = move-exception
                r0 = r8
                r4 = r5
                goto L_0x0085
            */
            throw new UnsupportedOperationException("Method not decompiled: data.MorePuzzleList.MorePuzzleProgress.loadProgressData(android.content.Context, int):data.MorePuzzleList$MorePuzzleProgress$ProgressData");
        }

        /* JADX WARNING: Removed duplicated region for block: B:29:0x0076 A[Catch:{ all -> 0x0092 }] */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x008c A[Catch:{ all -> 0x0092 }] */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x0097  */
        /* JADX WARNING: Unknown top exception splitter block from list: {B:26:0x0070=Splitter:B:26:0x0070, B:37:0x0086=Splitter:B:37:0x0086} */
        /* JADX WARNING: Unknown top exception splitter block from list: {B:43:0x0093=Splitter:B:43:0x0093, B:30:0x007d=Splitter:B:30:0x007d} */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static synchronized void saveProgressData(android.content.Context r8) {
            /*
                java.lang.Class<data.MorePuzzleList$MorePuzzleProgress> r5 = data.MorePuzzleList.MorePuzzleProgress.class
                monitor-enter(r5)
                data.MorePuzzleList$MorePuzzleProgress$ProgressData r6 = data.MorePuzzleList.MorePuzzleProgress.s_hProgressData     // Catch:{ all -> 0x0081 }
                int r6 = r6.iMarketId     // Catch:{ all -> 0x0081 }
                java.io.File r1 = data.MorePuzzleList.getMorePuzzleDir(r8, r6)     // Catch:{ all -> 0x0081 }
                java.io.File r2 = new java.io.File     // Catch:{ all -> 0x0081 }
                java.lang.String r6 = "progress_data.dat.tmp"
                r2.<init>(r1, r6)     // Catch:{ all -> 0x0081 }
                boolean r6 = helper.Constants.debug()     // Catch:{ all -> 0x0081 }
                if (r6 == 0) goto L_0x002e
                java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0081 }
                java.lang.String r7 = "Save puzzle progress data to SD: "
                r6.<init>(r7)     // Catch:{ all -> 0x0081 }
                java.lang.String r7 = r2.getAbsolutePath()     // Catch:{ all -> 0x0081 }
                java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0081 }
                java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0081 }
                helper.L.v(r6)     // Catch:{ all -> 0x0081 }
            L_0x002e:
                boolean r6 = helper.Global.isSDCardAvailable()     // Catch:{ all -> 0x0081 }
                if (r6 != 0) goto L_0x0041
                boolean r6 = helper.Constants.debug()     // Catch:{ all -> 0x0081 }
                if (r6 == 0) goto L_0x003f
                java.lang.String r6 = "SD Card unmounted or not present"
                helper.L.v(r6)     // Catch:{ all -> 0x0081 }
            L_0x003f:
                monitor-exit(r5)
                return
            L_0x0041:
                data.MorePuzzleList$MorePuzzleProgress$ProgressData r6 = data.MorePuzzleList.MorePuzzleProgress.s_hProgressData     // Catch:{ all -> 0x0081 }
                if (r6 == 0) goto L_0x003f
                r3 = 0
                java.io.File r6 = r2.getParentFile()     // Catch:{ IOException -> 0x006e, Exception -> 0x0084 }
                r6.mkdirs()     // Catch:{ IOException -> 0x006e, Exception -> 0x0084 }
                r2.createNewFile()     // Catch:{ IOException -> 0x006e, Exception -> 0x0084 }
                java.io.ObjectOutputStream r4 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x006e, Exception -> 0x0084 }
                java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x006e, Exception -> 0x0084 }
                r6.<init>(r2)     // Catch:{ IOException -> 0x006e, Exception -> 0x0084 }
                r4.<init>(r6)     // Catch:{ IOException -> 0x006e, Exception -> 0x0084 }
                data.MorePuzzleList$MorePuzzleProgress$ProgressData r6 = data.MorePuzzleList.MorePuzzleProgress.s_hProgressData     // Catch:{ IOException -> 0x00a2, Exception -> 0x009e, all -> 0x009b }
                r4.writeObject(r6)     // Catch:{ IOException -> 0x00a2, Exception -> 0x009e, all -> 0x009b }
                java.io.File r6 = new java.io.File     // Catch:{ IOException -> 0x00a2, Exception -> 0x009e, all -> 0x009b }
                java.lang.String r7 = "progress_data.dat"
                r6.<init>(r1, r7)     // Catch:{ IOException -> 0x00a2, Exception -> 0x009e, all -> 0x009b }
                r2.renameTo(r6)     // Catch:{ IOException -> 0x00a2, Exception -> 0x009e, all -> 0x009b }
                helper.Global.closeOutputStream(r4)     // Catch:{ all -> 0x0081 }
                r3 = r4
                goto L_0x003f
            L_0x006e:
                r6 = move-exception
                r0 = r6
            L_0x0070:
                boolean r6 = helper.Constants.debug()     // Catch:{ all -> 0x0092 }
                if (r6 == 0) goto L_0x007d
                java.lang.String r6 = r2.getAbsolutePath()     // Catch:{ all -> 0x0092 }
                helper.L.e(r6, r0)     // Catch:{ all -> 0x0092 }
            L_0x007d:
                helper.Global.closeOutputStream(r3)     // Catch:{ all -> 0x0081 }
                goto L_0x003f
            L_0x0081:
                r6 = move-exception
                monitor-exit(r5)
                throw r6
            L_0x0084:
                r6 = move-exception
                r0 = r6
            L_0x0086:
                boolean r6 = helper.Constants.debug()     // Catch:{ all -> 0x0092 }
                if (r6 == 0) goto L_0x0097
                java.lang.RuntimeException r6 = new java.lang.RuntimeException     // Catch:{ all -> 0x0092 }
                r6.<init>(r0)     // Catch:{ all -> 0x0092 }
                throw r6     // Catch:{ all -> 0x0092 }
            L_0x0092:
                r6 = move-exception
            L_0x0093:
                helper.Global.closeOutputStream(r3)     // Catch:{ all -> 0x0081 }
                throw r6     // Catch:{ all -> 0x0081 }
            L_0x0097:
                helper.Global.closeOutputStream(r3)     // Catch:{ all -> 0x0081 }
                goto L_0x003f
            L_0x009b:
                r6 = move-exception
                r3 = r4
                goto L_0x0093
            L_0x009e:
                r6 = move-exception
                r0 = r6
                r3 = r4
                goto L_0x0086
            L_0x00a2:
                r6 = move-exception
                r0 = r6
                r3 = r4
                goto L_0x0070
            */
            throw new UnsupportedOperationException("Method not decompiled: data.MorePuzzleList.MorePuzzleProgress.saveProgressData(android.content.Context):void");
        }
    }
}
