package com.zestadz.android;

import java.util.ArrayList;
import java.util.List;

public class ParsedExampleDataSet {
    static List<String> ExtractedError = new ArrayList();
    static List<String> extractedAdType = new ArrayList();
    static List<String> extractedPicture = new ArrayList();
    static List<String> extractedText = new ArrayList();
    static List<String> extractedURL = new ArrayList();

    public static List<String> getExtractedText() {
        return extractedText;
    }

    public void setExtractedAdType(String extractedAdType2) {
        extractedAdType.add(extractedAdType2);
    }

    public static List<String> getExtractedAdType() {
        return extractedAdType;
    }

    public void setExtractedText(String extractedText2) {
        extractedText.add(extractedText2);
    }

    public static List<String> getExtractedPicture() {
        return extractedPicture;
    }

    public void setExtractedPicture(String extractedPicture2) {
        extractedPicture.add(extractedPicture2);
    }

    public static List<String> getExtractedURL() {
        return extractedURL;
    }

    public void setExtractedURL(String extractedURL2) {
        extractedURL.add(extractedURL2);
    }

    public static List<String> getExtractedError() {
        return ExtractedError;
    }

    public void setExtractedError(String ExtractedError2) {
        ExtractedError.add(ExtractedError2);
    }
}
