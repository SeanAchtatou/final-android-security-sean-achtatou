package dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.SweepGradient;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import cfg.Option;
import res.ResDimens;
import res.ResString;
import view.ButtonContainer;
import view.TitleView;

public final class DialogScreenHintColor extends DialogScreenBase {
    private static final int BUTTON_ID_CANCEL = 0;
    private static final int BUTTON_ID_HINT_DISABLE = 1;
    private static final int BUTTON_ID_OK = 2;
    /* access modifiers changed from: private */
    public OnColorChangedListener mListener;
    /* access modifiers changed from: private */
    public final ColorPickerView m_hColorPicker;
    private final View.OnClickListener m_hOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case 0:
                    DialogScreenHintColor.this.m_hDialogManager.dismiss();
                    return;
                case 1:
                    DialogScreenHintColor.this.mListener.colorChanged(-1);
                    DialogScreenHintColor.this.m_hDialogManager.dismiss();
                    return;
                case 2:
                    DialogScreenHintColor.this.mListener.colorChanged(DialogScreenHintColor.this.m_hColorPicker.getSelectedColor());
                    DialogScreenHintColor.this.m_hDialogManager.dismiss();
                    return;
                default:
                    throw new RuntimeException("Invalid view id");
            }
        }
    };

    public interface OnColorChangedListener {
        void colorChanged(int i);
    }

    public DialogScreenHintColor(Activity hActivity, DialogManager hDialogManager) {
        super(hActivity, hDialogManager);
        TitleView hTitle = new TitleView(hActivity, getResources().getDisplayMetrics());
        hTitle.setTitle(ResString.dialog_screen_hint_color_title);
        addView(hTitle);
        LinearLayout vgContetContainer = new LinearLayout(hActivity);
        vgContetContainer.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        vgContetContainer.setGravity(17);
        addView(vgContetContainer);
        this.m_hColorPicker = new ColorPickerView(hActivity);
        vgContetContainer.addView(this.m_hColorPicker);
        ButtonContainer vgButtonContainer = new ButtonContainer(hActivity);
        vgButtonContainer.createButton(0, "btn_img_cancel", this.m_hOnClick);
        vgButtonContainer.createButton(1, "btn_img_hint_disable", this.m_hOnClick);
        vgButtonContainer.createButton(2, "btn_img_ok", this.m_hOnClick);
        addView(vgButtonContainer);
    }

    public void onShow(OnColorChangedListener hOnColorChanged) {
        this.mListener = hOnColorChanged;
        int iHintColor = Option.getHintColor(getContext());
        this.m_hColorPicker.setSelectedColor(iHintColor != -1 ? iHintColor : Option.HINT_COLOR_DEFAULT);
    }

    private static class ColorPickerView extends View {
        private static final float PI = 3.1415925f;
        private final int[] m_arrColor = {-65536, -65281, -16776961, Option.HINT_COLOR_DEFAULT, -1, -16711936, -256, -65536};
        private final Paint m_hPaintInnerCircleCenter;
        private final Paint m_hPaintInnerCircleOuterBorder;
        private final Paint m_hPaintOuterCircleCenter;
        private final Paint m_hPaintOuterCircleInnerBorder;
        private final Paint m_hPaintOuterCircleOuterBorder;
        private final int m_iDipCircleBorderSize;
        private final int m_iDipInnerCircleCenterRadius;
        private final int m_iDipInnerCircleOuterBorderRadius;
        private final int m_iDipOuterCircleCenterRadius;
        private final int m_iDipOuterCircleInnerBorderRadius;
        private final int m_iDipOuterCircleOuterBorderRadius;
        private final int m_iDipViewCenter;
        private final int m_iDipViewSize;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.SweepGradient.<init>(float, float, int[], float[]):void}
         arg types: [int, int, int[], ?[OBJECT, ARRAY]]
         candidates:
          ClspMth{android.graphics.SweepGradient.<init>(float, float, long, long):void}
          ClspMth{android.graphics.SweepGradient.<init>(float, float, int, int):void}
          ClspMth{android.graphics.SweepGradient.<init>(float, float, long[], float[]):void}
          ClspMth{android.graphics.SweepGradient.<init>(float, float, int[], float[]):void} */
        public ColorPickerView(Context hContext) {
            super(hContext);
            DisplayMetrics hMetrics = getResources().getDisplayMetrics();
            this.m_iDipViewSize = ResDimens.getDip(hMetrics, ResDimens.COLOR_PICKER_VIEW_SIZE);
            this.m_iDipViewCenter = this.m_iDipViewSize / 2;
            this.m_iDipCircleBorderSize = ResDimens.getDip(hMetrics, 2);
            this.m_iDipInnerCircleCenterRadius = ResDimens.getDip(hMetrics, 32);
            this.m_iDipOuterCircleOuterBorderRadius = ResDimens.getDip(hMetrics, ResDimens.COLOR_PICKER_OUTER_CIRCLE_RADIUS);
            int iDipOuterCircleStroke = ResDimens.getDip(hMetrics, 48);
            this.m_iDipOuterCircleCenterRadius = this.m_iDipOuterCircleOuterBorderRadius - (iDipOuterCircleStroke / 2);
            this.m_iDipInnerCircleOuterBorderRadius = this.m_iDipInnerCircleCenterRadius + this.m_iDipCircleBorderSize;
            this.m_iDipOuterCircleInnerBorderRadius = this.m_iDipOuterCircleCenterRadius - (iDipOuterCircleStroke / 2);
            this.m_hPaintOuterCircleOuterBorder = new Paint(1);
            this.m_hPaintOuterCircleOuterBorder.setStyle(Paint.Style.STROKE);
            this.m_hPaintOuterCircleOuterBorder.setStrokeWidth((float) this.m_iDipCircleBorderSize);
            this.m_hPaintOuterCircleOuterBorder.setColor(-1);
            this.m_hPaintOuterCircleCenter = new Paint(1);
            this.m_hPaintOuterCircleCenter.setStyle(Paint.Style.STROKE);
            this.m_hPaintOuterCircleCenter.setStrokeWidth((float) iDipOuterCircleStroke);
            this.m_hPaintOuterCircleCenter.setShader(new SweepGradient(0.0f, 0.0f, this.m_arrColor, (float[]) null));
            this.m_hPaintOuterCircleInnerBorder = new Paint(1);
            this.m_hPaintOuterCircleInnerBorder.setStyle(Paint.Style.STROKE);
            this.m_hPaintOuterCircleInnerBorder.setStrokeWidth((float) this.m_iDipCircleBorderSize);
            this.m_hPaintOuterCircleInnerBorder.setColor(-1);
            this.m_hPaintInnerCircleOuterBorder = new Paint(1);
            this.m_hPaintInnerCircleOuterBorder.setStyle(Paint.Style.FILL);
            this.m_hPaintInnerCircleOuterBorder.setColor(-1);
            this.m_hPaintInnerCircleCenter = new Paint(1);
            this.m_hPaintInnerCircleCenter.setStyle(Paint.Style.FILL);
        }

        public void setSelectedColor(int iColor) {
            this.m_hPaintInnerCircleCenter.setColor(iColor);
        }

        public int getSelectedColor() {
            return this.m_hPaintInnerCircleCenter.getColor();
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            canvas.translate((float) this.m_iDipViewCenter, (float) this.m_iDipViewCenter);
            canvas.drawCircle(0.0f, 0.0f, (float) this.m_iDipOuterCircleCenterRadius, this.m_hPaintOuterCircleCenter);
            canvas.drawCircle(0.0f, 0.0f, (float) this.m_iDipOuterCircleOuterBorderRadius, this.m_hPaintOuterCircleOuterBorder);
            canvas.drawCircle(0.0f, 0.0f, (float) this.m_iDipOuterCircleInnerBorderRadius, this.m_hPaintOuterCircleInnerBorder);
            canvas.drawCircle(0.0f, 0.0f, (float) this.m_iDipInnerCircleOuterBorderRadius, this.m_hPaintInnerCircleOuterBorder);
            canvas.drawCircle(0.0f, 0.0f, (float) this.m_iDipInnerCircleCenterRadius, this.m_hPaintInnerCircleCenter);
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            setMeasuredDimension(this.m_iDipViewSize, this.m_iDipViewSize);
        }

        private int interpColor(int[] arrColors, float fUnit) {
            if (fUnit <= 0.0f) {
                return arrColors[0];
            }
            if (fUnit >= 1.0f) {
                return arrColors[arrColors.length - 1];
            }
            float fFractionalPart = fUnit * ((float) (arrColors.length - 1));
            int iIndex = (int) fFractionalPart;
            float fFractionalPart2 = fFractionalPart - ((float) iIndex);
            int iColor0 = arrColors[iIndex];
            int iColor1 = arrColors[iIndex + 1];
            return Color.argb(hexToIntColor(Color.alpha(iColor0), Color.alpha(iColor1), fFractionalPart2), hexToIntColor(Color.red(iColor0), Color.red(iColor1), fFractionalPart2), hexToIntColor(Color.green(iColor0), Color.green(iColor1), fFractionalPart2), hexToIntColor(Color.blue(iColor0), Color.blue(iColor1), fFractionalPart2));
        }

        private int hexToIntColor(int iColorComponent1, int iColorComponent2, float fFractionalPart) {
            return Math.round(((float) (iColorComponent2 - iColorComponent1)) * fFractionalPart) + iColorComponent1;
        }

        public boolean onTouchEvent(MotionEvent event) {
            float fTouchPosX = event.getX() - ((float) this.m_iDipOuterCircleOuterBorderRadius);
            float fTouchPosY = event.getY() - ((float) this.m_iDipOuterCircleOuterBorderRadius);
            boolean bPickColor = true;
            double dRadiusTouch = Math.sqrt((double) ((fTouchPosX * fTouchPosX) + (fTouchPosY * fTouchPosY)));
            if (dRadiusTouch > ((double) this.m_iDipOuterCircleOuterBorderRadius)) {
                bPickColor = false;
            } else if (dRadiusTouch < ((double) this.m_iDipOuterCircleInnerBorderRadius)) {
                bPickColor = false;
            }
            switch (event.getAction()) {
                case 0:
                case 2:
                    if (!bPickColor) {
                        return true;
                    }
                    float fUnit = ((float) Math.atan2((double) fTouchPosY, (double) fTouchPosX)) / 6.283185f;
                    if (fUnit < 0.0f) {
                        fUnit += 1.0f;
                    }
                    this.m_hPaintInnerCircleCenter.setColor(interpColor(this.m_arrColor, fUnit));
                    invalidate();
                    return true;
                case 1:
                default:
                    return true;
            }
        }
    }
}
