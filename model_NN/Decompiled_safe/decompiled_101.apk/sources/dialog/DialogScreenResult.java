package dialog;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import cfg.Option;
import res.ResDimens;
import res.ResString;
import screen.ScreenPlayBase;
import view.ButtonContainer;
import view.TitleView;

public final class DialogScreenResult extends DialogScreenBase {
    private static final int BUTTON_ID_NEXT = 0;
    private static final int MAX_NAME_LENGTH = 20;
    /* access modifiers changed from: private */
    public boolean m_bNewRecord = false;
    /* access modifiers changed from: private */
    public final EditText m_etUsername;
    /* access modifiers changed from: private */
    public final Activity m_hActivity;
    private final View.OnClickListener m_hOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case 0:
                    break;
                default:
                    throw new RuntimeException("Invalid view id");
            }
            if (DialogScreenResult.this.m_bNewRecord) {
                String sUsername = DialogScreenResult.this.m_etUsername.getText().toString();
                if (TextUtils.isEmpty(sUsername)) {
                    sUsername = ResString.default_user_name;
                }
                PreferenceManager.getDefaultSharedPreferences(DialogScreenResult.this.m_hActivity).edit().putString(ScreenPlayBase.PREF_KEY_PLAYER_NAME, sUsername).commit();
            }
            DialogScreenResult.this.m_hOnDismiss.onDismiss(DialogScreenResult.this.m_hDialogManager);
        }
    };
    /* access modifiers changed from: private */
    public DialogInterface.OnDismissListener m_hOnDismiss;
    private final TitleView m_hTitle;
    private final String m_sMoves;
    private final String m_sTime;
    private final TextView m_tvBestMovesNew;
    private final TextView m_tvBestMovesOld;
    private final TextView m_tvBestTimeNew;
    private final TextView m_tvBestTimeOld;
    private final TextView m_tvRecordTitle;

    public DialogScreenResult(Activity hActivity, DialogManager hDialogManager) {
        super(hActivity, hDialogManager);
        this.m_hActivity = hActivity;
        this.m_sMoves = "Moves";
        this.m_sTime = "Time";
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        this.m_hTitle = new TitleView(hActivity, hMetrics);
        addView(this.m_hTitle);
        this.m_etUsername = new EditText(hActivity);
        this.m_etUsername.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.m_etUsername.setSelectAllOnFocus(true);
        this.m_etUsername.setSingleLine(true);
        this.m_etUsername.setRawInputType(1);
        this.m_etUsername.setImeOptions(6);
        this.m_etUsername.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
        this.m_etUsername.setText(ResString.default_user_name);
        addView(this.m_etUsername);
        LinearLayout vgTextContainer = new LinearLayout(hActivity);
        vgTextContainer.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        vgTextContainer.setOrientation(1);
        vgTextContainer.setGravity(17);
        int iDip20 = ResDimens.getDip(hMetrics, 20);
        vgTextContainer.setPadding(iDip20, 0, iDip20, 0);
        addView(vgTextContainer);
        this.m_tvRecordTitle = createTextView(hActivity, 25, 1.0f);
        this.m_tvRecordTitle.setText(ResString.dialog_screen_result_label_record);
        vgTextContainer.addView(this.m_tvRecordTitle);
        this.m_tvBestMovesOld = createTextView(hActivity, 16, 0.5f);
        vgTextContainer.addView(this.m_tvBestMovesOld);
        this.m_tvBestTimeOld = createTextView(hActivity, 16, 0.5f);
        this.m_tvBestTimeOld.setPadding(0, 0, 0, iDip20);
        vgTextContainer.addView(this.m_tvBestTimeOld);
        TextView tvYourResultTitle = createTextView(hActivity, 25, 1.0f);
        tvYourResultTitle.setText(ResString.dialog_screen_result_label_your_result);
        vgTextContainer.addView(tvYourResultTitle);
        this.m_tvBestMovesNew = createTextView(hActivity, 16, 0.5f);
        vgTextContainer.addView(this.m_tvBestMovesNew);
        this.m_tvBestTimeNew = createTextView(hActivity, 16, 0.5f);
        vgTextContainer.addView(this.m_tvBestTimeNew);
        ButtonContainer vgButtonContainer = new ButtonContainer(hActivity);
        vgButtonContainer.createButton(0, "btn_img_next", this.m_hOnClick);
        addView(vgButtonContainer);
    }

    private TextView createTextView(Context hContext, int iTextSize, float fShadowRadius) {
        TextView tvText = new TextView(hContext);
        tvText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        tvText.setSingleLine(true);
        tvText.setTextColor(-1);
        tvText.setTextSize(1, (float) iTextSize);
        tvText.setShadowLayer(fShadowRadius, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
        tvText.setTypeface(Typeface.DEFAULT, 1);
        return tvText;
    }

    public void onShow(ScreenPlayBase.ResultData hResultData, DialogInterface.OnDismissListener hOnDismiss) {
        this.m_hOnDismiss = hOnDismiss;
        switch (hResultData.iGameMode) {
            case 0:
            case 3:
                setResultForDefaultMode(hResultData);
                return;
            case 1:
                setResultForCasualMode(hResultData);
                return;
            case 2:
                throw new RuntimeException("TODO");
            default:
                throw new RuntimeException("Invalid game mode");
        }
    }

    private void setResultForCasualMode(ScreenPlayBase.ResultData hResultData) {
        long lTimeInMsNew = hResultData.lTimeInMsNew;
        int iMovesNew = hResultData.iMovesNew;
        this.m_tvRecordTitle.setVisibility(8);
        this.m_tvBestMovesOld.setVisibility(8);
        this.m_tvBestTimeOld.setVisibility(8);
        this.m_tvBestMovesNew.setTextColor(-1);
        this.m_tvBestTimeNew.setTextColor(-1);
        this.m_tvBestMovesNew.setText(String.valueOf(this.m_sMoves) + ": " + iMovesNew);
        this.m_tvBestTimeNew.setText(String.valueOf(this.m_sTime) + String.format(": %.3f s", Float.valueOf(((float) lTimeInMsNew) / 1000.0f)));
        this.m_hTitle.setTitle(ResString.dialog_screen_result_title_result);
        this.m_etUsername.setVisibility(8);
    }

    private void setResultForDefaultMode(ScreenPlayBase.ResultData hResultData) {
        String sMovesOldName = hResultData.sMovesNameRecord;
        int iMovesOld = hResultData.iMovesRecord;
        String sTimeInMsOldName = hResultData.sTimeInMsNameRecord;
        long lTimeInMsOld = hResultData.lTimeInMsRecord;
        long lTimeInMsNew = hResultData.lTimeInMsNew;
        int iMovesNew = hResultData.iMovesNew;
        this.m_tvRecordTitle.setVisibility(0);
        this.m_tvBestMovesOld.setVisibility(0);
        this.m_tvBestTimeOld.setVisibility(0);
        if (iMovesOld <= 0) {
            this.m_tvBestMovesOld.setText(String.valueOf(this.m_sMoves) + ": ---");
        } else {
            this.m_tvBestMovesOld.setText(String.valueOf(this.m_sMoves) + ": " + iMovesOld + " (" + sMovesOldName + ")");
        }
        if (lTimeInMsOld <= 0) {
            this.m_tvBestTimeOld.setText(String.valueOf(this.m_sTime) + ": ---");
        } else {
            this.m_tvBestTimeOld.setText(String.valueOf(this.m_sTime) + String.format(": %.3f s", Float.valueOf(((float) lTimeInMsOld) / 1000.0f)) + " (" + sTimeInMsOldName + ")");
        }
        this.m_tvBestMovesNew.setText(String.valueOf(this.m_sMoves) + ": " + iMovesNew);
        this.m_tvBestTimeNew.setText(String.valueOf(this.m_sTime) + String.format(": %.3f s", Float.valueOf(((float) lTimeInMsNew) / 1000.0f)));
        this.m_bNewRecord = false;
        if (iMovesOld <= 0 || iMovesNew < iMovesOld) {
            this.m_bNewRecord = true;
            this.m_tvBestMovesNew.setTextColor(-1883325);
        } else {
            this.m_tvBestMovesNew.setTextColor(-1);
        }
        if (lTimeInMsOld <= 0 || lTimeInMsNew < lTimeInMsOld) {
            this.m_bNewRecord = true;
            this.m_tvBestTimeNew.setTextColor(-1883325);
        } else {
            this.m_tvBestTimeNew.setTextColor(-1);
        }
        if (this.m_bNewRecord) {
            this.m_hTitle.setTitle(ResString.dialog_screen_result_title_new_record);
            this.m_etUsername.setVisibility(0);
            this.m_etUsername.setText(PreferenceManager.getDefaultSharedPreferences(this.m_hActivity).getString(ScreenPlayBase.PREF_KEY_PLAYER_NAME, ResString.default_user_name));
            return;
        }
        this.m_hTitle.setTitle(ResString.dialog_screen_result_title_result);
        this.m_etUsername.setVisibility(8);
    }

    public boolean unlockScreenOnDismiss() {
        return false;
    }

    public void cleanUp() {
        super.cleanUp();
        this.m_hTitle.setTitle(null);
        this.m_tvBestMovesOld.setText((CharSequence) null);
        this.m_tvBestTimeOld.setText((CharSequence) null);
        this.m_tvBestMovesNew.setText((CharSequence) null);
        this.m_tvBestTimeNew.setText((CharSequence) null);
        this.m_etUsername.setText((CharSequence) null);
    }
}
