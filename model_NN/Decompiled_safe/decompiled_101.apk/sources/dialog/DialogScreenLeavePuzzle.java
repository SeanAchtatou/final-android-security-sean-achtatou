package dialog;

import android.app.Activity;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cfg.Option;
import res.ResDimens;
import res.ResString;
import view.ButtonContainer;
import view.TitleView;

public final class DialogScreenLeavePuzzle extends DialogScreenBase {
    private static final int BUTTON_ID_CANCEL = 0;
    private static final int BUTTON_ID_OK = 1;
    private final View.OnClickListener m_hOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case 0:
                    DialogScreenLeavePuzzle.this.m_hDialogManager.dismiss();
                    return;
                case 1:
                    DialogScreenLeavePuzzle.this.m_hDialogManager.getGame().switchScreen(DialogScreenLeavePuzzle.this.m_iScreenId);
                    return;
                default:
                    throw new RuntimeException("Invalid view id");
            }
        }
    };
    /* access modifiers changed from: private */
    public int m_iScreenId;

    public DialogScreenLeavePuzzle(Activity hActivity, DialogManager hDialogManager) {
        super(hActivity, hDialogManager);
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        TitleView hTitle = new TitleView(hActivity, hMetrics);
        hTitle.setTitle(ResString.dialog_screen_leave_puzzle_title);
        addView(hTitle);
        RelativeLayout vgTextContainer = new RelativeLayout(hActivity);
        vgTextContainer.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        addView(vgTextContainer);
        TextView tvText = new TextView(hActivity);
        RelativeLayout.LayoutParams hTextParams = new RelativeLayout.LayoutParams(-1, -2);
        hTextParams.addRule(13);
        tvText.setLayoutParams(hTextParams);
        tvText.setGravity(17);
        tvText.setTextColor(-1);
        tvText.setTextSize(1, 20.0f);
        tvText.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
        tvText.setTypeface(Typeface.DEFAULT, 1);
        int iDip20 = ResDimens.getDip(hMetrics, 20);
        tvText.setPadding(iDip20, iDip20, iDip20, iDip20);
        tvText.setText(ResString.dialog_screen_leave_puzzle_text);
        vgTextContainer.addView(tvText);
        ButtonContainer vgButtonContainer = new ButtonContainer(hActivity);
        vgButtonContainer.createButton(0, "btn_img_cancel", this.m_hOnClick);
        vgButtonContainer.createButton(1, "btn_img_ok", this.m_hOnClick);
        addView(vgButtonContainer);
    }

    public void onShow(int iScreenId) {
        this.m_iScreenId = iScreenId;
    }
}
