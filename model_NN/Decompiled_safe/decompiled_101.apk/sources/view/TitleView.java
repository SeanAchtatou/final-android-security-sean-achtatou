package view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.widget.LinearLayout;
import android.widget.TextView;
import cfg.Option;
import res.Res;
import res.ResDimens;

public final class TitleView extends LinearLayout {
    private final TextView m_tvTitle;

    public TitleView(Context hContext, DisplayMetrics hMetrics) {
        super(hContext);
        setLayoutParams(new LinearLayout.LayoutParams(-1, ResDimens.getDip(hMetrics, 64)));
        setGravity(17);
        setBackgroundResource(Res.drawable(hContext, "background_head"));
        this.m_tvTitle = new TextView(hContext);
        this.m_tvTitle.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.m_tvTitle.setGravity(17);
        this.m_tvTitle.setSingleLine(true);
        this.m_tvTitle.setTextColor(-1);
        this.m_tvTitle.setTextSize(1, 25.0f);
        this.m_tvTitle.setTypeface(Typeface.DEFAULT, 1);
        this.m_tvTitle.setShadowLayer(1.0f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
        int iDip5 = ResDimens.getDip(hMetrics, 5);
        this.m_tvTitle.setPadding(iDip5, 0, iDip5, ResDimens.getDip(hMetrics, 10));
        addView(this.m_tvTitle);
    }

    public void setTitle(String sText) {
        this.m_tvTitle.setText(sText);
    }
}
