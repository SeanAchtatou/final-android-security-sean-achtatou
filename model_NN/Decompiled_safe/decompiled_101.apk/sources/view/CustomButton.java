package view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cfg.Option;
import res.Res;
import res.ResDimens;

public final class CustomButton extends RelativeLayout {
    private static final int[] ARR_STATE_NORMAL = {-16842919};
    private static final int[] ARR_STATE_PRESSED = {16842919};
    private final ImageView m_ivIcon;
    private final TextView m_tvIcon;

    public CustomButton(Context hContext, int iId) {
        super(hContext);
        setId(iId);
        setFocusable(false);
        Resources hRes = getResources();
        DisplayMetrics hMetrics = hRes.getDisplayMetrics();
        Drawable hBtnNormal = hRes.getDrawable(Res.drawable(hContext, "btn_bg_normal"));
        Drawable bBtnPressed = hRes.getDrawable(Res.drawable(hContext, "btn_bg_pressed"));
        StateListDrawable hStateListDrawable = new StateListDrawable();
        hStateListDrawable.addState(ARR_STATE_NORMAL, hBtnNormal);
        hStateListDrawable.addState(ARR_STATE_PRESSED, bBtnPressed);
        setBackgroundDrawable(hStateListDrawable);
        this.m_ivIcon = new ImageView(hContext);
        int iDipIconSize = ResDimens.getDip(hMetrics, 35);
        RelativeLayout.LayoutParams hIconParams = new RelativeLayout.LayoutParams(iDipIconSize, iDipIconSize);
        hIconParams.addRule(13);
        this.m_ivIcon.setLayoutParams(hIconParams);
        addView(this.m_ivIcon);
        this.m_tvIcon = new TextView(hContext);
        RelativeLayout.LayoutParams hTextParams = new RelativeLayout.LayoutParams(-1, -1);
        hTextParams.addRule(13);
        this.m_tvIcon.setLayoutParams(hTextParams);
        this.m_tvIcon.setGravity(17);
        this.m_tvIcon.setTextColor(-1);
        this.m_tvIcon.setTextSize(1, 20.0f);
        this.m_tvIcon.setTypeface(Typeface.DEFAULT, 1);
        this.m_tvIcon.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
        this.m_tvIcon.setPadding(0, 0, 0, ResDimens.getDip(hMetrics, 4));
        addView(this.m_tvIcon);
    }

    public void setIcon(int iDrawableResId) {
        this.m_ivIcon.setImageResource(iDrawableResId);
    }

    public void setText(String sString) {
        this.m_tvIcon.setText(sString);
    }

    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled) {
            this.m_ivIcon.setAlpha(255);
        } else {
            this.m_ivIcon.setAlpha(100);
        }
    }
}
