package screen;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import game.IGame;
import res.ResDimens;

public abstract class ScreenBase extends LinearLayout {
    public static final int ID_GAME_MODE = 1;
    public static final int ID_INVALID = -1;
    public static final int ID_MORE_PUZZLE_LIST = 9;
    public static final int ID_MY_PUZZLE = 11;
    public static final int ID_MY_PUZZLE_IMAGE_GRID_SIZE = 12;
    public static final int ID_MY_PUZZLE_LIST = 10;
    public static final int ID_PLAY_MY_PUZZLE = 7;
    public static final int ID_PLAY_PUZZLE_CASUAL = 5;
    public static final int ID_PLAY_PUZZLE_HOT_SEAT = 6;
    public static final int ID_PLAY_PUZZLE_PROGRESS = 4;
    public static final int ID_PUZZLE = 2;
    public static final int ID_PUZZLE_IMAGE = 3;
    public static final int ID_PUZZLE_NOT_REGISTERED = 13;
    public static final int ID_SOLVED_ALL = 8;
    private final int BUTTON_CONTAINER_ANIM_DURATION = ResDimens.COLOR_PICKER_VIEW_SIZE;
    private final float IN_FROM_X = 0.0f;
    private final float IN_FROM_Y = -1.0f;
    private final float IN_MORE_FROM_X = 0.0f;
    private final float IN_MORE_FROM_Y = 1.0f;
    private final float IN_MORE_TO_X = 0.0f;
    private final float IN_MORE_TO_Y = 0.0f;
    private final float IN_TO_X = 0.0f;
    private final float IN_TO_Y = 0.0f;
    private final float OUT_FROM_X = 0.0f;
    private final float OUT_FROM_Y = 0.0f;
    private final float OUT_MORE_FROM_X = 0.0f;
    private final float OUT_MORE_FROM_Y = 0.0f;
    private final float OUT_MORE_TO_X = 0.0f;
    private final float OUT_MORE_TO_Y = 1.0f;
    private final float OUT_TO_X = 0.0f;
    private final float OUT_TO_Y = -1.0f;
    private final int SCREEN_ANIM_DURATION = 1000;
    protected boolean m_bIsCurrentScreen = false;
    protected final Activity m_hActivity;
    private Animation m_hAnimButtonContainerIn;
    private Animation m_hAnimButtonContainerMoreIn;
    private Animation m_hAnimButtonContainerMoreOut;
    private Animation m_hAnimButtonContainerOut;
    protected Animation m_hAnimIn;
    private final Animation.AnimationListener m_hAnimListenerButtonContainerIn = new Animation.AnimationListener() {
        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
            ScreenBase.this.m_vgButtonContainer.setVisibility(0);
        }
    };
    private final Animation.AnimationListener m_hAnimListenerButtonContainerMoreIn = new Animation.AnimationListener() {
        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
            ScreenBase.this.m_vgButtonContainerMore.setVisibility(0);
        }
    };
    private final Animation.AnimationListener m_hAnimListenerButtonContainerMoreOut = new Animation.AnimationListener() {
        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
            ScreenBase.this.m_vgButtonContainerMore.setVisibility(4);
        }
    };
    private final Animation.AnimationListener m_hAnimListenerButtonContainerOut = new Animation.AnimationListener() {
        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
            ScreenBase.this.m_vgButtonContainer.setVisibility(4);
        }
    };
    protected final IGame m_hGame;
    protected final Handler m_hHandler;
    /* access modifiers changed from: private */
    public final Handler m_hHandlerOnClick = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            ScreenBase.this.onClick(msg.what);
        }
    };
    protected final View.OnClickListener m_hOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            if (ScreenBase.this.m_bIsCurrentScreen && !ScreenBase.this.m_hGame.isScreenLocked()) {
                ScreenBase.this.m_hGame.lockScreen();
                ScreenBase.this.m_hHandlerOnClick.sendEmptyMessage(v.getId());
            }
        }
    };
    protected final int m_iScreenId;
    protected int m_iSwitchBackScreenId;
    /* access modifiers changed from: private */
    public View m_vgButtonContainer;
    /* access modifiers changed from: private */
    public View m_vgButtonContainerMore;

    public ScreenBase(int iScreenId, Activity hActivity, IGame hGame) {
        super(hActivity);
        this.m_iScreenId = iScreenId;
        this.m_hActivity = hActivity;
        this.m_hGame = hGame;
        this.m_hHandler = hGame.getHandler();
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        setOrientation(1);
        setGravity(17);
        this.m_hAnimIn = new AlphaAnimation(0.0f, 1.0f);
        this.m_hAnimIn.setDuration(1000);
        this.m_hAnimIn.setInterpolator(hActivity, 17432582);
        createButtonContainerAnim(hActivity);
    }

    public boolean beforeShow(int iSwitchBackScreenId, boolean bReset, Object hData) {
        this.m_bIsCurrentScreen = true;
        this.m_iSwitchBackScreenId = iSwitchBackScreenId;
        return true;
    }

    public void onShow() {
    }

    public void onHide() {
        this.m_bIsCurrentScreen = false;
    }

    public int getId() {
        return this.m_iScreenId;
    }

    public void onAfterHide() {
    }

    public void onQuitRequest() {
        this.m_bIsCurrentScreen = false;
    }

    public void onActivityPause() {
    }

    public void onActivityResume() {
    }

    /* access modifiers changed from: protected */
    public void onClick(int iViewId) {
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        return false;
    }

    public void onActivityResult(int iRequestCode, int iResultCode, Intent hData) {
    }

    private void createButtonContainerAnim(Context hContext) {
        this.m_hAnimButtonContainerMoreIn = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
        this.m_hAnimButtonContainerMoreIn.setDuration(250);
        this.m_hAnimButtonContainerMoreIn.setAnimationListener(this.m_hAnimListenerButtonContainerMoreIn);
        this.m_hAnimButtonContainerMoreIn.setInterpolator(hContext, 17432582);
        this.m_hAnimButtonContainerMoreOut = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
        this.m_hAnimButtonContainerMoreOut.setDuration(250);
        this.m_hAnimButtonContainerMoreOut.setAnimationListener(this.m_hAnimListenerButtonContainerMoreOut);
        this.m_hAnimButtonContainerMoreOut.setInterpolator(hContext, 17432582);
        this.m_hAnimButtonContainerOut = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
        this.m_hAnimButtonContainerOut.setDuration(250);
        this.m_hAnimButtonContainerOut.setAnimationListener(this.m_hAnimListenerButtonContainerOut);
        this.m_hAnimButtonContainerOut.setInterpolator(hContext, 17432582);
        this.m_hAnimButtonContainerIn = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
        this.m_hAnimButtonContainerIn.setDuration(250);
        this.m_hAnimButtonContainerIn.setAnimationListener(this.m_hAnimListenerButtonContainerIn);
        this.m_hAnimButtonContainerIn.setInterpolator(hContext, 17432582);
    }

    /* access modifiers changed from: protected */
    public void setButtonContainer(View vgButtonContainer, View vgButtonContainerMore) {
        this.m_vgButtonContainer = vgButtonContainer;
        this.m_vgButtonContainerMore = vgButtonContainerMore;
        if (vgButtonContainer.getVisibility() == 0 && vgButtonContainerMore.getVisibility() == 0) {
            vgButtonContainerMore.setVisibility(4);
        }
    }

    /* access modifiers changed from: protected */
    public void showButtonContainerMore() {
        if (this.m_vgButtonContainerMore.getVisibility() != 0) {
            this.m_vgButtonContainerMore.startAnimation(this.m_hAnimButtonContainerMoreIn);
            this.m_vgButtonContainer.startAnimation(this.m_hAnimButtonContainerOut);
        }
    }

    /* access modifiers changed from: protected */
    public boolean isButtonContainerMoreVisible() {
        if (this.m_vgButtonContainerMore.getVisibility() == 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void hideButtonContainerMore(boolean bAnimated) {
        if (this.m_vgButtonContainerMore.getVisibility() != 0) {
            return;
        }
        if (bAnimated) {
            this.m_vgButtonContainer.startAnimation(this.m_hAnimButtonContainerIn);
            this.m_vgButtonContainerMore.startAnimation(this.m_hAnimButtonContainerMoreOut);
            return;
        }
        this.m_vgButtonContainer.setVisibility(0);
        this.m_vgButtonContainerMore.setVisibility(4);
    }

    public static boolean isIntentAvailable(Context hContext, Intent hIntent) {
        return hContext.getPackageManager().queryIntentActivities(hIntent, 65536).size() > 0;
    }
}
