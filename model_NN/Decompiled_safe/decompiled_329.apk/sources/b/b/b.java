package b.b;

import android.app.Activity;
import android.os.Build;
import com.uc.browser.R;
import com.uc.c.au;
import com.uc.c.w;

public class b {
    public static int MM = 0;
    public static int MN = 0;
    private static boolean cej = false;

    public static String PO() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(" (").append(a.cr.getString(R.string.f1491juc_ua_header)).append(' ');
        String str = Build.VERSION.RELEASE;
        if (str == null || str.length() <= 0) {
            stringBuffer.append(a.cr.getString(R.string.f1493default_release));
        } else {
            stringBuffer.append(str);
        }
        stringBuffer.append("; ");
        stringBuffer.append(a.cr.getString(R.string.f1494default_language)).append("; ");
        String str2 = Build.MODEL;
        if (str2.length() > 0) {
            stringBuffer.append(str2.replaceAll(" ", "_"));
        }
        stringBuffer.append("; ");
        if (MN == 0 && MM == 0) {
            PT();
        }
        stringBuffer.append(MM).append("*").append(MN);
        stringBuffer.append(")");
        return stringBuffer.toString();
    }

    public static String PP() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(" (").append(a.cr.getString(R.string.f1492webkit_ua_header)).append(' ');
        String str = Build.VERSION.RELEASE;
        if (str == null || str.length() <= 0) {
            stringBuffer.append(a.cr.getString(R.string.f1493default_release));
        } else {
            stringBuffer.append(str);
        }
        stringBuffer.append("; ");
        stringBuffer.append(a.cr.getString(R.string.f1494default_language)).append("; ");
        String str2 = Build.MODEL;
        if (str2.length() > 0) {
            stringBuffer.append(str2.replaceAll(" ", "_"));
        }
        stringBuffer.append(" Build/").append(Build.ID).append("; ");
        if (MN == 0 && MM == 0) {
            PT();
        }
        stringBuffer.append(MM).append("*").append(MN);
        stringBuffer.append(")");
        return stringBuffer.toString();
    }

    private static String PQ() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(' ').append(a.cr.getString(R.string.f1495browser_name)).append(w.NZ).append(au.aGF).append(w.NQ).append(au.aGF).append(w.NM);
        return stringBuffer.toString();
    }

    private static String PR() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(a.cr.getString(R.string.f1495browser_name)).append(w.NZ).append(au.aGF).append(w.NQ).append(au.aGF).append(w.NM);
        return stringBuffer.toString();
    }

    public static String PS() {
        boolean OX = a.OX();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(a.cr.getString(R.string.f1499mozilla)).append(PP()).append(' ').append(a.cr.getString(R.string.f1500webkit_tail));
        if (!OX) {
            stringBuffer.append(au.aGF).append(PR());
        }
        return stringBuffer.toString();
    }

    public static void PT() {
        MN = ((Activity) a.cr).getWindowManager().getDefaultDisplay().getHeight();
        MM = ((Activity) a.cr).getWindowManager().getDefaultDisplay().getWidth();
    }

    public static boolean PU() {
        return cej;
    }

    public static void cL(boolean z) {
        cej = z;
    }

    public static String q(int i, boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        if (!z) {
            switch (i) {
                case 1:
                    stringBuffer.append(a.cr.getString(R.string.f1496juc)).append(PO()).append(PQ());
                    break;
                case 2:
                    stringBuffer.append(a.cr.getString(R.string.f1497openwave)).append(PO()).append(PQ());
                    break;
                case 3:
                    stringBuffer.append(a.cr.getString(R.string.f1498opera)).append(PO()).append(PQ());
                    break;
            }
        } else {
            switch (i) {
                case 1:
                    stringBuffer.append(a.cr.getString(R.string.f1496juc)).append(PO());
                    break;
                case 2:
                    stringBuffer.append(a.cr.getString(R.string.f1497openwave));
                    break;
                case 3:
                    stringBuffer.append(a.cr.getString(R.string.f1498opera));
                    break;
            }
        }
        return stringBuffer.toString();
    }
}
