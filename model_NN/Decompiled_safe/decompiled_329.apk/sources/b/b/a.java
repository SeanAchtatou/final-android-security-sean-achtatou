package b.b;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import java.util.List;

public class a {
    public static final int bYA = 6;
    static final String[] bYB = {"\"CMCC\"", "\"CMCC-EDU\"", "\"ChinaUnicom\"", "\"ChinaNet\"", "\"CMCC-Starbucks\""};
    static final String bYC = "mcdonald";
    private static String bYD = null;
    private static int bYE = -1;
    private static boolean bYF = false;
    public static final int bYG = 0;
    public static final int bYH = 1;
    public static final int bYI = 2;
    public static final int bYJ = 99;
    public static final int bYu = 0;
    public static final int bYv = 1;
    public static final int bYw = 2;
    public static final int bYx = 3;
    public static final int bYy = 4;
    public static final int bYz = 5;
    public static Context cr;

    public static String OQ() {
        NetworkInfo activeNetworkInfo = getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return null;
        }
        int type = activeNetworkInfo.getType();
        String extraInfo = activeNetworkInfo.getExtraInfo();
        String lowerCase = extraInfo == null ? "" : extraInfo.trim().toLowerCase();
        if (type != 0) {
            bYF = false;
            return "wifi";
        } else if (lowerCase.contains("cmwap")) {
            bYF = true;
            return "cmwap";
        } else if (lowerCase.contains("cmnet")) {
            bYF = false;
            return "cmnet";
        } else if (lowerCase.contains("uniwap")) {
            bYF = true;
            return "uniwap";
        } else if (lowerCase.contains("uninet")) {
            bYF = false;
            return "uninet";
        } else if (lowerCase.contains("3gwap")) {
            bYF = true;
            return "3gwap";
        } else if (lowerCase.contains("3gnet")) {
            bYF = false;
            return "3gnet";
        } else if (lowerCase.contains("ctwap")) {
            bYF = true;
            return "ctwap";
        } else if (lowerCase.contains("ctnet")) {
            bYF = false;
            return "ctnet";
        } else {
            bYF = false;
            return null;
        }
    }

    public static int OR() {
        NetworkInfo activeNetworkInfo = getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return 0;
        }
        int type = activeNetworkInfo.getType();
        String extraInfo = activeNetworkInfo.getExtraInfo();
        String lowerCase = extraInfo == null ? "" : extraInfo.trim().toLowerCase();
        if (type == 1) {
            bYF = false;
            return 1;
        } else if (type != 0) {
            bYF = false;
            return 1;
        } else if (lowerCase.contains("cmwap") || lowerCase.contains("uniwap") || lowerCase.contains("3gwap") || lowerCase.contains("ctwap") || (OV() != null && !lowerCase.contains("cmnet") && !lowerCase.contains("ctnet") && !lowerCase.contains("uninet") && !lowerCase.contains("3gnet"))) {
            bYF = true;
            return 3;
        } else if (lowerCase.contains("cmnet") || lowerCase.contains("ctnet") || lowerCase.contains("#777")) {
            bYF = false;
            return 2;
        } else {
            bYF = false;
            return 2;
        }
    }

    public static int OS() {
        NetworkInfo activeNetworkInfo = getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return 0;
        }
        int type = activeNetworkInfo.getType();
        String extraInfo = activeNetworkInfo.getExtraInfo();
        String lowerCase = extraInfo == null ? "" : extraInfo.trim().toLowerCase();
        if (type == 1) {
            return 1;
        }
        if (type != 0) {
            return 6;
        }
        if (lowerCase.contains("cmwap") || lowerCase.contains("uniwap") || lowerCase.contains("ctwap") || (OV() != null && !lowerCase.contains("cmnet") && !lowerCase.contains("ctnet") && !lowerCase.contains("uninet") && !lowerCase.contains("3gnet"))) {
            return 3;
        }
        if (lowerCase.contains("cmnet") || lowerCase.contains("uninet") || lowerCase.contains("ctnet") || lowerCase.contains("#777")) {
            return 2;
        }
        if (lowerCase.contains("3gwap")) {
            return 5;
        }
        return lowerCase.contains("3gnet") ? 4 : 6;
    }

    public static boolean OT() {
        NetworkInfo activeNetworkInfo = getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return false;
        }
        int type = activeNetworkInfo.getType();
        int subtype = activeNetworkInfo.getSubtype();
        if (type == 1) {
            return true;
        }
        if (type == 0 && subtype == 3) {
            return true;
        }
        String subtypeName = activeNetworkInfo.getSubtypeName();
        return "".equals(subtypeName) || "HSDPA".equals(subtypeName);
    }

    public static boolean OU() {
        if (cr == null) {
            return false;
        }
        WifiManager wifiManager = (WifiManager) cr.getSystemService("wifi");
        if (wifiManager == null) {
            return false;
        }
        List<WifiConfiguration> configuredNetworks = wifiManager.getConfiguredNetworks();
        if (wifiManager == null) {
            return false;
        }
        for (WifiConfiguration next : configuredNetworks) {
            if (next != null && next.status == 0) {
                for (String equalsIgnoreCase : bYB) {
                    if (equalsIgnoreCase.equalsIgnoreCase(next.SSID)) {
                        return true;
                    }
                }
                if (next.SSID != null && next.SSID.toLowerCase().contains(bYC)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String OV() {
        if (cr == null) {
            return null;
        }
        if (bYD == null || !bYD.equals(Proxy.getHost(cr))) {
            bYD = Proxy.getHost(cr);
        }
        return bYD;
    }

    public static int OW() {
        if (cr == null) {
            return -1;
        }
        if (bYE != Proxy.getPort(cr)) {
            bYE = Proxy.getPort(cr);
        }
        return bYE;
    }

    public static boolean OX() {
        return bYF;
    }

    public static int OY() {
        if (cr == null) {
            return 99;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) cr.getSystemService("connectivity");
        if (connectivityManager.getAllNetworkInfo() == null) {
            return 99;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return 99;
        }
        int type = activeNetworkInfo.getType();
        String extraInfo = activeNetworkInfo.getExtraInfo();
        String lowerCase = extraInfo == null ? "" : extraInfo.trim().toLowerCase();
        if (type == 1) {
            return 2;
        }
        if (type != 0) {
            return 99;
        }
        if (lowerCase.contains("cmwap") || lowerCase.contains("uniwap") || lowerCase.contains("3gwap") || lowerCase.contains("ctwap") || (OV() != null && !lowerCase.contains("cmnet") && !lowerCase.contains("ctnet") && !lowerCase.contains("uninet") && !lowerCase.contains("3gnet"))) {
            return 0;
        }
        return (lowerCase.contains("cmnet") || lowerCase.contains("ctnet") || lowerCase.contains("uninet") || lowerCase.contains("3gnet") || lowerCase.contains("#777")) ? 1 : 99;
    }

    public static void a() {
        Context context = b.a.cr;
        if (cr == null) {
        }
    }

    public static void cI(boolean z) {
        if (bYF != z) {
            bYF = z;
        }
    }

    public static void close() {
        cr = null;
    }

    public static void d(Context context) {
        cr = context;
        if (cr == null) {
        }
    }

    private static NetworkInfo getActiveNetworkInfo() {
        NetworkInfo[] allNetworkInfo;
        if (cr == null) {
            return null;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) cr.getSystemService("connectivity");
        if (connectivityManager == null) {
            return null;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if ((activeNetworkInfo == null || !activeNetworkInfo.isConnected()) && (allNetworkInfo = connectivityManager.getAllNetworkInfo()) != null) {
            for (int i = 0; i < allNetworkInfo.length; i++) {
                if (allNetworkInfo[i] != null && allNetworkInfo[i].isConnected()) {
                    return allNetworkInfo[i];
                }
            }
        }
        return activeNetworkInfo;
    }
}
