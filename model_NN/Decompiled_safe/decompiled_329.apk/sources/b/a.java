package b;

import android.content.Context;
import java.io.InputStream;
import java.io.OutputStream;

public class a {
    public static final int MODE_APPEND = 32768;
    public static final int MODE_PRIVATE = 0;
    public static final int MODE_WORLD_READABLE = 1;
    public static final int MODE_WORLD_WRITEABLE = 2;
    public static Context cr;

    public static InputStream cs(String str) {
        try {
            if (cr.getFileStreamPath(str).exists()) {
                return cr.openFileInput(str);
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static void d(Context context) {
        cr = context;
        if (cr == null) {
        }
    }

    public static boolean deleteFile(String str) {
        try {
            return cr.deleteFile(str);
        } catch (Exception e) {
            return false;
        }
    }

    public static OutputStream l(String str, int i) {
        try {
            return cr.openFileOutput(str, i);
        } catch (Exception e) {
            return null;
        }
    }
}
