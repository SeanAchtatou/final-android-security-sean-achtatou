package b.a.a.b;

import b.a.a.a.b.b;
import com.uc.c.au;
import com.uc.c.w;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;

public class d {
    public static final int aTa = 0;
    public static final int aTb = 1;
    private static final int aTc = 2;
    private static final byte[] aTd = null;
    private static final int aTe = 8;
    private static final int aTf = 16;
    private static final int aTg = 16;
    private static final int aTh = 64;
    private static Vector aTi = null;
    private static int aTo = 10485760;
    private static final String aTp = ".db";
    public static String aut = w.Qq;
    private String aTj;
    private String aTk;
    private int aTl;
    Object aTm;
    private Vector aTn;
    private File aTq = null;
    public int index = -1;
    private String separator = au.aGF;

    private d(String str) {
        this.aTj = str;
        this.separator = System.getProperty("file.separator");
        this.aTq = new File(aut + str);
        if (!this.aTq.exists()) {
            this.aTq.mkdirs();
        }
    }

    private File AC() {
        return this.aTq;
    }

    public static String[] Az() {
        return new File("FILE_ROOT").list();
    }

    public static d a(String str, boolean z, int i, boolean z2) {
        return new d(str);
    }

    private void a(File file, byte[] bArr) {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(bArr);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    public static void dq(String str) {
        File file = new File(aut + str);
        if (file.isDirectory()) {
            for (File delete : file.listFiles()) {
                delete.delete();
            }
            file.delete();
        } else if (file.exists()) {
            file.delete();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.a.b.d.a(java.lang.String, boolean, int, boolean):b.a.a.b.d
     arg types: [java.lang.String, int, int, int]
     candidates:
      b.a.a.b.d.a(int, byte[], int, int):void
      b.a.a.b.d.a(java.lang.String, boolean, int, boolean):b.a.a.b.d */
    public static d e(String str, String str2, String str3) {
        return a(str, true, 1, true);
    }

    private File fF(int i) {
        return new File(aut + this.aTj + this.separator + i + aTp);
    }

    private int getLength() {
        String[] list = AC().list();
        if (list == null) {
            return 0;
        }
        return list.length;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.a.b.d.a(java.lang.String, boolean, int, boolean):b.a.a.b.d
     arg types: [java.lang.String, boolean, int, int]
     candidates:
      b.a.a.b.d.a(int, byte[], int, int):void
      b.a.a.b.d.a(java.lang.String, boolean, int, boolean):b.a.a.b.d */
    public static d j(String str, boolean z) {
        return a(str, z, 1, true);
    }

    public int AA() {
        return getSize();
    }

    public int AB() {
        this.index++;
        int wL = wL();
        while (this.index <= wL) {
            if (fF(this.index).exists()) {
                return this.index;
            }
            this.index++;
        }
        return -1;
    }

    public void Ay() {
    }

    public int a(int i, byte[] bArr, int i2) {
        if (new File(aut + this.aTj + this.separator + i + aTp).exists()) {
            return i;
        }
        throw new b(i + " not exists");
    }

    public i a(j jVar, a aVar, boolean z) {
        return new b(this);
    }

    public void a(int i, byte[] bArr, int i2, int i3) {
        File file = new File(aut + this.aTj + this.separator + i + aTp);
        if (file.exists()) {
            try {
                a(file, bArr);
            } catch (IOException e) {
                throw new c(e.toString());
            }
        } else {
            throw new b(i + " not exist!");
        }
    }

    public void a(k kVar) {
    }

    public void b(k kVar) {
    }

    public void fG(int i) {
        File file = new File(aut + this.aTj + this.separator + i + aTp);
        if (!file.exists()) {
            throw new b(i + " not exists");
        } else if (!file.delete()) {
            throw new b(i + " not exists");
        }
    }

    public int fH(int i) {
        return (int) AC().length();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        r2 = r0.toByteArray();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a8, code lost:
        if (r0 == null) goto L_0x00ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x009b A[SYNTHETIC, Splitter:B:31:0x009b] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a0 A[SYNTHETIC, Splitter:B:34:0x00a0] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] fI(int r10) {
        /*
            r9 = this;
            r3 = 0
            r7 = 0
            java.lang.String r4 = " not exists"
            java.io.File r0 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = b.a.a.b.d.aut
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r9.aTj
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r9.separator
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r10)
            java.lang.String r2 = ".db"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            boolean r1 = r0.exists()
            if (r1 != 0) goto L_0x004d
            b.a.a.b.b r0 = new b.a.a.b.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r10)
            java.lang.String r2 = " not exists"
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x004d:
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x00c3, all -> 0x00bb }
            r1.<init>(r0)     // Catch:{ FileNotFoundException -> 0x00c3, all -> 0x00bb }
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ FileNotFoundException -> 0x00c7, all -> 0x00bf }
            r0.<init>()     // Catch:{ FileNotFoundException -> 0x00c7, all -> 0x00bf }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ FileNotFoundException -> 0x007a }
            r3 = r7
        L_0x005c:
            r4 = -1
            r5 = 0
            int r6 = r2.length     // Catch:{ IOException -> 0x0071 }
            int r5 = r1.read(r2, r5, r6)     // Catch:{ IOException -> 0x0071 }
            if (r4 == r5) goto L_0x00a4
            if (r7 == 0) goto L_0x006b
            if (r7 <= 0) goto L_0x00a4
            if (r7 < r3) goto L_0x00a4
        L_0x006b:
            int r3 = r3 + r5
            r4 = 0
            r0.write(r2, r4, r5)     // Catch:{ IOException -> 0x0071 }
            goto L_0x005c
        L_0x0071:
            r2 = move-exception
            b.a.a.b.c r2 = new b.a.a.b.c     // Catch:{ FileNotFoundException -> 0x007a }
            java.lang.String r3 = "Read Faild!"
            r2.<init>(r3)     // Catch:{ FileNotFoundException -> 0x007a }
            throw r2     // Catch:{ FileNotFoundException -> 0x007a }
        L_0x007a:
            r2 = move-exception
        L_0x007b:
            b.a.a.b.b r2 = new b.a.a.b.b     // Catch:{ all -> 0x0094 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0094 }
            r3.<init>()     // Catch:{ all -> 0x0094 }
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ all -> 0x0094 }
            java.lang.String r4 = " not exists"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0094 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0094 }
            r2.<init>(r3)     // Catch:{ all -> 0x0094 }
            throw r2     // Catch:{ all -> 0x0094 }
        L_0x0094:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r0
            r0 = r8
        L_0x0099:
            if (r1 == 0) goto L_0x009e
            r1.close()     // Catch:{ IOException -> 0x00b7 }
        L_0x009e:
            if (r2 == 0) goto L_0x00a3
            r2.close()     // Catch:{ IOException -> 0x00b9 }
        L_0x00a3:
            throw r0
        L_0x00a4:
            byte[] r2 = r0.toByteArray()     // Catch:{ FileNotFoundException -> 0x007a }
            if (r0 == 0) goto L_0x00ad
            r0.close()     // Catch:{ IOException -> 0x00b3 }
        L_0x00ad:
            if (r1 == 0) goto L_0x00b2
            r1.close()     // Catch:{ IOException -> 0x00b5 }
        L_0x00b2:
            return r2
        L_0x00b3:
            r0 = move-exception
            goto L_0x00ad
        L_0x00b5:
            r0 = move-exception
            goto L_0x00b2
        L_0x00b7:
            r1 = move-exception
            goto L_0x009e
        L_0x00b9:
            r1 = move-exception
            goto L_0x00a3
        L_0x00bb:
            r0 = move-exception
            r1 = r3
            r2 = r3
            goto L_0x0099
        L_0x00bf:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x0099
        L_0x00c3:
            r0 = move-exception
            r0 = r3
            r1 = r3
            goto L_0x007b
        L_0x00c7:
            r0 = move-exception
            r0 = r3
            goto L_0x007b
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.b.d.fI(int):byte[]");
    }

    public long getLastModified() {
        return AC().lastModified();
    }

    public String getName() {
        return this.aTj;
    }

    public int getSize() {
        return aTo;
    }

    public int getVersion() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isOpen() {
        return true;
    }

    public void j(int i, boolean z) {
    }

    public int k(byte[] bArr, int i, int i2) {
        int length = getLength();
        try {
            a(new File(aut + this.aTj + this.separator + length + aTp), bArr);
            return length;
        } catch (IOException e) {
            throw new c(e.toString());
        }
    }

    public int wL() {
        String[] list = AC().list();
        if (list == null) {
            return 0;
        }
        return list.length;
    }
}
