package b.a.a.d;

import android.app.Activity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import b.a.a.a.l;
import b.a.a.a.w;
import b.a.a.d;

public abstract class a {
    static int ain = -1;
    private Activity aio = null;

    /* access modifiers changed from: protected */
    public abstract void aO(boolean z);

    public void b(Activity activity) {
        c(activity);
    }

    /* access modifiers changed from: package-private */
    public void c(Activity activity) {
        this.aio = null;
        this.aio = activity;
        l.a(this).aG();
    }

    public final String cF(String str) {
        return null;
    }

    public final boolean cG(String str) {
        d.m(str, d.cu(str));
        return true;
    }

    public final int cH(String str) {
        return 1;
    }

    /* access modifiers changed from: package-private */
    public void finish() {
        l.a(this).finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        l.a(this).a(menu);
        return true;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        try {
            l.a(this).vm().getView().onKeyDown(i, keyEvent);
        } catch (Throwable th) {
        }
        if (i != 4) {
            return this.aio.onKeyDown(i, keyEvent);
        }
        return true;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        try {
            l.a(this).vm().getView().onKeyUp(i, keyEvent);
        } catch (Throwable th) {
        }
        if (i != 4) {
            return this.aio.onKeyUp(i, keyEvent);
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        w vm = l.a(this).vm();
        vm.Ej().a(vm.c(menuItem), vm);
        return true;
    }

    /* access modifiers changed from: protected */
    public abstract void vZ();

    /* access modifiers changed from: protected */
    public abstract void wa();

    public void wb() {
        try {
            vZ();
        } catch (b e) {
        }
    }

    public final void wc() {
        finish();
        this.aio = null;
    }

    public final void wd() {
        wa();
    }

    public final void we() {
    }

    public Activity wf() {
        return this.aio;
    }
}
