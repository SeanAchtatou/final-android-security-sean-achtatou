package b.a.a;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;
import com.uc.b.g;
import com.uc.browser.ModelBrowser;

final class b implements AdapterView.OnItemClickListener {
    final /* synthetic */ Intent TG;
    final /* synthetic */ Context VQ;
    final /* synthetic */ Uri VR;

    b(Context context, Uri uri, Intent intent) {
        this.VQ = context;
        this.VR = uri;
        this.TG = intent;
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        String str = ((ResolveInfo) adapterView.getAdapter().getItem(i)).activityInfo.packageName;
        String str2 = ((ResolveInfo) adapterView.getAdapter().getItem(i)).activityInfo.name;
        if (!"uc.ucplayer".equals(str)) {
            this.TG.setPackage(str);
            this.VQ.startActivity(this.TG);
        } else if (g.x(this.VQ)) {
            Intent intent = new Intent("uc.ucplayer.intent.action.INVOKE");
            intent.putExtra("cmd", "OPENURL");
            intent.putExtra("uri", this.VR);
            this.VQ.startActivity(intent);
        } else if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().hW();
        }
    }
}
