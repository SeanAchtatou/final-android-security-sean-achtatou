package b.a.a.a;

import android.os.Handler;
import android.view.Menu;
import b.a.a.d.a;

public class l {
    private static l agM = null;
    private w agJ;
    /* access modifiers changed from: private */
    public w agK;
    private w agL;
    Menu agN;
    Handler mHandler = new a(this);

    private l(a aVar) {
    }

    public static synchronized l a(a aVar) {
        l lVar;
        synchronized (l.class) {
            if (agM == null) {
                agM = new l(aVar);
            }
            lVar = agM;
        }
        return lVar;
    }

    public static void vi() {
        agM = null;
    }

    public void a(Menu menu) {
        this.agN = menu;
        if (this.agK != null) {
            this.agK.b(menu);
        }
    }

    public void a(h hVar, w wVar) {
        a(hVar);
        this.agJ = wVar;
    }

    public void a(w wVar) {
        try {
            this.mHandler.sendMessage(this.mHandler.obtainMessage());
            this.agL = this.agK;
            this.agK = wVar;
        } catch (Throwable th) {
        }
    }

    public void a(Runnable runnable) {
    }

    public void aG() {
        if (this.agK != null) {
            this.agK.aG();
        }
        if (this.agJ != null) {
            this.agJ.aG();
        }
    }

    public int aM(boolean z) {
        return 0;
    }

    public boolean eH(int i) {
        return false;
    }

    public boolean eI(int i) {
        return false;
    }

    public int eJ(int i) {
        return 0;
    }

    public int eK(int i) {
        return 0;
    }

    public void finish() {
        this.agJ = null;
        this.agK = null;
    }

    public int getColor(int i) {
        return 0;
    }

    public boolean vj() {
        return true;
    }

    public int vk() {
        return 0;
    }

    public int vl() {
        return 256;
    }

    public w vm() {
        return this.agK;
    }

    /* access modifiers changed from: package-private */
    public void vn() {
        a(this.agJ);
    }
}
