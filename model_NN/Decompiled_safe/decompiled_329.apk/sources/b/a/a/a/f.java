package b.a.a.a;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import com.uc.b.d;
import com.uc.c.au;
import com.uc.h.e;
import java.io.InputStream;
import java.io.OutputStream;

public class f {
    private static final int FU = -8;
    public static final int FV = 1;
    public static final int FW = 2;
    public static final int FX = 3;
    public static final int RGB_565 = 0;
    public Bitmap FQ;
    Canvas FR = null;
    int FS;
    public boolean FT = false;
    private boolean FY = true;
    private int FZ = 1;
    private int Ga = 1;
    private float Gb = 1.0f;
    int height;
    int width;

    private f(int i, int i2) {
        try {
            this.FQ = Bitmap.createBitmap(i, i2, Bitmap.Config.RGB_565);
        } catch (OutOfMemoryError e) {
            this.FQ = null;
            System.gc();
        }
        if (this.FQ != null) {
            this.FR = new Canvas(this.FQ);
        }
    }

    private f(int i, int i2, int i3) {
        Bitmap.Config config;
        switch (i3) {
            case 0:
                config = Bitmap.Config.RGB_565;
                break;
            case 1:
                config = Bitmap.Config.ARGB_8888;
                break;
            case 2:
                config = Bitmap.Config.ARGB_4444;
                break;
            case 3:
                config = Bitmap.Config.ALPHA_8;
                break;
            default:
                config = Bitmap.Config.RGB_565;
                break;
        }
        try {
            this.FQ = Bitmap.createBitmap(i, i2, config);
        } catch (OutOfMemoryError e) {
            this.FQ = null;
            System.gc();
        }
        if (this.FQ != null) {
            this.FR = new Canvas(this.FQ);
        }
    }

    private f(int i, int i2, Canvas canvas) {
        try {
            this.FQ = Bitmap.createBitmap(i, i2, Bitmap.Config.RGB_565);
        } catch (OutOfMemoryError e) {
            this.FQ = null;
            System.gc();
        }
        this.FR = canvas;
    }

    private f(Resources resources, int i) {
        try {
            this.FQ = e.Pb().a(resources, i, new BitmapFactory.Options());
        } catch (OutOfMemoryError e) {
            this.FQ = null;
            System.gc();
        }
    }

    public f(Bitmap bitmap) {
        try {
            this.FQ = Bitmap.createBitmap(bitmap);
        } catch (OutOfMemoryError e) {
            this.FQ = null;
            System.gc();
        }
    }

    public f(Bitmap bitmap, int i) {
        this.FQ = bitmap;
    }

    private f(f fVar, int i, int i2, int i3, int i4, int i5) {
        try {
            this.FQ = Bitmap.createBitmap(fVar.FQ, i, i2, i3, i4);
        } catch (OutOfMemoryError e) {
            this.FQ = null;
            System.gc();
        }
    }

    private f(InputStream inputStream) {
        try {
            this.FQ = BitmapFactory.decodeStream(inputStream);
        } catch (OutOfMemoryError e) {
            this.FQ = null;
            System.gc();
        }
    }

    private f(String str) {
        try {
            this.FQ = BitmapFactory.decodeFile(str, d.fL());
        } catch (OutOfMemoryError e) {
            this.FQ = null;
            System.gc();
        }
    }

    private f(byte[] bArr, int i, int i2) {
        try {
            this.FQ = BitmapFactory.decodeByteArray(bArr, i, i2, d.fL());
        } catch (OutOfMemoryError e) {
            this.FQ = null;
            System.gc();
        }
    }

    private f(int[] iArr, int i, int i2) {
        try {
            this.FQ = Bitmap.createBitmap(iArr, i, i2, Bitmap.Config.RGB_565);
        } catch (OutOfMemoryError e) {
            this.FQ = null;
            System.gc();
        }
    }

    public static f J(int i, int i2) {
        if (i > 0 && i2 > 0) {
            return new f(i, i2);
        }
        throw new IllegalArgumentException();
    }

    public static f a(int i, int i2, Canvas canvas) {
        if (i > 0 && i2 > 0) {
            return new f(i, i2, canvas);
        }
        throw new IllegalArgumentException();
    }

    public static final f a(Resources resources, int i) {
        return new f(resources, i);
    }

    public static f a(f fVar) {
        return new f(fVar.FQ);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        return null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static b.a.a.a.f a(b.a.a.a.f r4, int r5, int r6) {
        /*
            r3 = 0
            android.graphics.Bitmap r0 = r4.FQ     // Catch:{ Throwable -> 0x0015, all -> 0x0018 }
            if (r0 != 0) goto L_0x0007
            r0 = r3
        L_0x0006:
            return r0
        L_0x0007:
            b.a.a.a.f r0 = new b.a.a.a.f     // Catch:{ Throwable -> 0x0015, all -> 0x0018 }
            android.graphics.Bitmap r1 = r4.FQ     // Catch:{ Throwable -> 0x0015, all -> 0x0018 }
            r2 = 1
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createScaledBitmap(r1, r5, r6, r2)     // Catch:{ Throwable -> 0x0015, all -> 0x0018 }
            r2 = 1
            r0.<init>(r1, r2)     // Catch:{ Throwable -> 0x0015, all -> 0x0018 }
            goto L_0x0006
        L_0x0015:
            r0 = move-exception
            r0 = r3
            goto L_0x0006
        L_0x0018:
            r0 = move-exception
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.f.a(b.a.a.a.f, int, int):b.a.a.a.f");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0010, code lost:
        return null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static b.a.a.a.f a(b.a.a.a.f r3, int r4, int r5, int r6, int r7) {
        /*
            b.a.a.a.f r0 = new b.a.a.a.f     // Catch:{ Exception -> 0x0012, OutOfMemoryError -> 0x000f, all -> 0x000d }
            android.graphics.Bitmap r1 = r3.FQ     // Catch:{ Exception -> 0x0012, OutOfMemoryError -> 0x000f, all -> 0x000d }
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createBitmap(r1, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0012, OutOfMemoryError -> 0x000f, all -> 0x000d }
            r2 = 1
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0012, OutOfMemoryError -> 0x000f, all -> 0x000d }
        L_0x000c:
            return r0
        L_0x000d:
            r0 = move-exception
            throw r0
        L_0x000f:
            r0 = move-exception
        L_0x0010:
            r0 = 0
            goto L_0x000c
        L_0x0012:
            r0 = move-exception
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.f.a(b.a.a.a.f, int, int, int, int):b.a.a.a.f");
    }

    public static f a(f fVar, int i, int i2, int i3, int i4, int i5) {
        if ((i5 & -8) != 0) {
            throw new IllegalArgumentException();
        } else if (i >= 0 && i2 >= 0 && i + i3 <= fVar.getWidth() && i2 + i4 <= fVar.getHeight() && i3 > 0 && i4 > 0) {
            return new f(fVar, i, i2, i3, i4, i5);
        } else {
            throw new IllegalArgumentException();
        }
    }

    public static f a(int[] iArr, int i, int i2, boolean z) {
        return new f(iArr, i, i2);
    }

    public static f aw(String str) {
        if (str != null && str.startsWith(au.aGF)) {
            return null;
        }
        throw new IllegalArgumentException("File Name is Error " + str);
    }

    public static f b(InputStream inputStream) {
        return new f(inputStream);
    }

    public static f e(int i, int i2, int i3) {
        if (i > 0 && i2 > 0) {
            return new f(i, i2, i3);
        }
        throw new IllegalArgumentException();
    }

    public static f g(byte[] bArr, int i, int i2) {
        if (i >= 0 && i < bArr.length && i2 >= 0 && i + i2 <= bArr.length) {
            return new f(bArr, i, i2);
        }
        throw new ArrayIndexOutOfBoundsException();
    }

    public void K(int i, int i2) {
        if (this.FY) {
            this.FZ = this.FQ.getWidth();
            this.Ga = this.FQ.getHeight();
            this.Gb = ((float) i) / ((float) this.FZ);
            this.FY = false;
        } else {
            this.Gb = ((float) i) / ((float) this.FZ);
        }
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(this.FQ, i, i2, true);
        Bitmap bitmap = this.FQ;
        this.FQ = createScaledBitmap;
        bitmap.recycle();
        this.FT = true;
    }

    public void a(OutputStream outputStream) {
        if (this.FQ != null) {
            this.FQ.compress(Bitmap.CompressFormat.PNG, 30, outputStream);
        }
    }

    public void a(int[] iArr, int i, int i2, int i3, int i4, int i5, int i6) {
        if (this.FQ != null) {
            this.FQ.getPixels(iArr, i, i2, i3, i4, i5, i6);
        }
    }

    public int getHeight() {
        if (this.FQ != null) {
            return this.FQ.getHeight();
        }
        return 0;
    }

    public int getWidth() {
        if (this.FQ != null) {
            return this.FQ.getWidth();
        }
        return 0;
    }

    public boolean isMutable() {
        if (this.FQ == null) {
            return false;
        }
        return this.FQ.isMutable();
    }

    public u jU() {
        return new u(this.FR);
    }

    public Bitmap jV() {
        return this.Gb > 1.0f ? Bitmap.createScaledBitmap(this.FQ, this.FZ, this.Ga, true) : Bitmap.createScaledBitmap(this.FQ, this.FQ.getWidth(), this.FQ.getHeight(), true);
    }

    public void recycle() {
        if (this.FQ != null && !this.FQ.isRecycled()) {
            this.FQ.recycle();
        }
        this.FQ = null;
    }

    public String toString() {
        return this.FQ == null ? "NULL" : this.FQ.toString();
    }
}
