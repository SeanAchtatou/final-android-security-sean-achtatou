package b.a.a.a;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PathEffect;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;

public class u {
    public static final int BOTTOM = 32;
    public static final int LEFT = 4;
    public static final int RIGHT = 8;
    public static final int TOP = 16;
    public static final int blW = 1;
    public static final int blX = 2;
    public static final int blY = 64;
    public static final int blZ = 0;
    public static final int bma = 1;
    public static final int bmi = 0;
    public static final int bmj = 90;
    public static final int bmk = 180;
    public static final int bml = 270;
    public static final int bmm = -1;
    public Paint adm;
    public Canvas bmb;
    public Canvas bmc;
    private k bmd;
    private int bme;
    private float bmf;
    private Paint bmg;
    private boolean bmh;
    public k bmn;
    public Paint bmo;

    u(Canvas canvas) {
        this(k.uD(), canvas);
    }

    public u(k kVar, Canvas canvas) {
        this.bme = -1;
        this.bmf = 0.0f;
        this.bmh = false;
        this.bmn = null;
        this.bmo = null;
        this.bmd = kVar;
        this.adm = new Paint(this.bmd.getPaint());
        this.bmb = canvas;
        this.bmg = new Paint();
        this.bmg.setStyle(Paint.Style.FILL);
    }

    private float DY() {
        return this.adm != null ? (float) this.bmd.uM() : (float) k.ads;
    }

    public k DA() {
        return this.bmd;
    }

    public int DB() {
        if (this.bmb == null) {
            return 0;
        }
        if (this.bmb.getClipBounds() == null) {
            return 0;
        }
        return this.bmb.getClipBounds().left;
    }

    public int DC() {
        if (this.bmb == null) {
            return 0;
        }
        if (this.bmb.getClipBounds() == null) {
            return 0;
        }
        return this.bmb.getClipBounds().top;
    }

    public int DD() {
        if (this.bmb == null) {
            return 0;
        }
        if (this.bmb.getClipBounds() == null) {
            return 0;
        }
        return this.bmb.getClipBounds().width();
    }

    public int DE() {
        if (this.bmb == null) {
            return 0;
        }
        if (this.bmb.getClipBounds() == null) {
            return 0;
        }
        return this.bmb.getClipBounds().height();
    }

    public void DF() {
        this.bme = this.adm.getColor();
    }

    public void DG() {
        this.adm.setColor(this.bme);
    }

    public int DP() {
        return Color.blue(this.adm.getColor());
    }

    public int DQ() {
        return (int) this.adm.getStrokeWidth();
    }

    public int DR() {
        return Color.green(this.adm.getColor());
    }

    public int DS() {
        return Color.red(this.adm.getColor());
    }

    public PathEffect DT() {
        return this.adm.getPathEffect();
    }

    public int DU() {
        return (int) this.adm.getStrokeWidth();
    }

    public int DV() {
        return 0;
    }

    public int DW() {
        return 0;
    }

    public void DX() {
        this.bmd = k.a(this.bmd);
        this.adm = new Paint(this.bmd.uK());
    }

    public void DZ() {
        this.bmf = this.adm.getTextSize();
    }

    public void Ds() {
        this.bmn = this.bmd;
        this.bmo = this.adm;
    }

    public void Dt() {
        this.bmd = this.bmn;
        this.adm = this.bmo;
    }

    public void Ea() {
        this.adm.setTextSize(this.bmf);
    }

    public void K(Drawable drawable) {
        if (this.bmb != null && drawable != null) {
            drawable.draw(this.bmb);
        }
    }

    public void a(char c, int i, int i2, int i3) {
        if (this.bmb != null && this.adm != null) {
            char[] cArr = {c};
            this.bmb.drawText(cArr, 0, cArr.length, (float) i, ((float) i2) + DY(), this.adm);
        }
    }

    public void a(PathEffect pathEffect) {
        this.adm.setPathEffect(pathEffect);
    }

    public void a(f fVar, int i, int i2, int i3) {
        if (fVar != null && fVar.FQ != null) {
            this.bmb.drawBitmap(fVar.FQ, (float) i, (float) i2, this.adm);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.a.a.u.a(b.a.a.a.f, android.graphics.Rect, android.graphics.Rect, boolean):void
     arg types: [b.a.a.a.f, android.graphics.Rect, android.graphics.Rect, int]
     candidates:
      b.a.a.a.u.a(char, int, int, int):void
      b.a.a.a.u.a(b.a.a.a.f, int, int, int):void
      b.a.a.a.u.a(b.a.a.a.f, android.graphics.Rect, android.graphics.Rect, boolean):void */
    public void a(f fVar, Rect rect, Rect rect2) {
        a(fVar, rect, rect2, false);
    }

    public void a(f fVar, Rect rect, Rect rect2, boolean z) {
        if (this.bmb != null && fVar != null && fVar.FQ != null) {
            if (z) {
                this.adm.setFlags(2);
                this.adm.setDither(true);
                this.adm.setAntiAlias(true);
            }
            this.bmb.drawBitmap(fVar.FQ, rect, rect2, this.adm);
        }
    }

    public void a(k kVar, boolean z) {
        d(kVar);
    }

    public void a(String str, int i, int i2, int i3, int i4, int i5) {
        if (this.bmb != null && this.adm != null) {
            this.bmb.drawText(str, i, i2, (float) i3, ((float) i4) + DY(), this.adm);
        }
    }

    public void a(char[] cArr, int i, int i2, int i3, int i4, int i5) {
        if (this.bmb != null && this.adm != null) {
            this.bmb.drawText(cArr, i, i2, (float) i3, ((float) i4) + DY(), this.adm);
        }
    }

    public void b(String str, int i, int i2, int i3) {
        if (this.bmb != null) {
            this.bmb.drawText(str, (float) i, ((float) i2) + DY(), this.adm);
        }
    }

    public void b(int[] iArr, int i, int i2, int i3, int i4, int i5, int i6, boolean z) {
        if (this.bmb != null && this.adm != null && iArr != null) {
            f a2 = f.a(iArr, i5, i6, true);
            this.bmb.drawBitmap(a2.FQ, (float) i3, (float) i4, this.adm);
            a2.recycle();
        }
    }

    public void bf(int i, int i2) {
        if (this.bmb != null && this.adm != null) {
            Paint.Style style = this.adm.getStyle();
            this.adm.setAntiAlias(false);
            this.adm.setStyle(Paint.Style.FILL);
            this.bmb.drawPoint((float) i, (float) i2, this.adm);
            this.adm.setStyle(style);
            this.adm.setAntiAlias(true);
        }
    }

    public void c(int i, int i2, int i3, int i4, int i5, int i6) {
        if (this.bmb != null && this.adm != null) {
            Paint.Style style = this.adm.getStyle();
            this.adm.setStyle(Paint.Style.FILL);
            this.bmb.drawArc(new RectF((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4)), (float) i5, (float) i6, true, this.adm);
            this.adm.setStyle(style);
        }
    }

    public void c(f fVar, int i, int i2, int i3, int i4) {
        if (this.bmb != null && fVar != null && fVar.FQ != null) {
            Canvas canvas = this.bmb;
            Bitmap bitmap = fVar.FQ;
            if (i4 != 0) {
                if (i4 != -1) {
                    switch (i4) {
                        case 90:
                            canvas.translate((float) ((bitmap.getWidth() + bitmap.getHeight()) >> 1), 0.0f);
                            canvas.rotate(90.0f, (float) i, (float) i2);
                            canvas.drawBitmap(bitmap, (float) i, (float) i2, new Paint(2));
                            break;
                        case bmk /*180*/:
                            canvas.translate((float) bitmap.getWidth(), (float) bitmap.getHeight());
                            canvas.rotate(180.0f, (float) i, (float) i2);
                            canvas.drawBitmap(bitmap, (float) i, (float) i2, new Paint(2));
                            break;
                        case bml /*270*/:
                            canvas.translate(0.0f, (float) ((bitmap.getWidth() + bitmap.getHeight()) >> 1));
                            canvas.rotate(270.0f, (float) i, (float) i2);
                            canvas.drawBitmap(bitmap, (float) i, (float) i2, new Paint(2));
                            break;
                    }
                } else {
                    canvas.scale(-1.0f, 1.0f);
                    canvas.translate((float) ((i * -2) - bitmap.getWidth()), 0.0f);
                    canvas.drawBitmap(bitmap, (float) i, (float) i2, new Paint(2));
                }
            } else {
                canvas.drawBitmap(bitmap, (float) i, (float) i2, (Paint) null);
            }
            canvas.restore();
        }
    }

    public int cD(String str) {
        return (int) this.adm.measureText(str);
    }

    public void d(int i, int i2, int i3, int i4, int i5, int i6) {
        if (this.bmb != null && this.adm != null) {
            Paint.Style style = this.adm.getStyle();
            this.adm.setStyle(Paint.Style.STROKE);
            this.bmb.drawArc(new RectF((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4)), (float) i5, (float) i6, true, this.adm);
            this.adm.setStyle(style);
        }
    }

    public void d(k kVar) {
        this.bmd = kVar;
        this.adm = new Paint(this.bmd.uK());
        this.bmh = this.bmd.isBold();
    }

    public void e(f fVar, int i, int i2, int i3, int i4) {
        if (this.bmb != null && fVar != null && fVar.FQ != null) {
            BitmapShader bitmapShader = new BitmapShader(fVar.FQ, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
            Paint paint = new Paint(1);
            paint.setShader(bitmapShader);
            this.bmb.drawRect((float) i, (float) i2, (float) i3, (float) i4, paint);
        }
    }

    public int ev(String str) {
        return (int) this.adm.measureText(str);
    }

    public void f(int i, int i2, int i3, int i4, int i5, int i6) {
        Paint.Style style = this.adm.getStyle();
        this.adm.setStyle(Paint.Style.FILL);
        if (i4 == i6) {
            for (int i7 = 1; i7 < (i2 - i6) - 1; i7++) {
                p(i3 + i7, i4 + i7, i5 - i7, i6 + i7);
            }
            p(i, i2, i3, i4);
            p(i, i2, i5, i6);
            p(i3, i4, i5, i6);
        } else if (i3 == i5) {
            for (int i8 = 0; i8 <= i4 - i6; i8++) {
                p(i3, i6 + i8, i, i2);
            }
        }
        this.adm.setStyle(style);
    }

    public void f(f fVar, int i, int i2, int i3, int i4) {
        if (this.bmb != null && fVar != null && fVar.FQ != null) {
            this.bmb.drawBitmap(fVar.FQ, new Rect(0, 0, fVar.FQ.getWidth(), fVar.FQ.getHeight()), new Rect(i, i2, i + i3, i2 + i4), this.adm);
        }
    }

    public void g(int i, int i2, int i3, int i4, int i5, int i6) {
        if (this.bmb != null && this.adm != null) {
            Paint.Style style = this.adm.getStyle();
            this.adm.setStyle(Paint.Style.STROKE);
            this.bmb.drawRoundRect(new RectF((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4)), (float) i5, (float) i6, this.adm);
            this.adm.setStyle(style);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [float, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public void gV(int i) {
        this.adm.setColor(i | -16777216);
        if (this.bmh) {
            this.adm.setShadowLayer(k.ady, 0.0f, 0.0f, -16777216 | i);
        }
    }

    public void gW(int i) {
        this.adm.setColor(i);
    }

    public void gY(int i) {
        this.adm.setStrokeWidth((float) i);
    }

    public int getColor() {
        return this.adm.getColor();
    }

    public int getHeight() {
        return (int) this.adm.getTextSize();
    }

    public int getTextSize() {
        return (int) this.adm.getTextSize();
    }

    public void h(float f) {
        this.bmd.uK().setTextSize(f);
    }

    public void h(int i, int i2, int i3, int i4, int i5, int i6) {
        if (this.bmb != null && this.adm != null) {
            Paint.Style style = this.adm.getStyle();
            this.adm.setStyle(Paint.Style.FILL);
            this.bmb.drawRoundRect(new RectF((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4)), (float) i5, (float) i6, this.adm);
            this.adm.setStyle(style);
        }
    }

    public void ha(int i) {
        if (this.bmg != null) {
            this.bmg.setColor(-16777216 | i);
        }
    }

    public void hb(int i) {
        if (this.bmg != null) {
            this.bmg.setColor(i);
        }
    }

    public int hh(int i) {
        return 0;
    }

    public void k(int i, int i2, int i3, int i4) {
        l(i, i2, i3, i4);
    }

    public int l(char c) {
        return (int) this.adm.measureText(new char[]{c}, 0, 1);
    }

    public void l(int i, int i2, int i3, int i4) {
        if (this.bmb != null) {
            this.bmb.clipRect(i, i2, i + i3, i2 + i4);
        }
    }

    public void m(int i, int i2, int i3) {
        if (this.bmb != null) {
            Paint.Style style = this.adm.getStyle();
            this.adm.setAntiAlias(true);
            this.adm.setStyle(Paint.Style.STROKE);
            this.bmb.drawCircle((float) i, (float) i2, (float) i3, this.adm);
            this.adm.setStyle(style);
            this.adm.setAntiAlias(true);
        }
    }

    public void m(int i, int i2, int i3, int i4) {
        if (this.bmb != null && this.adm != null) {
            this.bmb.drawLine((float) i, (float) i2, (float) i3, (float) i4, this.adm);
        }
    }

    public void n(int i, int i2, int i3, int i4) {
        if (this.bmb != null && this.adm != null) {
            this.bmb.drawRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), this.bmg);
        }
    }

    public void o(int i, int i2, int i3, int i4) {
        if (this.bmb != null && this.adm != null) {
            Paint.Style style = this.adm.getStyle();
            this.adm.setStyle(Paint.Style.FILL);
            this.bmb.drawRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), this.adm);
            this.adm.setStyle(style);
        }
    }

    public void p(int i, int i2, int i3, int i4) {
        if (this.bmb != null && this.adm != null) {
            this.adm.setAntiAlias(false);
            Paint.Style style = this.adm.getStyle();
            this.adm.setStyle(Paint.Style.FILL);
            this.bmb.drawLine((float) i, (float) i2, (float) i3, (float) i4, this.adm);
            this.adm.setAntiAlias(true);
            this.adm.setStyle(style);
        }
    }

    public void q(int i, int i2, int i3, int i4) {
        if (this.bmb != null && this.adm != null) {
            Paint.Style style = this.adm.getStyle();
            this.adm.setStyle(Paint.Style.STROKE);
            this.adm.setAntiAlias(false);
            this.bmb.drawRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), this.adm);
            this.adm.setAntiAlias(true);
            this.adm.setStyle(style);
        }
    }

    public void restore() {
        if (this.bmb != null) {
            this.bmb.restore();
        }
    }

    public void save() {
        if (this.bmb != null) {
            this.bmb.save();
        }
    }

    public void scale(float f, float f2) {
        if (this.bmb != null) {
            this.bmb.scale(f, f2);
        }
    }

    public void scale(float f, float f2, float f3, float f4) {
        if (this.bmb != null) {
            this.bmb.scale(f, f2, f3, f4);
        }
    }

    public void setColor(int i) {
        this.adm.setColor(-16777216 | i);
    }

    public void setTextSize(float f) {
        this.adm.setTextSize(f);
    }

    public void translate(float f, float f2) {
        if (this.bmb != null) {
            this.bmb.translate(f, f2);
        }
    }

    public int uF() {
        if (this.bmd != null) {
            return this.bmd.uF();
        }
        return 0;
    }
}
