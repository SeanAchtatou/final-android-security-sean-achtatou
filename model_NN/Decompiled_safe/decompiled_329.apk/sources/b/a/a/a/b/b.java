package b.a.a.a.b;

import b.a.a.b.c;
import b.a.a.b.d;
import b.a.a.b.h;
import b.a.a.b.i;

public class b implements i {
    d asA;
    int index;

    public b(d dVar) {
        this.asA = dVar;
    }

    public void aQ(boolean z) {
    }

    public void destroy() {
        try {
            this.asA.Ay();
        } catch (c | h e) {
        }
    }

    public int getIndex() {
        return this.index;
    }

    public void reset() {
    }

    public boolean wC() {
        try {
            return this.index < this.asA.wL();
        } catch (h e) {
            return false;
        }
    }

    public boolean wD() {
        return this.index > 0;
    }

    public boolean wE() {
        return false;
    }

    public byte[] wF() {
        byte[] fI = this.asA.fI(this.index);
        this.index++;
        return fI;
    }

    public int wG() {
        try {
            return this.asA.AB();
        } catch (c | h e) {
            return -1;
        }
    }

    public int wH() {
        try {
            return this.asA.wL();
        } catch (h e) {
            return -1;
        }
    }

    public byte[] wI() {
        return null;
    }

    public int wJ() {
        return 0;
    }

    public void wK() {
    }

    public int wL() {
        try {
            if (this.asA != null) {
                return this.asA.wL();
            }
        } catch (Exception e) {
        }
        return 0;
    }
}
