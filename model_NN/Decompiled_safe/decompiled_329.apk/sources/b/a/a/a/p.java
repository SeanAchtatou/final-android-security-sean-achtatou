package b.a.a.a;

import android.view.ViewGroup;
import android.widget.LinearLayout;
import java.util.Enumeration;
import java.util.Vector;

public class p extends x {
    LinearLayout auE;
    int count;
    Vector lO;

    public p() {
        this("");
    }

    public p(String str) {
        this(str, null);
    }

    public p(String str, s[] sVarArr) {
        this.auE = new LinearLayout(this.aio);
        this.lO = new Vector();
        if (sVarArr != null) {
            for (s a2 : sVarArr) {
                a(a2);
            }
        }
        this.auE.setOrientation(1);
        setView(this.auE);
    }

    public int a(s sVar) {
        if (sVar instanceof t) {
            sVar.getView().setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        }
        this.auE.addView(sVar.getView());
        this.lO.add(sVar);
        Enumeration keys = sVar.bmS.keys();
        while (keys.hasMoreElements()) {
            b((m) keys.nextElement());
        }
        return this.lO.size();
    }

    public void a(int i, s sVar) {
    }

    public void a(ad adVar) {
    }

    public void b(int i, s sVar) {
    }

    public int cQ(String str) {
        return 0;
    }

    public void cT() {
        this.auE.removeAllViews();
    }

    public int d(f fVar) {
        return 0;
    }

    public void delete(int i) {
        this.auE.removeViewAt(i);
    }

    public s eV(int i) {
        return (s) this.lO.get(i);
    }

    public int size() {
        return this.lO.size();
    }
}
