package b.a.a.a;

import android.app.Dialog;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class h extends x {
    public static final int Ty = -2;
    public static final m Tz = new m("", 4, 0);
    Dialog TA;
    int TB;
    String TC;
    TextView TD;
    TableLayout TE;

    public h(String str) {
        this(str, null, null, null);
    }

    public h(String str, String str2, f fVar, o oVar) {
        this.TB = 3000;
        try {
            this.TC = str;
            this.TA = new Dialog(this.aio);
            this.TE = new TableLayout(this.aio);
            this.TA.setTitle(str);
            this.TD = new TextView(this.aio);
            this.TD.setText(str2);
            TableRow tableRow = new TableRow(this.aio);
            tableRow.addView(this.TD);
            this.TE.addView(tableRow);
            TableRow tableRow2 = new TableRow(this.aio);
            Button button = new Button(this.aio);
            button.setText("OK");
            tableRow2.addView(button);
            this.TE.addView(tableRow2);
            button.setOnClickListener(new i(this));
            this.TA.setContentView(this.TE);
        } catch (Throwable th) {
        }
    }

    public void dX(int i) {
        this.TB = i;
    }

    /* access modifiers changed from: package-private */
    public void dismiss() {
        this.TA.dismiss();
    }

    public int rB() {
        return this.TB;
    }

    public int rC() {
        return this.TB;
    }

    public String rD() {
        return this.TC;
    }

    public void setString(String str) {
        this.TD.setText(str);
    }

    /* access modifiers changed from: package-private */
    public void show() {
        this.TA.show();
        new ab(this).start();
    }
}
