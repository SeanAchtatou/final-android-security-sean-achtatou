package b.a.a.a.b;

import com.uc.c.au;
import java.io.File;
import java.util.Enumeration;

public class a implements Enumeration {
    String[] apN;
    boolean apO;
    int index;

    public a(File[] fileArr) {
        String name;
        if (fileArr != null) {
            this.apN = new String[fileArr.length];
            int i = 0;
            for (File file : fileArr) {
                String path = file.getPath();
                if (file.isDirectory()) {
                    name = path.substring(1, path.length());
                    if (name.lastIndexOf(au.aGF) != -1) {
                        name = name.substring(name.lastIndexOf(au.aGF) + 1, name.length());
                    }
                } else {
                    name = file.getName();
                }
                this.apN[i] = file.isDirectory() ? name + File.separator : name;
                i++;
            }
            if (this.apN != null && this.apN.length > 1) {
                this.apO = true;
            }
        }
    }

    a(String[] strArr) {
        this.apN = strArr;
        if (this.apN != null && this.apN.length > 1) {
            this.apO = true;
        }
    }

    public boolean hasMoreElements() {
        return this.apO;
    }

    public Object nextElement() {
        if (this.apN == null) {
            return null;
        }
        if (this.index < 0 || this.index >= this.apN.length) {
            this.apO = false;
            return null;
        }
        if (this.index == this.apN.length - 1) {
            this.apO = false;
        }
        String str = this.apN[this.index];
        this.index++;
        return str;
    }
}
