package b.a.a.a;

import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import b.a.a.d.a;
import java.util.Enumeration;
import java.util.Hashtable;

public abstract class w {
    a RM;
    Activity aio;
    l bmP;
    private View bmQ;
    protected z bmR;
    Hashtable bmS;

    public w() {
        this.bmS = new Hashtable();
    }

    w(String str) {
    }

    public z Ej() {
        return this.bmR;
    }

    /* access modifiers changed from: protected */
    public void R(int i, int i2) {
    }

    public void a(z zVar) {
        this.bmR = zVar;
    }

    /* access modifiers changed from: package-private */
    public void aG() {
    }

    public void b(Menu menu) {
        int i = 0;
        if (menu != null) {
            try {
                menu.clear();
                Enumeration keys = this.bmS.keys();
                Hashtable hashtable = new Hashtable();
                while (keys.hasMoreElements()) {
                    m mVar = (m) keys.nextElement();
                    hashtable.put(menu.add(0, i, 0, mVar.wo()), mVar);
                    i++;
                }
                this.bmS = hashtable;
            } catch (Exception e) {
            }
        }
    }

    public void b(m mVar) {
        this.bmS.put(mVar, mVar);
    }

    public m c(MenuItem menuItem) {
        return (m) this.bmS.get(menuItem);
    }

    public void c(m mVar) {
        this.bmS.remove(mVar);
    }

    public int getHeight() {
        if (this.bmQ == null) {
            return 430;
        }
        return this.bmQ.getHeight();
    }

    public String getTitle() {
        return "";
    }

    public View getView() {
        return this.bmQ;
    }

    public int getWidth() {
        if (this.bmQ == null) {
            return com.uc.c.w.Qt;
        }
        this.bmQ.getWidth();
        return com.uc.c.w.Qt;
    }

    public boolean isShown() {
        return false;
    }

    public void setView(View view) {
        this.bmQ = view;
    }

    public void t(String str) {
    }
}
