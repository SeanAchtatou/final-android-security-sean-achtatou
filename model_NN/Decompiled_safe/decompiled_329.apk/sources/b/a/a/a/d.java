package b.a.a.a;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.util.ArrayList;

public class d extends x {
    public static final m lH = new m("", 1, 0);
    public static final int lI = 0;
    public static final int lJ = 1;
    private ListView lK;
    private ArrayList lL;
    private ArrayAdapter lM;
    private int lN;

    public d(String str, int i) {
        this(str, i, new String[0], new f[0]);
    }

    public d(String str, int i, String[] strArr, f[] fVarArr) {
        this.lL = new ArrayList(100);
        this.lN = -1;
        this.lK = new ListView(this.aio);
        this.lK.setItemsCanFocus(false);
        if (i == 0) {
            this.lK.setChoiceMode(2);
            this.lM = new ArrayAdapter(this.aio, 17367056, this.lL);
        } else if (i == 1) {
            this.lK.setChoiceMode(1);
            this.lM = new ArrayAdapter(this.aio, 17367055, this.lL);
        } else {
            this.lK.setChoiceMode(0);
            this.lM = new ArrayAdapter(this.aio, 17367043, this.lL);
        }
        this.lK.setAdapter((ListAdapter) this.lM);
        setView(this.lK);
        cS();
        this.lK.setOnItemClickListener(new c(this));
    }

    private void cS() {
        this.lK.setOnItemSelectedListener(new b(this));
    }

    public f E(int i) {
        return null;
    }

    public boolean F(int i) {
        return this.lM.isEnabled(i);
    }

    public void G(int i) {
    }

    public k H(int i) {
        return null;
    }

    public int a(String str, f fVar) {
        this.lL.add(str);
        return 0;
    }

    public void a(int i, k kVar) {
    }

    public void a(int i, String str, f fVar) {
        this.lL.add(i, str);
    }

    public void a(int i, boolean z) {
        this.lK.setItemChecked(i, z);
    }

    /* access modifiers changed from: package-private */
    public void a(AdapterView adapterView, View view, int i, long j) {
        this.lN = i;
    }

    public void a(m mVar) {
    }

    public void b(int i, String str, f fVar) {
        this.lL.set(i, str);
    }

    /* access modifiers changed from: package-private */
    public void b(AdapterView adapterView, View view, int i, long j) {
        this.lN = i;
    }

    public int c(boolean[] zArr) {
        if (zArr == null || zArr.length != size()) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < zArr.length; i++) {
            zArr[i] = this.lK.isItemChecked(i);
            if (zArr[i]) {
                int i2 = 0 + 1;
            }
        }
        return 0;
    }

    public int cQ() {
        return this.lK.getSelectedItemPosition();
    }

    public void cT() {
        this.lL.clear();
    }

    public int cU() {
        return 0;
    }

    public void d(boolean[] zArr) {
        if (zArr == null || zArr.length != size()) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < zArr.length; i++) {
            this.lK.setItemChecked(i, zArr[i]);
        }
    }

    public void delete(int i) {
        this.lL.remove(this.lL.get(i));
    }

    public String getString(int i) {
        return (String) this.lL.get(i);
    }

    public int size() {
        return this.lL.size();
    }
}
