package b.a.a.a;

import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.PathEffect;
import android.graphics.Typeface;
import com.uc.c.bc;
import java.util.Vector;

public class k {
    public static final int adA = 32;
    public static final int adB = 64;
    public static final int adC = 0;
    public static final int adD = 1;
    private static final k adH = new k(0, 0, ads);
    private static int adI = 0;
    private static int adJ = 0;
    private static int adK = 0;
    private static final int adL = 0;
    private static final int adM = 1;
    private static final int adN = 2;
    public static PathEffect adO = new DashPathEffect(new float[]{5.0f, 5.0f, 5.0f, 5.0f}, 1.0f);
    public static PathEffect adP = new DashPathEffect(new float[]{10.0f, 5.0f, 10.0f, 5.0f}, 1.0f);
    public static float adQ = 1.0f;
    public static float adR = 0.0f;
    public static float adS = 2.0f;
    public static float adT = 0.16f;
    public static float adU = 160.0f;
    public static float adV = 6.0f;
    public static float adW = 60.0f;
    public static float adX = 0.7f;
    public static float adY = 10.0f;
    public static float adZ = 12.0f;
    public static final int adn = 0;
    public static final int ado = 1;
    public static final int adp = 2;
    public static final int adq = 4;
    public static int adr = 16;
    public static int ads = 19;
    public static int adt = 22;
    public static int adu = 14;
    public static int adv = 12;
    public static int adw = 24;
    public static int adx = 26;
    public static float ady = 0.4f;
    public static final int adz = 0;
    public static int aeb = 99;
    public static int aec = (aeb - 3);
    public static int aed = (aeb - 2);
    public static int aee = (aeb - 1);
    public static int aef = 100;
    private int adE;
    private int adF;
    private int adG = -1;
    private Paint adm;
    private int[] aea = new int[aeb];
    private int sm;

    private k(int i, int i2, int i3) {
        this.adE = i;
        this.adF = i2;
        this.sm = i3 == 0 ? adr : i3;
        this.adm = new Paint();
        this.adm.setAntiAlias(true);
        this.adm.setTextSize((float) this.sm);
        a(this.adm, i2);
        this.adm.setTextAlign(Paint.Align.LEFT);
    }

    private k(int i, int i2, int i3, Paint paint) {
        this.adE = i;
        this.adF = i2;
        this.sm = i3 == 0 ? adr : i3;
        this.adm = new Paint(paint);
    }

    public static k H(int i) {
        return new k(i, 0, adr);
    }

    public static k a(k kVar) {
        return new k(kVar.adE, kVar.adF, kVar.sm, kVar.adm);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [float, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    private void a(Paint paint, int i) {
        Typeface typeface = Typeface.DEFAULT;
        switch (i) {
            case 1:
                this.adm.setShadowLayer(ady, 0.0f, 0.0f, -16777216);
                break;
            case 3:
                this.adm.setShadowLayer(ady, 0.0f, 0.0f, -16777216);
                break;
        }
        paint.setTypeface(Typeface.create(typeface, 0));
    }

    public static void e(float f) {
        adQ = f;
    }

    public static int eF(int i) {
        return i == 0 ? (int) (((float) adr) * adQ) : 1 == i ? (int) (((float) ads) * adQ) : (int) (((float) adt) * adQ);
    }

    public static k k(int i, int i2, int i3) {
        return new k(i, i2, i3);
    }

    public static void uC() {
        Paint paint = new Paint();
        paint.setTextSize((float) adr);
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        adI = ((int) ((Math.abs(fontMetrics.top) + Math.abs(fontMetrics.bottom)) - ((float) adr))) + 1;
        paint.setTextSize((float) ads);
        Paint.FontMetrics fontMetrics2 = paint.getFontMetrics();
        adJ = ((int) ((Math.abs(fontMetrics2.top) + Math.abs(fontMetrics2.bottom)) - ((float) ads))) + 1;
        paint.setTextSize((float) adt);
        Paint.FontMetrics fontMetrics3 = paint.getFontMetrics();
        adK = ((int) ((Math.abs(fontMetrics3.top) + Math.abs(fontMetrics3.bottom)) - ((float) adt))) + 1;
    }

    public static k uD() {
        return adH;
    }

    public static float uL() {
        return adQ;
    }

    private void uO() {
        for (int i = 0; i < this.aea.length; i++) {
            this.aea[i] = 0;
        }
    }

    public int a(char c) {
        return (int) this.adm.measureText(new char[]{c}, 0, 1);
    }

    public int a(char[] cArr) {
        if (cArr == null) {
            return 0;
        }
        return b(cArr, 0, cArr.length);
    }

    public int a(char[] cArr, int i, int i2) {
        return (int) this.adm.measureText(cArr, i, i2);
    }

    public int b(char c) {
        int i = (c < 19968 || c > 40869) ? (c < ' ' || c > '~') ? -1 : c - ' ' : 95;
        if (i == -1) {
            return a(c);
        }
        if (this.aea[i] == 0) {
            this.aea[i] = a(i == 95 ? 19968 : c);
        }
        return this.aea[i];
    }

    public int b(String str, int i, boolean z) {
        int i2;
        int length = str.length();
        int a2 = cD(str) <= i ? length : i / a(26342);
        if (z) {
            int indexOf = str.indexOf(13);
            int indexOf2 = str.indexOf(10);
            if (indexOf == -1 && indexOf2 == -1) {
                i2 = -1;
            } else {
                int min = Math.min(indexOf, indexOf2);
                i2 = min == -1 ? Math.max(indexOf, indexOf2) : min;
            }
            if (i2 != -1 && i2 <= a2) {
                return i2 + 1;
            }
        }
        if (a2 >= length) {
            return length;
        }
        int cD = cD(str.substring(0, a2));
        int a3 = a(str.charAt(a2));
        int i3 = a2;
        while (true) {
            int i4 = a3;
            if (cD + i4 >= i) {
                return i3;
            }
            int i5 = i4 + cD;
            int i6 = i3 + 1;
            if (i6 > length) {
                return i6;
            }
            a3 = a(str.charAt(i6));
            i3 = i6;
            cD = i5;
        }
    }

    public int b(char[] cArr, int i, int i2) {
        if (cArr == null) {
            return 0;
        }
        if (i < 0 || i >= cArr.length || i + i2 > cArr.length) {
            return 0;
        }
        return (int) this.adm.measureText(cArr, i, i2);
    }

    public String b(String str, int i, String str2) {
        int n = n(str, i - cE(str2));
        if (n == str.length()) {
            return str;
        }
        return bc.Z(str.substring(0, n), str2 == null ? ".." : str2);
    }

    public int cD(String str) {
        return (int) this.adm.measureText(str);
    }

    public int cE(String str) {
        if (str == null) {
            return 0;
        }
        int length = str.length();
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            i += b(str.charAt(i2));
        }
        return i;
    }

    public int getHeight() {
        return this.sm;
    }

    public Paint getPaint() {
        return this.adm;
    }

    public int getSize() {
        return this.sm;
    }

    public int getStyle() {
        return this.adF;
    }

    public boolean isBold() {
        return this.adF == 1 || this.adF == 3;
    }

    public boolean isItalic() {
        return this.adF == 2 || this.adF == 3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.a.a.k.b(java.lang.String, int, boolean):int
     arg types: [java.lang.String, int, int]
     candidates:
      b.a.a.a.k.b(char[], int, int):int
      b.a.a.a.k.b(java.lang.String, int, java.lang.String):java.lang.String
      b.a.a.a.k.b(java.lang.String, int, boolean):int */
    public int n(String str, int i) {
        return b(str, i, false);
    }

    public char[][] o(String str, int i) {
        Vector vector = new Vector(5);
        String str2 = str;
        while (true) {
            int n = n(str2, i);
            if (n == str2.length()) {
                break;
            }
            vector.addElement(str2.substring(0, n));
            str2 = str2.substring(n);
        }
        vector.addElement(str2);
        int size = vector.size();
        char[][] cArr = new char[size][];
        for (int i2 = 0; i2 < size; i2++) {
            cArr[i2] = ((String) vector.elementAt(i2)).toCharArray();
        }
        vector.removeAllElements();
        return cArr;
    }

    public void setSize(int i) {
        this.sm = i;
        this.adm.setTextSize((float) i);
        uO();
    }

    public int uE() {
        if (this.adG != -1) {
            this.adG = (int) Math.abs((this.adm.ascent() + this.adm.descent()) / 2.0f);
        }
        return this.adG;
    }

    public int uF() {
        if (getHeight() == adr) {
            return adI;
        }
        if (getHeight() == ads) {
            return adJ;
        }
        if (getHeight() == adt) {
            return adK;
        }
        return 0;
    }

    public int uG() {
        return this.adE;
    }

    public boolean uH() {
        return this.adF == 0;
    }

    public boolean uI() {
        return false;
    }

    public int uJ() {
        return 0;
    }

    public Paint uK() {
        return this.adm;
    }

    public int uM() {
        if (this.aea[aee] == 0) {
            this.aea[aee] = (int) this.adm.getTextSize();
        }
        return this.aea[aee];
    }

    public int uN() {
        if (this.aea[aed] == 0) {
            Paint.FontMetrics fontMetrics = this.adm.getFontMetrics();
            this.aea[aed] = (int) (Math.abs(fontMetrics.top) + Math.abs(fontMetrics.bottom));
        }
        return this.aea[aed];
    }

    public int uP() {
        return b(19968);
    }

    public int uQ() {
        if (this.aea[aec] == 0) {
            this.aea[aec] = Math.abs((int) (this.adm.ascent() - this.adm.getFontMetrics().top));
        }
        return this.aea[aec];
    }
}
