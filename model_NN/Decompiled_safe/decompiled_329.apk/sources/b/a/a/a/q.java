package b.a.a.a;

import android.widget.RadioButton;
import android.widget.RadioGroup;

public class q extends s {
    public static final int auF = 1;
    public static final int auG = 4;
    public static final int auH = 0;
    public static final int auI = 1;
    public static final int auJ = 2;
    public static final int lI = 2;
    public static final int lJ = 3;
    RadioGroup auK;

    public q(String str, int i) {
        this(str, i, null, null);
    }

    public q(String str, int i, String[] strArr, f[] fVarArr) {
        this(str, i, strArr, fVarArr, false);
    }

    q(String str, int i, String[] strArr, f[] fVarArr, boolean z) {
        super(str);
        this.auK = new RadioGroup(this.aio);
        setView(this.auK);
        if (strArr != null) {
            for (int i2 = 0; i2 < strArr.length; i2++) {
                a(strArr[i2], fVarArr[i2]);
            }
        }
    }

    private RadioButton cR(String str) {
        RadioButton radioButton = new RadioButton(this.aio);
        radioButton.setText(str);
        return radioButton;
    }

    public f E(int i) {
        return null;
    }

    public boolean F(int i) {
        return ((RadioButton) this.auK.getChildAt(i)).isChecked();
    }

    public void G(int i) {
    }

    public k H(int i) {
        return null;
    }

    public int a(String str, f fVar) {
        this.auK.addView(cR(str));
        return this.auK.getChildCount();
    }

    public void a(int i, k kVar) {
    }

    public void a(int i, String str, f fVar) {
        this.auK.addView(cR(str), i);
    }

    public void a(int i, boolean z) {
        ((RadioButton) this.auK.getChildAt(i)).setChecked(z);
    }

    public void b(int i, String str, f fVar) {
    }

    public int c(boolean[] zArr) {
        int i = 0;
        if (zArr == null || zArr.length != size()) {
            throw new IllegalArgumentException();
        }
        int i2 = 0;
        while (i2 < zArr.length) {
            zArr[i2] = ((RadioButton) this.auK.getChildAt(i2)).isChecked();
            if (zArr[i2]) {
                int i3 = i + 1;
            }
            i2++;
            i = i;
        }
        return i;
    }

    public int cQ() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.auK.getChildCount()) {
                return -1;
            }
            if (((RadioButton) this.auK.getChildAt(i2)).isChecked()) {
                return i2;
            }
            i = i2 + 1;
        }
    }

    public void cT() {
        this.auK.removeAllViews();
    }

    public int cU() {
        return 0;
    }

    public void d(boolean[] zArr) {
        if (zArr == null || zArr.length != size()) {
            throw new IllegalArgumentException();
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < zArr.length) {
                ((RadioButton) this.auK.getChildAt(i2)).setChecked(zArr[i2]);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void delete(int i) {
        this.auK.removeViewAt(i);
    }

    public String getString(int i) {
        return ((RadioButton) this.auK.getChildAt(i)).getText().toString();
    }

    public int size() {
        return this.auK.getChildCount();
    }
}
