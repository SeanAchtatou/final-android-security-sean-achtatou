package b.a.a;

import android.app.Dialog;
import android.view.View;
import android.widget.AdapterView;

final class c implements AdapterView.OnItemClickListener {
    final /* synthetic */ AdapterView.OnItemClickListener Wc;
    final /* synthetic */ Dialog Wd;

    c(AdapterView.OnItemClickListener onItemClickListener, Dialog dialog) {
        this.Wc = onItemClickListener;
        this.Wd = dialog;
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.Wc != null) {
            this.Wc.onItemClick(adapterView, view, i, j);
        }
        this.Wd.dismiss();
    }
}
