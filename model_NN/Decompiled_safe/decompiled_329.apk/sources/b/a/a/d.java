package b.a.a;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import b.a.a.a.t;
import com.uc.a.n;
import com.uc.b.b;
import com.uc.browser.ActivityBrowser;
import com.uc.browser.ActivityEditMyNavi;
import com.uc.browser.ActivityFlash;
import com.uc.browser.AdapterApplicationList;
import com.uc.browser.ModelBrowser;
import com.uc.browser.R;
import com.uc.browser.UCAlertDialog;
import com.uc.browser.UCR;
import com.uc.h.e;
import com.uc.i.g;
import com.uc.plugin.ad;
import java.io.File;
import java.util.List;

public class d {
    public static final int AUDIO = 2;
    public static final int ERROR = -5;
    public static final int Jh = 3;
    public static final int SUCCESS = 0;
    public static final int TEXT = 4;
    public static final int aaA = 23;
    public static final int aaB = 24;
    public static final int aaC = 25;
    public static final int aaD = 26;
    private static final String[][] aaE = {new String[0], new String[]{"gif", "tif", "tiff", "bmp", "wbmp", "cod", "fif"}, new String[]{"wma", "aif", "aiff", "mid", "midi", "mp2", "acc", "wav", "ram", "ra", "stream", "rpm", "au", "snd", "es", "voi"}, new String[]{"wmv", "mpeg", "mpg", "mpe", "qt", "mov", "rm", "3gp", "mp4", "mkv", "rmvb", "viv", "vivo", "movie"}, new String[]{"txt"}, new String[]{"pdf"}, new String[]{"apk"}, new String[]{"doc", "docx", "dot"}, new String[]{"xls", "xlsx", "xla"}, new String[]{"ppt", "pptx", "pps", "pot", "ppz"}, new String[]{"chm", "hlp"}, new String[]{"flv"}, new String[]{"swf"}, new String[]{"jad"}, new String[]{"jar"}, new String[]{"mp3"}, new String[]{"avi"}, new String[]{"png"}, new String[]{"jpg", "jpeg", "jpe"}, new String[]{"gif"}, new String[]{"zip", "rar"}, new String[]{"htm", "html"}, new String[]{"ucs"}, new String[]{"torrent"}, new String[]{"uct"}, new String[]{"upp"}, new String[]{"uhtml"}};
    public static final int aaF = -1;
    public static final int aaG = -2;
    public static final int aaH = -3;
    public static final int aaI = -4;
    public static final int aaJ = -6;
    public static final int aaK = -7;
    public static final int aaL = -8;
    public static String aaM = "没有可用程序打开本文件";
    public static String aaN = "无法识别文件类型";
    public static String aaO = "打开文件出现异常";
    static String aaP = null;
    public static final String[] aaf = {"", "image/*", "audio/*", "video/*", "text/plain", "application/pdf", "application/vnd.android.package-archive", "application/msword", "application/msexcel", "application/mspowerpoint", "application/mshelp", "video/x-flv", "application/x-shockwave-flash", "text/vnd.sun.j2me.app-descriptor", "pplication/java-archive", "audio/*", "video/x-msvideo", "image/jpeg", "image/png", "image/gif", "application/zip", "text/plain", "video/ucs", "application/x-bittorrent", "resource/uct", "resource/upp", "resource/uhtml"};
    public static final int aag = 0;
    public static final int aah = 1;
    public static final int aai = 5;
    public static final int aaj = 6;
    public static final int aak = 7;
    public static final int aal = 8;
    public static final int aam = 9;
    public static final int aan = 10;
    public static final int aao = 11;
    public static final int aap = 12;
    public static final int aaq = 13;
    public static final int aar = 14;
    public static final int aas = 15;
    public static final int aat = 16;
    public static final int aau = 17;
    public static final int aav = 18;
    public static final int aaw = 19;
    public static final int aax = 20;
    public static final int aay = 21;
    public static final int aaz = 22;
    private static Context cr;

    public static int L(String str, String str2) {
        return b(cr, str, str2);
    }

    public static int a(Context context, String str, int i) {
        return b(context, str, aaf[i]);
    }

    public static int a(Context context, byte[] bArr) {
        if (context == null) {
            return -1;
        }
        if (bArr == null || bArr.length <= 0) {
            return -7;
        }
        Intent intent = new Intent("uc.ucplayer.intent.action.OPENUCS");
        intent.putExtra("ucsbuffer", bArr);
        intent.setFlags(intent.getFlags() | t.bie);
        try {
            context.startActivity(intent);
            return 0;
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, aaM, 0).show();
            return -3;
        } catch (Exception e2) {
            Toast.makeText(context, aaO, 0).show();
            return -4;
        }
    }

    public static int a(Context context, String[] strArr) {
        if (context == null) {
            return -1;
        }
        if (strArr == null || strArr.length <= 0 || strArr[0] == null || strArr[0].length() <= 0) {
            return -6;
        }
        Intent intent = new Intent("uc.ucdl.intent.action.NEW_TASK");
        String str = strArr[0];
        if (str.startsWith("/sdcard/")) {
            str = "file://" + str;
        }
        intent.putExtra(ActivityEditMyNavi.bvi, str);
        if (strArr[1] == null) {
            strArr[1] = "";
        }
        intent.putExtra(ActivityFlash.cal, strArr[1]);
        if (strArr[2] == null) {
            strArr[2] = "";
        }
        intent.putExtra("ref", strArr[2]);
        intent.setFlags(intent.getFlags() | t.bie);
        try {
            context.startActivity(intent);
            return 0;
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, aaM, 0).show();
            return -3;
        } catch (Exception e2) {
            Toast.makeText(context, aaO, 0).show();
            return -4;
        }
    }

    public static Dialog a(Context context, String str, List list, AdapterView.OnItemClickListener onItemClickListener) {
        if (b.ec() == 0) {
            return null;
        }
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.f891dlg_application_list, (ViewGroup) null);
        builder.c(inflate);
        builder.L(str);
        builder.c((int) R.string.cancel, (DialogInterface.OnClickListener) null);
        AdapterApplicationList adapterApplicationList = new AdapterApplicationList();
        adapterApplicationList.g(list);
        ListView listView = (ListView) inflate.findViewById(R.id.list);
        listView.setVerticalFadingEdgeEnabled(false);
        listView.setSelector(e.Pb().getDrawable(UCR.drawable.aXb));
        listView.setDivider(e.Pb().getDrawable(UCR.drawable.aUo));
        listView.setDividerHeight(2);
        listView.setCacheColorHint(0);
        listView.setAdapter((ListAdapter) adapterApplicationList);
        UCAlertDialog fh = builder.fh();
        listView.setOnItemClickListener(new c(onItemClickListener, fh));
        fh.show();
        return fh;
    }

    public static void a(Context context, String str, n nVar) {
        if (context != null && str != null && nVar != null) {
            new g(context, nVar).eB(str);
        }
    }

    public static int b(Context context, String str, String str2) {
        if (context == null) {
            return -1;
        }
        if (str == null) {
            return -6;
        }
        if (str.endsWith("." + aaE[23][0])) {
            return a(context, new String[]{str, "", ""});
        } else if (str.endsWith("." + aaE[24][0])) {
            return f(context, str);
        } else {
            if (str2 == null || str2.equals(aaf[25])) {
                ad.h(context, str);
                return 1;
            }
            Uri parse = str.startsWith("/sdcard/") ? Uri.parse("file://" + str) : Uri.parse(str);
            if (aaf[26].equals(str2)) {
                ModelBrowser gD = ModelBrowser.gD();
                if (gD != null) {
                    gD.a((int) ModelBrowser.zS, parse.toString());
                    context.startActivity(new Intent(context, ActivityBrowser.class));
                }
                return 1;
            }
            Intent intent = new Intent("android.intent.action.VIEW");
            if (str2 == null || str2.equals(aaf[0])) {
                Toast.makeText(context, aaN, 0).show();
                return -2;
            }
            intent.setDataAndType(parse, str2);
            if (str2 == null || str2.equals(aaf[22])) {
                intent.setClassName("uc.ucplayer", "uc.ucplayer.activity.ActivitySplash");
            }
            intent.setFlags(intent.getFlags() | t.bie);
            try {
                context.startActivity(intent);
                return 0;
            } catch (ActivityNotFoundException e) {
                Toast.makeText(context, aaM, 0).show();
                return -3;
            } catch (Exception e2) {
                Toast.makeText(context, aaO, 0).show();
                return -4;
            }
        }
    }

    public static void b(Context context, String str) {
        if (context != null && str != null) {
            try {
                int indexOf = str.toLowerCase().indexOf(b.b.acQ);
                if (indexOf != -1) {
                    Uri parse = Uri.parse(b.b.acQ + str.substring(indexOf + b.b.acQ.length()));
                    Intent intent = new Intent();
                    intent.setAction("android.intent.action.VIEW");
                    intent.setData(parse);
                    if (b.ec() == 0) {
                        context.startActivity(intent);
                        return;
                    }
                    List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
                    if (!com.uc.b.g.i(queryIntentActivities) && com.uc.b.g.y(context)) {
                        ResolveInfo resolveInfo = new ResolveInfo();
                        ActivityInfo activityInfo = new ActivityInfo();
                        activityInfo.packageName = "uc.ucplayer";
                        resolveInfo.activityInfo = activityInfo;
                        queryIntentActivities.add(0, resolveInfo);
                    }
                    if (queryIntentActivities == null || queryIntentActivities.size() == 0) {
                        Toast.makeText(cr, aaM, 0).show();
                    } else {
                        a(context, "请选择播放器", queryIntentActivities, new b(context, parse, intent));
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    public static void close() {
        cr = null;
    }

    public static int cu(String str) {
        if (str == null) {
            return 0;
        }
        int lastIndexOf = str.lastIndexOf(46);
        if (lastIndexOf == -1) {
            return 0;
        }
        int i = lastIndexOf + 8;
        while (i > str.length()) {
            i--;
        }
        String substring = str.substring(lastIndexOf + 1, i);
        int indexOf = substring.indexOf(63);
        if (indexOf > 0) {
            substring = substring.substring(0, indexOf);
        }
        String lowerCase = substring.toLowerCase();
        for (int i2 = 1; i2 < aaE.length; i2++) {
            for (String equals : aaE[i2]) {
                if (lowerCase.equals(equals)) {
                    return i2;
                }
            }
        }
        return 0;
    }

    public static int cv(String str) {
        return b(cr, str, aaf[2]);
    }

    public static int cw(String str) {
        return b(cr, str, aaf[3]);
    }

    public static int cx(String str) {
        return b(cr, str, aaf[1]);
    }

    public static int cy(String str) {
        return b(cr, str, aaf[4]);
    }

    public static int cz(String str) {
        return b(cr, str, aaf[6]);
    }

    public static void d(Context context) {
        cr = context;
    }

    public static int e(Context context, String str) {
        if (context == null) {
            return -1;
        }
        if (str == null || str.length() <= 0) {
            return -6;
        }
        Intent intent = new Intent("uc.ucplayer.intent.action.OPENUCS");
        intent.putExtra("ucs", str.startsWith("/sdcard/") ? "file://" + str : str);
        intent.setFlags(intent.getFlags() | t.bie);
        try {
            context.startActivity(intent);
            return 0;
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, aaM, 0).show();
            return -3;
        } catch (Exception e2) {
            Toast.makeText(context, aaO, 0).show();
            return -4;
        }
    }

    public static int f(Context context, String str) {
        aaP = str;
        if (aaP.startsWith("file://")) {
            aaP = str.substring(7, str.length());
        }
        if (context == null) {
            return -1;
        }
        if (aaP == null || aaP.length() <= 0) {
            return -6;
        }
        File file = new File("/data/data/com.uc.browser/theme");
        if (!file.exists()) {
            file.mkdir();
        }
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.L("皮肤安装");
        com.uc.h.d dVar = new com.uc.h.d(cr);
        if (dVar.eS(aaP)) {
            builder.M("皮肤已经存在，覆盖安装并应用皮肤吗？");
        } else {
            builder.M("安装并应用皮肤吗？");
        }
        builder.a((int) R.string.ok, new a(dVar, context));
        builder.c((int) R.string.cancel, (DialogInterface.OnClickListener) null);
        builder.fh().show();
        return 0;
    }

    public static void g(Context context, String str) {
        String substring;
        if (context != null && str != null) {
            try {
                String lowerCase = str.toLowerCase();
                int indexOf = lowerCase.indexOf(b.b.acP);
                if (indexOf != -1) {
                    substring = str.substring(b.b.acP.length() + indexOf);
                } else {
                    int indexOf2 = lowerCase.indexOf(b.b.acO);
                    substring = indexOf2 != -1 ? str.substring(indexOf2 + b.b.acO.length()) : str;
                }
                String trim = substring.trim();
                if (trim.length() != 0) {
                    Uri parse = Uri.parse(b.b.acN + trim);
                    Intent intent = new Intent();
                    intent.setAction("android.intent.action.VIEW");
                    intent.setData(parse);
                    context.startActivity(intent);
                }
            } catch (Exception e) {
            }
        }
    }

    public static int m(String str, int i) {
        return b(cr, str, aaf[i]);
    }
}
