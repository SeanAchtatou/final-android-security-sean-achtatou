package b.a.a.c.b;

import b.a.a.c.e;
import b.b.a;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Hashtable;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.ConnectionPoolTimeoutException;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class b implements e {
    static HttpRequestRetryHandler rc = new a();
    private static final int rd = 200;
    private static int re = 0;
    private static int rf = 8192;
    private static int rg = 16384;
    private static Hashtable rh = new Hashtable(4);
    private static final byte ri = 0;
    private static final byte rj = 1;
    private static final byte rk = 2;
    private DefaultHttpClient qS;
    private HttpResponse qT;
    private String qU;
    private Hashtable qV;
    private String qW;
    private HttpRequestBase qX;
    private ByteArrayOutputStream qY;
    private BufferedInputStream qZ;
    private Header[] ra;
    private HttpEntity rb;
    private byte rl = 0;
    private short rm = 0;

    b() {
    }

    b(String str) {
        this.qU = str;
        String OQ = a.OQ();
        this.qS = G(OQ == null ? "" : OQ);
        this.rl = 1;
        BasicHttpParams params = this.qS.getParams();
        if (a.OT()) {
            HttpConnectionParams.setSocketBufferSize(params, rg);
        } else {
            HttpConnectionParams.setSocketBufferSize(params, rf);
        }
        this.qS.setParams(params);
    }

    private static DefaultHttpClient G(String str) {
        DefaultHttpClient defaultHttpClient = (DefaultHttpClient) rh.get(str);
        if (defaultHttpClient != null) {
            return defaultHttpClient;
        }
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpClientParams.setRedirecting(basicHttpParams, false);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, com.uc.h.e.bZw);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        HttpConnectionParams.setTcpNoDelay(basicHttpParams, true);
        ConnManagerParams.setMaxTotalConnections(basicHttpParams, 200);
        ConnPerRouteBean connPerRouteBean = new ConnPerRouteBean(20);
        connPerRouteBean.setMaxForRoute(new HttpRoute(new HttpHost("locahost", 80)), 5);
        ConnManagerParams.setMaxConnectionsPerRoute(basicHttpParams, connPerRouteBean);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schemeRegistry.register(new Scheme("https", a.b.a.yq(), 443));
        DefaultHttpClient defaultHttpClient2 = new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
        defaultHttpClient2.setHttpRequestRetryHandler(rc);
        defaultHttpClient2.setCookieStore(new e());
        rh.put(str, defaultHttpClient2);
        return defaultHttpClient2;
    }

    public static b a(String str, int i) {
        b bVar = new b();
        bVar.qU = str;
        bVar.rl = 2;
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        BasicHttpParams params = defaultHttpClient.getParams();
        if (a.OT()) {
            HttpConnectionParams.setSocketBufferSize(params, rg);
        } else {
            HttpConnectionParams.setSocketBufferSize(params, rf);
        }
        HttpConnectionParams.setConnectionTimeout(params, i);
        HttpConnectionParams.setSoTimeout(params, i);
        defaultHttpClient.setParams(params);
        bVar.qS = defaultHttpClient;
        return bVar;
    }

    public void close() {
        try {
            if (this.qX != null) {
                this.qX.abort();
            }
        } catch (Exception e) {
        } finally {
            this.qX = null;
        }
        try {
            if (this.qT != null) {
                this.qT.getEntity().consumeContent();
            }
        } catch (Exception e2) {
        } finally {
            this.qT = null;
        }
        if (this.qV != null) {
            this.qV.clear();
            this.qV = null;
        }
        try {
            if (this.qY != null) {
                this.qY.close();
            }
        } catch (Exception e3) {
        } finally {
            this.qY = null;
        }
        try {
            if (this.qZ != null) {
                this.qZ.close();
            }
        } catch (Exception e4) {
        } finally {
            this.qZ = null;
        }
        if (this.rl == 0 || this.rl == 2) {
            try {
                if (this.qS != null) {
                    this.qS.getConnectionManager().shutdown();
                }
            } catch (Exception e5) {
            }
        } else {
            int i = re + 1;
            re = i;
            if (i > 200) {
                re = 0;
                Enumeration elements = rh.elements();
                if (elements != null) {
                    while (elements.hasMoreElements()) {
                        ((DefaultHttpClient) elements.nextElement()).getConnectionManager().closeExpiredConnections();
                    }
                }
            }
        }
        this.qS = null;
    }

    public OutputStream cw() {
        if (this.qY == null) {
            this.qY = new ByteArrayOutputStream();
        }
        return this.qY;
    }

    public DataOutputStream cx() {
        if (this.qY == null) {
            this.qY = new ByteArrayOutputStream();
        }
        return new DataOutputStream(this.qY);
    }

    public void eA() {
        boolean z = a.OR() == 3;
        String OV = a.OV();
        if (!z || OV == null || OV.length() <= 0) {
            this.qS.getParams().setParameter("http.route.default-proxy", null);
        } else {
            this.qS.getParams().setParameter("http.route.default-proxy", new HttpHost(a.OV(), a.OW()));
        }
    }

    public short ex() {
        return this.rm;
    }

    public DataInputStream ey() {
        return new DataInputStream(ez());
    }

    public InputStream ez() {
        if (this.qZ == null && this.qT != null) {
            if (this.rl == 2) {
                return this.qT.getEntity().getContent();
            }
            this.qZ = new BufferedInputStream(this.qT.getEntity().getContent());
        }
        return this.qZ;
    }

    public long getDate() {
        return 0;
    }

    public String getEncoding() {
        return null;
    }

    public long getExpiration() {
        return 0;
    }

    public String getFile() {
        return null;
    }

    public String getHeaderField(int i) {
        if (this.ra != null && i < this.ra.length) {
            return this.ra[i].getValue();
        }
        throw new IOException();
    }

    public String getHeaderField(String str) {
        if (str == null) {
            throw new IOException();
        }
        if (this.ra != null) {
            int length = this.ra.length;
            for (int i = 0; i < length; i++) {
                if (str.equalsIgnoreCase(this.ra[i].getName())) {
                    return this.ra[i].getValue();
                }
            }
        }
        return null;
    }

    public long getHeaderFieldDate(String str, long j) {
        return 0;
    }

    public int getHeaderFieldInt(String str, int i) {
        return 0;
    }

    public String getHeaderFieldKey(int i) {
        if (this.ra == null || i >= this.ra.length) {
            return null;
        }
        return this.ra[i].getName();
    }

    public String getHost() {
        return null;
    }

    public long getLastModified() {
        return 0;
    }

    public long getLength() {
        if (this.rb != null) {
            return this.rb.getContentLength();
        }
        return 0;
    }

    public int getPort() {
        return 0;
    }

    public String getProtocol() {
        return null;
    }

    public String getQuery() {
        return null;
    }

    public String getRef() {
        return null;
    }

    public String getRequestMethod() {
        return this.qW;
    }

    public String getRequestProperty(String str) {
        Header[] headers = this.qX.getHeaders(str);
        if (headers == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (Header value : headers) {
            sb.append(value.getValue()).append(';').append(' ');
        }
        int length = sb.length();
        if (length >= 2) {
            sb.delete(length - 2, length);
        }
        return sb.toString().trim();
    }

    public int getResponseCode() {
        try {
            if (this.qT != null) {
                int statusCode = this.qT.getStatusLine().getStatusCode();
                if (this.qY == null) {
                    return statusCode;
                }
                this.qY.close();
                this.qY = null;
                return statusCode;
            } else if (this.qX == null || this.qS == null) {
                throw new IOException();
            } else {
                if (this.qV != null && this.qV.size() > 0) {
                    Hashtable hashtable = this.qV;
                    Enumeration keys = hashtable.keys();
                    while (keys.hasMoreElements()) {
                        String str = (String) keys.nextElement();
                        this.qX.setHeader(str, (String) hashtable.get(str));
                    }
                    this.qV = null;
                }
                if (this.qY != null) {
                    this.qX.setEntity(new ByteArrayEntity(this.qY.toByteArray()));
                }
                System.currentTimeMillis();
                String scheme = this.qX.getURI().getScheme();
                String host = this.qX.getURI().getHost();
                int port = this.qX.getURI().getPort();
                if (port < 0 && "https".equals(scheme)) {
                    port = 443;
                }
                HttpHost httpHost = new HttpHost(host, port, scheme);
                eA();
                try {
                    this.qT = this.qS.execute(httpHost, this.qX);
                } catch (NullPointerException e) {
                    this.qT = this.qS.execute(httpHost, this.qX);
                }
                if (this.qT != null) {
                    this.rb = this.qT.getEntity();
                    if (this.rb != null) {
                        this.qZ = new BufferedInputStream(this.rb.getContent());
                    }
                    this.ra = this.qT.getAllHeaders();
                    int statusCode2 = this.qT.getStatusLine().getStatusCode();
                    if (statusCode2 == 200) {
                        this.rm = 0;
                    } else if (statusCode2 < 300 || statusCode2 > 307) {
                        this.rm = e.aXV;
                    } else {
                        this.rm = -3;
                    }
                    if (this.qY == null) {
                        return statusCode2;
                    }
                    this.qY.close();
                    this.qY = null;
                    return statusCode2;
                }
                this.rm = e.aXV;
                if (this.qY != null) {
                    this.qY.close();
                    this.qY = null;
                }
                return -1;
            }
        } catch (Exception e2) {
            if (e2 instanceof NoHttpResponseException) {
                this.rm = -2;
            } else if ((e2 instanceof ConnectTimeoutException) || (e2 instanceof SocketTimeoutException) || (e2 instanceof ConnectionPoolTimeoutException)) {
                this.rm = -1;
            } else {
                this.rm = e.aXV;
            }
            throw new IOException();
        } catch (Throwable th) {
            if (this.qY != null) {
                this.qY.close();
                this.qY = null;
            }
            throw th;
        }
    }

    public String getResponseMessage() {
        if (this.qT != null) {
            return this.qT.getStatusLine().getReasonPhrase();
        }
        return null;
    }

    public String getType() {
        return null;
    }

    public String getURL() {
        return this.qU;
    }

    public void setRequestMethod(String str) {
        this.qW = str;
        URI uri = null;
        try {
            uri = new URI(this.qU);
        } catch (URISyntaxException e) {
            try {
                uri = new URI(this.qU.substring(0, e.getIndex()) + URLEncoder.encode(this.qU.substring(e.getIndex(), this.qU.length()), "ISO-8859-1"));
            } catch (URISyntaxException e2) {
            }
        }
        if (str.equalsIgnoreCase("get")) {
            this.qX = new HttpGet(uri);
        } else if (str.equalsIgnoreCase("post")) {
            this.qX = new HttpPost(uri);
        }
    }

    public void setRequestProperty(String str, String str2) {
        if (this.qX != null) {
            this.qX.setHeader(str, str2);
            return;
        }
        if (this.qV == null) {
            this.qV = new Hashtable();
        }
        this.qV.put(str, str2);
    }
}
