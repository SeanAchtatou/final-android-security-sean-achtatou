package b.a.a.c;

import b.a.a.c.b.d;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class g {
    private static String Ni = null;
    public static final int bQQ = 1;
    public static final int bQR = 2;
    public static final int bQS = 3;
    private static String bQT;

    static {
        Ni = System.getProperty("microedition.platform");
        if (Ni == null) {
            Ni = "j2me";
        }
        bQT = System.getProperty("j2me_adapter.javax.microedition.io.Connector.protocolpath");
        if (bQT == null) {
            bQT = "com.sun.cldc.io";
        }
    }

    private g() {
    }

    private static a c(String str, int i, boolean z) {
        int indexOf = str.indexOf(58);
        if (indexOf < 1) {
            throw new IllegalArgumentException("no ':' in URL");
        }
        char[] charArray = str.substring(0, indexOf).toCharArray();
        for (int i2 = 0; i2 < charArray.length; i2++) {
            char c = charArray[i2];
            if (('A' > c || c > 'Z') && (('a' > c || c > 'z') && (i2 <= 0 || !(('0' <= c && c <= '9') || c == '+' || c == '-' || c == '.')))) {
                throw new IllegalArgumentException("Invalid protocol name");
            }
        }
        return d.c(str, i, z);
    }

    public static a e(String str, int i, boolean z) {
        try {
            return c(str, i, z);
        } catch (ClassNotFoundException e) {
            throw new f("The requested protocol does not exist " + str);
        }
    }

    public static InputStream fA(String str) {
        return fy(str);
    }

    public static OutputStream fB(String str) {
        return fz(str);
    }

    public static a fx(String str) {
        return z(str, 3);
    }

    public static DataInputStream fy(String str) {
        try {
            c cVar = (c) z(str, 1);
            try {
                return cVar.ey();
            } finally {
                cVar.close();
            }
        } catch (ClassCastException e) {
            throw new IOException();
        }
    }

    public static DataOutputStream fz(String str) {
        try {
            b bVar = (b) z(str, 2);
            try {
                return bVar.cx();
            } finally {
                bVar.close();
            }
        } catch (ClassCastException e) {
            throw new IOException();
        }
    }

    public static a z(String str, int i) {
        return e(str, i, false);
    }
}
