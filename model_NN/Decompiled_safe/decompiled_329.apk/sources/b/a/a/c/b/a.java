package b.a.a.c.b;

import java.io.IOException;
import javax.net.ssl.SSLHandshakeException;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.protocol.HttpContext;

final class a implements HttpRequestRetryHandler {
    a() {
    }

    public boolean retryRequest(IOException iOException, int i, HttpContext httpContext) {
        if (i >= 2) {
            return false;
        }
        if (iOException instanceof NoHttpResponseException) {
            return true;
        }
        if (iOException instanceof SSLHandshakeException) {
            return false;
        }
        return !(((HttpRequest) httpContext.getAttribute("http.request")) instanceof HttpEntityEnclosingRequest);
    }
}
