package b.a.a.c;

import b.a.a.a.ac;

public interface e extends ac {
    public static final int HTTP_ACCEPTED = 202;
    public static final int HTTP_BAD_GATEWAY = 502;
    public static final int HTTP_BAD_METHOD = 405;
    public static final int HTTP_BAD_REQUEST = 400;
    public static final int HTTP_CLIENT_TIMEOUT = 408;
    public static final int HTTP_CONFLICT = 409;
    public static final int HTTP_CREATED = 201;
    public static final int HTTP_ENTITY_TOO_LARGE = 413;
    public static final int HTTP_FORBIDDEN = 403;
    public static final int HTTP_GATEWAY_TIMEOUT = 504;
    public static final int HTTP_GONE = 410;
    public static final int HTTP_INTERNAL_ERROR = 500;
    public static final int HTTP_LENGTH_REQUIRED = 411;
    public static final int HTTP_MOVED_PERM = 301;
    public static final int HTTP_MOVED_TEMP = 302;
    public static final int HTTP_MULT_CHOICE = 300;
    public static final int HTTP_NOT_ACCEPTABLE = 406;
    public static final int HTTP_NOT_AUTHORITATIVE = 203;
    public static final int HTTP_NOT_FOUND = 404;
    public static final int HTTP_NOT_IMPLEMENTED = 501;
    public static final int HTTP_NOT_MODIFIED = 304;
    public static final int HTTP_NO_CONTENT = 204;
    public static final int HTTP_OK = 200;
    public static final int HTTP_PARTIAL = 206;
    public static final int HTTP_PAYMENT_REQUIRED = 402;
    public static final int HTTP_PRECON_FAILED = 412;
    public static final int HTTP_PROXY_AUTH = 407;
    public static final int HTTP_REQ_TOO_LONG = 414;
    public static final int HTTP_RESET = 205;
    public static final int HTTP_SEE_OTHER = 303;
    public static final int HTTP_UNAUTHORIZED = 401;
    public static final int HTTP_UNAVAILABLE = 503;
    public static final int HTTP_UNSUPPORTED_TYPE = 415;
    public static final int HTTP_USE_PROXY = 305;
    public static final int HTTP_VERSION = 505;
    public static final String aXL = "HEAD";
    public static final String aXM = "GET";
    public static final String aXN = "POST";
    public static final int aXO = 307;
    public static final int aXP = 416;
    public static final int aXQ = 417;
    public static final short aXR = -1;
    public static final short aXS = -2;
    public static final short aXT = -3;
    public static final short aXU = 0;
    public static final short aXV = -127;

    void eA();

    short ex();

    long getDate();

    long getExpiration();

    String getFile();

    String getHeaderField(int i);

    String getHeaderField(String str);

    long getHeaderFieldDate(String str, long j);

    int getHeaderFieldInt(String str, int i);

    String getHeaderFieldKey(int i);

    String getHost();

    long getLastModified();

    int getPort();

    String getProtocol();

    String getQuery();

    String getRef();

    String getRequestMethod();

    String getRequestProperty(String str);

    int getResponseCode();

    String getResponseMessage();

    String getURL();

    void setRequestMethod(String str);

    void setRequestProperty(String str, String str2);
}
