package b.a.a.c.b;

import b.a.a.a.b.a;
import b.a.a.c.a.c;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

public class f implements c {
    static final int aDE = 1048576000;
    File aDD;

    f(String str) {
        this.aDD = new File(str);
    }

    public void O(String str) {
        if (!this.aDD.renameTo(new File(str))) {
            throw new IOException("renameTo Error！");
        }
        new File(str).setLastModified(System.currentTimeMillis());
    }

    public long aI(boolean z) {
        return 0;
    }

    public void aJ(boolean z) {
        if (!z) {
            this.aDD.setReadOnly();
        }
    }

    public void aK(boolean z) {
    }

    public void aL(boolean z) {
    }

    public boolean canRead() {
        return this.aDD.canRead();
    }

    public boolean canWrite() {
        return this.aDD.canWrite();
    }

    public void cf() {
        if (!this.aDD.delete()) {
            throw new IOException("Delete Error！");
        }
    }

    public void close() {
        this.aDD = null;
    }

    public void create() {
        if (!this.aDD.createNewFile()) {
            throw new IOException("Create File " + this.aDD.getName() + "  faile!");
        }
    }

    public void ct(String str) {
    }

    public OutputStream cw() {
        return new FileOutputStream(this.aDD);
    }

    public DataOutputStream cx() {
        return new DataOutputStream(new FileOutputStream(this.aDD));
    }

    public boolean exists() {
        return this.aDD.exists();
    }

    public DataInputStream ey() {
        return new DataInputStream(new FileInputStream(this.aDD));
    }

    public InputStream ez() {
        return new FileInputStream(this.aDD);
    }

    public Enumeration f(String str, boolean z) {
        return new a(this.aDD.listFiles());
    }

    public long fT() {
        return this.aDD.length();
    }

    public void fU() {
        if (!this.aDD.mkdirs()) {
            throw new IOException("mkdir faile!");
        }
    }

    public long fW() {
        return 1048576000;
    }

    public String getName() {
        return this.aDD.getName();
    }

    public String getPath() {
        return this.aDD.getPath();
    }

    public String getURL() {
        return "file://" + getPath() + d.aAn + getName();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public OutputStream i(long j) {
        return 0 == j ? new FileOutputStream(this.aDD, false) : new FileOutputStream(this.aDD, true);
    }

    public boolean isDirectory() {
        return this.aDD.isDirectory();
    }

    public boolean isHidden() {
        return this.aDD.isHidden();
    }

    public boolean isOpen() {
        return this.aDD.isAbsolute();
    }

    public long lastModified() {
        return this.aDD.lastModified();
    }

    public void truncate(long j) {
    }

    public long ug() {
        return this.aDD.length();
    }

    public long uh() {
        return 0;
    }

    public Enumeration ui() {
        return new a(this.aDD.listFiles());
    }
}
