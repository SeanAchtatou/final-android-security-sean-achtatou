package b.a.a.c.b;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;
import org.apache.http.cookie.CookieIdentityComparator;

class e implements CookieStore {
    private final ArrayList aCd = new ArrayList();
    private final Comparator aCe = new CookieIdentityComparator();

    public synchronized void addCookie(Cookie cookie) {
        if (cookie != null) {
            Iterator it = this.aCd.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (this.aCe.compare(cookie, it.next()) == 0) {
                        it.remove();
                        break;
                    }
                } else {
                    break;
                }
            }
            if (!cookie.isExpired(new Date())) {
                this.aCd.add(cookie);
            }
        }
    }

    public synchronized void addCookies(Cookie[] cookieArr) {
        if (cookieArr != null) {
            for (Cookie addCookie : cookieArr) {
                addCookie(addCookie);
            }
        }
    }

    public synchronized void clear() {
        this.aCd.clear();
    }

    public synchronized boolean clearExpired(Date date) {
        boolean z;
        boolean z2 = false;
        synchronized (this) {
            if (date == null) {
                z = false;
            } else {
                Iterator it = this.aCd.iterator();
                while (it.hasNext()) {
                    if (((Cookie) it.next()).isExpired(date)) {
                        it.remove();
                        z2 = true;
                    }
                }
                z = z2;
            }
        }
        return z;
    }

    public synchronized List getCookies() {
        return Collections.unmodifiableList(new ArrayList(0));
    }

    public String toString() {
        return this.aCd.toString();
    }
}
