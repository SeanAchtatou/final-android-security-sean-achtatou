package b.a.a.c.a;

import b.a.a.c.d;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

public interface c extends d {
    void O(String str);

    long aI(boolean z);

    void aJ(boolean z);

    void aK(boolean z);

    void aL(boolean z);

    boolean canRead();

    boolean canWrite();

    void cf();

    void create();

    void ct(String str);

    OutputStream cw();

    DataOutputStream cx();

    boolean exists();

    DataInputStream ey();

    InputStream ez();

    Enumeration f(String str, boolean z);

    long fT();

    void fU();

    long fW();

    String getName();

    String getPath();

    String getURL();

    OutputStream i(long j);

    boolean isDirectory();

    boolean isHidden();

    boolean isOpen();

    long lastModified();

    void truncate(long j);

    long ug();

    long uh();

    Enumeration ui();
}
