package a.a.a.c.c;

import a.a.a.c.a.ad;
import a.a.a.c.a.am;
import a.a.a.c.a.as;
import a.a.a.c.a.v;
import a.a.a.c.f.h;
import java.util.Date;
import java.util.List;

final class cc extends v {
    cc(am[] amVarArr) {
        super(amVarArr);
        eU(0);
        eU(4);
        eU(5);
        eU(6);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object[] objArr) {
        ao aoVar = (ao) obj;
        objArr[0] = aoVar.tN > 1 ? as.jV(aoVar.tN - 1) : null;
        objArr[1] = aoVar.vc;
        objArr[2] = aoVar.vd;
        objArr[3] = aoVar.aCq;
        objArr[4] = aoVar.aCr;
        objArr[5] = aoVar.aCs;
        objArr[6] = aoVar.aCt;
    }

    /* access modifiers changed from: protected */
    public Object b(ad adVar) {
        Object[] objArr = (Object[]) adVar.auW;
        return new ao(objArr[0] == null ? 1 : as.U(objArr[0]) + 1, (al) objArr[1], (h) objArr[2], (Date) objArr[3], (Date) objArr[4], (List) objArr[5], (br) objArr[6], adVar.getEncoded(), null);
    }
}
