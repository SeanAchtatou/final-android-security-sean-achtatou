package a.a.a.c.h;

import a.a.a.c.j;
import a.a.a.c.j.a.a;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;

public class p {
    public static o cad;
    private int aDH;
    private String bZY;
    private Provider.Service bZZ;
    private String caa;
    public Provider cab;
    public Object cac;

    public p(String str) {
        this.bZY = str;
    }

    public synchronized void a(String str, Provider provider, Object obj) {
        if (str == null) {
            throw new NoSuchAlgorithmException(a.c("security.14B", this.bZY));
        }
        Provider.Service service = provider.getService(this.bZY, str);
        if (service == null) {
            throw new NoSuchAlgorithmException(a.c("security.14A", this.bZY, str));
        }
        this.cac = service.newInstance(obj);
        this.cab = provider;
    }

    public synchronized void e(String str, Object obj) {
        Provider.Service di;
        if (str == null) {
            throw new NoSuchAlgorithmException(a.getString("security.149"));
        }
        h.refresh();
        if (this.bZZ != null && j.aa(str, this.caa) && this.aDH == h.aDH) {
            di = this.bZZ;
        } else if (h.isEmpty()) {
            throw new NoSuchAlgorithmException(a.c("security.14A", this.bZY, str));
        } else {
            di = h.di(new StringBuffer(128).append(this.bZY).append(".").append(j.ej(str)).toString());
            if (di == null) {
                throw new NoSuchAlgorithmException(a.c("security.14A", this.bZY, str));
            }
            this.bZZ = di;
            this.caa = str;
            this.aDH = h.aDH;
        }
        this.cac = di.newInstance(obj);
        this.cab = di.getProvider();
    }
}
