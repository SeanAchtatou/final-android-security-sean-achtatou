package a.a.a.c;

import java.security.Principal;

public final class a implements Principal {
    public static final String Gz = "*";
    private final String GA;
    private final String name;

    public a(String str, String str2) {
        if (str == null || str.length() == 0) {
            throw new IllegalArgumentException(a.a.a.c.j.a.a.getString("security.91"));
        }
        this.GA = str;
        this.name = str2;
    }

    public boolean a(Principal principal) {
        return principal != null && ("*".equals(this.GA) || (this.GA.equals(principal.getClass().getName()) && ("*".equals(this.name) || (this.name != null ? this.name.equals(principal.getName()) : principal.getName() == null))));
    }

    public boolean equals(Object obj) {
        if (obj instanceof a) {
            a aVar = (a) obj;
            return this.GA.equals(aVar.GA) && (this.name != null ? this.name.equals(aVar.name) : aVar.name == null);
        } else if (obj instanceof Principal) {
            return a((Principal) obj);
        } else {
            return false;
        }
    }

    public String getClassName() {
        return this.GA;
    }

    public String getName() {
        return this.name;
    }

    public int hashCode() {
        int i = 0;
        if (this.name != null) {
            i = 0 ^ this.name.hashCode();
        }
        return this.GA != null ? i ^ this.GA.hashCode() : i;
    }

    public String toString() {
        return "Principal " + this.GA + " \"" + this.name + "\"";
    }
}
