package a.a.a.c.a;

import a.a.a.c.j.a.a;
import java.io.InputStream;

public final class ap extends ad {
    private static final byte[] bQO = {1, 3, 7, 15, 31, 63, Byte.MAX_VALUE};

    public ap(InputStream inputStream) {
        super(inputStream);
    }

    public ap(byte[] bArr) {
        super(bArr, 0, bArr.length);
    }

    public ap(byte[] bArr, int i, int i2) {
        super(bArr, i, i2);
    }

    public void a(ah ahVar) {
        if (this.tag == ahVar.btX) {
            throw new ak(a.q("security.10A", this.auX));
        }
        super.a(ahVar);
    }

    public void c(a aVar) {
        super.c(aVar);
    }

    public void c(v vVar) {
        super.c(vVar);
    }

    public final int next() {
        int next = super.next();
        if (this.length != -1) {
            return next;
        }
        throw new ak(a.getString("security.105"));
    }

    public void xj() {
        if (this.tag == 35) {
            throw new ak(a.q("security.106", this.auX));
        }
        super.xj();
        if (this.length > 1 && this.buffer[this.auY] != 0 && (this.buffer[this.pG - 1] & bQO[this.buffer[this.auY] - 1]) != 0) {
            throw new ak(a.q("security.107", this.auY));
        }
    }

    public void xl() {
        super.xl();
        if (this.buffer[this.auY] != 0 && this.buffer[this.auY] != -1) {
            throw new ak(a.q("security.108", this.auY));
        }
    }

    public void xm() {
        if (this.tag == 56) {
            throw new ak(a.q("security.10D", this.auX));
        }
        super.xm();
    }

    public void xn() {
        if (this.tag == 55) {
            throw new ak(a.q("security.10B", this.auX));
        } else if (this.length != 13) {
            throw new ak(a.q("security.10C", this.auX));
        } else {
            super.xn();
        }
    }

    public void xp() {
        if (this.tag == 36) {
            throw new ak(a.q("security.109", this.auX));
        }
        super.xp();
    }
}
