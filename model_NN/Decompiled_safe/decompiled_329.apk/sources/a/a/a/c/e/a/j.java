package a.a.a.c.e.a;

import a.a.a.c.a.ad;
import a.a.a.c.j.a.a;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.CertPath;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactorySpi;
import java.security.cert.X509CRL;
import java.util.Iterator;
import java.util.List;

public class j extends CertificateFactorySpi {
    private static int aGW = 28;
    private static b aGX = new b(aGW);
    private static int aGY = 24;
    private static b aGZ = new b(aGY);
    private static byte[] aHa = "-----BEGIN".getBytes();
    private static byte[] aHb = "-----END".getBytes();
    private static byte[] aHc = null;
    private static byte[] aHd = " CERTIFICATE-----".getBytes();

    private static Certificate T(byte[] bArr) {
        Certificate gVar;
        if (bArr.length < aGW) {
            throw new CertificateException(a.getString("security.152"));
        }
        synchronized (aGX) {
            long x = aGX.x(bArr);
            if (!aGX.a(x) || (gVar = (Certificate) aGX.a(x, bArr)) == null) {
                gVar = new g(bArr);
                aGX.a(x, bArr, gVar);
            }
        }
        return gVar;
    }

    private static CRL U(byte[] bArr) {
        CRL aVar;
        if (bArr.length < aGY) {
            throw new CRLException(a.getString("security.152"));
        }
        synchronized (aGZ) {
            long x = aGZ.x(bArr);
            if (!aGZ.a(x) || (aVar = (X509CRL) aGZ.a(x, bArr)) == null) {
                aVar = new a(bArr);
                aGZ.a(x, bArr, aVar);
            }
        }
        return aVar;
    }

    private static byte[] a(InputStream inputStream, int i) {
        byte[] bArr = new byte[i];
        for (int i2 = 0; i2 < i; i2++) {
            int read = inputStream.read();
            if (read == -1) {
                return null;
            }
            bArr[i2] = (byte) read;
        }
        return bArr;
    }

    private byte[] a(InputStream inputStream, byte[] bArr) {
        int read;
        int read2;
        for (int i = 1; i < aHa.length; i++) {
            if (aHa[i] != inputStream.read()) {
                throw new IOException("Incorrect PEM encoding: '-----BEGIN" + (bArr == null ? "" : new String(bArr)) + "' is expected as opening delimiter boundary.");
            }
        }
        if (bArr == null) {
            do {
                read2 = inputStream.read();
                if (read2 != 10) {
                }
            } while (read2 != -1);
            throw new IOException(a.getString("security.156"));
        }
        for (byte b2 : bArr) {
            if (b2 != inputStream.read()) {
                throw new IOException(a.c("security.15B", new String(bArr)));
            }
        }
        int read3 = inputStream.read();
        if (read3 == 13) {
            read3 = inputStream.read();
        }
        if (read3 != 10) {
            throw new IOException(a.getString("security.15B2"));
        }
        byte[] bArr2 = new byte[1024];
        int i2 = 1024;
        int i3 = 0;
        while (true) {
            int read4 = inputStream.read();
            if (read4 != 45) {
                if (read4 == -1) {
                    throw new IOException(a.getString("security.157"));
                }
                int i4 = i3 + 1;
                bArr2[i3] = (byte) read4;
                if (i4 == i2) {
                    byte[] bArr3 = new byte[(i2 + 1024)];
                    System.arraycopy(bArr2, 0, bArr3, 0, i2);
                    i2 += 1024;
                    bArr2 = bArr3;
                    i3 = i4;
                } else {
                    i3 = i4;
                }
            } else if (bArr2[i3 - 1] != 10) {
                throw new IOException(a.getString("security.158"));
            } else {
                for (int i5 = 1; i5 < aHb.length; i5++) {
                    if (aHb[i5] != inputStream.read()) {
                        throw new IOException(a.c("security.15B1", bArr == null ? "" : new String(bArr)));
                    }
                }
                if (bArr == null) {
                    do {
                        read = inputStream.read();
                        if (read == -1 || read == 10) {
                            break;
                        }
                    } while (read != 13);
                } else {
                    for (byte b3 : bArr) {
                        if (b3 != inputStream.read()) {
                            throw new IOException(a.c("security.15B1", new String(bArr)));
                        }
                    }
                }
                inputStream.mark(1);
                while (true) {
                    int read5 = inputStream.read();
                    if (read5 == -1 || !(read5 == 10 || read5 == 13)) {
                        inputStream.reset();
                        byte[] a2 = a.a.a.b.a.a.a(bArr2, i3);
                    } else {
                        inputStream.mark(1);
                    }
                }
                inputStream.reset();
                byte[] a22 = a.a.a.b.a.a.a(bArr2, i3);
                if (a22 != null) {
                    return a22;
                }
                throw new IOException(a.getString("security.159"));
            }
        }
    }

    private static Certificate e(InputStream inputStream) {
        synchronized (aGX) {
            inputStream.mark(aGW);
            byte[] a2 = a(inputStream, aGW);
            inputStream.reset();
            if (a2 == null) {
                throw new CertificateException(a.getString("security.152"));
            }
            long x = aGX.x(a2);
            if (aGX.a(x)) {
                byte[] bArr = new byte[ad.Q(a2)];
                if (bArr.length < aGW) {
                    throw new CertificateException(a.getString("security.15B3"));
                }
                inputStream.read(bArr);
                Certificate certificate = (Certificate) aGX.a(x, bArr);
                if (certificate != null) {
                    return certificate;
                }
                g gVar = new g(bArr);
                aGX.a(x, bArr, gVar);
                return gVar;
            }
            inputStream.reset();
            g gVar2 = new g(inputStream);
            aGX.a(x, gVar2.getEncoded(), gVar2);
            return gVar2;
        }
    }

    private static CRL f(InputStream inputStream) {
        synchronized (aGZ) {
            inputStream.mark(aGY);
            byte[] a2 = a(inputStream, aGY);
            inputStream.reset();
            if (a2 == null) {
                throw new CRLException(a.getString("security.152"));
            }
            long x = aGZ.x(a2);
            if (aGZ.a(x)) {
                byte[] bArr = new byte[ad.Q(a2)];
                if (bArr.length < aGY) {
                    throw new CRLException(a.getString("security.15B4"));
                }
                inputStream.read(bArr);
                CRL crl = (CRL) aGZ.a(x, bArr);
                if (crl != null) {
                    return crl;
                }
                a aVar = new a(bArr);
                aGZ.a(x, bArr, aVar);
                return aVar;
            }
            a aVar2 = new a(inputStream);
            aGZ.a(x, aVar2.getEncoded(), aVar2);
            return aVar2;
        }
    }

    public CRL engineGenerateCRL(InputStream inputStream) {
        if (inputStream == null) {
            throw new CRLException(a.getString("security.153"));
        }
        try {
            InputStream kVar = !inputStream.markSupported() ? new k(inputStream) : inputStream;
            kVar.mark(1);
            if (kVar.read() == 45) {
                return U(a(kVar, aHc));
            }
            kVar.reset();
            return f(kVar);
        } catch (IOException e) {
            throw new CRLException(e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0059, code lost:
        if (r0.size() == 0) goto L_0x00a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x006c, code lost:
        if (r0.size() != 0) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0079, code lost:
        throw new java.security.cert.CRLException(a.a.a.c.j.a.a.getString("security.15F"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        r1.reset();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00a4, code lost:
        if (r4 != -1) goto L_0x00b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00b1, code lost:
        throw new java.security.cert.CRLException(a.a.a.c.j.a.a.getString("security.155"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00b3, code lost:
        if (r2 != 6) goto L_0x00f8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00b5, code lost:
        if (r3 == null) goto L_0x00d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00b7, code lost:
        r9 = a.a.a.c.d.i.en.ax(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00be, code lost:
        r1 = ((a.a.a.c.d.i) r9).Df();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00c4, code lost:
        if (r1 != null) goto L_0x00da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00d1, code lost:
        throw new java.security.cert.CRLException(a.a.a.c.j.a.a.getString("security.154"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00d2, code lost:
        r9 = a.a.a.c.d.i.en.h(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00da, code lost:
        r1 = r1.AG();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00de, code lost:
        if (r1 == null) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00e0, code lost:
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00e5, code lost:
        if (r2 >= r1.size()) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00e7, code lost:
        r0.add(new a.a.a.c.e.a.a((a.a.a.c.c.o) r1.get(r2)));
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0103, code lost:
        throw new java.security.cert.CRLException(a.a.a.c.j.a.a.getString("security.15F"));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Collection engineGenerateCRLs(java.io.InputStream r10) {
        /*
            r9 = this;
            r7 = 0
            r5 = 48
            r6 = -1
            java.lang.String r0 = "security.15F"
            if (r10 != 0) goto L_0x0014
            java.security.cert.CRLException r0 = new java.security.cert.CRLException
            java.lang.String r1 = "security.153"
            java.lang.String r1 = a.a.a.c.j.a.a.getString(r1)
            r0.<init>(r1)
            throw r0
        L_0x0014:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            boolean r1 = r10.markSupported()     // Catch:{ IOException -> 0x007a }
            if (r1 != 0) goto L_0x0104
            a.a.a.c.e.a.k r1 = new a.a.a.c.e.a.k     // Catch:{ IOException -> 0x007a }
            r1.<init>(r10)     // Catch:{ IOException -> 0x007a }
        L_0x0024:
            r2 = 1
            r1.mark(r2)     // Catch:{ IOException -> 0x007a }
            r2 = r6
            r3 = r7
        L_0x002a:
            int r4 = r1.read()     // Catch:{ IOException -> 0x007a }
            if (r4 == r6) goto L_0x0055
            r2 = 45
            if (r4 != r2) goto L_0x005c
            byte[] r2 = a.a.a.c.e.a.j.aHc     // Catch:{ IOException -> 0x007a }
            byte[] r2 = r9.a(r1, r2)     // Catch:{ IOException -> 0x007a }
        L_0x003a:
            if (r2 != 0) goto L_0x0085
            a.a.a.c.a.ad r3 = new a.a.a.c.a.ad     // Catch:{ IOException -> 0x007a }
            r3.<init>(r1)     // Catch:{ IOException -> 0x007a }
        L_0x0041:
            int r3 = r3.next()     // Catch:{ IOException -> 0x007a }
            if (r2 != 0) goto L_0x004a
            r1.reset()     // Catch:{ IOException -> 0x007a }
        L_0x004a:
            if (r3 == r5) goto L_0x008b
            int r5 = r0.size()     // Catch:{ IOException -> 0x007a }
            if (r5 != 0) goto L_0x005b
            r8 = r3
            r3 = r2
            r2 = r8
        L_0x0055:
            int r5 = r0.size()     // Catch:{ IOException -> 0x007a }
            if (r5 == 0) goto L_0x00a4
        L_0x005b:
            return r0
        L_0x005c:
            if (r4 != r5) goto L_0x0068
            r1.reset()     // Catch:{ IOException -> 0x007a }
            int r2 = a.a.a.c.e.a.j.aGY     // Catch:{ IOException -> 0x007a }
            r1.mark(r2)     // Catch:{ IOException -> 0x007a }
            r2 = r7
            goto L_0x003a
        L_0x0068:
            int r2 = r0.size()     // Catch:{ IOException -> 0x007a }
            if (r2 != 0) goto L_0x0081
            java.security.cert.CRLException r0 = new java.security.cert.CRLException     // Catch:{ IOException -> 0x007a }
            java.lang.String r1 = "security.15F"
            java.lang.String r1 = a.a.a.c.j.a.a.getString(r1)     // Catch:{ IOException -> 0x007a }
            r0.<init>(r1)     // Catch:{ IOException -> 0x007a }
            throw r0     // Catch:{ IOException -> 0x007a }
        L_0x007a:
            r0 = move-exception
            java.security.cert.CRLException r1 = new java.security.cert.CRLException
            r1.<init>(r0)
            throw r1
        L_0x0081:
            r1.reset()     // Catch:{ IOException -> 0x007a }
            goto L_0x005b
        L_0x0085:
            a.a.a.c.a.ad r3 = new a.a.a.c.a.ad     // Catch:{ IOException -> 0x007a }
            r3.<init>(r2)     // Catch:{ IOException -> 0x007a }
            goto L_0x0041
        L_0x008b:
            if (r2 != 0) goto L_0x009c
            java.security.cert.CRL r4 = f(r1)     // Catch:{ IOException -> 0x007a }
            r0.add(r4)     // Catch:{ IOException -> 0x007a }
        L_0x0094:
            r4 = 1
            r1.mark(r4)     // Catch:{ IOException -> 0x007a }
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x002a
        L_0x009c:
            java.security.cert.CRL r4 = U(r2)     // Catch:{ IOException -> 0x007a }
            r0.add(r4)     // Catch:{ IOException -> 0x007a }
            goto L_0x0094
        L_0x00a4:
            if (r4 != r6) goto L_0x00b2
            java.security.cert.CRLException r0 = new java.security.cert.CRLException     // Catch:{ IOException -> 0x007a }
            java.lang.String r1 = "security.155"
            java.lang.String r1 = a.a.a.c.j.a.a.getString(r1)     // Catch:{ IOException -> 0x007a }
            r0.<init>(r1)     // Catch:{ IOException -> 0x007a }
            throw r0     // Catch:{ IOException -> 0x007a }
        L_0x00b2:
            r4 = 6
            if (r2 != r4) goto L_0x00f8
            if (r3 == 0) goto L_0x00d2
            a.a.a.c.a.v r1 = a.a.a.c.d.i.en     // Catch:{ IOException -> 0x007a }
            java.lang.Object r1 = r1.ax(r3)     // Catch:{ IOException -> 0x007a }
            r9 = r1
        L_0x00be:
            a.a.a.c.d.i r9 = (a.a.a.c.d.i) r9     // Catch:{ IOException -> 0x007a }
            a.a.a.c.d.h r1 = r9.Df()     // Catch:{ IOException -> 0x007a }
            if (r1 != 0) goto L_0x00da
            java.security.cert.CRLException r0 = new java.security.cert.CRLException     // Catch:{ IOException -> 0x007a }
            java.lang.String r1 = "security.154"
            java.lang.String r1 = a.a.a.c.j.a.a.getString(r1)     // Catch:{ IOException -> 0x007a }
            r0.<init>(r1)     // Catch:{ IOException -> 0x007a }
            throw r0     // Catch:{ IOException -> 0x007a }
        L_0x00d2:
            a.a.a.c.a.v r2 = a.a.a.c.d.i.en     // Catch:{ IOException -> 0x007a }
            java.lang.Object r1 = r2.h(r1)     // Catch:{ IOException -> 0x007a }
            r9 = r1
            goto L_0x00be
        L_0x00da:
            java.util.List r1 = r1.AG()     // Catch:{ IOException -> 0x007a }
            if (r1 == 0) goto L_0x005b
            r2 = 0
        L_0x00e1:
            int r3 = r1.size()     // Catch:{ IOException -> 0x007a }
            if (r2 >= r3) goto L_0x005b
            a.a.a.c.e.a.a r3 = new a.a.a.c.e.a.a     // Catch:{ IOException -> 0x007a }
            java.lang.Object r9 = r1.get(r2)     // Catch:{ IOException -> 0x007a }
            a.a.a.c.c.o r9 = (a.a.a.c.c.o) r9     // Catch:{ IOException -> 0x007a }
            r3.<init>(r9)     // Catch:{ IOException -> 0x007a }
            r0.add(r3)     // Catch:{ IOException -> 0x007a }
            int r2 = r2 + 1
            goto L_0x00e1
        L_0x00f8:
            java.security.cert.CRLException r0 = new java.security.cert.CRLException     // Catch:{ IOException -> 0x007a }
            java.lang.String r1 = "security.15F"
            java.lang.String r1 = a.a.a.c.j.a.a.getString(r1)     // Catch:{ IOException -> 0x007a }
            r0.<init>(r1)     // Catch:{ IOException -> 0x007a }
            throw r0     // Catch:{ IOException -> 0x007a }
        L_0x0104:
            r1 = r10
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.c.e.a.j.engineGenerateCRLs(java.io.InputStream):java.util.Collection");
    }

    public CertPath engineGenerateCertPath(InputStream inputStream) {
        if (inputStream != null) {
            return engineGenerateCertPath(inputStream, "PkiPath");
        }
        throw new CertificateException(a.getString("security.153"));
    }

    public CertPath engineGenerateCertPath(InputStream inputStream, String str) {
        if (inputStream == null) {
            throw new CertificateException(a.getString("security.153"));
        }
        InputStream kVar = !inputStream.markSupported() ? new k(inputStream) : inputStream;
        try {
            kVar.mark(1);
            int read = kVar.read();
            if (read == 45) {
                return l.c(a(kVar, aHc), str);
            }
            if (read == 48) {
                kVar.reset();
                return l.c(kVar, str);
            }
            throw new CertificateException(a.getString("security.15F"));
        } catch (IOException e) {
            throw new CertificateException(e);
        }
    }

    public CertPath engineGenerateCertPath(List list) {
        return new l(list);
    }

    public Certificate engineGenerateCertificate(InputStream inputStream) {
        if (inputStream == null) {
            throw new CertificateException(a.getString("security.153"));
        }
        try {
            InputStream kVar = !inputStream.markSupported() ? new k(inputStream) : inputStream;
            kVar.mark(1);
            if (kVar.read() == 45) {
                return T(a(kVar, aHd));
            }
            kVar.reset();
            return e(kVar);
        } catch (IOException e) {
            throw new CertificateException(e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0059, code lost:
        if (r0.size() == 0) goto L_0x00a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x006c, code lost:
        if (r0.size() != 0) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0079, code lost:
        throw new java.security.cert.CertificateException(a.a.a.c.j.a.a.getString("security.15F"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        r1.reset();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00a4, code lost:
        if (r4 != -1) goto L_0x00b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00b1, code lost:
        throw new java.security.cert.CertificateException(a.a.a.c.j.a.a.getString("security.155"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00b3, code lost:
        if (r2 != 6) goto L_0x00f8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00b5, code lost:
        if (r3 == null) goto L_0x00d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00b7, code lost:
        r9 = a.a.a.c.d.i.en.ax(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00be, code lost:
        r1 = ((a.a.a.c.d.i) r9).Df();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00c4, code lost:
        if (r1 != null) goto L_0x00da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00d1, code lost:
        throw new java.security.cert.CertificateException(a.a.a.c.j.a.a.getString("security.154"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00d2, code lost:
        r9 = a.a.a.c.d.i.en.h(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00da, code lost:
        r1 = r1.getCertificates();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00de, code lost:
        if (r1 == null) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00e0, code lost:
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00e5, code lost:
        if (r2 >= r1.size()) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00e7, code lost:
        r0.add(new a.a.a.c.e.a.g((a.a.a.c.c.bp) r1.get(r2)));
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0103, code lost:
        throw new java.security.cert.CertificateException(a.a.a.c.j.a.a.getString("security.15F"));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Collection engineGenerateCertificates(java.io.InputStream r10) {
        /*
            r9 = this;
            r7 = 0
            r5 = 48
            r6 = -1
            java.lang.String r0 = "security.15F"
            if (r10 != 0) goto L_0x0014
            java.security.cert.CertificateException r0 = new java.security.cert.CertificateException
            java.lang.String r1 = "security.153"
            java.lang.String r1 = a.a.a.c.j.a.a.getString(r1)
            r0.<init>(r1)
            throw r0
        L_0x0014:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            boolean r1 = r10.markSupported()     // Catch:{ IOException -> 0x007a }
            if (r1 != 0) goto L_0x0104
            a.a.a.c.e.a.k r1 = new a.a.a.c.e.a.k     // Catch:{ IOException -> 0x007a }
            r1.<init>(r10)     // Catch:{ IOException -> 0x007a }
        L_0x0024:
            r2 = 1
            r1.mark(r2)     // Catch:{ IOException -> 0x007a }
            r2 = r6
            r3 = r7
        L_0x002a:
            int r4 = r1.read()     // Catch:{ IOException -> 0x007a }
            if (r4 == r6) goto L_0x0055
            r2 = 45
            if (r4 != r2) goto L_0x005c
            byte[] r2 = a.a.a.c.e.a.j.aHc     // Catch:{ IOException -> 0x007a }
            byte[] r2 = r9.a(r1, r2)     // Catch:{ IOException -> 0x007a }
        L_0x003a:
            if (r2 != 0) goto L_0x0085
            a.a.a.c.a.ad r3 = new a.a.a.c.a.ad     // Catch:{ IOException -> 0x007a }
            r3.<init>(r1)     // Catch:{ IOException -> 0x007a }
        L_0x0041:
            int r3 = r3.next()     // Catch:{ IOException -> 0x007a }
            if (r2 != 0) goto L_0x004a
            r1.reset()     // Catch:{ IOException -> 0x007a }
        L_0x004a:
            if (r3 == r5) goto L_0x008b
            int r5 = r0.size()     // Catch:{ IOException -> 0x007a }
            if (r5 != 0) goto L_0x005b
            r8 = r3
            r3 = r2
            r2 = r8
        L_0x0055:
            int r5 = r0.size()     // Catch:{ IOException -> 0x007a }
            if (r5 == 0) goto L_0x00a4
        L_0x005b:
            return r0
        L_0x005c:
            if (r4 != r5) goto L_0x0068
            r1.reset()     // Catch:{ IOException -> 0x007a }
            int r2 = a.a.a.c.e.a.j.aGW     // Catch:{ IOException -> 0x007a }
            r1.mark(r2)     // Catch:{ IOException -> 0x007a }
            r2 = r7
            goto L_0x003a
        L_0x0068:
            int r2 = r0.size()     // Catch:{ IOException -> 0x007a }
            if (r2 != 0) goto L_0x0081
            java.security.cert.CertificateException r0 = new java.security.cert.CertificateException     // Catch:{ IOException -> 0x007a }
            java.lang.String r1 = "security.15F"
            java.lang.String r1 = a.a.a.c.j.a.a.getString(r1)     // Catch:{ IOException -> 0x007a }
            r0.<init>(r1)     // Catch:{ IOException -> 0x007a }
            throw r0     // Catch:{ IOException -> 0x007a }
        L_0x007a:
            r0 = move-exception
            java.security.cert.CertificateException r1 = new java.security.cert.CertificateException
            r1.<init>(r0)
            throw r1
        L_0x0081:
            r1.reset()     // Catch:{ IOException -> 0x007a }
            goto L_0x005b
        L_0x0085:
            a.a.a.c.a.ad r3 = new a.a.a.c.a.ad     // Catch:{ IOException -> 0x007a }
            r3.<init>(r2)     // Catch:{ IOException -> 0x007a }
            goto L_0x0041
        L_0x008b:
            if (r2 != 0) goto L_0x009c
            java.security.cert.Certificate r4 = e(r1)     // Catch:{ IOException -> 0x007a }
            r0.add(r4)     // Catch:{ IOException -> 0x007a }
        L_0x0094:
            r4 = 1
            r1.mark(r4)     // Catch:{ IOException -> 0x007a }
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x002a
        L_0x009c:
            java.security.cert.Certificate r4 = T(r2)     // Catch:{ IOException -> 0x007a }
            r0.add(r4)     // Catch:{ IOException -> 0x007a }
            goto L_0x0094
        L_0x00a4:
            if (r4 != r6) goto L_0x00b2
            java.security.cert.CertificateException r0 = new java.security.cert.CertificateException     // Catch:{ IOException -> 0x007a }
            java.lang.String r1 = "security.155"
            java.lang.String r1 = a.a.a.c.j.a.a.getString(r1)     // Catch:{ IOException -> 0x007a }
            r0.<init>(r1)     // Catch:{ IOException -> 0x007a }
            throw r0     // Catch:{ IOException -> 0x007a }
        L_0x00b2:
            r4 = 6
            if (r2 != r4) goto L_0x00f8
            if (r3 == 0) goto L_0x00d2
            a.a.a.c.a.v r1 = a.a.a.c.d.i.en     // Catch:{ IOException -> 0x007a }
            java.lang.Object r1 = r1.ax(r3)     // Catch:{ IOException -> 0x007a }
            r9 = r1
        L_0x00be:
            a.a.a.c.d.i r9 = (a.a.a.c.d.i) r9     // Catch:{ IOException -> 0x007a }
            a.a.a.c.d.h r1 = r9.Df()     // Catch:{ IOException -> 0x007a }
            if (r1 != 0) goto L_0x00da
            java.security.cert.CertificateException r0 = new java.security.cert.CertificateException     // Catch:{ IOException -> 0x007a }
            java.lang.String r1 = "security.154"
            java.lang.String r1 = a.a.a.c.j.a.a.getString(r1)     // Catch:{ IOException -> 0x007a }
            r0.<init>(r1)     // Catch:{ IOException -> 0x007a }
            throw r0     // Catch:{ IOException -> 0x007a }
        L_0x00d2:
            a.a.a.c.a.v r2 = a.a.a.c.d.i.en     // Catch:{ IOException -> 0x007a }
            java.lang.Object r1 = r2.h(r1)     // Catch:{ IOException -> 0x007a }
            r9 = r1
            goto L_0x00be
        L_0x00da:
            java.util.List r1 = r1.getCertificates()     // Catch:{ IOException -> 0x007a }
            if (r1 == 0) goto L_0x005b
            r2 = 0
        L_0x00e1:
            int r3 = r1.size()     // Catch:{ IOException -> 0x007a }
            if (r2 >= r3) goto L_0x005b
            a.a.a.c.e.a.g r3 = new a.a.a.c.e.a.g     // Catch:{ IOException -> 0x007a }
            java.lang.Object r9 = r1.get(r2)     // Catch:{ IOException -> 0x007a }
            a.a.a.c.c.bp r9 = (a.a.a.c.c.bp) r9     // Catch:{ IOException -> 0x007a }
            r3.<init>(r9)     // Catch:{ IOException -> 0x007a }
            r0.add(r3)     // Catch:{ IOException -> 0x007a }
            int r2 = r2 + 1
            goto L_0x00e1
        L_0x00f8:
            java.security.cert.CertificateException r0 = new java.security.cert.CertificateException     // Catch:{ IOException -> 0x007a }
            java.lang.String r1 = "security.15F"
            java.lang.String r1 = a.a.a.c.j.a.a.getString(r1)     // Catch:{ IOException -> 0x007a }
            r0.<init>(r1)     // Catch:{ IOException -> 0x007a }
            throw r0     // Catch:{ IOException -> 0x007a }
        L_0x0104:
            r1 = r10
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.c.e.a.j.engineGenerateCertificates(java.io.InputStream):java.util.Collection");
    }

    public Iterator engineGetCertPathEncodings() {
        return l.bQJ.iterator();
    }
}
