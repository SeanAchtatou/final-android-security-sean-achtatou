package a.a.a.c.d;

import a.a.a.c.a.a;
import a.a.a.c.a.ag;
import a.a.a.c.a.am;
import a.a.a.c.a.as;
import a.a.a.c.a.n;
import a.a.a.c.a.v;
import a.a.a.c.c.al;
import a.a.a.c.f.h;
import java.math.BigInteger;
import java.util.List;
import javax.security.auth.x500.X500Principal;

public class e {
    public static final v agT = new c(new am[]{h.aCz, as.NW()});
    public static final v en = new b(new am[]{as.NW(), agT, al.en, new n(0, d.TO), al.en, ag.Bl(), new n(1, new a(a.a.a.c.f.e.en))});
    /* access modifiers changed from: private */
    public al agO;
    /* access modifiers changed from: private */
    public d agP;
    /* access modifiers changed from: private */
    public al agQ;
    /* access modifiers changed from: private */
    public byte[] agR;
    /* access modifiers changed from: private */
    public List agS;
    /* access modifiers changed from: private */
    public X500Principal dO;
    /* access modifiers changed from: private */
    public int tN;
    /* access modifiers changed from: private */
    public BigInteger vb;

    public e(int i, Object[] objArr, al alVar, d dVar, al alVar2, byte[] bArr, List list) {
        this.tN = i;
        this.dO = ((h) objArr[0]).yG();
        this.vb = BigInteger.valueOf((long) as.U(objArr[1]));
        this.agO = alVar;
        this.agP = dVar;
        this.agQ = alVar2;
        this.agR = bArr;
        this.agS = list;
    }

    public String getDigestAlgorithm() {
        return this.agO.getAlgorithm();
    }

    public X500Principal getIssuer() {
        return this.dO;
    }

    public BigInteger getSerialNumber() {
        return this.vb;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("-- SignerInfo:");
        stringBuffer.append("\n version : ");
        stringBuffer.append(this.tN);
        stringBuffer.append("\nissuerAndSerialNumber:  ");
        stringBuffer.append(this.dO);
        stringBuffer.append("   ");
        stringBuffer.append(this.vb);
        stringBuffer.append("\ndigestAlgorithm:  ");
        stringBuffer.append(this.agO.toString());
        stringBuffer.append("\nauthenticatedAttributes:  ");
        if (this.agP != null) {
            stringBuffer.append(this.agP.toString());
        }
        stringBuffer.append("\ndigestEncryptionAlgorithm: ");
        stringBuffer.append(this.agQ.toString());
        stringBuffer.append("\nunauthenticatedAttributes: ");
        if (this.agS != null) {
            stringBuffer.append(this.agS.toString());
        }
        stringBuffer.append("\n-- SignerInfo End\n");
        return stringBuffer.toString();
    }

    public String vo() {
        return this.agO.getAlgorithm();
    }

    public String vp() {
        return this.agQ.getAlgorithm();
    }

    public List vq() {
        if (this.agP == null) {
            return null;
        }
        return this.agP.kW();
    }

    public byte[] vr() {
        if (this.agP == null) {
            return null;
        }
        return this.agP.getEncoded();
    }

    public byte[] vs() {
        return this.agR;
    }
}
