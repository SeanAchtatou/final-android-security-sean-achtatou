package a.a.a.c.a;

public class ar extends ae {
    private static final ar bTH = new ar();

    public ar() {
        super(10);
    }

    public static ar MY() {
        return bTH;
    }

    public Object a(ad adVar) {
        adVar.xk();
        if (adVar.avc) {
            return null;
        }
        return b(adVar);
    }

    public void a(at atVar) {
        atVar.Om();
    }

    public Object b(ad adVar) {
        byte[] bArr = new byte[adVar.length];
        System.arraycopy(adVar.buffer, adVar.auY, bArr, 0, adVar.length);
        return bArr;
    }

    public void b(at atVar) {
        atVar.length = ((byte[]) atVar.auW).length;
    }
}
