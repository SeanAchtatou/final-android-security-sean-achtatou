package a.a.a.c.c;

import a.a.a.c.a.ad;
import a.a.a.c.a.am;
import a.a.a.c.a.v;
import java.math.BigInteger;

final class bo extends v {
    bo(am[] amVarArr) {
        super(amVarArr);
        eU(0);
        eU(1);
        eU(2);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object[] objArr) {
        r rVar = (r) obj;
        objArr[0] = rVar.GQ;
        objArr[1] = rVar.GR;
        if (rVar.GS != null) {
            objArr[2] = rVar.GS.toByteArray();
        }
    }

    /* access modifiers changed from: protected */
    public Object b(ad adVar) {
        Object[] objArr = (Object[]) adVar.auW;
        byte[] bArr = (byte[]) objArr[2];
        BigInteger bigInteger = null;
        if (bArr != null) {
            bigInteger = new BigInteger(bArr);
        }
        return new r((byte[]) objArr[0], (bl) objArr[1], bigInteger);
    }
}
