package a.a.a.c.c.a;

import a.a.a.c.a.ad;
import a.a.a.c.a.am;
import a.a.a.c.a.as;
import a.a.a.c.a.v;
import a.a.a.c.j.a.a;
import java.math.BigInteger;

final class l extends v {
    l(am[] amVarArr) {
        super(amVarArr);
        eU(0);
        eU(1);
        eU(2);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object[] objArr) {
        int[] iArr = (int[]) obj;
        int i = 0;
        while (i < 3) {
            if (i <= 0 || (iArr[i] >= 0 && iArr[i] <= 999)) {
                objArr[i] = BigInteger.valueOf((long) iArr[i]).toByteArray();
                i++;
            } else {
                throw new RuntimeException(a.q("security.1A3", iArr[i]));
            }
        }
    }

    /* access modifiers changed from: protected */
    public Object b(ad adVar) {
        Object[] objArr = (Object[]) adVar.auW;
        int[] iArr = new int[3];
        for (int i = 0; i < 3; i++) {
            if (objArr[i] != null) {
                iArr[i] = as.U(objArr[i]);
                if (i > 0 && (iArr[i] < 0 || iArr[i] > 999)) {
                    throw new RuntimeException(a.q("security.1A3", iArr[i]));
                }
            }
        }
        return iArr;
    }
}
