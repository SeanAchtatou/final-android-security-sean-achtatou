package a.a.a.c.d;

import a.a.a.c.a.am;
import a.a.a.c.a.c;
import a.a.a.c.a.o;
import a.a.a.c.a.r;
import a.a.a.c.a.v;
import com.uc.c.bx;
import java.util.Arrays;

public class i {
    public static final int[] bkk = {1, 2, 840, 113549, 1, 7, 1};
    public static final int[] bkl = {1, 2, 840, 113549, 1, 7, 2};
    public static final int[] bkm = {1, 2, 840, 113549, 1, 7, 3};
    public static final int[] bkn = {1, 2, 840, 113549, 1, 7, 4};
    public static final int[] bko = {1, 2, 840, 113549, 1, 7, 5};
    public static final int[] bkp = {1, 2, 840, 113549, 1, 7, 6};
    public static final v en = new f(new am[]{c.bb(), new o(0, r.iS())});
    /* access modifiers changed from: private */
    public int[] aG;
    /* access modifiers changed from: private */
    public Object auW;
    private byte[] dV;

    public i(int[] iArr, Object obj) {
        this.aG = iArr;
        this.auW = obj;
    }

    private i(int[] iArr, Object obj, byte[] bArr) {
        this.aG = iArr;
        this.auW = obj;
        this.dV = bArr;
    }

    /* synthetic */ i(int[] iArr, Object obj, byte[] bArr, f fVar) {
        this(iArr, obj, bArr);
    }

    public h Df() {
        if (Arrays.equals(this.aG, bkl)) {
            return (h) this.auW;
        }
        return null;
    }

    public int[] Dg() {
        return this.aG;
    }

    public Object getContent() {
        return this.auW;
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("==== ContentInfo:");
        stringBuffer.append("\n== ContentType (OID): ");
        for (int append : this.aG) {
            stringBuffer.append(append);
            stringBuffer.append(' ');
        }
        stringBuffer.append("\n== Content: ");
        if (this.auW != null) {
            stringBuffer.append(bx.bxN);
            stringBuffer.append(this.auW.toString());
        }
        stringBuffer.append("\n== Content End");
        stringBuffer.append("\n==== ContentInfo End\n");
        return stringBuffer.toString();
    }
}
