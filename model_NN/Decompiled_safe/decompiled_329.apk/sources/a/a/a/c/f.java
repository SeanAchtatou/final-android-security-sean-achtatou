package a.a.a.c;

import a.a.a.c.j.a.a;
import java.security.Identity;
import java.security.IdentityScope;
import java.security.KeyManagementException;
import java.security.PublicKey;
import java.util.Enumeration;
import java.util.Hashtable;

public class f extends IdentityScope {
    private static final long serialVersionUID = -4810285697932522607L;
    private Hashtable aYx = new Hashtable();
    private Hashtable aYy = new Hashtable();

    public f() {
    }

    public f(String str) {
        super(str);
    }

    public f(String str, IdentityScope identityScope) {
        super(str, identityScope);
    }

    public synchronized void addIdentity(Identity identity) {
        if (identity == null) {
            throw new NullPointerException(a.getString("security.92"));
        }
        String name = identity.getName();
        if (this.aYx.containsKey(name)) {
            throw new KeyManagementException(a.c("security.93", name));
        }
        PublicKey publicKey = identity.getPublicKey();
        if (publicKey == null || !this.aYy.containsKey(publicKey)) {
            this.aYx.put(name, identity);
            if (publicKey != null) {
                this.aYy.put(publicKey, identity);
            }
        } else {
            throw new KeyManagementException(a.c("security.94", publicKey));
        }
    }

    public synchronized Identity getIdentity(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        return (Identity) this.aYx.get(str);
    }

    public synchronized Identity getIdentity(PublicKey publicKey) {
        return publicKey == null ? null : (Identity) this.aYy.get(publicKey);
    }

    public Enumeration identities() {
        return this.aYx.elements();
    }

    public synchronized void removeIdentity(Identity identity) {
        boolean z;
        if (identity == null) {
            throw new NullPointerException(a.getString("security.92"));
        }
        String name = identity.getName();
        if (name == null) {
            throw new NullPointerException(a.getString("security.95"));
        }
        boolean containsKey = this.aYx.containsKey(name);
        this.aYx.remove(name);
        PublicKey publicKey = identity.getPublicKey();
        if (publicKey != null) {
            boolean z2 = containsKey || this.aYy.containsKey(publicKey);
            this.aYy.remove(publicKey);
            z = z2;
        } else {
            z = containsKey;
        }
        if (!z) {
            throw new KeyManagementException(a.getString("security.96"));
        }
    }

    public int size() {
        return this.aYx.size();
    }
}
