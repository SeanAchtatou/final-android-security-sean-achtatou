package a.a.a.c.b;

import com.uc.c.ca;

public class c {
    private c() {
    }

    public static String d(byte[] bArr, String str) {
        String[] strArr = {"", "000", "00", "0", ""};
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        int i = 0;
        while (i < bArr.length) {
            if (i % 16 == 0) {
                sb.append(str);
                String hexString = Integer.toHexString(i);
                sb.append(strArr[hexString.length()]);
                sb.append(hexString);
                sb2.delete(0, sb2.length());
            }
            sb.append(' ');
            byte b2 = bArr[i] & 255;
            String hexString2 = Integer.toHexString(b2);
            if (hexString2.length() == 1) {
                sb.append((char) ca.bNQ);
            }
            sb.append(hexString2);
            char c = (char) (b2 & 65535);
            if (Character.isISOControl(c)) {
                c = '.';
            }
            sb2.append(c);
            if ((i + 1) % 8 == 0) {
                sb.append(' ');
            }
            if ((i + 1) % 16 == 0) {
                sb.append(' ');
                sb.append(sb2.toString());
                sb.append(10);
            }
            i++;
        }
        if (i % 16 != 0) {
            int i2 = 16 - (i % 16);
            for (int i3 = 0; i3 < i2; i3++) {
                sb.append("   ");
            }
            if (i2 > 8) {
                sb.append(' ');
            }
            sb.append("  ");
            sb.append(sb2.toString());
            sb.append(10);
        }
        return sb.toString();
    }
}
