package a.a.a.c.i;

import a.a.a.c.a.a;
import a.a.a.c.a.am;
import a.a.a.c.a.as;
import a.a.a.c.a.n;
import a.a.a.c.a.v;
import a.a.a.c.c.bi;
import a.a.a.c.f.e;
import a.a.a.c.f.h;
import java.util.List;

public class c {
    public static final v en = new a(new am[]{as.NW(), h.aCz, bi.en, new n(0, new a(e.en))});
    /* access modifiers changed from: private */
    public List IX;
    private byte[] dV;
    /* access modifiers changed from: private */
    public int tN;
    /* access modifiers changed from: private */
    public h vf;
    /* access modifiers changed from: private */
    public bi vg;

    public c(int i, h hVar, bi biVar, List list) {
        this.tN = i;
        this.vf = hVar;
        this.vg = biVar;
        this.IX = list;
    }

    private c(int i, h hVar, bi biVar, List list, byte[] bArr) {
        this(i, hVar, biVar, list);
        this.dV = bArr;
    }

    /* synthetic */ c(int i, h hVar, bi biVar, List list, byte[] bArr, a aVar) {
        this(i, hVar, biVar, list, bArr);
    }

    public h fP() {
        return this.vf;
    }

    public bi fQ() {
        return this.vg;
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public int getVersion() {
        return this.tN;
    }

    public List kW() {
        return this.IX;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("-- CertificationRequestInfo:");
        stringBuffer.append("\n version: ");
        stringBuffer.append(this.tN);
        stringBuffer.append("\n subject: ");
        stringBuffer.append(this.vf.getName("CANONICAL"));
        stringBuffer.append("\n subjectPublicKeyInfo: ");
        stringBuffer.append("\n\t algorithm: " + this.vg.kV().getAlgorithm());
        stringBuffer.append("\n\t public key: " + this.vg.getPublicKey());
        stringBuffer.append("\n attributes: ");
        if (this.IX != null) {
            stringBuffer.append(this.IX.toString());
        } else {
            stringBuffer.append("none");
        }
        stringBuffer.append("\n-- CertificationRequestInfo End\n");
        return stringBuffer.toString();
    }
}
