package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.aq;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class bj extends ap {
    public static final am lG = new aq(new bz());
    private List bnr;

    public bj(List list) {
        this.bnr = list;
    }

    public bj(byte[] bArr) {
        super(bArr);
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("Extended Key Usage: ");
        if (this.bnr == null) {
            try {
                this.bnr = getExtendedKeyUsage();
            } catch (IOException e) {
                super.b(stringBuffer);
                return;
            }
        }
        stringBuffer.append('[');
        Iterator it = this.bnr.iterator();
        while (it.hasNext()) {
            stringBuffer.append(" \"").append(it.next()).append('\"');
            if (it.hasNext()) {
                stringBuffer.append(',');
            }
        }
        stringBuffer.append(" ]\n");
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = lG.S(this.bnr);
        }
        return this.dV;
    }

    public List getExtendedKeyUsage() {
        if (this.bnr == null) {
            this.bnr = (List) lG.ax(getEncoded());
        }
        return this.bnr;
    }
}
