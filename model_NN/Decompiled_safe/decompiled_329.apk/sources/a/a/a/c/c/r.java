package a.a.a.c.c;

import a.a.a.c.a.ag;
import a.a.a.c.a.am;
import a.a.a.c.a.as;
import a.a.a.c.a.n;
import a.a.a.c.b.c;
import java.math.BigInteger;

public class r extends ap {
    public static final am lG = new bo(new am[]{new n(0, ag.Bl()), new n(1, bl.lG), new n(2, as.NW())});
    /* access modifiers changed from: private */
    public final byte[] GQ;
    /* access modifiers changed from: private */
    public final bl GR;
    /* access modifiers changed from: private */
    public final BigInteger GS;

    public r(byte[] bArr, bl blVar, BigInteger bigInteger) {
        this.GQ = bArr;
        this.GR = blVar;
        this.GS = bigInteger;
    }

    public static r L(byte[] bArr) {
        r rVar = (r) lG.ax(bArr);
        rVar.dV = bArr;
        return rVar;
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("AuthorityKeyIdentifier [\n");
        if (this.GQ != null) {
            stringBuffer.append(str).append("  keyIdentifier:\n");
            stringBuffer.append(c.d(this.GQ, str + "    "));
        }
        if (this.GR != null) {
            stringBuffer.append(str).append("  authorityCertIssuer: [\n");
            this.GR.a(stringBuffer, str + "    ");
            stringBuffer.append(str).append("  ]\n");
        }
        if (this.GS != null) {
            stringBuffer.append(str).append("  authorityCertSerialNumber: ").append(this.GS).append(10);
        }
        stringBuffer.append(str).append("]\n");
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = lG.S(this);
        }
        return this.dV;
    }
}
