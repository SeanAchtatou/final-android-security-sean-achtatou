package a.a.a.c.c;

import a.a.a.c.a.ad;
import a.a.a.c.a.am;
import a.a.a.c.a.v;

final class by extends v {
    by(am[] amVarArr) {
        super(amVarArr);
        eU(0);
        eU(3);
        c(Boolean.FALSE, 1);
        c(Boolean.FALSE, 2);
        c(Boolean.FALSE, 4);
        c(Boolean.FALSE, 5);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object[] objArr) {
        cd cdVar = (cd) obj;
        objArr[0] = cdVar.qi;
        objArr[1] = cdVar.cek ? Boolean.TRUE : null;
        objArr[2] = cdVar.cel ? Boolean.TRUE : null;
        objArr[3] = cdVar.cem;
        objArr[4] = cdVar.cen ? Boolean.TRUE : null;
        objArr[5] = cdVar.ceo ? Boolean.TRUE : null;
    }

    /* access modifiers changed from: protected */
    public Object b(ad adVar) {
        Object[] objArr = (Object[]) adVar.auW;
        cd cdVar = new cd((bf) objArr[0], (ay) objArr[3]);
        cdVar.dV = adVar.getEncoded();
        if (objArr[1] != null) {
            cdVar.cM(((Boolean) objArr[1]).booleanValue());
        }
        if (objArr[2] != null) {
            cdVar.cN(((Boolean) objArr[2]).booleanValue());
        }
        if (objArr[4] != null) {
            cdVar.cO(((Boolean) objArr[4]).booleanValue());
        }
        if (objArr[5] != null) {
            cdVar.cP(((Boolean) objArr[5]).booleanValue());
        }
        return cdVar;
    }
}
