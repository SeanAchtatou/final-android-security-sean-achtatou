package a.a.a.c.f;

import a.a.a.c.a.a;
import a.a.a.c.a.ap;
import a.a.a.c.a.aq;
import a.a.a.c.c.ac;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import javax.security.auth.x500.X500Principal;

public class h {
    public static final a aCy = new a(e.en);
    public static final aq aCz = new i(aCy);
    private String aCu;
    private String aCv;
    private String aCw;
    /* access modifiers changed from: private */
    public List aCx;
    private volatile byte[] dg;

    public h(String str) {
        this.aCx = new ac(str).sw();
    }

    private h(List list) {
        this.aCx = list;
    }

    /* synthetic */ h(List list, i iVar) {
        this(list);
    }

    public h(byte[] bArr) {
        ap apVar = new ap(bArr);
        if (apVar.xs() != bArr.length) {
            throw new IOException(a.a.a.c.j.a.a.getString("security.111"));
        }
        aCz.a(apVar);
        this.aCx = (List) apVar.auW;
    }

    private String dh(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int size = this.aCx.size() - 1; size >= 0; size--) {
            List list = (List) this.aCx.get(size);
            if ("CANONICAL" == str) {
                LinkedList linkedList = new LinkedList(list);
                Collections.sort(linkedList, new k());
                list = linkedList;
            }
            Iterator it = list.iterator();
            while (it.hasNext()) {
                ((e) it.next()).a(str, stringBuffer);
                if (it.hasNext()) {
                    if ("RFC1779" == str) {
                        stringBuffer.append(" + ");
                    } else {
                        stringBuffer.append('+');
                    }
                }
            }
            if (size != 0) {
                stringBuffer.append(',');
                if (str == "RFC1779") {
                    stringBuffer.append(' ');
                }
            }
        }
        String stringBuffer2 = stringBuffer.toString();
        return "CANONICAL".equals(str) ? stringBuffer2.toLowerCase(Locale.US) : stringBuffer2;
    }

    public byte[] getEncoded() {
        if (this.dg == null) {
            this.dg = aCz.S(this);
        }
        return this.dg;
    }

    public String getName(String str) {
        if ("RFC1779".equals(str)) {
            if (this.aCu == null) {
                this.aCu = dh(str);
            }
            return this.aCu;
        } else if ("RFC2253".equals(str)) {
            if (this.aCv == null) {
                this.aCv = dh(str);
            }
            return this.aCv;
        } else if ("CANONICAL".equals(str)) {
            if (this.aCw == null) {
                this.aCw = dh(str);
            }
            return this.aCw;
        } else if ("RFC1779".equalsIgnoreCase(str)) {
            if (this.aCu == null) {
                this.aCu = dh("RFC1779");
            }
            return this.aCu;
        } else if ("RFC2253".equalsIgnoreCase(str)) {
            if (this.aCv == null) {
                this.aCv = dh("RFC2253");
            }
            return this.aCv;
        } else if ("CANONICAL".equalsIgnoreCase(str)) {
            if (this.aCw == null) {
                this.aCw = dh("CANONICAL");
            }
            return this.aCw;
        } else {
            throw new IllegalArgumentException(a.a.a.c.j.a.a.c("security.177", str));
        }
    }

    public X500Principal yG() {
        return new X500Principal(dh("RFC2253"));
    }
}
