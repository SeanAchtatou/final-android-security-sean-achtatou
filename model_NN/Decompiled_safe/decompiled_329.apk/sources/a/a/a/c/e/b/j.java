package a.a.a.c.e.b;

import java.security.PrivilegedAction;

class j implements PrivilegedAction {
    final /* synthetic */ h caK;

    j(h hVar) {
        this.caK = hVar;
    }

    /* renamed from: kU */
    public Void run() {
        this.caK.put("MessageDigest.SHA-1", "org.apache.harmony.security.provider.crypto.SHA1_MessageDigestImpl");
        this.caK.put("MessageDigest.SHA-1 ImplementedIn", "Software");
        this.caK.put("Alg.Alias.MessageDigest.SHA1", "SHA-1");
        this.caK.put("Alg.Alias.MessageDigest.SHA", "SHA-1");
        this.caK.put("Signature.SHA1withDSA", "org.apache.harmony.security.provider.crypto.SHA1withDSA_SignatureImpl");
        this.caK.put("Signature.SHA1withDSA ImplementedIn", "Software");
        this.caK.put("Alg.Alias.Signature.SHAwithDSA", "SHA1withDSA");
        this.caK.put("Alg.Alias.Signature.DSAwithSHA1", "SHA1withDSA");
        this.caK.put("Alg.Alias.Signature.SHA1/DSA", "SHA1withDSA");
        this.caK.put("Alg.Alias.Signature.SHA/DSA", "SHA1withDSA");
        this.caK.put("Alg.Alias.Signature.SHA-1/DSA", "SHA1withDSA");
        this.caK.put("Alg.Alias.Signature.DSA", "SHA1withDSA");
        this.caK.put("Alg.Alias.Signature.DSS", "SHA1withDSA");
        this.caK.put("Alg.Alias.Signature.OID.1.2.840.10040.4.3", "SHA1withDSA");
        this.caK.put("Alg.Alias.Signature.1.2.840.10040.4.3", "SHA1withDSA");
        this.caK.put("Alg.Alias.Signature.1.3.14.3.2.13", "SHA1withDSA");
        this.caK.put("Alg.Alias.Signature.1.3.14.3.2.27", "SHA1withDSA");
        this.caK.put("KeyFactory.DSA", "org.apache.harmony.security.provider.crypto.DSAKeyFactoryImpl");
        this.caK.put("KeyFactory.DSA ImplementedIn", "Software");
        this.caK.put("Alg.Alias.KeyFactory.1.3.14.3.2.12", "DSA");
        this.caK.put("Alg.Alias.KeyFactory.1.2.840.10040.4.1", "DSA");
        return null;
    }
}
