package a.a.a.c.d;

import a.a.a.c.a.a;
import a.a.a.c.a.am;
import a.a.a.c.a.as;
import a.a.a.c.a.n;
import a.a.a.c.a.v;
import a.a.a.c.c.al;
import a.a.a.c.c.bp;
import a.a.a.c.c.o;
import java.util.List;

public class h {
    public static final v en = new g(new am[]{as.NW(), new a(al.en), i.en, new n(0, new a(bp.en)), new n(1, new a(o.en)), new a(e.en)});
    /* access modifiers changed from: private */
    public List aYD;
    /* access modifiers changed from: private */
    public i aYE;
    /* access modifiers changed from: private */
    public List aYF;
    /* access modifiers changed from: private */
    public List aYG;
    /* access modifiers changed from: private */
    public List aYH;
    /* access modifiers changed from: private */
    public int tN;

    public h(int i, List list, i iVar, List list2, List list3, List list4) {
        this.tN = i;
        this.aYD = list;
        this.aYE = iVar;
        this.aYF = list2;
        this.aYG = list3;
        this.aYH = list4;
    }

    public List AG() {
        return this.aYG;
    }

    public List AH() {
        return this.aYH;
    }

    public i AI() {
        return this.aYE;
    }

    public List AJ() {
        return this.aYD;
    }

    public List getCertificates() {
        return this.aYF;
    }

    public int getVersion() {
        return this.tN;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("---- SignedData:");
        stringBuffer.append("\nversion: ");
        stringBuffer.append(this.tN);
        stringBuffer.append("\ndigestAlgorithms: ");
        stringBuffer.append(this.aYD.toString());
        stringBuffer.append("\ncontentInfo: ");
        stringBuffer.append(this.aYE.toString());
        stringBuffer.append("\ncertificates: ");
        if (this.aYF != null) {
            stringBuffer.append(this.aYF.toString());
        }
        stringBuffer.append("\ncrls: ");
        if (this.aYG != null) {
            stringBuffer.append(this.aYG.toString());
        }
        stringBuffer.append("\nsignerInfos:\n");
        stringBuffer.append(this.aYH.toString());
        stringBuffer.append("\n---- SignedData End\n]");
        return stringBuffer.toString();
    }
}
