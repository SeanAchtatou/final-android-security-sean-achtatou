package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.p;
import a.a.a.c.a.v;
import a.a.a.c.b.b;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class bi {
    public static final v en = new aj(new am[]{al.en, p.fB()});
    /* access modifiers changed from: private */
    public int BI;
    private PublicKey YR;
    /* access modifiers changed from: private */
    public al bna;
    /* access modifiers changed from: private */
    public byte[] bnb;
    private byte[] dV;

    public bi(al alVar, byte[] bArr) {
        this(alVar, bArr, 0);
    }

    public bi(al alVar, byte[] bArr, int i) {
        this(alVar, bArr, 0, null);
    }

    private bi(al alVar, byte[] bArr, int i, byte[] bArr2) {
        this.bna = alVar;
        this.bnb = bArr;
        this.BI = i;
        this.dV = bArr2;
    }

    /* synthetic */ bi(al alVar, byte[] bArr, int i, byte[] bArr2, aj ajVar) {
        this(alVar, bArr, i, bArr2);
    }

    public byte[] Ek() {
        return this.bnb;
    }

    public int El() {
        return this.BI;
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public PublicKey getPublicKey() {
        if (this.YR == null) {
            String algorithm = this.bna.getAlgorithm();
            try {
                String am = b.am(algorithm);
                if (am == null) {
                    am = algorithm;
                }
                this.YR = KeyFactory.getInstance(am).generatePublic(new X509EncodedKeySpec(getEncoded()));
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            }
            if (this.YR == null) {
                this.YR = new a(algorithm, getEncoded(), this.bnb);
            }
        }
        return this.YR;
    }

    public al kV() {
        return this.bna;
    }
}
