package a.a.a.c.a;

import a.a.a.c.j.a.a;
import java.io.InputStream;

public abstract class am implements y {
    public final int btX;
    public final int id;

    public am(int i) {
        this(0, i);
    }

    public am(int i, int i2) {
        if (i2 < 0) {
            throw new IllegalArgumentException(a.getString("security.102"));
        } else if (i != 0 && i != 64 && i != 128 && i != 192) {
            throw new IllegalArgumentException(a.getString("security.103"));
        } else if (i2 < 31) {
            this.id = i + i2;
            this.btX = this.id + 32;
        } else {
            throw new IllegalArgumentException(a.getString("security.104"));
        }
    }

    public final byte[] S(Object obj) {
        return new d(this, obj).dg;
    }

    public abstract Object a(ad adVar);

    public abstract void a(at atVar);

    public final Object ax(byte[] bArr) {
        return a(new ap(bArr));
    }

    public final void ay(byte[] bArr) {
        ap apVar = new ap(bArr);
        apVar.xv();
        a(apVar);
    }

    /* access modifiers changed from: protected */
    public Object b(ad adVar) {
        return adVar.auW;
    }

    public abstract void b(at atVar);

    public abstract void c(at atVar);

    public int d(at atVar) {
        int i = 1 + 1;
        if (atVar.length > 127) {
            int i2 = i + 1;
            int i3 = atVar.length >> 8;
            while (i3 > 0) {
                i3 >>= 8;
                i2++;
            }
            i = i2;
        }
        return i + atVar.length;
    }

    public final Object h(InputStream inputStream) {
        return a(new ap(inputStream));
    }

    public final void i(InputStream inputStream) {
        ap apVar = new ap(inputStream);
        apVar.xv();
        a(apVar);
    }

    public final Object o(byte[] bArr, int i, int i2) {
        return a(new ap(bArr, i, i2));
    }

    public String toString() {
        return getClass().getName() + "(tag: 0x" + Integer.toHexString(this.id & 255) + ")";
    }

    public abstract boolean v(int i);
}
