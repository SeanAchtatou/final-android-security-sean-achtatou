package a.a.a.c.c;

import a.a.a.c.b.c;

public class ap {
    protected byte[] dV;

    public ap() {
    }

    public ap(byte[] bArr) {
        this.dV = bArr;
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("Unparseable extension value:\n");
        if (this.dV == null) {
            this.dV = getEncoded();
        }
        if (this.dV == null) {
            stringBuffer.append("NULL\n");
        } else {
            stringBuffer.append(c.d(this.dV, str));
        }
    }

    public void b(StringBuffer stringBuffer) {
        a(stringBuffer, "");
    }

    public byte[] getEncoded() {
        return this.dV;
    }
}
