package a.a.a.c.c;

import a.a.a.c.a.ad;
import a.a.a.c.a.am;
import a.a.a.c.a.v;
import java.math.BigInteger;

final class l extends v {
    l(am[] amVarArr) {
        super(amVarArr);
        eU(0);
        eU(1);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object[] objArr) {
        u uVar = (u) obj;
        objArr[0] = uVar.Iv.toByteArray();
        objArr[1] = uVar.Iw.toByteArray();
    }

    /* access modifiers changed from: protected */
    public Object b(ad adVar) {
        Object[] objArr = (Object[]) adVar.auW;
        return new u(objArr[0] != null ? new BigInteger((byte[]) objArr[0]) : null, objArr[1] != null ? new BigInteger((byte[]) objArr[1]) : null, adVar.getEncoded(), null);
    }
}
