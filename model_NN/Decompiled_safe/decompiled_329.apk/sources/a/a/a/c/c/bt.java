package a.a.a.c.c;

import a.a.a.c.a.ad;
import a.a.a.c.a.am;
import a.a.a.c.a.v;

final class bt extends v {
    bt(am[] amVarArr) {
        super(amVarArr);
        c(Boolean.FALSE, 1);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object[] objArr) {
        af afVar = (af) obj;
        objArr[0] = afVar.agz;
        objArr[1] = afVar.agB ? Boolean.TRUE : Boolean.FALSE;
        objArr[2] = afVar.agC;
    }

    /* access modifiers changed from: protected */
    public Object b(ad adVar) {
        Object[] objArr = (Object[]) adVar.auW;
        int[] iArr = (int[]) objArr[0];
        byte[] bArr = (byte[]) ((Object[]) objArr[2])[0];
        return new af((int[]) objArr[0], ((Boolean) objArr[1]).booleanValue(), bArr, (byte[]) ((Object[]) objArr[2])[1], adVar.getEncoded(), af.a(iArr, af.agd) ? new bv(bArr) : af.a(iArr, af.agh) ? new ai(bArr) : null, null);
    }
}
