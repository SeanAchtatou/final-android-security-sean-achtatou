package a.a.a.c.h;

public class e extends Exception {
    private static final long serialVersionUID = 2869748055182612000L;

    public e(String str) {
        super(str);
    }

    public e(String str, Throwable th) {
        super(str, th);
    }
}
