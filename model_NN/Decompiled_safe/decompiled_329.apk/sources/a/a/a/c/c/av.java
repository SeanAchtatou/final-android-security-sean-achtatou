package a.a.a.c.c;

import a.a.a.c.a.as;
import java.math.BigInteger;

public class av extends ap {
    private int aGA;

    public av(int i) {
        this.aGA = i;
    }

    public av(byte[] bArr) {
        super(bArr);
        this.aGA = new BigInteger((byte[]) as.NW().ax(bArr)).intValue();
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("Inhibit Any-Policy: ").append(this.aGA).append(10);
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = as.NW().S(as.jV(this.aGA));
        }
        return this.dV;
    }

    public int zU() {
        return this.aGA;
    }
}
