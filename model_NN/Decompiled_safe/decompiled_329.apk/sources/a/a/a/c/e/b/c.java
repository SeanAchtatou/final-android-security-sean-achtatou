package a.a.a.c.e.b;

import a.a.a.c.j.a.a;
import java.security.DigestException;
import java.security.MessageDigestSpi;
import java.util.Arrays;

public class c extends MessageDigestSpi implements b, Cloneable {
    private int[] HX = new int[87];
    private byte[] HY = new byte[1];
    private int HZ;

    public c() {
        engineReset();
    }

    private void s(byte[] bArr, int i) {
        long j = (long) (this.HZ << 3);
        engineUpdate(Byte.MIN_VALUE);
        int i2 = (this.HX[81] + 3) >> 2;
        if (this.HX[81] == 0) {
            i2 = 0;
        } else if (i2 >= 15) {
            if (i2 == 15) {
                this.HX[15] = 0;
            }
            k.k(this.HX);
            i2 = 0;
        }
        Arrays.fill(this.HX, i2, 14, 0);
        this.HX[14] = (int) (j >>> 32);
        this.HX[15] = (int) (j & -1);
        k.k(this.HX);
        int i3 = i;
        for (int i4 = 82; i4 < 87; i4++) {
            int i5 = this.HX[i4];
            bArr[i3] = (byte) (i5 >>> 24);
            bArr[i3 + 1] = (byte) (i5 >>> 16);
            bArr[i3 + 2] = (byte) (i5 >>> 8);
            bArr[i3 + 3] = (byte) i5;
            i3 += 4;
        }
        engineReset();
    }

    public Object clone() {
        c cVar = (c) super.clone();
        cVar.HX = (int[]) this.HX.clone();
        cVar.HY = (byte[]) this.HY.clone();
        return cVar;
    }

    /* access modifiers changed from: protected */
    public int engineDigest(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new IllegalArgumentException(a.getString("security.162"));
        } else if (i > bArr.length || i2 > bArr.length || i2 + i > bArr.length) {
            throw new IllegalArgumentException(a.getString("security.163"));
        } else if (i2 < 20) {
            throw new DigestException(a.getString("security.164"));
        } else if (i < 0) {
            throw new ArrayIndexOutOfBoundsException(a.q("security.165", i));
        } else {
            s(bArr, i);
            return 20;
        }
    }

    /* access modifiers changed from: protected */
    public byte[] engineDigest() {
        byte[] bArr = new byte[20];
        s(bArr, 0);
        return bArr;
    }

    /* access modifiers changed from: protected */
    public int engineGetDigestLength() {
        return 20;
    }

    /* access modifiers changed from: protected */
    public void engineReset() {
        this.HZ = 0;
        this.HX[81] = 0;
        this.HX[82] = 1732584193;
        this.HX[83] = -271733879;
        this.HX[84] = -1732584194;
        this.HX[85] = 271733878;
        this.HX[86] = -1009589776;
    }

    /* access modifiers changed from: protected */
    public void engineUpdate(byte b2) {
        this.HY[0] = b2;
        k.a(this.HX, this.HY, 0, 0);
        this.HZ++;
    }

    /* access modifiers changed from: protected */
    public void engineUpdate(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new IllegalArgumentException(a.getString("security.166"));
        } else if (i2 > 0) {
            if (i < 0) {
                throw new ArrayIndexOutOfBoundsException(a.q("security.165", i));
            } else if (i > bArr.length || i2 > bArr.length || i2 + i > bArr.length) {
                throw new IllegalArgumentException(a.getString("security.167"));
            } else {
                k.a(this.HX, bArr, i, (i + i2) - 1);
                this.HZ += i2;
            }
        }
    }
}
