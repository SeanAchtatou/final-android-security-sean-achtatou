package a.a.a.c.h;

import a.a.a.c.j;
import java.security.AccessController;
import java.security.Provider;
import java.security.Security;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class h {
    private static final Map aDF = new HashMap(512);
    private static boolean aDG;
    static int aDH = 1;
    private static final List aDI = new ArrayList(20);
    private static final Map aDJ = new HashMap(20);

    static {
        AccessController.doPrivileged(new n());
    }

    public static void b(Provider provider) {
        StringBuffer stringBuffer = new StringBuffer(128);
        for (Provider.Service next : provider.getServices()) {
            String type = next.getType();
            stringBuffer.delete(0, stringBuffer.length());
            String stringBuffer2 = stringBuffer.append(type).append(".").append(j.ej(next.getAlgorithm())).toString();
            if (!aDF.containsKey(stringBuffer2)) {
                aDF.put(stringBuffer2, next);
            }
            Iterator a2 = p.cad.a(next);
            while (a2.hasNext()) {
                stringBuffer.delete(0, stringBuffer.length());
                String stringBuffer3 = stringBuffer.append(type).append(".").append(j.ej((String) a2.next())).toString();
                if (!aDF.containsKey(stringBuffer3)) {
                    aDF.put(stringBuffer3, next);
                }
            }
        }
    }

    public static Provider.Service di(String str) {
        return (Provider.Service) aDF.get(str);
    }

    public static void fj(int i) {
        aDJ.remove(((Provider) aDI.remove(i - 1)).getName());
        yR();
    }

    public static Provider getProvider(String str) {
        if (str == null) {
            return null;
        }
        return (Provider) aDJ.get(str);
    }

    public static Provider[] getProviders() {
        return (Provider[]) aDI.toArray(new Provider[aDI.size()]);
    }

    public static int insertProviderAt(Provider provider, int i) {
        int size = aDI.size();
        int i2 = (i < 1 || i > size) ? size + 1 : i;
        aDI.add(i2 - 1, provider);
        aDJ.put(provider.getName(), provider);
        yR();
        return i2;
    }

    public static boolean isEmpty() {
        return aDF.isEmpty();
    }

    public static void refresh() {
        if (aDG) {
            aDH++;
            yP();
        }
    }

    /* access modifiers changed from: private */
    public static void yN() {
        int i = 1;
        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
        while (true) {
            int i2 = i + 1;
            String property = Security.getProperty("security.provider." + i);
            if (property != null) {
                try {
                    Provider provider = (Provider) Class.forName(property.trim(), true, systemClassLoader).newInstance();
                    aDI.add(provider);
                    aDJ.put(provider.getName(), provider);
                    b(provider);
                    i = i2;
                } catch (ClassNotFoundException e) {
                    i = i2;
                } catch (IllegalAccessException e2) {
                    i = i2;
                } catch (InstantiationException e3) {
                    i = i2;
                }
            } else {
                p.cad.Od();
                return;
            }
        }
    }

    public static List yO() {
        return new ArrayList(aDI);
    }

    public static void yP() {
        aDF.clear();
        for (Provider b2 : aDI) {
            b(b2);
        }
        aDG = false;
    }

    public static void yQ() {
        refresh();
        for (String str : aDF.keySet()) {
        }
    }

    public static void yR() {
        aDG = true;
    }
}
