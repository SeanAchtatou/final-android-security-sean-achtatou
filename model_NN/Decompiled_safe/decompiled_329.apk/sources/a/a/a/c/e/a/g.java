package a.a.a.c.e.a;

import a.a.a.c.b.b;
import a.a.a.c.c.af;
import a.a.a.c.c.bp;
import a.a.a.c.c.br;
import a.a.a.c.c.m;
import a.a.a.c.j.a.a;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.Principal;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.security.auth.x500.X500Principal;

public class g extends X509Certificate {
    private static final long serialVersionUID = 2972248729446736154L;
    private final bp YL;
    private final m YM;
    private long YN;
    private long YO;
    private X500Principal YP;
    private byte[] YQ;
    private PublicKey YR;
    private final br dN;
    private X500Principal dO;
    private byte[] dR;
    private String dS;
    private String dT;
    private byte[] dU;
    private byte[] dV;
    private boolean dW;
    private BigInteger vb;

    public g(bp bpVar) {
        this.YN = -1;
        this.YL = bpVar;
        this.YM = bpVar.FO();
        this.dN = this.YM.fn();
    }

    public g(InputStream inputStream) {
        this.YN = -1;
        try {
            this.YL = (bp) bp.en.h(inputStream);
            this.YM = this.YL.FO();
            this.dN = this.YM.fn();
        } catch (IOException e) {
            throw new CertificateException(e);
        }
    }

    public g(byte[] bArr) {
        this((bp) bp.en.ax(bArr));
    }

    public void checkValidity() {
        if (this.YN == -1) {
            this.YN = this.YM.fO().getNotBefore().getTime();
            this.YO = this.YM.fO().getNotAfter().getTime();
        }
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis < this.YN) {
            throw new CertificateNotYetValidException();
        } else if (currentTimeMillis > this.YO) {
            throw new CertificateExpiredException();
        }
    }

    public void checkValidity(Date date) {
        if (this.YN == -1) {
            this.YN = this.YM.fO().getNotBefore().getTime();
            this.YO = this.YM.fO().getNotAfter().getTime();
        }
        long time = date.getTime();
        if (time < this.YN) {
            throw new CertificateNotYetValidException();
        } else if (time > this.YO) {
            throw new CertificateExpiredException();
        }
    }

    public int getBasicConstraints() {
        if (this.dN == null) {
            return Integer.MAX_VALUE;
        }
        return this.dN.GD();
    }

    public Set getCriticalExtensionOIDs() {
        if (this.dN == null) {
            return null;
        }
        return this.dN.Gx();
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = this.YL.getEncoded();
        }
        byte[] bArr = new byte[this.dV.length];
        System.arraycopy(this.dV, 0, bArr, 0, this.dV.length);
        return bArr;
    }

    public List getExtendedKeyUsage() {
        if (this.dN == null) {
            return null;
        }
        try {
            return this.dN.GC();
        } catch (IOException e) {
            throw new CertificateParsingException(e);
        }
    }

    public byte[] getExtensionValue(String str) {
        if (this.dN == null) {
            return null;
        }
        af eN = this.dN.eN(str);
        if (eN == null) {
            return null;
        }
        return eN.vd();
    }

    public Collection getIssuerAlternativeNames() {
        if (this.dN == null) {
            return null;
        }
        try {
            return this.dN.GF();
        } catch (IOException e) {
            throw new CertificateParsingException(e);
        }
    }

    public Principal getIssuerDN() {
        if (this.dO == null) {
            this.dO = this.YM.fN().yG();
        }
        return this.dO;
    }

    public boolean[] getIssuerUniqueID() {
        return this.YM.getIssuerUniqueID();
    }

    public X500Principal getIssuerX500Principal() {
        if (this.dO == null) {
            this.dO = this.YM.fN().yG();
        }
        return this.dO;
    }

    public boolean[] getKeyUsage() {
        if (this.dN == null) {
            return null;
        }
        return this.dN.GB();
    }

    public Set getNonCriticalExtensionOIDs() {
        if (this.dN == null) {
            return null;
        }
        return this.dN.Gy();
    }

    public Date getNotAfter() {
        if (this.YN == -1) {
            this.YN = this.YM.fO().getNotBefore().getTime();
            this.YO = this.YM.fO().getNotAfter().getTime();
        }
        return new Date(this.YO);
    }

    public Date getNotBefore() {
        if (this.YN == -1) {
            this.YN = this.YM.fO().getNotBefore().getTime();
            this.YO = this.YM.fO().getNotAfter().getTime();
        }
        return new Date(this.YN);
    }

    public PublicKey getPublicKey() {
        if (this.YR == null) {
            this.YR = this.YM.fQ().getPublicKey();
        }
        return this.YR;
    }

    public BigInteger getSerialNumber() {
        if (this.vb == null) {
            this.vb = this.YM.getSerialNumber();
        }
        return this.vb;
    }

    public String getSigAlgName() {
        if (this.dS == null) {
            this.dS = this.YM.fM().getAlgorithm();
            this.dT = b.am(this.dS);
            if (this.dT == null) {
                this.dT = this.dS;
            }
        }
        return this.dT;
    }

    public String getSigAlgOID() {
        if (this.dS == null) {
            this.dS = this.YM.fM().getAlgorithm();
            this.dT = b.am(this.dS);
            if (this.dT == null) {
                this.dT = this.dS;
            }
        }
        return this.dS;
    }

    public byte[] getSigAlgParams() {
        if (this.dW) {
            return null;
        }
        if (this.dU == null) {
            this.dU = this.YM.fM().xz();
            if (this.dU == null) {
                this.dW = true;
                return null;
            }
        }
        return this.dU;
    }

    public byte[] getSignature() {
        if (this.dR == null) {
            this.dR = this.YL.iQ();
        }
        byte[] bArr = new byte[this.dR.length];
        System.arraycopy(this.dR, 0, bArr, 0, this.dR.length);
        return bArr;
    }

    public Collection getSubjectAlternativeNames() {
        if (this.dN == null) {
            return null;
        }
        try {
            return this.dN.GE();
        } catch (IOException e) {
            throw new CertificateParsingException(e);
        }
    }

    public Principal getSubjectDN() {
        if (this.YP == null) {
            this.YP = this.YM.fP().yG();
        }
        return this.YP;
    }

    public boolean[] getSubjectUniqueID() {
        return this.YM.getSubjectUniqueID();
    }

    public X500Principal getSubjectX500Principal() {
        if (this.YP == null) {
            this.YP = this.YM.fP().yG();
        }
        return this.YP;
    }

    public byte[] getTBSCertificate() {
        if (this.YQ == null) {
            this.YQ = this.YM.getEncoded();
        }
        byte[] bArr = new byte[this.YQ.length];
        System.arraycopy(this.YQ, 0, bArr, 0, this.YQ.length);
        return bArr;
    }

    public int getVersion() {
        return this.YM.getVersion() + 1;
    }

    public boolean hasUnsupportedCriticalExtension() {
        if (this.dN == null) {
            return false;
        }
        return this.dN.Gz();
    }

    public String toString() {
        return this.YL.toString();
    }

    public void verify(PublicKey publicKey) {
        Signature instance = Signature.getInstance(getSigAlgName());
        instance.initVerify(publicKey);
        if (this.YQ == null) {
            this.YQ = this.YM.getEncoded();
        }
        instance.update(this.YQ, 0, this.YQ.length);
        if (!instance.verify(this.YL.iQ())) {
            throw new SignatureException(a.getString("security.15C"));
        }
    }

    public void verify(PublicKey publicKey, String str) {
        Signature instance = Signature.getInstance(getSigAlgName(), str);
        instance.initVerify(publicKey);
        if (this.YQ == null) {
            this.YQ = this.YM.getEncoded();
        }
        instance.update(this.YQ, 0, this.YQ.length);
        if (!instance.verify(this.YL.iQ())) {
            throw new SignatureException(a.getString("security.15C"));
        }
    }
}
