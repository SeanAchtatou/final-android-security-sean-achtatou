package a.a.a.c.a;

import java.util.GregorianCalendar;
import java.util.TimeZone;

public abstract class an extends ah {
    public an(int i) {
        super(i);
    }

    public Object b(ad adVar) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
        gregorianCalendar.set(1, adVar.ava[0]);
        gregorianCalendar.set(2, adVar.ava[1] - 1);
        gregorianCalendar.set(5, adVar.ava[2]);
        gregorianCalendar.set(11, adVar.ava[3]);
        gregorianCalendar.set(12, adVar.ava[4]);
        gregorianCalendar.set(13, adVar.ava[5]);
        gregorianCalendar.set(14, adVar.ava[6]);
        return gregorianCalendar.getTime();
    }
}
