package a.a.a.c.e.b;

import a.a.a.c.a.as;
import a.a.a.c.b.b;
import a.a.a.c.c.al;
import a.a.a.c.g;
import a.a.a.c.j.a.a;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.math.BigInteger;
import java.security.interfaces.DSAParams;
import java.security.interfaces.DSAPrivateKey;
import java.security.spec.DSAParameterSpec;
import java.security.spec.DSAPrivateKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

public class d extends g implements DSAPrivateKey {
    private static final long serialVersionUID = -4716227614104950081L;
    private BigInteger RX;
    private BigInteger RY;
    private BigInteger RZ;
    private BigInteger Sa;
    private transient DSAParams Sb;

    public d(DSAPrivateKeySpec dSAPrivateKeySpec) {
        super("DSA");
        this.RY = dSAPrivateKeySpec.getG();
        this.RZ = dSAPrivateKeySpec.getP();
        this.Sa = dSAPrivateKeySpec.getQ();
        al alVar = new al(b.al("DSA"), new i(this.RZ.toByteArray(), this.Sa.toByteArray(), this.RY.toByteArray()).getEncoded());
        this.RX = dSAPrivateKeySpec.getX();
        R(new a.a.a.c.g.b(0, alVar, as.NW().S(this.RX.toByteArray()), null).getEncoded());
        this.Sb = new DSAParameterSpec(this.RZ, this.Sa, this.RY);
    }

    public d(PKCS8EncodedKeySpec pKCS8EncodedKeySpec) {
        super("DSA");
        byte[] encoded = pKCS8EncodedKeySpec.getEncoded();
        try {
            a.a.a.c.g.b bVar = (a.a.a.c.g.b) a.a.a.c.g.b.en.ax(encoded);
            try {
                this.RX = new BigInteger((byte[]) as.NW().ax(bVar.kX()));
                al kV = bVar.kV();
                try {
                    i iVar = (i) i.en.ax(kV.xz());
                    this.RZ = new BigInteger(iVar.bQU);
                    this.Sa = new BigInteger(iVar.bQV);
                    this.RY = new BigInteger(iVar.bQW);
                    this.Sb = new DSAParameterSpec(this.RZ, this.Sa, this.RY);
                    R(encoded);
                    String algorithm = kV.getAlgorithm();
                    String am = b.am(algorithm);
                    dk(am != null ? am : algorithm);
                } catch (IOException e) {
                    throw new InvalidKeySpecException(a.c("security.19B", e));
                }
            } catch (IOException e2) {
                throw new InvalidKeySpecException(a.c("security.19B", e2));
            }
        } catch (IOException e3) {
            throw new InvalidKeySpecException(a.c("security.19A", e3));
        }
    }

    private void a(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        this.Sb = new DSAParameterSpec(this.RZ, this.Sa, this.RY);
    }

    public DSAParams getParams() {
        return this.Sb;
    }

    public BigInteger getX() {
        return this.RX;
    }
}
