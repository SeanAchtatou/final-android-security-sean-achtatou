package a.a.a.c.a;

import a.a.a.c.j.a.a;

public class n extends am {
    private static final int rn = 0;
    private static final int ro = 1;
    private static final int rp = 2;
    private final am rq;
    private final int rr;

    public n(int i, int i2, am amVar) {
        super(i, i2);
        if ((amVar instanceof e) || (amVar instanceof r)) {
            throw new IllegalArgumentException(a.getString("security.9F"));
        }
        this.rq = amVar;
        if (!amVar.v(amVar.id)) {
            this.rr = 1;
        } else if (amVar.v(amVar.btX)) {
            this.rr = 2;
        } else {
            this.rr = 0;
        }
    }

    public n(int i, am amVar) {
        this(128, i, amVar);
    }

    public Object a(ad adVar) {
        if (!v(adVar.tag)) {
            throw new ak(a.a("security.100", new Object[]{Integer.valueOf(adVar.auX), Integer.toHexString(this.id), Integer.toHexString(adVar.tag)}));
        }
        if (this.id == adVar.tag) {
            adVar.tag = this.rq.id;
        } else {
            adVar.tag = this.rq.btX;
        }
        adVar.auW = this.rq.a(adVar);
        if (adVar.avc) {
            return null;
        }
        return b(adVar);
    }

    public void a(at atVar) {
        this.rq.a(atVar);
    }

    public void b(at atVar) {
        this.rq.b(atVar);
    }

    public void c(at atVar) {
        if (this.rr == 1) {
            atVar.kc(this.btX);
        } else {
            atVar.kc(this.id);
        }
        a(atVar);
    }

    public final boolean v(int i) {
        switch (this.rr) {
            case 0:
                return this.id == i;
            case 1:
                return this.btX == i;
            default:
                return this.id == i || this.btX == i;
        }
    }
}
