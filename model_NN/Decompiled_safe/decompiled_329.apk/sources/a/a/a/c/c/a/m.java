package a.a.a.c.c.a;

import a.a.a.c.a.ad;
import a.a.a.c.a.aj;
import a.a.a.c.a.am;
import a.a.a.c.a.as;
import a.a.a.c.a.v;
import a.a.a.c.c.be;
import a.a.a.c.c.br;
import java.math.BigInteger;
import java.util.Date;

final class m extends v {
    m(am[] amVarArr) {
        super(amVarArr);
        eU(5);
        c(Boolean.FALSE, 6);
        eU(7);
        eU(8);
        eU(9);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object[] objArr) {
        h hVar = (h) obj;
        objArr[0] = as.jV(hVar.tN);
        objArr[1] = aj.eD(hVar.Yw);
        objArr[2] = hVar.tO;
        objArr[3] = hVar.vb.toByteArray();
        objArr[4] = hVar.Yx;
        objArr[5] = hVar.Yy;
        objArr[6] = hVar.Yz;
        objArr[7] = hVar.tQ == null ? null : hVar.tQ.toByteArray();
        objArr[8] = hVar.YA;
        objArr[9] = hVar.dN;
    }

    /* access modifiers changed from: protected */
    public Object b(ad adVar) {
        Object[] objArr = (Object[]) adVar.auW;
        return new h(as.U(objArr[0]), aj.toString((int[]) objArr[1]), (j) objArr[2], new BigInteger((byte[]) objArr[3]), (Date) objArr[4], (int[]) objArr[5], (Boolean) objArr[6], objArr[7] == null ? null : new BigInteger((byte[]) objArr[7]), (be) objArr[8], (br) objArr[9]);
    }
}
