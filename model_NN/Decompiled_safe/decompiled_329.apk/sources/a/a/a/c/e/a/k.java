package a.a.a.c.e.a;

import a.a.a.c.j.a.a;
import java.io.IOException;
import java.io.InputStream;

class k extends InputStream {
    private static final int bgJ = 32;
    private final InputStream bgI;
    private final int[] bgK = new int[64];
    private int bgL = 0;
    private int jv = 0;
    private int pos = -1;

    public k(InputStream inputStream) {
        this.bgI = inputStream;
    }

    public int available() {
        return (this.bgL - this.pos) + this.bgI.available();
    }

    public void close() {
        this.bgI.close();
    }

    public void mark(int i) {
        if (this.pos < 0) {
            this.pos = 0;
            this.bgL = 0;
            this.jv = 31;
            return;
        }
        this.jv = ((this.pos + 32) - 1) % 32;
    }

    public boolean markSupported() {
        return true;
    }

    public int read() {
        if (this.pos >= 0) {
            int i = this.pos % 32;
            if (i < this.bgL) {
                this.pos++;
                return this.bgK[i];
            } else if (i != this.jv) {
                this.bgK[i] = this.bgI.read();
                this.bgL = i + 1;
                this.pos++;
                return this.bgK[i];
            } else {
                this.pos = -1;
            }
        }
        return this.bgI.read();
    }

    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) {
        int i3 = 0;
        while (i3 < i2) {
            int read = read();
            if (read != -1) {
                bArr[i + i3] = (byte) read;
                i3++;
            } else if (i3 == 0) {
                return -1;
            } else {
                return i3;
            }
        }
        return i3;
    }

    public void reset() {
        if (this.pos >= 0) {
            this.pos = (this.jv + 1) % 32;
            return;
        }
        throw new IOException(a.getString("security.15A"));
    }

    public long skip(long j) {
        if (this.pos < 0) {
            return this.bgI.skip(j);
        }
        long j2 = 0;
        int available = available();
        long j3 = ((long) available) < j ? (long) available : j;
        while (j2 < j3 && read() != -1) {
            j2++;
        }
        return j2;
    }
}
