package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.as;
import a.a.a.c.a.u;
import java.math.BigInteger;

public class ai extends ap {
    public static final am lG = new t(new am[]{u.kZ(), as.NW()});
    private boolean auA = false;
    private int auB = Integer.MAX_VALUE;

    public ai(boolean z, int i) {
        this.auA = z;
        this.auB = i;
    }

    public ai(byte[] bArr) {
        super(bArr);
        Object[] objArr = (Object[]) lG.ax(bArr);
        this.auA = ((Boolean) objArr[0]).booleanValue();
        if (objArr[1] != null) {
            this.auB = new BigInteger((byte[]) objArr[1]).intValue();
        }
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("BasicConstraints [\n").append(str).append("  CA: ").append(this.auA).append("\n  ").append(str).append("pathLenConstraint: ").append(this.auB).append(10).append(str).append("]\n");
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = lG.S(new Object[]{Boolean.valueOf(this.auA), BigInteger.valueOf((long) this.auB)});
        }
        return this.dV;
    }

    public boolean wY() {
        return this.auA;
    }

    public int wZ() {
        return this.auB;
    }
}
