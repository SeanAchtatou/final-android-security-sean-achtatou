package a.a.a.c.c;

import a.a.a.c.a.aa;
import a.a.a.c.a.am;
import a.a.a.c.a.n;
import a.a.a.c.a.v;
import java.util.Date;

public class x {
    public static final v en = new h(new am[]{new n(0, aa.uB()), new n(1, aa.uB())});
    /* access modifiers changed from: private */
    public final Date Tq;
    /* access modifiers changed from: private */
    public final Date Tr;
    private byte[] dV;

    public x(Date date, Date date2) {
        this(date, date2, null);
    }

    private x(Date date, Date date2, byte[] bArr) {
        this.Tq = date;
        this.Tr = date2;
        this.dV = bArr;
    }

    /* synthetic */ x(Date date, Date date2, byte[] bArr, h hVar) {
        this(date, date2, bArr);
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public Date getNotAfter() {
        return this.Tr;
    }

    public Date getNotBefore() {
        return this.Tq;
    }
}
