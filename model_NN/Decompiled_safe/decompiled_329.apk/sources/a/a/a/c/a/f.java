package a.a.a.c.a;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class f extends an {
    public static final int mF = 11;
    public static final int mG = 13;
    public static final int mH = 15;
    public static final int mI = 17;
    private static final f mJ = new f();
    private static final String mK = "yyMMddHHmmss'Z'";

    public f() {
        super(23);
    }

    public static f db() {
        return mJ;
    }

    public Object a(ad adVar) {
        adVar.xn();
        if (adVar.avc) {
            return null;
        }
        return b(adVar);
    }

    public void a(at atVar) {
        atVar.Ol();
    }

    public void b(at atVar) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(mK);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        atVar.auW = simpleDateFormat.format(atVar.auW).getBytes();
        atVar.length = ((byte[]) atVar.auW).length;
    }
}
