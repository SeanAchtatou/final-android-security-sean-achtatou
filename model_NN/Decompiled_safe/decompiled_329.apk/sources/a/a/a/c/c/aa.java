package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.o;
import a.a.a.c.a.v;
import a.a.a.c.f.f;

public class aa {
    public static final v en = new ak(new am[]{new o(0, f.Yv), new o(1, f.Yv)});
    /* access modifiers changed from: private */
    public String TL;
    /* access modifiers changed from: private */
    public String TM;
    private byte[] dV;

    public aa(String str, String str2) {
        this.TL = str;
        this.TM = str2;
    }

    private aa(String str, String str2, byte[] bArr) {
        this.TL = str;
        this.TM = str2;
        this.dV = bArr;
    }

    /* synthetic */ aa(String str, String str2, byte[] bArr, ak akVar) {
        this(str, str2, bArr);
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public String rF() {
        return this.TL;
    }

    public String rG() {
        return this.TM;
    }
}
