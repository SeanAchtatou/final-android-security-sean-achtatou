package a.a.a.c.a;

import a.a.a.c.j.a.a;

public final class o extends x {
    public final am rq;

    public o(int i, int i2, am amVar) {
        super(i, i2);
        this.rq = amVar;
    }

    public o(int i, am amVar) {
        this(128, i, amVar);
    }

    public Object a(ad adVar) {
        if (this.btX != adVar.tag) {
            throw new ak(a.a("security.13F", new Object[]{Integer.valueOf(adVar.auX), Integer.toHexString(this.btX), Integer.toHexString(adVar.tag)}));
        }
        adVar.next();
        adVar.auW = this.rq.a(adVar);
        if (adVar.avc) {
            return null;
        }
        return b(adVar);
    }

    public void a(at atVar) {
        atVar.a(this);
    }

    public void b(at atVar) {
        atVar.b(this);
    }

    public String toString() {
        return super.toString() + " for type " + this.rq;
    }
}
