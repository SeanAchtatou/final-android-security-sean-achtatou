package a.a.a.c.c;

import a.a.a.c.a.am;
import java.util.Date;

public class v {
    public static final a.a.a.c.a.v en = new ab(new am[]{bq.Yv, bq.Yv});
    /* access modifiers changed from: private */
    public final Date Jn;
    /* access modifiers changed from: private */
    public final Date Jo;
    private byte[] dV;

    public v(Date date, Date date2) {
        this.Jn = date;
        this.Jo = date2;
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public Date getNotAfter() {
        return this.Jo;
    }

    public Date getNotBefore() {
        return this.Jn;
    }
}
