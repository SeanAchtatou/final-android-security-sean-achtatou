package a.a.a.c.a;

public class c extends ae {
    private static final c aZ = new c();
    private static final c ba = new ab();

    public c() {
        super(6);
    }

    public static c bb() {
        return aZ;
    }

    public static c bc() {
        return ba;
    }

    public Object a(ad adVar) {
        adVar.xq();
        if (adVar.avc) {
            return null;
        }
        return b(adVar);
    }

    public void a(at atVar) {
        atVar.Oo();
    }

    public Object b(ad adVar) {
        int i;
        int[] iArr = new int[adVar.avb];
        int i2 = 0;
        int i3 = 1;
        while (i3 < iArr.length) {
            byte b2 = adVar.buffer[adVar.auY + i2];
            int i4 = b2 & Byte.MAX_VALUE;
            while (true) {
                byte b3 = b2;
                i = i2;
                if ((b3 & 128) == 0) {
                    break;
                }
                i2 = i + 1;
                b2 = adVar.buffer[adVar.auY + i2];
                i4 = (i4 << 7) | (b2 & Byte.MAX_VALUE);
            }
            iArr[i3] = i4;
            i3++;
            i2 = i + 1;
        }
        if (iArr[1] > 79) {
            iArr[0] = 2;
            iArr[1] = iArr[1] - 80;
        } else {
            iArr[0] = iArr[1] / 40;
            iArr[1] = iArr[1] % 40;
        }
        return iArr;
    }

    public void b(at atVar) {
        int i;
        int[] iArr = (int[]) atVar.auW;
        int i2 = (iArr[0] * 40) + iArr[1];
        if (i2 == 0) {
            i = 1;
        } else {
            int i3 = 0;
            while (i2 > 0) {
                i3++;
                i2 >>= 7;
            }
            i = i3;
        }
        int i4 = i;
        for (int i5 = 2; i5 < iArr.length; i5++) {
            if (iArr[i5] == 0) {
                i4++;
            } else {
                int i6 = i4;
                for (int i7 = iArr[i5]; i7 > 0; i7 >>= 7) {
                    i6++;
                }
                i4 = i6;
            }
        }
        atVar.length = i4;
    }
}
