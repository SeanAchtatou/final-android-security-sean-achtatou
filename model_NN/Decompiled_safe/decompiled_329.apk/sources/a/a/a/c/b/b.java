package a.a.a.c.b;

import a.a.a.c.a.aj;
import a.a.a.c.j;
import java.security.Provider;
import java.security.Security;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class b {
    private static final String[] BB = {"Cipher", "AlgorithmParameters", "Signature"};
    private static final String[][] BC = {new String[]{"1.2.840.10040.4.1", "DSA"}, new String[]{"1.2.840.10040.4.3", "SHA1withDSA"}, new String[]{"1.2.840.113549.1.1.1", "RSA"}, new String[]{"1.2.840.113549.1.1.2", "MD2withRSA"}, new String[]{"1.2.840.113549.1.1.4", "MD5withRSA"}, new String[]{"1.2.840.113549.1.1.5", "SHA1withRSA"}, new String[]{"1.2.840.113549.1.3.1", "DiffieHellman"}, new String[]{"1.2.840.113549.1.5.3", "pbeWithMD5AndDES-CBC"}, new String[]{"1.2.840.113549.1.12.1.3", "pbeWithSHAAnd3-KeyTripleDES-CBC"}, new String[]{"1.2.840.113549.1.12.1.6", "pbeWithSHAAnd40BitRC2-CBC"}, new String[]{"1.2.840.113549.3.2", "RC2-CBC"}, new String[]{"1.2.840.113549.3.3", "RC2-EBC"}, new String[]{"1.2.840.113549.3.4", "RC4"}, new String[]{"1.2.840.113549.3.5", "RC4WithMAC"}, new String[]{"1.2.840.113549.3.6", "DESx-CBC"}, new String[]{"1.2.840.113549.3.7", "TripleDES-CBC"}, new String[]{"1.2.840.113549.3.8", "rc5CBC"}, new String[]{"1.2.840.113549.3.9", "RC5-CBC"}, new String[]{"1.2.840.113549.3.10", "DESCDMF"}, new String[]{"2.23.42.9.11.4.1", "ECDSA"}};
    private static final Map BD = new HashMap();
    private static final Map BE = new HashMap();
    private static final Map BF = new HashMap();

    static {
        for (String[] strArr : BC) {
            String ej = j.ej(strArr[1]);
            BD.put(ej, strArr[0]);
            BE.put(strArr[0], ej);
            BF.put(ej, strArr[1]);
        }
        for (Provider a2 : Security.getProviders()) {
            a(a2);
        }
    }

    private b() {
    }

    private static void a(Provider provider) {
        Set<Map.Entry<Object, Object>> entrySet = provider.entrySet();
        String[] strArr = BB;
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            String str = "Alg.Alias." + strArr[i] + ".";
            for (Map.Entry next : entrySet) {
                String str2 = (String) next.getKey();
                if (str2.startsWith(str)) {
                    String substring = str2.substring(str.length());
                    String str3 = (String) next.getValue();
                    String ej = j.ej(str3);
                    if (ao(substring)) {
                        if (substring.startsWith("OID.")) {
                            substring = substring.substring(4);
                        }
                        boolean containsKey = BE.containsKey(substring);
                        boolean containsKey2 = BD.containsKey(ej);
                        if (!containsKey || !containsKey2) {
                            if (!containsKey) {
                                BE.put(substring, ej);
                            }
                            if (!containsKey2) {
                                BD.put(ej, substring);
                            }
                            BF.put(ej, str3);
                        }
                    } else if (!BF.containsKey(j.ej(substring))) {
                        BF.put(j.ej(substring), str3);
                    }
                }
            }
        }
    }

    public static String al(String str) {
        return (String) BD.get(j.ej(str));
    }

    public static String am(String str) {
        String str2 = (String) BE.get(str);
        if (str2 == null) {
            return null;
        }
        return (String) BF.get(str2);
    }

    public static String an(String str) {
        return (String) BF.get(j.ej(str));
    }

    public static boolean ao(String str) {
        try {
            aj.eD(ap(str));
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    public static String ap(String str) {
        return str.startsWith("OID.") ? str.substring(4) : str;
    }
}
