package a.a.a.c.c.a;

import a.a.a.c.a.ah;
import a.a.a.c.a.am;
import a.a.a.c.a.aq;
import a.a.a.c.a.as;
import a.a.a.c.a.p;
import a.a.a.c.a.v;
import java.util.List;

public class e {
    public static final v en = new f(new am[]{as.NW(), new aq(ah.bgE), p.fB()});
    /* access modifiers changed from: private */
    public final i Sd;
    /* access modifiers changed from: private */
    public final List Se;
    /* access modifiers changed from: private */
    public final a Sf;

    public e(i iVar, List list, a aVar) {
        this.Sd = iVar;
        this.Se = list;
        this.Sf = aVar;
    }

    public a qP() {
        return this.Sf;
    }

    public i qQ() {
        return this.Sd;
    }

    public List qR() {
        return this.Se;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("-- PKIStatusInfo:");
        stringBuffer.append("\nPKIStatus : ");
        stringBuffer.append(this.Sd);
        stringBuffer.append("\nstatusString:  ");
        stringBuffer.append(this.Se);
        stringBuffer.append("\nfailInfo:  ");
        stringBuffer.append(this.Sf);
        stringBuffer.append("\n-- PKIStatusInfo End\n");
        return stringBuffer.toString();
    }
}
