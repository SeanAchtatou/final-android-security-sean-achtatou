package a.a.a.c.c.a;

import a.a.a.c.a.ad;
import a.a.a.c.a.aj;
import a.a.a.c.a.am;
import a.a.a.c.a.as;
import a.a.a.c.a.v;
import a.a.a.c.c.br;
import java.math.BigInteger;

final class b extends v {
    b(am[] amVarArr) {
        super(amVarArr);
        c(Boolean.FALSE, 4);
        eU(2);
        eU(3);
        eU(5);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object[] objArr) {
        d dVar = (d) obj;
        objArr[0] = as.jV(dVar.tN);
        objArr[1] = dVar.tO;
        objArr[2] = dVar.tP == null ? null : aj.eD(dVar.tP);
        objArr[3] = dVar.tQ == null ? null : dVar.tQ.toByteArray();
        objArr[4] = dVar.tR == null ? Boolean.FALSE : dVar.tR;
        objArr[5] = dVar.dN;
    }

    /* access modifiers changed from: protected */
    public Object b(ad adVar) {
        Object[] objArr = (Object[]) adVar.auW;
        String ajVar = objArr[2] == null ? null : aj.toString((int[]) objArr[2]);
        BigInteger bigInteger = objArr[3] == null ? null : new BigInteger((byte[]) objArr[3]);
        return objArr[5] == null ? new d(as.U(objArr[0]), (j) objArr[1], ajVar, bigInteger, (Boolean) objArr[4], null, adVar.getEncoded(), null) : new d(as.U(objArr[0]), (j) objArr[1], ajVar, bigInteger, (Boolean) objArr[4], (br) objArr[5], adVar.getEncoded(), null);
    }
}
