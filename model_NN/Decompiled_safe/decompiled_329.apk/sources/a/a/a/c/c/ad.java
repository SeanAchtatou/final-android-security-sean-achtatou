package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.aq;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final class ad extends aq {
    ad(am amVar) {
        super(amVar);
    }

    public Object b(a.a.a.c.a.ad adVar) {
        return new br((List) adVar.auW);
    }

    public Collection b(Object obj) {
        br brVar = (br) obj;
        return brVar.buK == null ? new ArrayList() : brVar.buK;
    }
}
