package a.a.a.c.c;

import a.a.a.c.a.ad;
import a.a.a.c.a.aj;
import a.a.a.c.a.am;
import a.a.a.c.a.e;
import a.a.a.c.f.h;
import a.a.a.c.j.a.a;
import com.uc.c.cd;
import java.io.IOException;

final class az extends e {
    az(am[] amVarArr) {
        super(amVarArr);
    }

    public Object b(ad adVar) {
        be beVar;
        switch (adVar.auZ) {
            case 0:
                beVar = new be((an) adVar.auW);
                break;
            case 1:
            case 2:
                beVar = new be(adVar.auZ, (String) adVar.auW);
                break;
            case 3:
                beVar = new be((as) adVar.auW);
                break;
            case 4:
                beVar = new be((h) adVar.auW);
                break;
            case 5:
                beVar = new be((aa) adVar.auW);
                break;
            case 6:
                String str = (String) adVar.auW;
                if (str.indexOf(cd.bVI) != -1) {
                    beVar = new be(adVar.auZ, str);
                    break;
                } else {
                    throw new IOException(a.c("security.190", str));
                }
            case 7:
                beVar = new be((byte[]) adVar.auW);
                break;
            case 8:
                beVar = new be(adVar.auZ, aj.toString((int[]) adVar.auW));
                break;
            default:
                throw new IOException(a.q("security.191", adVar.auZ));
        }
        byte[] unused = beVar.dV = adVar.getEncoded();
        return beVar;
    }

    public int f(Object obj) {
        return ((be) obj).tag;
    }

    public Object g(Object obj) {
        return ((be) obj).beE;
    }
}
