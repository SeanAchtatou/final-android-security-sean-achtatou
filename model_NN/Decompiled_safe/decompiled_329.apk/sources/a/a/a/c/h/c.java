package a.a.a.c.h;

import java.io.File;
import java.net.URL;
import java.security.PrivilegedExceptionAction;

final class c implements PrivilegedExceptionAction {
    final /* synthetic */ File Ip;

    c(File file) {
        this.Ip = file;
    }

    /* renamed from: kO */
    public URL run() {
        if (this.Ip.exists()) {
            return this.Ip.toURI().toURL();
        }
        return null;
    }
}
