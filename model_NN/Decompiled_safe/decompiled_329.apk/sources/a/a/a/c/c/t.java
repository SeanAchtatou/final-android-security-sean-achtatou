package a.a.a.c.c;

import a.a.a.c.a.ad;
import a.a.a.c.a.am;
import a.a.a.c.a.v;
import java.math.BigInteger;

final class t extends v {
    t(am[] amVarArr) {
        super(amVarArr);
        c(Boolean.FALSE, 0);
        eU(1);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object[] objArr) {
        Object[] objArr2 = (Object[]) obj;
        objArr[0] = (Boolean) objArr2[0];
        objArr[1] = ((BigInteger) objArr2[1]).toByteArray();
    }

    public Object b(ad adVar) {
        return adVar.auW;
    }
}
