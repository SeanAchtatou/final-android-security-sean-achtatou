package a.a.a.c.e.a;

import java.util.Arrays;

public class b {
    private static final long lU = -65536;
    private static final long lV = -4294967296L;
    private static final int lW = 65535;
    private final int lX;
    private final int lY;
    private final long[] lZ;
    private final byte[][] ma;
    private final Object[] mb;
    private final long[] mc;
    private int md;
    private boolean me;

    public b() {
        this(28, 900);
    }

    public b(int i) {
        this(i, 900);
    }

    public b(int i, int i2) {
        this.md = 0;
        this.me = false;
        this.lX = i2;
        this.lY = i;
        this.lZ = new long[this.lX];
        this.mc = new long[this.lX];
        this.ma = new byte[this.lX][];
        this.mb = new Object[this.lX];
    }

    private long y(byte[] bArr) {
        long j = 0;
        for (int length = bArr.length - 1; length > bArr.length - this.lY; length--) {
            j += (long) (bArr[length] & 255);
        }
        return j << 16;
    }

    public Object a(long j, byte[] bArr) {
        long y = y(bArr) | j;
        int binarySearch = (Arrays.binarySearch(this.mc, y) * -1) - 1;
        if (binarySearch == this.lX) {
            return null;
        }
        while ((this.mc[binarySearch] & lU) == y) {
            int i = ((int) (this.mc[binarySearch] & 65535)) - 1;
            if (Arrays.equals(bArr, this.ma[i])) {
                return this.mb[i];
            }
            binarySearch++;
            if (binarySearch == this.lX) {
                return null;
            }
        }
        return null;
    }

    public void a(long j, byte[] bArr, Object obj) {
        if (this.md == this.lX) {
            this.md = 0;
            this.me = true;
        }
        int i = this.md;
        this.md = i + 1;
        long y = y(bArr) | j;
        if (this.me) {
            int binarySearch = Arrays.binarySearch(this.mc, this.lZ[i] | ((long) (i + 1)));
            if (binarySearch < 0) {
                binarySearch = -(binarySearch + 1);
            }
            long j2 = ((long) (i + 1)) | y;
            int binarySearch2 = Arrays.binarySearch(this.mc, j2);
            if (binarySearch2 < 0) {
                int i2 = -(binarySearch2 + 1);
                if (i2 > binarySearch) {
                    System.arraycopy(this.mc, binarySearch + 1, this.mc, binarySearch, (i2 - binarySearch) - 1);
                    this.mc[i2 - 1] = j2;
                } else if (binarySearch > i2) {
                    System.arraycopy(this.mc, i2, this.mc, i2 + 1, binarySearch - i2);
                    this.mc[i2] = j2;
                } else {
                    this.mc[i2] = j2;
                }
            } else if (binarySearch != binarySearch2) {
            }
        } else {
            long j3 = ((long) (i + 1)) | y;
            int binarySearch3 = Arrays.binarySearch(this.mc, j3);
            if (binarySearch3 < 0) {
                binarySearch3 = -(binarySearch3 + 1);
            }
            int i3 = binarySearch3 - 1;
            if (i3 != (this.lX - i) - 1) {
                System.arraycopy(this.mc, this.lX - i, this.mc, (this.lX - i) - 1, (i3 - (this.lX - i)) + 1);
            }
            this.mc[i3] = j3;
        }
        this.lZ[i] = y;
        this.ma[i] = bArr;
        this.mb[i] = obj;
    }

    public boolean a(long j) {
        int binarySearch = (Arrays.binarySearch(this.mc, j) * -1) - 1;
        if (binarySearch == this.lX) {
            return false;
        }
        return (this.mc[binarySearch] & lV) == j;
    }

    public long x(byte[] bArr) {
        long j = 0;
        for (int i = 1; i < this.lY; i++) {
            j += (long) (bArr[i] & 255);
        }
        return j << 32;
    }
}
