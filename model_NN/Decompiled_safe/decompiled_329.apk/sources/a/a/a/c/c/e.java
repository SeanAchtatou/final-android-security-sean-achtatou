package a.a.a.c.c;

import a.a.a.c.a.am;
import java.io.IOException;
import javax.security.auth.x500.X500Principal;

public class e extends ap {
    public static final am lG = new g(new am[]{be.Yv});
    private X500Principal dO;

    public e(be beVar) {
        super(lG.S(beVar));
    }

    public e(byte[] bArr) {
        super(bArr);
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("Certificate Issuer: ");
        if (this.dO == null) {
            try {
                this.dO = getIssuer();
            } catch (IOException e) {
                stringBuffer.append("Unparseable (incorrect!) extension value:\n");
                super.b(stringBuffer);
            }
        }
        stringBuffer.append(this.dO).append(10);
    }

    public X500Principal getIssuer() {
        if (this.dO == null) {
            this.dO = (X500Principal) lG.ax(getEncoded());
        }
        return this.dO;
    }
}
