package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.n;
import a.a.a.c.a.o;
import a.a.a.c.a.v;
import a.a.a.c.j.a.a;

public class i {
    public static final v en = new bw(new am[]{new o(0, bf.Yv), new n(1, ay.uP), new n(2, bl.lG)});
    /* access modifiers changed from: private */
    public final bf qi;
    /* access modifiers changed from: private */
    public final ay qj;
    /* access modifiers changed from: private */
    public final bl qk;

    public i() {
        this.qi = null;
        this.qj = null;
        this.qk = null;
    }

    public i(bf bfVar, ay ayVar, bl blVar) {
        if (ayVar != null && bfVar == null && blVar == null) {
            throw new IllegalArgumentException(a.getString("security.17F"));
        }
        this.qi = bfVar;
        this.qj = ayVar;
        this.qk = blVar;
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str);
        stringBuffer.append("Distribution Point: [\n");
        if (this.qi != null) {
            this.qi.a(stringBuffer, str + "  ");
        }
        if (this.qj != null) {
            this.qj.a(stringBuffer, str + "  ");
        }
        if (this.qk != null) {
            stringBuffer.append(str);
            stringBuffer.append("  CRL Issuer: [\n");
            this.qk.a(stringBuffer, str + "    ");
            stringBuffer.append(str);
            stringBuffer.append("  ]\n");
        }
        stringBuffer.append(str);
        stringBuffer.append("]\n");
    }
}
