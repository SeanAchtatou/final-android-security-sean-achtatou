package a.a.a.c.h;

import a.a.a.c.j.a.a;
import java.security.PrivilegedAction;
import java.security.Security;

public class g implements PrivilegedAction {
    private Class YS;
    private String ds;

    public g(String str, Class cls) {
        this.ds = str;
        this.YS = cls;
    }

    public Object run() {
        String property = Security.getProperty(this.ds);
        if (property == null || property.length() == 0) {
            throw new SecurityException(a.c("security.14C", this.ds));
        }
        try {
            Class<?> cls = Class.forName(property, true, Thread.currentThread().getContextClassLoader());
            if (this.YS == null || !cls.isAssignableFrom(this.YS)) {
                return cls.newInstance();
            }
            throw new SecurityException(a.c("security.14D", property, this.YS.getName()));
        } catch (SecurityException e) {
            throw e;
        } catch (Exception e2) {
            SecurityException securityException = new SecurityException(a.c("security.14E", property));
            securityException.initCause(e2);
            throw securityException;
        }
    }
}
