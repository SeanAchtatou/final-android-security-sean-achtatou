package a.a.a.c.a;

public class p extends ah {
    private static final p uP = new p();

    public p() {
        super(3);
    }

    public static p fB() {
        return uP;
    }

    public Object a(ad adVar) {
        adVar.xj();
        if (adVar.avc) {
            return null;
        }
        return b(adVar);
    }

    public void a(at atVar) {
        atVar.Oi();
    }

    public Object b(ad adVar) {
        byte[] bArr = new byte[(adVar.length - 1)];
        System.arraycopy(adVar.buffer, adVar.auY + 1, bArr, 0, adVar.length - 1);
        return new q(bArr, adVar.buffer[adVar.auY]);
    }

    public void b(at atVar) {
        atVar.length = ((q) atVar.auW).sg.length + 1;
    }
}
