package a.a.a.c.c;

import a.a.a.c.a.aa;
import a.a.a.c.a.am;
import java.util.Date;

public class bu extends ap {
    public static final am lG = aa.uB();
    private final Date bvn;

    public bu(Date date) {
        this.bvn = date;
    }

    public bu(byte[] bArr) {
        super(bArr);
        this.bvn = (Date) lG.ax(bArr);
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("Invalidity Date: [ ").append(this.bvn).append(" ]\n");
    }

    public Date getDate() {
        return this.bvn;
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = lG.S(this.bvn);
        }
        return this.dV;
    }
}
