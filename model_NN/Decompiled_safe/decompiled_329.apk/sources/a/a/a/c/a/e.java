package a.a.a.c.a;

import a.a.a.c.j.a.a;
import java.lang.reflect.Array;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public abstract class e extends am {
    public final am[] dk;
    private final int[][] dl;

    public e(am[] amVarArr) {
        super(0);
        if (amVarArr.length == 0) {
            throw new IllegalArgumentException(a.c("security.10E", getClass().getName()));
        }
        TreeMap treeMap = new TreeMap();
        for (int i = 0; i < amVarArr.length; i++) {
            am amVar = amVarArr[i];
            if (amVar instanceof r) {
                throw new IllegalArgumentException(a.c("security.10F", getClass().getName()));
            }
            if (amVar instanceof e) {
                int[][] iArr = ((e) amVar).dl;
                for (int a2 : iArr[0]) {
                    a(treeMap, a2, i);
                }
            } else {
                if (amVar.v(amVar.id)) {
                    a(treeMap, amVar.id, i);
                }
                if (amVar.v(amVar.btX)) {
                    a(treeMap, amVar.btX, i);
                }
            }
        }
        int size = treeMap.size();
        this.dl = (int[][]) Array.newInstance(Integer.TYPE, 2, size);
        Iterator it = treeMap.entrySet().iterator();
        for (int i2 = 0; i2 < size; i2++) {
            Map.Entry entry = (Map.Entry) it.next();
            this.dl[0][i2] = ((BigInteger) entry.getKey()).intValue();
            this.dl[1][i2] = ((BigInteger) entry.getValue()).intValue();
        }
        this.dk = amVarArr;
    }

    private void a(TreeMap treeMap, int i, int i2) {
        if (treeMap.put(BigInteger.valueOf((long) i), BigInteger.valueOf((long) i2)) != null) {
            throw new IllegalArgumentException(a.c("security.10F", getClass().getName()));
        }
    }

    public Object a(ad adVar) {
        int binarySearch = Arrays.binarySearch(this.dl[0], adVar.tag);
        if (binarySearch < 0) {
            throw new ak(a.c("security.110", getClass().getName()));
        }
        int i = this.dl[1][binarySearch];
        adVar.auW = this.dk[i].a(adVar);
        adVar.auZ = i;
        if (adVar.avc) {
            return null;
        }
        return b(adVar);
    }

    public final void a(at atVar) {
        atVar.a(this);
    }

    public final void b(at atVar) {
        atVar.b(this);
    }

    public void c(at atVar) {
        a(atVar);
    }

    public abstract int f(Object obj);

    public abstract Object g(Object obj);

    public final boolean v(int i) {
        return Arrays.binarySearch(this.dl[0], i) >= 0;
    }
}
