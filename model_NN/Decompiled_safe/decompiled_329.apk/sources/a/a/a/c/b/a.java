package a.a.a.c.b;

import java.util.Arrays;

public final class a {
    private final int[] aG;
    private int aH;
    private String aI;
    private String aJ;
    private Object aK;
    private String name;

    public a(int[] iArr) {
        this.aH = -1;
        a(iArr);
        this.aG = iArr;
    }

    public a(int[] iArr, String str, Object obj) {
        this(iArr);
        if (obj == null) {
            throw new NullPointerException(a.a.a.c.j.a.a.getString("security.172"));
        }
        this.aK = obj;
        this.name = str;
        aZ();
    }

    public static void a(int[] iArr) {
        if (iArr == null) {
            throw new NullPointerException(a.a.a.c.j.a.a.getString("security.98"));
        } else if (iArr.length < 2) {
            throw new IllegalArgumentException(a.a.a.c.j.a.a.getString("security.99"));
        } else if (iArr[0] > 2) {
            throw new IllegalArgumentException(a.a.a.c.j.a.a.getString("security.9A"));
        } else if (iArr[0] != 2 && iArr[1] > 39) {
            throw new IllegalArgumentException(a.a.a.c.j.a.a.getString("security.9B"));
        }
    }

    public static int b(int[] iArr) {
        int i = 0;
        int i2 = 0;
        while (i < iArr.length && i < 4) {
            i2 += iArr[i] << (i * 8);
            i++;
        }
        return Integer.MAX_VALUE & i2;
    }

    public int[] aX() {
        return this.aG;
    }

    public Object aY() {
        return this.aK;
    }

    public String aZ() {
        if (this.aJ == null) {
            this.aJ = "OID." + toString();
        }
        return this.aJ;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return Arrays.equals(this.aG, ((a) obj).aG);
    }

    public String getName() {
        return this.name;
    }

    public int hashCode() {
        if (this.aH == -1) {
            this.aH = b(this.aG);
        }
        return this.aH;
    }

    public String toString() {
        if (this.aI == null) {
            StringBuffer stringBuffer = new StringBuffer(this.aG.length * 4);
            for (int i = 0; i < this.aG.length - 1; i++) {
                stringBuffer.append(this.aG[i]);
                stringBuffer.append('.');
            }
            stringBuffer.append(this.aG[this.aG.length - 1]);
            this.aI = stringBuffer.toString();
        }
        return this.aI;
    }
}
