package a.a.a.c.h;

import a.a.a.c.d;
import com.uc.c.au;
import java.io.File;
import java.net.URL;
import java.security.AccessController;
import java.security.CodeSource;
import java.security.PermissionCollection;
import java.security.Policy;
import java.security.Principal;
import java.security.ProtectionDomain;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.WeakHashMap;

public class f extends Policy {
    public static final String UQ = "java.security.policy";
    public static final String UR = "policy.url.";
    private final Set US;
    private final Map UT;
    private final b UU;
    private boolean UV;

    public f() {
        this(new b());
    }

    public f(b bVar) {
        this.US = new HashSet();
        this.UT = new WeakHashMap();
        this.UU = bVar;
        this.UV = false;
        refresh();
    }

    public PermissionCollection getPermissions(CodeSource codeSource) {
        if (!this.UV) {
            synchronized (this) {
                if (!this.UV) {
                    refresh();
                }
            }
        }
        Collection collection = (Collection) this.UT.get(codeSource);
        if (collection == null) {
            synchronized (this.UT) {
                collection = (Collection) this.UT.get(codeSource);
                if (collection == null) {
                    HashSet hashSet = new HashSet();
                    for (d dVar : this.US) {
                        if (dVar.a((Principal[]) null) && dVar.a(codeSource)) {
                            hashSet.addAll(dVar.wM());
                        }
                    }
                    this.UT.put(codeSource, hashSet);
                    collection = hashSet;
                }
            }
        }
        return l.a(collection);
    }

    public PermissionCollection getPermissions(ProtectionDomain protectionDomain) {
        if (!this.UV) {
            synchronized (this) {
                if (!this.UV) {
                    refresh();
                }
            }
        }
        Collection collection = (Collection) this.UT.get(protectionDomain);
        if (collection == null) {
            synchronized (this.UT) {
                collection = (Collection) this.UT.get(protectionDomain);
                if (collection == null) {
                    HashSet hashSet = new HashSet();
                    for (d dVar : this.US) {
                        if (dVar.a(protectionDomain == null ? null : protectionDomain.getPrincipals())) {
                            if (dVar.a(protectionDomain == null ? null : protectionDomain.getCodeSource())) {
                                hashSet.addAll(dVar.wM());
                            }
                        }
                    }
                    this.UT.put(protectionDomain, hashSet);
                    collection = hashSet;
                }
            }
        }
        return l.a(collection);
    }

    public synchronized void refresh() {
        HashSet hashSet = new HashSet();
        Properties properties = new Properties((Properties) AccessController.doPrivileged(new a()));
        properties.setProperty(au.aGF, File.separator);
        URL[] a2 = l.a(properties, UQ, UR);
        for (int i = 0; i < a2.length; i++) {
            try {
                hashSet.addAll(this.UU.a(a2[i], properties));
            } catch (Exception e) {
            }
        }
        synchronized (this.UT) {
            this.US.clear();
            this.US.addAll(hashSet);
            this.UT.clear();
        }
        this.UV = true;
    }
}
