package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.as;
import a.a.a.c.a.n;
import a.a.a.c.a.o;
import a.a.a.c.a.p;
import a.a.a.c.a.v;
import a.a.a.c.f.h;
import java.math.BigInteger;

public class m {
    public static final v en = new f(new am[]{new o(0, as.NW()), as.NW(), al.en, h.aCz, v.en, h.aCz, bi.en, new n(1, p.fB()), new n(2, p.fB()), new o(3, br.lG)});
    /* access modifiers changed from: private */
    public final br dN;
    byte[] dV;
    /* access modifiers changed from: private */
    public final int tN;
    /* access modifiers changed from: private */
    public final BigInteger vb;
    /* access modifiers changed from: private */
    public final al vc;
    /* access modifiers changed from: private */
    public final h vd;
    /* access modifiers changed from: private */
    public final v ve;
    /* access modifiers changed from: private */
    public final h vf;
    /* access modifiers changed from: private */
    public final bi vg;
    /* access modifiers changed from: private */
    public final boolean[] vh;
    /* access modifiers changed from: private */
    public final boolean[] vi;

    public m(int i, BigInteger bigInteger, al alVar, h hVar, v vVar, h hVar2, bi biVar) {
        this(i, bigInteger, alVar, hVar, vVar, hVar2, biVar, null, null, null);
    }

    public m(int i, BigInteger bigInteger, al alVar, h hVar, v vVar, h hVar2, bi biVar, boolean[] zArr, boolean[] zArr2, br brVar) {
        this.tN = i;
        this.vb = bigInteger;
        this.vc = alVar;
        this.vd = hVar;
        this.ve = vVar;
        this.vf = hVar2;
        this.vg = biVar;
        this.vh = zArr;
        this.vi = zArr2;
        this.dN = brVar;
    }

    private m(int i, BigInteger bigInteger, al alVar, h hVar, v vVar, h hVar2, bi biVar, boolean[] zArr, boolean[] zArr2, br brVar, byte[] bArr) {
        this(i, bigInteger, alVar, hVar, vVar, hVar2, biVar, zArr, zArr2, brVar);
        this.dV = bArr;
    }

    /* synthetic */ m(int i, BigInteger bigInteger, al alVar, h hVar, v vVar, h hVar2, bi biVar, boolean[] zArr, boolean[] zArr2, br brVar, byte[] bArr, f fVar) {
        this(i, bigInteger, alVar, hVar, vVar, hVar2, biVar, zArr, zArr2, brVar, bArr);
    }

    public void b(StringBuffer stringBuffer) {
        stringBuffer.append('[');
        stringBuffer.append("\n  Version: V").append(this.tN + 1);
        stringBuffer.append("\n  Subject: ").append(this.vf.getName("RFC2253"));
        stringBuffer.append("\n  Signature Algorithm: ");
        this.vc.b(stringBuffer);
        stringBuffer.append("\n  Key: ");
        stringBuffer.append(this.vg.getPublicKey().toString());
        stringBuffer.append("\n  Validity: [From: ");
        stringBuffer.append(this.ve.getNotBefore());
        stringBuffer.append("\n               To: ");
        stringBuffer.append(this.ve.getNotAfter()).append(']');
        stringBuffer.append("\n  Issuer: ");
        stringBuffer.append(this.vd.getName("RFC2253"));
        stringBuffer.append("\n  Serial Number: ");
        stringBuffer.append(this.vb);
        if (this.vh != null) {
            stringBuffer.append("\n  Issuer Id: ");
            for (int i = 0; i < this.vh.length; i++) {
                stringBuffer.append(this.vh[i] ? '1' : '0');
            }
        }
        if (this.vi != null) {
            stringBuffer.append("\n  Subject Id: ");
            for (int i2 = 0; i2 < this.vi.length; i2++) {
                stringBuffer.append(this.vi[i2] ? '1' : '0');
            }
        }
        if (this.dN != null) {
            stringBuffer.append("\n\n  Extensions: ");
            stringBuffer.append("[\n");
            this.dN.a(stringBuffer, "    ");
            stringBuffer.append("  ]");
        }
        stringBuffer.append("\n]");
    }

    public al fM() {
        return this.vc;
    }

    public h fN() {
        return this.vd;
    }

    public v fO() {
        return this.ve;
    }

    public h fP() {
        return this.vf;
    }

    public bi fQ() {
        return this.vg;
    }

    public br fn() {
        return this.dN;
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public boolean[] getIssuerUniqueID() {
        return this.vh;
    }

    public BigInteger getSerialNumber() {
        return this.vb;
    }

    public boolean[] getSubjectUniqueID() {
        return this.vi;
    }

    public int getVersion() {
        return this.tN;
    }
}
