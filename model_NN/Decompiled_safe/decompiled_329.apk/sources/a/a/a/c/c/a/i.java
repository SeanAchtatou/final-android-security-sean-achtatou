package a.a.a.c.c.a;

import java.security.InvalidParameterException;

public enum i {
    GRANTED(0),
    GRANTED_WITH_MODS(1),
    REJECTION(2),
    WAITING(3),
    REVOCATION_WARNING(4),
    REVOCATION_NOTIFICATION(5);
    
    private final int status;

    private i(int i) {
        this.status = i;
    }

    public static i cr(String str) {
        return (i) Enum.valueOf(i.class, str);
    }

    public static i ez(int i) {
        for (i iVar : ue()) {
            if (i == iVar.status) {
                return iVar;
            }
        }
        throw new InvalidParameterException("Unknown PKIStatus value");
    }

    public static i[] ue() {
        return (i[]) YK.clone();
    }

    public int getStatus() {
        return this.status;
    }
}
