package a.a.a.c.c;

import a.a.a.c.a.ag;
import a.a.a.c.b.c;

public class s extends ap {
    private final byte[] GQ;

    public s(byte[] bArr) {
        this.GQ = bArr;
    }

    public static s M(byte[] bArr) {
        s sVar = new s((byte[]) ag.Bl().ax(bArr));
        sVar.dV = bArr;
        return sVar;
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("SubjectKeyIdentifier: [\n");
        stringBuffer.append(c.d(this.GQ, str));
        stringBuffer.append(str).append("]\n");
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = ag.Bl().S(this.GQ);
        }
        return this.dV;
    }
}
