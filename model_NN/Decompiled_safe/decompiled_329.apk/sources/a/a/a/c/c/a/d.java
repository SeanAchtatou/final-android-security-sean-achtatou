package a.a.a.c.c.a;

import a.a.a.c.a.am;
import a.a.a.c.a.as;
import a.a.a.c.a.c;
import a.a.a.c.a.n;
import a.a.a.c.a.u;
import a.a.a.c.a.v;
import a.a.a.c.c.br;
import java.math.BigInteger;

public class d {
    public static final v en = new b(new am[]{as.NW(), j.en, c.bb(), as.NW(), u.kZ(), new n(0, br.lG)});
    /* access modifiers changed from: private */
    public final br dN;
    private byte[] dV;
    /* access modifiers changed from: private */
    public final int tN;
    /* access modifiers changed from: private */
    public final j tO;
    /* access modifiers changed from: private */
    public final String tP;
    /* access modifiers changed from: private */
    public final BigInteger tQ;
    /* access modifiers changed from: private */
    public final Boolean tR;

    public d(int i, j jVar, String str, BigInteger bigInteger, Boolean bool, br brVar) {
        this.tN = i;
        this.tO = jVar;
        this.tP = str;
        this.tQ = bigInteger;
        this.tR = bool;
        this.dN = brVar;
    }

    private d(int i, j jVar, String str, BigInteger bigInteger, Boolean bool, br brVar, byte[] bArr) {
        this(i, jVar, str, bigInteger, bool, brVar);
        this.dV = bArr;
    }

    /* synthetic */ d(int i, j jVar, String str, BigInteger bigInteger, Boolean bool, br brVar, byte[] bArr, b bVar) {
        this(i, jVar, str, bigInteger, bool, brVar, bArr);
    }

    public Boolean fm() {
        return this.tR;
    }

    public br fn() {
        return this.dN;
    }

    public j fo() {
        return this.tO;
    }

    public BigInteger fp() {
        return this.tQ;
    }

    public String fq() {
        return this.tP;
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public int getVersion() {
        return this.tN;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("-- TimeStampReq:");
        stringBuffer.append("\nversion : ");
        stringBuffer.append(this.tN);
        stringBuffer.append("\nmessageImprint:  ");
        stringBuffer.append(this.tO);
        stringBuffer.append("\nreqPolicy:  ");
        stringBuffer.append(this.tP);
        stringBuffer.append("\nnonce:  ");
        stringBuffer.append(this.tQ);
        stringBuffer.append("\ncertReq:  ");
        stringBuffer.append(this.tR);
        stringBuffer.append("\nextensions:  ");
        stringBuffer.append(this.dN);
        stringBuffer.append("\n-- TimeStampReq End\n");
        return stringBuffer.toString();
    }
}
