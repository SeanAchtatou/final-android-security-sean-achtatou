package a.a.a.c.a;

public class at {
    public Object auW;
    public byte[] dg;
    public int length;
    protected int pG;

    public void Oh() {
        System.arraycopy(this.auW, 0, this.dg, this.pG, this.length);
        this.pG += this.length;
    }

    public void Oi() {
        q qVar = (q) this.auW;
        this.dg[this.pG] = (byte) qVar.BI;
        System.arraycopy(qVar.sg, 0, this.dg, this.pG + 1, this.length - 1);
        this.pG += this.length;
    }

    public void Oj() {
        if (((Boolean) this.auW).booleanValue()) {
            this.dg[this.pG] = -1;
        } else {
            this.dg[this.pG] = 0;
        }
        this.pG++;
    }

    public void Ok() {
        System.arraycopy(this.auW, 0, this.dg, this.pG, this.length);
        this.pG += this.length;
    }

    public void Ol() {
        System.arraycopy(this.auW, 0, this.dg, this.pG, this.length);
        this.pG += this.length;
    }

    public void Om() {
        System.arraycopy(this.auW, 0, this.dg, this.pG, this.length);
        this.pG += this.length;
    }

    public void On() {
        System.arraycopy(this.auW, 0, this.dg, this.pG, this.length);
        this.pG += this.length;
    }

    public void Oo() {
        int[] iArr = (int[]) this.auW;
        int i = this.length;
        int length2 = iArr.length - 1;
        while (length2 > 1) {
            int i2 = iArr[length2];
            if (i2 > 127) {
                this.dg[(this.pG + i) - 1] = (byte) (i2 & 127);
                int i3 = i2 >> 7;
                int i4 = i;
                for (int i5 = i3; i5 > 0; i5 >>= 7) {
                    i4--;
                    this.dg[(this.pG + i4) - 1] = (byte) (i5 | 128);
                }
                i = i4;
            } else {
                this.dg[(this.pG + i) - 1] = (byte) i2;
            }
            length2--;
            i--;
        }
        int i6 = iArr[1] + (iArr[0] * 40);
        if (i6 > 127) {
            this.dg[(this.pG + i) - 1] = (byte) (i6 & 127);
            int i7 = i;
            for (int i8 = i6 >> 7; i8 > 0; i8 >>= 7) {
                i7--;
                this.dg[(this.pG + i7) - 1] = (byte) (i8 | 128);
            }
        } else {
            this.dg[(i + this.pG) - 1] = (byte) i6;
        }
        this.pG += this.length;
    }

    public void Op() {
        System.arraycopy(this.auW, 0, this.dg, this.pG, this.length);
        this.pG += this.length;
    }

    public int V(Object obj) {
        throw new RuntimeException("Is not implemented yet");
    }

    public void a(a aVar) {
        throw new RuntimeException("Is not implemented yet");
    }

    public void a(aq aqVar) {
        throw new RuntimeException("Is not implemented yet");
    }

    public void a(e eVar) {
        throw new RuntimeException("Is not implemented yet");
    }

    public void a(o oVar) {
        throw new RuntimeException("Is not implemented yet");
    }

    public void a(v vVar) {
        throw new RuntimeException("Is not implemented yet");
    }

    public void b(a aVar) {
        throw new RuntimeException("Is not implemented yet");
    }

    public void b(al alVar) {
        throw new RuntimeException("Is not implemented yet");
    }

    public void b(aq aqVar) {
        throw new RuntimeException("Is not implemented yet");
    }

    public void b(e eVar) {
        throw new RuntimeException("Is not implemented yet");
    }

    public void b(o oVar) {
        throw new RuntimeException("Is not implemented yet");
    }

    public void b(v vVar) {
        throw new RuntimeException("Is not implemented yet");
    }

    public void c(al alVar) {
        throw new RuntimeException("Is not implemented yet");
    }

    public final void kc(int i) {
        byte[] bArr = this.dg;
        int i2 = this.pG;
        this.pG = i2 + 1;
        bArr[i2] = (byte) i;
        if (this.length > 127) {
            byte b2 = 1;
            for (int i3 = this.length >> 8; i3 > 0; i3 >>= 8) {
                b2 = (byte) (b2 + 1);
            }
            this.dg[this.pG] = (byte) (b2 | 128);
            this.pG++;
            int i4 = this.length;
            int i5 = (this.pG + b2) - 1;
            int i6 = i4;
            int i7 = 0;
            while (i7 < b2) {
                this.dg[i5 - i7] = (byte) i6;
                i7++;
                i6 >>= 8;
            }
            this.pG = b2 + this.pG;
            return;
        }
        byte[] bArr2 = this.dg;
        int i8 = this.pG;
        this.pG = i8 + 1;
        bArr2[i8] = (byte) this.length;
    }
}
