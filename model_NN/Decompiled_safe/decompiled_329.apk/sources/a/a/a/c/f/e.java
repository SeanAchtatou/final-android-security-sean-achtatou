package a.a.a.c.f;

import a.a.a.c.a.ah;
import a.a.a.c.a.aj;
import a.a.a.c.a.am;
import a.a.a.c.a.c;
import a.a.a.c.a.v;
import a.a.a.c.b.a;
import a.a.a.c.j;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;

public class e {
    private static final int SIZE = 10;
    private static final HashMap VA = new HashMap(10);
    public static final am VD = new a(19);
    private static final a Vd = new a(new int[]{2, 5, 4, 6}, "C", Vy);
    private static final a Ve = new a(new int[]{2, 5, 4, 3}, "CN", Vy);
    private static final a Vf = new a(new int[]{0, 9, 2342, 19200300, 100, 1, 25}, "DC", Vz);
    private static final a Vg = new a(new int[]{2, 5, 4, 46}, "DNQ", VA);
    private static final a Vh = new a(new int[]{2, 5, 4, 46}, "DNQUALIFIER", VA);
    private static final a Vi = new a(new int[]{1, 2, 840, 113549, 1, 9, 1}, "EMAILADDRESS", VA);
    private static final a Vj = new a(new int[]{2, 5, 4, 44}, "GENERATION", VA);
    private static final a Vk = new a(new int[]{2, 5, 4, 42}, "GIVENNAME", VA);
    private static final a Vl = new a(new int[]{2, 5, 4, 43}, "INITIALS", VA);
    private static final a Vm = new a(new int[]{2, 5, 4, 7}, "L", Vy);
    private static final a Vn = new a(new int[]{2, 5, 4, 10}, "O", Vy);
    private static final a Vo = new a(new int[]{2, 5, 4, 11}, "OU", Vy);
    private static final a Vp = new a(new int[]{2, 5, 4, 5}, "SERIALNUMBER", VA);
    private static final a Vq = new a(new int[]{2, 5, 4, 8}, "ST", Vy);
    private static final a Vr = new a(new int[]{2, 5, 4, 9}, "STREET", Vy);
    private static final a Vs = new a(new int[]{2, 5, 4, 4}, "SURNAME", VA);
    private static final a Vt = new a(new int[]{2, 5, 4, 12}, "T", VA);
    private static final a Vu = new a(new int[]{0, 9, 2342, 19200300, 100, 1, 1}, "UID", Vz);
    private static final int Vv = 10;
    private static final a[][] Vw = ((a[][]) Array.newInstance(a.class, SIZE, Vv));
    private static final HashMap Vx = new HashMap(30);
    private static final HashMap Vy = new HashMap(10);
    private static final HashMap Vz = new HashMap(10);
    public static final v en = new b(new am[]{c.bb(), VD});
    /* access modifiers changed from: private */
    public final a VB;
    /* access modifiers changed from: private */
    public c VC;

    static {
        Vy.put(Ve.getName(), Ve);
        Vy.put(Vm.getName(), Vm);
        Vy.put(Vq.getName(), Vq);
        Vy.put(Vn.getName(), Vn);
        Vy.put(Vo.getName(), Vo);
        Vy.put(Vd.getName(), Vd);
        Vy.put(Vr.getName(), Vr);
        Vz.putAll(Vy);
        Vz.put(Vf.getName(), Vf);
        Vz.put(Vu.getName(), Vu);
        VA.put(Vg.getName(), Vg);
        VA.put(Vh.getName(), Vh);
        VA.put(Vi.getName(), Vi);
        VA.put(Vj.getName(), Vj);
        VA.put(Vk.getName(), Vk);
        VA.put(Vl.getName(), Vl);
        VA.put(Vp.getName(), Vp);
        VA.put(Vs.getName(), Vs);
        VA.put(Vt.getName(), Vt);
        for (a a2 : Vz.values()) {
            a(a2);
        }
        for (Object next : VA.values()) {
            if (next != Vh) {
                a((a) next);
            }
        }
        Vx.putAll(Vz);
        Vx.putAll(VA);
    }

    public e(String str, c cVar) {
        if (str.charAt(0) < '0' || str.charAt(0) > '9') {
            this.VB = (a) Vx.get(j.ej(str));
            if (this.VB == null) {
                throw new IOException(a.a.a.c.j.a.a.c("security.178", str));
            }
        } else {
            int[] eD = aj.eD(str);
            a f = f(eD);
            this.VB = f == null ? new a(eD) : f;
        }
        this.VC = cVar;
    }

    private e(int[] iArr, c cVar) {
        a f = f(iArr);
        this.VB = f == null ? new a(iArr) : f;
        this.VC = cVar;
    }

    /* synthetic */ e(int[] iArr, c cVar, a aVar) {
        this(iArr, cVar);
    }

    private static void a(a aVar) {
        int[] aX = aVar.aX();
        a[] aVarArr = Vw[b(aX) % Vv];
        int i = 0;
        while (aVarArr[i] != null) {
            if (Arrays.equals(aX, aVarArr[i].aX())) {
                throw new Error(a.a.a.c.j.a.a.c("security.17B", aVar.getName(), aVarArr[i].getName()));
            }
            i++;
        }
        if (i == Vv - 1) {
            throw new Error(a.a.a.c.j.a.a.getString("security.17C"));
        }
        aVarArr[i] = aVar;
    }

    private static int b(int[] iArr) {
        int i = 0;
        int i2 = 0;
        while (i < iArr.length && i < 4) {
            i2 += iArr[i] << (i * 8);
            i++;
        }
        return Integer.MAX_VALUE & i2;
    }

    private static a f(int[] iArr) {
        a[] aVarArr = Vw[b(iArr) % Vv];
        for (int i = 0; aVarArr[i] != null; i++) {
            if (Arrays.equals(iArr, aVarArr[i].aX())) {
                return aVarArr[i];
            }
        }
        return null;
    }

    public void a(String str, StringBuffer stringBuffer) {
        boolean z = false;
        if ("RFC1779".equals(str)) {
            if (Vy == this.VB.aY()) {
                stringBuffer.append(this.VB.getName());
            } else {
                stringBuffer.append(this.VB.aZ());
            }
            stringBuffer.append('=');
            if (this.VC.se == this.VC.eI()) {
                stringBuffer.append(j.ej(this.VC.eI()));
            } else if (this.VC.se.length() != this.VC.si.length()) {
                this.VC.a(stringBuffer);
            } else {
                stringBuffer.append(this.VC.se);
            }
        } else {
            Object aY = this.VB.aY();
            if (Vy == aY || Vz == aY) {
                stringBuffer.append(this.VB.getName());
                if ("CANONICAL".equals(str)) {
                    int eH = this.VC.eH();
                    if (!ah.bgE.v(eH) && !ah.bgB.v(eH)) {
                        z = true;
                    }
                }
            } else {
                stringBuffer.append(this.VB.toString());
                z = true;
            }
            stringBuffer.append('=');
            if (z) {
                stringBuffer.append(this.VC.eI());
            } else if ("CANONICAL".equals(str)) {
                stringBuffer.append(this.VC.eJ());
            } else {
                stringBuffer.append(this.VC.se);
            }
        }
    }

    public a sd() {
        return this.VB;
    }
}
