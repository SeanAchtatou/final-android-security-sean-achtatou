package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.as;
import a.a.a.c.a.v;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Date;
import javax.security.auth.x500.X500Principal;

public class bn {
    public static final v en = new au(new am[]{as.NW(), bq.Yv, br.lG});
    /* access modifiers changed from: private */
    public final BigInteger btQ;
    /* access modifiers changed from: private */
    public final Date btR;
    /* access modifiers changed from: private */
    public final br btS;
    private boolean btT;
    private X500Principal dO;
    private byte[] dV;

    public bn(BigInteger bigInteger, Date date, br brVar) {
        this.btQ = bigInteger;
        this.btR = date;
        this.btS = brVar;
    }

    public br FM() {
        return this.btS;
    }

    public BigInteger FN() {
        return this.btQ;
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("Certificate Serial Number: ").append(this.btQ).append(10);
        stringBuffer.append(str).append("Revocation Date: ").append(this.btR);
        if (this.btS != null) {
            stringBuffer.append(10).append(str).append("CRL Entry Extensions: [");
            this.btS.a(stringBuffer, str + "  ");
            stringBuffer.append(str).append(']');
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof bn)) {
            return false;
        }
        bn bnVar = (bn) obj;
        return this.btQ.equals(bnVar.btQ) && this.btR.getTime() / 1000 == bnVar.btR.getTime() / 1000 && (this.btS != null ? this.btS.equals(bnVar.btS) : bnVar.btS == null);
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public X500Principal getIssuer() {
        if (this.btS == null) {
            return null;
        }
        if (!this.btT) {
            try {
                this.dO = this.btS.GG();
            } catch (IOException e) {
            }
            this.btT = true;
        }
        return this.dO;
    }

    public Date getRevocationDate() {
        return this.btR;
    }

    public int hashCode() {
        return (this.btQ.hashCode() * 37) + (((int) this.btR.getTime()) / 1000) + (this.btS == null ? 0 : this.btS.hashCode());
    }
}
