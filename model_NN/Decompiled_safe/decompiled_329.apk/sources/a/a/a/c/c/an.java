package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.c;
import a.a.a.c.a.o;
import a.a.a.c.a.r;
import a.a.a.c.a.v;

public class an {
    public static final v en = new z(new am[]{c.bb(), new o(0, r.iS())});
    /* access modifiers changed from: private */
    public String aCb;
    /* access modifiers changed from: private */
    public byte[] aCc;
    private byte[] dV;

    public an(String str, byte[] bArr) {
        this(str, bArr, null);
    }

    private an(String str, byte[] bArr, byte[] bArr2) {
        this.aCb = str;
        this.aCc = bArr;
        this.dV = bArr2;
    }

    /* synthetic */ an(String str, byte[] bArr, byte[] bArr2, z zVar) {
        this(str, bArr, bArr2);
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public byte[] getValue() {
        return this.aCc;
    }

    public String yD() {
        return this.aCb;
    }
}
