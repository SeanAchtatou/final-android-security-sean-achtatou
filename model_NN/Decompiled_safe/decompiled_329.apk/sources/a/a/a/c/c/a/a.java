package a.a.a.c.c.a;

import java.security.InvalidParameterException;

public enum a {
    BAD_ALG(0),
    BAD_REQUEST(2),
    BAD_DATA_FORMAT(5),
    TIME_NOT_AVAILABLE(14),
    UNACCEPTED_POLICY(15),
    UNACCEPTED_EXTENSION(16),
    ADD_INFO_NOT_AVAILABLE(17),
    SYSTEM_FAILURE(25);
    
    private static int cZ;
    private final int value;

    private a(int i) {
        this.value = i;
    }

    public static a[] bW() {
        return (a[]) da.clone();
    }

    public static int bY() {
        if (cZ == 0) {
            for (a aVar : bW()) {
                if (aVar.value > cZ) {
                    cZ = aVar.value;
                }
            }
        }
        return cZ;
    }

    public static a s(String str) {
        return (a) Enum.valueOf(a.class, str);
    }

    public static a u(int i) {
        for (a aVar : bW()) {
            if (i == aVar.value) {
                return aVar;
            }
        }
        throw new InvalidParameterException("Unknown PKIFailureInfo value");
    }

    public int bX() {
        return this.value;
    }
}
