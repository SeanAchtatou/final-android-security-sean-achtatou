package a.a.a.c.f;

import a.a.a.c.a.ad;
import a.a.a.c.a.ah;
import a.a.a.c.a.am;
import a.a.a.c.a.at;

final class a extends am {
    a(int i) {
        super(i);
    }

    public Object a(ad adVar) {
        String str = null;
        if (f.Yv.v(adVar.tag)) {
            str = (String) f.Yv.a(adVar);
        } else {
            adVar.xw();
        }
        byte[] bArr = new byte[(adVar.getOffset() - adVar.xt())];
        System.arraycopy(adVar.xr(), adVar.xt(), bArr, 0, bArr.length);
        return new c(str, bArr, adVar.tag);
    }

    public void a(at atVar) {
        throw new RuntimeException(a.a.a.c.j.a.a.getString("security.17A"));
    }

    public Object b(ad adVar) {
        throw new RuntimeException(a.a.a.c.j.a.a.getString("security.179"));
    }

    public void b(at atVar) {
        c cVar = (c) atVar.auW;
        if (cVar.dg != null) {
            atVar.length = cVar.dg.length;
        } else if (cVar.eH() == 12) {
            atVar.auW = cVar.si;
            ah.bgE.b(atVar);
            cVar.sg = (byte[]) atVar.auW;
            atVar.auW = cVar;
        } else {
            cVar.sg = cVar.si.getBytes();
            atVar.length = cVar.sg.length;
        }
    }

    public void c(at atVar) {
        c cVar = (c) atVar.auW;
        if (cVar.dg != null) {
            atVar.auW = cVar.dg;
            atVar.Oh();
            return;
        }
        atVar.kc(cVar.eH());
        atVar.auW = cVar.sg;
        atVar.Op();
    }

    public int d(at atVar) {
        return ((c) atVar.auW).dg != null ? atVar.length : super.d(atVar);
    }

    public boolean v(int i) {
        return true;
    }
}
