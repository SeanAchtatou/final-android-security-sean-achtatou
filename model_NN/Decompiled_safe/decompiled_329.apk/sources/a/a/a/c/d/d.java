package a.a.a.c.d;

import a.a.a.c.a.a;
import a.a.a.c.f.e;
import java.util.List;

class d {
    public static final a TO = new a(e.en);
    private List TN;
    private byte[] dV;

    public d(byte[] bArr, List list) {
        this.dV = bArr;
        this.TN = list;
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = TO.S(this);
        }
        return this.dV;
    }

    public List kW() {
        return this.TN;
    }
}
