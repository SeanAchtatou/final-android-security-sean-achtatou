package a.a.a.c.e.a;

import a.a.a.c.j.a.a;
import java.security.AccessController;
import java.security.Provider;

public final class i extends Provider {
    private static final long serialVersionUID = -7269650779605195879L;

    public i() {
        super("DRLCertFactory", 1.0d, a.getString("security.151"));
        AccessController.doPrivileged(new h(this));
    }
}
