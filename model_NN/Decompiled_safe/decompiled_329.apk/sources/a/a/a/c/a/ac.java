package a.a.a.c.a;

import a.a.a.c.j.a.a;

public abstract class ac extends x {
    public final boolean[] asF;
    public final Object[] asG;
    public final am[] dk;

    public ac(int i, am[] amVarArr) {
        super(i);
        this.dk = amVarArr;
        this.asF = new boolean[amVarArr.length];
        this.asG = new Object[amVarArr.length];
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object[] objArr) {
        throw new RuntimeException(a.c("security.101", getClass().getName()));
    }

    /* access modifiers changed from: protected */
    public final void c(Object obj, int i) {
        this.asF[i] = true;
        this.asG[i] = obj;
    }

    /* access modifiers changed from: protected */
    public final void eU(int i) {
        this.asF[i] = true;
    }
}
