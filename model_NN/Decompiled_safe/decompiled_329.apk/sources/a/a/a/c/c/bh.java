package a.a.a.c.c;

import a.a.a.c.a.ag;
import a.a.a.c.a.am;
import a.a.a.c.a.n;
import a.a.a.c.a.v;
import a.a.a.c.j.a.a;
import java.io.IOException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class bh extends ap {
    public static final v en = new bc(new am[]{new n(0, ae.lG), new n(1, ae.lG)});
    /* access modifiers changed from: private */
    public final ae bmq;
    /* access modifiers changed from: private */
    public final ae bmr;
    private ArrayList[] bms;
    private ArrayList[] bmt;
    private byte[] dV;

    public bh() {
        this(null, null);
    }

    public bh(ae aeVar, ae aeVar2) {
        List uq;
        List uq2;
        if (aeVar != null && ((uq2 = aeVar.uq()) == null || uq2.size() == 0)) {
            throw new IllegalArgumentException(a.getString("security.17D"));
        } else if (aeVar2 == null || !((uq = aeVar2.uq()) == null || uq.size() == 0)) {
            this.bmq = aeVar;
            this.bmr = aeVar2;
        } else {
            throw new IllegalArgumentException(a.getString("security.17E"));
        }
    }

    private bh(ae aeVar, ae aeVar2, byte[] bArr) {
        this(aeVar, aeVar2);
        this.dV = bArr;
    }

    /* synthetic */ bh(ae aeVar, ae aeVar2, byte[] bArr, bc bcVar) {
        this(aeVar, aeVar2, bArr);
    }

    private void Eb() {
        this.bms = new ArrayList[9];
        if (this.bmq != null) {
            for (bk EI : this.bmq.uq()) {
                be EI2 = EI.EI();
                int eH = EI2.eH();
                if (this.bms[eH] == null) {
                    this.bms[eH] = new ArrayList();
                }
                this.bms[eH].add(EI2);
            }
        }
        this.bmt = new ArrayList[9];
        if (this.bmr != null) {
            for (bk EI3 : this.bmr.uq()) {
                be EI4 = EI3.EI();
                int eH2 = EI4.eH();
                if (this.bmt[eH2] == null) {
                    this.bmt[eH2] = new ArrayList();
                }
                this.bmt[eH2].add(EI4);
            }
        }
    }

    private byte[] a(X509Certificate x509Certificate, String str) {
        try {
            byte[] extensionValue = x509Certificate.getExtensionValue(str);
            if (extensionValue == null) {
                return null;
            }
            return (byte[]) ag.Bl().ax(extensionValue);
        } catch (IOException e) {
            return null;
        }
    }

    public static bh av(byte[] bArr) {
        return (bh) en.ax(bArr);
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("Name Constraints: [\n");
        if (this.bmq != null) {
            stringBuffer.append(str).append("  Permitted: [\n");
            for (bk a2 : this.bmq.uq()) {
                a2.a(stringBuffer, str + "    ");
            }
            stringBuffer.append(str).append("  ]\n");
        }
        if (this.bmr != null) {
            stringBuffer.append(str).append("  Excluded: [\n");
            for (bk a3 : this.bmr.uq()) {
                a3.a(stringBuffer, str + "    ");
            }
            stringBuffer.append(str).append("  ]\n");
        }
        stringBuffer.append(10).append(str).append("]\n");
    }

    public boolean a(X509Certificate x509Certificate) {
        List EL;
        if (this.bms == null) {
            Eb();
        }
        byte[] a2 = a(x509Certificate, "2.5.29.17");
        if (a2 == null) {
            try {
                EL = new ArrayList(1);
            } catch (IOException e) {
                return false;
            }
        } else {
            EL = ((bl) bl.lG.ax(a2)).EL();
        }
        if (!(this.bmt[4] == null && this.bms[4] == null)) {
            try {
                EL.add(new be(4, x509Certificate.getSubjectX500Principal().getName()));
            } catch (IOException e2) {
            }
        }
        return h(EL);
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public boolean h(List list) {
        if (this.bms == null) {
            Eb();
        }
        Iterator it = list.iterator();
        boolean[] zArr = new boolean[9];
        boolean[] zArr2 = new boolean[9];
        while (it.hasNext()) {
            be beVar = (be) it.next();
            int eH = beVar.eH();
            if (this.bmt[eH] != null) {
                for (int i = 0; i < this.bmt[eH].size(); i++) {
                    if (((be) this.bmt[eH].get(i)).a(beVar)) {
                        return false;
                    }
                }
            }
            if (this.bms[eH] != null && !zArr2[eH]) {
                zArr[eH] = true;
                for (int i2 = 0; i2 < this.bms[eH].size(); i2++) {
                    if (((be) this.bms[eH].get(i2)).a(beVar)) {
                        zArr2[eH] = true;
                    }
                }
            }
        }
        for (int i3 = 0; i3 < 9; i3++) {
            if (zArr[i3] && !zArr2[i3]) {
                return false;
            }
        }
        return true;
    }
}
