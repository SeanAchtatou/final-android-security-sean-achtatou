package a.a.a.c.c;

import a.a.a.c.a.ad;
import a.a.a.c.a.am;
import a.a.a.c.a.e;
import a.a.a.c.f.h;

final class ag extends e {
    ag(am[] amVarArr) {
        super(amVarArr);
    }

    /* access modifiers changed from: protected */
    public Object b(ad adVar) {
        return adVar.auZ == 0 ? new bf((bl) adVar.auW) : new bf((h) adVar.auW);
    }

    public int f(Object obj) {
        return ((bf) obj).bgv == null ? 1 : 0;
    }

    public Object g(Object obj) {
        bf bfVar = (bf) obj;
        return bfVar.bgv == null ? bfVar.bgw : bfVar.bgv;
    }
}
