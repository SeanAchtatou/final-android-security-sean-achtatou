package a.a.a.c.c.a;

import a.a.a.c.a.am;
import a.a.a.c.a.v;
import a.a.a.c.d.i;

public class k {
    public static final v en = new c(new am[]{e.en, i.en});
    /* access modifiers changed from: private */
    public final e blM;
    /* access modifiers changed from: private */
    public final i blN;

    public k(e eVar, i iVar) {
        this.blM = eVar;
        this.blN = iVar;
    }

    public e DM() {
        return this.blM;
    }

    public i DN() {
        return this.blN;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("-- TimeStampResp:");
        stringBuffer.append("\nstatus:  ");
        stringBuffer.append(this.blM);
        stringBuffer.append("\ntimeStampToken:  ");
        stringBuffer.append(this.blN);
        stringBuffer.append("\n-- TimeStampResp End\n");
        return stringBuffer.toString();
    }
}
