package a.a.a.c.h;

import a.a.a.c.a;
import a.a.a.c.d;
import a.a.a.c.h;
import a.a.a.c.i;
import a.a.a.c.k;
import a.a.a.c.l;
import com.uc.c.cd;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.AccessController;
import java.security.CodeSource;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.Permission;
import java.security.Principal;
import java.security.UnresolvedPermission;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Properties;
import java.util.StringTokenizer;

public class b {
    private final h uQ;

    public b() {
        this.uQ = new h();
    }

    public b(h hVar) {
        this.uQ = hVar;
    }

    /* access modifiers changed from: protected */
    public d a(k kVar, KeyStore keyStore, Properties properties, boolean z) {
        URL url;
        Certificate[] certificateArr;
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        if (kVar.bBX != null) {
            url = new URL(z ? l.b(kVar.bBX, properties) : kVar.bBX);
        } else {
            url = null;
        }
        if (kVar.bBW != null) {
            if (z) {
                kVar.bBW = l.a(kVar.bBW, properties);
            }
            certificateArr = a(keyStore, kVar.bBW);
        } else {
            certificateArr = null;
        }
        if (kVar.bBY != null) {
            for (i iVar : kVar.bBY) {
                if (z) {
                    iVar.name = l.a(iVar.name, properties);
                }
                if (iVar.GA == null) {
                    hashSet.add(b(keyStore, iVar.name));
                } else {
                    hashSet.add(new a(iVar.GA, iVar.name));
                }
            }
        }
        if (kVar.asE != null) {
            for (l a2 : kVar.asE) {
                try {
                    hashSet2.add(a(a2, kVar, keyStore, properties, z));
                } catch (Exception e) {
                }
            }
        }
        return new d(new CodeSource(url, certificateArr), hashSet, hashSet2);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.security.KeyStore a(java.util.List r6, java.net.URL r7, java.util.Properties r8, boolean r9) {
        /*
            r5 = this;
            r4 = 0
            r0 = 0
        L_0x0002:
            int r1 = r6.size()
            if (r0 >= r1) goto L_0x0060
            java.lang.Object r5 = r6.get(r0)     // Catch:{ Exception -> 0x005c }
            a.a.a.c.b r5 = (a.a.a.c.b) r5     // Catch:{ Exception -> 0x005c }
            if (r9 == 0) goto L_0x0024
            java.lang.String r1 = r5.GP     // Catch:{ Exception -> 0x005c }
            java.lang.String r1 = a.a.a.c.h.l.b(r1, r8)     // Catch:{ Exception -> 0x005c }
            r5.GP = r1     // Catch:{ Exception -> 0x005c }
            java.lang.String r1 = r5.type     // Catch:{ Exception -> 0x005c }
            if (r1 == 0) goto L_0x0024
            java.lang.String r1 = r5.type     // Catch:{ Exception -> 0x005c }
            java.lang.String r1 = a.a.a.c.h.l.a(r1, r8)     // Catch:{ Exception -> 0x005c }
            r5.type = r1     // Catch:{ Exception -> 0x005c }
        L_0x0024:
            java.lang.String r1 = r5.type     // Catch:{ Exception -> 0x005c }
            if (r1 == 0) goto L_0x0030
            java.lang.String r1 = r5.type     // Catch:{ Exception -> 0x005c }
            int r1 = r1.length()     // Catch:{ Exception -> 0x005c }
            if (r1 != 0) goto L_0x0036
        L_0x0030:
            java.lang.String r1 = java.security.KeyStore.getDefaultType()     // Catch:{ Exception -> 0x005c }
            r5.type = r1     // Catch:{ Exception -> 0x005c }
        L_0x0036:
            java.lang.String r1 = r5.type     // Catch:{ Exception -> 0x005c }
            java.security.KeyStore r1 = java.security.KeyStore.getInstance(r1)     // Catch:{ Exception -> 0x005c }
            java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x005c }
            java.lang.String r3 = r5.GP     // Catch:{ Exception -> 0x005c }
            r2.<init>(r7, r3)     // Catch:{ Exception -> 0x005c }
            a.a.a.c.h.k r3 = new a.a.a.c.h.k     // Catch:{ Exception -> 0x005c }
            r3.<init>(r2)     // Catch:{ Exception -> 0x005c }
            java.lang.Object r5 = java.security.AccessController.doPrivileged(r3)     // Catch:{ Exception -> 0x005c }
            java.io.InputStream r5 = (java.io.InputStream) r5     // Catch:{ Exception -> 0x005c }
            r2 = 0
            r1.load(r5, r2)     // Catch:{ all -> 0x0057 }
            r5.close()     // Catch:{ Exception -> 0x005c }
            r0 = r1
        L_0x0056:
            return r0
        L_0x0057:
            r1 = move-exception
            r5.close()     // Catch:{ Exception -> 0x005c }
            throw r1     // Catch:{ Exception -> 0x005c }
        L_0x005c:
            r1 = move-exception
            int r0 = r0 + 1
            goto L_0x0002
        L_0x0060:
            r0 = r4
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.c.h.b.a(java.util.List, java.net.URL, java.util.Properties, boolean):java.security.KeyStore");
    }

    /* access modifiers changed from: protected */
    public Permission a(l lVar, k kVar, KeyStore keyStore, Properties properties, boolean z) {
        if (lVar.name != null) {
            lVar.name = l.a(lVar.name, new i(this).a(kVar, keyStore));
        }
        if (z) {
            if (lVar.name != null) {
                lVar.name = l.a(lVar.name, properties);
            }
            if (lVar.bQE != null) {
                lVar.bQE = l.a(lVar.bQE, properties);
            }
            if (lVar.bBW != null) {
                lVar.bBW = l.a(lVar.bBW, properties);
            }
        }
        Certificate[] a2 = lVar.bBW == null ? null : a(keyStore, lVar.bBW);
        try {
            Class<?> cls = Class.forName(lVar.GA);
            if (l.g(a2, cls.getSigners())) {
                return l.a(cls, lVar.name, lVar.bQE);
            }
        } catch (ClassNotFoundException e) {
        }
        return new UnresolvedPermission(lVar.GA, lVar.name, lVar.bQE, a2);
    }

    /* JADX INFO: finally extract failed */
    public Collection a(URL url, Properties properties) {
        boolean Kk = l.Kk();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((InputStream) AccessController.doPrivileged(new k(url))));
        HashSet<k> hashSet = new HashSet<>();
        ArrayList arrayList = new ArrayList();
        try {
            this.uQ.a(bufferedReader, hashSet, arrayList);
            bufferedReader.close();
            KeyStore a2 = a(arrayList, url, properties, Kk);
            HashSet hashSet2 = new HashSet();
            for (k a3 : hashSet) {
                try {
                    d a4 = a(a3, a2, properties, Kk);
                    if (!a4.wN()) {
                        hashSet2.add(a4);
                    }
                } catch (Exception e) {
                }
            }
            return hashSet2;
        } catch (Throwable th) {
            bufferedReader.close();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public Certificate[] a(KeyStore keyStore, String str) {
        if (keyStore == null) {
            throw new KeyStoreException(a.a.a.c.j.a.a.c("security.146", str));
        }
        HashSet hashSet = new HashSet();
        StringTokenizer stringTokenizer = new StringTokenizer(str, cd.bVH);
        while (stringTokenizer.hasMoreTokens()) {
            hashSet.add(keyStore.getCertificate(stringTokenizer.nextToken().trim()));
        }
        return (Certificate[]) hashSet.toArray(new Certificate[hashSet.size()]);
    }

    /* access modifiers changed from: protected */
    public Principal b(KeyStore keyStore, String str) {
        if (keyStore == null) {
            throw new KeyStoreException(a.a.a.c.j.a.a.c("security.147", str));
        }
        Certificate certificate = keyStore.getCertificate(str);
        if (certificate instanceof X509Certificate) {
            return ((X509Certificate) certificate).getSubjectX500Principal();
        }
        throw new CertificateException(a.a.a.c.j.a.a.c("security.148", str, certificate));
    }
}
