package a.a.a.c.c;

import a.a.a.c.a.ag;
import a.a.a.c.a.aj;
import a.a.a.c.a.am;
import a.a.a.c.a.c;
import a.a.a.c.a.u;
import a.a.a.c.a.v;
import java.io.IOException;
import java.util.Arrays;

public class af {
    public static final boolean afZ = true;
    public static final boolean aga = false;
    static final int[] agb = {2, 5, 29, 9};
    static final int[] agc = {2, 5, 29, 14};
    static final int[] agd = {2, 5, 29, 15};
    static final int[] age = {2, 5, 29, 16};
    static final int[] agf = {2, 5, 29, 17};
    static final int[] agg = {2, 5, 29, 18};
    static final int[] agh = {2, 5, 29, 19};
    static final int[] agi = {2, 5, 29, 30};
    static final int[] agj = {2, 5, 29, 31};
    static final int[] agk = {2, 5, 29, 32};
    static final int[] agl = {2, 5, 29, 33};
    static final int[] agm = {2, 5, 29, 35};
    static final int[] agn = {2, 5, 29, 36};
    static final int[] ago = {2, 5, 29, 37};
    static final int[] agp = {2, 5, 29, 46};
    static final int[] agq = {2, 5, 29, 54};
    static final int[] agr = {1, 3, 6, 1, 5, 5, 7, 1, 1};
    static final int[] ags = {1, 3, 6, 1, 5, 5, 7, 1, 11};
    static final int[] agt = {2, 5, 29, 28};
    static final int[] agu = {2, 5, 29, 20};
    static final int[] agv = {2, 5, 29, 29};
    static final int[] agw = {2, 5, 29, 24};
    static final int[] agx = {2, 5, 29, 21};
    static final int[] agy = {2, 5, 29, 28};
    public static final v en = new bt(new am[]{c.bb(), u.kZ(), new bs()});
    private String agA;
    /* access modifiers changed from: private */
    public final boolean agB;
    /* access modifiers changed from: private */
    public final byte[] agC;
    private byte[] agD;
    protected ap agE;
    private boolean agF;
    /* access modifiers changed from: private */
    public final int[] agz;
    private byte[] dV;

    public af(String str, boolean z, ap apVar) {
        this.agF = false;
        this.agA = str;
        this.agz = aj.eD(str);
        this.agB = z;
        this.agE = apVar;
        this.agF = true;
        this.agC = apVar.getEncoded();
    }

    public af(String str, boolean z, byte[] bArr) {
        this.agF = false;
        this.agA = str;
        this.agz = aj.eD(str);
        this.agB = z;
        this.agC = bArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.c.c.af.<init>(java.lang.String, boolean, byte[]):void
     arg types: [java.lang.String, int, byte[]]
     candidates:
      a.a.a.c.c.af.<init>(java.lang.String, boolean, a.a.a.c.c.ap):void
      a.a.a.c.c.af.<init>(int[], boolean, byte[]):void
      a.a.a.c.c.af.<init>(java.lang.String, boolean, byte[]):void */
    public af(String str, byte[] bArr) {
        this(str, false, bArr);
    }

    public af(int[] iArr, boolean z, byte[] bArr) {
        this.agF = false;
        this.agz = iArr;
        this.agB = z;
        this.agC = bArr;
    }

    private af(int[] iArr, boolean z, byte[] bArr, byte[] bArr2, byte[] bArr3, ap apVar) {
        this(iArr, z, bArr);
        this.agD = bArr2;
        this.dV = bArr3;
        this.agE = apVar;
        this.agF = apVar != null;
    }

    /* synthetic */ af(int[] iArr, boolean z, byte[] bArr, byte[] bArr2, byte[] bArr3, ap apVar, bs bsVar) {
        this(iArr, z, bArr, bArr2, bArr3, apVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.c.c.af.<init>(int[], boolean, byte[]):void
     arg types: [int[], int, byte[]]
     candidates:
      a.a.a.c.c.af.<init>(java.lang.String, boolean, a.a.a.c.c.ap):void
      a.a.a.c.c.af.<init>(java.lang.String, boolean, byte[]):void
      a.a.a.c.c.af.<init>(int[], boolean, byte[]):void */
    public af(int[] iArr, byte[] bArr) {
        this(iArr, false, bArr);
    }

    /* access modifiers changed from: private */
    public static boolean a(int[] iArr, int[] iArr2) {
        int length = iArr.length;
        if (length != iArr2.length) {
            return false;
        }
        while (length > 0) {
            length--;
            if (iArr[length] != iArr2[length]) {
                return false;
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.c.c.b.<init>(boolean, byte[]):void
     arg types: [int, byte[]]
     candidates:
      a.a.a.c.c.b.<init>(boolean, a.a.a.c.c.bl):void
      a.a.a.c.c.b.<init>(boolean, byte[]):void */
    private void vh() {
        if (!this.agF) {
            this.agF = true;
            if (a(this.agz, agc)) {
                this.agE = s.M(this.agC);
            } else if (a(this.agz, agd)) {
                this.agE = new bv(this.agC);
            } else if (a(this.agz, agf)) {
                this.agE = new b(true, this.agC);
            } else if (a(this.agz, agg)) {
                this.agE = new b(true, this.agC);
            } else if (a(this.agz, agh)) {
                this.agE = new ai(this.agC);
            } else if (a(this.agz, agi)) {
                this.agE = bh.av(this.agC);
            } else if (a(this.agz, agk)) {
                this.agE = q.K(this.agC);
            } else if (a(this.agz, agm)) {
                this.agE = r.L(this.agC);
            } else if (a(this.agz, agn)) {
                this.agE = new u(this.agC);
            } else if (a(this.agz, ago)) {
                this.agE = new bj(this.agC);
            } else if (a(this.agz, agq)) {
                this.agE = new av(this.agC);
            } else if (a(this.agz, agv)) {
                this.agE = new e(this.agC);
            } else if (a(this.agz, agj)) {
                this.agE = ce.aR(this.agC);
            } else if (a(this.agz, agv)) {
                this.agE = new n(this.agC);
            } else if (a(this.agz, agw)) {
                this.agE = new bu(this.agC);
            } else if (a(this.agz, agx)) {
                this.agE = new n(this.agC);
            } else if (a(this.agz, agu)) {
                this.agE = new w(this.agC);
            } else if (a(this.agz, agy)) {
                this.agE = cd.aQ(this.agC);
            } else if (a(this.agz, agr)) {
                this.agE = bd.Y(this.agC);
            } else if (a(this.agz, ags)) {
                this.agE = bd.Y(this.agC);
            }
        }
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append("OID: ").append(va()).append(", Critical: ").append(this.agB).append(10);
        if (!this.agF) {
            try {
                vh();
            } catch (IOException e) {
            }
        }
        if (this.agE != null) {
            this.agE.a(stringBuffer, str);
            return;
        }
        stringBuffer.append(str);
        if (a(this.agz, agb)) {
            stringBuffer.append("Subject Directory Attributes Extension");
        } else if (a(this.agz, agc)) {
            stringBuffer.append("Subject Key Identifier Extension");
        } else if (a(this.agz, agd)) {
            stringBuffer.append("Key Usage Extension");
        } else if (a(this.agz, age)) {
            stringBuffer.append("Private Key Usage Period Extension");
        } else if (a(this.agz, agf)) {
            stringBuffer.append("Subject Alternative Name Extension");
        } else if (a(this.agz, agg)) {
            stringBuffer.append("Issuer Alternative Name Extension");
        } else if (a(this.agz, agh)) {
            stringBuffer.append("Basic Constraints Extension");
        } else if (a(this.agz, agi)) {
            stringBuffer.append("Name Constraints Extension");
        } else if (a(this.agz, agj)) {
            stringBuffer.append("CRL Distribution Points Extension");
        } else if (a(this.agz, agk)) {
            stringBuffer.append("Certificate Policies Extension");
        } else if (a(this.agz, agl)) {
            stringBuffer.append("Policy Mappings Extension");
        } else if (a(this.agz, agm)) {
            stringBuffer.append("Authority Key Identifier Extension");
        } else if (a(this.agz, agn)) {
            stringBuffer.append("Policy Constraints Extension");
        } else if (a(this.agz, ago)) {
            stringBuffer.append("Extended Key Usage Extension");
        } else if (a(this.agz, agq)) {
            stringBuffer.append("Inhibit Any-Policy Extension");
        } else if (a(this.agz, agr)) {
            stringBuffer.append("Authority Information Access Extension");
        } else if (a(this.agz, ags)) {
            stringBuffer.append("Subject Information Access Extension");
        } else if (a(this.agz, agw)) {
            stringBuffer.append("Invalidity Date Extension");
        } else if (a(this.agz, agu)) {
            stringBuffer.append("CRL Number Extension");
        } else if (a(this.agz, agx)) {
            stringBuffer.append("Reason Code Extension");
        } else {
            stringBuffer.append("Unknown Extension");
        }
        stringBuffer.append(10).append(str).append("Unparsed Extension Value:\n");
        stringBuffer.append(a.a.a.c.b.c.d(this.agC, str));
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof af)) {
            return false;
        }
        af afVar = (af) obj;
        return Arrays.equals(this.agz, afVar.agz) && this.agB == afVar.agB && Arrays.equals(this.agC, afVar.agC);
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public int hashCode() {
        return (((this.agz.hashCode() * 37) + (this.agB ? 1 : 0)) * 37) + this.agC.hashCode();
    }

    public String va() {
        if (this.agA == null) {
            this.agA = aj.toString(this.agz);
        }
        return this.agA;
    }

    public boolean vb() {
        return this.agB;
    }

    public byte[] vc() {
        return this.agC;
    }

    public byte[] vd() {
        if (this.agD == null) {
            this.agD = ag.Bl().S(this.agC);
        }
        return this.agD;
    }

    public ap ve() {
        if (!this.agF) {
            vh();
        }
        return this.agE;
    }

    public bv vf() {
        if (!this.agF) {
            try {
                vh();
            } catch (IOException e) {
            }
        }
        if (this.agE instanceof bv) {
            return (bv) this.agE;
        }
        return null;
    }

    public ai vg() {
        if (!this.agF) {
            try {
                vh();
            } catch (IOException e) {
            }
        }
        if (this.agE instanceof ai) {
            return (ai) this.agE;
        }
        return null;
    }
}
