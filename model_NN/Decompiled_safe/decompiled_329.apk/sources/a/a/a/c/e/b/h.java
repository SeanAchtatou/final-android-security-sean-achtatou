package a.a.a.c.e.b;

import java.security.AccessController;
import java.security.Provider;

public final class h extends Provider {
    private static final long serialVersionUID = 7991202868423459598L;

    public h() {
        super("Crypto", 1.0d, "HARMONY (SHA1 digest; SecureRandom; SHA1withDSA signature)");
        AccessController.doPrivileged(new j(this));
    }
}
