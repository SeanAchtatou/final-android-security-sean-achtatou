package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.c;
import a.a.a.c.a.r;
import a.a.a.c.a.v;

public class bm {
    public static final v en = new cb(new am[]{c.bb(), r.iS()});
    /* access modifiers changed from: private */
    public String boC;
    private byte[] dV;

    public bm(String str) {
        this.boC = str;
    }

    public String Fb() {
        return this.boC;
    }

    public void b(StringBuffer stringBuffer) {
        stringBuffer.append("Policy Identifier [").append(this.boC).append(']');
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }
}
