package a.a.a.c.e.b;

import a.a.a.c.j.a.a;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactorySpi;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.DSAParams;
import java.security.interfaces.DSAPrivateKey;
import java.security.interfaces.DSAPublicKey;
import java.security.spec.DSAPrivateKeySpec;
import java.security.spec.DSAPublicKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class e extends KeyFactorySpi {
    /* access modifiers changed from: protected */
    public PrivateKey engineGeneratePrivate(KeySpec keySpec) {
        if (keySpec != null) {
            if (keySpec instanceof DSAPrivateKeySpec) {
                return new d((DSAPrivateKeySpec) keySpec);
            }
            if (keySpec instanceof PKCS8EncodedKeySpec) {
                return new d((PKCS8EncodedKeySpec) keySpec);
            }
        }
        throw new InvalidKeySpecException(a.getString("security.19C"));
    }

    /* access modifiers changed from: protected */
    public PublicKey engineGeneratePublic(KeySpec keySpec) {
        if (keySpec != null) {
            if (keySpec instanceof DSAPublicKeySpec) {
                return new g((DSAPublicKeySpec) keySpec);
            }
            if (keySpec instanceof X509EncodedKeySpec) {
                return new g((X509EncodedKeySpec) keySpec);
            }
        }
        throw new InvalidKeySpecException(a.getString("security.19D"));
    }

    /* access modifiers changed from: protected */
    public KeySpec engineGetKeySpec(Key key, Class cls) {
        if (key != null) {
            if (cls == null) {
                throw new NullPointerException(a.getString("security.19E"));
            } else if (key instanceof DSAPrivateKey) {
                DSAPrivateKey dSAPrivateKey = (DSAPrivateKey) key;
                if (cls.equals(DSAPrivateKeySpec.class)) {
                    BigInteger x = dSAPrivateKey.getX();
                    DSAParams params = dSAPrivateKey.getParams();
                    return new DSAPrivateKeySpec(x, params.getP(), params.getQ(), params.getG());
                } else if (cls.equals(PKCS8EncodedKeySpec.class)) {
                    return new PKCS8EncodedKeySpec(key.getEncoded());
                } else {
                    throw new InvalidKeySpecException(a.getString("security.19C"));
                }
            } else if (key instanceof DSAPublicKey) {
                DSAPublicKey dSAPublicKey = (DSAPublicKey) key;
                if (cls.equals(DSAPublicKeySpec.class)) {
                    BigInteger y = dSAPublicKey.getY();
                    DSAParams params2 = dSAPublicKey.getParams();
                    return new DSAPublicKeySpec(y, params2.getP(), params2.getQ(), params2.getG());
                } else if (cls.equals(X509EncodedKeySpec.class)) {
                    return new X509EncodedKeySpec(key.getEncoded());
                } else {
                    throw new InvalidKeySpecException(a.getString("security.19D"));
                }
            }
        }
        throw new InvalidKeySpecException(a.getString("security.19F"));
    }

    /* access modifiers changed from: protected */
    public Key engineTranslateKey(Key key) {
        if (key != null) {
            if (key instanceof DSAPrivateKey) {
                DSAPrivateKey dSAPrivateKey = (DSAPrivateKey) key;
                DSAParams params = dSAPrivateKey.getParams();
                try {
                    return engineGeneratePrivate(new DSAPrivateKeySpec(dSAPrivateKey.getX(), params.getP(), params.getQ(), params.getG()));
                } catch (InvalidKeySpecException e) {
                    throw new InvalidKeyException(a.c("security.1A0", e));
                }
            } else if (key instanceof DSAPublicKey) {
                DSAPublicKey dSAPublicKey = (DSAPublicKey) key;
                DSAParams params2 = dSAPublicKey.getParams();
                try {
                    return engineGeneratePublic(new DSAPublicKeySpec(dSAPublicKey.getY(), params2.getP(), params2.getQ(), params2.getG()));
                } catch (InvalidKeySpecException e2) {
                    throw new InvalidKeyException(a.c("security.1A1", e2));
                }
            }
        }
        throw new InvalidKeyException(a.getString("security.19F"));
    }
}
