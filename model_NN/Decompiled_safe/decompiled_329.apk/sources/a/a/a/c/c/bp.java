package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.p;
import a.a.a.c.a.v;
import a.a.a.c.b.c;

public class bp {
    public static final v en = new bb(new am[]{m.en, al.en, p.fB()});
    /* access modifiers changed from: private */
    public final byte[] BA;
    /* access modifiers changed from: private */
    public final al Bz;
    /* access modifiers changed from: private */
    public final m btY;
    private byte[] dV;

    public bp(m mVar, al alVar, byte[] bArr) {
        this.btY = mVar;
        this.Bz = alVar;
        this.BA = new byte[bArr.length];
        System.arraycopy(bArr, 0, this.BA, 0, bArr.length);
    }

    private bp(m mVar, al alVar, byte[] bArr, byte[] bArr2) {
        this(mVar, alVar, bArr);
        this.dV = bArr2;
    }

    /* synthetic */ bp(m mVar, al alVar, byte[] bArr, byte[] bArr2, bb bbVar) {
        this(mVar, alVar, bArr, bArr2);
    }

    public m FO() {
        return this.btY;
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public al iP() {
        return this.Bz;
    }

    public byte[] iQ() {
        byte[] bArr = new byte[this.BA.length];
        System.arraycopy(this.BA, 0, bArr, 0, this.BA.length);
        return bArr;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("X.509 Certificate:\n[\n");
        this.btY.b(stringBuffer);
        stringBuffer.append("\n  Algorithm: [");
        this.Bz.b(stringBuffer);
        stringBuffer.append(']');
        stringBuffer.append("\n  Signature Value:\n");
        stringBuffer.append(c.d(this.BA, ""));
        stringBuffer.append(']');
        return stringBuffer.toString();
    }
}
