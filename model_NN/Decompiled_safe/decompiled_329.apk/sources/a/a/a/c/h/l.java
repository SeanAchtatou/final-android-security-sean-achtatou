package a.a.a.c.h;

import a.a.a.c.j;
import a.a.a.c.j.a.a;
import com.uc.c.au;
import java.io.File;
import java.net.URI;
import java.net.URL;
import java.security.AccessController;
import java.security.Permission;
import java.security.PermissionCollection;
import java.security.Permissions;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

public class l {
    public static final String FALSE = "false";
    public static final String TRUE = "true";
    public static final String bES = "policy.allowSystemProperty";
    public static final String bET = "policy.expandProperties";
    private static final Class[] bEU = new Class[0];
    private static final Class[] bEV;
    private static final Class[] bEW;

    static {
        Class<String> cls = String.class;
        Class<String> cls2 = String.class;
        bEV = new Class[]{cls};
        Class<String> cls3 = String.class;
        Class<String> cls4 = String.class;
        bEW = new Class[]{cls, cls};
    }

    private l() {
    }

    public static boolean Kk() {
        return !j.aa(FALSE, (String) AccessController.doPrivileged(new m(bET)));
    }

    public static String a(String str, j jVar) {
        int length = "${{".length();
        int length2 = "}}".length();
        StringBuilder sb = new StringBuilder(str);
        int indexOf = sb.indexOf("${{");
        while (indexOf >= 0) {
            int indexOf2 = sb.indexOf("}}", indexOf);
            if (indexOf2 >= 0) {
                String substring = sb.substring(indexOf + length, indexOf2);
                int indexOf3 = substring.indexOf(58);
                String T = jVar.T(indexOf3 >= 0 ? substring.substring(0, indexOf3) : substring, indexOf3 >= 0 ? substring.substring(indexOf3 + 1) : null);
                sb.replace(indexOf, indexOf2 + length2, T);
                indexOf += T.length();
            }
            indexOf = sb.indexOf("${{", indexOf);
        }
        return sb.toString();
    }

    public static String a(String str, Properties properties) {
        int length = "${".length();
        int length2 = "}".length();
        StringBuilder sb = new StringBuilder(str);
        int indexOf = sb.indexOf("${");
        while (indexOf >= 0) {
            int indexOf2 = sb.indexOf("}", indexOf);
            if (indexOf2 >= 0) {
                String substring = sb.substring(indexOf + length, indexOf2);
                String property = properties.getProperty(substring);
                if (property != null) {
                    sb.replace(indexOf, indexOf2 + length2, property);
                    indexOf += property.length();
                } else {
                    throw new e(a.c("security.14F", substring));
                }
            }
            indexOf = sb.indexOf("${", indexOf);
        }
        return sb.toString();
    }

    public static URL a(URL url) {
        if (url != null && "file".equals(url.getProtocol())) {
            try {
                if (url.getHost().length() != 0) {
                    return url.toURI().normalize().toURL();
                }
                String file = url.getFile();
                if (file.length() == 0) {
                    file = "*";
                }
                return fi(new File(file).getAbsolutePath()).normalize().toURL();
            } catch (Exception e) {
            }
        }
        return url;
    }

    public static Permission a(Class cls, String str, String str2) {
        Class[][] clsArr;
        Object[][] objArr;
        int i = 0;
        if (str2 != null) {
            objArr = new Object[][]{new Object[]{str, str2}, new Object[]{str}, new Object[0]};
            clsArr = new Class[][]{bEW, bEV, bEU};
        } else if (str != null) {
            objArr = new Object[][]{new Object[]{str}, new Object[]{str, str2}, new Object[0]};
            clsArr = new Class[][]{bEV, bEW, bEU};
        } else {
            objArr = new Object[][]{new Object[0], new Object[]{str}, new Object[]{str, str2}};
            clsArr = new Class[][]{bEU, bEV, bEW};
        }
        while (i < clsArr.length) {
            try {
                return (Permission) cls.getConstructor(clsArr[i]).newInstance(objArr[i]);
            } catch (NoSuchMethodException e) {
                i++;
            }
        }
        throw new IllegalArgumentException(a.c("security.150", cls));
    }

    public static PermissionCollection a(Collection collection) {
        Permissions permissions = new Permissions();
        if (collection != null) {
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                permissions.add((Permission) it.next());
            }
        }
        return permissions;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0073  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.net.URL[] a(java.util.Properties r9, java.lang.String r10, java.lang.String r11) {
        /*
            r7 = 0
            r6 = 1
            a.a.a.c.h.m r1 = new a.a.a.c.h.m
            r1.<init>(r7)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r3 = 0
            java.lang.String r4 = "false"
            java.lang.String r0 = "policy.allowSystemProperty"
            java.security.PrivilegedAction r0 = r1.aA(r0)
            java.lang.Object r0 = java.security.AccessController.doPrivileged(r0)
            java.lang.String r0 = (java.lang.String) r0
            boolean r0 = a.a.a.c.j.aa(r4, r0)
            if (r0 != 0) goto L_0x009d
            java.lang.String r0 = r9.getProperty(r10)
            if (r0 == 0) goto L_0x009d
            java.lang.String r4 = "="
            boolean r4 = r0.startsWith(r4)
            if (r4 == 0) goto L_0x0034
            java.lang.String r0 = r0.substring(r6)
            r3 = r6
        L_0x0034:
            java.lang.String r4 = b(r0, r9)     // Catch:{ Exception -> 0x0083 }
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0083 }
            r0.<init>(r4)     // Catch:{ Exception -> 0x0083 }
            a.a.a.c.h.c r5 = new a.a.a.c.h.c     // Catch:{ Exception -> 0x0083 }
            r5.<init>(r0)     // Catch:{ Exception -> 0x0083 }
            java.lang.Object r0 = java.security.AccessController.doPrivileged(r5)     // Catch:{ Exception -> 0x0083 }
            java.net.URL r0 = (java.net.URL) r0     // Catch:{ Exception -> 0x0083 }
            if (r0 != 0) goto L_0x0050
            java.net.URL r5 = new java.net.URL     // Catch:{ Exception -> 0x009b }
            r5.<init>(r4)     // Catch:{ Exception -> 0x009b }
            r0 = r5
        L_0x0050:
            r8 = r0
            r0 = r3
            r3 = r8
        L_0x0053:
            if (r0 != 0) goto L_0x0071
            r0 = r6
        L_0x0056:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>(r11)
            int r5 = r0 + 1
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r0 = r0.toString()
            java.security.PrivilegedAction r0 = r1.aA(r0)
            java.lang.Object r0 = java.security.AccessController.doPrivileged(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 != 0) goto L_0x0089
        L_0x0071:
            if (r3 == 0) goto L_0x0076
            r2.add(r3)
        L_0x0076:
            int r0 = r2.size()
            java.net.URL[] r0 = new java.net.URL[r0]
            java.lang.Object[] r9 = r2.toArray(r0)
            java.net.URL[] r9 = (java.net.URL[]) r9
            return r9
        L_0x0083:
            r0 = move-exception
            r0 = r7
        L_0x0085:
            r8 = r0
            r0 = r3
            r3 = r8
            goto L_0x0053
        L_0x0089:
            java.lang.String r0 = b(r0, r9)     // Catch:{ Exception -> 0x0099 }
            java.net.URL r4 = new java.net.URL     // Catch:{ Exception -> 0x0099 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0099 }
            if (r4 == 0) goto L_0x0097
            r2.add(r4)     // Catch:{ Exception -> 0x0099 }
        L_0x0097:
            r0 = r5
            goto L_0x0056
        L_0x0099:
            r0 = move-exception
            goto L_0x0097
        L_0x009b:
            r4 = move-exception
            goto L_0x0085
        L_0x009d:
            r0 = r3
            r3 = r7
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.c.h.l.a(java.util.Properties, java.lang.String, java.lang.String):java.net.URL[]");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [char, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static String b(String str, Properties properties) {
        return a(str, properties).replace(File.separatorChar, '/');
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [char, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static URI fi(String str) {
        String replace = str.replace(File.separatorChar, '/');
        return !replace.startsWith(au.aGF) ? new URI("file", null, new StringBuilder(replace.length() + 1).append('/').append(replace).toString(), null, null) : new URI("file", null, replace, null, null);
    }

    public static boolean g(Object[] objArr, Object[] objArr2) {
        boolean z;
        if (objArr == null) {
            return true;
        }
        for (int i = 0; i < objArr.length; i++) {
            if (objArr[i] != null) {
                if (objArr2 == null) {
                    return false;
                }
                int i2 = 0;
                while (true) {
                    if (i2 >= objArr2.length) {
                        z = false;
                        break;
                    } else if (objArr[i].equals(objArr2[i2])) {
                        z = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (!z) {
                    return false;
                }
            }
        }
        return true;
    }
}
