package a.a.a.c.a;

import a.a.a.c.j.a.a;

public class ao extends p {
    private static final byte[] BG = {Byte.MIN_VALUE, 64, 32, 16, 8, 4, 2, 1};
    private static final q bPi = new q(new byte[0], 0);
    private static final int bPj = -1;
    private final int bPk;
    private final int bPl;

    public ao() {
        this.bPk = -1;
        this.bPl = -1;
    }

    public ao(int i) {
        this.bPk = i;
        this.bPl = -1;
    }

    public ao(int i, int i2) {
        this.bPk = i;
        this.bPl = i2;
    }

    public Object b(ad adVar) {
        boolean[] zArr;
        byte b2 = adVar.buffer[adVar.auY];
        int i = ((adVar.length - 1) * 8) - b2;
        if (this.bPl == -1) {
            zArr = this.bPk == -1 ? new boolean[i] : i > this.bPk ? new boolean[i] : new boolean[this.bPk];
        } else if (i > this.bPl) {
            throw new ak(a.getString("security.97"));
        } else {
            zArr = new boolean[this.bPl];
        }
        if (i == 0) {
            return zArr;
        }
        byte b3 = adVar.buffer[adVar.auY + 1];
        int i2 = adVar.length - 1;
        int i3 = 0;
        int i4 = 1;
        while (i4 < i2) {
            int i5 = i3;
            int i6 = 0;
            while (i6 < 8) {
                zArr[i5] = (BG[i6] & b3) != 0;
                i6++;
                i5++;
            }
            int i7 = i4 + 1;
            i4 = i7 + 1;
            b3 = adVar.buffer[adVar.auY + i7];
            i3 = i5;
        }
        int i8 = 0;
        while (i8 < 8 - b2) {
            zArr[i3] = (BG[i8] & b3) != 0;
            i8++;
            i3++;
        }
        return zArr;
    }

    public void b(at atVar) {
        boolean[] zArr = (boolean[]) atVar.auW;
        int length = zArr.length - 1;
        while (length > -1 && !zArr[length]) {
            length--;
        }
        if (length == -1) {
            atVar.auW = bPi;
            atVar.length = 1;
            return;
        }
        int i = 7 - (length % 8);
        byte[] bArr = new byte[((length / 8) + 1)];
        int length2 = bArr.length - 1;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length2) {
            int i4 = i3;
            int i5 = 0;
            while (i5 < 8) {
                if (zArr[i4]) {
                    bArr[i2] = (byte) (bArr[i2] | BG[i5]);
                }
                i5++;
                i4++;
            }
            i2++;
            i3 = i4;
        }
        int i6 = 0;
        while (i6 < 8 - i) {
            if (zArr[i3]) {
                bArr[length2] = (byte) (bArr[length2] | BG[i6]);
            }
            i6++;
            i3++;
        }
        atVar.auW = new q(bArr, i);
        atVar.length = bArr.length + 1;
    }
}
