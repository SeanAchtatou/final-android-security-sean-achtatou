package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.c;
import a.a.a.c.a.r;
import a.a.a.c.a.v;
import a.a.a.c.b.b;
import java.util.Arrays;

public class al {
    public static final v en = new am(new am[]{c.bb(), r.iS()});
    private String avj;
    private byte[] avk;
    private byte[] dV;
    private String df;

    public al(String str) {
        this(str, null, null);
    }

    public al(String str, byte[] bArr) {
        this(str, bArr, null);
    }

    private al(String str, byte[] bArr, byte[] bArr2) {
        this.df = str;
        this.avk = bArr;
        this.dV = bArr2;
    }

    public void b(StringBuffer stringBuffer) {
        stringBuffer.append(xy());
        if (this.avk == null) {
            stringBuffer.append(", no params, ");
        } else {
            stringBuffer.append(", params unparsed, ");
        }
        stringBuffer.append("OID = ");
        stringBuffer.append(getAlgorithm());
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof al)) {
            return false;
        }
        al alVar = (al) obj;
        return this.df.equals(alVar.df) && (this.avk != null ? Arrays.equals(this.avk, alVar.avk) : alVar.avk == null);
    }

    public String getAlgorithm() {
        return this.df;
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public int hashCode() {
        return (this.df.hashCode() * 37) + (this.avk != null ? this.avk.hashCode() : 0);
    }

    public String xy() {
        if (this.avj == null) {
            this.avj = b.am(this.df);
            if (this.avj == null) {
                this.avj = this.df;
            }
        }
        return this.avj;
    }

    public byte[] xz() {
        return this.avk;
    }
}
