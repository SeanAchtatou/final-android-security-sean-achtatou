package a.a.a.c.c;

import a.a.a.c.a.am;
import java.util.ArrayList;
import java.util.List;

public class bl {
    public static final am lG = new j(be.Yv);
    /* access modifiers changed from: private */
    public List bny;
    private byte[] dV;

    public bl() {
        this.bny = new ArrayList();
    }

    public bl(List list) {
        this.bny = list;
    }

    private bl(List list, byte[] bArr) {
        this.bny = list;
        this.dV = bArr;
    }

    /* synthetic */ bl(List list, byte[] bArr, j jVar) {
        this(list, bArr);
    }

    public List EL() {
        if (this.bny == null || this.bny.size() == 0) {
            return null;
        }
        return new ArrayList(this.bny);
    }

    public List EM() {
        ArrayList arrayList = new ArrayList();
        if (this.bny != null) {
            for (be Ci : this.bny) {
                arrayList.add(Ci.Ci());
            }
        }
        return arrayList;
    }

    public void a(StringBuffer stringBuffer, String str) {
        if (this.bny != null) {
            for (Object append : this.bny) {
                stringBuffer.append(str);
                stringBuffer.append(append);
                stringBuffer.append(10);
            }
        }
    }

    public void d(be beVar) {
        this.dV = null;
        if (this.bny == null) {
            this.bny = new ArrayList();
        }
        this.bny.add(beVar);
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = lG.S(this);
        }
        return this.dV;
    }
}
