package a.a.a.c.c;

import java.util.List;

public class b extends ap {
    public static final boolean dm = false;
    public static final boolean dn = true;

    /* renamed from: do  reason: not valid java name */
    private boolean f0do;
    private bl dp;

    public b(boolean z, bl blVar) {
        this.f0do = z;
        this.dp = blVar;
    }

    public b(boolean z, byte[] bArr) {
        super(bArr);
        this.f0do = z;
        this.dp = (bl) bl.lG.ax(bArr);
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append(this.f0do ? "Subject" : "Issuer").append(" Alternative Names [\n");
        this.dp.a(stringBuffer, str + "  ");
        stringBuffer.append(str).append("]\n");
    }

    public List cb() {
        return this.dp.EM();
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = bl.lG.S(this.dp);
        }
        return this.dV;
    }
}
