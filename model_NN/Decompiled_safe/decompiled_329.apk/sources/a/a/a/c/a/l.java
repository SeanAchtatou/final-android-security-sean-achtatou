package a.a.a.c.a;

import com.uc.c.ca;
import java.io.UnsupportedEncodingException;

final class l extends ah {
    l(int i) {
        super(i);
    }

    public Object b(ad adVar) {
        return new String(adVar.buffer, adVar.auY, adVar.length, ca.bNE);
    }

    public void b(at atVar) {
        try {
            byte[] bytes = ((String) atVar.auW).getBytes(ca.bNE);
            atVar.auW = bytes;
            atVar.length = bytes.length;
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
