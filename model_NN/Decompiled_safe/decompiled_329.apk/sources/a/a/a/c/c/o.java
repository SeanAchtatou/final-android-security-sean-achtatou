package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.p;
import a.a.a.c.a.v;
import a.a.a.c.b.c;

public class o {
    public static final v en = new d(new am[]{ao.en, al.en, p.fB()});
    /* access modifiers changed from: private */
    public final byte[] BA;
    /* access modifiers changed from: private */
    public final al Bz;
    /* access modifiers changed from: private */
    public final ao dL;
    private byte[] dV;

    public o(ao aoVar, al alVar, byte[] bArr) {
        this.dL = aoVar;
        this.Bz = alVar;
        this.BA = new byte[bArr.length];
        System.arraycopy(bArr, 0, this.BA, 0, bArr.length);
    }

    private o(ao aoVar, al alVar, byte[] bArr, byte[] bArr2) {
        this(aoVar, alVar, bArr);
        this.dV = bArr2;
    }

    /* synthetic */ o(ao aoVar, al alVar, byte[] bArr, byte[] bArr2, d dVar) {
        this(aoVar, alVar, bArr, bArr2);
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public ao iO() {
        return this.dL;
    }

    public al iP() {
        return this.Bz;
    }

    public byte[] iQ() {
        byte[] bArr = new byte[this.BA.length];
        System.arraycopy(this.BA, 0, bArr, 0, this.BA.length);
        return bArr;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        this.dL.b(stringBuffer);
        stringBuffer.append("\nSignature Value:\n");
        stringBuffer.append(c.d(this.BA, ""));
        return stringBuffer.toString();
    }
}
