package a.a.a.c.e.a;

import a.a.a.c.c.af;
import a.a.a.c.c.bn;
import a.a.a.c.c.br;
import java.math.BigInteger;
import java.security.cert.X509CRLEntry;
import java.util.Date;
import java.util.Set;
import javax.security.auth.x500.X500Principal;

public class f extends X509CRLEntry {
    private final br dN;
    private final X500Principal dO;
    private byte[] dV;
    private final bn tS;

    public f(bn bnVar, X500Principal x500Principal) {
        this.tS = bnVar;
        this.dN = bnVar.FM();
        this.dO = x500Principal;
    }

    public X500Principal getCertificateIssuer() {
        return this.dO;
    }

    public Set getCriticalExtensionOIDs() {
        if (this.dN == null) {
            return null;
        }
        return this.dN.Gx();
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = this.tS.getEncoded();
        }
        byte[] bArr = new byte[this.dV.length];
        System.arraycopy(this.dV, 0, bArr, 0, this.dV.length);
        return bArr;
    }

    public byte[] getExtensionValue(String str) {
        if (this.dN == null) {
            return null;
        }
        af eN = this.dN.eN(str);
        if (eN == null) {
            return null;
        }
        return eN.vd();
    }

    public Set getNonCriticalExtensionOIDs() {
        if (this.dN == null) {
            return null;
        }
        return this.dN.Gy();
    }

    public Date getRevocationDate() {
        return this.tS.getRevocationDate();
    }

    public BigInteger getSerialNumber() {
        return this.tS.FN();
    }

    public boolean hasExtensions() {
        return (this.dN == null || this.dN.size() == 0) ? false : true;
    }

    public boolean hasUnsupportedCriticalExtension() {
        if (this.dN == null) {
            return false;
        }
        return this.dN.Gz();
    }

    public String toString() {
        return "X509CRLEntryImpl: " + this.tS.toString();
    }
}
