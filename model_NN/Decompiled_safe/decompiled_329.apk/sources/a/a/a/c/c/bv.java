package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.ao;

public class bv extends ap {
    private static final String[] bxh = {"digitalSignature", "nonRepudiation", "keyEncipherment", "dataEncipherment", "keyAgreement", "keyCertSign", "cRLSign", "encipherOnly", "decipherOnly"};
    private static final am lG = new ao(9);
    private final boolean[] bxi;

    public bv(byte[] bArr) {
        super(bArr);
        this.bxi = (boolean[]) lG.ax(bArr);
    }

    public bv(boolean[] zArr) {
        this.bxi = zArr;
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("KeyUsage [\n");
        for (int i = 0; i < this.bxi.length; i++) {
            if (this.bxi[i]) {
                stringBuffer.append(str).append("  ").append(bxh[i]).append(10);
            }
        }
        stringBuffer.append(str).append("]\n");
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = lG.S(this.bxi);
        }
        return this.dV;
    }

    public boolean[] getKeyUsage() {
        return this.bxi;
    }
}
