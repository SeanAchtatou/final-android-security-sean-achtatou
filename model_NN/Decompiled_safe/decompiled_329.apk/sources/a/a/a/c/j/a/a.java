package a.a.a.c.j.a;

public class a {
    public static String a(String str, Object[] objArr) {
        return format(str, objArr);
    }

    public static String b(String str, char c) {
        return a(str, new Object[]{String.valueOf(c)});
    }

    public static String c(String str, Object obj) {
        return a(str, new Object[]{obj});
    }

    public static String c(String str, Object obj, Object obj2) {
        return a(str, new Object[]{obj, obj2});
    }

    static String format(String str, Object[] objArr) {
        int i;
        StringBuilder sb = new StringBuilder(str.length() + (objArr.length * 20));
        String[] strArr = new String[objArr.length];
        for (int i2 = 0; i2 < objArr.length; i2++) {
            if (objArr[i2] == null) {
                strArr[i2] = "<null>";
            } else {
                strArr[i2] = objArr[i2].toString();
            }
        }
        int indexOf = str.indexOf(123, 0);
        int i3 = 0;
        while (indexOf >= 0) {
            if (indexOf != 0 && str.charAt(indexOf - 1) == '\\') {
                if (indexOf != 1) {
                    sb.append(str.substring(i3, indexOf - 1));
                }
                sb.append('{');
                i = indexOf + 1;
            } else if (indexOf > str.length() - 3) {
                sb.append(str.substring(i3, str.length()));
                i = str.length();
            } else {
                byte digit = (byte) Character.digit(str.charAt(indexOf + 1), 10);
                if (digit < 0 || str.charAt(indexOf + 2) != '}') {
                    sb.append(str.substring(i3, indexOf + 1));
                    i = indexOf + 1;
                } else {
                    sb.append(str.substring(i3, indexOf));
                    if (digit >= strArr.length) {
                        sb.append("<missing argument>");
                    } else {
                        sb.append(strArr[digit]);
                    }
                    i = indexOf + 3;
                }
            }
            i3 = i;
            indexOf = str.indexOf(123, i);
        }
        if (i3 < str.length()) {
            sb.append(str.substring(i3, str.length()));
        }
        return sb.toString();
    }

    public static String getString(String str) {
        return str;
    }

    public static String q(String str, int i) {
        return a(str, new Object[]{Integer.toString(i)});
    }
}
