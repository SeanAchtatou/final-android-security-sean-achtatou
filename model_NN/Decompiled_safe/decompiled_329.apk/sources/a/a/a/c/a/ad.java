package a.a.a.c.a;

import a.a.a.c.j.a.a;
import com.uc.c.r;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class ad {
    private static final int auU = 16384;
    protected static final int auV = -1;
    public Object auW;
    protected int auX;
    protected int auY;
    public int auZ;
    public int[] ava;
    public int avb;
    protected boolean avc;
    protected boolean avd;
    private Object[][] ave;
    protected byte[] buffer;
    protected InputStream in;
    protected int length;
    protected int pG;
    public int tag;

    public ad(InputStream inputStream) {
        this(inputStream, auU);
    }

    public ad(InputStream inputStream, int i) {
        this.pG = 0;
        this.in = inputStream;
        this.buffer = new byte[i];
        next();
        if (this.length == -1) {
            this.avd = true;
            throw new ak(a.getString("security.112"));
        } else if (this.buffer.length < this.length + this.pG) {
            byte[] bArr = new byte[(this.length + this.pG)];
            System.arraycopy(this.buffer, 0, bArr, 0, this.pG);
            this.buffer = bArr;
        }
    }

    public ad(byte[] bArr) {
        this(bArr, 0, bArr.length);
    }

    public ad(byte[] bArr, int i, int i2) {
        this.pG = 0;
        this.buffer = bArr;
        this.pG = i;
        next();
        if (this.length != -1 && i + i2 != this.pG + this.length) {
            throw new ak(a.getString("security.111"));
        }
    }

    /* JADX WARN: Type inference failed for: r2v3, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int Q(byte[] r5) {
        /*
            r0 = 1
            byte r0 = r5[r0]
            r0 = r0 & 255(0xff, float:3.57E-43)
            r1 = 0
            r2 = r0 & 128(0x80, float:1.794E-43)
            if (r2 == 0) goto L_0x0028
            r0 = r0 & 127(0x7f, float:1.78E-43)
            r1 = 2
            byte r1 = r5[r1]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r2 = 3
            r4 = r2
            r2 = r1
            r1 = r4
        L_0x0015:
            int r3 = r0 + 2
            if (r1 >= r3) goto L_0x0023
            int r2 = r2 << 8
            byte r3 = r5[r1]
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r2 = r2 + r3
            int r1 = r1 + 1
            goto L_0x0015
        L_0x0023:
            r1 = r2
        L_0x0024:
            int r0 = r0 + 2
            int r0 = r0 + r1
            return r0
        L_0x0028:
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.c.a.ad.Q(byte[]):int");
    }

    private int aB(int i, int i2) {
        int i3 = i + i2;
        int i4 = 0;
        for (int i5 = i; i5 < i3; i5++) {
            int i6 = this.buffer[i5] - r.Eh;
            if (i6 < 0 || i6 > 9) {
                throw new ak(a.getString("security.126"));
            }
            i4 = (i4 * 10) + i6;
        }
        return i4;
    }

    private final void c(t tVar) {
        int i = this.pG;
        int i2 = this.length + i;
        am amVar = tVar.rq;
        if (this.avc) {
            while (i2 > this.pG) {
                next();
                amVar.a(this);
            }
        } else {
            int i3 = this.auX;
            ArrayList arrayList = new ArrayList();
            while (i2 > this.pG) {
                next();
                arrayList.add(amVar.a(this));
            }
            this.auW = arrayList;
            this.auX = i3;
        }
        if (this.pG != i2) {
            throw new ak(a.q("security.134", i));
        }
    }

    public final void P(byte[] bArr) {
        this.buffer = bArr;
        next();
    }

    public void a(ah ahVar) {
        if (this.tag == ahVar.id) {
            xw();
        } else if (this.tag == ahVar.btX) {
            throw new ak(a.getString("security.139"));
        } else {
            throw new ak(a.c("security.13A", Integer.valueOf(this.auX), Integer.toHexString(this.tag)));
        }
    }

    public void a(al alVar) {
        if (this.tag != 49) {
            throw new ak(a.c("security.136", Integer.valueOf(this.auX), Integer.toHexString(this.tag)));
        }
        throw new ak(a.getString("security.137"));
    }

    public void b(Object obj, Object obj2) {
        if (this.ave == null) {
            this.ave = (Object[][]) Array.newInstance(Object.class, 2, 10);
        }
        int i = 0;
        while (i < this.ave[0].length && this.ave[0][i] != null) {
            if (this.ave[0][i] == obj) {
                this.ave[1][i] = obj2;
                return;
            }
            i++;
        }
        if (i == this.ave[0].length) {
            Object[][] objArr = (Object[][]) Array.newInstance(Object.class, this.ave[0].length * 2, 2);
            System.arraycopy(this.ave[0], 0, objArr[0], 0, this.ave[0].length);
            System.arraycopy(this.ave[1], 0, objArr[1], 0, this.ave[0].length);
            this.ave = objArr;
            return;
        }
        this.ave[0][i] = obj;
        this.ave[1][i] = obj2;
    }

    public void c(a aVar) {
        if (this.tag != 49) {
            throw new ak(a.c("security.138", Integer.valueOf(this.auX), Integer.toHexString(this.tag)));
        }
        c((t) aVar);
    }

    public void c(aq aqVar) {
        if (this.tag != 48) {
            throw new ak(a.c("security.135", Integer.valueOf(this.auX), Integer.toHexString(this.tag)));
        }
        c((t) aqVar);
    }

    public void c(v vVar) {
        if (this.tag != 48) {
            throw new ak(a.c("security.12F", Integer.valueOf(this.auX), Integer.toHexString(this.tag)));
        }
        int i = this.pG;
        int i2 = this.length + i;
        am[] amVarArr = vVar.dk;
        int i3 = 0;
        if (this.avc) {
            while (this.pG < i2 && i3 < amVarArr.length) {
                next();
                while (!amVarArr[i3].v(this.tag)) {
                    if (!vVar.asF[i3] || i3 == amVarArr.length - 1) {
                        throw new ak(a.q("security.130", this.auX));
                    }
                    i3++;
                }
                amVarArr[i3].a(this);
                i3++;
            }
            while (i3 < amVarArr.length) {
                if (!vVar.asF[i3]) {
                    throw new ak(a.q("security.131", this.auX));
                }
                i3++;
            }
        } else {
            int i4 = this.auX;
            Object[] objArr = new Object[amVarArr.length];
            while (this.pG < i2 && i3 < amVarArr.length) {
                next();
                while (!amVarArr[i3].v(this.tag)) {
                    if (!vVar.asF[i3] || i3 == amVarArr.length - 1) {
                        throw new ak(a.q("security.132", this.auX));
                    }
                    if (vVar.asG[i3] != null) {
                        objArr[i3] = vVar.asG[i3];
                    }
                    i3++;
                }
                objArr[i3] = amVarArr[i3].a(this);
                i3++;
            }
            while (i3 < amVarArr.length) {
                if (!vVar.asF[i3]) {
                    throw new ak(a.q("security.133", this.auX));
                }
                if (vVar.asG[i3] != null) {
                    objArr[i3] = vVar.asG[i3];
                }
                i3++;
            }
            this.auW = objArr;
            this.auX = i4;
        }
        if (this.pG != i2) {
            throw new ak(a.q("security.134", i));
        }
    }

    public Object get(Object obj) {
        if (this.ave == null) {
            return null;
        }
        for (int i = 0; i < this.ave[0].length; i++) {
            if (this.ave[0][i] == obj) {
                return this.ave[1][i];
            }
        }
        return null;
    }

    public byte[] getEncoded() {
        byte[] bArr = new byte[(this.pG - this.auX)];
        System.arraycopy(this.buffer, this.auX, bArr, 0, bArr.length);
        return bArr;
    }

    public final int getLength() {
        return this.length;
    }

    public final int getOffset() {
        return this.pG;
    }

    public int next() {
        this.auX = this.pG;
        this.tag = read();
        this.length = read();
        if (this.length == 128) {
            this.length = -1;
        } else if ((this.length & 128) != 0) {
            int i = this.length & 127;
            if (i > 5) {
                throw new ak(a.q("security.113", this.auX));
            }
            this.length = read();
            for (int i2 = 1; i2 < i; i2++) {
                this.length = read() + (this.length << 8);
            }
            if (this.length > 16777215) {
                throw new ak(a.q("security.113", this.auX));
            }
        }
        this.auY = this.pG;
        return this.tag;
    }

    /* access modifiers changed from: protected */
    public int read() {
        if (this.pG == this.buffer.length) {
            throw new ak(a.getString("security.13B"));
        } else if (this.in == null) {
            byte[] bArr = this.buffer;
            int i = this.pG;
            this.pG = i + 1;
            return bArr[i] & 255;
        } else {
            int read = this.in.read();
            if (read == -1) {
                throw new ak(a.getString("security.13B"));
            }
            byte[] bArr2 = this.buffer;
            int i2 = this.pG;
            this.pG = i2 + 1;
            bArr2[i2] = (byte) read;
            return read;
        }
    }

    public void xj() {
        if (this.tag == 3) {
            if (this.length == 0) {
                throw new ak(a.q("security.114", this.auX));
            }
            xw();
            if (this.buffer[this.auY] > 7) {
                throw new ak(a.q("security.115", this.auY));
            } else if (this.length == 1 && this.buffer[this.auY] != 0) {
                throw new ak(a.q("security.116", this.auY));
            }
        } else if (this.tag == 35) {
            throw new ak(a.getString("security.117"));
        } else {
            throw new ak(a.c("security.118", Integer.valueOf(this.auX), Integer.toHexString(this.tag)));
        }
    }

    public void xk() {
        if (this.tag != 10) {
            throw new ak(a.c("security.119", Integer.valueOf(this.auX), Integer.toHexString(this.tag)));
        } else if (this.length == 0) {
            throw new ak(a.q("security.11A", this.auX));
        } else {
            xw();
            if (this.length > 1) {
                int i = this.buffer[this.auY] & 255;
                if (this.buffer[this.auY + 1] < 0) {
                    i += 256;
                }
                if (i == 0 || i == 511) {
                    throw new ak(a.q("security.11B", this.auY));
                }
            }
        }
    }

    public void xl() {
        if (this.tag != 1) {
            throw new ak(a.c("security.11C", Integer.valueOf(this.auX), Integer.toHexString(this.tag)));
        } else if (this.length != 1) {
            throw new ak(a.q("security.11D", this.auX));
        } else {
            xw();
        }
    }

    public void xm() {
        byte b2;
        if (this.tag == 24) {
            xw();
            if (this.buffer[this.pG - 1] != 90) {
                throw new ak(a.getString("security.11E"));
            } else if (this.length != 15 && (this.length < 17 || this.length > 19)) {
                throw new ak(a.q("security.11F", this.auY));
            } else if (this.length <= 16 || (b2 = this.buffer[this.auY + 14]) == 46 || b2 == 44) {
                if (this.ava == null) {
                    this.ava = new int[7];
                }
                this.ava[0] = aB(this.auY, 4);
                this.ava[1] = aB(this.auY + 4, 2);
                this.ava[2] = aB(this.auY + 6, 2);
                this.ava[3] = aB(this.auY + 8, 2);
                this.ava[4] = aB(this.auY + 10, 2);
                this.ava[5] = aB(this.auY + 12, 2);
                if (this.length > 16) {
                    this.ava[6] = aB(this.auY + 15, this.length - 16);
                    if (this.length == 17) {
                        this.ava[6] = this.ava[6] * 100;
                    } else if (this.length == 18) {
                        this.ava[6] = this.ava[6] * 10;
                    }
                }
            } else {
                throw new ak(a.q("security.11F", this.auY));
            }
        } else if (this.tag == 56) {
            throw new ak(a.getString("security.120"));
        } else {
            throw new ak(a.c("security.121", Integer.valueOf(this.auX), Integer.toHexString(this.tag)));
        }
    }

    public void xn() {
        if (this.tag == 23) {
            switch (this.length) {
                case 11:
                case 13:
                    xw();
                    if (this.buffer[this.pG - 1] != 90) {
                        throw new ak("ASN.1 UTCTime wrongly encoded at [" + this.auY + ']');
                    }
                    if (this.ava == null) {
                        this.ava = new int[7];
                    }
                    this.ava[0] = aB(this.auY, 2);
                    if (this.ava[0] > 49) {
                        int[] iArr = this.ava;
                        iArr[0] = iArr[0] + 1900;
                    } else {
                        int[] iArr2 = this.ava;
                        iArr2[0] = iArr2[0] + 2000;
                    }
                    this.ava[1] = aB(this.auY + 2, 2);
                    this.ava[2] = aB(this.auY + 4, 2);
                    this.ava[3] = aB(this.auY + 6, 2);
                    this.ava[4] = aB(this.auY + 8, 2);
                    if (this.length == 13) {
                        this.ava[5] = aB(this.auY + 10, 2);
                        return;
                    }
                    return;
                case 12:
                case 14:
                case 16:
                default:
                    throw new ak(a.q("security.123", this.auX));
                case 15:
                case 17:
                    throw new ak(a.getString("security.122"));
            }
        } else if (this.tag == 55) {
            throw new ak(a.getString("security.124"));
        } else {
            throw new ak(a.c("security.125", Integer.valueOf(this.auX), Integer.toHexString(this.tag)));
        }
    }

    public void xo() {
        if (this.tag != 2) {
            throw new ak(a.c("security.127", Integer.valueOf(this.auX), Integer.toHexString(this.tag)));
        } else if (this.length < 1) {
            throw new ak(a.q("security.128", this.auX));
        } else {
            xw();
            if (this.length > 1) {
                byte b2 = this.buffer[this.pG - this.length];
                byte b3 = (byte) (this.buffer[(this.pG - this.length) + 1] & 128);
                if ((b2 == 0 && b3 == 0) || (b2 == -1 && b3 == Byte.MIN_VALUE)) {
                    throw new ak(a.q("security.129", this.pG - this.length));
                }
            }
        }
    }

    public void xp() {
        if (this.tag == 4) {
            xw();
        } else if (this.tag == 36) {
            throw new ak(a.getString("security.12A"));
        } else {
            throw new ak(a.c("security.12B", Integer.valueOf(this.auX), Integer.toHexString(this.tag)));
        }
    }

    public void xq() {
        if (this.tag != 6) {
            throw new ak(a.c("security.12C", Integer.valueOf(this.auX), Integer.toHexString(this.tag)));
        } else if (this.length < 1) {
            throw new ak(a.q("security.12D", this.auX));
        } else {
            xw();
            if ((this.buffer[this.pG - 1] & 128) != 0) {
                throw new ak(a.q("security.12E", this.pG - 1));
            }
            this.avb = 1;
            int i = 0;
            while (i < this.length) {
                while ((this.buffer[this.auY + i] & 128) == 128) {
                    i++;
                }
                i++;
                this.avb++;
            }
        }
    }

    public final byte[] xr() {
        return this.buffer;
    }

    public final int xs() {
        return this.pG + this.length;
    }

    public final int xt() {
        return this.auX;
    }

    public final int xu() {
        return this.auY;
    }

    public final void xv() {
        this.avc = true;
    }

    public void xw() {
        if (this.pG + this.length > this.buffer.length) {
            throw new ak(a.getString("security.13B"));
        } else if (this.in == null) {
            this.pG += this.length;
        } else {
            int read = this.in.read(this.buffer, this.pG, this.length);
            if (read != this.length) {
                int i = read;
                while (read >= 1 && i <= this.length) {
                    read = this.in.read(this.buffer, this.pG + i, this.length - i);
                    i += read;
                    if (i == this.length) {
                    }
                }
                throw new ak(a.getString("security.13C"));
            }
            this.pG += this.length;
        }
    }

    public void xx() {
        if (this.pG != this.buffer.length) {
            byte[] bArr = new byte[this.pG];
            System.arraycopy(this.buffer, 0, bArr, 0, this.pG);
            this.buffer = bArr;
        }
    }
}
