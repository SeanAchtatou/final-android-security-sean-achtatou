package a.a.a.c.a;

import a.a.a.c.f.d;

public class s extends r {
    private final ai FN;
    private final z FO;

    public s(ai aiVar, z zVar) {
        this.FN = aiVar;
        this.FO = zVar;
    }

    public Object a(ad adVar) {
        int[] iArr = (int[]) adVar.get(this.FN);
        if (iArr == null) {
            throw new RuntimeException("");
        }
        d dVar = (d) this.FO.get(iArr);
        if (dVar == null || !dVar.rq.v(adVar.tag)) {
            adVar.auW = (byte[]) super.b(adVar);
        } else {
            adVar.auW = dVar.rq.a(adVar);
        }
        return adVar.auW;
    }

    public Object b(ad adVar) {
        return adVar.auW;
    }
}
