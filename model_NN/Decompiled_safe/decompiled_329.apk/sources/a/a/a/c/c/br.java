package a.a.a.c.c;

import a.a.a.c.a.am;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.security.auth.x500.X500Principal;

public class br {
    private static List buJ = Arrays.asList("2.5.29.15", "2.5.29.19", "2.5.29.32", "2.5.29.17", "2.5.29.30", "2.5.29.36", "2.5.29.37", "2.5.29.54");
    public static final am lG = new ad(af.en);
    /* access modifiers changed from: private */
    public List buK;
    private Set buL;
    private Set buM;
    private boolean buN;
    private HashMap buO;
    private byte[] dV;

    public br() {
    }

    public br(List list) {
        this.buK = list;
    }

    private void GA() {
        if (this.buK != null) {
            int size = this.buK.size();
            this.buL = new HashSet(size);
            this.buM = new HashSet(size);
            for (int i = 0; i < size; i++) {
                af afVar = (af) this.buK.get(i);
                String va = afVar.va();
                if (afVar.vb()) {
                    if (!buJ.contains(va)) {
                        this.buN = true;
                    }
                    this.buL.add(va);
                } else {
                    this.buM.add(va);
                }
            }
        }
    }

    public boolean[] GB() {
        bv vf;
        af eN = eN("2.5.29.15");
        if (eN == null || (vf = eN.vf()) == null) {
            return null;
        }
        return vf.getKeyUsage();
    }

    public List GC() {
        af eN = eN("2.5.29.37");
        if (eN == null) {
            return null;
        }
        return ((bj) eN.ve()).getExtendedKeyUsage();
    }

    public int GD() {
        ai vg;
        af eN = eN("2.5.29.19");
        if (eN == null || (vg = eN.vg()) == null) {
            return Integer.MAX_VALUE;
        }
        return vg.wZ();
    }

    public List GE() {
        af eN = eN("2.5.29.17");
        if (eN == null) {
            return null;
        }
        return ((bl) bl.lG.ax(eN.vc())).EM();
    }

    public List GF() {
        af eN = eN("2.5.29.18");
        if (eN == null) {
            return null;
        }
        return ((bl) bl.lG.ax(eN.vc())).EM();
    }

    public X500Principal GG() {
        af eN = eN("2.5.29.29");
        if (eN == null) {
            return null;
        }
        return ((e) eN.ve()).getIssuer();
    }

    public List Gw() {
        return this.buK;
    }

    public Set Gx() {
        if (this.buL == null) {
            GA();
        }
        return this.buL;
    }

    public Set Gy() {
        if (this.buM == null) {
            GA();
        }
        return this.buM;
    }

    public boolean Gz() {
        if (this.buL == null) {
            GA();
        }
        return this.buN;
    }

    public void a(StringBuffer stringBuffer, String str) {
        if (this.buK != null) {
            int i = 1;
            for (af a2 : this.buK) {
                stringBuffer.append(10).append(str).append('[').append(i).append("]: ");
                a2.a(stringBuffer, str);
                i++;
            }
        }
    }

    public void d(af afVar) {
        this.dV = null;
        if (this.buK == null) {
            this.buK = new ArrayList();
        }
        this.buK.add(afVar);
        if (this.buO != null) {
            this.buO.put(afVar.va(), afVar);
        }
        if (this.buL != null) {
            String va = afVar.va();
            if (afVar.vb()) {
                if (!buJ.contains(va)) {
                    this.buN = true;
                }
                this.buL.add(va);
                return;
            }
            this.buM.add(va);
        }
    }

    public af eN(String str) {
        if (this.buK == null) {
            return null;
        }
        if (this.buO == null) {
            this.buO = new HashMap();
            for (af afVar : this.buK) {
                this.buO.put(afVar.va(), afVar);
            }
        }
        return (af) this.buO.get(str);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof br)) {
            return false;
        }
        br brVar = (br) obj;
        if (this.buK == null || this.buK.size() == 0) {
            return brVar.buK == null || brVar.buK.size() == 0;
        }
        if (brVar.buK == null || brVar.buK.size() == 0) {
            return false;
        }
        return this.buK.containsAll(brVar.buK) && this.buK.size() == brVar.buK.size();
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = lG.S(this);
        }
        return this.dV;
    }

    public int hashCode() {
        if (this.buK != null) {
            return this.buK.hashCode();
        }
        return 0;
    }

    public int size() {
        if (this.buK == null) {
            return 0;
        }
        return this.buK.size();
    }
}
