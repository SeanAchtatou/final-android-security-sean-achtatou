package a.a.a.c.e.a;

import a.a.a.c.b.b;
import a.a.a.c.c.af;
import a.a.a.c.c.ao;
import a.a.a.c.c.bn;
import a.a.a.c.c.br;
import a.a.a.c.c.o;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.Principal;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CRLException;
import java.security.cert.Certificate;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLEntry;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.security.auth.x500.X500Principal;

public class a extends X509CRL {
    private final o dK;
    private final ao dL;
    private byte[] dM;
    private final br dN;
    private X500Principal dO;
    private ArrayList dP;
    private int dQ;
    private byte[] dR;
    private String dS;
    private String dT;
    private byte[] dU;
    private byte[] dV;
    private boolean dW;
    private boolean dX;
    private boolean dY;
    private int dZ;

    public a(o oVar) {
        this.dK = oVar;
        this.dL = oVar.iO();
        this.dN = this.dL.yF();
    }

    public a(InputStream inputStream) {
        try {
            this.dK = (o) o.en.h(inputStream);
            this.dL = this.dK.iO();
            this.dN = this.dL.yF();
        } catch (IOException e) {
            throw new CRLException(e);
        }
    }

    public a(byte[] bArr) {
        this((o) o.en.ax(bArr));
    }

    private void cq() {
        this.dX = true;
        List yE = this.dL.yE();
        if (yE != null) {
            this.dQ = yE.size();
            this.dP = new ArrayList(this.dQ);
            X500Principal x500Principal = null;
            for (int i = 0; i < this.dQ; i++) {
                bn bnVar = (bn) yE.get(i);
                X500Principal issuer = bnVar.getIssuer();
                if (issuer != null) {
                    this.dY = true;
                    this.dZ = i;
                    x500Principal = issuer;
                }
                this.dP.add(new f(bnVar, x500Principal));
            }
        }
    }

    public Set getCriticalExtensionOIDs() {
        if (this.dN == null) {
            return null;
        }
        return this.dN.Gx();
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = this.dK.getEncoded();
        }
        byte[] bArr = new byte[this.dV.length];
        System.arraycopy(this.dV, 0, bArr, 0, this.dV.length);
        return bArr;
    }

    public byte[] getExtensionValue(String str) {
        if (this.dN == null) {
            return null;
        }
        af eN = this.dN.eN(str);
        if (eN == null) {
            return null;
        }
        return eN.vd();
    }

    public Principal getIssuerDN() {
        if (this.dO == null) {
            this.dO = this.dL.fN().yG();
        }
        return this.dO;
    }

    public X500Principal getIssuerX500Principal() {
        if (this.dO == null) {
            this.dO = this.dL.fN().yG();
        }
        return this.dO;
    }

    public Date getNextUpdate() {
        return this.dL.getNextUpdate();
    }

    public Set getNonCriticalExtensionOIDs() {
        if (this.dN == null) {
            return null;
        }
        return this.dN.Gy();
    }

    public X509CRLEntry getRevokedCertificate(BigInteger bigInteger) {
        if (!this.dX) {
            cq();
        }
        if (this.dP == null) {
            return null;
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.dZ) {
                return null;
            }
            X509CRLEntry x509CRLEntry = (X509CRLEntry) this.dP.get(i2);
            if (bigInteger.equals(x509CRLEntry.getSerialNumber())) {
                return x509CRLEntry;
            }
            i = i2 + 1;
        }
    }

    public X509CRLEntry getRevokedCertificate(X509Certificate x509Certificate) {
        if (x509Certificate == null) {
            throw new NullPointerException();
        }
        if (!this.dX) {
            cq();
        }
        if (this.dP == null) {
            return null;
        }
        BigInteger serialNumber = x509Certificate.getSerialNumber();
        if (this.dY) {
            X500Principal issuerX500Principal = x509Certificate.getIssuerX500Principal();
            X500Principal x500Principal = issuerX500Principal.equals(getIssuerX500Principal()) ? null : issuerX500Principal;
            for (int i = 0; i < this.dQ; i++) {
                X509CRLEntry x509CRLEntry = (X509CRLEntry) this.dP.get(i);
                if (serialNumber.equals(x509CRLEntry.getSerialNumber())) {
                    X500Principal certificateIssuer = x509CRLEntry.getCertificateIssuer();
                    if (x500Principal != null) {
                        if (x500Principal.equals(certificateIssuer)) {
                            return x509CRLEntry;
                        }
                    } else if (certificateIssuer == null) {
                        return x509CRLEntry;
                    }
                }
            }
        } else {
            for (int i2 = 0; i2 < this.dQ; i2++) {
                X509CRLEntry x509CRLEntry2 = (X509CRLEntry) this.dP.get(i2);
                if (serialNumber.equals(x509CRLEntry2.getSerialNumber())) {
                    return x509CRLEntry2;
                }
            }
        }
        return null;
    }

    public Set getRevokedCertificates() {
        if (!this.dX) {
            cq();
        }
        if (this.dP == null) {
            return null;
        }
        return new HashSet(this.dP);
    }

    public String getSigAlgName() {
        if (this.dS == null) {
            this.dS = this.dL.fM().getAlgorithm();
            this.dT = b.am(this.dS);
            if (this.dT == null) {
                this.dT = this.dS;
            }
        }
        return this.dT;
    }

    public String getSigAlgOID() {
        if (this.dS == null) {
            this.dS = this.dL.fM().getAlgorithm();
            this.dT = b.am(this.dS);
            if (this.dT == null) {
                this.dT = this.dS;
            }
        }
        return this.dS;
    }

    public byte[] getSigAlgParams() {
        if (this.dW) {
            return null;
        }
        if (this.dU == null) {
            this.dU = this.dL.fM().xz();
            if (this.dU == null) {
                this.dW = true;
                return null;
            }
        }
        return this.dU;
    }

    public byte[] getSignature() {
        if (this.dR == null) {
            this.dR = this.dK.iQ();
        }
        byte[] bArr = new byte[this.dR.length];
        System.arraycopy(this.dR, 0, bArr, 0, this.dR.length);
        return bArr;
    }

    public byte[] getTBSCertList() {
        if (this.dM == null) {
            this.dM = this.dL.getEncoded();
        }
        byte[] bArr = new byte[this.dM.length];
        System.arraycopy(this.dM, 0, bArr, 0, this.dM.length);
        return bArr;
    }

    public Date getThisUpdate() {
        return this.dL.getThisUpdate();
    }

    public int getVersion() {
        return this.dL.getVersion();
    }

    public boolean hasUnsupportedCriticalExtension() {
        if (this.dN == null) {
            return false;
        }
        return this.dN.Gz();
    }

    public boolean isRevoked(Certificate certificate) {
        if (!(certificate instanceof X509Certificate)) {
            return false;
        }
        return getRevokedCertificate((X509Certificate) certificate) != null;
    }

    public String toString() {
        return this.dK.toString();
    }

    public void verify(PublicKey publicKey) {
        Signature instance = Signature.getInstance(getSigAlgName());
        instance.initVerify(publicKey);
        byte[] encoded = this.dL.getEncoded();
        instance.update(encoded, 0, encoded.length);
        if (!instance.verify(this.dK.iQ())) {
            throw new SignatureException(a.a.a.c.j.a.a.getString("security.15C"));
        }
    }

    public void verify(PublicKey publicKey, String str) {
        Signature instance = Signature.getInstance(getSigAlgName(), str);
        instance.initVerify(publicKey);
        byte[] encoded = this.dL.getEncoded();
        instance.update(encoded, 0, encoded.length);
        if (!instance.verify(this.dK.iQ())) {
            throw new SignatureException(a.a.a.c.j.a.a.getString("security.15C"));
        }
    }
}
