package a.a.a.c.c;

import a.a.a.c.a.y;
import a.a.a.c.f.c;
import a.a.a.c.f.e;
import a.a.a.c.j.a.a;
import com.uc.c.ca;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ac {
    protected int We;
    protected int Wf;
    protected char[] Wg;
    protected byte[] dg;
    protected int jv;
    protected final int length;
    protected int pos;
    protected boolean sh;

    public ac(String str) {
        this.length = str.length();
        this.Wg = str.toCharArray();
    }

    private String ss() {
        if (this.pos + 4 >= this.length) {
            throw new IOException(a.getString("security.192"));
        }
        this.We = this.pos;
        this.pos++;
        while (true) {
            if (this.pos == this.length || this.Wg[this.pos] == '+' || this.Wg[this.pos] == ',' || this.Wg[this.pos] == ';') {
                this.jv = this.pos;
            } else if (this.Wg[this.pos] == ' ') {
                this.jv = this.pos;
                this.pos++;
                while (this.pos < this.length && this.Wg[this.pos] == ' ') {
                    this.pos++;
                }
            } else {
                if (this.Wg[this.pos] >= 'A' && this.Wg[this.pos] <= 'F') {
                    char[] cArr = this.Wg;
                    int i = this.pos;
                    cArr[i] = (char) (cArr[i] + ' ');
                }
                this.pos++;
            }
        }
        this.jv = this.pos;
        int i2 = this.jv - this.We;
        if (i2 < 5 || (i2 & 1) == 0) {
            throw new IOException(a.getString("security.192"));
        }
        this.dg = new byte[(i2 / 2)];
        int i3 = this.We + 1;
        for (int i4 = 0; i4 < this.dg.length; i4++) {
            this.dg[i4] = (byte) ec(i3);
            i3 += 2;
        }
        return new String(this.Wg, this.We, i2);
    }

    private char su() {
        this.pos++;
        if (this.pos == this.length) {
            throw new IOException(a.getString("security.192"));
        }
        switch (this.Wg[this.pos]) {
            case ' ':
            case '#':
            case '+':
            case ',':
            case ';':
            case '<':
            case '=':
            case '>':
                break;
            default:
                return sv();
            case '\"':
            case '\\':
                this.sh = true;
                break;
        }
        return this.Wg[this.pos];
    }

    /* access modifiers changed from: protected */
    public int ec(int i) {
        int i2;
        int i3;
        if (i + 1 >= this.length) {
            throw new IOException(a.getString("security.192"));
        }
        char c = this.Wg[i];
        if (c >= '0' && c <= '9') {
            i2 = c - ca.bNQ;
        } else if (c >= 'a' && c <= 'f') {
            i2 = c - 'W';
        } else if (c < 'A' || c > 'F') {
            throw new IOException(a.getString("security.192"));
        } else {
            i2 = c - '7';
        }
        char c2 = this.Wg[i + 1];
        if (c2 >= '0' && c2 <= '9') {
            i3 = c2 - ca.bNQ;
        } else if (c2 >= 'a' && c2 <= 'f') {
            i3 = c2 - 'W';
        } else if (c2 < 'A' || c2 > 'F') {
            throw new IOException(a.getString("security.192"));
        } else {
            i3 = c2 - '7';
        }
        return (i2 << 4) + i3;
    }

    /* access modifiers changed from: protected */
    public String sq() {
        this.sh = false;
        while (this.pos < this.length && this.Wg[this.pos] == ' ') {
            this.pos++;
        }
        if (this.pos == this.length) {
            return null;
        }
        this.We = this.pos;
        this.pos++;
        while (this.pos < this.length && this.Wg[this.pos] != '=' && this.Wg[this.pos] != ' ') {
            this.pos++;
        }
        if (this.pos >= this.length) {
            throw new IOException(a.getString("security.192"));
        }
        this.jv = this.pos;
        if (this.Wg[this.pos] == ' ') {
            while (this.pos < this.length && this.Wg[this.pos] != '=' && this.Wg[this.pos] == ' ') {
                this.pos++;
            }
            if (this.Wg[this.pos] != '=' || this.pos == this.length) {
                throw new IOException(a.getString("security.192"));
            }
        }
        this.pos++;
        while (this.pos < this.length && this.Wg[this.pos] == ' ') {
            this.pos++;
        }
        if (this.jv - this.We > 4 && this.Wg[this.We + 3] == '.' && ((this.Wg[this.We] == 'O' || this.Wg[this.We] == 'o') && ((this.Wg[this.We + 1] == 'I' || this.Wg[this.We + 1] == 'i') && (this.Wg[this.We + 2] == 'D' || this.Wg[this.We + 2] == 'd')))) {
            this.We += 4;
        }
        return new String(this.Wg, this.We, this.jv - this.We);
    }

    /* access modifiers changed from: protected */
    public String sr() {
        this.pos++;
        this.We = this.pos;
        this.jv = this.We;
        while (this.pos != this.length) {
            if (this.Wg[this.pos] == '\"') {
                this.pos++;
                while (this.pos < this.length && this.Wg[this.pos] == ' ') {
                    this.pos++;
                }
                return new String(this.Wg, this.We, this.jv - this.We);
            }
            if (this.Wg[this.pos] == '\\') {
                this.Wg[this.jv] = su();
            } else {
                this.Wg[this.jv] = this.Wg[this.pos];
            }
            this.pos++;
            this.jv++;
        }
        throw new IOException(a.getString("security.192"));
    }

    /* access modifiers changed from: protected */
    public String st() {
        this.We = this.pos;
        this.jv = this.pos;
        while (this.pos < this.length) {
            switch (this.Wg[this.pos]) {
                case ' ':
                    this.Wf = this.jv;
                    this.pos++;
                    char[] cArr = this.Wg;
                    int i = this.jv;
                    this.jv = i + 1;
                    cArr[i] = ' ';
                    while (this.pos < this.length && this.Wg[this.pos] == ' ') {
                        char[] cArr2 = this.Wg;
                        int i2 = this.jv;
                        this.jv = i2 + 1;
                        cArr2[i2] = ' ';
                        this.pos++;
                    }
                    if (this.pos != this.length && this.Wg[this.pos] != ',' && this.Wg[this.pos] != '+' && this.Wg[this.pos] != ';') {
                        break;
                    } else {
                        return new String(this.Wg, this.We, this.Wf - this.We);
                    }
                    break;
                case '+':
                case ',':
                case ';':
                    return new String(this.Wg, this.We, this.jv - this.We);
                case '\\':
                    char[] cArr3 = this.Wg;
                    int i3 = this.jv;
                    this.jv = i3 + 1;
                    cArr3[i3] = su();
                    this.pos++;
                    break;
                default:
                    char[] cArr4 = this.Wg;
                    int i4 = this.jv;
                    this.jv = i4 + 1;
                    cArr4[i4] = this.Wg[this.pos];
                    this.pos++;
                    break;
            }
        }
        return new String(this.Wg, this.We, this.jv - this.We);
    }

    /* access modifiers changed from: protected */
    public char sv() {
        int i;
        int i2;
        int ec = ec(this.pos);
        this.pos++;
        if (ec < 128) {
            return (char) ec;
        }
        if (ec < 192 || ec > 247) {
            return '?';
        }
        if (ec <= 223) {
            i = ec & 31;
            i2 = 1;
        } else if (ec <= 239) {
            i = ec & 15;
            i2 = 2;
        } else {
            i = ec & 7;
            i2 = 3;
        }
        int i3 = i;
        for (int i4 = 0; i4 < i2; i4++) {
            this.pos++;
            if (this.pos == this.length || this.Wg[this.pos] != '\\') {
                return '?';
            }
            this.pos++;
            int ec2 = ec(this.pos);
            this.pos++;
            if ((ec2 & y.YW) != 128) {
                return '?';
            }
            i3 = (i3 << 6) + (ec2 & 63);
        }
        return (char) i3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.c.f.c.<init>(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      a.a.a.c.f.c.<init>(java.lang.String, byte[]):void
      a.a.a.c.f.c.<init>(java.lang.String, boolean):void */
    public List sw() {
        ArrayList arrayList = new ArrayList();
        String sq = sq();
        if (sq != null) {
            String str = sq;
            ArrayList arrayList2 = new ArrayList();
            do {
                if (this.pos == this.length) {
                    arrayList2.add(new e(str, new c("", false)));
                    arrayList.add(0, arrayList2);
                } else {
                    switch (this.Wg[this.pos]) {
                        case '\"':
                            arrayList2.add(new e(str, new c(sr(), this.sh)));
                            break;
                        case '#':
                            arrayList2.add(new e(str, new c(ss(), this.dg)));
                            break;
                        case '+':
                        case ',':
                        case ';':
                            arrayList2.add(new e(str, new c("", false)));
                            break;
                        default:
                            arrayList2.add(new e(str, new c(st(), this.sh)));
                            break;
                    }
                    if (this.pos >= this.length) {
                        arrayList.add(0, arrayList2);
                    } else {
                        if (this.Wg[this.pos] == ',' || this.Wg[this.pos] == ';') {
                            arrayList.add(0, arrayList2);
                            arrayList2 = new ArrayList();
                        } else if (this.Wg[this.pos] != '+') {
                            throw new IOException(a.getString("security.192"));
                        }
                        this.pos++;
                        str = sq();
                    }
                }
            } while (str != null);
            throw new IOException(a.getString("security.192"));
        }
        return arrayList;
    }
}
