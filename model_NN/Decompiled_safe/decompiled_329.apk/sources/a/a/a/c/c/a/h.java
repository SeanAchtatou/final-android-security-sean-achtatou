package a.a.a.c.c.a;

import a.a.a.c.a.aa;
import a.a.a.c.a.am;
import a.a.a.c.a.as;
import a.a.a.c.a.c;
import a.a.a.c.a.n;
import a.a.a.c.a.o;
import a.a.a.c.a.u;
import a.a.a.c.a.v;
import a.a.a.c.c.be;
import a.a.a.c.c.br;
import java.math.BigInteger;
import java.util.Date;

public class h {
    public static final v YB = new l(new am[]{as.NW(), as.NW(), as.NW()});
    public static final v en = new m(new am[]{as.NW(), c.bb(), j.en, as.NW(), aa.uB(), YB, u.kZ(), as.NW(), new o(0, be.Yv), new n(1, br.lG)});
    /* access modifiers changed from: private */
    public final be YA;
    /* access modifiers changed from: private */
    public final String Yw;
    /* access modifiers changed from: private */
    public final Date Yx;
    /* access modifiers changed from: private */
    public final int[] Yy;
    /* access modifiers changed from: private */
    public final Boolean Yz;
    /* access modifiers changed from: private */
    public final br dN;
    /* access modifiers changed from: private */
    public final int tN;
    /* access modifiers changed from: private */
    public final j tO;
    /* access modifiers changed from: private */
    public final BigInteger tQ;
    /* access modifiers changed from: private */
    public final BigInteger vb;

    public h(int i, String str, j jVar, BigInteger bigInteger, Date date, int[] iArr, Boolean bool, BigInteger bigInteger2, be beVar, br brVar) {
        this.tN = i;
        this.Yw = str;
        this.tO = jVar;
        this.vb = bigInteger;
        this.Yx = date;
        this.Yy = iArr;
        this.Yz = bool;
        this.tQ = bigInteger2;
        this.YA = beVar;
        this.dN = brVar;
    }

    public br fn() {
        return this.dN;
    }

    public j fo() {
        return this.tO;
    }

    public BigInteger fp() {
        return this.tQ;
    }

    public BigInteger getSerialNumber() {
        return this.vb;
    }

    public int getVersion() {
        return this.tN;
    }

    public int[] tZ() {
        return this.Yy;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("-- TSTInfo:");
        stringBuffer.append("\nversion:  ");
        stringBuffer.append(this.tN);
        stringBuffer.append("\npolicy:  ");
        stringBuffer.append(this.Yw);
        stringBuffer.append("\nmessageImprint:  ");
        stringBuffer.append(this.tO);
        stringBuffer.append("\nserialNumber:  ");
        stringBuffer.append(this.vb);
        stringBuffer.append("\ngenTime:  ");
        stringBuffer.append(this.Yx);
        stringBuffer.append("\naccuracy:  ");
        if (this.Yy != null) {
            stringBuffer.append(this.Yy[0] + " sec, " + this.Yy[1] + " millis, " + this.Yy[2] + " micros");
        }
        stringBuffer.append("\nordering:  ");
        stringBuffer.append(this.Yz);
        stringBuffer.append("\nnonce:  ");
        stringBuffer.append(this.tQ);
        stringBuffer.append("\ntsa:  ");
        stringBuffer.append(this.YA);
        stringBuffer.append("\nextensions:  ");
        stringBuffer.append(this.dN);
        stringBuffer.append("\n-- TSTInfo End\n");
        return stringBuffer.toString();
    }

    public Date ua() {
        return this.Yx;
    }

    public Boolean ub() {
        return this.Yz;
    }

    public String uc() {
        return this.Yw;
    }

    public be ud() {
        return this.YA;
    }
}
