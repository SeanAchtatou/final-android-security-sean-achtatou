package a.a.a.c.d;

import a.a.a.c.a.ad;
import a.a.a.c.a.ag;
import a.a.a.c.a.am;
import a.a.a.c.a.v;
import java.util.Arrays;

final class f extends v {
    f(am[] amVarArr) {
        super(amVarArr);
        eU(1);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object[] objArr) {
        i iVar = (i) obj;
        objArr[0] = iVar.aG;
        if (iVar.auW == null) {
            return;
        }
        if (Arrays.equals(iVar.aG, i.bkk)) {
            if (iVar.auW != null) {
                objArr[1] = ag.Bl().S(iVar.auW);
            }
        } else if (iVar.auW instanceof h) {
            objArr[1] = h.en.S(iVar.auW);
        } else {
            objArr[1] = iVar.auW;
        }
    }

    /* access modifiers changed from: protected */
    public Object b(ad adVar) {
        Object[] objArr = (Object[]) adVar.auW;
        int[] iArr = (int[]) objArr[0];
        return Arrays.equals(iArr, i.bkk) ? objArr[1] != null ? new i(iArr, ag.Bl().ax((byte[]) objArr[1]), adVar.getEncoded(), null) : new i((int[]) objArr[0], null, adVar.getEncoded(), null) : Arrays.equals(iArr, i.bkl) ? new i((int[]) objArr[0], h.en.ax((byte[]) objArr[1]), adVar.getEncoded(), null) : new i((int[]) objArr[0], (byte[]) objArr[1], adVar.getEncoded(), null);
    }
}
