package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.c;
import a.a.a.c.a.v;

public class k {
    public static final v en = new y(new am[]{c.bb(), be.Yv});
    private byte[] dV;
    /* access modifiers changed from: private */
    public final String tL;
    /* access modifiers changed from: private */
    public final be tM;

    public k(String str, be beVar) {
        this.tL = str;
        this.tM = beVar;
    }

    private k(String str, be beVar, byte[] bArr) {
        this.tL = str;
        this.tM = beVar;
        this.dV = bArr;
    }

    /* synthetic */ k(String str, be beVar, byte[] bArr, y yVar) {
        this(str, beVar, bArr);
    }

    public be fk() {
        return this.tM;
    }

    public String fl() {
        return this.tL;
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("\n-- AccessDescription:");
        stringBuffer.append("\naccessMethod:  ");
        stringBuffer.append(this.tL);
        stringBuffer.append("\naccessLocation:  ");
        stringBuffer.append(this.tM);
        stringBuffer.append("\n-- AccessDescription END\n");
        return stringBuffer.toString();
    }
}
