package a.a.a.c.a;

public abstract class ah extends am {
    public static final ah bgA = new h(27);
    public static final ah bgB = new k(19);
    public static final ah bgC = new j(20);
    public static final ah bgD = new m(28);
    public static final ah bgE = new l(12);
    public static final ah bgy = new g(30);
    public static final ah bgz = new i(22);

    public ah(int i) {
        super(i);
    }

    public Object a(ad adVar) {
        adVar.a(this);
        if (adVar.avc) {
            return null;
        }
        return b(adVar);
    }

    public void a(at atVar) {
        atVar.Op();
    }

    public Object b(ad adVar) {
        return new String(adVar.buffer, adVar.auY, adVar.length, "ISO-8859-1");
    }

    public void b(at atVar) {
        byte[] bytes = ((String) atVar.auW).getBytes();
        atVar.auW = bytes;
        atVar.length = bytes.length;
    }

    public void c(at atVar) {
        atVar.kc(this.id);
        a(atVar);
    }

    public final boolean v(int i) {
        return this.id == i || this.btX == i;
    }
}
