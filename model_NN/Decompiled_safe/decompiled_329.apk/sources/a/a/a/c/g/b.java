package a.a.a.c.g;

import a.a.a.c.a.a;
import a.a.a.c.a.ag;
import a.a.a.c.a.am;
import a.a.a.c.a.as;
import a.a.a.c.a.n;
import a.a.a.c.a.v;
import a.a.a.c.c.al;
import a.a.a.c.f.e;
import java.util.List;

public class b {
    public static final v en = new a(new am[]{as.NW(), al.en, ag.Bl(), new n(0, new a(e.en))});
    /* access modifiers changed from: private */
    public al IV;
    /* access modifiers changed from: private */
    public byte[] IW;
    /* access modifiers changed from: private */
    public List IX;
    private byte[] dV;
    /* access modifiers changed from: private */
    public int tN;

    public b(int i, al alVar, byte[] bArr, List list) {
        this.tN = i;
        this.IV = alVar;
        this.IW = bArr;
        this.IX = list;
    }

    private b(int i, al alVar, byte[] bArr, List list, byte[] bArr2) {
        this(i, alVar, bArr, list);
        this.dV = bArr2;
    }

    /* synthetic */ b(int i, al alVar, byte[] bArr, List list, byte[] bArr2, a aVar) {
        this(i, alVar, bArr, list, bArr2);
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public int getVersion() {
        return this.tN;
    }

    public al kV() {
        return this.IV;
    }

    public List kW() {
        return this.IX;
    }

    public byte[] kX() {
        return this.IW;
    }
}
