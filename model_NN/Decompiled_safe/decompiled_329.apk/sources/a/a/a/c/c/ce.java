package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.j.a.a;
import java.util.List;

public class ce extends ap {
    public static final am lG = new ar(i.en);
    /* access modifiers changed from: private */
    public List chA;
    private byte[] dV;

    public ce(List list) {
        if (list == null || list.size() == 0) {
            throw new IllegalArgumentException(a.getString("security.17D"));
        }
        this.chA = list;
    }

    public ce(List list, byte[] bArr) {
        if (list == null || list.size() == 0) {
            throw new IllegalArgumentException(a.getString("security.17D"));
        }
        this.chA = list;
        this.dV = bArr;
    }

    public static ce aR(byte[] bArr) {
        return (ce) lG.ax(bArr);
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("CRL Distribution Points: [\n");
        int i = 0;
        for (i a2 : this.chA) {
            i++;
            stringBuffer.append(str).append("  [").append(i).append("]\n");
            a2.a(stringBuffer, str + "  ");
        }
        stringBuffer.append(str).append("]\n");
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = lG.S(this);
        }
        return this.dV;
    }
}
