package a.a.a.c.i;

import a.a.a.c.a.am;
import a.a.a.c.a.p;
import a.a.a.c.a.v;
import a.a.a.c.c.al;

public class b {
    public static final v en = new d(new am[]{c.en, al.en, p.fB()});
    /* access modifiers changed from: private */
    public c Gc;
    /* access modifiers changed from: private */
    public al Gd;
    /* access modifiers changed from: private */
    public byte[] dR;
    private byte[] dV;

    public b(c cVar, al alVar, byte[] bArr) {
        this.Gc = cVar;
        this.Gd = alVar;
        this.dR = new byte[bArr.length];
        System.arraycopy(bArr, 0, this.dR, 0, bArr.length);
    }

    private b(c cVar, al alVar, byte[] bArr, byte[] bArr2) {
        this(cVar, alVar, bArr);
        this.dV = bArr2;
    }

    /* synthetic */ b(c cVar, al alVar, byte[] bArr, byte[] bArr2, d dVar) {
        this(cVar, alVar, bArr, bArr2);
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public byte[] getSignature() {
        byte[] bArr = new byte[this.dR.length];
        System.arraycopy(this.dR, 0, bArr, 0, this.dR.length);
        return bArr;
    }

    public al jW() {
        return this.Gd;
    }

    public c jX() {
        return this.Gc;
    }
}
