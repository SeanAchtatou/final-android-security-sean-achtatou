package a.a.a.c.e.b;

import com.uc.c.r;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.InvalidParameterException;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.DSAKey;
import java.security.interfaces.DSAParams;
import java.security.interfaces.DSAPrivateKey;
import java.security.interfaces.DSAPublicKey;

public class a extends Signature {
    private MessageDigest ml = MessageDigest.getInstance("SHA1");
    private DSAKey mm;

    public a() {
        super("SHA1withDSA");
    }

    private boolean e(byte[] bArr, int i, int i2) {
        try {
            byte b2 = bArr[i + 3];
            byte b3 = bArr[i + b2 + 5];
            if (bArr[i + 0] == 48 && bArr[i + 2] == 2 && bArr[i + b2 + 4] == 2 && bArr[i + 1] == b2 + b3 + 4 && b2 <= 21 && b3 <= 21 && (i2 == 0 || bArr[i + 1] + 2 <= i2)) {
                byte b4 = bArr[b2 + 5 + b3];
                byte[] digest = this.ml.digest();
                byte[] bArr2 = new byte[b2];
                System.arraycopy(bArr, i + 4, bArr2, 0, b2);
                BigInteger bigInteger = new BigInteger(bArr2);
                byte[] bArr3 = new byte[b3];
                System.arraycopy(bArr, b2 + i + 6, bArr3, 0, b3);
                BigInteger bigInteger2 = new BigInteger(bArr3);
                DSAParams params = this.mm.getParams();
                BigInteger p = params.getP();
                BigInteger q = params.getQ();
                BigInteger g = params.getG();
                BigInteger y = ((DSAPublicKey) this.mm).getY();
                if (bigInteger.signum() != 1 || bigInteger.compareTo(q) != -1 || bigInteger2.signum() != 1 || bigInteger2.compareTo(q) != -1) {
                    return false;
                }
                BigInteger modInverse = bigInteger2.modInverse(q);
                return g.modPow(new BigInteger(1, digest).multiply(modInverse).mod(q), p).multiply(y.modPow(bigInteger.multiply(modInverse).mod(q), p)).mod(p).mod(q).compareTo(bigInteger) == 0;
            }
            throw new SignatureException(a.a.a.c.j.a.a.getString("security.16F"));
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new SignatureException(a.a.a.c.j.a.a.getString("security.170"));
        }
    }

    /* access modifiers changed from: protected */
    public Object engineGetParameter(String str) {
        if (str != null) {
            return null;
        }
        throw new NullPointerException(a.a.a.c.j.a.a.getString("security.01"));
    }

    /* access modifiers changed from: protected */
    public void engineInitSign(PrivateKey privateKey) {
        if (privateKey == null || !(privateKey instanceof DSAPrivateKey)) {
            throw new InvalidKeyException(a.a.a.c.j.a.a.getString("security.168"));
        }
        DSAParams params = ((DSAPrivateKey) privateKey).getParams();
        BigInteger p = params.getP();
        BigInteger q = params.getQ();
        BigInteger x = ((DSAPrivateKey) privateKey).getX();
        int bitLength = p.bitLength();
        if (p.compareTo(BigInteger.valueOf(1)) != 1 || bitLength < 512 || bitLength > 1024 || (bitLength & 63) != 0) {
            throw new InvalidKeyException(a.a.a.c.j.a.a.getString("security.169"));
        } else if (q.signum() != 1 && q.bitLength() != 160) {
            throw new InvalidKeyException(a.a.a.c.j.a.a.getString("security.16A"));
        } else if (x.signum() == 1 && x.compareTo(q) == -1) {
            this.mm = (DSAKey) privateKey;
            this.ml.reset();
        } else {
            throw new InvalidKeyException(a.a.a.c.j.a.a.getString("security.16B"));
        }
    }

    /* access modifiers changed from: protected */
    public void engineInitVerify(PublicKey publicKey) {
        if (publicKey == null || !(publicKey instanceof DSAPublicKey)) {
            throw new InvalidKeyException(a.a.a.c.j.a.a.getString("security.16C"));
        }
        DSAParams params = ((DSAPublicKey) publicKey).getParams();
        BigInteger p = params.getP();
        BigInteger q = params.getQ();
        BigInteger y = ((DSAPublicKey) publicKey).getY();
        int bitLength = p.bitLength();
        if (p.compareTo(BigInteger.valueOf(1)) != 1 || bitLength < 512 || bitLength > 1024 || (bitLength & 63) != 0) {
            throw new InvalidKeyException(a.a.a.c.j.a.a.getString("security.169"));
        } else if (q.signum() != 1 || q.bitLength() != 160) {
            throw new InvalidKeyException(a.a.a.c.j.a.a.getString("security.16A"));
        } else if (y.signum() != 1) {
            throw new InvalidKeyException(a.a.a.c.j.a.a.getString("security.16D"));
        } else {
            this.mm = (DSAKey) publicKey;
            this.ml.reset();
        }
    }

    /* access modifiers changed from: protected */
    public void engineSetParameter(String str, Object obj) {
        if (str == null) {
            throw new NullPointerException(a.a.a.c.j.a.a.c("security.83", "param"));
        }
        throw new InvalidParameterException(a.a.a.c.j.a.a.getString("security.16E"));
    }

    /* access modifiers changed from: protected */
    public byte[] engineSign() {
        BigInteger mod;
        BigInteger mod2;
        if (this.appRandom == null) {
            this.appRandom = new SecureRandom();
        }
        DSAParams params = this.mm.getParams();
        BigInteger p = params.getP();
        BigInteger q = params.getQ();
        BigInteger g = params.getG();
        BigInteger x = ((DSAPrivateKey) this.mm).getX();
        BigInteger bigInteger = new BigInteger(1, this.ml.digest());
        byte[] bArr = new byte[20];
        while (true) {
            this.appRandom.nextBytes(bArr);
            BigInteger bigInteger2 = new BigInteger(1, bArr);
            if (bigInteger2.compareTo(q) == -1) {
                mod = g.modPow(bigInteger2, p).mod(q);
                if (mod.signum() != 0) {
                    mod2 = bigInteger2.modInverse(q).multiply(bigInteger.add(x.multiply(mod)).mod(q)).mod(q);
                    if (mod2.signum() != 0) {
                        break;
                    }
                } else {
                    continue;
                }
            }
        }
        byte[] byteArray = mod.toByteArray();
        int length = byteArray.length;
        if ((byteArray[0] & 128) != 0) {
            length++;
        }
        byte[] byteArray2 = mod2.toByteArray();
        int length2 = byteArray2.length;
        if ((byteArray2[0] & 128) != 0) {
            length2++;
        }
        byte[] bArr2 = new byte[(length + 6 + length2)];
        bArr2[0] = r.Eh;
        bArr2[1] = (byte) (length + 4 + length2);
        bArr2[2] = 2;
        bArr2[3] = (byte) length;
        bArr2[length + 4] = 2;
        bArr2[length + 5] = (byte) length2;
        System.arraycopy(byteArray, 0, bArr2, length == byteArray.length ? 4 : 5, byteArray.length);
        System.arraycopy(byteArray2, 0, bArr2, length2 == byteArray2.length ? length + 6 : length + 7, byteArray2.length);
        return bArr2;
    }

    /* access modifiers changed from: protected */
    public void engineUpdate(byte b2) {
        this.ml.update(b2);
    }

    /* access modifiers changed from: protected */
    public void engineUpdate(byte[] bArr, int i, int i2) {
        this.ml.update(bArr, i, i2);
    }

    /* access modifiers changed from: protected */
    public boolean engineVerify(byte[] bArr) {
        if (bArr != null) {
            return e(bArr, 0, 0);
        }
        throw new NullPointerException(a.a.a.c.j.a.a.c("security.83", "sigBytes"));
    }

    /* access modifiers changed from: protected */
    public boolean engineVerify(byte[] bArr, int i, int i2) {
        return e(bArr, i, i2);
    }
}
