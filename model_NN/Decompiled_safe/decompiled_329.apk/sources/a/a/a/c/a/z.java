package a.a.a.c.a;

import a.a.a.c.f.d;
import java.lang.reflect.Array;
import java.util.Arrays;

public class z {
    private final int aaT;
    private final w[][] aaU;

    public z() {
        this(64, 10);
    }

    public z(int i, int i2) {
        this.aaT = i;
        this.aaU = (w[][]) Array.newInstance(w.class, i, i2);
    }

    private int b(int[] iArr) {
        int i = 0;
        int i2 = 0;
        while (i < iArr.length && i < 4) {
            i2 += iArr[i] << (i * 8);
            i++;
        }
        return Integer.MAX_VALUE & i2;
    }

    public void a(d dVar) {
        a(dVar.uL.aX(), dVar);
    }

    public void a(int[] iArr, Object obj) {
        w[] wVarArr = this.aaU[b(iArr) % this.aaT];
        int i = 0;
        while (wVarArr[i] != null) {
            if (Arrays.equals(iArr, wVarArr[i].aG)) {
                throw new Error();
            }
            i++;
        }
        if (i == this.aaT - 1) {
            throw new Error();
        }
        wVarArr[i] = new w(iArr, obj);
    }

    public Object get(int[] iArr) {
        w[] wVarArr = this.aaU[b(iArr) % this.aaT];
        for (int i = 0; wVarArr[i] != null; i++) {
            if (Arrays.equals(iArr, wVarArr[i].aG)) {
                return wVarArr[i].Ws;
            }
        }
        return null;
    }
}
