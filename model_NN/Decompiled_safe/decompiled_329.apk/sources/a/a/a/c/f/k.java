package a.a.a.c.f;

import a.a.a.c.b.a;
import java.io.Serializable;
import java.util.Comparator;

public class k implements Serializable, Comparator {
    private static final long serialVersionUID = -1286471842007103132L;

    private static int a(a aVar, a aVar2) {
        if (aVar == aVar2) {
            return 0;
        }
        int[] aX = aVar.aX();
        int[] aX2 = aVar2.aX();
        int length = aX.length < aX2.length ? aX.length : aX2.length;
        for (int i = 0; i < length; i++) {
            if (aX[i] < aX2[i]) {
                return -1;
            }
            if (aX[i] > aX2[i]) {
                return 1;
            }
            if (i + 1 == aX.length && i + 1 < aX2.length) {
                return -1;
            }
            if (i + 1 < aX.length && i + 1 == aX2.length) {
                return 1;
            }
        }
        return 0;
    }

    public int compare(Object obj, Object obj2) {
        if (obj == obj2) {
            return 0;
        }
        e eVar = (e) obj;
        e eVar2 = (e) obj2;
        String name = eVar.sd().getName();
        String name2 = eVar2.sd().getName();
        if (name != null && name2 == null) {
            return -1;
        }
        if (name != null || name2 == null) {
            return (name == null || name2 == null) ? a(eVar.sd(), eVar2.sd()) : name.compareTo(name2);
        }
        return 1;
    }
}
