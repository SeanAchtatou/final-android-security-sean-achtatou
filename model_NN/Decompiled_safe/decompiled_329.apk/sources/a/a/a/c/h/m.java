package a.a.a.c.h;

import java.security.PrivilegedAction;
import java.security.Security;

public class m implements PrivilegedAction {
    private String ds;

    public m(String str) {
        this.ds = str;
    }

    public PrivilegedAction aA(String str) {
        this.ds = str;
        return this;
    }

    /* renamed from: f */
    public String run() {
        return Security.getProperty(this.ds);
    }
}
