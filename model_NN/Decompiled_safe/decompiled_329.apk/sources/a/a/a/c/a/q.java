package a.a.a.c.a;

import a.a.a.c.j.a.a;

public final class q {
    private static final byte[] BG = {Byte.MIN_VALUE, 64, 32, 16, 8, 4, 2, 1};
    private static final byte[] BH = {Byte.MAX_VALUE, -65, -33, -17, -9, -5, -3, -2};
    public final int BI;
    public final byte[] sg;

    public q(byte[] bArr, int i) {
        if (i < 0 || i > 7) {
            throw new IllegalArgumentException(a.getString("security.13D"));
        } else if (bArr.length != 0 || i == 0) {
            this.sg = bArr;
            this.BI = i;
        } else {
            throw new IllegalArgumentException(a.getString("security.13E"));
        }
    }

    public q(boolean[] zArr) {
        this.BI = zArr.length % 8;
        int length = zArr.length / 8;
        this.sg = new byte[(this.BI != 0 ? length + 1 : length)];
        for (int i = 0; i < zArr.length; i++) {
            c(i, zArr[i]);
        }
    }

    public boolean aW(int i) {
        return (BG[i % 8] & this.sg[i / 8]) != 0;
    }

    public void c(int i, boolean z) {
        int i2 = i % 8;
        int i3 = i / 8;
        if (z) {
            byte[] bArr = this.sg;
            bArr[i3] = (byte) (BG[i2] | bArr[i3]);
            return;
        }
        byte[] bArr2 = this.sg;
        bArr2[i3] = (byte) (BH[i2] & bArr2[i3]);
    }

    public boolean[] iR() {
        boolean[] zArr = new boolean[((this.sg.length * 8) - this.BI)];
        for (int i = 0; i < zArr.length; i++) {
            zArr[i] = aW(i);
        }
        return zArr;
    }
}
