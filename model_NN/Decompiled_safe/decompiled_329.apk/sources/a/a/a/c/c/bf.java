package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.e;
import a.a.a.c.a.n;
import a.a.a.c.f.h;

public class bf {
    public static final e Yv = new ag(new am[]{new n(0, bl.lG), new n(1, h.aCy)});
    /* access modifiers changed from: private */
    public final bl bgv;
    /* access modifiers changed from: private */
    public final h bgw;

    public bf(bl blVar) {
        this.bgv = blVar;
        this.bgw = null;
    }

    public bf(h hVar) {
        this.bgv = null;
        this.bgw = hVar;
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str);
        stringBuffer.append("Distribution Point Name: [\n");
        if (this.bgv != null) {
            this.bgv.a(stringBuffer, str + "  ");
        } else {
            stringBuffer.append(str);
            stringBuffer.append("  ");
            stringBuffer.append(this.bgw.getName("RFC2253"));
        }
        stringBuffer.append(str);
        stringBuffer.append("]\n");
    }
}
