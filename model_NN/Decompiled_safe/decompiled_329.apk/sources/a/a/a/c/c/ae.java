package a.a.a.c.c;

import a.a.a.c.a.am;
import java.util.ArrayList;
import java.util.List;

public class ae {
    public static final am lG = new at(bk.en);
    /* access modifiers changed from: private */
    public List aac;
    private byte[] dV;

    public ae() {
    }

    public ae(List list) {
        this.aac = list;
    }

    public ae a(bk bkVar) {
        this.dV = null;
        if (this.aac == null) {
            this.aac = new ArrayList();
        }
        this.aac.add(bkVar);
        return this;
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = lG.S(this);
        }
        return this.dV;
    }

    public List uq() {
        return this.aac;
    }
}
