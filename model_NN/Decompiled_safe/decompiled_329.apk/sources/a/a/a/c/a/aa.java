package a.a.a.c.a;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class aa extends an {
    private static final aa adk = new aa();
    private static final String adl = "yyyyMMddHHmmss.SSS";

    public aa() {
        super(24);
    }

    public static aa uB() {
        return adk;
    }

    public Object a(ad adVar) {
        adVar.xm();
        if (adVar.avc) {
            return null;
        }
        return b(adVar);
    }

    public void a(at atVar) {
        atVar.Ok();
    }

    public void b(at atVar) {
        int length;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(adl);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String format = simpleDateFormat.format(atVar.auW);
        while (true) {
            length = format.length() - 1;
            int lastIndexOf = format.lastIndexOf(48, length);
            if (!(lastIndexOf != -1) || !(lastIndexOf == length)) {
                break;
            }
            format = format.substring(0, lastIndexOf);
        }
        if (format.charAt(length) == '.') {
            format = format.substring(0, length);
        }
        atVar.auW = (format + "Z").getBytes();
        atVar.length = ((byte[]) atVar.auW).length;
    }
}
