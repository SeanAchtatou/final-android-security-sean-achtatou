package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.ar;

public class n extends ap {
    public static final byte Bd = 0;
    public static final byte Be = 1;
    public static final byte Bf = 2;
    public static final byte Bg = 3;
    public static final byte Bh = 4;
    public static final byte Bi = 5;
    public static final byte Bj = 6;
    public static final byte Bk = 8;
    public static final byte Bl = 9;
    public static final byte Bm = 10;
    public static final am lG = ar.MY();
    private final byte Bn;

    public n(byte b2) {
        this.Bn = b2;
    }

    public n(byte[] bArr) {
        super(bArr);
        this.Bn = ((byte[]) lG.ax(bArr))[0];
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("Reason Code: [ ");
        switch (this.Bn) {
            case 0:
                stringBuffer.append("unspecified");
                break;
            case 1:
                stringBuffer.append("keyCompromise");
                break;
            case 2:
                stringBuffer.append("cACompromise");
                break;
            case 3:
                stringBuffer.append("affiliationChanged");
                break;
            case 4:
                stringBuffer.append("superseded");
                break;
            case 5:
                stringBuffer.append("cessationOfOperation");
                break;
            case 6:
                stringBuffer.append("certificateHold");
                break;
            case 8:
                stringBuffer.append("removeFromCRL");
                break;
            case 9:
                stringBuffer.append("privilegeWithdrawn");
                break;
            case 10:
                stringBuffer.append("aACompromise");
                break;
        }
        stringBuffer.append(" ]\n");
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = lG.S(new byte[]{this.Bn});
        }
        return this.dV;
    }

    public int iM() {
        return this.Bn;
    }
}
