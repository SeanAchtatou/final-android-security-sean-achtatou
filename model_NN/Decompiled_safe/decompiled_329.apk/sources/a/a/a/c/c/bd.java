package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.j.a.a;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class bd extends ap {
    public static final am lG = new bx(k.en);
    /* access modifiers changed from: private */
    public final List aYO;

    public bd(List list) {
        this(list, null);
    }

    private bd(List list, byte[] bArr) {
        if (list == null || list.isEmpty()) {
            throw new IOException(a.getString("security.1A3"));
        }
        this.aYO = list;
        this.dV = bArr;
    }

    /* synthetic */ bd(List list, byte[] bArr, bx bxVar) {
        this(list, bArr);
    }

    public static bd Y(byte[] bArr) {
        return (bd) lG.ax(bArr);
    }

    public List AK() {
        return new ArrayList(this.aYO);
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("AccessDescriptions:\n");
        if (this.aYO == null || this.aYO.isEmpty()) {
            stringBuffer.append("NULL\n");
            return;
        }
        for (Object obj : this.aYO) {
            stringBuffer.append(obj.toString());
        }
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = lG.S(this);
        }
        return this.dV;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("\n---- InfoAccessSyntax:");
        if (this.aYO != null) {
            for (Object append : this.aYO) {
                stringBuffer.append(10);
                stringBuffer.append(append);
            }
        }
        stringBuffer.append("\n---- InfoAccessSyntax END\n");
        return stringBuffer.toString();
    }
}
