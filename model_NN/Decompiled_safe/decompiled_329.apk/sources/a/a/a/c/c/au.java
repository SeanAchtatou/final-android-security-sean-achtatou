package a.a.a.c.c;

import a.a.a.c.a.ad;
import a.a.a.c.a.am;
import a.a.a.c.a.v;
import java.math.BigInteger;
import java.util.Date;

final class au extends v {
    au(am[] amVarArr) {
        super(amVarArr);
        eU(2);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object[] objArr) {
        bn bnVar = (bn) obj;
        objArr[0] = bnVar.btQ.toByteArray();
        objArr[1] = bnVar.btR;
        objArr[2] = bnVar.btS;
    }

    /* access modifiers changed from: protected */
    public Object b(ad adVar) {
        Object[] objArr = (Object[]) adVar.auW;
        return new bn(new BigInteger((byte[]) objArr[0]), (Date) objArr[1], (br) objArr[2]);
    }
}
