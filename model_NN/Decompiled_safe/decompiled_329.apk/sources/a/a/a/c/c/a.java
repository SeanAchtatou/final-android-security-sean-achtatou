package a.a.a.c.c;

import java.security.PublicKey;

public class a implements PublicKey {
    private final String df;
    private final byte[] dg;
    private final byte[] dh;

    public a(String str, byte[] bArr, byte[] bArr2) {
        this.df = str;
        this.dg = bArr;
        this.dh = bArr2;
    }

    public String getAlgorithm() {
        return this.df;
    }

    public byte[] getEncoded() {
        return this.dg;
    }

    public String getFormat() {
        return "X.509";
    }

    public String toString() {
        return "algorithm = " + this.df + ", params unparsed, unparsed keybits = \n";
    }
}
