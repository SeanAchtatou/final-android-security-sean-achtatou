package a.a.a.c.e.a;

import a.a.a.c.a.am;
import a.a.a.c.a.aq;
import a.a.a.c.a.n;
import a.a.a.c.a.o;
import a.a.a.c.a.r;
import a.a.a.c.a.v;
import a.a.a.c.c.bp;
import a.a.a.c.d.h;
import a.a.a.c.d.i;
import a.a.a.c.j.a.a;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertPath;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class l extends CertPath {
    public static final aq aCz = new e(r.iS());
    public static final int bQG = 0;
    public static final int bQH = 1;
    private static final String[] bQI = {"PkiPath", "PKCS7"};
    static final List bQJ = Collections.unmodifiableList(Arrays.asList(bQI));
    private static final v bQM = new c(new am[]{r.iS(), new n(0, aCz), r.iS()});
    private static final v bQN = new d(new am[]{r.iS(), new o(0, bQM)});
    private static final long serialVersionUID = 7989755106209515436L;
    /* access modifiers changed from: private */
    public final List aYF;
    private byte[] bQK;
    private byte[] bQL;

    public l(List list) {
        super("X.509");
        int size = list.size();
        this.aYF = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            Object obj = list.get(i);
            if (!(obj instanceof X509Certificate)) {
                throw new CertificateException(a.getString("security.15D"));
            }
            this.aYF.add(obj);
        }
    }

    private l(List list, int i, byte[] bArr) {
        super("X.509");
        if (i == 0) {
            this.bQK = bArr;
        } else {
            this.bQL = bArr;
        }
        this.aYF = list;
    }

    /* synthetic */ l(List list, int i, byte[] bArr, e eVar) {
        this(list, i, bArr);
    }

    public static l aJ(byte[] bArr) {
        try {
            return (l) aCz.ax(bArr);
        } catch (IOException e) {
            throw new CertificateException(a.c("security.15E", e.getMessage()));
        }
    }

    public static l c(InputStream inputStream, String str) {
        if (!bQJ.contains(str)) {
            throw new CertificateException(a.c("security.15F", str));
        }
        try {
            if (bQI[0].equals(str)) {
                return (l) aCz.h(inputStream);
            }
            i iVar = (i) i.en.h(inputStream);
            h Df = iVar.Df();
            if (Df == null) {
                throw new CertificateException(a.getString("security.160"));
            }
            List certificates = Df.getCertificates();
            ArrayList arrayList = certificates == null ? new ArrayList() : certificates;
            ArrayList arrayList2 = new ArrayList();
            for (int i = 0; i < arrayList.size(); i++) {
                arrayList2.add(new g((bp) arrayList.get(i)));
            }
            return new l(arrayList2, 1, iVar.getEncoded());
        } catch (IOException e) {
            throw new CertificateException(a.c("security.15E", e.getMessage()));
        }
    }

    public static l c(byte[] bArr, String str) {
        if (!bQJ.contains(str)) {
            throw new CertificateException(a.c("security.15F", str));
        }
        try {
            if (bQI[0].equals(str)) {
                return (l) aCz.ax(bArr);
            }
            i iVar = (i) i.en.ax(bArr);
            h Df = iVar.Df();
            if (Df == null) {
                throw new CertificateException(a.getString("security.160"));
            }
            List certificates = Df.getCertificates();
            ArrayList arrayList = certificates == null ? new ArrayList() : certificates;
            ArrayList arrayList2 = new ArrayList();
            for (int i = 0; i < arrayList.size(); i++) {
                arrayList2.add(new g((bp) arrayList.get(i)));
            }
            return new l(arrayList2, 1, iVar.getEncoded());
        } catch (IOException e) {
            throw new CertificateException(a.c("security.15E", e.getMessage()));
        }
    }

    public static l j(InputStream inputStream) {
        try {
            return (l) aCz.h(inputStream);
        } catch (IOException e) {
            throw new CertificateException(a.c("security.15E", e.getMessage()));
        }
    }

    public List getCertificates() {
        return Collections.unmodifiableList(this.aYF);
    }

    public byte[] getEncoded() {
        if (this.bQK == null) {
            this.bQK = aCz.S(this);
        }
        byte[] bArr = new byte[this.bQK.length];
        System.arraycopy(this.bQK, 0, bArr, 0, this.bQK.length);
        return bArr;
    }

    public byte[] getEncoded(String str) {
        if (!bQJ.contains(str)) {
            throw new CertificateEncodingException(a.c("security.15F", str));
        } else if (bQI[0].equals(str)) {
            return getEncoded();
        } else {
            if (this.bQL == null) {
                this.bQL = bQN.S(this);
            }
            byte[] bArr = new byte[this.bQL.length];
            System.arraycopy(this.bQL, 0, bArr, 0, this.bQL.length);
            return bArr;
        }
    }

    public Iterator getEncodings() {
        return bQJ.iterator();
    }
}
