package a.a.a.c.a;

public class ag extends ah {
    private static final ag baQ = new ag();

    public ag() {
        super(4);
    }

    public static ag Bl() {
        return baQ;
    }

    public Object a(ad adVar) {
        adVar.xp();
        if (adVar.avc) {
            return null;
        }
        return b(adVar);
    }

    public void a(at atVar) {
        atVar.On();
    }

    public Object b(ad adVar) {
        byte[] bArr = new byte[adVar.length];
        System.arraycopy(adVar.buffer, adVar.auY, bArr, 0, adVar.length);
        return bArr;
    }

    public void b(at atVar) {
        atVar.length = ((byte[]) atVar.auW).length;
    }
}
