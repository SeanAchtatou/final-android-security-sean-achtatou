package a.a.a.c.e.a;

import a.a.a.c.a.ad;
import a.a.a.c.a.am;
import a.a.a.c.a.c;
import a.a.a.c.a.v;
import a.a.a.c.d.i;

final class d extends v {
    private final byte[] oB = c.bb().S(i.bkl);

    d(am[] amVarArr) {
        super(amVarArr);
    }

    public Object a(ad adVar) {
        throw new RuntimeException("Invalid use of encoder for PKCS#7 SignedData object");
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object[] objArr) {
        objArr[0] = this.oB;
        objArr[1] = obj;
    }
}
