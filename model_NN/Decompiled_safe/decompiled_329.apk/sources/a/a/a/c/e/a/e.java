package a.a.a.c.e.a;

import a.a.a.c.a.ad;
import a.a.a.c.a.am;
import a.a.a.c.a.aq;
import a.a.a.c.c.bp;
import a.a.a.c.j.a.a;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final class e extends aq {
    e(am amVar) {
        super(amVar);
    }

    public Object b(ad adVar) {
        List list = (List) adVar.auW;
        int size = list.size();
        ArrayList arrayList = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            arrayList.add(new g((bp) bp.en.ax((byte[]) list.get(i))));
        }
        return new l(arrayList, 0, adVar.getEncoded(), null);
    }

    public Collection b(Object obj) {
        l lVar = (l) obj;
        if (lVar.aYF == null) {
            return new ArrayList();
        }
        int size = lVar.aYF.size();
        ArrayList arrayList = new ArrayList(size);
        int i = 0;
        while (i < size) {
            try {
                arrayList.add(((X509Certificate) lVar.aYF.get(i)).getEncoded());
                i++;
            } catch (CertificateEncodingException e) {
                throw new IllegalArgumentException(a.getString("security.161"));
            }
        }
        return arrayList;
    }
}
