package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.aq;
import a.a.a.c.a.as;
import a.a.a.c.a.o;
import a.a.a.c.a.v;
import a.a.a.c.f.h;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ao {
    public static final v en = new cc(new am[]{as.NW(), al.en, h.aCz, bq.Yv, bq.Yv, new aq(bn.en), new o(0, br.lG)});
    /* access modifiers changed from: private */
    public final Date aCq;
    /* access modifiers changed from: private */
    public final Date aCr;
    /* access modifiers changed from: private */
    public final List aCs;
    /* access modifiers changed from: private */
    public final br aCt;
    private byte[] dV;
    /* access modifiers changed from: private */
    public final int tN;
    /* access modifiers changed from: private */
    public final al vc;
    /* access modifiers changed from: private */
    public final h vd;

    public ao(int i, al alVar, h hVar, Date date, Date date2, List list, br brVar) {
        this.tN = i;
        this.vc = alVar;
        this.vd = hVar;
        this.aCq = date;
        this.aCr = date2;
        this.aCs = list;
        this.aCt = brVar;
    }

    private ao(int i, al alVar, h hVar, Date date, Date date2, List list, br brVar, byte[] bArr) {
        this.tN = i;
        this.vc = alVar;
        this.vd = hVar;
        this.aCq = date;
        this.aCr = date2;
        this.aCs = list;
        this.aCt = brVar;
        this.dV = bArr;
    }

    /* synthetic */ ao(int i, al alVar, h hVar, Date date, Date date2, List list, br brVar, byte[] bArr, cc ccVar) {
        this(i, alVar, hVar, date, date2, list, brVar, bArr);
    }

    public ao(al alVar, h hVar, Date date) {
        this.tN = 1;
        this.vc = alVar;
        this.vd = hVar;
        this.aCq = date;
        this.aCr = null;
        this.aCs = null;
        this.aCt = null;
    }

    public void b(StringBuffer stringBuffer) {
        stringBuffer.append("X.509 CRL v").append(this.tN);
        stringBuffer.append("\nSignature Algorithm: [");
        this.vc.b(stringBuffer);
        stringBuffer.append(']');
        stringBuffer.append("\nIssuer: ").append(this.vd.getName("RFC2253"));
        stringBuffer.append("\n\nThis Update: ").append(this.aCq);
        stringBuffer.append("\nNext Update: ").append(this.aCr).append(10);
        if (this.aCs != null) {
            stringBuffer.append("\nRevoked Certificates: ").append(this.aCs.size()).append(" [");
            int i = 1;
            for (bn a2 : this.aCs) {
                stringBuffer.append("\n  [").append(i).append(']');
                a2.a(stringBuffer, "  ");
                stringBuffer.append(10);
                i++;
            }
            stringBuffer.append("]\n");
        }
        if (this.aCt != null) {
            stringBuffer.append("\nCRL Extensions: ").append(this.aCt.size()).append(" [");
            this.aCt.a(stringBuffer, "  ");
            stringBuffer.append("]\n");
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ao)) {
            return false;
        }
        ao aoVar = (ao) obj;
        return this.tN == aoVar.tN && this.vc.equals(aoVar.vc) && Arrays.equals(this.vd.getEncoded(), aoVar.vd.getEncoded()) && this.aCq.getTime() / 1000 == aoVar.aCq.getTime() / 1000 && (this.aCr != null ? this.aCr.getTime() / 1000 == aoVar.aCr.getTime() / 1000 : aoVar.aCr == null) && (((this.aCs == null || aoVar.aCs == null) && this.aCs == aoVar.aCs) || (this.aCs.containsAll(aoVar.aCs) && this.aCs.size() == aoVar.aCs.size())) && (this.aCt != null ? this.aCt.equals(aoVar.aCt) : aoVar.aCt == null);
    }

    public al fM() {
        return this.vc;
    }

    public h fN() {
        return this.vd;
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }

    public Date getNextUpdate() {
        return this.aCr;
    }

    public Date getThisUpdate() {
        return this.aCq;
    }

    public int getVersion() {
        return this.tN;
    }

    public int hashCode() {
        return (((((this.tN * 37) + this.vc.hashCode()) * 37) + this.vd.getEncoded().hashCode()) * 37) + (((int) this.aCq.getTime()) / 1000);
    }

    public List yE() {
        return this.aCs;
    }

    public br yF() {
        return this.aCt;
    }
}
