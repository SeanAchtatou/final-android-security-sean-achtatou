package a.a.a.c;

import java.security.PrivateKey;

public class g implements PrivateKey {
    private static final long serialVersionUID = 7776497482533790279L;
    private byte[] dV;
    private String df;

    public g(String str) {
        this.df = str;
    }

    public void R(byte[] bArr) {
        this.dV = new byte[bArr.length];
        System.arraycopy(bArr, 0, this.dV, 0, bArr.length);
    }

    public void dk(String str) {
        this.df = str;
    }

    public String getAlgorithm() {
        return this.df;
    }

    public byte[] getEncoded() {
        byte[] bArr = new byte[this.dV.length];
        System.arraycopy(this.dV, 0, bArr, 0, this.dV.length);
        return bArr;
    }

    public String getFormat() {
        return "PKCS#8";
    }
}
