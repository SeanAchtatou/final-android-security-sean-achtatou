package a.a.a.c.f;

import a.a.a.c.a.ap;
import a.a.a.c.c.ah;
import com.uc.c.r;
import java.io.IOException;

public class c {
    public byte[] dg;
    public final boolean sd = false;
    public String se;
    private String sf;
    public byte[] sg;
    public boolean sh;
    public String si;
    private int tag = -1;

    public c(String str, boolean z) {
        this.sh = z;
        this.si = str;
        this.se = H(this.si);
    }

    public c(String str, byte[] bArr) {
        this.sf = str;
        this.dg = bArr;
        try {
            ap apVar = new ap(bArr);
            this.tag = apVar.tag;
            if (f.Yv.v(this.tag)) {
                this.si = (String) f.Yv.a(apVar);
                this.se = H(this.si);
                return;
            }
            this.si = str;
            this.se = str;
        } catch (IOException e) {
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
            illegalArgumentException.initCause(e);
            throw illegalArgumentException;
        }
    }

    public c(String str, byte[] bArr, int i) {
        this.dg = bArr;
        this.tag = i;
        if (str == null) {
            this.si = eI();
            this.se = this.sf;
            return;
        }
        this.si = str;
        this.se = H(str);
    }

    private String H(String str) {
        int length = str.length();
        if (length == 0) {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer(length * 2);
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            switch (charAt) {
                case ' ':
                    if (i == 0 || i == length - 1) {
                        stringBuffer.append('\\');
                    }
                    stringBuffer.append(' ');
                    continue;
                case '\"':
                case '\\':
                    this.sh = true;
                case '#':
                case '+':
                case ',':
                case ';':
                case '<':
                case '=':
                case '>':
                    stringBuffer.append('\\');
                    break;
            }
            stringBuffer.append(charAt);
        }
        return stringBuffer.toString();
    }

    public void a(StringBuffer stringBuffer) {
        stringBuffer.append('\"');
        if (this.sh) {
            for (int i = 0; i < this.si.length(); i++) {
                char charAt = this.si.charAt(i);
                if (charAt == '\"' || charAt == '\\') {
                    stringBuffer.append('\\');
                }
                stringBuffer.append(charAt);
            }
        } else {
            stringBuffer.append(this.si);
        }
        stringBuffer.append('\"');
    }

    public int eH() {
        if (this.tag == -1) {
            if (ah.cK(this.si)) {
                this.tag = a.a.a.c.a.ah.bgB.id;
            } else {
                this.tag = a.a.a.c.a.ah.bgE.id;
            }
        }
        return this.tag;
    }

    public String eI() {
        if (this.sf == null) {
            if (!this.sd) {
                if (ah.cK(this.si)) {
                    this.dg = a.a.a.c.a.ah.bgB.S(this.si);
                } else {
                    this.dg = a.a.a.c.a.ah.bgE.S(this.si);
                }
            }
            StringBuffer stringBuffer = new StringBuffer((this.dg.length * 2) + 1);
            stringBuffer.append('#');
            for (int i = 0; i < this.dg.length; i++) {
                int i2 = (this.dg[i] >> 4) & 15;
                if (i2 < 10) {
                    stringBuffer.append((char) (i2 + 48));
                } else {
                    stringBuffer.append((char) (i2 + 87));
                }
                byte b2 = this.dg[i] & 15;
                if (b2 < 10) {
                    stringBuffer.append((char) (b2 + r.Eh));
                } else {
                    stringBuffer.append((char) (b2 + 87));
                }
            }
            this.sf = stringBuffer.toString();
        }
        return this.sf;
    }

    public String eJ() {
        int i;
        int length = this.si.length();
        if (length == 0) {
            return this.si;
        }
        StringBuffer stringBuffer = new StringBuffer(length * 2);
        if (this.si.charAt(0) == '#') {
            stringBuffer.append('\\');
            stringBuffer.append('#');
            i = 0 + 1;
        } else {
            i = 0;
        }
        while (i < length) {
            char charAt = this.si.charAt(i);
            switch (charAt) {
                case ' ':
                    int length2 = stringBuffer.length();
                    if (!(length2 == 0 || stringBuffer.charAt(length2 - 1) == ' ')) {
                        stringBuffer.append(' ');
                    }
                    i++;
                case '\"':
                case '+':
                case ',':
                case ';':
                case '<':
                case '>':
                case '\\':
                    stringBuffer.append('\\');
                    break;
            }
            stringBuffer.append(charAt);
            i++;
        }
        int length3 = stringBuffer.length() - 1;
        while (length3 > -1 && stringBuffer.charAt(length3) == ' ') {
            length3--;
        }
        stringBuffer.setLength(length3 + 1);
        return stringBuffer.toString();
    }
}
