package a.a.a.c.a;

import a.a.a.c.j.a.a;
import java.util.Arrays;

public final class aj {
    private final int[] aG;
    private String aI;

    public aj(String str) {
        this.aG = eD(str);
        this.aI = str;
    }

    public aj(int[] iArr) {
        i(iArr);
        this.aG = iArr;
    }

    public static int[] eD(String str) {
        if (str == null) {
            throw new IllegalArgumentException(a.getString("security.9D"));
        }
        int length = str.length();
        if (length == 0) {
            throw new IllegalArgumentException(a.getString("security.9E"));
        }
        boolean z = true;
        int i = 1;
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = str.charAt(i2);
            if (charAt == '.') {
                if (z) {
                    throw new IllegalArgumentException(a.getString("security.9E"));
                }
                i++;
                z = true;
            } else if (charAt < '0' || charAt > '9') {
                throw new IllegalArgumentException(a.getString("security.9E"));
            } else {
                z = false;
            }
        }
        if (z) {
            throw new IllegalArgumentException(a.getString("security.9E"));
        } else if (i < 2) {
            throw new IllegalArgumentException(a.getString("security.99"));
        } else {
            int[] iArr = new int[i];
            int i3 = 0;
            for (int i4 = 0; i4 < length; i4++) {
                char charAt2 = str.charAt(i4);
                if (charAt2 == '.') {
                    i3++;
                } else {
                    iArr[i3] = (charAt2 + (iArr[i3] * 10)) - 48;
                }
            }
            if (iArr[0] > 2) {
                throw new IllegalArgumentException(a.getString("security.9A"));
            } else if (iArr[0] == 2 || iArr[1] <= 39) {
                return iArr;
            } else {
                throw new IllegalArgumentException(a.getString("security.9B"));
            }
        }
    }

    public static void i(int[] iArr) {
        if (iArr == null) {
            throw new IllegalArgumentException(a.getString("security.98"));
        } else if (iArr.length < 2) {
            throw new IllegalArgumentException(a.getString("security.99"));
        } else if (iArr[0] > 2) {
            throw new IllegalArgumentException(a.getString("security.9A"));
        } else if (iArr[0] == 2 || iArr[1] <= 39) {
            for (int i : iArr) {
                if (i < 0) {
                    throw new IllegalArgumentException(a.getString("security.9C"));
                }
            }
        } else {
            throw new IllegalArgumentException(a.getString("security.9B"));
        }
    }

    public static String toString(int[] iArr) {
        StringBuffer stringBuffer = new StringBuffer(iArr.length * 3);
        for (int i = 0; i < iArr.length - 1; i++) {
            stringBuffer.append(iArr[i]);
            stringBuffer.append('.');
        }
        stringBuffer.append(iArr[iArr.length - 1]);
        return stringBuffer.toString();
    }

    public int[] aX() {
        return this.aG;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return Arrays.equals(this.aG, ((aj) obj).aG);
    }

    public int hashCode() {
        int i = 0;
        int i2 = 0;
        while (i < this.aG.length && i < 4) {
            i2 += this.aG[i] << (i * 8);
            i++;
        }
        return Integer.MAX_VALUE & i2;
    }

    public String toString() {
        if (this.aI == null) {
            this.aI = toString(this.aG);
        }
        return this.aI;
    }
}
