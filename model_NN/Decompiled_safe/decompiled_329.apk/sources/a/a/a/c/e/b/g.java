package a.a.a.c.e.b;

import a.a.a.c.a.as;
import a.a.a.c.b.b;
import a.a.a.c.c.al;
import a.a.a.c.c.bi;
import a.a.a.c.e;
import a.a.a.c.j.a.a;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.math.BigInteger;
import java.security.interfaces.DSAParams;
import java.security.interfaces.DSAPublicKey;
import java.security.spec.DSAParameterSpec;
import java.security.spec.DSAPublicKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class g extends e implements DSAPublicKey {
    private static final long serialVersionUID = -2279672131310978336L;
    private BigInteger RY;
    private BigInteger RZ;
    private BigInteger Sa;
    private transient DSAParams Sb;
    private BigInteger bnx;

    public g(DSAPublicKeySpec dSAPublicKeySpec) {
        super("DSA");
        this.RZ = dSAPublicKeySpec.getP();
        this.Sa = dSAPublicKeySpec.getQ();
        this.RY = dSAPublicKeySpec.getG();
        al alVar = new al(b.al("DSA"), new i(this.RZ.toByteArray(), this.Sa.toByteArray(), this.RY.toByteArray()).getEncoded());
        this.bnx = dSAPublicKeySpec.getY();
        R(new bi(alVar, as.NW().S(this.bnx.toByteArray())).getEncoded());
        this.Sb = new DSAParameterSpec(this.RZ, this.Sa, this.RY);
    }

    public g(X509EncodedKeySpec x509EncodedKeySpec) {
        super("DSA");
        byte[] encoded = x509EncodedKeySpec.getEncoded();
        try {
            bi biVar = (bi) bi.en.ax(encoded);
            try {
                this.bnx = new BigInteger((byte[]) as.NW().ax(biVar.Ek()));
                al kV = biVar.kV();
                try {
                    i iVar = (i) i.en.ax(kV.xz());
                    this.RZ = new BigInteger(iVar.bQU);
                    this.Sa = new BigInteger(iVar.bQV);
                    this.RY = new BigInteger(iVar.bQW);
                    this.Sb = new DSAParameterSpec(this.RZ, this.Sa, this.RY);
                    R(encoded);
                    String algorithm = kV.getAlgorithm();
                    String am = b.am(algorithm);
                    dk(am != null ? am : algorithm);
                } catch (IOException e) {
                    throw new InvalidKeySpecException(a.c("security.19B", e));
                }
            } catch (IOException e2) {
                throw new InvalidKeySpecException(a.c("security.19B", e2));
            }
        } catch (IOException e3) {
            throw new InvalidKeySpecException(a.c("security.19A", e3));
        }
    }

    private void a(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        this.Sb = new DSAParameterSpec(this.RZ, this.Sa, this.RY);
    }

    public DSAParams getParams() {
        return this.Sb;
    }

    public BigInteger getY() {
        return this.bnx;
    }
}
