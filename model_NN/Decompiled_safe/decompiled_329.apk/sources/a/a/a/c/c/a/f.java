package a.a.a.c.c.a;

import a.a.a.c.a.ad;
import a.a.a.c.a.am;
import a.a.a.c.a.as;
import a.a.a.c.a.q;
import a.a.a.c.a.v;
import java.math.BigInteger;
import java.util.List;

final class f extends v {
    f(am[] amVarArr) {
        super(amVarArr);
        eU(1);
        eU(2);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object[] objArr) {
        e eVar = (e) obj;
        objArr[0] = BigInteger.valueOf((long) eVar.Sd.getStatus()).toByteArray();
        objArr[1] = eVar.Se;
        if (eVar.Sf != null) {
            boolean[] zArr = new boolean[a.bY()];
            zArr[eVar.Sf.bX()] = true;
            objArr[2] = new q(zArr);
            return;
        }
        objArr[2] = null;
    }

    /* access modifiers changed from: protected */
    public Object b(ad adVar) {
        int i;
        Object[] objArr = (Object[]) adVar.auW;
        if (objArr[2] != null) {
            boolean[] iR = ((q) objArr[2]).iR();
            int i2 = 0;
            while (true) {
                if (i2 >= iR.length) {
                    break;
                } else if (iR[i2]) {
                    i = i2;
                    break;
                } else {
                    i2++;
                }
            }
            return new e(i.ez(as.U(objArr[0])), (List) objArr[1], a.u(i));
        }
        i = -1;
        return new e(i.ez(as.U(objArr[0])), (List) objArr[1], a.u(i));
    }
}
