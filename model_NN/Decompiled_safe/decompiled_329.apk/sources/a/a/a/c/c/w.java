package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.as;
import java.math.BigInteger;

public class w extends ap {
    public static final am lG = as.NW();
    private final BigInteger Sk;

    public w(BigInteger bigInteger) {
        this.Sk = bigInteger;
    }

    public w(byte[] bArr) {
        super(bArr);
        this.Sk = new BigInteger((byte[]) lG.ax(bArr));
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("CRL Number: [ ").append(this.Sk).append(" ]\n");
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = lG.S(this.Sk.toByteArray());
        }
        return this.dV;
    }

    public BigInteger rf() {
        return this.Sk;
    }
}
