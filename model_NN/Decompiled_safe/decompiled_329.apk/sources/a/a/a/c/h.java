package a.a.a.c;

import a.a.a.c.j.a.a;
import com.uc.browser.UCR;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class h {
    /* access modifiers changed from: protected */
    public StreamTokenizer a(StreamTokenizer streamTokenizer) {
        streamTokenizer.slashSlashComments(true);
        streamTokenizer.slashStarComments(true);
        streamTokenizer.wordChars(95, 95);
        streamTokenizer.wordChars(36, 36);
        return streamTokenizer;
    }

    public void a(Reader reader, Collection collection, List list) {
        StreamTokenizer a2 = a(new StreamTokenizer(reader));
        while (true) {
            switch (a2.nextToken()) {
                case -3:
                    if (!j.aa("keystore", a2.sval)) {
                        if (!j.aa("grant", a2.sval)) {
                            a(a2, a.getString("security.89"));
                            break;
                        } else {
                            collection.add(c(a2));
                            break;
                        }
                    } else {
                        list.add(b(a2));
                        break;
                    }
                case -1:
                    return;
                case 59:
                    break;
                default:
                    g(a2);
                    break;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(StreamTokenizer streamTokenizer, String str) {
        throw new c(a.c("security.8F", f(streamTokenizer), str));
    }

    /* access modifiers changed from: protected */
    public b b(StreamTokenizer streamTokenizer) {
        b bVar = new b();
        if (streamTokenizer.nextToken() == 34) {
            bVar.GP = streamTokenizer.sval;
            if (streamTokenizer.nextToken() == 34 || (streamTokenizer.ttype == 44 && streamTokenizer.nextToken() == 34)) {
                bVar.type = streamTokenizer.sval;
            } else {
                streamTokenizer.pushBack();
            }
        } else {
            a(streamTokenizer, a.getString("security.8A"));
        }
        return bVar;
    }

    /* access modifiers changed from: protected */
    public k c(StreamTokenizer streamTokenizer) {
        k kVar = new k();
        while (true) {
            switch (streamTokenizer.nextToken()) {
                case -3:
                    if (!j.aa("signedby", streamTokenizer.sval)) {
                        if (!j.aa("codebase", streamTokenizer.sval)) {
                            if (!j.aa("principal", streamTokenizer.sval)) {
                                g(streamTokenizer);
                                break;
                            } else {
                                kVar.a(d(streamTokenizer));
                                break;
                            }
                        } else if (streamTokenizer.nextToken() != 34) {
                            a(streamTokenizer, a.getString("security.8C"));
                            break;
                        } else {
                            kVar.bBX = streamTokenizer.sval;
                            break;
                        }
                    } else if (streamTokenizer.nextToken() != 34) {
                        a(streamTokenizer, a.getString("security.8B"));
                        break;
                    } else {
                        kVar.bBW = streamTokenizer.sval;
                        break;
                    }
                case 44:
                    break;
                case 123:
                    kVar.asE = e(streamTokenizer);
                    break;
                default:
                    streamTokenizer.pushBack();
                    break;
            }
        }
        return kVar;
    }

    /* access modifiers changed from: protected */
    public i d(StreamTokenizer streamTokenizer) {
        i iVar = new i();
        if (streamTokenizer.nextToken() == -3) {
            iVar.GA = streamTokenizer.sval;
            streamTokenizer.nextToken();
        } else if (streamTokenizer.ttype == 42) {
            iVar.GA = "*";
            streamTokenizer.nextToken();
        }
        if (streamTokenizer.ttype == 34) {
            iVar.name = streamTokenizer.sval;
        } else if (streamTokenizer.ttype == 42) {
            iVar.name = "*";
        } else {
            a(streamTokenizer, a.getString("security.8D"));
        }
        return iVar;
    }

    /* access modifiers changed from: protected */
    public Collection e(StreamTokenizer streamTokenizer) {
        HashSet hashSet = new HashSet();
        while (true) {
            switch (streamTokenizer.nextToken()) {
                case -3:
                    if (j.aa("permission", streamTokenizer.sval)) {
                        l lVar = new l();
                        if (streamTokenizer.nextToken() == -3) {
                            lVar.GA = streamTokenizer.sval;
                            if (streamTokenizer.nextToken() == 34) {
                                lVar.name = streamTokenizer.sval;
                                streamTokenizer.nextToken();
                            }
                            if (streamTokenizer.ttype == 44) {
                                streamTokenizer.nextToken();
                            }
                            if (streamTokenizer.ttype == 34) {
                                lVar.bQE = streamTokenizer.sval;
                                if (streamTokenizer.nextToken() == 44) {
                                    streamTokenizer.nextToken();
                                }
                            }
                            if (streamTokenizer.ttype != -3 || !j.aa("signedby", streamTokenizer.sval)) {
                                streamTokenizer.pushBack();
                            } else if (streamTokenizer.nextToken() == 34) {
                                lVar.bBW = streamTokenizer.sval;
                            } else {
                                g(streamTokenizer);
                            }
                            hashSet.add(lVar);
                            break;
                        }
                    }
                    a(streamTokenizer, a.getString("security.8E"));
                    break;
                case 59:
                    break;
                case UCR.Color.bTn /*125*/:
                    return hashSet;
                default:
                    g(streamTokenizer);
                    break;
            }
        }
    }

    /* access modifiers changed from: protected */
    public String f(StreamTokenizer streamTokenizer) {
        return streamTokenizer.toString();
    }

    /* access modifiers changed from: protected */
    public final void g(StreamTokenizer streamTokenizer) {
        throw new c(a.c("security.90", f(streamTokenizer)));
    }
}
