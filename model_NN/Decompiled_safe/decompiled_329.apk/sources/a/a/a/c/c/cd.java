package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.n;
import a.a.a.c.a.o;
import a.a.a.c.a.u;

public class cd extends ap {
    public static final am lG = new by(new am[]{new o(0, bf.Yv), new n(1, u.kZ()), new n(2, u.kZ()), new n(3, ay.uP), new n(4, u.kZ()), new n(5, u.kZ())});
    /* access modifiers changed from: private */
    public boolean cek = false;
    /* access modifiers changed from: private */
    public boolean cel = false;
    /* access modifiers changed from: private */
    public ay cem;
    /* access modifiers changed from: private */
    public boolean cen = false;
    /* access modifiers changed from: private */
    public boolean ceo = false;
    /* access modifiers changed from: private */
    public bf qi;

    public cd(bf bfVar, ay ayVar) {
        this.qi = bfVar;
        this.cem = ayVar;
    }

    public static cd aQ(byte[] bArr) {
        cd cdVar = (cd) lG.ax(bArr);
        cdVar.dV = bArr;
        return cdVar;
    }

    public bf PV() {
        return this.qi;
    }

    public boolean PW() {
        return this.cek;
    }

    public boolean PX() {
        return this.cel;
    }

    public ay PY() {
        return this.cem;
    }

    public boolean PZ() {
        return this.cen;
    }

    public boolean Qa() {
        return this.ceo;
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("Issuing Distribution Point: [\n");
        if (this.qi != null) {
            this.qi.a(stringBuffer, "  " + str);
        }
        stringBuffer.append(str).append("  onlyContainsUserCerts: ").append(this.cek).append(10);
        stringBuffer.append(str).append("  onlyContainsCACerts: ").append(this.cel).append(10);
        if (this.cem != null) {
            this.cem.a(stringBuffer, str + "  ");
        }
        stringBuffer.append(str).append("  indirectCRL: ").append(this.cen).append(10);
        stringBuffer.append(str).append("  onlyContainsAttributeCerts: ").append(this.ceo).append(10);
    }

    public void cM(boolean z) {
        this.cek = z;
    }

    public void cN(boolean z) {
        this.cel = z;
    }

    public void cO(boolean z) {
        this.cen = z;
    }

    public void cP(boolean z) {
        this.ceo = z;
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = lG.S(this);
        }
        return this.dV;
    }
}
