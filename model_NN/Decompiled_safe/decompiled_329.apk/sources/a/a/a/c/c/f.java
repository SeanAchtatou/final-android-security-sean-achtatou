package a.a.a.c.c;

import a.a.a.c.a.ad;
import a.a.a.c.a.am;
import a.a.a.c.a.as;
import a.a.a.c.a.q;
import a.a.a.c.a.v;
import a.a.a.c.f.h;
import java.math.BigInteger;

final class f extends v {
    f(am[] amVarArr) {
        super(amVarArr);
        c(new byte[]{0}, 0);
        eU(7);
        eU(8);
        eU(9);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object[] objArr) {
        m mVar = (m) obj;
        objArr[0] = as.jV(mVar.tN);
        objArr[1] = mVar.vb.toByteArray();
        objArr[2] = mVar.vc;
        objArr[3] = mVar.vd;
        objArr[4] = mVar.ve;
        objArr[5] = mVar.vf;
        objArr[6] = mVar.vg;
        if (mVar.vh != null) {
            objArr[7] = new q(mVar.vh);
        }
        if (mVar.vi != null) {
            objArr[8] = new q(mVar.vi);
        }
        objArr[9] = mVar.dN;
    }

    /* access modifiers changed from: protected */
    public Object b(ad adVar) {
        Object[] objArr = (Object[]) adVar.auW;
        return new m(as.U(objArr[0]), new BigInteger((byte[]) objArr[1]), (al) objArr[2], (h) objArr[3], (v) objArr[4], (h) objArr[5], (bi) objArr[6], objArr[7] == null ? null : ((q) objArr[7]).iR(), objArr[8] == null ? null : ((q) objArr[8]).iR(), (br) objArr[9], adVar.getEncoded(), null);
    }
}
