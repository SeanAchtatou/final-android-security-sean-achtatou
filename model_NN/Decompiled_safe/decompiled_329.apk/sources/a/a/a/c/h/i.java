package a.a.a.c.h;

import a.a.a.c.j.a.a;
import a.a.a.c.k;
import java.security.KeyStore;
import java.security.Principal;

class i implements j {
    private KeyStore aYL;
    private k aYM;
    final /* synthetic */ b aYN;

    i(b bVar) {
        this.aYN = bVar;
    }

    private String b(Principal principal) {
        String name = principal.getClass().getName();
        String name2 = principal.getName();
        return new StringBuffer(name.length() + name2.length() + 5).append(name).append(" \"").append(name2).append("\"").toString();
    }

    public String T(String str, String str2) {
        if ("self".equals(str)) {
            if (this.aYM.bBY == null || this.aYM.bBY.size() == 0) {
                throw new e(a.getString("security.144"));
            }
            StringBuffer stringBuffer = new StringBuffer();
            for (a.a.a.c.i iVar : this.aYM.bBY) {
                if (iVar.GA == null) {
                    try {
                        stringBuffer.append(b(this.aYN.b(this.aYL, iVar.name)));
                    } catch (Exception e) {
                        throw new e(a.c("security.143", iVar.name), e);
                    }
                } else {
                    stringBuffer.append(iVar.GA).append(" \"").append(iVar.name).append("\" ");
                }
            }
            return stringBuffer.toString();
        } else if ("alias".equals(str)) {
            try {
                return b(this.aYN.b(this.aYL, str2));
            } catch (Exception e2) {
                throw new e(a.c("security.143", str2), e2);
            }
        } else {
            throw new e(a.c("security.145", str));
        }
    }

    public i a(k kVar, KeyStore keyStore) {
        this.aYM = kVar;
        this.aYL = keyStore;
        return this;
    }
}
