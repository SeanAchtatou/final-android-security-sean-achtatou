package a.a.a.c.c;

import a.a.a.c.a.ad;
import a.a.a.c.a.am;
import a.a.a.c.a.aq;
import java.util.Collection;
import java.util.List;

final class bx extends aq {
    bx(am amVar) {
        super(amVar);
    }

    public Object b(ad adVar) {
        return new bd((List) adVar.auW, adVar.getEncoded(), null);
    }

    public Collection b(Object obj) {
        return ((bd) obj).aYO;
    }
}
