package a.a.a.c.c;

import a.a.a.c.a.ag;
import a.a.a.c.a.ah;
import a.a.a.c.a.aj;
import a.a.a.c.a.am;
import a.a.a.c.a.c;
import a.a.a.c.a.e;
import a.a.a.c.a.n;
import a.a.a.c.f.h;
import a.a.a.c.j.a.a;
import com.uc.c.au;
import com.uc.c.cd;
import com.uc.c.r;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class be {
    public static final e Yv = new az(new am[]{new n(0, an.en), new n(1, ah.bgz), new n(2, ah.bgz), new n(3, as.en), new n(4, h.aCz), new n(5, aa.en), new n(6, ah.bgz), new n(7, ag.Bl()), new n(8, c.bb())});
    public static final int beA = 6;
    public static final int beB = 7;
    public static final int beC = 8;
    private static am[] beD = new am[9];
    public static final int beu = 0;
    public static final int bev = 1;
    public static final int bew = 2;
    public static final int bex = 3;
    public static final int bey = 4;
    public static final int bez = 5;
    /* access modifiers changed from: private */
    public Object beE;
    private byte[] beF;
    /* access modifiers changed from: private */
    public byte[] dV;
    /* access modifiers changed from: private */
    public int tag;

    static {
        beD[0] = an.en;
        beD[1] = ah.bgz;
        beD[2] = ah.bgz;
        beD[6] = ah.bgz;
        beD[3] = as.en;
        beD[4] = h.aCz;
        beD[5] = aa.en;
        beD[7] = ag.Bl();
        beD[8] = c.bb();
    }

    public be(int i, String str) {
        if (str == null) {
            throw new IOException(a.getString("security.28"));
        }
        this.tag = i;
        switch (i) {
            case 0:
            case 3:
            case 5:
                throw new IOException(a.q("security.180", i));
            case 1:
                this.beE = str;
                return;
            case 2:
                dI(str);
                this.beE = str;
                return;
            case 4:
                this.beE = new h(str);
                return;
            case 6:
                dJ(str);
                this.beE = str;
                return;
            case 7:
                this.beE = dL(str);
                return;
            case 8:
                this.beE = dK(str);
                return;
            default:
                throw new IOException(a.q("security.181", i));
        }
    }

    public be(int i, byte[] bArr) {
        if (bArr == null) {
            throw new NullPointerException(a.getString("security.28"));
        } else if (i < 0 || i > 8) {
            throw new IOException(a.q("security.183", i));
        } else {
            this.tag = i;
            this.beF = new byte[bArr.length];
            System.arraycopy(bArr, 0, this.beF, 0, bArr.length);
            this.beE = beD[i].ax(this.beF);
        }
    }

    public be(aa aaVar) {
        this.tag = 5;
        this.beE = aaVar;
    }

    public be(an anVar) {
        this.tag = 0;
        this.beE = anVar;
    }

    public be(as asVar) {
        this.tag = 3;
        this.beE = asVar;
    }

    public be(h hVar) {
        this.tag = 4;
        this.beE = hVar;
    }

    public be(byte[] bArr) {
        int length = bArr.length;
        if (length == 4 || length == 8 || length == 16 || length == 32) {
            this.tag = 7;
            this.beE = new byte[bArr.length];
            System.arraycopy(bArr, 0, this.beE, 0, bArr.length);
            return;
        }
        throw new IllegalArgumentException(a.getString("security.182"));
    }

    private String ah(byte[] bArr) {
        String str = "";
        for (byte b2 : bArr) {
            String hexString = Integer.toHexString(b2 & 255);
            if (hexString.length() == 1) {
                hexString = "0" + hexString;
            }
            str = str + hexString + " ";
        }
        return str;
    }

    public static String ai(byte[] bArr) {
        if (bArr.length < 9) {
            String str = "";
            int i = 0;
            while (i < bArr.length) {
                str = str + Integer.toString(bArr[i] & 255);
                if (i != bArr.length - 1) {
                    str = str + (i == 3 ? au.aGF : ".");
                }
                i++;
            }
            return str;
        }
        String str2 = "";
        int i2 = 0;
        while (i2 < bArr.length) {
            str2 = str2 + Integer.toHexString(bArr[i2] & 255);
            if (!(i2 % 2 == 0 || i2 == bArr.length - 1)) {
                str2 = str2 + (i2 == 15 ? au.aGF : cd.bVI);
            }
            i2++;
        }
        return str2;
    }

    public static void dI(String str) {
        byte[] bytes = str.toLowerCase().getBytes();
        boolean z = true;
        for (int i = 0; i < bytes.length; i++) {
            byte b2 = bytes[i];
            if (z) {
                if (b2 > 122 || b2 < 97) {
                    throw new IOException(a.c("security.184", Character.valueOf((char) b2), str));
                }
                z = false;
            } else if ((b2 < 97 || b2 > 122) && !((b2 >= 48 && b2 <= 57) || b2 == 45 || b2 == 46)) {
                throw new IOException(a.c("security.185", str));
            } else if (b2 != 46) {
                continue;
            } else if (bytes[i - 1] == 45) {
                throw new IOException(a.c("security.186", str));
            } else {
                z = true;
            }
        }
    }

    public static void dJ(String str) {
        try {
            URI uri = new URI(str);
            if (uri.getScheme() == null || uri.getRawSchemeSpecificPart().length() == 0) {
                throw new IOException(a.c("security.187", str));
            } else if (!uri.isAbsolute()) {
                throw new IOException(a.c("security.188", str));
            }
        } catch (URISyntaxException e) {
            throw ((IOException) new IOException(a.c("security.189", str)).initCause(e));
        }
    }

    public static int[] dK(String str) {
        int i;
        byte[] bytes = str.getBytes();
        if (bytes[bytes.length - 1] == 46) {
            throw new IOException(a.c("security.56", str));
        }
        int[] iArr = new int[((bytes.length / 2) + 1)];
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if (i2 >= bytes.length) {
                i = i3;
                break;
            }
            int i4 = 0;
            int i5 = i2;
            while (i5 < bytes.length && bytes[i5] >= 48 && bytes[i5] <= 57) {
                i4 = (i4 * 10) + (bytes[i5] - r.Eh);
                i5++;
            }
            if (i5 == i2) {
                throw new IOException(a.c("security.56", str));
            }
            int i6 = i3 + 1;
            iArr[i3] = i4;
            if (i5 >= bytes.length) {
                i = i6;
                break;
            } else if (bytes[i5] != 46) {
                throw new IOException(a.c("security.56", str));
            } else {
                i3 = i6;
                i2 = i5 + 1;
            }
        }
        if (i < 2) {
            throw new IOException(a.c("security.18A", str));
        }
        int[] iArr2 = new int[i];
        for (int i7 = 0; i7 < i; i7++) {
            iArr2[i7] = iArr[i7];
        }
        return iArr2;
    }

    public static byte[] dL(String str) {
        boolean z;
        int i;
        int i2;
        boolean z2 = str.indexOf(46) > 0;
        int i3 = z2 ? 4 : 16;
        if (str.indexOf(47) > 0) {
            i3 *= 2;
        }
        byte[] bArr = new byte[i3];
        byte[] bytes = str.getBytes();
        if (z2) {
            int i4 = 0;
            boolean z3 = false;
            int i5 = 0;
            while (true) {
                if (i4 >= bytes.length) {
                    i2 = i5;
                    break;
                }
                int i6 = 0;
                int i7 = i4;
                int i8 = 0;
                while (i7 < bytes.length && bytes[i7] >= 48 && bytes[i7] <= 57) {
                    i6++;
                    if (i6 > 3) {
                        throw new IOException(a.c("security.18B", str));
                    }
                    i8 = (i8 * 10) + (bytes[i7] - r.Eh);
                    i7++;
                }
                if (i6 == 0) {
                    throw new IOException(a.c("security.18C", str));
                }
                bArr[i5] = (byte) i8;
                i2 = i5 + 1;
                if (i7 >= bytes.length) {
                    break;
                } else if (bytes[i7] == 46 || bytes[i7] == 47) {
                    if (bytes[i7] == 47) {
                        if (z3) {
                            throw new IOException(a.c("security.18C", str));
                        } else if (i2 != 4) {
                            throw new IOException(a.c("security.18D", str));
                        } else {
                            z3 = true;
                        }
                    }
                    if (i2 > (z3 ? 7 : 3)) {
                        throw new IOException(a.c("security.18D", str));
                    }
                    i5 = i2;
                    i4 = i7 + 1;
                } else {
                    throw new IOException(a.c("security.18C", str));
                }
            }
            if (i2 != i3) {
                throw new IOException(a.c("security.18D", str));
            }
        } else if (bytes.length == 39 || bytes.length == 79) {
            boolean z4 = false;
            boolean z5 = false;
            boolean z6 = false;
            int i9 = 0;
            for (byte b2 : bytes) {
                if (b2 >= 48 && b2 <= 57) {
                    i = b2 - r.Eh;
                } else if (b2 >= 65 && b2 <= 70) {
                    i = b2 - r.Ea;
                } else if (b2 >= 97 && b2 <= 102) {
                    i = b2 - 87;
                } else if (z5) {
                    throw new IOException(a.c("security.18E", str));
                } else if (b2 != 58 && b2 != 47) {
                    throw new IOException(a.c("security.18E", str));
                } else if (i9 % 2 == 1) {
                    throw new IOException(a.c("security.18E", str));
                } else {
                    if (b2 != 47) {
                        z = z6;
                    } else if (z6) {
                        throw new IOException(a.c("security.18E", str));
                    } else if (i9 != 16) {
                        throw new IOException(a.c("security.18F", str));
                    } else {
                        z = true;
                    }
                    z6 = z;
                    z4 = false;
                }
                if (z4) {
                    throw new IOException(a.c("security.18E", str));
                }
                if (!z5) {
                    bArr[i9] = (byte) (i << 4);
                    z5 = true;
                } else {
                    bArr[i9] = (byte) ((bArr[i9] & 255) | i);
                    z4 = i9 % 2 == 1;
                    i9++;
                    z5 = false;
                }
            }
            if (z5 || i9 % 2 == 1) {
                throw new IOException(a.c("security.18E", str));
            }
        } else {
            throw new IOException(a.c("security.18E", str));
        }
        return bArr;
    }

    public Object Ch() {
        return this.beE;
    }

    public List Ci() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Integer(this.tag));
        switch (this.tag) {
            case 0:
                arrayList.add(((an) this.beE).getEncoded());
                break;
            case 1:
            case 2:
            case 6:
                arrayList.add(this.beE);
                break;
            case 3:
                arrayList.add(((as) this.beE).getEncoded());
                break;
            case 4:
                arrayList.add(((h) this.beE).getName("RFC2253"));
                break;
            case 5:
                arrayList.add(((aa) this.beE).getEncoded());
                break;
            case 7:
                arrayList.add(ai((byte[]) this.beE));
                break;
            case 8:
                arrayList.add(aj.toString((int[]) this.beE));
                break;
        }
        return Collections.unmodifiableList(arrayList);
    }

    public byte[] Cj() {
        if (this.beF == null) {
            this.beF = beD[this.tag].S(this.beE);
        }
        return this.beF;
    }

    public boolean a(be beVar) {
        if (this.tag != beVar.eH()) {
            return false;
        }
        switch (this.tag) {
            case 0:
            case 3:
            case 4:
            case 5:
            case 8:
                return Arrays.equals(getEncoded(), beVar.getEncoded());
            case 1:
                return ((String) beVar.Ch()).toLowerCase().endsWith(((String) this.beE).toLowerCase());
            case 2:
                String str = (String) this.beE;
                String str2 = (String) beVar.Ch();
                if (str.equalsIgnoreCase(str2)) {
                    return true;
                }
                return str2.toLowerCase().endsWith("." + str.toLowerCase());
            case 6:
                String str3 = (String) this.beE;
                int indexOf = str3.indexOf("://") + 3;
                int indexOf2 = str3.indexOf(47, indexOf);
                String substring = indexOf2 == -1 ? str3.substring(indexOf) : str3.substring(indexOf, indexOf2);
                String str4 = (String) beVar.Ch();
                int indexOf3 = str4.indexOf("://") + 3;
                int indexOf4 = str4.indexOf(47, indexOf3);
                String substring2 = indexOf4 == -1 ? str4.substring(indexOf3) : str4.substring(indexOf3, indexOf4);
                return substring.startsWith(".") ? substring2.toLowerCase().endsWith(substring.toLowerCase()) : substring.equalsIgnoreCase(substring2);
            case 7:
                byte[] bArr = (byte[]) this.beE;
                byte[] bArr2 = (byte[]) beVar.Ch();
                int length = bArr.length;
                int length2 = bArr2.length;
                if (length == length2) {
                    return Arrays.equals(bArr, bArr2);
                }
                if (length != length2 * 2) {
                    return false;
                }
                for (int i = 0; i < bArr2.length; i++) {
                    if (bArr2[i] < bArr[i] || bArr2[i] > bArr[i + length2]) {
                        return false;
                    }
                }
                return true;
            default:
                return true;
        }
    }

    public int eH() {
        return this.tag;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof be)) {
            return false;
        }
        be beVar = (be) obj;
        if (this.tag != beVar.tag) {
            return false;
        }
        switch (this.tag) {
            case 0:
            case 3:
            case 4:
            case 5:
                return Arrays.equals(getEncoded(), beVar.getEncoded());
            case 1:
            case 2:
            case 6:
                return ((String) this.beE).equalsIgnoreCase((String) beVar.Ch());
            case 7:
                return Arrays.equals((byte[]) this.beE, (byte[]) beVar.beE);
            case 8:
                return Arrays.equals((int[]) this.beE, (int[]) beVar.beE);
            default:
                return false;
        }
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = Yv.S(this);
        }
        return this.dV;
    }

    public int hashCode() {
        switch (this.tag) {
            case 0:
            case 3:
            case 4:
            case 5:
                return getEncoded().hashCode();
            case 1:
            case 2:
            case 6:
            case 7:
            case 8:
                return this.beE.hashCode();
            default:
                return super.hashCode();
        }
    }

    public String toString() {
        switch (this.tag) {
            case 0:
                return "otherName[0]: " + ah(getEncoded());
            case 1:
                return "rfc822Name[1]: " + this.beE;
            case 2:
                return "dNSName[2]: " + this.beE;
            case 3:
                return "x400Address[3]: " + ah(getEncoded());
            case 4:
                return "directoryName[4]: " + ((h) this.beE).getName("RFC2253");
            case 5:
                return "ediPartyName[5]: " + ah(getEncoded());
            case 6:
                return "uniformResourceIdentifier[6]: " + this.beE;
            case 7:
                return "iPAddress[7]: " + ai((byte[]) this.beE);
            case 8:
                return "registeredID[8]: " + aj.toString((int[]) this.beE);
            default:
                return "";
        }
    }
}
