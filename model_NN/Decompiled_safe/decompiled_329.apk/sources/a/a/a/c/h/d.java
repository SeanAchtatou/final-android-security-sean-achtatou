package a.a.a.c.h;

import java.security.PrivilegedAction;

public class d implements PrivilegedAction {
    public String ds;

    public d(String str) {
        this.ds = str;
    }

    public PrivilegedAction aA(String str) {
        this.ds = str;
        return this;
    }

    /* renamed from: f */
    public String run() {
        return System.getProperty(this.ds);
    }
}
