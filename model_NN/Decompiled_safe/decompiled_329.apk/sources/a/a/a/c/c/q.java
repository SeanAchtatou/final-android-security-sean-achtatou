package a.a.a.c.c;

import a.a.a.c.a.am;
import java.util.ArrayList;
import java.util.List;

public class q extends ap {
    public static final am lG = new bg(bm.en);
    /* access modifiers changed from: private */
    public List GO;
    private byte[] dV;

    public q() {
    }

    public q(List list) {
        this.GO = list;
    }

    private q(List list, byte[] bArr) {
        this.GO = list;
        this.dV = bArr;
    }

    /* synthetic */ q(List list, byte[] bArr, bg bgVar) {
        this(list, bArr);
    }

    public static q K(byte[] bArr) {
        q qVar = (q) lG.ax(bArr);
        qVar.dV = bArr;
        return qVar;
    }

    public q a(bm bmVar) {
        this.dV = null;
        if (this.GO == null) {
            this.GO = new ArrayList();
        }
        this.GO.add(bmVar);
        return this;
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("CertificatePolicies [\n");
        for (bm b2 : this.GO) {
            stringBuffer.append(str);
            stringBuffer.append("  ");
            b2.b(stringBuffer);
            stringBuffer.append(10);
        }
        stringBuffer.append(str).append("]\n");
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = lG.S(this);
        }
        return this.dV;
    }

    public List kp() {
        return new ArrayList(this.GO);
    }
}
