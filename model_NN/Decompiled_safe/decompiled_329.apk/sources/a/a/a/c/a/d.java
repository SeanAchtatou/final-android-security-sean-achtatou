package a.a.a.c.a;

public final class d extends at {
    private static final int dc = 32;
    private int[][] dd = new int[32][];
    private Object[][] de = new Object[32][];
    private int index;

    public d(am amVar, Object obj) {
        this.auW = obj;
        this.index = -1;
        amVar.b(this);
        this.dg = new byte[amVar.d(this)];
        this.index = 0;
        amVar.c(this);
    }

    private final void a(t tVar) {
        Object[] objArr = this.de[this.index];
        int[] iArr = this.dd[this.index];
        this.index++;
        for (int i = 0; i < objArr.length; i++) {
            this.auW = objArr[i];
            this.length = iArr[i];
            tVar.rq.c(this);
        }
    }

    private void a(int[] iArr, Object[] objArr) {
        this.index++;
        if (this.index == this.de.length) {
            int[][] iArr2 = new int[(this.de.length * 2)][];
            System.arraycopy(this.dd, 0, iArr2, 0, this.de.length);
            this.dd = iArr2;
            Object[][] objArr2 = new Object[(this.de.length * 2)][];
            System.arraycopy(this.de, 0, objArr2, 0, this.de.length);
            this.de = objArr2;
        }
        this.dd[this.index] = iArr;
        this.de[this.index] = objArr;
    }

    private void b(t tVar) {
        Object[] array = tVar.b(this.auW).toArray();
        Object[] objArr = new Object[array.length];
        int[] iArr = new int[objArr.length];
        a(iArr, objArr);
        int i = 0;
        for (int i2 = 0; i2 < objArr.length; i2++) {
            this.auW = array[i2];
            tVar.rq.b(this);
            iArr[i2] = this.length;
            objArr[i2] = this.auW;
            i += tVar.rq.d(this);
        }
        this.length = i;
    }

    public void a(a aVar) {
        a((t) aVar);
    }

    public void a(aq aqVar) {
        a((t) aqVar);
    }

    public void a(e eVar) {
        this.auW = this.de[this.index][1];
        this.index++;
        ((am) this.de[this.index][0]).c(this);
    }

    public void a(o oVar) {
        this.auW = this.de[this.index][0];
        this.length = this.dd[this.index][0];
        this.index++;
        oVar.rq.c(this);
    }

    public void a(v vVar) {
        am[] amVarArr = vVar.dk;
        Object[] objArr = this.de[this.index];
        int[] iArr = this.dd[this.index];
        this.index++;
        for (int i = 0; i < amVarArr.length; i++) {
            if (objArr[i] != null) {
                this.auW = objArr[i];
                this.length = iArr[i];
                amVarArr[i].c(this);
            }
        }
    }

    public void b(a aVar) {
        b((t) aVar);
    }

    public void b(aq aqVar) {
        b((t) aqVar);
    }

    public void b(e eVar) {
        int f = eVar.f(this.auW);
        this.auW = eVar.g(this.auW);
        Object[] objArr = {eVar.dk[f], this.auW};
        a(null, objArr);
        eVar.dk[f].b(this);
        objArr[1] = this.auW;
    }

    public void b(o oVar) {
        int[] iArr = new int[1];
        Object[] objArr = {this.auW};
        a(iArr, objArr);
        oVar.rq.b(this);
        objArr[0] = this.auW;
        iArr[0] = this.length;
        this.length = oVar.rq.d(this);
    }

    public void b(v vVar) {
        int i = 0;
        am[] amVarArr = vVar.dk;
        Object[] objArr = new Object[amVarArr.length];
        int[] iArr = new int[amVarArr.length];
        vVar.a(this.auW, objArr);
        a(iArr, objArr);
        for (int i2 = 0; i2 < amVarArr.length; i2++) {
            if (objArr[i2] == null) {
                if (!vVar.asF[i2]) {
                    throw new RuntimeException();
                }
            } else if (vVar.asG[i2] == null || !vVar.asG[i2].equals(objArr[i2])) {
                this.auW = objArr[i2];
                amVarArr[i2].b(this);
                iArr[i2] = this.length;
                objArr[i2] = this.auW;
                i += amVarArr[i2].d(this);
            } else {
                objArr[i2] = null;
            }
        }
        this.length = i;
    }
}
