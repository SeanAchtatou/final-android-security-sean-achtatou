package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.as;
import a.a.a.c.a.n;
import a.a.a.c.a.v;

public class bk {
    public static final v en = new p(new am[]{be.Yv, new n(0, as.NW()), new n(1, as.NW())});
    /* access modifiers changed from: private */
    public final be bnt;
    /* access modifiers changed from: private */
    public final int bnu;
    /* access modifiers changed from: private */
    public final int bnv;
    private byte[] dV;

    public bk(be beVar) {
        this(beVar, 0, -1);
    }

    public bk(be beVar, int i) {
        this(beVar, i, -1);
    }

    public bk(be beVar, int i, int i2) {
        this.bnt = beVar;
        this.bnu = i;
        this.bnv = i2;
    }

    public be EI() {
        return this.bnt;
    }

    public int EJ() {
        return this.bnv;
    }

    public int EK() {
        return this.bnu;
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("General Subtree: [\n");
        stringBuffer.append(str).append("  base: ").append(this.bnt).append(10);
        stringBuffer.append(str).append("  minimum: ").append(this.bnu).append(10);
        if (this.bnv >= 0) {
            stringBuffer.append(str).append("  maximum: ").append(this.bnv).append(10);
        }
        stringBuffer.append(str).append("]\n");
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }
}
