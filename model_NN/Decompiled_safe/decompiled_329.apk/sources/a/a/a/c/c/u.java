package a.a.a.c.c;

import a.a.a.c.a.am;
import a.a.a.c.a.as;
import a.a.a.c.a.n;
import a.a.a.c.a.v;
import java.math.BigInteger;

public class u extends ap {
    public static final v en = new l(new am[]{new n(0, as.NW()), new n(1, as.NW())});
    /* access modifiers changed from: private */
    public final BigInteger Iv;
    /* access modifiers changed from: private */
    public final BigInteger Iw;
    private byte[] dV;

    public u() {
        this((BigInteger) null, (BigInteger) null);
    }

    public u(int i, int i2) {
        this.Iv = BigInteger.valueOf((long) i);
        this.Iw = BigInteger.valueOf((long) i2);
    }

    public u(BigInteger bigInteger, BigInteger bigInteger2) {
        this.Iv = bigInteger;
        this.Iw = bigInteger2;
    }

    private u(BigInteger bigInteger, BigInteger bigInteger2, byte[] bArr) {
        this(bigInteger, bigInteger2);
        this.dV = bArr;
    }

    /* synthetic */ u(BigInteger bigInteger, BigInteger bigInteger2, byte[] bArr, l lVar) {
        this(bigInteger, bigInteger2, bArr);
    }

    public u(byte[] bArr) {
        super(bArr);
        u uVar = (u) en.ax(bArr);
        this.Iv = uVar.Iv;
        this.Iw = uVar.Iw;
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str).append("PolicyConstraints: [\n");
        if (this.Iv != null) {
            stringBuffer.append(str).append("  requireExplicitPolicy: ").append(this.Iv).append(10);
        }
        if (this.Iw != null) {
            stringBuffer.append(str).append("  inhibitPolicyMapping: ").append(this.Iw).append(10);
        }
        stringBuffer.append(str).append("]\n");
    }

    public byte[] getEncoded() {
        if (this.dV == null) {
            this.dV = en.S(this);
        }
        return this.dV;
    }
}
