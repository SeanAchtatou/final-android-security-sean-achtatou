package a.a.a.c.c;

import a.a.a.c.a.p;

public class ay {
    static final String[] aSU = {"unused", "keyCompromise", "cACompromise", "affiliationChanged", "superseded", "cessationOfOperation", "certificateHold", "privilegeWithdrawn", "aACompromise"};
    public static final p uP = new ca(aSU.length);
    /* access modifiers changed from: private */
    public boolean[] aSV;

    public ay(boolean[] zArr) {
        this.aSV = zArr;
    }

    public void a(StringBuffer stringBuffer, String str) {
        stringBuffer.append(str);
        stringBuffer.append("ReasonFlags [\n");
        for (int i = 0; i < this.aSV.length; i++) {
            if (this.aSV[i]) {
                stringBuffer.append(str).append("  ").append(aSU[i]).append(10);
            }
        }
        stringBuffer.append(str);
        stringBuffer.append("]\n");
    }
}
