package a.a.a.c.a;

import java.math.BigInteger;

public class as extends ae {
    private static final as bWj = new as();

    public as() {
        super(2);
    }

    public static as NW() {
        return bWj;
    }

    public static int U(Object obj) {
        return new BigInteger((byte[]) obj).intValue();
    }

    public static Object jV(int i) {
        return BigInteger.valueOf((long) i).toByteArray();
    }

    public Object a(ad adVar) {
        adVar.xo();
        if (adVar.avc) {
            return null;
        }
        return b(adVar);
    }

    public void a(at atVar) {
        atVar.Om();
    }

    public Object b(ad adVar) {
        byte[] bArr = new byte[adVar.length];
        System.arraycopy(adVar.buffer, adVar.auY, bArr, 0, adVar.length);
        return bArr;
    }

    public void b(at atVar) {
        atVar.length = ((byte[]) atVar.auW).length;
    }
}
