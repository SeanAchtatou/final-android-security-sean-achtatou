package a.a.a.c;

import a.a.a.c.h.l;
import java.net.URL;
import java.security.CodeSigner;
import java.security.CodeSource;
import java.security.Principal;
import java.util.Collection;
import java.util.Collections;

public class d {
    private final CodeSource asC;
    private final Principal[] asD;
    private final Collection asE;

    public d(CodeSource codeSource, Collection collection, Collection collection2) {
        this.asC = codeSource != null ? b(codeSource) : null;
        this.asD = (collection == null || collection.isEmpty()) ? null : (Principal[]) collection.toArray(new Principal[collection.size()]);
        this.asE = (collection2 == null || collection2.isEmpty()) ? null : Collections.unmodifiableCollection(collection2);
    }

    private CodeSource b(CodeSource codeSource) {
        URL a2 = l.a(codeSource.getLocation());
        if (a2 == codeSource.getLocation()) {
            return codeSource;
        }
        CodeSigner[] codeSigners = codeSource.getCodeSigners();
        return codeSigners == null ? new CodeSource(a2, codeSource.getCertificates()) : new CodeSource(a2, codeSigners);
    }

    public boolean a(CodeSource codeSource) {
        if (this.asC == null) {
            return true;
        }
        if (codeSource == null) {
            return false;
        }
        return this.asC.implies(b(codeSource));
    }

    public boolean a(Principal[] principalArr) {
        return l.g(this.asD, principalArr);
    }

    public Collection wM() {
        return this.asE;
    }

    public boolean wN() {
        return this.asE == null || this.asE.size() == 0;
    }
}
