package a.a.a.c.c;

import a.a.a.c.a.ad;
import a.a.a.c.a.am;
import a.a.a.c.a.aq;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final class at extends aq {
    at(am amVar) {
        super(amVar);
    }

    public Object b(ad adVar) {
        return new ae((List) adVar.auW);
    }

    public Collection b(Object obj) {
        ae aeVar = (ae) obj;
        return aeVar.aac == null ? new ArrayList() : aeVar.aac;
    }
}
