package a.a.a.c;

public class j {
    public static boolean aa(String str, String str2) {
        return ek(str).equals(ek(str2));
    }

    public static String ej(String str) {
        return ek(str);
    }

    static String ek(String str) {
        int length = str.length();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if ('a' > charAt || charAt > 'z') {
                sb.append(charAt);
            } else {
                sb.append((char) (charAt - ' '));
            }
        }
        return sb.toString();
    }
}
