package a.a.a.a.a.a;

public class k extends aj {
    byte[] ql;

    public k(l lVar, int i) {
        if (i == 0) {
            a((byte) 50, "DECODE ERROR: incorrect CertificateVerify");
        } else {
            if (lVar.xc() != i - 2) {
                a((byte) 50, "DECODE ERROR: incorrect CertificateVerify");
            }
            this.ql = lVar.ak(i - 2);
        }
        this.length = i;
    }

    public k(byte[] bArr) {
        if (bArr == null || bArr.length == 0) {
            a((byte) 80, "INTERNAL ERROR: incorrect certificate verify hash");
        }
        this.ql = bArr;
        this.length = bArr.length + 2;
    }

    public void a(l lVar) {
        if (this.ql.length != 0) {
            lVar.c((long) this.ql.length);
            lVar.write(this.ql);
        }
    }

    public int getType() {
        return 15;
    }
}
