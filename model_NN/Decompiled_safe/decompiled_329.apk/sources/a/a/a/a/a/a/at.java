package a.a.a.a.a.a;

import java.io.OutputStream;

public class at extends OutputStream {
    private aw IZ;
    private byte[] bjI = new byte[1];

    protected at(aw awVar) {
        this.IZ = awVar;
    }

    public void write(int i) {
        this.bjI[0] = (byte) (i & 255);
        this.IZ.n(this.bjI, 0, 1);
    }

    public void write(byte[] bArr) {
        this.IZ.n(bArr, 0, bArr.length);
    }

    public void write(byte[] bArr, int i, int i2) {
        this.IZ.n(bArr, i, i2);
    }
}
