package a.a.a.a.a.a;

import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;

public class ai implements Runnable {
    private final n aAg;
    private final PrivilegedExceptionAction aAh;
    private final AccessControlContext aAi;

    public ai(PrivilegedExceptionAction privilegedExceptionAction, n nVar, AccessControlContext accessControlContext) {
        this.aAh = privilegedExceptionAction;
        this.aAg = nVar;
        this.aAi = accessControlContext;
    }

    public void run() {
        synchronized (this.aAg) {
            try {
                AccessController.doPrivileged(this.aAh, this.aAi);
            } catch (PrivilegedActionException e) {
                this.aAg.tk = e.getException();
            } catch (RuntimeException e2) {
                this.aAg.tk = e2;
            }
        }
        return;
    }
}
