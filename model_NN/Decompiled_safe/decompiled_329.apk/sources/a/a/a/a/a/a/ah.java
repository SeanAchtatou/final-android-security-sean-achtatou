package a.a.a.a.a.a;

import javax.net.ssl.SSLException;

class ah implements z {
    final /* synthetic */ w avy;

    private ah(w wVar) {
        this.avy = wVar;
    }

    public void A(byte[] bArr) {
        int length = bArr.length;
        if (this.avy.sm - (this.avy.jv - this.avy.pos) < length) {
            throw new ba((byte) 80, new SSLException("Could not accept income app data."));
        }
        if (this.avy.jv + length > this.avy.sm) {
            System.arraycopy(this.avy.buffer, this.avy.pos, this.avy.buffer, 0, this.avy.jv - this.avy.pos);
            w.a(this.avy, this.avy.pos);
            int unused = this.avy.pos = 0;
        }
        System.arraycopy(bArr, 0, this.avy.buffer, this.avy.jv, length);
        int unused2 = this.avy.jv = length + this.avy.jv;
    }
}
