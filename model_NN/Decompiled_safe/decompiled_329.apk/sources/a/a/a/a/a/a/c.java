package a.a.a.a.a.a;

import java.security.AccessController;
import java.security.Provider;

public final class c extends Provider {
    private static final long serialVersionUID = 3075686092260669675L;

    public c() {
        super("HarmonyJSSE", 1.0d, "Harmony JSSE Provider");
        AccessController.doPrivileged(new v(this));
    }
}
