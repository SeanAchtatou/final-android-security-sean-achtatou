package a.a.a.a.a.a;

import java.io.IOException;
import java.security.AccessController;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Enumeration;
import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.interfaces.DHKey;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHParameterSpec;
import javax.crypto.spec.DHPublicKeySpec;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509ExtendedKeyManager;

public class bg extends n {
    bg(Object obj) {
        super(obj);
    }

    private void Mv() {
        if (this.sS.getEnableSessionCreation()) {
            this.ti = false;
            this.sV = new bk(this.sS.GQ());
            this.sV.bYj = o.c(this.sS.getEnabledProtocols());
            this.sR.Z(this.sV.bYj.uk);
            Mw();
            return;
        }
        this.status = 2;
        h((byte) 100);
    }

    private void Mw() {
        this.sW = new m(this.sS.GQ(), this.sV.bYj.uk, this.sV.FL, this.ti ? new bc[]{this.sV.bYk} : this.sS.bwS);
        this.sV.bYr = this.sW.rZ;
        a(this.sW);
        this.status = 1;
    }

    private void My() {
        String str = null;
        switch (this.sV.bYk.bsd) {
            case 1:
                str = "RSA";
                break;
            case 2:
                if (this.sZ == null) {
                    str = "RSA";
                    break;
                } else {
                    str = "RSA_EXPORT";
                    break;
                }
            case 3:
            case 4:
                str = "DHE_DSS";
                break;
            case 5:
            case 6:
                str = "DHE_RSA";
                break;
            case 7:
            case 11:
                str = "DH_DSS";
                break;
            case 8:
            case 12:
                str = "DH_RSA";
                break;
            case 9:
            case 10:
                return;
        }
        if (!sP.contains(this.tq.getInetAddress().getHostName())) {
            try {
                this.sS.GP().checkServerTrusted(this.sY.dI, str);
            } catch (CertificateException e) {
                a((byte) 42, "Not trusted server certificate", e);
                return;
            }
        }
        this.sV.bYn = this.sY.dI;
    }

    private bk Mz() {
        String str;
        int port;
        if (this.tp != null) {
            String peerHost = this.tp.getPeerHost();
            str = peerHost;
            port = this.tp.getPeerPort();
        } else {
            String hostAddress = this.tq.getInetAddress().getHostAddress();
            str = hostAddress;
            port = this.tq.getPort();
        }
        if (str == null || port == -1) {
            return null;
        }
        y GN = this.sS.GN();
        Enumeration<byte[]> ids = GN.getIds();
        while (ids.hasMoreElements()) {
            SSLSession session = GN.getSession(ids.nextElement());
            if (str.equals(session.getPeerHost()) && port == session.getPeerPort()) {
                return (bk) ((bk) session).clone();
            }
        }
        return null;
    }

    public void C(byte[] bArr) {
        if (this.tk != null) {
            Exception exc = this.tk;
            this.tk = null;
            a((byte) 40, "Error in delegated task", exc);
        }
        this.sQ.A(bArr);
        while (this.sQ.available() > 0) {
            this.sQ.ej();
            try {
                int read = this.sQ.read();
                int xd = this.sQ.xd();
                if (this.sQ.available() < xd) {
                    this.sQ.reset();
                    return;
                }
                switch (read) {
                    case 0:
                        this.sQ.ek();
                        if (this.sW == null || !(this.tf == null || this.tg == null)) {
                            if (!this.sV.isValid()) {
                                Mv();
                                break;
                            } else {
                                this.sV = (bk) this.sV.clone();
                                this.ti = true;
                                Mw();
                                break;
                            }
                        }
                    case 2:
                        if (this.sW != null && this.sX == null) {
                            this.sX = new x(this.sQ, xd);
                            o H = o.H(this.sX.UZ);
                            String[] enabledProtocols = this.sS.getEnabledProtocols();
                            int i = 0;
                            while (true) {
                                if (i >= enabledProtocols.length) {
                                    a((byte) 40, "Bad server hello protocol version");
                                } else if (!H.equals(o.N(enabledProtocols[i]))) {
                                    i++;
                                }
                            }
                            if (this.sX.Vb != 0) {
                                a((byte) 40, "Bad server hello compression method");
                            }
                            bc[] bcVarArr = this.sS.bwS;
                            int i2 = 0;
                            while (true) {
                                if (i2 >= bcVarArr.length) {
                                    a((byte) 40, "Bad server hello cipher suite");
                                } else if (!this.sX.Va.equals(bcVarArr[i2])) {
                                    i2++;
                                }
                            }
                            if (this.ti) {
                                if (this.sX.sa.length == 0) {
                                    this.ti = false;
                                } else if (!Arrays.equals(this.sX.sa, this.sW.sa)) {
                                    this.ti = false;
                                } else if (!this.sV.bYj.equals(H)) {
                                    a((byte) 40, "Bad server hello protocol version");
                                } else if (!this.sV.bYk.equals(this.sX.Va)) {
                                    a((byte) 40, "Bad server hello cipher suite");
                                }
                                if (this.sX.UZ[1] == 1) {
                                    J("server finished");
                                } else {
                                    E(bq.chC);
                                }
                            }
                            this.sV.bYj = H;
                            this.sR.Z(this.sV.bYj.uk);
                            this.sV.bYk = this.sX.Va;
                            this.sV.FL = (byte[]) this.sX.sa.clone();
                            this.sV.bYs = this.sX.rZ;
                            break;
                        } else {
                            fd();
                            return;
                        }
                        break;
                    case 11:
                        if (this.sX != null && this.sZ == null && this.sY == null && !this.ti) {
                            this.sY = new d(this.sQ, xd);
                            break;
                        } else {
                            fd();
                            return;
                        }
                        break;
                    case 12:
                        if (this.sX != null && this.sZ == null && !this.ti) {
                            this.sZ = new aa(this.sQ, xd, this.sV.bYk.bsd);
                            break;
                        } else {
                            fd();
                            return;
                        }
                        break;
                    case 13:
                        if (this.sY != null && this.ta == null && !this.sV.bYk.FC() && !this.ti) {
                            this.ta = new ar(this.sQ, xd);
                            break;
                        } else {
                            fd();
                            return;
                        }
                        break;
                    case 14:
                        if (this.sX != null && this.tb == null && !this.ti) {
                            this.tb = new ag(this.sQ, xd);
                            if (!this.sU) {
                                Mx();
                                break;
                            } else {
                                this.sT.add(new ai(new bb(this), this, AccessController.getContext()));
                                return;
                            }
                        } else {
                            fd();
                            return;
                        }
                    case 20:
                        if (this.th) {
                            this.tg = new az(this.sQ, xd);
                            F(this.tg.getData());
                            this.sV.bYi = System.currentTimeMillis();
                            this.sS.GN().a(this.sV);
                            if (!this.ti) {
                                this.sV.bYi = System.currentTimeMillis();
                                this.status = 3;
                                break;
                            } else {
                                eY();
                                break;
                            }
                        } else {
                            fd();
                            return;
                        }
                    default:
                        fd();
                        return;
                }
            } catch (IOException e) {
                this.sQ.reset();
                return;
            }
        }
    }

    public void D(byte[] bArr) {
        fd();
    }

    /* access modifiers changed from: package-private */
    public void Mx() {
        KeyFactory instance;
        KeyAgreement instance2;
        KeyPairGenerator instance3;
        DHParameterSpec params;
        PublicKey publicKey;
        X509Certificate[] x509CertificateArr;
        PrivateKey privateKey;
        PrivateKey privateKey2 = null;
        if (this.sY != null) {
            if (this.sV.bYk.bsd == bc.bsx || this.sV.bYk.bsd == bc.bsy) {
                fd();
                return;
            }
            My();
        } else if (!(this.sV.bYk.bsd == bc.bsx || this.sV.bYk.bsd == bc.bsy)) {
            fd();
            return;
        }
        if (this.ta != null) {
            String chooseClientAlias = ((X509ExtendedKeyManager) this.sS.GO()).chooseClientAlias(this.ta.BK(), this.ta.bdm, null);
            if (chooseClientAlias != null) {
                X509ExtendedKeyManager x509ExtendedKeyManager = (X509ExtendedKeyManager) this.sS.GO();
                X509Certificate[] certificateChain = x509ExtendedKeyManager.getCertificateChain(chooseClientAlias);
                privateKey = x509ExtendedKeyManager.getPrivateKey(chooseClientAlias);
                x509CertificateArr = certificateChain;
            } else {
                x509CertificateArr = null;
                privateKey = null;
            }
            this.sV.bYm = x509CertificateArr;
            this.tc = new d(x509CertificateArr);
            a(this.tc);
            privateKey2 = privateKey;
        }
        if (this.sV.bYk.bsd == bc.bsp || this.sV.bYk.bsd == bc.bsq) {
            try {
                Cipher instance4 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                if (this.sZ != null) {
                    instance4.init(1, this.sZ.um());
                } else {
                    instance4.init(1, this.sY.dI[0]);
                }
                this.tj = new byte[48];
                this.sS.GQ().nextBytes(this.tj);
                System.arraycopy(this.sW.rY, 0, this.tj, 0, 2);
                try {
                    this.td = new bh(instance4.doFinal(this.tj), this.sX.UZ[1] == 1);
                } catch (Exception e) {
                    a((byte) 80, "Unexpected exception", e);
                    return;
                }
            } catch (Exception e2) {
                a((byte) 80, "Unexpected exception", e2);
                return;
            }
        } else {
            try {
                instance = KeyFactory.getInstance("DH");
            } catch (NoSuchAlgorithmException e3) {
                instance = KeyFactory.getInstance("DiffieHellman");
            }
            try {
                instance2 = KeyAgreement.getInstance("DH");
            } catch (NoSuchAlgorithmException e4) {
                instance2 = KeyAgreement.getInstance("DiffieHellman");
            }
            try {
                instance3 = KeyPairGenerator.getInstance("DH");
            } catch (NoSuchAlgorithmException e5) {
                instance3 = KeyPairGenerator.getInstance("DiffieHellman");
            }
            try {
                if (this.sZ != null) {
                    publicKey = instance.generatePublic(new DHPublicKeySpec(this.sZ.ZW, this.sZ.ZS, this.sZ.ZU));
                    params = new DHParameterSpec(this.sZ.ZS, this.sZ.ZU);
                } else {
                    PublicKey publicKey2 = this.sY.dI[0].getPublicKey();
                    params = ((DHPublicKey) publicKey2).getParams();
                    publicKey = publicKey2;
                }
                instance3.initialize(params);
                KeyPair generateKeyPair = instance3.generateKeyPair();
                PublicKey publicKey3 = generateKeyPair.getPublic();
                if (this.tc == null || this.sY == null || !(this.sV.bYk.bsd == bc.bst || this.sV.bYk.bsd == bc.bsr)) {
                    this.td = new bh(((DHPublicKey) publicKey3).getY());
                } else {
                    PublicKey publicKey4 = this.tc.dI[0].getPublicKey();
                    PublicKey publicKey5 = this.sY.dI[0].getPublicKey();
                    if ((publicKey4 instanceof DHKey) && (publicKey5 instanceof DHKey) && ((DHKey) publicKey4).getParams().getG().equals(((DHKey) publicKey5).getParams().getG()) && ((DHKey) publicKey4).getParams().getP().equals(((DHKey) publicKey5).getParams().getG())) {
                        this.td = new bh();
                    }
                }
                instance2.init(generateKeyPair.getPrivate());
                instance2.doPhase(publicKey, true);
                this.tj = instance2.generateSecret();
            } catch (Exception e6) {
                a((byte) 80, "Unexpected exception", e6);
                return;
            }
        }
        if (this.td != null) {
            a(this.td);
        }
        fe();
        if (this.tc != null && !this.td.isEmpty()) {
            am amVar = new am(this.sV.bYk.bsd);
            amVar.a(privateKey2);
            if (this.sV.bYk.bsd == bc.bsq || this.sV.bYk.bsd == bc.bsp || this.sV.bYk.bsd == bc.bst || this.sV.bYk.bsd == bc.bsu) {
                amVar.V(this.sQ.em());
                amVar.W(this.sQ.en());
            } else if (this.sV.bYk.bsd == bc.bsr || this.sV.bYk.bsd == bc.bss) {
                amVar.W(this.sQ.en());
            }
            this.te = new k(amVar.sign());
            a(this.te);
        }
        eY();
    }

    public void fa() {
        if (this.ti) {
            if (this.sX == null) {
                fd();
            }
        } else if (this.tf == null) {
            fd();
        }
        this.th = true;
    }

    /* access modifiers changed from: protected */
    public void fb() {
        byte[] bArr;
        if (this.sX.UZ[1] == 1) {
            bArr = new byte[12];
            a("client finished", bArr);
        } else {
            bArr = new byte[36];
            e(bq.chB, bArr);
        }
        this.tf = new az(bArr);
        a(this.tf);
        if (this.ti) {
            this.sV.bYi = System.currentTimeMillis();
            this.status = 3;
            return;
        }
        if (this.sX.UZ[1] == 1) {
            J("server finished");
        } else {
            E(bq.chC);
        }
        this.status = 1;
    }

    public void start() {
        if (this.sV == null) {
            this.sV = Mz();
        } else if (this.sW != null && this.status != 3) {
            return;
        } else {
            if (!this.sV.isValid()) {
                this.sV = null;
            }
        }
        if (this.sV != null) {
            this.ti = true;
        } else if (this.sS.getEnableSessionCreation()) {
            this.ti = false;
            this.sV = new bk(this.sS.GQ());
            this.sV.bYj = o.c(this.sS.getEnabledProtocols());
            this.sR.Z(this.sV.bYj.uk);
        } else {
            a((byte) 40, "SSL Session may not be created ");
        }
        Mw();
    }
}
