package a.a.a.a.a.a;

import java.security.DigestException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.net.ssl.SSLException;

public class am {
    private final Signature aHe;
    private final Cipher aHf;
    private byte[] aHg;
    private byte[] aHh;
    private final MessageDigest qp;
    private final MessageDigest qq;

    public am(int i) {
        try {
            this.qq = MessageDigest.getInstance("SHA-1");
            if (i == bc.bsq || i == bc.bsp || i == bc.bst || i == bc.bsu) {
                this.qp = MessageDigest.getInstance("MD5");
                this.aHf = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                this.aHe = null;
            } else if (i == bc.bsr || i == bc.bss) {
                this.aHe = Signature.getInstance("NONEwithDSA");
                this.aHf = null;
                this.qp = null;
            } else {
                this.aHf = null;
                this.aHe = null;
                this.qp = null;
            }
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        } catch (NoSuchPaddingException e2) {
            throw new AssertionError(e2);
        }
    }

    public void V(byte[] bArr) {
        this.aHg = bArr;
    }

    public void W(byte[] bArr) {
        this.aHh = bArr;
    }

    public boolean X(byte[] bArr) {
        byte[] bArr2;
        if (this.aHe != null) {
            try {
                return this.aHe.verify(bArr);
            } catch (SignatureException e) {
                return false;
            }
        } else if (this.aHf == null) {
            return bArr == null || bArr.length == 0;
        } else {
            try {
                byte[] doFinal = this.aHf.doFinal(bArr);
                if (this.aHg == null || this.aHh == null) {
                    bArr2 = this.aHg != null ? this.aHg : this.aHh;
                } else {
                    bArr2 = new byte[(this.aHg.length + this.aHh.length)];
                    System.arraycopy(this.aHg, 0, bArr2, 0, this.aHg.length);
                    System.arraycopy(this.aHh, 0, bArr2, this.aHg.length, this.aHh.length);
                }
                return Arrays.equals(doFinal, bArr2);
            } catch (IllegalBlockSizeException e2) {
                return false;
            } catch (BadPaddingException e3) {
                return false;
            }
        }
    }

    public void a(PrivateKey privateKey) {
        try {
            if (this.aHe != null) {
                this.aHe.initSign(privateKey);
            } else if (this.aHf != null) {
                this.aHf.init(1, privateKey);
            }
        } catch (InvalidKeyException e) {
            throw new ba((byte) 42, new SSLException("init - invalid private key", e));
        }
    }

    public void a(Certificate certificate) {
        try {
            if (this.aHe != null) {
                this.aHe.initVerify(certificate);
            } else if (this.aHf != null) {
                this.aHf.init(2, certificate);
            }
        } catch (InvalidKeyException e) {
            throw new ba((byte) 42, new SSLException("init - invalid certificate", e));
        }
    }

    public byte[] sign() {
        try {
            if (this.qp != null && this.aHg == null) {
                this.aHg = new byte[16];
                this.qp.digest(this.aHg, 0, this.aHg.length);
            }
            if (this.aHg != null) {
                if (this.aHe != null) {
                    this.aHe.update(this.aHg);
                } else if (this.aHf != null) {
                    this.aHf.update(this.aHg);
                }
            }
            if (this.qq != null && this.aHh == null) {
                this.aHh = new byte[20];
                this.qq.digest(this.aHh, 0, this.aHh.length);
            }
            if (this.aHh != null) {
                if (this.aHe != null) {
                    this.aHe.update(this.aHh);
                } else if (this.aHf != null) {
                    this.aHf.update(this.aHh);
                }
            }
            return this.aHe != null ? this.aHe.sign() : this.aHf != null ? this.aHf.doFinal() : new byte[0];
        } catch (DigestException e) {
            return new byte[0];
        } catch (SignatureException e2) {
            return new byte[0];
        } catch (BadPaddingException e3) {
            return new byte[0];
        } catch (IllegalBlockSizeException e4) {
            return new byte[0];
        }
    }

    public void update(byte[] bArr) {
        if (this.qq != null) {
            this.qq.update(bArr);
        }
        if (this.qp != null) {
            this.qp.update(bArr);
        }
    }
}
