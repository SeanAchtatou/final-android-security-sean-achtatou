package a.a.a.a.a.a;

import java.lang.reflect.Array;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Vector;

public class d extends aj {
    X509Certificate[] dI;
    byte[][] dJ;

    public d(l lVar, int i) {
        int xd = lVar.xd();
        if (xd == 0) {
            if (i != 3) {
                a((byte) 50, "DECODE ERROR: incorrect CertificateMessage");
            }
            this.dI = new X509Certificate[0];
            this.dJ = (byte[][]) Array.newInstance(Byte.TYPE, 0, 0);
            this.length = 3;
            return;
        }
        try {
            CertificateFactory instance = CertificateFactory.getInstance("X509");
            Vector vector = new Vector();
            int i2 = 0;
            while (xd > 0) {
                int xd2 = lVar.xd();
                int i3 = xd - 3;
                try {
                    vector.add(instance.generateCertificate(lVar));
                } catch (CertificateException e) {
                    a((byte) 50, "DECODE ERROR", e);
                }
                xd = i3 - xd2;
                i2 += xd2;
            }
            this.dI = new X509Certificate[vector.size()];
            for (int i4 = 0; i4 < this.dI.length; i4++) {
                this.dI[i4] = (X509Certificate) vector.elementAt(i4);
            }
            this.length = (this.dI.length * 3) + 3 + i2;
            if (this.length != i) {
                a((byte) 50, "DECODE ERROR: incorrect CertificateMessage");
            }
        } catch (CertificateException e2) {
            a((byte) 80, "INTERNAL ERROR", e2);
        }
    }

    public d(X509Certificate[] x509CertificateArr) {
        if (x509CertificateArr == null) {
            this.dI = new X509Certificate[0];
            this.dJ = (byte[][]) Array.newInstance(Byte.TYPE, 0, 0);
            this.length = 3;
            return;
        }
        this.dI = x509CertificateArr;
        if (this.dJ == null) {
            this.dJ = new byte[x509CertificateArr.length][];
            for (int i = 0; i < x509CertificateArr.length; i++) {
                try {
                    this.dJ[i] = x509CertificateArr[i].getEncoded();
                } catch (CertificateEncodingException e) {
                    a((byte) 80, "INTERNAL ERROR", e);
                }
            }
        }
        this.length = (this.dJ.length * 3) + 3;
        for (int i2 = 0; i2 < this.dJ.length; i2++) {
            this.length += this.dJ[i2].length;
        }
    }

    public void a(l lVar) {
        if (this.dJ == null) {
            this.dJ = new byte[this.dI.length][];
            for (int i = 0; i < this.dI.length; i++) {
                try {
                    this.dJ[i] = this.dI[i].getEncoded();
                } catch (CertificateEncodingException e) {
                    a((byte) 80, "INTERNAL ERROR", e);
                }
            }
        }
        int length = this.dJ.length * 3;
        for (byte[] length2 : this.dJ) {
            length += length2.length;
        }
        lVar.d((long) length);
        for (int i2 = 0; i2 < this.dJ.length; i2++) {
            lVar.d((long) this.dJ[i2].length);
            lVar.write(this.dJ[i2]);
        }
    }

    public int getType() {
        return 11;
    }
}
