package a.a.a.a.a.a;

import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLProtocolException;

public class p extends h {
    private final MessageDigest uV;
    private final byte[] uW;
    private final byte[] uX;
    private final byte[] uY;
    private final byte[] uZ;
    private final byte[] va = new byte[3];

    protected p(bk bkVar) {
        IvParameterSpec ivParameterSpec;
        IvParameterSpec ivParameterSpec2;
        byte[] bArr;
        int i;
        byte[] bArr2;
        IvParameterSpec ivParameterSpec3;
        IvParameterSpec ivParameterSpec4;
        try {
            bc bcVar = bkVar.bYk;
            boolean FK = bcVar.FK();
            this.mi = bcVar.FJ();
            int i2 = FK ? bcVar.bsf : bcVar.bsg;
            int blockSize = bcVar.getBlockSize();
            String FG = bcVar.FG();
            String FI = bcVar.FI();
            byte[] bArr3 = bkVar.bYr;
            byte[] bArr4 = bkVar.bYs;
            byte[] bArr5 = new byte[((this.mi * 2) + (i2 * 2) + (blockSize * 2))];
            byte[] bArr6 = new byte[(bArr3.length + bArr4.length)];
            System.arraycopy(bArr4, 0, bArr6, 0, bArr4.length);
            System.arraycopy(bArr3, 0, bArr6, bArr4.length, bArr3.length);
            u.a(bArr5, bkVar.bYq, bArr6);
            byte[] bArr7 = new byte[this.mi];
            byte[] bArr8 = new byte[this.mi];
            byte[] bArr9 = new byte[i2];
            byte[] bArr10 = new byte[i2];
            boolean z = !bkVar.bYt;
            this.mh = blockSize > 0;
            System.arraycopy(bArr5, 0, bArr7, 0, this.mi);
            System.arraycopy(bArr5, this.mi, bArr8, 0, this.mi);
            System.arraycopy(bArr5, this.mi * 2, bArr9, 0, i2);
            System.arraycopy(bArr5, (this.mi * 2) + i2, bArr10, 0, i2);
            if (FK) {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                instance.update(bArr9);
                instance.update(bArr3);
                instance.update(bArr4);
                byte[] digest = instance.digest();
                instance.update(bArr10);
                instance.update(bArr4);
                instance.update(bArr3);
                byte[] digest2 = instance.digest();
                int i3 = bcVar.bsg;
                if (this.mh) {
                    instance.update(bArr3);
                    instance.update(bArr4);
                    IvParameterSpec ivParameterSpec5 = new IvParameterSpec(instance.digest(), 0, blockSize);
                    instance.update(bArr4);
                    instance.update(bArr3);
                    ivParameterSpec3 = new IvParameterSpec(instance.digest(), 0, blockSize);
                    ivParameterSpec4 = ivParameterSpec5;
                } else {
                    ivParameterSpec3 = null;
                    ivParameterSpec4 = null;
                }
                i = i3;
                ivParameterSpec = ivParameterSpec3;
                ivParameterSpec2 = ivParameterSpec4;
                bArr = digest;
                bArr2 = digest2;
            } else if (this.mh) {
                IvParameterSpec ivParameterSpec6 = new IvParameterSpec(bArr5, (this.mi * 2) + (i2 * 2), blockSize);
                IvParameterSpec ivParameterSpec7 = new IvParameterSpec(bArr5, (this.mi * 2) + (i2 * 2) + blockSize, blockSize);
                bArr = bArr9;
                i = i2;
                bArr2 = bArr10;
                IvParameterSpec ivParameterSpec8 = ivParameterSpec7;
                ivParameterSpec2 = ivParameterSpec6;
                ivParameterSpec = ivParameterSpec8;
            } else {
                ivParameterSpec = null;
                ivParameterSpec2 = null;
                bArr = bArr9;
                i = i2;
                bArr2 = bArr10;
            }
            this.mf = Cipher.getInstance(FG);
            this.mg = Cipher.getInstance(FG);
            this.uV = MessageDigest.getInstance(FI);
            if (z) {
                this.mf.init(1, new SecretKeySpec(bArr, 0, i, FG), ivParameterSpec2);
                this.mg.init(2, new SecretKeySpec(bArr2, 0, i, FG), ivParameterSpec);
                this.uW = bArr7;
                this.uX = bArr8;
            } else {
                this.mf.init(1, new SecretKeySpec(bArr2, 0, i, FG), ivParameterSpec);
                this.mg.init(2, new SecretKeySpec(bArr, 0, i, FG), ivParameterSpec2);
                this.uW = bArr8;
                this.uX = bArr7;
            }
            if (FI.equals("MD5")) {
                this.uY = bq.chD;
                this.uZ = bq.chF;
                return;
            }
            this.uY = bq.chE;
            this.uZ = bq.chG;
        } catch (Exception e) {
            throw new ba((byte) 80, new SSLProtocolException("Error during computation of security parameters"));
        }
    }

    /* access modifiers changed from: protected */
    public byte[] a(byte b2, byte[] bArr, int i, int i2) {
        int i3;
        int i4;
        try {
            int i5 = this.mi + i2;
            if (this.mh) {
                int i6 = i5 + 1;
                i3 = i6;
                i4 = (8 - (i6 & 7)) & 7;
            } else {
                i3 = i5;
                i4 = 0;
            }
            byte[] bArr2 = new byte[(i3 + i4)];
            System.arraycopy(bArr, i, bArr2, 0, i2);
            this.va[0] = b2;
            this.va[1] = (byte) ((65280 & i2) >> 8);
            this.va[2] = (byte) (i2 & 255);
            this.uV.update(this.uW);
            this.uV.update(this.uY);
            this.uV.update(this.mj);
            this.uV.update(this.va);
            this.uV.update(bArr, i, i2);
            byte[] digest = this.uV.digest();
            this.uV.update(this.uW);
            this.uV.update(this.uZ);
            this.uV.update(digest);
            System.arraycopy(this.uV.digest(), 0, bArr2, i2, this.mi);
            if (this.mh) {
                Arrays.fill(bArr2, i3 - 1, bArr2.length, (byte) i4);
            }
            byte[] bArr3 = new byte[this.mf.getOutputSize(bArr2.length)];
            this.mf.update(bArr2, 0, bArr2.length, bArr3);
            z(this.mj);
            return bArr3;
        } catch (GeneralSecurityException e) {
            throw new ba((byte) 80, new SSLProtocolException("Error during the encryption"));
        }
    }

    /* access modifiers changed from: protected */
    public byte[] b(byte b2, byte[] bArr, int i, int i2) {
        byte[] bArr2;
        byte[] update = this.mg.update(bArr, i, i2);
        if (this.mh) {
            byte b3 = update[update.length - 1];
            for (int i3 = 0; i3 < b3; i3++) {
                if (update[(update.length - 2) - i3] != b3) {
                    throw new ba((byte) 21, new SSLProtocolException("Received message has bad padding"));
                }
            }
            bArr2 = new byte[(((update.length - this.mi) - b3) - 1)];
        } else {
            bArr2 = new byte[(update.length - this.mi)];
        }
        this.va[0] = b2;
        this.va[1] = (byte) ((65280 & bArr2.length) >> 8);
        this.va[2] = (byte) (bArr2.length & 255);
        this.uV.update(this.uX);
        this.uV.update(this.uY);
        this.uV.update(this.mk);
        this.uV.update(this.va);
        this.uV.update(update, 0, bArr2.length);
        byte[] digest = this.uV.digest();
        this.uV.update(this.uX);
        this.uV.update(this.uZ);
        this.uV.update(digest);
        byte[] digest2 = this.uV.digest();
        for (int i4 = 0; i4 < this.mi; i4++) {
            if (digest2[i4] != update[bArr2.length + i4]) {
                throw new ba((byte) 20, new SSLProtocolException("Bad record MAC"));
            }
        }
        System.arraycopy(update, 0, bArr2, 0, bArr2.length);
        z(this.mk);
        return bArr2;
    }

    /* access modifiers changed from: protected */
    public void shutdown() {
        Arrays.fill(this.uW, (byte) 0);
        Arrays.fill(this.uX, (byte) 0);
        super.shutdown();
    }
}
