package a.a.a.a.a.a;

import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.Principal;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLPermission;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSessionBindingEvent;
import javax.net.ssl.SSLSessionBindingListener;
import javax.net.ssl.SSLSessionContext;
import javax.security.cert.CertificateException;

public class bk implements Cloneable, SSLSession {
    public static final bk bYe = new bk(null);
    byte[] FL;
    private long bYf;
    private boolean bYg;
    private Map bYh;
    long bYi;
    o bYj;
    bc bYk;
    y bYl;
    X509Certificate[] bYm;
    X509Certificate[] bYn;
    private String bYo;
    private int bYp;
    byte[] bYq;
    byte[] bYr;
    byte[] bYs;
    final boolean bYt;

    public bk(bc bcVar, SecureRandom secureRandom) {
        this.bYg = true;
        this.bYh = new HashMap();
        this.bYp = -1;
        this.bYf = System.currentTimeMillis();
        this.bYi = this.bYf;
        if (bcVar == null) {
            this.bYk = bc.btd;
            this.FL = new byte[0];
            this.bYt = false;
            return;
        }
        this.bYk = bcVar;
        this.FL = new byte[32];
        secureRandom.nextBytes(this.FL);
        long j = this.bYf / 1000;
        this.FL[28] = (byte) ((int) ((-16777216 & j) >>> 24));
        this.FL[29] = (byte) ((int) ((16711680 & j) >>> 16));
        this.FL[30] = (byte) ((int) ((65280 & j) >>> 8));
        this.FL[31] = (byte) ((int) (j & 255));
        this.bYt = true;
    }

    public bk(SecureRandom secureRandom) {
        this(null, secureRandom);
    }

    /* access modifiers changed from: package-private */
    public void B(String str, int i) {
        this.bYo = str;
        this.bYp = i;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public int getApplicationBufferSize() {
        return an.aYP;
    }

    public String getCipherSuite() {
        return this.bYk.getName();
    }

    public long getCreationTime() {
        return this.bYf;
    }

    public byte[] getId() {
        return this.FL;
    }

    public long getLastAccessedTime() {
        return this.bYi;
    }

    public Certificate[] getLocalCertificates() {
        return this.bYm;
    }

    public Principal getLocalPrincipal() {
        if (this.bYm == null || this.bYm.length <= 0) {
            return null;
        }
        return this.bYm[0].getSubjectX500Principal();
    }

    public int getPacketBufferSize() {
        return an.aYS;
    }

    public javax.security.cert.X509Certificate[] getPeerCertificateChain() {
        if (this.bYn == null) {
            throw new SSLPeerUnverifiedException("No peer certificate");
        }
        javax.security.cert.X509Certificate[] x509CertificateArr = new javax.security.cert.X509Certificate[this.bYn.length];
        for (int i = 0; i < x509CertificateArr.length; i++) {
            try {
                x509CertificateArr[i] = javax.security.cert.X509Certificate.getInstance(this.bYn[i].getEncoded());
            } catch (CertificateEncodingException | CertificateException e) {
            }
        }
        return x509CertificateArr;
    }

    public Certificate[] getPeerCertificates() {
        if (this.bYn != null) {
            return this.bYn;
        }
        throw new SSLPeerUnverifiedException("No peer certificate");
    }

    public String getPeerHost() {
        return this.bYo;
    }

    public int getPeerPort() {
        return this.bYp;
    }

    public Principal getPeerPrincipal() {
        if (this.bYn != null) {
            return this.bYn[0].getSubjectX500Principal();
        }
        throw new SSLPeerUnverifiedException("No peer certificate");
    }

    public String getProtocol() {
        return this.bYj.name;
    }

    public SSLSessionContext getSessionContext() {
        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null) {
            securityManager.checkPermission(new SSLPermission("getSSLSessionContext"));
        }
        return this.bYl;
    }

    public Object getValue(String str) {
        if (str != null) {
            return this.bYh.get(new af(str));
        }
        throw new IllegalArgumentException("Parameter is null");
    }

    public String[] getValueNames() {
        Vector vector = new Vector();
        AccessControlContext context = AccessController.getContext();
        for (af afVar : this.bYh.keySet()) {
            if ((context == null && afVar.auL == null) || (context != null && context.equals(afVar.auL))) {
                vector.add(afVar.name);
            }
        }
        return (String[]) vector.toArray(new String[vector.size()]);
    }

    public void invalidate() {
        this.bYg = false;
    }

    public boolean isValid() {
        if (this.bYg && this.bYl != null && this.bYl.getSessionTimeout() != 0 && this.bYi + ((long) this.bYl.getSessionTimeout()) > System.currentTimeMillis()) {
            this.bYg = false;
        }
        return this.bYg;
    }

    public void putValue(String str, Object obj) {
        if (str == null || obj == null) {
            throw new IllegalArgumentException("Parameter is null");
        }
        Object put = this.bYh.put(new af(str), obj);
        if (obj instanceof SSLSessionBindingListener) {
            ((SSLSessionBindingListener) obj).valueBound(new SSLSessionBindingEvent(this, str));
        }
        if (put != null && (put instanceof SSLSessionBindingListener)) {
            ((SSLSessionBindingListener) put).valueUnbound(new SSLSessionBindingEvent(this, str));
        }
    }

    public void removeValue(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Parameter is null");
        }
        this.bYh.remove(new af(str));
    }
}
