package a.a.a.a.a.a;

import java.math.BigInteger;

public class bh extends aj {
    final byte[] bPm;
    boolean bPn;
    final boolean bPo;

    public bh() {
        this.bPm = new byte[0];
        this.length = 0;
        this.bPo = false;
    }

    public bh(l lVar, int i, boolean z, boolean z2) {
        int xc;
        this.bPn = z;
        this.bPo = z2;
        if (i == 0) {
            this.length = 0;
            this.bPm = new byte[0];
            return;
        }
        if (!z2 || z) {
            xc = lVar.xc();
            this.length = xc + 2;
        } else {
            this.length = i;
            xc = i;
        }
        this.bPm = new byte[xc];
        lVar.read(this.bPm, 0, xc);
        if (this.length != i) {
            a((byte) 50, "DECODE ERROR: incorrect ClientKeyExchange");
        }
    }

    public bh(BigInteger bigInteger) {
        byte[] byteArray = bigInteger.toByteArray();
        if (byteArray[0] == 0) {
            this.bPm = new byte[(byteArray.length - 1)];
            System.arraycopy(byteArray, 1, this.bPm, 0, this.bPm.length);
        } else {
            this.bPm = byteArray;
        }
        this.length = this.bPm.length + 2;
        this.bPo = false;
    }

    public bh(byte[] bArr, boolean z) {
        this.bPm = bArr;
        this.length = this.bPm.length;
        if (z) {
            this.length += 2;
        }
        this.bPn = z;
        this.bPo = true;
    }

    public void a(l lVar) {
        if (this.bPm.length != 0) {
            if (!this.bPo || this.bPn) {
                lVar.c((long) this.bPm.length);
            }
            lVar.write(this.bPm);
        }
    }

    public int getType() {
        return 16;
    }

    public boolean isEmpty() {
        return this.bPm.length == 0;
    }
}
