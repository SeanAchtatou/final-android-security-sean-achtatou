package a.a.a.a.a.a;

import java.net.InetAddress;
import java.net.Socket;
import javax.net.ssl.SSLServerSocket;

public class g extends SSLServerSocket {
    private final bf lr;

    protected g(int i, int i2, bf bfVar) {
        super(i, i2);
        this.lr = bfVar;
    }

    protected g(int i, int i2, InetAddress inetAddress, bf bfVar) {
        super(i, i2, inetAddress);
        this.lr = bfVar;
    }

    protected g(int i, bf bfVar) {
        super(i);
        this.lr = bfVar;
    }

    protected g(bf bfVar) {
        this.lr = bfVar;
    }

    public Socket accept() {
        aw awVar = new aw((bf) this.lr.clone());
        implAccept(awVar);
        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null) {
            try {
                securityManager.checkAccept(awVar.getInetAddress().getHostAddress(), awVar.getPort());
            } catch (SecurityException e) {
                awVar.close();
                throw e;
            }
        }
        awVar.a();
        awVar.startHandshake();
        return awVar;
    }

    public boolean getEnableSessionCreation() {
        return this.lr.getEnableSessionCreation();
    }

    public String[] getEnabledCipherSuites() {
        return this.lr.getEnabledCipherSuites();
    }

    public String[] getEnabledProtocols() {
        return this.lr.getEnabledProtocols();
    }

    public boolean getNeedClientAuth() {
        return this.lr.getNeedClientAuth();
    }

    public String[] getSupportedCipherSuites() {
        return bc.FE();
    }

    public String[] getSupportedProtocols() {
        return (String[]) o.ug.clone();
    }

    public boolean getUseClientMode() {
        return this.lr.getUseClientMode();
    }

    public boolean getWantClientAuth() {
        return this.lr.getWantClientAuth();
    }

    public void setEnableSessionCreation(boolean z) {
        this.lr.setEnableSessionCreation(z);
    }

    public void setEnabledCipherSuites(String[] strArr) {
        this.lr.setEnabledCipherSuites(strArr);
    }

    public void setEnabledProtocols(String[] strArr) {
        this.lr.setEnabledProtocols(strArr);
    }

    public void setNeedClientAuth(boolean z) {
        this.lr.setNeedClientAuth(z);
    }

    public void setUseClientMode(boolean z) {
        this.lr.setUseClientMode(z);
    }

    public void setWantClientAuth(boolean z) {
        this.lr.setWantClientAuth(z);
    }

    public String toString() {
        return "[SSLServerSocketImpl]";
    }
}
