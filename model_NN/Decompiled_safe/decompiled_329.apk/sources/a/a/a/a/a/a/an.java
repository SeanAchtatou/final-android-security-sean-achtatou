package a.a.a.a.a.a;

import javax.net.ssl.SSLProtocolException;

public class an {
    protected static int aYP = 16384;
    protected static int aYQ = (aYP + 1024);
    protected static int aYR = (aYQ + 1024);
    protected static int aYS = (aYR + 5);
    private static final byte[] aZb = {1};
    private ae aYT;
    private n aYU;
    private bo aYV;
    private z aYW;
    private h aYX;
    private h aYY;
    private h aYZ;
    private boolean aZa = false;
    private bk sV;
    private byte[] uk;

    protected an(n nVar, bo boVar, ae aeVar, z zVar) {
        this.aYU = nVar;
        this.aYU.a(this);
        this.aYV = boVar;
        this.aYV.a(this);
        this.aYT = aeVar;
        this.aYW = zVar;
    }

    private byte[] a(byte b2, byte[] bArr, byte[] bArr2) {
        byte[] bArr3 = new byte[(bArr2.length + 5)];
        bArr3[0] = b2;
        if (bArr != null) {
            bArr3[1] = bArr[0];
            bArr3[2] = bArr[1];
        } else {
            bArr3[1] = 3;
            bArr3[2] = 1;
        }
        bArr3[3] = (byte) ((65280 & bArr2.length) >> 8);
        bArr3[4] = (byte) (bArr2.length & 255);
        System.arraycopy(bArr2, 0, bArr3, 5, bArr2.length);
        return bArr3;
    }

    private void b(bk bkVar) {
        if (!this.aZa) {
            this.sV = bkVar;
            this.aYZ = (this.uk == null || this.uk[1] == 1) ? new ax(eX()) : new p(eX());
            this.aZa = true;
            return;
        }
        this.aZa = false;
    }

    /* access modifiers changed from: protected */
    public int AL() {
        if (this.aYX == null) {
            return 6;
        }
        return this.aYX.cX() + 5;
    }

    /* access modifiers changed from: protected */
    public int AM() {
        int xb = this.aYT.xb();
        if (xb >= 20 && xb <= 23) {
            if (this.uk == null) {
                this.aYT.skip(2);
            } else if (!(this.aYT.read() == this.uk[0] && this.aYT.read() == this.uk[1])) {
                throw new ba((byte) 10, new SSLProtocolException("Unexpected message type has been received: " + xb));
            }
            int xc = this.aYT.xc();
            if (xc > aYR) {
                throw new ba((byte) 22, new SSLProtocolException("Received message is too big."));
            }
            byte[] ak = this.aYT.ak(xc);
            if (this.aYX != null) {
                ak = this.aYX.b((byte) xb, ak);
            }
            if (ak.length > aYP) {
                throw new ba((byte) 30, new SSLProtocolException("Decompressed plain data is too big."));
            }
            switch (xb) {
                case 20:
                    this.aYU.fa();
                    b(this.aYU.eX());
                    this.aYX = this.aYZ;
                    return xb;
                case 21:
                    a(ak[0], ak[1]);
                    return xb;
                case 22:
                    this.aYU.C(ak);
                    return xb;
                case 23:
                    this.aYW.A(ak);
                    return xb;
                default:
                    throw new ba((byte) 10, new SSLProtocolException("Unexpected message type has been received: " + xb));
            }
        } else if (xb >= 128) {
            this.aYU.D(this.aYT.ak(((xb & 127) << 8) | this.aYT.read()));
            return 22;
        } else {
            throw new ba((byte) 10, new SSLProtocolException("Unexpected message type has been received: " + xb));
        }
    }

    /* access modifiers changed from: protected */
    public void Z(byte[] bArr) {
        this.uk = bArr;
    }

    /* access modifiers changed from: protected */
    public void a(byte b2, byte b3) {
        this.aYV.a(b2, b3);
    }

    /* access modifiers changed from: protected */
    public byte[] a(byte b2, ak akVar) {
        byte[] ai = akVar.ai(aYP);
        return c(b2, ai, 0, ai.length);
    }

    /* access modifiers changed from: protected */
    public byte[] c(byte b2, byte[] bArr, int i, int i2) {
        byte[] bArr2;
        if (i2 > aYP) {
            throw new ba((byte) 80, new SSLProtocolException("The provided chunk of data is too big: " + i2 + " > MAX_DATA_LENGTH == " + aYP));
        }
        if (this.aYY != null) {
            bArr2 = this.aYY.a(b2, bArr, i, i2);
            if (bArr2.length > aYR) {
                throw new ba((byte) 80, new SSLProtocolException("The ciphered data increased more than on 1024 bytes"));
            }
        } else {
            bArr2 = bArr;
        }
        return a(b2, this.uk, bArr2);
    }

    /* access modifiers changed from: protected */
    public byte[] c(bk bkVar) {
        byte[] a2 = this.aYY == null ? new byte[]{20, this.uk[0], this.uk[1], 0, 1, 1} : a((byte) 20, this.uk, this.aYY.a((byte) 20, aZb, 0, 1));
        b(bkVar);
        this.aYY = this.aYZ;
        return a2;
    }

    /* access modifiers changed from: protected */
    public bk eX() {
        return this.sV;
    }

    /* access modifiers changed from: protected */
    public int fH(int i) {
        if (this.aYY == null) {
            return i + 5;
        }
        int J = this.aYY.J(i) + 5;
        return J > aYR ? aYR : J;
    }

    /* access modifiers changed from: protected */
    public int fM(int i) {
        int i2 = i - 5;
        return i2 > aYR ? aYP : this.aYX != null ? this.aYX.K(i2) : i2;
    }

    /* access modifiers changed from: protected */
    public void shutdown() {
        this.sV = null;
        this.uk = null;
        this.aYT = null;
        this.aYU = null;
        this.aYV = null;
        this.aYW = null;
        if (this.aYZ != null) {
            this.aYZ.shutdown();
        }
        this.aYZ = null;
        if (this.aYX != null) {
            this.aYX.shutdown();
        }
        this.aYX = null;
        if (this.aYX != null) {
            this.aYX.shutdown();
        }
        this.aYY = null;
    }
}
