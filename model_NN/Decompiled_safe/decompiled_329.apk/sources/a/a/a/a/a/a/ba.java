package a.a.a.a.a.a;

import javax.net.ssl.SSLException;

public class ba extends RuntimeException {
    private static final long serialVersionUID = -4448327177165687581L;
    private final SSLException bpk;
    private final byte bpl;

    protected ba(byte b2, SSLException sSLException) {
        super(sSLException);
        this.bpk = sSLException;
        this.bpl = b2;
    }

    /* access modifiers changed from: protected */
    public SSLException Fg() {
        return this.bpk;
    }

    /* access modifiers changed from: protected */
    public byte Fh() {
        return this.bpl;
    }
}
