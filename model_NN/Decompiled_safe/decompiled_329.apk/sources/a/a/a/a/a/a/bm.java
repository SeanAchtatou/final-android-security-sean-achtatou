package a.a.a.a.a.a;

import java.io.InputStream;

public class bm extends ae {
    private InputStream in;

    public bm(InputStream inputStream) {
        this.in = inputStream;
    }

    public int available() {
        return this.in.available();
    }

    public int read() {
        int read = this.in.read();
        if (read >= 0) {
            return read;
        }
        throw new ap();
    }
}
