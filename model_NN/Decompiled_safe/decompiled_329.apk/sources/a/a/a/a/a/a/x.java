package a.a.a.a.a.a;

import java.security.SecureRandom;
import java.util.Date;

public class x extends aj {
    byte[] UZ = new byte[2];
    bc Va;
    byte Vb;
    byte[] rZ = new byte[32];
    byte[] sa;

    public x(l lVar, int i) {
        this.UZ[0] = (byte) lVar.read();
        this.UZ[1] = (byte) lVar.read();
        lVar.read(this.rZ, 0, 32);
        int xb = lVar.xb();
        this.sa = new byte[xb];
        lVar.read(this.sa, 0, xb);
        this.Va = bc.b((byte) lVar.read(), (byte) lVar.read());
        this.Vb = (byte) lVar.read();
        this.length = this.sa.length + 38;
        if (this.length != i) {
            a((byte) 50, "DECODE ERROR: incorrect ServerHello");
        }
    }

    public x(SecureRandom secureRandom, byte[] bArr, byte[] bArr2, bc bcVar, byte b2) {
        long time = new Date().getTime() / 1000;
        secureRandom.nextBytes(this.rZ);
        this.rZ[0] = (byte) ((int) ((-16777216 & time) >>> 24));
        this.rZ[1] = (byte) ((int) ((16711680 & time) >>> 16));
        this.rZ[2] = (byte) ((int) ((65280 & time) >>> 8));
        this.rZ[3] = (byte) ((int) (time & 255));
        this.sa = bArr2;
        this.Va = bcVar;
        this.Vb = b2;
        this.UZ = bArr;
        this.length = bArr2.length + 38;
    }

    public void a(l lVar) {
        lVar.write(this.UZ);
        lVar.write(this.rZ);
        lVar.b((long) this.sa.length);
        lVar.write(this.sa);
        lVar.write(this.Va.FF());
        lVar.g(this.Vb);
        this.length = this.sa.length + 38;
    }

    public byte[] eG() {
        return this.rZ;
    }

    public int getType() {
        return 2;
    }
}
