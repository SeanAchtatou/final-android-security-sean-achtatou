package a.a.a.a.a.a;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.AccessController;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import javax.net.ssl.ManagerFactoryParameters;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactorySpi;

public class q extends TrustManagerFactorySpi {
    private KeyStore vw;

    public TrustManager[] engineGetTrustManagers() {
        if (this.vw == null) {
            throw new IllegalStateException("TrustManagerFactory is not initialized");
        }
        return new TrustManager[]{new bj(this.vw)};
    }

    public void engineInit(KeyStore keyStore) {
        if (keyStore != null) {
            this.vw = keyStore;
            return;
        }
        this.vw = KeyStore.getInstance(KeyStore.getDefaultType());
        String str = (String) AccessController.doPrivileged(new s(this));
        if (str == null || str.equalsIgnoreCase("NONE") || str.length() == 0) {
            try {
                this.vw.load(null, null);
            } catch (IOException e) {
                throw new KeyStoreException(e);
            } catch (CertificateException e2) {
                throw new KeyStoreException(e2);
            } catch (NoSuchAlgorithmException e3) {
                throw new KeyStoreException(e3);
            }
        } else {
            String str2 = (String) AccessController.doPrivileged(new r(this));
            try {
                this.vw.load(new FileInputStream(new File(str)), str2 == null ? new char[0] : str2.toCharArray());
            } catch (FileNotFoundException e4) {
                throw new KeyStoreException(e4);
            } catch (IOException e5) {
                throw new KeyStoreException(e5);
            } catch (CertificateException e6) {
                throw new KeyStoreException(e6);
            } catch (NoSuchAlgorithmException e7) {
                throw new KeyStoreException(e7);
            }
        }
    }

    public void engineInit(ManagerFactoryParameters managerFactoryParameters) {
        throw new InvalidAlgorithmParameterException("ManagerFactoryParameters not supported");
    }
}
