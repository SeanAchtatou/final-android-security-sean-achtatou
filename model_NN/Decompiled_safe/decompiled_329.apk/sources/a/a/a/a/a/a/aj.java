package a.a.a.a.a.a;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;

public abstract class aj {
    protected int length;

    /* access modifiers changed from: protected */
    public void a(byte b2, String str) {
        throw new ba(b2, new SSLHandshakeException(str));
    }

    /* access modifiers changed from: protected */
    public void a(byte b2, String str, Throwable th) {
        throw new ba(b2, new SSLException(str, th));
    }

    /* access modifiers changed from: package-private */
    public abstract void a(l lVar);

    /* access modifiers changed from: package-private */
    public abstract int getType();

    public int length() {
        return this.length;
    }
}
