package a.a.a.a.a.a;

import java.security.SecureRandom;
import java.util.Arrays;

public class m extends aj {
    final byte[] rY;
    final byte[] rZ;
    final byte[] sa;
    final bc[] sb;
    final byte[] sc;

    public m(l lVar) {
        this.rZ = new byte[32];
        if (lVar.xb() != 1) {
            a((byte) 50, "DECODE ERROR: incorrect V2ClientHello");
        }
        this.rY = new byte[2];
        this.rY[0] = (byte) lVar.xb();
        this.rY[1] = (byte) lVar.xb();
        int xc = lVar.xc();
        if (lVar.xc() != 0) {
            a((byte) 50, "DECODE ERROR: incorrect V2ClientHello, cannot be used for resuming");
        }
        int xc2 = lVar.xc();
        if (xc2 < 16) {
            a((byte) 50, "DECODE ERROR: incorrect V2ClientHello, short challenge data");
        }
        this.sa = new byte[0];
        this.sb = new bc[(xc / 3)];
        for (int i = 0; i < this.sb.length; i++) {
            this.sb[i] = bc.a((byte) lVar.read(), (byte) lVar.read(), (byte) lVar.read());
        }
        this.sc = new byte[]{0};
        if (xc2 < 32) {
            Arrays.fill(this.rZ, 0, 32 - xc2, (byte) 0);
            System.arraycopy(lVar.ak(xc2), 0, this.rZ, 32 - xc2, xc2);
        } else if (xc2 == 32) {
            System.arraycopy(lVar.ak(32), 0, this.rZ, 0, 32);
        } else {
            System.arraycopy(lVar.ak(xc2), xc2 - 32, this.rZ, 0, 32);
        }
        if (lVar.available() > 0) {
            a((byte) 50, "DECODE ERROR: incorrect V2ClientHello, extra data");
        }
        this.length = this.sa.length + 38 + (this.sb.length << 1) + this.sc.length;
    }

    public m(l lVar, int i) {
        this.rZ = new byte[32];
        this.rY = new byte[2];
        this.rY[0] = (byte) lVar.xb();
        this.rY[1] = (byte) lVar.xb();
        lVar.read(this.rZ, 0, 32);
        int read = lVar.read();
        this.sa = new byte[read];
        lVar.read(this.sa, 0, read);
        int xc = lVar.xc();
        if ((xc & 1) == 1) {
            a((byte) 50, "DECODE ERROR: incorrect ClientHello");
        }
        int i2 = xc >> 1;
        this.sb = new bc[i2];
        for (int i3 = 0; i3 < i2; i3++) {
            this.sb[i3] = bc.b((byte) lVar.read(), (byte) lVar.read());
        }
        int read2 = lVar.read();
        this.sc = new byte[read2];
        lVar.read(this.sc, 0, read2);
        this.length = this.sa.length + 38 + (this.sb.length << 1) + this.sc.length;
        if (this.length > i) {
            a((byte) 50, "DECODE ERROR: incorrect ClientHello");
        }
        if (this.length < i) {
            lVar.skip((long) (i - this.length));
            this.length = i;
        }
    }

    public m(SecureRandom secureRandom, byte[] bArr, byte[] bArr2, bc[] bcVarArr) {
        this.rZ = new byte[32];
        this.rY = bArr;
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        secureRandom.nextBytes(this.rZ);
        this.rZ[0] = (byte) ((int) (currentTimeMillis & 255));
        this.rZ[1] = (byte) ((int) (currentTimeMillis & 255));
        this.rZ[2] = (byte) ((int) (currentTimeMillis & 255));
        this.rZ[3] = (byte) ((int) (currentTimeMillis & 255));
        this.sa = bArr2;
        this.sb = bcVarArr;
        this.sc = new byte[]{0};
        this.length = this.sa.length + 38 + (this.sb.length << 1) + this.sc.length;
    }

    public void a(l lVar) {
        lVar.write(this.rY);
        lVar.write(this.rZ);
        lVar.b((long) this.sa.length);
        lVar.write(this.sa);
        lVar.c((long) (this.sb.length << 1));
        for (bc FF : this.sb) {
            lVar.write(FF.FF());
        }
        lVar.b((long) this.sc.length);
        for (byte g : this.sc) {
            lVar.g(g);
        }
    }

    public byte[] eG() {
        return this.rZ;
    }

    public int getType() {
        return 1;
    }
}
