package a.a.a.a.a.a;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.KeyManagementException;
import javax.net.ssl.SSLSocketFactory;

public class i extends SSLSocketFactory {
    private bf lr;
    private IOException mr;

    public i() {
        try {
            this.lr = bf.GL();
        } catch (KeyManagementException e) {
            this.mr = new IOException("Delayed instantiation exception:");
            this.mr.initCause(e);
        }
    }

    protected i(bf bfVar) {
        this.lr = bfVar;
    }

    public Socket createSocket() {
        if (this.mr == null) {
            return new aw((bf) this.lr.clone());
        }
        throw this.mr;
    }

    public Socket createSocket(String str, int i) {
        if (this.mr == null) {
            return new aw(str, i, (bf) this.lr.clone());
        }
        throw this.mr;
    }

    public Socket createSocket(String str, int i, InetAddress inetAddress, int i2) {
        if (this.mr != null) {
            throw this.mr;
        }
        return new aw(str, i, inetAddress, i2, (bf) this.lr.clone());
    }

    public Socket createSocket(InetAddress inetAddress, int i) {
        if (this.mr == null) {
            return new aw(inetAddress, i, (bf) this.lr.clone());
        }
        throw this.mr;
    }

    public Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) {
        if (this.mr != null) {
            throw this.mr;
        }
        return new aw(inetAddress, i, inetAddress2, i2, (bf) this.lr.clone());
    }

    public Socket createSocket(Socket socket, String str, int i, boolean z) {
        if (this.mr == null) {
            return new as(socket, z, (bf) this.lr.clone());
        }
        throw this.mr;
    }

    public String[] getDefaultCipherSuites() {
        return this.mr != null ? new String[0] : this.lr.getEnabledCipherSuites();
    }

    public String[] getSupportedCipherSuites() {
        return this.mr != null ? new String[0] : bc.FE();
    }
}
