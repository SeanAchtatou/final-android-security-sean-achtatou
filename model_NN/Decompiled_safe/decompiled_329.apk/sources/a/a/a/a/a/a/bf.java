package a.a.a.a.a.a;

import java.security.SecureRandom;
import javax.net.ssl.X509KeyManager;
import javax.net.ssl.X509TrustManager;

public class bf {
    private static X509KeyManager bwL;
    private static X509TrustManager bwM;
    private static SecureRandom bwN;
    private static bf bwO;
    private X509KeyManager bwP;
    private X509TrustManager bwQ;
    private SecureRandom bwR;
    protected bc[] bwS;
    private String[] bwT;
    private String[] bwU;
    private boolean bwV;
    private boolean bwW;
    private boolean bwX;
    private boolean bwY;
    private y lp;
    private y lq;

    private bf() {
        this.bwT = null;
        this.bwU = o.ug;
        this.bwV = true;
        this.bwW = false;
        this.bwX = false;
        this.bwY = true;
        this.bwS = bc.btJ;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000c, code lost:
        if (r7.length == 0) goto L_0x000e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0028 A[Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0064 A[Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x007b A[Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x009e A[SYNTHETIC, Splitter:B:50:0x009e] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00b0  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00c7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected bf(javax.net.ssl.KeyManager[] r7, javax.net.ssl.TrustManager[] r8, java.security.SecureRandom r9, a.a.a.a.a.a.y r10, a.a.a.a.a.a.y r11) {
        /*
            r6 = this;
            r5 = 1
            r4 = 0
            r6.<init>()
            r6.lq = r11
            r6.lp = r10
            if (r7 == 0) goto L_0x000e
            int r0 = r7.length     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            if (r0 != 0) goto L_0x004f
        L_0x000e:
            javax.net.ssl.X509KeyManager r0 = a.a.a.a.a.a.bf.bwL     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            if (r0 != 0) goto L_0x004b
            java.lang.String r0 = javax.net.ssl.KeyManagerFactory.getDefaultAlgorithm()     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            javax.net.ssl.KeyManagerFactory r0 = javax.net.ssl.KeyManagerFactory.getInstance(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            r1 = 0
            r2 = 0
            r0.init(r1, r2)     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            javax.net.ssl.KeyManager[] r0 = r0.getKeyManagers()     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            r1 = r5
        L_0x0024:
            javax.net.ssl.X509KeyManager r2 = r6.bwP     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            if (r2 != 0) goto L_0x005b
            r2 = r4
        L_0x0029:
            int r3 = r0.length     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            if (r2 >= r3) goto L_0x0038
            r3 = r0[r2]     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            boolean r3 = r3 instanceof javax.net.ssl.X509KeyManager     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            if (r3 == 0) goto L_0x0052
            r0 = r0[r2]     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            javax.net.ssl.X509KeyManager r0 = (javax.net.ssl.X509KeyManager) r0     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            r6.bwP = r0     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
        L_0x0038:
            javax.net.ssl.X509KeyManager r0 = r6.bwP     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            if (r0 != 0) goto L_0x0055
            java.security.KeyManagementException r0 = new java.security.KeyManagementException     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            java.lang.String r1 = "No X509KeyManager found"
            r0.<init>(r1)     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            throw r0     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
        L_0x0044:
            r0 = move-exception
            java.security.KeyManagementException r1 = new java.security.KeyManagementException
            r1.<init>(r0)
            throw r1
        L_0x004b:
            javax.net.ssl.X509KeyManager r0 = a.a.a.a.a.a.bf.bwL     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            r6.bwP = r0     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
        L_0x004f:
            r1 = r4
            r0 = r7
            goto L_0x0024
        L_0x0052:
            int r2 = r2 + 1
            goto L_0x0029
        L_0x0055:
            if (r1 == 0) goto L_0x005b
            javax.net.ssl.X509KeyManager r0 = r6.bwP     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            a.a.a.a.a.a.bf.bwL = r0     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
        L_0x005b:
            if (r8 == 0) goto L_0x0060
            int r0 = r8.length     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            if (r0 != 0) goto L_0x00a2
        L_0x0060:
            javax.net.ssl.X509TrustManager r0 = a.a.a.a.a.a.bf.bwM     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            if (r0 != 0) goto L_0x009e
            java.lang.String r0 = javax.net.ssl.TrustManagerFactory.getDefaultAlgorithm()     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            javax.net.ssl.TrustManagerFactory r1 = javax.net.ssl.TrustManagerFactory.getInstance(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            r0 = 0
            java.security.KeyStore r0 = (java.security.KeyStore) r0     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            r1.init(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            javax.net.ssl.TrustManager[] r0 = r1.getTrustManagers()     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            r1 = r5
        L_0x0077:
            javax.net.ssl.X509TrustManager r2 = r6.bwQ     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            if (r2 != 0) goto L_0x00ae
            r2 = r4
        L_0x007c:
            int r3 = r0.length     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            if (r2 >= r3) goto L_0x008b
            r3 = r0[r2]     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            boolean r3 = r3 instanceof javax.net.ssl.X509TrustManager     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            if (r3 == 0) goto L_0x00a5
            r0 = r0[r2]     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            javax.net.ssl.X509TrustManager r0 = (javax.net.ssl.X509TrustManager) r0     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            r6.bwQ = r0     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
        L_0x008b:
            javax.net.ssl.X509TrustManager r0 = r6.bwQ     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            if (r0 != 0) goto L_0x00a8
            java.security.KeyManagementException r0 = new java.security.KeyManagementException     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            java.lang.String r1 = "No X509TrustManager found"
            r0.<init>(r1)     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            throw r0     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
        L_0x0097:
            r0 = move-exception
            java.security.KeyManagementException r1 = new java.security.KeyManagementException
            r1.<init>(r0)
            throw r1
        L_0x009e:
            javax.net.ssl.X509TrustManager r0 = a.a.a.a.a.a.bf.bwM     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            r6.bwQ = r0     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
        L_0x00a2:
            r1 = r4
            r0 = r8
            goto L_0x0077
        L_0x00a5:
            int r2 = r2 + 1
            goto L_0x007c
        L_0x00a8:
            if (r1 == 0) goto L_0x00ae
            javax.net.ssl.X509TrustManager r0 = r6.bwQ     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
            a.a.a.a.a.a.bf.bwM = r0     // Catch:{ NoSuchAlgorithmException -> 0x0044, KeyStoreException -> 0x0097, UnrecoverableKeyException -> 0x00c0 }
        L_0x00ae:
            if (r9 != 0) goto L_0x00c7
            java.security.SecureRandom r0 = a.a.a.a.a.a.bf.bwN
            if (r0 != 0) goto L_0x00bb
            java.security.SecureRandom r0 = new java.security.SecureRandom
            r0.<init>()
            a.a.a.a.a.a.bf.bwN = r0
        L_0x00bb:
            java.security.SecureRandom r0 = a.a.a.a.a.a.bf.bwN
            r6.bwR = r0
        L_0x00bf:
            return
        L_0x00c0:
            r0 = move-exception
            java.security.KeyManagementException r1 = new java.security.KeyManagementException
            r1.<init>(r0)
            throw r1
        L_0x00c7:
            r6.bwR = r9
            goto L_0x00bf
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.a.a.bf.<init>(javax.net.ssl.KeyManager[], javax.net.ssl.TrustManager[], java.security.SecureRandom, a.a.a.a.a.a.y, a.a.a.a.a.a.y):void");
    }

    protected static bf GL() {
        if (bwO == null) {
            bwO = new bf(null, null, null, new y(), new y());
        }
        return (bf) bwO.clone();
    }

    /* access modifiers changed from: protected */
    public y GM() {
        return this.lq;
    }

    /* access modifiers changed from: protected */
    public y GN() {
        return this.lp;
    }

    /* access modifiers changed from: protected */
    public X509KeyManager GO() {
        return this.bwP;
    }

    /* access modifiers changed from: protected */
    public X509TrustManager GP() {
        return this.bwQ;
    }

    /* access modifiers changed from: protected */
    public SecureRandom GQ() {
        return this.bwR;
    }

    /* access modifiers changed from: protected */
    public Object clone() {
        bf bfVar = new bf();
        bfVar.lp = this.lp;
        bfVar.lq = this.lq;
        bfVar.bwP = this.bwP;
        bfVar.bwQ = this.bwQ;
        bfVar.bwR = this.bwR;
        bfVar.bwS = this.bwS;
        bfVar.bwU = this.bwU;
        bfVar.bwV = this.bwV;
        bfVar.bwW = this.bwW;
        bfVar.bwX = this.bwX;
        bfVar.bwY = this.bwY;
        return bfVar;
    }

    /* access modifiers changed from: protected */
    public boolean getEnableSessionCreation() {
        return this.bwY;
    }

    /* access modifiers changed from: protected */
    public String[] getEnabledCipherSuites() {
        if (this.bwT == null) {
            this.bwT = new String[this.bwS.length];
            for (int i = 0; i < this.bwS.length; i++) {
                this.bwT[i] = this.bwS[i].getName();
            }
        }
        return (String[]) this.bwT.clone();
    }

    /* access modifiers changed from: protected */
    public String[] getEnabledProtocols() {
        return (String[]) this.bwU.clone();
    }

    /* access modifiers changed from: protected */
    public boolean getNeedClientAuth() {
        return this.bwW;
    }

    /* access modifiers changed from: protected */
    public boolean getUseClientMode() {
        return this.bwV;
    }

    /* access modifiers changed from: protected */
    public boolean getWantClientAuth() {
        return this.bwX;
    }

    /* access modifiers changed from: protected */
    public void setEnableSessionCreation(boolean z) {
        this.bwY = z;
    }

    /* access modifiers changed from: protected */
    public void setEnabledCipherSuites(String[] strArr) {
        if (strArr == null) {
            throw new IllegalArgumentException("Provided parameter is null");
        }
        bc[] bcVarArr = new bc[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            bcVarArr[i] = bc.eI(strArr[i]);
            if (bcVarArr[i] == null || !bcVarArr[i].bsc) {
                throw new IllegalArgumentException(strArr[i] + " is not supported.");
            }
        }
        this.bwS = bcVarArr;
        this.bwT = strArr;
    }

    /* access modifiers changed from: protected */
    public void setEnabledProtocols(String[] strArr) {
        if (strArr == null) {
            throw new IllegalArgumentException("Provided parameter is null");
        }
        for (int i = 0; i < strArr.length; i++) {
            if (!o.isSupported(strArr[i])) {
                throw new IllegalArgumentException("Protocol " + strArr[i] + " is not supported.");
            }
        }
        this.bwU = strArr;
    }

    /* access modifiers changed from: protected */
    public void setNeedClientAuth(boolean z) {
        this.bwW = z;
        this.bwX = false;
    }

    /* access modifiers changed from: protected */
    public void setUseClientMode(boolean z) {
        this.bwV = z;
    }

    /* access modifiers changed from: protected */
    public void setWantClientAuth(boolean z) {
        this.bwX = z;
        this.bwW = false;
    }
}
