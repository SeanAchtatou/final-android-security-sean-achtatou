package a.a.a.a.a.a;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.AccessController;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.CertificateException;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactorySpi;
import javax.net.ssl.ManagerFactoryParameters;

public class ay extends KeyManagerFactorySpi {
    private char[] bnc;
    private KeyStore vw;

    public KeyManager[] engineGetKeyManagers() {
        if (this.vw == null) {
            throw new IllegalStateException("KeyManagerFactory is not initialized");
        }
        return new KeyManager[]{new bn(this.vw, this.bnc)};
    }

    public void engineInit(KeyStore keyStore, char[] cArr) {
        if (keyStore != null) {
            this.vw = keyStore;
            if (cArr != null) {
                this.bnc = (char[]) cArr.clone();
            } else {
                this.bnc = new char[0];
            }
        } else {
            this.vw = KeyStore.getInstance(KeyStore.getDefaultType());
            String str = (String) AccessController.doPrivileged(new b(this));
            if (str == null || str.equalsIgnoreCase("NONE") || str.length() == 0) {
                try {
                    this.vw.load(null, null);
                } catch (IOException e) {
                    throw new KeyStoreException(e);
                } catch (CertificateException e2) {
                    throw new KeyStoreException(e2);
                }
            } else {
                String str2 = (String) AccessController.doPrivileged(new a(this));
                if (str2 == null) {
                    this.bnc = new char[0];
                } else {
                    this.bnc = str2.toCharArray();
                }
                try {
                    this.vw.load(new FileInputStream(new File(str)), this.bnc);
                } catch (FileNotFoundException e3) {
                    throw new KeyStoreException(e3);
                } catch (IOException e4) {
                    throw new KeyStoreException(e4);
                } catch (CertificateException e5) {
                    throw new KeyStoreException(e5);
                }
            }
        }
    }

    public void engineInit(ManagerFactoryParameters managerFactoryParameters) {
        throw new InvalidAlgorithmParameterException("ManagerFactoryParameters not supported");
    }
}
