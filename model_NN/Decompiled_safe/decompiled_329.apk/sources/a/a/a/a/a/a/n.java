package a.a.a.a.a.a;

import com.uc.b.b;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.security.interfaces.RSAKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;
import javax.net.ssl.SSLEngineResult;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;

public abstract class n {
    public static final int sL = 1;
    public static final int sM = 2;
    public static final int sN = 3;
    public static final int sO = 4;
    public static Set sP = new HashSet();
    protected l sQ = new l();
    protected an sR;
    protected bf sS;
    protected Vector sT = new Vector();
    protected boolean sU;
    protected bk sV;
    protected m sW;
    protected x sX;
    protected d sY;
    protected aa sZ;
    protected int status = 2;
    protected ar ta;
    protected ag tb;
    protected d tc;
    protected bh td;
    protected k te;
    protected az tf;
    protected az tg;
    protected boolean th = false;
    protected boolean ti = false;
    protected byte[] tj;
    protected Exception tk;
    private byte[] tl = new byte[12];
    private byte[] tm = {109, 97, 115, 116, 101, 114, 32, 115, 101, b.pC, 114, 101, 116};
    private boolean tn = false;
    protected boolean to = false;
    public bp tp;
    public aw tq;

    protected n(Object obj) {
        if (obj instanceof bp) {
            this.tp = (bp) obj;
            this.sU = true;
            this.sS = this.tp.lr;
        } else if (obj instanceof aw) {
            this.tq = (aw) obj;
            this.sU = false;
            this.sS = this.tq.lr;
        }
    }

    protected static int a(PublicKey publicKey) {
        return (publicKey instanceof RSAKey ? ((RSAKey) publicKey).getModulus() : ((RSAPublicKeySpec) KeyFactory.getInstance("RSA").getKeySpec(publicKey, RSAPublicKeySpec.class)).getModulus()).bitLength();
    }

    public abstract void C(byte[] bArr);

    public abstract void D(byte[] bArr);

    /* access modifiers changed from: protected */
    public void E(byte[] bArr) {
        this.tl = new byte[36];
        e(bArr, this.tl);
    }

    /* access modifiers changed from: protected */
    public void F(byte[] bArr) {
        if (!Arrays.equals(this.tl, bArr)) {
            a((byte) 40, "Incorrect FINISED");
        }
    }

    /* access modifiers changed from: protected */
    public void J(String str) {
        a(str, this.tl);
    }

    /* access modifiers changed from: protected */
    public void a(byte b2, String str) {
        throw new ba(b2, new SSLHandshakeException(str));
    }

    /* access modifiers changed from: protected */
    public void a(byte b2, String str, Exception exc) {
        throw new ba(b2, new SSLException(str, exc));
    }

    /* access modifiers changed from: protected */
    public void a(byte b2, SSLException sSLException) {
        throw new ba(b2, sSLException);
    }

    public void a(aj ajVar) {
        this.sQ.b((long) ajVar.getType());
        this.sQ.d((long) ajVar.length());
        ajVar.a(this.sQ);
    }

    public void a(an anVar) {
        this.sR = anVar;
    }

    /* access modifiers changed from: protected */
    public void a(String str, byte[] bArr) {
        byte[] em = this.sQ.em();
        byte[] en = this.sQ.en();
        byte[] bArr2 = new byte[(em.length + en.length)];
        System.arraycopy(em, 0, bArr2, 0, em.length);
        System.arraycopy(en, 0, bArr2, em.length, en.length);
        try {
            u.a(bArr, this.sV.bYq, str.getBytes(), bArr2);
        } catch (GeneralSecurityException e) {
            a((byte) 80, "PRF error", e);
        }
    }

    /* access modifiers changed from: protected */
    public void e(byte[] bArr, byte[] bArr2) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            MessageDigest instance2 = MessageDigest.getInstance("SHA-1");
            try {
                byte[] eq = this.sQ.eq();
                instance.update(eq);
                instance.update(bArr);
                instance.update(this.sV.bYq);
                byte[] digest = instance.digest(bq.chD);
                instance.update(this.sV.bYq);
                instance.update(bq.chF);
                System.arraycopy(instance.digest(digest), 0, bArr2, 0, 16);
                instance2.update(eq);
                instance2.update(bArr);
                instance2.update(this.sV.bYq);
                byte[] digest2 = instance2.digest(bq.chE);
                instance2.update(this.sV.bYq);
                instance2.update(bq.chG);
                System.arraycopy(instance2.digest(digest2), 0, bArr2, 16, 20);
            } catch (Exception e) {
                a((byte) 80, "INTERNAL ERROR", e);
            }
        } catch (Exception e2) {
            a((byte) 80, "Could not initialize the Digest Algorithms.", e2);
        }
    }

    public SSLEngineResult.HandshakeStatus eW() {
        if (this.sQ.ed() || this.tn || this.to || this.tk != null) {
            return SSLEngineResult.HandshakeStatus.NEED_WRAP;
        }
        if (!this.sT.isEmpty()) {
            return SSLEngineResult.HandshakeStatus.NEED_TASK;
        }
        switch (this.status) {
            case 1:
                return SSLEngineResult.HandshakeStatus.NEED_UNWRAP;
            case 2:
            default:
                return SSLEngineResult.HandshakeStatus.NOT_HANDSHAKING;
            case 3:
                this.status = 2;
                fg();
                return SSLEngineResult.HandshakeStatus.FINISHED;
        }
    }

    public bk eX() {
        return this.sV;
    }

    /* access modifiers changed from: protected */
    public void eY() {
        this.tn = true;
    }

    /* access modifiers changed from: protected */
    public void eZ() {
        this.to = true;
    }

    /* access modifiers changed from: package-private */
    public abstract void fa();

    /* access modifiers changed from: package-private */
    public abstract void fb();

    public byte[] fc() {
        if (this.tk != null) {
            Exception exc = this.tk;
            this.tk = null;
            a((byte) 40, "Error occured in delegated task:" + exc.getMessage(), exc);
        }
        if (this.sQ.ed()) {
            return this.sR.a((byte) 22, this.sQ);
        }
        if (this.tn) {
            fb();
            this.tn = false;
            return this.sR.c(eX());
        } else if (!this.to) {
            return null;
        } else {
            this.to = false;
            return this.sR.c((byte) 22, new byte[]{0, 0, 0, 0}, 0, 4);
        }
    }

    /* access modifiers changed from: protected */
    public void fd() {
        a((byte) 10, "UNEXPECTED MESSAGE");
    }

    public void fe() {
        byte[] bArr = new byte[64];
        System.arraycopy(this.sW.eG(), 0, bArr, 0, 32);
        System.arraycopy(this.sX.eG(), 0, bArr, 32, 32);
        this.sV.bYq = new byte[48];
        if (this.sX.UZ[1] == 1) {
            try {
                u.a(this.sV.bYq, this.tj, this.tm, bArr);
            } catch (GeneralSecurityException e) {
                a((byte) 80, "PRF error", e);
            }
        } else {
            u.a(this.sV.bYq, this.tj, bArr);
        }
        Arrays.fill(this.tj, (byte) 0);
        this.tj = null;
    }

    public Runnable ff() {
        if (this.sT.isEmpty()) {
            return null;
        }
        return (Runnable) this.sT.remove(0);
    }

    /* access modifiers changed from: protected */
    public void fg() {
        this.sQ.el();
        this.sW = null;
        this.sX = null;
        this.sY = null;
        this.sZ = null;
        this.ta = null;
        this.tb = null;
        this.tc = null;
        this.td = null;
        this.te = null;
        this.tf = null;
        this.tg = null;
    }

    /* access modifiers changed from: protected */
    public void h(byte b2) {
        this.sR.a((byte) 1, b2);
    }

    /* access modifiers changed from: protected */
    public void shutdown() {
        fg();
        this.sV = null;
        this.tj = null;
        this.sT.clear();
    }

    public abstract void start();

    /* access modifiers changed from: protected */
    public void stop() {
        fg();
        this.status = 2;
    }
}
