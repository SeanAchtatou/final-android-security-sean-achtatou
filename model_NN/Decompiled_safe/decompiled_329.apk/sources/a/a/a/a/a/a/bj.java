package a.a.a.a.a.a;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.cert.CertPathValidator;
import java.security.cert.CertPathValidatorException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import javax.net.ssl.X509TrustManager;

public class bj implements X509TrustManager {
    private CertPathValidator bXM;
    private PKIXParameters bXN;
    private Exception bXO = null;
    private CertificateFactory bXP;

    public bj(KeyStore keyStore) {
        try {
            this.bXM = CertPathValidator.getInstance("PKIX");
            this.bXP = CertificateFactory.getInstance("X509");
            HashSet hashSet = new HashSet();
            Enumeration<String> aliases = keyStore.aliases();
            while (aliases.hasMoreElements()) {
                X509Certificate x509Certificate = (X509Certificate) keyStore.getCertificate(aliases.nextElement());
                if (x509Certificate != null) {
                    hashSet.add(new TrustAnchor(x509Certificate, null));
                }
            }
            this.bXN = new PKIXParameters(hashSet);
            this.bXN.setRevocationEnabled(false);
        } catch (Exception e) {
            this.bXO = e;
        }
    }

    public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
        if (x509CertificateArr == null || x509CertificateArr.length == 0 || str == null || str.length() == 0) {
            throw new IllegalArgumentException("null or zero-length parameter");
        } else if (this.bXO != null) {
            throw new CertificateException(this.bXO);
        } else {
            try {
                this.bXM.validate(this.bXP.generateCertPath(Arrays.asList(x509CertificateArr)), this.bXN);
            } catch (InvalidAlgorithmParameterException e) {
                throw new CertificateException(e);
            } catch (CertPathValidatorException e2) {
                throw new CertificateException(e2);
            }
        }
    }

    public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
        if (x509CertificateArr == null || x509CertificateArr.length == 0 || str == null || str.length() == 0) {
            throw new IllegalArgumentException("null or zero-length parameter");
        } else if (this.bXO != null) {
            throw new CertificateException(this.bXO);
        } else {
            try {
                this.bXM.validate(this.bXP.generateCertPath(Arrays.asList(x509CertificateArr)), this.bXN);
            } catch (InvalidAlgorithmParameterException e) {
                throw new CertificateException(e);
            } catch (CertPathValidatorException e2) {
                throw new CertificateException(e2);
            }
        }
    }

    public X509Certificate[] getAcceptedIssuers() {
        int i = 0;
        if (this.bXN == null) {
            return new X509Certificate[0];
        }
        Set<TrustAnchor> trustAnchors = this.bXN.getTrustAnchors();
        X509Certificate[] x509CertificateArr = new X509Certificate[trustAnchors.size()];
        for (TrustAnchor trustedCert : trustAnchors) {
            x509CertificateArr[i] = trustedCert.getTrustedCert();
            i++;
        }
        return x509CertificateArr;
    }
}
