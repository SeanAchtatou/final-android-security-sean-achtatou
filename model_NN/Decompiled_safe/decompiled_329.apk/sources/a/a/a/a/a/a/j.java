package a.a.a.a.a.a;

import java.nio.ByteBuffer;

public class j implements ak {
    private ByteBuffer[] pF;
    private int pG;
    private int pH;
    private int pI;
    private int pJ;

    protected j() {
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer[] byteBufferArr, int i, int i2) {
        this.pF = byteBufferArr;
        this.pG = i;
        this.pH = i + i2;
        this.pJ = 0;
        this.pI = 0;
        for (int i3 = i; i3 < this.pH; i3++) {
            if (byteBufferArr[i3] == null) {
                throw new IllegalStateException("Some of the input parameters are null");
            }
            this.pI += byteBufferArr[i3].remaining();
        }
    }

    public byte[] ai(int i) {
        int i2 = i < this.pI ? i : this.pI;
        this.pI -= i2;
        this.pJ += i2;
        byte[] bArr = new byte[i2];
        int i3 = i2;
        int i4 = 0;
        loop0:
        while (this.pG < this.pH) {
            while (this.pF[this.pG].hasRemaining()) {
                int i5 = i4 + 1;
                bArr[i4] = this.pF[this.pG].get();
                int i6 = i3 - 1;
                if (i6 == 0) {
                    break loop0;
                }
                i3 = i6;
                i4 = i5;
            }
            this.pG++;
        }
        return bArr;
    }

    public int available() {
        return this.pI;
    }

    public boolean ed() {
        return this.pI > 0;
    }

    /* access modifiers changed from: protected */
    public int ee() {
        return this.pJ;
    }
}
