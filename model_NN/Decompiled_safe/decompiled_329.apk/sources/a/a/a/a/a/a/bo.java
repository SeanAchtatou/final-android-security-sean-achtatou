package a.a.a.a.a.a;

import com.uc.browser.UCR;

public class bo {
    protected static final byte cbA = 51;
    protected static final byte cbB = 60;
    protected static final byte cbC = 70;
    protected static final byte cbD = 71;
    protected static final byte cbE = 80;
    protected static final byte cbF = 90;
    protected static final byte cbG = 100;
    protected static final byte cbi = 1;
    protected static final byte cbj = 2;
    protected static final byte cbk = 0;
    protected static final byte cbl = 10;
    protected static final byte cbm = 20;
    protected static final byte cbn = 21;
    protected static final byte cbo = 22;
    protected static final byte cbp = 30;
    protected static final byte cbq = 40;
    protected static final byte cbr = 42;
    protected static final byte cbs = 43;
    protected static final byte cbt = 44;
    protected static final byte cbu = 45;
    protected static final byte cbv = 46;
    protected static final byte cbw = 47;
    protected static final byte cbx = 48;
    protected static final byte cby = 49;
    protected static final byte cbz = 50;
    private final byte[] cbH = new byte[2];
    private an sR;

    protected bo() {
    }

    /* access modifiers changed from: protected */
    public byte Fh() {
        if (this.cbH[0] != 0) {
            return this.cbH[1];
        }
        return -100;
    }

    /* access modifiers changed from: protected */
    public boolean PA() {
        return this.cbH[0] == 2;
    }

    /* access modifiers changed from: protected */
    public String PB() {
        switch (this.cbH[1]) {
            case 0:
                return "close_notify";
            case 10:
                return "unexpected_message";
            case 20:
                return "bad_record_mac";
            case 21:
                return "decryption_failed";
            case 22:
                return "record_overflow";
            case 30:
                return "decompression_failure";
            case 40:
                return "handshake_failure";
            case 42:
                return "bad_certificate";
            case 43:
                return "unsupported_certificate";
            case 44:
                return "certificate_revoked";
            case UCR.Color.bSo /*45*/:
                return "certificate_expired";
            case UCR.Color.bSp /*46*/:
                return "certificate_unknown";
            case 47:
                return "illegal_parameter";
            case 48:
                return "unknown_ca";
            case 49:
                return "access_denied";
            case 50:
                return "decode_error";
            case 51:
                return "decrypt_error";
            case 60:
                return "export_restriction";
            case 70:
                return "protocol_version";
            case UCR.Color.bSK /*71*/:
                return "insufficient_security";
            case 80:
                return "internal_error";
            case 90:
                return "user_canceled";
            case 100:
                return "no_renegotiation";
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void Py() {
        this.cbH[0] = 0;
    }

    /* access modifiers changed from: protected */
    public boolean Pz() {
        return this.cbH[0] != 0;
    }

    /* access modifiers changed from: protected */
    public void a(byte b2, byte b3) {
        this.cbH[0] = b2;
        this.cbH[1] = b3;
    }

    /* access modifiers changed from: protected */
    public void a(an anVar) {
        this.sR = anVar;
    }

    /* access modifiers changed from: protected */
    public byte[] fc() {
        return this.sR.c((byte) 21, this.cbH, 0, 2);
    }

    /* access modifiers changed from: protected */
    public void shutdown() {
        this.cbH[0] = 0;
        this.cbH[1] = 0;
        this.sR = null;
    }
}
