package a.a.a.a.a.a;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;

public class as extends aw {
    private final Socket bes;
    private final boolean bet;

    protected as(Socket socket, boolean z, bf bfVar) {
        super(bfVar);
        if (!socket.isConnected()) {
            throw new SocketException("Socket is not connected.");
        }
        this.bes = socket;
        this.bet = z;
        a();
    }

    /* access modifiers changed from: protected */
    public void Cf() {
        this.blJ = this.bes.getInputStream();
        this.blK = this.bes.getOutputStream();
    }

    /* access modifiers changed from: protected */
    public void Cg() {
        if (this.bet && this.blJ != null) {
            this.bes.close();
            this.blJ.close();
            this.blK.close();
        }
    }

    public void bind(SocketAddress socketAddress) {
        throw new IOException("Underlying socket is already connected.");
    }

    public void connect(SocketAddress socketAddress) {
        throw new IOException("Underlying socket is already connected.");
    }

    public void connect(SocketAddress socketAddress, int i) {
        throw new IOException("Underlying socket is already connected.");
    }

    public InetAddress getInetAddress() {
        return this.bes.getInetAddress();
    }

    public boolean getKeepAlive() {
        return this.bes.getKeepAlive();
    }

    public InetAddress getLocalAddress() {
        return this.bes.getLocalAddress();
    }

    public int getLocalPort() {
        return this.bes.getLocalPort();
    }

    public SocketAddress getLocalSocketAddress() {
        return this.bes.getLocalSocketAddress();
    }

    public boolean getOOBInline() {
        return this.bes.getOOBInline();
    }

    public int getPort() {
        return this.bes.getPort();
    }

    public int getReceiveBufferSize() {
        return this.bes.getReceiveBufferSize();
    }

    public SocketAddress getRemoteSocketAddress() {
        return this.bes.getRemoteSocketAddress();
    }

    public boolean getReuseAddress() {
        return this.bes.getReuseAddress();
    }

    public int getSendBufferSize() {
        return this.bes.getSendBufferSize();
    }

    public int getSoLinger() {
        return this.bes.getSoLinger();
    }

    public int getSoTimeout() {
        return this.bes.getSoTimeout();
    }

    public boolean getTcpNoDelay() {
        return this.bes.getTcpNoDelay();
    }

    public int getTrafficClass() {
        return this.bes.getTrafficClass();
    }

    public boolean isBound() {
        return this.bes.isBound();
    }

    public boolean isClosed() {
        return this.bes.isClosed();
    }

    public boolean isConnected() {
        return this.bes.isConnected();
    }

    public boolean isInputShutdown() {
        return this.bes.isInputShutdown();
    }

    public boolean isOutputShutdown() {
        return this.bes.isOutputShutdown();
    }

    public void setKeepAlive(boolean z) {
        this.bes.setKeepAlive(z);
    }

    public void setReceiveBufferSize(int i) {
        this.bes.setReceiveBufferSize(i);
    }

    public void setReuseAddress(boolean z) {
        this.bes.setReuseAddress(z);
    }

    public void setSendBufferSize(int i) {
        this.bes.setSendBufferSize(i);
    }

    public void setSoLinger(boolean z, int i) {
        this.bes.setSoLinger(z, i);
    }

    public void setSoTimeout(int i) {
        this.bes.setSoTimeout(i);
    }

    public void setTcpNoDelay(boolean z) {
        this.bes.setTcpNoDelay(z);
    }

    public void setTrafficClass(int i) {
        this.bes.setTrafficClass(i);
    }

    public String toString() {
        return "SSL socket over " + this.bes.toString();
    }
}
