package a.a.a.a.a.a;

import java.io.IOException;
import java.math.BigInteger;
import java.security.AccessController;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHPublicKeySpec;

public class bi extends n {
    private PrivateKey bRA;

    public bi(Object obj) {
        super(obj);
        this.status = 1;
    }

    private bc a(bc[] bcVarArr) {
        for (int i = 0; i < bcVarArr.length; i++) {
            if (bcVarArr[i].bsc) {
                for (bc equals : this.sS.bwS) {
                    if (bcVarArr[i].equals(equals)) {
                        return bcVarArr[i];
                    }
                }
                continue;
            }
        }
        return null;
    }

    private bk aK(byte[] bArr) {
        return (bk) this.sS.GM().getSession(bArr);
    }

    public void C(byte[] bArr) {
        KeyFactory instance;
        KeyAgreement instance2;
        byte[] bArr2;
        byte[] bArr3;
        this.sQ.A(bArr);
        while (this.sQ.available() > 0) {
            this.sQ.ej();
            int read = this.sQ.read();
            int xd = this.sQ.xd();
            if (this.sQ.available() < xd) {
                this.sQ.reset();
                return;
            }
            switch (read) {
                case 1:
                    if (this.sW == null || this.status == 3) {
                        this.to = false;
                        this.sW = new m(this.sQ, xd);
                        if (!this.sU) {
                            MW();
                            break;
                        } else {
                            this.sT.add(new ai(new ad(this), this, AccessController.getContext()));
                            return;
                        }
                    } else {
                        fd();
                        return;
                    }
                    break;
                case 11:
                    if (this.ti || this.ta == null || this.tb == null || this.tc != null) {
                        fd();
                        return;
                    }
                    this.tc = new d(this.sQ, xd);
                    if (this.tc.dI.length == 0) {
                        if (!this.sS.getNeedClientAuth()) {
                            break;
                        } else {
                            a((byte) 40, "HANDSHAKE FAILURE: no client certificate received");
                            break;
                        }
                    } else {
                        try {
                            this.sS.GP().checkClientTrusted(this.tc.dI, this.tc.dI[0].getPublicKey().getAlgorithm());
                        } catch (CertificateException e) {
                            a((byte) 42, "Untrusted Client Certificate ", e);
                        }
                        try {
                            this.sV.bYn = this.tc.dI;
                            break;
                        } catch (IOException e2) {
                            this.sQ.reset();
                            return;
                        }
                    }
                case 15:
                    if (!this.ti && this.td != null && this.tc != null && !this.td.isEmpty() && this.te == null && !this.th) {
                        this.te = new k(this.sQ, xd);
                        am amVar = new am(this.sV.bYk.bsd);
                        amVar.a(this.sY.dI[0]);
                        if (this.sV.bYk.bsd == bc.bsq || this.sV.bYk.bsd == bc.bsp || this.sV.bYk.bsd == bc.bst || this.sV.bYk.bsd == bc.bsu) {
                            bArr2 = this.sQ.eo();
                            bArr3 = this.sQ.ep();
                        } else if (this.sV.bYk.bsd == bc.bsr || this.sV.bYk.bsd == bc.bss) {
                            bArr2 = null;
                            bArr3 = this.sQ.ep();
                        } else {
                            if (this.sV.bYk.bsd == bc.bsx || this.sV.bYk.bsd == bc.bsy) {
                            }
                            bArr2 = null;
                            bArr3 = null;
                        }
                        amVar.V(bArr2);
                        amVar.W(bArr3);
                        if (amVar.X(this.te.ql)) {
                            break;
                        } else {
                            a((byte) 51, "DECRYPT ERROR: CERTIFICATE_VERIFY incorrect signature");
                            break;
                        }
                    } else {
                        fd();
                        return;
                    }
                case 16:
                    if (!this.ti && this.tb != null && this.td == null && (this.tc != null || !this.sS.getNeedClientAuth())) {
                        if (this.sV.bYk.bsd == bc.bsp || this.sV.bYk.bsd == bc.bsq) {
                            this.td = new bh(this.sQ, xd, this.sX.UZ[1] == 1, true);
                            try {
                                Cipher instance3 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                                instance3.init(2, this.bRA);
                                this.tj = instance3.doFinal(this.td.bPm);
                                if (!(this.tj.length == 48 && this.tj[0] == this.sW.rY[0] && this.tj[1] == this.sW.rY[1])) {
                                    this.tj = new byte[48];
                                    this.sS.GQ().nextBytes(this.tj);
                                }
                            } catch (Exception e3) {
                                a((byte) 80, "INTERNAL ERROR", e3);
                            }
                        } else {
                            this.td = new bh(this.sQ, xd, this.sX.UZ[1] == 1, false);
                            if (this.td.isEmpty()) {
                                this.tj = ((DHPublicKey) this.tc.dI[0].getPublicKey()).getY().toByteArray();
                            } else {
                                try {
                                    instance = KeyFactory.getInstance("DH");
                                } catch (NoSuchAlgorithmException e4) {
                                    instance = KeyFactory.getInstance("DiffieHellman");
                                }
                                try {
                                    instance2 = KeyAgreement.getInstance("DH");
                                } catch (NoSuchAlgorithmException e5) {
                                    instance2 = KeyAgreement.getInstance("DiffieHellman");
                                }
                                try {
                                    PublicKey generatePublic = instance.generatePublic(new DHPublicKeySpec(new BigInteger(1, this.td.bPm), this.sZ.ZS, this.sZ.ZU));
                                    instance2.init(this.bRA);
                                    instance2.doPhase(generatePublic, true);
                                    this.tj = instance2.generateSecret();
                                } catch (Exception e6) {
                                    a((byte) 80, "INTERNAL ERROR", e6);
                                    return;
                                }
                            }
                        }
                        fe();
                        break;
                    } else {
                        fd();
                        return;
                    }
                case 20:
                    if (this.ti || this.th) {
                        this.tf = new az(this.sQ, xd);
                        F(this.tf.getData());
                        this.sS.GM().a(this.sV);
                        if (this.ti) {
                            this.sV.bYi = System.currentTimeMillis();
                            this.status = 3;
                            break;
                        } else {
                            eY();
                            break;
                        }
                    } else {
                        fd();
                        return;
                    }
                    break;
                default:
                    fd();
                    return;
            }
        }
    }

    public void D(byte[] bArr) {
        this.sQ.A(bArr);
        this.sQ.ej();
        try {
            this.sW = new m(this.sQ);
            if (this.sU) {
                this.sT.add(new ai(new ac(this), this, AccessController.getContext()));
            } else {
                MW();
            }
        } catch (IOException e) {
            this.sQ.reset();
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x02a6  */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x03dd  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x021c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void MW() {
        /*
            r14 = this;
            r12 = 65280(0xff00, float:9.1477E-41)
            r6 = 40
            r11 = 1
            r5 = 0
            r10 = 0
            r0 = r5
        L_0x0009:
            a.a.a.a.a.a.m r1 = r14.sW
            byte[] r1 = r1.sc
            int r1 = r1.length
            if (r0 >= r1) goto L_0x00d9
            a.a.a.a.a.a.m r1 = r14.sW
            byte[] r1 = r1.sc
            byte r1 = r1[r0]
            if (r1 != 0) goto L_0x00d5
        L_0x0018:
            a.a.a.a.a.a.m r0 = r14.sW
            byte[] r0 = r0.rY
            boolean r0 = a.a.a.a.a.a.o.G(r0)
            if (r0 != 0) goto L_0x004a
            r0 = 70
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "PROTOCOL VERSION. Unsupported client version "
            java.lang.StringBuilder r1 = r1.append(r2)
            a.a.a.a.a.a.m r2 = r14.sW
            byte[] r2 = r2.rY
            byte r2 = r2[r5]
            java.lang.StringBuilder r1 = r1.append(r2)
            a.a.a.a.a.a.m r2 = r14.sW
            byte[] r2 = r2.rY
            byte r2 = r2[r11]
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r14.a(r0, r1)
        L_0x004a:
            r14.ti = r5
            a.a.a.a.a.a.m r0 = r14.sW
            byte[] r0 = r0.sa
            int r0 = r0.length
            if (r0 == 0) goto L_0x006f
            a.a.a.a.a.a.bk r0 = r14.sV
            if (r0 == 0) goto L_0x0419
            a.a.a.a.a.a.bk r0 = r14.sV
            byte[] r0 = r0.FL
            a.a.a.a.a.a.m r1 = r14.sW
            byte[] r1 = r1.sa
            boolean r0 = java.util.Arrays.equals(r0, r1)
            if (r0 == 0) goto L_0x0419
            a.a.a.a.a.a.bk r0 = r14.sV
            boolean r0 = r0.isValid()
            if (r0 == 0) goto L_0x00e0
            r14.ti = r11
        L_0x006f:
            boolean r0 = r14.ti
            if (r0 == 0) goto L_0x0128
            a.a.a.a.a.a.bk r0 = r14.sV
            a.a.a.a.a.a.bc r0 = r0.bYk
            r1 = r5
        L_0x0078:
            a.a.a.a.a.a.m r2 = r14.sW
            a.a.a.a.a.a.bc[] r2 = r2.sb
            int r2 = r2.length
            if (r1 >= r2) goto L_0x0120
            a.a.a.a.a.a.m r2 = r14.sW
            a.a.a.a.a.a.bc[] r2 = r2.sb
            r2 = r2[r1]
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x011c
            r4 = r0
        L_0x008c:
            a.a.a.a.a.a.an r0 = r14.sR
            a.a.a.a.a.a.m r1 = r14.sW
            byte[] r1 = r1.rY
            r0.Z(r1)
            a.a.a.a.a.a.bk r0 = r14.sV
            a.a.a.a.a.a.m r1 = r14.sW
            byte[] r1 = r1.rY
            a.a.a.a.a.a.o r1 = a.a.a.a.a.a.o.H(r1)
            r0.bYj = r1
            a.a.a.a.a.a.bk r0 = r14.sV
            a.a.a.a.a.a.m r1 = r14.sW
            byte[] r1 = r1.rZ
            r0.bYr = r1
            a.a.a.a.a.a.x r0 = new a.a.a.a.a.a.x
            a.a.a.a.a.a.bf r1 = r14.sS
            java.security.SecureRandom r1 = r1.GQ()
            a.a.a.a.a.a.m r2 = r14.sW
            byte[] r2 = r2.rY
            a.a.a.a.a.a.bk r3 = r14.sV
            byte[] r3 = r3.getId()
            r0.<init>(r1, r2, r3, r4, r5)
            r14.sX = r0
            a.a.a.a.a.a.bk r0 = r14.sV
            a.a.a.a.a.a.x r1 = r14.sX
            byte[] r1 = r1.rZ
            r0.bYs = r1
            a.a.a.a.a.a.x r0 = r14.sX
            r14.a(r0)
            boolean r0 = r14.ti
            if (r0 == 0) goto L_0x0154
            r14.eY()
        L_0x00d4:
            return
        L_0x00d5:
            int r0 = r0 + 1
            goto L_0x0009
        L_0x00d9:
            java.lang.String r0 = "HANDSHAKE FAILURE. Incorrect client hello message"
            r14.a(r6, r0)
            goto L_0x0018
        L_0x00e0:
            r0 = r11
        L_0x00e1:
            a.a.a.a.a.a.m r1 = r14.sW
            byte[] r1 = r1.sa
            a.a.a.a.a.a.bk r1 = r14.aK(r1)
            if (r1 == 0) goto L_0x00f1
            boolean r2 = r1.isValid()
            if (r2 != 0) goto L_0x0110
        L_0x00f1:
            a.a.a.a.a.a.bf r1 = r14.sS
            boolean r1 = r1.getEnableSessionCreation()
            if (r1 != 0) goto L_0x010c
            if (r0 == 0) goto L_0x0107
            r0 = 100
            r14.h(r0)
            r0 = 2
            r14.status = r0
            r14.fg()
            goto L_0x00d4
        L_0x0107:
            java.lang.String r0 = "SSL Session may not be created"
            r14.a(r6, r0)
        L_0x010c:
            r14.sV = r10
            goto L_0x006f
        L_0x0110:
            java.lang.Object r0 = r1.clone()
            a.a.a.a.a.a.bk r0 = (a.a.a.a.a.a.bk) r0
            r14.sV = r0
            r14.ti = r11
            goto L_0x006f
        L_0x011c:
            int r1 = r1 + 1
            goto L_0x0078
        L_0x0120:
            java.lang.String r1 = "HANDSHAKE FAILURE. Incorrect client hello message"
            r14.a(r6, r1)
            r4 = r0
            goto L_0x008c
        L_0x0128:
            a.a.a.a.a.a.m r0 = r14.sW
            a.a.a.a.a.a.bc[] r0 = r0.sb
            a.a.a.a.a.a.bc r0 = r14.a(r0)
            if (r0 != 0) goto L_0x0137
            java.lang.String r1 = "HANDSHAKE FAILURE. NO COMMON SUITE"
            r14.a(r6, r1)
        L_0x0137:
            a.a.a.a.a.a.bf r1 = r14.sS
            boolean r1 = r1.getEnableSessionCreation()
            if (r1 != 0) goto L_0x0144
            java.lang.String r1 = "SSL Session may not be created"
            r14.a(r6, r1)
        L_0x0144:
            a.a.a.a.a.a.bk r1 = new a.a.a.a.a.a.bk
            a.a.a.a.a.a.bf r2 = r14.sS
            java.security.SecureRandom r2 = r2.GQ()
            r1.<init>(r0, r2)
            r14.sV = r1
            r4 = r0
            goto L_0x008c
        L_0x0154:
            boolean r0 = r4.FC()
            if (r0 != 0) goto L_0x01f4
            int r0 = r4.bsd
            int r1 = a.a.a.a.a.a.bc.bsp
            if (r0 == r1) goto L_0x0172
            int r0 = r4.bsd
            int r1 = a.a.a.a.a.a.bc.bsq
            if (r0 == r1) goto L_0x0172
            int r0 = r4.bsd
            int r1 = a.a.a.a.a.a.bc.bst
            if (r0 == r1) goto L_0x0172
            int r0 = r4.bsd
            int r1 = a.a.a.a.a.a.bc.bsu
            if (r0 != r1) goto L_0x019d
        L_0x0172:
            java.lang.String r0 = "RSA"
            r1 = r0
        L_0x0175:
            a.a.a.a.a.a.bf r0 = r14.sS
            javax.net.ssl.X509KeyManager r0 = r0.GO()
            boolean r2 = r0 instanceof javax.net.ssl.X509ExtendedKeyManager
            if (r2 == 0) goto L_0x01c8
            javax.net.ssl.X509ExtendedKeyManager r0 = (javax.net.ssl.X509ExtendedKeyManager) r0
            a.a.a.a.a.a.aw r2 = r14.tq
            if (r2 == 0) goto L_0x01c1
            a.a.a.a.a.a.aw r2 = r14.tq
            java.lang.String r1 = r0.chooseServerAlias(r1, r10, r2)
        L_0x018b:
            if (r1 == 0) goto L_0x0413
            java.security.cert.X509Certificate[] r0 = r0.getCertificateChain(r1)
        L_0x0191:
            r13 = r1
            r1 = r0
            r0 = r13
        L_0x0194:
            if (r1 != 0) goto L_0x01d8
            java.lang.String r0 = "NO SERVER CERTIFICATE FOUND"
            r14.a(r6, r0)
            goto L_0x00d4
        L_0x019d:
            int r0 = r4.bsd
            int r1 = a.a.a.a.a.a.bc.bsr
            if (r0 == r1) goto L_0x01a9
            int r0 = r4.bsd
            int r1 = a.a.a.a.a.a.bc.bss
            if (r0 != r1) goto L_0x01ad
        L_0x01a9:
            java.lang.String r0 = "DSA"
            r1 = r0
            goto L_0x0175
        L_0x01ad:
            int r0 = r4.bsd
            int r1 = a.a.a.a.a.a.bc.bsv
            if (r0 != r1) goto L_0x01b7
            java.lang.String r0 = "DH_DSA"
            r1 = r0
            goto L_0x0175
        L_0x01b7:
            int r0 = r4.bsd
            int r1 = a.a.a.a.a.a.bc.bsw
            if (r0 != r1) goto L_0x0416
            java.lang.String r0 = "DH_RSA"
            r1 = r0
            goto L_0x0175
        L_0x01c1:
            a.a.a.a.a.a.bp r2 = r14.tp
            java.lang.String r1 = r0.chooseEngineServerAlias(r1, r10, r2)
            goto L_0x018b
        L_0x01c8:
            a.a.a.a.a.a.aw r2 = r14.tq
            java.lang.String r1 = r0.chooseServerAlias(r1, r10, r2)
            if (r1 == 0) goto L_0x040f
            java.security.cert.X509Certificate[] r0 = r0.getCertificateChain(r1)
            r13 = r1
            r1 = r0
            r0 = r13
            goto L_0x0194
        L_0x01d8:
            a.a.a.a.a.a.bk r2 = r14.sV
            r2.bYm = r1
            a.a.a.a.a.a.d r2 = new a.a.a.a.a.a.d
            r2.<init>(r1)
            r14.sY = r2
            a.a.a.a.a.a.bf r1 = r14.sS
            javax.net.ssl.X509KeyManager r1 = r1.GO()
            java.security.PrivateKey r0 = r1.getPrivateKey(r0)
            r14.bRA = r0
            a.a.a.a.a.a.d r0 = r14.sY
            r14.a(r0)
        L_0x01f4:
            int r0 = r4.bsd     // Catch:{ Exception -> 0x0346 }
            int r1 = a.a.a.a.a.a.bc.bsq     // Catch:{ Exception -> 0x0346 }
            if (r0 != r1) goto L_0x02f6
            a.a.a.a.a.a.d r0 = r14.sY     // Catch:{ Exception -> 0x0346 }
            java.security.cert.X509Certificate[] r0 = r0.dI     // Catch:{ Exception -> 0x0346 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ Exception -> 0x0346 }
            java.security.PublicKey r0 = r0.getPublicKey()     // Catch:{ Exception -> 0x0346 }
            int r0 = a(r0)     // Catch:{ Exception -> 0x0346 }
            r1 = 512(0x200, float:7.175E-43)
            if (r0 <= r1) goto L_0x040c
            java.lang.String r0 = "RSA"
            java.security.KeyPairGenerator r0 = java.security.KeyPairGenerator.getInstance(r0)     // Catch:{ Exception -> 0x0346 }
            r1 = 512(0x200, float:7.175E-43)
            r0.initialize(r1)     // Catch:{ Exception -> 0x03f1 }
        L_0x0218:
            r1 = r10
            r2 = r10
        L_0x021a:
            if (r0 == 0) goto L_0x02ba
            a.a.a.a.a.a.am r3 = new a.a.a.a.a.a.am
            int r5 = r4.bsd
            r3.<init>(r5)
            java.security.KeyPair r5 = r0.genKeyPair()     // Catch:{ Exception -> 0x03ed }
            int r0 = r4.bsd     // Catch:{ Exception -> 0x03ed }
            int r6 = a.a.a.a.a.a.bc.bsq     // Catch:{ Exception -> 0x03ed }
            if (r0 != r6) goto L_0x0356
            java.security.PublicKey r0 = r5.getPublic()     // Catch:{ Exception -> 0x03ed }
            java.security.interfaces.RSAPublicKey r0 = (java.security.interfaces.RSAPublicKey) r0     // Catch:{ Exception -> 0x03ed }
            r6 = r0
            r0 = r10
        L_0x0235:
            boolean r7 = r4.FC()     // Catch:{ Exception -> 0x03c5 }
            if (r7 != 0) goto L_0x03d4
            java.security.PrivateKey r7 = r14.bRA     // Catch:{ Exception -> 0x03c5 }
            r3.a(r7)     // Catch:{ Exception -> 0x03c5 }
            java.security.PrivateKey r5 = r5.getPrivate()     // Catch:{ Exception -> 0x03c5 }
            r14.bRA = r5     // Catch:{ Exception -> 0x03c5 }
            a.a.a.a.a.a.m r5 = r14.sW     // Catch:{ Exception -> 0x03c5 }
            byte[] r5 = r5.eG()     // Catch:{ Exception -> 0x03c5 }
            r3.update(r5)     // Catch:{ Exception -> 0x03c5 }
            a.a.a.a.a.a.x r5 = r14.sX     // Catch:{ Exception -> 0x03c5 }
            byte[] r5 = r5.eG()     // Catch:{ Exception -> 0x03c5 }
            r3.update(r5)     // Catch:{ Exception -> 0x03c5 }
            r5 = 2
            byte[] r5 = new byte[r5]     // Catch:{ Exception -> 0x03c5 }
            int r7 = r4.bsd     // Catch:{ Exception -> 0x03c5 }
            int r8 = a.a.a.a.a.a.bc.bsq     // Catch:{ Exception -> 0x03c5 }
            if (r7 != r8) goto L_0x0375
            java.math.BigInteger r7 = r6.getModulus()     // Catch:{ Exception -> 0x03c5 }
            byte[] r7 = r7.toByteArray()     // Catch:{ Exception -> 0x03c5 }
            r8 = 0
            int r9 = r7.length     // Catch:{ Exception -> 0x03c5 }
            r9 = r9 & r12
            int r9 = r9 >>> 8
            byte r9 = (byte) r9     // Catch:{ Exception -> 0x03c5 }
            r5[r8] = r9     // Catch:{ Exception -> 0x03c5 }
            r8 = 1
            int r9 = r7.length     // Catch:{ Exception -> 0x03c5 }
            r9 = r9 & 255(0xff, float:3.57E-43)
            byte r9 = (byte) r9     // Catch:{ Exception -> 0x03c5 }
            r5[r8] = r9     // Catch:{ Exception -> 0x03c5 }
            r3.update(r5)     // Catch:{ Exception -> 0x03c5 }
            r3.update(r7)     // Catch:{ Exception -> 0x03c5 }
            java.math.BigInteger r7 = r6.getPublicExponent()     // Catch:{ Exception -> 0x03c5 }
            byte[] r7 = r7.toByteArray()     // Catch:{ Exception -> 0x03c5 }
            r8 = 0
            int r9 = r7.length     // Catch:{ Exception -> 0x03c5 }
            r9 = r9 & r12
            int r9 = r9 >>> 8
            byte r9 = (byte) r9     // Catch:{ Exception -> 0x03c5 }
            r5[r8] = r9     // Catch:{ Exception -> 0x03c5 }
            r8 = 1
            int r9 = r7.length     // Catch:{ Exception -> 0x03c5 }
            r9 = r9 & 255(0xff, float:3.57E-43)
            byte r9 = (byte) r9     // Catch:{ Exception -> 0x03c5 }
            r5[r8] = r9     // Catch:{ Exception -> 0x03c5 }
            r3.update(r7)     // Catch:{ Exception -> 0x03c5 }
        L_0x0298:
            byte[] r3 = r3.sign()     // Catch:{ Exception -> 0x03c5 }
        L_0x029c:
            r5 = r6
            r13 = r0
            r0 = r3
            r3 = r13
        L_0x02a0:
            int r4 = r4.bsd
            int r6 = a.a.a.a.a.a.bc.bsq
            if (r4 != r6) goto L_0x03dd
            a.a.a.a.a.a.aa r1 = new a.a.a.a.a.a.aa
            java.math.BigInteger r2 = r5.getModulus()
            java.math.BigInteger r3 = r5.getPublicExponent()
            r1.<init>(r2, r3, r10, r0)
            r14.sZ = r1
        L_0x02b5:
            a.a.a.a.a.a.aa r0 = r14.sZ
            r14.a(r0)
        L_0x02ba:
            a.a.a.a.a.a.bf r0 = r14.sS
            boolean r0 = r0.getWantClientAuth()
            if (r0 != 0) goto L_0x02ca
            a.a.a.a.a.a.bf r0 = r14.sS
            boolean r0 = r0.getNeedClientAuth()
            if (r0 == 0) goto L_0x02e6
        L_0x02ca:
            a.a.a.a.a.a.bf r0 = r14.sS     // Catch:{ ClassCastException -> 0x03ea }
            javax.net.ssl.X509TrustManager r0 = r0.GP()     // Catch:{ ClassCastException -> 0x03ea }
            java.security.cert.X509Certificate[] r0 = r0.getAcceptedIssuers()     // Catch:{ ClassCastException -> 0x03ea }
            r1 = 2
            byte[] r1 = new byte[r1]
            r1 = {1, 2} // fill-array
            a.a.a.a.a.a.ar r2 = new a.a.a.a.a.a.ar
            r2.<init>(r1, r0)
            r14.ta = r2
            a.a.a.a.a.a.ar r0 = r14.ta
            r14.a(r0)
        L_0x02e6:
            a.a.a.a.a.a.ag r0 = new a.a.a.a.a.a.ag
            r0.<init>()
            r14.tb = r0
            a.a.a.a.a.a.ag r0 = r14.tb
            r14.a(r0)
            r14.status = r11
            goto L_0x00d4
        L_0x02f6:
            int r0 = r4.bsd     // Catch:{ Exception -> 0x0346 }
            int r1 = a.a.a.a.a.a.bc.bsr     // Catch:{ Exception -> 0x0346 }
            if (r0 == r1) goto L_0x031a
            int r0 = r4.bsd     // Catch:{ Exception -> 0x0346 }
            int r1 = a.a.a.a.a.a.bc.bss     // Catch:{ Exception -> 0x0346 }
            if (r0 == r1) goto L_0x031a
            int r0 = r4.bsd     // Catch:{ Exception -> 0x0346 }
            int r1 = a.a.a.a.a.a.bc.bst     // Catch:{ Exception -> 0x0346 }
            if (r0 == r1) goto L_0x031a
            int r0 = r4.bsd     // Catch:{ Exception -> 0x0346 }
            int r1 = a.a.a.a.a.a.bc.bsu     // Catch:{ Exception -> 0x0346 }
            if (r0 == r1) goto L_0x031a
            int r0 = r4.bsd     // Catch:{ Exception -> 0x0346 }
            int r1 = a.a.a.a.a.a.bc.bsx     // Catch:{ Exception -> 0x0346 }
            if (r0 == r1) goto L_0x031a
            int r0 = r4.bsd     // Catch:{ Exception -> 0x0346 }
            int r1 = a.a.a.a.a.a.bc.bsy     // Catch:{ Exception -> 0x0346 }
            if (r0 != r1) goto L_0x0407
        L_0x031a:
            java.lang.String r0 = "DH"
            java.security.KeyPairGenerator r0 = java.security.KeyPairGenerator.getInstance(r0)     // Catch:{ NoSuchAlgorithmException -> 0x033e }
        L_0x0320:
            java.math.BigInteger r1 = new java.math.BigInteger     // Catch:{ Exception -> 0x03f1 }
            r2 = 1
            byte[] r3 = a.a.a.a.a.a.bd.FL()     // Catch:{ Exception -> 0x03f1 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x03f1 }
            java.math.BigInteger r2 = new java.math.BigInteger     // Catch:{ Exception -> 0x03f9 }
            java.lang.String r3 = "2"
            r2.<init>(r3)     // Catch:{ Exception -> 0x03f9 }
            javax.crypto.spec.DHParameterSpec r3 = new javax.crypto.spec.DHParameterSpec     // Catch:{ Exception -> 0x0400 }
            r3.<init>(r1, r2)     // Catch:{ Exception -> 0x0400 }
            r0.initialize(r3)     // Catch:{ Exception -> 0x0400 }
            r13 = r2
            r2 = r1
            r1 = r13
            goto L_0x021a
        L_0x033e:
            r0 = move-exception
            java.lang.String r0 = "DiffieHellman"
            java.security.KeyPairGenerator r0 = java.security.KeyPairGenerator.getInstance(r0)     // Catch:{ Exception -> 0x0346 }
            goto L_0x0320
        L_0x0346:
            r0 = move-exception
            r1 = r10
            r2 = r10
            r3 = r10
        L_0x034a:
            r5 = 80
            java.lang.String r6 = "INTERNAL ERROR"
            r14.a(r5, r6, r0)
            r0 = r1
            r1 = r2
            r2 = r3
            goto L_0x021a
        L_0x0356:
            java.security.PublicKey r0 = r5.getPublic()     // Catch:{ Exception -> 0x03ed }
            javax.crypto.interfaces.DHPublicKey r0 = (javax.crypto.interfaces.DHPublicKey) r0     // Catch:{ Exception -> 0x03ed }
            java.lang.String r6 = "DH"
            java.security.KeyFactory r6 = java.security.KeyFactory.getInstance(r6)     // Catch:{ NoSuchAlgorithmException -> 0x036d }
        L_0x0362:
            java.lang.Class<javax.crypto.spec.DHPublicKeySpec> r7 = javax.crypto.spec.DHPublicKeySpec.class
            java.security.spec.KeySpec r0 = r6.getKeySpec(r0, r7)     // Catch:{ Exception -> 0x03ed }
            javax.crypto.spec.DHPublicKeySpec r0 = (javax.crypto.spec.DHPublicKeySpec) r0     // Catch:{ Exception -> 0x03ed }
            r6 = r10
            goto L_0x0235
        L_0x036d:
            r6 = move-exception
            java.lang.String r6 = "DiffieHellman"
            java.security.KeyFactory r6 = java.security.KeyFactory.getInstance(r6)     // Catch:{ Exception -> 0x03ed }
            goto L_0x0362
        L_0x0375:
            java.math.BigInteger r7 = r0.getP()     // Catch:{ Exception -> 0x03c5 }
            byte[] r7 = r7.toByteArray()     // Catch:{ Exception -> 0x03c5 }
            r8 = 0
            int r9 = r7.length     // Catch:{ Exception -> 0x03c5 }
            r9 = r9 & r12
            int r9 = r9 >>> 8
            byte r9 = (byte) r9     // Catch:{ Exception -> 0x03c5 }
            r5[r8] = r9     // Catch:{ Exception -> 0x03c5 }
            r8 = 1
            int r9 = r7.length     // Catch:{ Exception -> 0x03c5 }
            r9 = r9 & 255(0xff, float:3.57E-43)
            byte r9 = (byte) r9     // Catch:{ Exception -> 0x03c5 }
            r5[r8] = r9     // Catch:{ Exception -> 0x03c5 }
            r3.update(r7)     // Catch:{ Exception -> 0x03c5 }
            java.math.BigInteger r7 = r0.getG()     // Catch:{ Exception -> 0x03c5 }
            byte[] r7 = r7.toByteArray()     // Catch:{ Exception -> 0x03c5 }
            r8 = 0
            int r9 = r7.length     // Catch:{ Exception -> 0x03c5 }
            r9 = r9 & r12
            int r9 = r9 >>> 8
            byte r9 = (byte) r9     // Catch:{ Exception -> 0x03c5 }
            r5[r8] = r9     // Catch:{ Exception -> 0x03c5 }
            r8 = 1
            int r9 = r7.length     // Catch:{ Exception -> 0x03c5 }
            r9 = r9 & 255(0xff, float:3.57E-43)
            byte r9 = (byte) r9     // Catch:{ Exception -> 0x03c5 }
            r5[r8] = r9     // Catch:{ Exception -> 0x03c5 }
            r3.update(r7)     // Catch:{ Exception -> 0x03c5 }
            java.math.BigInteger r7 = r0.getY()     // Catch:{ Exception -> 0x03c5 }
            byte[] r7 = r7.toByteArray()     // Catch:{ Exception -> 0x03c5 }
            r8 = 0
            int r9 = r7.length     // Catch:{ Exception -> 0x03c5 }
            r9 = r9 & r12
            int r9 = r9 >>> 8
            byte r9 = (byte) r9     // Catch:{ Exception -> 0x03c5 }
            r5[r8] = r9     // Catch:{ Exception -> 0x03c5 }
            r8 = 1
            int r9 = r7.length     // Catch:{ Exception -> 0x03c5 }
            r9 = r9 & 255(0xff, float:3.57E-43)
            byte r9 = (byte) r9     // Catch:{ Exception -> 0x03c5 }
            r5[r8] = r9     // Catch:{ Exception -> 0x03c5 }
            r3.update(r7)     // Catch:{ Exception -> 0x03c5 }
            goto L_0x0298
        L_0x03c5:
            r3 = move-exception
            r5 = r6
            r13 = r0
            r0 = r3
            r3 = r13
        L_0x03ca:
            r6 = 80
            java.lang.String r7 = "INTERNAL ERROR"
            r14.a(r6, r7, r0)
            r0 = r10
            goto L_0x02a0
        L_0x03d4:
            java.security.PrivateKey r3 = r5.getPrivate()     // Catch:{ Exception -> 0x03c5 }
            r14.bRA = r3     // Catch:{ Exception -> 0x03c5 }
            r3 = r10
            goto L_0x029c
        L_0x03dd:
            a.a.a.a.a.a.aa r4 = new a.a.a.a.a.a.aa
            java.math.BigInteger r3 = r3.getY()
            r4.<init>(r2, r1, r3, r0)
            r14.sZ = r4
            goto L_0x02b5
        L_0x03ea:
            r0 = move-exception
            goto L_0x02e6
        L_0x03ed:
            r0 = move-exception
            r3 = r10
            r5 = r10
            goto L_0x03ca
        L_0x03f1:
            r1 = move-exception
            r2 = r10
            r3 = r10
            r13 = r1
            r1 = r0
            r0 = r13
            goto L_0x034a
        L_0x03f9:
            r2 = move-exception
            r3 = r1
            r1 = r0
            r0 = r2
            r2 = r10
            goto L_0x034a
        L_0x0400:
            r3 = move-exception
            r13 = r3
            r3 = r1
            r1 = r0
            r0 = r13
            goto L_0x034a
        L_0x0407:
            r0 = r10
            r1 = r10
            r2 = r10
            goto L_0x021a
        L_0x040c:
            r0 = r10
            goto L_0x0218
        L_0x040f:
            r0 = r1
            r1 = r10
            goto L_0x0194
        L_0x0413:
            r0 = r10
            goto L_0x0191
        L_0x0416:
            r1 = r10
            goto L_0x0175
        L_0x0419:
            r0 = r5
            goto L_0x00e1
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.a.a.bi.MW():void");
    }

    public void fa() {
        if (!this.ti) {
            if ((!this.sS.getNeedClientAuth() || this.tc != null) && this.td != null && (this.tc == null || this.td.isEmpty() || this.te != null)) {
                this.th = true;
            } else {
                fd();
            }
            if (this.sX.UZ[1] == 1) {
                J("client finished");
            } else {
                E(bq.chB);
            }
        } else if (this.tg == null) {
            fd();
        } else {
            this.th = true;
        }
    }

    /* access modifiers changed from: protected */
    public void fb() {
        byte[] bArr;
        boolean z = this.sX.UZ[1] == 1;
        if (z) {
            bArr = new byte[12];
            a("server finished", bArr);
        } else {
            bArr = new byte[36];
            e(bq.chC, bArr);
        }
        this.tg = new az(bArr);
        a(this.tg);
        if (this.ti) {
            if (z) {
                J("client finished");
            } else {
                E(bq.chB);
            }
            this.status = 1;
            return;
        }
        this.sV.bYi = System.currentTimeMillis();
        this.status = 3;
    }

    public void start() {
        if (this.sV == null) {
            this.status = 1;
        } else if (this.sW == null || this.status == 3) {
            eZ();
            this.status = 1;
        }
    }
}
