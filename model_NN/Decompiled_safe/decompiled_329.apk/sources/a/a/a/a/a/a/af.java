package a.a.a.a.a.a;

import java.security.AccessControlContext;
import java.security.AccessController;

final class af {
    final AccessControlContext auL = AccessController.getContext();
    final String name;

    af(String str) {
        this.name = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof af)) {
            return false;
        }
        af afVar = (af) obj;
        if (this.auL == null) {
            if (afVar.auL != null) {
                return false;
            }
        } else if (!this.auL.equals(afVar.auL)) {
            return false;
        }
        if (this.name == null) {
            if (afVar.name != null) {
                return false;
            }
        } else if (!this.name.equals(afVar.name)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = 1 * 31;
        return (((this.auL == null ? 0 : this.auL.hashCode()) + 31) * 31) + (this.name == null ? 0 : this.name.hashCode());
    }
}
