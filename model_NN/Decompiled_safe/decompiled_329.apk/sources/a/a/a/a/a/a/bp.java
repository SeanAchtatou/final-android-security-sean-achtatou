package a.a.a.a.a.a;

import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ReadOnlyBufferException;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLEngineResult;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSession;

public class bp extends SSLEngine {
    private n aYU;
    private bo aYV;
    private boolean blF = false;
    private boolean ceI = false;
    private boolean ceJ = false;
    private boolean ceK = false;
    private boolean ceL = false;
    private boolean ceM = false;
    private boolean ceN = false;
    private boolean ceO = false;
    private aq ceP;
    private be ceQ;
    private j ceR = new j();
    private byte[] ceS = null;
    private byte[] ceT = null;
    protected bf lr;
    protected an sR;
    private bk sV;

    protected bp(bf bfVar) {
        this.lr = bfVar;
    }

    protected bp(String str, int i, bf bfVar) {
        super(str, i);
        this.lr = bfVar;
    }

    private SSLEngineResult.Status Qj() {
        return this.ceN ? SSLEngineResult.Status.CLOSED : SSLEngineResult.Status.OK;
    }

    private void shutdown() {
        this.ceN = true;
        this.ceO = true;
        this.ceK = true;
        this.ceJ = true;
        if (this.blF) {
            this.aYV.shutdown();
            this.aYV = null;
            this.aYU.shutdown();
            this.aYU = null;
            this.sR.shutdown();
            this.sR = null;
        }
    }

    public void beginHandshake() {
        if (this.ceN) {
            throw new SSLException("Engine has already been closed.");
        } else if (!this.ceI) {
            throw new IllegalStateException("Client/Server mode was not set");
        } else {
            if (!this.blF) {
                this.blF = true;
                if (getUseClientMode()) {
                    this.aYU = new bg(this);
                } else {
                    this.aYU = new bi(this);
                }
                this.ceQ = new be();
                this.aYV = new bo();
                this.ceP = new aq();
                this.sR = new an(this.aYU, this.aYV, this.ceP, this.ceQ);
            }
            this.aYU.start();
        }
    }

    public void closeInbound() {
        if (!this.ceJ) {
            this.ceJ = true;
            this.ceN = true;
            if (!this.blF) {
                shutdown();
            } else if (!this.ceM) {
                if (this.sV != null) {
                    this.sV.invalidate();
                }
                this.aYV.a((byte) 2, (byte) 80);
                throw new SSLException("Inbound is closed before close_notify alert has been received.");
            }
        }
    }

    public void closeOutbound() {
        if (!this.ceK) {
            this.ceK = true;
            if (this.blF) {
                this.aYV.a((byte) 1, (byte) 0);
                this.ceL = true;
            } else {
                shutdown();
            }
            this.ceN = true;
        }
    }

    public Runnable getDelegatedTask() {
        return this.aYU.ff();
    }

    public boolean getEnableSessionCreation() {
        return this.lr.getEnableSessionCreation();
    }

    public String[] getEnabledCipherSuites() {
        return this.lr.getEnabledCipherSuites();
    }

    public String[] getEnabledProtocols() {
        return this.lr.getEnabledProtocols();
    }

    public SSLEngineResult.HandshakeStatus getHandshakeStatus() {
        return (!this.blF || this.ceO) ? SSLEngineResult.HandshakeStatus.NOT_HANDSHAKING : this.aYV.Pz() ? SSLEngineResult.HandshakeStatus.NEED_WRAP : (!this.ceL || this.ceM) ? this.aYU.eW() : SSLEngineResult.HandshakeStatus.NEED_UNWRAP;
    }

    public boolean getNeedClientAuth() {
        return this.lr.getNeedClientAuth();
    }

    public SSLSession getSession() {
        return this.sV != null ? this.sV : bk.bYe;
    }

    public String[] getSupportedCipherSuites() {
        return bc.FE();
    }

    public String[] getSupportedProtocols() {
        return (String[]) o.ug.clone();
    }

    public boolean getUseClientMode() {
        return this.lr.getUseClientMode();
    }

    public boolean getWantClientAuth() {
        return this.lr.getWantClientAuth();
    }

    public boolean isInboundDone() {
        return this.ceJ || this.ceN;
    }

    public boolean isOutboundDone() {
        return this.ceK;
    }

    public void setEnableSessionCreation(boolean z) {
        this.lr.setEnableSessionCreation(z);
    }

    public void setEnabledCipherSuites(String[] strArr) {
        this.lr.setEnabledCipherSuites(strArr);
    }

    public void setEnabledProtocols(String[] strArr) {
        this.lr.setEnabledProtocols(strArr);
    }

    public void setNeedClientAuth(boolean z) {
        this.lr.setNeedClientAuth(z);
    }

    public void setUseClientMode(boolean z) {
        if (this.blF) {
            throw new IllegalArgumentException("Could not change the mode after the initial handshake has begun.");
        }
        this.lr.setUseClientMode(z);
        this.ceI = true;
    }

    public void setWantClientAuth(boolean z) {
        this.lr.setWantClientAuth(z);
    }

    public SSLEngineResult unwrap(ByteBuffer byteBuffer, ByteBuffer[] byteBufferArr, int i, int i2) {
        if (this.ceO) {
            return new SSLEngineResult(SSLEngineResult.Status.CLOSED, SSLEngineResult.HandshakeStatus.NOT_HANDSHAKING, 0, 0);
        }
        if (byteBuffer == null || byteBufferArr == null) {
            throw new IllegalStateException("Some of the input parameters are null");
        }
        if (!this.blF) {
            beginHandshake();
        }
        SSLEngineResult.HandshakeStatus handshakeStatus = getHandshakeStatus();
        if ((this.sV == null || this.ceN) && (handshakeStatus.equals(SSLEngineResult.HandshakeStatus.NEED_WRAP) || handshakeStatus.equals(SSLEngineResult.HandshakeStatus.NEED_TASK))) {
            return new SSLEngineResult(Qj(), handshakeStatus, 0, 0);
        }
        if (byteBuffer.remaining() < this.sR.AL()) {
            return new SSLEngineResult(SSLEngineResult.Status.BUFFER_UNDERFLOW, getHandshakeStatus(), 0, 0);
        }
        try {
            byteBuffer.mark();
            int i3 = i;
            int i4 = 0;
            while (i3 < i + i2) {
                if (byteBufferArr[i3] == null) {
                    throw new IllegalStateException("Some of the input parameters are null");
                } else if (byteBufferArr[i3].isReadOnly()) {
                    throw new ReadOnlyBufferException();
                } else {
                    i4 += byteBufferArr[i3].remaining();
                    i3++;
                }
            }
            if (i4 < this.sR.fM(byteBuffer.remaining())) {
                return new SSLEngineResult(SSLEngineResult.Status.BUFFER_OVERFLOW, getHandshakeStatus(), 0, 0);
            }
            this.ceP.a(byteBuffer);
            switch (this.sR.AM()) {
                case 20:
                case 22:
                    if (this.aYU.eW().equals(SSLEngineResult.HandshakeStatus.FINISHED)) {
                        this.sV = this.sR.eX();
                        break;
                    }
                    break;
                case 21:
                    if (!this.aYV.PA()) {
                        switch (this.aYV.Fh()) {
                            case 0:
                                this.aYV.Py();
                                this.ceM = true;
                                if (this.ceL) {
                                    closeInbound();
                                    shutdown();
                                    break;
                                } else {
                                    closeOutbound();
                                    closeInbound();
                                    break;
                                }
                            case 100:
                                this.aYV.Py();
                                if (this.sV != null) {
                                    this.aYU.stop();
                                    break;
                                } else {
                                    throw new ba((byte) 40, new SSLHandshakeException("Received no_renegotiation during the initial handshake"));
                                }
                            default:
                                this.aYV.Py();
                                break;
                        }
                    } else {
                        this.aYV.Py();
                        if (this.sV != null) {
                            this.sV.invalidate();
                        }
                        String str = "Fatal alert received " + this.aYV.PB();
                        shutdown();
                        throw new SSLException(str);
                    }
            }
            return new SSLEngineResult(Qj(), getHandshakeStatus(), this.ceP.ee(), this.ceQ.b(byteBufferArr, i, i2));
        } catch (BufferUnderflowException e) {
            byteBuffer.reset();
            return new SSLEngineResult(SSLEngineResult.Status.BUFFER_UNDERFLOW, getHandshakeStatus(), 0, 0);
        } catch (ba e2) {
            this.aYV.a((byte) 2, e2.Fh());
            this.ceN = true;
            byteBuffer.reset();
            if (this.sV != null) {
                this.sV.invalidate();
            }
            throw e2.Fg();
        } catch (SSLException e3) {
            throw e3;
        } catch (IOException e4) {
            this.aYV.a((byte) 2, (byte) 80);
            this.ceN = true;
            throw new SSLException(e4.getMessage());
        }
    }

    public SSLEngineResult wrap(ByteBuffer[] byteBufferArr, int i, int i2, ByteBuffer byteBuffer) {
        if (this.ceO) {
            return new SSLEngineResult(SSLEngineResult.Status.CLOSED, SSLEngineResult.HandshakeStatus.NOT_HANDSHAKING, 0, 0);
        }
        if (byteBufferArr == null || byteBuffer == null) {
            throw new IllegalStateException("Some of the input parameters are null");
        } else if (byteBuffer.isReadOnly()) {
            throw new ReadOnlyBufferException();
        } else {
            if (!this.blF) {
                beginHandshake();
            }
            SSLEngineResult.HandshakeStatus handshakeStatus = getHandshakeStatus();
            if ((this.sV == null || this.ceN) && (handshakeStatus.equals(SSLEngineResult.HandshakeStatus.NEED_UNWRAP) || handshakeStatus.equals(SSLEngineResult.HandshakeStatus.NEED_TASK))) {
                return new SSLEngineResult(Qj(), handshakeStatus, 0, 0);
            }
            int remaining = byteBuffer.remaining();
            if (this.aYV.Pz()) {
                if (remaining < this.sR.fH(2)) {
                    return new SSLEngineResult(SSLEngineResult.Status.BUFFER_OVERFLOW, handshakeStatus, 0, 0);
                }
                byte[] fc = this.aYV.fc();
                byteBuffer.put(fc);
                if (this.aYV.PA()) {
                    this.aYV.Py();
                    if (this.sV != null) {
                        this.sV.invalidate();
                    }
                    shutdown();
                    return new SSLEngineResult(SSLEngineResult.Status.CLOSED, SSLEngineResult.HandshakeStatus.NOT_HANDSHAKING, 0, fc.length);
                }
                this.aYV.Py();
                if (!this.ceL || !this.ceM) {
                    return new SSLEngineResult(Qj(), getHandshakeStatus(), 0, fc.length);
                }
                shutdown();
                return new SSLEngineResult(SSLEngineResult.Status.CLOSED, SSLEngineResult.HandshakeStatus.NOT_HANDSHAKING, 0, fc.length);
            } else if (remaining < this.sR.AL()) {
                return new SSLEngineResult(SSLEngineResult.Status.BUFFER_OVERFLOW, handshakeStatus, 0, 0);
            } else {
                try {
                    if (!handshakeStatus.equals(SSLEngineResult.HandshakeStatus.NEED_WRAP)) {
                        this.ceR.a(byteBufferArr, i, i2);
                        if (remaining < an.aYS && remaining < this.sR.fH(this.ceR.available())) {
                            return new SSLEngineResult(SSLEngineResult.Status.BUFFER_OVERFLOW, handshakeStatus, 0, 0);
                        }
                        if (this.ceS == null) {
                            this.ceS = this.sR.a((byte) 23, this.ceR);
                        }
                        if (remaining < this.ceS.length) {
                            return new SSLEngineResult(SSLEngineResult.Status.BUFFER_OVERFLOW, handshakeStatus, this.ceR.ee(), 0);
                        }
                        byteBuffer.put(this.ceS);
                        int length = this.ceS.length;
                        this.ceS = null;
                        return new SSLEngineResult(Qj(), handshakeStatus, this.ceR.ee(), length);
                    }
                    if (this.ceT == null) {
                        this.ceT = this.aYU.fc();
                    }
                    if (remaining < this.ceT.length) {
                        return new SSLEngineResult(SSLEngineResult.Status.BUFFER_OVERFLOW, handshakeStatus, 0, 0);
                    }
                    byteBuffer.put(this.ceT);
                    int length2 = this.ceT.length;
                    this.ceT = null;
                    if (this.aYU.eW().equals(SSLEngineResult.HandshakeStatus.FINISHED)) {
                        this.sV = this.sR.eX();
                    }
                    return new SSLEngineResult(Qj(), getHandshakeStatus(), 0, length2);
                } catch (ba e) {
                    this.aYV.a((byte) 2, e.Fh());
                    this.ceN = true;
                    if (this.sV != null) {
                        this.sV.invalidate();
                    }
                    throw e.Fg();
                }
            }
        }
    }
}
