package a.a.a.a.a.a;

import java.util.Arrays;

class t {
    /* access modifiers changed from: private */
    public byte[] FL;
    final /* synthetic */ y FM;

    private t(y yVar, byte[] bArr) {
        this.FM = yVar;
        this.FL = bArr;
    }

    /* synthetic */ t(y yVar, byte[] bArr, au auVar) {
        this(yVar, bArr);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof t)) {
            return false;
        }
        return Arrays.equals(this.FL, ((t) obj).FL);
    }

    public int hashCode() {
        return Arrays.hashCode(this.FL);
    }
}
