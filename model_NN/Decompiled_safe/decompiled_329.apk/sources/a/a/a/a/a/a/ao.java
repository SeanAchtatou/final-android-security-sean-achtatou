package a.a.a.a.a.a;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.security.KeyManagementException;
import javax.net.ssl.SSLServerSocketFactory;

public class ao extends SSLServerSocketFactory {
    private bf lr;
    private IOException mr;

    public ao() {
        try {
            this.lr = bf.GL();
            this.lr.setUseClientMode(false);
        } catch (KeyManagementException e) {
            this.mr = new IOException("Delayed instantiation exception:");
            this.mr.initCause(e);
        }
    }

    protected ao(bf bfVar) {
        this.lr = (bf) bfVar.clone();
        this.lr.setUseClientMode(false);
    }

    public ServerSocket createServerSocket() {
        if (this.mr == null) {
            return new g((bf) this.lr.clone());
        }
        throw this.mr;
    }

    public ServerSocket createServerSocket(int i) {
        if (this.mr == null) {
            return new g(i, (bf) this.lr.clone());
        }
        throw this.mr;
    }

    public ServerSocket createServerSocket(int i, int i2) {
        if (this.mr == null) {
            return new g(i, i2, (bf) this.lr.clone());
        }
        throw this.mr;
    }

    public ServerSocket createServerSocket(int i, int i2, InetAddress inetAddress) {
        if (this.mr == null) {
            return new g(i, i2, inetAddress, (bf) this.lr.clone());
        }
        throw this.mr;
    }

    public String[] getDefaultCipherSuites() {
        return this.mr != null ? new String[0] : this.lr.getEnabledCipherSuites();
    }

    public String[] getSupportedCipherSuites() {
        return this.mr != null ? new String[0] : bc.FE();
    }
}
