package a.a.a.a.a.a;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.ArrayList;
import javax.net.ssl.HandshakeCompletedEvent;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLEngineResult;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

public class aw extends SSLSocket {
    private n aYU;
    private bo aYV;
    private boolean blF = false;
    private w blG;
    private at blH;
    private boolean blI = false;
    protected InputStream blJ;
    protected OutputStream blK;
    private ArrayList blL;
    protected bf lr;
    protected an sR;
    private bk sV;

    protected aw(bf bfVar) {
        this.lr = bfVar;
    }

    protected aw(String str, int i, bf bfVar) {
        super(str, i);
        this.lr = bfVar;
        a();
    }

    protected aw(String str, int i, InetAddress inetAddress, int i2, bf bfVar) {
        super(str, i, inetAddress, i2);
        this.lr = bfVar;
        a();
    }

    protected aw(InetAddress inetAddress, int i, bf bfVar) {
        super(inetAddress, i);
        this.lr = bfVar;
        a();
    }

    protected aw(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2, bf bfVar) {
        super(inetAddress, i, inetAddress2, i2);
        this.lr = bfVar;
        a();
    }

    private void DK() {
        while (true) {
            try {
                SSLEngineResult.HandshakeStatus eW = this.aYU.eW();
                if (eW.equals(SSLEngineResult.HandshakeStatus.FINISHED)) {
                    break;
                }
                if (eW.equals(SSLEngineResult.HandshakeStatus.NEED_WRAP)) {
                    this.blK.write(this.aYU.fc());
                } else if (eW.equals(SSLEngineResult.HandshakeStatus.NEED_UNWRAP)) {
                    int AM = this.sR.AM();
                    switch (AM) {
                        case 20:
                        case 22:
                        case 23:
                            break;
                        case 21:
                            DL();
                            if (this.blI) {
                                return;
                            }
                            break;
                        default:
                            b((byte) 10, new SSLException("Unexpected message of type " + AM + " has been got"));
                            break;
                    }
                } else {
                    b((byte) 80, new SSLException("Handshake passed unexpected status: " + eW));
                }
                if (this.aYV.Pz()) {
                    this.blK.write(this.aYV.fc());
                    this.aYV.Py();
                }
            } catch (ap e) {
                this.blG.kY();
                throw new IOException("Connection was closed");
            } catch (ba e2) {
                b(e2.Fh(), e2.Fg());
            }
        }
        this.sV = this.sR.eX();
        if (this.blL != null) {
            HandshakeCompletedEvent handshakeCompletedEvent = new HandshakeCompletedEvent(this, this.sV);
            int size = this.blL.size();
            for (int i = 0; i < size; i++) {
                ((HandshakeCompletedListener) this.blL.get(i)).handshakeCompleted(handshakeCompletedEvent);
            }
        }
    }

    private void DL() {
        if (this.aYV.Pz()) {
            if (this.aYV.PA()) {
                this.aYV.Py();
                String str = "Fatal alert received " + this.aYV.PB();
                shutdown();
                throw new SSLException(str);
            }
            switch (this.aYV.Fh()) {
                case 0:
                    this.aYV.Py();
                    this.blG.kY();
                    close();
                    return;
                default:
                    this.aYV.Py();
                    return;
            }
        }
    }

    private void b(byte b2, SSLException sSLException) {
        this.aYV.a((byte) 2, b2);
        try {
            this.blK.write(this.aYV.fc());
        } catch (IOException e) {
        }
        this.aYV.Py();
        shutdown();
        throw sSLException;
    }

    private void shutdown() {
        if (this.blF) {
            this.aYV.shutdown();
            this.aYV = null;
            this.aYU.shutdown();
            this.aYU = null;
            this.sR.shutdown();
            this.sR = null;
        }
        this.blI = true;
    }

    /* access modifiers changed from: protected */
    public void Cf() {
        this.blJ = super.getInputStream();
        this.blK = super.getOutputStream();
    }

    /* access modifiers changed from: protected */
    public void Cg() {
        super.close();
        if (this.blJ != null) {
            this.blJ.close();
            this.blK.close();
        }
    }

    /* access modifiers changed from: protected */
    public void DJ() {
        if (!this.blF) {
            startHandshake();
        }
        do {
            try {
                if (this.blG.available() == 0) {
                    int AM = this.sR.AM();
                    switch (AM) {
                        case 21:
                            DL();
                            if (this.blI) {
                                return;
                            }
                            break;
                        case 22:
                            if (!this.aYU.eW().equals(SSLEngineResult.HandshakeStatus.NOT_HANDSHAKING)) {
                                DK();
                                break;
                            }
                            break;
                        case 23:
                            break;
                        default:
                            b((byte) 10, new SSLException("Unexpected message of type " + AM + " has been got"));
                            break;
                    }
                    if (this.aYV.Pz()) {
                        this.blK.write(this.aYV.fc());
                        this.aYV.Py();
                    }
                } else {
                    return;
                }
            } catch (ba e) {
                b(e.Fh(), e.Fg());
                return;
            } catch (ap e2) {
                this.blG.kY();
                return;
            }
        } while (!this.blI);
        this.blG.kY();
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.blG == null) {
            Cf();
            this.blG = new w(this);
            this.blH = new at(this);
        }
    }

    public void addHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
        if (handshakeCompletedListener == null) {
            throw new IllegalArgumentException("Provided listener is null");
        }
        if (this.blL == null) {
            this.blL = new ArrayList();
        }
        this.blL.add(handshakeCompletedListener);
    }

    public void close() {
        if (!this.blI) {
            if (this.blF) {
                this.aYV.a((byte) 1, (byte) 0);
                try {
                    this.blK.write(this.aYV.fc());
                } catch (IOException e) {
                }
                this.aYV.Py();
            }
            shutdown();
            Cg();
            this.blI = true;
        }
    }

    public void connect(SocketAddress socketAddress) {
        super.connect(socketAddress);
        a();
    }

    public void connect(SocketAddress socketAddress, int i) {
        super.connect(socketAddress, i);
        a();
    }

    public boolean getEnableSessionCreation() {
        return this.lr.getEnableSessionCreation();
    }

    public String[] getEnabledCipherSuites() {
        return this.lr.getEnabledCipherSuites();
    }

    public String[] getEnabledProtocols() {
        return this.lr.getEnabledProtocols();
    }

    public InputStream getInputStream() {
        if (!this.blI) {
            return this.blG;
        }
        throw new IOException("Socket has already been closed.");
    }

    public boolean getNeedClientAuth() {
        return this.lr.getNeedClientAuth();
    }

    public OutputStream getOutputStream() {
        if (!this.blI) {
            return this.blH;
        }
        throw new IOException("Socket has already been closed.");
    }

    public SSLSession getSession() {
        if (!this.blF) {
            try {
                startHandshake();
            } catch (IOException e) {
                return bk.bYe;
            }
        }
        return this.sV;
    }

    public String[] getSupportedCipherSuites() {
        return bc.FE();
    }

    public String[] getSupportedProtocols() {
        return (String[]) o.ug.clone();
    }

    public boolean getUseClientMode() {
        return this.lr.getUseClientMode();
    }

    public boolean getWantClientAuth() {
        return this.lr.getWantClientAuth();
    }

    /* access modifiers changed from: protected */
    public void n(byte[] bArr, int i, int i2) {
        if (!this.blF) {
            startHandshake();
        }
        try {
            if (i2 < an.aYP) {
                this.blK.write(this.sR.c((byte) 23, bArr, i, i2));
                return;
            }
            int i3 = i2;
            int i4 = i;
            while (i3 >= an.aYP) {
                this.blK.write(this.sR.c((byte) 23, bArr, i4, an.aYP));
                i4 += an.aYP;
                i3 -= an.aYP;
            }
            if (i3 > 0) {
                this.blK.write(this.sR.c((byte) 23, bArr, i4, i3));
            }
        } catch (ba e) {
            b(e.Fh(), e.Fg());
        }
    }

    public void removeHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
        if (handshakeCompletedListener == null) {
            throw new IllegalArgumentException("Provided listener is null");
        } else if (this.blL == null) {
            throw new IllegalArgumentException("Provided listener is not registered");
        } else if (!this.blL.remove(handshakeCompletedListener)) {
            throw new IllegalArgumentException("Provided listener is not registered");
        }
    }

    public void sendUrgentData(int i) {
        throw new SocketException("Method sendUrgentData() is not supported.");
    }

    public void setEnableSessionCreation(boolean z) {
        this.lr.setEnableSessionCreation(z);
    }

    public void setEnabledCipherSuites(String[] strArr) {
        this.lr.setEnabledCipherSuites(strArr);
    }

    public void setEnabledProtocols(String[] strArr) {
        this.lr.setEnabledProtocols(strArr);
    }

    public void setNeedClientAuth(boolean z) {
        this.lr.setNeedClientAuth(z);
    }

    public void setOOBInline(boolean z) {
        throw new SocketException("Methods sendUrgentData, setOOBInline are not supported.");
    }

    public void setUseClientMode(boolean z) {
        if (this.blF) {
            throw new IllegalArgumentException("Could not change the mode after the initial handshake has begun.");
        }
        this.lr.setUseClientMode(z);
    }

    public void setWantClientAuth(boolean z) {
        this.lr.setWantClientAuth(z);
    }

    public void shutdownInput() {
        throw new UnsupportedOperationException("Method shutdownInput() is not supported.");
    }

    public void shutdownOutput() {
        throw new UnsupportedOperationException("Method shutdownOutput() is not supported.");
    }

    public void startHandshake() {
        if (this.blG == null) {
            throw new IOException("Socket is not connected.");
        } else if (this.blI) {
            throw new IOException("Socket has already been closed.");
        } else {
            if (!this.blF) {
                this.blF = true;
                if (this.lr.getUseClientMode()) {
                    this.aYU = new bg(this);
                } else {
                    this.aYU = new bi(this);
                }
                this.aYV = new bo();
                this.sR = new an(this.aYU, this.aYV, new bm(this.blJ), this.blG.Jb);
            }
            this.aYU.start();
            DK();
        }
    }

    public String toString() {
        return "[SSLSocketImpl]";
    }
}
