package a.a.a.a.a.a;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLException;

public class u {
    private static Mac Il;
    private static Mac Im;
    private static int In;
    private static int Io;
    protected static MessageDigest qp;
    protected static MessageDigest qq;

    private static void a() {
        try {
            Il = Mac.getInstance("HmacMD5");
            Im = Mac.getInstance("HmacSHA1");
            In = Il.getMacLength();
            Io = Im.getMacLength();
            try {
                qp = MessageDigest.getInstance("MD5");
                qq = MessageDigest.getInstance("SHA-1");
            } catch (Exception e) {
                throw new ba((byte) 80, new SSLException("Could not initialize the Digest Algorithms."));
            }
        } catch (NoSuchAlgorithmException e2) {
            throw new ba((byte) 80, new SSLException("There is no provider of HmacSHA1 or HmacMD5 algorithms installed in the system"));
        }
    }

    static synchronized void a(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        int i;
        int i2 = 0;
        synchronized (u.class) {
            if (qq == null) {
                a();
            }
            int i3 = 1;
            while (i2 < bArr.length) {
                byte[] bArr4 = new byte[i3];
                int i4 = i3 + 1;
                Arrays.fill(bArr4, (byte) (i3 + 64));
                qq.update(bArr4);
                qq.update(bArr2);
                qq.update(bArr3);
                qp.update(bArr2);
                qp.update(qq.digest());
                byte[] digest = qp.digest();
                if (i2 + 16 > bArr.length) {
                    System.arraycopy(digest, 0, bArr, i2, bArr.length - i2);
                    i = bArr.length;
                } else {
                    System.arraycopy(digest, 0, bArr, i2, 16);
                    i = i2 + 16;
                }
                i2 = i;
                i3 = i4;
            }
        }
    }

    static synchronized void a(byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4) {
        SecretKeySpec secretKeySpec;
        SecretKeySpec secretKeySpec2;
        int i;
        synchronized (u.class) {
            if (Im == null) {
                a();
            }
            if (bArr2 == null || bArr2.length == 0) {
                byte[] bArr5 = new byte[8];
                secretKeySpec = new SecretKeySpec(bArr5, "HmacMD5");
                secretKeySpec2 = new SecretKeySpec(bArr5, "HmacSHA1");
            } else {
                int length = bArr2.length >> 1;
                int length2 = bArr2.length & 1;
                SecretKeySpec secretKeySpec3 = new SecretKeySpec(bArr2, 0, length + length2, "HmacMD5");
                secretKeySpec2 = new SecretKeySpec(bArr2, length, length2 + length, "HmacSHA1");
                secretKeySpec = secretKeySpec3;
            }
            Il.init(secretKeySpec);
            Im.init(secretKeySpec2);
            Il.update(bArr3);
            byte[] doFinal = Il.doFinal(bArr4);
            int i2 = 0;
            while (true) {
                if (i2 >= bArr.length) {
                    break;
                }
                Il.update(doFinal);
                Il.update(bArr3);
                Il.update(bArr4);
                if (In + i2 >= bArr.length) {
                    System.arraycopy(Il.doFinal(), 0, bArr, i2, bArr.length - i2);
                    break;
                }
                Il.doFinal(bArr, i2);
                i2 += In;
                doFinal = Il.doFinal(doFinal);
            }
            Im.update(bArr3);
            byte[] doFinal2 = Im.doFinal(bArr4);
            for (int i3 = 0; i3 < bArr.length; i3 = i) {
                Im.update(doFinal2);
                Im.update(bArr3);
                byte[] doFinal3 = Im.doFinal(bArr4);
                i = i3;
                int i4 = 0;
                while (true) {
                    if (!(i4 < Io) || !(i < bArr.length)) {
                        break;
                    }
                    bArr[i] = (byte) (bArr[i] ^ doFinal3[i4]);
                    i4++;
                    i++;
                }
                doFinal2 = Im.doFinal(doFinal2);
            }
        }
    }
}
