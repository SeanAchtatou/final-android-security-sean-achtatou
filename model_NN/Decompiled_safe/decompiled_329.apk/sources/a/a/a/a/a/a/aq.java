package a.a.a.a.a.a;

import java.nio.ByteBuffer;

public class aq extends ae {
    private ByteBuffer baN;
    private int baO;
    private int pJ = 0;

    protected aq() {
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        this.pJ = 0;
        this.baN = byteBuffer;
    }

    public int available() {
        return this.baN.remaining();
    }

    /* access modifiers changed from: protected */
    public int ee() {
        return this.pJ;
    }

    public int read() {
        this.baO = this.baN.get() & 255;
        this.pJ++;
        return this.baO;
    }
}
