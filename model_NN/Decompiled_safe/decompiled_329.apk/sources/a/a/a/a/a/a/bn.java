package a.a.a.a.a.a;

import java.net.Socket;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.X509ExtendedKeyManager;
import javax.security.auth.x500.X500Principal;

public class bn extends X509ExtendedKeyManager {
    private final Hashtable caJ = new Hashtable();

    public bn(KeyStore keyStore, char[] cArr) {
        try {
            Enumeration<String> aliases = keyStore.aliases();
            while (aliases.hasMoreElements()) {
                String nextElement = aliases.nextElement();
                try {
                    if (keyStore.entryInstanceOf(nextElement, KeyStore.PrivateKeyEntry.class)) {
                        this.caJ.put(nextElement, (KeyStore.PrivateKeyEntry) keyStore.getEntry(nextElement, new KeyStore.PasswordProtection(cArr)));
                    }
                } catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableEntryException e) {
                }
            }
        } catch (KeyStoreException e2) {
        }
    }

    private String[] a(String[] strArr, Principal[] principalArr) {
        if (strArr == null || strArr.length == 0) {
            return null;
        }
        Vector vector = new Vector();
        Enumeration keys = this.caJ.keys();
        while (keys.hasMoreElements()) {
            String str = (String) keys.nextElement();
            Certificate[] certificateChain = ((KeyStore.PrivateKeyEntry) this.caJ.get(str)).getCertificateChain();
            String algorithm = certificateChain[0].getPublicKey().getAlgorithm();
            for (String equals : strArr) {
                if (algorithm.equals(equals)) {
                    if (principalArr == null || principalArr.length == 0) {
                        vector.add(str);
                    } else {
                        int i = 0;
                        while (true) {
                            if (i >= certificateChain.length) {
                                break;
                            }
                            if (certificateChain[i] instanceof X509Certificate) {
                                X500Principal issuerX500Principal = ((X509Certificate) certificateChain[i]).getIssuerX500Principal();
                                for (Principal equals2 : principalArr) {
                                    if (issuerX500Principal.equals(equals2)) {
                                        vector.add(str);
                                        break;
                                    }
                                }
                                continue;
                            }
                            i++;
                        }
                    }
                }
            }
        }
        if (!vector.isEmpty()) {
            return (String[]) vector.toArray(new String[vector.size()]);
        }
        return null;
    }

    public String chooseClientAlias(String[] strArr, Principal[] principalArr, Socket socket) {
        String[] a2 = a(strArr, principalArr);
        if (a2 == null) {
            return null;
        }
        return a2[0];
    }

    public String chooseEngineClientAlias(String[] strArr, Principal[] principalArr, SSLEngine sSLEngine) {
        String[] a2 = a(strArr, principalArr);
        if (a2 == null) {
            return null;
        }
        return a2[0];
    }

    public String chooseEngineServerAlias(String str, Principal[] principalArr, SSLEngine sSLEngine) {
        String[] a2 = a(new String[]{str}, principalArr);
        if (a2 == null) {
            return null;
        }
        return a2[0];
    }

    public String chooseServerAlias(String str, Principal[] principalArr, Socket socket) {
        String[] a2 = a(new String[]{str}, principalArr);
        if (a2 == null) {
            return null;
        }
        return a2[0];
    }

    public X509Certificate[] getCertificateChain(String str) {
        if (this.caJ.containsKey(str)) {
            Certificate[] certificateChain = ((KeyStore.PrivateKeyEntry) this.caJ.get(str)).getCertificateChain();
            if (certificateChain[0] instanceof X509Certificate) {
                X509Certificate[] x509CertificateArr = new X509Certificate[certificateChain.length];
                for (int i = 0; i < certificateChain.length; i++) {
                    x509CertificateArr[i] = (X509Certificate) certificateChain[i];
                }
                return x509CertificateArr;
            }
        }
        return null;
    }

    public String[] getClientAliases(String str, Principal[] principalArr) {
        return a(new String[]{str}, principalArr);
    }

    public PrivateKey getPrivateKey(String str) {
        if (this.caJ.containsKey(str)) {
            return ((KeyStore.PrivateKeyEntry) this.caJ.get(str)).getPrivateKey();
        }
        return null;
    }

    public String[] getServerAliases(String str, Principal[] principalArr) {
        return a(new String[]{str}, principalArr);
    }
}
