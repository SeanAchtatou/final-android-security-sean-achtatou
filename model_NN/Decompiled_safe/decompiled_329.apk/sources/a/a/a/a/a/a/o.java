package a.a.a.a.a.a;

import a.b.a;
import java.util.Hashtable;

public class o {
    public static final String[] ug = {"TLSv1", "SSLv3"};
    private static Hashtable uh = new Hashtable(4);
    public static o ui = new o("SSLv3", new byte[]{3, 0});
    public static o uj = new o("TLSv1", new byte[]{3, 1});
    public final String name;
    public final byte[] uk;

    static {
        uh.put(ui.name, ui);
        uh.put(uj.name, uj);
        uh.put(a.SSL, ui);
        uh.put(a.TLS, uj);
    }

    private o(String str, byte[] bArr) {
        this.name = str;
        this.uk = bArr;
    }

    public static boolean G(byte[] bArr) {
        return bArr[0] == 3 && (bArr[1] == 0 || bArr[1] == 1);
    }

    public static o H(byte[] bArr) {
        if (bArr[0] == 3) {
            if (bArr[1] == 1) {
                return uj;
            }
            if (bArr[1] == 0) {
                return ui;
            }
        }
        return null;
    }

    public static o N(String str) {
        return (o) uh.get(str);
    }

    public static o c(String[] strArr) {
        if (strArr == null || strArr.length == 0) {
            return null;
        }
        o N = N(strArr[0]);
        for (int i = 1; i < strArr.length; i++) {
            o N2 = N(strArr[i]);
            if (N2 != null && (N == null || N.uk[0] < N2.uk[0] || (N.uk[0] == N2.uk[0] && N.uk[1] < N2.uk[1]))) {
                N = N2;
            }
        }
        return N;
    }

    public static boolean isSupported(String str) {
        return uh.containsKey(str);
    }

    public boolean equals(Object obj) {
        return (obj instanceof o) && this.uk[0] == ((o) obj).uk[0] && this.uk[1] == ((o) obj).uk[1];
    }
}
