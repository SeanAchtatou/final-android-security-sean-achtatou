package a.a.a.a.a.a;

import javax.crypto.Cipher;

public abstract class h {
    protected Cipher mf;
    protected Cipher mg;
    protected boolean mh;
    protected int mi;
    protected final byte[] mj = {0, 0, 0, 0, 0, 0, 0, 0};
    protected final byte[] mk = {0, 0, 0, 0, 0, 0, 0, 0};

    protected static void z(byte[] bArr) {
        int i = 7;
        while (i >= 0) {
            bArr[i] = (byte) (bArr[i] + 1);
            if (bArr[i] == 0) {
                i--;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public int J(int i) {
        return this.mf.getOutputSize(this.mi + i);
    }

    /* access modifiers changed from: protected */
    public int K(int i) {
        return this.mg.getOutputSize(i) - this.mi;
    }

    /* access modifiers changed from: protected */
    public byte[] a(byte b2, byte[] bArr) {
        return a(b2, bArr, 0, bArr.length);
    }

    /* access modifiers changed from: protected */
    public abstract byte[] a(byte b2, byte[] bArr, int i, int i2);

    /* access modifiers changed from: protected */
    public byte[] b(byte b2, byte[] bArr) {
        return b(b2, bArr, 0, bArr.length);
    }

    /* access modifiers changed from: protected */
    public abstract byte[] b(byte b2, byte[] bArr, int i, int i2);

    /* access modifiers changed from: protected */
    public int cX() {
        return this.mf.getOutputSize(this.mi + 1);
    }

    /* access modifiers changed from: protected */
    public void shutdown() {
        this.mf = null;
        this.mg = null;
        for (int i = 0; i < this.mj.length; i++) {
            this.mj[i] = 0;
            this.mk[i] = 0;
        }
    }
}
