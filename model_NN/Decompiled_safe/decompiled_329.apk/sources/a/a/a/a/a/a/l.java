package a.a.a.a.a.a;

import java.io.PrintStream;
import java.security.MessageDigest;
import java.util.Arrays;
import javax.net.ssl.SSLHandshakeException;

public class l extends ae implements ak, z {
    private static final MessageDigest qp;
    private static final MessageDigest qq;
    private byte[] buffer = new byte[this.qr];
    private int qr = 1024;
    private int qs = 1024;
    private int qt;
    private int qu;
    private int qv;
    private int qw;
    private int qx;

    static {
        try {
            qp = MessageDigest.getInstance("MD5");
            qq = MessageDigest.getInstance("SHA-1");
        } catch (Exception e) {
            throw new RuntimeException("Could not initialize the Digest Algorithms.");
        }
    }

    private void al(int i) {
        this.qr = i < this.qs ? this.qr + this.qs : this.qr + i;
        byte[] bArr = new byte[this.qr];
        System.arraycopy(this.buffer, 0, bArr, 0, this.buffer.length);
        this.buffer = bArr;
    }

    private void append(byte[] bArr, int i, int i2) {
        if (this.qt == this.qv) {
            if (this.qx != this.qw) {
                throw new ba((byte) 10, new SSLHandshakeException("Handshake message has been received before the last oubound message had been sent."));
            } else if (this.qt < this.qw) {
                this.qt = this.qw;
                this.qv = this.qt;
            }
        }
        if (this.qv + i2 > this.qr) {
            al((this.qv + i2) - this.qr);
        }
        System.arraycopy(bArr, i, this.buffer, this.qv, i2);
        this.qv += i2;
    }

    private void check(int i) {
        if (this.qw == this.qx) {
            if (this.qt != this.qv) {
                throw new ba((byte) 80, new SSLHandshakeException("Data was not fully read: " + this.qt + " " + this.qv));
            } else if (this.qx < this.qv) {
                this.qx = this.qv;
                this.qw = this.qx;
            }
        }
        if (this.qw + i >= this.qr) {
            al(i);
        }
    }

    public void A(byte[] bArr) {
        append(bArr, 0, bArr.length);
    }

    /* access modifiers changed from: protected */
    public void a(PrintStream printStream) {
        for (int i = this.qx; i < this.qw; i++) {
            String upperCase = Integer.toHexString(this.buffer[i] & 255).toUpperCase();
            if (upperCase.length() == 1) {
                upperCase = "0" + upperCase;
            }
            printStream.print(" " + upperCase + "");
            if (((i - this.qx) + 1) % 10 == 0) {
                printStream.print(" ");
            }
            if (((i - this.qx) + 1) % 20 == 0) {
                printStream.println();
            }
        }
        printStream.println();
    }

    public byte[] ai(int i) {
        if (this.qw - this.qx < i) {
            byte[] bArr = new byte[(this.qw - this.qx)];
            System.arraycopy(this.buffer, this.qx, bArr, 0, this.qw - this.qx);
            this.qx = this.qw;
            return bArr;
        }
        byte[] bArr2 = new byte[i];
        System.arraycopy(this.buffer, this.qx, bArr2, 0, i);
        this.qx += i;
        return bArr2;
    }

    public byte[] ak(int i) {
        if (i > available()) {
            throw new av();
        }
        byte[] bArr = new byte[i];
        System.arraycopy(this.buffer, this.qt, bArr, 0, i);
        this.qt += i;
        return bArr;
    }

    public int available() {
        return this.qv - this.qt;
    }

    public void b(long j) {
        check(1);
        byte[] bArr = this.buffer;
        int i = this.qw;
        this.qw = i + 1;
        bArr[i] = (byte) ((int) (255 & j));
    }

    public void c(long j) {
        check(2);
        byte[] bArr = this.buffer;
        int i = this.qw;
        this.qw = i + 1;
        bArr[i] = (byte) ((int) ((65280 & j) >> 8));
        byte[] bArr2 = this.buffer;
        int i2 = this.qw;
        this.qw = i2 + 1;
        bArr2[i2] = (byte) ((int) (255 & j));
    }

    public void d(long j) {
        check(3);
        byte[] bArr = this.buffer;
        int i = this.qw;
        this.qw = i + 1;
        bArr[i] = (byte) ((int) ((16711680 & j) >> 16));
        byte[] bArr2 = this.buffer;
        int i2 = this.qw;
        this.qw = i2 + 1;
        bArr2[i2] = (byte) ((int) ((65280 & j) >> 8));
        byte[] bArr3 = this.buffer;
        int i3 = this.qw;
        this.qw = i3 + 1;
        bArr3[i3] = (byte) ((int) (255 & j));
    }

    public void e(long j) {
        check(4);
        byte[] bArr = this.buffer;
        int i = this.qw;
        this.qw = i + 1;
        bArr[i] = (byte) ((int) ((-16777216 & j) >> 24));
        byte[] bArr2 = this.buffer;
        int i2 = this.qw;
        this.qw = i2 + 1;
        bArr2[i2] = (byte) ((int) ((16711680 & j) >> 16));
        byte[] bArr3 = this.buffer;
        int i3 = this.qw;
        this.qw = i3 + 1;
        bArr3[i3] = (byte) ((int) ((65280 & j) >> 8));
        byte[] bArr4 = this.buffer;
        int i4 = this.qw;
        this.qw = i4 + 1;
        bArr4[i4] = (byte) ((int) (255 & j));
    }

    public boolean ed() {
        return this.qw > this.qx;
    }

    public void ej() {
        this.qu = this.qt;
    }

    /* access modifiers changed from: protected */
    public void ek() {
        System.arraycopy(this.buffer, this.qt, this.buffer, this.qu, this.qv - this.qt);
        this.qv -= this.qt - this.qu;
        this.qt = this.qu;
    }

    /* access modifiers changed from: protected */
    public void el() {
        this.qt = 0;
        this.qu = 0;
        this.qv = 0;
        this.qw = 0;
        this.qx = 0;
        Arrays.fill(this.buffer, (byte) 0);
    }

    /* access modifiers changed from: protected */
    public byte[] em() {
        byte[] digest;
        synchronized (qp) {
            qp.update(this.buffer, 0, this.qv > this.qw ? this.qv : this.qw);
            digest = qp.digest();
        }
        return digest;
    }

    /* access modifiers changed from: protected */
    public byte[] en() {
        byte[] digest;
        synchronized (qq) {
            qq.update(this.buffer, 0, this.qv > this.qw ? this.qv : this.qw);
            digest = qq.digest();
        }
        return digest;
    }

    /* access modifiers changed from: protected */
    public byte[] eo() {
        byte[] digest;
        synchronized (qp) {
            qp.update(this.buffer, 0, this.qu);
            digest = qp.digest();
        }
        return digest;
    }

    /* access modifiers changed from: protected */
    public byte[] ep() {
        byte[] digest;
        synchronized (qq) {
            qq.update(this.buffer, 0, this.qu);
            digest = qq.digest();
        }
        return digest;
    }

    /* access modifiers changed from: protected */
    public byte[] eq() {
        int i = this.qv > this.qw ? this.qv : this.qw;
        byte[] bArr = new byte[i];
        System.arraycopy(this.buffer, 0, bArr, 0, i);
        return bArr;
    }

    public void f(long j) {
        check(8);
        byte[] bArr = this.buffer;
        int i = this.qw;
        this.qw = i + 1;
        bArr[i] = (byte) ((int) ((-72057594037927936L & j) >> 56));
        byte[] bArr2 = this.buffer;
        int i2 = this.qw;
        this.qw = i2 + 1;
        bArr2[i2] = (byte) ((int) ((71776119061217280L & j) >> 48));
        byte[] bArr3 = this.buffer;
        int i3 = this.qw;
        this.qw = i3 + 1;
        bArr3[i3] = (byte) ((int) ((280375465082880L & j) >> 40));
        byte[] bArr4 = this.buffer;
        int i4 = this.qw;
        this.qw = i4 + 1;
        bArr4[i4] = (byte) ((int) ((1095216660480L & j) >> 32));
        byte[] bArr5 = this.buffer;
        int i5 = this.qw;
        this.qw = i5 + 1;
        bArr5[i5] = (byte) ((int) ((-16777216 & j) >> 24));
        byte[] bArr6 = this.buffer;
        int i6 = this.qw;
        this.qw = i6 + 1;
        bArr6[i6] = (byte) ((int) ((16711680 & j) >> 16));
        byte[] bArr7 = this.buffer;
        int i7 = this.qw;
        this.qw = i7 + 1;
        bArr7[i7] = (byte) ((int) ((65280 & j) >> 8));
        byte[] bArr8 = this.buffer;
        int i8 = this.qw;
        this.qw = i8 + 1;
        bArr8[i8] = (byte) ((int) (255 & j));
    }

    public void g(byte b2) {
        check(1);
        byte[] bArr = this.buffer;
        int i = this.qw;
        this.qw = i + 1;
        bArr[i] = b2;
    }

    public void mark(int i) {
        this.qu = this.qt;
    }

    public boolean markSupported() {
        return true;
    }

    public int read() {
        if (this.qt == this.qv) {
            throw new av();
        }
        byte[] bArr = this.buffer;
        int i = this.qt;
        this.qt = i + 1;
        return bArr[i] & 255;
    }

    public int read(byte[] bArr, int i, int i2) {
        if (i2 > available()) {
            throw new av();
        }
        System.arraycopy(this.buffer, this.qt, bArr, i, i2);
        this.qt += i2;
        return i2;
    }

    public void reset() {
        this.qt = this.qu;
    }

    public void write(byte[] bArr) {
        check(bArr.length);
        System.arraycopy(bArr, 0, this.buffer, this.qw, bArr.length);
        this.qw += bArr.length;
    }
}
