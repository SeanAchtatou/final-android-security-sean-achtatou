package a.a.a.a.a.a;

import a.b.a;
import java.security.PrivilegedAction;

class v implements PrivilegedAction {
    final /* synthetic */ c IS;

    v(c cVar) {
        this.IS = cVar;
    }

    /* renamed from: kU */
    public Void run() {
        this.IS.put("SSLContext.TLS", f.class.getName());
        this.IS.put("Alg.Alias.SSLContext.TLSv1", a.TLS);
        this.IS.put("KeyManagerFactory.X509", ay.class.getName());
        this.IS.put("TrustManagerFactory.X509", q.class.getName());
        return null;
    }
}
