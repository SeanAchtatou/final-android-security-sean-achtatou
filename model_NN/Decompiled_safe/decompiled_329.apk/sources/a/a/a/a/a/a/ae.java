package a.a.a.a.a.a;

import java.io.InputStream;

public abstract class ae extends InputStream {
    public byte[] ak(int i) {
        byte[] bArr = new byte[i];
        for (int i2 = 0; i2 < i; i2++) {
            bArr[i2] = (byte) read();
        }
        return bArr;
    }

    public abstract int available();

    public abstract int read();

    public int read(byte[] bArr, int i, int i2) {
        int i3 = 0;
        do {
            int read = read();
            if (read != -1) {
                bArr[i + i3] = (byte) read;
                i3++;
                if (available() == 0) {
                    return i3;
                }
            } else if (i3 == 0) {
                return -1;
            } else {
                return i3;
            }
        } while (i3 < i2);
        return i3;
    }

    public long skip(long j) {
        for (long j2 = j; j2 > 0; j2--) {
            read();
        }
        return j;
    }

    public int xb() {
        return read() & 255;
    }

    public int xc() {
        return (read() << 8) | (read() & 255);
    }

    public int xd() {
        return (read() << 16) | (read() << 8) | (read() & 255);
    }

    public long xe() {
        return (long) ((read() << 24) | (read() << 16) | (read() << 8) | (read() & 255));
    }

    public long xf() {
        return (long) ((read() << 56) | (read() << 48) | (read() << 40) | (read() << 32) | (read() << 24) | (read() << 16) | (read() << 8) | (read() & 255));
    }
}
