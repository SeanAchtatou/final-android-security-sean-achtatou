package a.a.a.a.a.a;

import com.uc.b.b;
import com.uc.c.ca;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLProtocolException;

public class ax extends h {
    private static byte[] bmT = {107, 101, 121, 32, 101, ca.bKP, 112, 97, 110, 115, 105, 111, 110};
    private static byte[] bmU = {b.pC, 108, 105, 101, 110, 116, 32, 119, 114, 105, 116, 101, 32, 107, 101, 121};
    private static byte[] bmV = {115, 101, 114, 118, 101, 114, 32, 119, 114, 105, 116, 101, 32, 107, 101, 121};
    private static byte[] bmW = {73, 86, 32, 98, 108, 111, b.pC, 107};
    private final Mac bmX;
    private final Mac bmY;
    private final byte[] bmZ = {0, 3, 1, 0, 0};

    protected ax(bk bkVar) {
        IvParameterSpec ivParameterSpec;
        IvParameterSpec ivParameterSpec2;
        byte[] bArr;
        byte[] bArr2;
        IvParameterSpec ivParameterSpec3;
        IvParameterSpec ivParameterSpec4;
        try {
            bc bcVar = bkVar.bYk;
            this.mi = bcVar.FJ();
            boolean FK = bcVar.FK();
            int i = FK ? bcVar.bsf : bcVar.bsg;
            int blockSize = bcVar.getBlockSize();
            String FG = bcVar.FG();
            String FH = bcVar.FH();
            byte[] bArr3 = bkVar.bYr;
            byte[] bArr4 = bkVar.bYs;
            byte[] bArr5 = new byte[((this.mi * 2) + (i * 2) + (blockSize * 2))];
            byte[] bArr6 = new byte[(bArr3.length + bArr4.length)];
            System.arraycopy(bArr4, 0, bArr6, 0, bArr4.length);
            System.arraycopy(bArr3, 0, bArr6, bArr4.length, bArr3.length);
            u.a(bArr5, bkVar.bYq, bmT, bArr6);
            byte[] bArr7 = new byte[this.mi];
            byte[] bArr8 = new byte[this.mi];
            byte[] bArr9 = new byte[i];
            byte[] bArr10 = new byte[i];
            boolean z = !bkVar.bYt;
            this.mh = blockSize > 0;
            System.arraycopy(bArr5, 0, bArr7, 0, this.mi);
            System.arraycopy(bArr5, this.mi, bArr8, 0, this.mi);
            System.arraycopy(bArr5, this.mi * 2, bArr9, 0, i);
            System.arraycopy(bArr5, (this.mi * 2) + i, bArr10, 0, i);
            if (FK) {
                System.arraycopy(bArr3, 0, bArr6, 0, bArr3.length);
                System.arraycopy(bArr4, 0, bArr6, bArr3.length, bArr4.length);
                byte[] bArr11 = new byte[bcVar.bsg];
                byte[] bArr12 = new byte[bcVar.bsg];
                u.a(bArr11, bArr9, bmU, bArr6);
                u.a(bArr12, bArr10, bmV, bArr6);
                if (this.mh) {
                    byte[] bArr13 = new byte[(blockSize * 2)];
                    u.a(bArr13, null, bmW, bArr6);
                    IvParameterSpec ivParameterSpec5 = new IvParameterSpec(bArr13, 0, blockSize);
                    ivParameterSpec3 = new IvParameterSpec(bArr13, blockSize, blockSize);
                    ivParameterSpec4 = ivParameterSpec5;
                } else {
                    ivParameterSpec3 = null;
                    ivParameterSpec4 = null;
                }
                IvParameterSpec ivParameterSpec6 = ivParameterSpec3;
                bArr = bArr12;
                ivParameterSpec = ivParameterSpec6;
                IvParameterSpec ivParameterSpec7 = ivParameterSpec4;
                bArr2 = bArr11;
                ivParameterSpec2 = ivParameterSpec7;
            } else if (this.mh) {
                IvParameterSpec ivParameterSpec8 = new IvParameterSpec(bArr5, (this.mi + i) * 2, blockSize);
                IvParameterSpec ivParameterSpec9 = new IvParameterSpec(bArr5, ((i + this.mi) * 2) + blockSize, blockSize);
                bArr = bArr10;
                bArr2 = bArr9;
                IvParameterSpec ivParameterSpec10 = ivParameterSpec9;
                ivParameterSpec2 = ivParameterSpec8;
                ivParameterSpec = ivParameterSpec10;
            } else {
                ivParameterSpec = null;
                ivParameterSpec2 = null;
                bArr = bArr10;
                bArr2 = bArr9;
            }
            this.mf = Cipher.getInstance(FG);
            this.mg = Cipher.getInstance(FG);
            this.bmX = Mac.getInstance(FH);
            this.bmY = Mac.getInstance(FH);
            if (z) {
                this.mf.init(1, new SecretKeySpec(bArr2, FG), ivParameterSpec2);
                this.mg.init(2, new SecretKeySpec(bArr, FG), ivParameterSpec);
                this.bmX.init(new SecretKeySpec(bArr7, FH));
                this.bmY.init(new SecretKeySpec(bArr8, FH));
                return;
            }
            this.mf.init(1, new SecretKeySpec(bArr, FG), ivParameterSpec);
            this.mg.init(2, new SecretKeySpec(bArr2, FG), ivParameterSpec2);
            this.bmX.init(new SecretKeySpec(bArr8, FH));
            this.bmY.init(new SecretKeySpec(bArr7, FH));
        } catch (Exception e) {
            throw new ba((byte) 80, new SSLProtocolException("Error during computation of security parameters"));
        }
    }

    /* access modifiers changed from: protected */
    public byte[] a(byte b2, byte[] bArr, int i, int i2) {
        int i3;
        int i4;
        try {
            int i5 = this.mi + i2;
            if (this.mh) {
                int i6 = i5 + 1;
                i3 = i6;
                i4 = (8 - (i6 & 7)) & 7;
            } else {
                i3 = i5;
                i4 = 0;
            }
            byte[] bArr2 = new byte[(i3 + i4)];
            System.arraycopy(bArr, i, bArr2, 0, i2);
            this.bmZ[0] = b2;
            this.bmZ[3] = (byte) ((65280 & i2) >> 8);
            this.bmZ[4] = (byte) (i2 & 255);
            this.bmX.update(this.mj);
            this.bmX.update(this.bmZ);
            this.bmX.update(bArr, i, i2);
            this.bmX.doFinal(bArr2, i2);
            if (this.mh) {
                Arrays.fill(bArr2, i3 - 1, bArr2.length, (byte) i4);
            }
            byte[] bArr3 = new byte[this.mf.getOutputSize(bArr2.length)];
            this.mf.update(bArr2, 0, bArr2.length, bArr3);
            z(this.mj);
            return bArr3;
        } catch (GeneralSecurityException e) {
            throw new ba((byte) 80, new SSLProtocolException("Error during the encryption"));
        }
    }

    /* access modifiers changed from: protected */
    public byte[] b(byte b2, byte[] bArr, int i, int i2) {
        byte[] bArr2;
        byte[] update = this.mg.update(bArr, i, i2);
        if (this.mh) {
            byte b3 = update[update.length - 1];
            for (int i3 = 0; i3 < b3; i3++) {
                if (update[(update.length - 2) - i3] != b3) {
                    throw new ba((byte) 21, new SSLProtocolException("Received message has bad padding"));
                }
            }
            bArr2 = new byte[(((update.length - this.mi) - b3) - 1)];
        } else {
            bArr2 = new byte[(update.length - this.mi)];
        }
        this.bmZ[0] = b2;
        this.bmZ[3] = (byte) ((65280 & bArr2.length) >> 8);
        this.bmZ[4] = (byte) (bArr2.length & 255);
        this.bmY.update(this.mk);
        this.bmY.update(this.bmZ);
        this.bmY.update(update, 0, bArr2.length);
        byte[] doFinal = this.bmY.doFinal();
        for (int i4 = 0; i4 < this.mi; i4++) {
            if (doFinal[i4] != update[bArr2.length + i4]) {
                throw new ba((byte) 20, new SSLProtocolException("Bad record MAC"));
            }
        }
        System.arraycopy(update, 0, bArr2, 0, bArr2.length);
        z(this.mk);
        return bArr2;
    }
}
