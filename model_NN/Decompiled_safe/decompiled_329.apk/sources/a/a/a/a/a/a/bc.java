package a.a.a.a.a.a;

import java.security.GeneralSecurityException;
import java.util.Hashtable;
import javax.crypto.Cipher;

public class bc {
    static int bsA = 12;
    static byte[] bsB = {0, 0};
    static byte[] bsC = {0, 1};
    static byte[] bsD = {0, 2};
    static byte[] bsE = {0, 3};
    static byte[] bsF = {0, 4};
    static byte[] bsG = {0, 5};
    static byte[] bsH = {0, 6};
    static byte[] bsI = {0, 7};
    static byte[] bsJ = {0, 8};
    static byte[] bsK = {0, 9};
    static byte[] bsL = {0, 10};
    static byte[] bsM = {0, 11};
    static byte[] bsN = {0, 12};
    static byte[] bsO = {0, 13};
    static byte[] bsP = {0, 14};
    static byte[] bsQ = {0, 15};
    static byte[] bsR = {0, 16};
    static byte[] bsS = {0, 17};
    static byte[] bsT = {0, 18};
    static byte[] bsU = {0, 19};
    static byte[] bsV = {0, 20};
    static byte[] bsW = {0, 21};
    static byte[] bsX = {0, 22};
    static byte[] bsY = {0, 23};
    static byte[] bsZ = {0, 24};
    static int bsp = 1;
    static int bsq = 2;
    static int bsr = 3;
    static int bss = 4;
    static int bst = 5;
    static int bsu = 6;
    static int bsv = 7;
    static int bsw = 8;
    static int bsx = 9;
    static int bsy = 10;
    static int bsz = 11;
    static bc btA = new bc("TLS_DH_anon_EXPORT_WITH_RC4_40_MD5", true, bsy, "RC4_40", "MD5", bsY);
    static bc btB = new bc("TLS_DH_anon_WITH_RC4_128_MD5", false, bsx, "RC4_128", "MD5", bsZ);
    static bc btC = new bc("TLS_DH_anon_EXPORT_WITH_DES40_CBC_SHA", true, bsy, "DES40_CBC", "SHA", bta);
    static bc btD = new bc("TLS_DH_anon_WITH_DES_CBC_SHA", false, bsx, "DES_CBC", "SHA", btb);
    static bc btE = new bc("TLS_DH_anon_WITH_3DES_EDE_CBC_SHA", false, bsx, "3DES_EDE_CBC", "SHA", btc);
    private static bc[] btF = {btd, bte, btf, btg, bth, bti, btj, btk, btl, btm, btn, bto, btp, btq, btr, bts, btt, btu, btv, btw, btx, bty, btz, btA, btB, btC, btD, btE};
    private static Hashtable btG = new Hashtable();
    static bc[] btH;
    static String[] btI;
    static bc[] btJ;
    static byte[] bta = {0, 25};
    static byte[] btb = {0, 26};
    static byte[] btc = {0, 27};
    static bc btd = new bc("TLS_NULL_WITH_NULL_NULL", true, 0, null, null, bsB);
    static bc bte = new bc("TLS_RSA_WITH_NULL_MD5", true, bsp, null, "MD5", bsC);
    static bc btf = new bc("TLS_RSA_WITH_NULL_SHA", true, bsp, null, "SHA", bsD);
    static bc btg = new bc("TLS_RSA_EXPORT_WITH_RC4_40_MD5", true, bsq, "RC4_40", "MD5", bsE);
    static bc bth = new bc("TLS_RSA_WITH_RC4_128_MD5", false, bsp, "RC4_128", "MD5", bsF);
    static bc bti = new bc("TLS_RSA_WITH_RC4_128_SHA", false, bsp, "RC4_128", "SHA", bsG);
    static bc btj = new bc("TLS_RSA_EXPORT_WITH_RC2_CBC_40_MD5", true, bsq, "RC2_CBC_40", "MD5", bsH);
    static bc btk = new bc("TLS_RSA_WITH_IDEA_CBC_SHA", false, bsp, "IDEA_CBC", "SHA", bsI);
    static bc btl = new bc("TLS_RSA_EXPORT_WITH_DES40_CBC_SHA", true, bsq, "DES40_CBC", "SHA", bsJ);
    static bc btm = new bc("TLS_RSA_WITH_DES_CBC_SHA", false, bsp, "DES_CBC", "SHA", bsK);
    static bc btn = new bc("TLS_RSA_WITH_3DES_EDE_CBC_SHA", false, bsp, "3DES_EDE_CBC", "SHA", bsL);
    static bc bto = new bc("TLS_DH_DSS_EXPORT_WITH_DES40_CBC_SHA", true, bsz, "DES40_CBC", "SHA", bsM);
    static bc btp = new bc("TLS_DH_DSS_WITH_DES_CBC_SHA", false, bsv, "DES_CBC", "SHA", bsN);
    static bc btq = new bc("TLS_DH_DSS_WITH_3DES_EDE_CBC_SHA", false, bsv, "3DES_EDE_CBC", "SHA", bsO);
    static bc btr = new bc("TLS_DH_RSA_EXPORT_WITH_DES40_CBC_SHA", true, bsA, "DES40_CBC", "SHA", bsP);
    static bc bts = new bc("TLS_DH_RSA_WITH_DES_CBC_SHA", false, bsw, "DES_CBC", "SHA", bsQ);
    static bc btt = new bc("TLS_DH_RSA_WITH_3DES_EDE_CBC_SHA", false, bsw, "3DES_EDE_CBC", "SHA", bsR);
    static bc btu = new bc("TLS_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA", true, bss, "DES40_CBC", "SHA", bsS);
    static bc btv = new bc("TLS_DHE_DSS_WITH_DES_CBC_SHA", false, bsr, "DES_CBC", "SHA", bsT);
    static bc btw = new bc("TLS_DHE_DSS_WITH_3DES_EDE_CBC_SHA", false, bsr, "3DES_EDE_CBC", "SHA", bsU);
    static bc btx = new bc("TLS_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA", true, bsu, "DES40_CBC", "SHA", bsV);
    static bc bty = new bc("TLS_DHE_RSA_WITH_DES_CBC_SHA", false, bst, "DES_CBC", "SHA", bsW);
    static bc btz = new bc("TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA", false, bst, "3DES_EDE_CBC", "SHA", bsX);
    boolean bsc = true;
    final int bsd;
    final String bse;
    final int bsf;
    final int bsg;
    final int bsh;
    final int bsi;
    private final int bsj;
    private final byte[] bsk;
    private final boolean bsl;
    private final String bsm;
    private final String bsn;
    private final int bso;
    private final String name;

    static {
        int i = 0;
        for (int i2 = 0; i2 < btF.length; i2++) {
            btG.put(btF[i2].getName(), btF[i2]);
            if (btF[i2].bsc) {
                i++;
            }
        }
        btH = new bc[i];
        btI = new String[i];
        int i3 = 0;
        for (int i4 = 0; i4 < btF.length; i4++) {
            if (btF[i4].bsc) {
                btH[i3] = btF[i4];
                btI[i3] = btH[i3].getName();
                i3++;
            }
        }
        bc[] bcVarArr = {bth, bti, btn, btz, btw, btm, bty, btv, btg, btl, btx, btu};
        int i5 = 0;
        for (bc bcVar : bcVarArr) {
            if (bcVar.bsc) {
                i5++;
            }
        }
        btJ = new bc[i5];
        int i6 = 0;
        for (int i7 = 0; i7 < bcVarArr.length; i7++) {
            if (bcVarArr[i7].bsc) {
                btJ[i6] = bcVarArr[i7];
                i6++;
            }
        }
    }

    public bc(String str, boolean z, int i, String str2, String str3, byte[] bArr) {
        this.name = str;
        this.bsd = i;
        this.bsl = z;
        if (str2 == null) {
            this.bse = null;
            this.bsf = 0;
            this.bsg = 0;
            this.bsh = 0;
            this.bsi = 0;
            this.bsj = 0;
        } else if ("IDEA_CBC".equals(str2)) {
            this.bse = "IDEA/CBC/NoPadding";
            this.bsf = 16;
            this.bsg = 16;
            this.bsh = 16;
            this.bsi = 8;
            this.bsj = 8;
        } else if ("RC2_CBC_40".equals(str2)) {
            this.bse = "RC2/CBC/NoPadding";
            this.bsf = 5;
            this.bsg = 16;
            this.bsh = 5;
            this.bsi = 8;
            this.bsj = 8;
        } else if ("RC4_40".equals(str2)) {
            this.bse = "RC4";
            this.bsf = 5;
            this.bsg = 16;
            this.bsh = 5;
            this.bsi = 0;
            this.bsj = 0;
        } else if ("RC4_128".equals(str2)) {
            this.bse = "RC4";
            this.bsf = 16;
            this.bsg = 16;
            this.bsh = 16;
            this.bsi = 0;
            this.bsj = 0;
        } else if ("DES40_CBC".equals(str2)) {
            this.bse = "DES/CBC/NoPadding";
            this.bsf = 5;
            this.bsg = 8;
            this.bsh = 5;
            this.bsi = 8;
            this.bsj = 8;
        } else if ("DES_CBC".equals(str2)) {
            this.bse = "DES/CBC/NoPadding";
            this.bsf = 8;
            this.bsg = 8;
            this.bsh = 7;
            this.bsi = 8;
            this.bsj = 8;
        } else if ("3DES_EDE_CBC".equals(str2)) {
            this.bse = "DESede/CBC/NoPadding";
            this.bsf = 24;
            this.bsg = 24;
            this.bsh = 24;
            this.bsi = 8;
            this.bsj = 8;
        } else {
            this.bse = str2;
            this.bsf = 0;
            this.bsg = 0;
            this.bsh = 0;
            this.bsi = 0;
            this.bsj = 0;
        }
        if ("MD5".equals(str3)) {
            this.bsn = "HmacMD5";
            this.bsm = "MD5";
            this.bso = 16;
        } else if ("SHA".equals(str3)) {
            this.bsn = "HmacSHA1";
            this.bsm = "SHA-1";
            this.bso = 20;
        } else {
            this.bsn = null;
            this.bsm = null;
            this.bso = 0;
        }
        this.bsk = bArr;
        if (this.bse != null) {
            try {
                Cipher.getInstance(this.bse);
            } catch (GeneralSecurityException e) {
                this.bsc = false;
            }
        }
    }

    public static bc[] FD() {
        return btH;
    }

    public static String[] FE() {
        return (String[]) btI.clone();
    }

    public static bc a(byte b2, byte b3, byte b4) {
        if (b2 == 0 && b3 == 0 && (b4 & 255) <= btF.length) {
            return btF[b4];
        }
        return new bc("UNKNOUN_" + ((int) b2) + "_" + ((int) b3) + "_" + ((int) b4), false, 0, "", "", new byte[]{b2, b3, b4});
    }

    public static bc b(byte b2, byte b3) {
        if (b2 == 0 && (b3 & 255) <= btF.length) {
            return btF[b3];
        }
        return new bc("UNKNOUN_" + ((int) b2) + "_" + ((int) b3), false, 0, "", "", new byte[]{b2, b3});
    }

    public static bc eI(String str) {
        return (bc) btG.get(str);
    }

    public boolean FC() {
        return this.bsd == bsx || this.bsd == bsy;
    }

    public byte[] FF() {
        return this.bsk;
    }

    public String FG() {
        return this.bse;
    }

    public String FH() {
        return this.bsn;
    }

    public String FI() {
        return this.bsm;
    }

    public int FJ() {
        return this.bso;
    }

    public boolean FK() {
        return this.bsl;
    }

    public boolean equals(Object obj) {
        return (obj instanceof bc) && this.bsk[0] == ((bc) obj).bsk[0] && this.bsk[1] == ((bc) obj).bsk[1];
    }

    public int getBlockSize() {
        return this.bsj;
    }

    public String getName() {
        return this.name;
    }

    public String toString() {
        return this.name + ": " + ((int) this.bsk[0]) + " " + ((int) this.bsk[1]);
    }
}
