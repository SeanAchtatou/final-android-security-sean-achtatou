package a.a.a.a.a.a;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;

public class aa extends aj {
    final BigInteger ZS;
    final byte[] ZT;
    final BigInteger ZU;
    final byte[] ZV;
    final BigInteger ZW;
    final byte[] ZX;
    final byte[] ZY;
    private RSAPublicKey ZZ;

    public aa(l lVar, int i, int i2) {
        this.ZT = lVar.ak(lVar.xc());
        this.ZS = new BigInteger(1, this.ZT);
        this.length = this.ZT.length + 2;
        this.ZV = lVar.ak(lVar.xc());
        this.ZU = new BigInteger(1, this.ZV);
        this.length += this.ZV.length + 2;
        if (i2 != bc.bsq) {
            this.ZX = lVar.ak(lVar.xc());
            this.ZW = new BigInteger(1, this.ZX);
            this.length += this.ZX.length + 2;
        } else {
            this.ZW = null;
            this.ZX = null;
        }
        if (i2 == bc.bsy || i2 == bc.bsx) {
            this.ZY = null;
        } else {
            this.ZY = lVar.ak(lVar.xc());
            this.length += this.ZY.length + 2;
        }
        if (this.length != i) {
            a((byte) 50, "DECODE ERROR: incorrect ServerKeyExchange");
        }
    }

    public aa(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, byte[] bArr) {
        this.ZS = bigInteger;
        this.ZU = bigInteger2;
        this.ZW = bigInteger3;
        this.ZY = bArr;
        byte[] byteArray = this.ZS.toByteArray();
        if (byteArray[0] == 0) {
            this.ZT = new byte[(byteArray.length - 1)];
            System.arraycopy(byteArray, 1, this.ZT, 0, this.ZT.length);
        } else {
            this.ZT = byteArray;
        }
        byte[] byteArray2 = this.ZU.toByteArray();
        if (byteArray2[0] == 0) {
            this.ZV = new byte[(byteArray2.length - 1)];
            System.arraycopy(byteArray2, 1, this.ZV, 0, this.ZV.length);
        } else {
            this.ZV = byteArray2;
        }
        this.length = this.ZT.length + 4 + this.ZV.length;
        if (bArr != null) {
            this.length += bArr.length + 2;
        }
        if (bigInteger3 == null) {
            this.ZX = null;
            return;
        }
        byte[] byteArray3 = this.ZW.toByteArray();
        if (byteArray3[0] == 0) {
            this.ZX = new byte[(byteArray3.length - 1)];
            System.arraycopy(byteArray3, 1, this.ZX, 0, this.ZX.length);
        } else {
            this.ZX = byteArray3;
        }
        this.length += this.ZX.length + 2;
    }

    public void a(l lVar) {
        lVar.c((long) this.ZT.length);
        lVar.write(this.ZT);
        lVar.c((long) this.ZV.length);
        lVar.write(this.ZV);
        if (this.ZX != null) {
            lVar.c((long) this.ZX.length);
            lVar.write(this.ZX);
        }
        if (this.ZY != null) {
            lVar.c((long) this.ZY.length);
            lVar.write(this.ZY);
        }
    }

    public int getType() {
        return 12;
    }

    public RSAPublicKey um() {
        if (this.ZZ != null) {
            return this.ZZ;
        }
        try {
            this.ZZ = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(this.ZS, this.ZU));
            return this.ZZ;
        } catch (Exception e) {
            return null;
        }
    }
}
