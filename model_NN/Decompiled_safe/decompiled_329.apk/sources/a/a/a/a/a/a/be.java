package a.a.a.a.a.a;

import java.nio.ByteBuffer;
import javax.net.ssl.SSLException;

public class be implements z {
    byte[] buffer;

    protected be() {
    }

    public void A(byte[] bArr) {
        if (this.buffer != null) {
            throw new ba((byte) 80, new SSLException("Attempt to override the data"));
        }
        this.buffer = bArr;
    }

    /* access modifiers changed from: protected */
    public int b(ByteBuffer[] byteBufferArr, int i, int i2) {
        int i3;
        if (this.buffer == null) {
            return 0;
        }
        int length = this.buffer.length;
        int i4 = 0;
        int i5 = i;
        while (true) {
            if (i5 >= i + i2) {
                i3 = i4;
                break;
            }
            int remaining = byteBufferArr[i5].remaining();
            if (length - i4 < remaining) {
                byteBufferArr[i5].put(this.buffer, i4, length - i4);
                i3 = length;
                break;
            }
            byteBufferArr[i5].put(this.buffer, i4, remaining);
            i4 += remaining;
            i5++;
        }
        if (i3 != length) {
            throw new ba((byte) 80, new SSLException("The received application data could not be fully writteninto the destination buffers"));
        }
        this.buffer = null;
        return length;
    }
}
