package a.a.a.a.a.a;

import java.util.Enumeration;
import java.util.Hashtable;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSessionContext;

public class y implements SSLSessionContext {
    private int Wh = 0;
    private long Wi = 0;
    /* access modifiers changed from: private */
    public final Hashtable Wj = new Hashtable();

    private void ed(int i) {
    }

    /* access modifiers changed from: package-private */
    public void a(bk bkVar) {
        if (this.Wh > 0 && this.Wj.size() == this.Wh) {
            ed(1);
        }
        bkVar.bYl = this;
        this.Wj.put(new t(this, bkVar.getId(), null), bkVar);
    }

    public Enumeration getIds() {
        return new au(this);
    }

    public SSLSession getSession(byte[] bArr) {
        return (SSLSession) this.Wj.get(new t(this, bArr, null));
    }

    public int getSessionCacheSize() {
        return this.Wh;
    }

    public int getSessionTimeout() {
        return (int) (this.Wi / 1000);
    }

    public void setSessionCacheSize(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("size < 0");
        }
        this.Wh = i;
        if (i > 0 && this.Wj.size() < i) {
            ed(i - this.Wj.size());
        }
    }

    public void setSessionTimeout(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("seconds < 0");
        }
        this.Wi = (long) (i * 1000);
        Enumeration keys = this.Wj.keys();
        while (keys.hasMoreElements()) {
            bk bkVar = (bk) this.Wj.get(keys.nextElement());
            if (!bkVar.isValid()) {
                this.Wj.remove(bkVar.getId());
            }
        }
    }
}
