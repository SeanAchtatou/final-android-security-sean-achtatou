package a.a.a.a.a.a;

import java.security.SecureRandom;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContextSpi;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSessionContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

public class f extends SSLContextSpi {
    private y lp = new y();
    private y lq = new y();
    protected bf lr;

    public SSLEngine engineCreateSSLEngine() {
        if (this.lr != null) {
            return new bp((bf) this.lr.clone());
        }
        throw new IllegalStateException("SSLContext is not initiallized.");
    }

    public SSLEngine engineCreateSSLEngine(String str, int i) {
        if (this.lr != null) {
            return new bp(str, i, (bf) this.lr.clone());
        }
        throw new IllegalStateException("SSLContext is not initiallized.");
    }

    public SSLSessionContext engineGetClientSessionContext() {
        return this.lp;
    }

    public SSLSessionContext engineGetServerSessionContext() {
        return this.lq;
    }

    public SSLServerSocketFactory engineGetServerSocketFactory() {
        if (this.lr != null) {
            return new ao(this.lr);
        }
        throw new IllegalStateException("SSLContext is not initiallized.");
    }

    public SSLSocketFactory engineGetSocketFactory() {
        if (this.lr != null) {
            return new i(this.lr);
        }
        throw new IllegalStateException("SSLContext is not initiallized.");
    }

    public void engineInit(KeyManager[] keyManagerArr, TrustManager[] trustManagerArr, SecureRandom secureRandom) {
        this.lr = new bf(keyManagerArr, trustManagerArr, secureRandom, this.lp, this.lq);
    }
}
