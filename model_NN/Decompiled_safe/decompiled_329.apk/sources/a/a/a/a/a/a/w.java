package a.a.a.a.a.a;

import java.io.IOException;
import java.io.InputStream;

public final class w extends InputStream {
    private final aw IZ;
    private boolean Ja = false;
    protected ah Jb = new ah(this);
    /* access modifiers changed from: private */
    public byte[] buffer = new byte[this.sm];
    /* access modifiers changed from: private */
    public int jv;
    /* access modifiers changed from: private */
    public int pos;
    /* access modifiers changed from: private */
    public int sm = an.aYP;

    protected w(aw awVar) {
        this.IZ = awVar;
    }

    static /* synthetic */ int a(w wVar, int i) {
        int i2 = wVar.jv - i;
        wVar.jv = i2;
        return i2;
    }

    public int available() {
        return this.jv - this.pos;
    }

    public void close() {
        this.buffer = null;
    }

    /* access modifiers changed from: protected */
    public void kY() {
        this.Ja = true;
    }

    public int read() {
        if (this.buffer == null) {
            throw new IOException("Stream was closed.");
        }
        while (this.pos == this.jv) {
            if (this.Ja) {
                return -1;
            }
            this.IZ.DJ();
        }
        byte[] bArr = this.buffer;
        int i = this.pos;
        this.pos = i + 1;
        return bArr[i] & 255;
    }

    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) {
        int i3 = 0;
        do {
            int read = read();
            if (read != -1) {
                bArr[i + i3] = (byte) read;
                i3++;
                if (available() == 0) {
                    return i3;
                }
            } else if (i3 == 0) {
                return -1;
            } else {
                return i3;
            }
        } while (i3 < i2);
        return i3;
    }

    public long skip(long j) {
        long j2 = 0;
        int available = available();
        long j3 = ((long) available) < j ? (long) available : j;
        while (j2 < j3 && read() != -1) {
            j2++;
        }
        return j2;
    }
}
