package a.a.a.a.a.a;

import java.security.cert.X509Certificate;
import java.util.Vector;
import javax.security.auth.x500.X500Principal;

public class ar extends aj {
    public static final byte bdh = 1;
    public static final byte bdi = 2;
    public static final byte bdj = 3;
    public static final byte bdk = 4;
    final byte[] bdl;
    X500Principal[] bdm;
    private String[] bdn;
    private byte[][] bdo;

    public ar(l lVar, int i) {
        int xb = lVar.xb();
        this.bdl = new byte[xb];
        lVar.read(this.bdl, 0, xb);
        int xc = lVar.xc();
        this.bdm = new X500Principal[xc];
        Vector vector = new Vector();
        int i2 = 0;
        while (i2 < xc) {
            int xc2 = lVar.xc();
            vector.add(new X500Principal(lVar));
            i2 = i2 + 2 + xc2;
        }
        this.bdm = new X500Principal[vector.size()];
        for (int i3 = 0; i3 < this.bdm.length; i3++) {
            this.bdm[i3] = (X500Principal) vector.elementAt(i3);
        }
        this.length = this.bdl.length + 3 + i2;
        if (this.length != i) {
            a((byte) 50, "DECODE ERROR: incorrect CertificateRequest");
        }
    }

    public ar(byte[] bArr, X509Certificate[] x509CertificateArr) {
        if (x509CertificateArr == null) {
            a((byte) 80, "CertificateRequest: array of certificate authority certificates is null");
        }
        this.bdl = bArr;
        this.bdm = new X500Principal[x509CertificateArr.length];
        this.bdo = new byte[x509CertificateArr.length][];
        int i = 0;
        for (int i2 = 0; i2 < x509CertificateArr.length; i2++) {
            this.bdm[i2] = x509CertificateArr[i2].getIssuerX500Principal();
            this.bdo[i2] = this.bdm[i2].getEncoded();
            i += this.bdo[i2].length + 2;
        }
        this.length = bArr.length + 3 + i;
    }

    public String[] BK() {
        if (this.bdn == null) {
            this.bdn = new String[this.bdl.length];
            for (int i = 0; i < this.bdn.length; i++) {
                switch (this.bdl[i]) {
                    case 1:
                        this.bdn[i] = "RSA";
                        break;
                    case 2:
                        this.bdn[i] = "DSA";
                        break;
                    case 3:
                        this.bdn[i] = "DH_RSA";
                        break;
                    case 4:
                        this.bdn[i] = "DH_DSA";
                        break;
                    default:
                        a((byte) 50, "DECODE ERROR: incorrect CertificateRequest");
                        break;
                }
            }
        }
        return this.bdn;
    }

    public void a(l lVar) {
        lVar.b((long) this.bdl.length);
        for (byte g : this.bdl) {
            lVar.g(g);
        }
        int i = 0;
        for (int i2 = 0; i2 < this.bdm.length; i2++) {
            i += this.bdo[i2].length + 2;
        }
        lVar.c((long) i);
        for (int i3 = 0; i3 < this.bdm.length; i3++) {
            lVar.c((long) this.bdo[i3].length);
            lVar.write(this.bdo[i3]);
        }
    }

    public int getType() {
        return 13;
    }
}
