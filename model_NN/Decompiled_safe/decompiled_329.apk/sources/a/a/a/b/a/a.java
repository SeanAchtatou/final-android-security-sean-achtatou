package a.a.a.b.a;

import com.uc.c.ay;

public class a {
    public static byte[] a(byte[] bArr, int i) {
        int i2;
        int i3;
        int i4 = (i / 4) * 3;
        if (i4 == 0) {
            return new byte[0];
        }
        byte[] bArr2 = new byte[i4];
        int i5 = 0;
        int i6 = i;
        while (true) {
            byte b2 = bArr[i6 - 1];
            if (!(b2 == 10 || b2 == 13 || b2 == 32 || b2 == 9)) {
                if (b2 != 61) {
                    break;
                }
                i5++;
            }
            i6--;
        }
        byte b3 = 0;
        int i7 = 0;
        int i8 = 0;
        for (int i9 = 0; i9 < i6; i9++) {
            byte b4 = bArr[i9];
            if (!(b4 == 10 || b4 == 13 || b4 == 32 || b4 == 9)) {
                if (b4 >= 65 && b4 <= 90) {
                    i3 = b4 - ay.aZR;
                } else if (b4 >= 97 && b4 <= 122) {
                    i3 = b4 - 71;
                } else if (b4 >= 48 && b4 <= 57) {
                    i3 = b4 + 4;
                } else if (b4 == 43) {
                    i3 = 62;
                } else if (b4 != 47) {
                    return null;
                } else {
                    i3 = 63;
                }
                b3 = (b3 << 6) | ((byte) i3);
                if (i7 % 4 == 3) {
                    int i10 = i8 + 1;
                    bArr2[i8] = (byte) ((16711680 & b3) >> 16);
                    int i11 = i10 + 1;
                    bArr2[i10] = (byte) ((65280 & b3) >> 8);
                    bArr2[i11] = (byte) (b3 & 255);
                    i8 = i11 + 1;
                }
                i7++;
            }
        }
        if (i5 > 0) {
            int i12 = b3 << (i5 * 6);
            int i13 = i8 + 1;
            bArr2[i8] = (byte) ((16711680 & i12) >> 16);
            if (i5 == 1) {
                i2 = i13 + 1;
                bArr2[i13] = (byte) ((i12 & 65280) >> 8);
            } else {
                i2 = i13;
            }
        } else {
            i2 = i8;
        }
        byte[] bArr3 = new byte[i2];
        System.arraycopy(bArr2, 0, bArr3, 0, i2);
        return bArr3;
    }
}
