package a.b;

import a.a.a.a.a.a.i;
import a.a.a.a.a.a.n;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import org.apache.http.conn.scheme.LayeredSocketFactory;
import org.apache.http.conn.ssl.BrowserCompatHostnameVerifier;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class a implements LayeredSocketFactory {
    public static final X509HostnameVerifier BROWSER_COMPATIBLE_HOSTNAME_VERIFIER = new BrowserCompatHostnameVerifier();
    public static final String SSL = "SSL";
    public static final String SSLV2 = "SSLv2";
    public static final String TLS = "TLS";
    private static final a aAZ = new a();
    private final SSLSocketFactory aBa = new i();
    private X509HostnameVerifier aBb = BROWSER_COMPATIBLE_HOSTNAME_VERIFIER;

    private a() {
    }

    public static a yq() {
        return aAZ;
    }

    public Socket connectSocket(Socket socket, String str, int i, InetAddress inetAddress, int i2, HttpParams httpParams) {
        if (str == null) {
            throw new IllegalArgumentException("Target host may not be null.");
        } else if (httpParams == null) {
            throw new IllegalArgumentException("Parameters may not be null.");
        } else {
            SSLSocket sSLSocket = (SSLSocket) (socket != null ? socket : createSocket());
            if (inetAddress != null || i2 > 0) {
                sSLSocket.bind(new InetSocketAddress(inetAddress, i2 < 0 ? 0 : i2));
            }
            int connectionTimeout = HttpConnectionParams.getConnectionTimeout(httpParams);
            int soTimeout = HttpConnectionParams.getSoTimeout(httpParams);
            sSLSocket.connect(new InetSocketAddress(str, i), connectionTimeout);
            sSLSocket.setSoTimeout(soTimeout);
            try {
                if (!n.sP.contains(str)) {
                    this.aBb.verify(str, sSLSocket);
                }
                return sSLSocket;
            } catch (IOException e) {
                try {
                    sSLSocket.close();
                } catch (Exception e2) {
                }
                throw e;
            }
        }
    }

    public Socket createSocket() {
        return (SSLSocket) this.aBa.createSocket();
    }

    public Socket createSocket(Socket socket, String str, int i, boolean z) {
        SSLSocket sSLSocket = (SSLSocket) this.aBa.createSocket(socket, str, i, z);
        if (!n.sP.contains(str)) {
            this.aBb.verify(str, sSLSocket);
        } else {
            n.sP.add(sSLSocket.getInetAddress().getHostName());
        }
        return sSLSocket;
    }

    public X509HostnameVerifier getHostnameVerifier() {
        return this.aBb;
    }

    public boolean isSecure(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null.");
        } else if (!(socket instanceof SSLSocket)) {
            throw new IllegalArgumentException("Socket not created by this factory.");
        } else if (!socket.isClosed()) {
            return true;
        } else {
            throw new IllegalArgumentException("Socket is closed.");
        }
    }

    public void setHostnameVerifier(X509HostnameVerifier x509HostnameVerifier) {
        if (x509HostnameVerifier == null) {
            throw new IllegalArgumentException("Hostname verifier may not be null");
        }
        this.aBb = x509HostnameVerifier;
    }
}
