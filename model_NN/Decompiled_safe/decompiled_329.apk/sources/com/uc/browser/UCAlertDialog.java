package com.uc.browser;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.uc.browser.UCR;
import com.uc.h.e;

public class UCAlertDialog extends Dialog {
    private static final int aDa = -1;
    /* access modifiers changed from: private */
    public static Drawable aDb;
    /* access modifiers changed from: private */
    public static Drawable aDc;
    private RelativeLayout aDd;
    /* access modifiers changed from: private */
    public LinearLayout aDe;
    /* access modifiers changed from: private */
    public TextView aDf;
    /* access modifiers changed from: private */
    public View aDg;
    /* access modifiers changed from: private */
    public LinearLayout aDh;

    public class Builder {
        private static int tH = 200;
        private int dv;
        private Context mContext;
        UCAlertDialog tD;
        private boolean tE;
        private boolean tF;
        private int tG;

        public Builder(Context context) {
            this(context, false);
        }

        public Builder(Context context, boolean z) {
            this.tE = true;
            this.tF = false;
            this.dv = 21;
            this.tG = 18;
            this.mContext = context;
            this.tD = new UCAlertDialog(context, z);
            tH = (int) context.getResources().getDimension(R.dimen.f302dialog_content_max_height);
        }

        private Builder a(String str, final int i, final DialogInterface.OnClickListener onClickListener) {
            int i2;
            if (this.tD.aDh == null) {
                LinearLayout unused = this.tD.aDh = new LinearLayout(this.mContext);
                this.tD.aDh.setOrientation(0);
                this.tD.aDh.setGravity(17);
            }
            UCButton uCButton = new UCButton(this.mContext);
            uCButton.setPadding(0, 0, 0, 0);
            uCButton.setText(str);
            uCButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (onClickListener != null) {
                        onClickListener.onClick(Builder.this.tD, i);
                    }
                    Builder.this.tD.dismiss();
                }
            });
            this.tD.aDh.addView(uCButton, new LinearLayout.LayoutParams((int) this.tD.getContext().getResources().getDimension(R.dimen.f299dialog_button_width), (int) this.tD.getContext().getResources().getDimension(R.dimen.f300dialog_button_height), 1.0f));
            int childCount = this.tD.aDh.getChildCount();
            int kp = e.Pb().kp(R.dimen.f301dialog_button_margin);
            switch (childCount) {
                case 0:
                case 1:
                case 2:
                    i2 = kp;
                    break;
                case 3:
                    i2 = kp / 3;
                    break;
                default:
                    i2 = 0;
                    break;
            }
            for (int i3 = 0; i3 < childCount; i3++) {
                View childAt = this.tD.aDh.getChildAt(i3);
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) childAt.getLayoutParams();
                layoutParams.rightMargin = i2;
                layoutParams.leftMargin = i2;
                if (i3 == 0) {
                    layoutParams.leftMargin = 0;
                } else if (i3 == childCount - 1) {
                    layoutParams.rightMargin = 0;
                }
                childAt.setLayoutParams(layoutParams);
            }
            return this;
        }

        public Builder L(String str) {
            if (this.tD.aDf == null) {
                TextView unused = this.tD.aDf = new TextView(this.mContext);
            }
            this.tD.aDf.setText(str);
            this.tD.aDf.setTextColor(e.Pb().getColor(78));
            return this;
        }

        public Builder M(String str) {
            TextView textView = new TextView(this.mContext);
            textView.setTextColor(e.Pb().getColor(78));
            textView.setTextSize((float) this.tG);
            textView.setText(str);
            c(textView);
            return this;
        }

        public Builder a(int i, int i2, DialogInterface.OnClickListener onClickListener) {
            return a(this.mContext.getResources().getStringArray(i), i2, onClickListener);
        }

        public Builder a(int i, DialogInterface.OnClickListener onClickListener) {
            return a(this.mContext.getResources().getString(i), onClickListener);
        }

        public Builder a(String str, DialogInterface.OnClickListener onClickListener) {
            return a(str, -1, onClickListener);
        }

        public Builder a(String[] strArr, int i, DialogInterface.OnClickListener onClickListener) {
            return a(strArr, i, true, onClickListener);
        }

        public Builder a(String[] strArr, int i, boolean z, final DialogInterface.OnClickListener onClickListener) {
            ListView listView = new ListView(this.mContext);
            listView.setChoiceMode(1);
            listView.setVerticalFadingEdgeEnabled(false);
            listView.setSelector(e.Pb().getDrawable(UCR.drawable.aXb));
            listView.setDivider(e.Pb().getDrawable(UCR.drawable.aUo));
            listView.setDividerHeight(2);
            listView.setCacheColorHint(0);
            listView.setAdapter((ListAdapter) new ArrayAdapter(this.mContext, z ? R.layout.f940single_choice_list_item : R.layout.f941single_choice_list_item_no_checkmark, strArr));
            listView.measure(View.MeasureSpec.makeMeasureSpec(this.tD.getWindow().getWindowManager().getDefaultDisplay().getWidth(), Integer.MIN_VALUE), View.MeasureSpec.makeMeasureSpec(tH, Integer.MIN_VALUE));
            if (i >= 0 && i < strArr.length) {
                listView.setItemChecked(i, true);
            }
            if (onClickListener != null) {
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
                        onClickListener.onClick(Builder.this.tD, i);
                    }
                });
            }
            c(listView);
            return this;
        }

        public Builder a(String[] strArr, boolean[] zArr, final DialogInterface.OnMultiChoiceClickListener onMultiChoiceClickListener) {
            ListView listView = new ListView(this.mContext);
            listView.setChoiceMode(2);
            listView.setCacheColorHint(0);
            listView.setVerticalFadingEdgeEnabled(false);
            listView.setSelector(e.Pb().getDrawable(UCR.drawable.aXb));
            listView.setDivider(e.Pb().getDrawable(UCR.drawable.aUo));
            listView.setCacheColorHint(0);
            listView.setAdapter((ListAdapter) new ArrayAdapter(this.mContext, (int) R.layout.f940single_choice_list_item, strArr));
            listView.measure(View.MeasureSpec.makeMeasureSpec(this.tD.getWindow().getWindowManager().getDefaultDisplay().getWidth(), Integer.MIN_VALUE), View.MeasureSpec.makeMeasureSpec(tH, Integer.MIN_VALUE));
            if (zArr != null) {
                for (int i = 0; i < zArr.length; i++) {
                    listView.setItemChecked(i, zArr[i]);
                }
            }
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView adapterView, View view, int i, long j) {
                    onMultiChoiceClickListener.onClick(Builder.this.tD, i, ((ListView) adapterView).getCheckedItemPositions().get(i));
                }
            });
            c(listView);
            return this;
        }

        public Builder aG(int i) {
            return M(this.mContext.getResources().getString(i));
        }

        public Builder aH(int i) {
            return L(this.mContext.getResources().getString(i));
        }

        public Builder b(int i, DialogInterface.OnClickListener onClickListener) {
            return b(this.mContext.getResources().getString(i), onClickListener);
        }

        public Builder b(String str, DialogInterface.OnClickListener onClickListener) {
            return a(str, -3, onClickListener);
        }

        public Builder c(int i, DialogInterface.OnClickListener onClickListener) {
            return c(this.mContext.getResources().getString(i), onClickListener);
        }

        public Builder c(View view) {
            View unused = this.tD.aDg = view;
            return this;
        }

        public Builder c(String str, DialogInterface.OnClickListener onClickListener) {
            return a(str, -2, onClickListener);
        }

        public UCAlertDialog fh() {
            this.tD.setCancelable(this.tE);
            int kp = e.Pb().kp(R.dimen.f308dialog_layout_margin);
            if (this.tD.aDf != null) {
                this.tD.aDf.setTextSize((float) this.dv);
                this.tD.aDf.setCompoundDrawablePadding(3);
                this.tD.aDf.setCompoundDrawablesWithIntrinsicBounds(UCAlertDialog.aDb, (Drawable) null, (Drawable) null, (Drawable) null);
                this.tD.aDe.addView(this.tD.aDf, new LinearLayout.LayoutParams(-1, -2));
                MyImageView myImageView = new MyImageView(this.mContext);
                myImageView.setBackgroundDrawable(UCAlertDialog.aDc);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, 2);
                layoutParams.setMargins(0, 5, 0, 0);
                this.tD.aDe.addView(myImageView, layoutParams);
            }
            if (this.tD.aDg != null) {
                LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
                layoutParams2.setMargins(0, this.tD.aDf != null ? kp : 0, 0, 0);
                this.tD.aDg.setPadding(10, 0, 10, 0);
                if (this.tD.aDg instanceof ListView) {
                    layoutParams2.height = this.tD.aDg.getMeasuredHeight();
                }
                this.tD.aDe.addView(this.tD.aDg, layoutParams2);
            }
            if (this.tD.aDh != null) {
                LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
                layoutParams3.setMargins(0, kp, 0, 0);
                layoutParams3.gravity = 17;
                this.tD.aDe.addView(this.tD.aDh, layoutParams3);
                this.tD.aDh.requestFocus();
            }
            this.tF = true;
            return this.tD;
        }

        public void setCancelable(boolean z) {
            this.tE = z;
        }

        public void show() {
            if (this.tF) {
                this.tD.show();
            } else {
                fh().show();
            }
        }
    }

    public class MyImageView extends View {
        private Drawable aBW;

        public MyImageView(Context context) {
            super(context);
        }

        public void onDraw(Canvas canvas) {
            this.aBW.setBounds(0, 0, getWidth(), getHeight());
            this.aBW.draw(canvas);
        }

        public void setBackgroundDrawable(Drawable drawable) {
            this.aBW = drawable;
        }
    }

    public UCAlertDialog(Context context) {
        this(context, false);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UCAlertDialog(Context context, boolean z) {
        super(context, !z ? R.style.f1575context_menu : R.style.f1577quit_dialog);
        yH();
        requestWindowFeature(1);
        yI();
    }

    private void yH() {
        aDc = e.Pb().getDrawable(UCR.drawable.aUo);
        aDb = e.Pb().getDrawable(UCR.drawable.aUJ);
    }

    private void yI() {
        this.aDd = new RelativeLayout(getContext()) {
            int abD;
            int abE;

            /* access modifiers changed from: protected */
            public void onLayout(boolean z, int i, int i2, int i3, int i4) {
                super.onLayout(z, i, i2, i3, i4);
                if (!z) {
                    return;
                }
                if (this.abD != i3 - i || this.abE != i4 - i2) {
                    this.abD = i3 - i;
                    this.abE = i4 - i2;
                    post(new Runnable() {
                        public void run() {
                            UCAlertDialog.this.yJ();
                        }
                    });
                }
            }
        };
        this.aDe = new LinearLayout(getContext());
        this.aDe.setOrientation(1);
        e Pb = e.Pb();
        this.aDe.setBackgroundDrawable(Pb.getDrawable(UCR.drawable.aUm));
        this.aDe.setPadding(Pb.kp(R.dimen.f305dialog_padding_left), Pb.kp(R.dimen.f304dialog_padding_top), Pb.kp(R.dimen.f306dialog_padding_right), Pb.kp(R.dimen.f307dialog_padding_bottom));
    }

    /* access modifiers changed from: private */
    public void yJ() {
        int i;
        int kp;
        int i2;
        int i3;
        int i4;
        e Pb = e.Pb();
        Display defaultDisplay = getWindow().getWindowManager().getDefaultDisplay();
        if (defaultDisplay.getWidth() > defaultDisplay.getHeight()) {
            int kp2 = Pb.kp(R.dimen.f318dialog_frame_padding_left_land);
            int kp3 = Pb.kp(R.dimen.f319dialog_frame_padding_right_land);
            int kp4 = Pb.kp(R.dimen.f317dialog_frame_padding_top_land);
            int kp5 = Pb.kp(R.dimen.f320dialog_frame_padding_bottom_land);
            i = kp2;
            kp = Pb.kp(R.dimen.f316dialog_content_max_height_land);
            int i5 = kp3;
            i2 = kp5;
            i3 = kp4;
            i4 = i5;
        } else {
            int kp6 = Pb.kp(R.dimen.f310dialog_frame_padding_left);
            int kp7 = Pb.kp(R.dimen.f311dialog_frame_padding_right);
            int kp8 = Pb.kp(R.dimen.f309dialog_frame_padding_top);
            int kp9 = Pb.kp(R.dimen.f312dialog_frame_padding_bottom);
            i = kp6;
            kp = Pb.kp(R.dimen.f302dialog_content_max_height);
            int i6 = kp7;
            i2 = kp9;
            i3 = kp8;
            i4 = i6;
        }
        this.aDd.setPadding(i, i3, i4, i2);
        this.aDe.setBackgroundDrawable(Pb.getDrawable(UCR.drawable.aUm));
        this.aDe.setPadding(Pb.kp(R.dimen.f305dialog_padding_left), Pb.kp(R.dimen.f304dialog_padding_top), Pb.kp(R.dimen.f306dialog_padding_right), Pb.kp(R.dimen.f307dialog_padding_bottom));
        if (this.aDg != null && !(this.aDg instanceof TextView) && this.aDg.getLayoutParams() != null) {
            int width = defaultDisplay.getWidth();
            if (this.aDf == null) {
                kp += Pb.kp(R.dimen.f303dialog_content_height_fix);
            }
            this.aDg.measure(View.MeasureSpec.makeMeasureSpec(width, Integer.MIN_VALUE), View.MeasureSpec.makeMeasureSpec(this.aDh == null ? Pb.kp(R.dimen.f303dialog_content_height_fix) + kp : kp, Integer.MIN_VALUE));
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.aDg.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = new LinearLayout.LayoutParams(-1, -2);
            }
            layoutParams.height = this.aDg.getMeasuredHeight();
            this.aDg.setLayoutParams(layoutParams);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() != 84) {
            return super.dispatchKeyEvent(keyEvent);
        }
        dismiss();
        ((InputMethodManager) getContext().getSystemService("input_method")).hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        return true;
    }

    public View findViewById(int i) {
        if (this.aDe == null) {
            return null;
        }
        return this.aDe.findViewById(i);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
    }

    public void show() {
        e Pb = e.Pb();
        this.aDd.setPadding(Pb.kp(R.dimen.f310dialog_frame_padding_left), Pb.kp(R.dimen.f309dialog_frame_padding_top), Pb.kp(R.dimen.f311dialog_frame_padding_right), Pb.kp(R.dimen.f312dialog_frame_padding_bottom));
        this.aDd.addView(this.aDe, new ViewGroup.LayoutParams(-1, -2));
        setContentView(this.aDd);
        if (this.aDh != null) {
            this.aDh.requestFocus();
        }
        super.show();
    }
}
