package com.uc.browser;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import b.a.a.f;
import com.uc.browser.UCR;
import com.uc.h.a;
import com.uc.h.e;

public class ViewMainpage extends Workspace implements a {
    ViewMainpageNavi ep;
    BookmarkPageView eq;
    ChannelPageView er;
    OnScreenChangeListener es;

    public interface OnScreenChangeListener {
        void s(int i);
    }

    public ViewMainpage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ViewMainpage(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.b(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, int):int
      com.uc.browser.ModelBrowser.b(android.content.Context, int):android.app.Dialog
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, android.content.Context):android.app.Dialog
      com.uc.browser.ModelBrowser.b(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, boolean):boolean
      com.uc.browser.ModelBrowser.b(boolean, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean A(int i) {
        if (!(uX() == 1 || i != 1 || ModelBrowser.gD() == null)) {
            ModelBrowser.gD().b(false, false);
        }
        boolean A = super.A(i);
        if (A) {
            if (i == 0) {
                this.eq.Ph();
                uR().kj(1);
                f.l(1, f.atW);
            } else {
                uR().kj(0);
                if (i == 2) {
                    f.l(1, f.atX);
                }
            }
            if (this.es != null) {
                this.es.s(i);
            }
        }
        return A;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.b(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, int):int
      com.uc.browser.ModelBrowser.b(android.content.Context, int):android.app.Dialog
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, android.content.Context):android.app.Dialog
      com.uc.browser.ModelBrowser.b(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, boolean):boolean
      com.uc.browser.ModelBrowser.b(boolean, boolean):void */
    public void B(int i) {
        if (i == 1 && ModelBrowser.gD() != null) {
            ModelBrowser.gD().b(false, false);
        }
        super.B(i);
        if (i == 0) {
            this.eq.Ph();
            uR().kj(1);
        } else {
            uR().kj(0);
        }
        if (this.es != null) {
            this.es.s(i);
        }
    }

    public void P() {
        this.ep.P();
    }

    public void a() {
        this.ep = (ViewMainpageNavi) findViewById(R.id.f776mainpage_navi);
        this.eq = (BookmarkPageView) findViewById(R.id.f775mainpage_bookmark);
        this.eq.a(uR());
        this.er = (ChannelPageView) findViewById(R.id.f777mainpage_channel);
        k();
        e.Pb().a(this);
    }

    public void a(OnScreenChangeListener onScreenChangeListener) {
        this.es = onScreenChangeListener;
    }

    public void cA() {
        if (this.ep != null) {
            this.ep.ds();
        }
    }

    public void cB() {
        if (this.eq != null) {
            this.eq.Ph();
        }
    }

    public void cC() {
        if (this.er != null && this.er.Dr() != null) {
            this.er.Dr().ds();
        }
    }

    public BuzLayout cD() {
        if (this.er != null) {
            return this.er.Dr();
        }
        return null;
    }

    public String cE() {
        if (this.er == null) {
            return null;
        }
        return this.er.bi();
    }

    public ViewMainpageNavi cy() {
        return this.ep;
    }

    public WebViewJUC cz() {
        return this.ep.ry();
    }

    public void k() {
        e Pb = e.Pb();
        setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.background));
        p(Pb.getDrawable(UCR.drawable.aWs));
        q(Pb.getDrawable(UCR.drawable.aWr));
        this.lastIndex = -1;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 1 && ModelBrowser.gD() != null) {
            ModelBrowser.gD().aS(134);
        }
        return super.onTouchEvent(motionEvent);
    }
}
