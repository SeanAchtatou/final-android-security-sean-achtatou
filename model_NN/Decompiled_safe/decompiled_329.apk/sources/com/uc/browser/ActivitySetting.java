package com.uc.browser;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.uc.c.au;
import com.uc.f.a;
import com.uc.f.g;
import com.uc.plugin.ad;

public class ActivitySetting extends ActivityWithUCMenu {
    public static final int Ij = 2;
    private static final String Ik = "K";
    private static final String ds = "k";
    public static final int lu = 1;
    a Ii;

    private void a(Intent intent) {
        Bundle extras;
        if (intent != null && (extras = intent.getExtras()) != null) {
            ad.h(this, extras.getString(ActivityChooseFile.uE) + au.aGF + extras.getString(ActivityChooseFile.uF));
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        switch (i) {
            case 1:
                a(intent);
                break;
            case 2:
                if (intent != null) {
                    String stringExtra = intent.getStringExtra(ActivityChooseFile.uE);
                    this.Ii.D(stringExtra + au.aGF + intent.getStringExtra(ActivityChooseFile.uF) + au.aGF);
                    break;
                }
                break;
        }
        if (i2 == 77) {
            finish();
            return;
        }
        if (i2 == -1) {
            finish();
        }
        super.onActivityResult(i, i2, intent);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ActivityBrowser.g(this);
        requestWindowFeature(1);
        g.Jn().Jo();
        RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(this).inflate((int) R.layout.f942title_container, (ViewGroup) null);
        setContentView(relativeLayout);
        ((TextView) relativeLayout.findViewById(R.id.f654Browser_TitleBar)).setText((int) R.string.f1002activity_title_preference);
        this.Ii = new a(this);
        this.Ii.setFocusable(true);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(3, R.id.f654Browser_TitleBar);
        relativeLayout.addView(this.Ii, layoutParams);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i != 84) {
            return super.onKeyUp(i, keyEvent);
        }
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a((int) ModelBrowser.zJ, this);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        g.Jn().update();
        int JI = g.Jn().JI();
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(57, Integer.valueOf(JI));
            ModelBrowser.gD().aS(135);
            ModelBrowser.gD().aS(ModelBrowser.zY);
            ModelBrowser.gD().aS(ModelBrowser.Ac);
            if (this.Ii.lz) {
                ModelBrowser.gD().aS(ModelBrowser.zQ);
            }
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        int i = bundle.getInt(ds);
        if (i != -1) {
            this.Ii.m(i, bundle.getInt("K"));
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt(ds, this.Ii.cQ());
        bundle.putInt("K", this.Ii.cR());
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        this.Ii.requestFocus();
        if (z && ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(this);
        }
        if (z && ActivityBrowser.Fz()) {
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
            }
            ActivityBrowser.e(this);
        }
    }

    public void setTitle(CharSequence charSequence) {
        if (charSequence == null || charSequence.equals("")) {
            ((TextView) findViewById(R.id.f654Browser_TitleBar)).setText((int) R.string.f1002activity_title_preference);
        } else {
            ((TextView) findViewById(R.id.f654Browser_TitleBar)).setText(charSequence);
        }
    }
}
