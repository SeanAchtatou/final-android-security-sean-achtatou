package com.uc.browser;

import android.content.Context;
import com.uc.browser.WebsiteSearchSchDialog;
import com.uc.browser.WebsiteSearchWebDialog;
import com.uc.e.a.j;
import com.uc.e.a.x;

public class URLSearchBarManager {
    private x aux = new x() {
        public void ay(String str) {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().a(119, 1, str);
            }
        }

        public void kP() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().a(119, 0, (Object) null);
            }
        }

        public void kQ() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(120);
            }
        }

        public void kR() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(128);
            }
        }

        public void onCancel() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(12);
            }
        }
    };
    private j auy = new j() {
        public void ay(String str) {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().d((String) null, 1);
            }
        }

        public void kQ() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().W(null);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
         arg types: [?, boolean]
         candidates:
          com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
          com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
          com.uc.browser.ModelBrowser.a(int, long):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
          com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
          com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
          com.uc.browser.ModelBrowser.a(int, java.lang.Object):void */
        public void rE() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().a((int) ModelBrowser.Aa, (Object) true);
            }
        }
    };
    private WebsiteSearchSchDialog.SearchBarListener auz = new WebsiteSearchSchDialog.SearchBarListener() {
        public void E(String str) {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(122);
                ModelBrowser.gD().a(39, 2, str);
                ModelBrowser.gD().a(127, str);
                ModelBrowser.gD().a(123, 1, (Object) null);
            }
        }

        public void dp() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().a(123, 0, (Object) null);
            }
        }

        public void onCancel() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(122);
            }
        }
    };
    private WebsiteSearchWebDialog.URLBarListener cs = new WebsiteSearchWebDialog.URLBarListener() {
        public void bS(String str) {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(121);
                ModelBrowser.gD().a(11, str);
                ModelBrowser.gD().a(123, 1, (Object) null);
            }
        }

        public void dp() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aK(0);
            }
        }

        public void onCancel() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().gu();
            }
        }
    };

    public URLSearchBarManager(Context context) {
    }

    public x wU() {
        return this.aux;
    }

    public j wV() {
        return this.auy;
    }

    public WebsiteSearchSchDialog.SearchBarListener wW() {
        return this.auz;
    }

    public WebsiteSearchWebDialog.URLBarListener wX() {
        return this.cs;
    }
}
