package com.uc.browser;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ListAdapter;
import android.widget.ListView;
import b.a.a.f;
import b.b;
import com.uc.a.e;
import com.uc.browser.UCR;
import com.uc.c.w;
import com.uc.e.ac;
import com.uc.e.h;
import com.uc.e.z;
import java.util.Vector;

public class ActivitySelector extends Activity implements h {
    public static final String aSI = "select";
    private ac aSJ;
    private ac aSK;
    private ListView aSL;
    private BarLayout um;

    private boolean At() {
        Vector qc = e.nR().nZ().qc();
        if (qc == null || true == qc.isEmpty()) {
            return false;
        }
        AdapterSearchEngineList adapterSearchEngineList = new AdapterSearchEngineList();
        adapterSearchEngineList.i(qc);
        this.aSL = (ListView) findViewById(R.id.f656selector_list);
        this.aSL.setDivider(new ColorDrawable(com.uc.h.e.Pb().getColor(108)));
        this.aSL.setDividerHeight(1);
        this.aSL.setSelector(com.uc.h.e.Pb().getDrawable(UCR.drawable.aXb));
        this.aSL.setAdapter((ListAdapter) adapterSearchEngineList);
        this.aSL.setChoiceMode(1);
        this.aSL.setItemChecked(0, true);
        this.aSL.requestFocus();
        return true;
    }

    private void Au() {
        this.um = (BarLayout) findViewById(R.id.f655controlbar);
        Resources resources = getResources();
        com.uc.h.e Pb = com.uc.h.e.Pb();
        int kp = Pb.kp(R.dimen.f183controlbar_item_width_2);
        int kp2 = Pb.kp(R.dimen.f177controlbar_height);
        int dimension = (int) resources.getDimension(R.dimen.f188controlbar_text_size);
        int dimension2 = (int) resources.getDimension(R.dimen.f187controlbar_item_paddingTop);
        this.um.HN.aw(kp, kp2);
        this.aSJ = new ac(R.string.f965confirm, 0, 0);
        this.aSJ.bB(0, 0);
        this.aSJ.aV(dimension);
        this.aSJ.setText((String) resources.getText(R.string.f965confirm));
        this.aSJ.setPadding(0, dimension2, 0, 4);
        this.aSJ.setTextColor(com.uc.h.e.Pb().getColor(10));
        this.um.a(this.aSJ);
        this.aSK = new ac(R.string.cancel, 0, 0);
        this.aSK.bB(0, 0);
        this.aSK.aV(dimension);
        this.aSK.setText((String) resources.getText(R.string.cancel));
        this.aSK.setPadding(0, dimension2, 0, 4);
        this.aSK.setTextColor(com.uc.h.e.Pb().getColor(10));
        this.um.a(this.aSK);
        this.um.kJ();
        this.um.b(this);
    }

    public void b(z zVar, int i) {
        switch (i) {
            case R.string.f965confirm /*2131296271*/:
                int checkedItemPosition = this.aSL.getCheckedItemPosition();
                try {
                    String qa = e.nR().qa();
                    if (qa != null && !qa.startsWith(w.Os)) {
                        e.nR().t((byte) checkedItemPosition);
                        ModelBrowser.gD().a(11, b.acz);
                        String lh = e.nR().lh();
                        if (lh != null && lh.length() > 0) {
                            f.l(2, lh);
                        }
                        finish();
                    }
                } catch (Exception e) {
                }
                finish();
                return;
            case R.string.cancel /*2131296272*/:
                setResult(0);
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 86 && i2 == -1) {
            setResult(0);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ActivityBrowser.g(this);
        requestWindowFeature(1);
        setContentView((int) R.layout.f872activity_selector);
        findViewById(R.id.f656selector_list).setBackgroundColor(com.uc.h.e.Pb().getColor(83));
        if (!At()) {
            setResult(0);
            finish();
        }
        Au();
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i != 84) {
            return super.onKeyUp(i, keyEvent);
        }
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a((int) ModelBrowser.zJ, this);
        }
        return true;
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z && ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(this);
        }
        if (z && ActivityBrowser.Fz()) {
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
            }
            ActivityBrowser.e(this);
        }
    }
}
