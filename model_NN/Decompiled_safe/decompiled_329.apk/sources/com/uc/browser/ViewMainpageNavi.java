package com.uc.browser;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;
import b.a.a.a.f;
import b.b;
import com.uc.a.n;
import com.uc.c.bk;
import com.uc.e.b.d;
import com.uc.e.h;
import com.uc.e.w;
import com.uc.e.z;
import com.uc.h.a;
import com.uc.h.e;
import java.io.InputStream;
import java.util.Vector;

public class ViewMainpageNavi extends RelativeLayout implements h, w, a {
    static Matrix Tu = new Matrix();
    private boolean Ts = false;
    /* access modifiers changed from: private */
    public WebViewJUCMainpage Tt;
    private n p = new n() {
        Bitmap VE = null;

        /* access modifiers changed from: protected */
        public Bitmap a(int[] iArr, boolean z, boolean z2, String str, int i) {
            Paint paint = new Paint();
            paint.setColor(z ? e.Pb().getColor(49) : e.Pb().getColor(48));
            paint.setAntiAlias(true);
            paint.setTextSize((float) e.Pb().kp(R.dimen.f228bookmark_tabfont));
            Bitmap bitmap = this.VE;
            if (bitmap == null || bitmap.isRecycled() || bitmap.getWidth() != iArr[0]) {
                bitmap = Bitmap.createBitmap(iArr[0], iArr[1], Bitmap.Config.ARGB_8888);
                this.VE = bitmap;
            }
            new Canvas(bitmap).drawText(str, 10.0f, (((float) iArr[1]) - paint.ascent()) / 2.0f, paint);
            return bitmap;
        }

        public boolean a(Canvas canvas, int[] iArr) {
            if (ViewMainpageNavi.this.Tt != null) {
                return ViewMainpageNavi.this.Tt.a(canvas, iArr);
            }
            return false;
        }

        public boolean a(bk bkVar, int[] iArr, String str, int i, boolean z, boolean z2, int i2, int i3, int i4) {
            Canvas canvas = bkVar.bkB.bmb;
            canvas.save();
            canvas.translate((float) (bkVar.left + i2), (float) (bkVar.top + i3));
            ViewMainpageNavi.this.Tt.a(canvas, iArr, str, i, z, z2, i2, i3, i4);
            canvas.restore();
            return true;
        }

        public boolean a(int[] iArr, boolean z, Canvas canvas) {
            int length = iArr.length / 4;
            Rect[] rectArr = new Rect[length];
            for (int i = 0; i < length; i++) {
                rectArr[i] = new Rect(iArr[i * 4] - 1, iArr[(i * 4) + 1] - 1, iArr[(i * 4) + 2] + 1, iArr[(i * 4) + 3] + 1);
            }
            e Pb = e.Pb();
            new d(rectArr, Pb.kp(R.dimen.f535webwidget_bold_stroke), Pb.kp(R.dimen.f537webwidget_frame_corner), Pb.getColor(114), Pb.getColor(122)).draw(canvas);
            return true;
        }

        public int aB() {
            return e.Pb().kp(R.dimen.f211navigation_channel_item_height);
        }

        public int aC() {
            return (com.uc.a.e.nR().bw(com.uc.a.h.afA).equals(com.uc.a.e.RD) || ViewMainpageNavi.this.getWidth() - ViewMainpageNavi.this.getHeight() > 20) ? e.Pb().kp(R.dimen.f193mynavi_bar_height) : e.Pb().kp(R.dimen.f194mynavi_bar_height_double);
        }

        public void aT() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(89);
            }
        }

        public InputStream ag(String str) {
            if (ViewMainpageNavi.this.Tt == null || ModelBrowser.gD() == null) {
                return null;
            }
            return ModelBrowser.gD().ag(str);
        }

        public boolean bL(String str) {
            if (ViewMainpageNavi.this.Tt == null) {
                return false;
            }
            com.uc.a.e.nR().ob().r(str, ViewMainpageNavi.this.Tt.getUrl());
            return false;
        }

        public boolean d(Vector vector, boolean z) {
            if (ViewMainpageNavi.this.Tt == null) {
                return false;
            }
            if (z) {
                ViewMainpageNavi.this.Tt.D(vector);
            } else {
                ViewMainpageNavi.this.Tt.B(vector);
            }
            return false;
        }

        public boolean dY(int i) {
            if (ViewMainpageNavi.this.Tt == null || ModelBrowser.gD() == null) {
                return false;
            }
            ModelBrowser.gD().a(13, Integer.valueOf(i));
            return false;
        }

        public void dZ(int i) {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().a(85, Integer.valueOf(i));
            }
        }

        public void g(boolean z) {
        }

        public void gB() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(86);
            }
        }

        public void gC() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(48);
            }
        }

        public int getOrientation() {
            try {
                if (ViewMainpageNavi.this.Tt != null) {
                    return ViewMainpageNavi.this.Tt.getOrientation();
                }
            } catch (Exception e) {
            }
            return b.ORIENTATION_PORTRAIT;
        }

        public f is() {
            if (ViewMainpageNavi.this.Tt == null || ModelBrowser.gD() == null) {
                return null;
            }
            return ModelBrowser.gD().is();
        }

        public boolean j(String[] strArr) {
            if (strArr.length > 4 && ModelBrowser.gD() != null) {
                ModelBrowser.gD().a(26, strArr);
                g(false);
            }
            return false;
        }

        public boolean qW() {
            if (ViewMainpageNavi.this.Tt == null) {
                return false;
            }
            ViewMainpageNavi.this.Tt.postInvalidate();
            return false;
        }

        public int qX() {
            if (ViewMainpageNavi.this.Tt != null) {
                return ViewMainpageNavi.this.Tt.Qk();
            }
            return 0;
        }

        public int qY() {
            if (ViewMainpageNavi.this.Tt != null) {
                return ViewMainpageNavi.this.Tt.Ql();
            }
            return 0;
        }

        public void re() {
            ModelBrowser.gD().aS(131);
        }

        public boolean se() {
            if (ModelBrowser.gD() == null) {
                return false;
            }
            ModelBrowser.gD().aS(49);
            return false;
        }

        public boolean sf() {
            if (ModelBrowser.gD() == null) {
                return false;
            }
            ModelBrowser.gD().aS(54);
            return false;
        }

        public f sg() {
            try {
                return f.a(ViewMainpageNavi.this.getResources(), R.drawable.f546camera);
            } catch (Exception e) {
                return null;
            }
        }

        public void sh() {
            if (ViewMainpageNavi.this.Tt != null) {
                ViewMainpageNavi.this.Tt.postInvalidate();
                com.uc.a.e.nR().c((f) null);
            }
        }

        public boolean shouldOverrideUrlLoading(String str) {
            String str2;
            if (ModelBrowser.gD() == null) {
                return false;
            }
            if (str.startsWith(b.acw)) {
                str.substring(15);
                str2 = "ext:press_button:maincontent:";
            } else {
                str2 = str;
            }
            ModelBrowser.gD().a(11, str2);
            ViewMainpageNavi.this.ap();
            return false;
        }

        public void si() {
            try {
                if (ViewMainpageNavi.this.Tt != null) {
                    ViewMainpageNavi.this.Tt.si();
                }
            } catch (Exception e) {
            }
        }

        public boolean sj() {
            return ViewMainpageNavi.this.Tt.sj();
        }

        public void sk() {
            if (ViewMainpageNavi.this.Tt != null) {
                ViewMainpageNavi.this.Tt.Qq();
            }
        }

        public boolean w(Object obj) {
            if (ModelBrowser.gD() == null) {
                return false;
            }
            ModelBrowser.gD().a(72, obj);
            return false;
        }
    };
    private ViewWebSchMainPage vE;

    static {
        Tu.postRotate(90.0f);
    }

    public ViewMainpageNavi(Context context) {
        super(context);
        d(context);
    }

    public ViewMainpageNavi(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        d(context);
    }

    public void P() {
        if (this.Tt != null) {
            this.Tt.P();
            this.Tt.a(this.p);
        }
    }

    public void a(z zVar, int i) {
        Toast.makeText(getContext(), "long click " + zVar.getClass().toString() + " child " + i, 0).show();
    }

    public void ap() {
        if (this.Tt != null) {
            this.Tt.ap();
            com.uc.a.e.nR().a((Picture) null);
        }
    }

    public void b(z zVar, int i) {
        ModelBrowser.gD().a(11, ((com.uc.e.a.b) zVar).aI(i).abL);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.uc.browser.ViewMainpageNavi, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void d(Context context) {
        this.vE = (ViewWebSchMainPage) LayoutInflater.from(context).inflate((int) R.layout.f912mainpage_navi, (ViewGroup) this, true).findViewById(R.id.f783input_url_bar);
        this.Tt = (WebViewJUCMainpage) findViewById(R.id.f784navi);
        this.Ts = this.Tt.b(com.uc.c.w.Pa, null, this.p, true, null);
        this.Tt.bringToFront();
        e.Pb().a(this);
    }

    public void ds() {
        com.uc.a.e.nR().a((Picture) null);
        rA();
        this.Ts = this.Tt.b(com.uc.c.w.Pa, null, this.p, true, null);
        this.Tt.bringToFront();
    }

    public void k() {
    }

    public void rA() {
        this.Tt.v(com.uc.a.e.nR().nO());
        this.Tt.by(this.Tt.getWidth(), this.Tt.getHeight());
    }

    public boolean rx() {
        return this.Ts;
    }

    public WebViewJUCMainpage ry() {
        return this.Tt;
    }

    public ViewWebSchMainPage rz() {
        return this.vE;
    }
}
