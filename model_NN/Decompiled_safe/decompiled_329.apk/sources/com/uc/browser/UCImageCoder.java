package com.uc.browser;

import b.a.a.a.t;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;

public class UCImageCoder {
    private UCImageCoder() {
    }

    public static String N(byte[] bArr) {
        if (bArr != null) {
            return new String(Base64Coder.J(bArr));
        }
        return null;
    }

    public static byte[] bJ(String str) {
        try {
            FileInputStream fileInputStream = new FileInputStream(str);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr = new byte[t.bie];
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    fileInputStream.close();
                    byteArrayOutputStream.close();
                    return byteArray;
                }
            }
        } catch (Exception e) {
            return null;
        }
    }
}
