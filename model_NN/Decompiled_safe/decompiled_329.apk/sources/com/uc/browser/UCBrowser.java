package com.uc.browser;

import android.app.Application;
import android.webkit.CookieSyncManager;
import b.a.a.f;
import b.b.a;
import com.uc.a.e;
import com.uc.c.s;
import dalvik.system.VMRuntime;
import java.lang.Thread;

public class UCBrowser extends Application {
    long bxj = 0;
    String bxk = null;
    Thread.UncaughtExceptionHandler bxl = null;

    /* access modifiers changed from: package-private */
    public synchronized void GR() {
        if (this.bxl == null) {
            this.bxj = Thread.currentThread().getId();
            this.bxk = Thread.currentThread().getName();
            this.bxl = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                private void a(Thread thread, Throwable th) {
                    if (th instanceof NullPointerException) {
                        f.l(1, f.aud);
                    } else if (th instanceof ArrayIndexOutOfBoundsException) {
                        f.l(1, f.aue);
                    } else if (th instanceof OutOfMemoryError) {
                        f.l(1, f.auf);
                    } else {
                        f.l(1, f.auc);
                    }
                    e.qC();
                    if (true == f.h(f.auu, false)) {
                        f.wP();
                    }
                    s.jY().a(th);
                }

                /* JADX WARNING: Removed duplicated region for block: B:15:0x002f  */
                /* JADX WARNING: Removed duplicated region for block: B:19:0x003b  */
                /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void uncaughtException(java.lang.Thread r7, java.lang.Throwable r8) {
                    /*
                        r6 = this;
                        r4 = 1
                        long r0 = r7.getId()     // Catch:{ Throwable -> 0x002b, all -> 0x0037 }
                        com.uc.browser.UCBrowser r2 = com.uc.browser.UCBrowser.this     // Catch:{ Throwable -> 0x002b, all -> 0x0037 }
                        long r2 = r2.bxj     // Catch:{ Throwable -> 0x002b, all -> 0x0037 }
                        int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                        if (r0 != 0) goto L_0x0029
                        java.lang.String r0 = r7.getName()     // Catch:{ Throwable -> 0x002b, all -> 0x0037 }
                        com.uc.browser.UCBrowser r1 = com.uc.browser.UCBrowser.this     // Catch:{ Throwable -> 0x002b, all -> 0x0037 }
                        java.lang.String r1 = r1.bxk     // Catch:{ Throwable -> 0x002b, all -> 0x0037 }
                        boolean r0 = r0.equals(r1)     // Catch:{ Throwable -> 0x002b, all -> 0x0037 }
                        if (r0 == 0) goto L_0x0029
                        r0 = r4
                    L_0x001c:
                        r6.a(r7, r8)     // Catch:{ Throwable -> 0x0048, all -> 0x0043 }
                        if (r0 == 0) goto L_0x0028
                        com.uc.browser.UCBrowser r0 = com.uc.browser.UCBrowser.this
                        java.lang.Thread$UncaughtExceptionHandler r0 = r0.bxl
                        r0.uncaughtException(r7, r8)
                    L_0x0028:
                        return
                    L_0x0029:
                        r0 = 0
                        goto L_0x001c
                    L_0x002b:
                        r0 = move-exception
                        r0 = r4
                    L_0x002d:
                        if (r0 == 0) goto L_0x0028
                        com.uc.browser.UCBrowser r0 = com.uc.browser.UCBrowser.this
                        java.lang.Thread$UncaughtExceptionHandler r0 = r0.bxl
                        r0.uncaughtException(r7, r8)
                        goto L_0x0028
                    L_0x0037:
                        r0 = move-exception
                        r1 = r4
                    L_0x0039:
                        if (r1 == 0) goto L_0x0042
                        com.uc.browser.UCBrowser r1 = com.uc.browser.UCBrowser.this
                        java.lang.Thread$UncaughtExceptionHandler r1 = r1.bxl
                        r1.uncaughtException(r7, r8)
                    L_0x0042:
                        throw r0
                    L_0x0043:
                        r1 = move-exception
                        r5 = r1
                        r1 = r0
                        r0 = r5
                        goto L_0x0039
                    L_0x0048:
                        r1 = move-exception
                        goto L_0x002d
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.uc.browser.UCBrowser.AnonymousClass1.uncaughtException(java.lang.Thread, java.lang.Throwable):void");
                }
            });
        }
    }

    public void onCreate() {
        GR();
        super.onCreate();
        VMRuntime.getRuntime().setTargetHeapUtilization(0.75f);
        CookieSyncManager.createInstance(this);
        a.d(this);
        b.a.d(this);
        com.uc.b.a.a(getApplicationContext());
    }

    public void onLowMemory() {
        super.onLowMemory();
    }
}
