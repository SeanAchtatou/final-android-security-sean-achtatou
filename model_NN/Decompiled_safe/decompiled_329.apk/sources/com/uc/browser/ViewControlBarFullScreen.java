package com.uc.browser;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import com.uc.browser.UCR;
import com.uc.e.a.p;
import com.uc.e.a.x;
import com.uc.e.ac;
import com.uc.e.h;
import com.uc.e.z;
import com.uc.h.a;
import com.uc.h.e;

public class ViewControlBarFullScreen extends RelativeLayout implements View.OnClickListener, h, a {
    private static final int chw = 2500;
    private static final int chx = 1;
    private ac HO;
    private ac HP;
    private ac HQ;
    private ac HR;
    private p HS;
    private ac HT;
    private x aux = new x() {
        public void ay(String str) {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().a(119, 1, str);
            }
        }

        public void kP() {
        }

        public void kQ() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(120);
            }
        }

        public void kR() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(128);
            }
        }

        public void onCancel() {
        }
    };
    private ViewWebSchVisitPage bBK;
    private Animation bBU;
    private ActivityBrowser bk;
    private String chm;
    private BarLayout chn;
    private boolean cho;
    private View chp;
    private View chq;
    private View chr;
    private View chs;
    private Animation cht;
    private Animation chu;
    private Animation chv;
    private TickThread chy;
    /* access modifiers changed from: private */
    public int chz = -1;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    ViewControlBarFullScreen.this.cY(true);
                    return;
                default:
                    return;
            }
        }
    };

    class TickThread extends Thread {
        private TickThread() {
        }

        public void run() {
            while (ViewControlBarFullScreen.this.chz >= 0) {
                ViewControlBarFullScreen.a(ViewControlBarFullScreen.this, 325);
                if (ViewControlBarFullScreen.this.chz < 0) {
                    ViewControlBarFullScreen.this.aS(1);
                    int unused = ViewControlBarFullScreen.this.chz = -1;
                }
                try {
                    Thread.sleep(325);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public ViewControlBarFullScreen(Context context, boolean z) {
        super(context);
        this.bk = (ActivityBrowser) context;
        cU(z);
    }

    static /* synthetic */ int a(ViewControlBarFullScreen viewControlBarFullScreen, int i) {
        int i2 = viewControlBarFullScreen.chz - i;
        viewControlBarFullScreen.chz = i2;
        return i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.uc.browser.ViewControlBarFullScreen, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void cU(boolean z) {
        LayoutInflater.from(this.bk).inflate((int) R.layout.f885browser_controlbar_fullscreen, (ViewGroup) this, true);
        this.chn = (BarLayout) findViewById(R.id.f704controlbar_fs_main);
        Kl();
        QR();
        this.chp = findViewById(R.id.f702controlbar_fs_popup);
        this.chp.setOnClickListener(this);
        this.chq = findViewById(R.id.f703controlbar_fs_stop);
        this.chq.setOnClickListener(this);
        this.chr = findViewById(R.id.f704controlbar_fs_main);
        this.chs = findViewById(R.id.f701controlbar_fs_main_default);
        this.chy = null;
        this.bBU = AnimationUtils.loadAnimation(getContext(), R.anim.f5controlbar_fs_fade_in);
        this.cht = AnimationUtils.loadAnimation(getContext(), R.anim.f6controlbar_fs_fade_out);
        this.chu = AnimationUtils.loadAnimation(getContext(), R.anim.f7controlbar_fs_urlbar_fade_in);
        this.chv = AnimationUtils.loadAnimation(getContext(), R.anim.f8controlbar_fs_urlbar_fade_out);
        k();
        e.Pb().a(this);
        this.bBK = (ViewWebSchVisitPage) findViewById(R.id.f705inputurl_layout);
        this.bBK.a(this.aux);
        this.bBK.setVisibility(8);
    }

    public void Kl() {
        e Pb = e.Pb();
        Pb.a(this);
        this.chn.aw(Pb.kp(R.dimen.f186controlbar_item_width_5), Pb.kp(R.dimen.f177controlbar_height));
        this.chn.eB(2);
        int kp = Pb.kp(R.dimen.f190controlbar_button_image_height);
        int kp2 = Pb.kp(R.dimen.f191controlbar_button_image_width);
        int kp3 = Pb.kp(R.dimen.f188controlbar_text_size);
        int kp4 = Pb.kp(R.dimen.f187controlbar_item_paddingTop);
        this.HP = new ac(UCR.drawable.aUe, 0, 0);
        this.HP.bB(kp2, kp);
        this.HP.setPadding(0, kp4, 0, 4);
        this.HP.H(false);
        this.chn.a(this.HP);
        this.HQ = new ac(UCR.drawable.aKa, 0, 0);
        this.HQ.bB(kp2, kp);
        this.HQ.setPadding(0, kp4, 0, 4);
        this.HQ.setVisibility(2);
        this.chn.a(this.HQ);
        this.HR = new ac(UCR.drawable.aUg, 0, 0);
        this.HR.bB(kp2, kp);
        this.HR.setPadding(0, kp4, 0, 4);
        this.HR.H(false);
        this.chn.a(this.HR);
        this.HT = new ac(UCR.drawable.aJS, 0, 0);
        this.HT.bB(kp2, kp);
        this.HT.aV(kp3);
        this.HT.setText(getResources().getString(R.string.f1016controlbar_menu));
        this.HT.setPadding(0, kp4, 0, 4);
        this.chn.a(this.HT);
        this.HS = new p(UCR.drawable.aKc, 0, 0);
        this.HS.bB(kp2, kp);
        this.HS.aV(kp3);
        this.HS.setText(getResources().getString(R.string.f1014controlbar_window));
        this.HS.setPadding(0, kp4, 0, 4);
        this.HS.he(Pb.kp(R.dimen.f180controlbar_winnum_margin_left));
        this.HS.hf(Pb.kp(R.dimen.f181controlbar_winnum_margin_top));
        this.HS.hd(Pb.kp(R.dimen.f189controlbar_winnum_text_size));
        this.chn.a(this.HS);
        this.HO = new ac(UCR.drawable.aJQ, 0, 0);
        this.HO.bB(kp2, kp);
        this.HO.aV(kp3);
        this.HO.setPadding(0, kp4, 0, 4);
        this.chn.a(this.HO);
        this.chn.kJ();
        this.chn.b(this);
    }

    /* access modifiers changed from: package-private */
    public void P(boolean z) {
        if (!z) {
            this.HO.H(true);
        } else {
            this.HO.H(false);
        }
    }

    /* access modifiers changed from: package-private */
    public void Q(boolean z) {
        if (true == z) {
            this.HR.H(true);
        } else {
            this.HR.H(false);
        }
    }

    public void QR() {
        MultiWindowManager multiWindowManager = null;
        if (ModelBrowser.gD() != null) {
            multiWindowManager = ModelBrowser.gD().hj();
        }
        if (multiWindowManager == null) {
            this.HS.hc(1);
        } else {
            this.HS.hc(multiWindowManager.wh());
        }
    }

    public void QS() {
        invalidate();
    }

    public String QT() {
        return this.chm;
    }

    public void aS(int i) {
        this.mHandler.sendEmptyMessage(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
     arg types: [int, boolean]
     candidates:
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
     arg types: [int, int]
     candidates:
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void */
    public void b(z zVar, int i) {
        switch (i) {
            case UCR.drawable.aUe:
            case UCR.drawable.aKa:
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(40);
                    return;
                }
                return;
            case UCR.drawable.aUf:
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(18, (Object) 0);
                    return;
                }
                return;
            case UCR.drawable.aUg:
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a((Integer) 1);
                    return;
                }
                return;
            case UCR.drawable.aUh:
            case UCR.drawable.aUi:
            case UCR.drawable.aUj:
            case UCR.drawable.aUk:
            default:
                return;
            case UCR.drawable.aJQ:
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(10);
                    return;
                }
                return;
            case UCR.drawable.aJS:
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(118, (Object) false);
                    return;
                }
                return;
            case UCR.drawable.aKc:
                if (ModelBrowser.gD().hj() != null && ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(14);
                    return;
                }
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void cV(boolean z) {
    }

    public void cW(boolean z) {
        if (true == z) {
            this.HP.O(e.Pb().getDrawable(UCR.drawable.aKa));
            this.HP.H(true);
            if (this.chr.getVisibility() != 0) {
                this.chq.setVisibility(0);
            }
            this.cho = true;
        } else if (ModelBrowser.gD() != null) {
            d(ModelBrowser.gD().canGoBack(), ModelBrowser.gD().bo());
            this.chq.setVisibility(4);
            this.cho = false;
        }
    }

    public void cX(boolean z) {
        if (this.chr.getVisibility() != 0) {
            if (z) {
                this.chr.startAnimation(this.bBU);
            }
            this.chr.setVisibility(0);
            this.chs.setVisibility(4);
            this.chq.setVisibility(4);
            this.bBK.eF(this.chm);
            this.bBK.setVisibility(0);
            this.bBK.b(this.chu);
        }
    }

    public void cY(boolean z) {
        if (this.chr.getVisibility() == 0) {
            if (z) {
                this.chr.startAnimation(this.cht);
                this.chs.startAnimation(this.bBU);
            }
            this.chr.setVisibility(8);
            this.chs.setVisibility(0);
            if (this.cho) {
                this.chq.setVisibility(0);
            }
            this.chz = -1;
            this.bBK.c(this.chv);
        }
    }

    /* access modifiers changed from: package-private */
    public void d(boolean z, boolean z2) {
        if (true == z2) {
            this.HP.O(e.Pb().getDrawable(UCR.drawable.aJJ));
            this.HP.H(true);
        } else {
            this.HP.O(e.Pb().getDrawable(UCR.drawable.aUe));
            this.HP.H(true);
        }
        invalidate();
    }

    public void gd(String str) {
        this.chm = str;
        this.bBK.eF(str);
    }

    public void k() {
        e Pb = e.Pb();
        this.chn.n(Pb.getDrawable(UCR.drawable.aXp));
        this.HQ.O(Pb.getDrawable(UCR.drawable.aKa));
        this.HP.O(Pb.getDrawable(UCR.drawable.aUe));
        this.HR.O(Pb.getDrawable(UCR.drawable.aUg));
        this.HS.O(Pb.getDrawable(UCR.drawable.aKc));
        this.HS.hg(Pb.getColor(12));
        this.HO.O(Pb.getDrawable(UCR.drawable.aJQ));
        this.HT.O(Pb.getDrawable(UCR.drawable.aJS));
        this.chp.setBackgroundDrawable(Pb.getDrawable(UCR.drawable.fX));
        this.chq.setBackgroundDrawable(Pb.getDrawable(UCR.drawable.fY));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, int, java.lang.Object):void
     arg types: [int, int, boolean]
     candidates:
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.util.HashMap, int):void
      com.uc.browser.ModelBrowser.a(int, int, long):void
      com.uc.browser.ModelBrowser.a(int, java.lang.String, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WebViewJUC, int, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, com.uc.browser.WindowUCWeb, int):void
      com.uc.browser.ModelBrowser.a(int, int, java.lang.Object):void */
    public void onClick(View view) {
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(83, 0, (Object) false);
        }
        switch (view.getId()) {
            case R.id.f702controlbar_fs_popup:
                cX(true);
                break;
            case R.id.f703controlbar_fs_stop:
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(40);
                    break;
                }
                break;
        }
        this.chz = chw;
    }
}
