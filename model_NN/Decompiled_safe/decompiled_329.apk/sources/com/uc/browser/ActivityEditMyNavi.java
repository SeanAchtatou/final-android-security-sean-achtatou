package com.uc.browser;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Selection;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.uc.browser.BookmarkTabContainer;
import com.uc.browser.UCAlertDialog;
import com.uc.browser.UCR;
import com.uc.e.b.j;
import com.uc.h.a;
import com.uc.h.e;
import java.util.Vector;

public class ActivityEditMyNavi extends ActivityWithUCMenu implements View.OnClickListener, BookmarkTabContainer.BookmarkDelegate, a {
    public static final String bvi = "url";
    public static final String bvj = "title";
    public static final String bvk = "pagetype";
    public static final String bvl = "itemid";
    private boolean asw = false;
    private boolean asx = false;
    private EditText buU;
    private EditText buV;
    private ImageView buW;
    private View buX;
    private View buY;
    private View buZ;
    private RelativeLayout bva;
    /* access modifiers changed from: private */
    public BookmarkTabContainer bvb;
    private boolean bvc = false;
    private boolean bvd = false;
    private Dialog bve;
    private byte bvf = -1;
    private View bvg;
    /* access modifiers changed from: private */
    public int bvh = -1;
    private int bvm = 0;

    private void GI() {
        if (this.bve == null) {
            this.bve = new Dialog(this, R.style.f1577quit_dialog) {
                public boolean dispatchKeyEvent(KeyEvent keyEvent) {
                    return ActivityEditMyNavi.this.bvb.dispatchKeyEvent(keyEvent);
                }
            };
            this.bve.setContentView((int) R.layout.f917mynavi_edit_list_dlg);
            this.bva = (RelativeLayout) this.bve.findViewById(R.id.f798bookmark_container);
            this.bvb = (BookmarkTabContainer) this.bve.findViewById(R.id.f799bookmark_list);
            this.bvb.M(false);
            this.bvb.b(0, 0, 0, e.Pb().kp(R.dimen.f297mynavi_edit_list_tail_height));
            this.bve.findViewById(R.id.f800cancel_bar).setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aWg));
            this.buZ = this.bve.findViewById(R.id.f801cancel_bookmark_list);
            this.buZ.setOnClickListener(this);
            this.bvb.a(this);
        }
        this.bve.show();
        this.bve.findViewById(R.id.f798bookmark_container).requestFocus();
    }

    private void GJ() {
        if (this.bve != null) {
            this.bve.dismiss();
        }
    }

    private void GK() {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this);
        builder.aH(R.string.f1210choose_mynavi_position_title);
        Vector CJ = com.uc.a.e.nR().nZ().CJ();
        if (CJ != null && CJ.size() > 0) {
            String[] strArr = new String[CJ.size()];
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= CJ.size()) {
                    break;
                }
                String string = getString(R.string.f1212null_mynavi_title);
                b.a.a.e eVar = (b.a.a.e) CJ.elementAt(i2);
                if (eVar == null) {
                    strArr[i2] = new StringBuffer().append(i2 + 1).append(".").append(string).toString();
                } else {
                    strArr[i2] = new StringBuffer().append(i2 + 1).append(".").append(eVar.abM).toString();
                }
                i = i2 + 1;
            }
            builder.a(strArr, this.bvh, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    int unused = ActivityEditMyNavi.this.bvh = i;
                    dialogInterface.dismiss();
                }
            });
        }
        builder.c((int) R.string.cancel, (DialogInterface.OnClickListener) null);
        builder.fh().show();
    }

    public void a(b.a.a.e eVar, int i, int i2, int i3) {
        if (eVar.abL != null) {
            this.buU.setText(eVar.abL);
            this.buV.setText(eVar.abM);
            if (eVar.abM != null && eVar.abM.length() > 0) {
                Selection.setSelection(this.buV.getText(), eVar.abM.length());
            }
            this.bvf = eVar.abS;
            GJ();
        }
    }

    public void a(b.a.a.e eVar, int i, boolean z) {
    }

    public void b(b.a.a.e eVar, int i, int i2, int i3) {
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (!this.bvd || keyEvent.getKeyCode() != 4) {
            return super.dispatchKeyEvent(keyEvent);
        }
        if (keyEvent.getAction() == 1) {
            GJ();
        }
        return true;
    }

    public void k() {
        e Pb = e.Pb();
        this.bvg.setBackgroundDrawable(Pb.getDrawable(UCR.drawable.aWD));
        this.bvg.findViewById(R.id.divider).setBackgroundDrawable(new j(new int[]{Pb.getColor(87), Pb.getColor(88)}));
        this.bvg.invalidate();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 86 && i2 == -1) {
            GJ();
            finish();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.f795open_bookmark_history /*2131099799*/:
                if (this.bvm == 0) {
                    GI();
                    return;
                } else {
                    GK();
                    return;
                }
            case R.id.f796mynavi_confirm /*2131099800*/:
                if (this.bvh != -1) {
                    b.a.a.e eVar = new b.a.a.e();
                    eVar.abM = this.buV.getText().toString();
                    eVar.abL = this.buU.getText().toString();
                    eVar.abT = true;
                    eVar.abS = this.bvf;
                    if (eVar.abM == null || eVar.abM.length() <= 0 || eVar.abL == null || eVar.abL.length() <= 0) {
                        Toast.makeText(this, (int) R.string.f1477navi_not_allow_null, 1).show();
                        return;
                    } else if (com.uc.a.e.nR().a(this.bvh, eVar)) {
                        ModelBrowser.gD().aS(131);
                        finish();
                        return;
                    } else {
                        Toast.makeText(this, (int) R.string.f1476navi_already_exist, 1).show();
                        return;
                    }
                } else {
                    Toast.makeText(this, (int) R.string.f1211msg_no_position, 1).show();
                    return;
                }
            case R.id.f797mynavi_cancel /*2131099801*/:
                finish();
                return;
            case R.id.f798bookmark_container /*2131099802*/:
            case R.id.f799bookmark_list /*2131099803*/:
            case R.id.f800cancel_bar /*2131099804*/:
            default:
                return;
            case R.id.f801cancel_bookmark_list /*2131099805*/:
                GJ();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        ActivityBrowser.g(this);
        this.bvg = LayoutInflater.from(this).inflate((int) R.layout.f916mynavi_edit, (ViewGroup) null);
        setContentView(this.bvg);
        ((TextView) findViewById(R.id.f654Browser_TitleBar)).setText((int) R.string.f1009activity_add_mynavi);
        int color = e.Pb().getColor(46);
        ((TextView) findViewById(R.id.f792prefix_url_add)).setTextColor(color);
        ((TextView) findViewById(R.id.f790prefix_title)).setTextColor(color);
        this.buU = (EditText) findViewById(R.id.f793mynavi_url);
        this.buU.setImeOptions(6);
        this.buV = (EditText) findViewById(R.id.f791mynavi_title);
        this.buW = (ImageView) findViewById(R.id.f795open_bookmark_history);
        this.buW.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aXa));
        this.buX = findViewById(R.id.f796mynavi_confirm);
        this.buY = findViewById(R.id.f797mynavi_cancel);
        this.buY.setOnClickListener(this);
        this.buW.setOnClickListener(this);
        this.buX.setOnClickListener(this);
        k();
        this.buW.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aWQ));
        Intent intent = getIntent();
        if (!(intent == null || intent.getExtras() == null)) {
            String string = intent.getExtras().getString(bvi);
            this.bvh = intent.getExtras().getInt(bvl);
            this.bvf = intent.getExtras().getByte(bvk);
            if (string != null) {
                this.buU.setText(string);
                if (this.bvh >= 0) {
                    ((TextView) findViewById(R.id.f654Browser_TitleBar)).setText((int) R.string.f1008activity_edit_mynavi);
                    this.bvc = true;
                } else {
                    this.bvm = 1;
                    this.buW.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aXj));
                    GK();
                }
            }
            String string2 = intent.getExtras().getString(bvj);
            if (string2 != null) {
                this.buV.setText(string2);
                if (string2.length() > 1) {
                    this.asx = true;
                }
            }
        }
        this.asw = true;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i != 84) {
            return super.onKeyUp(i, keyEvent);
        }
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a((int) ModelBrowser.zJ, this);
        }
        return true;
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z && ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(this);
        }
        if (z && ActivityBrowser.Fz()) {
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
            }
            ActivityBrowser.e(this);
        }
        if (z && this.asw) {
            if (!this.asx || this.bvc) {
                this.buV.post(new Runnable() {
                    public void run() {
                        ((InputMethodManager) ActivityEditMyNavi.this.getSystemService("input_method")).toggleSoftInput(0, 1);
                    }
                });
                this.asw = false;
            }
        }
    }
}
