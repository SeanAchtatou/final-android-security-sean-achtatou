package com.uc.browser;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class ActivityShield extends Activity {
    public static final String tr = "extra_package";
    public static final String ts = "extra_activity";
    public static final String tt = "shield_action";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Intent intent;
        super.onCreate(bundle);
        Intent intent2 = getIntent();
        if ("com.android.mms".equals(intent2.getStringExtra(tr))) {
            Intent intent3 = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:"));
            String stringExtra = intent2.getStringExtra("android.intent.extra.TEXT");
            if (stringExtra == null) {
                stringExtra = "";
            }
            intent3.putExtra("sms_body", stringExtra);
            intent = intent3;
        } else {
            Intent intent4 = new Intent(intent2.getStringExtra(tt));
            intent4.setComponent(new ComponentName(intent2.getStringExtra(tr), intent2.getStringExtra(ts)));
            intent4.setType(intent2.getType());
            intent4.putExtra("android.intent.extra.TEXT", intent2.getStringExtra("android.intent.extra.TEXT"));
            intent4.putExtra("android.intent.extra.STREAM", intent2.getParcelableExtra("android.intent.extra.STREAM"));
            intent = intent4;
        }
        startActivity(intent);
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
