package com.uc.browser;

import android.graphics.PointF;
import android.view.MotionEvent;

public class MultiTouchHandler implements UCTouchHandler {
    PointF[] aad = {new PointF(), new PointF()};

    public MultiTouchHandler() {
        MotionEvent.obtain(1, 2, 0, 1.0f, 1.0f, 1).getPointerCount();
    }

    public int c(MotionEvent motionEvent) {
        if (261 == motionEvent.getAction()) {
            if (2 == motionEvent.getPointerCount()) {
                this.aad[0].x = motionEvent.getX();
                this.aad[0].y = motionEvent.getY();
                this.aad[1].x = motionEvent.getX(1);
                this.aad[1].y = motionEvent.getY(1);
            }
            return 1;
        } else if (2 != motionEvent.getAction() || 2 != motionEvent.getPointerCount()) {
            return 0;
        } else {
            this.aad[0].x = motionEvent.getX();
            this.aad[0].y = motionEvent.getY();
            this.aad[1].x = motionEvent.getX(1);
            this.aad[1].y = motionEvent.getY(1);
            return 2;
        }
    }

    public PointF[] ur() {
        return this.aad;
    }
}
