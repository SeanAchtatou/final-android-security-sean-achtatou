package com.uc.browser;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

public class LongClickableButton extends ImageView {
    private static final int[] Ia = {16842919};
    private static final int[] Ib = new int[0];
    private static final int Ie = 20;
    public int Ic = -1;
    public int Id = -1;
    LongClickingProcess If;

    class LongClickingProcess extends Thread {
        final int delay;
        boolean mn;
        int mo;
        final int mp;

        public LongClickingProcess() {
            this.mn = false;
            this.mo = 0;
            this.mp = 500;
            this.delay = 200;
            this.mn = true;
        }

        public void run() {
            this.mn = true;
            this.mo = 0;
            while (this.mn) {
                ModelBrowser gD = ModelBrowser.gD();
                if (gD != null) {
                    if (this.mo < 6) {
                        gD.aS(38);
                        if (LongClickableButton.this.Ic > 0) {
                            gD.aS(LongClickableButton.this.Ic);
                        }
                        this.mo++;
                    } else {
                        this.mn = false;
                        gD.aS(38);
                        if (LongClickableButton.this.Id > 0) {
                            gD.aS(LongClickableButton.this.Id);
                        }
                    }
                }
                try {
                    if (this.mo < 2) {
                        Thread.sleep(500);
                    } else {
                        Thread.sleep(200);
                    }
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public LongClickableButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    private boolean T(boolean z) {
        if (z) {
            Drawable drawable = getDrawable();
            if (drawable != null) {
                drawable.setState(Ia);
            }
            Drawable background = getBackground();
            if (background != null) {
                background.setState(Ia);
            }
        } else {
            Drawable drawable2 = getDrawable();
            if (drawable2 != null) {
                drawable2.setState(Ib);
            }
            Drawable background2 = getBackground();
            if (background2 != null) {
                background2.setState(Ib);
            }
        }
        invalidate();
        return z;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                T(true);
                if (this.If != null) {
                    this.If.mn = false;
                }
                this.If = new LongClickingProcess();
                this.If.start();
                break;
            case 1:
                T(false);
                if (this.If != null) {
                    this.If.mn = false;
                    break;
                }
                break;
            case 2:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                if (x < -20.0f || x > ((float) (getWidth() + 20)) || y < -20.0f || y > ((float) (getHeight() + 20))) {
                    T(false);
                    if (this.If != null) {
                        this.If.mn = false;
                        break;
                    }
                }
                break;
        }
        return true;
    }
}
