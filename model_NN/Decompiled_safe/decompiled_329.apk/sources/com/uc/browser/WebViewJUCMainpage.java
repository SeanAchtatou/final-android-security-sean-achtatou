package com.uc.browser;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.Toast;
import b.a.a.e;
import b.b;
import com.uc.a.l;
import com.uc.browser.UCAlertDialog;
import com.uc.browser.UCR;
import com.uc.c.cd;
import com.uc.d.c;
import com.uc.d.d;
import com.uc.d.f;
import com.uc.e.ad;
import com.uc.e.h;
import com.uc.e.w;
import com.uc.e.z;
import com.uc.f.g;
import com.uc.h.a;
import java.util.Vector;

public class WebViewJUCMainpage extends WebViewJUC implements d, ad, h, w, a {
    private static final float bRr = 1.0f;
    private static final int bRs = 20;
    private static int bRt = 0;
    static final String bRz = "T";
    private int XT = 10;
    private Drawable aEg;
    private int bAZ = -1;
    private int bBa = -1;
    private com.uc.e.a.h bQX;
    private e bQY;
    /* access modifiers changed from: private */
    public int bQZ;
    private boolean bRa = false;
    private int bRb = 10;
    private Drawable bRc;
    private Drawable bRd;
    private Drawable bRe;
    private int bRf = 10;
    private int bRg = 4;
    private Drawable bRh;
    private Drawable bRi;
    private Drawable bRj;
    private Drawable bRk;
    private int bRl;
    private int bRm;
    private int bRn;
    private int bRo;
    private int bRp;
    private float bRq;
    private Drawable bRu;
    private Rect bRv;
    private boolean bRw;
    private boolean bRx;
    private Paint bRy = new Paint();
    private c lE;
    private f lF;

    public WebViewJUCMainpage(Context context) {
        super(context);
        MU();
    }

    public WebViewJUCMainpage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        MU();
    }

    private void MU() {
        com.uc.h.e Pb = com.uc.h.e.Pb();
        Pb.a(this);
        l nZ = com.uc.a.e.nR().nZ();
        com.uc.a.e.nR().nQ();
        Vector CJ = nZ.CJ();
        this.bQX = new com.uc.e.a.h();
        this.bRb = Pb.kp(R.dimen.f193mynavi_bar_height);
        this.bQX.aw(Pb.kp(R.dimen.f195mynavi_item_width), Pb.kp(R.dimen.f196mynavi_item_height));
        this.bQX.a(Pb.kp(R.dimen.f197mynavi_item_padding_left), Pb.kp(R.dimen.f198mynavi_item_padding_top), Pb.kp(R.dimen.f199mynavi_item_padding_right), Pb.kp(R.dimen.f200mynavi_item_padding_bottom));
        this.bQX.hP(Pb.kp(R.dimen.f201mynavi_padding_top));
        this.bQX.hN(Pb.kp(R.dimen.f202mynavi_padding_left));
        this.bQX.hO(Pb.kp(R.dimen.f203mynavi_padding_right));
        this.bQX.fh(Pb.kp(R.dimen.f205mynavi_item_textarea_height));
        this.bQX.fd(Pb.kp(R.dimen.f204mynavi_item_textsize));
        k();
        this.bQX.p(CJ);
        this.bQX.c(this);
        this.bQX.a((w) this);
        this.bQX.a((ad) this);
    }

    private void MV() {
        z(getContext()).show();
    }

    private boolean d(int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            return super.e(i, keyEvent);
        }
        if (1 == keyEvent.getAction()) {
            return super.f(i, keyEvent);
        }
        return false;
    }

    private void m(Canvas canvas) {
        int i;
        int width;
        boolean z;
        int i2;
        int i3;
        int i4;
        com.uc.h.e Pb = com.uc.h.e.Pb();
        this.bRu.setBounds(0, this.bRb, getWidth(), getHeight());
        this.bRu.draw(canvas);
        Bitmap kl = Pb.kl(UCR.drawable.aWb);
        Resources resources = getContext().getResources();
        int dimensionPixelOffset = resources.getDimensionPixelOffset(R.dimen.f390network_error_font_size);
        int dimensionPixelOffset2 = resources.getDimensionPixelOffset(R.dimen.f391network_check_guide_font_size);
        Rect clipBounds = canvas.getClipBounds();
        int i5 = (((clipBounds.bottom - clipBounds.top) - this.bRb) - this.bRv.bottom) - this.bRv.top;
        int i6 = clipBounds.right - clipBounds.left;
        String str = resources.getString(R.string.f1485network_error) + cd.bVH + resources.getString(R.string.f1487navi_load_failed);
        String string = resources.getString(R.string.f1488network_check_guid);
        Paint paint = new Paint();
        paint.setColor(Pb.getColor(84));
        paint.setTextSize((float) dimensionPixelOffset);
        paint.setAntiAlias(true);
        int i7 = dimensionPixelOffset / 2;
        float measureText = paint.measureText(str);
        if (i5 > ((int) (((double) dimensionPixelOffset) * 3.2d)) + kl.getHeight()) {
            int height = ((((i5 - kl.getHeight()) - (dimensionPixelOffset * 2)) - i7) / 2) + this.bRb;
            int width2 = clipBounds.left + ((i6 - kl.getWidth()) / 2);
            width = (int) ((((float) i6) - measureText) / 2.0f);
            i = i7;
            i4 = kl.getHeight() + height + (i7 * 3);
            z = false;
            int i8 = width2;
            i3 = height;
            i2 = i8;
        } else {
            int height2 = ((i5 - kl.getHeight()) / 2) + this.bRb;
            int width3 = (int) (((float) clipBounds.left) + ((((float) (i6 - kl.getWidth())) - measureText) / 2.0f));
            int i9 = dimensionPixelOffset / 2;
            int i10 = ((i5 - dimensionPixelOffset) / 2) + this.bRb;
            i = i9;
            width = kl.getWidth() + width3 + i9;
            z = true;
            int i11 = i10;
            i2 = width3;
            i3 = height2;
            i4 = i11;
        }
        canvas.drawBitmap(kl, (float) i2, (float) i3, (Paint) null);
        canvas.drawText(str, (float) width, (float) i4, paint);
        paint.setColor(Pb.getColor(85));
        int i12 = i4 + i;
        paint.setTextSize((float) dimensionPixelOffset2);
        float measureText2 = paint.measureText(string);
        canvas.drawLine((float) width, (float) i12, ((float) width) + measureText, (float) i12, paint);
        int i13 = (i * 2) + i12;
        paint.setColor(Pb.getColor(86));
        canvas.drawText(string, z ? (float) width : (((float) getWidth()) - measureText2) / 2.0f, (float) i13, paint);
    }

    public void a(Canvas canvas, int[] iArr, String str, int i, boolean z, boolean z2, int i2, int i3, int i4) {
        if (i4 != 0) {
            Rect clipBounds = canvas.getClipBounds();
            canvas.save();
            if (((float) i3) < ((float) clipBounds.top) + this.bRq) {
                Path path = new Path();
                path.addRoundRect(new RectF(clipBounds), this.bRq, this.bRq, Path.Direction.CCW);
                canvas.clipPath(path);
            }
            Rect rect = new Rect();
            boolean z3 = false;
            int i5 = clipBounds.right - clipBounds.left;
            if (i4 == 1 && this.bRi != null) {
                this.bRi.setBounds(0, 0, i5, iArr[1]);
                this.bRi.draw(canvas);
                z3 = this.bRi.getPadding(rect);
            } else if (this.bRh != null) {
                this.bRh.setBounds(0, 0, i5, iArr[1]);
                this.bRh.draw(canvas);
                z3 = this.bRh.getPadding(rect);
            }
            if (z && this.bRj != null) {
                if (z3) {
                    this.bRj.setBounds(rect.left, rect.top, i5 - rect.left, iArr[1] - rect.bottom);
                } else {
                    this.bRj.setBounds(this.bRg, 0, i5 - this.bRg, iArr[1]);
                }
                this.bRj.draw(canvas);
            }
            canvas.translate((float) this.bRf, 0.0f);
            Drawable drawable = z ? this.bRe : this.bRd;
            int i6 = 0;
            if (drawable != null) {
                i6 = drawable.getIntrinsicWidth();
                int intrinsicHeight = (iArr[1] - drawable.getIntrinsicHeight()) / 2;
                drawable.setBounds(this.bRf, intrinsicHeight, this.bRf + i6, drawable.getIntrinsicHeight() + intrinsicHeight);
                drawable.draw(canvas);
            }
            int i7 = i6;
            this.bRy.setColor(z ? this.bRo : this.bRn);
            this.bRy.setAntiAlias(true);
            this.bRy.setTextSize((float) this.bRm);
            canvas.drawText(str, (float) (i7 + this.bRf + this.XT), ((((float) iArr[1]) - this.bRy.ascent()) - this.bRy.descent()) / 2.0f, this.bRy);
            if (z2) {
                if (this.bRk != null) {
                    this.bRk.setBounds(-this.bRf, iArr[1], i5, iArr[1] + this.bRl);
                    this.bRk.draw(canvas);
                }
                if (this.bRc != null) {
                    canvas.translate((float) (i5 - (this.bRf * 2)), (float) ((iArr[1] - this.bRc.getIntrinsicWidth()) / 2));
                    canvas.rotate(90.0f);
                    this.bRc.setBounds(0, 0, this.bRc.getIntrinsicWidth(), this.bRc.getIntrinsicHeight());
                    this.bRc.draw(canvas);
                }
            } else if (this.bRc != null) {
                canvas.translate((float) ((i5 - this.bRc.getIntrinsicWidth()) - (this.bRf * 2)), (float) ((iArr[1] - this.bRc.getIntrinsicWidth()) / 2));
                this.bRc.setBounds(0, 0, this.bRc.getIntrinsicWidth(), this.bRc.getIntrinsicHeight());
                this.bRc.draw(canvas);
            }
            canvas.restore();
        }
    }

    public void a(z zVar, int i) {
        e aI = ((com.uc.e.a.h) zVar).aI(i);
        this.bQY = aI;
        this.bQZ = i;
        if (aI != null) {
            n(aI);
        } else {
            n(null);
        }
    }

    public boolean a(Canvas canvas, int[] iArr) {
        int max = Math.max(0, this.bRb - iArr[1]);
        this.bRu.setBounds(-this.bRv.left, max - this.bRv.top, getWidth() - this.bRv.left, getHeight() - this.bRv.top);
        canvas.save();
        if (this.bRv != null) {
            canvas.clipRect((float) (-this.bRv.left), (float) (max - this.bRv.top), (float) (getWidth() - this.bRv.left), (float) (getHeight() - this.bRv.top), Region.Op.REPLACE);
        }
        this.bRu.draw(canvas);
        canvas.restore();
        return true;
    }

    public boolean a(MenuItem menuItem) {
        Class<ActivityEditMyNavi> cls = ActivityEditMyNavi.class;
        switch (menuItem.getItemId()) {
            case com.uc.d.h.ccZ /*524289*/:
                if (this.bQY == null || this.bQY.abL == null) {
                    return false;
                }
                if (this.bQY.abS == 1) {
                    ModelBrowser.gD().hj().cI("wap:" + this.bQY.abL + b.acu);
                    return false;
                }
                ModelBrowser.gD().hj().cI(this.bQY.abL + b.acu);
                return false;
            case com.uc.d.h.aqW /*524290*/:
                Class<ActivityEditMyNavi> cls2 = ActivityEditMyNavi.class;
                Intent intent = new Intent(getContext(), cls);
                intent.putExtra(ActivityEditMyNavi.bvj, this.bQY.abM);
                intent.putExtra(ActivityEditMyNavi.bvi, this.bQY.abL);
                intent.putExtra(ActivityEditMyNavi.bvl, this.bQZ);
                intent.putExtra(ActivityEditMyNavi.bvk, this.bQY.abS);
                getContext().startActivity(intent);
                return false;
            case com.uc.d.h.cda /*524291*/:
                MV();
                return false;
            case com.uc.d.h.cdb /*524292*/:
                Class<ActivityEditMyNavi> cls3 = ActivityEditMyNavi.class;
                Intent intent2 = new Intent(getContext(), cls);
                intent2.putExtra(ActivityEditMyNavi.bvl, this.bQZ);
                getContext().startActivity(intent2);
                return false;
            case com.uc.d.h.cdc /*524293*/:
                com.uc.a.e.nR().cg(this.bQZ);
                ModelBrowser.gD().aS(131);
                return false;
            case com.uc.d.h.cdd /*524294*/:
                com.uc.a.e.nR().ch(this.bQZ);
                ModelBrowser.gD().aS(131);
                return false;
            case com.uc.d.h.cde /*524295*/:
                g.Jn().ix(1);
                ModelBrowser.gD().aS(131);
                return false;
            case com.uc.d.h.cdf /*524296*/:
                g.Jn().ix(2);
                ModelBrowser.gD().aS(131);
                return false;
            default:
                return false;
        }
    }

    public void an(boolean z) {
        if (z) {
            this.bQX.ey(-1728053248);
        } else {
            this.bQX.ey(0);
        }
        this.bQX.Ix();
    }

    public void ap() {
        if (this.ag != null) {
            this.ag.ap();
        }
    }

    public void b(z zVar, int i) {
        e aI = ((com.uc.e.a.h) zVar).aI(i);
        if (aI != null) {
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            int i2 = defaultSharedPreferences.getInt(bRz, 0);
            if (i2 < 3) {
                SharedPreferences.Editor edit = defaultSharedPreferences.edit();
                edit.putInt(bRz, new Integer(i2 + 1).intValue());
                edit.commit();
                Toast.makeText(getContext(), (int) R.string.f1480mynavi_message_show, 1).show();
            }
            ModelBrowser gD = ModelBrowser.gD();
            if (gD != null) {
                if (aI.abS == 1) {
                    gD.a(11, "wap:" + aI.abL + b.acu);
                } else {
                    gD.a(11, aI.abL + b.acu);
                }
                com.uc.a.e.nR().ci(i);
                return;
            }
            return;
        }
        Intent intent = new Intent(getContext(), ActivityEditMyNavi.class);
        intent.putExtra(ActivityEditMyNavi.bvl, i);
        getContext().startActivity(intent);
    }

    /* access modifiers changed from: package-private */
    public void by(int i, int i2) {
        boolean equals = com.uc.a.e.nR().bw(com.uc.a.h.afA).equals(com.uc.a.e.RD);
        if (i - i2 > 20) {
            this.bQX.aJ(6);
            this.bRb = com.uc.h.e.Pb().kp(R.dimen.f193mynavi_bar_height);
            this.bQX.fg(1);
            this.bQX.setSize((i - getPaddingLeft()) - getPaddingRight(), this.bRb);
        } else {
            this.bQX.aJ(4);
            if (equals) {
                this.bRb = com.uc.h.e.Pb().kp(R.dimen.f193mynavi_bar_height);
                this.bQX.fg(1);
                this.bQX.setSize((i - getPaddingLeft()) - getPaddingRight(), this.bRb);
            } else {
                this.bRb = com.uc.h.e.Pb().kp(R.dimen.f194mynavi_bar_height_double);
                this.bQX.fg(2);
                this.bQX.setSize((i - getPaddingLeft()) - getPaddingRight(), this.bRb);
            }
        }
        bG(this.bRb, i);
    }

    public void cJ() {
        invalidate(new Rect(0, 0, getWidth(), this.bRb - this.ag.am()));
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        switch (keyCode) {
            case 19:
            case 21:
            case 22:
            case 23:
                return !this.bRa ? this.bQX.c(keyEvent) : d(keyCode, keyEvent);
            case 20:
                if (this.bRa) {
                    return d(keyCode, keyEvent);
                }
                if (!this.bQX.c(keyEvent)) {
                    this.bRa = true;
                    d(keyCode, keyEvent);
                    return true;
                }
                break;
        }
        return false;
    }

    public void k() {
        com.uc.h.e Pb = com.uc.h.e.Pb();
        this.bRu = Pb.getDrawable(UCR.drawable.aVT);
        if (this.bRu != null) {
            if (this.bRv == null) {
                this.bRv = new Rect();
            }
            this.bRu.getPadding(this.bRv);
        }
        this.bQX.s(Pb.getDrawable(UCR.drawable.aVU));
        this.bQX.e(new Drawable[]{Pb.getDrawable(UCR.drawable.aVV), Pb.getDrawable(UCR.drawable.aVW), Pb.getDrawable(UCR.drawable.aVW)});
        this.bQX.A(Pb.getDrawable(UCR.drawable.aVX));
        this.bQX.x(Pb.getDrawable(UCR.drawable.jJ));
        this.bQX.y(Pb.getDrawable(UCR.drawable.aVS));
        this.bQX.fi(Pb.getColor(42));
        this.bQX.fe(Pb.getColor(41));
        this.bRc = Pb.getDrawable(UCR.drawable.aTC);
        new BitmapFactory.Options().inPreferredConfig = Bitmap.Config.RGB_565;
        this.bRi = Pb.getDrawable(UCR.drawable.aXt);
        this.bRh = Pb.getDrawable(UCR.drawable.aXs);
        this.bRj = Pb.getDrawable(UCR.drawable.aXu);
        this.bRd = Pb.getDrawable(UCR.drawable.aUz);
        this.bRe = Pb.getDrawable(UCR.drawable.aUA);
        this.bRn = Pb.getColor(48);
        this.bRo = Pb.getColor(49);
        this.bRm = Pb.kp(R.dimen.f228bookmark_tabfont);
        this.aEg = Pb.getDrawable(UCR.drawable.aWK);
        this.bRp = Pb.kp(R.dimen.f208navigation_shadow_height);
        this.bRg = Pb.kp(R.dimen.f212bookmark_selected_bg_padding);
        int kp = Pb.kp(R.dimen.f210navigation_icon_padding);
        this.XT = kp;
        this.bRf = kp;
        this.bRk = Pb.getDrawable(UCR.drawable.aWL);
        this.bRl = Pb.kp(R.dimen.f209navigation_folder_shadow_height);
        this.bRq = Pb.kq(R.dimen.f163homepage_corner_size);
    }

    /* access modifiers changed from: protected */
    public void l(Canvas canvas) {
    }

    public void n(e eVar) {
        if (this.lE == null) {
            this.lE = new c(getContext());
        }
        this.lE.clear();
        if (this.lF == null) {
            this.lF = new f(getContext());
            this.lF.a();
        }
        this.lF.a(this.lE);
        this.lE.a(this.lF);
        this.lF.a(this);
        this.lE.cV();
        this.lF.show();
        com.uc.d.g.a(getContext(), com.uc.d.h.cdg, this.lE);
        c cVar = this.lE;
        if (eVar != null) {
            cVar.findItem(com.uc.d.h.cdb).setVisible(false);
            if (ModelBrowser.gD() == null || true != ModelBrowser.gD().hj().wg()) {
                cVar.findItem(com.uc.d.h.ccZ).setVisible(false);
            } else {
                cVar.findItem(com.uc.d.h.ccZ).setVisible(true);
            }
            cVar.findItem(com.uc.d.h.aqW).setVisible(true);
            cVar.findItem(com.uc.d.h.cda).setVisible(true);
            if (eVar.abT) {
                cVar.findItem(com.uc.d.h.cdc).setVisible(false);
                cVar.findItem(com.uc.d.h.cdd).setVisible(true);
            } else {
                cVar.findItem(com.uc.d.h.cdc).setVisible(true);
                cVar.findItem(com.uc.d.h.cdd).setVisible(false);
            }
            if (com.uc.a.e.nR().bw(com.uc.a.h.afA).equals(com.uc.a.e.RD)) {
                cVar.findItem(com.uc.d.h.cde).setVisible(false);
                cVar.findItem(com.uc.d.h.cdf).setVisible(true);
                return;
            }
            cVar.findItem(com.uc.d.h.cde).setVisible(true);
            cVar.findItem(com.uc.d.h.cdf).setVisible(false);
            return;
        }
        cVar.findItem(com.uc.d.h.cdb).setVisible(true);
        cVar.findItem(com.uc.d.h.ccZ).setVisible(false);
        cVar.findItem(com.uc.d.h.aqW).setVisible(false);
        cVar.findItem(com.uc.d.h.cda).setVisible(false);
        cVar.findItem(com.uc.d.h.cdc).setVisible(false);
        cVar.findItem(com.uc.d.h.cdd).setVisible(false);
        cVar.findItem(com.uc.d.h.cde).setVisible(false);
        cVar.findItem(com.uc.d.h.cdf).setVisible(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.WebViewJUC.a(android.graphics.Canvas, boolean):void
     arg types: [android.graphics.Canvas, int]
     candidates:
      com.uc.browser.WebViewJUCMainpage.a(com.uc.e.z, int):void
      com.uc.browser.WebViewJUCMainpage.a(android.graphics.Canvas, int[]):boolean
      com.uc.browser.WebViewJUC.a(com.uc.browser.WebViewJUC, byte):byte
      com.uc.browser.WebViewJUC.a(com.uc.browser.WebViewJUC, int):int
      com.uc.browser.WebViewJUC.a(com.uc.browser.WebViewJUC, android.view.MotionEvent):android.view.MotionEvent
      com.uc.browser.WebViewJUC.a(java.lang.String, java.lang.String):void
      com.uc.browser.WebViewJUC.a(java.io.File, byte):boolean
      com.uc.e.w.a(com.uc.e.z, int):void
      com.uc.browser.WebViewJUC.a(android.graphics.Canvas, boolean):void */
    public void onDraw(Canvas canvas) {
        ViewMainpage gn;
        ViewMainpageNavi cy;
        if (canvas.getClipBounds().bottom + this.ag.am() > this.bRb) {
            ModelBrowser gD = ModelBrowser.gD();
            if (gD != null && (gn = gD.gn()) != null && (cy = gn.cy()) != null) {
                if (cy.rx()) {
                    canvas.save();
                    canvas.translate((float) this.bRv.left, (float) this.bRv.top);
                    int width = (getWidth() - this.bRv.right) - this.bRv.left;
                    int height = (getHeight() - this.bRv.bottom) - this.bRv.top;
                    canvas.clipRect(0.0f, 0.0f, (float) width, (float) height, Region.Op.REPLACE);
                    super.a(canvas, true);
                    if (this.aEg != null) {
                        this.aEg.setBounds(0, height - this.bRp, width, height);
                        this.aEg.draw(canvas);
                    }
                    canvas.restore();
                } else {
                    m(canvas);
                }
            } else {
                return;
            }
        }
        int am = this.ag.am();
        if (am < this.bRb) {
            canvas.save();
            canvas.translate(0.0f, (float) (-am));
            float f = this.bRq;
            int width2 = getWidth();
            RectF rectF = new RectF((float) this.bRv.left, (float) (this.bRv.top + am), (float) (getWidth() - this.bRv.right), (float) this.bRb);
            Path path = new Path();
            path.addRoundRect(rectF, f, f, Path.Direction.CW);
            path.addRect(0.0f, ((float) (am + this.bRv.top)) + f, (float) width2, (float) this.bRb, Path.Direction.CW);
            path.addRect(((float) this.bRv.left) + f, 0.0f, ((float) (width2 - this.bRv.right)) - f, (float) this.bRb, Path.Direction.CW);
            canvas.clipPath(path);
            this.bQX.onDraw(canvas);
            canvas.restore();
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        by(i3 - i, i4 - i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
     arg types: [?, boolean]
     candidates:
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0 && ModelBrowser.gD() != null) {
            ModelBrowser.gD().a((int) ModelBrowser.zP, (Object) false);
        }
        int am = this.ag.am();
        switch (motionEvent.getAction()) {
            case 0:
                if (((float) (this.bRb - am)) <= motionEvent.getY()) {
                    this.bRw = false;
                    this.bAZ = (int) motionEvent.getX();
                    this.bBa = (int) motionEvent.getY();
                    this.bRx = false;
                    break;
                } else {
                    this.bRw = true;
                    if (ModelBrowser.gD() != null) {
                        ModelBrowser.gD().aS(134);
                    }
                    return this.bQX.b((byte) 0, (int) motionEvent.getX(), am + ((int) motionEvent.getY()));
                }
            case 1:
                if (this.bRw) {
                    this.bRw = false;
                    return this.bQX.b((byte) 1, (int) motionEvent.getX(), am + ((int) motionEvent.getY()));
                }
                float abs = Math.abs(motionEvent.getX() - ((float) this.bAZ));
                float abs2 = Math.abs(motionEvent.getY() - ((float) this.bBa));
                this.bAZ = -1;
                this.bBa = -1;
                this.bRx = false;
                if (abs2 == 0.0f) {
                    abs2 = 1.0E-6f;
                }
                boolean z = abs > 20.0f;
                if (abs2 > 20.0f) {
                }
                if (z && abs / abs2 > 1.0f) {
                    super.aF();
                    ((Activity) getContext()).closeContextMenu();
                    return false;
                }
            case 2:
                if (this.bRw) {
                    return this.bQX.b((byte) 2, (int) motionEvent.getX(), am + ((int) motionEvent.getY()));
                }
                if (this.bRx) {
                    super.aF();
                    ((Activity) getContext()).closeContextMenu();
                    return false;
                } else if (this.bAZ < 0) {
                    this.bAZ = (int) motionEvent.getX();
                    this.bBa = (int) motionEvent.getY();
                    break;
                } else {
                    float abs3 = Math.abs(motionEvent.getX() - ((float) this.bAZ));
                    float abs4 = Math.abs(motionEvent.getY() - ((float) this.bBa));
                    if (abs4 == 0.0f) {
                        abs4 = 1.0E-6f;
                    }
                    boolean z2 = abs3 > 20.0f;
                    if (abs4 > 20.0f) {
                    }
                    if (z2 && abs3 / abs4 > 1.0f) {
                        this.bRx = true;
                        super.aF();
                        ((Activity) getContext()).closeContextMenu();
                        return false;
                    }
                }
        }
        this.bQX.clearFocus();
        return super.onTouchEvent(motionEvent);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            ModelBrowser.gD().aS(131);
        }
    }

    public boolean sj() {
        this.bRa = false;
        dispatchKeyEvent(new KeyEvent(0, 19));
        return this.bRa;
    }

    public void v(Vector vector) {
        this.bQX.p(vector);
    }

    /* access modifiers changed from: protected */
    public Dialog z(Context context) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.L(getResources().getString(R.string.f1478navi_dialog_delete_title));
        builder.M(getResources().getString(R.string.f1479navi_dialog_delete_msg));
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                com.uc.a.e.nR().cf(WebViewJUCMainpage.this.bQZ);
                ModelBrowser.gD().aS(131);
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return builder.fh();
    }
}
