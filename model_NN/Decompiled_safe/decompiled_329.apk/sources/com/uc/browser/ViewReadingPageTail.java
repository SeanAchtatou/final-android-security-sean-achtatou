package com.uc.browser;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ViewReadingPageTail extends RelativeLayout {
    private TextView aYJ;
    private Button aYK;
    public int type;

    public ViewReadingPageTail(Context context) {
        super(context);
        d(context);
    }

    public ViewReadingPageTail(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        d(context);
    }

    public ViewReadingPageTail(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        d(context);
    }

    private void d(Context context) {
        LayoutInflater.from(context).inflate((int) R.layout.f931reading_page_tail, this);
        this.aYJ = (TextView) findViewById(R.id.text);
        this.aYK = (Button) findViewById(R.id.button);
    }

    public void E(Drawable drawable) {
        this.aYK.setBackgroundDrawable(drawable);
    }

    public void fL(int i) {
        this.aYK.setVisibility(i);
    }

    public void setAlpha(int i) {
        this.aYJ.setTextColor((this.aYJ.getTextColors().getDefaultColor() & 16777215) | (i << 24));
        Drawable background = this.aYK.getBackground();
        if (background != null) {
            background.setAlpha(i);
        }
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.aYK.setOnClickListener(onClickListener);
    }

    public void setText(String str) {
        this.aYJ.setText(str);
    }

    public void setTextColor(int i) {
        this.aYJ.setTextColor(i);
    }
}
