package com.uc.browser;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import com.uc.c.az;
import com.uc.c.bc;
import com.uc.c.n;
import java.util.Vector;

public class UCWebBookmarkProvider extends ContentProvider {
    private static final String AUTHORITY = "com.uc.browser.UCWebBookmarkProvider";
    private static final String aZc = "getall";
    private static final int aZd = 0;
    private static final UriMatcher aZe = new UriMatcher(-1);

    static {
        aZe.addURI(AUTHORITY, aZc, 0);
    }

    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    public boolean onCreate() {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.n.a(java.util.Vector, boolean, boolean):java.lang.String[]
     arg types: [java.util.Vector, int, int]
     candidates:
      com.uc.c.n.a(java.lang.String, java.util.Vector, short):short
      com.uc.c.n.a(int, java.lang.String, java.lang.String):void
      com.uc.c.n.a(com.uc.c.ca, java.lang.String, short):void
      com.uc.c.n.a(java.util.Vector, boolean, boolean):java.lang.String[] */
    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        byte[] Bw = az.Bw();
        Vector vector = new Vector();
        n.g(vector, Bw);
        String[] a2 = n.a(vector, false, false);
        a2[0] = String.valueOf(bc.aq(bc.am(a2[0].getBytes())));
        String[] strArr3 = {b.XJ};
        Object[] objArr = {a2[0]};
        MatrixCursor matrixCursor = new MatrixCursor(strArr3);
        matrixCursor.addRow(objArr);
        return matrixCursor;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return 0;
    }
}
