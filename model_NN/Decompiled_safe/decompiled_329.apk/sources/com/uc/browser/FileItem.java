package com.uc.browser;

import b.a.a.d;
import java.io.File;

public class FileItem implements Comparable {
    static final int aqa = -2;
    boolean blv;
    File blw;
    int blx;
    private int bly;

    public FileItem() {
        this.blv = false;
        this.blx = -2;
        this.bly = 0;
    }

    public FileItem(File file) {
        this.blv = false;
        this.blw = file;
        this.blx = d.cu(file.getName());
        if (file.isDirectory()) {
            this.bly = 14680064;
        } else {
            this.bly = 15728640;
        }
        this.bly += this.blx;
    }

    public File AC() {
        return this.blw;
    }

    public boolean DH() {
        return this.blv;
    }

    public void DI() {
        this.blv = !this.blv;
    }

    /* renamed from: a */
    public int compareTo(FileItem fileItem) {
        int i = this.bly - fileItem.bly;
        return i == 0 ? this.blw.getName().compareToIgnoreCase(fileItem.blw.getName()) : i;
    }

    public void bx(boolean z) {
        this.blv = z;
    }
}
