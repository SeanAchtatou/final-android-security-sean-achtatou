package com.uc.browser;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.uc.a.e;
import com.uc.a.h;
import com.uc.e.a.ab;
import com.uc.e.a.j;
import com.uc.e.ad;

public class ViewWebSchMainPage extends View {
    private ab af = new ab();

    public ViewWebSchMainPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.af.a(new ad() {
            public void cJ() {
                ViewWebSchMainPage.this.postInvalidate();
            }
        });
    }

    public void a(j jVar) {
        this.af.a(jVar);
    }

    public void aD() {
        if (e.nR().bw(h.afR).contains(e.RD)) {
            this.af.h(e.nR().qF());
        } else {
            this.af.h((Bitmap) null);
        }
    }

    public void k(int i) {
        this.af.k(i);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (canvas.getClipBounds().intersect(getLeft(), getTop(), getRight(), getBottom())) {
            this.af.onDraw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.af.setSize(i3 - i, i4 - i2);
        this.af.init(getResources().getConfiguration().orientation == 1 ? 0 : 1);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.af.onTouchEvent(motionEvent);
    }
}
