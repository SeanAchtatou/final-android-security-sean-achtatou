package com.uc.browser;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.uc.a.d;
import com.uc.a.e;
import com.uc.a.n;
import com.uc.browser.UCAlertDialog;
import com.uc.browser.UCR;
import com.uc.c.au;
import com.uc.d.g;
import com.uc.e.ac;
import com.uc.e.h;
import com.uc.e.z;
import com.uc.h.a;
import java.io.File;

public class ActivityDownload extends ActivityWithUCMenu implements h, a {
    private static final int azQ = 255;
    static final int bFb = 0;
    static final int bFc = 1;
    static final int bFd = 3;
    static final int bFe = 4;
    static final int bFf = 5;
    static final int bFg = 6;
    static final int bFi = 8;
    static final int bFj = 9;
    static final int bFk = 10;
    static final int zG = 2;
    private final int FILL_PARENT = -1;
    private final int WRAP_CONTENT = -2;
    /* access modifiers changed from: private */
    public d ah;
    private RadioGroup auK;
    /* access modifiers changed from: private */
    public ViewDownload bEY = null;
    private ac bEZ;
    private ac bFa;
    private final int bFh = 7;
    private RadioButton bFl;
    private RadioButton bFm;
    /* access modifiers changed from: private */
    public int bFn = 0;
    /* access modifiers changed from: private */
    public EditText bFo;
    /* access modifiers changed from: private */
    public String bFp;
    private int bFq = 0;
    private int bFr = 0;
    private int bFs = 0;
    private int bFt = 0;
    private final String bFu = "need_destroy";
    private final int bFv = azQ;
    private final int bFw = 90;
    private int bFx = 0;
    private int bFy = 0;
    private boolean bFz = true;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    String[] strArr = (String[]) message.obj;
                    if (strArr[0] != null && strArr[3] != null) {
                        ActivityDownload.this.bEY.a(strArr);
                        return;
                    }
                    return;
                case 1:
                    if (message.obj != null) {
                        try {
                            ActivityDownload.this.iz(Integer.parseInt(message.obj.toString()));
                            return;
                        } catch (Exception e) {
                            return;
                        }
                    } else {
                        return;
                    }
                case 2:
                    Toast.makeText(ActivityDownload.this, (int) R.string.f1400avaliablespacenotenough, 0).show();
                    return;
                case 3:
                    Toast.makeText(ActivityDownload.this, (int) R.string.f1412maxwindownumber, 1).show();
                    return;
                case 4:
                    ActivityDownload.this.dt();
                    return;
                case 5:
                    ActivityDownload.this.R();
                    return;
                case 6:
                    if (e.nR().op()) {
                        ActivityDownload.this.ci(true);
                        return;
                    } else {
                        ActivityDownload.this.ci(false);
                        return;
                    }
                case 7:
                    ActivityDownload.this.Kn();
                    return;
                case 8:
                    Intent intent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
                    intent.setData(Uri.fromFile(new File((String) message.obj)));
                    ActivityDownload.this.sendBroadcast(intent);
                    return;
                case 9:
                    Toast.makeText(ActivityDownload.this, message.obj.toString(), 1).show();
                    return;
                case 10:
                default:
                    return;
            }
        }
    };
    private BarLayout um;

    private void Kl() {
        this.um = (BarLayout) findViewById(R.id.f655controlbar);
        com.uc.h.e Pb = com.uc.h.e.Pb();
        int kp = Pb.kp(R.dimen.f183controlbar_item_width_2);
        int kp2 = Pb.kp(R.dimen.f177controlbar_height);
        int kp3 = Pb.kp(R.dimen.f188controlbar_text_size);
        int kp4 = Pb.kp(R.dimen.f187controlbar_item_paddingTop);
        this.um.HN.aw(kp, kp2);
        Resources resources = getResources();
        this.bFa = new ac(R.string.f1334controlbar_download_clear, 0, 0);
        this.bFa.bB(0, 0);
        this.bFa.aV(kp3);
        this.bFa.setText(resources.getString(R.string.f1334controlbar_download_clear));
        this.bFa.setPadding(0, kp4, 0, 4);
        this.bFa.H(true);
        this.um.a(this.bFa);
        this.bEZ = new ac(R.string.f1017controlbar_back, 0, 0);
        this.bEZ.bB(0, 0);
        this.bEZ.aV(kp3);
        this.bEZ.setText(resources.getString(R.string.f1017controlbar_back));
        this.bEZ.setPadding(0, kp4, 0, 4);
        this.bEZ.H(true);
        this.um.a(this.bEZ);
        this.um.kJ();
        this.um.b(this);
    }

    private void Ko() {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this);
        builder.aH(R.string.f1344dialog_title_download_task_unbreakable);
        builder.aG(R.string.f1345dialog_msg_download_task_unbreakable);
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (ActivityDownload.this.bEY != null) {
                    ActivityDownload.this.bEY.aJ();
                }
                dialogInterface.cancel();
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.fh().show();
    }

    private void Kp() {
        if (this.ah.gg()) {
            String gh = this.ah.gh();
            String gi = this.ah.gi();
            UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this);
            builder.L(gh);
            ScrollView scrollView = new ScrollView(this);
            TextView textView = new TextView(this);
            textView.setText(gi);
            textView.setTextColor(com.uc.h.e.Pb().getColor(78));
            textView.setTextSize(18.0f);
            scrollView.addView(textView);
            builder.c(scrollView);
            builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            builder.fh();
            builder.show();
        }
    }

    /* access modifiers changed from: private */
    public boolean fj(String str) {
        if (str == null) {
            return false;
        }
        return new File(str).exists();
    }

    private void j(Context context, String str) {
        if (str != null) {
            final String str2 = !str.endsWith(au.aGF) ? str + au.aGF : str;
            View inflate = LayoutInflater.from(context).inflate((int) R.layout.f896download_dialog_inputfilename, (ViewGroup) null);
            ((TextView) inflate.findViewById(R.id.f738filename)).setTextColor(com.uc.h.e.Pb().getColor(78));
            final UCAlertDialog fh = new UCAlertDialog.Builder(context).aH(R.string.f1341downLoad).c(inflate).fh();
            View findViewById = inflate.findViewById(R.id.f740dlg_ok);
            View findViewById2 = inflate.findViewById(R.id.f741dlg_cancel);
            this.bFo = (EditText) inflate.findViewById(R.id.f739download_dlg_filename);
            if (this.bFp != null) {
                this.bFo.setText(this.bFp);
            }
            AnonymousClass3 r0 = new View.OnClickListener() {
                public void onClick(View view) {
                    switch (view.getId()) {
                        case R.id.f740dlg_ok /*2131099743*/:
                            String trim = ActivityDownload.this.bFo.getText().toString().trim();
                            if (trim == null || trim.length() == 0) {
                                Toast.makeText(ActivityDownload.this, (int) R.string.f1444filenamenotnull, 1).show();
                                return;
                            } else if (ActivityDownload.this.bFp.equals(trim)) {
                                fh.dismiss();
                                return;
                            } else if (!ActivityDownload.this.fj(str2 + trim)) {
                                String unused = ActivityDownload.this.bFp = trim;
                                if (!ActivityDownload.this.ah.U(trim)) {
                                    Toast.makeText(ActivityDownload.this, (int) R.string.f1440invalid_name, 1).show();
                                    return;
                                } else if (trim.getBytes().length >= ActivityDownload.azQ) {
                                    Toast.makeText(ActivityDownload.this, (int) R.string.f1153text_max_size, 0).show();
                                    return;
                                } else {
                                    ActivityDownload.this.ah.S(ActivityDownload.this.bFp);
                                    String unused2 = ActivityDownload.this.bFp = (String) null;
                                    fh.dismiss();
                                    ActivityDownload.this.aS(10);
                                    return;
                                }
                            } else {
                                Toast.makeText(ActivityDownload.this, (int) R.string.f1443reinput_note, 1).show();
                                return;
                            }
                        case R.id.f741dlg_cancel /*2131099744*/:
                            String unused3 = ActivityDownload.this.bFp = (String) null;
                            fh.dismiss();
                            return;
                        default:
                            return;
                    }
                }
            };
            findViewById.setOnClickListener(r0);
            findViewById2.setOnClickListener(r0);
            fh.show();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean Km() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return false;
        }
        Toast.makeText(this, (int) R.string.f1389tcardnotusable, 0).show();
        return true;
    }

    public void Kn() {
        this.bFp = this.ah.gb();
        String ge = this.ah.ge();
        String gf = this.ah.gf();
        if (ge == null || !fj(ge)) {
            Toast.makeText(this, getResources().getString(R.string.f1452file_notin_sdcard), 0).show();
        } else {
            j(this, gf);
        }
    }

    public boolean Kq() {
        return this.bFz;
    }

    public void R() {
        if (this.bEY != null) {
            this.bEY.R();
        }
    }

    public void a() {
        int i;
        String[] stringArrayExtra;
        if (!e.nR().pO()) {
            Resources resources = getResources();
            e.nR().a((int) resources.getDimension(R.dimen.f338juc_text_small), (int) resources.getDimension(R.dimen.f339juc_text_medium), (int) resources.getDimension(R.dimen.f340juc_text_large), (int) resources.getDimension(R.dimen.f341juc_download_xoffset), (int) resources.getDimension(R.dimen.f342juc_download_buttontext_size), (int) resources.getDimension(R.dimen.f343juc_download_filenametext_size), (int) resources.getDimension(R.dimen.f344juc_download_speedtext_size), (int) resources.getDimension(R.dimen.f345juc_download_statebar_size), (int) resources.getDimension(R.dimen.f346juc_download_itemtext_size), resources.getDimension(R.dimen.f347juc_multiple_font), resources.getDisplayMetrics().xdpi, new float[]{resources.getDimension(R.dimen.f403unit_pt), resources.getDimension(R.dimen.f404unit_in), resources.getDimension(R.dimen.f405unit_mm)}, (int) resources.getDimension(R.dimen.f406wap10_line_space));
            e.nR().e((n) null);
            e.nR().oE();
            b.a.a.d.d(this);
        }
        ActivityBrowser.g(this);
        requestWindowFeature(1);
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        int i2 = getResources().getConfiguration().orientation;
        int dimension = (int) getResources().getDimension(R.dimen.f237statusbar_height);
        int dimension2 = (int) getResources().getDimension(R.dimen.f236titlebar_height);
        int dimension3 = (int) getResources().getDimension(R.dimen.f239controlbar_port_height);
        int dimension4 = (int) getResources().getDimension(R.dimen.f238controlbar_land_height);
        if (!"0".equals(e.nR().nY().bw(com.uc.a.h.afK))) {
            dimension = -1;
        }
        if (2 == i2) {
            this.bFq = width;
            this.bFr = height;
            this.bFs = height;
            this.bFt = width;
            i = ((height - dimension) - dimension4) - dimension2;
        } else {
            this.bFs = width;
            this.bFt = height;
            this.bFq = height;
            this.bFr = width;
            i = ((height - dimension) - dimension3) - dimension2;
        }
        this.ah = e.nR().nX();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean("need_destroy", false).commit();
        this.bEY = new ViewDownload(this, width, i);
        RelativeLayout relativeLayout = (RelativeLayout) ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.f894download, (ViewGroup) null);
        ((RelativeLayout) relativeLayout.findViewById(R.id.f734download_content)).addView(this.bEY);
        setContentView(relativeLayout);
        Kl();
        Intent intent = getIntent();
        if (intent == null || (stringArrayExtra = intent.getStringArrayExtra("downloadinfo")) == null) {
            e.nR().qt();
        } else {
            a(0, stringArrayExtra);
        }
        this.bFx = getResources().getColor(R.color.f108controlbar_text);
        this.bFy = getResources().getColor(R.color.f109controlbar_text_disable);
        if (e.nR().op()) {
            ci(true);
        } else {
            ci(false);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i, Object obj) {
        this.mHandler.sendMessage(Message.obtain(null, i, obj));
    }

    /* access modifiers changed from: package-private */
    public void aS(int i) {
        this.mHandler.sendMessage(Message.obtain((Handler) null, i));
    }

    public void b(z zVar, int i) {
        switch (i) {
            case R.string.f1017controlbar_back /*2131296324*/:
                PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean("need_destroy", true).commit();
                finish();
                e.nR().aN();
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(87);
                    return;
                }
                return;
            case R.string.f1334controlbar_download_clear /*2131296641*/:
                if (this.bEY != null) {
                    iy(1);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void ci(boolean z) {
        if (z) {
            if (this.bFa != null) {
                this.bFa.H(true);
            }
        } else if (this.bFa != null) {
            this.bFa.H(false);
        }
        this.um.invalidate();
    }

    public void cj(boolean z) {
        PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean("need_destroy", z).commit();
    }

    public void dt() {
        if (this.bEY != null && this.bEY.B()) {
            this.bEY.b(false);
            openContextMenu(this.bEY);
        }
    }

    public void fk(String str) {
        b.a.a.d.a(this, str, b.a.a.d.cu(str));
    }

    public void iy(int i) {
        this.bFn = i;
        if (1 != this.bFn || this.ah.ga() != 0) {
            UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this);
            builder.a(new String[]{"只删除任务", "删除任务和源文件"}, -1, false, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    switch (i) {
                        case 0:
                            if (ActivityDownload.this.bFn != 0) {
                                ActivityDownload.this.ah.u(false);
                                break;
                            } else {
                                ActivityDownload.this.bEY.d(false);
                                break;
                            }
                        case 1:
                            if (!ActivityDownload.this.Km()) {
                                if (ActivityDownload.this.bFn != 0) {
                                    ActivityDownload.this.ah.u(true);
                                    break;
                                } else {
                                    ActivityDownload.this.bEY.d(true);
                                    break;
                                }
                            }
                            break;
                    }
                    if (dialogInterface != null) {
                        dialogInterface.dismiss();
                    }
                }
            });
            if (this.bFn == 0) {
                builder.L("删除当前选中项");
            } else if (1 == this.bFn) {
                if (1 == this.ah.ga()) {
                    builder.L("删除未下载成功项");
                } else if (2 == this.ah.ga()) {
                    builder.L("删除已下载成功项");
                }
            }
            builder.c((int) R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            builder.fh().show();
        }
    }

    public void iz(int i) {
        if (-1 != i && !Km()) {
            switch (i) {
                case 0:
                    if (!this.bEY.aI()) {
                        Ko();
                        return;
                    } else {
                        this.bEY.aJ();
                        return;
                    }
                case 1:
                    this.bEY.aL();
                    return;
                case 2:
                    this.bEY.aL();
                    return;
                case 3:
                    Kp();
                    this.bEY.aS();
                    return;
                case 4:
                case 5:
                case 6:
                    String ge = this.ah.ge();
                    if (ge == null || !new File(ge).exists()) {
                        Toast.makeText(this, (int) R.string.f1521file_not_found, 1).show();
                        return;
                    }
                    b.a.a.d.a(this, ge, b.a.a.d.cu(ge));
                    this.bEY.aS();
                    return;
                default:
                    return;
            }
        }
    }

    public void k() {
        com.uc.h.e Pb = com.uc.h.e.Pb();
        e.nR().a(Pb.getDrawable(UCR.drawable.aWB), Pb.getDrawable(UCR.drawable.aWC), Pb.getColor(77), Pb.getColor(47), Pb.getColor(47), Pb.getColor(108));
        this.um.k();
        ((TitleBarTextView) findViewById(R.id.f654Browser_TitleBar)).k();
        if (this.bEY != null) {
            this.bEY.a(this, 0, 0);
        }
        this.um.invalidate();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 86 && i2 == -1) {
            PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean("need_destroy", true).commit();
            finish();
            e.nR().aN();
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(87);
            }
            finish();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        ActivityBrowser.a(this, configuration);
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        boolean onContextItemSelected = super.onContextItemSelected(menuItem);
        if (1 != this.bEY.aO()) {
            if (2 == this.bEY.aO()) {
                switch (menuItem.getItemId()) {
                    case com.uc.d.h.ccI /*393217*/:
                        if (!Km()) {
                            Kn();
                            break;
                        }
                        break;
                    case com.uc.d.h.ccJ /*393218*/:
                        if (!Km()) {
                            this.bEY.aM();
                            break;
                        }
                        break;
                    case com.uc.d.h.ccK /*393219*/:
                        iy(0);
                        break;
                    case com.uc.d.h.ccL /*393220*/:
                        Kp();
                        break;
                    case com.uc.d.h.ccM /*393221*/:
                        if (ModelBrowser.gD().hj().wg()) {
                            ModelBrowser.gD().hj().cJ(e.nR().ql());
                        } else {
                            ModelBrowser.gD().X(e.nR().ql());
                        }
                        finish();
                        break;
                    case com.uc.d.h.ccN /*393222*/:
                        if (ModelBrowser.gD().hj().wg()) {
                            ModelBrowser.gD().hj().cJ(e.nR().qm());
                        } else {
                            ModelBrowser.gD().X(e.nR().qm());
                        }
                        finish();
                        break;
                }
            }
        } else {
            switch (menuItem.getItemId()) {
                case com.uc.d.h.ccE /*327681*/:
                    if (!Km()) {
                        this.bEY.aM();
                        break;
                    }
                    break;
                case com.uc.d.h.ccF /*327682*/:
                    iy(0);
                    break;
                case com.uc.d.h.ccG /*327683*/:
                    Kp();
                    break;
            }
        }
        return onContextItemSelected;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        String action;
        super.onCreate(bundle);
        if (!e.nR().pO()) {
            finish();
            return;
        }
        a();
        Intent intent = getIntent();
        if (intent != null && (action = intent.getAction()) != null) {
            if (action.equals("com.uc.browser.openDownloadList")) {
                e.nR().qs();
            } else if (action.equals("com.uc.browser.clickDownloadNotification")) {
                this.bEY.aQ();
            }
        }
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        if (view == this.bEY) {
            int gl = e.nR().nX() != null ? e.nR().nX().gl() : 0;
            if (1 == gl) {
                g.a(this, com.uc.d.h.ccH, contextMenu);
                contextMenu.setHeaderTitle("下载管理");
                if (this.bEY.aP() != 0) {
                    contextMenu.findItem(com.uc.d.h.ccE).setVisible(true);
                } else {
                    contextMenu.findItem(com.uc.d.h.ccE).setVisible(false);
                }
                contextMenu.findItem(com.uc.d.h.ccF).setVisible(true);
                contextMenu.findItem(com.uc.d.h.ccG).setVisible(true);
            } else if (2 == gl) {
                g.a(this, com.uc.d.h.ccO, contextMenu);
                contextMenu.setHeaderTitle("下载管理");
                contextMenu.findItem(com.uc.d.h.ccI).setVisible(true);
                contextMenu.findItem(com.uc.d.h.ccJ).setVisible(true);
                contextMenu.findItem(com.uc.d.h.ccK).setVisible(true);
                contextMenu.findItem(com.uc.d.h.ccL).setVisible(true);
                if (e.nR().qq()) {
                    String gb = e.nR().nX().gb();
                    if (gb == null || (!gb.endsWith(".jar") && !gb.endsWith(".jad") && !gb.endsWith(".apk"))) {
                        contextMenu.findItem(com.uc.d.h.ccN).setVisible(false);
                        contextMenu.findItem(com.uc.d.h.ccM).setVisible(false);
                        return;
                    }
                    contextMenu.findItem(com.uc.d.h.ccN).setVisible(true);
                    contextMenu.findItem(com.uc.d.h.ccM).setVisible(true);
                    return;
                }
                contextMenu.findItem(com.uc.d.h.ccN).setVisible(false);
                contextMenu.findItem(com.uc.d.h.ccM).setVisible(false);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (true == PreferenceManager.getDefaultSharedPreferences(this).getBoolean("need_destroy", false) && this.bEY != null) {
            PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean("need_destroy", false).commit();
            this.bEY.aN();
            this.bEY.destroyDrawingCache();
            this.bEY = null;
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(87);
            }
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 == i) {
            PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean("need_destroy", true).commit();
            finish();
            e.nR().aN();
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(87);
            }
            return true;
        }
        if (this.bEY != null) {
            this.bEY.onKeyDown(i, keyEvent);
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i == 84) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().a((int) ModelBrowser.zJ, this);
            }
            return true;
        }
        if (this.bEY != null) {
            this.bEY.onKeyUp(i, keyEvent);
        }
        return super.onKeyUp(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        String action;
        if (intent != null && (action = intent.getAction()) != null) {
            if (action.equals("com.uc.browser.openDownloadList")) {
                e.nR().qs();
            } else if (action.equals("com.uc.browser.clickDownloadNotification")) {
                this.bEY.aQ();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.bFz = false;
        if (this.bEY != null) {
            this.ah.fZ();
        }
        com.uc.h.e.Pb().b(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.bFz = true;
        if (!e.nR().pO()) {
            Resources resources = getResources();
            e.nR().a((int) resources.getDimension(R.dimen.f338juc_text_small), (int) resources.getDimension(R.dimen.f339juc_text_medium), (int) resources.getDimension(R.dimen.f340juc_text_large), (int) resources.getDimension(R.dimen.f341juc_download_xoffset), (int) resources.getDimension(R.dimen.f342juc_download_buttontext_size), (int) resources.getDimension(R.dimen.f343juc_download_filenametext_size), (int) resources.getDimension(R.dimen.f344juc_download_speedtext_size), (int) resources.getDimension(R.dimen.f345juc_download_statebar_size), (int) resources.getDimension(R.dimen.f346juc_download_itemtext_size), resources.getDimension(R.dimen.f347juc_multiple_font), resources.getDisplayMetrics().xdpi, new float[]{resources.getDimension(R.dimen.f403unit_pt), resources.getDimension(R.dimen.f404unit_in), resources.getDimension(R.dimen.f405unit_mm)}, (int) resources.getDimension(R.dimen.f406wap10_line_space));
            e.nR().e((n) null);
            e.nR().oE();
            b.a.a.d.d(this);
            a();
        }
        k();
        com.uc.h.e.Pb().a(this);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z && ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(this);
        }
        if (z && ActivityBrowser.Fz()) {
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
            }
            ActivityBrowser.e(this);
        }
    }
}
