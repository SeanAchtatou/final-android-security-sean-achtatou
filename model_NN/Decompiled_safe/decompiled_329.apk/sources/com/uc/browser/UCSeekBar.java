package com.uc.browser;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.SeekBar;
import com.uc.browser.UCR;
import com.uc.e.b.k;
import com.uc.h.e;

public class UCSeekBar extends SeekBar {
    private Drawable pO;
    private Drawable pP;
    private Drawable pQ;
    private int tT = 25;

    public UCSeekBar(Context context) {
        super(context);
        a();
    }

    public UCSeekBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public UCSeekBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private void a() {
        e Pb = e.Pb();
        Bitmap kl = Pb.kl(UCR.drawable.aWl);
        Bitmap kl2 = Pb.kl(UCR.drawable.aWm);
        Matrix matrix = new Matrix();
        matrix.setRotate(180.0f);
        Bitmap createBitmap = Bitmap.createBitmap(kl2, 0, 0, kl2.getWidth(), kl2.getHeight(), matrix, true);
        Bitmap kl3 = Pb.kl(UCR.drawable.aWn);
        Bitmap kl4 = Pb.kl(UCR.drawable.aWo);
        this.pP = new k(new Bitmap[]{createBitmap, kl, kl2}, new byte[]{0, 3, 0}, (byte) 0);
        this.pQ = new k(new Bitmap[]{kl4, kl3, kl3}, new byte[]{0, 3, 0}, (byte) 0);
        this.pO = Pb.getDrawable(UCR.drawable.kU);
        this.tT = kl2.getHeight();
    }

    public void draw(Canvas canvas) {
        int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
        int width2 = (getWidth() - width) / 2;
        int height = (getHeight() - this.tT) / 2;
        int height2 = (getHeight() + this.tT) / 2;
        int progress = ((getProgress() * width) / getMax()) + width2;
        this.pP.setBounds(new Rect(width2, height, width + width2, height2));
        this.pP.draw(canvas);
        this.pQ.setBounds(new Rect(width2, height, progress, height2));
        this.pQ.draw(canvas);
        this.pO.setBounds(progress - (this.pO.getIntrinsicWidth() / 2), (getHeight() - this.pO.getIntrinsicHeight()) / 2, (this.pO.getIntrinsicWidth() / 2) + progress, (getHeight() + this.pO.getIntrinsicHeight()) / 2);
        this.pO.draw(canvas);
    }
}
