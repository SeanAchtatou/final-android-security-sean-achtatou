package com.uc.browser;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import b.a.a.f;
import com.uc.browser.UCR;
import com.uc.e.a.p;
import com.uc.e.ac;
import com.uc.e.ad;
import com.uc.e.h;
import com.uc.e.r;
import com.uc.e.z;
import com.uc.h.a;
import com.uc.h.e;
import java.util.Iterator;
import java.util.Vector;

public class ViewMainBar extends View implements ad, a {
    private static Vector HG;
    private static boolean HH;
    private static boolean HI;
    private static boolean HJ;
    private static boolean HK;
    private static boolean HL;
    private static int HM = 1;
    private r HN;
    private ac HO;
    private ac HP;
    private ac HQ;
    private ac HR;
    protected p HS;
    private ac HT;
    private ac HU;
    private boolean HV = false;
    private h HW = new h() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.uc.browser.ModelBrowser.a(int, int, java.lang.Object):void
         arg types: [int, int, boolean]
         candidates:
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object, int):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String, java.lang.String):void
          com.uc.browser.ModelBrowser.a(java.lang.String, java.util.HashMap, int):void
          com.uc.browser.ModelBrowser.a(int, int, long):void
          com.uc.browser.ModelBrowser.a(int, java.lang.String, boolean):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, java.lang.String, java.lang.String):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.WebViewJUC, int, int):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, com.uc.browser.WindowUCWeb, int):void
          com.uc.browser.ModelBrowser.a(int, int, java.lang.Object):void */
        public void b(z zVar, int i) {
            ModelBrowser gD = ModelBrowser.gD();
            if (gD != null) {
                if (gD != null) {
                    gD.aS(38);
                }
                gD.a(83, 0, (Object) false);
                switch (i) {
                    case UCR.drawable.aJJ:
                    case UCR.drawable.aUe:
                    case UCR.drawable.aKa:
                        gD.aS(40);
                        if (true == gD.io()) {
                            ViewMainBar.this.hy();
                            ViewMainBar.this.bC(1);
                        }
                        gD.hl();
                        return;
                    case UCR.drawable.aUf:
                    case UCR.drawable.aUh:
                    case UCR.drawable.aUi:
                    case UCR.drawable.aUj:
                    case UCR.drawable.aUk:
                    default:
                        return;
                    case UCR.drawable.aUg:
                        if (true == gD.io()) {
                            ViewMainBar.this.hy();
                            ViewMainBar.this.bC(1);
                        }
                        gD.m(true);
                        gD.a((Integer) 1);
                        f.l(0, f.asT);
                        gD.hl();
                        return;
                    case UCR.drawable.aJQ:
                        if (true == gD.io()) {
                            ViewMainBar.this.hy();
                            ViewMainBar.this.bC(1);
                        }
                        gD.aS(10);
                        f.l(0, f.asO);
                        gD.hl();
                        return;
                    case UCR.drawable.aJS:
                        if (!gD.io()) {
                            ViewMainBar.this.kG();
                        } else {
                            ViewMainBar.this.hy();
                            ViewMainBar.this.bC(0);
                        }
                        gD.hl();
                        return;
                    case UCR.drawable.aKc:
                        if (gD.hj() != null) {
                            gD.hm();
                            if (true == gD.io()) {
                                ViewMainBar.this.hy();
                                ViewMainBar.this.bC(1);
                            }
                            f.l(0, f.asX);
                            return;
                        }
                        return;
                }
            }
        }
    };

    public ViewMainBar(Context context) {
        super(context);
        a();
        if (HG == null) {
            HG = new Vector();
        }
        HG.add(this);
        kH();
    }

    public ViewMainBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
        if (HG == null) {
            HG = new Vector();
        }
        HG.add(this);
        kH();
    }

    public static void P(boolean z) {
        if (HG != null) {
            HI = z;
            Iterator it = HG.iterator();
            while (it.hasNext()) {
                ViewMainBar viewMainBar = (ViewMainBar) it.next();
                viewMainBar.HO.H(!z);
                viewMainBar.invalidate();
            }
        }
    }

    public static void Q(boolean z) {
        if (HG != null) {
            HL = z;
            Iterator it = HG.iterator();
            while (it.hasNext()) {
                ViewMainBar viewMainBar = (ViewMainBar) it.next();
                viewMainBar.HR.H(z);
                viewMainBar.postInvalidate();
            }
        }
    }

    public static void R(boolean z) {
        if (ModelBrowser.gD() != null) {
            a(z, ModelBrowser.gD().canGoBack(), ModelBrowser.gD().bo());
        }
    }

    public static void S(boolean z) {
        if (HG != null) {
            MultiWindowManager hj = ModelBrowser.gD().hj();
            HM = hj == null ? 1 : hj.wh();
            Iterator it = HG.iterator();
            while (it.hasNext()) {
                ViewMainBar viewMainBar = (ViewMainBar) it.next();
                if (viewMainBar.HS != null) {
                    viewMainBar.HS.hc(HM);
                    if (z) {
                        viewMainBar.invalidate();
                    }
                } else {
                    return;
                }
            }
        }
    }

    public static void a(boolean z, boolean z2, boolean z3) {
        if (HG != null) {
            HJ = z2;
            HK = z3;
            HH = z;
            Iterator it = HG.iterator();
            while (it.hasNext()) {
                ViewMainBar viewMainBar = (ViewMainBar) it.next();
                if (z) {
                    viewMainBar.HP.setVisibility(2);
                    viewMainBar.HU.setVisibility(2);
                    viewMainBar.HQ.setVisibility(0);
                    viewMainBar.kJ();
                    viewMainBar.postInvalidate();
                } else {
                    viewMainBar.HQ.setVisibility(2);
                    viewMainBar.HP.setVisibility(0);
                    if (ModelBrowser.gD() != null) {
                        d(z2, z3);
                    }
                    viewMainBar.kJ();
                    viewMainBar.postInvalidate();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void bC(int i) {
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(117, i, (Object) null);
        }
    }

    private static void d(boolean z, boolean z2) {
        if (HG != null) {
            Iterator it = HG.iterator();
            while (it.hasNext()) {
                ViewMainBar viewMainBar = (ViewMainBar) it.next();
                if (true == z2) {
                    if (viewMainBar.HP != null) {
                        viewMainBar.HP.setVisibility(2);
                    }
                    if (viewMainBar.HU != null) {
                        viewMainBar.HU.setVisibility(0);
                    }
                } else {
                    if (viewMainBar.HU != null) {
                        viewMainBar.HU.setVisibility(2);
                    }
                    if (viewMainBar.HP != null) {
                        viewMainBar.HP.H(z);
                    }
                }
                viewMainBar.kJ();
                viewMainBar.postInvalidate();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
     arg types: [int, boolean]
     candidates:
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void */
    /* access modifiers changed from: private */
    public void kG() {
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(118, (Object) true);
        }
    }

    private void kH() {
        this.HS.hc(HM);
        this.HR.H(HL);
        this.HO.H(!HI);
        if (HH) {
            this.HP.setVisibility(2);
            this.HU.setVisibility(2);
            this.HQ.setVisibility(0);
        } else if (HK) {
            this.HP.setVisibility(2);
            this.HU.setVisibility(0);
        } else {
            this.HP.setVisibility(0);
            this.HP.H(HJ);
            this.HU.setVisibility(2);
        }
        kJ();
    }

    public static void kI() {
        S(false);
    }

    /* access modifiers changed from: protected */
    public void a() {
        e Pb = e.Pb();
        Pb.a(this);
        int kp = Pb.kp(R.dimen.f186controlbar_item_width_5);
        int kp2 = Pb.kp(R.dimen.f177controlbar_height);
        this.HN = new r();
        this.HN.a(this);
        this.HN.aw(kp, kp2);
        this.HN.eB(2);
        int kp3 = Pb.kp(R.dimen.f190controlbar_button_image_height);
        int kp4 = Pb.kp(R.dimen.f191controlbar_button_image_width);
        int kp5 = Pb.kp(R.dimen.f188controlbar_text_size);
        int kp6 = Pb.kp(R.dimen.f187controlbar_item_paddingTop);
        this.HP = new ac(UCR.drawable.aUe, 0, 0);
        this.HP.bB(kp4, kp3);
        this.HP.aV(kp5);
        this.HP.setText(getResources().getString(R.string.f1011controlbar_backward));
        this.HP.setPadding(0, kp6, 0, 4);
        this.HP.H(false);
        this.HN.b(this.HP);
        this.HQ = new ac(UCR.drawable.aKa, 0, 0);
        this.HQ.bB(kp4, kp3);
        this.HQ.aV(kp5);
        this.HQ.setText(getResources().getString(R.string.f1012controlbar_stop));
        this.HQ.setPadding(0, kp6, 0, 4);
        this.HQ.setVisibility(2);
        this.HN.b(this.HQ);
        this.HU = new ac(UCR.drawable.aJJ, 0, 0);
        this.HU.bB(kp4, kp3);
        this.HU.aV(kp5);
        this.HU.setText(getResources().getString(R.string.f1017controlbar_back));
        this.HU.setPadding(0, kp6, 0, 4);
        this.HU.setVisibility(2);
        this.HN.b(this.HU);
        this.HR = new ac(UCR.drawable.aUg, 0, 0);
        this.HR.bB(kp4, kp3);
        this.HR.aV(kp5);
        this.HR.setText(getResources().getString(R.string.f1013controlbar_forward));
        this.HR.setPadding(0, kp6, 0, 4);
        this.HR.H(false);
        this.HN.b(this.HR);
        this.HT = new ac(UCR.drawable.aJS, 0, 0);
        this.HT.bB(kp4, kp3);
        this.HT.aV(kp5);
        this.HT.setText(getResources().getString(R.string.f1016controlbar_menu));
        this.HT.setPadding(0, kp6, 0, 4);
        this.HN.b(this.HT);
        this.HS = new p(UCR.drawable.aKc, kp, kp2);
        this.HS.bB(kp4, kp3);
        this.HS.aV(kp5);
        this.HS.setText(getResources().getString(R.string.f1014controlbar_window));
        this.HS.setPadding(0, kp6, 0, 4);
        this.HS.he(Pb.kp(R.dimen.f180controlbar_winnum_margin_left));
        this.HS.hf(Pb.kp(R.dimen.f181controlbar_winnum_margin_top));
        this.HS.hd(Pb.kp(R.dimen.f189controlbar_winnum_text_size));
        S(false);
        this.HN.b(this.HS);
        this.HS.a(this);
        this.HO = new ac(UCR.drawable.aJQ, 0, 0);
        this.HO.bB(kp4, kp3);
        this.HO.aV(kp5);
        this.HO.setText(getResources().getString(R.string.f1010controlbar_homepage));
        this.HO.setPadding(0, kp6, 0, 4);
        this.HN.b(this.HO);
        k();
        this.HN.yi();
        this.HN.c(this.HW);
    }

    public void a(ac acVar) {
        this.HN.b(acVar);
    }

    public void a(ad adVar) {
        if (adVar != null) {
            this.HN.a(adVar);
        } else {
            this.HN.a(this);
        }
    }

    public void b(h hVar) {
        this.HN.c(hVar);
    }

    public void bD(int i) {
        this.HN.eB(i);
    }

    public void cJ() {
        invalidate();
    }

    public void clear() {
        this.HN.clear();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
     arg types: [int, int]
     candidates:
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void */
    public void hy() {
        if (true == this.HV && ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(35, (Object) 1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
     arg types: [int, int]
     candidates:
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void */
    public void hz() {
        if (true == this.HV && ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(35, (Object) 0);
            bringToFront();
        }
    }

    public void k() {
        e Pb = e.Pb();
        this.HN.s(Pb.getDrawable(UCR.drawable.aXp));
        this.HN.m(Pb.getDrawable(UCR.drawable.aWt));
        this.HU.O(Pb.getDrawable(UCR.drawable.aJJ));
        this.HQ.O(Pb.getDrawable(UCR.drawable.aKa));
        this.HP.O(Pb.getDrawable(UCR.drawable.aUe));
        this.HR.O(Pb.getDrawable(UCR.drawable.aUg));
        this.HT.O(Pb.getDrawable(UCR.drawable.aJS));
        this.HS.O(Pb.getDrawable(UCR.drawable.aKc));
        this.HS.hg(Pb.getColor(12));
        this.HO.O(Pb.getDrawable(UCR.drawable.aJQ));
    }

    public r kF() {
        return this.HN;
    }

    public void kJ() {
        this.HN.yi();
    }

    public void kK() {
        this.HS.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.f25winnum_anim));
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.HN.draw(canvas);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 4:
                if (true == ModelBrowser.gD().io()) {
                    hy();
                    bC(0);
                    return true;
                }
                break;
        }
        return false;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        switch (i) {
            case 82:
                if (!ModelBrowser.gD().io()) {
                    hz();
                    kG();
                    return true;
                }
                hy();
                bC(0);
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.HN.setSize(i3 - i, i4 - i2);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean onTouchEvent = super.onTouchEvent(motionEvent);
        if (this.HN.d(motionEvent)) {
            return true;
        }
        return onTouchEvent;
    }
}
