package com.uc.browser;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import b.a.a.e;
import com.uc.a.i;
import com.uc.browser.TabContainer;
import com.uc.browser.UCR;
import com.uc.c.bh;
import com.uc.e.ab;
import com.uc.e.f;
import com.uc.e.m;
import com.uc.e.z;
import com.uc.h.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;

public class BookmarkTabContainer extends TabContainer implements m, a {
    public static final int FA = 0;
    public static final int FB = 1;
    private static final String FC = "书签";
    private static final String FD = "历史";
    /* access modifiers changed from: private */
    public int EN;
    private int EO;
    /* access modifiers changed from: private */
    public int EP;
    /* access modifiers changed from: private */
    public int EQ;
    /* access modifiers changed from: private */
    public int ER;
    /* access modifiers changed from: private */
    public int ES;
    /* access modifiers changed from: private */
    public int ET;
    /* access modifiers changed from: private */
    public int EU;
    /* access modifiers changed from: private */
    public int EV;
    /* access modifiers changed from: private */
    public int EW;
    /* access modifiers changed from: private */
    public int EX;
    /* access modifiers changed from: private */
    public int EY;
    /* access modifiers changed from: private */
    public int EZ;
    /* access modifiers changed from: private */
    public com.uc.a.m FE;
    private i FF;
    private boolean FG = true;
    private f FH;
    private f FI;
    /* access modifiers changed from: private */
    public BookmarkDelegate FJ;
    /* access modifiers changed from: private */
    public DataViewPool FK = new DataViewPool();
    /* access modifiers changed from: private */
    public int Fa;
    /* access modifiers changed from: private */
    public int Fb;
    private int Fc;
    /* access modifiers changed from: private */
    public int Fd;
    /* access modifiers changed from: private */
    public Bitmap Fe;
    /* access modifiers changed from: private */
    public Bitmap Ff;
    /* access modifiers changed from: private */
    public Bitmap Fg;
    /* access modifiers changed from: private */
    public Bitmap Fh;
    /* access modifiers changed from: private */
    public Bitmap Fi;
    /* access modifiers changed from: private */
    public Bitmap Fj;
    /* access modifiers changed from: private */
    public Bitmap Fk;
    /* access modifiers changed from: private */
    public Bitmap Fl;
    private Bitmap Fm;
    private Bitmap Fn;
    private Bitmap Fo;
    private Bitmap Fp;
    private Bitmap Fq;
    private Bitmap Fr;
    private Drawable Fs;
    private Drawable Ft;
    private Drawable Fu;
    private Drawable Fv;
    private Drawable Fw;
    private Drawable Fx;
    private Drawable Fy;
    private Drawable Fz;

    public interface BookmarkDelegate {
        void a(e eVar, int i, int i2, int i3);

        void a(e eVar, int i, boolean z);

        void b(e eVar, int i, int i2, int i3);
    }

    public class BookmarkExpandableList extends f {
        Vector qm;

        public BookmarkExpandableList() {
        }

        public void a(ab abVar) {
            if (this.qm == null) {
                this.qm = new Vector();
            }
            if (!this.qm.contains(Integer.valueOf(abVar.getId()))) {
                this.qm.add(Integer.valueOf(abVar.getId()));
            }
            super.a(abVar);
        }

        public void b(ab abVar) {
            this.qm.removeElement(Integer.valueOf(abVar.getId()));
            super.b(abVar);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.uc.e.f.a(com.uc.e.ab, boolean):void
         arg types: [com.uc.e.ab, int]
         candidates:
          com.uc.e.f.a(int, com.uc.e.z):void
          com.uc.e.f.a(android.graphics.drawable.Drawable, int):void
          com.uc.e.f.a(android.graphics.drawable.Drawable[], int):void
          com.uc.e.f.a(android.graphics.drawable.Drawable[], int[]):void
          com.uc.e.s.a(android.graphics.Canvas, int):void
          com.uc.e.s.a(android.graphics.drawable.Drawable, int):void
          com.uc.e.s.a(java.util.Collection, int):void
          com.uc.e.u.a(java.util.Collection, int):void
          com.uc.e.f.a(com.uc.e.ab, boolean):void */
        public void c(Vector vector) {
            super.c(vector);
            if (vector != null) {
                Iterator it = new ArrayList(vector).iterator();
                while (it.hasNext()) {
                    z zVar = (z) it.next();
                    if (this.qm != null && this.qm.contains(Integer.valueOf(zVar.getId())) && (zVar instanceof ab)) {
                        a((ab) zVar, true);
                    }
                }
            }
            zf();
        }
    }

    public class BookmarkFileItemView extends z implements URLItemView {
        private e Go;
        private Paint bWV = new Paint(1);
        /* access modifiers changed from: private */
        public String bWW = null;
        private int bmw;
        /* access modifiers changed from: private */
        public String bmy = null;

        public BookmarkFileItemView(e eVar) {
            this.Go = eVar;
            this.pL.setColor(BookmarkTabContainer.this.EX);
            this.pL.setTextSize((float) BookmarkTabContainer.this.ET);
            this.bWV.setColor(BookmarkTabContainer.this.EY);
            this.bWV.setTextSize((float) BookmarkTabContainer.this.EU);
            this.bmw = BookmarkTabContainer.this.EN;
        }

        public void b(e eVar) {
            this.Go = eVar;
        }

        public void draw(Canvas canvas) {
            byte Iz = Iz();
            if (Iz == 0) {
                this.pL.setColor(BookmarkTabContainer.this.EX);
                this.bWV.setColor(BookmarkTabContainer.this.EY);
            } else if (Iz == 2 || Iz == 1) {
                this.pL.setColor(BookmarkTabContainer.this.Fa);
                this.bWV.setColor(BookmarkTabContainer.this.Fb);
            }
            canvas.drawBitmap(BookmarkTabContainer.this.Fe, (float) this.bmw, (float) ((getHeight() - BookmarkTabContainer.this.Fe.getHeight()) / 2), (Paint) null);
            int width = this.bmw + BookmarkTabContainer.this.Fe.getWidth() + BookmarkTabContainer.this.EQ;
            if (this.bmy == null) {
                this.bmy = BookmarkTabContainer.this.a(this.Go.abM, (this.aSP - width) - BookmarkTabContainer.this.ER, this.pL);
            }
            canvas.drawText(this.bmy, (float) width, (float) (getHeight() / 2), this.pL);
            if (this.bWW == null) {
                this.bWW = BookmarkTabContainer.this.a(this.Go.abL, (this.aSP - width) - BookmarkTabContainer.this.ER, this.bWV);
            }
            canvas.drawText(this.bWW, (float) width, ((float) ((getHeight() / 2) + 3)) - this.bWV.ascent(), this.bWV);
        }

        public void hi(int i) {
            this.bmw = i;
        }

        public e ka() {
            return this.Go;
        }
    }

    public class BookmarkFolderItemView extends ab implements URLItemView {
        private e Go;

        public BookmarkFolderItemView(e eVar) {
            this.Go = eVar;
            w(lc());
            this.pL.setColor(BookmarkTabContainer.this.EW);
            this.pL.setTextSize((float) BookmarkTabContainer.this.ES);
        }

        private Vector lc() {
            Vector vector = new Vector();
            Vector e = BookmarkTabContainer.this.FE.e(this.Go);
            if (e != null) {
                Iterator it = e.iterator();
                while (it.hasNext()) {
                    BookmarkFileItemView l = BookmarkTabContainer.this.FK.l((e) it.next());
                    l.hi(BookmarkTabContainer.this.EP);
                    vector.add(l);
                }
            }
            return vector;
        }

        public void b(e eVar) {
            this.Go = eVar;
            w(lc());
        }

        public void draw(Canvas canvas) {
            byte Iz = Iz();
            if (Iz == 0) {
                this.pL.setColor(BookmarkTabContainer.this.EW);
            } else if (Iz == 2 || Iz == 1) {
                this.pL.setColor(BookmarkTabContainer.this.EZ);
            }
            canvas.drawBitmap(BookmarkTabContainer.this.Ff, (float) BookmarkTabContainer.this.EN, (float) ((getHeight() - BookmarkTabContainer.this.Ff.getHeight()) / 2), (Paint) null);
            Bitmap r = this.bRD ? BookmarkTabContainer.this.Fh : BookmarkTabContainer.this.Fg;
            int e = BookmarkTabContainer.this.EN + BookmarkTabContainer.this.Ff.getWidth() + BookmarkTabContainer.this.EQ;
            int width = (this.aSP - r.getWidth()) - 13;
            canvas.drawText(BookmarkTabContainer.this.a(this.Go.abM, (width - e) - BookmarkTabContainer.this.ER, this.pL), (float) e, (((float) getHeight()) - this.pL.ascent()) / 2.0f, this.pL);
            canvas.drawBitmap(r, (float) width, (float) ((getHeight() - r.getHeight()) / 2), (Paint) null);
        }

        public e ka() {
            return this.Go;
        }
    }

    class DataViewPool {
        private LinkedList bxd;
        private LinkedList bxe;
        private LinkedList bxf;
        private LinkedList bxg;

        private DataViewPool() {
        }

        public boolean a(BookmarkFileItemView bookmarkFileItemView) {
            if (this.bxe == null) {
                this.bxe = new LinkedList();
            }
            this.bxe.add(bookmarkFileItemView);
            return true;
        }

        public boolean a(BookmarkFolderItemView bookmarkFolderItemView) {
            if (this.bxd == null) {
                this.bxd = new LinkedList();
            }
            this.bxd.add(bookmarkFolderItemView);
            return true;
        }

        public boolean a(HistoryFileItemView historyFileItemView) {
            if (this.bxg == null) {
                this.bxg = new LinkedList();
            }
            this.bxg.add(historyFileItemView);
            return true;
        }

        public boolean a(HistoryFolderItemView historyFolderItemView) {
            if (this.bxf == null) {
                this.bxf = new LinkedList();
            }
            this.bxf.add(historyFolderItemView);
            return true;
        }

        public HistoryFolderItemView c(int i, Vector vector, boolean z) {
            if (this.bxf == null || this.bxf.size() == 0) {
                return new HistoryFolderItemView(i, vector, z);
            }
            HistoryFolderItemView historyFolderItemView = (HistoryFolderItemView) this.bxf.getFirst();
            this.bxf.remove();
            historyFolderItemView.z((byte) 0);
            historyFolderItemView.b(i, vector, z);
            return historyFolderItemView;
        }

        public BookmarkFolderItemView k(e eVar) {
            if (this.bxd == null || this.bxd.size() == 0) {
                return new BookmarkFolderItemView(eVar);
            }
            BookmarkFolderItemView bookmarkFolderItemView = (BookmarkFolderItemView) this.bxd.getFirst();
            this.bxd.remove();
            bookmarkFolderItemView.z((byte) 0);
            bookmarkFolderItemView.b(eVar);
            return bookmarkFolderItemView;
        }

        public BookmarkFileItemView l(e eVar) {
            if (this.bxe == null || this.bxe.size() == 0) {
                return new BookmarkFileItemView(eVar);
            }
            BookmarkFileItemView bookmarkFileItemView = (BookmarkFileItemView) this.bxe.getFirst();
            this.bxe.remove();
            bookmarkFileItemView.hi(BookmarkTabContainer.this.EN);
            bookmarkFileItemView.z((byte) 0);
            bookmarkFileItemView.b(eVar);
            String unused = bookmarkFileItemView.bmy = null;
            String unused2 = bookmarkFileItemView.bWW = null;
            return bookmarkFileItemView;
        }

        public HistoryFileItemView m(e eVar) {
            if (this.bxg == null || this.bxg.size() == 0) {
                return new HistoryFileItemView(eVar);
            }
            HistoryFileItemView historyFileItemView = (HistoryFileItemView) this.bxg.getFirst();
            this.bxg.remove();
            historyFileItemView.z((byte) 0);
            boolean unused = historyFileItemView.bmv = com.uc.a.e.nR().C(eVar.abM, eVar.abL);
            historyFileItemView.b(eVar);
            historyFileItemView.bmy = null;
            return historyFileItemView;
        }

        public void q(Vector vector) {
            if (vector != null) {
                Iterator it = vector.iterator();
                while (it.hasNext()) {
                    z zVar = (z) it.next();
                    if (zVar instanceof BookmarkFolderItemView) {
                        a((BookmarkFolderItemView) zVar);
                    } else if (zVar instanceof BookmarkFileItemView) {
                        a((BookmarkFileItemView) zVar);
                    } else if (zVar instanceof HistoryFolderItemView) {
                        a((HistoryFolderItemView) zVar);
                    } else if (zVar instanceof HistoryFileItemView) {
                        a((HistoryFileItemView) zVar);
                    }
                }
            }
        }
    }

    public class HistoryFileItemView extends z implements URLItemView {
        private e Go;
        private Bitmap bmu;
        /* access modifiers changed from: private */
        public boolean bmv;
        private int bmw;
        /* access modifiers changed from: private */
        public boolean bmx = true;
        String bmy = null;

        public HistoryFileItemView(e eVar) {
            this.Go = eVar;
            this.bmv = com.uc.a.e.nR().C(eVar.abM, eVar.abL);
            this.bmu = this.bmv ? BookmarkTabContainer.this.Fk : BookmarkTabContainer.this.Fl;
            this.pL.setTextSize((float) BookmarkTabContainer.this.ET);
            this.pL.setColor(BookmarkTabContainer.this.EX);
            this.bmw = BookmarkTabContainer.this.EN;
            bL(true);
        }

        /* access modifiers changed from: protected */
        public boolean ar(int i, int i2) {
            if (!this.bmx) {
                return false;
            }
            int width = (getWidth() - this.bmu.getWidth()) - 10;
            return new Rect(width, 0, this.bmu.getWidth() + width + 10, getHeight() + 0).contains(i, i2);
        }

        public void b(e eVar) {
            this.Go = eVar;
            Ix();
        }

        public void draw(Canvas canvas) {
            byte Iz = Iz();
            if (Iz == 0) {
                this.pL.setColor(BookmarkTabContainer.this.EX);
            } else if (Iz == 2 || Iz == 1) {
                this.pL.setColor(BookmarkTabContainer.this.Fa);
            }
            canvas.drawBitmap(BookmarkTabContainer.this.Fj, (float) this.bmw, (float) ((getHeight() - BookmarkTabContainer.this.Fj.getHeight()) / 2), (Paint) null);
            int width = this.bmw + BookmarkTabContainer.this.Fj.getWidth() + BookmarkTabContainer.this.EQ;
            int width2 = (this.aSP - this.bmu.getWidth()) - 10;
            String str = this.Go.abM;
            if (str == null || str.length() <= 1) {
                str = this.Go.abL;
            }
            if (this.bmy == null) {
                this.bmy = BookmarkTabContainer.this.a(str, (width2 - width) - BookmarkTabContainer.this.ER, this.pL);
            }
            canvas.drawText(this.bmy, (float) width, (((float) getHeight()) - this.pL.ascent()) / 2.0f, this.pL);
            if (this.bmx) {
                this.bmu = this.bmv ? BookmarkTabContainer.this.Fk : BookmarkTabContainer.this.Fl;
                canvas.drawBitmap(this.bmu, (float) width2, (float) ((getHeight() - this.bmu.getHeight()) / 2), (Paint) null);
            }
        }

        public void hi(int i) {
            this.bmw = i;
        }

        /* access modifiers changed from: protected */
        public boolean k(int i, int i2) {
            if (!this.bmx) {
                return false;
            }
            boolean contains = new Rect((getWidth() - this.bmu.getWidth()) - 20, 0, getWidth(), getHeight() + 0).contains(i, i2);
            if (!contains) {
                return contains;
            }
            this.bmv = !this.bmv;
            this.bmu = this.bmv ? BookmarkTabContainer.this.Fk : BookmarkTabContainer.this.Fl;
            Ix();
            if (BookmarkTabContainer.this.FJ != null) {
                BookmarkTabContainer.this.FJ.a(this.Go, 1, this.bmv);
            }
            return true;
        }

        public e ka() {
            return this.Go;
        }
    }

    public class HistoryFolderItemView extends ab implements URLItemView {
        public static final int Gk = 0;
        public static final int Gl = 1;
        public static final int Gm = 2;
        public static final int Gn = 3;
        private e Go = new e();

        public HistoryFolderItemView(int i, Vector vector, boolean z) {
            b(i, vector, z);
        }

        private Vector b(Vector vector, boolean z) {
            Vector vector2 = new Vector();
            if (vector != null) {
                Iterator it = vector.iterator();
                while (it.hasNext()) {
                    HistoryFileItemView m = BookmarkTabContainer.this.FK.m((e) it.next());
                    boolean unused = m.bmx = z;
                    vector2.add(m);
                }
            }
            return vector2;
        }

        private String bp(int i) {
            switch (i) {
                case 0:
                    return "今天";
                case 1:
                    return "昨天";
                case 2:
                    return "更早";
                case 3:
                    return "阅读记录";
                default:
                    return "";
            }
        }

        public void b(int i, Vector vector, boolean z) {
            this.Go.abP = true;
            this.Go.abM = bp(i);
            w(b(vector, z));
            this.pL.setColor(BookmarkTabContainer.this.EW);
            this.pL.setTextSize((float) BookmarkTabContainer.this.ES);
        }

        public void b(e eVar) {
        }

        public void draw(Canvas canvas) {
            byte Iz = Iz();
            if (Iz == 0) {
                this.pL.setColor(BookmarkTabContainer.this.EW);
            } else if (Iz == 2 || Iz == 1) {
                this.pL.setColor(BookmarkTabContainer.this.EZ);
            }
            canvas.drawBitmap(BookmarkTabContainer.this.Fi, (float) BookmarkTabContainer.this.EN, (float) ((getHeight() - BookmarkTabContainer.this.Ff.getHeight()) / 2), (Paint) null);
            Bitmap r = this.bRD ? BookmarkTabContainer.this.Fh : BookmarkTabContainer.this.Fg;
            canvas.drawText(this.Go.abM, (float) (BookmarkTabContainer.this.EN + BookmarkTabContainer.this.Fi.getWidth() + 5), (((float) getHeight()) - this.pL.ascent()) / 2.0f, this.pL);
            canvas.drawBitmap(r, (float) ((this.aSP - r.getWidth()) - 13), (float) ((getHeight() - r.getHeight()) / 2), (Paint) null);
        }

        public e ka() {
            return this.Go;
        }
    }

    class NoBookmarkView extends z {
        private String Bo;
        private Bitmap FQ;

        public NoBookmarkView(String str, Bitmap bitmap) {
            this.Bo = str;
            this.FQ = bitmap;
            this.pL.setColor(BookmarkTabContainer.this.Fd);
            this.pL.setTextSize((float) BookmarkTabContainer.this.EV);
        }

        public void draw(Canvas canvas) {
            int height = (int) ((((float) (this.aSQ - this.FQ.getHeight())) - this.pL.getTextSize()) / 2.0f);
            canvas.drawBitmap(this.FQ, (float) ((this.aSP - this.FQ.getWidth()) / 2), (float) height, this.pL);
            canvas.drawText(this.Bo, (((float) this.aSP) - this.pL.measureText(this.Bo)) / 2.0f, ((float) (height + this.FQ.getHeight())) + this.pL.getTextSize(), this.pL);
        }
    }

    class SubItemDivider extends ColorDrawable {
        private int Jp;

        public SubItemDivider(int i, int i2) {
            super(i);
            this.Jp = i2;
        }

        public void draw(Canvas canvas) {
            Rect bounds = getBounds();
            canvas.save();
            canvas.clipRect(bounds.left + this.Jp, bounds.top, bounds.right - this.Jp, bounds.bottom);
            super.draw(canvas);
            canvas.restore();
        }
    }

    public interface URLItemView {
        void b(e eVar);

        e ka();
    }

    public BookmarkTabContainer(Context context) {
        super(context);
        a();
    }

    public BookmarkTabContainer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        a();
    }

    /* access modifiers changed from: private */
    public String a(String str, int i, Paint paint) {
        if (str == null) {
            return "";
        }
        if (paint.measureText(str) <= ((float) i)) {
            return str;
        }
        int length = str.length();
        while (paint.measureText(str.substring(0, length)) > ((float) ((int) (((float) i) - paint.measureText("..."))))) {
            length--;
        }
        return ((Object) str.subSequence(0, length - 1)) + "...";
    }

    private void a() {
        this.FH = new BookmarkExpandableList();
        this.FH.am(com.uc.h.e.Pb().kp(R.dimen.f212bookmark_selected_bg_padding));
        this.FI = new BookmarkExpandableList();
        this.FI.am(com.uc.h.e.Pb().kp(R.dimen.f212bookmark_selected_bg_padding));
        com.uc.h.e.Pb().a(this);
        k();
        bo(0);
    }

    private void jQ() {
        com.uc.h.e Pb = com.uc.h.e.Pb();
        this.EN = (int) getResources().getDimension(R.dimen.f213bookmark_file_padding_left);
        this.EO = (int) getResources().getDimension(R.dimen.f214bookmark_file_padding_right);
        this.EP = (int) getResources().getDimension(R.dimen.f215bookmark_subfile_padding_left);
        this.EQ = (int) getResources().getDimension(R.dimen.f216bookmark_text_image_gap);
        this.ER = Pb.kp(R.dimen.f230bookmark_text_padding_right);
        this.ES = (int) getResources().getDimension(R.dimen.f220bookmark_folder_font);
        this.ET = (int) getResources().getDimension(R.dimen.f221bookmark_webname_font);
        this.EU = (int) getResources().getDimension(R.dimen.f222bookmark_weburl_font);
        this.EV = (int) getResources().getDimension(R.dimen.f224bookmark_notip_font);
        this.EW = Pb.getColor(105);
        this.EZ = Pb.getColor(23);
        this.EX = Pb.getColor(106);
        this.Fa = Pb.getColor(22);
        this.EY = Pb.getColor(UCR.Color.bwj);
        this.Fb = Pb.getColor(24);
        this.Fc = Pb.getColor(108);
        this.Fd = Pb.getColor(18);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private void jR() {
        com.uc.h.e Pb = com.uc.h.e.Pb();
        this.Fe = Pb.kl(UCR.drawable.aTN);
        this.Ff = Pb.kl(UCR.drawable.aTJ);
        this.Fj = Pb.kl(UCR.drawable.aUI);
        this.Fi = Pb.kl(UCR.drawable.aTS);
        this.Fg = Pb.kl(UCR.drawable.aTC);
        Matrix matrix = new Matrix();
        matrix.postRotate(90.0f);
        this.Fh = Bitmap.createBitmap(this.Fg, 0, 0, this.Fg.getWidth(), this.Fg.getHeight(), matrix, true);
        this.Fk = Pb.kl(UCR.drawable.aTM);
        this.Fl = Pb.kl(UCR.drawable.aTL);
        this.Fo = Pb.kl(UCR.drawable.aVY);
        this.Fp = Pb.kl(UCR.drawable.aVZ);
        this.Fs = Pb.getDrawable(UCR.drawable.aTH);
        this.Ft = Pb.getDrawable(UCR.drawable.aTE);
        this.Fu = Pb.getDrawable(UCR.drawable.aXu);
        this.Fv = Pb.getDrawable(UCR.drawable.aXu);
    }

    public void M(boolean z) {
        this.FG = z;
    }

    public int a(e eVar) {
        long currentTimeMillis = System.currentTimeMillis();
        long j = currentTimeMillis - (currentTimeMillis % bh.bjz);
        if (eVar.time >= j) {
            return 0;
        }
        return (eVar.time <= j - bh.bjz || eVar.time >= j) ? 2 : 1;
    }

    public void a(i iVar) {
        this.FF = iVar;
        Vector vector = new Vector();
        Vector ot = iVar.ot();
        if (ot != null) {
            Vector vector2 = new Vector();
            Vector vector3 = new Vector();
            Vector vector4 = new Vector();
            Iterator it = ot.iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                switch (a(eVar)) {
                    case 0:
                        vector2.add(eVar);
                        break;
                    case 1:
                        vector3.add(eVar);
                        break;
                    case 2:
                        vector4.add(eVar);
                        break;
                }
            }
            if (ot != null && ot.size() > 0) {
                HistoryFolderItemView c = this.FK.c(0, vector2, this.FG);
                c.setId(WebViewJUC.cga);
                vector.add(c);
                HistoryFolderItemView c2 = this.FK.c(1, vector3, this.FG);
                c2.setId(WebViewJUC.cgb);
                vector.add(c2);
                HistoryFolderItemView c3 = this.FK.c(2, vector4, this.FG);
                c3.setId(WebViewJUC.cgc);
                vector.add(c3);
            }
        }
        Vector qM = com.uc.a.e.nR().qM();
        if (qM != null && qM.size() > 0) {
            HistoryFolderItemView c4 = this.FK.c(3, qM, false);
            c4.setId(4100);
            vector.add(c4);
        }
        this.FI.c(vector);
        this.FI.fo(-1);
        invalidate();
    }

    public void a(com.uc.a.m mVar) {
        this.FE = mVar;
        Vector vector = new Vector();
        Vector ol = mVar.ol();
        if (ol != null) {
            Iterator it = ol.iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                BookmarkFolderItemView k = this.FK.k(eVar);
                k.setId(eVar.abQ);
                vector.add(k);
            }
        }
        Vector e = mVar.e(null);
        if (e != null) {
            Iterator it2 = e.iterator();
            while (it2.hasNext()) {
                vector.add(this.FK.l((e) it2.next()));
            }
        }
        this.FH.c(vector);
        this.FH.fo(-1);
        invalidate();
    }

    public void a(BookmarkDelegate bookmarkDelegate) {
        this.FJ = bookmarkDelegate;
    }

    public void a(z zVar, int i, int i2) {
        if (this.FJ != null) {
            this.FJ.a(((URLItemView) zVar).ka(), Bi(), i, i2);
        }
    }

    public void b(int i, int i2, int i3, int i4) {
        this.FH.setPadding(i, i2, i3, i4);
        this.FI.setPadding(i, i2, i3, i4);
    }

    public void b(z zVar, int i, int i2) {
        if (this.FJ != null) {
            this.FJ.b(((URLItemView) zVar).ka(), Bi(), i, i2);
        }
    }

    public void bo(int i) {
        super.bo(i);
        if (i == 0) {
            I(this.Fx);
            H(this.Fy);
            jS();
            return;
        }
        I(this.Fz);
        H(this.Fw);
        jT();
    }

    public void jS() {
        this.FK.q(this.FH.BM());
        a(com.uc.a.e.nR().oj());
    }

    public void jT() {
        this.FK.q(this.FI.BM());
        a(com.uc.a.e.nR().oa());
    }

    public void k() {
        com.uc.h.e Pb = com.uc.h.e.Pb();
        setBackgroundDrawable(Pb.getDrawable(UCR.drawable.aTK));
        F(Pb.getDrawable(UCR.drawable.aTO));
        G(Pb.getDrawable(UCR.drawable.aTP));
        fU(Pb.getColor(103));
        fV(Pb.getColor(102));
        jQ();
        jR();
        this.FH.f(new Drawable[]{this.Ft, null, this.Fv, this.Fu});
        this.FH.a(new Drawable[]{null, this.Fs, new SubItemDivider(this.Fc, 0)}, new int[]{1, 4, 1});
        this.FH.setDividerHeight(0);
        this.FH.a((m) this);
        this.FH.fm(Pb.kp(R.dimen.f219bookmark_item_height));
        this.FH.g(new NoBookmarkView("当前没有书签记录", this.Fo));
        this.FH.B(Pb.getDrawable(UCR.drawable.aWM));
        this.FH.ft(Pb.kp(R.dimen.f166list_scrollbar_size));
        this.FI.f(new Drawable[]{this.Ft, null, this.Fv, this.Fu});
        this.FI.a(new Drawable[]{null, this.Fs, new SubItemDivider(this.Fc, 0)}, new int[]{1, 4, 1});
        this.FI.setDividerHeight(0);
        this.FI.a((m) this);
        this.FI.fm(Pb.kp(R.dimen.f219bookmark_item_height));
        this.FI.g(new NoBookmarkView("当前没有历史记录", this.Fp));
        this.FI.B(Pb.getDrawable(UCR.drawable.aWM));
        this.FI.ft(Pb.kp(R.dimen.f166list_scrollbar_size));
        com.uc.h.e Pb2 = com.uc.h.e.Pb();
        this.Fw = Pb2.getDrawable(UCR.drawable.aXl);
        this.Fx = Pb2.getDrawable(UCR.drawable.aXm);
        this.Fy = Pb2.getDrawable(UCR.drawable.aXn);
        this.Fz = Pb2.getDrawable(UCR.drawable.aXo);
        bo(0);
        a(new TabContainer.TabData[]{new TabContainer.TabData(FC, this.Fq, this.FH), new TabContainer.TabData(FD, this.Fr, this.FI)});
        requestLayout();
    }
}
