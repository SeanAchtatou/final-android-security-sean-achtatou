package com.uc.browser;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.Scroller;
import com.uc.e.ad;
import com.uc.h.e;

public class Workspace extends ViewGroup implements ad {
    private static final int aeg = -1;
    private static final int aeh = 10;
    private static final float aei = 1.6f;
    private static final int aej = 1;
    private static int aek = 20;
    private static final float ael = 0.14f;
    private static final int aev = 0;
    private static final int aew = 1;
    Bitmap FQ;
    private Picture aeA;
    private int aeB;
    private Drawable aeC;
    private Drawable aeD;
    private Drawable aeE;
    private int aeF;
    private Drawable aeG;
    private int aeH;
    private ViewMainBarMainPage aeI;
    private int aeJ;
    private int aeK;
    private Bitmap aem;
    private boolean aen;
    private int aeo;
    private int aep;
    private int aeq;
    private float aer;
    private float aes;
    private long aet;
    private float aeu;
    private int aex;
    private Scroller aey;
    private boolean aez;
    protected int lastIndex;

    public Workspace(Context context) {
        this(context, null, 0);
    }

    public Workspace(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public Workspace(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.aep = -1;
        this.aeq = aek;
        this.aex = 0;
        this.aez = true;
        this.aeA = null;
        this.aeK = -1;
        this.FQ = null;
        this.lastIndex = -1;
        uS();
    }

    private void f(float f) {
        int scrollX;
        int width = getWidth();
        int i = this.aeo;
        if (Math.abs(f) > ael) {
            scrollX = (f > 0.0f ? 1 : -1) + i;
            if (scrollX < 0) {
                scrollX = 0;
            }
            if (scrollX > 2) {
                scrollX = 2;
            }
        } else {
            scrollX = (getScrollX() + (width / 2)) / width;
        }
        A(scrollX);
    }

    private void g(Canvas canvas) {
        canvas.save();
        canvas.translate((float) getScrollX(), (float) ((getMeasuredHeight() - this.aeF) - e.Pb().kp(R.dimen.f177controlbar_height)));
        int childCount = getChildCount();
        int intrinsicWidth = (this.aeD != null ? this.aeD.getIntrinsicWidth() * (childCount - 1) : 0) + (this.aeE != null ? this.aeE.getIntrinsicWidth() : 0) + (this.aeB * (childCount + 1));
        canvas.translate((float) ((getMeasuredWidth() - intrinsicWidth) / 2), 0.0f);
        if (this.aeC != null) {
            this.aeC.setBounds(0, 0, intrinsicWidth, this.aeF);
            this.aeC.draw(canvas);
        }
        int intrinsicHeight = this.aeE == null ? 0 : (this.aeF - this.aeE.getIntrinsicHeight()) / 2;
        int intrinsicHeight2 = this.aeD == null ? 0 : (this.aeF - this.aeD.getIntrinsicHeight()) / 2;
        for (int i = 0; i < childCount; i++) {
            canvas.translate((float) this.aeB, 0.0f);
            if (i == this.aeo && this.aeD != null) {
                canvas.translate(0.0f, (float) intrinsicHeight2);
                this.aeD.draw(canvas);
                canvas.translate((float) this.aeD.getIntrinsicWidth(), (float) (-intrinsicHeight2));
            } else if (this.aeE != null) {
                canvas.translate(0.0f, (float) intrinsicHeight);
                this.aeE.draw(canvas);
                canvas.translate((float) this.aeE.getIntrinsicWidth(), (float) (-intrinsicHeight));
            }
        }
        canvas.restore();
    }

    private void uS() {
        this.aey = new Scroller(getContext());
        aek = ViewConfiguration.get(getContext()).getScaledTouchSlop();
        e Pb = e.Pb();
        this.aeB = Pb.kp(R.dimen.f171spot_dot_padding);
        this.aeF = Pb.kp(R.dimen.f172spot_area_height);
        this.aeH = Pb.kp(R.dimen.f173widget_bottom_padding);
        this.aeI = new ViewMainBarMainPage(getContext());
        this.aeI.a(this);
        this.aeJ = Pb.kp(R.dimen.f177controlbar_height);
    }

    private int uT() {
        return getMeasuredWidth() * (getChildCount() - 1);
    }

    private void uU() {
        f(0.0f);
    }

    /* access modifiers changed from: package-private */
    public boolean A(int i) {
        if (!this.aey.isFinished()) {
            return false;
        }
        int max = Math.max(0, Math.min(i, getChildCount() - 1));
        boolean z = max != this.aeo;
        this.aep = max;
        View focusedChild = getFocusedChild();
        if (focusedChild != null && z && focusedChild == getChildAt(this.aeo)) {
            focusedChild.clearFocus();
        }
        int width = (max * getWidth()) - getScrollX();
        this.aey.startScroll(getScrollX(), 0, width, 0, (int) (((float) Math.abs(width)) * aei));
        invalidate();
        return true;
    }

    public void B(int i) {
        this.aeo = i;
        scrollTo(getWidth() * i, 0);
    }

    public void cJ() {
        invalidate(getScrollX(), getHeight() - this.aeJ, getScrollX() + getWidth(), getHeight());
    }

    public void computeScroll() {
        if (this.aey.computeScrollOffset()) {
            scrollTo(this.aey.getCurrX(), 0);
            postInvalidate();
        } else if (this.aep != -1) {
            this.aeo = Math.max(0, Math.min(this.aep, getChildCount() - 1));
            this.aep = -1;
            uW();
        }
    }

    public void dispatchDraw(Canvas canvas) {
        if (canvas.getClipBounds().top < getMeasuredHeight() - this.aeJ) {
            if (this.aeA != null) {
                this.aeA.draw(canvas);
            } else {
                super.dispatchDraw(canvas);
            }
        }
        g(canvas);
        if (this.aeG != null) {
            this.aeG.setBounds(getScrollX(), getHeight() - this.aeG.getIntrinsicHeight(), getScrollX() + getWidth(), getHeight());
            this.aeG.draw(canvas);
        }
        f(canvas);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return getChildAt(this.aeo).dispatchKeyEvent(keyEvent);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        boolean z;
        int scrollX = getScrollX();
        int width = getWidth();
        int action = motionEvent.getAction();
        float x = motionEvent.getX();
        switch (action) {
            case 0:
                this.aer = x;
                this.aes = motionEvent.getY();
                if (this.aes < ((float) (getHeight() - this.aeJ))) {
                    this.aet = System.currentTimeMillis();
                    this.aeK = (int) ((((float) scrollX) + x) / ((float) width));
                    if (this.aeK >= 0 && this.aeK < getChildCount()) {
                        z = getChildAt(this.aeK).dispatchTouchEvent(motionEvent);
                        break;
                    } else {
                        this.aeK = -1;
                        z = false;
                        break;
                    }
                } else {
                    this.aeK = -2;
                    return this.aeI.dispatchTouchEvent(motionEvent);
                }
                break;
            default:
                if (this.aeK != -2) {
                    if (this.aeK < 0) {
                        z = false;
                        break;
                    } else {
                        z = getChildAt(this.aeK).dispatchTouchEvent(motionEvent);
                        break;
                    }
                } else {
                    z = this.aeI.dispatchTouchEvent(motionEvent);
                    break;
                }
        }
        if (!z) {
            this.aeK = -1;
            onTouchEvent(motionEvent);
        }
        return true;
    }

    public Bitmap eG(int i) {
        if (this.lastIndex != i) {
            View childAt = getChildAt(i);
            int dimension = (int) getResources().getDimension(R.dimen.f453multiwindow_snapshot_width);
            int dimension2 = (int) getResources().getDimension(R.dimen.f454multiwindow_snapshot_height);
            float max = Math.max(((float) dimension) / ((float) childAt.getWidth()), ((float) dimension2) / ((float) ((childAt.getHeight() - this.aeJ) - this.aeF)));
            if (childAt != null) {
                this.FQ = Bitmap.createBitmap(dimension, dimension2, Bitmap.Config.RGB_565);
                Canvas canvas = new Canvas(this.FQ);
                canvas.scale(max, max);
                childAt.draw(canvas);
            }
            this.lastIndex = i;
        }
        return this.FQ;
    }

    /* access modifiers changed from: protected */
    public void f(Canvas canvas) {
        if (this.aeI != null) {
            canvas.save();
            canvas.translate((float) getScrollX(), (float) (getMeasuredHeight() - this.aeJ));
            this.aeI.draw(canvas);
            canvas.restore();
        }
    }

    public void o(Drawable drawable) {
        this.aeC = drawable;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int childCount = getChildCount();
        int i5 = 0;
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (childAt.getVisibility() != 8) {
                int measuredWidth = getMeasuredWidth();
                childAt.layout(i5, 0, i5 + measuredWidth, getMeasuredHeight());
                i5 += measuredWidth;
            }
        }
        if (this.aeI != null) {
            this.aeI.layout(0, 0, i3 - i, this.aeJ);
        }
        if (z) {
            B(this.aeo);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), Integer.MIN_VALUE);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), Integer.MIN_VALUE);
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            getChildAt(i3).measure(makeMeasureSpec, makeMeasureSpec2);
        }
        if (this.aeI != null) {
            this.aeI.measure(makeMeasureSpec, this.aeJ);
            this.aeI.requestLayout();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        uV();
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        int i = this.aer < 0.0f ? 0 : (int) (this.aer - x);
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis == this.aet) {
            currentTimeMillis = this.aet + 1;
        }
        switch (motionEvent.getAction()) {
            case 0:
                if (!this.aey.isFinished()) {
                    this.aey.abortAnimation();
                }
                this.aer = x;
                break;
            case 1:
                this.aeu = (((this.aer - x) / ((float) (currentTimeMillis - this.aet))) + this.aeu) / 2.0f;
                f(this.aeu);
                this.aer = -1.0f;
                this.aes = -1.0f;
                this.aex = 0;
                break;
            case 2:
                int scrollX = getScrollX();
                if (scrollX + i > -10 && scrollX + i < uT() + 10) {
                    scrollBy(i, 0);
                }
                this.aeu = (this.aer - x) / ((float) (currentTimeMillis - this.aet));
                this.aer = x;
                this.aes = y;
                this.aet = System.currentTimeMillis();
                return true;
            case 3:
                this.aex = 0;
                break;
        }
        return true;
    }

    public void onWindowFocusChanged(boolean z) {
        if (z && !this.aen) {
            this.aen = true;
            B(getChildCount() / 2);
        }
    }

    public void p(Drawable drawable) {
        this.aeD = drawable;
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
    }

    public void q(Drawable drawable) {
        this.aeE = drawable;
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
    }

    public void r(Drawable drawable) {
        this.aeG = drawable;
        if (drawable != null) {
            drawable.setBounds(0, getHeight() - drawable.getIntrinsicHeight(), getWidth(), getHeight());
        }
    }

    public void setWallpaper(Bitmap bitmap) {
        this.aem = bitmap;
    }

    public ViewMainBarMainPage uR() {
        return this.aeI;
    }

    /* access modifiers changed from: protected */
    public void uV() {
        if (this.aeA == null) {
            this.aeA = new Picture();
            super.dispatchDraw(this.aeA.beginRecording(getWidth() * 3, getHeight()));
            this.aeA.endRecording();
        }
    }

    /* access modifiers changed from: protected */
    public void uW() {
        this.aeA = null;
    }

    public int uX() {
        return this.aeo;
    }
}
