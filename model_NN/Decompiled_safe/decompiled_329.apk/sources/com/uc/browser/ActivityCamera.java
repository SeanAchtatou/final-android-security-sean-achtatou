package com.uc.browser;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.uc.a.e;
import com.uc.browser.UCR;
import com.uc.browser.UcCamera;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ActivityCamera extends Activity implements UcCamera.CALLBACK_CAPTURE {
    private static final String tV = "NUMBERPHOTOCAPTURED";
    public static final String tW = "file_maxlength";
    Sensor sensor = null;
    UcCamera tU;
    /* access modifiers changed from: private */
    public int tX = -1;
    public final String tY = "buffer";
    public final int tZ = 1;
    /* access modifiers changed from: private */
    public boolean ua = true;
    SensorManager ub = null;
    SensorEventListener uc = null;
    /* access modifiers changed from: private */
    public Bitmap ud;
    private boolean ue;

    public static int fr() {
        return e.nR().fr();
    }

    /* access modifiers changed from: private */
    public void fs() {
        SharedPreferences sharedPreferences = getSharedPreferences(tV, 0);
        int dk = e.nR().dk(sharedPreferences.getInt(tV, 0) + 1);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt(tV, dk);
        edit.commit();
    }

    private void fu() {
        ((ImageView) findViewById(R.id.f659imgCaptured)).setImageBitmap(this.ud);
    }

    private void fv() {
        View findViewById = findViewById(R.id.f659imgCaptured);
        View findViewById2 = findViewById(R.id.f658sfPreview);
        View findViewById3 = findViewById(R.id.f662btnConfirm);
        View findViewById4 = findViewById(R.id.f663btnCancel);
        View findViewById5 = findViewById(R.id.f661btnReshut);
        View findViewById6 = findViewById(R.id.f660btnShut);
        findViewById.setVisibility(0);
        findViewById2.setVisibility(8);
        findViewById4.setVisibility(0);
        findViewById5.setVisibility(0);
        findViewById6.setVisibility(8);
        if (this.ud != null) {
            findViewById3.setVisibility(0);
            findViewById3.setFocusable(true);
            findViewById3.setFocusableInTouchMode(true);
            findViewById3.requestFocus();
            findViewById3.requestFocusFromTouch();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            int length = this.ud.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream) ? byteArrayOutputStream.toByteArray().length / 1024 : 0;
            try {
                byteArrayOutputStream.close();
            } catch (IOException e) {
            }
            ((TextView) findViewById(R.id.title)).setText(String.format("照片大小: %dKB  照片尺寸:%d*%d", Integer.valueOf(length), Integer.valueOf(this.ud.getWidth()), Integer.valueOf(this.ud.getHeight())));
            return;
        }
        findViewById3.setVisibility(8);
        ((TextView) findViewById(R.id.title)).setText("拍照失败");
    }

    /* access modifiers changed from: private */
    public void fw() {
        View findViewById = findViewById(R.id.f659imgCaptured);
        View findViewById2 = findViewById(R.id.f658sfPreview);
        View findViewById3 = findViewById(R.id.f662btnConfirm);
        View findViewById4 = findViewById(R.id.f663btnCancel);
        View findViewById5 = findViewById(R.id.f661btnReshut);
        View findViewById6 = findViewById(R.id.f660btnShut);
        findViewById.setVisibility(8);
        findViewById2.setVisibility(0);
        findViewById3.setVisibility(8);
        findViewById4.setVisibility(0);
        findViewById5.setVisibility(8);
        findViewById6.setVisibility(0);
        findViewById6.setFocusable(true);
        findViewById6.setFocusableInTouchMode(true);
        findViewById6.requestFocus();
        findViewById6.requestFocusFromTouch();
        ((TextView) findViewById(R.id.title)).setText("拍照上传");
    }

    /* access modifiers changed from: private */
    public void fx() {
        View findViewById = findViewById(R.id.f662btnConfirm);
        View findViewById2 = findViewById(R.id.f663btnCancel);
        View findViewById3 = findViewById(R.id.f661btnReshut);
        View findViewById4 = findViewById(R.id.f660btnShut);
        findViewById.setVisibility(8);
        findViewById2.setVisibility(8);
        findViewById3.setVisibility(8);
        findViewById4.setVisibility(8);
    }

    private void fy() {
        int i;
        int i2;
        Display defaultDisplay = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        if (width / 4 >= height / 3) {
            i = (height * 4) / 3;
            i2 = height;
        } else if (width >= height) {
            i = width;
            i2 = (width * 3) / 4;
        } else if (height / 4 < width / 3) {
            i = (height * 3) / 4;
            i2 = height;
        } else {
            i = width;
            i2 = (width * 4) / 3;
        }
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.f657layoutDisplay);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) linearLayout.getLayoutParams();
        layoutParams.width = i;
        layoutParams.height = i2;
        linearLayout.setLayoutParams(layoutParams);
    }

    public void b(Bitmap bitmap) {
        if (this.ud != null) {
            this.ud.recycle();
            this.ud = null;
            System.gc();
        }
        this.ud = bitmap;
        fu();
        fv();
    }

    /* access modifiers changed from: protected */
    public void ft() {
        com.uc.h.e Pb = com.uc.h.e.Pb();
        Button button = (Button) findViewById(R.id.f663btnCancel);
        button.setBackgroundDrawable(Pb.getDrawable(UCR.drawable.aWS));
        button.getBackground().setAlpha(200);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ActivityCamera.this.setResult(0);
                ActivityCamera.this.finish();
            }
        });
        Button button2 = (Button) findViewById(R.id.f662btnConfirm);
        button2.setBackgroundDrawable(Pb.getDrawable(UCR.drawable.aWU));
        button2.getBackground().setAlpha(200);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (ActivityCamera.this.ud != null) {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    if (ActivityCamera.this.ud.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream)) {
                        Intent intent = new Intent();
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        if (ActivityCamera.this.tX == -1 || byteArray.length <= ActivityCamera.this.tX) {
                            intent.putExtra("buffer", byteArrayOutputStream.toByteArray());
                            ActivityCamera.this.setResult(-1, intent);
                            ActivityCamera.this.fs();
                        } else {
                            float b2 = (float) ActivityCamera.this.tX;
                            int i = 0;
                            while (b2 > 1024.0f) {
                                b2 /= 1024.0f;
                                i++;
                            }
                            Toast.makeText(ActivityCamera.this, String.format(ActivityCamera.this.getString(R.string.f1445file_upload_too_large), Float.valueOf(b2), ActivityChooseFile.uJ[i]), 0).show();
                            return;
                        }
                    } else {
                        ActivityCamera.this.setResult(1);
                    }
                    try {
                        byteArrayOutputStream.close();
                    } catch (IOException e) {
                    }
                    ActivityCamera.this.finish();
                    return;
                }
                ActivityCamera.this.setResult(1);
                ActivityCamera.this.finish();
            }
        });
        Button button3 = (Button) findViewById(R.id.f660btnShut);
        button3.setBackgroundDrawable(Pb.getDrawable(UCR.drawable.aWT));
        button3.getBackground().setAlpha(200);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (ActivityCamera.this.tU.wr().fG()) {
                    ActivityCamera.this.fx();
                    return;
                }
                ActivityCamera.this.setResult(1);
                ActivityCamera.this.finish();
            }
        });
        Button button4 = (Button) findViewById(R.id.f661btnReshut);
        button4.setBackgroundDrawable(Pb.getDrawable(UCR.drawable.aWT));
        button4.getBackground().setAlpha(200);
        button4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (ActivityCamera.this.tU.wr().fH()) {
                    ActivityCamera.this.fw();
                    return;
                }
                ActivityCamera.this.setResult(1);
                ActivityCamera.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 86 && i2 == -1) {
            setResult(1);
            finish();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        fy();
        fu();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.ub = (SensorManager) getSystemService("sensor");
        if (this.ub != null) {
            this.sensor = this.ub.getDefaultSensor(1);
            this.uc = new SensorEventListener() {
                public void onAccuracyChanged(Sensor sensor, int i) {
                }

                public void onSensorChanged(SensorEvent sensorEvent) {
                    float f = sensorEvent.values[0];
                    if (sensorEvent.values[1] > 7.0f) {
                        boolean unused = ActivityCamera.this.ua = false;
                    } else if (f > 7.0f) {
                        boolean unused2 = ActivityCamera.this.ua = true;
                    }
                }
            };
            this.ub.registerListener(this.uc, this.sensor, 1);
        }
        getWindow().setFormat(-3);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.f873activitycamera);
        this.tU = UcCamera.a((SurfaceView) findViewById(R.id.f658sfPreview), this);
        this.ud = null;
        this.ue = true;
        Intent intent = getIntent();
        if (intent != null) {
            this.tX = intent.getIntExtra("file_maxlength", -1);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.ud = null;
        if (!(this.ub == null || this.sensor == null || this.uc == null)) {
            this.ub.unregisterListener(this.uc, this.sensor);
            this.ub = null;
            this.uc = null;
            this.sensor = null;
        }
        super.onDestroy();
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i != 84) {
            return super.onKeyUp(i, keyEvent);
        }
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a((int) ModelBrowser.zJ, this);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.ue = this.tU.wr().fD();
        this.tU.wr().fK();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        fy();
        if (!this.tU.wr().fJ()) {
            setResult(1);
            finish();
        } else if (!this.ue) {
            fv();
        } else if (!this.tU.wr().fH()) {
            setResult(1);
            finish();
        } else {
            fw();
        }
    }

    public void onShutter() {
        if (this.ua) {
            this.tU.ws().IT();
        } else {
            this.tU.ws().IU();
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        ft();
    }
}
