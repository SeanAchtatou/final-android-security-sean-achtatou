package com.uc.browser;

import android.content.Context;
import android.view.View;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

public class AdapterMultiWindow {
    private final String LOGTAG = "ADAPTER_MULTI_WINDOW";
    /* access modifiers changed from: private */
    public Vector aDN = new Vector();
    private ArrayList aDO;
    CPThread aDP = null;

    class CPThread extends Thread {
        private CPThread() {
        }

        public void run() {
            MultiWindowManager hj = ModelBrowser.gD().hj();
            int wh = hj.wh();
            for (int i = 0; i < wh; i++) {
                WindowItem windowItem = (WindowItem) AdapterMultiWindow.this.aDN.elementAt(i);
                if (windowItem.bt == null || windowItem.bt.isRecycled()) {
                    try {
                        windowItem.bt = hj.eN(i).bp();
                        AdapterMultiWindow.this.yV();
                    } catch (Throwable th) {
                        windowItem.bt = null;
                    }
                }
            }
        }
    }

    public interface WindowObserver {
        void update();
    }

    public AdapterMultiWindow(Context context) {
        if (ModelBrowser.gD() != null) {
            MultiWindowManager hj = ModelBrowser.gD().hj();
            int wh = hj.wh();
            if (wh <= 4) {
                for (int i = 0; i < wh; i++) {
                    WindowUCWeb eN = hj.eN(i);
                    WindowItem windowItem = new WindowItem();
                    windowItem.bs = eN.getTitle();
                    windowItem.br = eN.getUrl();
                    this.aDN.add(windowItem);
                }
                return;
            }
            for (int i2 = 0; i2 < wh; i2++) {
                WindowUCWeb eN2 = hj.eN(i2);
                WindowItem windowItem2 = new WindowItem();
                windowItem2.bs = eN2.getTitle();
                windowItem2.br = eN2.getUrl();
                windowItem2.bt = null;
                this.aDN.add(windowItem2);
            }
        }
    }

    public void a(WindowObserver windowObserver) {
        if (this.aDO == null) {
            this.aDO = new ArrayList();
        }
        this.aDO.add(windowObserver);
    }

    public void b(WindowObserver windowObserver) {
        if (this.aDO != null) {
            this.aDO.remove(windowObserver);
            if (this.aDO.size() == 0) {
                this.aDO.clear();
                this.aDO = null;
            }
        }
    }

    public String cn(int i) {
        if (i < 0 || i >= this.aDN.size()) {
            return null;
        }
        return ((WindowItem) this.aDN.get(i)).bs;
    }

    public String fk(int i) {
        if (i < 0 || i >= this.aDN.size()) {
            return null;
        }
        return ((WindowItem) this.aDN.get(i)).br;
    }

    public void fl(int i) {
        if (this.aDN.size() > 1 && ModelBrowser.gD() != null && i >= 0 && i < this.aDN.size()) {
            this.aDN.remove(i);
            ModelBrowser.gD().hj().eO(i);
        }
    }

    public int getCount() {
        if (this.aDN == null) {
            return 0;
        }
        return this.aDN.size();
    }

    public Object getItem(int i) {
        if (this.aDN == null) {
            return null;
        }
        return (WindowItem) this.aDN.get(i % this.aDN.size());
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public void i(View view) {
        if (this.aDP == null) {
            this.aDP = new CPThread();
            this.aDP.start();
        }
    }

    public void kc() {
        if (this.aDN != null) {
            Iterator it = this.aDN.iterator();
            while (it.hasNext()) {
                WindowItem windowItem = (WindowItem) it.next();
                if (windowItem.bt != null && !windowItem.bt.isRecycled()) {
                    windowItem.bt.recycle();
                    windowItem.bt = null;
                }
            }
            this.aDN.clear();
            this.aDN = null;
            System.gc();
        }
    }

    public void yV() {
        if (this.aDO != null) {
            Iterator it = this.aDO.iterator();
            while (it.hasNext()) {
                ((WindowObserver) it.next()).update();
            }
        }
    }

    public void yW() {
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().hj().cJ(null);
        }
    }
}
