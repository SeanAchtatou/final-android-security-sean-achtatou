package com.uc.browser;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import com.uc.c.au;
import java.io.File;

public class UzoneFileUpload {
    private File file;
    private String name;
    private ActivityBrowser sj;
    private WebView sk;
    private String sl;
    private int sm;
    private String sn;
    private String type;

    public UzoneFileUpload(ActivityBrowser activityBrowser, WebView webView) {
        this.sj = activityBrowser;
        this.sk = webView;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0072, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0073, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x007f, code lost:
        r0 = r2;
        r1 = r3;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0072 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:7:0x0025] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] c(java.io.File r11) {
        /*
            r10 = this;
            r8 = 0
            r7 = 2131296297(0x7f090029, float:1.8210507E38)
            r6 = 1
            long r0 = r11.length()
            java.lang.System.gc()
            java.lang.Runtime r2 = java.lang.Runtime.getRuntime()
            long r2 = r2.freeMemory()
            r4 = 800000(0xc3500, double:3.952525E-318)
            int r4 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r4 >= 0) goto L_0x0082
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 >= 0) goto L_0x0082
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0040, all -> 0x0061 }
            r2.<init>(r11)     // Catch:{ Exception -> 0x0040, all -> 0x0061 }
            int r3 = (int) r0
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x007a, all -> 0x0072 }
            r4 = 0
            int r0 = (int) r0
            r2.read(r3, r4, r0)     // Catch:{ Exception -> 0x007e, all -> 0x0072 }
            r2.close()     // Catch:{ Exception -> 0x007e, all -> 0x0072 }
            r2.close()     // Catch:{ IOException -> 0x0034 }
            r0 = r3
        L_0x0033:
            return r0
        L_0x0034:
            r0 = move-exception
            com.uc.browser.ActivityBrowser r0 = r10.sj
            android.widget.Toast r0 = android.widget.Toast.makeText(r0, r7, r6)
            r0.show()
            r0 = r3
            goto L_0x0033
        L_0x0040:
            r0 = move-exception
            r0 = r8
            r1 = r8
        L_0x0043:
            com.uc.browser.ActivityBrowser r2 = r10.sj     // Catch:{ all -> 0x0075 }
            r3 = 2131296297(0x7f090029, float:1.8210507E38)
            r4 = 1
            android.widget.Toast r2 = android.widget.Toast.makeText(r2, r3, r4)     // Catch:{ all -> 0x0075 }
            r2.show()     // Catch:{ all -> 0x0075 }
            r0.close()     // Catch:{ IOException -> 0x0055 }
            r0 = r1
            goto L_0x0033
        L_0x0055:
            r0 = move-exception
            com.uc.browser.ActivityBrowser r0 = r10.sj
            android.widget.Toast r0 = android.widget.Toast.makeText(r0, r7, r6)
            r0.show()
            r0 = r1
            goto L_0x0033
        L_0x0061:
            r0 = move-exception
            r1 = r8
        L_0x0063:
            r1.close()     // Catch:{ IOException -> 0x0067 }
        L_0x0066:
            throw r0
        L_0x0067:
            r1 = move-exception
            com.uc.browser.ActivityBrowser r1 = r10.sj
            android.widget.Toast r1 = android.widget.Toast.makeText(r1, r7, r6)
            r1.show()
            goto L_0x0066
        L_0x0072:
            r0 = move-exception
            r1 = r2
            goto L_0x0063
        L_0x0075:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0063
        L_0x007a:
            r0 = move-exception
            r0 = r2
            r1 = r8
            goto L_0x0043
        L_0x007e:
            r0 = move-exception
            r0 = r2
            r1 = r3
            goto L_0x0043
        L_0x0082:
            r0 = r8
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.browser.UzoneFileUpload.c(java.io.File):byte[]");
    }

    public void I(String str) {
        this.sn = str;
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(113, 1, this);
        }
    }

    public void fileUpload(String str) {
        this.sn = str;
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(113, 2, this);
        }
    }

    public String getFileContent() {
        String str = this.sl;
        this.sl = null;
        System.gc();
        return str;
    }

    public String getFileName() {
        return this.name;
    }

    public int getFileSize() {
        return this.sm;
    }

    public String getFileType() {
        return this.type;
    }

    public void h(Object obj) {
        Bundle extras;
        Intent intent = (Intent) obj;
        if (obj != null && (extras = intent.getExtras()) != null) {
            String string = extras.getString(ActivityChooseFile.uE);
            String string2 = extras.getString(ActivityChooseFile.uF);
            this.file = new File(string + au.aGF + string2);
            this.name = string2;
            this.sm = (int) this.file.length();
            this.type = ((String) string2.subSequence(string2.indexOf(".") + 1, string2.length())).toUpperCase();
            this.sl = UCImageCoder.N(c(this.file));
            this.sk.loadUrl("javascript:" + this.sn + "()");
        }
    }
}
