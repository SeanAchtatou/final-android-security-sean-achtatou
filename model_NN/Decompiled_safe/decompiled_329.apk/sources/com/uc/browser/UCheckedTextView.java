package com.uc.browser;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.CheckedTextView;
import com.uc.browser.UCR;
import com.uc.h.e;

public class UCheckedTextView extends CheckedTextView {
    private static final int[] ea = {16842912};
    private Drawable eb;

    public UCheckedTextView(Context context) {
        super(context);
        d(context);
    }

    public UCheckedTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, context.obtainStyledAttributes(attributeSet, R.styleable.aEY).getBoolean(0, true));
    }

    public UCheckedTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context, context.obtainStyledAttributes(attributeSet, R.styleable.aEY).getBoolean(0, true));
    }

    private void a(Context context, boolean z) {
        setCheckMarkDrawable(z ? e.Pb().getDrawable(UCR.drawable.aWW) : null);
        setTextColor(new ColorStateList(new int[][]{new int[]{16842919, 16842910, 16842909}, new int[0]}, new int[]{e.Pb().getColor(79), e.Pb().getColor(78)}));
    }

    private void d(Context context) {
        a(context, true);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int height;
        super.onDraw(canvas);
        if (this.eb != null) {
            if (isChecked()) {
                this.eb.setState(ea);
            } else {
                this.eb.setState(new int[0]);
            }
            Drawable current = this.eb.getCurrent();
            if (current != null) {
                int gravity = getGravity() & 112;
                int intrinsicHeight = current.getIntrinsicHeight();
                switch (gravity) {
                    case 16:
                        height = (getHeight() - intrinsicHeight) / 2;
                        break;
                    case 80:
                        height = getHeight() - intrinsicHeight;
                        break;
                    default:
                        height = 0;
                        break;
                }
                int width = getWidth();
                current.setBounds(width - getPaddingRight(), height, (width - getPaddingRight()) + this.eb.getIntrinsicWidth(), intrinsicHeight + height);
                current.draw(canvas);
            }
        }
    }

    public void setCheckMarkDrawable(Drawable drawable) {
        this.eb = drawable;
        if (drawable != null) {
            setPadding(getPaddingLeft(), getPaddingTop(), this.eb.getIntrinsicWidth() + getPaddingRight(), getPaddingBottom());
        }
    }
}
