package com.uc.browser;

import android.app.Activity;
import android.text.ClipboardManager;
import android.text.method.PasswordTransformationMethod;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Toast;
import com.uc.browser.UCR;
import com.uc.d.c;
import com.uc.d.d;
import com.uc.d.f;
import com.uc.h.e;
import java.util.Vector;

public class ActivityWithUCMenu extends Activity implements View.OnLongClickListener, AdapterView.OnItemLongClickListener, d {
    private static final int aCh = 16908324;
    private static final int aCi = 16908321;
    private static final int aCj = 16908320;
    private static final int aCk = 16908322;
    private static final int aCl = 16908319;
    private static final int aCm = 16908328;
    private static final int aCn = 16908329;
    protected Vector aCf;
    protected ContextMenu aCg;
    protected String aCo;
    protected UCEditText aCp;
    protected f lF;

    public boolean a(MenuItem menuItem) {
        return onContextItemSelected(menuItem);
    }

    public void closeContextMenu() {
        if (this.lF != null && true == this.lF.isShowing()) {
            this.lF.dismiss();
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public boolean onContextItemSelected(MenuItem menuItem) {
        boolean z;
        switch (menuItem.getItemId()) {
            case aCl /*16908319*/:
                if (this.aCp != null) {
                    this.aCp.setSelection(0, this.aCp.length());
                }
                z = true;
                break;
            case aCj /*16908320*/:
                if (!(this.aCo == null || this.aCp == null)) {
                    ((ClipboardManager) getSystemService("clipboard")).setText(this.aCo);
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append(this.aCp.getText().subSequence(0, this.aCp.getSelectionStart()));
                    stringBuffer.append(this.aCp.getText().subSequence(this.aCp.getSelectionEnd(), this.aCp.length()));
                    this.aCp.setText(stringBuffer.toString());
                }
                z = true;
                break;
            case aCi /*16908321*/:
                if (this.aCo != null) {
                    ((ClipboardManager) getSystemService("clipboard")).setText(this.aCo);
                    Toast.makeText(this, (int) R.string.f1361dialog_pageattr_msg_copy, 0).show();
                }
                z = true;
                break;
            case aCk /*16908322*/:
                if (this.aCp != null) {
                    String obj = ((ClipboardManager) getSystemService("clipboard")).getText().toString();
                    if (!(this.aCp instanceof UCEditText)) {
                        obj = obj.replace(10, ' ');
                    } else if (this.aCp.yX()) {
                        obj = obj.replace(10, ' ');
                    }
                    StringBuffer stringBuffer2 = new StringBuffer();
                    int length = this.aCp.getText().length();
                    if (this.aCp.aDR > 0 && length >= this.aCp.aDR) {
                        return true;
                    }
                    int selectionStart = this.aCp.getSelectionStart();
                    stringBuffer2.append(this.aCp.getText().subSequence(0, selectionStart));
                    int length2 = obj.length();
                    int i = (this.aCp.aDR <= 0 || length + length2 <= this.aCp.aDR) ? length2 : this.aCp.aDR - length;
                    stringBuffer2.append(obj.subSequence(0, i));
                    stringBuffer2.append(this.aCp.getText().subSequence(this.aCp.getSelectionEnd(), this.aCp.length()));
                    this.aCp.setText(stringBuffer2.toString());
                    this.aCp.setSelection(selectionStart, selectionStart + i);
                }
                z = true;
                break;
            case 16908323:
            case 16908325:
            case 16908326:
            case 16908327:
            default:
                z = false;
                break;
            case aCh /*16908324*/:
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
                if (inputMethodManager != null) {
                    inputMethodManager.showInputMethodPicker();
                }
                z = true;
                break;
            case aCm /*16908328*/:
                if (this.aCp != null) {
                    this.aCp.aDQ = true;
                    z = false;
                    break;
                }
                z = false;
                break;
            case aCn /*16908329*/:
                if (this.aCp != null) {
                    this.aCp.aDQ = false;
                }
                z = false;
                break;
        }
        this.aCp = null;
        closeContextMenu();
        return z;
    }

    public void onContextMenuClosed(Menu menu) {
        closeContextMenu();
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        if (this.aCg == null) {
            this.aCg = new c(this);
        }
        contextMenu.clear();
        if (this.lF == null) {
            this.lF = new f(this);
            this.lF.a();
        }
        c cVar = (c) contextMenu;
        this.lF.a(cVar);
        cVar.a(this.lF);
        this.lF.a(this);
        cVar.cV();
        if (view instanceof UCEditText) {
            view.requestFocus();
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), 0);
            CharSequence text = ((ClipboardManager) getSystemService("clipboard")).getText();
            this.aCp = (UCEditText) view;
            int selectionStart = this.aCp.getSelectionStart();
            int selectionEnd = this.aCp.getSelectionEnd();
            if (selectionStart > selectionEnd) {
                this.aCo = this.aCp.getText().toString().substring(selectionEnd, selectionStart);
            } else {
                this.aCo = this.aCp.getText().toString().substring(selectionStart, selectionEnd);
            }
            if (this.aCp.length() != 0) {
                contextMenu.add(0, (int) aCl, 0, (int) R.string.f1104contextmenu_input_select_all).setVisible(true).setEnabled(true).setIcon(e.Pb().getDrawable(UCR.drawable.aVJ));
            }
            if (this.aCp.getSelectionStart() != this.aCp.getSelectionEnd() && (this.aCp.getTransformationMethod() == null || !(this.aCp.getTransformationMethod() instanceof PasswordTransformationMethod))) {
                contextMenu.add(0, (int) aCi, 0, (int) R.string.f1101contextmenu_input_copy).setVisible(true).setEnabled(true).setIcon(e.Pb().getDrawable(UCR.drawable.aUZ));
            }
            if (text != null && text.length() > 0) {
                contextMenu.add(0, (int) aCk, 0, (int) R.string.f1103contextmenu_input_paste).setVisible(true).setEnabled(true).setIcon(e.Pb().getDrawable(UCR.drawable.aVB));
            }
            contextMenu.add(0, (int) aCh, 0, (int) R.string.f1100contextmenu_pick_inputmethod).setVisible(true).setEnabled(true).setIcon(e.Pb().getDrawable(UCR.drawable.aVn));
        }
        this.lF.show();
        ((c) this.aCg).notifyDataSetChanged();
    }

    public boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
        if (!this.aCf.contains(Integer.valueOf(adapterView.getId()))) {
            return false;
        }
        if (this.aCg == null) {
            this.aCg = new c(this);
        }
        onCreateContextMenu(this.aCg, adapterView, null);
        return true;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 82) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onLongClick(View view) {
        if (!this.aCf.contains(Integer.valueOf(view.getId()))) {
            return false;
        }
        if (this.aCg == null) {
            this.aCg = new c(this);
        }
        onCreateContextMenu(this.aCg, view, null);
        return true;
    }

    public void openContextMenu(View view) {
        if (this.aCg == null) {
            this.aCg = new c(this);
        }
        onCreateContextMenu(this.aCg, view, null);
    }

    public void registerForContextMenu(View view) {
        if (view instanceof AdapterView) {
            ((AdapterView) view).setOnItemLongClickListener(this);
        } else {
            view.setOnLongClickListener(this);
        }
        if (this.aCf == null) {
            this.aCf = new Vector();
        }
        this.aCf.add(Integer.valueOf(view.getId()));
    }

    public void unregisterForContextMenu(View view) {
        this.aCf.remove(Integer.valueOf(view.getId()));
    }
}
