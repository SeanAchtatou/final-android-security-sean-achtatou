package com.uc.browser;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import b.a.a.c.e;
import com.uc.browser.UCR;

public class ViewZoomControls extends RelativeLayout implements View.OnClickListener {
    public static final int aAO = 3;
    public static int bCE = 0;
    public static int bCF = 2;
    private static final int bCH = 2000;
    private static final int bCI = 100;
    public static final int bCK = 1;
    public static final int bCL = 2;
    public static int xH = 2;
    public static int xI = 1;
    public ZoomController bCB;
    private ImageView bCC;
    private ImageView bCD;
    private int bCG = bCE;
    private HideRunnable bCJ;
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    if (ViewZoomControls.this.getVisibility() != 4) {
                        ViewZoomControls.super.setVisibility(4);
                        return;
                    }
                    return;
                case 2:
                    if (ViewZoomControls.this.getVisibility() != 0) {
                        ViewZoomControls.super.setVisibility(0);
                        return;
                    }
                    return;
                case 3:
                    ViewZoomControls.this.hT(message.arg1);
                    return;
                default:
                    return;
            }
        }
    };

    class HideRunnable extends Thread {
        public int delay;

        private HideRunnable() {
        }

        public void run() {
            this.delay = 1501;
            ViewZoomControls.this.handler.sendMessage(ViewZoomControls.this.handler.obtainMessage(2));
            while (this.delay > 0) {
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                }
                this.delay -= e.HTTP_MULT_CHOICE;
            }
            ViewZoomControls.this.handler.sendMessage(ViewZoomControls.this.handler.obtainMessage(1));
        }
    }

    public interface ZoomController {
        int ah();

        int j(int i);

        boolean uf();

        boolean zoomIn();

        boolean zoomOut();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.uc.browser.ViewZoomControls, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ViewZoomControls(Context context) {
        super(context);
        LayoutInflater.from(context).inflate((int) R.layout.f949zoom_juc_controls, (ViewGroup) this, true);
        this.bCC = (ImageView) findViewById(R.id.f871zoom_in);
        this.bCC.setOnClickListener(this);
        this.bCD = (ImageView) findViewById(R.id.f870zoom_out);
        this.bCD.setOnClickListener(this);
        setVisibility(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.uc.browser.ViewZoomControls, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ViewZoomControls(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        LayoutInflater.from(context).inflate((int) R.layout.f949zoom_juc_controls, (ViewGroup) this, true);
        this.bCC = (ImageView) findViewById(R.id.f871zoom_in);
        this.bCC.setOnClickListener(this);
        this.bCD = (ImageView) findViewById(R.id.f870zoom_out);
        this.bCD.setOnClickListener(this);
        setVisibility(4);
    }

    private void Jc() {
        synchronized (HideRunnable.class) {
            if (this.bCJ == null || this.bCJ.delay < 0) {
                this.bCJ = new HideRunnable();
                this.bCJ.start();
            } else {
                this.bCJ.delay = bCH;
            }
        }
    }

    public boolean a(ZoomController zoomController) {
        if (zoomController == null) {
            this.bCB = null;
            return false;
        }
        this.bCB = zoomController;
        return true;
    }

    public void hT(int i) {
        try {
            this.bCG = i;
            boolean Fz = ActivityBrowser.Fz();
            com.uc.h.e Pb = com.uc.h.e.Pb();
            if (this.bCG == bCF) {
                this.bCD.setImageDrawable(Pb.km(Fz ? R.drawable.f587juc_out_available_night : R.drawable.f586juc_out_available));
                this.bCC.setImageDrawable(Pb.km(Fz ? R.drawable.f585juc_in_unavailable_night : R.drawable.f584juc_in_unavailable));
            } else if (this.bCG == bCE) {
                this.bCD.setImageDrawable(Pb.km(Fz ? R.drawable.f589juc_out_unavailable_night : R.drawable.f588juc_out_unavailable));
                this.bCC.setImageDrawable(Pb.km(Fz ? R.drawable.f583juc_in_available_night : R.drawable.f582juc_in_available));
            } else {
                this.bCD.setImageDrawable(Pb.km(Fz ? R.drawable.f587juc_out_available_night : R.drawable.f586juc_out_available));
                this.bCC.setImageDrawable(Pb.km(Fz ? R.drawable.f583juc_in_available_night : R.drawable.f582juc_in_available));
            }
        } catch (Exception e) {
        } catch (OutOfMemoryError e2) {
            System.gc();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, int, java.lang.Object):void
     arg types: [int, int, boolean]
     candidates:
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.util.HashMap, int):void
      com.uc.browser.ModelBrowser.a(int, int, long):void
      com.uc.browser.ModelBrowser.a(int, java.lang.String, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WebViewJUC, int, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, com.uc.browser.WindowUCWeb, int):void
      com.uc.browser.ModelBrowser.a(int, int, java.lang.Object):void */
    public void onClick(View view) {
        int j;
        Jc();
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(83, 0, (Object) false);
        }
        if (this.bCB != null) {
            int id = view.getId();
            int i = bCE;
            switch (id) {
                case R.id.f870zoom_out:
                    if (this.bCG != bCE) {
                        j = this.bCB.j(xI);
                        this.bCB.zoomOut();
                        this.bCD.setImageDrawable(com.uc.h.e.Pb().getDrawable(UCR.drawable.aTQ));
                        break;
                    } else {
                        return;
                    }
                case R.id.f871zoom_in:
                    if (this.bCG != bCF) {
                        j = this.bCB.j(xH);
                        this.bCB.zoomIn();
                        this.bCC.setImageDrawable(com.uc.h.e.Pb().getDrawable(UCR.drawable.aTR));
                        break;
                    } else {
                        return;
                    }
                default:
                    return;
            }
            Message obtain = Message.obtain();
            obtain.what = 3;
            obtain.arg1 = j;
            this.handler.sendMessageDelayed(obtain, 100);
        }
    }

    public void setVisibility(int i) {
        super.setVisibility(i);
        if (i == 0) {
            Jc();
        }
    }
}
