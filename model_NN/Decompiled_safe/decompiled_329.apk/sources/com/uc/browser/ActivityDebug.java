package com.uc.browser;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.uc.a.e;

public class ActivityDebug extends Activity implements View.OnClickListener {
    public static final String aAA = "debug_stat_add1";
    public static final String aAw = "debug_ip_set";
    public static final String aAx = "debug_nemo_add";
    public static final String aAy = "debug_dispatch_add";
    public static final String aAz = "debug_stat_add";
    View aAp;
    Spinner aAq;
    EditText aAr;
    EditText aAs;
    EditText aAt;
    EditText aAu;
    EditText aAv;

    public void a(int i, String str, String str2, String str3, String str4) {
        e.nR().a(i, str, str2, str3, str4);
    }

    public void onClick(View view) {
        int selectedItemPosition = this.aAq.getSelectedItemPosition();
        String obj = this.aAr.getText().toString();
        String obj2 = this.aAs.getText().toString();
        String obj3 = this.aAt.getText().toString();
        String obj4 = this.aAu.getText().toString();
        a(selectedItemPosition, obj, obj2, obj3, obj4);
        PreferenceManager.getDefaultSharedPreferences(this).edit().putInt(aAw, selectedItemPosition).putString(aAx, obj).putString(aAy, obj2).putString(aAz, obj3).putString(aAA, obj4).commit();
        try {
            WebViewJUC.cff = Integer.parseInt(this.aAv.getText().toString());
        } catch (Exception e) {
            WebViewJUC.cff = 750;
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.f890debug_setting);
        this.aAp = findViewById(R.id.f728debug_submit);
        this.aAq = (Spinner) findViewById(R.id.f722test_ip);
        this.aAr = (EditText) findViewById(R.id.f723zjj_add);
        this.aAs = (EditText) findViewById(R.id.f724dpc_add);
        this.aAt = (EditText) findViewById(R.id.f725upl_add0);
        this.aAu = (EditText) findViewById(R.id.f726upl_add1);
        this.aAv = (EditText) findViewById(R.id.f727scrollTimer);
        this.aAp.setOnClickListener(this);
        this.aAq.setAdapter((SpinnerAdapter) new ArrayAdapter(this, (int) R.layout.f883bookmark_dialog_spinneritem, new String[]{"不使用", "dispatcher", "中间件", "中间件+dispatcher", "统计", "统计+dispatcher", "统计+中间件", "统计+中间件+dispatcher"}));
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        byte b2 = (byte) defaultSharedPreferences.getInt(aAw, e.nR().pJ());
        String string = defaultSharedPreferences.getString(aAx, e.nR().pK());
        String string2 = defaultSharedPreferences.getString(aAy, e.nR().pL());
        String string3 = defaultSharedPreferences.getString(aAz, e.nR().da(0));
        String string4 = defaultSharedPreferences.getString(aAA, e.nR().da(1));
        a(b2, string, string2, string3, string4);
        this.aAq.setSelection(b2);
        this.aAr.setText(string);
        this.aAs.setText(string2);
        this.aAt.setText(string3);
        this.aAu.setText(string4);
        this.aAv.setText(String.valueOf(WebViewJUC.cff));
    }
}
