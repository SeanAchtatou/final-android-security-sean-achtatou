package com.uc.browser;

import android.os.Message;
import android.webkit.WebView;
import b.a.a.f;
import b.b;
import java.util.Iterator;
import java.util.Vector;

public class MultiWindowManager {
    private static final String LOGTAG = "MULTI_WINDOW";
    private static final int apR = 15;
    private Vector apP = new Vector();
    private Vector apQ = new Vector();
    private ActivityBrowser bk;
    private WindowUCWeb vK;

    private void h(WindowUCWeb windowUCWeb, boolean z) {
        if (windowUCWeb != null) {
            if (this.vK != null) {
                this.vK.bD();
            }
            this.vK = windowUCWeb;
            this.vK.bE();
        }
    }

    private void t(WindowUCWeb windowUCWeb) {
        windowUCWeb.destroy();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.c(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.uc.browser.ModelBrowser.c(com.uc.browser.ModelBrowser, android.content.Context):android.app.Dialog
      com.uc.browser.ModelBrowser.c(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.c(com.uc.browser.ModelBrowser, boolean):boolean
      com.uc.browser.ModelBrowser.c(boolean, boolean):void */
    /* access modifiers changed from: package-private */
    public void I() {
        if (this.vK != null) {
            f.l(0, f.atp);
            f.l(1, f.atM);
            this.vK.I();
        }
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().iL();
            ModelBrowser.gD().c(true, false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.c(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.uc.browser.ModelBrowser.c(com.uc.browser.ModelBrowser, android.content.Context):android.app.Dialog
      com.uc.browser.ModelBrowser.c(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.c(com.uc.browser.ModelBrowser, boolean):boolean
      com.uc.browser.ModelBrowser.c(boolean, boolean):void */
    /* access modifiers changed from: package-private */
    public void J() {
        if (this.vK != null) {
            this.vK.J();
        }
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().c(false, false);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean M() {
        if (this.vK != null) {
            return this.vK.M();
        }
        return false;
    }

    public WindowUCWeb a(Message message, ActivityBrowser activityBrowser) {
        if (true == wg()) {
            WindowUCWeb windowUCWeb = new WindowUCWeb();
            this.apP.add(windowUCWeb);
            this.apQ.add(windowUCWeb);
            windowUCWeb.a(activityBrowser);
            ((WebView.WebViewTransport) message.obj).setWebView(windowUCWeb.bh());
            message.sendToTarget();
            h(windowUCWeb, true);
            if (ModelBrowser.gD() == null) {
                return null;
            }
            ModelBrowser.gD().a(17, 1, windowUCWeb);
            return windowUCWeb;
        }
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().aS(15);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void aT() {
        Iterator it = this.apP.iterator();
        while (it.hasNext()) {
            WindowUCWeb windowUCWeb = (WindowUCWeb) it.next();
            if (windowUCWeb != null && windowUCWeb == this.vK) {
                windowUCWeb.r(1);
            } else if (windowUCWeb != null) {
                windowUCWeb.r(0);
            }
        }
    }

    public void bz() {
        Iterator it = this.apP.iterator();
        while (it.hasNext()) {
            WindowUCWeb windowUCWeb = (WindowUCWeb) it.next();
            if (windowUCWeb != null) {
                windowUCWeb.bz();
            }
        }
    }

    public WindowUCWeb cI(String str) {
        String str2;
        WindowUCWeb windowUCWeb = new WindowUCWeb();
        if (str == null || !str.endsWith(b.acu)) {
            str2 = str;
        } else {
            windowUCWeb.l(true);
            str2 = str.substring(0, str.length() - b.acu.length());
        }
        windowUCWeb.bX = str2;
        this.apP.add(windowUCWeb);
        if (this.apP.size() >= 2) {
            this.apQ.insertElementAt(windowUCWeb, this.apP.size() - 2);
        } else {
            this.apQ.add(windowUCWeb);
        }
        windowUCWeb.i(false);
        Object[] objArr = {windowUCWeb, this.vK};
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(53, 2, objArr);
            ModelBrowser.gD().aS(ModelBrowser.zM);
        }
        return windowUCWeb;
    }

    public WindowUCWeb cJ(String str) {
        if (true == wg()) {
            WindowUCWeb windowUCWeb = new WindowUCWeb();
            windowUCWeb.bX = str;
            this.apP.add(windowUCWeb);
            this.apQ.add(windowUCWeb);
            h(windowUCWeb, true);
            if (ModelBrowser.gD() == null) {
                return null;
            }
            ModelBrowser.gD().a(17, 0, windowUCWeb);
            return windowUCWeb;
        }
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().aS(15);
        }
        return null;
    }

    public void clearFormData() {
        Iterator it = this.apP.iterator();
        while (it.hasNext()) {
            WindowUCWeb windowUCWeb = (WindowUCWeb) it.next();
            if (windowUCWeb != null) {
                windowUCWeb.clearFormData();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void destroy() {
        Iterator it = this.apP.iterator();
        while (it.hasNext()) {
            WindowUCWeb windowUCWeb = (WindowUCWeb) it.next();
            if (windowUCWeb != null) {
                windowUCWeb.destroy();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public WindowUCWeb eN(int i) {
        if (i < 0 || i >= this.apP.size()) {
            return null;
        }
        return (WindowUCWeb) this.apP.get(i);
    }

    public boolean eO(int i) {
        WindowUCWeb eN = eN(i);
        if (eN == null) {
            return false;
        }
        eN.by();
        this.apP.remove(eN);
        this.apQ.remove(eN);
        if (eN == this.vK) {
            if (wh() == 0) {
                h(cJ(null), false);
            } else if (i == this.apP.size()) {
                WindowUCWeb windowUCWeb = (WindowUCWeb) this.apP.get(i - 1);
                h(windowUCWeb, false);
                this.apQ.remove(windowUCWeb);
                this.apQ.add(windowUCWeb);
            } else {
                WindowUCWeb windowUCWeb2 = (WindowUCWeb) this.apP.get(i);
                h(windowUCWeb2, false);
                this.apQ.remove(windowUCWeb2);
                this.apQ.add(windowUCWeb2);
            }
        }
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().aS(103);
        }
        eN.destroy();
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().s(this.vK);
            ModelBrowser.gD().hP();
        }
        ViewMainBar.kI();
        return true;
    }

    public void eP(int i) {
        WindowUCWeb windowUCWeb = (WindowUCWeb) this.apP.get(i);
        h(windowUCWeb, false);
        this.apQ.remove(windowUCWeb);
        this.apQ.add(windowUCWeb);
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(this.vK, (WindowUCWeb) null, 1);
        }
    }

    public void eQ(int i) {
        WindowUCWeb windowUCWeb;
        if (i >= 0 && i < this.apP.size() && (windowUCWeb = (WindowUCWeb) this.apP.elementAt(i)) != null) {
            windowUCWeb.by();
            this.apP.remove(windowUCWeb);
            this.apQ.remove(windowUCWeb);
            windowUCWeb.destroy();
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(103);
            }
        }
    }

    public void h(boolean z) {
        Iterator it = this.apP.iterator();
        while (it.hasNext()) {
            WindowUCWeb windowUCWeb = (WindowUCWeb) it.next();
            if (windowUCWeb != null) {
                windowUCWeb.h(z);
            }
        }
    }

    public WindowUCWeb k(int i, String str) {
        WindowUCWeb windowUCWeb = new WindowUCWeb();
        windowUCWeb.bX = str;
        this.apP.insertElementAt(windowUCWeb, i);
        this.apQ.add(windowUCWeb);
        h(windowUCWeb, true);
        if (ModelBrowser.gD() == null) {
            return null;
        }
        ModelBrowser.gD().a(17, 0, windowUCWeb);
        return windowUCWeb;
    }

    public int u(byte b2) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.apP.size()) {
                return -1;
            }
            WindowUCWeb windowUCWeb = (WindowUCWeb) this.apP.elementAt(i2);
            if (windowUCWeb != null && b2 == windowUCWeb.bH()) {
                return i2;
            }
            i = i2 + 1;
        }
    }

    public boolean wg() {
        return 15 > wh();
    }

    public int wh() {
        return this.apP.size();
    }

    public WindowUCWeb wi() {
        return this.vK;
    }

    public int wj() {
        for (int i = 0; i < this.apP.size(); i++) {
            if (this.vK == this.apP.get(i)) {
                return i;
            }
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public void wk() {
        if (1 != wh()) {
            WindowUCWeb windowUCWeb = (WindowUCWeb) this.apQ.get(0);
            if (windowUCWeb != null) {
                t(windowUCWeb);
                System.gc();
                return;
            }
            System.gc();
        }
    }

    public int wl() {
        int i = 0;
        Iterator it = this.apP.iterator();
        while (it.hasNext()) {
            WindowUCWeb windowUCWeb = (WindowUCWeb) it.next();
            if (windowUCWeb != null && windowUCWeb.bF()) {
                i++;
            }
        }
        return i;
    }
}
