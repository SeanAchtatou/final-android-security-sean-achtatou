package com.uc.browser;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import com.uc.browser.UCAlertDialog;
import com.uc.browser.UCR;
import com.uc.h.e;
import java.util.List;

public class UCSpinner extends View implements View.OnClickListener {
    private List VZ;
    private float Wa;
    UCAlertDialog Wb;
    /* access modifiers changed from: private */
    public int kV;
    private int textColor;

    public UCSpinner(Context context) {
        this(context, null);
    }

    public UCSpinner(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.VZ = null;
        this.kV = 0;
        this.textColor = -65536;
        this.Wa = 15.0f;
        this.Wb = null;
        e Pb = e.Pb();
        this.textColor = getResources().getColor(R.color.f97uc_spinner_text_color);
        this.Wa = getResources().getDimension(R.dimen.f327listitem_title_text);
        setBackgroundDrawable(Pb.getDrawable(UCR.drawable.aXk));
        setOnClickListener(this);
    }

    public int cQ() {
        return this.kV;
    }

    public void d(List list) {
        this.VZ = list;
    }

    public void draw(Canvas canvas) {
        String sp;
        Drawable background = getBackground();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        if (background != null) {
            background.setBounds(0, 0, getWidth(), getHeight());
            background.draw(canvas);
        }
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setTextSize(this.Wa);
        int ascent = (int) ((-paint.descent()) - paint.ascent());
        paint.setColor(this.textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        int width = (getWidth() - paddingRight) - paddingLeft;
        if (width > 0 && (sp = sp()) != null) {
            String valueOf = String.valueOf(sp);
            if (paint.measureText(valueOf, 0, valueOf.length()) <= ((float) width)) {
                canvas.drawText(valueOf, (float) paddingLeft, (float) ((getHeight() + ascent) >> 1), paint);
                return;
            }
            float f = 0.0f;
            int i = 1;
            while (f <= ((float) width)) {
                f = paint.measureText(valueOf, 0, i);
                i++;
            }
            int i2 = i - 2;
            if (i2 > 0) {
                canvas.drawText(valueOf.substring(0, i2), (float) paddingLeft, (float) ((ascent + getHeight()) >> 1), paint);
            }
        }
    }

    public void eb(int i) {
        this.kV = i;
    }

    public void onClick(View view) {
        if (this.VZ != null) {
            String[] strArr = new String[this.VZ.size()];
            this.VZ.toArray(strArr);
            UCAlertDialog.Builder builder = new UCAlertDialog.Builder(getContext());
            builder.aH(R.string.f954uc_spinner_title);
            builder.a(strArr, this.kV, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    int unused = UCSpinner.this.kV = i;
                    UCSpinner.this.invalidate();
                    UCSpinner.this.Wb.dismiss();
                }
            });
            builder.b((int) R.string.f955uc_spinner_cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    UCSpinner.this.Wb.dismiss();
                }
            });
            this.Wb = builder.fh();
            this.Wb.show();
        }
    }

    public void setTextColor(int i) {
        this.textColor = i;
    }

    public void setTextSize(float f) {
        this.Wa = f;
    }

    public String sp() {
        if (this.VZ == null || this.kV < 0 || this.kV >= this.VZ.size()) {
            return null;
        }
        return (String) this.VZ.get(this.kV);
    }
}
