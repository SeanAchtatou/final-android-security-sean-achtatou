package com.uc.browser;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.uc.browser.UCR;
import com.uc.e.ac;
import com.uc.e.ad;
import com.uc.e.h;
import com.uc.e.r;
import com.uc.h.a;
import com.uc.h.e;
import java.util.Iterator;
import java.util.Vector;

public class BarLayout extends View implements ad, a {
    r HN;

    public BarLayout(Context context) {
        super(context);
        a();
    }

    public BarLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public BarLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    private void a() {
        this.HN = new r();
        this.HN.a(this);
        e Pb = e.Pb();
        Pb.a(this);
        this.HN.aw(Pb.kp(R.dimen.f182controlbar_item_width), Pb.kp(R.dimen.f177controlbar_height));
        k();
    }

    public void a(ac acVar) {
        this.HN.b(acVar);
    }

    public void aw(int i, int i2) {
        this.HN.aw(i, i2);
    }

    public void b(h hVar) {
        this.HN.c(hVar);
    }

    public void bD(int i) {
        this.HN.eB(i);
    }

    public void cJ() {
        invalidate();
    }

    public void clear() {
        this.HN.clear();
    }

    public void eB(int i) {
        this.HN.eB(i);
    }

    public void k() {
        e Pb = e.Pb();
        this.HN.s(Pb.getDrawable(UCR.drawable.aXp));
        this.HN.m(Pb.getDrawable(UCR.drawable.aWt));
        int color = Pb.getColor(10);
        int color2 = Pb.getColor(11);
        Vector xa = this.HN.xa();
        if (xa != null) {
            Iterator it = xa.iterator();
            while (it.hasNext()) {
                ac acVar = (ac) it.next();
                acVar.setTextColor(color);
                acVar.ks(color2);
            }
        }
    }

    public void kJ() {
        this.HN.yi();
    }

    public void m(Drawable drawable) {
        this.HN.m(drawable);
    }

    public void n(Drawable drawable) {
        this.HN.s(drawable);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.HN.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.HN.setSize(i3 - i, i4 - i2);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean onTouchEvent = super.onTouchEvent(motionEvent);
        if (this.HN.d(motionEvent)) {
            return true;
        }
        return onTouchEvent;
    }
}
