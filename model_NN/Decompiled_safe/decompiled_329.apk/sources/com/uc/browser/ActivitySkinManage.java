package com.uc.browser;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.uc.c.au;
import com.uc.h.a;
import com.uc.h.d;
import com.uc.h.e;

public class ActivitySkinManage extends ActivityWithUCMenu implements a {
    com.uc.g.a bXL;
    d lC;

    public void ON() {
        onStart();
    }

    public void k() {
        ((TitleBarTextView) findViewById(R.id.f654Browser_TitleBar)).k();
        this.bXL.k();
        this.bXL.invalidate();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        switch (i) {
            case 0:
                if (i2 != 77) {
                    if (intent != null) {
                        b.a.a.d.f(this, intent.getExtras().getString(ActivityChooseFile.uE) + au.aGF + intent.getExtras().getString(ActivityChooseFile.uF));
                        break;
                    }
                } else {
                    finish();
                    return;
                }
                break;
        }
        if (i2 == -1) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ActivityBrowser.g(this);
        requestWindowFeature(1);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i != 84) {
            return super.onKeyUp(i, keyEvent);
        }
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a((int) ModelBrowser.zJ, this);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        e.Pb().b(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        e.Pb().a(this);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        this.lC = new d(this);
        RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(this).inflate((int) R.layout.f942title_container, (ViewGroup) null);
        ((TitleBarTextView) relativeLayout.findViewById(R.id.f654Browser_TitleBar)).setText("皮肤管理");
        setContentView(relativeLayout);
        this.bXL = new com.uc.g.a(this, this.lC);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(3, R.id.f654Browser_TitleBar);
        relativeLayout.addView(this.bXL, layoutParams);
        super.onStart();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z && ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(this);
        }
        if (z && ActivityBrowser.Fz()) {
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
            }
            ActivityBrowser.e(this);
        }
    }
}
