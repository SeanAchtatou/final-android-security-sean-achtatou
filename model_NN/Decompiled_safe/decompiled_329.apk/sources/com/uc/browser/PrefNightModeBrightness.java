package com.uc.browser;

import android.app.Activity;
import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.SeekBar;
import com.uc.a.e;
import com.uc.a.h;

public class PrefNightModeBrightness extends DialogPreference implements SeekBar.OnSeekBarChangeListener {
    public static final int bxB = 25;
    SeekBar bxC;
    int bxD = 25;

    public PrefNightModeBrightness(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setDialogLayoutResource(R.layout.f927pref_seekbar);
        setPositiveButtonText((int) R.string.f1266dlg_button_ok);
        setNegativeButtonText((int) R.string.f1267dlg_button_cancle);
    }

    /* access modifiers changed from: protected */
    public View onCreateDialogView() {
        View onCreateDialogView = super.onCreateDialogView();
        this.bxC = (SeekBar) onCreateDialogView.findViewById(R.id.f819seekbar);
        this.bxC.setOnSeekBarChangeListener(this);
        String bw = e.nR().nY().bw(h.afv);
        if (bw != null) {
            this.bxD = Integer.parseInt(bw);
        }
        this.bxD -= 25;
        this.bxC.setProgress(this.bxD);
        ActivityBrowser.a((Activity) getContext(), this.bxD);
        return onCreateDialogView;
    }

    /* access modifiers changed from: protected */
    public void onDialogClosed(boolean z) {
        super.onDialogClosed(z);
        if (!ActivityBrowser.Fz()) {
            ActivityBrowser.a((Activity) getContext(), -1);
        }
        if (z) {
            this.bxD = this.bxC.getProgress() + 25;
            e.nR().nY().w(h.afv, "" + this.bxD);
        } else if (ActivityBrowser.Fz()) {
            ActivityBrowser.a((Activity) getContext(), this.bxD);
        }
    }

    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        ActivityBrowser.a((Activity) getContext(), i + 25);
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }
}
