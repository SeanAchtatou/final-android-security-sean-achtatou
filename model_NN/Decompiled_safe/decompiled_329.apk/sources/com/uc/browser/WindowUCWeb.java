package com.uc.browser;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieSyncManager;
import android.webkit.HttpAuthHandler;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebIconDatabase;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import b.a.a.e;
import b.a.a.f;
import b.b.a;
import com.uc.a.c;
import com.uc.a.h;
import com.uc.a.n;
import com.uc.a.q;
import com.uc.b.b;
import com.uc.browser.ModelBrowser;
import com.uc.browser.UCAlertDialog;
import com.uc.browser.UCR;
import com.uc.browser.ViewMainpage;
import com.uc.c.an;
import com.uc.c.au;
import com.uc.c.bk;
import com.uc.c.bx;
import com.uc.c.cd;
import com.uc.c.w;
import com.uc.e.b.d;
import com.uc.e.b.i;
import com.uc.f.g;
import com.uc.plugin.Plugin;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Vector;

public class WindowUCWeb implements ViewMainpage.OnScreenChangeListener {
    private static final String LOGTAG = "WINDOW_UCWEB";
    public static final String bA = "u.uc.cn";
    public static int bE = 200;
    public static String bF = "com.alipay.android.app";
    private static final byte bH = 0;
    private static final byte bI = 1;
    private static final byte bJ = 2;
    private static final byte bK = -1;
    static final String bN = "javascript:";
    private static final String bS = "currentViewSign";
    private static final String bT = "currentWebViewSign";
    private static final String bU = "viewMainContentInstantiated";
    private static final String bV = "webViewJUCInstantiated";
    private static final String bW = "webViewZoomInstantiated";
    public static final byte cc = -1;
    public static final byte cd = 0;
    public static final byte ce = 1;
    public static final byte cf = 2;
    public static final byte cg = 3;
    public static final float ci = 0.60723f;
    private String bB;
    boolean bC;
    /* access modifiers changed from: private */
    public StreamingMediaThread bD;
    /* access modifiers changed from: private */
    public Vector bG;
    /* access modifiers changed from: private */
    public int bL;
    private byte bM;
    private Bundle bO;
    private Vector bP;
    private SparseArray bQ;
    /* access modifiers changed from: private */
    public boolean bR;
    public String bX;
    public boolean bY;
    private WebViewClient bZ;
    private ViewMainpage be;
    private int bf;
    /* access modifiers changed from: private */
    public WebViewJUC bg;
    /* access modifiers changed from: private */
    public WebViewZoom bh;
    /* access modifiers changed from: private */
    public View bi;
    private View bj;
    /* access modifiers changed from: private */
    public ActivityBrowser bk;
    /* access modifiers changed from: private */
    public boolean bl;
    /* access modifiers changed from: private */
    public boolean bm;
    /* access modifiers changed from: private */
    public boolean bn;
    /* access modifiers changed from: private */
    public boolean bo;
    private boolean bp;
    /* access modifiers changed from: private */
    public boolean bq;
    private String br;
    private String bs;
    private Bitmap bt;
    private Bitmap bu;
    /* access modifiers changed from: private */
    public e bv;
    /* access modifiers changed from: private */
    public boolean bw;
    private long bx;
    private boolean by;
    private boolean bz;
    /* access modifiers changed from: private */
    public int ca;
    private WebChromeClient cb;
    private byte ch;
    private int cj;
    private String ck;
    /* access modifiers changed from: private */
    public n p;

    class BackForwardItem {
        byte abU;
        byte ceq;

        BackForwardItem(byte b2) {
            this.abU = b2;
            this.ceq = 0;
        }

        BackForwardItem(byte b2, byte b3) {
            this.abU = b2;
            this.ceq = b3;
        }

        /* access modifiers changed from: package-private */
        public void G(byte b2) {
            this.ceq = b2;
        }
    }

    class StreamingMediaThread extends Thread {
        public Object UW = null;

        public StreamingMediaThread(Object obj) {
            this.UW = obj;
        }

        public void run() {
            try {
                Thread.sleep((long) WindowUCWeb.bE);
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a((int) ModelBrowser.zL, this.UW);
                }
            } catch (Exception e) {
            }
            StreamingMediaThread unused = WindowUCWeb.this.bD = (StreamingMediaThread) null;
        }
    }

    public WindowUCWeb() {
        this.bf = 1;
        this.bl = false;
        this.bm = false;
        this.bn = true;
        this.bo = false;
        this.bp = false;
        this.bq = true;
        this.bw = false;
        this.by = false;
        this.bz = false;
        this.bB = "";
        this.bC = false;
        this.bD = null;
        this.bG = new Vector();
        this.bL = -1;
        this.bM = 0;
        this.bR = false;
        this.bX = null;
        this.bY = false;
        this.bZ = new WebViewClient() {
            public void doUpdateVisitedHistory(WebView webView, String str, boolean z) {
                WebIconDatabase.getInstance().open(WindowUCWeb.this.bk.getDir("icons", 0).getPath());
                if (!str.regionMatches(true, 0, "about:", 0, 6)) {
                    super.doUpdateVisitedHistory(webView, str, z);
                }
            }

            public void onFormResubmission(WebView webView, final Message message, final Message message2) {
                new UCAlertDialog.Builder(WindowUCWeb.this.bk).aH(R.string.f1339browserFrameFormResubmitLabel).aG(R.string.f1340browserFrameFormResubmitMessage).a((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        message2.sendToTarget();
                    }
                }).c((int) R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        message.sendToTarget();
                    }
                }).show();
            }

            public void onLoadResource(WebView webView, String str) {
                if (str == null || str.length() > 0) {
                }
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.uc.browser.WindowUCWeb.b(com.uc.browser.WindowUCWeb, boolean):boolean
             arg types: [com.uc.browser.WindowUCWeb, int]
             candidates:
              com.uc.browser.WindowUCWeb.b(com.uc.browser.WindowUCWeb, int):void
              com.uc.browser.WindowUCWeb.b(android.content.Context, java.lang.String):void
              com.uc.browser.WindowUCWeb.b(com.uc.browser.WindowUCWeb, boolean):boolean */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.uc.browser.WindowUCWeb.c(com.uc.browser.WindowUCWeb, boolean):void
             arg types: [com.uc.browser.WindowUCWeb, int]
             candidates:
              com.uc.browser.WindowUCWeb.c(java.lang.String, java.lang.String):java.lang.String
              com.uc.browser.WindowUCWeb.c(android.content.Context, java.lang.String):void
              com.uc.browser.WindowUCWeb.c(int, android.view.KeyEvent):boolean
              com.uc.browser.WindowUCWeb.c(com.uc.browser.WindowUCWeb, boolean):void */
            public void onPageFinished(WebView webView, String str) {
                ModelBrowser gD = ModelBrowser.gD();
                if (true == WindowUCWeb.this.bl) {
                    boolean unused = WindowUCWeb.this.bl = false;
                    if (WindowUCWeb.this.bs() && gD != null) {
                        gD.hx();
                    }
                }
                if (WindowUCWeb.this.bi == WindowUCWeb.this.bh) {
                    WindowUCWeb.this.g(false);
                }
                if (gD != null) {
                    gD.removeMessages(24);
                    gD.gG();
                }
                CookieSyncManager.getInstance().sync();
                com.uc.a.e.nR().D(str, webView.getTitle());
                super.onPageFinished(webView, str);
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.uc.browser.WindowUCWeb.a(com.uc.browser.WindowUCWeb, boolean):boolean
             arg types: [com.uc.browser.WindowUCWeb, int]
             candidates:
              com.uc.browser.WindowUCWeb.a(com.uc.browser.WindowUCWeb, int):int
              com.uc.browser.WindowUCWeb.a(java.lang.String, boolean):android.view.View
              com.uc.browser.WindowUCWeb.a(com.uc.browser.WindowUCWeb, b.a.a.e):b.a.a.e
              com.uc.browser.WindowUCWeb.a(com.uc.browser.WindowUCWeb, com.uc.browser.WindowUCWeb$StreamingMediaThread):com.uc.browser.WindowUCWeb$StreamingMediaThread
              com.uc.browser.WindowUCWeb.a(java.lang.String, android.content.Context):android.view.View
              com.uc.browser.WindowUCWeb.a(android.content.Context, int):com.uc.browser.ViewMainpage
              com.uc.browser.WindowUCWeb.a(android.content.Context, java.lang.String):void
              com.uc.browser.WindowUCWeb.a(java.lang.String, com.uc.a.n):void
              com.uc.browser.WindowUCWeb.a(com.uc.browser.WindowUCWeb, boolean):boolean */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.uc.browser.WindowUCWeb.b(com.uc.browser.WindowUCWeb, boolean):boolean
             arg types: [com.uc.browser.WindowUCWeb, int]
             candidates:
              com.uc.browser.WindowUCWeb.b(com.uc.browser.WindowUCWeb, int):void
              com.uc.browser.WindowUCWeb.b(android.content.Context, java.lang.String):void
              com.uc.browser.WindowUCWeb.b(com.uc.browser.WindowUCWeb, boolean):boolean */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.uc.browser.WindowUCWeb.c(com.uc.browser.WindowUCWeb, boolean):void
             arg types: [com.uc.browser.WindowUCWeb, int]
             candidates:
              com.uc.browser.WindowUCWeb.c(java.lang.String, java.lang.String):java.lang.String
              com.uc.browser.WindowUCWeb.c(android.content.Context, java.lang.String):void
              com.uc.browser.WindowUCWeb.c(int, android.view.KeyEvent):boolean
              com.uc.browser.WindowUCWeb.c(com.uc.browser.WindowUCWeb, boolean):void */
            public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
                if (true == WindowUCWeb.this.bo) {
                    boolean unused = WindowUCWeb.this.bo = false;
                }
                if (!WindowUCWeb.this.bl) {
                    boolean unused2 = WindowUCWeb.this.bl = true;
                    WindowUCWeb.this.br();
                }
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(24, 30000);
                }
                if (WindowUCWeb.this.bi == WindowUCWeb.this.bh) {
                    WindowUCWeb.this.g(true);
                }
                WindowUCWeb.e(str);
                super.onPageStarted(webView, str, bitmap);
            }

            public void onReceivedError(WebView webView, int i, String str, String str2) {
            }

            public void onReceivedHttpAuthRequest(WebView webView, HttpAuthHandler httpAuthHandler, String str, String str2) {
                String str3;
                String str4;
                String[] httpAuthUsernamePassword;
                if (!httpAuthHandler.useHttpAuthUsernamePassword() || (httpAuthUsernamePassword = WindowUCWeb.this.bh.getHttpAuthUsernamePassword(str, str2)) == null || httpAuthUsernamePassword.length != 2) {
                    str3 = null;
                    str4 = null;
                } else {
                    str4 = httpAuthUsernamePassword[0];
                    str3 = httpAuthUsernamePassword[1];
                }
                if (str4 != null && str3 != null) {
                    httpAuthHandler.proceed(str4, str3);
                }
            }

            public void onTooManyRedirects(WebView webView, final Message message, final Message message2) {
                new UCAlertDialog.Builder(WindowUCWeb.this.bk).aH(R.string.f1337browserFrameRedirect).aG(R.string.f1338browserFrame307Post).a((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        message2.sendToTarget();
                    }
                }).c((int) R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        message.sendToTarget();
                    }
                }).show();
            }

            public void onUnhandledKeyEvent(WebView webView, KeyEvent keyEvent) {
                super.onUnhandledKeyEvent(webView, keyEvent);
            }

            public boolean shouldOverrideKeyEvent(WebView webView, KeyEvent keyEvent) {
                return WindowUCWeb.this.bk.getWindow().isShortcutKey(keyEvent.getKeyCode(), keyEvent);
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.uc.browser.WindowUCWeb.a(com.uc.browser.WindowUCWeb, boolean):boolean
             arg types: [com.uc.browser.WindowUCWeb, int]
             candidates:
              com.uc.browser.WindowUCWeb.a(com.uc.browser.WindowUCWeb, int):int
              com.uc.browser.WindowUCWeb.a(java.lang.String, boolean):android.view.View
              com.uc.browser.WindowUCWeb.a(com.uc.browser.WindowUCWeb, b.a.a.e):b.a.a.e
              com.uc.browser.WindowUCWeb.a(com.uc.browser.WindowUCWeb, com.uc.browser.WindowUCWeb$StreamingMediaThread):com.uc.browser.WindowUCWeb$StreamingMediaThread
              com.uc.browser.WindowUCWeb.a(java.lang.String, android.content.Context):android.view.View
              com.uc.browser.WindowUCWeb.a(android.content.Context, int):com.uc.browser.ViewMainpage
              com.uc.browser.WindowUCWeb.a(android.content.Context, java.lang.String):void
              com.uc.browser.WindowUCWeb.a(java.lang.String, com.uc.a.n):void
              com.uc.browser.WindowUCWeb.a(com.uc.browser.WindowUCWeb, boolean):boolean */
            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                String str2;
                if (str != null && str.startsWith("about:blank")) {
                    return super.shouldOverrideUrlLoading(webView, str);
                }
                if (str == null || !str.contains("ext:startpage")) {
                    str2 = str;
                } else {
                    str2 = str.trim();
                    if (str2.startsWith("ext:startpage")) {
                        return true;
                    }
                }
                if (true == WindowUCWeb.this.bl) {
                    boolean unused = WindowUCWeb.this.bo = true;
                    WindowUCWeb.this.bG.remove(WindowUCWeb.this.bL);
                    WindowUCWeb.g(WindowUCWeb.this);
                    String bw = com.uc.a.e.nR().nY().bw(h.afq);
                    if (String.valueOf(0).equals(bw.trim())) {
                        f.m(1, f.atz);
                    } else if (String.valueOf(1).equals(bw.trim())) {
                        f.m(1, f.atB);
                    } else {
                        f.m(1, f.atA);
                    }
                    if (!ActivityBrowser.Fz()) {
                        f.m(1, f.atC);
                    } else {
                        f.m(1, f.atD);
                    }
                    switch (a.OS()) {
                        case 1:
                            f.m(1, f.atI);
                            break;
                        case 2:
                            f.m(1, f.atF);
                            break;
                        case 3:
                            f.m(1, f.atE);
                            break;
                        case 4:
                            f.m(1, f.atH);
                            break;
                        case 5:
                            f.m(1, f.atG);
                            break;
                        case 6:
                            f.m(1, f.atJ);
                            break;
                    }
                    if (WindowUCWeb.this.bk != null && 2 == WindowUCWeb.this.bk.getResources().getConfiguration().orientation) {
                        f.m(1, f.atK);
                    }
                    if (com.uc.a.e.RD.equals(com.uc.a.e.nR().bw(h.afy))) {
                        f.m(1, f.atL);
                    }
                    switch (b.ec()) {
                        case 0:
                            f.m(1, f.atP);
                            break;
                        case 1:
                            f.m(1, f.atQ);
                            break;
                        case 2:
                            f.m(1, f.atR);
                            break;
                        case 3:
                            f.m(1, f.atS);
                            break;
                        case 4:
                            f.m(1, f.atT);
                            break;
                        case 5:
                            f.m(1, f.atU);
                            break;
                        case 99:
                            f.m(1, f.atV);
                            break;
                    }
                    if (!WindowUCWeb.this.bq) {
                        str2 = b.b.acH + str2;
                    }
                } else {
                    str2 = b.b.acH + str2;
                }
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(WindowUCWeb.this, str2);
                }
                return true;
            }
        };
        this.ca = 0;
        this.cb = new WebChromeClient() {
            public boolean onCreateWindow(WebView webView, boolean z, boolean z2, Message message) {
                if (ModelBrowser.gD() == null) {
                    return true;
                }
                ModelBrowser.gD().a(39, message);
                return true;
            }

            public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
                return super.onJsAlert(webView, str, str2, jsResult);
            }

            public boolean onJsBeforeUnload(WebView webView, String str, String str2, JsResult jsResult) {
                return super.onJsBeforeUnload(webView, str, str2, jsResult);
            }

            public boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
                return super.onJsConfirm(webView, str, str2, jsResult);
            }

            public boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
                return super.onJsPrompt(webView, str, str2, str3, jsPromptResult);
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.uc.browser.WindowUCWeb.d(com.uc.browser.WindowUCWeb, boolean):boolean
             arg types: [com.uc.browser.WindowUCWeb, int]
             candidates:
              com.uc.browser.WindowUCWeb.d(android.content.Context, java.lang.String):void
              com.uc.browser.WindowUCWeb.d(com.uc.browser.WindowUCWeb, boolean):boolean */
            public void onProgressChanged(WebView webView, int i) {
                if ((i > 0 && i != WindowUCWeb.this.ca && (i > WindowUCWeb.this.ca + 1 || i < WindowUCWeb.this.ca)) || 99 <= i) {
                    int unused = WindowUCWeb.this.ca = i;
                    if (true == WindowUCWeb.this.bn && true == WindowUCWeb.this.bm && ModelBrowser.gD() != null) {
                        ModelBrowser.gD().a(22, i, (Object) null);
                    }
                    if (100 == i) {
                        if (true == WindowUCWeb.this.bw) {
                            e unused2 = WindowUCWeb.this.bv = new e();
                            WindowUCWeb.this.bv.abP = false;
                            WindowUCWeb.this.bv.abL = webView.getUrl();
                            WindowUCWeb.this.bv.abN = webView.getFavicon();
                            WindowUCWeb.this.bv.abM = webView.getTitle();
                            if (WindowUCWeb.this.bv.abL != null && WindowUCWeb.this.bv.abL.length() > 0) {
                                com.uc.a.e.nR().oa().i(WindowUCWeb.this.bv);
                            }
                            e unused3 = WindowUCWeb.this.bv = (e) null;
                            boolean unused4 = WindowUCWeb.this.bw = false;
                        }
                        if (!WindowUCWeb.this.bh.zF()) {
                            if (WindowUCWeb.this.bn) {
                                ModelBrowser.gD().a(21, "UC浏览器");
                            }
                            if (true == WindowUCWeb.this.bh.zH() && !WindowUCWeb.this.bw && !WindowUCWeb.this.bo) {
                                WindowUCWeb.this.bh.zG();
                            } else if (!WindowUCWeb.this.bh.zH()) {
                                boolean unused5 = WindowUCWeb.this.bq = false;
                                WindowUCWeb.this.bh.bh(!WindowUCWeb.this.bq);
                            }
                        }
                    }
                }
            }

            public void onReceivedIcon(WebView webView, Bitmap bitmap) {
                if (bitmap != null) {
                }
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.uc.a.e.a(java.lang.String, java.lang.String, byte, boolean):boolean
             arg types: [java.lang.String, java.lang.String, byte, int]
             candidates:
              com.uc.a.e.a(java.lang.String, com.uc.a.n, int, boolean):int
              com.uc.a.e.a(int, int, float, float):void
              com.uc.a.e.a(int, java.util.Vector, int, boolean):void
              com.uc.a.e.a(java.lang.String, int, int, boolean):void
              com.uc.a.e.a(java.lang.String, java.lang.String, byte, boolean):boolean */
            public void onReceivedTitle(WebView webView, String str) {
                if (true == WindowUCWeb.this.bn) {
                    WindowUCWeb.this.bk.t(str);
                    if (!(ModelBrowser.gD() == null || WindowUCWeb.this.bh == null)) {
                        ModelBrowser.gD().a(127, WindowUCWeb.this.bh.getUrl());
                    }
                }
                if (WindowUCWeb.this.bh != null) {
                    com.uc.a.e.nR().ob().r(str, WindowUCWeb.this.bh.getUrl());
                }
                if (WindowUCWeb.this.bh != null && WindowUCWeb.this.bh.zN() && com.uc.a.e.nR().a(str, WindowUCWeb.this.bh.getUrl(), (byte) 0, true)) {
                    WindowUCWeb.this.p.re();
                }
            }
        };
        this.p = new n() {
            public void EA() {
                WindowUCWeb.this.bn();
            }

            public void EB() {
                WindowUCWeb.this.bG.remove(WindowUCWeb.this.bL);
                WindowUCWeb.g(WindowUCWeb.this);
            }

            public void EC() {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(19);
                }
            }

            public void ED() {
                WindowUCWeb.this.bn();
                WindowUCWeb.this.m(1);
            }

            public boolean EE() {
                if (ModelBrowser.gD() != null) {
                    return ModelBrowser.gD().aj(WindowUCWeb.bF);
                }
                return false;
            }

            public void EF() {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(22, 0, (Object) null);
                    ModelBrowser.gD().a(22, 10, (Object) null);
                }
            }

            public void EG() {
                if (WindowUCWeb.this.bg != null) {
                    WindowUCWeb.this.bg.EG();
                }
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.uc.browser.ModelBrowser.a(int, int, java.lang.Object):void
             arg types: [int, int, boolean]
             candidates:
              com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object, int):void
              com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String, java.lang.String):void
              com.uc.browser.ModelBrowser.a(java.lang.String, java.util.HashMap, int):void
              com.uc.browser.ModelBrowser.a(int, int, long):void
              com.uc.browser.ModelBrowser.a(int, java.lang.String, boolean):void
              com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, java.lang.String, java.lang.String):void
              com.uc.browser.ModelBrowser.a(com.uc.browser.WebViewJUC, int, int):void
              com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, com.uc.browser.WindowUCWeb, int):void
              com.uc.browser.ModelBrowser.a(int, int, java.lang.Object):void */
            public void EH() {
                if (an.vA().vC() && ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(83, 0, (Object) false);
                }
            }

            public boolean Em() {
                if (ModelBrowser.gD() == null) {
                    return false;
                }
                ModelBrowser.gD().a(44, (Object) null);
                return false;
            }

            public boolean Eo() {
                if (WindowUCWeb.this.bR) {
                    boolean unused = WindowUCWeb.this.bR = false;
                    if (ModelBrowser.gD() != null) {
                        ModelBrowser.gD().aS(38);
                    }
                }
                return false;
            }

            public void Es() {
                try {
                    if (WindowUCWeb.this.bg != null && WindowUCWeb.this.bk != null) {
                        WindowUCWeb.this.bg.b(WindowUCWeb.this.bk.getContentResolver());
                    }
                } catch (Exception e) {
                }
            }

            public void Et() {
                if (WindowUCWeb.this.bg != null) {
                    WindowUCWeb.this.bg.zG();
                }
            }

            public boolean Eu() {
                if (ModelBrowser.gD() != null) {
                    return ModelBrowser.gD().wK;
                }
                return false;
            }

            public void Ev() {
                if (WindowUCWeb.this.bg != null) {
                    WindowUCWeb.this.bg.stopLoading();
                }
            }

            public void Ez() {
                WindowUCWeb.this.bg.Ez();
            }

            public void H() {
                if (WindowUCWeb.this.bi != null && WindowUCWeb.this.bi == WindowUCWeb.this.bg) {
                    WindowUCWeb.this.bg.H();
                }
            }

            public void L(boolean z) {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a((int) ModelBrowser.zT, Boolean.valueOf(z));
                }
            }

            public void a(c cVar) {
                if (true == WindowUCWeb.this.bn && true == WindowUCWeb.this.bm && ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(92, cVar);
                }
            }

            public void a(Plugin plugin) {
                WindowUCWeb.this.bg.a(plugin);
            }

            public void a(Plugin plugin, int i, int i2, int i3, int i4) {
                WindowUCWeb.this.bg.a(plugin, i, i2, i3, i4);
            }

            public void a(String str, String str2, String str3, q qVar) {
                Object[] objArr = {new ModelBrowser.YesOrNoDlgData("", str, qVar), str2, str3};
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(150, objArr);
                }
            }

            public void a(int[] iArr, int[] iArr2, bk bkVar) {
                if (WindowUCWeb.this.bg != null && iArr != null && iArr2 != null) {
                    WindowUCWeb.this.bg.e(new int[]{iArr[0] - bkVar.left, iArr[1] - bkVar.top, iArr[2]}, new int[]{iArr2[0] - bkVar.left, iArr2[1] - bkVar.top, iArr2[2]});
                }
            }

            public boolean a(int i, int i2, int i3, int i4, int i5, String str, int i6, boolean z, Canvas canvas) {
                com.uc.h.e Pb = com.uc.h.e.Pb();
                com.uc.e.b.h hVar = z ? new com.uc.e.b.h(Pb.kq(R.dimen.f535webwidget_bold_stroke), Pb.getColor(114), Pb.getColor(116), Pb.getColor(118), Pb.kq(R.dimen.f540webwidget_frame_shadow_bold), Pb.kq(R.dimen.f537webwidget_frame_corner)) : new com.uc.e.b.h(Pb.kq(R.dimen.f536webwidget_thin_stroke), Pb.getColor(115), Pb.getColor(117), Pb.getColor(119), Pb.kq(R.dimen.f541webwidget_frame_shadow_thin), Pb.kq(R.dimen.f537webwidget_frame_corner));
                hVar.setBounds(i + 1, i2 + 1, (i + i3) - 1, (i2 + i4) - 1);
                hVar.draw(canvas);
                return true;
            }

            public boolean a(int i, int i2, int i3, int i4, int i5, boolean z, Canvas canvas) {
                i iVar;
                com.uc.h.e Pb = com.uc.h.e.Pb();
                if (z) {
                    iVar = new i(Pb.kq(R.dimen.f535webwidget_bold_stroke), Pb.getColor(114), Pb.getColor(116), Pb.getColor(116), Pb.getColor(118), Pb.kq(R.dimen.f540webwidget_frame_shadow_bold), Pb.kq(R.dimen.f538webwidget_button_corner), Pb.getColor(137), Pb.getColor(138), Pb.getColor(136), i5, Pb.getColor(139), true, Pb.getColor(140), Pb.getColor(141));
                } else {
                    iVar = new i(Pb.kq(R.dimen.f536webwidget_thin_stroke), Pb.getColor(115), Pb.getColor(117), Pb.getColor(117), Pb.getColor(119), Pb.kq(R.dimen.f540webwidget_frame_shadow_bold), Pb.kq(R.dimen.f538webwidget_button_corner), Pb.getColor(131), Pb.getColor(132), Pb.getColor(130), i5, Pb.getColor(133), true, Pb.getColor(134), Pb.getColor(135));
                }
                iVar.setBounds(i + 1, i2 + 1, (i + i3) - 1, (i2 + i4) - 1);
                iVar.draw(canvas);
                return true;
            }

            public boolean a(int i, int i2, int i3, int i4, Canvas canvas) {
                Drawable km = com.uc.h.e.Pb().km(R.drawable.f635separator);
                km.setBounds(i, i2, i + i3, i2 + i4);
                km.draw(canvas);
                return true;
            }

            public boolean a(int i, int i2, int i3, int i4, boolean z, Canvas canvas) {
                com.uc.h.e Pb = com.uc.h.e.Pb();
                com.uc.e.b.h hVar = z ? new com.uc.e.b.h(Pb.kq(R.dimen.f535webwidget_bold_stroke), Pb.getColor(114), Pb.getColor(127), Pb.getColor(128), Pb.getColor(126), 1.0f, Pb.kq(R.dimen.f538webwidget_button_corner)) : new com.uc.e.b.b(Pb.kq(R.dimen.f536webwidget_thin_stroke), Pb.getColor(115), Pb.getColor(UCR.Color.bTm), Pb.getColor(UCR.Color.bTn), Pb.getColor(123), 1.0f, Pb.kq(R.dimen.f538webwidget_button_corner), Pb.kq(R.dimen.f540webwidget_frame_shadow_bold), 536870912);
                hVar.setBounds(i + 1, i2 + 1, (i + i3) - 1, (i2 + i4) - 1);
                hVar.draw(canvas);
                return true;
            }

            public boolean a(int i, int i2, boolean z, boolean z2, Canvas canvas) {
                com.uc.h.e Pb = com.uc.h.e.Pb();
                com.uc.e.b.h hVar = z2 ? new com.uc.e.b.h(Pb.kq(R.dimen.f535webwidget_bold_stroke), Pb.getColor(114), Pb.getColor(116), Pb.getColor(118), Pb.kq(R.dimen.f539webwidget_frame_shadow_large), Pb.kq(R.dimen.f537webwidget_frame_corner)) : new com.uc.e.b.h(Pb.kq(R.dimen.f536webwidget_thin_stroke), Pb.getColor(115), Pb.getColor(117), Pb.getColor(119), Pb.kq(R.dimen.f539webwidget_frame_shadow_large), Pb.kq(R.dimen.f537webwidget_frame_corner));
                int kp = Pb.kp(R.dimen.f542webwidget_checkbox_size);
                hVar.setBounds(i + 1, i2 + 1, (i + kp) - 1, (i2 + kp) - 1);
                hVar.draw(canvas);
                if (z) {
                    com.uc.e.b.a aVar = new com.uc.e.b.a(Pb.getColor(142));
                    aVar.setBounds(i + 1, i2 + 1, (i + kp) - 1, (kp + i2) - 1);
                    aVar.draw(canvas);
                }
                return true;
            }

            public boolean a(int[] iArr, boolean z, Canvas canvas) {
                int length = iArr.length / 4;
                Rect[] rectArr = new Rect[length];
                for (int i = 0; i < length; i++) {
                    rectArr[i] = new Rect(iArr[i * 4] - 1, iArr[(i * 4) + 1] - 1, iArr[(i * 4) + 2] + 1, iArr[(i * 4) + 3] + 1);
                }
                com.uc.h.e Pb = com.uc.h.e.Pb();
                new d(rectArr, Pb.kp(R.dimen.f535webwidget_bold_stroke), Pb.kp(R.dimen.f537webwidget_frame_corner), Pb.getColor(114), z ? 0 : Pb.getColor(122)).draw(canvas);
                return true;
            }

            public void aD() {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(ModelBrowser.zY);
                }
            }

            public void aT() {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(89);
                }
            }

            public InputStream ag(String str) {
                if (WindowUCWeb.this.bg == null || WindowUCWeb.this.bi != WindowUCWeb.this.bg || ModelBrowser.gD() == null) {
                    return null;
                }
                return ModelBrowser.gD().ag(str);
            }

            public void aw(byte[] bArr) {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(116, bArr);
                    WindowUCWeb.this.bn();
                }
            }

            public b.a.a.a.f ax() {
                try {
                    return b.a.a.a.f.a(WindowUCWeb.this.bk.getResources(), R.drawable.f650webview_safe_popup);
                } catch (Exception e) {
                    return null;
                }
            }

            public void b(Object[] objArr, boolean z) {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a((int) ModelBrowser.zX, z ? 1 : 0, objArr);
                }
            }

            public boolean b(int i, int i2, int i3, int i4, int i5, boolean z, Canvas canvas) {
                i iVar;
                com.uc.h.e Pb = com.uc.h.e.Pb();
                if (z) {
                    iVar = new i(Pb.kq(R.dimen.f535webwidget_bold_stroke), Pb.getColor(114), Pb.getColor(116), Pb.getColor(116), Pb.getColor(118), Pb.kq(R.dimen.f540webwidget_frame_shadow_bold), Pb.kq(R.dimen.f538webwidget_button_corner), Pb.getColor(137), Pb.getColor(138), Pb.getColor(136), i5, Pb.getColor(139));
                } else {
                    iVar = new i(Pb.kq(R.dimen.f536webwidget_thin_stroke), Pb.getColor(115), Pb.getColor(117), Pb.getColor(117), Pb.getColor(119), Pb.kq(R.dimen.f540webwidget_frame_shadow_bold), Pb.kq(R.dimen.f538webwidget_button_corner), Pb.getColor(131), Pb.getColor(132), Pb.getColor(130), i5, Pb.getColor(133));
                }
                iVar.setBounds(i + 1, i2 + 1, (i + i3) - 1, (i2 + i4) - 1);
                iVar.draw(canvas);
                return true;
            }

            public boolean b(int i, int i2, boolean z, boolean z2, Canvas canvas) {
                com.uc.h.e Pb = com.uc.h.e.Pb();
                int kp = Pb.kp(R.dimen.f542webwidget_checkbox_size);
                com.uc.e.b.h hVar = z2 ? new com.uc.e.b.h(Pb.kq(R.dimen.f535webwidget_bold_stroke), Pb.getColor(114), Pb.getColor(116), Pb.getColor(118), Pb.kq(R.dimen.f539webwidget_frame_shadow_large), (float) (kp / 2)) : new com.uc.e.b.h(Pb.kq(R.dimen.f536webwidget_thin_stroke), Pb.getColor(115), Pb.getColor(117), Pb.getColor(119), Pb.kq(R.dimen.f539webwidget_frame_shadow_large), (float) (kp / 2));
                hVar.setBounds(i + 1, i2 + 1, (i + kp) - 1, (i2 + kp) - 1);
                hVar.draw(canvas);
                if (z) {
                    com.uc.e.b.c cVar = new com.uc.e.b.c(Pb.getColor(143), Pb.getColor(UCR.Color.bTG));
                    cVar.setBounds(i + 1, i2 + 1, (i + kp) - 1, (i2 + kp) - 1);
                    cVar.draw(canvas);
                }
                return true;
            }

            public boolean bG() {
                return WindowUCWeb.this.bG();
            }

            public void bK(String str) {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(62, str);
                }
            }

            public boolean bL(String str) {
                if (ModelBrowser.gD() != null && true == WindowUCWeb.this.bn) {
                    ModelBrowser.gD().a(21, str);
                }
                if (WindowUCWeb.this.bg == null) {
                    return false;
                }
                com.uc.a.e.nR().ob().r(str, WindowUCWeb.this.bg.getUrl());
                return false;
            }

            public boolean bM(String str) {
                ModelBrowser gD = ModelBrowser.gD();
                if (gD == null) {
                    return true;
                }
                gD.a(39, 2, str);
                return true;
            }

            public void bN(String str) {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(65, str);
                }
            }

            public byte[] bP(String str) {
                try {
                    InputStream open = WindowUCWeb.this.bk.getAssets().open(str);
                    if (WindowUCWeb.this.bg != null && WindowUCWeb.this.bi == WindowUCWeb.this.bg) {
                        return WindowUCWeb.this.bg.a(open);
                    }
                } catch (IOException e) {
                }
                return null;
            }

            public void by(boolean z) {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(108, Boolean.valueOf(z));
                }
            }

            public void c(Object[] objArr, boolean z) {
                if (!z) {
                    f.l(1, f.atM);
                }
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().iL();
                    WindowUCWeb.this.bg.ag.an();
                    WindowUCWeb.this.bg.getHeight();
                    WindowUCWeb.this.bg.ag.am();
                }
            }

            public boolean d(Vector vector, boolean z) {
                if (WindowUCWeb.this.bg == null || WindowUCWeb.this.bi != WindowUCWeb.this.bg) {
                    return false;
                }
                if (z) {
                    WindowUCWeb.this.bg.D(vector);
                    return false;
                }
                WindowUCWeb.this.bg.B(vector);
                return false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.uc.browser.WindowUCWeb.b(com.uc.browser.WindowUCWeb, boolean):boolean
             arg types: [com.uc.browser.WindowUCWeb, int]
             candidates:
              com.uc.browser.WindowUCWeb.b(com.uc.browser.WindowUCWeb, int):void
              com.uc.browser.WindowUCWeb.b(android.content.Context, java.lang.String):void
              com.uc.browser.WindowUCWeb.b(com.uc.browser.WindowUCWeb, boolean):boolean */
            public boolean dO(int i) {
                int progress = WindowUCWeb.this.bg.getProgress();
                if ((i > 0 && i != progress && (i > progress + 1 || i < progress)) || 99 <= i) {
                    if (WindowUCWeb.this.bg != null) {
                        WindowUCWeb.this.bg.setProgress(i);
                    }
                    if (true == WindowUCWeb.this.bn && true == WindowUCWeb.this.bm && ModelBrowser.gD() != null) {
                        if (com.uc.a.e.nR() == null) {
                            ModelBrowser.gD().a(22, i, (Object) null);
                        } else if (!com.uc.a.e.nR().F(WindowUCWeb.this.bg.getUrl(), WindowUCWeb.this.bg.getTitle())) {
                            ModelBrowser.gD().a(22, i, (Object) null);
                        }
                    }
                    if (i >= 99) {
                        if (WindowUCWeb.this.bg != null) {
                            WindowUCWeb.this.bg.postInvalidate();
                        }
                        if (true == WindowUCWeb.this.bm) {
                            boolean unused = WindowUCWeb.this.bl = false;
                            boolean unused2 = WindowUCWeb.this.bm = false;
                            if (true == WindowUCWeb.this.bn && ModelBrowser.gD() != null) {
                                ModelBrowser.gD().aS(23);
                                if (true == ModelBrowser.gD().wK) {
                                    WindowUCWeb.this.bf();
                                }
                            }
                        }
                        if (!WindowUCWeb.this.bg.zF()) {
                            if (WindowUCWeb.this.bn) {
                                ModelBrowser.gD().a(21, "UC浏览器");
                            }
                            if (true == WindowUCWeb.this.bg.zH() && WindowUCWeb.this.bg.U()) {
                                WindowUCWeb.this.bg.zG();
                            } else if (!WindowUCWeb.this.bg.zH()) {
                                boolean unused3 = WindowUCWeb.this.bq = false;
                                WindowUCWeb.this.bg.bh(!WindowUCWeb.this.bq);
                            }
                        }
                    }
                }
                return false;
            }

            public boolean dY(int i) {
                if (WindowUCWeb.this.bg == null || WindowUCWeb.this.bi != WindowUCWeb.this.bg || ModelBrowser.gD() == null || bG()) {
                    return false;
                }
                ModelBrowser.gD().a(13, Integer.valueOf(i));
                return false;
            }

            public void dZ(int i) {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(85, Integer.valueOf(i));
                }
            }

            public void eA(String str) {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(94, new String[]{str, str, "http://wap.uc.cn"});
                }
            }

            public void ew(String str) {
                if (ModelBrowser.gD() != null && true == WindowUCWeb.this.bn) {
                    ModelBrowser.gD().a(21, str);
                }
            }

            public void ex(String str) {
                if (WindowUCWeb.this.bn) {
                    ModelBrowser.gD().a(127, str);
                }
            }

            public void ey(String str) {
                String str2;
                String substring;
                int indexOf;
                if (str != null) {
                    try {
                        int indexOf2 = str.indexOf(b.b.add);
                        if (-1 == indexOf2 || -1 == (indexOf = (substring = str.substring(indexOf2 + b.b.add.length())).indexOf(34))) {
                            str2 = null;
                        } else {
                            String substring2 = substring.substring(indexOf + 1);
                            int indexOf3 = substring2.indexOf(34);
                            str2 = indexOf3 >= 1 ? substring2.substring(0, indexOf3) : null;
                            if (WindowUCWeb.this.bg != null) {
                                ModelBrowser.gD().a(22, 0, (Object) null);
                            }
                            ModelBrowser.gD().a(22, 10, (Object) null);
                            WindowUCWeb.this.bg.aw();
                            WindowUCWeb.this.bg.d(b.b.adi + str2, WindowUCWeb.this.p);
                        }
                        String c = WindowUCWeb.this.c(str, "resultStatus");
                        String c2 = WindowUCWeb.this.c(str, "memo");
                        String c3 = -1 != str.indexOf(new StringBuilder().append("result").append(" ").toString()) ? WindowUCWeb.this.c(str, "result" + " ") : WindowUCWeb.this.c(str, "result" + "=");
                        if (WindowUCWeb.this.bg != null) {
                            WindowUCWeb.this.bg.a(c, c2, c3, str2);
                        }
                    } catch (Exception e) {
                    }
                }
            }

            public void ez(String str) {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a((int) ModelBrowser.zV, new Object[]{str, WindowUCWeb.this.p});
                }
            }

            public void g(boolean z) {
                boolean unused = WindowUCWeb.this.bm = z;
                if (true == WindowUCWeb.this.bn && ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(23);
                }
            }

            public void gB() {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(86);
                }
            }

            public void gC() {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(48);
                }
            }

            public int getOrientation() {
                try {
                    if (WindowUCWeb.this.bg != null) {
                        return WindowUCWeb.this.bg.getOrientation();
                    }
                } catch (Exception e) {
                }
                return b.b.ORIENTATION_PORTRAIT;
            }

            public void gp() {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(ModelBrowser.Ae);
                }
            }

            public void hn() {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(23);
                }
            }

            public void hn(int i) {
                ModelBrowser.gD().a(106, i, WindowUCWeb.this.bg);
            }

            public void ho(int i) {
                Vector f = WindowUCWeb.this.bG;
                Iterator it = f.iterator();
                int i2 = i;
                while (it.hasNext() && i2 > 0) {
                    BackForwardItem backForwardItem = (BackForwardItem) it.next();
                    if (backForwardItem.abU == 1) {
                        f.removeElement(backForwardItem);
                        WindowUCWeb.g(WindowUCWeb.this);
                        i2--;
                    }
                }
            }

            public void hp(int i) {
                if (WindowUCWeb.this.bg != null) {
                }
            }

            public void hq(int i) {
            }

            public b.a.a.a.f is() {
                if (WindowUCWeb.this.bg == null || WindowUCWeb.this.bi != WindowUCWeb.this.bg || ModelBrowser.gD() == null) {
                    return null;
                }
                return ModelBrowser.gD().is();
            }

            public boolean j(String[] strArr) {
                if (strArr.length > 4 && ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(26, strArr);
                    g(false);
                }
                return false;
            }

            public void q(int i, String str) {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().d(i, str);
                }
            }

            public boolean qV() {
                if (ModelBrowser.gD() == null || true != ModelBrowser.gD().hj().wg()) {
                    return false;
                }
                ModelBrowser.gD().aS(52);
                return true;
            }

            public boolean qW() {
                if (WindowUCWeb.this.bg == null || WindowUCWeb.this.bi != WindowUCWeb.this.bg) {
                    return false;
                }
                WindowUCWeb.this.bg.postInvalidate();
                return false;
            }

            public int qX() {
                if (WindowUCWeb.this.bg != null) {
                    return WindowUCWeb.this.bg.Qk();
                }
                return 0;
            }

            public int qY() {
                if (WindowUCWeb.this.bg != null) {
                    return WindowUCWeb.this.bg.Ql();
                }
                return 0;
            }

            public void re() {
                ModelBrowser.gD().aS(131);
            }

            public boolean s(Object[] objArr) {
                if (!(WindowUCWeb.this.bg == null || WindowUCWeb.this.bi != WindowUCWeb.this.bg || objArr == null || objArr.length < 11 || ModelBrowser.gD() == null)) {
                    int parseInt = Integer.parseInt(objArr[1].toString());
                    Bundle bundle = new Bundle();
                    bundle.putString("x", objArr[0].toString());
                    bundle.putString("y", String.valueOf(parseInt));
                    bundle.putString("width", objArr[2].toString());
                    bundle.putString("height", objArr[3].toString());
                    bundle.putString("single", objArr[4].toString());
                    bundle.putString("text", objArr[5].toString());
                    bundle.putString("textsize", objArr[6].toString());
                    bundle.putBoolean("password", ((Boolean) objArr[7]).booleanValue());
                    bundle.putInt("maxlength", ((Integer) objArr[8]).intValue());
                    bundle.putString("backgroundColor", objArr[9].toString());
                    bundle.putString("textColor", objArr[10].toString());
                    ModelBrowser.gD().a(36, bundle);
                    boolean unused = WindowUCWeb.this.bR = true;
                }
                return false;
            }

            public boolean se() {
                if (ModelBrowser.gD() == null) {
                    return false;
                }
                ModelBrowser.gD().aS(49);
                return false;
            }

            public boolean sf() {
                if (ModelBrowser.gD() == null) {
                    return false;
                }
                ModelBrowser.gD().aS(54);
                return false;
            }

            public b.a.a.a.f sg() {
                try {
                    return b.a.a.a.f.a(WindowUCWeb.this.bk.getResources(), R.drawable.f546camera);
                } catch (Exception e) {
                    return null;
                }
            }

            public void sh() {
                if (true == WindowUCWeb.this.bq && WindowUCWeb.this.bi == WindowUCWeb.this.bg && (ModelBrowser.gD() == null || true != ModelBrowser.gD().wK)) {
                    boolean unused = WindowUCWeb.this.bq = false;
                    WindowUCWeb.this.bg.bh(!WindowUCWeb.this.bq);
                    WindowUCWeb.this.bg.postInvalidate();
                    ex(WindowUCWeb.this.bg.getUrl());
                }
                com.uc.a.e.nR().c((b.a.a.a.f) null);
            }

            public boolean shouldOverrideUrlLoading(String str) {
                if (ModelBrowser.gD() == null) {
                    return false;
                }
                ModelBrowser.gD().a(11, b.b.acH + str);
                return false;
            }

            public void si() {
                try {
                    if (WindowUCWeb.this.bg != null) {
                        WindowUCWeb.this.bg.si();
                    }
                } catch (Exception e) {
                }
            }

            public void sk() {
                if (WindowUCWeb.this.bg != null) {
                    WindowUCWeb.this.bg.Qq();
                }
            }

            public void t(Object[] objArr) {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a((int) ModelBrowser.zX, 0, objArr);
                }
            }

            public void u(String str, int i) {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a((int) ModelBrowser.Ad, i, str);
                }
            }

            public boolean w(Object obj) {
                if (ModelBrowser.gD() == null) {
                    return false;
                }
                ModelBrowser.gD().a(72, obj);
                return false;
            }

            public void y(int i, int i2) {
                ModelBrowser.gD().a((int) ModelBrowser.zU, new int[]{i, i2});
            }
        };
        this.ch = -1;
        this.cj = -1;
        this.ck = null;
        this.bL = -1;
    }

    private View a(String str, boolean z) {
        boolean z2;
        String n = n(str);
        if (this.bC) {
            this.bC = false;
            z2 = false;
        } else {
            z2 = z;
        }
        com.uc.a.e.bi(j(n)[0]);
        String bE2 = com.uc.a.e.nR().bE(n);
        if (this.bh != null) {
            this.bh.bh(!this.bq);
            this.bh.zI();
        }
        if (this.bn) {
            this.bk.t(this.bk.getString(R.string.f1305page_loading));
        }
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().Z(bE2);
        }
        if (this.bh == null || !bE2.equals(this.bB) || !bE2.equals(this.bh.getUrl()) || ((bE2.equals(this.bB) || bE2.equals(this.bh.getUrl())) && true == n(this.bL))) {
            m(2);
            if (this.bh != this.bi) {
                this.bL -= bm();
            }
            if (this.bh != null) {
                this.bG.add(new BackForwardItem((byte) 2, this.bM));
                this.bL++;
                this.bB = bE2;
            }
        }
        if (this.bh == null) {
            this.bh = new WebViewZoom(this.bk);
            this.bh.setWebViewClient(this.bZ);
            this.bh.setWebChromeClient(this.cb);
            this.bh.setDownloadListener(this.bk);
            this.bh.setPictureListener(new WebView.PictureListener() {
                boolean bxc = true;

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.uc.browser.WindowUCWeb.d(com.uc.browser.WindowUCWeb, boolean):boolean
                 arg types: [com.uc.browser.WindowUCWeb, int]
                 candidates:
                  com.uc.browser.WindowUCWeb.d(android.content.Context, java.lang.String):void
                  com.uc.browser.WindowUCWeb.d(com.uc.browser.WindowUCWeb, boolean):boolean */
                public void onNewPicture(WebView webView, Picture picture) {
                    if (true == WindowUCWeb.this.bq && WindowUCWeb.this.bi == WindowUCWeb.this.bh) {
                        boolean unused = WindowUCWeb.this.bq = false;
                        WindowUCWeb.this.bh.bh(!WindowUCWeb.this.bq);
                        WindowUCWeb.this.bh.invalidate();
                    }
                    if (!WindowUCWeb.this.bw) {
                        boolean unused2 = WindowUCWeb.this.bw = true;
                    }
                    if (true == this.bxc) {
                        this.bxc = false;
                        if (WindowUCWeb.this.bn) {
                            ModelBrowser.gD().a(127, WindowUCWeb.this.bh.getUrl());
                        }
                    }
                }
            });
            this.bh.bh(!this.bq);
            this.bh.zI();
            if (true == this.bp) {
                this.bh.getSettings().setBuiltInZoomControls(false);
            }
            this.bk.registerForContextMenu(this.bh);
            this.bG.add(new BackForwardItem((byte) 2, this.bM));
            this.bL++;
            this.bB = bE2;
        }
        if (this.bz && this.bh.zJ() == null) {
            this.bh.getSettings().setJavaScriptEnabled(true);
            this.bh.a(new JavaScriptAndroidBridge(this.bk, this.bh));
        }
        if (this.bn && ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(127, bE2);
        }
        this.bh.i(bE2, z2);
        if (this.bh != this.bi) {
            this.bi = this.bh;
            this.bj = this.bh;
            if (true == this.bp && ModelBrowser.gD() != null) {
                ModelBrowser gD = ModelBrowser.gD();
                ModelBrowser.gD();
                gD.a(34, 1, (Object) null);
            }
        }
        return this.bi;
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x007a A[SYNTHETIC, Splitter:B:38:0x007a] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x007f A[SYNTHETIC, Splitter:B:41:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0096 A[SYNTHETIC, Splitter:B:54:0x0096] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x009b A[SYNTHETIC, Splitter:B:57:0x009b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r9, java.lang.String r10, java.lang.String r11) {
        /*
            r7 = 0
            r6 = 1
            r5 = 0
            if (r9 == 0) goto L_0x0009
            if (r10 == 0) goto L_0x0009
            if (r11 != 0) goto L_0x000b
        L_0x0009:
            r0 = r5
        L_0x000a:
            return r0
        L_0x000b:
            java.lang.String r0 = b.b.ade
            r1 = 0
            r2 = 0
            java.lang.String r3 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x00c0, all -> 0x0091 }
            java.lang.String r4 = "mounted"
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x00c0, all -> 0x0091 }
            if (r3 != 0) goto L_0x0032
            r0 = 2131296838(0x7f090246, float:1.8211604E38)
            r3 = 1
            android.widget.Toast r0 = android.widget.Toast.makeText(r9, r0, r3)     // Catch:{ Exception -> 0x00c0, all -> 0x0091 }
            r0.show()     // Catch:{ Exception -> 0x00c0, all -> 0x0091 }
            if (r7 == 0) goto L_0x002b
            r2.close()     // Catch:{ Exception -> 0x009f }
        L_0x002b:
            if (r7 == 0) goto L_0x0030
            r1.close()     // Catch:{ Exception -> 0x00a1 }
        L_0x0030:
            r0 = r5
            goto L_0x000a
        L_0x0032:
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x00c0, all -> 0x0091 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x00c0, all -> 0x0091 }
            boolean r0 = r1.exists()     // Catch:{ Exception -> 0x00c0, all -> 0x0091 }
            if (r0 == 0) goto L_0x0043
            boolean r0 = r1.isDirectory()     // Catch:{ Exception -> 0x00c0, all -> 0x0091 }
            if (r0 != 0) goto L_0x0046
        L_0x0043:
            r1.mkdirs()     // Catch:{ Exception -> 0x00c0, all -> 0x0091 }
        L_0x0046:
            android.content.res.AssetManager r0 = r9.getAssets()     // Catch:{ Exception -> 0x00c0, all -> 0x0091 }
            java.io.InputStream r0 = r0.open(r10)     // Catch:{ Exception -> 0x00c0, all -> 0x0091 }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x00c4, all -> 0x00af }
            r1.<init>(r11)     // Catch:{ Exception -> 0x00c4, all -> 0x00af }
            r1.createNewFile()     // Catch:{ Exception -> 0x00c4, all -> 0x00af }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00c4, all -> 0x00af }
            r2.<init>(r1)     // Catch:{ Exception -> 0x00c4, all -> 0x00af }
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x006a, all -> 0x00b4 }
        L_0x005f:
            int r3 = r0.read(r1)     // Catch:{ Exception -> 0x006a, all -> 0x00b4 }
            if (r3 <= 0) goto L_0x0084
            r4 = 0
            r2.write(r1, r4, r3)     // Catch:{ Exception -> 0x006a, all -> 0x00b4 }
            goto L_0x005f
        L_0x006a:
            r1 = move-exception
            r1 = r0
            r0 = r2
        L_0x006d:
            r2 = 2131296837(0x7f090245, float:1.8211602E38)
            r3 = 1
            android.widget.Toast r2 = android.widget.Toast.makeText(r9, r2, r3)     // Catch:{ all -> 0x00ba }
            r2.show()     // Catch:{ all -> 0x00ba }
            if (r0 == 0) goto L_0x007d
            r0.close()     // Catch:{ Exception -> 0x00a7 }
        L_0x007d:
            if (r1 == 0) goto L_0x0082
            r1.close()     // Catch:{ Exception -> 0x00a9 }
        L_0x0082:
            r0 = r5
            goto L_0x000a
        L_0x0084:
            if (r2 == 0) goto L_0x0089
            r2.close()     // Catch:{ Exception -> 0x00a3 }
        L_0x0089:
            if (r0 == 0) goto L_0x008e
            r0.close()     // Catch:{ Exception -> 0x00a5 }
        L_0x008e:
            r0 = r6
            goto L_0x000a
        L_0x0091:
            r0 = move-exception
            r1 = r7
            r2 = r7
        L_0x0094:
            if (r1 == 0) goto L_0x0099
            r1.close()     // Catch:{ Exception -> 0x00ab }
        L_0x0099:
            if (r2 == 0) goto L_0x009e
            r2.close()     // Catch:{ Exception -> 0x00ad }
        L_0x009e:
            throw r0
        L_0x009f:
            r0 = move-exception
            goto L_0x002b
        L_0x00a1:
            r0 = move-exception
            goto L_0x0030
        L_0x00a3:
            r1 = move-exception
            goto L_0x0089
        L_0x00a5:
            r0 = move-exception
            goto L_0x008e
        L_0x00a7:
            r0 = move-exception
            goto L_0x007d
        L_0x00a9:
            r0 = move-exception
            goto L_0x0082
        L_0x00ab:
            r1 = move-exception
            goto L_0x0099
        L_0x00ad:
            r1 = move-exception
            goto L_0x009e
        L_0x00af:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r7
            goto L_0x0094
        L_0x00b4:
            r1 = move-exception
            r8 = r1
            r1 = r2
            r2 = r0
            r0 = r8
            goto L_0x0094
        L_0x00ba:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r0
            r0 = r8
            goto L_0x0094
        L_0x00c0:
            r0 = move-exception
            r0 = r7
            r1 = r7
            goto L_0x006d
        L_0x00c4:
            r1 = move-exception
            r1 = r0
            r0 = r7
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.browser.WindowUCWeb.a(android.content.Context, java.lang.String, java.lang.String):boolean");
    }

    private void b(byte b2) {
        switch (b2) {
            case 0:
                if (this.be == null) {
                    this.be = ModelBrowser.gD().gn();
                }
                if (this.be != this.bi) {
                    this.bi = this.be;
                    if (true == this.bp && ModelBrowser.gD() != null) {
                        ModelBrowser gD = ModelBrowser.gD();
                        ModelBrowser.gD();
                        gD.a(34, 0, (Object) null);
                    }
                }
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().hf();
                    return;
                }
                return;
            case 1:
                if (!(this.bg == null || this.bg == this.bi)) {
                    this.bi = this.bg;
                    this.bj = this.bg;
                    if (true == this.bp && ModelBrowser.gD() != null) {
                        ModelBrowser gD2 = ModelBrowser.gD();
                        ModelBrowser.gD();
                        gD2.a(34, 0, (Object) null);
                    }
                }
                this.bk.t(this.bg.getTitle());
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().hf();
                    return;
                }
                return;
            case 2:
                if (!(this.bh == null || this.bh == this.bi)) {
                    this.bi = this.bh;
                    this.bj = this.bh;
                    if (true == this.bp && ModelBrowser.gD() != null) {
                        ModelBrowser gD3 = ModelBrowser.gD();
                        ModelBrowser.gD();
                        gD3.a(34, 0, (Object) null);
                    }
                }
                this.bk.t(this.bh.getTitle());
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().hf();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void bl() {
        boolean z;
        boolean z2 = false;
        if (this.bG != null && this.bG.size() > 0) {
            boolean z3 = false;
            int i = 0;
            while (true) {
                if (i > this.bL) {
                    z = z2;
                    z2 = z3;
                    break;
                }
                if (1 == ((BackForwardItem) this.bG.get(i)).abU) {
                    z = z2;
                    z2 = true;
                } else {
                    z = true;
                    z2 = z3;
                }
                if (true == z2 && true == z) {
                    break;
                }
                i++;
                z3 = z2;
                z2 = z;
            }
        } else {
            z = false;
        }
        if (!z2 && this.bg != null) {
            this.bg.Qv();
            this.bg = null;
        }
        if (!z && this.bh != null && !this.bo) {
            g.Jn().b(this.bh.getSettings());
            this.bh.clearHistory();
            this.bh.destroy();
            this.bh = null;
        }
    }

    private int bm() {
        int i;
        int i2;
        int i3;
        int i4;
        if (this.bh == null || p(this.bL) - this.bh.copyBackForwardList().getCurrentIndex() <= 0) {
            return 0;
        }
        int currentIndex = this.bh.copyBackForwardList().getCurrentIndex();
        int i5 = -1;
        int i6 = 0;
        while (true) {
            if (i6 >= this.bL) {
                i2 = i6;
                i = 0;
                break;
            }
            if (2 == ((BackForwardItem) this.bG.get(i6)).abU || -1 == ((BackForwardItem) this.bG.get(i6)).abU) {
                i4 = i5 + 1;
                if (i4 == currentIndex) {
                    i2 = i6;
                    i = 0;
                    break;
                }
            } else {
                i4 = i5;
            }
            i6++;
            i5 = i4;
        }
        while (true) {
            int i7 = i2 + 1;
            if (i7 >= this.bL) {
                return i;
            }
            if (-1 == ((BackForwardItem) this.bG.get(i7)).abU) {
                this.bG.remove(i7);
                i3 = i + 1;
            } else {
                i3 = i;
            }
            i = i3;
            i2 = i7 + 1;
        }
    }

    /* access modifiers changed from: private */
    public void bn() {
        if (this.bG != null) {
            if (this.bh != null) {
                if (this.bh.copyBackForwardList().getSize() <= p(this.bG.size())) {
                    if (this.bh.copyBackForwardList().getSize() < p(this.bG.size())) {
                        this.bG.remove(this.bG.size() - 1);
                        this.bL--;
                        if (-1 != this.bL) {
                            if (this.bL >= 0 && this.bL <= this.bG.size() - 1) {
                                switch (((BackForwardItem) this.bG.get(this.bL)).abU) {
                                    case 1:
                                        b((byte) 1);
                                        break;
                                }
                            } else {
                                return;
                            }
                        } else {
                            b((byte) 0);
                            return;
                        }
                    }
                } else {
                    this.bG.add(new BackForwardItem((byte) 2, this.bM));
                    this.bL++;
                }
            }
            if (this.bg == null) {
                return;
            }
            if (this.bg.E() > o(this.bG.size())) {
                this.bG.add(new BackForwardItem((byte) 1));
                this.bL++;
            } else if (this.bg.E() < o(this.bG.size())) {
                this.bG.remove(this.bG.size() - 1);
                this.bL--;
                if (-1 == this.bL) {
                    b((byte) 0);
                } else if (this.bL >= 0 && this.bL <= this.bG.size() - 1) {
                    switch (((BackForwardItem) this.bG.get(this.bL)).abU) {
                        case 1:
                        default:
                            return;
                        case 2:
                            b((byte) 2);
                            return;
                    }
                }
            }
        }
    }

    private void c(Context context) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.L(context.getResources().getString(R.string.f1523fresh_us_data_dlg_title));
        builder.M(context.getResources().getString(R.string.f1524fresh_us_data_dlg_msg));
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Bundle bundle = new Bundle();
                bundle.putString(b.b.acW, "freshUsData");
                Intent intent = new Intent(WindowUCWeb.this.bk, ActivityInitial.class);
                intent.putExtras(bundle);
                WindowUCWeb.this.bk.startActivityForResult(intent, 8);
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        UCAlertDialog fh = builder.fh();
        if (fh != null) {
            fh.show();
        }
    }

    public static void e(String str) {
        int bD2 = com.uc.a.e.nR().bD(str);
        ModelBrowser gD = ModelBrowser.gD();
        if (gD != null) {
            if (1 <= bD2 && bD2 <= 3) {
                gD.a(114, 1, com.uc.a.e.nR().dt(bD2));
            } else if (gD.gY()) {
                gD.a(114, 0, (Object) null);
            }
            gD.i((byte) ModelBrowser.ak(str));
        }
    }

    private boolean f(String str) {
        if (str == null || str.length() == 0) {
            return false;
        }
        if (str.contains(b.b.acI)) {
            return true;
        }
        if (str.startsWith(b.b.acH) || true == this.bo) {
            if (this.bi == null || this.bL < 0) {
                if (1 == this.bM) {
                    return true;
                }
            } else if (this.bi instanceof WebViewZoom) {
                BackForwardItem backForwardItem = (BackForwardItem) this.bG.get(this.bL);
                if (backForwardItem.ceq == 1) {
                    return true;
                }
                if (backForwardItem.abU != 2 && true == this.bo && 1 == this.bM) {
                    return true;
                }
            }
        }
        return false;
    }

    static /* synthetic */ int g(WindowUCWeb windowUCWeb) {
        int i = windowUCWeb.bL;
        windowUCWeb.bL = i - 1;
        return i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.WindowUCWeb.a(java.lang.String, boolean):android.view.View
     arg types: [java.lang.String, int]
     candidates:
      com.uc.browser.WindowUCWeb.a(com.uc.browser.WindowUCWeb, int):int
      com.uc.browser.WindowUCWeb.a(com.uc.browser.WindowUCWeb, b.a.a.e):b.a.a.e
      com.uc.browser.WindowUCWeb.a(com.uc.browser.WindowUCWeb, com.uc.browser.WindowUCWeb$StreamingMediaThread):com.uc.browser.WindowUCWeb$StreamingMediaThread
      com.uc.browser.WindowUCWeb.a(com.uc.browser.WindowUCWeb, boolean):boolean
      com.uc.browser.WindowUCWeb.a(java.lang.String, android.content.Context):android.view.View
      com.uc.browser.WindowUCWeb.a(android.content.Context, int):com.uc.browser.ViewMainpage
      com.uc.browser.WindowUCWeb.a(android.content.Context, java.lang.String):void
      com.uc.browser.WindowUCWeb.a(java.lang.String, com.uc.a.n):void
      com.uc.browser.WindowUCWeb.a(java.lang.String, boolean):android.view.View */
    private View g(String str) {
        String str2;
        ModelBrowser gD = ModelBrowser.gD();
        if (gD == null || !gD.gK() || !a.OU() || str.contains(com.uc.a.e.nR().nZ().oN()) || str.contains("ext:null") || str.contains(b.b.acU)) {
            if (this.bg != null) {
                this.bg.bh(!this.bq);
                this.bg.zI();
            }
            if (this.bn) {
                this.bk.t(this.bk.getString(R.string.f1305page_loading));
            }
            if (this.bO != null && true == this.bO.getBoolean(bV)) {
                this.bO.putBoolean(bV, false);
                if (!this.bO.getBoolean(bW) && !this.bO.getBoolean(bU)) {
                    this.bO.clear();
                    this.bO = null;
                }
            }
            m(1);
            if (str.equals("ext:download:ser:")) {
                str2 = "ext:download";
            } else {
                if (!str.contains("command=download_by_ucweb") && !str.contains(com.uc.a.e.qk()) && !str.contains(w.Pe) && !str.contains(w.Ph)) {
                    this.bG.add(new BackForwardItem((byte) 1));
                    this.bL++;
                }
                str2 = str;
            }
            if (this.bg == null) {
                this.bg = new WebViewJUC(this.bk);
                r(0);
                this.bg.a(this.p);
                this.bk.registerForContextMenu(this.bg);
                this.bg.setOnLongClickListener(this.bk);
                this.bg.bh(!this.bq);
                this.bg.zI();
            }
            if (this.bg != this.bi) {
                this.bi = this.bg;
                this.bj = this.bg;
                if (true == this.bp && ModelBrowser.gD() != null) {
                    ModelBrowser gD2 = ModelBrowser.gD();
                    ModelBrowser.gD();
                    gD2.a(34, 0, (Object) null);
                }
            }
            if (this.bC) {
                str2 = str2 + b.b.acu;
                this.bC = false;
            }
            this.bg.d(str2, this.p);
            this.bx = System.currentTimeMillis();
            return this.bi;
        }
        String h = h(str);
        gD.y(false);
        return a(h, false);
    }

    /* access modifiers changed from: private */
    public void g(boolean z) {
        this.bm = z;
        if (true == this.bn && ModelBrowser.gD() != null) {
            ModelBrowser.gD().aS(23);
        }
    }

    private String h(String str) {
        int indexOf = str.toLowerCase().indexOf("http");
        if (indexOf == 0) {
            return str;
        }
        if (indexOf > 0) {
            int indexOf2 = str.indexOf(46);
            return indexOf < indexOf2 ? new String(str.substring(indexOf)) : str.indexOf(58) < indexOf2 ? new String(str.substring(str.lastIndexOf(58, indexOf2) + 1)) : str;
        } else if (indexOf != -1) {
            return str;
        } else {
            int indexOf3 = str.indexOf(46);
            return str.indexOf(58) < indexOf3 ? new String(str.substring(str.lastIndexOf(58, indexOf3) + 1)) : str;
        }
    }

    private String i(String str) {
        String trim = str.trim();
        if (trim.toLowerCase().startsWith(bx.bAh) || trim.toLowerCase().startsWith("https://") || trim.startsWith(w.Os) || trim.startsWith("u:") || trim.startsWith(w.Pz) || trim.startsWith("forceusejuc:") || trim.startsWith("wap:") || trim.startsWith("javascript") || trim.equals(w.Ps) || trim.startsWith("viewmaincontent:") || trim.startsWith("sms:") || trim.startsWith(b.b.acx)) {
            return trim.startsWith("HTTP") ? trim.replaceFirst("HTTP", "http") : trim;
        }
        if (!trim.startsWith("http") && !trim.startsWith("https")) {
            if (trim.startsWith("//")) {
                return "http:" + trim;
            }
            trim = "http" + trim;
        }
        return (trim.startsWith("http") || trim.startsWith("https")) ? (trim.startsWith("http:") || trim.startsWith("https:")) ? (trim.startsWith("http:/") || trim.startsWith("https:/")) ? trim.replaceFirst(au.aGF, "//") : trim.replaceFirst(cd.bVI, "://") : trim.replaceFirst("http", bx.bAh) : trim;
    }

    public static final String[] j(String str) {
        String str2;
        String str3;
        int indexOf = str.indexOf("://");
        String substring = -1 != indexOf ? str.substring(indexOf + 3) : str;
        int indexOf2 = substring.indexOf(au.aGF);
        if (indexOf2 > 0) {
            String substring2 = substring.substring(0, indexOf2);
            str3 = substring.substring(indexOf2);
            str2 = (str3 == null || str3.length() != 0) ? substring2 : substring2;
        } else {
            int indexOf3 = substring.indexOf("?");
            if (indexOf3 > 0) {
                String substring3 = substring.substring(0, indexOf3);
                str3 = substring.substring(indexOf3);
                str2 = substring3;
            } else {
                str2 = substring;
                str3 = "";
            }
        }
        String[] strArr = new String[2];
        if (str2 != null && str2.endsWith(cd.bVI)) {
            str2 = str2.substring(0, str2.length() - 1);
        }
        strArr[0] = str2;
        strArr[1] = str3;
        return strArr;
    }

    private boolean k(String str) {
        String lowerCase = str.toLowerCase();
        String[] j = j(lowerCase);
        String[] pR = com.uc.a.e.nR().pR();
        if (pR != null) {
            for (int i = 0; i < pR.length; i++) {
                if (pR[i] != null && lowerCase.contains(pR[i])) {
                    return false;
                }
            }
        }
        String[] pS = com.uc.a.e.nR().pS();
        if (pS != null) {
            for (int i2 = 0; i2 < pS.length; i2++) {
                if (lowerCase != null && pS[i2] != null && lowerCase.startsWith(pS[i2])) {
                    return true;
                }
            }
        }
        String[] pT = com.uc.a.e.nR().pT();
        if (pT != null) {
            for (int i3 = 0; i3 < pT.length; i3++) {
                if (lowerCase != null && pT[i3] != null && lowerCase.startsWith(pT[i3])) {
                    return true;
                }
            }
        }
        if (j[0].startsWith("wap.") || j[0].startsWith("m.") || j[0].startsWith("3g.") || lowerCase.startsWith(w.Os) || lowerCase.startsWith("u:") || lowerCase.startsWith(w.Pz) || lowerCase.startsWith("forceusejuc:") || lowerCase.startsWith("wap:") || lowerCase.equals(w.Ps) || j[0].startsWith("command") || lowerCase.startsWith("command")) {
            return true;
        }
        return j[1] != null && (j[1].endsWith(".wml") || j[1].endsWith(".xhtml") || ((j[1].contains(".wml") || j[1].contains(".xhtml")) && -1 != j[1].indexOf("#")));
    }

    private boolean l(String str) {
        String lowerCase = str.toLowerCase();
        String[] j = j(lowerCase);
        String lowerCase2 = lowerCase.toLowerCase();
        String[] pR = com.uc.a.e.nR().pR();
        if (pR != null) {
            for (int i = 0; i < pR.length; i++) {
                if (pR[i] != null && lowerCase2.contains(pR[i])) {
                    return true;
                }
            }
        }
        String[] pS = com.uc.a.e.nR().pS();
        if (pS != null) {
            for (int i2 = 0; i2 < pS.length; i2++) {
                if (lowerCase != null && pS[i2] != null && lowerCase.startsWith(pS[i2])) {
                    return false;
                }
            }
        }
        String[] pT = com.uc.a.e.nR().pT();
        if (pT != null) {
            for (int i3 = 0; i3 < pT.length; i3++) {
                if (lowerCase != null && pT[i3] != null && lowerCase.startsWith(pT[i3])) {
                    return false;
                }
            }
        }
        if (lowerCase.startsWith("forceusejuc:") || lowerCase.startsWith("wap:") || lowerCase.equals(w.Ps)) {
            return false;
        }
        if (j[0].startsWith("www.")) {
            return true;
        }
        return j[1].endsWith(".htm") || j[1].endsWith(".html");
    }

    /* access modifiers changed from: private */
    public void m(int i) {
        boolean z;
        boolean z2;
        if (-1 == this.bL || this.bL != this.bG.size() - 1) {
            bl();
            if (2 != i || this.bh == null) {
                z = false;
            } else {
                z = p(this.bL) <= this.bh.copyBackForwardList().getCurrentIndex();
            }
            while (this.bL < this.bG.size() - 1) {
                if (1 == ((BackForwardItem) this.bG.get(this.bL + 1)).abU) {
                    if (this.bg != null) {
                        this.bg.s();
                    }
                    this.bG.remove(this.bL + 1);
                } else if (2 != i) {
                    if (this.bh == null) {
                        this.bG.remove(this.bL + 1);
                    } else {
                        if (-1 != ((BackForwardItem) this.bG.get(this.bL + 1)).abU) {
                            this.bG.set(this.bL + 1, new BackForwardItem((byte) -1));
                        }
                        this.bL++;
                    }
                } else if (this.bh == null || this.bh == this.bi || !z) {
                    this.bG.remove(this.bL + 1);
                } else if (true == z) {
                    if (-1 != ((BackForwardItem) this.bG.get(this.bL + 1)).abU) {
                        this.bG.set(this.bL + 1, new BackForwardItem((byte) -1));
                        z2 = false;
                    } else {
                        z2 = z;
                    }
                    this.bL++;
                    z = z2;
                }
            }
        }
    }

    public static String n(String str) {
        return str == null ? str : str.endsWith(b.b.act) ? str.substring(0, str.length() - b.b.act.length()) : str.endsWith(b.b.acv) ? str.substring(0, str.length() - b.b.acv.length()) : str;
    }

    private boolean n(int i) {
        int i2 = i;
        while (true) {
            int i3 = i2 + 1;
            if (i3 >= this.bG.size()) {
                return false;
            }
            if (2 == ((BackForwardItem) this.bG.get(i3)).abU) {
                return true;
            }
            i2 = i3;
        }
    }

    private int o(int i) {
        int i2 = 0;
        if (this.bG == null) {
            return 0;
        }
        int i3 = 0;
        while (i2 < i) {
            int i4 = 1 == ((BackForwardItem) this.bG.get(i2)).abU ? i3 + 1 : i3;
            i2++;
            i3 = i4;
        }
        return i3;
    }

    private View o(String str) {
        if (str == null) {
            return this.bi;
        }
        String substring = str.startsWith(b.b.acH) ? str.substring(str.indexOf(cd.bVI) + 1) : str;
        if (substring.contains(b.b.acI)) {
            substring = substring.replace(b.b.acI, "");
        }
        return g(substring);
    }

    private int p(int i) {
        int i2 = 0;
        if (this.bG == null) {
            return 0;
        }
        int i3 = 0;
        while (i2 < i) {
            int i4 = (2 == ((BackForwardItem) this.bG.get(i2)).abU || -1 == ((BackForwardItem) this.bG.get(i2)).abU) ? i3 + 1 : i3;
            i2++;
            i3 = i4;
        }
        return i3;
    }

    private boolean p(String str) {
        if (str == null || str.length() == 0) {
            return false;
        }
        String[] j = j(str);
        for (String contains : b.b.acX) {
            if (j[0].contains(contains)) {
                return true;
            }
        }
        return false;
    }

    public String F() {
        if (this.bi == null) {
            return null;
        }
        if (this.bi == this.be) {
            return null;
        }
        if (this.bi == this.bg) {
            return this.bg.F();
        }
        return null;
    }

    public void I() {
        if (this.bg != null && ModelBrowser.gD() != null && !ModelBrowser.gD().hY()) {
            this.bg.I();
        }
    }

    public void J() {
        if (this.bg != null && ModelBrowser.gD() != null && !ModelBrowser.gD().hY()) {
            this.bg.J();
        }
    }

    public boolean M() {
        if (this.bg == null || ModelBrowser.gD() == null || ModelBrowser.gD().hY()) {
            return false;
        }
        return this.bg.M();
    }

    public View a(String str, Context context) {
        this.bk = (ActivityBrowser) context;
        if (str == null || this.bG == null) {
            return this.bi;
        }
        stopLoading();
        String bI2 = com.uc.a.e.nR().bI(str);
        if (bI2.contains(w.Pi)) {
            Bundle bundle = new Bundle();
            bundle.putString("networkcheck", "networkcheck");
            Intent intent = new Intent(this.bk, ActivityInitial.class);
            intent.putExtras(bundle);
            this.bk.startActivity(intent);
            return this.bi;
        } else if (bI2.contains(b.b.acV) && bI2.startsWith(b.b.acH)) {
            if (p(getUrl())) {
                c(this.bk);
            }
            return this.bi;
        } else if (!com.uc.a.e.nR().qK() || bI2 == null || !bI2.contains(b.b.QZ)) {
            String lowerCase = bI2.toLowerCase();
            if (lowerCase.contains(b.b.acO) || lowerCase.contains(b.b.acP)) {
                b.a.a.d.g(this.bk, n(bI2));
                return this.bi;
            } else if (lowerCase.contains(b.b.acQ)) {
                a(this.bk, n(bI2));
                return this.bi;
            } else {
                if (bI2.startsWith(ModelBrowser.wU) || bI2.startsWith(ModelBrowser.wV)) {
                    c((byte) 0);
                } else if (bI2.startsWith(ModelBrowser.wW)) {
                    c((byte) 1);
                } else if (bI2.startsWith(ModelBrowser.wX)) {
                    c((byte) 2);
                }
                if (bI2.startsWith(b.b.acH) || this.bi == null || bI2.startsWith("ext:download") || bI2.startsWith("ext:null") || bI2.startsWith(b.b.adi)) {
                    this.bq = false;
                } else {
                    this.bq = true;
                }
                if (bI2.contains(w.OS)) {
                    String n = n(bI2);
                    Vector bk2 = com.uc.a.e.nR().bk(n.substring(n.indexOf(w.OS)));
                    if (!(bk2 == null || bk2.size() < 5 || bk2.get(4) == null)) {
                        bI2 = ((String[]) bk2.get(4))[0];
                    }
                }
                if (bI2.contains(w.Qj)) {
                    String url = getUrl();
                    String[] j = j(url);
                    if (url != null && j[0].indexOf("uc.cn") == -1 && j[0].indexOf("ucweb.com") == -1) {
                        return null;
                    }
                    String substring = bI2.substring(bI2.indexOf(w.Qj) + 19);
                    String[] j2 = j(substring);
                    if (substring != null && j2[0].indexOf("uc.cn") == -1 && j2[0].indexOf("ucweb.com") == -1) {
                        return null;
                    }
                }
                if (!bI2.startsWith("InLink:ext:close_window") && !bI2.contains(w.Oz) && !bI2.startsWith("ext:null") && !bI2.startsWith(b.b.adi) && true == this.bn) {
                    g(true);
                    String id = ModelBrowser.gD().id();
                    if (ModelBrowser.gD() != null) {
                        if (com.uc.a.e.nR() == null) {
                            ModelBrowser.gD().a(22, 0, (Object) null);
                            ModelBrowser.gD().a(22, 10, (Object) null);
                        } else if (!com.uc.a.e.nR().F(bI2, id)) {
                            ModelBrowser.gD().a(22, 0, (Object) null);
                            ModelBrowser.gD().a(22, 10, (Object) null);
                        }
                    }
                }
                if (this.bi != null) {
                    bn();
                }
                if (bI2.startsWith("ext:report_website")) {
                    return g(bI2);
                }
                if (bI2.startsWith(b.b.acU)) {
                    return g(bI2);
                }
                String bw2 = com.uc.a.e.nR().nY().bw(h.afq);
                boolean z = bw2.trim().equals(com.uc.a.e.RE);
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().iL();
                }
                if (String.valueOf(0).equals(bw2.trim())) {
                    f.l(1, f.atz);
                } else if (String.valueOf(1).equals(bw2.trim())) {
                    f.l(1, f.atB);
                } else {
                    f.l(1, f.atA);
                }
                if (!ActivityBrowser.Fz()) {
                    f.l(1, f.atC);
                } else {
                    f.l(1, f.atD);
                }
                if (this.bk != null && 2 == this.bk.getResources().getConfiguration().orientation) {
                    f.l(1, f.atK);
                }
                if (com.uc.a.e.RD.equals(com.uc.a.e.nR().bw(h.afy))) {
                    f.l(1, f.atL);
                }
                switch (b.ec()) {
                    case 0:
                        f.l(1, f.atP);
                        break;
                    case 1:
                        f.l(1, f.atQ);
                        break;
                    case 2:
                        f.l(1, f.atR);
                        break;
                    case 3:
                        f.l(1, f.atS);
                        break;
                    case 4:
                        f.l(1, f.atT);
                        break;
                    case 5:
                        f.l(1, f.atU);
                        break;
                    case 99:
                        f.l(1, f.atV);
                        break;
                }
                if (this.bh != null) {
                    this.bh.getSettings().setJavaScriptEnabled(g.Jn().JD());
                }
                boolean startsWith = bI2.startsWith(b.b.acH);
                if (bI2.contains(bN)) {
                    if (bI2.startsWith(b.b.acH)) {
                        bI2 = bI2.substring(bI2.indexOf(cd.bVI) + 1);
                    }
                    if (this.bi == this.bh) {
                        return a(bI2, !startsWith);
                    } else if (this.bi == this.bg) {
                        return g(bI2);
                    }
                }
                if (true == f(bI2)) {
                    if (bI2.contains(b.b.acI)) {
                        bI2 = bI2.substring(bI2.indexOf(b.b.acI) + b.b.acI.length());
                        if (bI2.endsWith(b.b.act)) {
                            bI2 = bI2.substring(0, bI2.indexOf(b.b.act));
                        }
                    }
                    if (bI2.startsWith(b.b.acH)) {
                        bI2 = bI2.substring(bI2.indexOf(cd.bVI) + 1);
                    }
                    if (this.bM == 0) {
                        this.bM = 1;
                    }
                    return a(i(bI2), !startsWith);
                } else if (com.uc.a.e.bH(bI2)) {
                    return o(bI2);
                } else {
                    if (1 == this.bM) {
                        this.bM = 0;
                    }
                    if (!z) {
                        if (bI2.startsWith(b.b.acH)) {
                            bI2 = bI2.substring(bI2.indexOf(cd.bVI) + 1);
                        }
                        String i = i(bI2);
                        String lowerCase2 = i.toLowerCase();
                        String[] pR = com.uc.a.e.nR().pR();
                        if (pR != null) {
                            int i2 = 0;
                            while (i2 < pR.length) {
                                if (pR[i2] == null || !lowerCase2.contains(pR[i2])) {
                                    i2++;
                                } else {
                                    int indexOf = i.indexOf(bx.bAh);
                                    if (indexOf > 0) {
                                        i = i.substring(indexOf);
                                    }
                                    return a(i, !startsWith);
                                }
                            }
                        }
                        if (i.contains(w.Ou) && true == canGoBack()) {
                            return q(-1);
                        }
                        if (!i.contains(w.Pc)) {
                            return g(i);
                        }
                        if (ModelBrowser.gD() != null) {
                            ModelBrowser.gD().aS(54);
                        }
                        return null;
                    } else if (this.bi == null || this.bi != this.be || !bI2.contains("ext:es:") || true != l(bI2) || -1 == bI2.indexOf(bx.bAh)) {
                        if (bI2.startsWith(b.b.acH)) {
                            bI2 = i(bI2.substring(bI2.indexOf(cd.bVI) + 1));
                            if (this.bi == this.bh) {
                                if (true == k(bI2)) {
                                    return (!bI2.contains(w.Ou) || true != canGoBack()) ? g(bI2) : q(-1);
                                }
                                int indexOf2 = bI2.indexOf(bx.bAh);
                                if (indexOf2 > 0) {
                                    bI2 = bI2.substring(indexOf2);
                                }
                                return a(bI2, !startsWith);
                            } else if (bI2.contains(w.Ou) && true == canGoBack()) {
                                return q(-1);
                            } else {
                                if (1 == p()) {
                                    if (true != l(bI2)) {
                                        return g(bI2);
                                    }
                                    int indexOf3 = bI2.indexOf(bx.bAh);
                                    if (indexOf3 > 0) {
                                        bI2 = bI2.substring(indexOf3);
                                    }
                                    return a(bI2, !startsWith);
                                } else if (p() == 0) {
                                    if (true == k(bI2)) {
                                        return g(bI2);
                                    }
                                    return a(bI2, !startsWith);
                                }
                            }
                        }
                        String i3 = i(bI2);
                        if (true == k(i3)) {
                            return (!i3.contains(w.Ou) || true != canGoBack()) ? g(i3) : q(-1);
                        }
                        int indexOf4 = i3.indexOf(bx.bAh);
                        if (indexOf4 > 0) {
                            i3 = i3.substring(indexOf4);
                        }
                        return a(i3, !startsWith);
                    } else {
                        return a(bI2.substring(bI2.indexOf(bx.bAh)), !startsWith);
                    }
                }
            }
        } else {
            a(n(bI2), this.p);
            return null;
        }
    }

    public ViewMainpage a(Context context, int i) {
        this.bk = (ActivityBrowser) context;
        stopLoading();
        bn();
        if (true == bG() && !canGoBack()) {
            ModelBrowser.gD().aS(23);
        }
        this.be = ModelBrowser.gD().gn();
        if (this.cj == -1) {
            this.be.B(1);
        } else {
            this.cj = i;
            this.be.B(this.cj);
        }
        this.bi = this.be;
        return this.be;
    }

    public void a(Context context, String str) {
        if (context != null && str != null) {
            int OS = a.OS();
            if (3 == OS || 5 == OS || 6 == OS) {
                c(this.bk, str);
            } else {
                b(context, str);
            }
        }
    }

    public void a(ActivityBrowser activityBrowser) {
        if (activityBrowser != null) {
            this.bk = activityBrowser;
            if (this.bh == null) {
                this.bh = new WebViewZoom(this.bk);
                this.bh.setWebViewClient(this.bZ);
                this.bh.setWebChromeClient(this.cb);
                this.bh.setDownloadListener(this.bk);
                this.bh.setPictureListener(new WebView.PictureListener() {
                    boolean bxc = true;

                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.uc.browser.WindowUCWeb.d(com.uc.browser.WindowUCWeb, boolean):boolean
                     arg types: [com.uc.browser.WindowUCWeb, int]
                     candidates:
                      com.uc.browser.WindowUCWeb.d(android.content.Context, java.lang.String):void
                      com.uc.browser.WindowUCWeb.d(com.uc.browser.WindowUCWeb, boolean):boolean */
                    public void onNewPicture(WebView webView, Picture picture) {
                        if (true == WindowUCWeb.this.bq) {
                            boolean unused = WindowUCWeb.this.bq = false;
                            WindowUCWeb.this.bh.bh(!WindowUCWeb.this.bq);
                            WindowUCWeb.this.bh.zI();
                            WindowUCWeb.this.bh.invalidate();
                        }
                        if (!WindowUCWeb.this.bw) {
                            boolean unused2 = WindowUCWeb.this.bw = true;
                        }
                        if (true == this.bxc) {
                            this.bxc = false;
                            if (WindowUCWeb.this.bn) {
                                ModelBrowser.gD().a(127, WindowUCWeb.this.bh.getUrl());
                            }
                        }
                    }
                });
                this.bk.registerForContextMenu(this.bh);
                this.bi = this.bh;
            }
        }
    }

    public void a(Object obj) {
        if (this.bg != null && this.bi == this.bg) {
            this.bg.a(obj);
        }
    }

    public void a(String str, n nVar) {
        if (str != null && str.length() != 0) {
            try {
                if (ModelBrowser.gD() != null && ModelBrowser.gD().aj(bF)) {
                    int indexOf = str.indexOf(b.b.QZ);
                    int length = b.b.QZ.length();
                    if (-1 != indexOf && str.length() > indexOf + length) {
                        String substring = str.substring(indexOf + length);
                        int indexOf2 = substring.indexOf(b.b.adc);
                        if (-1 != indexOf2) {
                            int i = indexOf2;
                            String str2 = substring;
                            int i2 = i;
                            while (-1 != i2) {
                                String trim = str2.substring(0, i2).trim();
                                if (trim.endsWith(b.XF)) {
                                    trim = trim.substring(0, trim.length() - 1);
                                }
                                String substring2 = str2.substring(i2);
                                String substring3 = substring2.substring(substring2.indexOf(34) + 1);
                                String str3 = trim + substring3.substring(substring3.indexOf(34) + 1);
                                str2 = str3;
                                i2 = str3.indexOf(b.b.adc);
                            }
                            substring = str2;
                        }
                        if (substring.endsWith(b.XF)) {
                            substring = substring.substring(0, substring.length() - 1);
                        }
                        String str4 = substring + b.b.adb;
                        if (this.bg != null) {
                            this.bg.b(String.valueOf(System.currentTimeMillis()), str4);
                        }
                        b.a.a.d.a(this.bk, str4, nVar);
                        return;
                    }
                }
                if (bM()) {
                    if (a(this.bk, b.b.adf, b.b.ade + b.b.adf)) {
                        d(this.bk, b.b.ade + b.b.adf);
                    }
                } else if (this.bg != null) {
                    bL();
                }
            } catch (Exception e) {
            }
        }
    }

    public void a(Object[] objArr) {
        if (this.bD == null) {
            this.bD = new StreamingMediaThread(objArr);
            this.bD.start();
        }
    }

    public boolean a(View view) {
        return view == this.be;
    }

    public boolean au() {
        if (this.bi == null || this.bi != this.bg) {
            return false;
        }
        return this.bg.au();
    }

    public void b(Context context, String str) {
        if (context != null && str != null && this.bk != null && this.bD == null) {
            int OS = a.OS();
            if (1 == OS) {
                Toast.makeText(this.bk, (int) R.string.f993toast_wifi, 1).show();
            } else if (2 == OS || 4 == OS) {
                Toast.makeText(this.bk, (int) R.string.f994toast_net, 1).show();
            }
            a(new Object[]{context, str});
        }
    }

    public void bA() {
        if (this.bh == this.bi) {
            this.bh.zoomIn();
        }
    }

    public void bB() {
        if (this.bh == this.bi) {
            this.bh.zoomOut();
        }
    }

    public void bC() {
    }

    public void bD() {
        this.bn = false;
        if (this.be != null) {
        }
    }

    public void bE() {
        this.bn = true;
        if (this.be != null) {
        }
    }

    public boolean bF() {
        return this.by;
    }

    public boolean bG() {
        return this.ch != -1;
    }

    public byte bH() {
        return this.ch;
    }

    public boolean bI() {
        return this.bi != null && (this.bi instanceof WebViewZoom) && this.bL >= 0 && ((BackForwardItem) this.bG.get(this.bL)).ceq == 1;
    }

    public WebViewJUC bJ() {
        return this.bg;
    }

    public int bK() {
        if (this.cj == -1) {
            return 1;
        }
        return this.cj;
    }

    public void bL() {
        new UCAlertDialog.Builder(this.bk).aH(R.string.f1531alipay_is_donwload_title).aG(R.string.f1532alipay_is_download).a((int) R.string.f1533alipay_yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (WindowUCWeb.this.bg != null) {
                    WindowUCWeb.this.bg.d(com.uc.a.e.nR().qJ(), WindowUCWeb.this.p);
                }
            }
        }).c((int) R.string.f1534alipay_no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show();
    }

    public boolean bM() {
        try {
            return this.bk.getAssets().open(b.b.adf) != null;
        } catch (Exception e) {
        }
    }

    public String bN() {
        return this.ck;
    }

    public int bO() {
        if (this.bi instanceof WebViewZoom) {
            return ModelBrowser.ak(getUrl());
        }
        if (this.bi instanceof WebViewJUC) {
            return ((WebViewJUC) this.bi).n();
        }
        return 0;
    }

    public void bf() {
        ModelBrowser.gD().wK = false;
        Vector qc = com.uc.a.e.nR().qc();
        if (qc != null && true != qc.isEmpty()) {
            this.bk.startActivityForResult(new Intent(this.bk.getBaseContext(), ActivitySelector.class), 4);
            if (this.bg != null) {
                this.bg.zI();
            }
        } else if (this.bg != null) {
            stopLoading();
            this.bg.zG();
        }
    }

    public View bg() {
        return this.bj;
    }

    public WebViewZoom bh() {
        return this.bh;
    }

    public String bi() {
        if (this.bi == null) {
            return null;
        }
        if (this.bi == this.be) {
            return null;
        }
        if (this.bi == this.bg) {
            return this.bg.y();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public Bundle bj() {
        return this.bO;
    }

    public boolean bk() {
        if (this.bi != this.be || this.be == null) {
            return this.bm;
        }
        return false;
    }

    public boolean bo() {
        return !canGoBack() && true == bG();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.WebViewJUC.a(android.graphics.Canvas, boolean, boolean):void
     arg types: [android.graphics.Canvas, int, int]
     candidates:
      com.uc.browser.WebViewJUC.a(int, float, float):void
      com.uc.browser.WebViewJUC.a(android.graphics.Canvas, boolean, boolean):void */
    public Bitmap bp() {
        int width;
        int height;
        if (this.bi != null) {
            if (this.bt != null) {
                this.bt.recycle();
                this.bt = null;
            }
            if (this.bu != null) {
                this.bu.recycle();
                this.bu = null;
            }
            Canvas canvas = new Canvas();
            try {
                Resources resources = this.bk.getResources();
                int dimension = (int) resources.getDimension(R.dimen.f454multiwindow_snapshot_height);
                int dimension2 = (int) resources.getDimension(R.dimen.f453multiwindow_snapshot_width);
                if (this.bi == this.bh) {
                    this.bt = Bitmap.createBitmap(dimension2, dimension, Bitmap.Config.RGB_565);
                    canvas.setBitmap(this.bt);
                    canvas.save();
                    int height2 = this.bi.getHeight();
                    int width2 = this.bi.getWidth();
                    Picture capturePicture = this.bh.capturePicture();
                    float width3 = ((float) this.bt.getWidth()) / ((float) width2);
                    canvas.scale(width3, width3);
                    int width4 = capturePicture.getWidth() - width2;
                    int height3 = capturePicture.getHeight() - height2;
                    if (this.bi.getScrollX() <= width4) {
                        width4 = this.bi.getScrollX();
                    }
                    if (this.bi.getScrollY() <= height3) {
                        height3 = this.bi.getScrollY();
                    }
                    canvas.translate((float) (-width4), (float) (-height3));
                    canvas.drawColor(-1);
                    capturePicture.draw(canvas);
                    return this.bt;
                } else if (this.bi != this.bg || this.bg == null || !this.bg.ac()) {
                    if (this.bi == this.be && this.be != null) {
                        return this.cj != -1 ? this.be.eG(this.cj) : this.be.eG(this.be.uX());
                    }
                    this.bt = Bitmap.createBitmap(dimension2, dimension, Bitmap.Config.RGB_565);
                    if (this.bi != this.bg || this.bg == null) {
                        width = this.bi.getWidth();
                        height = this.bi.getHeight();
                    } else {
                        width = this.bg.Qn();
                        height = this.bg.Qo();
                    }
                    float f = ((float) dimension2) / ((float) width);
                    float f2 = ((float) dimension) / ((float) height);
                    if (f2 > f) {
                        f = f2;
                    }
                    canvas.scale(f, f);
                    canvas.setBitmap(this.bt);
                    canvas.save();
                    if (this.bi == this.bg) {
                        this.bg.a(canvas, false, false);
                    } else {
                        this.bi.draw(canvas);
                    }
                    canvas.restore();
                    return this.bt;
                } else if (!this.bg.aj()) {
                    return this.bg.ai();
                } else {
                    try {
                        Bitmap a2 = this.bg.a(0.60723f);
                        this.bg.a(a2);
                        return a2;
                    } catch (Exception | OutOfMemoryError e) {
                        return bq();
                    }
                }
            } catch (OutOfMemoryError e2) {
                System.gc();
                throw e2;
            }
        } else {
            if (this.bu != null) {
                this.bu.recycle();
            }
            if (this.bt == null) {
                return null;
            }
            int i = this.bt.getHeight() > this.bt.getWidth() ? 1 : 2;
            int i2 = this.bk.getResources().getConfiguration().orientation;
            if (i == i2) {
                return this.bt;
            }
            if (i2 == 1) {
                try {
                    this.bu = Bitmap.createBitmap(this.bt, 0, 0, 92, 112);
                } catch (OutOfMemoryError e3) {
                    System.gc();
                    return null;
                }
            } else {
                this.bu = Bitmap.createBitmap(this.bt, 0, 0, 185, 92);
            }
            return this.bu;
        }
    }

    public Bitmap bq() {
        return null;
    }

    public boolean br() {
        if (ModelBrowser.gD() == null) {
            return false;
        }
        boolean hw = ModelBrowser.gD().hw();
        if ((hw || this.bl) && (!hw || !this.bl)) {
            return false;
        }
        if (this.bi != null && this.bi == this.bh) {
            this.bh.resumeTimers();
        }
        return true;
    }

    public boolean bs() {
        if (ModelBrowser.gD() == null) {
            return false;
        }
        if (!ModelBrowser.gD().hw() || this.bl) {
            return false;
        }
        if (this.bi != null && this.bi == this.bh) {
            this.bh.pauseTimers();
        }
        return true;
    }

    public void bt() {
        if (this.bi != null && this.bi == this.bh) {
            this.bh.zM();
        }
    }

    public void bu() {
        if (this.bi != null && this.bi == this.bh) {
            this.bh.zL();
        }
    }

    /* access modifiers changed from: package-private */
    public void bv() {
    }

    public void bw() {
        r(1);
        bx();
    }

    public void bx() {
        if (this.be == null || this.bi == this.be) {
        }
    }

    public void by() {
        if (this.bg != null) {
            this.bg.Qs();
        }
    }

    public void bz() {
        if (true == bG() && !bo()) {
            c((byte) -1);
        }
        if (this.bG != null) {
            this.bG.clear();
        }
        if (this.be == this.bi) {
            if (this.bG != null) {
                this.bG.clear();
            }
            this.bL = -1;
            if (this.bg != null) {
                this.bg.Qv();
                this.bg = null;
            }
            if (this.bh != null) {
                g.Jn().b(this.bh.getSettings());
                this.bh.destroy();
                this.bh = null;
            }
        } else if (this.bg == this.bi) {
            this.bG.add(new BackForwardItem((byte) 1));
            this.bg.oS();
            this.bL = 0;
            if (this.bh != null) {
                g.Jn().b(this.bh.getSettings());
                this.bh.destroy();
                this.bh = null;
            }
        } else if (this.bh == this.bi) {
            this.bG.add(new BackForwardItem((byte) 2, this.bM));
            this.bh.clearHistory();
            this.bL = 0;
            if (this.bg != null) {
                this.bg.Qv();
                this.bg = null;
            }
        }
        if (this.bh != null) {
            this.bh.clearCache(true);
        }
        if (this.bg != null) {
            this.bg.oS();
        }
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().aS(23);
        }
    }

    public String c(String str, String str2) {
        String substring;
        int indexOf;
        String substring2;
        int indexOf2;
        if (str == null || ((str.length() == 0 && str2 == null) || str2.length() == 0)) {
            return null;
        }
        try {
            int indexOf3 = str.indexOf(str2);
            if (-1 == indexOf3 || -1 == (indexOf = (substring = str.substring(indexOf3 + str2.length())).indexOf("{")) || -1 == (indexOf2 = (substring2 = substring.substring(indexOf + 1)).indexOf("}"))) {
                return null;
            }
            return substring2.substring(0, indexOf2);
        } catch (Exception e) {
            return null;
        }
    }

    public void c(byte b2) {
        this.ch = b2;
    }

    /* access modifiers changed from: package-private */
    public void c(final Context context, final String str) {
        if (context != null) {
            UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
            builder.aH(R.string.f970tip);
            builder.aG(R.string.f1202dialog_rtsp_not_support);
            builder.a((int) R.string.f1129alert_dialog_play, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    WindowUCWeb.this.b(context, str);
                    dialogInterface.cancel();
                }
            });
            builder.c((int) R.string.f1130alert_dialog_change_access_points, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    context.startActivity(new Intent("android.settings.WIRELESS_SETTINGS"));
                    dialogInterface.cancel();
                }
            });
            builder.fh().show();
        }
    }

    public boolean c(int i, KeyEvent keyEvent) {
        if (this.bg != null && this.bi == this.bg && (23 == i || 19 == i || 20 == i || 21 == i || 22 == i)) {
            if (keyEvent.getAction() == 0) {
                return this.bg.e(i, keyEvent);
            }
            if (1 == keyEvent.getAction()) {
                return this.bg.f(i, keyEvent);
            }
        }
        return false;
    }

    public boolean canGoBack() {
        return (this.bG != null && this.bL > 0 && this.bL <= this.bG.size() - 1) || (this.bi != this.be && !bG()) || (this.bi == this.be && this.bL > -1);
    }

    public boolean canGoForward() {
        return (this.bG != null && -1 <= this.bL && this.bL < this.bG.size() - 1) || (true == au() && ModelBrowser.gD() != null && !ModelBrowser.gD().iE());
    }

    public void clearFormData() {
        if (this.bh != null) {
            this.bh.clearFormData();
        }
    }

    public void d(byte b2) {
        this.bM = b2;
    }

    public void d(Context context, String str) {
        if (context != null && str != null) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.addFlags(268435456);
            intent.setDataAndType(Uri.parse("file://" + str), "application/vnd.android.package-archive");
            context.startActivity(intent);
        }
    }

    public void destroy() {
        if (this.bG != null) {
            this.bG.clear();
        }
        this.cb = null;
        this.bZ = null;
        if (this.bt != null && !this.bt.isRecycled()) {
            this.bt.recycle();
            this.bt = null;
        }
        this.p = null;
        if (this.bg != null) {
            this.bg.QA();
        }
        if (this.bi != null) {
            try {
                ViewGroup viewGroup = (ViewGroup) this.bi.getParent();
                if (viewGroup != null) {
                    viewGroup.removeView(this.bi);
                }
            } catch (Exception e) {
            }
            this.bi = null;
        }
        if (this.bj != null) {
            try {
                ViewGroup viewGroup2 = (ViewGroup) this.bj.getParent();
                if (viewGroup2 != null) {
                    viewGroup2.removeView(this.bj);
                }
            } catch (Exception e2) {
            }
            this.bj = null;
        }
        if (this.be != null) {
            this.be = null;
        }
        if (this.bg != null) {
            this.bg.QA();
            try {
                this.bg.Qv();
            } catch (Exception e3) {
            }
            this.bg = null;
        }
        if (this.bh != null) {
            g.Jn().b(this.bh.getSettings());
            this.bh.destroy();
            this.bh = null;
        }
    }

    public View getCurrentView() {
        return this.bi;
    }

    /* access modifiers changed from: package-private */
    public int getProgress() {
        if (this.bi == null) {
            return 100;
        }
        return this.bi == this.bg ? this.bg.getProgress() : this.bi == this.bh ? this.bh.getProgress() : this.bi == this.be ? 100 : 100;
    }

    public String getTitle() {
        try {
            if (this.bi != null) {
                if (this.bi == this.be) {
                    this.bs = this.bk.getString(R.string.f952app_name);
                } else if (this.bi == this.bg) {
                    this.bs = this.bg.getTitle();
                } else {
                    this.bs = this.bh.getTitle();
                }
                if (this.bs == null) {
                    this.bs = this.bk.getString(R.string.f952app_name);
                }
            }
        } catch (Exception e) {
        }
        return this.bs;
    }

    public String getUrl() {
        if (this.bi != null) {
            if (this.bi == this.be) {
                this.br = "UCBrowser";
            } else if (this.bi == this.bg) {
                this.br = this.bg.getUrl();
                if (this.br != null && this.br.startsWith("ext")) {
                    this.br = null;
                }
            } else if (this.bi == this.bh) {
                this.br = this.bh.getUrl();
            }
        }
        return this.br;
    }

    public void h(boolean z) {
        this.bp = z;
        if (this.bh != null) {
            this.bh.getSettings().setBuiltInZoomControls(!z);
        }
    }

    public void i(boolean z) {
        this.bn = z;
    }

    public void j(boolean z) {
        this.by = z;
    }

    public void k(boolean z) {
        if (true == this.bn && this.bi != null && this.bi == this.bg) {
            this.bg.k(z);
        }
    }

    public void l(boolean z) {
        this.bC = z;
    }

    /* access modifiers changed from: package-private */
    public void m(String str) {
        if (str == null || this.be == null) {
        }
    }

    public void m(boolean z) {
        this.bY = z;
    }

    public byte p() {
        if (this.bg == null || this.bi != this.bg) {
            return -1;
        }
        return this.bg.p();
    }

    public int q() {
        if (this.bi == this.bg) {
            return this.bg.q();
        }
        return -1;
    }

    public View q(int i) {
        BackForwardItem backForwardItem;
        stopLoading();
        if (this.bG == null) {
            return this.bi;
        }
        int size = this.bG.size();
        bn();
        if (size > this.bG.size()) {
            if (this.be == this.bi) {
                this.bL = -1;
                this.be.B(1);
                if (true == this.bp && ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(34, 0, (Object) null);
                }
                this.bM = 0;
                return this.bi;
            }
            if (this.bg == this.bi) {
                this.bg.bh(true);
                this.bg.qu();
                this.bg.postInvalidate();
            } else if (this.bh == this.bi) {
                this.bh.bh(true);
                this.bh.postInvalidate();
                bt();
            }
            if (-1 < this.bL && this.bL < this.bG.size()) {
                this.bM = ((BackForwardItem) this.bG.get(this.bL)).ceq;
            }
            return this.bi;
        } else if ((-1 == this.bL && i < 0 && this.bi == this.be) || (this.bL == this.bG.size() - 1 && i > 0 && !au())) {
            return this.bi;
        } else {
            if (this.bL <= 0 && i < 0 && this.bi != this.be) {
                if (this.be == null) {
                    this.be = ModelBrowser.gD().gn();
                }
                this.bL = -1;
                this.be.B(1);
                this.bi = this.be;
                if (true == this.bp && ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(34, 0, (Object) null);
                }
                this.bM = 0;
                return this.bi;
            } else if (this.bG == null || this.bG.size() <= 0) {
                return this.bi;
            } else {
                if (i < 0) {
                    if (this.bL > 0 && this.bi != this.be) {
                        this.bL--;
                    }
                    Object obj = this.bG.get(this.bL);
                    while (true) {
                        backForwardItem = (BackForwardItem) obj;
                        if (-1 != backForwardItem.abU) {
                            break;
                        }
                        int i2 = this.bL - 1;
                        this.bL = i2;
                        if (i2 < 0) {
                            break;
                        }
                        obj = this.bG.get(this.bL);
                    }
                } else if (i > 0) {
                    if (this.bL < this.bG.size() - 1) {
                        Vector vector = this.bG;
                        int i3 = this.bL + 1;
                        this.bL = i3;
                        Object obj2 = vector.get(i3);
                        while (true) {
                            backForwardItem = (BackForwardItem) obj2;
                            if (-1 != backForwardItem.abU) {
                                break;
                            }
                            int i4 = this.bL + 1;
                            this.bL = i4;
                            if (i4 >= this.bG.size()) {
                                break;
                            }
                            obj2 = this.bG.get(this.bL);
                        }
                    } else {
                        backForwardItem = null;
                    }
                    if (backForwardItem == null) {
                        if (true == au()) {
                            this.bg.av();
                            if (true == this.bY) {
                                f.l(0, f.ato);
                                f.m(0, f.asT);
                                this.bY = false;
                            }
                            this.bi = this.bg;
                            this.bG.add(new BackForwardItem((byte) 1));
                            this.bL++;
                            if (ModelBrowser.gD() != null) {
                                ModelBrowser.gD().ix();
                            }
                        }
                        return this.bi;
                    }
                } else {
                    backForwardItem = null;
                }
                this.bM = backForwardItem.ceq;
                switch (backForwardItem.abU) {
                    case 1:
                        this.bg.bh(true);
                        if (-1 != this.bg.x()) {
                            int o = o(this.bL) - this.bg.x();
                            if (o != 0) {
                                if (o < 0) {
                                    this.bg.goBack();
                                } else {
                                    this.bg.goForward();
                                }
                            }
                            if (this.bg != this.bi) {
                                this.bg.aT();
                                this.bi = this.bg;
                                this.bj = this.bg;
                                if (true == this.bp && ModelBrowser.gD() != null) {
                                    ModelBrowser gD = ModelBrowser.gD();
                                    ModelBrowser.gD();
                                    gD.a(34, 0, (Object) null);
                                }
                            }
                            this.bk.t(this.bg.getTitle());
                            break;
                        }
                        break;
                    case 2:
                        this.bh.bh(true);
                        if (-1 != this.bh.copyBackForwardList().getCurrentIndex()) {
                            int p2 = p(this.bL) - this.bh.copyBackForwardList().getCurrentIndex();
                            if (p2 != 0) {
                                this.bh.goBackOrForward(p2);
                                if (true == this.bn) {
                                    g(true);
                                    if (ModelBrowser.gD() != null) {
                                        ModelBrowser.gD().a(22, 10, (Object) null);
                                    }
                                }
                            } else {
                                e(this.bh.getUrl());
                            }
                            if (this.bh != this.bi) {
                                this.bi = this.bh;
                                this.bj = this.bh;
                                if (true == this.bp && ModelBrowser.gD() != null) {
                                    ModelBrowser gD2 = ModelBrowser.gD();
                                    ModelBrowser.gD();
                                    gD2.a(34, 1, (Object) null);
                                }
                            }
                            bt();
                            this.bk.t(this.bh.getTitle());
                            break;
                        }
                        break;
                }
                return this.bi;
            }
        }
    }

    public void q(String str) {
        this.ck = str;
        c((byte) 3);
    }

    public void r(int i) {
        if (this.bg != null && 1 == i && ModelBrowser.gD() != null && !ModelBrowser.gD().hY()) {
            this.bg.aT();
        }
    }

    public void refresh() {
        if (this.bi != this.be) {
            if (this.bi != this.bg) {
                if (!this.bh.zF() && this.bn) {
                    ModelBrowser.gD().a(21, this.bk.getString(R.string.f1305page_loading));
                }
                g(true);
                this.bh.reload();
            } else if (this.bg.Qm()) {
                if (!this.bg.zF() && this.bn) {
                    ModelBrowser.gD().a(21, this.bk.getString(R.string.f1305page_loading));
                }
                g(true);
            }
        }
        ModelBrowser.gD().a(22, 10, (Object) null);
    }

    public void s(int i) {
        this.cj = i;
    }

    /* access modifiers changed from: package-private */
    public void stopLoading() {
        g(false);
        if (this.be != null && this.bi == this.be) {
            return;
        }
        if (this.bh != null && this.bi == this.bh) {
            this.bh.stopLoading();
            this.bZ.onPageFinished(this.bh, this.bh.getUrl());
        } else if (this.bg != null && this.bi == this.bg) {
            this.bg.stopLoading();
        }
    }
}
