package com.uc.browser;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.uc.a.e;
import com.uc.c.as;

public class ViewUploadFont extends RelativeLayout {
    private Drawable au = getResources().getDrawable(R.drawable.f648uploadfont_progress_background);
    private Drawable av = getResources().getDrawable(R.drawable.f647uploadfont_progress);
    private int aw = ((int) getResources().getDimension(R.dimen.f287upload_font_progress_margin));
    private int ax = ((int) getResources().getDimension(R.dimen.f288upload_font_progress_text_size));

    public ViewUploadFont(Context context) {
        super(context);
        addView(LayoutInflater.from(context).inflate((int) R.layout.f945upload_font, (ViewGroup) null));
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        ((HightQualityImageView) findViewById(R.id.f761ucmascot)).bpj = BitmapFactory.decodeResource(getResources(), R.drawable.f646ucmascot, options);
        ((TextView) findViewById(R.id.f862upload_msg)).setText(e.nR().pG());
    }

    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        int width = (int) (((double) getWidth()) * 0.6d);
        int width2 = (int) (((double) getWidth()) * 0.2d);
        int intrinsicHeight = this.au.getIntrinsicHeight();
        this.au.setBounds(width2, this.aw, width2 + width, this.aw + intrinsicHeight);
        this.au.draw(canvas);
        int intrinsicHeight2 = this.av.getIntrinsicHeight();
        int pH = e.nR().pH();
        if (pH != 100 && pH >= 60) {
            pH = Math.min(pH + 1, 99);
        }
        this.av.setBounds(width2, this.aw, ((width * pH) / 100) + width2, intrinsicHeight + this.aw);
        this.av.draw(canvas);
        Paint paint = new Paint();
        paint.setTextSize((float) this.ax);
        paint.setAntiAlias(true);
        paint.setColor(getResources().getColor(R.color.f129upload_font_text_color));
        String str = "" + pH + as.azr;
        canvas.drawText(str, (((float) (width + width2)) - paint.measureText(str)) - 2.0f, ((float) this.aw) + ((((float) intrinsicHeight2) - paint.ascent()) / 2.0f), paint);
    }
}
