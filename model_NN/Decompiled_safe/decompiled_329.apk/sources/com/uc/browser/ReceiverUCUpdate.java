package com.uc.browser;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ReceiverUCUpdate extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent.getDataString().contains("com.uc.browser") && "android.intent.action.PACKAGE_REPLACED".equals(intent.getAction())) {
            context.getSharedPreferences(ModelBrowser.xg, 0).edit().putInt(ModelBrowser.xi, 0).commit();
        }
    }
}
