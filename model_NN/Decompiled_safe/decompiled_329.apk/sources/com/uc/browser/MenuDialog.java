package com.uc.browser;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.Toast;
import b.a.a.f;
import b.b;
import com.uc.browser.UCAlertDialog;
import com.uc.browser.UCR;
import com.uc.browser.ViewWebSchVisitPage;
import com.uc.c.w;
import com.uc.e.a.x;
import com.uc.e.h;
import com.uc.e.l;
import com.uc.e.z;
import com.uc.h.a;
import com.uc.h.c;
import com.uc.h.d;
import com.uc.h.e;

public class MenuDialog extends Dialog implements h, l, a {
    public static final String bBV = "pref_pic_qual_bak";
    private x aux = new x() {
        public void ay(String str) {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().a(119, 1, str);
            }
        }

        public void kP() {
        }

        public void kQ() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(120);
            }
        }

        public void kR() {
            MenuDialog.this.IQ();
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(128);
            }
        }

        public void onCancel() {
        }
    };
    private Animation avt = AnimationUtils.loadAnimation(getContext(), R.anim.f14fade_out_menubox);
    private Drawable bBA;
    private Drawable bBB;
    private Drawable bBC;
    private Drawable bBD;
    private Drawable bBE;
    private Drawable bBF;
    private Drawable bBG;
    private Drawable bBH;
    private String bBI;
    private String bBJ;
    /* access modifiers changed from: private */
    public ViewWebSchVisitPage bBK;
    private boolean bBL;
    private String bBM;
    /* access modifiers changed from: private */
    public boolean bBN;
    /* access modifiers changed from: private */
    public boolean bBO;
    /* access modifiers changed from: private */
    public boolean bBP;
    public boolean bBQ = false;
    public boolean bBR;
    private RelativeLayout bBS;
    private ViewWebSchVisitPage.URLBarAniListener bBT = new ViewWebSchVisitPage.URLBarAniListener() {
        public void ef() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.uc.browser.MenuDialog.b(com.uc.browser.MenuDialog, boolean):boolean
         arg types: [com.uc.browser.MenuDialog, int]
         candidates:
          com.uc.browser.MenuDialog.b(com.uc.e.z, int):void
          com.uc.e.h.b(com.uc.e.z, int):void
          com.uc.browser.MenuDialog.b(com.uc.browser.MenuDialog, boolean):boolean */
        public void eg() {
            boolean unused = MenuDialog.this.bBP = true;
            if (MenuDialog.this.bBO) {
                boolean unused2 = MenuDialog.this.bBP = false;
                MenuDialog.this.bBK.setVisibility(4);
            }
            boolean unused3 = MenuDialog.this.bBN = false;
        }
    };
    private Animation bBU = AnimationUtils.loadAnimation(getContext(), R.anim.f11fade_in_menubox);
    /* access modifiers changed from: private */
    public MenuLayout bBp;
    private boolean bBq = true;
    private RelativeLayout bBr;
    private Drawable bBs;
    private String bBt;
    private Drawable bBu;
    private String bBv;
    private Drawable bBw;
    private String bBx;
    private Drawable bBy;
    private String bBz;
    private ActivityBrowser bk;
    private e vD;
    private ViewMainBar vQ;

    public MenuDialog(Context context) {
        super(context, R.style.f1576multi_window_dialog);
        IA();
    }

    private void IA() {
        if (ModelBrowser.gD() != null) {
            this.bk = (ActivityBrowser) ModelBrowser.gD().getContext();
        }
        this.vD = e.Pb();
        this.vD.a(this);
        this.bBr = (RelativeLayout) LayoutInflater.from(super.getContext()).inflate((int) R.layout.f913menu_dialog_layout, (ViewGroup) null);
        this.vQ = new ViewMainBar(getContext());
        this.vD.kp(R.dimen.f177controlbar_height);
        this.bBS = (RelativeLayout) this.bBr.findViewById(R.id.f699controlbar_layout);
        this.bBS.addView(this.vQ, new RelativeLayout.LayoutParams(-1, -1));
        IB();
    }

    private void IB() {
        this.bBs = e.Pb().getDrawable(UCR.drawable.aNY);
        this.bBt = getContext().getResources().getString(R.string.f1073menu_stoprefreshtimer);
        this.bBu = e.Pb().getDrawable(UCR.drawable.aNP);
        this.bBv = getContext().getResources().getString(R.string.f1072menu_refreshtimer);
        this.bBw = e.Pb().getDrawable(UCR.drawable.aNz);
        this.bBx = getContext().getResources().getString(R.string.f1051menu_nightmode);
        this.bBy = e.Pb().getDrawable(UCR.drawable.aNo);
        this.bBz = getContext().getResources().getString(R.string.f1052menu_day);
        this.bBA = e.Pb().getDrawable(UCR.drawable.aVA);
        this.bBB = e.Pb().getDrawable(UCR.drawable.aNF);
        this.bBC = e.Pb().getDrawable(UCR.drawable.aVv);
        this.bBD = e.Pb().getDrawable(UCR.drawable.aND);
        this.bBE = e.Pb().getDrawable(UCR.drawable.aUS);
        this.bBF = e.Pb().getDrawable(UCR.drawable.aUR);
        this.bBG = e.Pb().getDrawable(UCR.drawable.aNs);
        this.bBH = e.Pb().getDrawable(UCR.drawable.aNE);
        this.bBI = getContext().getResources().getString(R.string.f1049menu_fullscreen);
        this.bBJ = getContext().getResources().getString(R.string.f1050menu_outfullscreen);
    }

    private void IE() {
        if (ModelBrowser.gD() != null) {
            MenuLayout menuLayout = this.bBp;
            if (MenuLayout.K != null) {
                boolean hK = ModelBrowser.gD().hK();
                MenuLayout menuLayout2 = this.bBp;
                MenuLayout.K.H(hK);
            }
        }
    }

    private void IF() {
        if (ModelBrowser.gD() != null) {
            MenuLayout menuLayout = this.bBp;
            if (MenuLayout.L != null) {
                boolean z = ModelBrowser.gD().hN() || ModelBrowser.gD().iE() || ModelBrowser.gD().iF() || ModelBrowser.gD().iG();
                MenuLayout menuLayout2 = this.bBp;
                MenuLayout.L.H(!z);
            }
        }
    }

    private void IG() {
        if (ModelBrowser.gD() != null) {
            MenuLayout menuLayout = this.bBp;
            if (MenuLayout.M != null) {
                boolean hL = ModelBrowser.gD().hL();
                MenuLayout menuLayout2 = this.bBp;
                MenuLayout.M.H(hL);
            }
        }
    }

    private void IH() {
        if (ModelBrowser.gD() != null) {
            MenuLayout menuLayout = this.bBp;
            if (MenuLayout.R != null) {
                boolean hM = ModelBrowser.gD().hM();
                MenuLayout menuLayout2 = this.bBp;
                MenuLayout.R.H(hM);
            }
        }
    }

    private void II() {
        if (ModelBrowser.gD() != null) {
            MenuLayout menuLayout = this.bBp;
            if (MenuLayout.U != null) {
                boolean hN = ModelBrowser.gD().hN();
                MenuLayout menuLayout2 = this.bBp;
                MenuLayout.U.H(!hN);
            }
        }
    }

    private void IJ() {
        if (true == ActivityBrowser.Fz()) {
            hS(1);
        } else {
            hS(0);
        }
    }

    private void IK() {
        if (ModelBrowser.gD() != null) {
            MenuLayout menuLayout = this.bBp;
            if (MenuLayout.B != null) {
                boolean z = true;
                if (ModelBrowser.gD().hN() || ModelBrowser.gD().as() || ModelBrowser.gD().iE()) {
                    z = false;
                }
                MenuLayout menuLayout2 = this.bBp;
                MenuLayout.B.H(z);
            }
        }
    }

    private void IL() {
        if (ModelBrowser.gD() != null) {
            MenuLayout menuLayout = this.bBp;
            if (MenuLayout.D != null) {
                boolean z = true;
                if (ModelBrowser.gD().iE()) {
                    z = false;
                }
                MenuLayout menuLayout2 = this.bBp;
                MenuLayout.D.H(z);
            }
        }
    }

    private void IM() {
        int parseInt = Integer.parseInt(com.uc.a.e.nR().nY().bw(com.uc.a.h.aeV));
        e Pb = e.Pb();
        if (parseInt > 0) {
            MenuLayout menuLayout = this.bBp;
            if (MenuLayout.W != null) {
                MenuLayout menuLayout2 = this.bBp;
                MenuLayout.W.setIcon(Pb.getDrawable(UCR.drawable.aNp));
                MenuLayout menuLayout3 = this.bBp;
                MenuLayout.W.setText(Pb.getString(R.string.f1084menu_nopic));
                MenuLayout menuLayout4 = this.bBp;
                MenuLayout.W.bE(UCR.drawable.aNp);
                return;
            }
        }
        if (parseInt == 0) {
            MenuLayout menuLayout5 = this.bBp;
            if (MenuLayout.W != null) {
                MenuLayout menuLayout6 = this.bBp;
                MenuLayout.W.setIcon(Pb.getDrawable(UCR.drawable.aOd));
                MenuLayout menuLayout7 = this.bBp;
                MenuLayout.W.setText(Pb.getString(R.string.f1085menu_nonopic));
                MenuLayout menuLayout8 = this.bBp;
                MenuLayout.W.bE(UCR.drawable.aOd);
            }
        }
    }

    private void IN() {
        if (Integer.parseInt(com.uc.a.e.nR().nY().bw(com.uc.a.h.afw)) > 0) {
            bO(true);
        } else {
            bO(false);
        }
    }

    private void IO() {
        bP(!"0".equals(com.uc.a.e.nR().nY().bw(com.uc.a.h.afy)));
    }

    private void IP() {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this.bk);
        builder.aH(R.string.f1069menu_help);
        builder.a(this.bk.getResources().getStringArray(R.array.f59help_orders), 0, false, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case 0:
                        String oN = com.uc.a.e.nR().nZ().oN();
                        if (ModelBrowser.gD() != null) {
                            ModelBrowser.gD().a(11, oN);
                        }
                        f.l(0, f.atk);
                        break;
                    case 1:
                        if (ModelBrowser.gD() != null) {
                            ModelBrowser.gD().a(42, (Object) null);
                        }
                        f.l(0, f.ath);
                        break;
                    case 2:
                        if (ModelBrowser.gD() != null) {
                            ModelBrowser.gD().aS(76);
                            break;
                        }
                        break;
                }
                dialogInterface.dismiss();
            }
        });
        builder.c((int) R.string.cancel, (DialogInterface.OnClickListener) null);
        builder.show();
    }

    private void a() {
        getWindow().addFlags(32);
        getWindow().setLayout(-1, -1);
        getWindow().setGravity(80);
        setContentView(this.bBr);
        this.bBK = (ViewWebSchVisitPage) this.bBr.findViewById(R.id.f705inputurl_layout);
        this.bBp = (MenuLayout) this.bBr.findViewById(R.id.f785menu_layout);
        this.bBK.a(this.aux);
        this.bBK.setVisibility(8);
        this.bBK.a(this.bBT);
        this.bBp.a((l) this);
        this.bBp.a((h) this);
        if (1 == getContext().getResources().getConfiguration().orientation) {
            MenuLayout.setOrientation(0);
        } else if (2 == getContext().getResources().getConfiguration().orientation) {
            MenuLayout.setOrientation(1);
        }
    }

    private void hR(int i) {
        switch (i) {
            case MenuLayout.t /*-1002*/:
                II();
                IE();
                IG();
                IF();
                return;
            case MenuLayout.s /*-1001*/:
                IN();
                IO();
                ID();
                IL();
                IM();
                return;
            case MenuLayout.r /*-1000*/:
                IJ();
                IH();
                IK();
                return;
            default:
                return;
        }
    }

    public String Fm() {
        return this.bBM;
    }

    public boolean IC() {
        return this.bBq;
    }

    public void ID() {
        if (ModelBrowser.gD() != null) {
            boolean iE = ModelBrowser.gD().iE();
            g(!iE, ModelBrowser.gD().V());
        }
    }

    public void IQ() {
        this.bBq = true;
        super.hide();
        if (this.bBp != null) {
            this.bBp.reset();
        }
        if (this.bBK != null) {
            this.bBK.Fo();
        }
        if (this.bBK != null) {
            this.bBK.setVisibility(8);
        }
        this.bBL = false;
        this.bBN = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
     arg types: [int, int]
     candidates:
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
     arg types: [?, boolean]
     candidates:
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void */
    public void b(z zVar, int i) {
        switch (i) {
            case MenuLayout.t /*-1002*/:
                hR(MenuLayout.t);
                this.bBp.a((int) MenuLayout.t);
                return;
            case MenuLayout.s /*-1001*/:
                hR(MenuLayout.s);
                this.bBp.a((int) MenuLayout.s);
                return;
            case MenuLayout.r /*-1000*/:
                hR(MenuLayout.r);
                this.bBp.a((int) MenuLayout.r);
                return;
            case UCR.drawable.aNh /*10089*/:
                IQ();
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(11, w.OW);
                    return;
                }
                return;
            case UCR.drawable.aUP /*10090*/:
                IQ();
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(128);
                }
                f.l(0, f.asW);
                return;
            case UCR.drawable.aUR /*10092*/:
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(59);
                }
                IQ();
                return;
            case UCR.drawable.aUT /*10095*/:
                IQ();
                ModelBrowser.gD().a(10, (Object) 0);
                f.l(0, f.asP);
                return;
            case UCR.drawable.aNk /*10099*/:
                IQ();
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(105);
                    return;
                }
                return;
            case UCR.drawable.aUX /*10100*/:
                IQ();
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a((int) ModelBrowser.zZ, (Object) false);
                    return;
                }
                return;
            case UCR.drawable.aNm /*10101*/:
                IQ();
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(42, (Object) null);
                }
                f.l(0, f.ath);
                return;
            case UCR.drawable.aNo /*10106*/:
                IQ();
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(33, (Object) 0);
                }
                int parseInt = Integer.parseInt(com.uc.a.e.nR().nY().bw(com.uc.a.h.afa));
                d dVar = new d(this.bk);
                if (parseInt == -1) {
                    com.uc.a.e.nR().w(com.uc.a.h.afa, String.valueOf(0));
                    dVar.hy(0);
                } else if (parseInt >= dVar.GS().size() || dVar.GS().get(parseInt) == null || !((c) dVar.GS().get(parseInt)).Dj()) {
                    dVar.hy(0);
                } else {
                    dVar.hy(parseInt);
                }
                f.l(0, f.atf);
                return;
            case UCR.drawable.aNp /*10107*/:
                IQ();
                PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putInt(bBV, Integer.parseInt(com.uc.a.e.nR().nY().bw(com.uc.a.h.aeV))).commit();
                com.uc.a.e.nR().nY().w(com.uc.a.h.aeV, "0");
                com.uc.a.e.nR().nY().w(com.uc.a.h.afh, "0");
                Toast.makeText(getContext(), String.format(getContext().getString(R.string.f1086toast_nopic), getContext().getResources().getStringArray(R.array.f30pref_picture_quality)[0]), 0).show();
                f.l(0, f.atn);
                return;
            case UCR.drawable.aVi /*10115*/:
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().vI = 0;
                }
                IQ();
                this.bk.startActivity(new Intent(getContext(), ActivityDownload.class));
                f.l(0, f.ata);
                return;
            case UCR.drawable.aVl /*10118*/:
                IQ();
                String qy = com.uc.a.e.nR().qy();
                if (qy != null && !"".equals(qy) && ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(11, qy);
                    return;
                }
                return;
            case UCR.drawable.aNr /*10119*/:
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().vI = 0;
                }
                IQ();
                getContext().startActivity(new Intent(getContext(), ActivityFileMaintain.class));
                f.l(0, f.asS);
                return;
            case UCR.drawable.aNs /*10121*/:
                IQ();
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(18, (Object) 1);
                }
                f.l(0, f.atj);
                return;
            case UCR.drawable.aNt /*10122*/:
                IQ();
                IP();
                return;
            case UCR.drawable.aNv /*10126*/:
                IQ();
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(ModelBrowser.zR);
                }
                f.l(0, f.asQ);
                return;
            case UCR.drawable.aNz /*10131*/:
                IQ();
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(33, (Object) 1);
                }
                f.l(0, f.atg);
                e.Pb().fY("theme/night.uct");
                e.Pb().update();
                return;
            case UCR.drawable.aND /*10134*/:
                IQ();
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(70, (Object) 0);
                    return;
                }
                return;
            case UCR.drawable.aVv /*10135*/:
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(70, (Object) 1);
                }
                IQ();
                return;
            case UCR.drawable.aNE /*10139*/:
                IQ();
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(18, (Object) 0);
                    return;
                }
                return;
            case UCR.drawable.aNF /*10141*/:
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(60);
                }
                IQ();
                return;
            case UCR.drawable.aVA /*10142*/:
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(60);
                }
                f.l(0, f.atl);
                IQ();
                return;
            case UCR.drawable.aVC /*10145*/:
                IQ();
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(81, (Object) 1);
                    return;
                }
                return;
            case UCR.drawable.aNL /*10146*/:
                IQ();
                f.l(0, f.ati);
                ModelBrowser.gD().ip();
                return;
            case UCR.drawable.aNO /*10148*/:
                IQ();
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(19);
                }
                f.l(0, f.asV);
                return;
            case UCR.drawable.aNP /*10149*/:
                IQ();
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(32, (Object) null);
                }
                f.l(0, f.atc);
                return;
            case UCR.drawable.aVG /*10152*/:
                IQ();
                ModelBrowser gD = ModelBrowser.gD();
                if (gD != null) {
                    String str = b.ach + gD.ie() + "||" + gD.id();
                    if (true == ActivityBrowser.bqy) {
                        gD.a(35, (Object) 1);
                    }
                    gD.a(11, str);
                    return;
                }
                return;
            case UCR.drawable.aNW /*10161*/:
                hide();
                MenuLayout menuLayout = this.bBp;
                if (MenuLayout.K.zP()) {
                    IQ();
                    if (ModelBrowser.gD() != null) {
                        String string = getContext().getResources().getString(R.string.f957share_suffix);
                        String id = ModelBrowser.gD().id();
                        String ie = ModelBrowser.gD().ie();
                        ModelBrowser.gD().a(94, 0, new String[]{id + " " + ie + " " + string, id, ie});
                        return;
                    }
                    return;
                }
                return;
            case UCR.drawable.aVO /*10162*/:
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().vI = 0;
                }
                IQ();
                this.bk.startActivity(new Intent(getContext(), ActivitySkinManage.class));
                return;
            case UCR.drawable.aNY /*10163*/:
                IQ();
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(74, (Object) null);
                    return;
                }
                return;
            case UCR.drawable.aNZ /*10164*/:
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().vI = 0;
                }
                IQ();
                this.bk.startActivity(new Intent(getContext(), ActivitySetting.class));
                f.l(0, f.atb);
                return;
            case UCR.drawable.aOd /*10165*/:
                IQ();
                int i2 = PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(bBV, 2);
                if (i2 == 0) {
                    i2 = 2;
                }
                com.uc.a.e.nR().nY().w(com.uc.a.h.aeV, String.valueOf(i2));
                com.uc.a.e.nR().nY().w(com.uc.a.h.afh, String.valueOf(i2));
                Toast.makeText(getContext(), String.format(getContext().getString(R.string.f1086toast_nopic), getContext().getResources().getStringArray(R.array.f30pref_picture_quality)[i2 - 1]), 0).show();
                f.l(0, f.atn);
                return;
            default:
                return;
        }
    }

    public void bM(boolean z) {
        if (!this.bBq) {
            this.bBq = true;
            boolean z2 = ModelBrowser.gD() != null && ModelBrowser.gD().gR() <= 1;
            if (this.bBN) {
                this.bBK.jW(z2 ? 2 : 1);
                this.bBK.bM(z2 && z && !this.bBQ);
            }
            if (z2) {
                this.avt.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationEnd(Animation animation) {
                        MenuDialog.this.bBp.h();
                        MenuDialog.this.dismiss();
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationStart(Animation animation) {
                    }
                });
                this.bBp.a(this.avt);
            } else {
                this.bBp.a((Animation) null);
                this.bBp.h();
                dismiss();
            }
        }
        this.bBL = false;
        this.bBN = false;
    }

    public void bN(boolean z) {
        super.show();
        boolean z2 = ModelBrowser.gD() != null && ModelBrowser.gD().gR() <= 1;
        this.bBp.j();
        hR(this.bBp.i());
        if (this.bBQ) {
            this.bBK.setVisibility(4);
        } else {
            if (this.bBL) {
                this.bBK.eF(this.bBM);
                this.bBK.setVisibility(0);
                this.bBK.jW(1);
                this.bBL = false;
            }
            if (this.bBN) {
                this.bBK.eF(this.bBM);
                this.bBK.setVisibility(0);
                this.bBK.jW(z2 ? 2 : 1);
                this.bBK.bN(z2 && z);
            }
        }
        this.bBp.refresh();
        this.bBp.g();
        this.bBp.a(z2 ? this.bBU : null);
        this.bBq = false;
    }

    public void bO(boolean z) {
        if (!z) {
            MenuLayout menuLayout = this.bBp;
            if (MenuLayout.F != null) {
                MenuLayout menuLayout2 = this.bBp;
                MenuLayout.F.bE(UCR.drawable.aVA);
                MenuLayout menuLayout3 = this.bBp;
                MenuLayout.F.setIcon(this.bBA);
                return;
            }
            return;
        }
        MenuLayout menuLayout4 = this.bBp;
        if (MenuLayout.F != null) {
            MenuLayout menuLayout5 = this.bBp;
            MenuLayout.F.bE(UCR.drawable.aNF);
            MenuLayout menuLayout6 = this.bBp;
            MenuLayout.F.setIcon(this.bBB);
        }
    }

    public void bP(boolean z) {
        if (!z) {
            MenuLayout menuLayout = this.bBp;
            if (MenuLayout.E != null) {
                MenuLayout menuLayout2 = this.bBp;
                MenuLayout.E.bE(UCR.drawable.aVv);
                MenuLayout menuLayout3 = this.bBp;
                MenuLayout.E.setIcon(this.bBC);
                return;
            }
            return;
        }
        MenuLayout menuLayout4 = this.bBp;
        if (MenuLayout.E != null) {
            MenuLayout menuLayout5 = this.bBp;
            MenuLayout.E.bE(UCR.drawable.aND);
            MenuLayout menuLayout6 = this.bBp;
            MenuLayout.E.setIcon(this.bBD);
        }
    }

    public void bQ(boolean z) {
        if (true == z) {
            MenuLayout menuLayout = this.bBp;
            if (MenuLayout.H != null) {
                MenuLayout menuLayout2 = this.bBp;
                MenuLayout.H.bE(UCR.drawable.aNE);
                MenuLayout menuLayout3 = this.bBp;
                MenuLayout.H.setIcon(this.bBH);
                MenuLayout menuLayout4 = this.bBp;
                MenuLayout.H.setText(this.bBJ);
                return;
            }
            return;
        }
        MenuLayout menuLayout5 = this.bBp;
        if (MenuLayout.H != null) {
            MenuLayout menuLayout6 = this.bBp;
            MenuLayout.H.bE(UCR.drawable.aNs);
            MenuLayout menuLayout7 = this.bBp;
            MenuLayout.H.setIcon(this.bBG);
            MenuLayout menuLayout8 = this.bBp;
            MenuLayout.H.setText(this.bBI);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            if (keyEvent.getKeyCode() == 4 && ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(117);
                return true;
            } else if (keyEvent.getKeyCode() == 82) {
                return true;
            } else {
                if (keyEvent.getKeyCode() == 84) {
                    if (ModelBrowser.gD() != null) {
                        ModelBrowser.gD().aS(120);
                    }
                    return true;
                }
            }
        } else if (ModelBrowser.gD() != null && 1 == keyEvent.getAction() && keyEvent.getKeyCode() == 82) {
            ModelBrowser.gD().aS(117);
        }
        return this.bBp.dispatchKeyEvent(keyEvent);
    }

    public void dr() {
        if (1 == getContext().getResources().getConfiguration().orientation) {
            MenuLayout.setOrientation(0);
        } else if (2 == getContext().getResources().getConfiguration().orientation) {
            MenuLayout.setOrientation(1);
        }
        if (this.bBr != null) {
            setContentView(this.bBr);
        }
    }

    public void ff(String str) {
        this.bBM = str;
    }

    public void g(boolean z, boolean z2) {
        if (z2) {
            MenuLayout menuLayout = this.bBp;
            if (MenuLayout.I != null) {
                MenuLayout menuLayout2 = this.bBp;
                MenuLayout.I.H(z);
                MenuLayout menuLayout3 = this.bBp;
                MenuLayout.I.setIcon(this.bBs);
                MenuLayout menuLayout4 = this.bBp;
                MenuLayout.I.setText(this.bBt);
                MenuLayout menuLayout5 = this.bBp;
                MenuLayout.I.bE(UCR.drawable.aNY);
                return;
            }
            return;
        }
        MenuLayout menuLayout6 = this.bBp;
        if (MenuLayout.I != null) {
            MenuLayout menuLayout7 = this.bBp;
            MenuLayout.I.H(z);
            MenuLayout menuLayout8 = this.bBp;
            MenuLayout.I.setIcon(this.bBu);
            MenuLayout menuLayout9 = this.bBp;
            MenuLayout.I.setText(this.bBv);
            MenuLayout menuLayout10 = this.bBp;
            MenuLayout.I.bE(UCR.drawable.aNP);
        }
    }

    public void hS(int i) {
        if (i == 0) {
            MenuLayout menuLayout = this.bBp;
            if (MenuLayout.P != null) {
                MenuLayout menuLayout2 = this.bBp;
                MenuLayout.P.setIcon(this.bBw);
                MenuLayout menuLayout3 = this.bBp;
                MenuLayout.P.setText(this.bBx);
                MenuLayout menuLayout4 = this.bBp;
                MenuLayout.P.bE(UCR.drawable.aNz);
                return;
            }
            return;
        }
        MenuLayout menuLayout5 = this.bBp;
        if (MenuLayout.P != null) {
            MenuLayout menuLayout6 = this.bBp;
            MenuLayout.P.setIcon(this.bBy);
            MenuLayout menuLayout7 = this.bBp;
            MenuLayout.P.setText(this.bBz);
            MenuLayout menuLayout8 = this.bBp;
            MenuLayout.P.bE(UCR.drawable.aNo);
        }
    }

    public void hide() {
        bM(this.bBR);
        this.bBR = true;
    }

    public void k() {
        this.bBs = e.Pb().getDrawable(UCR.drawable.aNY);
        this.bBu = e.Pb().getDrawable(UCR.drawable.aNP);
        this.bBw = e.Pb().getDrawable(UCR.drawable.aNz);
        this.bBy = e.Pb().getDrawable(UCR.drawable.aNo);
        this.bBA = e.Pb().getDrawable(UCR.drawable.aVA);
        this.bBB = e.Pb().getDrawable(UCR.drawable.aNF);
        this.bBC = e.Pb().getDrawable(UCR.drawable.aVv);
        this.bBD = e.Pb().getDrawable(UCR.drawable.aND);
        this.bBE = e.Pb().getDrawable(UCR.drawable.aUS);
        this.bBF = e.Pb().getDrawable(UCR.drawable.aUR);
        this.bBG = e.Pb().getDrawable(UCR.drawable.aNs);
        this.bBH = e.Pb().getDrawable(UCR.drawable.aNE);
        if (this.bBp != null) {
            this.bBp.k();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 82) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        ModelBrowser.gD().b(motionEvent);
        if (motionEvent.getAction() == 1) {
            hide();
        }
        return true;
    }

    public void q(String str, boolean z) {
        this.bBL = true;
        bN(z);
    }

    public void r(String str, boolean z) {
        this.bBN = true;
        bN(z);
    }

    public void show() {
        bN(true);
    }

    public void uj() {
    }

    public void uk() {
        this.bBO = true;
        if (!this.bBN || this.bBP) {
            this.bBO = false;
            super.hide();
            this.bBK.setVisibility(8);
        }
    }

    public void ul() {
        hide();
    }
}
