package com.uc.browser;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import b.a.a.a.f;
import b.a.a.a.k;
import b.a.a.a.u;
import b.b;
import com.uc.a.a;
import com.uc.browser.UCAlertDialog;
import com.uc.browser.UCR;
import com.uc.browser.UcContact;
import com.uc.browser.ViewZoomControls;
import com.uc.c.an;
import com.uc.e.a.q;
import com.uc.e.a.w;
import com.uc.e.ad;
import com.uc.e.b.d;
import com.uc.e.b.h;
import com.uc.e.b.j;
import com.uc.e.b.n;
import com.uc.h.e;
import com.uc.plugin.Plugin;
import com.uc.plugin.ag;
import java.io.File;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

public class WebViewJUC extends AbsoluteLayout implements ViewZoomControls.ZoomController {
    private static Drawable aEg = e.Pb().getDrawable(UCR.drawable.aWu);
    private static Drawable aEh = e.Pb().km(R.drawable.f644top_title_shadow);
    private static u ai = null;
    public static final int bCE = 1;
    public static final int bCF = 4;
    public static f ceU = null;
    public static f ceV = null;
    private static final int cfE = 1;
    private static final int cfF = 2;
    private static final int cfG = 3;
    private static final int cfH = 4;
    private static final int cfI = 5;
    private static final float cfN = 1.5f;
    private static final int cfO = 80;
    static final int cfR = 300;
    static final int cfS = 400;
    /* access modifiers changed from: private */
    public static int cfT = 0;
    public static int cff = 750;
    static final int cfj = 1201;
    private static UCTouchHandler cfl = null;
    private static float cfo = 0.01f;
    private static final byte cfq = 0;
    private static final byte cfr = 1;
    private static final byte cft = 1;
    private static final byte cfu = 2;
    private static final byte cfv = 3;
    private static final byte cfw = 4;
    private static final byte cfx = 7;
    private static final byte cfy = 8;
    private static final byte cfz = 11;
    public static final int cga = 4097;
    public static final int cgb = 4098;
    public static final int cgc = 4099;
    private static Drawable cgg;
    private static Drawable cgh;
    private static GradientDrawable cgi;
    private static GradientDrawable cgj;
    private static j cgk;
    private static int cgn = 120;
    private static int cgo = 60;
    private static Drawable cgp;
    private static Bitmap cgq;
    private static Bitmap cgr;
    Paint aBF = new Paint();
    private boolean aFL = true;
    private boolean aFM = false;
    private DrawWaitingPage aFP;
    protected a ag = null;
    private int ao = 0;
    private int ap = 0;
    public Timer aq = null;
    private int ar = -1;
    private int as = -1;
    Vector bBZ = new Vector();
    private ActivityBrowser bk;
    /* access modifiers changed from: private */
    public int ceW = 0;
    /* access modifiers changed from: private */
    public Vector ceX = null;
    private boolean[] ceY = null;
    private boolean ceZ = true;
    /* access modifiers changed from: private */
    public MotionEvent cfA;
    private long cfB;
    private long cfC;
    private int cfD = 1;
    private boolean cfJ;
    private int cfK;
    private int cfL;
    private int cfM;
    private float cfP;
    private float cfQ;
    private boolean cfU = false;
    private boolean cfV = false;
    private com.uc.e.a.a cfW;
    private boolean cfX = false;
    private int cfY = 30;
    private int cfZ = 30;
    public Timer cfa = null;
    public boolean cfb = true;
    VelocityTracker cfc = null;
    private boolean cfd = false;
    private int cfe = 0;
    private String cfg = null;
    private boolean cfh = false;
    private Picture cfi = null;
    private int cfk;
    float cfm;
    private float cfn;
    private byte cfp = 0;
    /* access modifiers changed from: private */
    public byte cfs = 7;
    private w[] cgd = {new w(cga, com.uc.b.a.ae.getResources().getString(R.string.f1535freecopy_menu_copy)), new w(cgc, com.uc.b.a.ae.getResources().getString(R.string.f1536freecopy_menu_search)), new w(cgb, com.uc.b.a.ae.getResources().getString(R.string.f1537freecopy_menu_share))};
    private int[] cge;
    private int[] cgf;
    private int cgl = -1;
    private float cgm = 1.0f;
    Handler mHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message message) {
            if (message.what == WebViewJUC.cfT && 4 == WebViewJUC.this.cfs) {
                WebViewJUC.this.ag.a(WebViewJUC.this.cfA);
                byte unused = WebViewJUC.this.cfs = (byte) 7;
                MotionEvent unused2 = WebViewJUC.this.cfA = (MotionEvent) null;
            }
        }
    };
    private int orientation;

    class ContactApp implements UcContact.CALLBACK {
        private ContactApp() {
        }

        /* access modifiers changed from: private */
        public void a(ContentResolver contentResolver) {
            try {
                new UcContact(contentResolver, this).rX();
            } catch (Exception e) {
            }
        }

        public void e(List list) {
            WebViewJUC.this.a(list);
        }
    }

    class Displayer extends View {
        public Displayer(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            WebViewJUC.this.onDraw(canvas);
        }

        public boolean onTouchEvent(MotionEvent motionEvent) {
            requestFocusFromTouch();
            return super.onTouchEvent(motionEvent);
        }
    }

    class createContextMenuTask extends TimerTask {
        LoadingProgressBar bru = null;

        public createContextMenuTask() {
        }

        public void run() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(61);
            }
            WebViewJUC.this.aF();
            byte unused = WebViewJUC.this.cfs = (byte) 7;
        }
    }

    class createRollPageTask extends TimerTask {
        createRollPageTask() {
        }

        public void run() {
            if (WebViewJUC.this.ag != null) {
                if (WebViewJUC.this.cfb) {
                    WebViewJUC.this.ag.h(25);
                } else if (!WebViewJUC.this.cfb) {
                    WebViewJUC.this.ag.h(-25);
                }
                WebViewJUC.this.gB();
            }
        }
    }

    static {
        Qp();
    }

    public WebViewJUC(Context context) {
        super(context);
        d(context);
    }

    public WebViewJUC(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        d(context);
    }

    private void QB() {
        if (this.cfW == null) {
            this.cfW = new com.uc.e.a.a();
        }
        e Pb = e.Pb();
        this.cfU = ActivityBrowser.Fz();
        Drawable[] drawableArr = {Pb.km(R.drawable.f575freemenu_top_left), Pb.km(R.drawable.f576freemenu_top_middle), Pb.km(R.drawable.f577freemenu_top_right)};
        Drawable[] drawableArr2 = {Pb.km(R.drawable.f572freemenu_bottom_left), Pb.km(R.drawable.f573freemenu_bottom_middle), Pb.km(R.drawable.f574freemenu_bottom_right)};
        n nVar = new n();
        n nVar2 = new n();
        nVar.i(drawableArr);
        nVar.kA(drawableArr[0].getIntrinsicWidth());
        nVar2.i(drawableArr2);
        nVar2.kA(drawableArr2[0].getIntrinsicWidth());
        this.cfW.c(nVar2);
        this.cfW.b(nVar);
        this.cfW.cY();
        this.cfW.a(this.cgd);
        this.cfW.setDirection(0);
        if (this.cfU) {
            this.cfW.setTextColor(-7168597);
            this.cfW.P(-8410658);
            this.cfW.d(new h(1.0f, -15456445, -15454637, -14203534, 570425344, 8.0f, 3.0f));
        } else {
            this.cfW.setTextColor(-1);
            this.cfW.P(-1);
            this.cfW.d(new h(1.0f, -16240553, -15309392, -14255413, 570425344, 8.0f, 3.0f));
        }
        this.cfW.setTextSize(getResources().getDimension(R.dimen.f407free_menu_text_size));
        this.cfW.a((int) getResources().getDimension(R.dimen.f408free_menu_item_padding_h), (int) getResources().getDimension(R.dimen.f409free_menu_item_padding_v), (int) getResources().getDimension(R.dimen.f408free_menu_item_padding_h), (int) getResources().getDimension(R.dimen.f409free_menu_item_padding_v));
        this.cfW.N(8);
        this.cfW.O(2);
        j jVar = new j(new int[]{-1795162112, 587202559}, true);
        jVar.hU(1);
        this.cfW.a(jVar);
        this.cfW.a(new q() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.uc.browser.ModelBrowser.a(int, int, java.lang.Object):void
             arg types: [int, int, boolean]
             candidates:
              com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object, int):void
              com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String, java.lang.String):void
              com.uc.browser.ModelBrowser.a(java.lang.String, java.util.HashMap, int):void
              com.uc.browser.ModelBrowser.a(int, int, long):void
              com.uc.browser.ModelBrowser.a(int, java.lang.String, boolean):void
              com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, java.lang.String, java.lang.String):void
              com.uc.browser.ModelBrowser.a(com.uc.browser.WebViewJUC, int, int):void
              com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, com.uc.browser.WindowUCWeb, int):void
              com.uc.browser.ModelBrowser.a(int, int, java.lang.Object):void */
            public void fJ(int i) {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(83, i, (Object) true);
                    WebViewJUC.this.QC();
                }
            }
        });
        this.cfW.a(new ad() {
            public void cJ() {
                WebViewJUC.this.invalidate();
            }
        });
    }

    private void QD() {
        if (this.cge != null && this.cgf != null) {
            int am = this.cge[1] - this.ag.am();
            int am2 = this.cgf[1] - this.ag.am();
            int al = this.cge[0] - this.ag.al();
            int al2 = this.cgf[0] - this.ag.al();
            int width = this.cfW.getWidth();
            int height = this.cfW.getHeight();
            int dimension = (int) getResources().getDimension(R.dimen.f410free_menu_margin);
            if ((am > getHeight() && am2 > getHeight()) || (am < 0 && this.cgf[2] + am2 < 0)) {
                this.cfZ = -1;
            } else if (am > height + dimension) {
                this.cfW.setDirection(0);
                this.cfZ = (am - dimension) - height;
            } else if (getHeight() - am2 > height + dimension + this.cgf[2]) {
                this.cfZ = am2 + dimension + this.cgf[2];
                this.cfW.setDirection(1);
            } else {
                this.cfW.setDirection(0);
                this.cfZ = (getHeight() - height) >> 1;
            }
            Drawable da = this.cfW.getDirection() == 1 ? this.cfW.da() : this.cfW.cZ();
            if (am != am2) {
                this.cfY = (getWidth() - width) >> 1;
                if (da instanceof n) {
                    ((n) da).kB((this.cfW.getWidth() - ((n) da).PN()) / 2);
                }
            } else if (al2 - al > width) {
                this.cfY = ((al2 + al) - width) >> 1;
                if (da instanceof n) {
                    ((n) da).kB((width - ((n) da).PN()) / 2);
                }
            } else if (da instanceof n) {
                n nVar = (n) da;
                int i = (al + al2) >> 1;
                if (i >= width / 2 && getWidth() - i >= width / 2) {
                    this.cfY = i - (width / 2);
                    nVar.kB((width - nVar.PN()) / 2);
                } else if (i < width / 2) {
                    this.cfY = 0;
                    nVar.kB(i - (nVar.PN() / 2));
                } else if (getWidth() - i < width / 2) {
                    nVar.kC((getWidth() - i) - (nVar.PN() / 2));
                    this.cfY = getWidth() - width;
                }
            }
        }
    }

    private static void QH() {
        int[] iArr;
        boolean Fz = ActivityBrowser.Fz();
        e Pb = e.Pb();
        if (Fz) {
            cgg = Pb.km(R.drawable.f641start_night);
            cgh = Pb.km(R.drawable.f557end_night);
            cgi = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{-2012192352, 1073568});
            cgj = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[]{-2012192352, 1073568});
            iArr = new int[]{-13335632, -14720622, -15908747};
        } else {
            cgg = Pb.km(R.drawable.f640start);
            cgh = Pb.km(R.drawable.f556end);
            cgi = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{-2011331114, 1934806});
            cgj = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[]{-2011331114, 1934806});
            iArr = new int[]{-15887393, -16288047, -16623932};
        }
        cgk = new j(iArr);
        cgk.hU(1);
        cgq = Pb.fZ("manifier_mask").extractAlpha();
        Bitmap fZ = Pb.fZ("manifier");
        cgp = new BitmapDrawable(fZ);
        cgn = fZ.getWidth();
        cgo = fZ.getHeight();
    }

    private static void Qp() {
        try {
            cfl = new MultiTouchHandler();
        } catch (NoSuchMethodError | VerifyError e) {
        }
    }

    private void a(float f, float f2, float f3, boolean z) {
        if (this.ag != null) {
            this.ag.a(f, f2, f3, z);
            postInvalidate();
        }
    }

    private void a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, Canvas canvas) {
        Rect[] rectArr;
        cgk.setBounds(i - (i7 / 2), i2, (i - (i7 / 2)) + i7, i2 + i5);
        cgk.draw(canvas);
        cgi.setBounds((i - (i7 / 2)) + i7, i2, (i - (i7 / 2)) + i7 + i8, i2 + i5);
        cgi.draw(canvas);
        cgk.setBounds(i3 - (i7 / 2), i4, (i3 - (i7 / 2)) + i7, i4 + i6);
        cgk.draw(canvas);
        cgj.setBounds((i3 - (i7 / 2)) - i8, i4, i3 - (i7 / 2), i4 + i6);
        cgj.draw(canvas);
        int intrinsicWidth = i - ((cgg.getIntrinsicWidth() - i7) / 2);
        int intrinsicHeight = (i2 + i9) - cgg.getIntrinsicHeight();
        cgg.setBounds(intrinsicWidth, intrinsicHeight, cgg.getIntrinsicWidth() + intrinsicWidth, cgg.getIntrinsicHeight() + intrinsicHeight);
        cgg.draw(canvas);
        int intrinsicWidth2 = i3 - ((cgh.getIntrinsicWidth() - i7) / 2);
        int i11 = i4 + i10 + i6;
        cgh.setBounds(intrinsicWidth2, i11, cgg.getIntrinsicWidth() + intrinsicWidth2, cgg.getIntrinsicHeight() + i11);
        cgh.draw(canvas);
        if (i2 + i5 >= i4) {
            Rect[] rectArr2 = new Rect[1];
            rectArr2[0] = new Rect(i, i2, i3, i4 + i6 > i2 + i5 ? i4 + i6 : i2 + i5);
            rectArr = rectArr2;
        } else {
            rectArr = new Rect[]{new Rect(i, i2, getWidth(), i2 + i5), new Rect(0, i2 + i5, getWidth(), i4), new Rect(0, i2 + i5, i3, i4 + i6)};
        }
        new d(rectArr, 0, 0, 1, 855928798).draw(canvas);
    }

    private void a(int i, int i2, int i3, int i4, int i5, int i6, Bitmap bitmap, Canvas canvas) {
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        if (cgq == null || cgq.isRecycled()) {
            cgq = e.Pb().fZ("manifier_mask").extractAlpha();
        }
        int i12 = (int) (((float) cgn) / this.cgm);
        int i13 = (int) (((float) cgo) / this.cgm);
        if (this.cgl == 0) {
            i9 = i - (i12 / 2);
            i8 = i2 - ((i13 - i3) / 2);
            i7 = (int) getResources().getDimension(R.dimen.f414manifier_margin_start);
        } else if (this.cgl == 1) {
            i9 = i4 - (i12 / 2);
            i8 = i5 - ((i13 - i6) / 2);
            i7 = (int) getResources().getDimension(R.dimen.f415manifier_margin_end);
        } else {
            i7 = 0;
            i8 = 0;
            i9 = 0;
        }
        int i14 = (i8 - (cgo / 2)) - i7;
        if (i14 < 0) {
            int i15 = (i9 - (i12 / 2)) - i7;
            if (i15 < 0) {
                i10 = i12 + (i7 * 2) + i15;
                i11 = 0;
            } else {
                i10 = i15;
                i11 = 0;
            }
        } else {
            i10 = i9;
            i11 = i14;
        }
        if (i9 < 0) {
            i10 = 0;
        } else if (cgn + i9 > getWidth()) {
            i10 = getWidth() - cgn;
        }
        Paint paint = new Paint();
        BitmapShader bitmapShader = new BitmapShader(ceU.FQ, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        Matrix matrix = new Matrix();
        bitmapShader.getLocalMatrix(matrix);
        matrix.postTranslate((float) (-i9), (float) (-i8));
        bitmapShader.setLocalMatrix(matrix);
        paint.setShader(bitmapShader);
        canvas.drawBitmap(cgq, (float) i10, (float) i11, paint);
        cgq.getHeight();
        paint.setShader(new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
        canvas.drawBitmap(cgq, (float) i10, (float) i11, paint);
        cgp.setBounds(i10, i11, cgn + i10, cgo + i11);
        cgp.draw(canvas);
    }

    private float ab() {
        if (this.ag == null) {
            return 0.0f;
        }
        return this.ag.ab();
    }

    private boolean bH(int i, int i2) {
        return i >= this.cfY && i <= this.cfY + this.cfW.getWidth() && i2 >= this.cfZ && i2 <= this.cfZ + this.cfW.getHeight();
    }

    private void d(Context context) {
        this.bk = (ActivityBrowser) context;
        this.orientation = getContext().getResources().getConfiguration().orientation;
        bE(this.bk.getWindowManager().getDefaultDisplay().getWidth(), this.bk.getWindowManager().getDefaultDisplay().getHeight());
        aT();
        Displayer displayer = new Displayer(context);
        displayer.setId(cfj);
        displayer.setLayoutParams(new AbsoluteLayout.LayoutParams(-1, -1, 0, 0));
        addView(displayer);
        displayer.setFocusableInTouchMode(true);
        this.cfL = ViewConfiguration.get(this.bk).getScaledTouchSlop();
        this.cfK = this.cfL * this.cfL;
        this.cfM = (int) (((float) this.cfL) * 0.4f);
        QB();
        QH();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.WebViewJUC.a(float, float, float, boolean):void
     arg types: [float, float, float, int]
     candidates:
      com.uc.browser.WebViewJUC.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String):void
      com.uc.browser.WebViewJUC.a(float, float, float, boolean):void */
    private boolean e(MotionEvent motionEvent) {
        if (cfl != null && ac()) {
            switch (cfl.c(motionEvent)) {
                case 1:
                    aF();
                    ModelBrowser gD = ModelBrowser.gD();
                    if (gD != null) {
                        gD.a(106, b.ack, this);
                    }
                    PointF[] ur = cfl.ur();
                    float f = ur[0].x - ur[1].x;
                    float f2 = ur[0].y - ur[1].y;
                    this.cfm = FloatMath.sqrt((f2 * f2) + (f * f));
                    return true;
                case 2:
                    PointF[] ur2 = cfl.ur();
                    float f3 = ur2[0].x - ur2[1].x;
                    float f4 = ur2[0].y - ur2[1].y;
                    float sqrt = FloatMath.sqrt((f3 * f3) + (f4 * f4));
                    this.cfn = ab();
                    float f5 = (this.cfn * sqrt) / this.cfm;
                    if (Math.abs(f5 - this.cfn) < cfo) {
                        return true;
                    }
                    a(f5, (ur2[0].x + ur2[1].x) / 2.0f, (ur2[1].y + ur2[0].y) / 2.0f, false);
                    this.cfm = sqrt;
                    this.cfn = f5;
                    return true;
            }
        }
        return false;
    }

    private String h(String str) {
        int indexOf = str.toLowerCase().indexOf("http");
        if (indexOf == 0) {
            return str;
        }
        if (indexOf > 0) {
            int indexOf2 = str.indexOf(46);
            return indexOf < indexOf2 ? new String(str.substring(indexOf)) : str.indexOf(58) < indexOf2 ? new String(str.substring(str.lastIndexOf(58, indexOf2) + 1)) : str;
        } else if (indexOf != -1) {
            return str;
        } else {
            int indexOf3 = str.indexOf(46);
            return str.indexOf(58) < indexOf3 ? new String(str.substring(str.lastIndexOf(58, indexOf3) + 1)) : str;
        }
    }

    public void A(Vector vector) {
        this.ag = (a) vector.get(0);
    }

    public boolean A() {
        if (this.ag == null) {
            return false;
        }
        return this.ag.A();
    }

    public void B(Vector vector) {
        Dialog C = C(vector);
        if (C != null) {
            C.show();
        }
    }

    public boolean B() {
        if (this.ag == null) {
            return false;
        }
        return this.ag.B();
    }

    public int C() {
        if (this.ag == null) {
            return 0;
        }
        return this.ag.C();
    }

    public Dialog C(Vector vector) {
        if (vector == null || vector.size() <= 0) {
            return null;
        }
        if (this.ceX != null && this.ceX.size() > 0) {
            this.ceX.clear();
        }
        this.ceX = vector;
        this.ceW = 0;
        String[] strArr = new String[vector.size()];
        for (int i = 0; i < vector.size(); i++) {
            Object[] objArr = (Object[]) vector.get(i);
            if (objArr.length > 2) {
                if (objArr[0] != null) {
                    strArr[i] = objArr[0].toString();
                } else {
                    strArr[i] = "";
                }
                int[] iArr = (int[]) objArr[2];
                if (iArr != null && iArr.length > 0 && 1 == iArr[0]) {
                    this.ceW = i;
                }
            }
        }
        return new UCAlertDialog.Builder(this.bk).aH(R.string.f1191dialog_title_please_select).a(strArr, this.ceW, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                int unused = WebViewJUC.this.ceW = i;
            }
        }).a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.uc.a.a.a(int, java.util.Vector, boolean):void
             arg types: [int, java.util.Vector, int]
             candidates:
              com.uc.a.a.a(int, float, float):void
              com.uc.a.a.a(java.lang.String, int, boolean):void
              com.uc.a.a.a(int, java.util.Vector, boolean):void */
            public void onClick(DialogInterface dialogInterface, int i) {
                if (WebViewJUC.this.ag != null) {
                    WebViewJUC.this.ag.a(WebViewJUC.this.ceW, WebViewJUC.this.ceX, false);
                    WebViewJUC.this.ag.ap();
                }
            }
        }).c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                WebViewJUC.this.ag.ap();
            }
        }).fh();
    }

    public String D() {
        if (this.ag == null) {
            return null;
        }
        return this.ag.D();
    }

    public void D(Vector vector) {
        Dialog E = E(vector);
        if (E != null) {
            E.show();
        }
    }

    public int E() {
        if (this.ag == null) {
            return 0;
        }
        return this.ag.E();
    }

    public Dialog E(Vector vector) {
        if (vector == null || vector.size() <= 0) {
            return null;
        }
        if (this.ceX != null && this.ceX.size() > 0) {
            this.ceX.clear();
        }
        this.ceX = vector;
        this.ceY = null;
        this.ceY = new boolean[this.ceX.size()];
        String[] strArr = new String[vector.size()];
        for (int i = 0; i < this.ceX.size(); i++) {
            Object[] objArr = (Object[]) this.ceX.get(i);
            if (objArr.length > 2) {
                int[] iArr = (int[]) objArr[2];
                if (iArr == null || iArr.length <= 0 || 1 != iArr[0]) {
                    this.ceY[i] = false;
                } else {
                    this.ceY[i] = true;
                }
                strArr[i] = objArr[0].toString();
            }
        }
        return new UCAlertDialog.Builder(this.bk).L("请选择").a(strArr, this.ceY, new DialogInterface.OnMultiChoiceClickListener() {
            public void onClick(DialogInterface dialogInterface, int i, boolean z) {
                int i2 = 1;
                if (i >= 0 && i < WebViewJUC.this.ceX.size()) {
                    Object[] objArr = (Object[]) WebViewJUC.this.ceX.get(i);
                    int[] iArr = new int[1];
                    if (!z) {
                        i2 = 0;
                    }
                    iArr[0] = i2;
                    objArr[2] = iArr;
                }
            }
        }).a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.uc.a.a.a(int, java.util.Vector, boolean):void
             arg types: [int, java.util.Vector, int]
             candidates:
              com.uc.a.a.a(int, float, float):void
              com.uc.a.a.a(java.lang.String, int, boolean):void
              com.uc.a.a.a(int, java.util.Vector, boolean):void */
            public void onClick(DialogInterface dialogInterface, int i) {
                if (WebViewJUC.this.ag != null) {
                    WebViewJUC.this.ag.a(0, WebViewJUC.this.ceX, true);
                    WebViewJUC.this.ag.ap();
                }
            }
        }).c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                WebViewJUC.this.ag.ap();
            }
        }).fh();
    }

    public void EG() {
        if (!this.cfV) {
            if (this.cfU != ActivityBrowser.Fz()) {
                QB();
                QH();
            }
            this.cfV = true;
            invalidate();
        }
    }

    public void Ez() {
        Iterator it = this.bBZ.iterator();
        while (it.hasNext()) {
            Plugin plugin = (Plugin) it.next();
            if (plugin != null) {
                plugin.d("REMOVE", null, null);
                ag Qh = plugin.Qh();
                if (Qh != null) {
                    plugin.d("BACKGROUND", null, null);
                    removeView(Qh);
                }
            }
        }
        this.bBZ.removeAllElements();
    }

    public String F() {
        if (this.ag == null) {
            return null;
        }
        return this.ag.F();
    }

    public int G() {
        if (this.ag != null) {
            return this.ag.G();
        }
        return 32767;
    }

    public void H() {
        if (this.ag != null) {
            this.ag.H();
        }
    }

    public void I() {
        if (this.ag != null) {
            this.ag.I();
        }
    }

    public void J() {
        if (this.ag != null) {
            this.ag.J();
        }
    }

    public void K() {
        if (this.ag != null) {
            this.ag.K();
        }
    }

    public void L() {
        if (this.ag != null) {
            this.ag.L();
        }
    }

    public boolean M() {
        if (this.ag != null) {
            return this.ag.M();
        }
        return false;
    }

    public int N() {
        if (this.ag != null) {
            return this.ag.N();
        }
        return -1;
    }

    public boolean O() {
        if (this.ag == null) {
            return false;
        }
        return this.ag.O();
    }

    public void P() {
        if (this.ag != null) {
            this.ag.P();
        }
    }

    public void Q() {
        if (this.ag != null) {
            this.ag.Q();
        }
    }

    public void QA() {
        if (this.ag != null) {
            com.uc.a.e.nR().a(this.ag);
        }
    }

    public void QC() {
        if (this.cfV) {
            this.cfV = false;
            invalidate();
        }
        this.cfX = false;
    }

    public void QE() {
        this.cgl = 0;
    }

    public void QF() {
        this.cgl = 1;
    }

    public void QG() {
        this.cgl = -1;
        if (cgq != null) {
            cgq.recycle();
            cgq = null;
        }
        if (cgr != null) {
            cgr.recycle();
            cgr = null;
        }
    }

    public int Qk() {
        return this.ao;
    }

    public int Ql() {
        return this.ap;
    }

    public boolean Qm() {
        if (this.ag == null) {
            return false;
        }
        return this.ag.r();
    }

    public int Qn() {
        return this.ao;
    }

    public int Qo() {
        return this.ap;
    }

    /* access modifiers changed from: package-private */
    public void Qq() {
        this.cfs = 7;
    }

    public void Qr() {
        this.cfs = 7;
    }

    public void Qs() {
        if (this.ag != null) {
            com.uc.a.e.nR().nW().b(this.ag);
        }
    }

    public void Qt() {
        Dialog Qu = Qu();
        if (Qu != null) {
            Qu.show();
        }
    }

    public Dialog Qu() {
        if (this.ceX != null && this.ceX.size() > 0) {
            this.ceX.clear();
        }
        this.ceW = 0;
        return new UCAlertDialog.Builder(this.bk).aH(R.string.f1373pictureview).a((int) R.array.f57view_image, this.ceW, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                int unused = WebViewJUC.this.ceW = i;
            }
        }).a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (WebViewJUC.this.ag != null) {
                    WebViewJUC.this.ag.b(WebViewJUC.this.ceW);
                }
            }
        }).c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                WebViewJUC.this.ag.ap();
            }
        }).fh();
    }

    public void Qv() {
        Qs();
        if (this.ceX != null) {
            this.ceX.clear();
            this.ceX = null;
        }
        if (this.aFP != null) {
            this.aFP.FB();
            this.aFP = null;
        }
        ViewGroup viewGroup = (ViewGroup) getParent();
        if (viewGroup != null) {
            viewGroup.removeView(this);
        }
        this.ceY = null;
    }

    public void Qw() {
        com.uc.a.e.nR().c((f) null);
        postInvalidate();
    }

    public void Qx() {
        if (this.ag != null) {
            this.ag.e(getOrientation());
        }
    }

    public void Qy() {
        if (this.cfa == null) {
            this.cfa = new Timer();
            if (this.cfa != null) {
                this.cfa.schedule(new createRollPageTask(), 0, 500);
            }
        }
    }

    public void Qz() {
        Iterator it = this.bBZ.iterator();
        while (it.hasNext()) {
            Plugin plugin = (Plugin) it.next();
            if (plugin.Qh() != null) {
                plugin.d("BACKGROUND", null, null);
            }
        }
    }

    public boolean U() {
        if (this.ag == null) {
            return true;
        }
        return this.ag.U();
    }

    public boolean V() {
        if (this.ag == null) {
            return false;
        }
        return this.ag.V();
    }

    public void W() {
        if (this.ag != null) {
            this.ag.W();
        }
    }

    public boolean X() {
        if (this.ag == null) {
            return false;
        }
        return this.ag.X();
    }

    public void Y() {
        if (this.ag != null) {
            this.ag.Y();
        }
    }

    public Bitmap a(float f) {
        if (this.ag == null) {
            return null;
        }
        bE(this.ao, this.ap);
        return this.ag.a(f);
    }

    public void a(int i, float f, float f2) {
        if (this.ag != null) {
            this.ag.a(i, f, f2);
            postInvalidate();
        }
    }

    public void a(int i, int i2, int i3, int i4, int i5, int i6, Canvas canvas) {
        if (this.ag != null) {
            if (this.cfU != ActivityBrowser.Fz()) {
                QH();
            }
            int dimension = (int) getResources().getDimension(R.dimen.f411free_copy_shadow_width);
            int dimension2 = (int) getResources().getDimension(R.dimen.f412free_copy_lollipop_margin_top);
            int dimension3 = (int) getResources().getDimension(R.dimen.f413free_copy_lollipop_margin_bottom);
            int am = i2 - this.ag.am();
            int am2 = i4 - this.ag.am();
            int al = i - this.ag.al();
            int al2 = i3 - this.ag.al();
            a(al, am, al2, am2, i5, i6, 3, dimension, dimension2, dimension3, canvas);
            if (ceU != null && ceU.FQ != null && this.cgl >= 0) {
                if (cgr == null || cgr.isRecycled()) {
                    cgr = Bitmap.createBitmap(cgn, cgo, Bitmap.Config.ARGB_8888);
                }
                cgr.eraseColor(0);
                Canvas canvas2 = new Canvas();
                canvas2.setBitmap(cgr);
                if (this.cgl == 0) {
                    int i7 = (cgo - i5) / 2;
                    int i8 = cgn / 2;
                    a(i8, i7, (al2 - al) + i8, (am2 - am) + i7, i5, i6, 3, dimension, dimension2, dimension3, canvas2);
                } else if (this.cgl == 1) {
                    int i9 = (cgo - i6) / 2;
                    int i10 = cgn / 2;
                    a((al - al2) + i10, (am - am2) + i9, i10, i9, i5, i6, 3, dimension, dimension2, dimension3, canvas2);
                }
                a(al, am, i5, al2, am2, i6, cgr, canvas);
            }
        }
    }

    public void a(Bitmap bitmap) {
        if (this.ag != null) {
            this.ag.a(bitmap);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.WebViewJUC.a(android.graphics.Canvas, boolean, boolean):void
     arg types: [android.graphics.Canvas, boolean, int]
     candidates:
      com.uc.browser.WebViewJUC.a(int, float, float):void
      com.uc.browser.WebViewJUC.a(android.graphics.Canvas, boolean, boolean):void */
    public void a(Canvas canvas, boolean z) {
        a(canvas, z, true);
    }

    public void a(Canvas canvas, boolean z, boolean z2) {
        int save = canvas.save();
        try {
            if (true == this.aFL) {
                b(canvas, z, z2);
                if (ActivityBrowser.Fz() && ac()) {
                    canvas.drawColor(-2013265920);
                }
                if (this.aFP != null) {
                    this.aFP.FB();
                    this.aFP = null;
                }
                if (true == this.aFM) {
                    this.aFM = false;
                }
                if (this.ag != null) {
                    int am = this.ag.am();
                    if (am != this.cfk && z2) {
                        bF(this.cfk, am);
                    }
                    this.cfk = am;
                }
            } else {
                if (!this.aFM) {
                    this.bk.t(this.bk.getString(R.string.f1305page_loading));
                    this.aFM = true;
                }
                if (this.aFP == null) {
                    this.aFP = new DrawWaitingPage(this, true, (this.cfg == null || !this.cfg.equals("ext:update_sharepage")) ? null : getResources().getString(R.string.f1449sharepage_update_tips));
                }
                if (true == this.aFP.zH()) {
                    this.aFP.j(canvas);
                } else {
                    if (this.cfg != null && this.cfg.equals("ext:update_sharepage")) {
                        this.aFP.eG(getResources().getString(R.string.f1450sharepage_update_failure));
                    }
                    this.aFP.k(canvas);
                }
                com.uc.a.e.nR().c((f) null);
            }
        } catch (Throwable th) {
            postInvalidate();
        }
        canvas.restoreToCount(save);
        l(canvas);
    }

    public void a(com.uc.a.n nVar) {
        if (this.ag != null) {
            this.ag.a(nVar);
        }
    }

    public void a(Plugin plugin) {
        if (!this.bBZ.contains(plugin)) {
            this.bBZ.add(plugin);
        }
    }

    public void a(Plugin plugin, int i, int i2, int i3, int i4) {
        ag Qh;
        if (!Looper.getMainLooper().getThread().equals(Thread.currentThread()) || plugin == null || !this.bBZ.contains(plugin) || (Qh = plugin.Qh()) == null) {
            return;
        }
        if (i3 < 0) {
            Qh.setVisibility(4);
            return;
        }
        if (Qh.getParent() == null) {
            addView(Qh);
        }
        Qh.setVisibility(0);
        Qh.setLayoutParams(new AbsoluteLayout.LayoutParams(i3, i4, i, i2));
    }

    public void a(File file) {
        if (this.ag != null) {
            this.ag.a(file);
        }
    }

    public void a(Object obj) {
        if (this.ag != null) {
            this.ag.a(obj);
        }
    }

    public void a(String str) {
        if (this.ag != null) {
            this.ag.a(str);
        }
    }

    public void a(String str, String str2) {
        if (this.ag != null) {
            this.ag.a(str, str2);
        }
    }

    public void a(String str, String str2, String str3, String str4) {
        if (this.ag != null) {
            this.ag.a(str, str2, str3, str4);
        }
    }

    public void a(List list) {
        if (this.ag != null) {
            this.ag.a(list);
        }
    }

    public void a(boolean z) {
        if (this.ag != null) {
            this.ag.a(z);
        }
    }

    public void a(byte[] bArr) {
        if (this.ag != null) {
            this.ag.a(bArr);
        }
    }

    public boolean a(File file, byte b2) {
        if (this.ag == null) {
            return false;
        }
        return this.ag.a(file, b2);
    }

    public byte[] a(InputStream inputStream) {
        if (this.ag != null) {
            return this.ag.a(inputStream);
        }
        return null;
    }

    public void aE() {
        if (this.aq == null) {
            this.aq = new Timer();
            if (this.aq != null) {
                this.aq.schedule(new createContextMenuTask(), (long) cff, (long) cff);
            }
        }
    }

    public void aF() {
        if (this.aq != null) {
            this.aq.cancel();
        }
        this.aq = null;
    }

    public void aT() {
        if (this.ag != null) {
            if (true == this.ag.c(getOrientation())) {
                postInvalidate();
            }
        }
    }

    public boolean aa() {
        if (this.ag == null) {
            return false;
        }
        return this.ag.aa();
    }

    public boolean ac() {
        if (this.ag == null) {
            return false;
        }
        return this.ag.ac();
    }

    public boolean ad() {
        if (this.ag == null) {
            return false;
        }
        return this.ag.ad();
    }

    public boolean ae() {
        if (this.ag == null) {
            return false;
        }
        return this.ag.ae();
    }

    public void af() {
        if (this.ag != null) {
            this.ag.af();
        }
    }

    public void ag() {
        if (this.ag != null) {
            this.ag.ag();
        }
    }

    public int ah() {
        if (this.ag == null) {
            return 4;
        }
        return this.ag.ah();
    }

    public Bitmap ai() {
        if (this.ag == null) {
            return null;
        }
        return this.ag.ai();
    }

    public boolean aj() {
        if (this.ag == null) {
            return false;
        }
        return this.ag.aj();
    }

    public void ak() {
        if (this.ag != null) {
            this.ag.ak();
        }
    }

    public boolean ao() {
        if (this.ag == null) {
            return false;
        }
        return this.ag.ao();
    }

    public void ap() {
        if (this.ag != null) {
            this.ag.ap();
        }
    }

    public boolean ar() {
        if (this.ag == null) {
            return false;
        }
        return this.ag.ar();
    }

    public boolean as() {
        if (this.ag == null) {
            return false;
        }
        return this.ag.as();
    }

    public boolean at() {
        if (this.ag == null) {
            return true;
        }
        return this.ag.at();
    }

    public boolean au() {
        if (this.ag == null) {
            return false;
        }
        return this.ag.au();
    }

    public boolean av() {
        if (this.ag == null) {
            return false;
        }
        return this.ag.av();
    }

    public void aw() {
        if (this.ag != null) {
            this.ag.aw();
        }
    }

    public void b(ContentResolver contentResolver) {
        try {
            new ContactApp().a(contentResolver);
        } catch (Exception e) {
        }
    }

    public void b(Canvas canvas, boolean z, boolean z2) {
        boolean z3;
        boolean z4;
        System.currentTimeMillis();
        try {
            if (this.ao > 0 && this.ap > 0) {
                u uVar = new u(k.uD(), canvas);
                if (ceU == null || ceU.getHeight() != this.ap) {
                    if (ceU != null) {
                        ceU.recycle();
                    }
                    ceU = f.J(this.ao, this.ap);
                    z3 = true;
                } else {
                    z3 = false;
                }
                boolean z5 = !z2 ? true : z3;
                ai = ceU.jU();
                if (this instanceof WebViewJUCMainpage) {
                    this.cfi = new Picture();
                    Rect clipBounds = canvas.getClipBounds();
                    ai.bmb = this.cfi.beginRecording(clipBounds.width(), clipBounds.height());
                    ai.bmc = canvas;
                } else if (!z2) {
                    ai.bmb = canvas;
                }
                this.orientation = getContext().getResources().getConfiguration().orientation;
                boolean ar2 = ar();
                if (this.ag != null) {
                    ai.save();
                    z4 = this.ag.a(ai, this.ao, this.ap, z5, z);
                    ai.restore();
                } else {
                    z4 = false;
                }
                if (z4) {
                    postInvalidate();
                }
                if (this instanceof WebViewJUCMainpage) {
                    if (this.cfi != null) {
                        this.cfi.endRecording();
                        this.cfi.draw(canvas);
                    }
                } else if (z2) {
                    uVar.a(ceU, 0, 0, 20);
                }
                if (z) {
                    cQ(ar2);
                }
                if (!(!com.uc.a.e.nR().qO() || this.cge == null || this.cgf == null)) {
                    a(this.cge[0], this.cge[1], this.cgf[0], this.cgf[1], this.cge[2], this.cgf[2], canvas);
                }
                if (this.cfV) {
                    QD();
                    if (this.cfZ >= 0) {
                        canvas.save();
                        canvas.translate((float) this.cfY, (float) this.cfZ);
                        this.cfW.draw(canvas);
                        canvas.restore();
                    }
                }
            }
        } catch (Throwable th) {
        }
    }

    public void b(String str) {
        if (this.ag != null) {
            this.ag.b(str);
        }
    }

    public void b(String str, String str2) {
        if (this.ag != null) {
            this.ag.b(str, str2);
        }
    }

    public boolean b(String str, String str2, com.uc.a.n nVar, boolean z, String str3) {
        if (this.ag == null) {
            this.ag = com.uc.a.e.nR().nW().c(str, nVar);
        }
        if (this.ag == null) {
            return false;
        }
        boolean a2 = this.ag.a(str, str2, nVar, z, str3);
        Qx();
        return a2;
    }

    public boolean bE(int i, int i2) {
        if (i == this.ao && i2 == this.ap) {
            return false;
        }
        if (i > 0 && i2 > 0) {
            this.ao = i;
            this.ap = i2;
        }
        this.orientation = getContext().getResources().getConfiguration().orientation;
        com.uc.a.e.nR().b(i, i2);
        return true;
    }

    /* access modifiers changed from: protected */
    public void bF(int i, int i2) {
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().y((this.ag.an() - getHeight()) - i2, 0);
        }
    }

    public void bG(int i, int i2) {
        if (this.ag != null) {
            com.uc.a.e.nR().a((Picture) null);
            if (true == this.ag.a(i, i2)) {
                postInvalidate();
            }
        }
    }

    public void bh(boolean z) {
        this.aFL = z;
        if (true == z && this.aFP != null) {
            this.aFP.FB();
            this.aFP = null;
        }
        this.aFM = false;
    }

    public void c(String str) {
        if (this.ag != null) {
            this.ag.c(str);
        }
    }

    public void cQ(boolean z) {
        if (this instanceof WebViewJUCMainpage) {
            boolean aq2 = this.ag.aq();
            if (ac() || aq2 || this.cfi == null) {
                com.uc.a.e.nR().a((Picture) null);
                return;
            }
            try {
                com.uc.a.e.nR().a(this.cfi);
                ag();
            } catch (Exception e) {
            }
        } else if (ac() || this.cfe < 99 || z || !at()) {
            com.uc.a.e.nR().c((f) null);
        } else {
            try {
                if (ceV != null && ceV.getWidth() == ceU.getWidth() && ceV.getHeight() == ceU.getHeight()) {
                    new Canvas(ceV.FQ).drawBitmap(ceU.FQ, 0.0f, 0.0f, this.aBF);
                } else {
                    if (ceV != null) {
                        ceV.recycle();
                        ceV = null;
                    }
                    ceV = f.a(ceU, 0, 0, this.ao, this.ap, 0);
                }
                com.uc.a.e.nR().c(ceV);
                af();
            } catch (Throwable th) {
            }
        }
    }

    public void cR(boolean z) {
        this.ceZ = z;
    }

    public void d(int i) {
        if (this.ag != null) {
            this.ag.d(i);
            Qw();
        }
    }

    public void d(String str, com.uc.a.n nVar) {
        String str2;
        boolean z;
        String str3;
        boolean z2;
        if (str != null) {
            if (!str.startsWith("sms:")) {
                Ez();
            }
            this.cfg = str;
            String substring = (str.startsWith(ModelBrowser.wU) || str.startsWith(ModelBrowser.wW) || str.startsWith(ModelBrowser.wX)) ? str.substring(str.indexOf(47) + 1) : str;
            int i = -1;
            if (substring.startsWith("wap:")) {
                i = 1;
                str2 = substring.substring(4);
                z = true;
            } else {
                str2 = substring;
                z = false;
            }
            if (str2.startsWith("forceusejuc:")) {
                str3 = str2.substring(12);
                z2 = true;
            } else {
                str3 = str2;
                z2 = false;
            }
            if (str3 == null || !str3.equals("ext:null") || this.ag == null) {
                String oN = com.uc.a.e.nR().nZ().oN();
                String oO = com.uc.a.e.nR().nZ().oO();
                if (this.ag != null) {
                    a(nVar);
                    P();
                    if (str3.startsWith("ext:download")) {
                        u();
                    } else {
                        this.ag.a(str3, i, z);
                    }
                } else if (oN != null && str3.startsWith(oN)) {
                    this.ag = com.uc.a.e.nR().nW().b(null, nVar, i, z);
                    a(nVar);
                    this.ag.a(str3, i, z);
                } else if (oO != null && str3 != null && str3.startsWith(oO)) {
                    this.ag = com.uc.a.e.nR().nW().b(null, nVar, i, z);
                    a(nVar);
                    this.ag.a(str3, i, z);
                } else if (str3.startsWith(b.acw)) {
                    this.ag = com.uc.a.e.nR().nW().b(null, nVar, i, z);
                    a(nVar);
                    this.ag.a(str3, i, z);
                } else if (str3.startsWith("ext:openwindowbackground")) {
                    this.ag = com.uc.a.e.nR().nW().b(str3, nVar, i, z);
                    a(nVar);
                } else if (str3 != null && str3.equals("ext:null")) {
                    this.ag = com.uc.a.e.nR().nW().b(null, nVar, i, z);
                    a(nVar);
                } else if (str3 != null && (str3.equals(com.uc.c.w.Pt) || str3.startsWith("viewmaincontent:"))) {
                    this.ag = com.uc.a.e.nR().nW().b(null, nVar, i, z);
                    a(nVar);
                    this.ag.a(str3, i, z);
                } else if (str3 != null && str3.equals(b.acz)) {
                    this.ag = com.uc.a.e.nR().nW().b(null, nVar, i, z);
                    a(nVar);
                    this.ag.a(str3, i, z);
                } else if (str3.equals("ext:update_sharepage")) {
                    this.ag = com.uc.a.e.nR().nW().b(null, nVar, i, z);
                    a(nVar);
                    this.ag.a(str3, i, z);
                } else if (str3.startsWith(com.uc.c.w.Pl)) {
                    this.ag = com.uc.a.e.nR().nW().b(null, nVar, i, z);
                    a(nVar);
                    this.ag.a(str3, i, z);
                } else if (str3.startsWith("ext:report_website")) {
                    this.ag = com.uc.a.e.nR().nW().b(null, nVar, i, z);
                    a(nVar);
                    this.ag.a(str3, i, z);
                } else if (str3 == null || !str3.startsWith(b.acU)) {
                    this.ag = com.uc.a.e.nR().nW().b(str3, nVar, i, z);
                    a(nVar);
                    this.ag.a(str3, i, z);
                    Qx();
                } else {
                    this.ag = com.uc.a.e.nR().nW().b(null, nVar, i, z);
                    a(nVar);
                    this.ag.a(str3, i, z);
                }
                if (z2) {
                    a(true);
                }
                if (!str3.startsWith("ext:update_sharepage") && !str3.contains("ext:scan_addr_book")) {
                    Q();
                }
                if (!aa()) {
                    Qx();
                }
                ap();
            }
        }
    }

    public void dZ(int i) {
        if (this.ag != null) {
            if (i < 0) {
                this.cfb = false;
            } else if (i > 0) {
                this.cfb = true;
            }
            Qy();
        }
    }

    public void e(int[] iArr, int[] iArr2) {
        this.cge = iArr;
        this.cgf = iArr2;
    }

    public boolean e(int i, KeyEvent keyEvent) {
        try {
            if (this.ag != null) {
                this.ag.a(i, keyEvent);
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public void f(int i) {
        if (this.ag != null) {
            this.ag.f(i);
        }
    }

    public boolean f(int i, KeyEvent keyEvent) {
        try {
            if (this.ag != null) {
                this.ag.b(i, keyEvent);
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public void g(int i) {
        if (this.ag != null) {
            this.ag.g(i);
            postInvalidate();
        }
    }

    public void gB() {
        if (this.cfa != null) {
            this.cfa.cancel();
        }
        this.cfa = null;
    }

    public int getOrientation() {
        try {
            return getWidth() == 0 ? b.aci : ((float) (getWidth() - 20)) > this.bk.Fy() ? b.ORIENTATION_LANDSCAPE : b.ORIENTATION_PORTRAIT;
        } catch (Exception e) {
            return b.aci;
        }
    }

    public int getProgress() {
        return this.cfe;
    }

    public String getTitle() {
        if (this.ag != null) {
            return this.ag.getTitle();
        }
        return null;
    }

    public String getUrl() {
        if (this.ag != null) {
            return this.ag.o();
        }
        return null;
    }

    public void goBack() {
        if (this.ag != null) {
            this.ag.goBack();
            aT();
        }
    }

    public void goForward() {
        if (this.ag != null) {
            this.ag.goForward();
            aT();
        }
    }

    public int j(int i) {
        if (this.ag == null) {
            return 4;
        }
        return this.ag.j(i);
    }

    public void k(boolean z) {
        this.cfh = z;
    }

    public void kE(int i) {
        if (this.ag != null) {
            this.ag.a(i, -1.0f, -1.0f);
            postInvalidate();
        }
    }

    /* access modifiers changed from: protected */
    public void l(Canvas canvas) {
        if (aEg != null) {
            int intrinsicHeight = aEg.getIntrinsicHeight();
            if (intrinsicHeight <= 0) {
                intrinsicHeight = 5;
            }
            aEg.setBounds(0, getHeight() - intrinsicHeight, getWidth(), getHeight());
            aEg.draw(canvas);
        }
        if (aEh != null) {
            int intrinsicHeight2 = aEh.getIntrinsicHeight();
            if (intrinsicHeight2 <= 0) {
                intrinsicHeight2 = 5;
            }
            aEh.setBounds(0, 0, getWidth(), intrinsicHeight2);
            aEh.draw(canvas);
        }
    }

    public int n() {
        return this.ag.n();
    }

    public void oS() {
        com.uc.a.e.nR().nY().oS();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.WebViewJUC.a(android.graphics.Canvas, boolean):void
     arg types: [android.graphics.Canvas, int]
     candidates:
      com.uc.browser.WebViewJUC.a(com.uc.browser.WebViewJUC, byte):byte
      com.uc.browser.WebViewJUC.a(com.uc.browser.WebViewJUC, int):int
      com.uc.browser.WebViewJUC.a(com.uc.browser.WebViewJUC, android.view.MotionEvent):android.view.MotionEvent
      com.uc.browser.WebViewJUC.a(java.lang.String, java.lang.String):void
      com.uc.browser.WebViewJUC.a(java.io.File, byte):boolean
      com.uc.browser.WebViewJUC.a(android.graphics.Canvas, boolean):void */
    public void onDraw(Canvas canvas) {
        a(canvas, true);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        getResources().getDimension(R.dimen.f290progressbar_height);
        try {
            if (bE(i3 - i, i4 - i2)) {
                aT();
            }
        } catch (Exception e) {
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, int, java.lang.Object):void
     arg types: [int, int, boolean]
     candidates:
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.util.HashMap, int):void
      com.uc.browser.ModelBrowser.a(int, int, long):void
      com.uc.browser.ModelBrowser.a(int, java.lang.String, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WebViewJUC, int, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, com.uc.browser.WindowUCWeb, int):void
      com.uc.browser.ModelBrowser.a(int, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.WebViewJUC.a(float, float, float, boolean):void
     arg types: [int, float, float, int]
     candidates:
      com.uc.browser.WebViewJUC.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String):void
      com.uc.browser.WebViewJUC.a(float, float, float, boolean):void */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z;
        int i;
        int i2;
        byte b2;
        if (this.cfX) {
            switch (motionEvent.getAction()) {
                case 0:
                    b2 = 0;
                    break;
                case 1:
                    this.cfX = false;
                    b2 = 1;
                    break;
                case 2:
                    b2 = 2;
                    break;
                default:
                    this.cfX = false;
                    b2 = 1;
                    break;
            }
            return this.cfW.b(b2, ((int) motionEvent.getX()) - this.cfY, ((int) motionEvent.getY()) - this.cfZ);
        }
        if (motionEvent.getAction() == 0) {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(ModelBrowser.zP);
            }
            if (this.cfV && bH((int) motionEvent.getX(), (int) motionEvent.getY())) {
                this.cfX = true;
                return this.cfW.b((byte) 0, ((int) motionEvent.getX()) - this.cfY, ((int) motionEvent.getY()) - this.cfZ);
            }
        }
        this.cfX = false;
        if (!this.aFL) {
            return true;
        }
        if (e(motionEvent)) {
            return true;
        }
        int action = motionEvent.getAction();
        boolean ac = ac();
        boolean z2 = this instanceof WebViewJUCMainpage;
        if ((!ac || !ad()) && an.vA().vC() && an.vA().vx()) {
            this.cfs = 8;
        }
        if (z2 || ac || ae() || !com.uc.a.e.RD.equals(com.uc.a.e.nR().nY().bw(com.uc.a.h.afy)) || an.vA().vC()) {
            this.cfp = 0;
        } else {
            this.cfp = 1;
        }
        if (action == 0) {
            this.ceZ = false;
            this.ar = (int) motionEvent.getX();
            this.as = (int) motionEvent.getY();
            this.ag.a(motionEvent);
            if (11 == this.cfs) {
                if (!an.vA().vC()) {
                    aE();
                }
                this.ag.T();
                this.cfs = 2;
            } else if (!(8 == this.cfs || 4 == this.cfs)) {
                this.cfB = System.currentTimeMillis();
                if (!an.vA().vC()) {
                    aE();
                }
                this.cfs = 1;
            }
            if (an.vA().vx()) {
                this.cfs = 8;
                int vy = an.vA().vy();
                if (vy != -1) {
                    if (vy == 0) {
                        QE();
                    } else if (vy == 1) {
                        QF();
                    }
                }
            } else if (an.vA().vC()) {
                this.cfs = 1;
            }
            if (an.vA().vC()) {
                QC();
            }
        } else if (2 == action) {
            if (7 == this.cfs || this.ceZ) {
                return true;
            }
            switch (this.cfs) {
                case 1:
                case 2:
                    int y = (int) (((float) this.as) - motionEvent.getY());
                    if (Math.abs((int) (((float) this.ar) - motionEvent.getX())) > this.cfL || Math.abs(y) > this.cfM) {
                        aF();
                        ModelBrowser gD = ModelBrowser.gD();
                        if (ac && gD != null) {
                            gD.a(106, b.acj, this);
                        } else if (gD != null) {
                            gD.a(106, b.ack, this);
                        }
                        if (this.cfc == null) {
                            this.cfc = VelocityTracker.obtain();
                        }
                        if (this.cfc != null) {
                            this.cfc.addMovement(motionEvent);
                        }
                        this.cfs = 3;
                        if (ac) {
                            this.cfD = 1;
                            this.cfP = (float) this.ar;
                            this.cfQ = (float) this.as;
                            float x = motionEvent.getX();
                            float y2 = motionEvent.getY();
                            int i3 = (int) (this.cfP - x);
                            int i4 = (int) (this.cfQ - y2);
                            int abs = Math.abs(i3);
                            int abs2 = Math.abs(i4);
                            if (((float) abs) <= ((float) abs2) * cfN) {
                                if (((float) abs2) > ((float) abs) * cfN) {
                                    this.cfD = 3;
                                    this.cfJ = i4 > 0;
                                    motionEvent.setLocation(this.cfP, y2);
                                    this.cfQ = y2;
                                    break;
                                }
                            } else {
                                this.cfD = 2;
                                this.cfJ = i3 > 0;
                                motionEvent.setLocation(x, this.cfQ);
                                this.cfP = x;
                                break;
                            }
                        }
                    } else {
                        return true;
                    }
                    break;
                case 3:
                    if (this.cfc != null) {
                        this.cfc.addMovement(motionEvent);
                    }
                    if (ac) {
                        float x2 = motionEvent.getX();
                        float y3 = motionEvent.getY();
                        int i5 = (int) (this.cfP - x2);
                        int i6 = (int) (this.cfQ - y3);
                        if ((i5 * i5) + (i6 * i6) >= this.cfK) {
                            int abs3 = Math.abs(i5);
                            int abs4 = Math.abs(i6);
                            if (this.cfD == 2 || this.cfD == 3) {
                                if (this.cfD == 2) {
                                    if (((float) abs4) > ((float) abs3) * cfN && abs4 > 80) {
                                        this.cfD = 1;
                                    }
                                    if (((float) abs3) > ((float) abs4) * cfN && ((this.cfJ && i5 < (-this.cfL)) || (!this.cfJ && i5 > this.cfL))) {
                                        this.cfD = 4;
                                    }
                                } else {
                                    if (((float) abs3) > ((float) abs4) * cfN && abs3 > 80) {
                                        this.cfD = 1;
                                    }
                                    if (((float) abs4) > ((float) abs3) * cfN && ((this.cfJ && i6 < (-this.cfL)) || (!this.cfJ && i6 > this.cfL))) {
                                        this.cfD = 5;
                                    }
                                }
                            }
                            if (this.cfD != 2 && this.cfD != 4) {
                                if (this.cfD != 3 && this.cfD != 5) {
                                    this.cfP = x2;
                                    this.cfQ = y3;
                                    break;
                                } else {
                                    motionEvent.setLocation(this.cfP, y3);
                                    this.cfQ = y3;
                                    break;
                                }
                            } else {
                                motionEvent.setLocation(x2, this.cfQ);
                                this.cfP = x2;
                                break;
                            }
                        } else {
                            return true;
                        }
                    }
                    break;
            }
            this.ag.a(motionEvent);
        } else if (1 == action) {
            com.uc.c.w.Rh = 0;
            switch (this.cfs) {
                case 1:
                    aF();
                    if (ac) {
                        this.cfA = motionEvent;
                        this.cfC = this.cfB;
                        this.cfs = 4;
                        this.mHandler.sendEmptyMessageDelayed(cfT, 300);
                    } else {
                        if (this.cfp == 1) {
                            boolean z3 = this.ag.Z() == -1;
                            this.ag.a(motionEvent);
                            if (z3) {
                                f((int) motionEvent.getY());
                            }
                        } else {
                            this.ag.a(motionEvent);
                        }
                        this.cfs = 7;
                    }
                    if (an.vA().vC() && !an.vA().b(motionEvent.getX(), motionEvent.getY()) && ModelBrowser.gD() != null) {
                        ModelBrowser.gD().a(83, 0, (Object) false);
                        z = false;
                        break;
                    }
                    z = true;
                    break;
                case 2:
                    aF();
                    this.cfs = 7;
                    z = true;
                    break;
                case 3:
                    if (this.cfc != null) {
                        this.cfc.computeCurrentVelocity(1000);
                        int yVelocity = (int) this.cfc.getYVelocity();
                        com.uc.c.w.Rh = yVelocity;
                        int i7 = yVelocity;
                        i2 = (int) this.cfc.getXVelocity();
                        i = i7;
                    } else {
                        i = 0;
                        i2 = 0;
                    }
                    this.ag.a(motionEvent);
                    if (this.cfc == null) {
                        this.cfs = 7;
                        z = true;
                        break;
                    } else {
                        this.cfs = 11;
                        this.ag.c(i2, i);
                        if (this.cfc != null) {
                            this.cfc.recycle();
                            this.cfc = null;
                        }
                        z = false;
                        break;
                    }
                case 4:
                    if (System.currentTimeMillis() < this.cfC + 400) {
                        this.cfs = 7;
                        a(-1.0f, motionEvent.getX(), motionEvent.getY(), true);
                        z = true;
                        break;
                    }
                    z = true;
                    break;
                case 5:
                case 6:
                case 7:
                default:
                    z = true;
                    break;
                case 8:
                    this.ag.a(motionEvent);
                    if (an.vA().vC()) {
                        QG();
                        an.vA().aA((int) motionEvent.getX(), (int) motionEvent.getY());
                        z = true;
                        break;
                    }
                    z = true;
                    break;
            }
            if (z && an.vA().vC()) {
                EG();
            }
            Y();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        if (i != 0) {
            Qz();
        }
        super.onWindowVisibilityChanged(i);
    }

    public byte p() {
        if (this.ag == null) {
            return -1;
        }
        return this.ag.p();
    }

    public int q() {
        if (this.ag == null) {
            return -1;
        }
        return this.ag.q();
    }

    public void qu() {
        com.uc.a.e.nR().qu();
    }

    public void s() {
        if (this.ag != null) {
            this.ag.s();
        }
    }

    public void setProgress(int i) {
        this.cfe = i;
    }

    public void si() {
        try {
            if (this.ag != null) {
                this.ag.i(getOrientation());
            }
        } catch (Exception e) {
        }
    }

    public void stopLoading() {
        if (this.ag != null) {
            this.ag.stopLoading();
        }
    }

    public int t() {
        if (this.ag == null) {
            return 2;
        }
        return this.ag.t();
    }

    public void u() {
        if (this.ag != null) {
            this.ag.u();
        }
    }

    public boolean uf() {
        return ac();
    }

    public void v() {
        if (this.ag != null) {
            this.ag.v();
        }
    }

    public boolean w() {
        if (this.ag == null) {
            return true;
        }
        return this.ag.w();
    }

    public int x() {
        if (this.ag == null) {
            return -1;
        }
        return this.ag.x();
    }

    public String y() {
        if (this.ag == null) {
            return null;
        }
        return this.ag.y();
    }

    public void z(Vector vector) {
        a aVar = this.ag;
        this.ag = null;
        vector.add(aVar);
    }

    public boolean z() {
        if (this.ag == null) {
            return false;
        }
        return this.ag.z();
    }

    public boolean zF() {
        return this.aFL;
    }

    public void zG() {
        if (this.aFP != null) {
            this.aFP.FB();
        } else {
            this.aFP = new DrawWaitingPage(this, false, null);
        }
        postInvalidate();
    }

    public boolean zH() {
        if (this.aFP != null) {
            return this.aFP.zH();
        }
        return false;
    }

    public void zI() {
        if (this.aFP != null) {
            this.aFP.FB();
            this.aFP = null;
        }
    }

    public boolean zoomIn() {
        kE(2);
        return true;
    }

    public boolean zoomOut() {
        kE(1);
        return true;
    }
}
