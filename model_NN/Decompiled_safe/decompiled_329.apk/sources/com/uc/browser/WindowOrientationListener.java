package com.uc.browser;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import b.a.a.a.u;
import b.a.a.c.e;

public abstract class WindowOrientationListener {
    private SensorManager bTN;
    private boolean bTO;
    private int bTP;
    private Sensor bTQ;
    private SensorEventListenerImpl bTR;

    class SensorEventListenerImpl implements SensorEventListener {
        private static final int JA = 45;
        private static final int JB = 1;
        private static final int JC = 200;
        private static final int JD = 200;
        private static final int JE = 600;
        private static final int JF = 5000;
        private static final float JG = 0.5f;
        private static final float JH = 0.25f;
        private static final float JI = 0.03846154f;
        private static final float Jq = 57.29578f;
        private static final int Jr = 0;
        private static final int Js = 1;
        private static final int Jt = 2;
        private static final int Jy = 65;
        private static final int ROTATION_0 = 0;
        private static final int ROTATION_270 = 2;
        private static final int ROTATION_90 = 1;
        private float[] JJ = {0.0f, 0.0f, 0.0f};
        private int Ju = 0;
        private final int[] Jv = {0, 1, 3};
        private final int[][][] Jw = {new int[][]{new int[]{60, u.bmk}, new int[]{u.bmk, e.HTTP_MULT_CHOICE}}, new int[][]{new int[]{0, 45}, new int[]{45, ModelBrowser.Ac}, new int[]{330, 360}}, new int[][]{new int[]{0, 30}, new int[]{195, 315}, new int[]{315, 360}}};
        private final int[][] Jx = {new int[]{2, 1}, new int[]{0, 2, 0}, new int[]{0, 1, 0}};
        private final int[] Jz = {65, 65, 65};

        SensorEventListenerImpl() {
        }

        private void U(int i, int i2) {
            int i3;
            int[][] iArr = this.Jw[this.Ju];
            int i4 = 0;
            while (true) {
                if (i4 < iArr.length) {
                    if (i >= iArr[i4][0] && i < iArr[i4][1]) {
                        i3 = i4;
                        break;
                    }
                    i4++;
                } else {
                    i3 = -1;
                    break;
                }
            }
            if (i3 != -1) {
                int i5 = this.Jx[this.Ju][i3];
                if (i2 <= this.Jz[i5]) {
                    this.Ju = i5;
                    WindowOrientationListener.this.onOrientationChanged(this.Jv[i5]);
                }
            }
        }

        private float a(float f, float f2) {
            return Math.abs(((float) Math.asin((double) (f / f2))) * Jq);
        }

        private float a(float f, float f2, float f3) {
            return (f3 * f) + ((1.0f - f3) * f2);
        }

        private float b(float f, float f2, float f3) {
            return (float) Math.sqrt((double) ((f * f) + (f2 * f2) + (f3 * f3)));
        }

        /* access modifiers changed from: package-private */
        public int ld() {
            return this.Jv[this.Ju];
        }

        public void onAccuracyChanged(Sensor sensor, int i) {
        }

        public void onSensorChanged(SensorEvent sensorEvent) {
            float f = sensorEvent.values[0];
            float f2 = sensorEvent.values[1];
            float f3 = sensorEvent.values[2];
            float b2 = b(f, f2, f3);
            float abs = Math.abs(b2 - 9.80665f);
            float a2 = a(f3, b2);
            if (a2 <= 65.0f) {
                float f4 = abs > 1.0f ? JI : a2 > 45.0f ? JH : 0.5f;
                float[] fArr = this.JJ;
                float a3 = a(f, this.JJ[0], f4);
                fArr[0] = a3;
                float[] fArr2 = this.JJ;
                float a4 = a(f2, this.JJ[1], f4);
                fArr2[1] = a4;
                float[] fArr3 = this.JJ;
                float a5 = a(f3, this.JJ[2], f4);
                fArr3[2] = a5;
                float a6 = a(a5, b(a3, a4, a5));
                int round = Math.round(((float) Math.atan2((double) (-a3), (double) a4)) * Jq);
                if (round < 0) {
                    round += 360;
                }
                U(round, Math.round(a6));
            }
        }
    }

    public WindowOrientationListener(Context context) {
        this(context, 3);
    }

    private WindowOrientationListener(Context context, int i) {
        this.bTO = false;
        this.bTN = (SensorManager) context.getSystemService("sensor");
        this.bTP = i;
        this.bTQ = this.bTN.getDefaultSensor(1);
        if (this.bTQ != null) {
            this.bTR = new SensorEventListenerImpl();
        }
    }

    public boolean canDetectOrientation() {
        return this.bTQ != null;
    }

    public void disable() {
        if (this.bTQ != null && this.bTO) {
            this.bTN.unregisterListener(this.bTR);
            this.bTO = false;
        }
    }

    public void enable() {
        if (this.bTQ != null && !this.bTO) {
            this.bTN.registerListener(this.bTR, this.bTQ, this.bTP);
            this.bTO = true;
        }
    }

    public int ld() {
        if (this.bTO) {
            return this.bTR.ld();
        }
        return -1;
    }

    public abstract void onOrientationChanged(int i);
}
