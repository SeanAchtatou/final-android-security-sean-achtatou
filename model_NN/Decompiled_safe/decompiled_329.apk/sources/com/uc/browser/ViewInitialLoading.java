package com.uc.browser;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import com.uc.a.e;
import java.util.ArrayList;
import java.util.List;

public class ViewInitialLoading extends RelativeLayout implements ViewSwitcher.ViewFactory {
    public static final int bCz = 1;
    public static final int c = 2;
    private View.OnClickListener bCA = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.f771fresh_cancel:
                    ViewInitialLoading.this.bCy.a((byte) 1);
                    e.nR().nZ().cm(4);
                    ViewInitialLoading.this.bCy.setResult(0);
                    ViewInitialLoading.this.bCy.finish();
                    return;
                case R.id.f772goto_mainpage:
                    ViewInitialLoading.this.bCy.setResult(-1);
                    ViewInitialLoading.this.bCy.finish();
                    return;
                case R.id.f773fresh_back:
                    ViewInitialLoading.this.bCy.setResult(0);
                    ViewInitialLoading.this.bCy.finish();
                    return;
                default:
                    return;
            }
        }
    };
    private ImageView bCi;
    private ImageView bCj;
    private ImageView bCk;
    private ImageView bCl;
    private ImageView bCm;
    private ImageView bCn;
    private int bCo;
    private Drawable bCp;
    private Drawable bCq;
    private Drawable bCr;
    private TextSwitcher bCs;
    private List bCt;
    TextView bCu;
    TextView bCv;
    TextView bCw;
    TextView bCx;
    /* access modifiers changed from: private */
    public ActivityInitial bCy = null;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    ViewInitialLoading.this.IY();
                    return;
                case 2:
                    ViewInitialLoading.this.setText(message.obj.toString());
                    return;
                default:
                    return;
            }
        }
    };

    public ViewInitialLoading(Context context) {
        super(context);
        Drawable drawable;
        this.bCy = (ActivityInitial) context;
        LayoutInflater.from(context).inflate((int) R.layout.f906initial_loading, this);
        try {
            drawable = context.getResources().getDrawable(R.drawable.f645ucbackground);
        } catch (Exception e) {
            drawable = null;
        }
        ((RelativeLayout) findViewById(R.id.f760initial_main)).setBackgroundDrawable(drawable);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        ((HightQualityImageView) findViewById(R.id.f761ucmascot)).bpj = BitmapFactory.decodeResource(getResources(), R.drawable.f646ucmascot, options);
        this.bCo = -1;
        this.bCt = new ArrayList();
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.f762initial_dotset);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.leftMargin = (int) context.getResources().getDimension(R.dimen.f279dot_padding);
        this.bCi = (ImageView) findViewById(R.id.f763initial_loading_1);
        this.bCj = (ImageView) findViewById(R.id.f764initial_loading_2);
        this.bCk = (ImageView) findViewById(R.id.f765initial_loading_3);
        this.bCl = (ImageView) findViewById(R.id.f766initial_loading_4);
        this.bCm = (ImageView) findViewById(R.id.f767initial_loading_5);
        this.bCn = (ImageView) findViewById(R.id.f768initial_loading_6);
        this.bCt.add(this.bCi);
        this.bCt.add(this.bCj);
        this.bCt.add(this.bCk);
        this.bCt.add(this.bCl);
        this.bCt.add(this.bCm);
        this.bCt.add(this.bCn);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.bCt.size()) {
                ((ImageView) this.bCt.get(i2)).setLayoutParams(layoutParams);
                i = i2 + 1;
            } else {
                try {
                    break;
                } catch (Exception e2) {
                }
            }
        }
        this.bCr = context.getResources().getDrawable(R.drawable.f578initdot1);
        this.bCq = context.getResources().getDrawable(R.drawable.f579initdot2);
        this.bCp = context.getResources().getDrawable(R.drawable.f580initdot3);
        this.bCs = (TextSwitcher) findViewById(R.id.f769initial_loading_text);
        this.bCu = (TextView) findViewById(R.id.f770initial_website);
        this.bCu.setTextSize(context.getResources().getDimension(R.dimen.f285website_textsize));
        this.bCv = (TextView) findViewById(R.id.f771fresh_cancel);
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.bCv.getLayoutParams();
        layoutParams2.topMargin = (int) context.getResources().getDimension(R.dimen.f286fresh_us_data_btn_margintop);
        this.bCv.setLayoutParams(layoutParams2);
        this.bCv.setTextSize(context.getResources().getDimension(R.dimen.f282loading_text_size));
        this.bCv.setOnClickListener(this.bCA);
        this.bCx = (TextView) findViewById(R.id.f772goto_mainpage);
        RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) this.bCx.getLayoutParams();
        layoutParams3.topMargin = (int) context.getResources().getDimension(R.dimen.f286fresh_us_data_btn_margintop);
        this.bCx.setLayoutParams(layoutParams3);
        this.bCx.setTextSize(context.getResources().getDimension(R.dimen.f282loading_text_size));
        this.bCx.setOnClickListener(this.bCA);
        this.bCw = (TextView) findViewById(R.id.f773fresh_back);
        RelativeLayout.LayoutParams layoutParams4 = (RelativeLayout.LayoutParams) this.bCw.getLayoutParams();
        layoutParams4.topMargin = (int) context.getResources().getDimension(R.dimen.f286fresh_us_data_btn_margintop);
        this.bCw.setLayoutParams(layoutParams4);
        this.bCw.setTextSize(context.getResources().getDimension(R.dimen.f282loading_text_size));
        this.bCw.setOnClickListener(this.bCA);
        Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), 17432576);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(getContext(), 17432577);
        this.bCs.setInAnimation(loadAnimation);
        this.bCs.setOutAnimation(loadAnimation2);
        this.bCs.setFactory(this);
    }

    public void EN() {
    }

    public void IW() {
        IY();
    }

    public void IX() {
        this.mHandler.removeMessages(1);
    }

    public void IY() {
        this.bCo = (this.bCo + 1) % 6;
        ((ImageView) this.bCt.get(this.bCo)).setBackgroundDrawable(this.bCp);
        if (this.bCo == 0) {
            int i = this.bCo + 1;
            while (true) {
                int i2 = i;
                if (i2 >= 6) {
                    break;
                }
                ((ImageView) this.bCt.get(i2)).setBackgroundDrawable(this.bCr);
                i = i2 + 1;
            }
        }
        if (this.bCo >= 1) {
            ((ImageView) this.bCt.get(this.bCo - 1)).setBackgroundDrawable(this.bCq);
        }
        if (this.bCo >= 2) {
            ((ImageView) this.bCt.get(this.bCo - 2)).setBackgroundDrawable(this.bCr);
        }
        this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(1), 150);
    }

    public void IZ() {
        if (this.bCu != null) {
            this.bCu.setVisibility(8);
        }
        if (this.bCv != null) {
            this.bCv.setVisibility(0);
        }
    }

    public void Ja() {
        if (this.bCv != null) {
            this.bCv.setVisibility(8);
        }
        if (this.bCx != null) {
            this.bCx.setVisibility(0);
        }
        if (this.bCw != null) {
            this.bCw.setVisibility(0);
        }
        IX();
    }

    public void Jb() {
        if (this.bCv != null) {
            this.bCv.setVisibility(8);
        }
    }

    public void a(int i, Object obj) {
        this.mHandler.sendMessage(Message.obtain(null, i, obj));
    }

    public void destroy() {
        IX();
        this.bCs = null;
        removeAllViews();
        this.bCt = null;
    }

    public View makeView() {
        TextView textView = new TextView(getContext());
        textView.setGravity(49);
        textView.setTextSize(getContext().getResources().getDimension(R.dimen.f282loading_text_size));
        textView.setTextColor(-3281411);
        return textView;
    }

    public void setText(String str) {
        this.bCs.setText(str);
    }
}
