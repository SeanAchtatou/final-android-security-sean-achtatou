package com.uc.browser;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ListAdapter;
import android.widget.ListView;

public class WebsiteSearchListView extends ListView {
    public WebsiteSearchListView(Context context) {
        super(context);
    }

    public WebsiteSearchListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean z, int i, Rect rect) {
        super.onFocusChanged(z, i, rect);
        if (z && rect != null) {
            ListAdapter adapter = getAdapter();
            int count = adapter.getCount();
            switch (i) {
                case 33:
                    int i2 = count - 1;
                    while (i2 >= 0) {
                        if (!adapter.isEnabled(i2)) {
                            i2--;
                        } else {
                            setSelection(i2);
                            return;
                        }
                    }
                    return;
                case 130:
                    int i3 = 0;
                    while (i3 < count) {
                        if (!adapter.isEnabled(i3)) {
                            i3++;
                        } else {
                            setSelection(i3);
                            return;
                        }
                    }
                    return;
                default:
                    return;
            }
        }
    }
}
