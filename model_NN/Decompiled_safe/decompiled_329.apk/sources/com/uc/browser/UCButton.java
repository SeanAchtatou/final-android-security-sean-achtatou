package com.uc.browser;

import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.widget.Button;
import com.uc.browser.UCR;
import com.uc.h.a;
import com.uc.h.e;

public class UCButton extends Button implements a {
    public UCButton(Context context) {
        super(context);
        a();
    }

    public UCButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public UCButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    private void a() {
        setTextSize(getResources().getDimension(R.dimen.f313dialog_button_textsize));
        setSingleLine();
        int paddingLeft = getPaddingLeft();
        getPaddingTop();
        int paddingRight = getPaddingRight();
        getPaddingBottom();
        k();
        setPadding(paddingLeft, 0, paddingRight, 0);
        setGravity(17);
        e.Pb().a(this);
    }

    public void k() {
        setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aWR));
        setTextColor(new ColorStateList(new int[][]{new int[]{16842919, 16842910, 16842909}, new int[0]}, new int[]{e.Pb().getColor(9), e.Pb().getColor(8)}));
    }
}
