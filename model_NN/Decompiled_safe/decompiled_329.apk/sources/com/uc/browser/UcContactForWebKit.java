package com.uc.browser;

import android.content.Context;
import android.content.DialogInterface;
import android.webkit.WebView;
import com.uc.a.e;
import com.uc.browser.UcContact;
import com.uc.c.cd;
import java.util.HashSet;
import java.util.List;

public class UcContactForWebKit implements UcContact.CALLBACK {
    public static String aGu = "error";
    public static String aGv = cd.bUi;
    private UcContact aGw;
    private String aGx;
    private String aGy;
    public DialogInterface.OnCancelListener aGz = new DialogInterface.OnCancelListener() {
        public void onCancel(DialogInterface dialogInterface) {
            UcContactForWebKit.this.cancel();
        }
    };
    private boolean mCancel = false;
    private Context mContext;
    private WebView sk;

    public UcContactForWebKit(Context context) {
        this.mContext = context;
    }

    public UcContactForWebKit(Context context, WebView webView) {
        this.mContext = context;
        this.sk = webView;
    }

    private void f(List list) {
        HashSet hashSet = new HashSet(list);
        list.clear();
        list.addAll(hashSet);
    }

    public void cancel() {
        this.mCancel = true;
    }

    public void e(List list) {
        if (list == null) {
            this.aGx = aGu;
        } else if (list.size() != 0) {
            f(list);
            this.aGx = e.c(list);
        } else if (this.aGw.rY()) {
            this.aGx = aGu;
        } else {
            this.aGx = aGv;
        }
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().aS(112);
        }
        if (!this.mCancel) {
            this.sk.loadUrl("javascript:" + this.aGy + "()");
        }
    }

    public String getContactList() {
        String str = this.aGx;
        this.aGx = null;
        return str;
    }

    public void scanContactList(String str) {
        this.mCancel = false;
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(111, this.aGz);
        }
        this.aGy = str;
        this.aGw = new UcContact(this.mContext.getContentResolver(), this);
        this.aGw.rX();
    }
}
