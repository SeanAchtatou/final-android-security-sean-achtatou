package com.uc.browser;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.uc.browser.UCR;
import com.uc.h.a;
import com.uc.h.e;

public class ViewPageUpDownButton extends RelativeLayout implements a {
    LongClickableButton bir;
    LongClickableButton bis;
    LinearLayout bit;
    int state;

    public ViewPageUpDownButton(Context context) {
        super(context);
        a();
    }

    public ViewPageUpDownButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.uc.browser.ViewPageUpDownButton, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a() {
        LayoutInflater.from(getContext()).inflate((int) R.layout.f919page_up_down_button, (ViewGroup) this, true);
        this.bit = (LinearLayout) findViewById(R.id.f802container);
        this.bir = (LongClickableButton) findViewById(R.id.f803page_up);
        this.bir.Ic = 55;
        this.bir.Id = 77;
        this.bis = (LongClickableButton) findViewById(R.id.f804page_down);
        this.bis.Ic = 56;
        this.bis.Id = 78;
        this.state = 2;
        this.bir.setFocusable(false);
        this.bis.setFocusable(false);
        k();
        e.Pb().a(this);
    }

    public void CL() {
        if (this.state == 0) {
            this.state = 2;
        } else {
            this.state = -this.state;
        }
    }

    public boolean gA(int i) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.bit.getLayoutParams();
        switch (i) {
            case 1:
                layoutParams.addRule(11, 0);
                layoutParams.addRule(9);
                break;
            case 2:
                layoutParams.addRule(9, 0);
                layoutParams.addRule(11);
                break;
            default:
                return false;
        }
        this.bit.setLayoutParams(layoutParams);
        return true;
    }

    public int getState() {
        return this.state;
    }

    public void k() {
        this.bir.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aXd));
        this.bis.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aXc));
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return false;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        return false;
    }

    public void setState(int i) {
        this.state = i;
    }

    public void setVisibility(int i) {
        if (i != 0 || gA(this.state)) {
            super.setVisibility(i);
        } else {
            setVisibility(4);
        }
    }
}
