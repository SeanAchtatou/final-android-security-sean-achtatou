package com.uc.browser;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Toast;
import com.uc.a.e;

public class UzoneCamera {
    private String name;
    private ActivityBrowser sj;
    private WebView sk;
    private String sl;
    private int sm;
    private String sn;
    private String type = "JPEG";

    public UzoneCamera(ActivityBrowser activityBrowser, WebView webView) {
        this.sj = activityBrowser;
        this.sk = webView;
    }

    private String tT() {
        String str = "UC_Photo_";
        int fr = e.nR().fr() % 1000;
        if (fr == 0) {
            fr = 1;
        }
        if (fr < 100) {
            str = fr < 10 ? str + "00" : str + "0";
        }
        return str + "" + fr;
    }

    public void cn(String str) {
        this.sn = str;
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(113, 0, this);
        }
    }

    public void h(Object obj) {
        Intent intent = (Intent) obj;
        if (obj != null) {
            try {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    byte[] byteArray = extras.getByteArray("buffer");
                    String N = UCImageCoder.N(byteArray);
                    this.name = tT();
                    this.sl = N;
                    this.sm = byteArray.length;
                    this.sk.loadUrl("javascript:" + this.sn + "()");
                }
            } catch (Exception e) {
                Toast.makeText(this.sj, (int) R.string.f1447camera_fail, 1).show();
            }
        }
    }

    public String tP() {
        return this.name;
    }

    public String tQ() {
        return this.type;
    }

    public String tR() {
        String str = this.sl;
        this.sl = null;
        System.gc();
        return str;
    }

    public int tS() {
        return this.sm;
    }
}
