package com.uc.browser;

import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import com.uc.browser.UCR;
import com.uc.h.e;

public class UCEditText extends EditText {
    public boolean aDQ;
    public int aDR = -1;
    private boolean aDS = true;
    private boolean aDT = false;

    public class ccc extends ColorStateList {
        public ccc(int[][] iArr, int[] iArr2) {
            super(iArr, iArr2);
        }

        public int getColorForState(int[] iArr, int i) {
            if (iArr != null) {
                String str = "";
                for (int i2 = 0; i2 < iArr.length; i2++) {
                    str = str + " " + iArr[i2];
                }
                Log.e("adafdfads", "adadfadf" + str);
            }
            return super.getColorForState(iArr, i);
        }
    }

    public UCEditText(Context context) {
        super(context);
        a();
        yY();
    }

    public UCEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
        yY();
    }

    public UCEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
        yY();
    }

    /* access modifiers changed from: protected */
    public void a() {
        e Pb = e.Pb();
        setHighlightColor(Pb.getColor(0));
        setSelectAllOnFocus(true);
        setBackgroundDrawable(Pb.getDrawable(UCR.drawable.aWZ));
        setPadding(Pb.kp(R.dimen.f167edittext_padding_top), getPaddingTop(), Pb.kp(R.dimen.f167edittext_padding_top), getPaddingBottom());
        setGravity(16);
        setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (keyEvent == null || keyEvent.getKeyCode() != 66) {
                    return false;
                }
                if (keyEvent.getAction() != 1) {
                    return true;
                }
                ((InputMethodManager) UCEditText.this.getContext().getSystemService("input_method")).hideSoftInputFromWindow(UCEditText.this.getWindowToken(), 0);
                return true;
            }
        });
    }

    public void bb(boolean z) {
        this.aDS = z;
    }

    /* access modifiers changed from: protected */
    public void onCreateContextMenu(ContextMenu contextMenu) {
        this.aDT = true;
        ((ActivityWithUCMenu) getContext()).openContextMenu(this);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.aDT = false;
        } else if (this.aDT && motionEvent.getAction() == 1) {
            return true;
        }
        return super.onTouchEvent(motionEvent);
    }

    public boolean yX() {
        return this.aDS;
    }

    public void yY() {
    }
}
