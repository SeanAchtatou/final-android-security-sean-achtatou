package com.uc.browser;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;
import b.b.b;
import com.uc.a.e;
import com.uc.b.a;

public class ActivityUpdate extends Activity {
    public static final String IA = "com.uc.browser.intent.action.SEND";
    private static final String IB = "com.uc.browser.intent.action.LOADBUFFER";
    public static final String IC = "search_action:";
    public static final String ID = "view_action:";
    public static final String IE = "buffer_action:";
    public static final String IF = "ucshare_action:";
    public static final String IG = "time_stamp";
    public static final String IH = "recall_action";
    public static final String II = "share_title";
    public static final String IJ = "share_url";
    public static final long IK = 4000;
    public static String IL = null;
    public static String IM = null;
    public static Uri IN = null;
    public static byte[] IO = null;
    public static boolean IP = true;
    public static final String Ix = "url_to_load";
    private static final String Iy = "com.uc.browser.intent.action.LOADURL";
    private static final String Iz = "com.uc.browser.intent.action.WEBSEARCH";
    public static String ck;

    public static void kS() {
        IL = null;
        IO = null;
        IP = true;
        IM = null;
        IN = null;
    }

    private void kT() {
        a.a(getApplicationContext());
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        String type;
        super.onCreate(bundle);
        if (ActivityBrowser.bqO) {
            finish();
            return;
        }
        kT();
        kS();
        Intent intent = getIntent();
        String action = intent.getAction();
        long currentTimeMillis = System.currentTimeMillis() - intent.getLongExtra(IG, 0);
        ck = intent.getStringExtra(IH);
        if (currentTimeMillis <= IK || (action != null && !action.equals(Iz) && !action.equals(Iy) && !action.equals(IB))) {
            if ("android.intent.action.SEARCH".equals(action) || "android.intent.action.WEB_SEARCH".equals(action) || Iz.equals(action)) {
                IL = intent.getStringExtra("query");
                if (IL == null) {
                    IL = intent.getStringExtra("UC_WEBSEARCH");
                }
                if (IL != null && IL.length() > 0) {
                    IP = false;
                    IL = IC + IL;
                    ck = "";
                }
            } else if ("android.intent.action.VIEW".equals(action) || Iy.equals(action)) {
                Uri data = intent.getData();
                if (data != null) {
                    IL = data.toString();
                } else {
                    IL = intent.getStringExtra("UC_LOADURL");
                }
                if (IL != null && IL.length() > 0) {
                    IP = false;
                    IL = ID + IL;
                    ck = "";
                }
            } else if (IB.equals(action)) {
                IO = intent.getByteArrayExtra("UC_LOADBUFFER");
                IL = intent.getStringExtra("UC_LOADURL");
                if (IL != null && IL.length() > 0) {
                    IP = false;
                    IL = IE + IL;
                }
            } else if (IA.equals(action) && (type = intent.getType()) != null) {
                if ("text/plain".equalsIgnoreCase(type)) {
                    String stringExtra = intent.getStringExtra(II);
                    String stringExtra2 = intent.getStringExtra(IJ);
                    StringBuilder append = new StringBuilder().append(IF);
                    if (stringExtra == null || stringExtra2 == null) {
                        stringExtra2 = "";
                    }
                    IL = append.append(stringExtra2).toString();
                    IM = stringExtra == null ? intent.getStringExtra("android.intent.extra.TEXT") : stringExtra;
                } else if (type.toLowerCase().startsWith("image/")) {
                    IL = null;
                    Toast.makeText(this, (int) R.string.f963share_image_not_yet, 0).show();
                }
            }
        }
        PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean("uc_destroy", false).commit();
        if (true != e.nS()) {
            e.Ro = false;
            Intent intent2 = new Intent(this, ActivityInitial.class);
            b.cL(false);
            startActivity(intent2);
        } else if (IL == null && IO == null && e.Ro) {
            finish();
        } else {
            e.Ro = false;
            startActivity(new Intent(this, ActivityBrowser.class));
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }
}
