package com.uc.browser;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.uc.a.l;
import com.uc.browser.UCR;
import com.uc.h.a;
import com.uc.h.e;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class WebsiteSearchSchDialog extends Dialog implements TextWatcher, a {
    public static final int bQe = 100;
    private l Rq;
    /* access modifiers changed from: private */
    public SearchBarListener auz;
    private ImageView bQA;
    private RelativeLayout bQB;
    private ImageView bQC;
    private TextView bQD;
    private RelativeLayout bQf;
    /* access modifiers changed from: private */
    public EditText bQg;
    /* access modifiers changed from: private */
    public TextView bQh;
    private TextView bQi;
    /* access modifiers changed from: private */
    public ImageView bQj;
    /* access modifiers changed from: private */
    public LinearLayout bQk;
    private View bQl;
    private RelativeLayout bQm;
    private List bQn;
    private boolean bQo;
    private Animation bQp;
    private Animation bQq;
    /* access modifiers changed from: private */
    public WebsiteSearchListView bQr;
    /* access modifiers changed from: private */
    public Vector bQs;
    /* access modifiers changed from: private */
    public int bQt;
    private LinearLayout bQu;
    private Drawable bQv;
    private Drawable bQw;
    private int bQx;
    private int bQy;
    private int bQz;
    private boolean bxc = true;
    /* access modifiers changed from: private */
    public InputMethodManager cA;
    private DialogInterface.OnDismissListener cB;
    private boolean cD = false;
    private Context cr;
    private String ct;
    /* access modifiers changed from: private */
    public String cu;
    AdapterAutoComplete cv;
    private int cw;
    private Drawable cy;
    private Drawable cz;

    public interface SearchBarListener {
        void E(String str);

        void dp();

        void onCancel();
    }

    public WebsiteSearchSchDialog(Context context) {
        super(context, R.style.f1582Transparent2);
        this.cr = context;
        this.cA = (InputMethodManager) context.getSystemService("input_method");
        getWindow().setSoftInputMode(4);
        this.bQf = (RelativeLayout) LayoutInflater.from(context).inflate((int) R.layout.f938searchdialog, (ViewGroup) null);
        this.bQk = (LinearLayout) this.bQf.findViewById(R.id.f857search_engine_list);
        this.bQl = this.bQf.findViewById(R.id.f856engine_list_cover);
        this.bQl.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                boolean unused = WebsiteSearchSchDialog.this.MP();
            }
        });
        this.bQB = (RelativeLayout) this.bQf.findViewById(R.id.f851search_bar_input);
        this.bQr = (WebsiteSearchListView) this.bQf.findViewById(R.id.f849search_input_list);
        this.bQh = (TextView) this.bQf.findViewById(R.id.f852search_input_cancle);
        this.bQh.setTextSize(0, (float) e.Pb().kp(R.dimen.f529add_sch_state_text));
        this.bQg = (EditText) this.bQf.findViewById(R.id.f850search_bar_editText);
        this.bQC = (ImageView) this.bQf.findViewById(R.id.f855search_engine_downicon);
        bP();
        e.Pb().a(this);
    }

    /* access modifiers changed from: private */
    public void MO() {
        this.cA.hideSoftInputFromWindow(this.bQg.getWindowToken(), 0);
        this.bQl.setVisibility(0);
        this.bQl.bringToFront();
        this.bQk.setVisibility(0);
        this.bQk.bringToFront();
        if (this.bQn.size() > 0) {
            this.bQg.setNextFocusDownId(0);
        }
        this.bQk.startAnimation(this.bQp);
    }

    /* access modifiers changed from: private */
    public boolean MP() {
        if (this.bQk.getVisibility() == 8) {
            return false;
        }
        this.bQk.setVisibility(8);
        this.bQl.setVisibility(8);
        this.bQg.requestFocus();
        this.cA.showSoftInput(this.bQg, 0);
        this.bQk.startAnimation(this.bQq);
        this.bQg.setNextFocusDownId(this.bQr.getId());
        return true;
    }

    public void MQ() {
        if (!(this.bQf == null || this.bQk == null || this.bQn == null)) {
            this.bQx = e.Pb().kp(R.dimen.f444add_sch_enginelist_width);
            this.bQy = e.Pb().kp(R.dimen.f446add_sch_schengine_width);
            this.bQz = e.Pb().kp(R.dimen.f448add_sch_schengine_height);
            int i = this.bQx / this.bQy;
            if (this.bQs != null) {
                int i2 = 0;
                while (i2 < i && i2 < this.bQs.size()) {
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.bQy, this.bQz);
                    layoutParams.topMargin = 0;
                    layoutParams.leftMargin = this.bQy * i2;
                    ((LinearLayout) this.bQn.get(i2)).setLayoutParams(layoutParams);
                    i2++;
                }
                for (int i3 = i; i3 < this.bQs.size(); i3++) {
                    RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(this.bQy, this.bQz);
                    layoutParams2.topMargin = this.bQz;
                    layoutParams2.leftMargin = (i3 - i) * this.bQy;
                    ((LinearLayout) this.bQn.get(i3)).setLayoutParams(layoutParams2);
                }
            }
        }
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(e.Pb().kp(R.dimen.f445add_sch_engine_divider_width), e.Pb().kp(R.dimen.f521add_sch_engine_divider_height));
        layoutParams3.leftMargin = e.Pb().kp(R.dimen.f522add_sch_engine_divider_marginleft);
        if (this.bQA != null) {
            this.bQA.setLayoutParams(layoutParams3);
        }
    }

    public LinearLayout MR() {
        return this.bQk;
    }

    public WebsiteSearchListView MS() {
        return this.bQr;
    }

    public Animation MT() {
        return this.bQq;
    }

    public void a(DialogInterface.OnDismissListener onDismissListener) {
        this.cB = onDismissListener;
    }

    public void a(Configuration configuration) {
        MQ();
        if (configuration.orientation == 1) {
            if (this.bQg != null && this.bQg.hasFocus() && isShowing() && this.bQk.getVisibility() == 8) {
                this.cA.showSoftInput(this.bQg, 1);
            }
        } else if (configuration.orientation == 2 && this.bQg != null && this.bQg.hasFocus() && isShowing()) {
            this.cA.hideSoftInputFromWindow(this.bQg.getWindowToken(), 0);
        }
    }

    public void a(View.OnClickListener onClickListener) {
        this.bQh.setOnClickListener(onClickListener);
    }

    public void a(View.OnKeyListener onKeyListener) {
        this.bQg.setOnKeyListener(onKeyListener);
    }

    public void a(SearchBarListener searchBarListener) {
        this.auz = searchBarListener;
        a(new View.OnClickListener() {
            public void onClick(View view) {
                if (WebsiteSearchSchDialog.this.bQh.getText().toString().equals(WebsiteSearchSchDialog.this.cu)) {
                    String obj = WebsiteSearchSchDialog.this.bQg.getText().toString();
                    if (com.uc.a.e.nR().nZ() != null) {
                        com.uc.a.e.nR().nZ().bo(obj);
                    }
                    WebsiteSearchSchDialog.this.auz.E(com.uc.a.e.nR().nZ().a(obj, (byte) WebsiteSearchSchDialog.this.bQt));
                    return;
                }
                WebsiteSearchSchDialog.this.auz.onCancel();
            }
        });
        a(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                WebsiteSearchSchDialog.this.auz.dp();
            }
        });
    }

    public void afterTextChanged(Editable editable) {
        if (!this.cD && editable.toString().length() > 0) {
            this.cv.k(com.uc.a.e.nR().qD());
            this.cD = true;
        } else if (editable.toString().length() <= 0) {
            this.cD = false;
            this.cv.k(null);
        }
        this.cv.bV(editable.toString());
    }

    public void b(View.OnClickListener onClickListener) {
        this.bQi.setOnClickListener(onClickListener);
    }

    public void bP() {
        int i;
        k();
        this.bQo = true;
        this.ct = this.cr.getResources().getString(R.string.f1469sch_cancel);
        this.cu = this.cr.getResources().getString(R.string.f1468sch_search);
        this.bQh.setText(this.ct);
        this.bQg.setImeOptions(6);
        this.bQg.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (6 != i) {
                    return true;
                }
                if (WebsiteSearchSchDialog.this.bQg.getText().length() != 0) {
                    String obj = WebsiteSearchSchDialog.this.bQg.getText().toString();
                    if (com.uc.a.e.nR().nZ() != null) {
                        com.uc.a.e.nR().nZ().bo(obj);
                    }
                    WebsiteSearchSchDialog.this.auz.E(com.uc.a.e.nR().nZ().a(obj, (byte) WebsiteSearchSchDialog.this.bQt));
                    return true;
                }
                WebsiteSearchSchDialog.this.auz.onCancel();
                return true;
            }
        });
        this.bQg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                boolean unused = WebsiteSearchSchDialog.this.MP();
            }
        });
        this.bQg.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i != 23 && i != 66) {
                    return false;
                }
                if (keyEvent.getAction() == 0) {
                    if (WebsiteSearchSchDialog.this.bQh.getText().toString().equals(WebsiteSearchSchDialog.this.cu)) {
                        String obj = WebsiteSearchSchDialog.this.bQg.getText().toString();
                        if (com.uc.a.e.nR().nZ() != null) {
                            com.uc.a.e.nR().nZ().bo(obj);
                        }
                        WebsiteSearchSchDialog.this.auz.E(com.uc.a.e.nR().nZ().a(obj, (byte) WebsiteSearchSchDialog.this.bQt));
                    } else {
                        WebsiteSearchSchDialog.this.auz.onCancel();
                    }
                }
                return true;
            }
        });
        this.bQr = (WebsiteSearchListView) this.bQf.findViewById(R.id.f849search_input_list);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.setMargins(0, e.Pb().kp(R.dimen.f483add_sch_height_input_state) - 5, 0, 0);
        this.bQr.setLayoutParams(layoutParams);
        this.bQh.setNextFocusDownId(this.bQr.getId());
        this.bQr.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScroll(AbsListView absListView, int i, int i2, int i3) {
            }

            public void onScrollStateChanged(AbsListView absListView, int i) {
                WebsiteSearchSchDialog.this.cA.hideSoftInputFromWindow(WebsiteSearchSchDialog.this.bQr.getWindowToken(), 0);
            }
        });
        this.Rq = com.uc.a.e.nR().nZ();
        if (this.cv == null) {
            this.cv = new AdapterAutoComplete();
        }
        this.bQr.setAdapter((ListAdapter) this.cv);
        this.bQr.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView adapterView, View view, int i, long j) {
                WebsiteSearchSchDialog.this.bQg.setText(((TextView) view).getText());
                Selection.setSelection(WebsiteSearchSchDialog.this.bQg.getText(), WebsiteSearchSchDialog.this.bQg.getText().length());
            }
        });
        this.bQu = (LinearLayout) this.bQf.findViewById(R.id.f853search_engine_icon_layout);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -1);
        layoutParams2.leftMargin = e.Pb().kp(R.dimen.f506add_sch_engineicon_layout_paddingleft);
        this.bQu.setLayoutParams(layoutParams2);
        this.bQj = (ImageView) this.bQf.findViewById(R.id.f854search_engine_icon);
        int kp = e.Pb().kp(R.dimen.f507add_sch_engineicon_size);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(kp, kp);
        layoutParams3.topMargin = e.Pb().kp(R.dimen.f508add_sch_engineicon_paddingtop);
        this.bQj.setLayoutParams(layoutParams3);
        this.bQu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (WebsiteSearchSchDialog.this.bQk.getVisibility() == 8) {
                    WebsiteSearchSchDialog.this.MO();
                } else {
                    boolean unused = WebsiteSearchSchDialog.this.MP();
                }
            }
        });
        this.bQn = new ArrayList();
        this.bQm = (RelativeLayout) this.bQf.findViewById(R.id.f860search_engine_list_icon);
        this.bQm.removeAllViews();
        this.bQm.setClickable(true);
        int kp2 = e.Pb().kp(R.dimen.f504add_sch_schengine_padding);
        this.bQm.setPadding(kp2, kp2, kp2, kp2);
        this.Rq = com.uc.a.e.nR().nZ();
        this.bQs = this.Rq.oy();
        this.bQt = this.Rq.oL();
        this.bQy = e.Pb().kp(R.dimen.f446add_sch_schengine_width);
        if (this.bQs != null) {
            if (this.bQs.size() == 0) {
            }
            i = this.bQx / this.bQy;
        } else {
            i = 0;
        }
        if (this.bQs != null) {
            for (int i2 = 0; i2 < this.bQs.size(); i2++) {
                LinearLayout linearLayout = new LinearLayout(this.cr);
                linearLayout.setId(i2);
                linearLayout.setOrientation(1);
                RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(this.bQy, e.Pb().kp(R.dimen.f448add_sch_schengine_height));
                layoutParams4.leftMargin = this.bQy * i2;
                this.bQm.addView(linearLayout, layoutParams4);
                ImageView imageView = new ImageView(this.cr);
                imageView.setId(100);
                int kp3 = e.Pb().kp(R.dimen.f505add_sch_schengine_image_size);
                LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(kp3, kp3);
                layoutParams5.setMargins(0, e.Pb().kp(R.dimen.f502add_sch_schengine_img_paddingtop), 0, e.Pb().kp(R.dimen.f503add_sch_schengine_img_paddingbottom));
                layoutParams5.gravity = 17;
                linearLayout.addView(imageView, layoutParams5);
                TextView textView = new TextView(this.cr);
                textView.setTextColor(e.Pb().getColor(30));
                textView.setTextSize(0, (float) e.Pb().kp(R.dimen.f501add_sch_schengine_text));
                textView.setSingleLine();
                textView.setId(101);
                LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(-2, -2);
                layoutParams6.gravity = 17;
                linearLayout.addView(textView, layoutParams6);
                linearLayout.setClickable(true);
                linearLayout.setFocusable(true);
                this.bQn.add(linearLayout);
            }
            for (int i3 = 0; i3 < this.bQs.size(); i3++) {
                b.a.a.e eVar = (b.a.a.e) this.bQs.get(i3);
                LinearLayout linearLayout2 = (LinearLayout) this.bQn.get(i3);
                BitmapDrawable bitmapDrawable = null;
                if (eVar.abN != null) {
                    bitmapDrawable = new BitmapDrawable(eVar.abN);
                }
                String str = eVar.abM;
                linearLayout2.findViewById(100).setBackgroundDrawable(bitmapDrawable);
                ((TextView) linearLayout2.findViewById(101)).setText(str);
                if (i3 == 0) {
                    linearLayout2.setNextFocusUpId(this.bQg.getId());
                }
                if (i3 > 0) {
                    linearLayout2.setNextFocusLeftId(((LinearLayout) this.bQn.get(i3 - 1)).getId());
                    linearLayout2.setNextFocusUpId(this.bQg.getId());
                }
                if (i3 < this.bQs.size() - 1) {
                    linearLayout2.setNextFocusRightId(((LinearLayout) this.bQn.get(i3 + 1)).getId());
                    linearLayout2.setNextFocusDownId(linearLayout2.getId());
                }
                if (i3 == this.bQs.size() - 1) {
                    linearLayout2.setNextFocusRightId(linearLayout2.getId());
                }
                if (i3 > i - 1) {
                    linearLayout2.setNextFocusUpId(((LinearLayout) this.bQn.get(i3 - i)).getId());
                    ((LinearLayout) this.bQn.get(i3 - i)).setNextFocusDownId(linearLayout2.getId());
                }
                linearLayout2.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aXi));
                if (i3 == this.bQt) {
                    this.bQj.setBackgroundDrawable(new BitmapDrawable(eVar.abN));
                }
                linearLayout2.setOnKeyListener(new View.OnKeyListener() {
                    public boolean onKey(View view, int i, KeyEvent keyEvent) {
                        if (i != 23 && i != 66) {
                            return false;
                        }
                        if (keyEvent.getAction() == 0) {
                            int k = WebsiteSearchSchDialog.this.k(view);
                            boolean unused = WebsiteSearchSchDialog.this.MP();
                            if (k != -1) {
                                WebsiteSearchSchDialog.this.bQj.setBackgroundDrawable(new BitmapDrawable(((b.a.a.e) WebsiteSearchSchDialog.this.bQs.get(k)).abN));
                            }
                        }
                        return true;
                    }
                });
                linearLayout2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        int k = WebsiteSearchSchDialog.this.k(view);
                        int unused = WebsiteSearchSchDialog.this.bQt = k;
                        boolean unused2 = WebsiteSearchSchDialog.this.MP();
                        if (k != -1) {
                            WebsiteSearchSchDialog.this.bQj.setBackgroundDrawable(new BitmapDrawable(((b.a.a.e) WebsiteSearchSchDialog.this.bQs.get(k)).abN));
                        }
                    }
                });
            }
        }
        LinearLayout.LayoutParams layoutParams7 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams7.leftMargin = e.Pb().kp(R.dimen.f509add_sch_engine_dropdown_paddingleft);
        this.bQC.setLayoutParams(layoutParams7);
        LinearLayout.LayoutParams layoutParams8 = new LinearLayout.LayoutParams(e.Pb().kp(R.dimen.f445add_sch_engine_divider_width), e.Pb().kp(R.dimen.f521add_sch_engine_divider_height));
        layoutParams8.leftMargin = e.Pb().kp(R.dimen.f522add_sch_engine_divider_marginleft);
        this.bQA.setLayoutParams(layoutParams8);
        LinearLayout.LayoutParams layoutParams9 = new LinearLayout.LayoutParams(-1, e.Pb().kp(R.dimen.f519add_sch_engine_text_height));
        layoutParams9.setMargins(e.Pb().kp(R.dimen.f518add_sch_engine_text_marginLeft), e.Pb().kp(R.dimen.f517add_sch_engine_text_marginTop), 0, 0);
        this.bQD.setLayoutParams(layoutParams9);
        this.bQD.setTextSize(0, (float) e.Pb().kp(R.dimen.f520add_sch_engine_textsize));
        this.bQD.setTextColor(e.Pb().getColor(30));
        this.bQi = (TextView) this.bQf.findViewById(R.id.f848search_bar_textView);
        this.bQi.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                WebsiteSearchSchDialog.this.hide();
            }
        });
        this.bQp = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.1f, 1, 0.0f);
        this.bQp.setDuration(100);
        this.bQq = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.1f, 1, 0.0f);
        this.bQq.setDuration(100);
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 4) {
            if (keyEvent.getAction() == 1) {
                return true;
            }
            if (MP()) {
                return true;
            }
            hide();
            return true;
        } else if (keyEvent.getKeyCode() == 84) {
            return true;
        } else {
            return super.dispatchKeyEvent(keyEvent);
        }
    }

    public void fv(String str) {
        if (ActivityBrowser.bqy) {
            getWindow().setFlags(1024, 3072);
        } else {
            getWindow().setFlags(2048, 3072);
        }
        l nZ = com.uc.a.e.nR().nZ();
        if (this.cv == null) {
            this.cv = new AdapterAutoComplete();
        }
        this.cv.aH(false);
        this.cv.i(com.uc.a.e.nR().oA());
        this.cD = false;
        this.cv.k(null);
        this.cv.bV("");
        this.bQr.setAdapter((ListAdapter) this.cv);
        this.bQt = nZ.oL();
        if (!(this.bQs == null || this.bQs.size() <= 0 || this.bQs.get(this.bQt) == null)) {
            this.bQj.setBackgroundDrawable(new BitmapDrawable(((b.a.a.e) this.bQs.get(this.bQt)).abN));
        }
        this.bQf.setBackgroundColor(e.Pb().getColor(112));
        this.bQg.removeTextChangedListener(this);
        if (str == null || str.length() <= 0) {
            this.bQg.setText("");
            this.bQh.setText(this.ct);
            this.bQo = true;
        } else {
            this.bQg.setText(str);
            this.bQg.selectAll();
            this.bQh.setText(this.cu);
            this.bQo = false;
        }
        this.bQg.addTextChangedListener(this);
        this.bQg.requestFocus();
        super.show();
        MQ();
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().aS(132);
        }
        if (this.cr.getResources().getConfiguration().orientation == 1) {
            this.cA.toggleSoftInput(0, 1);
        }
        MP();
    }

    public void fw(String str) {
        if (ActivityBrowser.bqy) {
            getWindow().setFlags(1024, 3072);
        } else {
            getWindow().setFlags(2048, 3072);
        }
        l nZ = com.uc.a.e.nR().nZ();
        if (this.cv == null) {
            this.cv = new AdapterAutoComplete();
        }
        this.cv.aH(false);
        this.cv.i(com.uc.a.e.nR().oA());
        this.bQr.setAdapter((ListAdapter) this.cv);
        this.bQt = nZ.oL();
        if (this.bQs != null) {
            if (this.bQt < this.bQs.size() && this.bQs.get(this.bQt) != null) {
                this.bQj.setBackgroundDrawable(new BitmapDrawable(((b.a.a.e) this.bQs.get(this.bQt)).abN));
            } else if (this.bQs.size() > 1) {
                this.bQj.setBackgroundDrawable(new BitmapDrawable(((b.a.a.e) this.bQs.get(0)).abN));
            }
        }
        this.bQf.setBackgroundColor(e.Pb().getColor(112));
        if (this.bQr.getCount() > 0) {
            this.bQr.setVisibility(0);
            this.bQg.setNextFocusDownId(this.bQr.getId());
        }
        this.bQg.addTextChangedListener(this);
        if (str == null || str.length() <= 0) {
            this.bQg.setText("");
            this.bQh.setText(this.ct);
            this.bQo = true;
        } else {
            this.bQg.setText(str);
            this.bQg.selectAll();
            this.bQh.setText(this.cu);
            this.bQo = false;
        }
        this.bQg.requestFocus();
        super.show();
        if (this.bxc) {
            MQ();
            this.bxc = false;
        }
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().aS(132);
        }
        MP();
    }

    public EditText getEditText() {
        return this.bQg;
    }

    public void hide() {
        dismiss();
        if (this.cB != null) {
            this.cB.onDismiss(null);
        }
    }

    public int k(View view) {
        return this.bQn.indexOf(view);
    }

    public void k() {
        this.bQw = e.Pb().getDrawable(UCR.drawable.aUn);
        this.bQk.setBackgroundDrawable(this.bQw);
        this.bQv = e.Pb().getDrawable(UCR.drawable.aXx);
        this.bQB.setBackgroundDrawable(this.bQv);
        this.bQr.setDivider(new ColorDrawable(e.Pb().getColor(113)));
        this.bQr.setDividerHeight(1);
        this.bQD = (TextView) this.bQf.findViewById(R.id.f858search_engine_text);
        this.cz = e.Pb().getDrawable(UCR.drawable.aXe);
        this.bQh.setBackgroundDrawable(this.cz);
        this.bQh.setTextColor(e.Pb().getColor(27));
        this.cy = e.Pb().getDrawable(UCR.drawable.aXf);
        this.bQg.setTextSize(0, (float) e.Pb().kp(R.dimen.f500add_sch_edittext));
        this.bQg.setBackgroundDrawable(this.cy);
        this.cw = e.Pb().kp(R.dimen.f526add_sch_sch_edittext_paddingleft);
        this.bQg.setPadding(this.cw, this.bQg.getPaddingTop(), this.cw / 3, this.bQg.getPaddingBottom());
        this.bQg.setTextColor(e.Pb().getColor(33));
        this.bQg.setHighlightColor(e.Pb().getColor(0));
        this.bQC.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aWk));
        this.bQA = (ImageView) this.bQf.findViewById(R.id.f859search_engine_divider);
        this.bQA.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aUo));
        if (this.bQs != null) {
            for (int i = 0; i < this.bQs.size(); i++) {
                ((LinearLayout) this.bQn.get(i)).setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aXi));
                ((TextView) ((LinearLayout) this.bQn.get(i)).findViewById(101)).setTextColor(e.Pb().getColor(30));
            }
        }
        this.bQD = (TextView) this.bQf.findViewById(R.id.f858search_engine_text);
        this.bQD.setTextColor(e.Pb().getColor(30));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(this.bQf);
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (this.bQg.getText().length() == 0) {
            if (!this.bQo) {
                this.bQh.setText(this.ct);
                this.bQo = true;
                this.bQg.setTextColor(e.Pb().getColor(33));
            }
        } else if (this.bQo) {
            this.bQh.setText(this.cu);
            this.bQo = false;
        }
    }
}
