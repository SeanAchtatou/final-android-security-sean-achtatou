package com.uc.browser;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import b.a.a.d;
import com.uc.a.e;
import com.uc.a.h;
import com.uc.browser.UCAlertDialog;
import com.uc.browser.UCR;

public class ActivityDownloadDialog extends Activity implements View.OnClickListener {
    String aU;
    String bCP;
    String bCQ;
    int bCR = -1;
    String name;

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 4) {
            this.aU = null;
            this.bCP = null;
            finish();
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 86 && i2 == -1) {
            finish();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.string.cancel /*2131296272*/:
                this.aU = null;
                this.bCP = null;
                finish();
                return;
            case R.string.ok /*2131296646*/:
                if (this.bCR == 4 || this.bCR == 3 || this.bCR == 2) {
                    if (ModelBrowser.gD() != null) {
                        ModelBrowser.gD().aS(ModelBrowser.zK);
                    }
                } else if (this.name != null && this.name.toLowerCase().endsWith(".apk")) {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.addFlags(268435456);
                    intent.setDataAndType(Uri.parse("file://" + this.bCQ + this.name), "application/vnd.android.package-archive");
                    startActivity(intent);
                } else if (this.name == null || !this.name.toLowerCase().endsWith(".uct")) {
                    d.a(ModelBrowser.gD().gm(), this.bCQ + this.name, d.cu(this.name));
                } else if (ModelBrowser.gD() != null) {
                    String bw = e.nR().nY().bw(h.afm);
                    if (!ModelBrowser.gD().gm().isFinishing()) {
                        d.f(ModelBrowser.gD().gm(), bw + this.name);
                    } else if (ModelBrowser.gD().getContext() != null) {
                        d.f(ModelBrowser.gD().getContext(), bw + this.name);
                    }
                }
                finish();
                return;
            default:
                finish();
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        int kp;
        int kp2;
        int i;
        int kp3;
        super.onCreate(bundle);
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra(ActivityEditMyNavi.bvj) != null) {
                this.aU = intent.getStringExtra(ActivityEditMyNavi.bvj);
            }
            if (intent.getStringExtra("message") != null) {
                this.bCP = intent.getStringExtra("message");
            }
            if (intent.getStringExtra("fileName") != null) {
                this.name = intent.getStringExtra("fileName");
            }
            if (intent.getStringExtra("filePath") != null) {
                this.bCQ = intent.getStringExtra("filePath");
            }
            if (intent.getStringExtra("safestatus") != null) {
                this.bCR = Integer.parseInt(intent.getStringExtra("safestatus"));
            }
        }
        com.uc.h.e Pb = com.uc.h.e.Pb();
        RelativeLayout relativeLayout = new RelativeLayout(this);
        if (getWindowManager().getDefaultDisplay().getWidth() > getWindowManager().getDefaultDisplay().getHeight()) {
            int kp4 = Pb.kp(R.dimen.f318dialog_frame_padding_left_land);
            kp = Pb.kp(R.dimen.f319dialog_frame_padding_right_land);
            kp2 = Pb.kp(R.dimen.f317dialog_frame_padding_top_land);
            i = kp4;
            kp3 = Pb.kp(R.dimen.f320dialog_frame_padding_bottom_land);
        } else {
            int kp5 = Pb.kp(R.dimen.f310dialog_frame_padding_left);
            kp = Pb.kp(R.dimen.f311dialog_frame_padding_right);
            kp2 = Pb.kp(R.dimen.f309dialog_frame_padding_top);
            i = kp5;
            kp3 = Pb.kp(R.dimen.f312dialog_frame_padding_bottom);
        }
        relativeLayout.setPadding(i, kp2, kp, kp3);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(1);
        linearLayout.setBackgroundDrawable(Pb.getDrawable(UCR.drawable.aUm));
        relativeLayout.addView(linearLayout, new RelativeLayout.LayoutParams(-1, -2));
        linearLayout.setPadding(Pb.kp(R.dimen.f305dialog_padding_left), Pb.kp(R.dimen.f304dialog_padding_top), Pb.kp(R.dimen.f306dialog_padding_right), Pb.kp(R.dimen.f307dialog_padding_bottom));
        TextView textView = new TextView(this);
        textView.setText(this.aU);
        textView.setTextColor(com.uc.h.e.Pb().getColor(7));
        textView.setTextSize(21.0f);
        textView.setCompoundDrawablePadding(3);
        textView.setCompoundDrawablesWithIntrinsicBounds(Pb.getDrawable(UCR.drawable.aUJ), (Drawable) null, (Drawable) null, (Drawable) null);
        linearLayout.addView(textView, new LinearLayout.LayoutParams(-1, -2));
        UCAlertDialog.MyImageView myImageView = new UCAlertDialog.MyImageView(this);
        myImageView.setBackgroundDrawable(Pb.getDrawable(UCR.drawable.aUo));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, 2);
        layoutParams.setMargins(0, 5, 0, 0);
        linearLayout.addView(myImageView, layoutParams);
        TextView textView2 = new TextView(this);
        textView2.setText(this.bCP);
        textView2.setTextColor(com.uc.h.e.Pb().getColor(7));
        textView2.setTextSize(18.0f);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.setMargins(0, Pb.kp(R.dimen.f308dialog_layout_margin), 0, 0);
        textView2.setPadding(10, 0, 10, 0);
        linearLayout.addView(textView2, layoutParams2);
        LinearLayout linearLayout2 = new LinearLayout(this);
        linearLayout2.setOrientation(0);
        linearLayout2.setGravity(17);
        int kp6 = Pb.kp(R.dimen.f300dialog_button_height);
        int kp7 = Pb.kp(R.dimen.f299dialog_button_width);
        int kp8 = Pb.kp(R.dimen.f301dialog_button_margin);
        UCButton uCButton = new UCButton(this);
        uCButton.setId(R.string.ok);
        uCButton.setText((int) R.string.ok);
        uCButton.setOnClickListener(this);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(kp7, kp6, 1.0f);
        layoutParams3.rightMargin = kp8;
        layoutParams3.leftMargin = kp8;
        linearLayout2.addView(uCButton, layoutParams3);
        UCButton uCButton2 = new UCButton(this);
        uCButton2.setId(R.string.cancel);
        uCButton2.setText((int) R.string.cancel);
        uCButton2.setOnClickListener(this);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(kp7, kp6, 1.0f);
        layoutParams4.rightMargin = kp8;
        layoutParams4.leftMargin = kp8;
        linearLayout2.addView(uCButton2, layoutParams4);
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams5.setMargins(0, Pb.kp(R.dimen.f308dialog_layout_margin), 0, 0);
        layoutParams5.gravity = 17;
        linearLayout.addView(linearLayout2);
        setContentView(relativeLayout);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i != 84) {
            return super.onKeyUp(i, keyEvent);
        }
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a((int) ModelBrowser.zJ, this);
        }
        return true;
    }
}
