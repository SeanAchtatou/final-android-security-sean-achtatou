package com.uc.browser;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.RemoteViews;
import com.uc.c.as;
import com.uc.c.cd;
import java.util.Calendar;
import java.util.HashMap;

public class NotificationMgr {
    private HashMap aDK = new HashMap();
    /* access modifiers changed from: private */
    public NotificationManager aDL;
    private PendingIntent aDM;
    private Context cr;

    class updateDownloadProgress extends AsyncTask {
        private int id;
        private Notification pU;

        public updateDownloadProgress(int i, Notification notification) {
            this.id = i;
            this.pU = notification;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Void doInBackground(Void... voidArr) {
            NotificationMgr.this.aDL.notify(this.id, this.pU);
            return null;
        }
    }

    public NotificationMgr(Context context, PendingIntent pendingIntent) {
        this.cr = context;
        this.aDM = pendingIntent;
        this.aDL = (NotificationManager) context.getSystemService("notification");
    }

    private String yT() {
        Calendar instance = Calendar.getInstance();
        int i = instance.get(10);
        int i2 = instance.get(12);
        return instance.get(9) == 0 ? "" + i + cd.bVI + i2 + " AM" : "" + i + cd.bVI + i2 + " PM";
    }

    public void a(int i, String str, int i2) {
        Notification notification = (Notification) this.aDK.get(Integer.valueOf(i));
        if (notification == null || !(notification == null || notification.contentView.getLayoutId() == R.layout.f892downlaod_sys_notification)) {
            notification = new Notification();
            notification.icon = 17301633;
            RemoteViews remoteViews = new RemoteViews(this.cr.getPackageName(), (int) R.layout.f892downlaod_sys_notification);
            remoteViews.setTextViewText(R.id.f730downloadTaskName, str);
            remoteViews.setProgressBar(R.id.f732downloadProgressBar, 100, 0, false);
            notification.contentView = remoteViews;
            notification.contentIntent = this.aDM;
            new updateDownloadProgress(i, notification).execute(new Void[0]);
            this.aDK.remove(Integer.valueOf(i));
            this.aDK.put(Integer.valueOf(i), notification);
        } else {
            notification.contentView.setTextViewText(R.id.f731downloadProgressText, String.valueOf(i2) + as.azr);
            notification.contentView.setProgressBar(R.id.f732downloadProgressBar, 100, i2, false);
        }
        try {
            if (this.aDK.containsKey(Integer.valueOf(i))) {
                new updateDownloadProgress(i, notification).execute(new Void[0]);
            }
        } catch (Exception e) {
        }
    }

    public void cancel(int i) {
        this.aDL.cancel(i);
        this.aDK.remove(Integer.valueOf(i));
    }

    public void n(int i, String str) {
        Notification notification = (Notification) this.aDK.get(Integer.valueOf(i));
        if (notification != null) {
            notification.icon = 17301634;
            RemoteViews remoteViews = new RemoteViews(this.cr.getPackageName(), (int) R.layout.f893downlaod_sys_notification_complete);
            remoteViews.setTextViewText(R.id.f730downloadTaskName, str);
            notification.contentView = remoteViews;
            remoteViews.setTextViewText(R.id.f733completeTimeText, yT());
        }
        try {
            if (this.aDK.containsKey(Integer.valueOf(i))) {
                this.aDL.notify(i, notification);
            }
        } catch (Exception e) {
        }
    }

    public void yU() {
        try {
            if (this.aDL != null) {
                this.aDL.cancelAll();
            }
            this.aDK.clear();
        } catch (Exception e) {
        }
    }
}
