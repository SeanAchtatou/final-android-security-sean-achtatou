package com.uc.browser;

import android.content.Context;
import android.graphics.Canvas;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import b.a.a.a.f;
import b.a.a.a.u;
import com.uc.a.a;
import com.uc.a.d;
import com.uc.a.e;
import com.uc.a.n;
import com.uc.browser.UCR;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

public class ViewDownload extends View {
    public static a ag = null;
    /* access modifiers changed from: private */
    public d ah;
    private u ai;
    private f aj;
    /* access modifiers changed from: private */
    public ActivityDownload ak = null;
    /* access modifiers changed from: private */
    public boolean al = true;
    /* access modifiers changed from: private */
    public boolean am = true;
    private boolean an = true;
    private int ao;
    private int ap;
    public Timer aq = null;
    private int ar = -1;
    private int as = -1;
    private boolean at = false;
    private n p = new n() {
        public void Dh() {
            ViewDownload.this.ak.aS(6);
        }

        public n Di() {
            ModelBrowser gD = ModelBrowser.gD();
            if (gD != null) {
                return gD.ch();
            }
            return null;
        }

        public void a(String str, String str2, String str3, String str4, int i) {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().a(136, new String[]{str, str2, str3, str4, String.valueOf(i)});
            }
        }

        public void aD() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(ModelBrowser.zY);
            }
        }

        public InputStream ag(String str) {
            if (ModelBrowser.gD() != null) {
                return ModelBrowser.gD().ag(str);
            }
            return null;
        }

        public void bN(String str) {
            if (ViewDownload.this.ak != null) {
                ViewDownload.this.ak.a(9, str);
            }
        }

        public void bR(String str) {
            ViewDownload.this.ak.a(8, str);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.uc.browser.ViewDownload.b(com.uc.browser.ViewDownload, boolean):boolean
         arg types: [com.uc.browser.ViewDownload, int]
         candidates:
          com.uc.browser.ViewDownload.b(int, int):void
          com.uc.browser.ViewDownload.b(com.uc.browser.ViewDownload, boolean):boolean */
        public boolean bw(boolean z) {
            boolean unused = ViewDownload.this.al = z;
            if (!ViewDownload.this.al) {
                boolean unused2 = ViewDownload.this.am = false;
            }
            return false;
        }

        public void dP(int i) {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().a(139, Integer.valueOf(i));
            }
        }

        public boolean gR(int i) {
            if (ViewDownload.this.ah == null || !ViewDownload.this.am || ViewDownload.this.ak == null) {
                return false;
            }
            ViewDownload.this.ak.a(1, Integer.valueOf(i));
            return false;
        }

        public void i(int i, String str) {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().a(137, i, str);
            }
        }

        public void j(int i, String str) {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().a(140, i, str);
            }
        }

        public void q(Object obj) {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().a(138, obj);
            }
        }

        public boolean qW() {
            if (ViewDownload.this.ak == null || !ViewDownload.this.ak.Kq()) {
                return false;
            }
            ViewDownload.this.postInvalidate();
            return false;
        }

        public void rb() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(141);
            }
        }

        public void rc() {
        }

        public void rd() {
            ViewDownload.this.ak.a(2, (Object) null);
        }

        public boolean shouldOverrideUrlLoading(String str) {
            if (ModelBrowser.gD() == null || true != ModelBrowser.gD().hj().wg()) {
                ViewDownload.this.ak.a(3, (Object) null);
                return false;
            }
            ModelBrowser.gD().a(39, 1, str);
            ViewDownload.this.ak.cj(true);
            ViewDownload.this.ak.finish();
            e.nR().aN();
            return true;
        }
    };

    class createContextMenuTask extends TimerTask {
        LoadingProgressBar bru = null;

        public createContextMenuTask() {
        }

        public void run() {
            if (ViewDownload.this.ak != null) {
                ViewDownload.this.ak.aS(4);
            }
            ViewDownload.this.aF();
        }
    }

    public ViewDownload(Context context, int i, int i2) {
        super(context);
        a(context, i, i2);
    }

    public boolean B() {
        if (ag == null) {
            return false;
        }
        return ag.B();
    }

    public void R() {
        if (ag != null) {
            ag.R();
        }
    }

    public short Z() {
        if (ag != null) {
            return ag.Z();
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public void a(Context context, int i, int i2) {
        this.ah = e.nR().nX();
        if (this.ah != null) {
            com.uc.h.e Pb = com.uc.h.e.Pb();
            if (i > 0 && i2 > 0) {
                this.ao = i;
                this.ap = i2;
            }
            e.nR().nX().b(this.ao, this.ap);
            ag = this.ah.c(this.p);
            ag.a(this.p);
            this.ah.gk();
            this.ah.a(0, getResources(), R.drawable.f565fileicon_exe);
            this.ah.a(1, getResources(), R.drawable.f562fileicon_default);
            this.ah.a(3, getResources(), R.drawable.f560fileicon_audio);
            this.ah.a(5, Pb.kl(UCR.drawable.aTD));
            this.ah.a(6, getResources(), UCR.drawable.aUK);
            this.ah.a(7, getResources(), UCR.drawable.aUL);
            this.ah.a(8, getResources(), UCR.drawable.aUt);
            this.ah.a(11, getResources(), UCR.drawable.aTE);
            this.ah.a(12, getResources(), UCR.drawable.aUu);
            this.ah.a(13, getResources(), R.drawable.f626safe_not_scan);
            this.ah.a(14, getResources(), R.drawable.f628safe_scanning);
            this.ah.a(15, getResources(), R.drawable.f627safe_safe);
            this.ah.a(16, getResources(), R.drawable.f625safe_no_info);
            this.ah.a(17, getResources(), R.drawable.f624safe_malware);
            this.ah.a(18, getResources(), R.drawable.f555download_waiting);
            this.ah.a(19, getResources(), R.drawable.f551download_downloading);
            this.ah.a(20, getResources(), R.drawable.f554download_paused);
            this.ah.a(21, getResources(), R.drawable.f552download_error);
            this.ah.x(0, Pb.getColor(15));
            this.ah.x(1, Pb.getColor(16));
            this.ak = (ActivityDownload) context;
            aT();
        }
    }

    public void a(short s) {
        if (ag != null) {
            ag.a(s);
        }
    }

    public void a(String[] strArr) {
        this.ah.a(strArr);
    }

    public void aE() {
        if (this.aq == null) {
            this.aq = new Timer();
            if (this.aq != null) {
                this.aq.schedule(new createContextMenuTask(), 500, 500);
            }
        }
    }

    public void aF() {
        if (this.aq != null) {
            this.aq.cancel();
        }
        this.aq = null;
    }

    public void aG() {
        this.ai = null;
        this.aj = null;
    }

    public void aH() {
        this.ah.aH();
    }

    public boolean aI() {
        if (this.ah != null) {
            return this.ah.aI();
        }
        return true;
    }

    public void aJ() {
        this.ah.aJ();
    }

    public void aK() {
        this.ah.aK();
    }

    public void aL() {
        this.ah.aL();
    }

    public void aM() {
        this.ah.aM();
    }

    public void aN() {
        if (this.ah != null) {
            this.ah.gj();
        }
    }

    public byte aO() {
        return this.ah.aO();
    }

    public byte aP() {
        return this.ah.aP();
    }

    public void aQ() {
        this.p.rb();
    }

    public short aR() {
        return this.ah.gd();
    }

    public void aS() {
        short aR = aR();
        if (aR != -1) {
            this.p.dP(aR);
        }
    }

    public void aT() {
        if (ag != null) {
            short Z = Z();
            ag.c(getWidth() - 20 > ModelBrowser.gD().az() ? 2 : 1);
            a(Z);
        }
    }

    public void b(int i, int i2) {
        if (i > 0 && i2 > 0) {
            this.ao = i;
            this.ap = i2;
        }
        if (ag != null) {
            ag.b(i, i2);
        }
        e.du(i);
    }

    public void b(boolean z) {
        this.an = z;
    }

    public void c(boolean z) {
        this.ah.u(z);
    }

    public void d(boolean z) {
        this.ah.d(z);
    }

    public void e(boolean z) {
        this.ah.e(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.a.a.a(b.a.a.a.u, int, int, boolean, boolean):boolean
     arg types: [b.a.a.a.u, int, int, int, int]
     candidates:
      com.uc.a.a.a(java.lang.String, java.lang.String, com.uc.a.n, boolean, java.lang.String):boolean
      com.uc.a.a.a(b.a.a.a.u, int, int, boolean, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        try {
            if (this.at) {
                requestLayout();
            }
            if (this.aj == null || this.aj.getHeight() != getHeight()) {
                this.aj = f.a(getWidth(), getHeight(), canvas);
            }
            this.ai = this.aj.jU();
            this.ai.save();
            ag.a(this.ai, this.ao, this.ap, false, false);
            if (this.al) {
                this.am = true;
            }
            this.ai.restore();
        } catch (Throwable th) {
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (23 != i || this.ak == null) {
            ag.a(i, keyEvent);
            return true;
        }
        this.ak.a(1, Integer.valueOf(ag.S()));
        return true;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        ag.b(i, keyEvent);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        b(i, i2);
        aT();
        this.at = true;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        if (1 == motionEvent.getAction()) {
            aF();
            this.ak.aS(5);
            this.ar = -1;
            this.as = -1;
        } else if (motionEvent.getAction() == 0) {
            aF();
            aE();
            this.an = true;
            this.ar = (int) motionEvent.getX();
            this.as = (int) motionEvent.getY();
        } else if (2 == motionEvent.getAction() && (Math.abs(this.ar - ((int) motionEvent.getX())) > 20 || Math.abs(this.as - ((int) motionEvent.getY())) > 20)) {
            aF();
        }
        if ((motionEvent.getAction() == 0 || ((2 == motionEvent.getAction() && true == this.an) || (1 == motionEvent.getAction() && true == this.an))) && ag != null) {
            ag.a(motionEvent);
        }
        if (1 == motionEvent.getAction()) {
            this.an = true;
        }
        return true;
    }
}
