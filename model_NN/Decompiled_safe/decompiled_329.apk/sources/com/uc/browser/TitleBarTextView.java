package com.uc.browser;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import com.uc.browser.UCR;
import com.uc.h.a;
import com.uc.h.e;

public class TitleBarTextView extends TextView implements a {
    public TitleBarTextView(Context context) {
        super(context);
        k();
    }

    public TitleBarTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        k();
    }

    public TitleBarTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        k();
    }

    public void k() {
        setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aWw));
        setTextColor(e.Pb().getColor(82));
    }
}
