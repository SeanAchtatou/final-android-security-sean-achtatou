package com.uc.browser;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import b.a.a.d;
import com.uc.browser.UCAlertDialog;
import com.uc.browser.UCR;
import com.uc.c.au;
import com.uc.d.g;
import com.uc.e.ac;
import com.uc.e.h;
import com.uc.e.z;
import com.uc.h.a;
import com.uc.h.e;
import com.uc.plugin.ad;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

public class ActivityFileMaintain extends ActivityWithUCMenu implements View.OnClickListener, AdapterView.OnItemClickListener, h, a {
    public static final int SUCCESS = 0;
    public static final int aAa = 3;
    public static final int aAb = 4;
    public static final int aAc = 5;
    public static final int aAd = 6;
    public static final int aAe = 7;
    private static final int azD = 0;
    private static final int azE = 1;
    private static final int azF = 2;
    private static final String azG = "show_type";
    private static final int azI = 0;
    private static final int azJ = 1;
    private static final String azK = "file_vector_temp";
    private static final String azL = "file_edit_state";
    private static final String azM = "file_selecting";
    public static final String azN = "file_show_type";
    public static final String azO = "pref_file_show_type";
    public static final int azQ = 240;
    private static final int azU = 0;
    private static final int azV = 1;
    private static final int azW = 2;
    private static final int azX = 4;
    public static final int azY = 1;
    public static final int azZ = 2;
    private static final String uB = "file_root_temp";
    /* access modifiers changed from: private */
    public Dialog aAf = null;
    private AdapterView azA;
    /* access modifiers changed from: private */
    public FileItem azB;
    private Vector azC;
    private int azH;
    private boolean azP = false;
    private View azR = null;
    private int azS = -14055718;
    private int azT = -1;
    private ac azy;
    private ac azz;
    Handler handler = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    ActivityFileMaintain.this.yh();
                    return;
                case 1:
                    if (ActivityFileMaintain.this.aAf != null) {
                        ActivityFileMaintain.this.aAf.dismiss();
                    }
                    ActivityFileMaintain.this.ur.EN();
                    return;
                case 2:
                    if (ActivityFileMaintain.this.aAf != null) {
                        ActivityFileMaintain.this.aAf.dismiss();
                    }
                    ActivityFileMaintain.this.ur.EN();
                    Toast.makeText(ActivityFileMaintain.this, (int) R.string.f1152dialog_msg_sdcard_no_space, 0).show();
                    return;
                case 3:
                default:
                    return;
                case 4:
                    ((InputMethodManager) ActivityFileMaintain.this.getSystemService("input_method")).toggleSoftInput(0, 1);
                    return;
            }
        }
    };
    private int state = 0;
    private File uD;
    private RelativeLayout ul;
    private BarLayout um;
    private ac uo;
    private ac up;
    /* access modifiers changed from: private */
    public AdapterFile ur;

    class CopyThread extends Thread {
        File bdA;
        String bdB;

        public CopyThread(File file, String str) {
            this.bdA = file;
            this.bdB = str;
        }

        public void run() {
            ActivityFileMaintain.this.handler.sendMessage(ActivityFileMaintain.this.handler.obtainMessage(0));
            if (4 == ActivityFileMaintain.d(this.bdA, this.bdB)) {
                ActivityFileMaintain.this.handler.sendMessage(ActivityFileMaintain.this.handler.obtainMessage(2));
            }
            ActivityFileMaintain.this.handler.sendMessage(ActivityFileMaintain.this.handler.obtainMessage(1));
        }
    }

    public static float a(File file, float f) {
        float f2;
        if (file == null || !file.exists()) {
            return f;
        }
        if (!file.isDirectory()) {
            return ((float) file.length()) + f;
        }
        if (file.listFiles() != null) {
            float f3 = 0.0f;
            for (File a2 : file.listFiles()) {
                f3 += a(a2, 0.0f);
            }
            f2 = f3;
        } else {
            f2 = 0.0f;
        }
        return f2 + f;
    }

    public static int a(File file, String str) {
        if (file == null || !file.exists()) {
            return 1;
        }
        File file2 = new File(file.getParent() + au.aGF + str);
        if (file2.exists()) {
            return 2;
        }
        if (!file.renameTo(file2)) {
            return 5;
        }
        file2.setLastModified(System.currentTimeMillis());
        return 0;
    }

    private Dialog a(Context context, final File file, final String str) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.aH(R.string.f1162dialog_title_replace_file);
        builder.aG(R.string.f1163dialog_msg_replace_file);
        builder.a((int) R.string.f1266dlg_button_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ActivityFileMaintain.c(file, str);
                ActivityFileMaintain.f(file);
                ActivityFileMaintain.this.ur.EN();
            }
        });
        builder.c((int) R.string.f1267dlg_button_cancle, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return builder.fh();
    }

    public static void a(File file, File file2) {
        FileInputStream fileInputStream = new FileInputStream(file);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        FileOutputStream fileOutputStream = new FileOutputStream(file2);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
        byte[] bArr = new byte[8192];
        while (true) {
            try {
                int read = bufferedInputStream.read(bArr);
                if (read != -1) {
                    bufferedOutputStream.write(bArr, 0, read);
                } else {
                    return;
                }
            } finally {
                bufferedOutputStream.flush();
                bufferedInputStream.close();
                fileInputStream.close();
                bufferedOutputStream.close();
                fileOutputStream.close();
            }
        }
    }

    public static int b(File file, String str) {
        if (file == null || !file.exists()) {
            return 1;
        }
        if (!file.canWrite()) {
            return 7;
        }
        String str2 = str + au.aGF + file.getName();
        if (file.getAbsolutePath().equals(str2)) {
            return 0;
        }
        if (file.isDirectory()) {
            File file2 = new File(str + au.aGF + file.getName());
            if (file2.exists()) {
                return 2;
            }
            file2.mkdir();
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                for (File b2 : listFiles) {
                    b(b2, str2);
                }
            }
            return 0;
        }
        File file3 = new File(str + au.aGF + file.getName());
        if (file3.exists()) {
            return 2;
        }
        try {
            a(file, file3);
            return 0;
        } catch (IOException e) {
            return 5;
        }
    }

    private Vector b(char[] cArr) {
        if (cArr == null || cArr.length <= 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(cArr);
        File file = new File(stringBuffer.toString());
        if (file == null || !file.exists()) {
            return null;
        }
        Vector vector = new Vector();
        vector.add(file);
        return vector;
    }

    public static int c(File file, String str) {
        if (file == null || !file.exists()) {
            return 1;
        }
        if (file.isDirectory()) {
            File file2 = new File(str + au.aGF + file.getName());
            if (!file2.exists()) {
                file2.mkdir();
            }
            String str2 = str + au.aGF + file.getName();
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                for (File b2 : listFiles) {
                    b(b2, str2);
                }
            }
            return 0;
        }
        File file3 = new File(str + au.aGF + file.getName());
        if (file3.exists()) {
            f(file3);
        }
        try {
            a(file, file3);
            return 0;
        } catch (IOException e) {
            return 5;
        }
    }

    public static int d(File file, String str) {
        File file2;
        String str2;
        if (file == null || !file.exists()) {
            return 1;
        }
        File file3 = new File(str + au.aGF + file.getName());
        if (file3.exists()) {
            String name = file.getName();
            if (-1 != name.lastIndexOf(46)) {
                String substring = name.substring(name.lastIndexOf(46));
                str2 = name.substring(0, name.lastIndexOf(46));
                name = substring;
            } else {
                str2 = "";
            }
            StringBuffer stringBuffer = new StringBuffer();
            File file4 = file3;
            int i = 1;
            while (file4.exists()) {
                stringBuffer.delete(0, stringBuffer.length());
                stringBuffer.append(str).append(au.aGF).append(str2).append("(").append(i).append(")").append(name);
                i++;
                file4 = new File(stringBuffer.toString());
            }
            file2 = file4;
        } else {
            file2 = file3;
        }
        if (!file.isDirectory()) {
            try {
                a(file, file2);
            } catch (Exception e) {
                return 4;
            }
        } else if (!file2.mkdir()) {
            return 3;
        } else {
            if (file.listFiles() != null) {
                for (File d : file.listFiles()) {
                    d(d, file2.getPath());
                }
            }
        }
        return 0;
    }

    private void dd(final String str) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this);
        builder.aH(R.string.f970tip);
        builder.setCancelable(false);
        builder.aG(R.string.f1461plugin_install_query);
        builder.a((int) R.string.f965confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ad.h(ActivityFileMaintain.this, str);
            }
        }).c((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.fh();
        builder.show();
    }

    public static int de(String str) {
        File file = new File(str);
        if (file.exists()) {
            return 2;
        }
        return file.mkdir() ? 0 : 5;
    }

    private File e(File file) {
        File ER = this.ur.ER();
        switch (this.state) {
            case 1:
                if (file == null || !file.isDirectory() || !ER.getAbsolutePath().startsWith(file.getAbsolutePath())) {
                    new CopyThread(file, ER.getPath()).start();
                    break;
                } else {
                    Toast.makeText(this, (int) R.string.f1146dialog_msg_filecopy_error, 0).show();
                    return null;
                }
                break;
            case 2:
                if (file != null && file.isDirectory() && ER.getAbsolutePath().startsWith(file.getAbsolutePath())) {
                    Toast.makeText(this, (int) R.string.f1147dialog_msg_filecut_error, 0).show();
                    return null;
                } else if (file == null || !file.getParentFile().equals(ER)) {
                    int b2 = b(file, ER.getPath());
                    if (b2 == 2) {
                        a(this, file, ER.getPath()).show();
                    }
                    if (b2 != 0) {
                        if (b2 == 7) {
                            Toast.makeText(this, (int) R.string.f977file_opera_failed, 0).show();
                            break;
                        }
                    } else {
                        if (!file.getAbsolutePath().equals(ER.getPath() + au.aGF + file.getName())) {
                            int f = f(file);
                            if (f != 6) {
                                if (f == 7) {
                                    Toast.makeText(this, (int) R.string.f977file_opera_failed, 0).show();
                                    break;
                                }
                            } else {
                                Toast.makeText(this, (int) R.string.f977file_opera_failed, 0).show();
                                break;
                            }
                        } else {
                            return null;
                        }
                    }
                } else {
                    return null;
                }
                break;
        }
        this.ur.n(ER);
        return null;
    }

    public static int f(File file) {
        int i;
        File[] listFiles;
        if (file == null || !file.exists()) {
            return 7;
        }
        if (!file.isDirectory() || (listFiles = file.listFiles()) == null) {
            i = 0;
        } else {
            int i2 = 0;
            for (File f : listFiles) {
                if (i2 != f(f)) {
                    i2 = 6;
                }
            }
            i = i2;
        }
        int i3 = file.delete() ? 0 : 7;
        if (i3 == i) {
            return i3;
        }
        return 6;
    }

    private Dialog f(final Context context) {
        LayoutInflater from = LayoutInflater.from(context);
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        View inflate = from.inflate((int) R.layout.f882bookmark_dialog_newfolder, (ViewGroup) null);
        final EditText editText = (EditText) inflate.findViewById(R.id.f690bookmark_dlg_foldername);
        ((TextView) inflate.findViewById(R.id.f689bookmark_dlg_name)).setTextColor(e.Pb().getColor(78));
        builder.L(getResources().getString(R.string.f1144dialog_title_makedir));
        builder.c(inflate);
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                String trim = editText.getText().toString().trim();
                if (trim != null && trim.length() > 0 && trim.getBytes().length <= 240) {
                    int de = ActivityFileMaintain.de(ActivityFileMaintain.this.ur.ER().getPath() + au.aGF + trim);
                    if (de == 0) {
                        ActivityFileMaintain.this.ur.EN();
                        dialogInterface.dismiss();
                    } else if (2 == de) {
                        Toast.makeText(context, (int) R.string.f1148dialog_msg_filename_dulplicate, 0).show();
                    } else if (!com.uc.a.e.nR().ae(trim.toString())) {
                        Toast.makeText(context, "目录不能包含下列任何字符\\/:*?\"<>|", 0).show();
                    } else {
                        Toast.makeText(context, (int) R.string.f1149dialog_msg_folder_create_failure, 0).show();
                        dialogInterface.dismiss();
                    }
                } else if (240 < trim.getBytes().length) {
                    Toast.makeText(ActivityFileMaintain.this, (int) R.string.f1153text_max_size, 0).show();
                } else {
                    editText.setText(trim);
                    Toast.makeText(context, (int) R.string.f1145dialog_msg_filename_cannot_null, 0).show();
                }
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        return builder.fh();
    }

    private boolean fz() {
        if (this.ur == null) {
            return false;
        }
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return false;
        }
        File ER = this.ur.ER();
        if (ER == null) {
            return false;
        }
        File parentFile = ER.getParentFile();
        if (parentFile == null || !parentFile.exists() || !parentFile.getPath().startsWith("/sdcard")) {
            return false;
        }
        this.ur.n(parentFile);
        this.uD = parentFile;
        t(parentFile.getPath());
        return true;
    }

    private char[] o(Vector vector) {
        if (vector == null || vector.size() <= 0) {
            return null;
        }
        return ((File) vector.elementAt(0)).getPath().toCharArray();
    }

    private Dialog t(final Context context) {
        LayoutInflater from = LayoutInflater.from(this);
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.aH(R.string.f1157dialog_title_rename);
        View inflate = from.inflate((int) R.layout.f882bookmark_dialog_newfolder, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.f689bookmark_dlg_name)).setTextColor(e.Pb().getColor(78));
        final EditText editText = (EditText) inflate.findViewById(R.id.f690bookmark_dlg_foldername);
        final File file = this.azB.blw;
        builder.c(inflate);
        if (this.azB != null) {
            editText.setText(this.azB.blw.getName());
            editText.setSelection(0, editText.length());
        }
        builder.a((int) R.string.f1266dlg_button_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                String obj = editText.getText().toString();
                int length = obj.trim().getBytes().length;
                int a2 = ActivityFileMaintain.a(file, obj);
                if (obj == null || length == 0) {
                    Toast.makeText(context, (int) R.string.f1154dialog_msg_rename_failure, 0).show();
                } else if (2 == a2) {
                    Toast.makeText(context, (int) R.string.f1154dialog_msg_rename_failure, 0).show();
                } else if (!com.uc.a.e.nR().ae(obj.toString())) {
                    Toast.makeText(context, (int) R.string.f1154dialog_msg_rename_failure, 0).show();
                } else if (240 < length) {
                    Toast.makeText(context, (int) R.string.f1154dialog_msg_rename_failure, 0).show();
                } else if (a2 == 0) {
                    ActivityFileMaintain.this.ur.EN();
                } else if (!file.canWrite()) {
                    Toast.makeText(context, (int) R.string.f977file_opera_failed, 0).show();
                } else {
                    Toast.makeText(context, (int) R.string.f1154dialog_msg_rename_failure, 0).show();
                }
                ActivityFileMaintain.this.yg();
            }
        });
        builder.c((int) R.string.f1267dlg_button_cancle, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ActivityFileMaintain.this.yg();
            }
        });
        return builder.fh();
    }

    private void t(String str) {
        TextView textView = (TextView) findViewById(R.id.f654Browser_TitleBar);
        if (textView != null) {
            textView.setText(str);
        }
    }

    private Dialog u(Context context) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.aH(R.string.f1155dialog_title_deletefile);
        String string = getString(R.string.f1156dialog_msg_deletefile);
        if (!(this.azB == null || this.azB.blw == null)) {
            string = (this.azB.blw.isDirectory() ? getString(R.string.f1165dialog_msg_deletefolder) : getString(R.string.f1156dialog_msg_deletefile)) + "\"" + this.azB.blw.getName() + "\"?";
        }
        builder.M(string);
        builder.a((int) R.string.f1266dlg_button_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ActivityFileMaintain.this.yg();
                if (ActivityFileMaintain.this.azB != null) {
                    int f = ActivityFileMaintain.f(ActivityFileMaintain.this.azB.blw);
                    if (f == 6) {
                        Toast.makeText(ActivityFileMaintain.this, (int) R.string.f977file_opera_failed, 0).show();
                    } else if (f == 7) {
                        Toast.makeText(ActivityFileMaintain.this, (int) R.string.f977file_opera_failed, 0).show();
                    }
                }
                ActivityFileMaintain.this.ur.EN();
            }
        });
        builder.c((int) R.string.f1267dlg_button_cancle, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ActivityFileMaintain.this.yg();
            }
        });
        return builder.fh();
    }

    private Dialog v(Context context) {
        LayoutInflater from = LayoutInflater.from(this);
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.aH(R.string.f1158dialog_title_fileattr);
        View inflate = from.inflate((int) R.layout.f899file_dialog_attr, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.f747filepath)).setTextColor(e.Pb().getColor(78));
        ((TextView) inflate.findViewById(R.id.f749lastmodify)).setTextColor(e.Pb().getColor(78));
        ((TextView) inflate.findViewById(R.id.f751filesize)).setTextColor(e.Pb().getColor(78));
        builder.c(inflate);
        if (!(this.azB == null || this.azB.blw == null)) {
            TextView textView = (TextView) inflate.findViewById(R.id.f752fileattr_filesize);
            DecimalFormat decimalFormat = new DecimalFormat("0.##");
            float a2 = a(this.azB.blw, 0.0f);
            int i = 0;
            while (a2 > 1000.0f) {
                a2 /= 1024.0f;
                i++;
            }
            textView.setText(decimalFormat.format((double) a2) + AdapterFile.bnF[i]);
            ((TextView) inflate.findViewById(R.id.f748fileattr_filepath)).setText(this.azB.blw.getPath());
            ((TextView) inflate.findViewById(R.id.f750fileattr_lastmodify)).setText(new SimpleDateFormat("yyyy-MM-dd hh:mm").format(new Date(this.azB.blw.lastModified())));
        }
        builder.a((int) R.string.f1266dlg_button_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ActivityFileMaintain.this.yg();
            }
        });
        return builder.fh();
    }

    private void yb() {
        this.um = (BarLayout) findViewById(R.id.f655controlbar);
        e Pb = e.Pb();
        int kp = Pb.kp(R.dimen.f184controlbar_item_width_3);
        int kp2 = Pb.kp(R.dimen.f177controlbar_height);
        int kp3 = Pb.kp(R.dimen.f188controlbar_text_size);
        int kp4 = Pb.kp(R.dimen.f187controlbar_item_paddingTop);
        this.um.HN.aw(kp, kp2);
        Resources resources = getResources();
        this.up = new ac(R.string.f1019controlbar_makedir, 0, 0);
        this.up.bB(0, 0);
        this.up.aV(kp3);
        this.up.setText(resources.getString(R.string.f1019controlbar_makedir));
        this.up.setPadding(0, kp4, 0, 4);
        this.up.H(true);
        this.um.a(this.up);
        this.azy = new ac(R.string.f1022controlbar_showtype_list, 0, 0);
        this.azy.bB(0, 0);
        this.azy.aV(kp3);
        this.azy.setText(resources.getString(R.string.f1022controlbar_showtype_list));
        this.azy.setPadding(0, kp4, 0, 4);
        this.azy.H(true);
        this.um.a(this.azy);
        this.azy.setVisibility(2);
        this.azz = new ac(R.string.f1021controlbar_showtype_grid, 0, 0);
        this.azz.bB(0, 0);
        this.azz.aV(kp3);
        this.azz.setText(resources.getString(R.string.f1021controlbar_showtype_grid));
        this.azz.setPadding(0, kp4, 0, 4);
        this.azz.H(true);
        this.um.a(this.azz);
        this.uo = new ac(R.string.f1017controlbar_back, 0, 0);
        this.uo.bB(0, 0);
        this.uo.aV(kp3);
        this.uo.setText(resources.getString(R.string.f1017controlbar_back));
        this.uo.setPadding(0, kp4, 0, 4);
        this.uo.H(true);
        this.um.a(this.uo);
        this.um.kJ();
        this.um.b(this);
    }

    private void ye() {
        if (this.azA != null) {
            this.ul.removeView(this.azA);
            unregisterForContextMenu(this.azA);
            this.azA = null;
        }
        this.azA = new GridView(this);
        this.azA.setId(1048577);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(3, R.id.f654Browser_TitleBar);
        layoutParams.addRule(2, R.id.f655controlbar);
        this.azA.setLayoutParams(layoutParams);
        GridView gridView = (GridView) this.azA;
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, e.Pb().getDrawable(UCR.drawable.aVI));
        stateListDrawable.addState(new int[]{16842908}, e.Pb().getDrawable(UCR.drawable.aVK));
        gridView.setSelector(stateListDrawable);
        gridView.setNumColumns(-1);
        gridView.setColumnWidth((int) getResources().getDimension(R.dimen.f271file_grid_item_width));
        gridView.setVerticalSpacing(10);
        gridView.setHorizontalSpacing(10);
        gridView.setPadding(5, 5, 5, 5);
        int color = e.Pb().getColor(47);
        gridView.setBackgroundColor(color);
        gridView.setCacheColorHint(color);
        if (this.ur == null) {
            File file = new File("/sdcard/");
            this.ur = new AdapterFile();
            this.ur.n(file);
        }
        this.ur.setType(0);
        this.azA.setAdapter(this.ur);
        this.azA.setOnItemClickListener(this);
        registerForContextMenu(this.azA);
        this.ul.addView(this.azA);
        this.azy.setVisibility(0);
        this.azz.setVisibility(2);
        this.um.kJ();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putInt(azO, 0).commit();
    }

    private void yf() {
        if (this.azA != null) {
            this.ul.removeView(this.azA);
            unregisterForContextMenu(this.azA);
            this.azA = null;
        }
        this.azA = new ListView(this);
        this.azA.setId(1048577);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(3, R.id.f654Browser_TitleBar);
        layoutParams.addRule(2, R.id.f655controlbar);
        this.azA.setLayoutParams(layoutParams);
        ListView listView = (ListView) this.azA;
        listView.setDivider(new ColorDrawable(e.Pb().getColor(108)));
        listView.setDividerHeight(1);
        listView.setPadding(0, 0, 0, 0);
        int color = e.Pb().getColor(47);
        listView.setBackgroundColor(color);
        listView.setCacheColorHint(color);
        if (this.ur == null) {
            File file = new File("/sdcard/");
            this.ur = new AdapterFile();
            this.ur.n(file);
        }
        this.ur.setType(1);
        this.azA.setAdapter(this.ur);
        ((ListView) this.azA).setSelector(e.Pb().getDrawable(UCR.drawable.aXb));
        this.azA.setOnItemClickListener(this);
        registerForContextMenu(this.azA);
        this.ul.addView(this.azA);
        this.azy.setVisibility(2);
        this.azz.setVisibility(0);
        this.um.kJ();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putInt(azO, 1).commit();
    }

    /* access modifiers changed from: private */
    public void yg() {
        if (this.azR != null) {
            this.azR.setBackgroundDrawable(null);
        }
        this.azR = null;
    }

    public void b(z zVar, int i) {
        switch (i) {
            case R.string.f1017controlbar_back /*2131296324*/:
                if (!fz()) {
                    finish();
                    return;
                }
                return;
            case R.string.f1018controlbar_ok /*2131296325*/:
            case R.string.f1020controlbar_showtype /*2131296327*/:
            default:
                return;
            case R.string.f1019controlbar_makedir /*2131296326*/:
                f(this).show();
                this.handler.sendEmptyMessageDelayed(4, 300);
                return;
            case R.string.f1021controlbar_showtype_grid /*2131296328*/:
                ye();
                this.azH = 0;
                return;
            case R.string.f1022controlbar_showtype_list /*2131296329*/:
                yf();
                this.azH = 1;
                return;
        }
    }

    public void k() {
        this.um.k();
        this.um.invalidate();
        TitleBarTextView titleBarTextView = (TitleBarTextView) findViewById(R.id.f654Browser_TitleBar);
        if (titleBarTextView != null) {
            titleBarTextView.k();
        }
        if (this.azH == 1) {
            yf();
        } else if (this.azH == 0) {
            ye();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 86 && i2 == -1) {
            finish();
        }
    }

    public void onClick(View view) {
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        boolean onContextItemSelected = super.onContextItemSelected(menuItem);
        switch (menuItem.getItemId()) {
            case com.uc.d.h.ccP /*458753*/:
                if (this.azC != null && this.azC.size() > 0) {
                    e((File) this.azC.elementAt(0));
                    this.azC.clear();
                    this.state = 0;
                }
                yg();
                break;
            case com.uc.d.h.ccR /*462849*/:
                if (this.azC == null) {
                    this.azC = new Vector();
                }
                this.azC.clear();
                if (this.azB != null) {
                    this.azC.add(this.azB.blw);
                }
                this.state = 1;
                yg();
                break;
            case com.uc.d.h.ccS /*462850*/:
                if (this.azC == null) {
                    this.azC = new Vector();
                }
                this.azC.clear();
                if (this.azB != null) {
                    this.azC.add(this.azB.blw);
                }
                this.state = 2;
                yg();
                break;
            case com.uc.d.h.ccT /*462851*/:
            case com.uc.d.h.ccV /*462853*/:
                t(this).show();
                this.handler.sendEmptyMessageDelayed(4, 300);
                break;
            case com.uc.d.h.ccU /*462852*/:
            case com.uc.d.h.ccW /*462854*/:
                u(this).show();
                break;
            case com.uc.d.h.ccX /*462855*/:
                v(this).show();
                break;
        }
        return onContextItemSelected;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.RelativeLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ActivityBrowser.g(this);
        requestWindowFeature(1);
        this.ul = (RelativeLayout) LayoutInflater.from(this).inflate((int) R.layout.f902file_maintain, (ViewGroup) this.ul, true);
        setContentView(this.ul);
        yb();
        this.azH = PreferenceManager.getDefaultSharedPreferences(this).getInt(azO, 1);
        String str = null;
        if (bundle != null) {
            str = bundle.getString(uB);
            this.azC = b(bundle.getCharArray(azK));
            this.state = bundle.getInt(azL);
            this.azH = bundle.getInt(azG);
            String string = bundle.getString(azM);
            if (string != null) {
                this.azB = new FileItem(new File(string));
            }
        }
        if (str == null || str == "") {
            str = "/sdcard/";
        }
        this.uD = new File(str);
        if (!this.uD.exists() || !Environment.getExternalStorageState().equals("mounted")) {
            yc();
            return;
        }
        this.ur = new AdapterFile();
        this.ur.n(this.uD);
        t(this.uD.getPath());
        yd();
        k();
        e.Pb().a(this);
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        if (this.lF != null) {
            this.lF.a(new com.uc.d.e() {
                public void onDismiss() {
                    ActivityFileMaintain.this.yg();
                }
            });
        }
        if (view == this.azA) {
            g.a(this, com.uc.d.h.ccY, contextMenu);
            if (this.azB.blx == -2) {
                contextMenu.setGroupVisible(com.uc.d.h.ccQ, false);
            }
            if (this.azC == null || this.azC.size() == 0) {
                contextMenu.findItem(com.uc.d.h.ccP).setVisible(false);
            }
            if (this.azB.blw == null) {
                contextMenu.findItem(com.uc.d.h.ccW).setVisible(false);
                contextMenu.findItem(com.uc.d.h.ccV).setVisible(false);
                contextMenu.findItem(com.uc.d.h.ccU).setVisible(false);
                contextMenu.findItem(com.uc.d.h.ccT).setVisible(false);
            } else if (this.azB.blw.isDirectory()) {
                contextMenu.findItem(com.uc.d.h.ccW).setVisible(true);
                contextMenu.findItem(com.uc.d.h.ccV).setVisible(true);
                contextMenu.findItem(com.uc.d.h.ccU).setVisible(false);
                contextMenu.findItem(com.uc.d.h.ccT).setVisible(false);
            } else {
                contextMenu.findItem(com.uc.d.h.ccW).setVisible(false);
                contextMenu.findItem(com.uc.d.h.ccV).setVisible(false);
                contextMenu.findItem(com.uc.d.h.ccU).setVisible(true);
                contextMenu.findItem(com.uc.d.h.ccT).setVisible(true);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.ul.removeAllViews();
        e.Pb().b(this);
        super.onDestroy();
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        FileItem fileItem = (FileItem) this.azA.getItemAtPosition(i);
        if (fileItem.blx == -2) {
            if (!fz()) {
                Toast.makeText(this, (int) R.string.f974file_already_root, 0).show();
            }
        } else if (fileItem.blw.isDirectory()) {
            this.ur.n(fileItem.blw);
            this.uD = fileItem.blw;
            t(fileItem.blw.getPath());
        } else if (fileItem.blw.getName().endsWith(".upp")) {
            dd(fileItem.blw.getAbsolutePath());
        } else {
            String uri = fileItem.blw.toURI().toString();
            if (uri.startsWith("file:/")) {
                uri = "file:///" + uri.substring(6);
            }
            d.a(this, uri, fileItem.blx);
        }
    }

    public boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
        this.azB = (FileItem) this.ur.getItem(i);
        boolean onItemLongClick = super.onItemLongClick(adapterView, view, i, j);
        if (this.lF.isShowing()) {
            this.azR = view;
            if (this.azR != null) {
                if (this.azH == 1) {
                    this.azR.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aXb));
                } else {
                    this.azR.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aVI));
                }
            }
        }
        return onItemLongClick;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || !fz()) {
            return super.onKeyDown(i, keyEvent);
        }
        return true;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i != 84) {
            return super.onKeyUp(i, keyEvent);
        }
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a((int) ModelBrowser.zJ, this);
        }
        return true;
    }

    public boolean onLongClick(View view) {
        return super.onLongClick(view);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString(uB, this.uD.getPath());
        bundle.putCharArray(azK, o(this.azC));
        bundle.putInt(azL, this.state);
        bundle.putInt(azG, this.azH);
        if (this.azB != null && this.azB.blw != null) {
            bundle.putString(azM, this.azB.blw.toString());
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z && ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(this);
        }
        if (z && ActivityBrowser.Fz()) {
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
            }
            ActivityBrowser.e(this);
        }
    }

    public void yc() {
        if (this.azA != null) {
            this.ul.removeView(this.azA);
            unregisterForContextMenu(this.azA);
            this.azA = null;
        }
        this.up.H(false);
        this.azy.H(false);
        this.azz.H(false);
        findViewById(R.id.f745no_sd_card_warning).setVisibility(0);
        findViewById(R.id.f745no_sd_card_warning).bringToFront();
    }

    public void yd() {
        this.up.H(true);
        this.azy.H(true);
        this.azz.H(true);
        switch (this.azH) {
            case 0:
                ye();
                com.uc.a.e.nR().nY().w(azN, "0");
                return;
            case 1:
                yf();
                com.uc.a.e.nR().nY().w(azN, com.uc.a.e.RD);
                return;
            default:
                ye();
                com.uc.a.e.nR().nY().w(azN, "0");
                return;
        }
    }

    public void yh() {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this);
        builder.aH(R.string.f1150dialog_title_waiting_copy);
        View inflate = LayoutInflater.from(this).inflate((int) R.layout.f929progress_dlg_content, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.message)).setText(getString(R.string.f1151dialog_msg_waiting_copy));
        builder.c(inflate);
        builder.setCancelable(false);
        this.aAf = builder.fh();
        this.aAf.show();
    }
}
