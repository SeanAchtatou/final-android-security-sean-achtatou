package com.uc.browser;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import b.a.a.e;
import java.util.Vector;

public class AdapterSearchEngineList extends BaseAdapter {
    private Vector lO;

    public int getCount() {
        if (this.lO != null) {
            return this.lO.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        if (this.lO == null || this.lO.size() == 0) {
            return null;
        }
        return this.lO.elementAt(i % this.lO.size());
    }

    public long getItemId(int i) {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        e eVar = (e) getItem(i);
        CheckedTextView checkedTextView = view == null ? (CheckedTextView) LayoutInflater.from(viewGroup.getContext()).inflate((int) R.layout.f936search_enginelist_item, viewGroup, false) : (CheckedTextView) view;
        checkedTextView.setTextColor(com.uc.h.e.Pb().getColor(16));
        if (eVar != null) {
            checkedTextView.setText(eVar.abM);
            Drawable bitmapDrawable = eVar.abN != null ? new BitmapDrawable(eVar.abN) : checkedTextView.getResources().getDrawable(R.drawable.f632searchengine_icon_default);
            bitmapDrawable.setBounds(0, 0, 24, 24);
            checkedTextView.setCompoundDrawables(bitmapDrawable, null, null, null);
            checkedTextView.setCompoundDrawablePadding(5);
        } else {
            checkedTextView.setText((int) R.string.f968emptydata);
        }
        return checkedTextView;
    }

    public void i(Vector vector) {
        this.lO = vector;
    }
}
