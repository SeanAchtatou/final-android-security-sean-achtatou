package com.uc.browser;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.hardware.Camera;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.uc.c.cd;
import com.uc.c.w;
import java.io.IOException;
import java.util.Vector;

public class UcCamera {
    /* access modifiers changed from: private */
    public Configuration arA;
    /* access modifiers changed from: private */
    public PreviewDisplay arB;
    /* access modifiers changed from: private */
    public EventDispatcher arC;
    /* access modifiers changed from: private */
    public Shutter arD;
    /* access modifiers changed from: private */
    public String arE;
    private final String arF = "OK";
    private final String arG = "Could not open camera";
    private final String arH = "Not open";
    private final String arI = "Could not set holder";
    private final String arJ = "Capture while capturing";
    private final String arK = "No preview display";
    Camera ary;
    /* access modifiers changed from: private */
    public Controller arz;

    public interface CALLBACK_CAPTURE {
        void b(Bitmap bitmap);

        void onShutter();
    }

    class Configuration {
        private final int ORIENTATION_LANDSCAPE;
        private final int ORIENTATION_PORTRAIT;
        private final int bCb;
        private final int bCc;
        private final int bCd;
        private final int bCe;
        private final int bCf;
        private final int bCg;
        private final String bCh;
        private int orientation;

        private Configuration() {
            this.bCb = w.Qt;
            this.bCc = 240;
            this.bCd = w.Qt;
            this.bCe = 240;
            this.bCf = 800;
            this.bCg = 600;
            this.ORIENTATION_PORTRAIT = 0;
            this.ORIENTATION_LANDSCAPE = 1;
            this.bCh = "picture-size-values";
            this.orientation = 0;
        }

        /* access modifiers changed from: private */
        public boolean IV() {
            Camera.Parameters parameters = UcCamera.this.ary.getParameters();
            String str = parameters.get("picture-size-values");
            if (str != null) {
                Point fg = fg(str);
                parameters.setPictureSize(fg.x, fg.y);
            }
            parameters.setPreviewSize(w.Qt, 240);
            try {
                UcCamera.this.ary.setParameters(parameters);
                return true;
            } catch (Exception e) {
                return true;
            }
        }

        private Point fg(String str) {
            Point point = null;
            try {
                String[] split = str.split(cd.bVH);
                Vector vector = new Vector();
                if (split.length > 0) {
                    for (String split2 : split) {
                        String[] split3 = split2.split("x");
                        vector.add(new Point(Integer.parseInt(split3[0]), Integer.parseInt(split3[1])));
                    }
                    int size = vector.size();
                    Point point2 = null;
                    int i = 0;
                    while (i < size) {
                        try {
                            Point point3 = (Point) vector.get(i);
                            if (point3.x >= 800 && (point2 == null || point2.x > point3.x)) {
                                point2 = point3;
                            }
                            i++;
                        } catch (Throwable th) {
                            point = point2;
                        }
                    }
                    point = point2;
                }
            } catch (Throwable th2) {
            }
            return point == null ? new Point(w.Qt, 240) : point;
        }

        public void IT() {
            this.orientation = 1;
        }

        public void IU() {
            this.orientation = 0;
        }

        public int getOrientation() {
            return this.orientation;
        }
    }

    class Controller {
        private boolean uR;

        private Controller() {
            this.uR = false;
        }

        public boolean fC() {
            return this.uR;
        }

        public boolean fD() {
            if (!this.uR) {
                String unused = UcCamera.this.arE = "Not open";
                return false;
            } else if (UcCamera.this.arB != null) {
                return UcCamera.this.arB.ue || UcCamera.this.arB.aGU;
            } else {
                return false;
            }
        }

        public boolean fE() {
            if (!this.uR) {
                String unused = UcCamera.this.arE = "Not open";
                return false;
            } else if (UcCamera.this.arD != null) {
                return UcCamera.this.arD.caf;
            } else {
                return false;
            }
        }

        public boolean fF() {
            if (this.uR) {
                return UcCamera.this.arD.fF();
            }
            String unused = UcCamera.this.arE = "Not open";
            return false;
        }

        public boolean fG() {
            if (this.uR) {
                return UcCamera.this.arD.fG();
            }
            String unused = UcCamera.this.arE = "Not open";
            return false;
        }

        public boolean fH() {
            if (!this.uR) {
                String unused = UcCamera.this.arE = "Not open";
                return false;
            } else if (UcCamera.this.arB != null) {
                return UcCamera.this.arB.fH();
            } else {
                String unused2 = UcCamera.this.arE = "No preview display";
                return false;
            }
        }

        public boolean fI() {
            if (UcCamera.this.arB != null) {
                return UcCamera.this.arB.fI();
            }
            String unused = UcCamera.this.arE = "OK";
            return true;
        }

        public boolean fJ() {
            if (this.uR) {
                String unused = UcCamera.this.arE = "OK";
                return true;
            }
            try {
                UcCamera.this.ary = Camera.open();
            } catch (Exception e) {
                UcCamera.this.ary = null;
            }
            if (UcCamera.this.ary == null) {
                String unused2 = UcCamera.this.arE = "Could not open camera";
                return false;
            } else if (!UcCamera.this.arA.IV()) {
                UcCamera.this.ary.release();
                this.uR = false;
                return false;
            } else {
                this.uR = true;
                String unused3 = UcCamera.this.arE = "OK";
                return true;
            }
        }

        public boolean fK() {
            fI();
            if (UcCamera.this.ary != null) {
                UcCamera.this.ary.release();
            }
            UcCamera.this.ary = null;
            this.uR = false;
            String unused = UcCamera.this.arE = "OK";
            System.gc();
            return true;
        }
    }

    class EventDispatcher implements Camera.AutoFocusCallback, Camera.PictureCallback, Camera.ShutterCallback {
        CALLBACK_CAPTURE brC;

        private EventDispatcher(CALLBACK_CAPTURE callback_capture) {
            this.brC = callback_capture;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
         arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
         candidates:
          ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
          ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
        private Bitmap f(Bitmap bitmap) {
            if (bitmap == null) {
                return null;
            }
            Matrix matrix = new Matrix();
            if (UcCamera.this.arA.getOrientation() == 0) {
                matrix.postRotate(90.0f);
            }
            int width = bitmap.getWidth();
            if (width > 800) {
                float f = 800.0f / ((float) width);
                matrix.postScale(f, f);
            }
            try {
                return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            } catch (Exception e) {
                return null;
            } catch (Error e2) {
                return null;
            }
        }

        public void onAutoFocus(boolean z, Camera camera) {
            if (UcCamera.this.ary == camera) {
                UcCamera.this.arD.cJ(z);
            }
        }

        public void onPictureTaken(byte[] bArr, Camera camera) {
            UcCamera.this.arz.fI();
            Bitmap bitmap = null;
            if (!(bArr == null || camera == null)) {
                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
                    int i = options.outWidth;
                    BitmapFactory.Options options2 = new BitmapFactory.Options();
                    if (i > 800) {
                        options2.inSampleSize = i / 800;
                    }
                    Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options2);
                    if (decodeByteArray != null) {
                        if (bitmap != null) {
                            bitmap.recycle();
                        }
                        bitmap = f(decodeByteArray);
                        if (!decodeByteArray.equals(bitmap)) {
                            decodeByteArray.recycle();
                        }
                        System.gc();
                    }
                } catch (Error | Exception e) {
                }
            }
            UcCamera.this.arD.ud = bitmap;
            if (this.brC != null) {
                this.brC.b(bitmap);
            }
            boolean unused = UcCamera.this.arD.caf = false;
        }

        public void onShutter() {
            if (this.brC != null) {
                this.brC.onShutter();
            }
        }
    }

    class PreviewDisplay implements SurfaceHolder.Callback {
        private SurfaceView aGT;
        /* access modifiers changed from: private */
        public boolean aGU;
        boolean aGV;
        /* access modifiers changed from: private */
        public boolean ue;

        private PreviewDisplay(SurfaceView surfaceView) {
            this.aGV = false;
            this.aGT = surfaceView;
            this.aGU = false;
            this.ue = false;
        }

        public boolean fH() {
            if (this.ue) {
                String unused = UcCamera.this.arE = "OK";
                return true;
            }
            SurfaceHolder holder = this.aGT.getHolder();
            holder.addCallback(this);
            Surface surface = holder.getSurface();
            if (this.aGV || surface.isValid()) {
                try {
                    UcCamera.this.ary.setPreviewDisplay(holder);
                    this.aGU = false;
                    try {
                        UcCamera.this.ary.startPreview();
                        this.ue = true;
                        String unused2 = UcCamera.this.arE = "OK";
                        return true;
                    } catch (Exception e) {
                        return false;
                    }
                } catch (IOException e2) {
                    holder.removeCallback(this);
                    String unused3 = UcCamera.this.arE = "Could not set holder";
                    return false;
                }
            } else {
                this.aGU = true;
                String unused4 = UcCamera.this.arE = "OK";
                this.aGT.postDelayed(new Runnable() {
                    public void run() {
                        try {
                            UcCamera.this.arz.fH();
                        } catch (Throwable th) {
                        }
                    }
                }, 500);
                return true;
            }
        }

        public boolean fI() {
            this.aGT.getHolder().removeCallback(this);
            this.aGU = false;
            try {
                UcCamera.this.ary.stopPreview();
            } catch (Exception e) {
            }
            this.ue = false;
            this.aGV = false;
            String unused = UcCamera.this.arE = "OK";
            return true;
        }

        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        }

        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            if (this.aGU) {
                UcCamera.this.arz.fH();
            }
            this.aGV = true;
        }

        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            this.aGV = false;
            UcCamera.this.arz.fI();
        }
    }

    class Shutter {
        private boolean cae;
        /* access modifiers changed from: private */
        public boolean caf;
        Bitmap ud;

        private Shutter() {
            this.ud = null;
            this.cae = false;
            this.caf = false;
        }

        /* access modifiers changed from: private */
        public void cJ(boolean z) {
            if (this.cae) {
                this.cae = false;
                fF();
            }
        }

        /* access modifiers changed from: private */
        public boolean fF() {
            this.cae = false;
            try {
                UcCamera.this.ary.takePicture(UcCamera.this.arC, null, UcCamera.this.arC);
                this.caf = true;
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        public Bitmap Pg() {
            return this.ud;
        }

        public boolean fG() {
            if (this.caf) {
                String unused = UcCamera.this.arE = "Capture while capturing";
                return false;
            }
            try {
                UcCamera.this.ary.autoFocus(UcCamera.this.arC);
                this.cae = true;
                String unused2 = UcCamera.this.arE = "OK";
                return true;
            } catch (Exception e) {
                return false;
            }
        }
    }

    private UcCamera(SurfaceView surfaceView, CALLBACK_CAPTURE callback_capture) {
        this.arC = new EventDispatcher(callback_capture);
        this.arD = new Shutter();
        if (surfaceView != null) {
            surfaceView.getHolder().setType(3);
            this.arB = new PreviewDisplay(surfaceView);
        } else {
            this.arB = null;
        }
        this.arA = new Configuration();
        this.arz = new Controller();
        this.arE = "OK";
    }

    public static UcCamera a(SurfaceView surfaceView, CALLBACK_CAPTURE callback_capture) {
        return new UcCamera(surfaceView, callback_capture);
    }

    public static boolean wu() {
        return true;
    }

    public void finalize() {
        this.arz.fI();
        this.arz.fK();
    }

    /* access modifiers changed from: package-private */
    public Controller wr() {
        return this.arz;
    }

    public Configuration ws() {
        return this.arA;
    }

    public String wt() {
        return this.arE;
    }
}
