package com.uc.browser;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

public class HightQualityImageView extends View {
    public Bitmap bpj;

    public HightQualityImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.bpj != null) {
            canvas.drawBitmap(this.bpj, new Rect(0, 0, this.bpj.getWidth(), this.bpj.getHeight()), new Rect(0, 0, getWidth(), getHeight()), new Paint());
        }
    }
}
