package com.uc.browser;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import b.b;
import com.uc.browser.ViewZoomControls;
import com.uc.c.an;
import com.uc.f.g;

public class WebViewZoom extends WebView implements ViewZoomControls.ZoomController {
    final g aFK = g.Jn();
    private boolean aFL = true;
    private boolean aFM = false;
    private boolean aFN = false;
    private boolean aFO = false;
    private DrawWaitingPage aFP;
    private JavaScriptAndroidBridge aFQ;
    private int aFR = 20;
    private int aFS = -1;
    private int aFT = -1;
    private ActivityBrowser bk;

    public WebViewZoom(Context context) {
        super(context);
        this.bk = (ActivityBrowser) context;
        this.aFK.a(getSettings());
        this.aFR = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public WebViewZoom(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.bk = (ActivityBrowser) context;
        this.aFK.a(getSettings());
        this.aFR = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public WebViewZoom(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.bk = (ActivityBrowser) context;
        this.aFK.a(getSettings());
        this.aFR = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    private void bi(boolean z) {
        this.aFO = z;
    }

    public void a(JavaScriptAndroidBridge javaScriptAndroidBridge) {
        this.aFQ = javaScriptAndroidBridge;
        addJavascriptInterface(this.aFQ, "JavaScriptAndroidBridge");
    }

    public int ah() {
        return 1;
    }

    public void bh(boolean z) {
        this.aFL = z;
        if (true == z && this.aFP != null) {
            this.aFP.FB();
            this.aFP = null;
        }
        this.aFM = false;
    }

    public void destroy() {
        ViewGroup viewGroup = (ViewGroup) getParent();
        if (viewGroup != null) {
            viewGroup.removeView(this);
        }
        super.destroy();
    }

    public String getTitle() {
        return super.getTitle();
    }

    public String getUrl() {
        return super.getUrl();
    }

    public void goBackOrForward(int i) {
        super.goBackOrForward(i);
    }

    public void i(String str, boolean z) {
        bi(z);
        String url = getUrl();
        if (url != null && str != null && url.length() > 0 && str.length() > 0) {
            StringBuffer stringBuffer = new StringBuffer(url);
            StringBuffer stringBuffer2 = new StringBuffer(str);
            while (stringBuffer.charAt(stringBuffer.length() - 1) == '/') {
                stringBuffer.deleteCharAt(stringBuffer.length() - 1);
            }
            while (stringBuffer2.charAt(stringBuffer2.length() - 1) == '/') {
                stringBuffer2.deleteCharAt(stringBuffer2.length() - 1);
            }
            if (stringBuffer.toString().equals(stringBuffer2.toString())) {
                WindowUCWeb.e(str);
            }
        }
        super.loadUrl(str);
        zM();
    }

    public int j(int i) {
        return 1;
    }

    public void loadData(String str, String str2, String str3) {
        super.loadData(str, str2, str3);
    }

    public void loadUrl(String str) {
        i(str, false);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        canvas.save();
        if (true == this.aFL) {
            super.onDraw(canvas);
            if (ActivityBrowser.Fz()) {
                canvas.drawColor(-2013265920);
            }
            if (this.aFP != null) {
                this.aFP.FB();
                this.aFP = null;
            }
            if (true == this.aFM) {
                this.aFM = false;
                return;
            }
            return;
        }
        if (!this.aFM) {
            this.bk.t(this.bk.getString(R.string.f1305page_loading));
            this.aFM = true;
        }
        if (this.aFP == null) {
            this.aFP = new DrawWaitingPage(this, true, null);
        }
        if (true == this.aFP.zH()) {
            this.aFP.j(canvas);
        } else {
            this.aFP.k(canvas);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0 && ModelBrowser.gD() != null) {
            ModelBrowser.gD().aS(ModelBrowser.zP);
        }
        if (!this.aFL) {
            return true;
        }
        if (motionEvent.getAction() == 0 && an.vA().vW()) {
            zE();
            an.vA().vX();
        }
        if (!getSettings().getBuiltInZoomControls()) {
            switch (motionEvent.getAction()) {
                case 0:
                    this.aFS = (int) motionEvent.getX();
                    this.aFT = (int) motionEvent.getY();
                    break;
                case 1:
                case 3:
                case 4:
                    this.aFS = -1;
                    this.aFT = -1;
                    break;
                case 2:
                    if (this.aFS >= 0 && this.aFT >= 0) {
                        if (Math.abs(motionEvent.getX() - ((float) this.aFS)) + Math.abs(motionEvent.getY() - ((float) this.aFT)) > ((float) this.aFR) && ModelBrowser.gD() != null) {
                            ModelBrowser.gD().a(106, b.acj, this);
                            break;
                        }
                    } else {
                        this.aFS = (int) motionEvent.getX();
                        this.aFT = (int) motionEvent.getY();
                        break;
                    }
                    break;
            }
        }
        try {
            super.onTouchEvent(motionEvent);
        } catch (Exception e) {
        }
        return true;
    }

    public void reload() {
        super.reload();
        zM();
    }

    public void requestFocusNodeHref(Message message) {
        super.requestFocusNodeHref(message);
    }

    public void setWebViewClient(WebViewClient webViewClient) {
        super.setWebViewClient(webViewClient);
    }

    public void stopLoading() {
        super.stopLoading();
        zL();
    }

    public boolean uf() {
        return true;
    }

    public void zE() {
        try {
            WebView.class.getMethod("emulateShiftHeld", new Class[0]).invoke(this, new Object[0]);
        } catch (Exception e) {
        }
    }

    public boolean zF() {
        return this.aFL;
    }

    public void zG() {
        if (this.aFP != null) {
            this.aFP.FB();
        } else {
            this.aFP = new DrawWaitingPage(this, false, null);
        }
        postInvalidate();
    }

    public boolean zH() {
        if (this.aFP != null) {
            return this.aFP.zH();
        }
        return false;
    }

    public void zI() {
        if (this.aFP != null) {
            this.aFP.FB();
            this.aFP = null;
        }
        postInvalidate();
    }

    public JavaScriptAndroidBridge zJ() {
        return this.aFQ;
    }

    public String zK() {
        CookieManager instance = CookieManager.getInstance();
        String url = getUrl();
        if (url != null) {
            return instance.getCookie(url);
        }
        return null;
    }

    public void zL() {
        if (true != this.aFN) {
            try {
                WebView.class.getMethod("onPause", new Class[0]).invoke(this, new Object[0]);
                this.aFN = true;
            } catch (Exception e) {
            }
        }
    }

    public void zM() {
        if (this.aFN) {
            try {
                WebView.class.getMethod("onResume", new Class[0]).invoke(this, new Object[0]);
                this.aFN = false;
            } catch (Exception e) {
            }
        }
    }

    public boolean zN() {
        return this.aFO;
    }

    public boolean zoomIn() {
        return super.zoomIn();
    }

    public boolean zoomOut() {
        return super.zoomOut();
    }
}
