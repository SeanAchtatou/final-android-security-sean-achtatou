package com.uc.browser;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class FlashWrapperView extends RelativeLayout {
    private View aGm;

    public FlashWrapperView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void j(View view) {
        this.aGm = view;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.aGm.requestFocus();
    }
}
