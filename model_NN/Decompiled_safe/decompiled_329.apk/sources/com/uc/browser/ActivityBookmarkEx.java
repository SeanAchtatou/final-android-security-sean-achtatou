package com.uc.browser;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import b.a.a.e;
import com.uc.a.m;
import com.uc.browser.UCAlertDialog;
import com.uc.browser.UCR;
import com.uc.c.au;
import java.util.ArrayList;
import java.util.Vector;

public class ActivityBookmarkEx extends ActivityWithUCMenu {
    public static final String asd = "COM.UC.NEWBOOKMARK";
    public static final String ase = "COM.UC.EDITBOOKMARK";
    public static final String asf = "filename";
    public static final String asg = "path";
    public static final String ash = "folder";
    public static final String asi = "page_attribute";
    public static final String asj = "id";
    private static final int ask = 101;
    private static final int asl = 102;
    private static final int asm = 103;
    private static final int asn = 104;
    private byte abS = -1;
    private m aso;
    private Button asp;
    private Button asq;
    /* access modifiers changed from: private */
    public EditText asr;
    /* access modifiers changed from: private */
    public EditText ass;
    /* access modifiers changed from: private */
    public UCSpinner ast;
    /* access modifiers changed from: private */
    public int asu = -1;
    private int asv = -1;
    private boolean asw = false;
    private boolean asx = false;
    private View.OnClickListener asy = new View.OnClickListener() {
        public void onClick(View view) {
            if (view.getId() == R.id.f687submit) {
                String trim = ActivityBookmarkEx.this.asr.getText().toString().trim();
                String trim2 = ActivityBookmarkEx.this.ass.getText().toString().trim();
                int c = ActivityBookmarkEx.this.asu;
                int cQ = ActivityBookmarkEx.this.ast.cQ();
                if (trim == null || trim.length() <= 0) {
                    Toast.makeText(ActivityBookmarkEx.this, (int) R.string.f1393bookmarknamenotnull, 0).show();
                    return;
                } else if (trim2 == null || trim2.length() <= 0) {
                    Toast.makeText(ActivityBookmarkEx.this, (int) R.string.f1394bookmarkvalueotnull, 0).show();
                    return;
                } else {
                    ActivityBookmarkEx.this.c(trim, trim2, cQ, c);
                }
            }
            if (view.getId() == R.id.f688cancle) {
                ActivityBookmarkEx.this.finish();
                ActivityBookmarkEx.this.setResult(102);
            }
        }
    };
    private View.OnClickListener asz = new View.OnClickListener() {
        public void onClick(View view) {
            if (view.getId() == R.id.f687submit) {
                String trim = ActivityBookmarkEx.this.asr.getText().toString().trim();
                String trim2 = ActivityBookmarkEx.this.ass.getText().toString().trim();
                int cQ = ActivityBookmarkEx.this.ast.cQ();
                if (trim == null || trim.trim().length() == 0) {
                    Toast.makeText(ActivityBookmarkEx.this, (int) R.string.f1393bookmarknamenotnull, 0).show();
                    return;
                } else if (trim2 == null || trim2.trim().length() == 0) {
                    Toast.makeText(ActivityBookmarkEx.this, (int) R.string.f1394bookmarkvalueotnull, 0).show();
                    return;
                } else {
                    ActivityBookmarkEx.this.b(trim, trim2, cQ);
                }
            }
            if (view.getId() == R.id.f688cancle) {
                ActivityBookmarkEx.this.finish();
                ActivityBookmarkEx.this.setResult(104);
            }
        }
    };

    private void P(String str, String str2) {
        this.asr = (EditText) findViewById(R.id.f678bookmark_dlg_filename);
        this.ass = (EditText) findViewById(R.id.f680bookmark_dlg_filepath);
        if (str != null && str.length() > 0) {
            this.asr.setText(str);
        }
        if (str2 != null && str2.length() > 0) {
            this.ass.setText(str2);
        }
    }

    private void Q(String str, String str2) {
        P(str, str2);
        this.ast = (UCSpinner) findViewById(R.id.f682bookmark_dlg_folder);
        this.asp = (Button) findViewById(R.id.f687submit);
        this.asp.setOnClickListener(this.asz);
        this.asq = (Button) findViewById(R.id.f688cancle);
        this.asq.setOnClickListener(this.asz);
        ArrayList arrayList = new ArrayList();
        arrayList.add(getString(R.string.f1464dir_root));
        Vector ol = this.aso.ol();
        int i = 0;
        while (true) {
            if (i < (ol != null ? ol.size() : 0)) {
                arrayList.add(getString(R.string.f1464dir_root) + au.aGF + ((e) ol.elementAt(i)).abM);
                i++;
            } else {
                this.ast.d(arrayList);
                return;
            }
        }
    }

    private void b(String str, String str2, int i, int i2) {
        P(str, str2);
        this.ast = (UCSpinner) findViewById(R.id.f682bookmark_dlg_folder);
        this.asp = (Button) findViewById(R.id.f687submit);
        this.asp.setOnClickListener(this.asy);
        this.asq = (Button) findViewById(R.id.f688cancle);
        this.asq.setOnClickListener(this.asy);
        ArrayList arrayList = new ArrayList();
        arrayList.add(getString(R.string.f1464dir_root));
        Vector ol = this.aso.ol();
        int i3 = 0;
        while (true) {
            if (i3 >= (ol != null ? ol.size() : 0)) {
                break;
            }
            arrayList.add(getString(R.string.f1464dir_root) + au.aGF + ((e) ol.elementAt(i3)).abM);
            i3++;
        }
        this.ast.d(arrayList);
        Vector ol2 = com.uc.a.e.nR().ol();
        if (ol2 != null) {
            int size = ol2.size();
            for (int i4 = 0; i4 < size; i4++) {
                if (((e) ol2.get(i4)).abQ == i2) {
                    this.ast.eb(i4 + 1);
                    return;
                }
            }
        }
    }

    public void b(String str, String str2, int i) {
        Vector ol = this.aso.ol();
        e eVar = new e();
        eVar.abM = str;
        eVar.abL = str2;
        eVar.abS = this.abS;
        eVar.abP = false;
        e eVar2 = i == 0 ? null : (e) ol.elementAt(i - 1);
        int a2 = this.aso.a(eVar2, eVar);
        if (a2 == 0) {
            Toast.makeText(this, (int) R.string.f1169msg_add_success, 0).show();
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(129);
            }
            setResult(103);
            finish();
        }
        if (3 == a2) {
            c(eVar2, eVar, 1);
        }
    }

    public void c(final e eVar, final e eVar2, final int i) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this);
        builder.L(getString(R.string.f1396bookmarkisexists));
        builder.M(getString(R.string.f1397confirmcoverbookmark));
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                int a2 = com.uc.a.e.nR().oj().a(eVar, eVar2, i);
                if (a2 == 0) {
                    if (1 == i) {
                        Toast.makeText(ActivityBookmarkEx.this, (int) R.string.f1169msg_add_success, 0).show();
                    } else {
                        Toast.makeText(ActivityBookmarkEx.this, (int) R.string.f1173msg_edit_success, 0).show();
                    }
                } else if (1 == a2) {
                    Toast.makeText(ActivityBookmarkEx.this, (int) R.string.f1393bookmarknamenotnull, 0).show();
                } else if (2 == a2) {
                    Toast.makeText(ActivityBookmarkEx.this, (int) R.string.f1394bookmarkvalueotnull, 0).show();
                }
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(129);
                }
                ActivityBookmarkEx.this.setResult(103);
                ActivityBookmarkEx.this.finish();
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ActivityBookmarkEx.this.finish();
                ActivityBookmarkEx.this.setResult(102);
            }
        });
        builder.fh().show();
    }

    public void c(String str, String str2, int i, int i2) {
        Vector ol = this.aso.ol();
        e eVar = new e();
        eVar.abM = str;
        eVar.abL = str2;
        eVar.abQ = i2;
        eVar.abP = false;
        e eVar2 = i == 0 ? null : (e) ol.elementAt(i - 1);
        int b2 = this.aso.b(eVar2, eVar);
        if (b2 == 0) {
            Toast.makeText(this, (int) R.string.f1173msg_edit_success, 0).show();
            setResult(101);
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(129);
            }
            finish();
        } else if (3 == b2) {
            c(eVar2, eVar, 2);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 86 && i2 == -1) {
            finish();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        ActivityBrowser.g(this);
        this.aso = com.uc.a.e.nR().oj();
        RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(this).inflate((int) R.layout.f881bookmark_dialog_newbookmark, (ViewGroup) null);
        relativeLayout.setBackgroundDrawable(com.uc.h.e.Pb().getDrawable(UCR.drawable.aWD));
        relativeLayout.findViewById(R.id.divider).setBackgroundDrawable(com.uc.h.e.Pb().getDrawable(UCR.drawable.aUo));
        int color = com.uc.h.e.Pb().getColor(46);
        ((TextView) relativeLayout.findViewById(R.id.f685bookmarkname)).setTextColor(color);
        ((TextView) relativeLayout.findViewById(R.id.f686bookmarkaddress)).setTextColor(color);
        ((TextView) relativeLayout.findViewById(R.id.f681t_msg)).setTextColor(color);
        setContentView(relativeLayout);
        Intent intent = getIntent();
        String action = intent.getAction();
        if (action != null) {
            if (asd.equals(action)) {
                ((TextView) findViewById(R.id.f654Browser_TitleBar)).setText((int) R.string.f1024controlbar_newbookmark);
                String stringExtra = intent.getStringExtra(asf);
                String stringExtra2 = intent.getStringExtra(asg);
                this.abS = intent.getByteExtra(asi, this.abS);
                if (stringExtra != null && stringExtra.length() > 1) {
                    this.asx = true;
                }
                Q(stringExtra, stringExtra2);
            }
            if (ase.equals(action)) {
                ((TextView) findViewById(R.id.f654Browser_TitleBar)).setText((int) R.string.f1132dialog_title_editbookmark);
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    String string = extras.getString(asf);
                    String string2 = extras.getString(asg);
                    this.asu = extras.getInt(asj);
                    this.asv = extras.getInt(ash);
                    if (string != null && string.length() > 1) {
                        this.asx = true;
                    }
                    b(string, string2, this.asu, this.asv);
                }
            }
        }
        this.asw = true;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i != 84) {
            return super.onKeyUp(i, keyEvent);
        }
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a((int) ModelBrowser.zJ, this);
        }
        return true;
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z && ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(this);
        }
        if (z && ActivityBrowser.Fz()) {
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
            }
            ActivityBrowser.e(this);
        }
        if (z && this.asw && (!this.asx || this.asu != -1)) {
            this.asr.post(new Runnable() {
                public void run() {
                    ((InputMethodManager) ActivityBookmarkEx.this.getSystemService("input_method")).toggleSoftInput(0, 1);
                }
            });
        }
        this.asw = false;
    }
}
