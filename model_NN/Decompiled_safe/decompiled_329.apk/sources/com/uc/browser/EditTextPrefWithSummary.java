package com.uc.browser;

import android.content.Context;
import android.preference.EditTextPreference;
import android.util.AttributeSet;

public class EditTextPrefWithSummary extends EditTextPreference {
    public EditTextPrefWithSummary(Context context) {
        super(context);
    }

    public EditTextPrefWithSummary(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onDialogClosed(boolean z) {
        super.onDialogClosed(z);
        setSummary(getText());
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(boolean z, Object obj) {
        super.onSetInitialValue(z, obj);
        setSummary(getText());
    }
}
