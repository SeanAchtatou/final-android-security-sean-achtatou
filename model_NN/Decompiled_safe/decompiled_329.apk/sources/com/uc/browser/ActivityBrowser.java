package com.uc.browser;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Process;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.DownloadListener;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;
import b.a.a.a.t;
import b.b.b;
import com.uc.a.h;
import com.uc.a.n;
import com.uc.browser.UCR;
import com.uc.c.bx;
import com.uc.c.cd;
import com.uc.d.c;
import com.uc.d.d;
import com.uc.d.f;
import com.uc.d.g;
import com.uc.e.z;
import com.uc.h.a;
import com.uc.h.e;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

public class ActivityBrowser extends ActivityWithUCMenu implements View.OnCreateContextMenuListener, DownloadListener {
    private static final byte bqA = 1;
    private static final byte bqB = 2;
    public static final byte bqC = 0;
    public static final byte bqD = 2;
    public static final byte bqE = 1;
    public static boolean bqO = false;
    public static boolean bqP = false;
    public static final String bqW = "pref_key_popup_anim";
    public static final String bqX = "pref_key_popup_anim_slow_counter";
    public static final long bqY = 40;
    public static final long bqZ = 30;
    private static final byte[] bqs = {-1, -3};
    public static boolean bqy = false;
    static final String bqz = "uc_destroy";
    /* access modifiers changed from: private */
    public int bqF = 0;
    private byte bqG = 2;
    private RelativeLayout bqH;
    /* access modifiers changed from: private */
    public Button bqI = null;
    /* access modifiers changed from: private */
    public Drawable bqJ;
    /* access modifiers changed from: private */
    public Drawable bqK;
    private int bqL = 2500;
    private a bqM = new a() {
        public void k() {
            Drawable unused = ActivityBrowser.this.bqJ = e.Pb().getDrawable(UCR.drawable.kR);
            Drawable unused2 = ActivityBrowser.this.bqK = e.Pb().getDrawable(UCR.drawable.kS);
            Button c = ActivityBrowser.this.bqI;
            if (c == null) {
                return;
            }
            if (ActivityBrowser.this.bqF == 0) {
                c.setBackgroundDrawable(ActivityBrowser.this.bqJ);
            } else {
                c.setBackgroundDrawable(ActivityBrowser.this.bqK);
            }
        }
    };
    private boolean bqN = false;
    private int bqQ = -1;
    private int[] bqR = {25, 25, 25, 25, 25};
    private c bqS;
    private f bqT;
    private d bqU = new d() {
        public boolean a(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case 1:
                    ActivityBrowser.this.startActivity(new Intent(ActivityBrowser.this, ActivityDebug.class));
                    break;
                case 2:
                    ActivityBrowser.this.startActivity(new Intent(ActivityBrowser.this, ActivityUploadFont.class));
                    break;
                case 3:
                    for (int i = 0; i < 20; i++) {
                        b.a.a.e eVar = new b.a.a.e();
                        eVar.abS = 0;
                        eVar.time = System.currentTimeMillis();
                        eVar.abL = "adder" + System.currentTimeMillis();
                        eVar.abM = "adder" + System.currentTimeMillis();
                        com.uc.a.e.nR().i(eVar);
                    }
                    break;
                case 6:
                    ActivityBrowser.this.startActivity(new Intent(ActivityBrowser.this, ActivityFlash.class));
                    break;
                case 9:
                    String str = null;
                    str.substring(0);
                    break;
                case 16:
                    int[] iArr = new int[1];
                    iArr[1] = iArr[1] + 1;
                    break;
                case 17:
                    Bitmap.createBitmap(10000, bx.bAs, Bitmap.Config.ARGB_8888);
                    break;
                case 18:
                    String str2 = (String) new Integer(1);
                    break;
                case 19:
                    String wR = b.a.a.f.wR();
                    StringBuffer stringBuffer = new StringBuffer();
                    Log.i("input", wR);
                    for (int append : com.uc.a.e.qC()) {
                        stringBuffer.append(append).append(cd.bVH);
                    }
                    Log.i("t1&t3", stringBuffer.toString());
                    Toast.makeText(ActivityBrowser.this, "custom-" + wR + ",  t1&t3-" + stringBuffer.toString(), 1).show();
                    break;
                case 21:
                    ModelBrowser.gD().aS(ModelBrowser.Ag);
                    break;
            }
            return false;
        }
    };
    private int bqV = -1;
    private final int bqt = 1;
    private IntentFilter bqu;
    private BroadcastReceiver bqv;
    private final byte bqw = 0;
    private final byte bqx = 1;

    class createLeavingThread extends Thread {
        private createLeavingThread() {
        }

        public void run() {
            ActivityBrowser.this.Fs();
        }
    }

    public static int Fq() {
        try {
            InputStream inputStream = Runtime.getRuntime().exec("cat /proc/stat").getInputStream();
            byte[] bArr = new byte[10000];
            int i = 0;
            while (true) {
                int read = inputStream.read(bArr, i, 1000);
                if (read < 0) {
                    break;
                }
                i += read;
            }
            inputStream.close();
            String str = new String(bArr, 0, i);
            InputStream inputStream2 = Runtime.getRuntime().exec("cat /proc/cpuinfo").getInputStream();
            byte[] bArr2 = new byte[10000];
            int i2 = 0;
            while (true) {
                int read2 = inputStream2.read(bArr2, i2, 1000);
                if (read2 < 0) {
                    break;
                }
                i2 += read2;
            }
            inputStream2.close();
            String str2 = new String(bArr2, 0, i2);
            InputStream inputStream3 = Runtime.getRuntime().exec("cat /proc/stat").getInputStream();
            byte[] bArr3 = new byte[10000];
            int i3 = 0;
            while (true) {
                int read3 = inputStream3.read(bArr3, i3, 1000);
                if (read3 < 0) {
                    break;
                }
                i3 += read3;
            }
            inputStream3.close();
            String str3 = new String(bArr3, 0, i3);
            Log.e("CHECK CPU", str);
            Log.e("CHECK CPU", str3);
            Log.e("CHECK CPU", str2);
            int i4 = 0;
            while (i4 < bArr2.length) {
                StringBuffer stringBuffer = new StringBuffer();
                if (bArr2[i4] != 66 || !"BogoMIPS".startsWith(new String(bArr2, i4, 7))) {
                    i4++;
                } else {
                    int i5 = i4 + 7;
                    boolean z = false;
                    while (i5 < bArr2.length) {
                        if (bArr2[i5] > 57 || bArr2[i5] < 48) {
                            if (z) {
                                break;
                            }
                            i5++;
                        } else {
                            z = true;
                            stringBuffer.append((char) bArr2[i5]);
                            i5++;
                        }
                    }
                    return Integer.valueOf(stringBuffer.toString()).intValue();
                }
            }
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public static int Fw() {
        return Integer.valueOf(com.uc.a.e.nR().bw(h.afB)).intValue();
    }

    public static boolean Fz() {
        return com.uc.a.e.nR().nY().bw(h.afN).equals(com.uc.a.e.RD);
    }

    public static float a(Activity activity, int i) {
        if ((i >= 0 && i < 10) || i > 255) {
            return -1.0f;
        }
        WindowManager.LayoutParams attributes = activity.getWindow().getAttributes();
        float f = ((float) i) / 255.0f;
        attributes.screenBrightness = f;
        activity.getWindow().setAttributes(attributes);
        return f;
    }

    public static boolean a(Activity activity, Configuration configuration) {
        if (Fw() != 0) {
            return false;
        }
        if (configuration.hardKeyboardHidden == 1) {
            activity.setRequestedOrientation(-1);
        } else if (configuration.hardKeyboardHidden == 2) {
            activity.setRequestedOrientation(-1);
        }
        return true;
    }

    public static void d(Activity activity) {
        if (activity.getResources().getConfiguration().hardKeyboardHidden == 1) {
            activity.setRequestedOrientation(-1);
            return;
        }
        int Fw = Fw();
        if (Fw == 0) {
            if (activity.getRequestedOrientation() != -1) {
                activity.setRequestedOrientation(-1);
            }
        } else if (Fw == 1) {
            if (activity.getRequestedOrientation() != 0) {
                activity.setRequestedOrientation(0);
            }
        } else if (Fw == 2 && activity.getRequestedOrientation() != 1) {
            activity.setRequestedOrientation(1);
        }
    }

    public static float e(Activity activity) {
        int i;
        try {
            i = Integer.parseInt(com.uc.a.e.nR().nY().bw(h.afv));
        } catch (Exception e) {
            i = 25;
        }
        if ((i >= 0 && i < 10) || i > 255) {
            return -1.0f;
        }
        WindowManager.LayoutParams attributes = activity.getWindow().getAttributes();
        float f = ((float) i) / 255.0f;
        attributes.screenBrightness = f;
        activity.getWindow().setAttributes(attributes);
        return f;
    }

    private static void f(Activity activity) {
        if (bqy) {
            activity.getWindow().setFlags(1024, 3072);
        } else {
            activity.getWindow().setFlags(2048, 3072);
        }
    }

    public static void g(Activity activity) {
        d(activity);
        f(activity);
    }

    public void FA() {
        if (this.bqS == null) {
            this.bqS = new c(this);
        }
        this.bqS.clear();
        if (this.bqT == null) {
            this.bqT = new f(this);
            this.bqT.a();
        }
        this.bqT.a(this.bqS);
        this.bqS.a(this.bqT);
        this.bqT.a(this.bqU);
        this.bqS.cV();
        g.a(this, com.uc.d.h.ccj, this.bqS);
        this.bqT.show();
    }

    public void Fr() {
        new createLeavingThread().start();
    }

    public void Fs() {
        try {
            b.b.a.d(this);
            b.a.d(this);
            this.bqu = new IntentFilter();
            this.bqu.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            this.bqv = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                        ActivityBrowser.this.bA(!intent.getBooleanExtra("noConnectivity", false));
                        NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra("networkInfo");
                        if (networkInfo != null && networkInfo.isConnected()) {
                            b.b.a.OR();
                            String JT = com.uc.f.g.Jn().JT();
                            String PS = b.PS();
                            if (JT == null || JT.length() == 0 || (PS != null && !JT.equals(PS))) {
                                com.uc.f.g.Jn().fh(PS);
                                com.uc.f.g.Jn().update();
                            }
                        }
                    }
                }
            };
            b.a.a.f.cL(b.a.a.f.auu);
            if (true == b.a.a.f.cO(b.a.a.f.auv)) {
                if (b.a.a.f.wT() == 0) {
                    b.a.a.f.l(1, b.a.a.f.atN);
                } else if (1 == b.a.a.f.wT()) {
                    b.a.a.f.l(1, b.a.a.f.atO);
                }
            }
            b.a.a.f.l(1, b.a.a.f.aua);
            com.uc.a.e.nR().pm();
            Ft();
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().hZ();
            }
            onNewIntent(getIntent());
        } catch (Exception e) {
        }
    }

    public void Ft() {
        try {
            b.a.a.f.c((byte) 0, false);
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().onResume();
            }
            registerReceiver(this.bqv, this.bqu);
            WebView.enablePlatformNotifications();
        } catch (Exception e) {
        }
    }

    public void Fu() {
        showDialog(1);
    }

    public void Fv() {
        Notification notification = new Notification();
        notification.defaults = 1;
        ((NotificationManager) getSystemService("notification")).notify(0, notification);
    }

    public float Fx() {
        return getWindow().getAttributes().screenBrightness;
    }

    public float Fy() {
        int height = getWindowManager().getDefaultDisplay().getHeight();
        int width = getWindowManager().getDefaultDisplay().getWidth();
        return width > height ? (float) height : (float) width;
    }

    public void aN(int i) {
        this.bqV = i;
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
        edit.putInt(bqW, i);
        if (i == 1) {
            edit.putInt(bqX, 0);
        }
        edit.commit();
    }

    public void bA(boolean z) {
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().D(z);
        }
    }

    public int bB(boolean z) {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        int i = defaultSharedPreferences.getInt(bqX, 0) + (z ? 1 : -1);
        defaultSharedPreferences.edit().putInt(bqX, i).commit();
        return i;
    }

    public void destroy() {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (true == defaultSharedPreferences.getBoolean(bqz, false)) {
            defaultSharedPreferences.edit().putBoolean(bqz, false).commit();
            b.b.a.close();
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().onDestroy();
                ModelBrowser.gF();
            }
        }
        Process.killProcess(Process.myPid());
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (isFinishing()) {
            return false;
        }
        if (keyEvent.getKeyCode() != 84 || keyEvent.getAction() != 1) {
            return super.dispatchKeyEvent(keyEvent);
        }
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().aS(120);
        }
        return true;
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (isFinishing()) {
            return false;
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public int gR() {
        if (this.bqV < 0) {
            this.bqV = PreferenceManager.getDefaultSharedPreferences(this).getInt(bqW, -1);
        }
        return this.bqV;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().onActivityResult(i, i2, intent);
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().onConfigurationChanged(configuration);
        }
        d((Activity) this);
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        return ModelBrowser.gD() != null ? ModelBrowser.gD().onContextItemSelected(menuItem) : super.onContextItemSelected(menuItem);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        CookieSyncManager.getInstance().startSync();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean(bqz, false).commit();
        com.uc.a.e.Rn = true;
        requestWindowFeature(1);
        ModelBrowser gD = ModelBrowser.gD();
        setPersistent(true);
        gD.Aw = (int) getResources().getDimension(R.dimen.f236titlebar_height);
        gD.Ax = (int) getResources().getDimension(R.dimen.f237statusbar_height);
        gD.Ay = (int) getResources().getDimension(R.dimen.f239controlbar_port_height);
        gD.Az = (int) getResources().getDimension(R.dimen.f238controlbar_land_height);
        if (com.uc.b.b.pE == null || com.uc.b.b.pE.length() == 0) {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService("phone");
            com.uc.b.b.pE = telephonyManager.getDeviceId();
            com.uc.a.e.nR().br(com.uc.b.b.pE);
            com.uc.a.e.nR().bs(telephonyManager.getSubscriberId());
        }
        gD.b(this);
        int scaledTouchSlop = ViewConfiguration.get(this).getScaledTouchSlop();
        z.hL(scaledTouchSlop);
        com.uc.a.e.bh(scaledTouchSlop);
        com.uc.a.e.an(e.Pb().kp(R.dimen.f462page_seperator_height), e.Pb().kp(R.dimen.f463reading_mode_tail_height));
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().onCreateContextMenu(contextMenu, view, contextMenuInfo);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        BroadcastReceiver gH;
        super.onDestroy();
        ModelBrowser gD = ModelBrowser.gD();
        if (!(gD == null || (gH = gD.gH()) == null)) {
            try {
                unregisterReceiver(gH);
            } catch (Exception e) {
            }
        }
        bqP = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
     arg types: [int, int]
     candidates:
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void */
    public void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        ModelBrowser gD = ModelBrowser.gD();
        if (gD != null) {
            if (22 != b.a.a.d.cu(str) && 22 != b.a.a.d.cu(URLUtil.guessFileName(str, str3, str4)) && !str4.equals("video/ucs") && (str3 == null || !str3.contains("filename=") || !str3.contains(".ucs"))) {
                if (str3 == null || !str3.regionMatches(true, 0, "attachment", 0, 10)) {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setDataAndType(Uri.parse(str), str4);
                    if (getPackageManager().queryIntentActivities(intent, t.bib).size() != 0) {
                        try {
                            startActivity(intent);
                            return;
                        } catch (ActivityNotFoundException e) {
                        }
                    }
                }
                String[] strArr = new String[5];
                strArr[0] = str;
                strArr[1] = ModelBrowser.gD().hX();
                CookieSyncManager.createInstance(getApplicationContext());
                strArr[2] = CookieManager.getInstance().getCookie(str);
                if (strArr[1] != null && (strArr[2] == null || strArr[2].length() == 0)) {
                    strArr[2] = CookieManager.getInstance().getCookie(strArr[1]);
                }
                String guessFileName = URLUtil.guessFileName(str, str3, str4);
                if (guessFileName.length() != guessFileName.getBytes().length) {
                    try {
                        String str5 = new String(guessFileName.getBytes("iso8859-1"), "gb2312");
                        try {
                            guessFileName = new String(str5.getBytes(), "utf-8");
                        } catch (UnsupportedEncodingException e2) {
                            guessFileName = str5;
                        }
                    } catch (UnsupportedEncodingException e3) {
                    }
                }
                strArr[3] = guessFileName;
                strArr[4] = "down:webkit";
                ModelBrowser.gD().e(strArr);
            } else if (!com.uc.b.g.i(this, "uc.ucplayer")) {
                gD.a(90, (Object) 2);
            } else {
                b.a.a.d.e(this, str);
            }
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (ModelBrowser.gD() == null || true != ModelBrowser.gD().onKeyDown(i, keyEvent)) {
            return super.onKeyDown(i, keyEvent);
        }
        return true;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (ModelBrowser.gD() == null || true != ModelBrowser.gD().onKeyUp(i, keyEvent)) {
            return super.onKeyUp(i, keyEvent);
        }
        return true;
    }

    /* JADX WARN: Type inference failed for: r3v1, types: [java.lang.String[]] */
    /* JADX WARN: Type inference failed for: r3v10, types: [java.lang.String[]] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onNewIntent(android.content.Intent r10) {
        /*
            r9 = this;
            r8 = 1
            r7 = 39
            r6 = 0
            r3 = 58
            r5 = 2
            com.uc.browser.ModelBrowser r0 = com.uc.browser.ModelBrowser.gD()
            if (r0 != 0) goto L_0x000e
        L_0x000d:
            return
        L_0x000e:
            com.uc.browser.ModelBrowser r0 = com.uc.browser.ModelBrowser.gD()
            r0.P()
            java.lang.String r0 = com.uc.browser.ActivityUpdate.IL
            java.lang.String r1 = com.uc.browser.ActivityUpdate.ck
            if (r0 == 0) goto L_0x000d
            java.lang.String r2 = "search_action:"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x009f
            int r2 = r0.indexOf(r3)
            int r2 = r2 + 1
            java.lang.String r0 = r0.substring(r2)
            if (r0 == 0) goto L_0x0081
            com.uc.a.e r2 = com.uc.a.e.nR()
            com.uc.a.l r2 = r2.nZ()
            java.lang.String r3 = "ext:dt_search/"
            boolean r3 = r0.startsWith(r3)
            if (r3 == 0) goto L_0x0085
            r3 = 47
            int r3 = r0.indexOf(r3)
            int r3 = r3 + 1
            java.lang.String r3 = r0.substring(r3)
            int r4 = r2.oL()
            byte r4 = (byte) r4
            java.lang.String r2 = r2.a(r3, r4)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r4 = 47
            int r4 = r0.indexOf(r4)
            int r4 = r4 + 1
            java.lang.String r0 = r0.substring(r6, r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
        L_0x0071:
            com.uc.browser.ModelBrowser r2 = com.uc.browser.ModelBrowser.gD()
            if (r1 == 0) goto L_0x007e
            java.lang.String[] r3 = new java.lang.String[r5]
            r3[r6] = r0
            r3[r8] = r1
            r0 = r3
        L_0x007e:
            r2.a(r7, r5, r0)
        L_0x0081:
            com.uc.browser.ActivityUpdate.kS()
            goto L_0x000d
        L_0x0085:
            java.lang.String r3 = "http://"
            boolean r3 = r0.startsWith(r3)
            if (r3 != 0) goto L_0x0071
            java.lang.String r3 = "https://"
            boolean r3 = r0.startsWith(r3)
            if (r3 != 0) goto L_0x0071
            int r3 = r2.oL()
            byte r3 = (byte) r3
            java.lang.String r0 = r2.a(r0, r3)
            goto L_0x0071
        L_0x009f:
            java.lang.String r2 = "view_action:"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x00d2
            int r2 = r0.indexOf(r3)
            int r2 = r2 + 1
            java.lang.String r0 = r0.substring(r2)
            java.lang.String r2 = "ext:lp:lp_ucl"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x00bf
            java.lang.String r0 = "ext:lp:lp_ucl"
            java.lang.String r0 = com.uc.browser.BuzData.bh(r0)
        L_0x00bf:
            if (r0 == 0) goto L_0x0081
            com.uc.browser.ModelBrowser r2 = com.uc.browser.ModelBrowser.gD()
            if (r1 == 0) goto L_0x00ce
            java.lang.String[] r3 = new java.lang.String[r5]
            r3[r6] = r0
            r3[r8] = r1
            r0 = r3
        L_0x00ce:
            r2.a(r7, r5, r0)
            goto L_0x0081
        L_0x00d2:
            java.lang.String r1 = "buffer_action:"
            boolean r1 = r0.startsWith(r1)
            if (r1 == 0) goto L_0x0104
            byte[] r1 = com.uc.browser.ActivityUpdate.IO
            if (r1 == 0) goto L_0x000d
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream
            r2.<init>(r1)
            java.io.DataInputStream r1 = new java.io.DataInputStream
            r1.<init>(r2)
            com.uc.a.e r2 = com.uc.a.e.nR()
            r2.n(r1)
            int r1 = r0.indexOf(r3)
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)
            if (r0 == 0) goto L_0x0081
            com.uc.browser.ModelBrowser r1 = com.uc.browser.ModelBrowser.gD()
            r1.a(r7, r5, r0)
            goto L_0x0081
        L_0x0104:
            java.lang.String r1 = "ucshare_action:"
            boolean r1 = r0.startsWith(r1)
            if (r1 == 0) goto L_0x0081
            int r1 = r0.indexOf(r3)
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)
            if (r0 == 0) goto L_0x0081
            com.uc.a.e r1 = com.uc.a.e.nR()
            java.lang.String r2 = com.uc.browser.ActivityUpdate.IM
            r1.bA(r2)
            com.uc.a.e r1 = com.uc.a.e.nR()
            r1.bB(r0)
            com.uc.a.e r0 = com.uc.a.e.nR()
            java.lang.String r1 = "1"
            r0.bC(r1)
            com.uc.browser.ModelBrowser r0 = com.uc.browser.ModelBrowser.gD()
            r1 = 95
            r0.aS(r1)
            goto L_0x0081
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.browser.ActivityBrowser.onNewIntent(android.content.Intent):void");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        CookieSyncManager.getInstance().sync();
        try {
            unregisterReceiver(this.bqv);
            WebView.disablePlatformNotifications();
            if (ModelBrowser.gD() != null) {
                b.a.a.f.c((byte) 1, false);
                ModelBrowser.gD().onPause();
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        CookieSyncManager.getInstance().stopSync();
        if (!com.uc.a.e.nR().pO()) {
            getResources();
            ModelBrowser.gE();
            n nVar = null;
            if (ModelBrowser.gD() != null) {
                nVar = ModelBrowser.gD().ch();
            }
            com.uc.a.e.nR().e(nVar);
            com.uc.a.e.nR().oE();
            com.uc.a.e.nR().qh();
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().gT();
            }
        }
        d((Activity) this);
        this.bqF = Fw();
        if (ModelBrowser.gD() != null && !ModelBrowser.gD().ih()) {
            Ft();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().hv();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.b(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, int):int
      com.uc.browser.ModelBrowser.b(android.content.Context, int):android.app.Dialog
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, android.content.Context):android.app.Dialog
      com.uc.browser.ModelBrowser.b(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, boolean):boolean
      com.uc.browser.ModelBrowser.b(boolean, boolean):void */
    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        ModelBrowser gD = ModelBrowser.gD();
        if (gD != null) {
            if (z) {
                gD.a(this);
                gD.aS(69);
            } else {
                gD.b(true, true);
            }
        }
        if (z && Fz()) {
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
            }
            e((Activity) this);
        }
    }

    public void t(String str) {
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().t(str);
        }
    }
}
