package com.uc.browser;

import android.telephony.CellLocation;
import android.telephony.cdma.CdmaCellLocation;

class CdmaCellLocationEx {
    CdmaCellLocationEx() {
    }

    public int a(CellLocation cellLocation) {
        if (cellLocation != null) {
            return ((CdmaCellLocation) cellLocation).getNetworkId();
        }
        return 0;
    }

    public int b(CellLocation cellLocation) {
        if (cellLocation != null) {
            return ((CdmaCellLocation) cellLocation).getBaseStationId();
        }
        return 0;
    }
}
